package dw.comm;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import com.moneygram.agentconnect.BusinessError;

import dw.i18n.Messages;

/** 
 * Thrown by MessageFacade. Subclassing RuntimeException because many
 * callers of the facade will invoke it in such as way that it won't 
 * throw any exceptions. So its dumb to force them to have catches.
 */
public class MessageFacadeError extends RuntimeException {
	private static final long serialVersionUID = 1L;

    private List<BusinessError> errors;
    
    public MessageFacadeError(int code, String text) {
    	errors = new ArrayList<BusinessError>();
    	addError(null, text, null, BigInteger.valueOf(code), BigInteger.ZERO);
    }
    
    public MessageFacadeError(String name, String text) {
    	errors = new ArrayList<BusinessError>();
    	addError(name, text, null, BigInteger.valueOf(MessageFacadeParam.OTHER_ERROR), BigInteger.ZERO);
    }
    
    public MessageFacadeError(int code, int subCode, String text, String subText) {
    	errors = new ArrayList<BusinessError>();
    	addError(null, text, subText, BigInteger.valueOf(code), BigInteger.valueOf(subCode));
    }

    public MessageFacadeError(Throwable e) {
    	errors = new ArrayList<BusinessError>();
    	String className = e.getClass().getSimpleName();
    	String errorText = e.getMessage();
        
        if (errorText == null) {
        	errorText = e.getClass().getName();
        } else {
        	errorText = makeFriendly(errorText);
        }
        
        addError(className, errorText, null, BigInteger.valueOf(MessageFacadeParam.OTHER_ERROR), BigInteger.ZERO);
    }
    
    public MessageFacadeError(List<BusinessError> businessErrors) {
    	errors = new ArrayList<BusinessError>();
    	if ((businessErrors != null) && (businessErrors.size() > 0)) {
    		for (BusinessError be : businessErrors) {
    			BusinessError ei = new BusinessError();
    			int code = 0;
    			try {
    				code = be.getErrorCode().intValue();
    			} catch (Exception e) {
    			}
    			ei.setMessageShort(be.getMessageShort());
    			ei.setMessage(be.getMessage());
    			ei.setDetailString(be.getDetailString());
    			ei.setErrorCode(BigInteger.valueOf(code));
    			ei.setSubErrorCode(be.getSubErrorCode() != null ? be.getSubErrorCode() : BigInteger.ZERO);
    			ei.setOffendingField(be.getOffendingField());
    			
    			errors.add(ei);
    		}
    	}
    }
    
	@Override
	public String getMessage() {
		StringBuffer sb = new StringBuffer();
		if (errors.size() > 0) {
			int c = 1;
			for (BusinessError error : errors) {
				if (errors.size() > 1) {
					sb.append(c).append(") ");
					if (c > 1) {
						sb.append("; ");
					}
				}
				sb.append(formatErrorMessageText(error.getMessageShort(), error.getMessage(), error.getDetailString(), error.getErrorCode(), error.getSubErrorCode(), error.getOffendingField()));
	    		c++;
			}
    		return sb.toString();
    	} else {
    		return "Error:";
    	}
    }
    
	public static String formatCommunicationsErrorMessageText(BusinessError error) {
		StringBuffer sb = new StringBuffer();
		sb.append("Communication Error\n");
		String shortMessage = error.getMessage();
		if ((shortMessage != null) && (! shortMessage.isEmpty())) {
			sb.append(shortMessage);
		}
		String message = error.getMessage();
		if ((message != null) && (! message.isEmpty())) {
			if (sb.length() > 0) {
				sb.append(": ");
			}
			sb.append(message);
		}
		return sb.toString();
	}

	public boolean hasErrorCode(int code) {
		for (BusinessError error : errors) {
			if ((error.getErrorCode() != null) && (error.getErrorCode().intValue() == code))  {
				return true;
			}
		}
		return false;
	}

	public boolean hasErrorCode(int code, int subCode) {
		for (BusinessError error : errors) {
			if ((error.getErrorCode() != null) && (error.getErrorCode().intValue() == code))  {
				return true;
			}
		}
		return false;
	}
	
    private String makeFriendly(String message) {
        int i = message.lastIndexOf("Exception:"); 
        if (i != -1) {
            return message.substring(i + 10).trim();
        }
        else {
            return message;
        }
    }

	public List<BusinessError> getErrors() {
		return errors;
	}
	
	public BusinessError addError(String messageShort, String message, String detailString, BigInteger code, BigInteger subCode) {
		BusinessError errorInfo = new BusinessError();
    	errorInfo.setMessageShort(messageShort);
    	errorInfo.setMessage(message);
    	errorInfo.setDetailString(detailString);
    	errorInfo.setSubErrorCode(subCode);
    	errorInfo.setErrorCode(code);
    	errors.add(errorInfo);
    	return errorInfo;
	}
	
	public int[] getErrorCodes() {
		int[] errorCodes = new int[errors.size()];
		int i = 0;
		for (BusinessError error : errors) {
			if (error.getErrorCode() != null) {
				errorCodes[i] = error.getErrorCode().intValue();
				i++;
			}
		}
		return errorCodes;
	}

	public String getErrorCodeList() {
		StringBuffer sb = new StringBuffer();
		boolean firstFlag = true;
		for (BusinessError error : errors) {
			if (error.getErrorCode() != null) {
				if (! firstFlag) {
					sb.append(", ");
				}
				sb.append(error.getErrorCode().intValue());
			}
		}
		return sb.toString();
	}

	public boolean errorCodesBetween(int lowErrorCode, int highErrorCode) {
		for (BusinessError error : errors) {
			if ((error.getErrorCode() != null) && (error.getErrorCode().intValue() >= lowErrorCode) && (error.getErrorCode().intValue() <= highErrorCode)) {
				return true;
			}
		}
		return false;
	}
	
	public static String formatErrorMessageText(BusinessError ei) {
		return formatErrorMessageText(ei.getMessageShort(), ei.getMessage(), ei.getDetailString(), ei.getErrorCode(), ei.getSubErrorCode(), ei.getOffendingField());
	}

	public static String formatErrorMessageText(String shortText, String errorText, String detailText, BigInteger code, BigInteger subcode, String field) {
		return formatErrorMessageText(shortText, errorText, detailText, code != null ? code.intValue() : 0, subcode != null ? subcode.intValue() : 0, field);
	}
	
	public static String formatErrorMessageText(String shortText, String errorText, String detailText, int code, int subcode, String field) {
		StringBuffer sb = new StringBuffer();
		if ((shortText != null) && (! shortText.isEmpty())) {
			sb.append(shortText.trim());
		}
		
		if ((errorText != null) && (! errorText.isEmpty())) {
			if (sb.length() > 0) {
				sb.append(" - ");
			}
			sb.append(errorText.trim());
		}
		
		if ((detailText != null) && (! detailText.isEmpty())) {
			if (sb.length() > 0) {
				sb.append(" - ");
			}
			sb.append(detailText.trim());
		}

		if ((code > 0) || (subcode > 0) || ((field != null) && (! field.isEmpty()))) {
			if (sb.length() > 0) {
				sb.append(" ");
			}
			sb.append("(");
			if (code > 0) {
				sb.append("AC " + Messages.getString("Error.1")).append(": ").append(code);
				if ((subcode > 0) || ((field != null) && (! field.isEmpty()))) {
					sb.append(", ");
				}
			}
			if (subcode > 0) {
				sb.append(Messages.getString("DataCollectionItem.Subcode")).append(": ").append(subcode);
				if ((field != null) && (! field.isEmpty())) {
					sb.append(", ");
				}
			}
			if ((field != null) && (! field.isEmpty())) {
				sb.append(Messages.getString("DataCollectionItem.Field")).append(": ").append(field);
			}
			sb.append(")");
		}
		return sb.toString();
	}
}
