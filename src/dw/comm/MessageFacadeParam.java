package dw.comm;


import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.BitSet;

import dw.profile.UnitProfile;
import dw.ras.RasDialParams;
import dw.utility.Debug;

/** 
 * Contains the desired configuration of the dialup connection, and the
 * list of error codes the transaction wants to handle itself. 
 */
public class MessageFacadeParam {

    /* Flags that control how the connection is established. */

    /** Indicates the transaction should never trigger a dialout. */
    public static final int NETWORK_ONLY = 0;

    /** Indicates the user is standing there waiting for the transaction. */
    public static final int REAL_TIME = 1;

    /** 
     * Indicates the user may have gone home for the day. No retry 
     * prompts should be issued, and special dialout retry logic applies.
     */
    public static final int NON_REAL_TIME = 2;
    /**      
    * Indicates that the user has entered MANUAL DIAL information.
    */
    public static final int MANUAL = 3;
    
    /**
     * Indicates that the transaction will run through the auto retry scenaria.
     * the auto retry will automatically increment the retry count for the MW04,
     * MW12, and MWAK records and after four attempts promp the user to retry using
     * the standard retry box.
     */
    public static final int AUTO_RETRY = 4;


    /** 
     * Indicates the transaction is happening in the background, and doesn't
     * interfere with foreground transactions. It's totally transparent to 
     * the user, so no retry prompts.
     */
    public static final int NO_DIALOG = 5;
    
    /**
    * Indicates that the transaction  is an initial install. This flag allows
    * the connection for initial install to work in ensure connection.  Because
    * durring the initial install the DEMO MODE flag it set.
    */
    public static final int INSTALL = 6;
    
    /**
     * Indicates that the transaction will need a predial.
     */
    public static final int WILL_NEED = 7;
   
    /**
     * Indicates that the dialog should not be hidden.
     */
    public static final int DO_NOT_HIDE = 8;
   
    public static final int HIDE_WITHOUT_FOCUS_UNTIL_ERROR = 9;
    
    /**
    * Indicates that the transaction has received an SOH message from the host.
    */
    public static final int SOH_RETRY = 715;

   
    /* Error codes that one or more callers may want to handle themselves. */
    
    public static final int USER_CANCEL = 50;
    public static final int INVALID_PIN = 387; 
    public static final int INVALID_REFERENCE_NUMBER = 604; 
    public static final int MG_FEE_RATE_CHANGED = 602;
    public static final int MG_CUSTOMER_NOT_FOUND = 616;
    public static final int MG_CARD_NOT_FOUND = 319;
   // public static final int MG_PAYOUT_CURRENCY_SELECTION = 52;  // failure code 616
    public static final int NO_HOST_RECORDS_FOUND = 623; //Used for MoneyGram reports
    public static final int NO_HOST_SEND_RECORDS_FOUND = 622; //Used for MoneyGram reports
//    public static final int NOT_MGS_HOME_DELIVERY = 54;
//    public static final int MGS_HOME_DELIVERY = 55;
    public static final int EP_INVALID_ACCOUNT = 615;
//    public static final int MG_SEND_INFO_INVALID = 57; // pre MC not used
    // This was going to be 624 in AC4.2, but they changed thier minds because soap fault
    // doesn't provide a convenient way to pass back the source ID. So back to 58.
    public static final int MO_OFAC_CHECK_FAILED = 58;	
    public static final int INVALID_CHECK_NUMBER = 603; 
    // Not Sure if needed - had to be removed if not - aep4 - 07/07/2015
    public static final int INVALID_TRANSACTION = 627;
    
    //add additional error codes above this line

    /** 
     * Error code for everything else. Callers should not try to handle this. 
     * If a caller comes along that wants to handle something currently reported
     * as an unknown error, then that error should be assigned a different number.
     */
    public static final int OTHER_ERROR = 49;
    
    // Error codes for I/O problems in the messaging layer. Callers should not 
    // attempt to handle themselves. I'm giving them numbers to improve the
    // consistency of comm-related errors.
    
    /** Contains all our parameter flags and error codes. */
    private BitSet bits;

    /** Keep track of redial attempts. */
    private int attempt = 0;
    private int diagAttempt = 0;

    // Flag to keep track of the number of AUTO_RETRY's made.
    private static int autoRetryCount = 0;
    private static final int MAXRETRYCOUNT = 4;
    
    private RasDialParams rasParams;

    public MessageFacadeParam(int param) {
        bits = new BitSet();
        bits.set(param);
        // Set the RasDialParams
        rasParams = getUnitProfileRasDialParams();
    }

    public MessageFacadeParam(int param1, int param2) {
        bits = new BitSet();
        bits.set(param1);
        bits.set(param2);
        // Set the RasDialParams
        rasParams = getUnitProfileRasDialParams();
    }
    
    public MessageFacadeParam(int param1, int param2, int param3) {
        bits = new BitSet();
        bits.set(param1);
        bits.set(param2);
        bits.set(param3);
        // Set the RasDialParams
        rasParams = getUnitProfileRasDialParams();
    }
    
    public MessageFacadeParam(int param1, int param2, int param3, int param4) {
        bits = new BitSet();
        bits.set(param1);
        bits.set(param2);
        bits.set(param3);
        bits.set(param4);
        // Set the RasDialParams
        rasParams = getUnitProfileRasDialParams();
    }
    
    public MessageFacadeParam(int param1, int param2, int param3, int param4, int param5) {
        bits = new BitSet();
        bits.set(param1);
        bits.set(param2);
        bits.set(param3);
        bits.set(param4);
        bits.set(param5);
        // Set the RasDialParams
        rasParams = getUnitProfileRasDialParams();
    }
    
    public MessageFacadeParam(int param1, int param2, int param3, int param4, int param5, int param6) {
        bits = new BitSet();
        bits.set(param1);
        bits.set(param2);
        bits.set(param3);
        bits.set(param4);
        bits.set(param5);
        bits.set(param6);
        // Set the RasDialParams
        rasParams = getUnitProfileRasDialParams();
    }
    
    
    // This constructor is used when the user enters dialing information
    // on the initial install screen.
    
    public MessageFacadeParam(int param1,
                              String hostSpecialDial,
                              String phoneNumber,
                              String userId,
                              String password) {
         bits = new BitSet();
         bits.set(MessageFacadeParam.MANUAL);
         bits.set(MessageFacadeParam.REAL_TIME);
         bits.set(param1);
         
         rasParams = new RasDialParams((hostSpecialDial + phoneNumber), userId, password);
    }

    // This constructor is used when the user chooses the default dialing information
    // on the initial install screen, but this takes in the data that the user entered
    // in the host special dial field.
    
    public MessageFacadeParam(int param1,
                              String hostSpecialDial) {
        bits = new BitSet();
        bits.set(MessageFacadeParam.MANUAL);
        bits.set(MessageFacadeParam.REAL_TIME);
        bits.set(param1);
        
        rasParams = new RasDialParams((hostSpecialDial + UnitProfile.getInstance().get("HOST_PRIMARY_PHONE", "")),	 
                                       UnitProfile.getInstance().get("PRIMARY_DUN_USER_ID", ""),	 
                                      UnitProfile.getInstance().get("PRIMARY_DUN_PASSWORD", ""));	 
     }

    /** Create a new instance of RasDialParams filling in the values from the
    * Unit Profile.
    * @author: Rob Morgan
    */
    private RasDialParams getUnitProfileRasDialParams() {
        
        return new RasDialParams(UnitProfile.getInstance().get("HOST_SPECIAL_DIAL", "") +	 
                                 UnitProfile.getInstance().get("HOST_PRIMARY_PHONE", ""),	 
                                 UnitProfile.getInstance().get("PRIMARY_DUN_USER_ID", ""),	 
                                 UnitProfile.getInstance().get("PRIMARY_DUN_PASSWORD", ""));	 
    }    

    /** 
     * Returns true if the given error number was specified when this
     * object was created, indicating that the caller wants to handle it.
     * Same as get(errorNumber).
     */
    public boolean callerWillHandle(int[] is) {
    	for (int code : is) {
    		if (! bits.get(code)) {
    			return false;
    		}
    	}
        return true;
    }

    /** Returns true if the specifed flag or error number is set. */    
    public boolean get(int param) {
        return bits.get(param);
    }

    /** 
     * Compute a random delay of between 7 and 10 minutes. Moved from main
     * panel. Supposedly required by federal regulation.
     */     
    public long getDiagDelay() {
        ++diagAttempt;
        if ((diagAttempt > 1) && get(NON_REAL_TIME)) {
			try {
				SecureRandom prng = SecureRandom.getInstance("SHA1PRNG");
	            return (Math.round(prng.nextDouble() * 3) + 7) * 60000;
			} catch (NoSuchAlgorithmException e) {
				Debug.printStackTrace(e);
	            return 0;
			}
        }
        else
            return 0;
    }        

    public int getDiagAttempt() {
         return diagAttempt;
    }
    
    
    /** Returns an instance of RasDialExtensions based on our params. */
//    public RasDialExtensions rasDialExtensions() {
//        //add support for modem speaker volume here
//        RasDialExtensions rasDialExtensions = new RasDialExtensions();
//        if (RasProperties.isMuted()) {
//            rasDialExtensions.setModemSpeakerEnabled(false);
//            rasDialExtensions.setModemSpeakerPhonebookSettingIgnored(true);
//        }
//        return rasDialExtensions;
//    }

    /** Returns an instance of RasDialParams based on our params. */
    public RasDialParams rasDialParams() {
        return rasParams;
    }
    
    /** handleAutoRetry is used to handle and process the autoRetry.  It will
        check if AUTO_RETRY is set as part of the objects parameters.  If it is
        not a part, handleAutoRetry will return false.  If so, it will compare 
        the autoRetryCount to MAXRETRYCOUNT, if it is less than maxRetryCount, 
        then true will be returned to the calling routine.  Otherwise, false will
        return false.  If autoRetryCount is not less than MAXRETRYCOUNT, then 
        set autoRetryCount to 0 and return false.
    */
    public boolean handleAutoRetry(){
        boolean result = false;
        if (get(AUTO_RETRY) && (autoRetryCount < MAXRETRYCOUNT)){
            ++autoRetryCount;
            result = true;
        }
        else{
            autoRetryCount = 0;
            result = false;
        }
        return result;
    }

	public int getAttempt() {
		return attempt;
	}

	public void setAttempt(int attempt) {
		this.attempt = attempt;
	}
	
	public void addParam(int param) {
		bits.set(param);
	}
}

