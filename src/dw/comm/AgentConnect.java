/*
 * AgentConnect.java
 * 
 * Copyright (c) 2004 MoneyGram, Inc. All rights reserved
 */

package dw.comm;

import java.math.BigInteger;
import java.net.Authenticator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.ws.Binding;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.MessageContext;

import com.moneygram.agentconnect.AgentConnectService;
import com.moneygram.agentconnect.AmendValidationRequest;
import com.moneygram.agentconnect.AmendValidationResponse;
import com.moneygram.agentconnect.BPValidationRequest;
import com.moneygram.agentconnect.BPValidationResponse;
import com.moneygram.agentconnect.BillPaymentDetailReportRequest;
import com.moneygram.agentconnect.BillPaymentDetailReportResponse;
import com.moneygram.agentconnect.BillerSearchRequest;
import com.moneygram.agentconnect.BillerSearchResponse;
import com.moneygram.agentconnect.BusinessError;
import com.moneygram.agentconnect.CheckInRequest;
import com.moneygram.agentconnect.CheckInResponse;
import com.moneygram.agentconnect.CityListRequest;
import com.moneygram.agentconnect.CityListResponse;
import com.moneygram.agentconnect.CompleteSessionRequest;
import com.moneygram.agentconnect.CompleteSessionResponse;
import com.moneygram.agentconnect.ComplianceTransactionRequest;
import com.moneygram.agentconnect.ComplianceTransactionResponse;
import com.moneygram.agentconnect.ConfirmTokenRequest;
import com.moneygram.agentconnect.ConfirmTokenResponse;
import com.moneygram.agentconnect.ConsumerHistoryLookupRequest;
import com.moneygram.agentconnect.ConsumerHistoryLookupResponse;
import com.moneygram.agentconnect.CreateOrUpdateProfileReceiverRequest;
import com.moneygram.agentconnect.CreateOrUpdateProfileReceiverResponse;
import com.moneygram.agentconnect.CreateOrUpdateProfileSenderRequest;
import com.moneygram.agentconnect.CreateOrUpdateProfileSenderResponse;
import com.moneygram.agentconnect.DirectoryOfAgentsByAreaCodePrefixRequest;
import com.moneygram.agentconnect.DirectoryOfAgentsByAreaCodePrefixResponse;
import com.moneygram.agentconnect.DirectoryOfAgentsByCityRequest;
import com.moneygram.agentconnect.DirectoryOfAgentsByCityResponse;
import com.moneygram.agentconnect.DirectoryOfAgentsByZipRequest;
import com.moneygram.agentconnect.DirectoryOfAgentsByZipResponse;
import com.moneygram.agentconnect.DoddFrankStateRegulatorInfoRequest;
import com.moneygram.agentconnect.DoddFrankStateRegulatorInfoResponse;
import com.moneygram.agentconnect.DwInitialSetupRequest;
import com.moneygram.agentconnect.DwInitialSetupResponse;
import com.moneygram.agentconnect.DwPasswordRequest;
import com.moneygram.agentconnect.DwPasswordResponse;
import com.moneygram.agentconnect.DwProfileRequest;
import com.moneygram.agentconnect.DwProfileResponse;
import com.moneygram.agentconnect.FeeLookupRequest;
import com.moneygram.agentconnect.FeeLookupResponse;
import com.moneygram.agentconnect.GetBankDetailsByLevelRequest;
import com.moneygram.agentconnect.GetBankDetailsByLevelResponse;
import com.moneygram.agentconnect.GetBankDetailsRequest;
import com.moneygram.agentconnect.GetBankDetailsResponse;
import com.moneygram.agentconnect.GetBroadcastMessagesResponse;
import com.moneygram.agentconnect.GetCountryInfoRequest;
import com.moneygram.agentconnect.GetCountryInfoResponse;
import com.moneygram.agentconnect.GetCountrySubdivisionRequest;
import com.moneygram.agentconnect.GetCountrySubdivisionResponse;
import com.moneygram.agentconnect.GetCurrencyInfoRequest;
import com.moneygram.agentconnect.GetCurrencyInfoResponse;
import com.moneygram.agentconnect.GetProfileReceiverRequest;
import com.moneygram.agentconnect.GetProfileReceiverResponse;
import com.moneygram.agentconnect.GetProfileSenderRequest;
import com.moneygram.agentconnect.GetProfileSenderResponse;
import com.moneygram.agentconnect.GetServiceOptionsRequest;
import com.moneygram.agentconnect.GetServiceOptionsResponse;
import com.moneygram.agentconnect.MoneyGramReceiveDetailReportRequest;
import com.moneygram.agentconnect.MoneyGramReceiveDetailReportResponse;
import com.moneygram.agentconnect.MoneyGramSendDetailReportRequest;
import com.moneygram.agentconnect.MoneyGramSendDetailReportResponse;
import com.moneygram.agentconnect.MoneyGramSendDetailReportWithTaxRequest;
import com.moneygram.agentconnect.MoneyGramSendDetailReportWithTaxResponse;
import com.moneygram.agentconnect.MoneyGramSendWithTaxDetailInfo;
import com.moneygram.agentconnect.MoneyOrderTotalRequest;
import com.moneygram.agentconnect.MoneyOrderTotalResponse;
import com.moneygram.agentconnect.OpenOTPLoginResponse;
import com.moneygram.agentconnect.Payload;
import com.moneygram.agentconnect.ProfileChangeRequest;
import com.moneygram.agentconnect.ReceiptsFormatDetailsRequest;
import com.moneygram.agentconnect.ReceiptsFormatDetailsResponse;
import com.moneygram.agentconnect.ReceiveReversalValidationRequest;
import com.moneygram.agentconnect.ReceiveReversalValidationResponse;
import com.moneygram.agentconnect.ReceiveValidationRequest;
import com.moneygram.agentconnect.ReceiveValidationResponse;
import com.moneygram.agentconnect.RegisterHardTokenResponse;
import com.moneygram.agentconnect.Request;
import com.moneygram.agentconnect.Response;
import com.moneygram.agentconnect.SaveDebugDataRequest;
import com.moneygram.agentconnect.SaveSubagentsRequest;
import com.moneygram.agentconnect.SaveSubagentsResponse;
import com.moneygram.agentconnect.SearchConsumerProfilesRequest;
import com.moneygram.agentconnect.SearchConsumerProfilesResponse;
import com.moneygram.agentconnect.SearchStagedTransactionsRequest;
import com.moneygram.agentconnect.SearchStagedTransactionsResponse;
import com.moneygram.agentconnect.SendReversalValidationRequest;
import com.moneygram.agentconnect.SendReversalValidationResponse;
import com.moneygram.agentconnect.SendValidationRequest;
import com.moneygram.agentconnect.SendValidationResponse;
import com.moneygram.agentconnect.SubagentsRequest;
import com.moneygram.agentconnect.SubagentsResponse;
import com.moneygram.agentconnect.SystemError;
import com.moneygram.agentconnect.SystemError_Exception;
import com.moneygram.agentconnect.TransactionLookupRequest;
import com.moneygram.agentconnect.TransactionLookupResponse;
import com.moneygram.agentconnect.VersionManifestRequest;
import com.moneygram.agentconnect.VersionManifestResponse;
import com.sun.xml.internal.ws.api.message.Header;
import com.sun.xml.internal.ws.api.message.Headers;
import com.sun.xml.internal.ws.developer.WSBindingProvider;

import dw.profile.UnitProfile;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.TimeUtility;
import flags.CompileTimeFlags;

/**
 * Handles I/O to and from the AgentConnect middleware. This class 
 * replaces Transceiver, MessageDescriptor, MessageForwarder, and
 * various subclasses of MessageForwarder (AgentDirectoryDemoForwarder, 
 * AgentDirectoryMessageForwarder, InstallMessageForwarder, Item
 * Forwarder, MoneyGramMessageForwarder, MoneyGramDemoForwarder, 
 * ProfileMessageForwarder, and SoftwareUpdateMessageForwarder).
 * 
 * Some of those classes may contain code that goes beyond handling
 * I/O to and from the middleware. Those responsibilities will need
 * to be assigned elsewhere.  
 *  
 * @author Geoff Atkin
 */
public class AgentConnect {
	
	public static final String API_VERSION = "1705";
	public static final String TARGET_NAMESPACE = "http://www.moneygram.com/AgentConnect" + API_VERSION;
	
	private static AgentConnectService agentConnectService = null;
	private static com.moneygram.agentconnect.AgentConnect agentConnect = null;
	private static boolean useCompression = true;
	
	/**
	 * Format for parsing incoming timestamps from RTS.
	 * The format used by AC4 is 2004-09-02T09:50:45.651-05:00
	 * which is slightly different than the format we use for outgoing
	 * timestamps, which is defined in AgentConnectTemplates.
	 */
	
	
	
	// Prior to Agent Connect API migration, the caller of the messaging
	// code had to choose whether to use a demo forwarder or a real one.
	// This is replaced by the four flags below, which specify the rule
	// for choosing between live messaging to the middleware or canned 
	// responses. 
	//
	// When we have time, MessageFacade.ensureConnection() should be
	// modified to take these flags into account, so that we don't
	// dial out if we're going to fake the response message.
	
	/** 
	 * Use canned data even if we're not in demo or training mode.
	 * This is appropriate for messages that aren't implemented 
	 * in the middleware yet. 
	 */
	public static final int ALWAYS_FAKE = 0;
	
	/** 
	 * Send live messages to the middleware even in demo mode.
	 * This is appropriate for the initial setup message. 
	 */
	public static final int ALWAYS_LIVE = 1;
	
	/** 
	 * Use canned data in demo and training mode, otherwise live.
	 * This is appropriate for typical financial transactions. 
	 */
	public static final int AUTOMATIC = 2;
	
	/** 
	 * Use canned data in demo mode, but send live messages in 
	 * training mode. This is appropriate for daily transmit stuff
	 * like check-in and profile exchange.  
	 */
	public static final int LIVE_TRAINING = 3;
	
	private SOAPFactory factory;
	private String username;
	
	public AgentConnect() {
		try {
			factory = SOAPFactory.newInstance();
			String agentId = UnitProfile.getInstance().get("AGENT_ID", "");
			String seq = UnitProfile.getInstance().get("AGENT_POS_SEQ", "1");
	        username = agentId + seq;
		} catch (SOAPException e) {
			Debug.printException(e);
		}
	}
	
	public static final boolean KEEP_ALIVE = false;
	
	public static final boolean AUDIT_ENCRYPTION = false;
	public static boolean debugEncryption = false;
	
	public static void enableDebugEncryption() {
		debugEncryption = true;
	}
	
	public static void disableDebugEncryption() {
		debugEncryption = false;
	}
	
	static {
		Authenticator.setDefault(new AgentConnectAuthenticator());
	}
	
	public static void setUseCompression(boolean useCompression) {
		AgentConnect.useCompression = useCompression;
	}
	
	private Response processResponse(Response response, Payload payload, boolean fake, String token, boolean throwBusinessErrors) throws MessageFacadeError {
		
		if (response != null) {
			if (! fake) {
				if (token != null) {
					tokenUpdate(token);
				}

				if ((payload != null) && (payload.getTimeStamp() != null)) {
					try {
						TimeUtility.synchronize(TimeUtility.toCalendar(payload.getTimeStamp()));
					} catch (Exception e) {
						Debug.println("Exception while synchronizing: " + e);
						Debug.println("  serverTimeStamp=" + payload.getTimeStamp());
					}
				}
				
				// Process the flags tag from the response.
				
				try {
					int flags = payload != null ? payload.getFlags() : 0;
					Debug.println("CheckIn flags = " + flags);

					if (flags != 0) {
						// check if profile at host has changed
						if ((flags & 2) != 0) {
							Debug.println("     profile changed ");
							UnitProfile.getInstance().setProfileChanged();
						}
						// check if there is new software to download.
						if ((flags & 8) != 0) {
							//SLS Bug 593 - Don't do software downloads if we are currently acting
							//as a location profile
							// BUG 830 - implement bug 593 correctly.
							//if (UnitProfile.getInstance().isSuperAgent()) {
							if (!UnitProfile.getInstance().isSubAgent()) {
								Debug.println("     software pending ");
								UnitProfile.getInstance().setSoftwareVersionPending(true);
								// trigger this to pick up any new profile items
								UnitProfile.getInstance().setProfileChanged();
							}
						}
						
						// check if state regulator info is out of date
						if ((flags & 32) != 0) {
							Debug.println("     State Regulator Info pending");
							UnitProfile.getInstance().setNeedStateRegInfo(true);
						}
					}
				} catch (Exception e) {
					Debug.println("Error while checking flags: " + e);
				}
			}
			
			// Check if any business errors were returned.
			
			if ((response.getErrors() != null) && (response.getErrors().getError().size() > 0)) {
				List<BusinessError> businessErrors = new ArrayList<BusinessError>();
				for (BusinessError error : response.getErrors().getError()) {
					if (throwBusinessErrors) {
						businessErrors.add(error);
					}
					
					if ((error.getErrorCategory() != null) && (error.getErrorCategory().equals("GENERIC_ERROR"))) {
						Debug.print("\tBusiness Error: Code = " + error.getErrorCode());
						if (error.getSubErrorCode() != null) {
							Debug.print(", Subcode = " + error.getSubErrorCode());
						}
						if (error.getOffendingField() != null) {
							Debug.print(", Offending Field = " + error.getOffendingField());
						}
						if (error.getMessageShort() != null) {
							Debug.print(", Short Message = " + error.getMessageShort());
						}
						if (error.getMessage() != null) {
							Debug.print(", Message = " + error.getMessage());
						}
						if (error.getDetailString() != null) {
							Debug.print(", Detailed Message = " + error.getDetailString());
						}
						Debug.println("");;
					}
				}

				if (businessErrors.size() > 0) {
					response = null;
					MessageFacadeError e = new MessageFacadeError(businessErrors);
					throw e;
				}
			}
		}
		
		return response;
	}

	private void processError(boolean fake, Exception exception) throws MessageFacadeError {
		int code = 0;
		int subCode = 0;
		
		MessageFacadeError messageFacadeError = null;
		if (exception instanceof SystemError_Exception) {
			SystemError_Exception systemErrors = (SystemError_Exception) exception;
			SystemError faultInfo = systemErrors.getFaultInfo();
			
			if ((faultInfo.getErrorCode() != null) || (faultInfo.getErrorString() != null)) {
				String errorString = "";
				String mainframeString = "";
				String offendingField = "";
				
				BigInteger errorCode = faultInfo.getErrorCode();
				if (faultInfo.getErrorString() != null) {
					errorString = faultInfo.getErrorString().trim();
				}
				if (faultInfo.getDetailString() != null) {
					mainframeString = faultInfo.getDetailString().trim();
				}
				BigInteger subErrorCode = faultInfo.getSubErrorCode();
				
				if (errorString.isEmpty()) {
					errorString = systemErrors.getMessage() != null ? (systemErrors.getMessage() + " ") : "" + " (SOAP fault)";
				}
				
				try {
					if (errorCode != null) {
						code = errorCode.intValue();
					} else {
						Debug.println("***ErrorCode is null, Defaulting to OTHER_ERROR***");
						code = MessageFacadeParam.OTHER_ERROR;
					}
				} catch (NumberFormatException e) {
					code = MessageFacadeParam.OTHER_ERROR;
				}
				
				try {
					if (subErrorCode != null) {
						subCode = subErrorCode.intValue();
					}
				} catch (NumberFormatException e) {
					subCode = MessageFacadeParam.OTHER_ERROR;
				}
				
				if ((code == 900) && (mainframeString.length() > 0)) {
					errorString = mainframeString;
				} else if ((code == 702) && (mainframeString.length() > 0)) {
					errorString = mainframeString;
				} else if (code == 3000 || code == 3003) {
					errorString = errorString + " " + offendingField;
				} else if (code >= 100) {
					if (errorString.toUpperCase().equals(mainframeString)) {
						mainframeString = "";
					}
				}
				Debug.println("code::" + code + "::subCode::" + subCode + "::errorString::" + errorString);
				messageFacadeError = new MessageFacadeError(code, subCode, errorString, mainframeString);
			} else {
				Debug.println("code::" + code + "::subCode::" + subCode);
				messageFacadeError = new MessageFacadeError(code, subCode, "No Error Code or Error String Returned", "");
			}
			throw messageFacadeError;
				
		} else if (!fake) {
            Debug.println("Unhandled exception in processError: " + exception.getClass().getName() + ": " + exception.getMessage());
            Debug.printException(exception);
            Debug.printStackTrace(exception);
            String className = exception.getClass().getSimpleName();
            String message = exception.getMessage();
            MessageFacadeError error = new MessageFacadeError(className, message);
            throw error;
		}
	}
	
	private void tokenUpdate(String newToken) {
		if (newToken != null && !newToken.isEmpty()) {
			if (UnitProfile.getInstance().getLastCheckInTime().getTime() != Long.parseLong(newToken)) {
				UnitProfile.getInstance().setLastCheckInTime(Long.parseLong(newToken));
			}
		}
	}
	
	private boolean isFake(int rule) {
		boolean fake = false;
		switch (rule) {
		case ALWAYS_FAKE:
			fake = true;
			break;
		case ALWAYS_LIVE:
			fake = false;
			break;
		case AUTOMATIC:
			fake = UnitProfile.getInstance().isTrainingMode();
			break;
		case LIVE_TRAINING:
			fake = false;
			break;
		}
		return fake;
	}
	
	private boolean isDemo(int rule) {
		boolean demo = false;
		switch (rule) {
		case ALWAYS_FAKE:
			demo = true;
			break;
		case ALWAYS_LIVE:
			demo = false;
			break;
		case AUTOMATIC:
			demo = UnitProfile.getInstance().isDemoMode();
			break;
		case LIVE_TRAINING:
			demo = UnitProfile.getInstance().isDemoMode();
			break;
		}
		return demo;
	}
	
	private String getAcUrl(int rule) {
		boolean fake = isFake(rule);
		boolean isDemo = isDemo(rule);
		String endpoint = null;
		String prodNetworkUrl = DWValues.DW_AC_URL;
		String prodDialupUrl = DWValues.DW_AC_URL;
		String demoModeUrl = "http://localhost:4545/ac/services/";
		
		if (fake || isDemo)
			endpoint = UnitProfile.getInstance().get("DEMO_MODE_URL", demoModeUrl);
		else if (UnitProfile.getInstance().isDialUpMode())
			endpoint = UnitProfile.getInstance().get("AC_DIALUP_URL", prodDialupUrl);
		else
			endpoint = UnitProfile.getInstance().get("AC_NETWORK_URL", prodNetworkUrl);
			
		//workaround for partially-implemented middleware that sends us empty URL
		if ("".equals(endpoint)) {
			endpoint = prodNetworkUrl;
		}
		
		/*
		 * For debugging with files retrieved from the field, force communcitation
		 * to a dev/qa host instead of production.
		 */
		//        endpoint = "https://q2ws.qa.moneygram.com/ac2/services/";
		
		// Transition: Lisa and I decided the profile URL should leave off 
		// the endpoint, and POS should hardcode it based on which version of 
		// the API is assumed by the code and the templates. (The reason this 
		// makes sense is that an "API" is more than just the set of request 
		// messages the middleware accepts -- it specifies the behavior of 
		// those messages. Our code is not only assuming that the outgoing
		// messages generated by the templates are valid -- it is assuming that
		// the RTS will respond in a certain way to those requests.)
		//
		// When you change this value you also have to change the namespace
		// attributes in the templates file. (Probably with a global search 
		// and replace. As currently envisioned, the RTS will never allow the 
		// client to mix and match outgoing requests from different versions 
		// of the API; to take advantage of one API change the client software 
		// has to take all the changes in that API version. This is so the RTS
		// doesn't have to preserve backward-compatibility code forever.)
		
		if (endpoint.endsWith("/")) {
			endpoint += "AgentConnect" + AgentConnect.API_VERSION;
		}
		return endpoint;
	}
	
	private com.moneygram.agentconnect.AgentConnect getAgentConnect(int rule) throws SOAPException {
		com.moneygram.agentconnect.AgentConnect agentConnect = getAcInstance();
		
		BindingProvider bindingProvider = (BindingProvider) agentConnect;
		String endpoint = getAcUrl(rule);
		Map<String, Object> req_ctx = bindingProvider.getRequestContext();
		req_ctx.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpoint);
		
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		
		headers.put("Content-type", Collections.singletonList("text/xml; charset=UTF-8"));
        if (useCompression) {
        	headers.put("Accept-Encoding", Collections.singletonList("gzip"));
        	headers.put("Content-Encoding", Collections.singletonList("gzip"));
        }
		
		if (KEEP_ALIVE) {
			headers.put("Connection", Collections.singletonList("Keep-Alive"));
		}
		req_ctx.put(MessageContext.HTTP_REQUEST_HEADERS, headers);
		
		try {
	        SOAPElement securityNode = factory.createElement("Security");
	        SOAPElement usernameTokenNode = securityNode.addChildElement("UsernameToken");
	        
	        SOAPElement usernameNode = usernameTokenNode.addChildElement("Username");
			usernameNode.addTextNode(username);
			
	        SOAPElement passwordNode = usernameTokenNode.addChildElement("Password");
			String curToken = String.valueOf(UnitProfile.getInstance().getLastCheckInTime().getTime());
			passwordNode.addTextNode(curToken);
			
			Header header = Headers.create(securityNode);
			WSBindingProvider bp = (WSBindingProvider) agentConnect;
			bp.setOutboundHeaders(header);
		
		} catch (SOAPException e1) {
			Debug.printException(e1);
			throw e1;
		}

		return agentConnect;
	}

	private synchronized com.moneygram.agentconnect.AgentConnect getAcInstance() {
		if (agentConnectService == null || agentConnect==null) {
			agentConnectService = new AgentConnectService();
			agentConnect = agentConnectService.getAgentConnect();
			BindingProvider bindingProvider = (BindingProvider) agentConnect;
			//bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, getAcUrl(rule));
			Binding binding = bindingProvider.getBinding();
			List<Handler> handlerChain = binding.getHandlerChain();
			if (CompileTimeFlags.isValidateAgainstWSDL()) {
				handlerChain.add(new ValidateResponseHandler());
			}
			handlerChain.add(new SOAPMessageHandler());
			binding.setHandlerChain(handlerChain);
		}
		return agentConnect;
	}

	public TransactionLookupResponse transactionLookup(TransactionLookupRequest request, int rule, boolean isInitial) throws MessageFacadeError {
		TransactionLookupResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (TransactionLookupResponse) FakeResponse.fakeResponse(request, isInitial);
			} else {
				response = getAgentConnect(rule).transactionLookup(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response != null && response.getPayload() != null ? response.getPayload().getValue() : null;
		return (TransactionLookupResponse) processResponse(response, payload, fake, null, true);
	}

	public ReceiveValidationResponse receiveValidation(ReceiveValidationRequest request, int rule, boolean isInitial) throws MessageFacadeError {
		ReceiveValidationResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (ReceiveValidationResponse) FakeResponse.fakeResponse(request, isInitial);
			} else {
				response = getAgentConnect(rule).receiveValidation(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		ReceiveValidationResponse.Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		String token = payload != null ? payload.getToken() : null;
		return (ReceiveValidationResponse) processResponse(response, payload, fake, token, false);
	}
	
	public CompleteSessionResponse completeSession(CompleteSessionRequest request, Request validationRequest, String referenceNumber, int rule) throws MessageFacadeError {
		CompleteSessionResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (CompleteSessionResponse) FakeResponse.fakeResponse(request, validationRequest, referenceNumber);
			} else {
				response = getAgentConnect(rule).completeSession(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (CompleteSessionResponse) processResponse(response, payload, fake, null, true);
	}
	
	public BillerSearchResponse billerSearch(BillerSearchRequest request, int rule) throws MessageFacadeError {
		BillerSearchResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (BillerSearchResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).billerSearch(request);
			}
		} catch (Exception e) {
			Debug.printStackTrace(e);
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (BillerSearchResponse) processResponse(response, payload, fake, null, true);
	}
	
	public BPValidationResponse bpValidation(BPValidationRequest request, int rule, String referenceNumber, boolean isInitial) throws MessageFacadeError {
		BPValidationResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (BPValidationResponse) FakeResponse.fakeResponse(request, referenceNumber, isInitial);
			} else {
				response = getAgentConnect(rule).bpValidation(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		BPValidationResponse.Payload payload = response != null && response.getPayload() != null ? response.getPayload().getValue() : null;
		String token = payload != null ? payload.getToken() : null;
		return (BPValidationResponse) processResponse(response, payload, fake, token, false);
	}
	
	public ConsumerHistoryLookupResponse consumerHistoryLookup(ConsumerHistoryLookupRequest request, int rule) throws MessageFacadeError {
		ConsumerHistoryLookupResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (ConsumerHistoryLookupResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).consumerHistoryLookup(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (ConsumerHistoryLookupResponse) processResponse(response, payload, fake, null, true);
	}
	
	public FeeLookupResponse feeLookup(FeeLookupRequest request, int rule) throws MessageFacadeError {
		FeeLookupResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (FeeLookupResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).feeLookup(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (FeeLookupResponse) processResponse(response, payload, fake, null, true);
	}
	
	public CheckInResponse checkIn(CheckInRequest request, int rule) throws MessageFacadeError {
		CheckInResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (CheckInResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).checkIn(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		CheckInResponse.Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		String token = payload != null ? payload.getToken() : null;
		return (CheckInResponse) processResponse(response, payload, fake, token, true);
	}
	
	public ConfirmTokenResponse confirmToken(ConfirmTokenRequest request, int rule) throws MessageFacadeError {
		ConfirmTokenResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (ConfirmTokenResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).confirmToken(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		ConfirmTokenResponse.Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		response = (ConfirmTokenResponse) processResponse(response, payload, fake, null, true);
		if (payload != null) {
			if (payload.isProfileChanged()) {
				UnitProfile.getInstance().setProfileChanged();
			}
		}
		return response;
	}
	
	public SendValidationResponse sendValidation(SendValidationRequest request, int rule, String referenceNumber, boolean isInitial) throws MessageFacadeError {
		SendValidationResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (SendValidationResponse) FakeResponse.fakeResponse(request, referenceNumber, isInitial);
			} else {
				response = getAgentConnect(rule).sendValidation(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		SendValidationResponse.Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		String token = payload != null ? payload.getToken() : null;
		return (SendValidationResponse) processResponse(response, payload, fake, token, false);
	}
	
	public DirectoryOfAgentsByAreaCodePrefixResponse directoryOfAgentsByAreaCodePrefix(DirectoryOfAgentsByAreaCodePrefixRequest request, int rule) throws MessageFacadeError {
		DirectoryOfAgentsByAreaCodePrefixResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (DirectoryOfAgentsByAreaCodePrefixResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).directoryOfAgentsByAreaCodePrefix(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (DirectoryOfAgentsByAreaCodePrefixResponse) processResponse(response, payload, fake, null, true);
	}
	
	public SubagentsResponse subagents(SubagentsRequest request, int rule) throws MessageFacadeError {
		SubagentsResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (SubagentsResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).subagents(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (SubagentsResponse) processResponse(response, payload, fake, null, true);
	}
	
	public MoneyGramReceiveDetailReportResponse moneyGramReceiveDetailReport(MoneyGramReceiveDetailReportRequest request, int rule) throws MessageFacadeError {
		MoneyGramReceiveDetailReportResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (MoneyGramReceiveDetailReportResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).moneyGramReceiveDetailReport(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (MoneyGramReceiveDetailReportResponse) processResponse(response, payload, fake, null, true);
	}
	
	public MoneyGramSendDetailReportResponse moneyGramSendDetailReport(MoneyGramSendDetailReportRequest request, int rule) throws MessageFacadeError {
		MoneyGramSendDetailReportResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (MoneyGramSendDetailReportResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).moneyGramSendDetailReport(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (MoneyGramSendDetailReportResponse) processResponse(response, payload, fake, null, true);
	}

	public BillPaymentDetailReportResponse billPaymentDetailReport(BillPaymentDetailReportRequest request, int rule) throws MessageFacadeError {
		BillPaymentDetailReportResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (BillPaymentDetailReportResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).billPaymentDetailReport(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (BillPaymentDetailReportResponse) processResponse(response, payload, fake, null, true);
	}

	public MoneyGramSendDetailReportWithTaxResponse moneyGramSendDetailReportWithTax(MoneyGramSendDetailReportWithTaxRequest request, int rule) throws MessageFacadeError {
		MoneyGramSendDetailReportWithTaxResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (MoneyGramSendDetailReportWithTaxResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).moneyGramSendDetailReportWithTax(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (MoneyGramSendDetailReportWithTaxResponse) processResponse(response, payload, fake, null, true);
	}
	
	public void saveDebugData(SaveDebugDataRequest request, int rule) throws MessageFacadeError {
		boolean fake = isFake(rule);
		boolean isDemo = isDemo(rule);
		boolean isTraining = fake || isDemo;
		try {
			if (fake) {
				FakeResponse.fakeResponse(request);
			} else {
				getAgentConnect(rule).saveDebugData(request);
			}
		} catch (Exception ex) {
			processError(isTraining, ex);
		}
	}
	
	public void profileChange(ProfileChangeRequest request, int rule) throws MessageFacadeError {
		boolean fake = isFake(rule);
		boolean isDemo = isDemo(rule);
		boolean isTraining = fake || isDemo;
		try {
			if (fake) {
				FakeResponse.fakeResponse(request);
			} else {
				getAgentConnect(rule).profileChange(request);
			}
		} catch (Exception ex) {
			processError(isTraining, ex);
		}
	}
	
	public DwProfileResponse dwProfile(DwProfileRequest request, int rule) throws MessageFacadeError {
		DwProfileResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (DwProfileResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).dwProfile(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (DwProfileResponse) processResponse(response, payload, fake, null, true);
	}
	
	public SaveSubagentsResponse saveSubagents(SaveSubagentsRequest request, int rule) throws MessageFacadeError {
		SaveSubagentsResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (SaveSubagentsResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).saveSubagents(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (SaveSubagentsResponse) processResponse(response, payload, fake, null, true);
	}
	
	public MoneyOrderTotalResponse moneyOrderTotal(MoneyOrderTotalRequest request, int rule) throws MessageFacadeError {
		MoneyOrderTotalResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (MoneyOrderTotalResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).moneyOrderTotal(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (MoneyOrderTotalResponse) processResponse(response, payload, fake, null, true);
	}
	
	public VersionManifestResponse versionManifest(VersionManifestRequest request, int rule) throws MessageFacadeError {
		VersionManifestResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (VersionManifestResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).versionManifest(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (VersionManifestResponse) processResponse(response, payload, fake, null, true);
	}
	
	public DwPasswordResponse dwPassword(DwPasswordRequest request, int rule) throws MessageFacadeError {
		DwPasswordResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (DwPasswordResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).dwPassword(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (DwPasswordResponse) processResponse(response, payload, fake, null, true);
	}
	
	public ReceiptsFormatDetailsResponse receiptsFormatDetails(ReceiptsFormatDetailsRequest request, int rule) throws MessageFacadeError {
		ReceiptsFormatDetailsResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (ReceiptsFormatDetailsResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).receiptsFormatDetails(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (ReceiptsFormatDetailsResponse) processResponse(response, payload, fake, null, true);
	}
	
	public GetBankDetailsByLevelResponse getBankDetailsByLevel(GetBankDetailsByLevelRequest request, int rule) throws MessageFacadeError {
		GetBankDetailsByLevelResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (GetBankDetailsByLevelResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).getBankDetailsByLevel(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (GetBankDetailsByLevelResponse) processResponse(response, payload, fake, null, true);
	}

	public GetBankDetailsResponse getBankDetails(GetBankDetailsRequest request, int rule) throws MessageFacadeError {
		GetBankDetailsResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (GetBankDetailsResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).getBankDetails(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (GetBankDetailsResponse) processResponse(response, payload, fake, null, true);
	}

	public OpenOTPLoginResponse openOTPLogin(com.moneygram.agentconnect.OpenOTPLoginRequest request, int rule) throws MessageFacadeError {
		OpenOTPLoginResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (OpenOTPLoginResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).openOTPLogin(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (OpenOTPLoginResponse) processResponse(response, payload, fake, null, true);
	}
	
	public GetBroadcastMessagesResponse getBroadcastMessages(com.moneygram.agentconnect.GetBroadcastMessagesRequest request, int rule) throws MessageFacadeError {
		GetBroadcastMessagesResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (GetBroadcastMessagesResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).getBroadcastMessages(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (GetBroadcastMessagesResponse) processResponse(response, payload, fake, null, true);
	}
	
	public DwInitialSetupResponse dwInitialSetup(DwInitialSetupRequest request, int rule) throws MessageFacadeError {
		DwInitialSetupResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (DwInitialSetupResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).dwInitialSetup(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		DwInitialSetupResponse.Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		String token = payload != null ? payload.getToken() : null;
		return (DwInitialSetupResponse) processResponse(response, payload, fake, token, true);
	}
	
	public ComplianceTransactionResponse complianceTransaction(ComplianceTransactionRequest request, int rule) throws MessageFacadeError {
		ComplianceTransactionResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (ComplianceTransactionResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).complianceTransaction(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (ComplianceTransactionResponse) processResponse(response, payload, fake, null, true);
	}
	
	public RegisterHardTokenResponse registerHardToken(com.moneygram.agentconnect.RegisterHardTokenRequest request, int rule) throws MessageFacadeError {
		RegisterHardTokenResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (RegisterHardTokenResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).registerHardToken(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (RegisterHardTokenResponse) processResponse(response, payload, fake, null, true);
	}
	
	public CityListResponse cityList(CityListRequest request, int rule) throws MessageFacadeError {
		CityListResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (CityListResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).cityList(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (CityListResponse) processResponse(response, payload, fake, null, true);
	}
	
	public DirectoryOfAgentsByCityResponse directoryOfAgentsByCity(DirectoryOfAgentsByCityRequest request, int rule) throws MessageFacadeError {
		DirectoryOfAgentsByCityResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (DirectoryOfAgentsByCityResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).directoryOfAgentsByCity(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (DirectoryOfAgentsByCityResponse) processResponse(response, payload, fake, null, true);
	}
	
	public DirectoryOfAgentsByZipResponse directoryOfAgentsByZip(DirectoryOfAgentsByZipRequest request, int rule) throws MessageFacadeError {
		DirectoryOfAgentsByZipResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (DirectoryOfAgentsByZipResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).directoryOfAgentsByZip(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (DirectoryOfAgentsByZipResponse) processResponse(response, payload, fake, null, true);
	}
	
	public GetCountryInfoResponse getCountryInfo(GetCountryInfoRequest request, int rule) throws MessageFacadeError {
		GetCountryInfoResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (GetCountryInfoResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).getCountryInfo(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (GetCountryInfoResponse) processResponse(response, payload, fake, null, true);
	}
	
	public GetCountrySubdivisionResponse countrySubdivision(GetCountrySubdivisionRequest request, int rule) throws MessageFacadeError {
		GetCountrySubdivisionResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (GetCountrySubdivisionResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).getCountrySubdivision(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (GetCountrySubdivisionResponse) processResponse(response, payload, fake, null, true);
	}
	
	public SendReversalValidationResponse sendReversalValidation(SendReversalValidationRequest request, int rule, boolean isInitial) throws MessageFacadeError {
		SendReversalValidationResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (SendReversalValidationResponse) FakeResponse.fakeResponse(request, isInitial);
			} else {
				response = getAgentConnect(rule).sendReversalValidation(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (SendReversalValidationResponse) processResponse(response, payload, fake, null, false);
	}
	
	public AmendValidationResponse amendValidation(AmendValidationRequest request, int rule, boolean isInitial) throws MessageFacadeError {
		AmendValidationResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (AmendValidationResponse) FakeResponse.fakeResponse(request, isInitial);
			} else {
				response = getAgentConnect(rule).amendValidation(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (AmendValidationResponse) processResponse(response, payload, fake, null, false);
	}
	
	public ReceiveReversalValidationResponse receiveReversalValidation(ReceiveReversalValidationRequest request, int rule, boolean isInitial) throws MessageFacadeError {
		ReceiveReversalValidationResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (ReceiveReversalValidationResponse) FakeResponse.fakeResponse(request, isInitial);
			} else {
				response = getAgentConnect(rule).receiveReversalValidation(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (ReceiveReversalValidationResponse) processResponse(response, payload, fake, null, false);
	}
	
	public SearchStagedTransactionsResponse searchStagedTransactions(SearchStagedTransactionsRequest request, int rule) throws MessageFacadeError {
		SearchStagedTransactionsResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (SearchStagedTransactionsResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).searchStagedTransactions(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (SearchStagedTransactionsResponse) processResponse(response, payload, fake, null, true);
	}
	
	public GetServiceOptionsResponse getServiceOptions(GetServiceOptionsRequest request, int rule) throws MessageFacadeError {
		GetServiceOptionsResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (GetServiceOptionsResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).getServiceOptions(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (GetServiceOptionsResponse) processResponse(response, payload, fake, null, true);
	}

	public GetCurrencyInfoResponse getCurrencyInfo(GetCurrencyInfoRequest request, int rule) throws MessageFacadeError {
		GetCurrencyInfoResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (GetCurrencyInfoResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).getCurrencyInfo(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (GetCurrencyInfoResponse) processResponse(response, payload, fake, null, true);
	}

	public SearchConsumerProfilesResponse searchConsumerProfiles(SearchConsumerProfilesRequest request, int rule, Map<String, String> values) throws MessageFacadeError {
		SearchConsumerProfilesResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (SearchConsumerProfilesResponse) FakeResponse.fakeResponse(request, values);
			} else {
				response = getAgentConnect(rule).searchConsumerProfiles(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (SearchConsumerProfilesResponse) processResponse(response, payload, fake, null, true);
	}
	
	public GetProfileSenderResponse getProfileSender(GetProfileSenderRequest request, int rule) throws MessageFacadeError {
		GetProfileSenderResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (GetProfileSenderResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).getProfileSender(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (GetProfileSenderResponse) processResponse(response, payload, fake, null, true);
	}
	
	public GetProfileReceiverResponse getProfileReceiver(GetProfileReceiverRequest request, int rule, Map<String, String> values) throws MessageFacadeError {
		GetProfileReceiverResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (GetProfileReceiverResponse) FakeResponse.fakeResponse(request, values);
			} else {
				response = getAgentConnect(rule).getProfileReceiver(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (GetProfileReceiverResponse) processResponse(response, payload, fake, null, true);
	}
	
	public CreateOrUpdateProfileSenderResponse createOrUpdateProfileSender(CreateOrUpdateProfileSenderRequest request, int rule) throws MessageFacadeError {
		CreateOrUpdateProfileSenderResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (CreateOrUpdateProfileSenderResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).createOrUpdateProfileSender(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (CreateOrUpdateProfileSenderResponse) processResponse(response, payload, fake, null, false);
	}

	public CreateOrUpdateProfileReceiverResponse createOrUpdateProfileReceiver(CreateOrUpdateProfileReceiverRequest request, int rule) throws MessageFacadeError {
		CreateOrUpdateProfileReceiverResponse response = null;
		boolean fake = isFake(rule);
		boolean training = fake || isDemo(rule);
		try {
			if (fake) {
				response = (CreateOrUpdateProfileReceiverResponse) FakeResponse.fakeResponse(request);
			} else {
				response = getAgentConnect(rule).createOrUpdateProfileReceiver(request);
			}
		} catch (Exception e) {
			processError(training, e);
			return null;
		}
		Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
		return (CreateOrUpdateProfileReceiverResponse) processResponse(response, payload, fake, null, false);
	}
}
