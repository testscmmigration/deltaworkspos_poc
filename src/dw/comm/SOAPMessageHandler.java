package dw.comm;

import java.lang.reflect.Field;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.w3c.dom.NodeList;

import com.sun.xml.internal.ws.api.message.Packet;
import com.sun.xml.internal.ws.message.jaxb.JAXBMessage;
import com.sun.xml.internal.ws.message.stream.StreamMessage;

import dw.utility.Debug;
import dw.utility.ExtraDebug;
import dw.utility.TimeUtility;

public class SOAPMessageHandler implements SOAPHandler<SOAPMessageContext> {
	
	private static Transformer transformer = null;
	private static StreamResult result = null;
	
    public static boolean extraDebug = false;
	
	public static void enableExtraDebug() { 
    	extraDebug = true; 
    }
    
    public static void disableExtraDebug() { 
    	extraDebug = false; 
    }

	@Override
	public Set<QName> getHeaders() {
		return null;
	}
	
	@Override
	public boolean handleMessage(SOAPMessageContext smc) {
		processMessage(smc);
		return true;
	}
	
	@Override
	public boolean handleFault(SOAPMessageContext smc) {
		processMessage(smc);
		return true;
	}
	
	@Override
	public void close(MessageContext messageContext) {
	}
	
	private void processMessage(SOAPMessageContext smc) {
		Boolean outboundProperty = (Boolean) smc.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		String timeStamp = TimeUtility.currentTime().toString();
		String opnName = getOperationName(smc);
		if (outboundProperty.booleanValue()) {
			SOAPMessage message = smc.getMessage();
			String sessionID = getMsgElement(message, "mgiSessionID");
			
			Debug.println("Request headers:  " + smc.get(MessageContext.HTTP_REQUEST_HEADERS));
			String endpointAddress = (String) smc.get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
			Debug.println("AC request being sent: " + opnName + " " + timeStamp);
			Debug.println("timeStamp = " + getTimeStamp(message));
			if (sessionID.length() > 0) {
				Debug.println("mgiSessionID = " + sessionID);
			}
			Debug.println("url = " + endpointAddress);
			ExtraDebug.println("Waiting for response:" + Thread.currentThread().getName());
			
		} else {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
			}
			ExtraDebug.println("Done Waiting" + Thread.currentThread().getName());
			Debug.println("AC response received: " + opnName + " " + timeStamp);
			Debug.println("\nResponse SOAP Message = ");
		}
		try {
			SOAPMessage message = smc.getMessage();
			printSOAPResponse(message);
		} catch (Exception e) {
		}
	}
	
	
	/**
	 * get content value of timeStamp element in Soap body  
	 * @param message
	 * @return timeStamp
	 */
	private String getTimeStamp(final SOAPMessage message) {
		String timeStamp = "";
		if (message != null) {
			try {
				SOAPBody soapBody = message.getSOAPBody();
				if (soapBody != null) {
					NodeList nodes = soapBody.getElementsByTagName("timeStamp");
					if ((nodes != null) && (nodes.getLength() > 0)) {
						timeStamp = nodes.item(0).getTextContent();
					}
				}
			} catch (SOAPException exc) {
				Debug.println("Error: unable to find timeStamp");
			}
		}
		return timeStamp;
	}
	
	private String getOperationName(SOAPMessageContext context) {
		String operationName = null;
		try {
			Field field = context.getClass().getSuperclass().getDeclaredField("packet");
			field.setAccessible(true);
			Packet packet = (Packet) field.get(context);
			
			Object obj = packet.getMessage();
			if (obj instanceof StreamMessage) {
				operationName = ((StreamMessage) packet.getMessage()).getPayloadLocalPart();
			} else if (obj instanceof JAXBMessage) {
				operationName = ((JAXBMessage) packet.getMessage()).getPayloadLocalPart();
			}
		} catch (Exception e) {
			Debug.println("Unable to find operation name");
		}
		return operationName;
	}
	
	/**
	 * get content value of a specified element in Soap body  
	 * @param message - SOAP Message
	 * @param element - String containing an element name to search for in the 
	 * SOAP message
	 * @return element contents
	 */
	private String getMsgElement(final SOAPMessage message, String element) {
		String sessionID = "";
		if (message != null) {
			try {
				SOAPBody soapBody = message.getSOAPBody();
				if (soapBody != null) {
					NodeList nodes = soapBody.getElementsByTagName(element);
					if ((nodes != null) && (nodes.getLength() > 0)) {
						sessionID = nodes.item(0).getTextContent();
					}
				}
			} catch (SOAPException exc) {
				Debug.println("Error: unable to find " + element);
			}
		}
		return sessionID;
	}
	
	private static void printSOAPResponse(SOAPMessage soapResponse) throws Exception {
		if (transformer == null) {
			transformer = getTransformer();
		}
		if (result == null) {
			result = new StreamResult(System.out);
		}
		if (extraDebug) {
			Source sourceContent = soapResponse.getSOAPPart().getContent();
			transformer.transform(sourceContent, result);
		}
	}
	
	private static Transformer getTransformer()
			throws TransformerFactoryConfigurationError, TransformerConfigurationException {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		return transformer;
	}
}