package dw.comm;

import dw.ras.RasListener;

/**
 * Pass RasListener events to two different listeners.
 */
public class RasMulticaster implements RasListener {

    private RasListener a;
    private RasListener b;

    public RasMulticaster(RasListener a, RasListener b) {
        this.a = a;
        this.b = b;
    }
    
    @Override
	public void deviceConnected(int rasErrorCode) {
        a.deviceConnected(rasErrorCode);
        b.deviceConnected(rasErrorCode);
    }

    @Override
	public void openPort(int rasErrorCode) {
        a.openPort(rasErrorCode);
        b.openPort(rasErrorCode);
    }

    @Override
	public void portOpened(int rasErrorCode) {
        a.portOpened(rasErrorCode);
        b.portOpened(rasErrorCode);
    }

    @Override
	public void connectDevice(int rasErrorCode) {
        a.connectDevice(rasErrorCode);
        b.connectDevice(rasErrorCode);
    }

    @Override
	public void allDevicesConnected(int rasErrorCode) {
        a.allDevicesConnected(rasErrorCode);
        b.allDevicesConnected(rasErrorCode);
    }

    @Override
	public void authenticate(int rasErrorCode) {
        a.authenticate(rasErrorCode);
        b.authenticate(rasErrorCode);
    }

    @Override
	public void authNotify(int rasErrorCode) {
        a.authNotify(rasErrorCode);
        b.authNotify(rasErrorCode);
    }

    @Override
	public void authRetry(int rasErrorCode) {
        a.authRetry(rasErrorCode);
        b.authRetry(rasErrorCode);
    }

    @Override
	public void authCallback(int rasErrorCode) {
        a.authCallback(rasErrorCode);
        b.authCallback(rasErrorCode);
    }

    @Override
	public void authChangePassword(int rasErrorCode) {
        a.authChangePassword(rasErrorCode);
        b.authChangePassword(rasErrorCode);
    }

    @Override
	public void authProject(int rasErrorCode) {
        a.authProject(rasErrorCode);
        b.authProject(rasErrorCode);
    }

    @Override
	public void authLinkSpeed(int rasErrorCode) {
        a.authLinkSpeed(rasErrorCode);
        b.authLinkSpeed(rasErrorCode);
    }

    @Override
	public void authAck(int rasErrorCode) {
        a.authAck(rasErrorCode);
        b.authAck(rasErrorCode);
    }

    @Override
	public void reAuthenticate(int rasErrorCode) {
        a.reAuthenticate(rasErrorCode);
        b.reAuthenticate(rasErrorCode);
    }

    @Override
	public void authenticated(int rasErrorCode) {
        a.authenticated(rasErrorCode);
        b.authenticated(rasErrorCode);
    }

    @Override
	public void prepareForCallback(int rasErrorCode) {
        a.prepareForCallback(rasErrorCode);
        b.prepareForCallback(rasErrorCode);
    }

    @Override
	public void waitForModemReset(int rasErrorCode) {
        a.waitForModemReset(rasErrorCode);
        b.waitForModemReset(rasErrorCode);
    }

    @Override
	public void waitForCallback(int rasErrorCode) {
        a.waitForCallback(rasErrorCode);
        b.waitForCallback(rasErrorCode);
    }

    @Override
	public void projected(int rasErrorCode) {
        a.projected(rasErrorCode);
        b.projected(rasErrorCode);
    }

    @Override
	public void subentryConnected(int rasErrorCode) {
        a.subentryConnected(rasErrorCode);
        b.subentryConnected(rasErrorCode);
    }

    @Override
	public void subentryDisconnected(int rasErrorCode) {
        a.subentryDisconnected(rasErrorCode);
        b.subentryDisconnected(rasErrorCode);
    }

    @Override
	public void interactive(int rasErrorCode) {
        a.interactive(rasErrorCode);
        b.interactive(rasErrorCode);
    }

    @Override
	public void retryAuthentication(int rasErrorCode) {
        a.retryAuthentication(rasErrorCode);
        b.retryAuthentication(rasErrorCode);
    }

    @Override
	public void callbackSetByCaller(int rasErrorCode) {
        a.callbackSetByCaller(rasErrorCode);
        b.callbackSetByCaller(rasErrorCode);
    }

    @Override
	public void passwordExpired(int rasErrorCode) {
        a.passwordExpired(rasErrorCode);
        b.passwordExpired(rasErrorCode);
    }

    @Override
	public void invokeEAPUI(int rasErrorCode) {
        a.invokeEAPUI(rasErrorCode);
        b.invokeEAPUI(rasErrorCode);
    }

    @Override
	public void connected(int rasErrorCode) {
        a.connected(rasErrorCode);
        b.connected(rasErrorCode);
    }

    @Override
	public void disconnected(int rasErrorCode) {
        a.disconnected(rasErrorCode);
        b.disconnected(rasErrorCode);
    }

    @Override
	public void attemptSequenceFailed() {
        a.attemptSequenceFailed();
        b.attemptSequenceFailed();
    }
}


