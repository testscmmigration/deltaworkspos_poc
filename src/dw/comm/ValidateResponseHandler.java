package dw.comm;

import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Set;

import javax.xml.XMLConstants;
import javax.xml.namespace.QName;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.w3c.dom.Document;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import dw.utility.Debug;

public class ValidateResponseHandler implements SOAPHandler<SOAPMessageContext> {
	private static final String XSD_FILE = "META-INF/wsdl/AgentConnect.xsd";
	private Validator validator;
	private boolean resultFlag;
	
	public ValidateResponseHandler() {
		try {
	        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
	        URL fileUrl = ClassLoader.getSystemResource(XSD_FILE);
	        InputStream xsd = new FileInputStream(fileUrl.getFile());
			Schema schema = factory.newSchema(new StreamSource(xsd));
	        validator = schema.newValidator();
	        validator.setErrorHandler(new ErrorHandler() {
				@Override
				public void warning(SAXParseException exception) throws SAXException {
					Debug.println("AC Response Validation Warning: " + exception.getMessage());
					resultFlag = false;
				}
				@Override
				public void error(SAXParseException exception) throws SAXException {
					Debug.println("AC Response Validation Error: " + exception.getMessage());
					resultFlag = false;
				}
				@Override
				public void fatalError(SAXParseException exception) throws SAXException {
					Debug.println("AC Response Validation Fatal Error: " + exception.getMessage());
					resultFlag = false;
				}
	        });
		} catch (Exception e) {
			Debug.printException(e);
			Debug.printStackTrace(e);
		}
	}
	
	@Override
	public boolean handleMessage(SOAPMessageContext context) {
		
		Boolean outboundProperty = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		if (outboundProperty.booleanValue()) {
			return true;
		}
		
		String name = null;
		resultFlag = true;
		try {
			name = context.getMessage().getSOAPBody().getFirstChild().getLocalName();
			Document doc = context.getMessage().getSOAPBody().extractContentAsDocument();
			validator.validate(new DOMSource(doc));
			context.getMessage().getSOAPBody().addDocument(doc);
	    } catch(Exception e) {
			Debug.println("AC Response Validation Exception: " + e.getMessage());
			resultFlag = false;
	    }
		
		if (resultFlag) {
			Debug.println("Validation of " + name + " against the WSDL passed.");
		} else {
			Debug.println("Validation of " + name + " against the WSDL FAILED!!!");
			(new Exception("AC Response Validation Failed")).printStackTrace();
		}
        return true;
	}

	@Override
	public boolean handleFault(SOAPMessageContext context) {
		return true;
	}

	@Override
	public void close(MessageContext context) {
	}

	@Override
	public Set<QName> getHeaders() {
		return null;
	}
}
