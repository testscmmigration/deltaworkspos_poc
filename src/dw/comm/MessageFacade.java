/*
 * MessageFacade.java
 *
 * Copyright (c) 2004-2014 MoneyGram International
 */

package dw.comm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.comm.CommPortIdentifier;
import javax.swing.SwingUtilities;

import com.jpackages.jdun.DialUpDevice;
import com.jpackages.jdun.DialUpEntryProperties;
import com.jpackages.jdun.DialUpManager;
import com.jpackages.jdun.DialingNotification;
import com.jpackages.jdun.DialingState;
import com.jpackages.jdun.LibraryLoadFailedException;
import com.moneygram.agentconnect.*;
import com.moneygram.agentconnect.SaveSubagentsRequest.SubagentProfileUpdates;

import dw.dialogs.CommunicationDialog;
import dw.dialogs.DialogReferenceNumberText;
import dw.dialogs.Dialogs;
import dw.framework.ClientTransaction;
import dw.framework.CommCompleteInterface;
import dw.framework.DataCollectionField;
import dw.framework.DataCollectionSet;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.FieldKey;
import dw.framework.WaitToken;
import dw.i18n.Messages;
import dw.io.report.AuditLog;
import dw.main.DeltaworksMainPanel;
import dw.main.MainPanelDelegate;
import dw.mgreceive.MoneyGramReceiveTransaction;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;
import dw.mgsend.MoneyGramSendTransaction;
import dw.model.adapters.ConsumerProfile;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.CountryInfo.Country;
import dw.model.adapters.CountryInfo.CountrySubdivision;
import dw.model.adapters.CustomerInfo;
import dw.model.adapters.GetInitialSetupMsg;
import dw.model.adapters.MoneyGramAgentInfo;
import dw.model.adapters.ReceiptTemplateInfo;
import dw.pinpad.Pinpad;
import dw.profile.ClientInfo;
import dw.profile.Profile;
import dw.profile.ProfileAccessor;
import dw.profile.ProfileItem;
import dw.profile.UnitProfile;
import dw.ras.RasAdapter;
import dw.ras.RasException;
import dw.ras.RasProperties;
import dw.simplepm.SimplePMTransaction;
import dw.softwareversions.VersionManifest;
import dw.utility.AgentCountryInfo;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.ExtraDebug;
import dw.utility.IdleBackoutTimer;
import dw.utility.ReceiptTemplates;
import dw.utility.Scheduler;
import dw.utility.StringUtility;
import dw.utility.TimeUtility;
import dw.utility.TimeZoneCorrection;

/**
 * Hides the complexity of RAS and MessageForwarder. Allows transaction code to
 * be written in terms of what it wants to accomplish rather than how to coax
 * RAS and MessageForwarder into doing it. Supports improved error handling and
 * user feedback.
 *
 * @author Geoff Atkin
 * @author Pat Ottman
 * @author Rob Morgan
 * @author Scott Kostelecky
 */
public class MessageFacade {

    private static final Request.PoeCapabilities POE_CAPABILITIES;
	
	static {
    	POE_CAPABILITIES = new Request.PoeCapabilities();

        try {
    		ObjectFactory objectFactory = new ObjectFactory();
    		
    		KeyValuePairType pair = new KeyValuePairType();
    		pair.setInfoKey("MGI_INTERNAL_FIELDS");
    		pair.setValue(objectFactory.createKeyValuePairTypeValue("true"));
    		POE_CAPABILITIES.getPoeCapability().add(pair);
    		
    		pair = new KeyValuePairType();
    		pair.setInfoKey("CHECK_PAYOUT_FIELDS");
    		pair.setValue(objectFactory.createKeyValuePairTypeValue("true"));
    		POE_CAPABILITIES.getPoeCapability().add(pair);

    		pair = new KeyValuePairType();
    		pair.setInfoKey("NOTF_CHOICE_FIELDS");
    		pair.setValue(objectFactory.createKeyValuePairTypeValue("true"));
    		POE_CAPABILITIES.getPoeCapability().add(pair);
        } catch (Exception e) {
            Debug.printStackTrace(e);
        }
    }
    
    /** Singleton instance. */
    private static volatile MessageFacade instance = null;

    /** Number of times to attempt to dial. */
    private static final int MAX_DIAL_ATTEMPTS = 6;
//    private static final int MAX_DIAL_ATTEMPTS = 2;
	private static final DecimalFormat NUMBER_FORMATTER = new DecimalFormat("00");

    /** RAS connection handle. */
    //   private RasConnection rasConnection = null;
    private DialUpManager dialUp;
    private String entry = "DW_DUN_Entry";

    /** This object gets notified when the connection succeeds or fails. */
    private Object lock = new Object();

    private WaitToken retryLock = new WaitToken();

    /** Simple timer reset mechanism. */
    private long counter = 0;

    private static long dialogCounter = 0;

    /** Error string to display later, if there's an error. */
    private static String futureError = ""; 

    /** Number of seconds to let a connection lie around. */
    private int connectionCloseDelay = RasProperties.getCloseConnectionDelay();

    // Flag the timer checks, to determine if it is valid to hangup.
    private static boolean timerIsPaused = false;

    private boolean idleBackoutTimerIsPaused = false;

    // moved to MessageFacadeParam
    // Flag to keep track of the number of AUTO_RETRY's made.
    //private static int autoRetryCount = 0;

    private InnerListener listener = null;

    private RasMulticaster multiListener = null;

    private static Thread current = null;
  

    private static ThreadGroup tg = new ThreadGroup("tg"); 


    private static final int RETRY = 0;

    private static final int CANCEL = 1;

    public int retryCancelOption = CANCEL;

    private boolean reportCanceled = false;

    /** Use canned data even if we're not in demo or training mode. */
    public static final int ALWAYS_FAKE = AgentConnect.ALWAYS_FAKE;

    /** Send live messages to the middleware even in demo mode. */
    public static final int ALWAYS_LIVE = AgentConnect.ALWAYS_LIVE;

    /** Use canned data in demo and training mode, otherwise live. */
    public static final int AUTOMATIC = AgentConnect.AUTOMATIC;

    /** Use canned data in demo mode, but live messages in training. */
    public static final int LIVE_TRAINING = AgentConnect.LIVE_TRAINING;

    private static volatile AgentConnect agentConnectLocal = null;
    
    private MessageFacadeHelper messageFacadeHelper;
    
    private int userID=0;
    
    private int dialAttempt = 0;

    /**
     * Private constructor because we're a singleton. Use getInstance() to
     * obtain an instance.
     */
    private MessageFacade() {
        // Add a Thread runner shutdown hook to the Java VM.
    	messageFacadeHelper = new MessageFacadeHelper();
        Runtime.getRuntime().addShutdownHook(
                new Thread(new ShutdownHookRunner()));

    }

    /**
     * Returns the singleton instance.
     */
    public static MessageFacade getInstance() {
        if (instance == null)
            instance = new MessageFacade();
        return instance;
    }

    /**
     * Returns the MAX_DIAL_ATTEMPTS.
     */
    public static int getMaxDialAttempts() {
        return MAX_DIAL_ATTEMPTS;
    }
    
    /** Shows the communication dialog. */
    private void showDialog() {
        //Debug.println("MessageFacade.showDialog() lag = " 
        //        + (System.currentTimeMillis() - tempPrevHideTime)
        //        + " showing = " + CommunicationDialog.getInstance().isShowing());
        // 
        ++dialogCounter;
        ExtraDebug.println("-x-MessageFacade.showDialog(" + dialogCounter + ") "
                + Thread.currentThread());
        if (CommunicationDialog.getInstance().isShowing()) {
            return;
        }
        Runnable r = new Runnable() {
            @Override
			public void run() {
                Dialogs.showPanel(CommunicationDialog.getInstance());
            }
        };
        //t.start();
        SwingUtilities.invokeLater(r);
    }

    /**
     * Hides the communication dialog. Uses a short delay so that back-to-back
     * calls to showDialog and hideDialog don't flash the dialog up and down.
     */
    public static synchronized void hideDialog(long sleep) {
        Debug.println("MessageFacade.hideDialog(" + sleep + ") "
                + Thread.currentThread());
    	ExtraDebug.println("-x-MessageFacade.hideDialog dialogCounter["
        		+ (dialogCounter + 1) + "]");

        final long current = ++dialogCounter;
        closeInSeparateThread(sleep, current);
    }

//    private void hideDialog() {
//        hideDialog(500);
//    }

    /**
     * Hide the communication dialog immediately.
     */
    public static void hideDialogNow() {
        Debug.println("MessageFacade.hideDialogNow() " + Thread.currentThread());
        CommunicationDialog.getInstance().closeDialog();
    }
    
   

    /**
     * Registers interest in keeping the dialog open. Has no effect if the
     * dialog is not currently open. The caller must make a subsequent call to
     * specialCloseDialog() or it may be left open indefinitely.
     */
    public static synchronized long specialKeepDialogOpen() {
    	ExtraDebug.println("-x-specialKeepDialogOpen() returning " + (dialogCounter + 1)); 
        return ++dialogCounter;
    }

    /**
     * Closes the dialog if the only reason it is still open is because of a
     * matching call to specialKeepDialogOpen. The caller must provide the
     * counter that was returned by that method.
     */
    public static synchronized void specialCloseDialog(long counter) {
        Debug.println("specialCloseDialog(" + counter + ") "
                + Thread.currentThread());
        closeInSeparateThread(500, counter);
    }

    /**
     * Start a separate thread to close the dialog after a short delay.
     */
    private static void closeInSeparateThread(final long sleep,
            final long counter) {
        Thread t = new Thread() {
            @Override
			public void run() {
                if (sleep != 0) {
                    try {
                        // 20 milliseconds = 1/50 second
                        // 500 milliseconds = 1/2 second
                        Thread.sleep(sleep);
                    } catch (InterruptedException e) {
                    }
                }
                maybeClose(counter);
            }
        };
        //t.start();
        SwingUtilities.invokeLater(t);
    }

    private static synchronized void maybeClose(final long counter) {
        Debug.println("MessageFacade.maybeClose(" + counter
                + ") dialogCounter=" + dialogCounter);

        if (counter == dialogCounter) {
            CommunicationDialog.getInstance().closeDialog();
        }
    }

    public boolean isReportCanceled() {
        return reportCanceled;
    }

    public void resetReportCanceled() {
        reportCanceled = false;
    }

    /**
     * This method is used to disable the timer used to hangup the modem in
     * Dialup Mode. This method pauseTimer() needs to have a matching call to
     * restartTimer(), otherwise the phone line will never hangup.
     *
     * @author: Rob Morgan
     */
    private void pauseTimer() {
        timerIsPaused = true;
    }

    /**
     * This method is used to restart the timer used to hangup the modem in
     * Dialup Mode after a call to pauseTimer() has been made. This method MUST
     * be called after the pauseTimer() method, otherwise the phone line will
     * never hangup.
     *
     * @author: Rob Morgan
     */

    private void restartTimer() {
        timerIsPaused = false;
        resetTimer();
    }

    private void resetTimer() {
        resetTimer(connectionCloseDelay);
    }

    private void resetTimer(final int delay) {
        final long current = ++counter;
        Thread t = new Thread(tg, "resetTimer") { 
            @Override
			public void run() {
                try {
                    Thread.sleep(delay * 1000);
                } catch (InterruptedException e) {
                    Debug.println("     MessageFacade.resetTimer(): Caught InterruptedException"); 
                }

                if ((current == counter) && (!timerIsPaused)) {
                    if (dialUp != null) {
                        dialUp.hangUpEntry(entry);
                        dialUp.deleteEntry(entry);
                    }
                }
            }
        };
        t.start();
    }

    public void cancelConnection() {
        Debug.println("[Cancel]"); 
        retryCancelOption = CANCEL;
        if (idleBackoutTimerIsPaused) {
            IdleBackoutTimer.start();
            idleBackoutTimerIsPaused = false;
        }
        //deletes temg script file on canceling the dialup connection
        deleteScriptFile();
        synchronized (retryLock) {
        	retryLock.setWaitFlag(false);
            retryLock.notifyAll();
        }
    }

    public void retryConnection() {
        Debug.println("[Retry]"); 
        retryCancelOption = RETRY;
        synchronized (retryLock) {
        	retryLock.setWaitFlag(false);
            retryLock.notifyAll();
        }
    }

    public void okToContinue() {
        Debug.println("[OK]"); 
        if (idleBackoutTimerIsPaused) {
            IdleBackoutTimer.start();
            idleBackoutTimerIsPaused = false;
        }
    }

    /**
     * Set a timer, then return immediately. When the timer goes off, check to
     * see if there were any calls to ensureConnection while the timer was
     * ticking. If not, hang up the phone.
     *
     * Note: depending on how much code gets converted to this new facade, we
     * may have to coordinate with existing code to make sure we don't hang up
     * in the middle of a daily transmit or software download.
     */
    private void dropConnection(MessageFacadeParam parameters) {
        resetTimer();
        
        /*
         * Things that change the UI must be done on the swing thread.  
         */
        SwingUtilities.invokeLater(new Runnable() {

			@Override
            public void run () {
                CommunicationDialog.getInstance().setCancelButtonProperty(false);
            }
        });
        

        ExtraDebug.println("-x-dropConnection--" );
        
        if (idleBackoutTimerIsPaused) {
            IdleBackoutTimer.start();
            idleBackoutTimerIsPaused = false;
        }

        // moved to async run method
        
        boolean showDialogFlag = ! parameters.get(MessageFacadeParam.NO_DIALOG);
        boolean hideDialogFlag = ! parameters.get(MessageFacadeParam.DO_NOT_HIDE);
        
        if (showDialogFlag && hideDialogFlag) {
            hideDialog(500);
        }
    }

    /**
     * Establishes a dialup connection, if appropriate and necessary. Displays a
     * dialog box (if it isn't already showing) to keep the user informed of
     * dialing progress, unless there's a flag in the parameters telling us not
     * to. Block until the connection is established.
     *
     * Also set a timer. When the timer goes off, hang up the phone like
     * dropConnection does.
     * 
   
     */
    
    public void ensureConnection(MessageFacadeParam parameters)
    	throws RasException, InterruptedException {
    	// reset the dialAttemp because you are coming from an initial message.
    	ensureConnection(parameters, false, true);        
    }

    private void ensureConnection(MessageFacadeParam parameters, boolean cancelDisabled, boolean resetDialAttempt)
            throws RasException, InterruptedException {

        if (IdleBackoutTimer.isRunning()) {
            IdleBackoutTimer.pause();
            idleBackoutTimerIsPaused = true;
        }

        // reset flag
        reportCanceled = false;
        //display the dialog box, making it a ras listener
        if (parameters.get(MessageFacadeParam.NON_REAL_TIME) || cancelDisabled) {
            CommunicationDialog.getInstance().setCancelButtonProperty(false);
        } else {
            CommunicationDialog.getInstance().setCancelButtonProperty(true);
        }
        CommunicationDialog.getInstance().setRetryButtonProperty(false);

        ExtraDebug.println("Show Dialog - " + (! parameters.get(MessageFacadeParam.NO_DIALOG)));
        ExtraDebug.println("Do not hide Dialog - " + (parameters.get(MessageFacadeParam.DO_NOT_HIDE)));
        ExtraDebug.println("Real Time - " + (parameters.get(MessageFacadeParam.REAL_TIME)));
        ExtraDebug.println("Non Real Time - " + (parameters.get(MessageFacadeParam.NON_REAL_TIME)));
        ExtraDebug.println("Auto Retry - " + (parameters.get(MessageFacadeParam.AUTO_RETRY)));
        
        boolean hideWithoutFocus = (DeltaworksMainPanel.getFocusOwner() == null) && 
        		(parameters.get(MessageFacadeParam.HIDE_WITHOUT_FOCUS_UNTIL_ERROR));
        if (! (parameters.get(MessageFacadeParam.NO_DIALOG) || hideWithoutFocus)) {
            showDialog();
        }

        // 4x the UnitProfile Disconnect timeout when connecting, since
        // the timer starts when the connection starts not once actually
        // connected.
        resetTimer(4 * connectionCloseDelay);

        // return immediately for demo mode and network mode

        // return immediately for network mode
        if (!UnitProfile.getInstance().isDialUpMode()) {
            // if modem is still connected from changing from dialup to network
            // disconnect modem NOW.
            if ((dialUp != null) && (dialUp.isEntryConnected(entry))) {
                dialUp.hangUpEntry(entry);
                dialUp.deleteEntry(entry);
            }

            return;
        }
        // application is in dialup mode
        // return immediately for demo mode however not for an initial install
        if (UnitProfile.getInstance().isDemoMode()
                && (!parameters.get(MessageFacadeParam.INSTALL)))
            return;
        
        if (resetDialAttempt){
        	dialAttempt = 0;
        }

        // If no dialing attempt is in progress, start it
        if (dialUp == null) {
            setDialUpMode();
            listener = new InnerListener(lock);
            multiListener = new RasMulticaster(listener, CommunicationDialog.getInstance());
            String username = UnitProfile.getInstance().get("PRIMARY_DUN_USER_ID", "");
            String password = UnitProfile.getInstance().get("PRIMARY_DUN_PASSWORD", "");
            if (!usePrimaryDialupMode()){
                username = UnitProfile.getInstance().get("SECONDARY_DUN_USER_ID", "");
                password = UnitProfile.getInstance().get("SECONDARY_DUN_PASSWORD", "");
            }
            dialUp.dialEntryAsynchronous(entry, username, password);
        } else if (dialUp.isEntryConnected(entry)) {
            // Dialing is already finished, no need to wait on the lock.
            return;
        } else {
            dialUp.hangUpEntry(entry);
            dialUp.deleteEntry(entry);
            setDialUpMode();
            listener = new InnerListener(lock);
            multiListener = new RasMulticaster(listener, CommunicationDialog.getInstance());
            String username = UnitProfile.getInstance().get("PRIMARY_DUN_USER_ID", "");
            String password = UnitProfile.getInstance().get("PRIMARY_DUN_PASSWORD", "");
            if (!usePrimaryDialupMode()){
                username = UnitProfile.getInstance().get("SECONDARY_DUN_USER_ID", "");
                password = UnitProfile.getInstance().get("SECONDARY_DUN_PASSWORD", "");
            }
            dialUp.dialEntryAsynchronous(entry, username, password);
        }

        synchronized (lock) {
            try {
            	lock.wait(3 * (connectionCloseDelay * 1000));
            } catch (InterruptedException e) {
                // This happens when the user clicks Cancel on dialog
                dialUp.hangUpEntry(entry);
                dialUp.deleteEntry(entry);
                throw e;
            }
            // finally listener.nevermind() ?
        }

        if (!dialUp.isEntryConnected(entry)) {
            dialUp.hangUpEntry(entry);
            dialUp.deleteEntry(entry);
            if (dialAttempt < MessageFacade.MAX_DIAL_ATTEMPTS && 
            	!parameters.get(MessageFacadeParam.WILL_NEED)) {
                dialAttempt++;
                dropConnection(parameters);
                Debug.println("Shutting down RAS connection:"); 
                // Clean up the cached rasConnection handle.
                dialUp.hangUpEntry(entry);
                dialUp.deleteEntry(entry);
                dialUp = null;
//                Thread.sleep(3000);
                this.wait(3000);
            	ensureConnection(parameters,cancelDisabled, false);
            	
            } else {
            	dialAttempt = 0;
	            throw new MessageFacadeError(
	                    MessageFacadeParam.OTHER_ERROR,
	                    Messages.getString("MessageFacade.1807") 
	                            + listener.getErrorCode() + ")"); 
            }
        }

        // If we're not connected, something's wrong.
        if (dialUp == null) {
            if (dialAttempt < MessageFacade.MAX_DIAL_ATTEMPTS && 
                !parameters.get(MessageFacadeParam.WILL_NEED)) {
                dialAttempt++;
                setDialUpMode();
                Debug.println("Shutting down RAS connection:"); 
                // Clean up the cached rasConnection handle.
                dialUp.hangUpEntry(entry);
                dialUp.deleteEntry(entry);
                dialUp = null;
//                Thread.sleep(3000);
                this.wait(3000);
            	ensureConnection(parameters,cancelDisabled, false);       	
            } else {
            	dialAttempt = 0;
	            throw new MessageFacadeError(
	                    MessageFacadeParam.OTHER_ERROR,
	                    Messages.getString("MessageFacade.1806")); 
            }
        } else if (!dialUp.isEntryConnected(entry)) {
            dialUp.hangUpEntry(entry);
            dialUp.deleteEntry(entry);
            if (dialAttempt < MessageFacade.MAX_DIAL_ATTEMPTS && 
                	!parameters.get(MessageFacadeParam.WILL_NEED)) {
                dialAttempt++;
                Debug.println("Shutting down RAS connection:"); 
                // Clean up the cached rasConnection handle.
                dialUp.hangUpEntry(entry);
                dialUp.deleteEntry(entry);
                dialUp = null;
//                Thread.sleep(3000);
                this.wait(3000);
            	ensureConnection(parameters,cancelDisabled, false);       	
            } else {
            	dialAttempt = 0;
	            throw new MessageFacadeError(
	                    MessageFacadeParam.OTHER_ERROR,
	                    Messages.getString("MessageFacade.1807") 
	                            + listener.getErrorCode() + ")"); 
            }
        }
    }
    
    public String[] getCommPortList(){        
        String[] tempPortList = new String[100];
        int i= 0;
    	Enumeration<CommPortIdentifier> port_list = CommPortIdentifier.getPortIdentifiers ();    	
    	while (port_list.hasMoreElements ()) {
    		//Get the list of ports
    		CommPortIdentifier port_id = port_list.nextElement ();

    		//Find each ports type and name
    		if (port_id.getPortType () == CommPortIdentifier.PORT_SERIAL) {
    			tempPortList[i] = port_id.getName();
    		    i++;
    		} 
    	}
    	
    	// loop through to remove null strings.
    	String[] portList = new String[i];
    	
    	for (int j=0; j < i; j++) {
    		portList[j] = tempPortList[j];
    	}
    	
        ArrayList<String> a = new ArrayList<String>();
        for (int y=0; y < portList.length; y++) {
        	a.add(portList[y]);
        }
        Collections.sort(a);
        String[] sortedCommPorts = a.toArray(new String[a.size()]);
    	
        return sortedCommPorts;
        //return portList;
    }
    
    public String[] getModemList(){
        
        DialNotify dnot = new DialNotify();
        DialUpManager.setRegistrationKey("9N2D-PS1G-NU6R-DJU6-IGSZ-BLEX");
        try {
            dialUp = new DialUpManager(dnot);
        } catch (LibraryLoadFailedException e1) {
            Debug.printStackTrace(e1);
        }
        DialUpDevice[] dev = dialUp.getDialUpDevices();
        String[] allList = new String[dev.length];
        int j = 0;
        for (int i=0; i<dev.length; i++){
            if(dev[i].getType().equals("modem")){
                allList[j] = dev[i].getName();
                j++;
            }
        }
        
        String[] modemList = new String[j];
        for (int i=0; i<modemList.length; i++){
            modemList[i] = allList[i];
        }
        
        return modemList;
    }


    public int getSelectedModem(){
        int modemIndex = -1;
        String modemSelection = UnitProfile.getInstance().get("MODEM_NAME", "");  

        String[] modemList = getModemList();
        
        for (int i=0; i<modemList.length; i++){
            if(modemList[i].equalsIgnoreCase(modemSelection)){
                modemIndex = i;
                break;
            }
        }
        if(modemIndex < 0){
            modemIndex = 0;
        }
            
        return modemIndex;
    }
    
    /** If you have a value for the following fields (HOST_SECONDARY_PHONE, 
     	SECONDARY_DUN_USER_ID, SECONDARY_DUN_PASSWORD you are eligible to perform
     	a secondary dial.  The dialing sequence is the following:
     	1 Primary, 2 Primary, 3 Secondary, 4 Secondary, 
     	5 Primary, 6 Primary, 7 Secondary
     	
    **/
    private boolean usePrimaryDialupMode(){
    	boolean retValue = true;
    	String hostSecondaryPhone = UnitProfile.getInstance().get("HOST_SECONDARY_PHONE","");
    	String secondaryDunUserid = UnitProfile.getInstance().get("SECONDARY_DUN_USER_ID","");
    	String secondaryDunPassword = UnitProfile.getInstance().get("SECONDARY_DUN_PASSWORD","");
    	if (hostSecondaryPhone.length()< 1 || 
    		secondaryDunUserid.length() <1 || 
    		secondaryDunPassword.length() < 1) {
    		Debug.println("Secondary Dial Up not configured correctly using primary!!!!");
    		return retValue;
    	}

        switch (this.dialAttempt) {
        	// Primary cases
        	case 0:  
        	case 1:  
        	case 4:
        	case 5:  retValue = true;
        		break;
        	case 2:
        	case 3:
        	case 6: retValue = false;
        		break;
        }
        return retValue;
    }
    
    private void setDialUpMode() {
    	
    	
        DialNotify dnot = new DialNotify();
        
        String hostSpecialDial = UnitProfile.getInstance().find("HOST_SPECIAL_DIAL").stringValue();
        String phoneNumber = UnitProfile.getInstance().find("HOST_PRIMARY_PHONE").stringValue();
        String scriptName = UnitProfile.getInstance().get("PRIMARY_DIALUP_SCRIPT_NAME", "");
        
        if (!usePrimaryDialupMode()){
            hostSpecialDial = UnitProfile.getInstance().find("SECONDARY_HOST_SPECIAL_DIAL").stringValue();
            phoneNumber = UnitProfile.getInstance().find("HOST_SECONDARY_PHONE").stringValue();
            scriptName = UnitProfile.getInstance().get("SECONDARY_DIALUP_SCRIPT_NAME", "");       	
        }
        
        boolean isProfileModemAvailable = false;
        DialUpManager.setRegistrationKey("9N2D-PS1G-NU6R-DJU6-IGSZ-BLEX");
        try {
            dialUp = new DialUpManager(dnot);
         } catch (LibraryLoadFailedException e1) {
        	 Debug.printStackTrace(e1);
        }
        String modemName = UnitProfile.getInstance().get("MODEM_NAME","");
        DialUpDevice devUsed = dialUp.getDialUpDeviceModem();
        DialUpDevice[] devList = dialUp.getDialUpDevices();
        for (int i=0; i<devList.length; i++){
            if (modemName.equalsIgnoreCase(devList[i].getName())){
                devUsed = devList[i]; 
                isProfileModemAvailable = true;  
            }
        }
        // If the profile value is not found as an available Modem
        // default to the first modem the JDUN finds.
        if (!isProfileModemAvailable){
            UnitProfile.getInstance().set("MODEM_NAME", devUsed.getName(),"P", true);
            Debug.println("Profile value for MODEM_NAME not valid setting to " + devUsed.getName());
        }


        DialUpEntryProperties props = new DialUpEntryProperties(hostSpecialDial + phoneNumber, devUsed);
        props.setUseDefaultGatewayOnRemoteNetwork(true);     
        props.setUseIPHeaderCompression(true);
        
        if (usePrimaryDialupMode()){
			if (UnitProfile.getInstance().get("PASS_THROUGH_LOGON","").equals("Y")){
	            createRealScript(scriptName, true);
	        	props.setScriptFilePath("mgram.scp");
	        }
	        else{
	        	props.setScriptFilePath("");
	        }
        } else {
			if (UnitProfile.getInstance().get("SECONDARY_PASS_THROUGH_LOGON","").equals("Y")){
	            createRealScript(scriptName, false);
	        	props.setScriptFilePath("mgram.scp");
	        }
	        else{
	        	props.setScriptFilePath("");
	        }
        	
        }

        // create a DUN connection
        dialUp.createEntry(entry, props);
    }
    
    public void hangupModem(){
        if (dialUp != null) {
            dialUp.hangUpEntry(entry);
            dialUp.deleteEntry(entry);
        }        
    }

    /**
     * Listener used to block until the connection is complete or failed, so we
     * can use it for a sync connection. Refactored from RasActivatorImpl.
     */
    class InnerListener extends RasAdapter {
        // lock object to block - unblock the caller for sync connection
        Object myLock = null;

        int errorCode;

        public InnerListener(Object lock) {
            myLock = lock;
        }

        // dialing is done
        private void releaseLock() {
            errorCode = 0;
            synchronized (myLock) {
                myLock.notifyAll();
            }
        }

        // check to see if dialing failed
        private void releaseLock(int rasStatusCode) {
            if (rasStatusCode != 0) {
                errorCode = rasStatusCode;
                synchronized (myLock) {
                    myLock.notifyAll();
                }
            }
        }

        public int getErrorCode() {
            return errorCode;
        }

        /* The rest of this inner class implements RasListener. */

        @Override
		public void connected(int rasStatusCode) {
            if (rasStatusCode == 0) {
                releaseLock();
            } else {
                releaseLock(rasStatusCode);
            }
        }

        @Override
		public void disconnected(int rasStatusCode) {
            releaseLock(rasStatusCode);
        }

        @Override
		public void attemptSequenceFailed() {
            releaseLock(-1);
        }

        @Override
		public void deviceConnected(int rasStatusCode) {
            releaseLock(rasStatusCode);
        }

        @Override
		public void openPort(int rasStatusCode) {
            releaseLock(rasStatusCode);
        }

        @Override
		public void portOpened(int rasStatusCode) {
            releaseLock(rasStatusCode);
        }

        @Override
		public void connectDevice(int rasStatusCode) {
            releaseLock(rasStatusCode);
        }

        @Override
		public void allDevicesConnected(int rasStatusCode) {
            releaseLock(rasStatusCode);
        }

        @Override
		public void authenticate(int rasStatusCode) {
            releaseLock(rasStatusCode);
        }

        @Override
		public void authNotify(int rasStatusCode) {
            releaseLock(rasStatusCode);
        }

        @Override
		public void authRetry(int rasStatusCode) {
            releaseLock(rasStatusCode);
        }

        @Override
		public void authCallback(int rasStatusCode) {
            releaseLock(rasStatusCode);
        }

        @Override
		public void authChangePassword(int rasStatusCode) {
            releaseLock(rasStatusCode);
        }

        @Override
		public void authProject(int rasStatusCode) {
            releaseLock(rasStatusCode);
        }

        @Override
		public void authLinkSpeed(int rasStatusCode) {
            releaseLock(rasStatusCode);
        }

        @Override
		public void authAck(int rasStatusCode) {
            releaseLock(rasStatusCode);
        }

        @Override
		public void reAuthenticate(int rasStatusCode) {
            releaseLock(rasStatusCode);
        }

        @Override
		public void authenticated(int rasStatusCode) {
            releaseLock(rasStatusCode);
        }

        @Override
		public void prepareForCallback(int rasStatusCode) {
            releaseLock(rasStatusCode);
        }

        @Override
		public void waitForModemReset(int rasStatusCode) {
            releaseLock(rasStatusCode);
        }

        @Override
		public void projected(int rasStatusCode) {
            releaseLock(rasStatusCode);
        }

        @Override
		public void subentryConnected(int rasStatusCode) {
            releaseLock(rasStatusCode);
        }

        @Override
		public void interactive(int rasStatusCode) {
            releaseLock(rasStatusCode);
        }

        @Override
		public void retryAuthentication(int rasStatusCode) {
            releaseLock(rasStatusCode);
        }

        @Override
		public void callbackSetByCaller(int rasStatusCode) {
            releaseLock(rasStatusCode);
        }

        @Override
		public void passwordExpired(int rasStatusCode) {
            releaseLock(rasStatusCode);
        }

        @Override
		public void invokeEAPUI(int rasStatusCode) {
            releaseLock(rasStatusCode);
        }
    }

    /**
     * Indicates future interest in a connection. Triggers a dialing sequence,
     * but returns immediately. Really just a special case of run(), where the
     * caller doesn't have any logic to run yet.
     */
    public static void willNeed() {
        MessageLogic logic = new MessageLogic() {
            @Override
			public Object run() {
                try {
                    MessageFacadeParam param = new MessageFacadeParam(
                            MessageFacadeParam.REAL_TIME,
                            MessageFacadeParam.NO_DIALOG,
                            MessageFacadeParam.WILL_NEED);
                    MessageFacade.getInstance().ensureConnection(param);
                } catch (RasException e) {
                } catch (InterruptedException e) {
                }
                return null;
            }
        };
        run(logic, null, 0, null);
    }

    /**
     * Unify error handling. Assign a unique error number to everything than can
     * go wrong during IO to the middleware, or at least to all conditions that
     * any callers want to handle themselves. If the error number is on the list
     * of errors our caller set in the parameters, throw the error to the
     * caller. Otherwise handle the error internally.
     *
     * If AUTO_RETRY is set in the parameters, and the AC error code is 900 or
     * above, and the retry limit hasn't been reached, this method returns
     * RETRY.
     *
     * If NO_DIALOG or NON_REAL_TIME is set, returns CANCEL. Otherwise, shows a
     * retry/cancel dialog.
     *
     * Future enhancement: Show an OK dialog instead of retry/cancel if the AC
     * error code is 600 or lower. (Errors in that range are client errors,
     * meaning the middleware will always return the same error no matter how
     * many times we send the request.) But this isn't really in scope for
     * version 5.0 and we don't have a variant of okDialog() that waits like
     * retryCancelDialog() does.
     *
     * @return RETRY if the user wants to try again, otherwise CANCEL.
     * @throws MessageFacadeError
     *             if the error is listed in parameters.
     *
     * Note that USER_CANCEL is a condition that callers can specify on the
     * list, so thread interrupts can result in either a return or a thrown
     * exception.
     */
    
    private int handleError(MessageFacadeParam parameters, Exception e) {
        boolean errorFlag = false;
//        hasBillerData = true;
        String userID = "0";
        userID = String.valueOf(getUserID());
            
        MessageFacadeError result = new MessageFacadeError(e);
        // then dig in the nested exceptions to see if known error
        if (e instanceof MessageFacadeError) {
            result = (MessageFacadeError) e;
            if (((result.hasErrorCode(403)) && (!UnitProfile.getInstance().isSubAgent())) || (result.hasErrorCode(413))) {
                UnitProfile.getInstance().find("POS_STATUS").setValue("D");
            //if host returns a 410 check if the POS has changed the item
            // status to A
            //else set it D.
            } else if (result.hasErrorCode(419)) {
                UnitProfile.getInstance().find("POS_STATUS").setValue("X");
            //if host returns a 410 check if the POS has changed the item
            // status to A
            //else set it D.
            } else if (result.hasErrorCode(410)) {
                ProfileItem item = UnitProfile.getInstance().find("SA_RESTRICT_STATUS");
                if (! item.isChanged()) {
                    item.setValue("D");
                }
            }
            if (result.errorCodesBetween(400, 500)) {
                errorFlag = true;
            }
            AuditLog.writeErrorRecord(userID, String.valueOf(result.getErrorCodeList()), result.getMessage());
            
            if (result.hasErrorCode(1049)) {
         	    ProfileAccessor accessor = ProfileAccessor.getInstance();
        	    if (accessor.getStandardReceiveEnabled()) {
//        	    	return result.getErrorCode();
        	    	return 702;
        	    }
            }
        } else if (e instanceof InterruptedException) {
            result = new MessageFacadeError(MessageFacadeParam.USER_CANCEL, Messages.getString("MessageFacade.1808"));
        } else if (e instanceof IOException) {
            // This can happen during software download. No special handling is
            // needed, the MessageFacadeError(OTHER_ERROR) created above is
            // fine.
            Debug.println(e.toString());
        } else {
        	AuditLog.writeErrorRecord(userID, String.valueOf(result.getErrorCodeList()), result.getMessage());
            Debug.println("  handleError doesn't know this one:");
            Debug.printStackTrace(e);
        }

        // If the caller wants to handle this error code, let it.
        // Otherwise we show a dialog ourselves.
        if (parameters.callerWillHandle(result.getErrorCodes())) {
            throw result;
        } else if (result.hasErrorCode(MessageFacadeParam.USER_CANCEL)) {
            return CANCEL;
        } else if (parameters.handleAutoRetry() && result.errorCodesBetween(900, Integer.MAX_VALUE)) {
            return RETRY;
        } else if (parameters.get(MessageFacadeParam.NO_DIALOG) || parameters.get(MessageFacadeParam.NON_REAL_TIME)) {
            return CANCEL;
        // if a 400 series error is returned during the transmit process ,
        // ignore and continue process.
    	} else if (errorFlag && DeltaworksMainPanel.isDuringManualTransmit()) {
            DeltaworksMainPanel.continueCheckinProcess = true;
            if (result.hasErrorCode(403)) {
                MainPanelDelegate.setDeauthMessage(true);
            } else if (result.hasErrorCode(419)) {
                MainPanelDelegate.setDeActivateMessage(true);
            }
            
            return 99;
        } else if (SimplePMTransaction.isSaProfileItemFlag()) {
            return retryCancelOption;
        } else {
            //autoRetryCount = 0;
            int rty = retryCancelDialog(result, parameters);
            return rty;
        }
    }

	/** 
	 * Returns the data model for an AC message and fills in the header fields.
	 */

    public void populateRequestWithHeader(Request request) {
		
		//request.setApiVersion(API_VERSION);
		
		request.setClientSoftwareVersion(UnitProfile.getInstance().getVersionNumber(false));
		request.setTimeStamp(TimeUtility.toXmlCalendar(TimeUtility.currentTime()));
		request.setUnitProfileID(UnitProfile.getInstance().getUnitProfileID());
		request.setPoeType(UnitProfile.getInstance().getPoeType());
		request.setChannelType(UnitProfile.getInstance().getChannelType());
		request.setTargetAudience(UnitProfile.getInstance().getTargetAudience());
		request.setPoeCapabilities(POE_CAPABILITIES);
		DeltaworksMainPanel mp =  DeltaworksMainPanel.getMainPanel();
		if (mp != null) {
			ClientTransaction tran =  DeltaworksMainPanel.getMainPanel().getCurrentTransaction();
			if (tran != null) {
				int iUserId =tran.getUserID();
				if (iUserId > 0) {
					Profile userProfile = (Profile) UnitProfile.getInstance().getProfileInstance().find("USER", iUserId);
					String opName = userProfile.find("NAME").stringValue();
					if (opName != null) {
						if (opName.length() > 7) {
							opName = opName.substring(0, 7);
						}
						request.setOperatorName(opName);
					}
				}
			}
		}
		
		if (UnitProfile.getInstance().get("SUBAGENT_MODE", "N").equals("Y")) {
			request.setAgentID(UnitProfile.getInstance().get("AGENT_ID", ""));
			String seq = UnitProfile.getInstance().get("AGENT_POS_SEQ", "1");
			if (seq.length() > 2) {
				seq = seq.substring(seq.length() - 2);
			}
			request.setAgentSequence(seq);
		} else {
			request.setAgentID(null);
			request.setAgentSequence(null);
		}
		request.setLanguage(AgentCountryInfo.getCurrentLocale());
		//request.setToken(String.valueOf(UnitProfile.getInstance().getLastCheckInTime().getTime()));
	}

    /**
     * Configure the dialog to show an error message with Retry and Cancel
     * buttons.
     */
    private int retryCancelDialog(MessageFacadeError e, MessageFacadeParam parameters) {
        String errorText = e.getMessage();

        Debug.println("----- (Communication Retry/Cancel Dialog)"); 
        Debug.println(futureError);
        Debug.println(errorText);
        List<String> errors = getErrors(e);
        // setting the communication dialog box to disabled will not allow
        // the user to click any buttons until the dialog box is ready.
        CommunicationDialog.getInstance().setEnabled(false);
        CommunicationDialog.getInstance().setCancelButtonProperty(true);
        CommunicationDialog.getInstance().setFocusTo(CommunicationDialog.RETRY);
        CommunicationDialog.getInstance().setRetryButtonProperty(true);
      
        boolean closeDialogFlag = ! parameters.get(MessageFacadeParam.DO_NOT_HIDE);
		if (errors != null && errors.size() > 0) {
	        CommunicationDialog.getInstance().setErrorMessages(futureError, errors, closeDialogFlag);
			if (parameters.get(MessageFacadeParam.HIDE_WITHOUT_FOCUS_UNTIL_ERROR)) {
				this.showDialog();
			}
		}

        // Fix mysterious bug where old Deltagram is spontaneously reprinted.
        // Create a new lock so retry button doesn't notify any but the most
        // recent caller of this method.
//        retryLock = new WaitToken();
        retryLock.setWaitFlag(true);

        synchronized (retryLock) {
            try {
                CommunicationDialog.getInstance().setEnabled(true);
                while (retryLock.isWaitFlag()) {
                	retryLock.wait();
                }
            } catch (InterruptedException ie) {
            }
        }
        return retryCancelOption;
    }
    
    private List<String> getErrors(MessageFacadeError e) {
    	List<BusinessError> errors = e.getErrors();
    	if ((errors != null) && (! errors.isEmpty())) {
    		List<String> retList = new ArrayList<String>();
    		for (BusinessError errorInfo : errors) {
    			int errorCode = errorInfo.getErrorCode().intValue(); 
    			String errorText;
    			if (errorCode == MessageFacadeParam.OTHER_ERROR) {
    				errorText = MessageFacadeError.formatCommunicationsErrorMessageText(errorInfo);
    			} else if (errorCode == MessageFacadeParam.MO_OFAC_CHECK_FAILED) {
    				errorText = errorInfo.getMessage();
    			} else if (errorCode == MessageFacadeParam.USER_CANCEL) {
    				errorText = errorInfo.getMessage();
    			} else {
    				errorText = MessageFacadeError.formatErrorMessageText(errorInfo);
    			}
    			retList.add(errorText);
    		}
    		return retList;
    	}
		return null;
    }
    /**
     * Configure the dialog to show an information message with an OK button.
     * The original intention was that this call would block until the dialog
     * closed (eg until OK was clicked) but that blocking was never implemented
     * and the existing callers seem to be written not to care.
     */
    private static void okDialog(String s1, String s2, MessageFacadeParam parameters) {
    	int buttonOptions = CommunicationDialog.ENABLE_RETRY_BUTTON;
    	if (! parameters.get(MessageFacadeParam.DO_NOT_HIDE)) {
    		buttonOptions += CommunicationDialog.ENABLE_CANCEL_BUTTON;
    	}
    	String message = s1 + " \n\r" + s2;
        CommunicationDialog.getInstance().setMessage(message, buttonOptions);
        Debug.println("----- (Communication OK Dialog)"); 
        Debug.println(s1);
        Debug.println(s2);
    }

    /** Records text for line one of the dialog. */
    public static void note(String status, String error) {
    	ExtraDebug.println("-x-Note["+ status +"]"); 
		ExtraDebug.println("-x-setActivity[" + status + "]"); 
        CommunicationDialog.getInstance().setMessage(status + ".", CommunicationDialog.ENABLE_CANCEL_BUTTON);
        futureError = error + "."; 
    }

    /**
     * Set up a thread to run the transaction's message handling logic. Ensure
     * that commComplete gets called.
     */
    public static void run(final MessageLogic logic,
            final CommCompleteInterface cci, final int commTag,
            final Object defaultValue) {
        current = new Thread(tg, new Runnable() {
      
            @Override
			public void run() {
                Object result = defaultValue;
                try {
                    result = logic.run();
                    // Note: This should really be Thread.currentThread().interrupted(), but
                    // works as written because there is only one foreground comm at a time.
                    if (Thread.interrupted()) {
                        Debug.println("MessageFacade.run() current.interrupted() = true"); 
                        Debug.println("  setting result to defaultValue = " + defaultValue); 
                        result = defaultValue;
                    } else {
                    	ExtraDebug.println("MessageFacade.run() current.interrupted() = false");
                    	ExtraDebug.println("Thread:" + Thread.currentThread().getName());
                    }
                }
                // no catch here, because logic.run() isn't allowed to throw
                // anything
                finally {
                    final Object invoke_result = result;
                    if (cci != null) {
                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
							public void run() {
                                cci.commComplete(commTag, invoke_result);
                            }
                        });
                    }
                    //                    hideDialog();
                }
            }
        });
        current.start();
    }

    /**
     * Run the transaction's message handling logic synchronously. Used by debug
     * and diag FTP. Could be used by software download. Also used by runACycle
     * for batch item processing.
     */
    public static Object run(final MessageFacadeParam parameters,
            final MessageLogic logic, final Object defaultValue) {
        Object result = defaultValue;

        try {
            MessageFacade.getInstance().ensureConnection(parameters);
            MessageFacade.getInstance().pauseTimer();
            result = logic.run();

            if (Thread.interrupted()) {
            	ExtraDebug.println("Interrupted in run(), Thread:" + Thread.currentThread().getName());
                result = defaultValue;
            }
        } catch (Exception exception) {
            if (MessageFacade.getInstance().handleError(parameters, exception) == RETRY)
                return run(parameters, logic, defaultValue);
        } finally {
            MessageFacade.getInstance().restartTimer();
            MessageFacade.getInstance().dropConnection(parameters);
        }

        return result;
    }
    
    //==========================================================================

	public synchronized List<CustomerInfo> consumerHistoryLookup(MessageFacadeParam parameters, String number, int lookupType, SessionType sessionType, String filter) throws MessageFacadeError {
		ConsumerHistoryLookupRequest request = new ConsumerHistoryLookupRequest();
        note(Messages.getString("MessageFacade.1876"), Messages.getString("MessageFacade.1877"));
		try {
			populateRequestWithHeader(request);
			
			if ((lookupType < 0) || (lookupType > DWValues.LOOKUP_MAX)) {
	        	Debug.println("moneyGramConsumerLookup - Bad Lookup Type["+lookupType+"]");
	        	throw new MessageFacadeError(MessageFacadeParam.MG_CUSTOMER_NOT_FOUND, "Customer is not found.");
	        }
			  
			if (lookupType == DWValues.LOOKUP_CUST_ID) {
				request.setMgiRewardsNumber(number);
			} else if (lookupType == DWValues.LOOKUP_PHONE) {
				request.setConsumerPhone(number);
			} else if (lookupType == DWValues.LOOKUP_ACCT) {
				request.setBillerAccountNumber(number);
			}
			
			request.setMgiSessionType(sessionType);
			
			int sRows = 10;

			request.setMaxReceiversToReturn(sRows);
			request.setMaxSendersToReturn(sRows);
			ensureConnection(parameters);
			
			ConsumerHistoryLookupResponse moneyGramConsumerLookupResponse = getAgentConnectLocal().consumerHistoryLookup(request, LIVE_TRAINING);
			
			if ((moneyGramConsumerLookupResponse != null) && (moneyGramConsumerLookupResponse.getErrors() != null) && (! moneyGramConsumerLookupResponse.getErrors().getError().isEmpty())) {
				throw new MessageFacadeError(moneyGramConsumerLookupResponse.getErrors().getError());
			}
			
			List<SenderLookupInfo> senders = ((moneyGramConsumerLookupResponse != null) && 
					(moneyGramConsumerLookupResponse.getPayload() != null) &&
					(moneyGramConsumerLookupResponse.getPayload().getValue().getSenderInfos() != null)) ? moneyGramConsumerLookupResponse.getPayload().getValue().getSenderInfos().getSenderInfo() : new ArrayList<SenderLookupInfo>();
			List<CustomerInfo> customerInfos = messageFacadeHelper.convertToCustomerInfo(senders);
			
			// If a filter was provided, check that the first three characters of 
			// the last name are the same as the first three characters of the filter.
			
			if (filter != null) {
				String s1 = filter.length() > 2 ? filter.substring(0, 3) : filter;
				List<CustomerInfo> filteredCustomerInfos = new ArrayList<CustomerInfo>();
				for (CustomerInfo info : customerInfos) {
					String lastName = info.getSender().getLastName(); 
					if (lastName != null) {
						String s2 = lastName.length() > 2 ? lastName.substring(0, 3) : lastName;
						if (s1.equalsIgnoreCase(s2)) {
							filteredCustomerInfos.add(info);
						}
					}
				}
				customerInfos = filteredCustomerInfos;
			}
			
			if (customerInfos != null) {
				return customerInfos;
			}
			
		} catch (Exception exception) {
			int errResult = handleError(parameters, exception);
			if (errResult == RETRY) {
				return consumerHistoryLookup(parameters, number, lookupType, sessionType, filter);
			} else {
				if (errResult == CANCEL) {
					throw new MessageFacadeError(MessageFacadeParam.USER_CANCEL, "Canceled");
				}
			}
		} finally {
			dropConnection(parameters);
		}
		return null;
	}
    
    /**
     * Looks up BP billers.
     */
    public synchronized BillerInfo[] getBillersList(MessageFacadeParam parameters,
    		BillerSearchType searchType, ProductVariantType prodVar, String indId, String recCode, String billerName,
			String billerId, String bin, BigDecimal defaultInformationalFee, boolean isDefaultMaxFee) {

    	BillerInfo[] result = new BillerInfo[0];
    	boolean cached = false;

        note(
        		Messages.getString("MessageFacade.1868"),
                Messages.getString("MessageFacade.1869"));

        try {
            BillerSearchRequest billerSearchRequest = new BillerSearchRequest();
            populateRequestWithHeader(billerSearchRequest);
            billerSearchRequest.setSearchType(searchType);
            billerSearchRequest.setProductVariant(prodVar);
            if (indId != null && ! indId.trim().equalsIgnoreCase("All")) {
            	billerSearchRequest.setIndustryID(indId);
            }
    		// Work around for new demo server. Since "ALL" is not sent in the
            // industryID field.
            if (UnitProfile.getInstance().isDemoMode() && indId != null && indId.trim().equalsIgnoreCase("( All )")) {
            	billerSearchRequest.setIndustryID("GAS");
            }
            billerSearchRequest.setReceiveCode(recCode);
            billerSearchRequest.setBillerName(billerName);
            billerSearchRequest.setReceiveAgentID(billerId);
            billerSearchRequest.setBinNumber(bin);
            billerSearchRequest.setDefaultInformationalFee(defaultInformationalFee);
            billerSearchRequest.setDefaultMaxFee(isDefaultMaxFee);
            
            // Check if you are retrieving from cache if so don't ensure connection
            // and retrieve from file.
            
            Debug.println("Real Time Biller Infomation");
	        ensureConnection(parameters);
	        BillerSearchResponse billerSearchResponse = getAgentConnectLocal().billerSearch(billerSearchRequest, LIVE_TRAINING);
	        
	        List<BillerInfo> billerInfos = billerSearchResponse.getPayload().getValue().getBillerInfo();
			if (billerInfos != null) {
//				for (BillerInfo billerInfo : billerInfos) {
//					dw.model.adapters.BillPayInfo newInfo = new dw.model.adapters.BillPayInfo(billerInfo);
//					newInfos.add(newInfo);
//				}
//				result = newInfos.toArray(new dw.model.adapters.BillPayInfo[0]);
//				return result;
				return billerInfos.toArray(new BillerInfo[0]);
			}
			
//            if (result.length > 0) {
//            	hasBillerData = true;
//            } else {
//            	hasBillerData = false;
//            }

        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY)
                return getBillersList(parameters, searchType, prodVar, indId, 
                		recCode, billerName, billerId, bin, defaultInformationalFee, isDefaultMaxFee);
        } finally {
        	if(!cached)
        		dropConnection(parameters);
        }
        return result;
    }

    /** Looks up MSG or EP fee. 
     * Change from returning an array of FeeMsgMC since the response now has
     * a session ID field that is a sibling to the FeeMsgMC array and that
     * field is needed for subsequent messages.
     */
    public synchronized FeeLookupResponse getFeeCalculation(FeeLookupRequest feeLookupRequest, MessageFacadeParam parameters) {

        note(Messages.getString("MessageFacade.1814"),
                Messages.getString("MessageFacade.1815"));
        FeeLookupResponse result = null;
        try {
        	populateRequestWithHeader(feeLookupRequest);
        	
            /*
             * If doing all options, do not limit to a single currency.
             */
            if (feeLookupRequest.isAllOptions()) {
            	feeLookupRequest.setReceiveCurrency(null);
            }

            ensureConnection(parameters);
            FeeLookupResponse feeLookupResponse = getAgentConnectLocal().feeLookup(feeLookupRequest, LIVE_TRAINING);
                
            List<FeeInfo> feeInfos = feeLookupResponse != null ? feeLookupResponse.getPayload().getValue().getFeeInfos().getFeeInfo() : null;
            if (feeInfos != null) {
            	result = feeLookupResponse;
            	return result;
            }
        } catch (Exception exception) {
        	
            if (handleError(parameters, exception) == RETRY) {
                return getFeeCalculation(feeLookupRequest, parameters);
            }
            return result;
        } finally {
            dropConnection(parameters);
        }
        return result;
    }

    /**
     * Checks in with the middleware. Returns true if successful. Called from
     * main panel, which keeps its forwarder as a private member variable. So it
     * has to be passed in, which makes this call a little atypical.
     */
    
    public synchronized Object clientCheckIn(CommCompleteInterface cci,
            MessageFacadeParam parameters) {
        note(
            Messages.getString("MessageFacade.1818"), 
            Messages.getString("MessageFacade.1819"));
        Object result = Boolean.FALSE;
        try {
            if (UnitProfile.getInstance().isDemoMode()) {
                return Boolean.TRUE;
            }
            
            CheckInRequest checkInRequest = new CheckInRequest();
            populateRequestWithHeader(checkInRequest);
            
            ensureConnection(parameters);
            // send the first of two checkin messages.
            
            CheckInResponse checkInResponse = getAgentConnectLocal().checkIn(checkInRequest, LIVE_TRAINING);
            
            // good checkin, set last check in time in unit profile
            // and send the second message (confirmation)
            sendConfirmTokenRequest();
            
            if (checkInResponse.getPayload().getValue().getTimeZoneCorrection()!= null) {
	            if (setTimeZoneCorrectionValues(checkInResponse)) {
	            	Scheduler.clearCurrentLocalCalendar();
		            Scheduler.getLocalCalendar();
	            }
            }
                      
            result = Boolean.TRUE;
            MainPanelDelegate.setDeActivateMessage(false);
            MainPanelDelegate.setDeauthMessage(false);
        } catch (Exception e) {
            if (handleError(parameters, e) == RETRY)
                return clientCheckIn(cci, parameters);
        } finally {
            dropConnection(parameters);
        }
        return result;

    }

    private synchronized boolean setTimeZoneCorrectionValues(CheckInResponse response) {
    	
        TimeZoneCorrection correction = TimeZoneCorrection.getInstance();
        CheckInResponse.Payload payload = response.getPayload().getValue();
        
        String id = payload.getTimeZoneCorrection().getTimeZoneId();
        int offset = payload.getTimeZoneCorrection().getOffset();
        int dstSavings = payload.getTimeZoneCorrection().getDstSavings();
        int sMode = payload.getTimeZoneCorrection().getStartMode();
        int sMonth = payload.getTimeZoneCorrection().getStartMonth();
        int sDay = payload.getTimeZoneCorrection().getStartDay();
        int sDow = payload.getTimeZoneCorrection().getStartDayOfWeek();
        int sTime = payload.getTimeZoneCorrection().getStartTime();
        int sTimeMode = payload.getTimeZoneCorrection().getStartTimeMode();       
        int eMode = payload.getTimeZoneCorrection().getEndMode();
        int eMonth = payload.getTimeZoneCorrection().getEndMonth();
        int eDay = payload.getTimeZoneCorrection().getEndDay();
        int eDow = payload.getTimeZoneCorrection().getEndDayOfWeek();
        int eTime = payload.getTimeZoneCorrection().getEndTime();
        int eTimeMode = payload.getTimeZoneCorrection().getEndTimeMode();
        
        if (offset == 99999 || dstSavings == 99999 || sMode == 99999 || sMonth == 99999 ||
            sDay == 99999 || sDow == 99999 || sTime == 99999 || sTimeMode == 99999 ||
			eMode == 99999 || eMonth == 99999 || eDay == 99999 || eDow == 99999 ||
			eTime == 99999 || eTimeMode == 99999 || id.equals("")) {
        	Debug.println("**** Invalid timezone correction values...");
        	return false;
        }
        else {        
	        correction.setId(id);
			correction.setOffset(offset);
			correction.setDstSavings(dstSavings);		
			correction.setStartMode(sMode);
			correction.setStartMonth(sMonth);
			correction.setStartDay(sDay);
			correction.setStartDayOfWeek(sDow);
			correction.setStartTime(sTime);
			correction.setStartTimeMode(sTimeMode);		
			correction.setEndMode(eMode);
			correction.setEndMonth(eMonth);
			correction.setEndDay(eDay);
			correction.setEndDayOfWeek(eDow);
			correction.setEndTime(eTime);
			correction.setEndTimeMode(eTimeMode);				
			return true;      
        }
    }
    
    // Note that this method uses LIVE_TRAINING, which is right because this is
    // only used by checkin, which is part of daily transmit.
    // Changing to private to make sure that stays true.
    // If the apiVersion is not the same as the apiversion in 
    private synchronized void sendConfirmTokenRequest() throws MessageFacadeError {
    	ConfirmTokenRequest confirmTokenRequest = new ConfirmTokenRequest();
    	populateRequestWithHeader(confirmTokenRequest);
        getAgentConnectLocal().confirmToken(confirmTokenRequest,LIVE_TRAINING);
    }

    /** Looks up MoneyGram transaction data by reference number. */
    public synchronized TransactionLookupResponse transactionLookup(TransactionLookupRequest transactionLookupRequest, CommCompleteInterface callingPage, MessageFacadeParam parameters, String refNum, String operatorName, String pin, String purposeOfLookup, boolean isInitial) throws MessageFacadeError {

        note(
        		Messages.getString("MessageFacade.1820"), 
        		Messages.getString("MessageFacade.1821"));  
        TransactionLookupResponse transactionLookupResponse = new TransactionLookupResponse();
        try {
        	populateRequestWithHeader(transactionLookupRequest);
        	String pol = purposeOfLookup.toUpperCase().trim(); 
        	if ((! pol.equals(DWValues.TRX_LOOKUP_SEND_COMP)) && (! pol.equals(DWValues.TRX_LOOKUP_BP_COMP))) {
            	transactionLookupRequest.setReferenceNumber(refNum);
        	} else {
        		transactionLookupRequest.setConfirmationNumber(refNum);
        	}
        	transactionLookupRequest.setPurposeOfLookup(purposeOfLookup);
        	
        	Debug.println("Reference Number in transactionLookup: " + refNum + "; purposeOfLookup in transactionLookup : " + purposeOfLookup);
        	
        	
        	transactionLookupRequest.setOperatorName(operatorName);
        	        	 
	        if ((pin != null) && (! pin.isEmpty())) {
	        	transactionLookupRequest.setTransactionPin(pin.trim());
            }
            ensureConnection(parameters);
            
            transactionLookupResponse = getAgentConnectLocal().transactionLookup(transactionLookupRequest, AUTOMATIC, isInitial);
            if (UnitProfile.getInstance().isDemoMode() && transactionLookupRequest.getReferenceNumber().equals("24682468")) {
            	throw new MessageFacadeError(706, "Please call MoneyGram customer service center to complete this transaction (code 4) (AC error 706). CODE 4: CALL MONEYGRAM AGT SERVICES 800-444-3010. FOR INTR AGTS CALL YOUR TOLL-FREE # FOR AGT SERVICES");
            }    
            return transactionLookupResponse;
        } catch (Exception exception) {
        	int error = handleError(parameters, exception);
        	if (error == RETRY) {
                return transactionLookup(transactionLookupRequest, callingPage, parameters, refNum, operatorName, pin, purposeOfLookup, isInitial);
        	} else if ((error == CANCEL) && Pinpad.INSTANCE.getIsPinpadRequired()) {
            	return null;
            }
            return transactionLookupResponse;
        } finally {
            dropConnection(parameters);
        }
    }    

    public synchronized SendValidationResponse sendValidation(CommCompleteInterface callingPage, MessageFacadeParam parameters, SendValidationRequest sendValidationRequest, DataCollectionSet dataCollectionSet, int rule, String referenceNumber, boolean isInitial, boolean isNotRetry) throws MessageFacadeError {
        dataCollectionSet.setValidationStatus(DataCollectionStatus.ERROR);

        note(Messages.getString("MessageFacade.1844a"), Messages.getString("MessageFacade.1845a"));
        
        try {
        	if (isNotRetry) {
	        	populateRequestWithHeader(sendValidationRequest);
	
	        	// Add fieldValue data to the request.
	        	
	        	List<KeyValuePairType> keyVals = addFieldValues(dataCollectionSet);
				if ((keyVals != null) && (! keyVals.isEmpty())) {
					
					SendValidationRequest.FieldValues values = sendValidationRequest.getFieldValues();
					if (values == null) {
						values = new SendValidationRequest.FieldValues();
						sendValidationRequest.setFieldValues(values);
					}
					values.getFieldValue().addAll(keyVals);
				}
	    		
	        	// Add verifiedFields data to the request.
				
				if ((dataCollectionSet.getVerifiedFieldsSet() != null) && (! dataCollectionSet.getVerifiedFieldsSet().isEmpty())) {
					SendValidationRequest.VerifiedFields verifiedFields = new SendValidationRequest.VerifiedFields();
		    		verifiedFields.getInfoKey().addAll(dataCollectionSet.getVerifiedFieldsSet());
			        sendValidationRequest.setVerifiedFields(verifiedFields);
				}
        	}
        	
            ensureConnection(parameters);

            SendValidationResponse response = getAgentConnectLocal().sendValidation(sendValidationRequest, rule, referenceNumber, isInitial);
            
            // Get the RetryCount from
            
            dataCollectionSet.getRetryCount();
            
			SendValidationResponse.Payload payload = ((response != null) && (response.getPayload() != null)) ? response.getPayload().getValue() : null;
			if ((payload != null) && (payload.getReceipts() != null)) {
				PreCompletionReceiptType receipts = payload.getReceipts();
				Debug.println("Receipt Mime Type: " + receipts.getReceiptMimeType());
				Debug.println("Disclosure Receipt 1: " + getMimeDataLength(receipts.getDisclosure1MimeData()));
				Debug.println("Disclosure Receipt 2: " + getMimeDataLength(receipts.getDisclosure2MimeData()));
			} else {
				Debug.println("No Receipt Data...");
			}
            
        	return response;
            
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY) {
                // Increment the Retry Counters
            	dataCollectionSet.incrementRetryCount();
                return sendValidation(callingPage, parameters, sendValidationRequest, dataCollectionSet, rule, referenceNumber, isInitial, false);
            }
            // Initialize the Retry Counters
            dataCollectionSet.setRetryCount(0);
            
            return null;
        } finally {
            dropConnection(parameters);
        }
    }
    
    public synchronized BPValidationResponse bpValidation(CommCompleteInterface callingPage, MessageFacadeParam parameters, BPValidationRequest bpValidationRequest, DataCollectionSet dataCollectionSet, int rule, String referenceNumber, boolean isInitial, boolean isNotRetry) {

        note(Messages.getString("MessageFacade.1878"), Messages.getString("MessageFacade.1879"));
        
        try {
        	if (isNotRetry) {
	        	populateRequestWithHeader(bpValidationRequest);
	        	
	        	bpValidationRequest.setPrimaryReceiptLanguage((String) dataCollectionSet.getFieldValue(FieldKey.MF_VALID_LANG_RCPT_PRIME_KEY));
	        	String secondaryLanguage = (String) dataCollectionSet.getFieldValue(FieldKey.MF_VALID_LANG_RCPT_SECOND_KEY);
	        	if (!StringUtility.getNonNullString(secondaryLanguage).isEmpty()) {
	               	bpValidationRequest.setSecondaryReceiptLanguage(secondaryLanguage);
	        	}
	
	        	// Add fieldValue data to the request.
	        	
	        	BPValidationRequest.FieldValues values = bpValidationRequest.getFieldValues();
	        	if (values == null) {
					values = new BPValidationRequest.FieldValues();
					bpValidationRequest.setFieldValues(values);
	        	}
	        	
	        	List<KeyValuePairType> keyVals = addFieldValues(dataCollectionSet);
				if ((keyVals != null) && (! keyVals.isEmpty())) {
					values.getFieldValue().addAll(keyVals);
				}
	    		
	        	// Add verifiedFields data to the request.
				
				if ((dataCollectionSet.getVerifiedFieldsSet() != null) && (! dataCollectionSet.getVerifiedFieldsSet().isEmpty())) {
					BPValidationRequest.VerifiedFields verifiedFields = new BPValidationRequest.VerifiedFields();
		    		verifiedFields.getInfoKey().addAll(dataCollectionSet.getVerifiedFieldsSet());
		    		bpValidationRequest.setVerifiedFields(verifiedFields);
				}
        	}

            ensureConnection(parameters);

            BPValidationResponse response = getAgentConnectLocal().bpValidation(bpValidationRequest, rule, referenceNumber, isInitial);
            
            // Get the RetryCount from
            
            dataCollectionSet.getRetryCount();

            return response;
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY) {
            	
            	// On a retry, set the accountNumberRetryCount value to 2 if the product variant is not UBP.
            
            	if (! bpValidationRequest.getProductVariant().equals(ProductVariantType.UBP)) {
	            	List<KeyValuePairType> list = bpValidationRequest.getFieldValues().getFieldValue();
	            	if (list != null) {
	            		for (KeyValuePairType pair : list) {
	            			if (pair.getInfoKey().equals(FieldKey.ACCOUNTNUMBERRETRYCOUNT_KEY.getInfoKey())) {
	        	        		ObjectFactory objectFactory = new ObjectFactory();
	            				pair.setValue(objectFactory.createKeyValuePairTypeValue("2"));
	            				break;
	            			}
	            		}
	            	}
            	}
            	
                return bpValidation(callingPage, parameters, bpValidationRequest, dataCollectionSet, rule, referenceNumber, isInitial, false);
        	} else {
//        		MessageFacadeError mfe = (MessageFacadeError) exception;
//        		if (mfe.hasErrorCode(DWValues.PPC_TEMP_CARD_ERROR) || 
//                		mfe.hasErrorCode(DWValues.PPC_BAD_FIELD_LENGTH) || 
//                		mfe.hasErrorCode(DWValues.PPC_BAD_SENDER_INFO_ERROR) || 
//                		mfe.hasErrorCode(DWValues.PPC_BAD_SENDER_CHARS_ERROR)) {
//                	return false;
//                } else {
                	return null;
//                }
        	}
        } finally {
            dropConnection(parameters);
        }
    }

    public synchronized ReceiveValidationResponse receiveValidation(CommCompleteInterface callingPage,  
    		MessageFacadeParam parameters, DataCollectionSet dataCollectionSet, int payoutMethod,
    		TransactionLookupResponse transactionLookupResponse, ConsumerProfile profile, 
    		boolean isInitial) throws MessageFacadeError {
    	
        dataCollectionSet.setValidationStatus(DataCollectionStatus.ERROR);

        note(
            Messages.getString("MessageFacade.1828"),
            Messages.getString("MessageFacade.1829"));  
        TransactionLookupResponse.Payload payload = transactionLookupResponse.getPayload().getValue();
        String mgiTransactionSessionID = payload.getMgiSessionID();
        try {
        	ReceiveValidationRequest receiveValidationRequest = new ReceiveValidationRequest();
        	populateRequestWithHeader(receiveValidationRequest);

//        	receiveValidationRequest.setReturnCachedInfo(DWValues.MSG_DEFAULT_RETURNED_CACHED_INFO);
        	receiveValidationRequest.setMgiSessionID(mgiTransactionSessionID);
        	receiveValidationRequest.setTransactionStaging(false);

        	// Add field values to the request.
        	
        	List<KeyValuePairType> keyVals = addFieldValues(dataCollectionSet);
        	
        	/*
        	 * Setup check fields (if not Net Settle
        	 */
        	setupCheckFields(dataCollectionSet, keyVals, payoutMethod);
			
        	BigDecimal otherAmount = (BigDecimal) dataCollectionSet.getFieldValue(FieldKey.OTHERPAYOUTAMOUNT_KEY);
        	if (otherAmount != null) {
            	addKeyValuePair(keyVals, dataCollectionSet, FieldKey.OTHERPAYOUTTYPE_KEY.getInfoKey(), (String) dataCollectionSet.getFieldValue(FieldKey.OTHERPAYOUTTYPE_KEY));
            	addKeyValuePair(keyVals, dataCollectionSet, FieldKey.OTHERPAYOUTAMOUNT_KEY.getInfoKey(), otherAmount.toString());
        	}
        	
        	receiveValidationRequest.setReceiveCurrency(payload.getReceiveAmounts().getReceiveCurrency());
        	receiveValidationRequest.setReceiveAmount(payload.getReceiveAmounts().getReceiveAmount());

        	// Add fieldValue data to the request.
            
			if ((keyVals != null) && (! keyVals.isEmpty())) {
				ReceiveValidationRequest.FieldValues value = new ReceiveValidationRequest.FieldValues();
				value.getFieldValue().addAll(keyVals);
				receiveValidationRequest.setFieldValues(value);
			}
    		
        	// Add verifiedFields data to the request.
			
			if ((dataCollectionSet.getVerifiedFieldsSet() != null) && (! dataCollectionSet.getVerifiedFieldsSet().isEmpty())) {
				ReceiveValidationRequest.VerifiedFields verifiedFields = new ReceiveValidationRequest.VerifiedFields();
	    		verifiedFields.getInfoKey().addAll(dataCollectionSet.getVerifiedFieldsSet());
		        receiveValidationRequest.setVerifiedFields(verifiedFields);
			}

			String primaryLanguage = (String) dataCollectionSet.getFieldStringValue(FieldKey.MF_VALID_LANG_RCPT_PRIME_KEY);
        	receiveValidationRequest.setPrimaryReceiptLanguage(primaryLanguage);
			String secondaryLanguage = (String) dataCollectionSet.getFieldStringValue(FieldKey.MF_VALID_LANG_RCPT_SECOND_KEY);
			if (!StringUtility.getNonNullString(secondaryLanguage).isEmpty()) {
				receiveValidationRequest.setSecondaryReceiptLanguage(secondaryLanguage);
			}

            ensureConnection(parameters);

            ReceiveValidationResponse response = getAgentConnectLocal().receiveValidation(receiveValidationRequest, AUTOMATIC, isInitial);
            
            // if in demo mode update the session ID so we can move on to the 
            // next validation response in the cycle 
             if (UnitProfile.getInstance().isDemoMode()) {
            	payload.setMgiSessionID(response.getPayload().getValue().getMgiSessionID());            
            }
            
            // Get the RetryCount from
            
            dataCollectionSet.getRetryCount();
            
			ReceiveValidationResponse.Payload rvPayload = ((response != null) && (response.getPayload() != null)) ? response.getPayload().getValue() : null;
			if ((rvPayload != null) && (rvPayload.getReceipts() != null)) {
				PreCompletionReceiptType receipts = rvPayload.getReceipts();
				Debug.println("Receipt Mime Type: " + receipts.getReceiptMimeType());
				Debug.println("Disclosure Receipt 1: " + getMimeDataLength(receipts.getDisclosure1MimeData()));
				Debug.println("Disclosure Receipt 2: " + getMimeDataLength(receipts.getDisclosure2MimeData()));
			} else {
				Debug.println("No Receipt Data...");
			}
            
        	return response;
            
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY) {
                // Increment the Retry Counters
            	dataCollectionSet.incrementRetryCount();
                return receiveValidation(callingPage, parameters, dataCollectionSet, payoutMethod, transactionLookupResponse, profile, isInitial);
            }
            // Initialize the Retry Counters
            dataCollectionSet.setRetryCount(0);
            
            return null;
        } finally {
            dropConnection(parameters);
        }
    }
    
    private List<KeyValuePairType> addFieldValues(DataCollectionSet dataSet) {
    	List<KeyValuePairType> keyValuePairs = new ArrayList<KeyValuePairType>();
    	
		ObjectFactory objectFactory = new ObjectFactory();
		
    	for (DataCollectionField field : dataSet.getFieldMap().values()) {
    		
    		// Do not include the item if the panel is hidden on the data collection screen.
    		
//    		if ((! item.isHidden()) && ((item.getPanel() == null) || (! item.getPanel().isVisible()))) {
//    			continue;
//    		}
    		
    		// Do not include the item if the item is a child field that is not currently visible.
    		
    		if ((field.getParentField() != null) && (! field.isChildFieldVisible())) {
    			continue;
    		}
    		
    		// Get the xmlTag and value for the item. If no value has been specified
    		// for the item, use the default value if exists.
    		
    		String xmlTag = field.getInfoKey();
    		String fieldValue = field.getValue();
    		String defaultValue = field.getDefaultValue();
    		
    		// Add the item to the request as a keyValuePair if a value exists.

    		String value = null;
    		if ((fieldValue != null) && (! fieldValue.isEmpty())) {
    			value = fieldValue;
    		} else if ((defaultValue != null) && (! defaultValue.isEmpty())) {
    			if ((field.isDisplay() && field.isModified())) {
    				value = "";
    			} else {
    				value = defaultValue;
    			}
    		}
    		
    		if (value != null) {
    			KeyValuePairType keyValuePair = new KeyValuePairType();
				keyValuePair.setInfoKey(xmlTag);
				keyValuePair.setValue(objectFactory.createKeyValuePairTypeValue(value));
				keyValuePairs.add(keyValuePair);
				dataSet.getCurrentValueMap().put(xmlTag, value);
//    		} else {
//    			dataSet.getCurrentValueMap().remove(xmlTag);
    		}
    	}
		return keyValuePairs;
    }

    private List<KeyValuePairType> addFieldValuesToAllowEmptyValues(DataCollectionSet dataSet) {
    	List<KeyValuePairType> keyValuePairs = new ArrayList<KeyValuePairType>();
		ObjectFactory objectFactory = new ObjectFactory();
		
    	for (DataCollectionField field : dataSet.getFieldMap().values()) {
    		
    		// Do not include the item if the panel is hidden on the data collection screen.
    		
//    		if ((! item.isHidden()) && ((item.getPanel() == null) || (! item.getPanel().isVisible()))) {
//    			continue;
//    		}
    		
    		// Do not include the item if the item is a child field that is not currently visible.
    		
    		if ((field.getParentField() != null) && (! field.isChildFieldVisible())) {
    			continue;
    		}
    		
    		// Get the xmlTag and value for the item. If no value has been specified
    		// for the item, use the default value if exists.
    		
    		String xmlTag = field.getInfoKey();
    		String value = field.getValue();
    		String defaultValue = field.getDefaultValue();
    		if (field.isVisible()) {
    			value = field.getValue();
    		} else {
    			Object o = dataSet.getFieldValue(field.getKey());
    			if (o != null) {
    				value = o.toString();
    			}
    		}
    		
    		// Add the item to the request as a keyValuePair if a value exists.
    		
    		if ((value != null) && (! value.isEmpty())) {
    			KeyValuePairType keyValuePair = new KeyValuePairType();
				keyValuePair.setInfoKey(xmlTag);
				keyValuePair.setValue(objectFactory.createKeyValuePairTypeValue(value));
				keyValuePairs.add(keyValuePair);
				dataSet.getCurrentValueMap().put(xmlTag, value);
			} else {
				if (! StringUtility.getNonNullString(defaultValue).isEmpty()) {
	    			KeyValuePairType keyValuePair = new KeyValuePairType();
					keyValuePair.setInfoKey(xmlTag);
					keyValuePair.setValue(objectFactory.createKeyValuePairTypeValue(""));
					keyValuePairs.add(keyValuePair);
					dataSet.getCurrentValueMap().put(xmlTag, value);
				} else {
					dataSet.getCurrentValueMap().remove(xmlTag);
				}
			}
    	}
		return keyValuePairs;
    }
 
    private void addKeyValuePair(List<KeyValuePairType> keyValuePairs, DataCollectionSet dataSet, String infoKey, String value) {
		KeyValuePairType keyValuePair = new KeyValuePairType();
		keyValuePair.setInfoKey(infoKey);
		ObjectFactory objectFactory = new ObjectFactory();
		keyValuePair.setValue(objectFactory.createKeyValuePairTypeValue(value));
		keyValuePairs.add(keyValuePair);
		dataSet.getCurrentValueMap().put(infoKey, value);
    }
    
    public synchronized CompleteSessionResponse completeSession(MessageFacadeParam parameters, CompleteSessionRequest completeSessionRequest, Request validationRequest, String referenceNumber) {
    	
    	CompleteSessionResponse response = null;

        note(
            Messages.getString("MessageFacade.1844"),
            Messages.getString("MessageFacade.1845"));
        try {
        	populateRequestWithHeader(completeSessionRequest);
            
//            String mgiTransactionSessionID = dataCollectionSet.getFieldStringValue(FieldKey.MGI_TRANSACTION_SESSION_ID_KEY);
//            TransactionType productType = (TransactionType) dataCollectionSet.getFieldObject(FieldKey.PRODUCT_TYPE_KEY);
//            String stateRegulatorVersion = dataCollectionSet.getFieldStringValue(FieldKey.STATE_REGULATOR_VERSION_KEY);

//            completeSessionRequest.setMgiSessionID(mgiTransactionSessionID);
//            completeSessionRequest.setMgiSessionType(productType);
            completeSessionRequest.setCommit(true);
//            completeSessionRequest.setStateRegulatorVersion(stateRegulatorVersion);
            ensureConnection(parameters);
            response = getAgentConnectLocal().completeSession(completeSessionRequest, validationRequest, referenceNumber, AUTOMATIC);
            
			CompleteSessionResponse.Payload payload = ((response != null) && (response.getPayload() != null)) ? response.getPayload().getValue() : null;
			if ((payload != null) && (payload.getReceipts() != null)) {
				CompletionReceiptType receipts = payload.getReceipts();
				Debug.println("Receipt Mime Type: " + receipts.getReceiptMimeType());
				Debug.println("Agent Receipt: " + getMimeDataLength(receipts.getAgentReceiptMimeData()));
				Debug.println("Consumer Receipt 1: " + getMimeDataLength(receipts.getConsumerReceipt1MimeData()));
				Debug.println("Consumer Receipt 2: " + getMimeDataLength(receipts.getConsumerReceipt2MimeData()));
			} else {
				Debug.println("No Receipt Data...");
			}
            
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY) {
                return completeSession(parameters, completeSessionRequest, validationRequest, referenceNumber);
            }
        } finally {
            dropConnection(parameters);
        }
        
        // Check if the reference number text tag has a value. If it does, 
        // display the content of the tag in a dialog window.
        
        if ((response != null) && (response.getPayload() != null) && (response.getPayload().getValue() != null) && (response.getPayload().getValue().getReferenceNumberText() != null)) {
        	String text = response.getPayload().getValue().getReferenceNumber();
        	String refNumber = response.getPayload().getValue().getReferenceNumberText();
        	DialogReferenceNumberText dialog = new DialogReferenceNumberText(refNumber, text);
        	dialog.show();
        }

        return response;
    }
    
    private int getMimeDataLength(ReceiptResponseType receiptData) {
    	int length = 0;
    	if (receiptData != null) {
	    	for (ReceiptSegmentType segment : receiptData.getReceiptMimeDataSegment()) {
	    		if (segment.getMimeData() != null) {
	    			try {
	    				ByteArrayInputStream is = (ByteArrayInputStream) segment.getMimeData().getContent();
	    				length += is.available();
	    			} catch (Exception e) {
	    			}
	    		}
	    	}
    	}
    			
    	return length;
    }

    /** Directory of agents by phone number. */
    public synchronized MoneyGramAgentInfo[] getAgentsByAreaCodePrefix(
            CommCompleteInterface callingPage, MessageFacadeParam parameters,
            String areaCodePrefix) throws MessageFacadeError {

        MoneyGramAgentInfo[] result = new MoneyGramAgentInfo[0];
        note(
        		Messages.getString("MessageFacade.1830"),
        		Messages.getString("MessageFacade.1831"));

        try {
            ensureConnection(parameters);

            DirectoryOfAgentsByAreaCodePrefixRequest directoryOfAgentsByAreaCodePrefixRequest = new DirectoryOfAgentsByAreaCodePrefixRequest();
            populateRequestWithHeader(directoryOfAgentsByAreaCodePrefixRequest);
            directoryOfAgentsByAreaCodePrefixRequest.setAreaCodePrefix(areaCodePrefix);

            DirectoryOfAgentsByAreaCodePrefixResponse response = getAgentConnectLocal().directoryOfAgentsByAreaCodePrefix(directoryOfAgentsByAreaCodePrefixRequest,LIVE_TRAINING);
            DirectoryOfAgentsByAreaCodePrefixResponse.Payload payload = response.getPayload().getValue();

        	if (payload.getAgentInfo() != null && ! payload.getAgentInfo().isEmpty()) {
    			List<MoneyGramAgentInfo> aInfos = new ArrayList<MoneyGramAgentInfo>();
    			for (AgentInfo agentInfo : payload.getAgentInfo()) {
    				MoneyGramAgentInfo moneyGramAgentInfo = new MoneyGramAgentInfo(agentInfo);
    				aInfos.add(moneyGramAgentInfo);
    			}
    			result = aInfos.toArray(new MoneyGramAgentInfo[0]);
    		}
        	
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY)
                return getAgentsByAreaCodePrefix(callingPage, parameters,
                        areaCodePrefix);
        } finally {
            dropConnection(parameters);
        }
        return result;
    }

    /**
     * Directory of agents partial city name search. Returns list of cities
     * corresponding to city,state and country entered on screen.
     */
	public synchronized String[] getCityList(CommCompleteInterface callingPage, MessageFacadeParam parameters, String city,
			CountrySubdivision state, Country country) throws MessageFacadeError {

		String[] result = null;
		note(Messages.getString("MessageFacade.1830"), 
				Messages.getString("MessageFacade.1831")); 

		try {
			CityListRequest cityListRequest = new CityListRequest();
			ensureConnection(parameters);
			populateRequestWithHeader(cityListRequest);
			cityListRequest.setCountry(country.getCountryCode());
			if ((state.getLegacyCode() != null) && (! state.getLegacyCode().isEmpty())) {
				cityListRequest.setCountrySubdivisionCode(state.getCountrySubdivisionCode());
			}
			
			cityListRequest.setCity(city);
			CityListResponse response = getAgentConnectLocal().cityList(cityListRequest, LIVE_TRAINING);
			List<String> list = response.getPayload().getValue().getCity();
			if (list != null) {
				result = list.toArray(new String[0]);
			}
		} catch (Exception exception) {
			if (handleError(parameters, exception) == RETRY)
				return getCityList(callingPage, parameters, city, state, country);
		} finally {
			dropConnection(parameters);
		}
		return result;
	}

    /** Directory of agents by city name. */
    public synchronized MoneyGramAgentInfo[] getAgentsByCity(
            CommCompleteInterface callingPage, MessageFacadeParam parameters,
            String city, CountrySubdivision countrySubdivision, Country country, boolean citySelect)
            throws MessageFacadeError {

        MoneyGramAgentInfo[] result = new MoneyGramAgentInfo[0];
        note(
        		Messages.getString("MessageFacade.1830"), 
        		Messages.getString("MessageFacade.1831"));  

        try {
            ensureConnection(parameters);

            DirectoryOfAgentsByCityRequest directoryOfAgentsByCityRequest = new DirectoryOfAgentsByCityRequest();
            populateRequestWithHeader(directoryOfAgentsByCityRequest);
 
            directoryOfAgentsByCityRequest.setCountry(country.getCountryCode());
            if ((countrySubdivision.getLegacyCode() != null) && (! countrySubdivision.getLegacyCode().isEmpty())) {
            	directoryOfAgentsByCityRequest.setCountrySubdivisionCode(countrySubdivision.getCountrySubdivisionCode());
            }
			
			directoryOfAgentsByCityRequest.setCity(city);

            DirectoryOfAgentsByCityResponse response = getAgentConnectLocal().directoryOfAgentsByCity(directoryOfAgentsByCityRequest, LIVE_TRAINING);
            
            DirectoryOfAgentsByCityResponse.Payload payload = response.getPayload().getValue();
            if (payload.getAgentInfo() != null && ! payload.getAgentInfo().isEmpty()) {
    			List<MoneyGramAgentInfo> aInfos = new ArrayList<MoneyGramAgentInfo>();
    			for (AgentInfo agentInfo : payload.getAgentInfo()) {
    				MoneyGramAgentInfo moneyGramAgentInfo = new MoneyGramAgentInfo(agentInfo);
    				aInfos.add(moneyGramAgentInfo);
    			}
    			result = aInfos.toArray(new MoneyGramAgentInfo[0]);
    		}
            
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY)
                return getAgentsByCity(callingPage, parameters, city, countrySubdivision,
                        country, citySelect);
        } finally {
            dropConnection(parameters);
        }
        return result;
    }

    /** Directory of agents by zip code. */
    public synchronized MoneyGramAgentInfo[] getAgentsByZipCode(
            CommCompleteInterface callingPage, MessageFacadeParam parameters,
            String zipCode, Country country) throws MessageFacadeError {

        MoneyGramAgentInfo[] result = new MoneyGramAgentInfo[0];
        note(
            Messages.getString("MessageFacade.1830"),
            Messages.getString("MessageFacade.1831"));

        try {
        	DirectoryOfAgentsByZipRequest directoryOfAgentsByZipRequest = new DirectoryOfAgentsByZipRequest();
        	populateRequestWithHeader(directoryOfAgentsByZipRequest);
        	
        	directoryOfAgentsByZipRequest.setZipCode(zipCode);
        	directoryOfAgentsByZipRequest.setCountry(country.getCountryCode());
            
            ensureConnection(parameters);
            DirectoryOfAgentsByZipResponse response = getAgentConnectLocal().directoryOfAgentsByZip(directoryOfAgentsByZipRequest,LIVE_TRAINING);
            DirectoryOfAgentsByZipResponse.Payload payload = response.getPayload().getValue();
            if (payload.getAgentInfo() != null && ! payload.getAgentInfo().isEmpty()) {
    			List<MoneyGramAgentInfo> aInfos = new ArrayList<MoneyGramAgentInfo>();
    			for (AgentInfo agentInfo : payload.getAgentInfo()) {
    				MoneyGramAgentInfo moneyGramAgentInfo = new MoneyGramAgentInfo(agentInfo);
    				aInfos.add(moneyGramAgentInfo);
    			}
    			result = aInfos.toArray(new MoneyGramAgentInfo[0]);
    		}
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY)
                return getAgentsByZipCode(callingPage, parameters, zipCode, country);
        } finally {
            dropConnection(parameters);
        }
        return result;

    }

    /** Retreive list of agents for multi-agent mode. */
    public synchronized SubagentInfo[] getAgentList(CommCompleteInterface callingPage,
            MessageFacadeParam parameters) throws MessageFacadeError {

    	SubagentInfo[] result = null;
        note(Messages.getString("MessageFacade.1"), Messages.getString("MessageFacade.2"));

        try {
            SubagentsRequest subagentsRequest = new SubagentsRequest();
            populateRequestWithHeader(subagentsRequest);

            ensureConnection(parameters);
            SubagentsResponse response = getAgentConnectLocal().subagents(subagentsRequest,LIVE_TRAINING);
            List<SubagentInfo> aInfos = response.getPayload().getValue().getSubagents().getSubagent();
			result = aInfos.toArray(new SubagentInfo[0]);
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY) {
                return getAgentList(callingPage, parameters);
            }
            return result;
        } finally {
            dropConnection(parameters);
        }
        return result;

    }

    /** Retrieves report data. */
    public synchronized MoneyGramReceiveDetailInfo[] getMoneyGramReceiveDetailReport(MessageFacadeParam parameters, Date activityDate, int userID) throws MessageFacadeError {
		this.userID = userID;

		MoneyGramReceiveDetailInfo[] result = new MoneyGramReceiveDetailInfo[0];
        note(
            Messages.getString("MessageFacade.1830"),
            Messages.getString("MessageFacade.1831"));  

        try {
            MoneyGramReceiveDetailReportRequest moneyGramReceiveDetailReportRequest = new MoneyGramReceiveDetailReportRequest();
            populateRequestWithHeader(moneyGramReceiveDetailReportRequest);
            moneyGramReceiveDetailReportRequest.setActivityDate(TimeUtility.toXmlCalendarDateOnly(activityDate));
            ensureConnection(parameters);
            MoneyGramReceiveDetailReportResponse response = getAgentConnectLocal().moneyGramReceiveDetailReport(moneyGramReceiveDetailReportRequest,LIVE_TRAINING);

            MoneyGramReceiveDetailReportResponse.Payload payload = response.getPayload().getValue();
            if (payload.getReportInfo() != null && ! payload.getReportInfo().isEmpty()) {
    			result = payload.getReportInfo().toArray(new MoneyGramReceiveDetailInfo[0]);
    		}
            return result;

        } catch (Exception exception) {
            int status = handleError(parameters, exception);

            if (status == RETRY)
                return getMoneyGramReceiveDetailReport(parameters, activityDate, userID);
            else if (status == CANCEL)
                reportCanceled = true;
            return result;
        } finally {
            dropConnection(parameters);
        }
    }

    /** Retreives report data. */
    public synchronized MoneyGramSendDetailInfo[] getMoneyGramSendDetailReport(MessageFacadeParam parameters, Date activityDate, int userID) throws MessageFacadeError {
    	this.userID = userID;

    	MoneyGramSendDetailInfo[] result = new MoneyGramSendDetailInfo[0];
        note(
            Messages.getString("MessageFacade.1830"),
            Messages.getString("MessageFacade.1831"));

        try {
            MoneyGramSendDetailReportRequest moneyGramSendDetailReportRequest = new MoneyGramSendDetailReportRequest();
            populateRequestWithHeader(moneyGramSendDetailReportRequest);
            moneyGramSendDetailReportRequest.setActivityDate(TimeUtility.toXmlCalendarDateOnly(activityDate));

            ensureConnection(parameters);
            MoneyGramSendDetailReportResponse response = getAgentConnectLocal().moneyGramSendDetailReport(moneyGramSendDetailReportRequest,LIVE_TRAINING);

            MoneyGramSendDetailReportResponse.Payload payload = response.getPayload().getValue();
            if (payload.getReportInfo() != null && ! payload.getReportInfo().isEmpty()) {
    			result = payload.getReportInfo().toArray(new MoneyGramSendDetailInfo[0]);
    		}
            
            return result;
        
        } catch (Exception exception) {
            int status = handleError(parameters, exception);

            if (status == RETRY)
                return getMoneyGramSendDetailReport(parameters, activityDate, userID);
            else if (status == CANCEL)
                reportCanceled = true;

            return result;
        } finally {
            dropConnection(parameters);
        }
    }

    public MoneyGramSendWithTaxDetailInfo[] getMoneyGramSendDetailReportWithTax(MessageFacadeParam parameters, Date activityDate, int userID) throws MessageFacadeError {
    	this.userID = userID;

    	MoneyGramSendWithTaxDetailInfo[] result = new MoneyGramSendWithTaxDetailInfo[0];
        note(
            Messages.getString("MessageFacade.1830a"),
            Messages.getString("MessageFacade.1831a"));

        try {
			MoneyGramSendDetailReportWithTaxRequest moneyGramSendDetailReportWithTaxRequest = new MoneyGramSendDetailReportWithTaxRequest();
			populateRequestWithHeader(moneyGramSendDetailReportWithTaxRequest);
            moneyGramSendDetailReportWithTaxRequest.setActivityDate(TimeUtility.toXmlCalendarDateOnly(activityDate));

			ensureConnection(parameters);
			MoneyGramSendDetailReportWithTaxResponse response = getAgentConnectLocal()
					.moneyGramSendDetailReportWithTax(moneyGramSendDetailReportWithTaxRequest, LIVE_TRAINING);

            result = (MoneyGramSendWithTaxDetailInfo[]) response.getPayload().getValue().getReportInfo().toArray(result);

            return result;
        
        } catch (Exception exception) {
            int status = handleError(parameters, exception);

            if (status == RETRY)
                return getMoneyGramSendDetailReportWithTax(parameters, activityDate, userID);
            else if (status == CANCEL)
                reportCanceled = true;

            return result;
        } finally {
            dropConnection(parameters);
        }
    }

    /** Retrieves report data. */
    public synchronized BillPaymentDetailInfo[] getMoneyGramBillPayDetailReport(MessageFacadeParam parameters, Date activityDate) throws MessageFacadeError {

    	BillPaymentDetailInfo[] result = new BillPaymentDetailInfo[0];
        note(
            Messages.getString("MessageFacade.1830"),
            Messages.getString("MessageFacade.1831"));

        try {
            BillPaymentDetailReportRequest billPaymentDetailReportRequest = new BillPaymentDetailReportRequest();
            populateRequestWithHeader(billPaymentDetailReportRequest);
            billPaymentDetailReportRequest.setActivityDate(TimeUtility.toXmlCalendarDateOnly(activityDate));
            ensureConnection(parameters);
            BillPaymentDetailReportResponse response = getAgentConnectLocal().billPaymentDetailReport(billPaymentDetailReportRequest,LIVE_TRAINING);
           
            BillPaymentDetailReportResponse.Payload payload = response.getPayload().getValue();
            if (payload.getReportInfo() != null && ! payload.getReportInfo().isEmpty()) {
    			result = payload.getReportInfo().toArray(new BillPaymentDetailInfo[0]);
    		}

            return result;
        } catch (Exception exception) {
            int status = handleError(parameters, exception);

            if (status == RETRY)
                return getMoneyGramBillPayDetailReport(parameters, activityDate);
            else if (status == CANCEL)
                reportCanceled = true;

            return result;
        } finally {
            dropConnection(parameters);
        }
    }


    /** Sends debug log (trace file). */
    public synchronized boolean transmitDebugLog(MessageFacadeParam parameters,
            String debugData, long length, long endTimeStamp,
            long sequenceNumber, boolean endOfLog) throws MessageFacadeError {

        boolean returnVal = true;
        String debugData2 = "";
        note(
                Messages.getString("MessageFacade.1834"),
                Messages.getString("MessageFacade.1835"));  

        try {
            SaveDebugDataRequest saveDebugDataRequest = new SaveDebugDataRequest();
            populateRequestWithHeader(saveDebugDataRequest);
            
			/*
			 * Get rid of any unprintable characters from the debug log
			 * (probably as the result of some sort of exception that did not
			 * get caught and 'encrypted' by DW). Otherwise a SOAP fault will get
			 * thrown and the transmit will fail.
			 */
			debugData2 = debugData;
			for (int ii = 0x10; ii < 0x20; ii++) {
				debugData2 = debugData2.replace((char) ii, (char) (ii + 0x10));
			}
			debugData2 = "<![CDATA[" + debugData2 + "]]>";
			saveDebugDataRequest.setDebugData(debugData2);
			saveDebugDataRequest.setLength(BigInteger.valueOf(length));
			saveDebugDataRequest.setCollectionEndTimestamp(endTimeStamp);
			saveDebugDataRequest.setSequenceNumber(BigInteger.valueOf(sequenceNumber));
			saveDebugDataRequest.setEndOfLog(endOfLog);
			
            ensureConnection(parameters);
            getAgentConnectLocal().saveDebugData(saveDebugDataRequest,LIVE_TRAINING);
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY) {
                return transmitDebugLog(parameters, debugData2, length,
                        endTimeStamp, sequenceNumber, endOfLog);
            }
            returnVal = false;
        } finally {
            dropConnection(parameters);
        }
        return returnVal;
    }

    /** Transmits profile data. */
    public synchronized Object sendProfile(MessageFacadeParam parameters,
            String changedProfile) throws MessageFacadeError {

        note(
        		Messages.getString("MessageFacade.1836"),
        		Messages.getString("MessageFacade.1837"));  
        Object result = Boolean.TRUE;
        try {
        	ProfileChangeRequest profileChangeRequest = new ProfileChangeRequest();
        	populateRequestWithHeader(profileChangeRequest);
            profileChangeRequest.setProfile(changedProfile);
            ensureConnection(parameters);
            getAgentConnectLocal().profileChange(profileChangeRequest,LIVE_TRAINING);
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY) {
                return sendProfile(parameters, changedProfile);
            }
            result = Boolean.FALSE;
        } finally {
            dropConnection(parameters);
        }
        return result;
    }

    /** Forces updates of a SuperAgent Profile, regardless of whether it is a 
     * superAgent or a subagent that is active
     */
    public synchronized Boolean updateAgentProfile(String changedProfile) throws MessageFacadeError {

		MessageFacadeParam parameters = new MessageFacadeParam(MessageFacadeParam.REAL_TIME, MessageFacadeParam.AUTO_RETRY);

        note(
            Messages.getString("MessageFacade.1836"),
            Messages.getString("MessageFacade.1837"));  
        Boolean result = Boolean.TRUE;
        try {
        	ProfileChangeRequest profileChangeRequest = new ProfileChangeRequest();
        	populateRequestWithHeader(profileChangeRequest);
            profileChangeRequest.setProfile(changedProfile);
            
            ensureConnection(parameters);
            getAgentConnectLocal().profileChange(profileChangeRequest,LIVE_TRAINING);
         } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY) {
                return updateAgentProfile(changedProfile);
            }
            result = Boolean.FALSE;
        } finally {
            dropConnection(parameters);
        }
        return result;
    }

    /** Retrieves updated profile. */
    public synchronized String getProfile(MessageFacadeParam parameters)
            throws MessageFacadeError {

        String retValue = null;
        note(
        		Messages.getString("MessageFacade.1836"),
        		Messages.getString("MessageFacade.1837"));  
        try {
            DwProfileRequest dwProfileRequest = new DwProfileRequest();
            populateRequestWithHeader(dwProfileRequest);
            ensureConnection(parameters);
            DwProfileResponse response = getAgentConnectLocal().dwProfile(dwProfileRequest,LIVE_TRAINING);
            return response.getPayload().getValue().getProfile();
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY)
                return getProfile(parameters);

            return retValue;
        } finally {
            dropConnection(parameters);
        }
    }

    /**
     * Retrieve the subagent profile. Send Superagent UnitID and the subagent ID
     * in header to indicate to RTS that request is in multiagent mode.
     */
    public synchronized String getSubagentProfile(MessageFacadeParam parameters,
            int unitProfileID, String agentID, String sequenceNumber)
            throws MessageFacadeError {

        String retValue = null;
		String seqNo = sequenceNumber != null && !sequenceNumber.equals("null") ? sequenceNumber : "1";
        note(
        		Messages.getString("MessageFacade.1836"),
        		Messages.getString("MessageFacade.1837"));
        try {
            //set the unit profile id and agent ID in header of Request.
        	
        	DwProfileRequest dwProfileRequest = new DwProfileRequest();
        	populateRequestWithHeader(dwProfileRequest);
            ensureConnection(parameters);
            dwProfileRequest.setUnitProfileID(unitProfileID);
            dwProfileRequest.setAgentID(agentID);
            dwProfileRequest.setAgentSequence(seqNo);
            DwProfileResponse response = getAgentConnectLocal().dwProfile(dwProfileRequest,LIVE_TRAINING);
            return response.getPayload().getValue().getProfile();
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY)
				return getSubagentProfile(parameters, unitProfileID, agentID, seqNo);

            return retValue;
        } finally {
            dropConnection(parameters);
        }
    }
    
    /**
     * Update subagent Profile items in multi agent mode.
     * @param parameters
     * @param unitProfileID
     * @param subAgentItems
     * @return
     * @throws MessageFacadeError
     */
    public synchronized Boolean updateSubagentProfile(MessageFacadeParam parameters,
            int unitProfileID, SubagentProfileUpdateType[] subAgentItems)
            throws MessageFacadeError {

    	note(
    			Messages.getString("MessageFacade.1836"),
    			Messages.getString("MessageFacade.1837"));
        try {
            //set the unit profile id and the subagnetProfileitems in header of Request.
            SaveSubagentsRequest saveSubagentsRequest = new SaveSubagentsRequest();
            populateRequestWithHeader(saveSubagentsRequest);
            saveSubagentsRequest.setUnitProfileID(unitProfileID);
            if (saveSubagentsRequest.getSubagentProfileUpdates() == null) {
            	saveSubagentsRequest.setSubagentProfileUpdates(new SubagentProfileUpdates());
            }
            for(int i=0;i < subAgentItems.length; i++) {
            	SubagentProfileUpdateType subagentProfileUpdate = new SubagentProfileUpdateType();
                subagentProfileUpdate.setAgentID(subAgentItems[i].getAgentID().trim());
                subagentProfileUpdate.setSequenceNumber(subAgentItems[i].getSequenceNumber());
                subagentProfileUpdate.setProfileItemValue(subAgentItems[i].getProfileItemValue().trim());
                subagentProfileUpdate.setProfileItemName(subAgentItems[i].getProfileItemName().trim());
                saveSubagentsRequest.getSubagentProfileUpdates().getSubagentProfileUpdate().add(subagentProfileUpdate);
            }
            
            ensureConnection(parameters);
            getAgentConnectLocal().saveSubagents(saveSubagentsRequest,LIVE_TRAINING);
            return Boolean.TRUE;
        }catch (Exception exception) {
        	if (handleError(parameters, exception) == RETRY) {
        		return updateSubagentProfile(parameters, unitProfileID,subAgentItems);
        	}
        	return Boolean.FALSE;
        }finally {
            dropConnection(parameters);
        }
    }

    /** Attempts initial setup and retrieves initial profile. */
    public synchronized GetInitialSetupMsg getInitialProfile(MessageFacadeParam parameters,
            String unitNumber, String password, int versionNumber)
            throws MessageFacadeError {

        GetInitialSetupMsg retValue = null;
        note(
            Messages.getString("MessageFacade.1838"),
            Messages.getString("MessageFacade.1839"));  
        try {
            DwInitialSetupRequest dwInitialSetupRequest = new DwInitialSetupRequest();
            populateRequestWithHeader(dwInitialSetupRequest);
            dwInitialSetupRequest.setPassword(password);
            dwInitialSetupRequest.setDeviceID(unitNumber);
            dwInitialSetupRequest.setClientSoftwareVersion(String.valueOf(versionNumber));
            dwInitialSetupRequest.setPoeCapabilities(POE_CAPABILITIES);
            ensureConnection(parameters);
            DwInitialSetupResponse response = getAgentConnectLocal().dwInitialSetup(dwInitialSetupRequest, ALWAYS_LIVE);

            retValue = new GetInitialSetupMsg(response);
            return retValue;
        } catch (Exception exception) {
            Debug.println("there was an exception in getInitialProfile "
                    + exception);
            if (handleError(parameters, exception) == RETRY)
                return getInitialProfile(parameters, unitNumber, password,
                        versionNumber);
            return retValue;
        } finally {
            dropConnection(parameters);
        }
    }

    /**
     * Sends the DW40 compliance request. This is for real-time queries (such as
     * agg/ofac check), not transmission of items in the CCI log. In demo and
     * training mode, canned demo data will be returned.
     */
    public synchronized ComplianceTransactionResponse checkOFAC(
            MessageFacadeParam parameters,
            ComplianceTransactionRequest request) throws MessageFacadeError {

        note(Messages.getString("MessageFacade.1846"),
        		Messages.getString("MessageFacade.1847"));

        return getComplianceResponse(parameters, request, AUTOMATIC);
    }

    /**
     * Sends the DW40 compliance request. This is for transmission of real, live
     * money orders in the CCI log, not real-time agg/ofac queries. The request
     * message is sent to the middleware even in training mode.
     */
    public synchronized ComplianceTransactionResponse transmitItems(
            MessageFacadeParam parameters,
            ComplianceTransactionRequest request) throws MessageFacadeError {

        note(Messages.getString("MessageFacade.1834"),
        		Messages.getString("MessageFacade.1835"));

        return getComplianceResponse(parameters, request, LIVE_TRAINING);
    }

    private ComplianceTransactionResponse getComplianceResponse(MessageFacadeParam parameters, ComplianceTransactionRequest request, int rule) throws MessageFacadeError {
    	ComplianceTransactionResponse response = null;

        try {

        	// Remove any money order entries that does not have a status of CONFIRMED_PRINTED or ATTEMPTED.
            
            List<MoneyOrderInfo> list = new ArrayList<MoneyOrderInfo>();
            for (MoneyOrderInfo moi : request.getMoneyOrder()) {
            	if (moi.getPrintStatus().equals(MoneyOrderPrintStatusType.CONFIRMED_PRINTED) || moi.getPrintStatus().equals(MoneyOrderPrintStatusType.ATTEMPTED)) {
            		list.add(moi);
            	}
            }
            request.getMoneyOrder().clear();
            request.getMoneyOrder().addAll(list);

            populateRequestWithHeader(request);
            ensureConnection(parameters);
            response = getAgentConnectLocal().complianceTransaction(request, rule);
            
            // Check for result indicating OFAC hit. Transaction logic (per
            // design)
            // expects this to be handled like an error.
            
            ComplianceTransactionResponse.Payload payload = response.getPayload().getValue();
            if (! payload.isOfacStatus()) {
                String sourceID = payload.getOfacSourceID();
                throw new MessageFacadeError(
                        MessageFacadeParam.MO_OFAC_CHECK_FAILED, sourceID);
            }
            return response;
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY)
                return getComplianceResponse(parameters, request, rule);
            return response;
        } finally {
            dropConnection(parameters);
        }
    }

    public synchronized MoneyOrderTotalResponse transmitMoneyOrderTotals(
            MessageFacadeParam parameters,
            String batchID, BigInteger batchCount, BigDecimal batchAmount,
			String moAccountNumber) throws MessageFacadeError {

        note(Messages.getString("MessageFacade.1866"),
        		Messages.getString("MessageFacade.1867"));
        
        MoneyOrderTotalResponse response = null;
        
        try {
            MoneyOrderTotalRequest moneyOrderTotalRequest = new MoneyOrderTotalRequest();
            populateRequestWithHeader(moneyOrderTotalRequest);
            moneyOrderTotalRequest.setBatchID(batchID);
            moneyOrderTotalRequest.setBatchCount(batchCount);
            moneyOrderTotalRequest.setBatchTotal(batchAmount);
            moneyOrderTotalRequest.setMoAccountNumber(moAccountNumber);
            
			ensureConnection(parameters);
	        response = getAgentConnectLocal().moneyOrderTotal(moneyOrderTotalRequest,AUTOMATIC);
	        return response;
		} catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY)
                return transmitMoneyOrderTotals(parameters,
                        batchID, batchCount, batchAmount, moAccountNumber);
            return response;
		} finally {
            dropConnection(parameters);
        }		
    }
   
    
    /**
     * Get the current manifest for this POS system.
     */
    public synchronized VersionManifest getCurrentManifest(StringBuffer resultBuffer, int reterivalType) throws MessageFacadeError {
    	
		MessageFacadeParam parameters = getTransmitParams(reterivalType);
    		
        VersionManifest manifest = null;
        VersionManifestResponse.Payload payload = null;
        note(Messages.getString("MessageFacade.1916"), Messages.getString("MessageFacade.1917"));

        try {
            ensureConnection(parameters);

            VersionManifestRequest versionManifestRequest = new VersionManifestRequest();
            populateRequestWithHeader(versionManifestRequest);

            VersionManifestResponse response = getAgentConnectLocal().versionManifest(versionManifestRequest, LIVE_TRAINING);
            payload = ((response != null) && (response.getPayload() != null)) ? response.getPayload().getValue() : null;
            
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY) {
                return getCurrentManifest(resultBuffer, reterivalType);
            }
            return null;
        } finally {
            dropConnection(parameters);
        }
        
        if (payload != null) {
            String location;
            String baseUrl = "";
	        if (UnitProfile.getInstance().isDialUpMode()) {
	            location = UnitProfile.getInstance().get("SSL_DIALUP_URL", "");
	        } else {
	            location = UnitProfile.getInstance().get("SSL_NETWORK_URL", "");
	        }
	        int endIndex = location.indexOf("servlet");
	
	        if (endIndex > -1) {
	            baseUrl = location.substring(0, endIndex);
	        }
	
	        try {
	        	manifest = new VersionManifest(baseUrl, payload.getManifestURL(), payload.getManifestSize(), resultBuffer);
	        } catch (IOException e) {
	        	Debug.printException(e);
	        }
        } 

        return manifest;
    }
    
    public synchronized DwPasswordResponse validatePin(CommCompleteInterface cci,MessageFacadeParam parameters,
    		DwPasswordRequest validatePinRequest)  {

    	DwPasswordResponse result = null;

        note(
            Messages.getString("MessageFacade.1870"),
            Messages.getString("MessageFacade.1871"));
        try {
            ensureConnection(parameters);
            populateRequestWithHeader(validatePinRequest);
            result = getAgentConnectLocal().dwPassword(validatePinRequest, AUTOMATIC);
          
        } catch (Exception exception) {
            Debug.println("exception thrown "+exception); 
            if (handleError(parameters, exception) == RETRY) {
              
                return validatePin(cci,parameters, validatePinRequest);
            }
          
        } finally {
            dropConnection(parameters);           
        }
        return result;
       
    }
   
    /**
     * Hang up the dialup connection when the application is exited.
     */

    public void exitDeltaWorks() {
        if (dialUp != null){
            dialUp.hangUpEntry(entry);
            dialUp.deleteEntry(entry);
        }
    }

    /**
     * Manual Dial
     */
    public synchronized void manualDial(MessageFacadeParam parameters) {

        note(
            Messages.getString("MessageFacade.1860"),
            Messages.getString("MessageFacade.1861"));  
        try {
            ensureConnection(parameters);
            dropConnection(parameters);
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY)
                manualDial(parameters);
        }
    }

    public class DialNotify implements DialingNotification {
        // callback method

        @Override
		public void notifyDialingState(String name, int state, int error) {
            // output the meaning of the new dialing state
//            Debug.println("notify - " + name + ": (" + state + ") "
//                    + DialingState.getDialingStateString(state) +" ,error = " + error);
            if (state == DialingState.OpenPort) {
                multiListener.openPort(error);
            }
            if (state == DialingState.Connected) {
                multiListener.connected(error);
            }
            if (state == DialingState.Authenticate) {
                multiListener.authenticate(error);
            }
            if (state == DialingState.Authenticated) {
                multiListener.authenticated(error);
            }
            // if an error, output the meaning of the error code
            if (error != 0) {
                Debug.println("ERROR: " + error + " " + dialUp.getErrorMessageForCode(error));
                multiListener.interactive(error);
            }
        }
    }

	/**
	 * This method takes in the script template file and creates
	 * temg.scp file with the given username, password and delay values embedded
	 * in it at appropriate places.
	 *
	 * @param filename script filename
	 *
	 * 	 * @author Karthik
	 */
    private void createRealScript(String filename, boolean primaryDialup) {

        BufferedWriter bwrite = null;
        BufferedReader bread = null;
        String temp = null;
        try {
	        // Try to get script file as a system resource
	        URL scrpTemplateUrl = ClassLoader.getSystemResource("xml/dunscripts/"
	                + filename.toLowerCase(Locale.US) + ".scp");
	        File scrpfile = new File("mgram.scp");
            bread = new BufferedReader(new InputStreamReader(scrpTemplateUrl.openStream()));
            bwrite = new BufferedWriter(new FileWriter(scrpfile));
            temp = bread.readLine();
            while (temp != null) {
            	if (primaryDialup){
	                temp = temp.replaceAll("%SCRIPT_USER_NAME%", UnitProfile.getInstance().get(
	                    "PRIMARY_SCRIPT_USER_NAME", ""));
	                temp = temp.replaceAll("%SCRIPT_PASSWORD%", UnitProfile.getInstance().get(
	                    "PRIMARY_SCRIPT_PASSWORD", ""));
	                temp = temp.replaceAll("%DELAY_PHONE_DIAL%", UnitProfile.getInstance().get(
	                    "PRIMARY_SCRIPT_DELAY_PHONE_DIAL", ""));
	                temp = temp.replaceAll("%DELAY_PT_USER_NAME%", UnitProfile.getInstance().get(
	                    "PRIMARY_SCRIPT_DELAY_PT_USERNAME", ""));
	                temp = temp.replaceAll("%DELAY_PT_PASSWORD%", UnitProfile.getInstance().get(
	                    "PRIMARY_SCRIPT_DELAY_PT_PASSWORD", ""));
	                temp = temp.replaceAll("%DELAY_A%", UnitProfile.getInstance().get(
	                    "PRIMARY_SCRIPT_DELAY_A", ""));
	                temp = temp.replaceAll("%DELAY_B%", UnitProfile.getInstance().get(
	                    "PRIMARY_SCRIPT_DELAY_B", ""));
            	} else {
	                temp = temp.replaceAll("%SCRIPT_USER_NAME%", UnitProfile.getInstance().get(
		                    "SECONDARY_SCRIPT_USER_NAME", ""));
		                temp = temp.replaceAll("%SCRIPT_PASSWORD%", UnitProfile.getInstance().get(
		                    "SECONDARY_SCRIPT_PASSWORD", ""));
		                temp = temp.replaceAll("%DELAY_PHONE_DIAL%", UnitProfile.getInstance().get(
		                    "SECONDARY_SCRIPT_DELAY_PHONE_DIAL", ""));
		                temp = temp.replaceAll("%DELAY_PT_USER_NAME%", UnitProfile.getInstance().get(
		                    "SECONDARY_SCRIPT_DELAY_PT_USERNAME", ""));
		                temp = temp.replaceAll("%DELAY_PT_PASSWORD%", UnitProfile.getInstance().get(
		                    "SECONDARY_SCRIPT_DELAY_PT_PASSWORD", ""));
		                temp = temp.replaceAll("%DELAY_A%", UnitProfile.getInstance().get(
		                    "SECONDARY_SCRIPT_DELAY_A", ""));
		                temp = temp.replaceAll("%DELAY_B%", UnitProfile.getInstance().get(
		                    "SECONDARY_SCRIPT_DELAY_B", ""));            		
            	}
                bwrite.write(temp);
                bwrite.newLine();
                temp = bread.readLine();
            }
            bread.close();
            bwrite.close();

            scrpfile.getCanonicalPath();
        }
        catch (Exception ex) {
            Debug.println("[Script File Creation] : " + ex.getMessage());
        }
    }
    /**
     * This method deletes the temg.scp file when the application is closed
     * or when the connection is canceled
     *
     * @author Karthik
     */
    private void deleteScriptFile(){
    	File scriptFile = null;
    	try{
    		scriptFile = new File("mgram.scp");
    		scriptFile.delete();

    	}catch(Exception ex){
    		Debug.println("[Script File Deletion] :"+ex.getMessage());
    	}
    }

    /**
     * Java VM shutdown thread Runnable implementation. Used with the
     * <code>java.lang.Runtime.addShutdownHook(Thread hook)</code> method.
     */
    class ShutdownHookRunner implements Runnable {

        @Override
		public void run() {
            Debug.println("Shutting down."); 
            //deletes Script file temg.scp if exists
            deleteScriptFile();
            if (dialUp != null) {
                try {
                    Debug.println("Shutting down RAS connection:"); 
                    // Clean up the cached rasConnection handle.
                    dialUp.hangUpEntry(entry);
                    dialUp.deleteEntry(entry);
                    dialUp = null;
                    Thread.sleep(3000);
                } catch (Exception e) {
                    Debug.printStackTrace(e);
                }
            }
        }
    }


	/**
	 * @return
	 */
	public int getSelectedCommPort() {
        int portIndex = -1;
        String portSelection = UnitProfile.getInstance().get("COM_PORT", "COM1");  

        String[] portList = getCommPortList();
        
        for (int i=0; i<portList.length; i++){
            if(portList[i].equalsIgnoreCase(portSelection)){
                portIndex = i;
                break;
            }
        }
        if(portIndex < 0){
            portIndex = 0;
        }
            
        return portIndex;
	}
	
	private int getUserID(){
	    return this.userID;
	}
	
//    /**
//     * Get the State Regulator Information from the middleware.
//     */
//	public synchronized DoddFrankStateRegulatorInfoResponse getStateRegInfo(MessageFacadeParam parameters) {
//		note(Messages.getString("MessageFacade.1882"),
//				Messages.getString("MessageFacade.1883"));
//		DoddFrankStateRegulatorInfoResponse result = null;
//		try {
//			DoddFrankStateRegulatorInfoRequest doddFrankStateRegulatorInfoRequest = new DoddFrankStateRegulatorInfoRequest();
//			populateRequestWithHeader(doddFrankStateRegulatorInfoRequest);
//			 
//			ensureConnection(parameters);
//			String sCountryCode = new ClientInfo().getAgentCountry().getCountryCode();
//			/*
//			 * US territories may show up as their own countries, so if the
//			 * country is US, use the state as a jurisdiction, if not use the
//			 * country as the jurisdiction.
//			 */
//			if (sCountryCode.compareToIgnoreCase("USA") == 0) {
//				if(CountryInfo.getAgentCountrySubdivision() !=null)
//				{
//					String stateAbbv = CountryInfo.getAgentCountrySubdivision().getSubdivisionCode();
//					doddFrankStateRegulatorInfoRequest.setDfJurisdiction(stateAbbv);
//				}
//			} else {
//				
////              /*
////               * Convert 3 Character language code to a 2 character code, since
////               * DCA has territories associated with state tags.
////               */
//				doddFrankStateRegulatorInfoRequest.setDfJurisdiction(sCountryCode);
//			}
//			List<String> receiptLanguageList = MoneyGramSendTransaction.getSendReceiptAllLanguageList();
//			Iterator<String> i = receiptLanguageList.iterator();
//			DoddFrankStateRegulatorInfoRequest.Languages lng= new DoddFrankStateRegulatorInfoRequest.Languages();
//			while (i.hasNext()) {
//				String sLan = i.next();
//				lng.getLongLanguageCode().add(sLan);
//			}
//			doddFrankStateRegulatorInfoRequest.setLanguages(lng);
//			result = getAgentConnectLocal().doddFrankStateRegulatorInfo(doddFrankStateRegulatorInfoRequest,LIVE_TRAINING);
//		} catch (Exception exception) {
//			if (handleError(parameters, exception) == RETRY) {
//				return getStateRegInfo(parameters);
//			}
//			return result;
//		} finally {
//			dropConnection(parameters);
//		}
//		return result;
//	}

    /**
     * Get the Receipt Templates from the middleware.
     */
	public synchronized ReceiptsFormatDetailsResponse getReceiptFormats(MessageFacadeParam parameters) {
		note(Messages.getString("MessageFacade.1884"),
				Messages.getString("MessageFacade.1885"));
		ReceiptsFormatDetailsResponse result = null;
		try {
			ReceiptsFormatDetailsRequest receiptsFormatDetailsRequest = new ReceiptsFormatDetailsRequest();
			populateRequestWithHeader(receiptsFormatDetailsRequest);
			
			ensureConnection(parameters);
			
			List<ReceiptTemplateInfo> rtiList = ReceiptTemplates.getTemplateInventory();
			
			MoneyGramReceiveTransaction.populateRecvReceiptLanguageList();
			List<String> lLanguages = MoneyGramReceiveTransaction.getRecvReceiptAllLanguageList();

			ReceiptsFormatDetailsRequest.Languages langs = new ReceiptsFormatDetailsRequest.Languages();
			
			/*
			 * Add required languages
			 */
			if(lLanguages!=null && lLanguages.size() > 0) {				
				Iterator<String> itr = lLanguages.iterator();
				while (itr.hasNext()) {
					langs.getLongLanguageCode().add( itr.next());
				}			
				receiptsFormatDetailsRequest.setLanguages(langs);
			}
			ReceiptsFormatDetailsRequest.ReceiptTypeValues receiptTypeValues = new ReceiptsFormatDetailsRequest.ReceiptTypeValues();
			Iterator<ReceiptTemplateInfo> itr2 = rtiList.iterator();
			while (itr2.hasNext()) {
				ReceiptTemplateInfo eTemplateInfo = itr2.next();
					ReceiptType receiptType = new ReceiptType();
					receiptType.setMd5CheckSum(eTemplateInfo.getmD5());
					receiptType.setVersion(eTemplateInfo.getVersion());
					receiptType.setLongLanguageCode(eTemplateInfo.getLongLanguageCode());
					receiptType.setReceiptText(eTemplateInfo.getType());
					if (eTemplateInfo.getAdditionalLanguages() != null
							&& eTemplateInfo.getAdditionalLanguages().size() > 0) {
						ReceiptType.AdditionalLanguages addLangs = new ReceiptType.AdditionalLanguages();
						for (String lang : eTemplateInfo.getAdditionalLanguages()) {
							addLangs.getLongLanguageCode().add(lang);
						}
						receiptType.setAdditionalLanguages(addLangs);
					}
					
					receiptTypeValues.getReceiptType().add(receiptType);
					
			}
			receiptsFormatDetailsRequest.setReceiptTypeValues(receiptTypeValues);
			result = getAgentConnectLocal().receiptsFormatDetails(receiptsFormatDetailsRequest,LIVE_TRAINING);
		} catch (Exception exception) {
			if (handleError(parameters, exception) == RETRY) {
				return getReceiptFormats(parameters);
			}
			return result;
		} finally {
			dropConnection(parameters);
		}
		return result;
	}

	public synchronized GetBankDetailsByLevelResponse getBankDetailsByLevel(String receiveCountry, int hierarchyLevelNumber, int previousLevelElementNumber) throws MessageFacadeError {

		MessageFacadeParam parameters = new MessageFacadeParam(MessageFacadeParam.REAL_TIME);
		GetBankDetailsByLevelResponse response = null;

		note(Messages.getString("MessageFacade.3"), Messages.getString("MessageFacade.4"));

		try {
			GetBankDetailsByLevelRequest getBankDetailsByLevelRequest = new GetBankDetailsByLevelRequest();
			populateRequestWithHeader(getBankDetailsByLevelRequest);
			
			ensureConnection(parameters);
			getBankDetailsByLevelRequest.setCountryCode(receiveCountry);
			getBankDetailsByLevelRequest.setHierarchyLevelNumber(hierarchyLevelNumber);
			if (previousLevelElementNumber > 0) {
				getBankDetailsByLevelRequest.setPreviousLevelElementNumber((long) previousLevelElementNumber);
			} else {
				getBankDetailsByLevelRequest.setPreviousLevelElementNumber((long) 0);
			}
			response = getAgentConnectLocal().getBankDetailsByLevel(getBankDetailsByLevelRequest, LIVE_TRAINING);

		} catch (Exception exception) {
			if (handleError(parameters, exception) == RETRY) {
				return getBankDetailsByLevel(receiveCountry, hierarchyLevelNumber, previousLevelElementNumber);
			}
		} finally {
			dropConnection(parameters);
		}
		return response;
	}
	

	public synchronized GetBankDetailsResponse getBankDetails(String receiveCountry, String infoKey, String value) {

		MessageFacadeParam parameters = new MessageFacadeParam(MessageFacadeParam.REAL_TIME, 3022);
		GetBankDetailsResponse response = null;

		note(Messages.getString("MessageFacade.3"), Messages.getString("MessageFacade.4"));

		try {
			GetBankDetailsRequest getBankDetailsRequest = new GetBankDetailsRequest();
			populateRequestWithHeader(getBankDetailsRequest);
			
			ensureConnection(parameters);
			getBankDetailsRequest.setCountryCode(receiveCountry);
			getBankDetailsRequest.setInfoKey(infoKey);
			getBankDetailsRequest.setValue(value);
			response = getAgentConnectLocal().getBankDetails(getBankDetailsRequest, LIVE_TRAINING);

		} catch (Exception exception) {
			if (handleError(parameters, exception) == RETRY) {
				return getBankDetails(receiveCountry, infoKey, value);
			}
		} finally {
			dropConnection(parameters);
		}
		return response;
	}

	public synchronized RegisterHardTokenResponse registerHardToken(RegisterHardTokenRequest query) throws Exception {
    	
		MessageFacadeParam parameters = new MessageFacadeParam(MessageFacadeParam.REAL_TIME, MessageFacadeParam.AUTO_RETRY);
		RegisterHardTokenResponse response = null;

        note(
        		Messages.getString("MessageFacade.1890"), 
        		Messages.getString("MessageFacade.1891"));
        try {
            RegisterHardTokenRequest registerHardTokenRequest = new RegisterHardTokenRequest();
            populateRequestWithHeader(registerHardTokenRequest);
            
            registerHardTokenRequest.setHardToken(query.getHardToken());
            registerHardTokenRequest.setLdapUserId(UnitProfile.getInstance().getUnitProfileID()+"");
            ensureConnection(parameters);
            response = getAgentConnectLocal().registerHardToken(registerHardTokenRequest,AUTOMATIC);
        } catch (MessageFacadeError e) {
        	//
			// If the ErrorCode = 49 or 90 (communication errors) let the user
        	// retry or cancel
        	//
			// Otherwise, throw the exception to the calling method and let it
			// handle it.
			Debug.println("Exception: " + e.getClass() + " - "
					+ e.getMessage());
			if (e.hasErrorCode(MessageFacadeParam.OTHER_ERROR)) {
				int errResult = handleError(parameters, e);
				if (errResult == RETRY) {
					return registerHardToken(query);
				} else {
					if (errResult == CANCEL) {
						throw new MessageFacadeError(
								MessageFacadeParam.USER_CANCEL, "Canceled");
					} else {
						throw e;
					}
				}
			} else {
				Debug.println("Exception: " + e.getClass() + " - "
						+ e.getMessage());
				throw e;
			}
        } finally {
            dropConnection(parameters);
        }
        return response;
    }
    
    public synchronized OpenOTPLoginResponse openOTPLogin(OpenOTPLoginRequest query) throws Exception {

		MessageFacadeParam parameters = new MessageFacadeParam(MessageFacadeParam.REAL_TIME, MessageFacadeParam.AUTO_RETRY);
		OpenOTPLoginResponse response = null;

        note(Messages.getString("MessageFacade.1888"), 
        		Messages.getString("MessageFacade.1889"));
        try {
        	OpenOTPLoginRequest openOTPLoginRequest = new OpenOTPLoginRequest();
        	populateRequestWithHeader(openOTPLoginRequest);
            openOTPLoginRequest.setPassword(query.getPassword());
            openOTPLoginRequest.setLdapUserId(UnitProfile.getInstance().getUnitProfileID()+"");
            ensureConnection(parameters);
            response = getAgentConnectLocal().openOTPLogin(openOTPLoginRequest,AUTOMATIC);
        } catch (MessageFacadeError e) {
        	//
			// If the ErrorCode = 49 or 90 (communication errors) let the user
        	// retry or cancel
        	//
			// Otherwise, throw the exception to the calling method and let it
			// handle it.
			Debug.println("Exception: " + e.getClass() + " - "
					+ e.getMessage());
			if (e.hasErrorCode(MessageFacadeParam.OTHER_ERROR)) {
				int errResult = handleError(parameters, e);
				if (errResult == RETRY) {
					return openOTPLogin(query);
				} else {
					if (errResult == CANCEL) {
						throw new MessageFacadeError(
								MessageFacadeParam.USER_CANCEL, "Canceled");
					} else {
						Debug.println("Exception: " + e.getClass() + " - "
								+ e.getMessage());
						throw e;
					}
				}
			} else {
				throw e;
			}
        } finally {
            dropConnection(parameters);
        }

        return response;
    }
    
    public synchronized GetBroadcastMessagesResponse getBroadcastMessages(GetBroadcastMessagesRequest query, int reterivalType) throws Exception {

		MessageFacadeParam parameters = new MessageFacadeParam(MessageFacadeParam.HIDE_WITHOUT_FOCUS_UNTIL_ERROR);
		GetBroadcastMessagesResponse response = null;

        note(Messages.getString("MessageFacade.1892"), 
        		Messages.getString("MessageFacade.1893"));
        try {
        	GetBroadcastMessagesRequest getBroadcastMessagesRequest = new GetBroadcastMessagesRequest();
        	populateRequestWithHeader(getBroadcastMessagesRequest);
            
            String agentId = UnitProfile.getInstance().get("AGENT_ID", "");
            String profileSequence = UnitProfile.getInstance().get("AGENT_POS_SEQ", "");
            
            getBroadcastMessagesRequest.setAgentID(agentId);
            getBroadcastMessagesRequest.setAgentSequence(profileSequence);
            getBroadcastMessagesRequest.setMsgLanguageCode(query.getMsgLanguageCode());

            String offsetGMT = "+00:00";
            try {
	            Calendar c = new GregorianCalendar();
	            int os = ((c.get(Calendar.ZONE_OFFSET) + c.get(Calendar.DST_OFFSET)) / 60000);
	            int h = os / 60;
	            int m = os % 60;
	            StringBuffer sb = new StringBuffer();
	            sb.append((h >= 0) ? '+' : '-');
	            sb.append(NUMBER_FORMATTER.format(Math.abs(h)));
	            sb.append(':');
	            sb.append(NUMBER_FORMATTER.format(Math.abs(m)));
	            offsetGMT = sb.toString();
            } catch (Exception e) {
            }
            
            getBroadcastMessagesRequest.setOffsetGMT(offsetGMT);
            
            GetBroadcastMessagesRequest.MessageInfos messageInfos = query.getMessageInfos();
            if (messageInfos != null) {
    			for (int i = 0; i < messageInfos.getMessageInfo().size(); i++) {
    				MessageInfo bm = query.getMessageInfos().getMessageInfo().get(i);
    				MessageInfo messageInfo = new MessageInfo(); 
    				messageInfo.setMessageId(bm.getMessageId());
    				messageInfo.setMessageVersion(bm.getMessageVersion());
					if (getBroadcastMessagesRequest.getMessageInfos() == null) {
						getBroadcastMessagesRequest.setMessageInfos(new GetBroadcastMessagesRequest.MessageInfos());
					}
    				getBroadcastMessagesRequest.getMessageInfos().getMessageInfo().add(messageInfo);
    			}
            }
			
            ensureConnection(parameters);
            response = getAgentConnectLocal().getBroadcastMessages(getBroadcastMessagesRequest, ALWAYS_LIVE);
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY) {
                return getBroadcastMessages(query, reterivalType);
            }
            return response;
        } finally {
            dropConnection(parameters);
        }

        return response;
    }

	public synchronized SearchStagedTransactionsResponse searchStagedTransactions(MessageFacadeParam parameters, SessionType transactionType) throws Exception {
		SearchStagedTransactionsResponse response = null;

        note(Messages.getString("MessageFacade.1894"), Messages.getString("MessageFacade.1895"));
        try {
        	SearchStagedTransactionsRequest searchStagedTransactionsRequest = new SearchStagedTransactionsRequest();
            populateRequestWithHeader(searchStagedTransactionsRequest);
            
            searchStagedTransactionsRequest.setMgiSessionType(transactionType);
            searchStagedTransactionsRequest.setThisLocationOnly(true);
            searchStagedTransactionsRequest.setReturnFormFreeOnly(true);
            searchStagedTransactionsRequest.setMaxRowsToReturn(BigInteger.valueOf(99L));
            ensureConnection(parameters);
            response = getAgentConnectLocal().searchStagedTransactions(searchStagedTransactionsRequest, AUTOMATIC);
            
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY) {
                return searchStagedTransactions(parameters, transactionType);
            }
            return response;
        } finally {
            dropConnection(parameters);
        }

        return response;
	}
    
	private AgentConnect getAgentConnectLocal() {
		if (agentConnectLocal == null) {
			agentConnectLocal =  new AgentConnect();
		}
		return agentConnectLocal;
	}

	public MessageFacadeParam getTransmitParams(int retrievalType) {
		MessageFacadeParam parameters;
		if (retrievalType == CountryInfo.AUTOMATIC_TRANSMIT_RETERIVAL) {
			parameters = new MessageFacadeParam(MessageFacadeParam.NON_REAL_TIME, MessageFacadeParam.AUTO_RETRY);
		} else if (retrievalType == CountryInfo.MANUAL_TRANSMIT_RETERIVAL) {
			parameters = new MessageFacadeParam(MessageFacadeParam.REAL_TIME, MessageFacadeParam.DO_NOT_HIDE);
		} else {
			parameters = new MessageFacadeParam(MessageFacadeParam.REAL_TIME, MessageFacadeParam.NO_DIALOG);
		}
		return parameters;
	}
    
	public synchronized void getCountryInfo(int retrievalType, String countryVersion) throws Exception {
		MessageFacadeParam parameters = getTransmitParams(retrievalType);
		
		note(Messages.getString("MessageFacade.1900"), Messages.getString("MessageFacade.1901"));
		
		try {
			GetCountryInfoRequest countryInfoRequest = new GetCountryInfoRequest();
			countryInfoRequest.setCachedVersion(countryVersion);
			populateRequestWithHeader(countryInfoRequest);
			ensureConnection(parameters);

			GetCountryInfoResponse response = getAgentConnectLocal().getCountryInfo(countryInfoRequest, ALWAYS_LIVE);
			GetCountryInfoResponse.Payload payload = ((response != null) && (response.getPayload() != null)) ? response.getPayload().getValue() : null;
			if (payload == null) {
				return;
			}
			
			List<com.moneygram.agentconnect.CountryInfo> countryInfoTags = response.getPayload().getValue().getCountryInfos() != null ? response.getPayload().getValue().getCountryInfos().getCountryInfo() : null;
			CountryInfo.parseCountryInfo(countryInfoTags, response.getPayload().getValue().getVersion());
		} catch (MessageFacadeError e) {
			try {
				if (handleError(parameters, e) == RETRY) {
					getCountryInfo(retrievalType, countryVersion);
				}
			} catch (Exception ee) {
				Debug.printStackTrace(ee);
			}
			throw e;
		} finally {
			dropConnection(parameters);
		}
	}

	public synchronized void countrySubdivision(int retrievalType, String countrySubdivisionVersion) throws Exception {
		MessageFacadeParam parameters = getTransmitParams(retrievalType);
		
        note(Messages.getString("MessageFacade.1902"), Messages.getString("MessageFacade.1903"));
        try {

        	GetCountrySubdivisionRequest countrySubdivisionRequest = new GetCountrySubdivisionRequest();
        	countrySubdivisionRequest.setCachedVersion(countrySubdivisionVersion);
        	populateRequestWithHeader(countrySubdivisionRequest);
        	 
        	ensureConnection(parameters);
        	 
			GetCountrySubdivisionResponse response = getAgentConnectLocal().countrySubdivision(countrySubdivisionRequest, ALWAYS_LIVE);
			GetCountrySubdivisionResponse.Payload payload = ((response != null) && (response.getPayload() != null)) ? response.getPayload().getValue() : null;
			
			if (payload == null) {
				return;
			}
			
            if (response != null) {
            	List<CountrySubdivisionInfo> countrySubdivisionInfoTags = response.getPayload().getValue().getCountrySubdivisionInfos() != null ? response.getPayload().getValue().getCountrySubdivisionInfos().getCountrySubdivisionInfo() : null;
            	CountryInfo.parseCountrySubdivisions(countrySubdivisionInfoTags, response.getPayload().getValue().getVersion());
            }
            
        } catch (MessageFacadeError e) {
            if (handleError(parameters, e) == RETRY) {
                countrySubdivision(retrievalType, countrySubdivisionVersion);
            }
            
            throw e;
        } finally {
            dropConnection(parameters);
        }
	}
	
	public synchronized SendReversalValidationResponse sendReversalValidation(MessageFacadeParam parameters, String mgiTransactionSessionID, 
			BigDecimal sendAmount, String sendCurrency, boolean feeRefundFlag, String operatorName, DataCollectionSet validationData, 
			ValidationType validationType) {
		SendReversalValidationResponse response = null;

        note(Messages.getString("MessageFacade.1908"), Messages.getString("MessageFacade.1909"));
        try {
            SendReversalValidationRequest sendReversalValidationRequest = new SendReversalValidationRequest();
            populateRequestWithHeader(sendReversalValidationRequest);
            sendReversalValidationRequest.setMgiSessionID(mgiTransactionSessionID);
            sendReversalValidationRequest.setSendAmount(sendAmount);
            sendReversalValidationRequest.setSendCurrency(sendCurrency);
            sendReversalValidationRequest.setFeeRefund(feeRefundFlag);
            sendReversalValidationRequest.setOperatorName(operatorName);
            String primaryReceiptLanguage = (String) validationData.getFieldValue(FieldKey.MF_VALID_LANG_RCPT_PRIME_KEY);
            sendReversalValidationRequest.setPrimaryReceiptLanguage(primaryReceiptLanguage);
            String secondaryReceiptLanguage = (String) validationData.getFieldValue(FieldKey.MF_VALID_LANG_RCPT_SECOND_KEY);
            if (StringUtility.getNonNullString(secondaryReceiptLanguage).length() > 0) {
               	sendReversalValidationRequest.setSecondaryReceiptLanguage(secondaryReceiptLanguage);
            }

            // Add information about the pay out.
            
            if (validationType.equals(ValidationType.SECONDARY)) {

	        	// Add fieldValue data to the request.
            
	        	List<KeyValuePairType> keyVals = addFieldValues(validationData);
	        	
	        	/*
	        	 * Setup check fields (if needed)
	        	 */
	        	setupCheckFields(validationData, keyVals, MoneyGramReceiveTransaction.PAYOUT_CASH);
	        	
				if ((keyVals != null) && (! keyVals.isEmpty())) {
					if (sendReversalValidationRequest.getFieldValues() == null) {
						SendReversalValidationRequest.FieldValues values = new SendReversalValidationRequest.FieldValues();
						sendReversalValidationRequest.setFieldValues(values);
					}		
					sendReversalValidationRequest.getFieldValues().getFieldValue().addAll(keyVals);
				}
	    		
	        	// Add verifiedFields data to the request.
				
				if ((validationData.getVerifiedFieldsSet() != null) && (! validationData.getVerifiedFieldsSet().isEmpty())) {
					SendReversalValidationRequest.VerifiedFields verifiedFields = new SendReversalValidationRequest.VerifiedFields();
		    		verifiedFields.getInfoKey().addAll(validationData.getVerifiedFieldsSet());
		    		sendReversalValidationRequest.setVerifiedFields(verifiedFields);
				}
            }
            
            ensureConnection(parameters);
            boolean isInitial = ! validationType.equals(ValidationType.SECONDARY);
            response = getAgentConnectLocal().sendReversalValidation(sendReversalValidationRequest, AUTOMATIC, isInitial);
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY) {
                return sendReversalValidation(parameters, mgiTransactionSessionID, sendAmount, sendCurrency, feeRefundFlag, operatorName, validationData, validationType);
            }
            return response;
        } finally {
            dropConnection(parameters);
        }
        return response;
	}
	
	public synchronized AmendValidationResponse amendValidation(MessageFacadeParam parameters, AmendValidationRequest amendValidationRequest, String mgiTransactionSessionID, String operatorName, DataCollectionSet validationData, ValidationType validationType) {
		AmendValidationResponse response = null;

        note(Messages.getString("MessageFacade.1906"), Messages.getString("MessageFacade.1907"));
        try {
            populateRequestWithHeader(amendValidationRequest);
            amendValidationRequest.setMgiSessionID(mgiTransactionSessionID);
            amendValidationRequest.setOperatorName(operatorName);
            String primaryReceiptLanguage = (String) validationData.getFieldValue(FieldKey.MF_VALID_LANG_RCPT_PRIME_KEY);
            amendValidationRequest.setPrimaryReceiptLanguage(primaryReceiptLanguage);
            String secondaryReceiptLanguage = (String) validationData.getFieldValue(FieldKey.MF_VALID_LANG_RCPT_SECOND_KEY);
            if (StringUtility.getNonNullString(secondaryReceiptLanguage).length() > 0) {
            	amendValidationRequest.setSecondaryReceiptLanguage(secondaryReceiptLanguage);
            }

            // Add information about the pay out.
            
            if (validationType.equals(ValidationType.SECONDARY)) {

	        	// Add fieldValue data to the request.
            
	        	List<KeyValuePairType> keyVals = addFieldValuesToAllowEmptyValues(validationData);
				if ((keyVals != null) && (! keyVals.isEmpty())) {
					if (amendValidationRequest.getFieldValues() == null) {
						AmendValidationRequest.FieldValues values = new AmendValidationRequest.FieldValues();
						amendValidationRequest.setFieldValues(values);
					}		
					amendValidationRequest.getFieldValues().getFieldValue().addAll(keyVals);
				}
	    		
	        	// Add verifiedFields data to the request.
				
				if ((validationData.getVerifiedFieldsSet() != null) && (! validationData.getVerifiedFieldsSet().isEmpty())) {
					AmendValidationRequest.VerifiedFields verifiedFields = new AmendValidationRequest.VerifiedFields();
		    		verifiedFields.getInfoKey().addAll(validationData.getVerifiedFieldsSet());
		    		amendValidationRequest.setVerifiedFields(verifiedFields);
				}
            }
            
            ensureConnection(parameters);
            boolean isInitial = ! validationType.equals(ValidationType.SECONDARY);
            response = getAgentConnectLocal().amendValidation(amendValidationRequest, AUTOMATIC, isInitial);
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY) {
                return amendValidation(parameters, amendValidationRequest, mgiTransactionSessionID, operatorName, validationData, validationType);
            }
            return response;
        } finally {
            dropConnection(parameters);
        }
        return response;
	}
	
	public synchronized ReceiveReversalValidationResponse receiveReversalValidation(MessageFacadeParam parameters, String mgiTransactionSessionID, BigDecimal sendAmount, String sendCurrency, String operatorName, DataCollectionSet validationData, ValidationType validationType) {
		ReceiveReversalValidationResponse response = null;

        note(Messages.getString("MessageFacade.1904"), Messages.getString("MessageFacade.1905"));
        try {
        	ReceiveReversalValidationRequest receiveReversalValidationRequest = new ReceiveReversalValidationRequest();
            populateRequestWithHeader(receiveReversalValidationRequest);
            receiveReversalValidationRequest.setMgiSessionID(mgiTransactionSessionID);
            receiveReversalValidationRequest.setReceiveAmount(sendAmount);
            receiveReversalValidationRequest.setReceiveCurrency(sendCurrency);

            receiveReversalValidationRequest.setOperatorName(operatorName);
            String primaryReceiptLanguage = (String) validationData.getFieldValue(FieldKey.MF_VALID_LANG_RCPT_PRIME_KEY);
            receiveReversalValidationRequest.setPrimaryReceiptLanguage(primaryReceiptLanguage);
            String secondaryReceiptLanguage = (String) validationData.getFieldValue(FieldKey.MF_VALID_LANG_RCPT_SECOND_KEY);
            if (StringUtility.getNonNullString(secondaryReceiptLanguage).length() > 0) {
               	receiveReversalValidationRequest.setSecondaryReceiptLanguage(secondaryReceiptLanguage);
            }

            // Add information about the pay out.
            
            if (validationType.equals(ValidationType.SECONDARY)) {
            	
	        	// Add fieldValue data to the request.            
	        	List<KeyValuePairType> keyVals = addFieldValues(validationData);
				if ((keyVals != null) && (! keyVals.isEmpty())) {
					if (receiveReversalValidationRequest.getFieldValues() == null) {
						ReceiveReversalValidationRequest.FieldValues values = new ReceiveReversalValidationRequest.FieldValues();
						receiveReversalValidationRequest.setFieldValues(values);
					}		
					receiveReversalValidationRequest.getFieldValues().getFieldValue().addAll(keyVals);
				}
	    		
	        	// Add verifiedFields data to the request.
				
				if ((validationData.getVerifiedFieldsSet() != null) && (! validationData.getVerifiedFieldsSet().isEmpty())) {
					ReceiveReversalValidationRequest.VerifiedFields verifiedFields = new ReceiveReversalValidationRequest.VerifiedFields();
		    		verifiedFields.getInfoKey().addAll(validationData.getVerifiedFieldsSet());
		    		receiveReversalValidationRequest.setVerifiedFields(verifiedFields);
				}
            }
            
            ensureConnection(parameters);
            boolean isInitial = ! validationType.equals(ValidationType.SECONDARY);
            response = getAgentConnectLocal().receiveReversalValidation(receiveReversalValidationRequest, AUTOMATIC, isInitial);
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY) {
                return receiveReversalValidation(parameters, mgiTransactionSessionID, sendAmount, sendCurrency, operatorName, validationData, validationType);
            }
            return response;
        } finally {
            dropConnection(parameters);
        }
        return response;
	}	

	public synchronized GetServiceOptionsResponse getServiceOptions(int retrievalType) throws Exception {
		GetServiceOptionsResponse response = null;

        note(Messages.getString("MessageFacade.1910"), Messages.getString("MessageFacade.1911"));

        MessageFacadeParam parameters = getTransmitParams(retrievalType);
		
        try {
        	GetServiceOptionsRequest request = new GetServiceOptionsRequest();
            populateRequestWithHeader(request);
            
            request.setAgentAllowedOnly(true);
            ensureConnection(parameters);
            response = getAgentConnectLocal().getServiceOptions(request, ALWAYS_LIVE);

        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY) {
                return getServiceOptions(retrievalType);
            }
            return response;
        } finally {
            dropConnection(parameters);
        }

        return response;
	}

	public synchronized GetCurrencyInfoResponse getCurrencyInfo(int retrievalType, String currentVersion) {
		GetCurrencyInfoResponse response = null;

        note(Messages.getString("MessageFacade.1912"), Messages.getString("MessageFacade.1913"));

        MessageFacadeParam parameters = getTransmitParams(retrievalType);
		
        try {
        	GetCurrencyInfoRequest request = new GetCurrencyInfoRequest();
            populateRequestWithHeader(request);
            
            request.setVersion(currentVersion);
            ensureConnection(parameters);
            response = getAgentConnectLocal().getCurrencyInfo(request, ALWAYS_LIVE);
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY) {
                return getCurrencyInfo(retrievalType, currentVersion);
            }
            return response;
        } finally {
            dropConnection(parameters);
        }

        return response;
	}

	public synchronized SearchConsumerProfilesResponse searchConsumerProfiles(DataCollectionSet data, int rule, Map<String, String> values) {
		SearchConsumerProfilesResponse response = null;

		if (data != null) {
	        note(Messages.getString("MessageFacade.1918"), Messages.getString("MessageFacade.1919"));
		} else {
			note(Messages.getString("MessageFacade.1928"), Messages.getString("MessageFacade.1929"));
		}

        MessageFacadeParam parameters = new MessageFacadeParam(MessageFacadeParam.REAL_TIME);
		
        try {
        	SearchConsumerProfilesRequest request = new SearchConsumerProfilesRequest();
            populateRequestWithHeader(request);
            ensureConnection(parameters);
            
            if (data != null) {
            	List<KeyValuePairType> keyVals = addFieldValues(data);
    			if ((keyVals != null) && (! keyVals.isEmpty())) {
    				
    				SearchConsumerProfilesRequest.FieldValues fieldValues = request.getFieldValues();
    				if (fieldValues == null) {
    					fieldValues = new SearchConsumerProfilesRequest.FieldValues();
    					request.setFieldValues(fieldValues);
    				}
    				fieldValues.getFieldValue().addAll(keyVals);
    			}
            }
            
            response = getAgentConnectLocal().searchConsumerProfiles(request, rule, values);
            
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY) {
                return searchConsumerProfiles(data, rule, values);
            }
            return response;
        } finally {
            dropConnection(parameters);
        }

        return response;
	}

	public synchronized GetProfileSenderResponse getProfileSender(ConsumerProfile profile, int rule) {
		GetProfileSenderResponse response = null;

        note(Messages.getString("MessageFacade.1920"), Messages.getString("MessageFacade.1921"));

        MessageFacadeParam parameters = new MessageFacadeParam(MessageFacadeParam.REAL_TIME);
		
        try {
        	GetProfileSenderRequest request = new GetProfileSenderRequest();
        	request.setConsumerProfileID(profile.getProfileId());
        	request.setConsumerProfileIDType(profile.getProfileType());
        	request.setMaxReceiversToReturn(50);
        	request.setMaxBillersToReturn(50);
            populateRequestWithHeader(request);
            ensureConnection(parameters);
            response = getAgentConnectLocal().getProfileSender(request, rule);
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY) {
                return getProfileSender(profile, rule);
            }
            return response;
        } finally {
            dropConnection(parameters);
        }

        return response;
	}
	
	public synchronized GetProfileReceiverResponse getProfileReceiver(ConsumerProfile profile, String mgiSessionID, Map<String, String> values) {
		GetProfileReceiverResponse response = null;

        note(Messages.getString("MessageFacade.1924"), Messages.getString("MessageFacade.1925"));

        MessageFacadeParam parameters = new MessageFacadeParam(MessageFacadeParam.REAL_TIME);
		
        try {
        	GetProfileReceiverRequest request = new GetProfileReceiverRequest();
        	request.setConsumerProfileID(profile.getProfileId());
        	request.setConsumerProfileIDType(profile.getProfileType());
        	request.setMgiSessionID(mgiSessionID);
            populateRequestWithHeader(request);
            ensureConnection(parameters);
            response = getAgentConnectLocal().getProfileReceiver(request, AUTOMATIC, values);
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY) {
                return getProfileReceiver(profile, mgiSessionID, values);
            }
            return response;
        } finally {
            dropConnection(parameters);
        }

        return response;
	}
	
	public synchronized CreateOrUpdateProfileSenderResponse createOrUpdateProfileSender(ConsumerProfile profile, DataCollectionSet data, boolean exists) {
		CreateOrUpdateProfileSenderResponse response = null;
		int rule;

		if (data != null) {
	        note(Messages.getString("MessageFacade.1922"), Messages.getString("MessageFacade.1923"));
			rule = AUTOMATIC;
		} else {
			note(Messages.getString("MessageFacade.1930"), Messages.getString("MessageFacade.1931"));
	        rule = ALWAYS_LIVE;
		}

        MessageFacadeParam parameters = new MessageFacadeParam(MessageFacadeParam.REAL_TIME);
		
        try {
        	CreateOrUpdateProfileSenderRequest request = new CreateOrUpdateProfileSenderRequest();
        	request.setConsumerProfileIDType(profile.getProfileType());
        	if (exists) {
            	request.setConsumerProfileID(profile.getProfileId());
        		request.setMgiSessionID(profile.getMgiSessionId());
        	}
        	
			if (data != null) {
            	List<KeyValuePairType> keyVals = addFieldValues(data);
    			if ((keyVals != null) && (! keyVals.isEmpty())) {
    				CreateOrUpdateProfileSenderRequest.FieldValues values = request.getFieldValues();
    				if (values == null) {
    					values = new CreateOrUpdateProfileSenderRequest.FieldValues();
    					request.setFieldValues(values);
    				}
    				values.getFieldValue().addAll(keyVals);
    			}
        	}
        	
            populateRequestWithHeader(request);
            ensureConnection(parameters);
            response = getAgentConnectLocal().createOrUpdateProfileSender(request, rule);
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY) {
                return createOrUpdateProfileSender(profile, data, exists);
            }
            return response;
        } finally {
            dropConnection(parameters);
        }

        return response;
	}
	
	public synchronized CreateOrUpdateProfileReceiverResponse createOrUpdateProfileReceiver(ConsumerProfile profile, DataCollectionSet data, boolean exists) {
		CreateOrUpdateProfileReceiverResponse response = null;

		int rule;
		if (data != null) {
			note(Messages.getString("MessageFacade.1926"), Messages.getString("MessageFacade.1927"));
			rule = AUTOMATIC;
		} else {
			note(Messages.getString("MessageFacade.1932"), Messages.getString("MessageFacade.1933"));
	        rule = ALWAYS_LIVE;
		}

        MessageFacadeParam parameters = new MessageFacadeParam(MessageFacadeParam.REAL_TIME);
		
        try {
        	CreateOrUpdateProfileReceiverRequest request = new CreateOrUpdateProfileReceiverRequest();
        	request.setConsumerProfileIDType(profile.getProfileType());
        	if (exists) {
            	request.setConsumerProfileID(profile.getProfileId());
        		request.setMgiSessionID(profile.getMgiSessionId());
        	}
        	
			if (data != null) {
            	List<KeyValuePairType> keyVals = addFieldValues(data);
    			if ((keyVals != null) && (! keyVals.isEmpty())) {
    				CreateOrUpdateProfileReceiverRequest.FieldValues values = request.getFieldValues();
    				if (values == null) {
    					values = new CreateOrUpdateProfileReceiverRequest.FieldValues();
    					request.setFieldValues(values);
    				}
    				values.getFieldValue().addAll(keyVals);
    			}
        	}
        	
            populateRequestWithHeader(request);
            ensureConnection(parameters);
            response = getAgentConnectLocal().createOrUpdateProfileReceiver(request, rule);
        } catch (Exception exception) {
            if (handleError(parameters, exception) == RETRY) {
                return createOrUpdateProfileReceiver(profile, data, exists);
            }
            return response;
        } finally {
            dropConnection(parameters);
        }

        return response;
	}
	
	void setupCheckFields(DataCollectionSet dataCollectionSet, List<KeyValuePairType> keyVals, int payoutMethod) {
		
    	BigDecimal amount1 = (BigDecimal) dataCollectionSet.getFieldValue(FieldKey.PAYOUT1_CHECKAMOUNT_KEY);
    	BigDecimal amount2 = (BigDecimal) dataCollectionSet.getFieldValue(FieldKey.PAYOUT2_CHECKAMOUNT_KEY);
    	
    	// Remove any old payout key value pairs.
    	
    	for (KeyValuePairType pair : new ArrayList<KeyValuePairType>(keyVals)) {
    		String xmlTag = pair.getInfoKey();
    		if (xmlTag.startsWith("payout1_") || xmlTag.startsWith("payout2_")) {
    			keyVals.remove(pair);
    		}
    	}
    	
    	/*
    	 * Setup check fields if Payout type 1 or Payout type 2 are defined.
    	 */
//		if (payoutMethod != MoneyGramReceiveTransaction.PAYOUT_CASH) {
    	/*
    	 * Setup check fields if Payout type 1 or Payout type 2 are defined.
    	 */
		if ( ((dataCollectionSet.getFieldMap().get(FieldKey.PAYOUT1_TYPE_KEY) != null) 
        		|| (dataCollectionSet.getFieldMap().get(FieldKey.PAYOUT2_TYPE_KEY) != null))) {
			/*
			 * Amount1 is present, put it in payout1 (Screen should always force a value in Amount1)
			 */
			if ((amount1 != null) && (amount1.compareTo(BigDecimal.ZERO) != 0)) {
				String payout1_checkType = dataCollectionSet.getFieldValue(FieldKey.PAYOUT1_TYPE_KEY) != null ? (String) dataCollectionSet.getFieldValue(FieldKey.PAYOUT1_TYPE_KEY) : "N";
				if (! payout1_checkType.isEmpty()) {
					addKeyValuePair(keyVals, dataCollectionSet, FieldKey.PAYOUT1_TYPE_KEY.getInfoKey(), payout1_checkType);
					String payout1_checkNumber = dataCollectionSet.getFieldValue(FieldKey.PAYOUT1_CHECKNUMBER_KEY) != null ? (String) dataCollectionSet.getFieldValue(FieldKey.PAYOUT1_CHECKNUMBER_KEY) : "";
					if (! payout1_checkNumber.isEmpty()) {
						addKeyValuePair(keyVals, dataCollectionSet, FieldKey.PAYOUT1_CHECKNUMBER_KEY.getInfoKey(), payout1_checkNumber);
					}
					addKeyValuePair(keyVals, dataCollectionSet, FieldKey.PAYOUT1_CHECKAMOUNT_KEY.getInfoKey(), amount1.toString());
					/*
					 * If amount2 is present, put it in payout2
					 */
	    			if ((amount2 != null) && (amount2.compareTo(BigDecimal.ZERO) != 0)) {
	    				String payout2_checkType = dataCollectionSet.getFieldValue(FieldKey.PAYOUT1_TYPE_KEY) != null ? (String) dataCollectionSet.getFieldValue(FieldKey.PAYOUT2_TYPE_KEY) : "N";
	    				if (! payout2_checkType.isEmpty()) {
	    					addKeyValuePair(keyVals, dataCollectionSet, FieldKey.PAYOUT2_TYPE_KEY.getInfoKey(), payout2_checkType);
	    					String payout2_checkNumber = dataCollectionSet.getFieldValue(FieldKey.PAYOUT2_CHECKNUMBER_KEY) != null ? (String) dataCollectionSet.getFieldValue(FieldKey.PAYOUT2_CHECKNUMBER_KEY) : "";
	    					if (! payout2_checkNumber.isEmpty()) {
	    						addKeyValuePair(keyVals, dataCollectionSet, FieldKey.PAYOUT2_CHECKNUMBER_KEY.getInfoKey(), payout2_checkNumber);
	    					}
	    					addKeyValuePair(keyVals, dataCollectionSet, FieldKey.PAYOUT2_CHECKAMOUNT_KEY.getInfoKey(), amount2.toString());
	    				}
	    				/*
	    				 * Set Payout 2 with dummy values (AC gets upset if we don't send it)
	    				 */
	    			} else {
	    				if (! payout1_checkType.isEmpty() ) {
	        				if (payout1_checkType.compareTo("A") == 0) {
	        					addKeyValuePair(keyVals, dataCollectionSet, FieldKey.PAYOUT2_TYPE_KEY.getInfoKey(), "C");
	        				} else {
	        					addKeyValuePair(keyVals, dataCollectionSet, FieldKey.PAYOUT2_TYPE_KEY.getInfoKey(), "A");
	        				}
	    					addKeyValuePair(keyVals, dataCollectionSet, FieldKey.PAYOUT2_CHECKNUMBER_KEY.getInfoKey(), "0");
	    					addKeyValuePair(keyVals, dataCollectionSet, FieldKey.PAYOUT2_CHECKAMOUNT_KEY.getInfoKey(), "0.00");
	    				}
	    			}
				}
			/*
			 * Amount1 was not present, put the amount2 values in payout1 (Should never happen)
			 */
			} else {
				if (amount2 != null) {
    				String payout2_checkType = dataCollectionSet.getFieldValue(FieldKey.PAYOUT2_TYPE_KEY) != null ? (String) dataCollectionSet.getFieldValue(FieldKey.PAYOUT2_TYPE_KEY) : "N";
    				if (! payout2_checkType.isEmpty()) {
    					addKeyValuePair(keyVals, dataCollectionSet, FieldKey.PAYOUT1_TYPE_KEY.getInfoKey(), payout2_checkType);
    					String payout2_checkNumber = dataCollectionSet.getFieldValue(FieldKey.PAYOUT1_CHECKNUMBER_KEY) != null ? (String) dataCollectionSet.getFieldValue(FieldKey.PAYOUT2_CHECKNUMBER_KEY) : "";
    					if (! payout2_checkNumber.isEmpty()) {
    						addKeyValuePair(keyVals, dataCollectionSet, FieldKey.PAYOUT1_CHECKNUMBER_KEY.getInfoKey(), payout2_checkNumber);
    					}
        				/*
        				 * Set Payout 2 with dummy values (AC gets upset if we don't send it)
        				 */
        				if (payout2_checkType.compareTo("A") == 0) {
        					addKeyValuePair(keyVals, dataCollectionSet, FieldKey.PAYOUT2_TYPE_KEY.getInfoKey(), "C");
        				} else {
        					addKeyValuePair(keyVals, dataCollectionSet, FieldKey.PAYOUT2_TYPE_KEY.getInfoKey(), "A");
        				}
    					addKeyValuePair(keyVals, dataCollectionSet, FieldKey.PAYOUT2_CHECKNUMBER_KEY.getInfoKey(), "0");
    					addKeyValuePair(keyVals, dataCollectionSet, FieldKey.PAYOUT2_CHECKAMOUNT_KEY.getInfoKey(), "0.00");
    				}
				}
			}
		}
	}
}
