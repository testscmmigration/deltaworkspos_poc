/*
 * AgentConnectTemplates.java
 * 
 * $Revision$
 *
 * Copyright (c) 2004 MoneyGram, Inc. All rights reserved
 */

package dw.comm;

import java.util.List;

import org.w3c.dom.Document;

import com.moneygram.agentconnect.BillPaymentDetailReportResponse;
import com.moneygram.agentconnect.BillerSearchResponse;
import com.moneygram.agentconnect.FeeLookupResponse;
import com.moneygram.agentconnect.GetProfileReceiverResponse;
import com.moneygram.agentconnect.MoneyGramSendDetailReportResponse;
import com.moneygram.agentconnect.Response;
import com.moneygram.agentconnect.SearchConsumerProfilesResponse;
import com.moneygram.agentconnect.SubagentsResponse;

import dw.model.XmlUtil;
import dw.utility.Debug;

/**
 * Produces templates for Agent Connect messages. Also has a static
 * method for generating adapter classes, which can be useful during
 * development.
 * @author Geoff Atkin
 */
public class AgentConnectTemplates {

	static Document templates = null;

	/**
	 * Format for outgoing timestamps to the RTS. Matches the format used by
	 * the AC4 simulator, which is 2004-09-02T09:50:45Z
	 * which is slightly different than the format the RTS uses for response
	 * timestamps, which is defined in AgentConnect.java.
	 */

	/** 
	 * Returns the XML data model for a message in the AC template file.
	 * Actually returns a clone, so callers can't accidently modify the template.
	 * Returns null if no such message exists.
	 */

	static synchronized Document getTemplates() {
		if (templates == null) {
			showMemory("AgentConnectTemplates before Builder.parse: ");
			templates = XmlUtil.loadFile("AgentConnectTemplates.xml", "xml/comm/AgentConnectTemplates.xml",
					"Failed to parse local AC template files, using internal one.");
			if (templates != null) {
				templates.getDocumentElement().normalize();
			}
			showMemory("AgentConnectTemplates after Builder.parse: ");

		}
		return templates;
	}

	private static void showMemory(String s) {
		Runtime runtime = Runtime.getRuntime();
		long total = runtime.totalMemory();
		long free = runtime.freeMemory();
		long used = (total - free) / 1024;
		long alloc = (total / 1024);

		Debug.println(s + used + " / " + alloc);
	}
	
	public static class ValidationResponse<T> {
		public T getValidationResponse(Class<T> responseClass, String className, boolean isInitial) {
			String xPath = "//" + className + "[@initial='" + (isInitial ? "true" : "false") + "']";
			XmlUtil<T> xmlUtil2 = new XmlUtil<T>(responseClass);
			List<T> list = xmlUtil2.getInfo(getTemplates(), xPath);
			if ((list != null) && (list.size() > 0)) {
				T response = responseClass.cast(list.get(0));
				return response;
			}
			return null;
		}
	}

	public static FeeLookupResponse feeLookup() {
		String expStr = "//feeLookupResponse";
		XmlUtil<FeeLookupResponse> xmlUtil2 = new XmlUtil<FeeLookupResponse>(FeeLookupResponse.class);
		List<FeeLookupResponse> infos = xmlUtil2.getInfo(getTemplates(), expStr);
		if (infos != null && infos.size() > 0) {
			return infos.get(0);
		}
		return null;
	}

	public static SubagentsResponse subagents() {
		String expStr = "//subagentsResponse";
		XmlUtil<SubagentsResponse> xmlUtil2 = new XmlUtil<SubagentsResponse>(SubagentsResponse.class);
		List<SubagentsResponse> infos = xmlUtil2.getInfo(getTemplates(), expStr);
		if (infos != null && infos.size() > 0) {
			return infos.get(0);
		}
		return null;
	}

	public static BillerSearchResponse billerSearch() {
		String expStr = "//billerSearchResponse";
		XmlUtil<BillerSearchResponse> xmlUtil2 = new XmlUtil<BillerSearchResponse>(BillerSearchResponse.class);
		List<BillerSearchResponse> infos = xmlUtil2.getInfo(getTemplates(), expStr);
		if (infos != null && infos.size() > 0) {
			return infos.get(0);
		}
		return null;
	}

	public static MoneyGramSendDetailReportResponse getMoneyGramSendDetailReport() {
		String expStr = "//moneyGramSendDetailReportResponse";
		XmlUtil<MoneyGramSendDetailReportResponse> xmlUtil2 = new XmlUtil<MoneyGramSendDetailReportResponse>(
				MoneyGramSendDetailReportResponse.class);
		List<MoneyGramSendDetailReportResponse> infos = xmlUtil2.getInfo(getTemplates(), expStr);
		if (infos != null && infos.size() > 0) {
			return infos.get(0);
		}
		return null;
	}

	public static BillPaymentDetailReportResponse billPaymentDetailReport() {
		String expStr = "//billPaymentDetailReportResponse";
		XmlUtil<BillPaymentDetailReportResponse> xmlUtil2 = new XmlUtil<BillPaymentDetailReportResponse>(
				BillPaymentDetailReportResponse.class);
		List<BillPaymentDetailReportResponse> infos = xmlUtil2.getInfo(getTemplates(), expStr);
		if (infos != null && infos.size() > 0) {
			return infos.get(0);
		}
		return null;
	}

	public static SearchConsumerProfilesResponse searchConsumerProfiles() {
		String expStr = "//searchConsumerProfilesResponse";
		XmlUtil<SearchConsumerProfilesResponse> xmlUtil2 = new XmlUtil<SearchConsumerProfilesResponse>(
				SearchConsumerProfilesResponse.class);
		List<SearchConsumerProfilesResponse> infos = xmlUtil2.getInfo(getTemplates(), expStr);
		if (infos != null && infos.size() > 0) {
			return infos.get(0);
		}
		return null;
	}
	
	public static GetProfileReceiverResponse getProfileReceiver() {
		String expStr = "//getProfileReceiverResponse";
		XmlUtil<GetProfileReceiverResponse> xmlUtil2 = new XmlUtil<GetProfileReceiverResponse>(
				GetProfileReceiverResponse.class);
		List<GetProfileReceiverResponse> infos = xmlUtil2.getInfo(getTemplates(), expStr);
		if (infos != null && infos.size() > 0) {
			return infos.get(0);
		}
		return null;
	}
	
	public static Response getResponse(Class<Response> tClass, String expStr) {
		XmlUtil<Response> xmlUtil2 = new XmlUtil<Response>(tClass);
		String xpathExpr = expStr;
		if (expStr != null && !expStr.isEmpty()) {
			if (!expStr.startsWith("//")) {
				xpathExpr = "//" + expStr;
			}
		}
		List<Response> infos = xmlUtil2.getInfo(getTemplates(), xpathExpr);
		if (infos != null && infos.size() > 0) {
			return infos.get(0);
		}
		return null;
	}
}
