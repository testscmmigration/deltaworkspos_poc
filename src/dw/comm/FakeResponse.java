package dw.comm;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.activation.DataHandler;
import javax.xml.datatype.XMLGregorianCalendar;

import com.moneygram.agentconnect.*;
import com.moneygram.agentconnect.CompleteSessionResponse.Payload.ReceiptInfo;
import com.moneygram.agentconnect.Response.Errors;

import dw.framework.ClientTransaction;
import dw.framework.FieldKey;
import dw.main.DeltaworksMainPanel;
import dw.mgsend.MoneyGramSendTransaction;
import dw.model.XmlUtil;
import dw.model.adapters.CountryInfo;
import dw.profile.Profile;
import dw.profile.ProfileItem;
import dw.profile.UnitProfile;
import dw.utility.Base64;
import dw.utility.Currency;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.ExtraDebug;
import dw.utility.TimeUtility;

public class FakeResponse {
	private static Map<Class<? extends Request>, ResponseObject> requestMap = null;
	private static Map<String, Object> transactionMap = null;
	
	private static final List<String> PRE_DEFINED_REFERENCE_NUMBERS = Arrays.asList("12345678", "23456789", "34567890", "87654321", "88888888", "99999999");
	
	private static final String AGENT_PDF_RECEIPT = "JVBERi0xLjUNCiW1tbW1DQoxIDAgb2JqDQo8PC9UeXBlL0NhdGFsb2cvUGFnZXMgMiAwIFIvTGFuZyhlbi1VUykgL1N0cnVjdFRyZWVSb290IDEwIDAgUi9NYXJrSW5mbzw8L01hcmtlZCB0cnVlPj4+Pg0KZW5kb2JqDQoyIDAgb2JqDQo8PC9UeXBlL1BhZ2VzL0NvdW50IDEvS2lkc1sgMyAwIFJdID4+DQplbmRvYmoNCjMgMCBvYmoNCjw8L1R5cGUvUGFnZS9QYXJlbnQgMiAwIFIvUmVzb3VyY2VzPDwvRm9udDw8L0YxIDUgMCBSPj4vRXh0R1N0YXRlPDwvR1M3IDcgMCBSL0dTOCA4IDAgUj4+L1Byb2NTZXRbL1BERi9UZXh0L0ltYWdlQi9JbWFnZUMvSW1hZ2VJXSA+Pi9NZWRpYUJveFsgMCAwIDYxMiA3OTJdIC9Db250ZW50cyA0IDAgUi9Hcm91cDw8L1R5cGUvR3JvdXAvUy9UcmFuc3BhcmVuY3kvQ1MvRGV2aWNlUkdCPj4vVGFicy9TL1N0cnVjdFBhcmVudHMgMD4+DQplbmRvYmoNCjQgMCBvYmoNCjw8L0ZpbHRlci9GbGF0ZURlY29kZS9MZW5ndGggMjM1Pj4NCnN0cmVhbQ0KeJyFkM9LwzAYhu+B/A/vMTksTZpfK4yB7WZRLAzNbXgYrgs9WEX9/zEWKx3SjZw+eN8nz/ch22G1yprqbgOZPRz6CNb2i7rk6zXKTYUyUJLdKuQG4USJgkwvjS4XTsEVXvglwmsK1U8e8ZMSiThMy9+ppmTPwseh6ztuWc8XmkX+jHBPyTbBfz4YqdoUQhdT6p5hksW2qYCJsppT/vP0TkgL56VQI7F5O7ZzAjrF/Xn8okB+XcAMhzK5sCPxJnKdGl+zEn64wrRyUUJflXBW6LSWMqIYiY9tknhpu/dZDeOEt+elfxrfyAF4dw0KZW5kc3RyZWFtDQplbmRvYmoNCjUgMCBvYmoNCjw8L1R5cGUvRm9udC9TdWJ0eXBlL1RydWVUeXBlL05hbWUvRjEvQmFzZUZvbnQvQXJpYWwvRW5jb2RpbmcvV2luQW5zaUVuY29kaW5nL0ZvbnREZXNjcmlwdG9yIDYgMCBSL0ZpcnN0Q2hhciAzMi9MYXN0Q2hhciAxMTYvV2lkdGhzIDIwIDAgUj4+DQplbmRvYmoNCjYgMCBvYmoNCjw8L1R5cGUvRm9udERlc2NyaXB0b3IvRm9udE5hbWUvQXJpYWwvRmxhZ3MgMzIvSXRhbGljQW5nbGUgMC9Bc2NlbnQgOTA1L0Rlc2NlbnQgLTIxMC9DYXBIZWlnaHQgNzI4L0F2Z1dpZHRoIDQ0MS9NYXhXaWR0aCAyNjY1L0ZvbnRXZWlnaHQgNDAwL1hIZWlnaHQgMjUwL0xlYWRpbmcgMzMvU3RlbVYgNDQvRm9udEJCb3hbIC02NjUgLTIxMCAyMDAwIDcyOF0gPj4NCmVuZG9iag0KNyAwIG9iag0KPDwvVHlwZS9FeHRHU3RhdGUvQk0vTm9ybWFsL2NhIDE+Pg0KZW5kb2JqDQo4IDAgb2JqDQo8PC9UeXBlL0V4dEdTdGF0ZS9CTS9Ob3JtYWwvQ0EgMT4+DQplbmRvYmoNCjkgMCBvYmoNCjw8L0F1dGhvcihHYWdlLCBEb3VnKSAvQ3JlYXRvcij+/wBNAGkAYwByAG8AcwBvAGYAdACuACAAVwBvAHIAZAAgADIAMAAxADApIC9DcmVhdGlvbkRhdGUoRDoyMDE3MDMyMTEyMjk0Ny0wNScwMCcpIC9Nb2REYXRlKEQ6MjAxNzAzMjExMjI5NDctMDUnMDAnKSAvUHJvZHVjZXIo/v8ATQBpAGMAcgBvAHMAbwBmAHQArgAgAFcAbwByAGQAIAAyADAAMQAwKSA+Pg0KZW5kb2JqDQoxNiAwIG9iag0KPDwvVHlwZS9PYmpTdG0vTiA5L0ZpcnN0IDYwL0ZpbHRlci9GbGF0ZURlY29kZS9MZW5ndGggMzE5Pj4NCnN0cmVhbQ0KeJyVUk2LwjAQvQv+h/kH07S2KoiwrMouokgr7EE8xDpbi20iMQX995tpK/Yg7O4l85H3XiYvEQPwQAwhFCBGIDwfxBhEGIAIwPcGrgP+MAQhIIgiED5EDAkh8kOYTHDDOA9iTHCD2/uFMLGmSu28oBKXO/D2gJsMasx02u/9gSL+T/H/TwleUrwHRRr7cjZ2K2a/6jBqwpjDHlqZDm1riGKtLca6oJW8sI18gJMnVe+yo9xh7aCR6eyu6WaXdAfRSi+cltKWcM3LXB2fxdZBD/qGCaUWP0geyTQ5cx75pypyRclJ8oTceFNOQdpcq7Y2Nv+WLqmrL23OB63PONNpVbqZ6s71RGQbh1YyNbpTv5/c2qlnuSx01mkkRX6kDrY5x8EyI0tc5FllqL3ruiqvO/6b4dPdXx6g3/sBizHPsg0KZW5kc3RyZWFtDQplbmRvYmoNCjIwIDAgb2JqDQpbIDI3OCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgNjY3IDAgMCAwIDAgMCAwIDAgMCAwIDAgMCA4MzMgMCAwIDAgMCA3MjIgMCA2MTEgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgNTU2IDAgNTAwIDU1NiA1NTYgMCA1NTYgMCAyMjIgMCAwIDAgMCA1NTYgNTU2IDU1NiAwIDMzMyAwIDI3OF0gDQplbmRvYmoNCjIxIDAgb2JqDQo8PC9UeXBlL1hSZWYvU2l6ZSAyMS9XWyAxIDQgMl0gL1Jvb3QgMSAwIFIvSW5mbyA5IDAgUi9JRFs8NjE0RkZGMjU5MUMxNUI0REIzNTQ1QUU2NDUwRkJBNDI+PDYxNEZGRjI1OTFDMTVCNERCMzU0NUFFNjQ1MEZCQTQyPl0gL0ZpbHRlci9GbGF0ZURlY29kZS9MZW5ndGggODI+Pg0Kc3RyZWFtDQp4nGNgAIL//xmBpCADA4iqhVBbwRTjXjDF9AlMMU8EUyyFEGoZhLoNlAdqEGBghVBsEIodQrFAKKgSDqAG1l0wHiOEYoJQzEA59lgGBgCYeQl6DQplbmRzdHJlYW0NCmVuZG9iag0KeHJlZg0KMCAyMg0KMDAwMDAwMDAxMCA2NTUzNSBmDQowMDAwMDAwMDE3IDAwMDAwIG4NCjAwMDAwMDAxMjUgMDAwMDAgbg0KMDAwMDAwMDE4MSAwMDAwMCBuDQowMDAwMDAwNDQ1IDAwMDAwIG4NCjAwMDAwMDA3NTQgMDAwMDAgbg0KMDAwMDAwMDkxMyAwMDAwMCBuDQowMDAwMDAxMTM3IDAwMDAwIG4NCjAwMDAwMDExOTAgMDAwMDAgbg0KMDAwMDAwMTI0MyAwMDAwMCBuDQowMDAwMDAwMDExIDY1NTM1IGYNCjAwMDAwMDAwMTIgNjU1MzUgZg0KMDAwMDAwMDAxMyA2NTUzNSBmDQowMDAwMDAwMDE0IDY1NTM1IGYNCjAwMDAwMDAwMTUgNjU1MzUgZg0KMDAwMDAwMDAxNiA2NTUzNSBmDQowMDAwMDAwMDE3IDY1NTM1IGYNCjAwMDAwMDAwMTggNjU1MzUgZg0KMDAwMDAwMDAxOSA2NTUzNSBmDQowMDAwMDAwMDAwIDY1NTM1IGYNCjAwMDAwMDE4ODUgMDAwMDAgbg0KMDAwMDAwMjExMCAwMDAwMCBuDQp0cmFpbGVyDQo8PC9TaXplIDIyL1Jvb3QgMSAwIFIvSW5mbyA5IDAgUi9JRFs8NjE0RkZGMjU5MUMxNUI0REIzNTQ1QUU2NDUwRkJBNDI+PDYxNEZGRjI1OTFDMTVCNERCMzU0NUFFNjQ1MEZCQTQyPl0gPj4NCnN0YXJ0eHJlZg0KMjM5MQ0KJSVFT0YNCnhyZWYNCjAgMA0KdHJhaWxlcg0KPDwvU2l6ZSAyMi9Sb290IDEgMCBSL0luZm8gOSAwIFIvSURbPDYxNEZGRjI1OTFDMTVCNERCMzU0NUFFNjQ1MEZCQTQyPjw2MTRGRkYyNTkxQzE1QjREQjM1NDVBRTY0NTBGQkE0Mj5dIC9QcmV2IDIzOTEvWFJlZlN0bSAyMTEwPj4NCnN0YXJ0eHJlZg0KMjk4Ng0KJSVFT0Y=";
	private static final String AGENT_TEXT_RECEIPT = "VHJhaW5pbmcgDQogIE1vZGUNCiAgQWdlbnQNCiBSZWNlaXB0";
	private static final String CUSTOMER_PDF_RECEIPT = "JVBERi0xLjUNCiW1tbW1DQoxIDAgb2JqDQo8PC9UeXBlL0NhdGFsb2cvUGFnZXMgMiAwIFIvTGFuZyhlbi1VUykgL1N0cnVjdFRyZWVSb290IDggMCBSL01hcmtJbmZvPDwvTWFya2VkIHRydWU+Pj4+DQplbmRvYmoNCjIgMCBvYmoNCjw8L1R5cGUvUGFnZXMvQ291bnQgMS9LaWRzWyAzIDAgUl0gPj4NCmVuZG9iag0KMyAwIG9iag0KPDwvVHlwZS9QYWdlL1BhcmVudCAyIDAgUi9SZXNvdXJjZXM8PC9Gb250PDwvRjEgNSAwIFI+Pi9Qcm9jU2V0Wy9QREYvVGV4dC9JbWFnZUIvSW1hZ2VDL0ltYWdlSV0gPj4vTWVkaWFCb3hbIDAgMCA2MTIgNzkyXSAvQ29udGVudHMgNCAwIFIvR3JvdXA8PC9UeXBlL0dyb3VwL1MvVHJhbnNwYXJlbmN5L0NTL0RldmljZVJHQj4+L1RhYnMvUy9TdHJ1Y3RQYXJlbnRzIDA+Pg0KZW5kb2JqDQo0IDAgb2JqDQo8PC9GaWx0ZXIvRmxhdGVEZWNvZGUvTGVuZ3RoIDIzND4+DQpzdHJlYW0NCnichdHPa8IwFMDxeyD/wzsmh77mR5MYEA+tTpQVxshNdpAtlh6s4tz/v3iotEiVQCDw3pcPBPIPmM/zutosQeTv+64BFrtsXfLFAsplBWWgJH+ToAoIB0okiHTS0yq0Eqx36GYQjpQIaG7XmpIdC5d927XcsI5nmjX8C8KWklVK3XJ9QxcetR82dgwGs7CqK4ABUE4B7ypnURiwTqDsi/XpJ04BdBp34/GnAPUSYDT6VCwUmr5Y/XHNfq+nI88Ui5cpi5mh0OPNpxb90mIN6mSRBfq++BmT5Tu25+vkn1h0Zrz0wPgHbOh2Zw0KZW5kc3RyZWFtDQplbmRvYmoNCjUgMCBvYmoNCjw8L1R5cGUvRm9udC9TdWJ0eXBlL1RydWVUeXBlL05hbWUvRjEvQmFzZUZvbnQvQXJpYWwvRW5jb2RpbmcvV2luQW5zaUVuY29kaW5nL0ZvbnREZXNjcmlwdG9yIDYgMCBSL0ZpcnN0Q2hhciAzMi9MYXN0Q2hhciAxMTcvV2lkdGhzIDE4IDAgUj4+DQplbmRvYmoNCjYgMCBvYmoNCjw8L1R5cGUvRm9udERlc2NyaXB0b3IvRm9udE5hbWUvQXJpYWwvRmxhZ3MgMzIvSXRhbGljQW5nbGUgMC9Bc2NlbnQgOTA1L0Rlc2NlbnQgLTIxMC9DYXBIZWlnaHQgNzI4L0F2Z1dpZHRoIDQ0MS9NYXhXaWR0aCAyNjY1L0ZvbnRXZWlnaHQgNDAwL1hIZWlnaHQgMjUwL0xlYWRpbmcgMzMvU3RlbVYgNDQvRm9udEJCb3hbIC02NjUgLTIxMCAyMDAwIDcyOF0gPj4NCmVuZG9iag0KNyAwIG9iag0KPDwvQXV0aG9yKEdhZ2UsIERvdWcpIC9DcmVhdG9yKP7/AE0AaQBjAHIAbwBzAG8AZgB0AK4AIABXAG8AcgBkACAAMgAwADEAMCkgL0NyZWF0aW9uRGF0ZShEOjIwMTcwMzIxMTIyNjU4LTA1JzAwJykgL01vZERhdGUoRDoyMDE3MDMyMTEyMjY1OC0wNScwMCcpIC9Qcm9kdWNlcij+/wBNAGkAYwByAG8AcwBvAGYAdACuACAAVwBvAHIAZAAgADIAMAAxADApID4+DQplbmRvYmoNCjE0IDAgb2JqDQo8PC9UeXBlL09ialN0bS9OIDkvRmlyc3QgNTgvRmlsdGVyL0ZsYXRlRGVjb2RlL0xlbmd0aCAzMTg+Pg0Kc3RyZWFtDQp4nJVSwWrCQBC9C/7D/MFkE40WRChVaRFFEqEH8bAm0xhMdmXdgP59d5LY5iC0Xnb2zbz3dvKI8MEDMYShABGC8HwQI4cDEAJ8bwBj8EcDeIEgHLgphJ5jBxD6AUwmuGGWBxHGuMHt7UwYW1Mldl5QicsdeHvATQYBc6bTfu8fEvG8xH9eEjySjO8KaezD1TiqiMOqS9iUEZc9tC4d2dYQRVpbjHRBK3l2IbK/cydVDzlP7rC1aFw60zVd7ZJuIFrnhbNS2hKu+Zir9BdsHfWgrxhTYvGdZEqmubPmfv9QRa4oPkpekBuvyjlIm2vVYmPzL+kuNfrU5nTQ+oQznVSl26nuXI5EtgloJROjO/jt6M4OnuWy0FmnERd5Sh1u846jZUaWuMizylD7reuqvOyg/tN+wv0j/37vGykYztsNCmVuZHN0cmVhbQ0KZW5kb2JqDQoxOCAwIG9iag0KWyAyNzggMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCA3MjIgMCAwIDAgMCAwIDAgMCAwIDAgODMzIDAgMCAwIDAgNzIyIDAgNjExIDAgMCAwIDAgMCAwIDAgMCAwIDAgMCAwIDU1NiAwIDUwMCA1NTYgNTU2IDAgNTU2IDAgMjIyIDAgMCAwIDgzMyA1NTYgNTU2IDU1NiAwIDMzMyA1MDAgMjc4IDU1Nl0gDQplbmRvYmoNCjE5IDAgb2JqDQo8PC9UeXBlL1hSZWYvU2l6ZSAxOS9XWyAxIDQgMl0gL1Jvb3QgMSAwIFIvSW5mbyA3IDAgUi9JRFs8QTBGQkIyMzQwMTFGQjk0NzlEQjFGNEUzMEZBNzM1ODg+PEEwRkJCMjM0MDExRkI5NDc5REIxRjRFMzBGQTczNTg4Pl0gL0ZpbHRlci9GbGF0ZURlY29kZS9MZW5ndGggNzY+Pg0Kc3RyZWFtDQp4nGNgAIL//xmBpCADA4iqgVBbwBTjLDDFdA5MMeeCKRZfoAhQCR8DK4Rig1DsEIoFQkGVcAA1sOrAeIwQiglCMQPl2IBGAwB9wAcuDQplbmRzdHJlYW0NCmVuZG9iag0KeHJlZg0KMCAyMA0KMDAwMDAwMDAwOCA2NTUzNSBmDQowMDAwMDAwMDE3IDAwMDAwIG4NCjAwMDAwMDAxMjQgMDAwMDAgbg0KMDAwMDAwMDE4MCAwMDAwMCBuDQowMDAwMDAwNDEwIDAwMDAwIG4NCjAwMDAwMDA3MTggMDAwMDAgbg0KMDAwMDAwMDg3NyAwMDAwMCBuDQowMDAwMDAxMTAxIDAwMDAwIG4NCjAwMDAwMDAwMDkgNjU1MzUgZg0KMDAwMDAwMDAxMCA2NTUzNSBmDQowMDAwMDAwMDExIDY1NTM1IGYNCjAwMDAwMDAwMTIgNjU1MzUgZg0KMDAwMDAwMDAxMyA2NTUzNSBmDQowMDAwMDAwMDE0IDY1NTM1IGYNCjAwMDAwMDAwMTUgNjU1MzUgZg0KMDAwMDAwMDAxNiA2NTUzNSBmDQowMDAwMDAwMDE3IDY1NTM1IGYNCjAwMDAwMDAwMDAgNjU1MzUgZg0KMDAwMDAwMTc0MiAwMDAwMCBuDQowMDAwMDAxOTc1IDAwMDAwIG4NCnRyYWlsZXINCjw8L1NpemUgMjAvUm9vdCAxIDAgUi9JbmZvIDcgMCBSL0lEWzxBMEZCQjIzNDAxMUZCOTQ3OURCMUY0RTMwRkE3MzU4OD48QTBGQkIyMzQwMTFGQjk0NzlEQjFGNEUzMEZBNzM1ODg+XSA+Pg0Kc3RhcnR4cmVmDQoyMjUwDQolJUVPRg0KeHJlZg0KMCAwDQp0cmFpbGVyDQo8PC9TaXplIDIwL1Jvb3QgMSAwIFIvSW5mbyA3IDAgUi9JRFs8QTBGQkIyMzQwMTFGQjk0NzlEQjFGNEUzMEZBNzM1ODg+PEEwRkJCMjM0MDExRkI5NDc5REIxRjRFMzBGQTczNTg4Pl0gL1ByZXYgMjI1MC9YUmVmU3RtIDE5NzU+Pg0Kc3RhcnR4cmVmDQoyODA1DQolJUVPRg==";	
	private static final String CUSTOMER_TEXT_RECEIPT = "VHJhaW5pbmcgDQogIE1vZGUNCkN1c3RvbWVyDQogUmVjZWlwdA==";
	
	public static final DecimalFormat FORMAT2 = new DecimalFormat("0.00");

	static {
		requestMap = new HashMap<Class<? extends Request>, ResponseObject>();

		createReqMap(AmendValidationRequest.class, AmendValidationResponse.class, "amendValidationResponse");
		createReqMap(BillerSearchRequest.class, BillerSearchResponse.class, "billerSearchResponse");
		createReqMap(BillPaymentDetailReportRequest.class, BillPaymentDetailReportResponse.class, "billPaymentDetailReportResponse");
		createReqMap(BillPaymentSummaryReportRequest.class, BillPaymentSummaryReportResponse.class, "billPaymentSummaryReportResponse");
		createReqMap(CheckInRequest.class, CheckInResponse.class, "checkInResponse");
		createReqMap(CityListRequest.class, CityListResponse.class, "cityListResponse");
		createReqMap(ComplianceTransactionRequest.class, ComplianceTransactionResponse.class, "complianceTransactionResponse");
		createReqMap(ConfirmTokenRequest.class, ConfirmTokenResponse.class, "confirmTokenResponse");
		createReqMap(CreateOrUpdateProfileSenderRequest.class, CreateOrUpdateProfileSenderResponse.class, "createOrUpdateProfileSenderResponse");
		createReqMap(CreateOrUpdateProfileReceiverRequest.class, CreateOrUpdateProfileReceiverResponse.class, "createOrUpdateProfileReceiverResponse");
		createReqMap(GetBankDetailsByLevelRequest.class, GetBankDetailsByLevelResponse.class, "getBankDetailsByLevelResponse");
		createReqMap(GetCountryInfoRequest.class, GetCountryInfoResponse.class, "countryInfoResponse");
		createReqMap(GetProfileSenderRequest.class, GetProfileSenderResponse.class, "getProfileSenderResponse");
		createReqMap(GetProfileReceiverRequest.class, GetProfileReceiverResponse.class, "getProfileReceiverResponse");
		createReqMap(DirectoryOfAgentsByAreaCodePrefixRequest.class, DirectoryOfAgentsByAreaCodePrefixResponse.class, "directoryOfAgentsByAreaCodePrefixResponse");
		createReqMap(DirectoryOfAgentsByCityRequest.class, DirectoryOfAgentsByCityResponse.class, "directoryOfAgentsByCityResponse");
		createReqMap(DirectoryOfAgentsByZipRequest.class, DirectoryOfAgentsByZipResponse.class, "directoryOfAgentsByZipResponse");
		createReqMap(DoddFrankStateRegulatorInfoRequest.class, DoddFrankStateRegulatorInfoResponse.class, "doddFrankStateRegulatorInfoResponse");
		createReqMap(DwPasswordRequest.class, DwPasswordResponse.class, "dwPasswordResponse");
		createReqMap(DwProfileRequest.class, DwProfileResponse.class, "dwProfileResponse");
		createReqMap(DwInitialSetupRequest.class, DwInitialSetupResponse.class, "dwInitialSetupResponse");
		createReqMap(FeeLookupRequest.class, FeeLookupResponse.class, "feeLookupResponse");
		createReqMap(ConsumerHistoryLookupRequest.class, ConsumerHistoryLookupResponse.class, "consumerHistoryLookupResponse");
		createReqMap(MoneyGramReceiveDetailReportRequest.class, MoneyGramReceiveDetailReportResponse.class, "moneyGramReceiveDetailReportResponse");
		createReqMap(MoneyGramReceiveSummaryReportRequest.class, MoneyGramReceiveSummaryReportResponse.class, "moneyGramReceiveSummaryReportResponse");
		createReqMap(MoneyGramSendDetailReportRequest.class, MoneyGramSendDetailReportResponse.class, "moneyGramSendDetailReportResponse");
		createReqMap(MoneyGramSendSummaryReportRequest.class, MoneyGramSendSummaryReportResponse.class, "moneyGramSendSummaryReportResponse");
		createReqMap(MoneyOrderTotalRequest.class, MoneyOrderTotalResponse.class, "moneyOrderTotalResponse");
		createReqMap(OpenOTPLoginRequest.class, OpenOTPLoginResponse.class, "openOTPLoginResponse");
		createReqMap(ProfileChangeRequest.class, ProfileChangeResponse.class, "profileChangeResponse");
		createReqMap(ReceiptsFormatDetailsRequest.class, ReceiptsFormatDetailsResponse.class, "receiptsFormatDetailsResponse");
		createReqMap(RegisterHardTokenRequest.class, RegisterHardTokenResponse.class, "registerHardTokenResponse");
		createReqMap(SaveDebugDataRequest.class, SaveDebugDataResponse.class, "saveDebugDataResponse");
		createReqMap(SaveSubagentsRequest.class, SaveSubagentsResponse.class, "saveSubagentsResponse");
		createReqMap(SearchConsumerProfilesRequest.class, SearchConsumerProfilesResponse.class, "searchConsumerProfilesResponse");
		createReqMap(SearchStagedTransactionsRequest.class, SearchStagedTransactionsResponse.class, "searchStagedTransactionsResponse");
		createReqMap(SendReversalValidationRequest.class, SendReversalValidationResponse.class, "sendReversalValidationResponse");
		createReqMap(SendValidationRequest.class, SendValidationResponse.class, "sendValidationResponse");
		createReqMap(SubagentsRequest.class, SubagentsResponse.class, "subagentsResponse");
		createReqMap(TransactionLookupRequest.class, TransactionLookupResponse.class, "transactionLookupResponse");
		createReqMap(VariableReceiptInfoRequest.class, VariableReceiptInfoResponse.class, "variableReceiptInfoResponse");
		createReqMap(VersionManifestRequest.class, VersionManifestResponse.class, "versionManifestResponse");
	}
	
	private static class ResponseObject {
		private Class<? extends Response> responseClass;
		private String responseName;
	}
	
	private static String generateReferenceNumber() {
		String referenceNumber = "00000000";
		try {
			SecureRandom sr = new SecureRandom();
			int rn = 10000000 + (int) (sr.nextDouble() * 90000000);
			referenceNumber =  String.valueOf(rn);
		} catch (Exception e) {
			Debug.printException(e);
		}
		return referenceNumber;
	}

	private static void createReqMap(Class<? extends Request> reqClazz, Class<? extends Response> respClazz, String respClazzName) {
		ResponseObject object = new ResponseObject();
		object.responseClass = respClazz;
		object.responseName = respClazzName;
		requestMap.put(reqClazz, object);
	}

	static final BigDecimal fakeFeeAmount2 = new BigDecimal("2.00");
	static final BigDecimal fakeFeeAmount0 = new BigDecimal("2000");

	private static String TRANSACTION_LOOKUP_RESPONSE = "transactionLookupResponse";

	public static void initDataMap(){
		transactionMap = new HashMap<String, Object>();
	}

	/** 
	 * Generate a fake response message to match the given request message.
	 * Usually, the fake response is just pulled from the templates file.
	 * Response messages are dynamically generated for transactionLookupRequest,
	 * moneyGramSendRequest, and feeLookupRequest to provide more realism by
	 * taking request data into account.
	 */
	
	static Response fakeResponse(Request request) {
		return fakeResponse(request, null, null, false, null); 
	}
	
	static Response fakeResponse(Request request, boolean flag) {
		return fakeResponse(request, null, null, flag, null); 
	}
	
	static Response fakeResponse(Request request, String referenceNumber, boolean flag) {
		return fakeResponse(request, null, referenceNumber, flag, null); 
	}
	
	static Response fakeResponse(Request request, Request validationRequest, String referenceNumber) {
		return fakeResponse(request, validationRequest, referenceNumber, false, null);
	}
	
	static public Response fakeResponse(Request request, Map<String, String> values) {
		return fakeResponse(request, null, null, false, values);
	}	
	
	static private Response fakeResponse(Request request, Request validationRequest, String referenceNumber, boolean flag, Map<String, String> values) {
		ExtraDebug.println("Fake AC Call - Request class = " + request.getClass());
		Response resp = null;

		try {
			Thread.sleep(500); // Added to fix a DEMO mode thread problem in MGR.
			BigDecimal feeAmount = fakeFeeAmount0;
			String agentCurrency = UnitProfile.getInstance().get("AGENT_CURRENCY", "USD");
			if (Currency.getCurrencyPrecision(agentCurrency) == 2) {
				feeAmount = fakeFeeAmount2;
			}
			
			if (request instanceof SendValidationRequest) { 
				SendValidationResponse sendValidationRespone = new AgentConnectTemplates.ValidationResponse<SendValidationResponse>().getValidationResponse(SendValidationResponse.class, "SendValidationResponse", flag);
				TransactionLookupResponse transactionLookupResponse = (TransactionLookupResponse) getCachedResponse(TRANSACTION_LOOKUP_RESPONSE, referenceNumber);
				if (transactionLookupResponse != null) {
					SendAmountInfo lookupSendAmounts = transactionLookupResponse.getPayload().getValue().getSendAmounts();
					sendValidationRespone.getPayload().getValue().setSendAmounts(lookupSendAmounts);
				}
				resp = sendValidationRespone;
				
			} else if (request instanceof BPValidationRequest) {
				BPValidationResponse bpValidationResponse = new AgentConnectTemplates.ValidationResponse<BPValidationResponse>().getValidationResponse(BPValidationResponse.class, "BPValidationResponse", flag);
				TransactionLookupResponse transactionLookupResponse = (TransactionLookupResponse) getCachedResponse(TRANSACTION_LOOKUP_RESPONSE, referenceNumber);
				if (transactionLookupResponse != null) {
					SendAmountInfo lookupSendAmounts = transactionLookupResponse.getPayload().getValue().getSendAmounts();
					bpValidationResponse.getPayload().getValue().setSendAmounts(lookupSendAmounts);
				}
				resp = bpValidationResponse;
				
			} else if (request instanceof ReceiveValidationRequest) {
				resp = new AgentConnectTemplates.ValidationResponse<ReceiveValidationResponse>().getValidationResponse(ReceiveValidationResponse.class, "ReceiveValidationResponse", flag);
				
			} else if (request instanceof AmendValidationRequest) {
				resp = new AgentConnectTemplates.ValidationResponse<AmendValidationResponse>().getValidationResponse(AmendValidationResponse.class, "AmendValidationResponse", flag);
				
			} else if (request instanceof SendReversalValidationRequest) {
				resp = new AgentConnectTemplates.ValidationResponse<SendReversalValidationResponse>().getValidationResponse(SendReversalValidationResponse.class, "SendReversalValidationResponse", flag);
				
			} else if (request instanceof ReceiveReversalValidationRequest) {
				resp = new AgentConnectTemplates.ValidationResponse<ReceiveReversalValidationResponse>().getValidationResponse(ReceiveReversalValidationResponse.class, "ReceiveReversalValidationResponse", flag);
				
			} else if (request instanceof BillerSearchRequest) {
				resp = billerSearch((BillerSearchRequest) request);
		
			} else if (request instanceof CompleteSessionRequest) {
				resp = completeSession((CompleteSessionRequest) request, validationRequest, referenceNumber);
			
			} else if (request instanceof DwProfileRequest) {
				resp = dwProfile();
			
			} else if (request instanceof FeeLookupRequest) {
				resp = feeLookup(request, feeAmount);
			
			} else if (request instanceof TransactionLookupRequest) {
				resp = transactionLookup((TransactionLookupRequest) request, flag);
				
			} else if (request instanceof SearchStagedTransactionsRequest) {
				resp = searchStagedTransactions();
			
			} else if (request instanceof SearchConsumerProfilesRequest) {
				resp = AgentConnectTemplates.searchConsumerProfiles();
				List<KeyValuePairType> pairs = ((SearchConsumerProfilesResponse) resp).getPayload().getValue().getConsumerProfileSearchInfos().getConsumerProfileSearch().get(0).getCurrentValues().getCurrentValue();
				addValues(pairs, values);
			
			} else if (request instanceof GetProfileReceiverRequest) {
				resp = AgentConnectTemplates.getProfileReceiver();
				List<KeyValuePairType> pairs = ((GetProfileReceiverResponse) resp).getPayload().getValue().getCurrentValues().getCurrentValue();
				addValues(pairs, values);

			} else {
				ResponseObject objArr = requestMap.get(request.getClass());
				if (objArr != null) {
					Debug.println("AgentConnect.fakeResponse() request=" + objArr.responseName + "::" + objArr.responseClass);
					resp = AgentConnectTemplates.getResponse((Class<Response>) objArr.responseClass, objArr.responseName);
					Debug.println("AgentConnect.fakeResponse() response=" + resp);
				}
			}
			
		} catch (Exception e) {
			Debug.printException(e);
			Debug.printStackTrace(e);
			Debug.dumpStack();
			Debug.println("AgentConnect.fakeResponse() falling back to canned response for " + request.getClass());
		}
		return resp;
	}

	static private void addValues(List<KeyValuePairType> pairs, Map<String, String> values) {
		ObjectFactory factory = new ObjectFactory();
		
		nextKey: for (Map.Entry<String, String> entry : values.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			for (KeyValuePairType pair : pairs) {
				if (pair.getInfoKey().equals(key)) {
					pair.getValue().setValue(value);
					continue nextKey;
				}
			}
			KeyValuePairType pair = new KeyValuePairType();
			pair.setInfoKey(key);
			pair.setValue(factory.createKeyValuePairTypeValue(value));
			pairs.add(pair);
		}
	}
	
	private static Response searchStagedTransactions() {
		ResponseObject o = requestMap.get(SearchStagedTransactionsRequest.class);
		SearchStagedTransactionsResponse searchStagedTransactionsResponse = (SearchStagedTransactionsResponse) AgentConnectTemplates.getResponse((Class<Response>) o.responseClass, o.responseName);
		List<StagedTransactionInfo> list = searchStagedTransactionsResponse.getPayload().getValue().getStagedTransactionInfos().getStagedTransactionInfo();

        try {
            Profile productProfile = (Profile) UnitProfile.getInstance().getProfileInstance().find("PRODUCT", DWValues.MONEY_GRAM_SEND_DOC_SEQ);
            boolean formFreeEnabled = productProfile.find("FORM_FREE_ENABLED").stringValue().equalsIgnoreCase("Y");
            boolean auth = productProfile.getStatus().equalsIgnoreCase("A");
            if ((! formFreeEnabled) || (! auth)) {
            	list.remove(1);
            }
        } catch (NoSuchElementException e) {
        }

        try {
            Profile productProfile = (Profile) UnitProfile.getInstance().getProfileInstance().find("PRODUCT", DWValues.BILL_PAYMENT_DOC_SEQ);
            boolean formFreeEnabled = productProfile.find("FORM_FREE_ENABLED").stringValue().equalsIgnoreCase("Y");
            boolean auth = productProfile.getStatus().equalsIgnoreCase("A");
            if ((! formFreeEnabled) || (! auth)) {
            	list.remove(0);
            }
        } catch (NoSuchElementException e) {
        }

		return searchStagedTransactionsResponse;
	}

	private static Response billerSearch(Request request) {
		BillerSearchRequest tRequest = (BillerSearchRequest) request;
		if (BillerSearchType.BIN.equals(tRequest.getSearchType())) {

			String binNumber = tRequest.getBinNumber();
			BillerSearchResponse response = AgentConnectTemplates.billerSearch();
			BillerSearchResponse.Payload payload = response.getPayload().getValue();
			if (binNumber.equals("123456")) {

				List<BillerInfo> list = payload.getBillerInfo();
				Iterator<BillerInfo> i = list.iterator();
				while (i.hasNext()) {
					BillerInfo e = i.next();
					e.setBillerName("MGRAM ANOW INSTANT ISSUE CD");
					e.setReceiveCode("7027");
					e.setExpeditedEligibleFlag(true);
				}
			} else if (binNumber.equals("555566")) {
				List<BillerInfo> list = payload.getBillerInfo();
				Iterator<BillerInfo> i = list.iterator();
				while (i.hasNext()) {
					BillerInfo e = i.next();
					e.setBillerName("RUSHCARD");
					e.setReceiveCode("2495");
					e.setExpeditedEligibleFlag(false);

				}
			} else if (binNumber.equals("876543")) {
				List<BillerInfo> list = payload.getBillerInfo();
				Iterator<BillerInfo> i = list.iterator();
				while (i.hasNext()) {
					BillerInfo e = i.next();
					e.setBillerName("MONEYGRAM ACCOUNTNOW VISA");
					e.setReceiveCode("7028");
					e.setExpeditedEligibleFlag(true);
				}
			}
			return response;
		}
		return null;
	}

	private static Response dwProfile() {
		try {
			SubagentsResponse subagents = AgentConnectTemplates.subagents();
			SubagentsResponse.Payload payload = subagents.getPayload().getValue();
			String agentID = String.valueOf(payload.getSubagents().getSubagent().get(0).getAgentID()); 
			SubagentInfo elem = payload.getSubagents().getSubagent().get(0);
			Profile p = (Profile) UnitProfile.getInstance().getProfileInstance().clone();

			String branch = elem.getBranchName();
			ProfileItem name = new ProfileItem("ITEM", "GENERIC_DOCUMENT_STORE", "V", branch, null);
			p.insertOrUpdate("GENERIC_DOCUMENT_STORE", name);

			String yn = (agentID.equals("40000000")) ? "Y" : "N";
			ProfileItem superagent = new ProfileItem("ITEM", "SUPERAGENT", "H", yn, null);
			p.insertOrUpdate("SUPERAGENT", superagent);

			// set subagent item to the opposite of superagent
			yn = (agentID.equals("40000000")) ? "N" : "YN";
			ProfileItem subagent = new ProfileItem("ITEM", "SUBAGENT_MODE", "H", yn, null);
			p.insertOrUpdate("SUBAGENT_MODE", subagent);

			String legacy = elem.getLegacyAgentID();
			ProfileItem account = new ProfileItem("ITEM", "MG_ACCOUNT", "V", legacy, null);
			p.insertOrUpdate("MG_ACCOUNT", account);
			return subagents;
		} catch (CloneNotSupportedException e) {
			Debug.printStackTrace(e);
			return null;
		}
	}

	private static FeeLookupResponse feeLookup(Request request, BigDecimal feeAmount)
			throws SystemError_Exception {
		FeeLookupRequest tRequest = (FeeLookupRequest) request;
		FeeLookupResponse response = AgentConnectTemplates.feeLookup();
		List<FeeInfo> feeInfos = response.getPayload().getValue().getFeeInfos().getFeeInfo();
		FeeInfo feeInfo = feeInfos.get(0);
		BigDecimal sendAmount = BigDecimal.ZERO;
		BigDecimal receiveAmount = null;
		BigDecimal totalAmount;
		String receiveCurrency = CountryInfo.getPayoutCurrencyForCountry(tRequest.getDestinationCountry());
		String sendCurrency = tRequest.getSendCurrency();
		BigDecimal exchangeRate = fakeExchange(sendCurrency, receiveCurrency);

		if (tRequest.getAmountExcludingFee() != null) {
			sendAmount = tRequest.getAmountExcludingFee();
		} else if (tRequest.getAmountIncludingFee() != null) {
			sendAmount = tRequest.getAmountIncludingFee();
			if (sendAmount.compareTo(feeAmount) <= 0) {
				FakeResponse.fakeError("702", "", "INVALID SCENARIO -- FEE AMT GREATER THAN SEND AMT");
			}
			sendAmount = sendAmount.subtract(feeAmount);
		} else if (tRequest.getReceiveAmount() != null) {
			receiveAmount = tRequest.getReceiveAmount();
			sendAmount = receiveAmount.divide(exchangeRate, Currency.getCurrencyPrecision(receiveCurrency),
					BigDecimal.ROUND_HALF_DOWN);
		} else {
			FakeResponse.fakeError("702", "", "INVALID SCENARIO");
		}

		//response.setChildText(DWValues.MF_MGI_TRX_SESSION_ID,Integer.toString(prng.nextInt(90000000) + 10000000));

		receiveAmount = sendAmount.multiply(exchangeRate)
				.setScale(Currency.getCurrencyPrecision(receiveCurrency), BigDecimal.ROUND_HALF_UP);
		totalAmount = sendAmount.add(feeAmount);

		SendAmountInfo sendAmt = feeInfo.getSendAmounts();
		if (sendAmt == null) {
			sendAmt = new SendAmountInfo();
		}
		sendAmt.setSendAmount(sendAmount);
		sendAmt.setSendCurrency(sendCurrency);
		sendAmt.setTotalSendFees(feeAmount);
		sendAmt.getDetailSendAmounts().getDetailSendAmount().addAll(feeInfo.getSendAmounts().getDetailSendAmounts().getDetailSendAmount());

		EstimatedReceiveAmountInfo eRecvAmts = feeInfo.getReceiveAmounts();
		eRecvAmts.setReceiveAmount(receiveAmount);
		eRecvAmts.setTotalReceiveAmount(receiveAmount);
		eRecvAmts.setReceiveCurrency(receiveCurrency);
		eRecvAmts = new EstimatedReceiveAmountInfo();
		eRecvAmts.getDetailEstimatedReceiveAmounts().getDetailEstimatedReceiveAmount().addAll(feeInfo.getReceiveAmounts().getDetailEstimatedReceiveAmounts().getDetailEstimatedReceiveAmount());

		feeInfo.setValidExchangeRate(exchangeRate);
		feeInfo.setTotalAmount(totalAmount);
		feeInfo.setDestinationCountry(tRequest.getDestinationCountry());
		if (tRequest.isAllOptions()) {
			feeInfo.setServiceOption("WILL_CALL");
		}

		feeInfo.setReceiveAmountAltered(false);
		feeInfo.setRevisedInformationalFee(false);
		feeInfo.setServiceOptionCategoryId("0");
		feeInfo.setServiceOptionDisplayName("10 Minute Service");
		return response;
	}
	
	private static final BigDecimal GBP_RATE = new BigDecimal("0.53821313");

	private static final BigDecimal EUR_RATE = new BigDecimal("0.80879974");

	private static final BigDecimal MAD_RATE = new BigDecimal("8.88");

	private static final BigDecimal MXN_RATE = new BigDecimal("11.53");

	/**
	 * Computes a cross rate, based on a very limited set of hard-coded exchange
	 * rates. Currencies other than GBP, EUR, MAD, and MXN are assumed equal to
	 * USD.
	 */
	public static BigDecimal fakeExchange(String sendCurrency,
			String receiveCurrency) {
		BigDecimal exchange = BigDecimal.valueOf(1);
		exchange = exchange.setScale(6, BigDecimal.ROUND_HALF_UP);
		if (!sendCurrency.equals(receiveCurrency)) {
			// convert to US dollar
			if (sendCurrency.equals("GBP"))
				exchange = exchange.divide(GBP_RATE, BigDecimal.ROUND_HALF_UP);
			else if (sendCurrency.equals("EUR"))
				exchange = exchange.divide(EUR_RATE, BigDecimal.ROUND_HALF_UP);
			else if (sendCurrency.equals("MAD"))
				exchange = exchange.divide(MAD_RATE, BigDecimal.ROUND_HALF_UP);
			else if (sendCurrency.equals("MXN"))
				exchange = exchange.divide(MXN_RATE, BigDecimal.ROUND_HALF_UP);

			// convert to receive currency
			if (receiveCurrency.equals("GBP"))
				exchange = exchange.multiply(GBP_RATE);
			else if (receiveCurrency.equals("EUR"))
				exchange = exchange.multiply(EUR_RATE);
			else if (receiveCurrency.equals("MAD"))
				exchange = exchange.multiply(MAD_RATE);
			else if (receiveCurrency.equals("MXN"))
				exchange = exchange.multiply(MXN_RATE);

			exchange = exchange.setScale(6, BigDecimal.ROUND_HALF_UP);
		}

		return exchange;
	}


	
	private static ReceiveAmountInfo getReceiveAmounts(String payoutCurrency, EstimatedReceiveAmountInfo estimated) {
		ReceiveAmountInfo receiveAmount = new ReceiveAmountInfo();
		ReceiveAmountInfo.DetailReceiveAmounts detailReceiveAmounts = new ReceiveAmountInfo.DetailReceiveAmounts();
		receiveAmount.setDetailReceiveAmounts(detailReceiveAmounts);
		detailReceiveAmounts.getDetailReceiveAmount().addAll(estimated.getDetailEstimatedReceiveAmounts().getDetailEstimatedReceiveAmount());
		
		receiveAmount.setPayoutCurrency(payoutCurrency);
		receiveAmount.setReceiveAmount(estimated.getReceiveAmount());
		receiveAmount.setReceiveCurrency(estimated.getReceiveCurrency());
		receiveAmount.setReceiveFeesAreEstimated(estimated.isReceiveFeesAreEstimated());
		receiveAmount.setReceiveTaxesAreEstimated(estimated.isReceiveTaxesAreEstimated());
		receiveAmount.setTotalReceiveAmount(estimated.getTotalReceiveAmount());
		receiveAmount.setTotalReceiveFees(estimated.getTotalReceiveFees());
		receiveAmount.setTotalReceiveFees(estimated.getTotalReceiveFees());
		receiveAmount.setTotalReceiveTaxes(estimated.getTotalReceiveTaxes());
		receiveAmount.setValidCurrencyIndicator(estimated.isValidCurrencyIndicator());
		
		return receiveAmount;
	}
	
	private static CurrentValuesType getCurrentValues(List<KeyValuePairType> fieldValueList) {
		CurrentValuesType currentValues = new CurrentValuesType();
		currentValues.getCurrentValue().addAll(fieldValueList);
		return currentValues;
	}

	private static CompleteSessionResponse completeSession(CompleteSessionRequest request, Request validationRequest, String referenceNumber) throws NoSuchAlgorithmException, SystemError_Exception {
		CompleteSessionRequest completeSessionRequest = (CompleteSessionRequest) request;
		ClientTransaction transaction = DeltaworksMainPanel.getMainPanel().getCurrentTransaction();

		ObjectFactory factory = new ObjectFactory();
		CompleteSessionResponse response = factory.createCompleteSessionResponse();
		CompleteSessionResponse.Payload payload = new CompleteSessionResponse.Payload();
		response.setPayload(factory.createCompleteSessionResponsePayload(payload));
		
		if (payload != null) {
			XMLGregorianCalendar now = TimeUtility.toXmlCalendar(TimeUtility.currentTime());
			payload.setTransactionDateTime(now);
			payload.setExpectedDateOfDelivery(now);
			
			// Receipt Data
			
			ReceiptInfo receiptInfo = new ReceiptInfo();
			payload.setReceiptInfo(receiptInfo);

			CompletionReceiptType completionReceiptType = new CompletionReceiptType();
			payload.setReceipts(completionReceiptType);
			String printerType = UnitProfile.getInstance().get("AGENT_PRINTER_RECEIPT_TYPE", "");
			
			completionReceiptType.setReceiptMimeType(printerType.equals("TEXT") ? "application/vnd.text" :"application/pdf");
			
			// Agent Receipt
			
			ReceiptResponseType receipt = new ReceiptResponseType();
			completionReceiptType.setAgentReceiptMimeData(receipt);
			ReceiptSegmentType segment = new ReceiptSegmentType();
			receipt.getReceiptMimeDataSegment().add(segment);
			String receiptContents = printerType.equals("TEXT") ? AGENT_TEXT_RECEIPT : AGENT_PDF_RECEIPT;
			segment.setMimeData(new DataHandler(new ByteArrayInputStream(Base64.decodedBase64Item(receiptContents)), "text/plain"));

			// Customer Receipt
			
			receipt = new ReceiptResponseType();
			completionReceiptType.setConsumerReceipt1MimeData(receipt);
			segment = new ReceiptSegmentType();
			receipt.getReceiptMimeDataSegment().add(segment);
			receiptContents = printerType.equals("TEXT") ? CUSTOMER_TEXT_RECEIPT : CUSTOMER_PDF_RECEIPT;
			segment.setMimeData(new DataHandler(new ByteArrayInputStream(Base64.decodedBase64Item(receiptContents)), "text/plain"));

			payload.setFreePhoneCallPIN("1234567890");
		}
		
		if ((completeSessionRequest.getMgiSessionType().equals(SessionType.SEND)) || (completeSessionRequest.getMgiSessionType().equals(SessionType.BP))) {
			MoneyGramSendTransaction tran = (MoneyGramSendTransaction) transaction;
			TransactionLookupResponse transactionLookupResponse = null;
			TransactionLookupResponse.Payload transactionLookupResponsePayload = null;
			
			if (tran.isFormFreeTransaction()) {
				
				transactionLookupResponse = (TransactionLookupResponse) getCachedResponse(TRANSACTION_LOOKUP_RESPONSE, referenceNumber);
				transactionLookupResponsePayload = transactionLookupResponse.getPayload().getValue();
				payload.setReferenceNumber(referenceNumber);
				
			} else {
				
				// Generate a reference number.
				
				referenceNumber = generateReferenceNumber();
				response.getPayload().getValue().setReferenceNumber(referenceNumber);
				
				// Create a transactionLookup response.
				
				ResponseObject o = requestMap.get(TransactionLookupRequest.class);
				transactionLookupResponse = (TransactionLookupResponse) AgentConnectTemplates.getResponse((Class<Response>) o.responseClass, o.responseName);
				FeeInfo feeInfo = tran.getCurrentFeeInfo();
				SendValidationRequest sendValidationRequest = tran.getSendValidationRequest();
				BPValidationRequest bpValidationRequest = (BPValidationRequest) tran.getValidationRequest();

				transactionLookupResponsePayload = ((TransactionLookupResponse) transactionLookupResponse).getPayload().getValue();
				if (transactionLookupResponsePayload != null) {
					transactionLookupResponsePayload.setSendAmounts(feeInfo.getSendAmounts());
					transactionLookupResponsePayload.setReceiveAmounts(getReceiveAmounts(feeInfo.getEstimatedReceiveCurrency(), feeInfo.getReceiveAmounts()));
					if (tran.isBillPayment()) {
						transactionLookupResponsePayload.setCurrentValues(getCurrentValues(bpValidationRequest.getFieldValues().getFieldValue()));
					} else {
						transactionLookupResponsePayload.setCurrentValues(getCurrentValues(sendValidationRequest.getFieldValues().getFieldValue()));
					}
				}
			}

			// Add the date and time the transaction was sent.
			
			if (transactionLookupResponsePayload != null) {
	    		ObjectFactory objectFactory = new ObjectFactory();
	    		
	    		KeyValuePairType pair = new KeyValuePairType();
	    		pair.setInfoKey("dateTimeSent");
	    		String dts = new SimpleDateFormat(TimeUtility.SERVER_DATE_TIME_FORMAT_2).format(TimeUtility.currentTime());
	    		if ((dts != null) && (dts.length() > 2)) {
	    			int i = dts.length() - 2;
	    			dts = dts.substring(0, i) + ":" + dts.substring(i);
	    		}
	    		pair.setValue(objectFactory.createKeyValuePairTypeValue(dts));
	    		transactionLookupResponsePayload.getCurrentValues().getCurrentValue().add(pair);
	    		
	    		if ((tran.isBillPayment()) || (tran.isDss())) {
	    			
	    			// If the transaction is a bill payment or a directed send, set the status as received.
	    			
					transactionLookupResponsePayload.setTransactionStatus(TransactionStatusType.RECVD);
				} else {
					
					// If the transaction is a regular send, set the status as available and can be canceled.
					
					transactionLookupResponsePayload.setTransactionStatus(TransactionStatusType.AVAIL);
	
					pair = new KeyValuePairType();
		    		pair.setInfoKey("isCancelAllowedFlag");
		    		pair.setValue(objectFactory.createKeyValuePairTypeValue("true"));
		    		transactionLookupResponsePayload.getCurrentValues().getCurrentValue().add(pair);
					
		    		pair = new KeyValuePairType();
		    		pair.setInfoKey("isRefundAllowedFlag");
		    		pair.setValue(objectFactory.createKeyValuePairTypeValue("false"));
		    		transactionLookupResponsePayload.getCurrentValues().getCurrentValue().add(pair);
				}
			}
			
			setCachedResponse(TRANSACTION_LOOKUP_RESPONSE, transactionLookupResponse, referenceNumber);

		} else if (completeSessionRequest.getMgiSessionType().equals(SessionType.RCV)) {
			
			String checkType = UnitProfile.getInstance().get("MG_CHECKS", "MTC");
	        if (! checkType.equals("DG")) {
	        	payload.setAgentCheckAuthorizationNumber("12345");
			}

			TransactionLookupResponse transactionLookupResponse = (TransactionLookupResponse) getCachedResponse(TRANSACTION_LOOKUP_RESPONSE, referenceNumber);
			if (transactionLookupResponse != null) {
				transactionLookupResponse.getPayload().getValue().setTransactionStatus(TransactionStatusType.RECVD);
				setCachedResponse(TRANSACTION_LOOKUP_RESPONSE, transactionLookupResponse, referenceNumber);
			}

		} else if (completeSessionRequest.getMgiSessionType().equals(SessionType.AMD)) {
			TransactionLookupResponse transactionLookupResponse = (TransactionLookupResponse) getCachedResponse(TRANSACTION_LOOKUP_RESPONSE, referenceNumber);
			if (transactionLookupResponse != null) {
				List<KeyValuePairType> lookupCurrentValues = transactionLookupResponse.getPayload().getValue().getCurrentValues().getCurrentValue();
				for (KeyValuePairType pair : new ArrayList<KeyValuePairType>(lookupCurrentValues)) {
					if ((pair.getInfoKey().equals(FieldKey.RECEIVER_FIRSTNAME_KEY.getInfoKey())) || (pair.getInfoKey().equals(FieldKey.RECEIVER_MIDDLENAME_KEY.getInfoKey())) || (pair.getInfoKey().equals(FieldKey.RECEIVER_LASTNAME_KEY.getInfoKey())) || (pair.getInfoKey().equals(FieldKey.RECEIVER_LASTNAME2_KEY.getInfoKey())) || (pair.getInfoKey().equals(FieldKey.RECEIVER_NAMESUFFIX_KEY.getInfoKey())) || (pair.getInfoKey().equals(FieldKey.RECEIVER_NAMESUFFIXOTHER_KEY.getInfoKey()))) {
						lookupCurrentValues.remove(pair);
					}
				}
				
				AmendValidationRequest amendValidationRequest = (AmendValidationRequest) validationRequest;				
				
				List<KeyValuePairType> validationCurrentValues = amendValidationRequest.getFieldValues().getFieldValue();
				
				for (KeyValuePairType pair : validationCurrentValues) {
					if ((pair.getInfoKey().equals(FieldKey.RECEIVER_FIRSTNAME_KEY.getInfoKey())) || (pair.getInfoKey().equals(FieldKey.RECEIVER_MIDDLENAME_KEY.getInfoKey())) || (pair.getInfoKey().equals(FieldKey.RECEIVER_LASTNAME_KEY.getInfoKey())) || (pair.getInfoKey().equals(FieldKey.RECEIVER_LASTNAME2_KEY.getInfoKey())) || (pair.getInfoKey().equals(FieldKey.RECEIVER_NAMESUFFIX_KEY.getInfoKey())) || (pair.getInfoKey().equals(FieldKey.RECEIVER_NAMESUFFIXOTHER_KEY.getInfoKey()))) {
						lookupCurrentValues.add(pair);
					}
				}
				
				setCachedResponse(TRANSACTION_LOOKUP_RESPONSE, transactionLookupResponse, referenceNumber);
			}

		} else if (completeSessionRequest.getMgiSessionType().equals(SessionType.SREV)) {
			
			// Change the status of the transaction to cancelled.
			
			TransactionLookupResponse transactionLookupResponse = (TransactionLookupResponse) getCachedResponse(TRANSACTION_LOOKUP_RESPONSE, referenceNumber);
			if (transactionLookupResponse != null) {
				transactionLookupResponse.getPayload().getValue().setTransactionStatus(TransactionStatusType.CANCL);
				setCachedResponse(TRANSACTION_LOOKUP_RESPONSE, transactionLookupResponse, referenceNumber);
			}
		} else if (completeSessionRequest.getMgiSessionType().equals(SessionType.RREV)) {
			
			TransactionLookupResponse transactionLookupResponse = (TransactionLookupResponse) getCachedResponse(TRANSACTION_LOOKUP_RESPONSE, referenceNumber);
			if (transactionLookupResponse != null) {
				transactionLookupResponse.getPayload().getValue().setTransactionStatus(TransactionStatusType.AVAIL);
				setCachedResponse(TRANSACTION_LOOKUP_RESPONSE, transactionLookupResponse, referenceNumber);
			}
		}
		return response;
	}

	private static BigDecimal randomNumber(double low, double high, int precision, boolean round) {
		SecureRandom sr = new SecureRandom();
		double value = ((high - low) * sr.nextDouble()) + low;
		if (round) {
			value = Math.round(value);
		}
		value = value * Math.pow(10, precision);
		BigDecimal decimal = BigDecimal.valueOf(((long) value), precision);
		return decimal;
	}
	
	private static TransactionLookupResponse transactionLookup(TransactionLookupRequest transactionLookupRequest, boolean isInitial) throws SystemError_Exception {
		TransactionLookupResponse transactionLookupResponse = null;
		String purposeOfLookup = transactionLookupRequest.getPurposeOfLookup();
		String referenceNumber = transactionLookupRequest.getReferenceNumber();
		String receiveCurrency = CountryInfo.getAgentCountry().getBaseReceiveCurrency();

		if (referenceNumber != null) {
			if (isInitial && ((PRE_DEFINED_REFERENCE_NUMBERS.contains(referenceNumber)))) {
				setCachedResponse(TRANSACTION_LOOKUP_RESPONSE, null, referenceNumber);
			}

			// Check if the transaction is in the cached responses.
		
			if ((purposeOfLookup.equals(DWValues.TRX_LOOKUP_RECV)) || (purposeOfLookup.equals(DWValues.TRX_LOOKUP_RECV_COMP)) || (purposeOfLookup.equals(DWValues.TRX_LOOKUP_STATUS)) || (purposeOfLookup.equals(DWValues.TRX_LOOKUP_SENDREV)) ||(purposeOfLookup.equals(DWValues.TRX_LOOKUP_RECEIVE_REV)) || (purposeOfLookup.equals(DWValues.TRX_LOOKUP_AMEND))) {
				transactionLookupResponse = (TransactionLookupResponse) getCachedResponse(TRANSACTION_LOOKUP_RESPONSE, transactionLookupRequest.getReferenceNumber());
				
				if (transactionLookupResponse != null) {
					TransactionLookupResponse.Payload payload = transactionLookupResponse.getPayload().getValue();
					ReceiveAmountInfo receiveAmounts = payload.getReceiveAmounts();
					if (! receiveAmounts.getReceiveCurrency().equals(receiveCurrency)) {
						addRedirectInfo(payload, payload.getSendAmounts(), receiveAmounts, receiveCurrency);
						
						payload.getRedirectInfo().setOriginalReceiveAmount(receiveAmounts.getReceiveAmount());
						payload.getRedirectInfo().setOriginalReceiveCurrency(receiveAmounts.getReceiveCurrency());

						receiveAmounts.setReceiveAmount(payload.getRedirectInfo().getNewReceiveAmount());
						receiveAmounts.setReceiveCurrency(receiveCurrency);
					}
				}
			}
		}
		
		// If a response could not be found in the cached responses, check if a canned response exists.
		
		if (transactionLookupResponse == null) {
				
			if ((! purposeOfLookup.equals(DWValues.TRX_LOOKUP_SEND_COMP)) && (! purposeOfLookup.equals(DWValues.TRX_LOOKUP_BP_COMP))) {
				purposeOfLookup = "RECEIVE";
			}
			
			if (PRE_DEFINED_REFERENCE_NUMBERS.contains(referenceNumber)) {
				XmlUtil<TransactionLookupResponse> xmlUtil2 = new XmlUtil<TransactionLookupResponse>(TransactionLookupResponse.class);
				String xPath = "//transactionLookupResponse" + "[@purposeOfLookup='" + purposeOfLookup + "']";
				List<TransactionLookupResponse> list = xmlUtil2.getInfo(AgentConnectTemplates.getTemplates(), xPath);
				
				if ((list != null) && (! list.isEmpty())) {
					transactionLookupResponse = list.get(0);
				}
			}
			
			if ((transactionLookupResponse != null) && (transactionLookupResponse.getPayload() != null)) {
				TransactionLookupResponse.Payload payload = transactionLookupResponse.getPayload().getValue();
				SendAmountInfo sendAmounts = payload.getSendAmounts();
				ReceiveAmountInfo receiveAmounts = payload.getReceiveAmounts();
			
				// Set the send currency.
			
				sendAmounts.setSendCurrency(receiveCurrency);

				// Set the receive currency.
			
				receiveAmounts.setPayoutCurrency(receiveCurrency);
				receiveAmounts.setReceiveCurrency(receiveCurrency);
				
				if ((purposeOfLookup.equals(DWValues.TRX_LOOKUP_SEND_COMP)) || (purposeOfLookup.equals(DWValues.TRX_LOOKUP_BP_COMP))) {
					referenceNumber = generateReferenceNumber();
					transactionLookupRequest.setReferenceNumber(referenceNumber);
					setCachedResponse(TRANSACTION_LOOKUP_RESPONSE, transactionLookupResponse, referenceNumber);
					
					sendAmounts.setSendAmount(randomNumber(300, 700, 2, true));
					sendAmounts.setTotalAmountToCollect(sendAmounts.getSendAmount().add(sendAmounts.getTotalSendFees()));
					
					receiveAmounts.setReceiveAmount(sendAmounts.getSendAmount());
					receiveAmounts.setPayoutCurrency(sendAmounts.getSendCurrency());
					receiveAmounts.setReceiveCurrency(sendAmounts.getSendCurrency());
				
				} else if (referenceNumber != null) {
					if (PRE_DEFINED_REFERENCE_NUMBERS.contains(referenceNumber)) {
						
						// Set the send and receive amount.
						
						BigDecimal amount = randomNumber(200, 800, 2, true);
						sendAmounts.setSendAmount(amount);
						receiveAmounts.setReceiveAmount(amount);
						
						// Set the send currency.
						
						sendAmounts.setSendCurrency(receiveCurrency);
						
						if (referenceNumber.equals("12345678")) {
							payload.setTransactionStatus(TransactionStatusType.AVAIL);
							ObjectFactory objectFactory = new ObjectFactory();
					    	KeyValuePairType kvp = new KeyValuePairType();
					    	kvp.setInfoKey("isCancelAllowedFlag");
					    	kvp.setValue(objectFactory.createKeyValuePairTypeValue("true"));
							payload.getCurrentValues().getCurrentValue().add(kvp);
					    	kvp = new KeyValuePairType();
					    	kvp.setInfoKey("isRefundAllowedFlag");
					    	kvp.setValue(objectFactory.createKeyValuePairTypeValue("false"));
							payload.getCurrentValues().getCurrentValue().add(kvp);
						
						} else if (referenceNumber.equals("23456789")) {
							payload.setTransactionStatus(TransactionStatusType.AVAIL);
							ObjectFactory objectFactory = new ObjectFactory();
							KeyValuePairType kvp = new KeyValuePairType();
					    	kvp.setInfoKey("isCancelAllowedFlag");
					    	kvp.setValue(objectFactory.createKeyValuePairTypeValue("false"));
							payload.getCurrentValues().getCurrentValue().add(kvp);
					    	kvp = new KeyValuePairType();
					    	kvp.setInfoKey("isRefundAllowedFlag");
					    	kvp.setValue(objectFactory.createKeyValuePairTypeValue("true"));
							payload.getCurrentValues().getCurrentValue().add(kvp);
						
						} else if (referenceNumber.equals("34567890")) {
							payload.setTransactionStatus(TransactionStatusType.AVAIL);
							ObjectFactory objectFactory = new ObjectFactory();
					    	KeyValuePairType kvp = new KeyValuePairType();
					    	kvp.setInfoKey("isCancelAllowedFlag");
					    	kvp.setValue(objectFactory.createKeyValuePairTypeValue("true"));
							payload.getCurrentValues().getCurrentValue().add(kvp);
					    	kvp = new KeyValuePairType();
					    	kvp.setInfoKey("isRefundAllowedFlag");
					    	kvp.setValue(objectFactory.createKeyValuePairTypeValue("false"));
							payload.getCurrentValues().getCurrentValue().add(kvp);
							
							// Change the original receive currency.
							
							String sendCurrency = receiveCurrency.equals("USD") ? "EUR" : "USD";
							sendAmounts.setSendCurrency(sendCurrency);
							
							addRedirectInfo(payload, sendAmounts, receiveAmounts, receiveCurrency);
							
						} else if (referenceNumber.equals("87654321")) {
							payload.setTransactionStatus(TransactionStatusType.RECVD);
						
						} else if (referenceNumber.equals("88888888")) {
							payload.setTransactionStatus(TransactionStatusType.CANCL);
						
						} else if (referenceNumber.equals("99999999")) {
							payload.setTransactionStatus(TransactionStatusType.REFND);
						}
					} else {
						Debug.println("transactionLookup: WARNING: transactionLookupResponse or its Payload was not defined");
					}
					setCachedResponse(TRANSACTION_LOOKUP_RESPONSE, transactionLookupResponse, referenceNumber);
				}
			} else {
				ObjectFactory objectFactory = new ObjectFactory();
				transactionLookupResponse = objectFactory.createTransactionLookupResponse();
				transactionLookupResponse.setPayload(objectFactory.createTransactionLookupResponsePayload(objectFactory.createTransactionLookupResponsePayload()));
				
				BusinessError be = new BusinessError();
				be.setErrorCode(BigInteger.valueOf(627L));
				be.setMessageShort("Transaction Not Found");
				be.setMessage("TRAINING MODE HINT: TRY 12345678 (Ready to receive and cancel), 23456789 (Ready to receive and refund), 34567890 (Redirected receive), 87654321 (Previously received), 88888888 (Canceled), 99999999 (Refunded)");
				Errors errors = transactionLookupResponse.getErrors();
				if (errors == null) {
					errors = new Errors();
					transactionLookupResponse.setErrors(errors);
				}
				errors.getError().add(be);
			}
		}
		
		// If the transactionLookup response is for a form free transaction, generated a reference number for the transaction and save the response in the cached responses.
		
		if ((purposeOfLookup.equals(DWValues.TRX_LOOKUP_SEND_COMP)) || (purposeOfLookup.equals(DWValues.TRX_LOOKUP_BP_COMP))) {
			referenceNumber = generateReferenceNumber();
			transactionLookupRequest.setReferenceNumber(referenceNumber);
			
			setCachedResponse(TRANSACTION_LOOKUP_RESPONSE, transactionLookupResponse, referenceNumber);
		}
		
		if ((transactionLookupResponse != null) && (transactionLookupResponse.getPayload() != null)) {
			TransactionLookupResponse.Payload payload = transactionLookupResponse.getPayload().getValue();
			payload.setTimeStamp(TimeUtility.toXmlCalendar(TimeUtility.currentTime()));
		} else {
			Debug.println("transactionLookup: WARNING: Payload was not defined");
		}
		
		return transactionLookupResponse;
	}
	
	private static void addRedirectInfo(TransactionLookupResponse.Payload payload, SendAmountInfo sendAmounts, ReceiveAmountInfo receiveAmounts, String receiveCurrency) {
		RedirectInfo redirectInfo = new RedirectInfo();
		payload.setRedirectInfo(redirectInfo);
		redirectInfo.setRedirectType(RedirectInfoRedirectType.CURRENCY_REDIRECT);
		redirectInfo.setNewReceiveCurrency(receiveCurrency);
		redirectInfo.setOriginalReceiveAmount(sendAmounts.getSendAmount());
		redirectInfo.setOriginalReceiveCurrency(sendAmounts.getSendCurrency());
		
		// Set exchange rate.
		
		BigDecimal exchangeRate = randomNumber(0.5, 2.0, 6, false);
		redirectInfo.setNewExchangeRate(exchangeRate);
		
		// Set new receive amount.
		
		double receiveAmount = sendAmounts.getSendAmount().doubleValue() * exchangeRate.doubleValue(); 
		BigDecimal a = new BigDecimal(FORMAT2.format(receiveAmount));
		redirectInfo.setNewReceiveAmount(a);
		receiveAmounts.setReceiveAmount(a);
	}
	
	private static void fakeError(String code, String s, String detail) throws SystemError_Exception {
		SystemError faultInfo = new SystemError();
		faultInfo.setErrorCode(new BigInteger(code));
		faultInfo.setDetailString(detail);
		faultInfo.setErrorString(s);
		throw new SystemError_Exception(s, faultInfo);
	}
	
	private static Object getCachedResponse(String key, String keySuffix) {
		String mapKey = key;
		if (keySuffix != null && !keySuffix.isEmpty()) {
			mapKey = key + "_" + keySuffix;
		}
		return transactionMap.get(mapKey);
	}

	private static void setCachedResponse(String key, Object obj, String keySuffix) {
		String mapKey = key;
		if (keySuffix != null && !keySuffix.isEmpty()) {
			mapKey = key + "_" + keySuffix;
		}
		transactionMap.put(mapKey, obj);
	}
}
