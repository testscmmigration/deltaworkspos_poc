package dw.comm;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.moneygram.agentconnect.BillerLookupInfo;
import com.moneygram.agentconnect.KeyValuePairType;
import com.moneygram.agentconnect.ReceiverLookupInfo;
import com.moneygram.agentconnect.SenderLookupInfo;

import dw.framework.FieldKey;
import dw.model.adapters.CompanyInfo;
import dw.model.adapters.CustomerInfo;
import dw.model.adapters.Data.DataValue;
import dw.model.adapters.ReceiverInfo;
import dw.model.adapters.SenderInfo;

public class MessageFacadeHelper {

	public List<CustomerInfo> convertToCustomerInfo(List<SenderLookupInfo> senderInfos) {
		
		Set<SenderInfo> senders = new HashSet<SenderInfo>();
		
		List<CustomerInfo> customerInfos = new ArrayList<CustomerInfo>();
		if (senderInfos != null) {
			for (SenderLookupInfo sendInfo : senderInfos) {
				
				SenderInfo si = new SenderInfo(sendInfo.getCurrentValues().getCurrentValue());
				
				if ((sendInfo.getReceiverInfos() != null) && (! sendInfo.getReceiverInfos().getReceiverInfo().isEmpty())) {
					for (ReceiverLookupInfo receiverInfo : sendInfo.getReceiverInfos().getReceiverInfo()) {
						CustomerInfo customerInfo = new CustomerInfo();
						
						ReceiverInfo ri = new ReceiverInfo();
						for (KeyValuePairType pair : receiverInfo.getCurrentValues().getCurrentValue()) {
							if ((pair.getInfoKey() != null) && (pair.getValue() != null)) {
								FieldKey key = FieldKey.key(pair.getInfoKey());
								DataValue value = DataValue.value(pair.getValue().getValue());
								ri.getData().put(key, value);
							}
						}
						
						customerInfo.setSender(si);
						customerInfo.setReceiver(ri);
						customerInfo.setCompanyInfo(new CompanyInfo());
						customerInfos.add(customerInfo);
					}
				}

				if ((sendInfo.getBillerInfos() != null) && (! sendInfo.getBillerInfos().getBillerInfo().isEmpty())) {

					for (BillerLookupInfo billerInfo : sendInfo.getBillerInfos().getBillerInfo()) {
						CustomerInfo customerInfo = new CustomerInfo();
						
						CompanyInfo ci = new CompanyInfo();
						for (KeyValuePairType pair : billerInfo.getCurrentValues().getCurrentValue()) {
							FieldKey key = FieldKey.key(pair.getInfoKey());
							DataValue value = DataValue.value(pair.getValue().getValue());
							ci.getData().put(key, value);
						}

						customerInfo.setSender(si);
						customerInfo.setReceiver(new ReceiverInfo());
						customerInfo.setCompanyInfo(ci);
						customerInfos.add(customerInfo);
					}
				}

				CustomerInfo customerInfo = new CustomerInfo();
				
				if ((si.getFirstName() != null) || (si.getLastName() != null)) {
					boolean add = senders.add(si);
					if (add) {
						customerInfo.setSender(si);
						customerInfo.setReceiver(new ReceiverInfo());
						customerInfo.setCompanyInfo(new CompanyInfo());
						customerInfos.add(customerInfo);
					}
				}
			}
		}
		return customerInfos;
	}
}
