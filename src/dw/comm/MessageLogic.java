package dw.comm;

/** 
 * Callers of MessageFacade will create an instance of this class to specify
 * logic that (potentially) asks for data from the middleware. MessageFacade
 * sets up a thread to run this method.
 */
public interface MessageLogic{
    public Object run();
}
