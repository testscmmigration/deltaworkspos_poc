/*
 * AgentConnectAuthenticator.java
 * 
 * $Revision$
 *
 * Copyright (c) 2004 MoneyGram International
 */
package dw.comm;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

/**
 * Provides proxy username and password to Java network classes that request it. 
 */
public class AgentConnectAuthenticator extends Authenticator {
    
    private static PasswordAuthentication auth;
    
    @Override
	protected PasswordAuthentication getPasswordAuthentication() {
        return auth;
    }

    public static void setPasswordAuthentication(PasswordAuthentication pa) {
        auth = pa;
    }
    
}
