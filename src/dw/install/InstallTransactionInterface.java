package dw.install;

import dw.framework.ClientTransaction;
import dw.framework.PageNameInterface;
import dw.framework.TransactionInterface;

/**
 * TransactionInterface implementation for the Installation functionality.
 * @author Chris Bartling
 */
public abstract class InstallTransactionInterface
                                                extends TransactionInterface {
	private static final long serialVersionUID = 1L;

	protected InstallTransaction transaction;
	
	// Used for testing purposes
   // private String version = null;
   // private String profileFileName = "profile.xml";

    // exit codes to send back to indicate app exit status
    public static final int NORMAL_EXIT = 0;
    public static final int ERROR_EXIT = 1;
    public static final int RESTART_EXIT = 99;

    // status severity codes
    //public static final int INFORMATION = 0;
    //public static final int WARNING = 1;
    //public static final int ERROR = 2;

    /**
     * Constructor.
     */
    public InstallTransactionInterface(String fileName,
                                 InstallTransaction transaction,
                                 PageNameInterface naming, boolean sidebar) {
        super(fileName, naming, sidebar);
        this.transaction = transaction;
    }

    @Override
	public ClientTransaction getTransaction() {
        return transaction;
    }
}
