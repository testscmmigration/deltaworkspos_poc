package dw.install;

import java.awt.event.KeyEvent;

import javax.swing.JButton;

import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNameInterface;

/**
 * The wizard interface for initial setup.
 * @author Christopher Bartling
 */
public class InstallWizard extends InstallTransactionInterface {
	private static final long serialVersionUID = 1L;

    public InstallWizard(InstallTransaction transaction,
                         PageNameInterface naming) {
        super("install/AgentSetupWizardPanel.xml", transaction, naming, false);	
        createPages();
    }

    /**
     * Add custom page traversal logic here. If no custom logic needed, just
     * call super.exit(direction) to do default traversal.
     */
    @Override
	public void exit(int direction) {
        String currentPage = cardLayout.getVisiblePageName();
        String nextPage = null;
        
        if (direction == PageExitListener.DONOTHING) {
            return;
        }
        
        if (direction != PageExitListener.NEXT) {
            // default case
            super.exit(direction);
            return;
        }
        if (currentPage.equals("installwpage1b")) { 
            if (transaction.getConnectionType() == InstallTransaction.NETWORK) {
                nextPage = "installwpage2";	
            }
            else if (transaction.getConnectionType() == InstallTransaction.NETWORK_WITH_PROXY) {
                nextPage = "installwpage1d"; 
            }
        }
    /*    else if (currentPage.equals("installwpage1c")) {
            nextPage = "installwpage2";    
        }
     */
        else if (currentPage.equals("installwpage1c")) {
        	if (transaction.getModifyScript()) {
                nextPage = "installwpage1e";    
        	}
        	else{
                nextPage = "installwpage2"; 
        	}
        }
        else if (currentPage.equals("installwpage1e")) {
            nextPage = "installwpage2";    
        }
        

        if (nextPage != null) {
            showPage(nextPage, direction);
            return;
        }
        else {
            super.exit(direction);
        }
    }

    @Override
	protected void createPages() {
        addPage(InstallWizardPage1.class, transaction, "installwpage1", "INW05", buttons);	 
        addPage(InstallWizardPage1b.class, transaction, "installwpage1b", "INW10", buttons);	 
        addPage(InstallWizardPage1c.class, transaction, "installwpage1c", "INW15", buttons);	 
        addPage(InstallWizardPage1e.class, transaction, "installwpage1e", "INW17", buttons);	 
        addPage(InstallWizardPage1d.class, transaction, "installwpage1d", "INW16", buttons);     
        addPage(InstallWizardPage2.class, transaction, "installwpage2", "INW20", buttons);	 
        addPage(InstallWizardPage3.class, transaction, "installwpage3", "INW25", buttons);	 
    }

    /**
     * Add the single keystroke mappings to the buttons. Use the function keys
     *   for back, next, cancel, etc.
     */
    private void addKeyMappings() {
        buttons.addKeyMapping("back", KeyEvent.VK_F11, 0);	
        buttons.addKeyMapping("next", KeyEvent.VK_F12, 0);	
        buttons.addKeyMapping("cancel", KeyEvent.VK_ESCAPE, 0);	
    }

    @Override
	public void start() {
        super.start();
        addKeyMappings();
    }

    /**
        Create the wizard buttons.
        @result A PageFlowButtons object containing the page flow buttons 
        for use in the wizard.
     */
    @Override
	public PageFlowButtons createButtons() {
        javax.swing.JButton[] buttons = 
            { 
                (JButton) getComponent("backButton"),	
                (JButton) getComponent("nextButton"), 	
                (JButton) getComponent("cancelButton") 	
            };
        String[] names = { "back", "next", "cancel" };   
        return new PageFlowButtons(buttons, names, this);
    }
}
