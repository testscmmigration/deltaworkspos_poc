package dw.install;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;

/**
 * Page 1d of the agent setup wizard. Prompts for proxy settings.
 *
 * @author Geoff Atkin
 */
public class InstallWizardPage1d extends InstallFlowPage {
	private static final long serialVersionUID = 1L;

	private JComboBox proxyCombo = null;
    private JTextField usernameField = null;
    private JTextField passwordField = null;
    private JTextField serverField = null;
    private JTextField portField = null;

    /**
     * Constructor.
     */
    public InstallWizardPage1d(InstallTransaction tran,
                       String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "install/AgentSetupPage1d.xml", name, pageCode, buttons);	
    }

    /**
     * Called before the enclosing container shows this page.
     */
    @Override
	public void start(int direction) {
        collectComponents();
        
        // copy transaction values to fields
        proxyCombo.setSelectedIndex(transaction.getProxyType());
        serverField.setText(transaction.getProxyServer());
        portField.setText(transaction.getProxyPort());
        usernameField.setText(transaction.getProxyUsername());
        passwordField.setText(transaction.getProxyPassword());
        
        flowButtons.reset();
    }

    /**
     * Called before the enclosing container hides this page.
     */
    @Override
	public void finish(int direction) {
        //copy field values to transaction
        transaction.setProxyType(proxyCombo.getSelectedIndex());
        transaction.setProxyServer(serverField.getText());
        transaction.setProxyPort(portField.getText());
        transaction.setProxyUsername(usernameField.getText());
        transaction.setProxyPassword(passwordField.getText());

        // disallow NEXT if required fields not filled in
        if (direction == PageExitListener.NEXT) {
           JTextField[] fields = { serverField, portField };
           if (! validateFields(fields))
               return;
        }
        
        PageNotification.notifyExitListeners(this, direction);
    }

    /**
     * Collects all the XML defined components.
     */
    private void collectComponents() {
        proxyCombo = (JComboBox) getComponent("proxyCombo");
        usernameField = (JTextField) getComponent("usernameField");
        passwordField = (JTextField) getComponent("passwordField");
        serverField = (JTextField) getComponent("serverField");
        portField = (JTextField) getComponent("portField");
        
        if (proxyCombo.getModel().getSize() == 0) {
            proxyCombo.setModel(new DefaultComboBoxModel(
                InstallTransaction.PROXY_TYPES));
            proxyCombo.setEditable(false);
        }
    }
}
