package dw.install;

import dw.framework.FlowPage;
import dw.framework.PageFlowButtons;

/**
 * Abstract flow page for the installation GUI.
 * @author Christopher Bartling
 */
public abstract class InstallFlowPage extends FlowPage {
	private static final long serialVersionUID = 1L;
    
    protected InstallTransaction transaction;

    @Override
	public void commComplete(int commTag, Object returnValue) {}

    public InstallFlowPage(InstallTransaction tran, String fileName,
                                       String name, String pageCode,
                                       PageFlowButtons buttons) {
        super(fileName, name, pageCode, buttons);
        transaction = tran;
    }
}
