package dw.install;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTextField;

import dw.dwgui.CityProviderNumberListComboBox;
import dw.dwgui.MultiListComboBox;
import dw.dwgui.StateListComboBox;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.CountryInfo.Country;
import dw.model.adapters.CountryInfo.CountrySubdivision;
import dw.profile.UnitProfile;
import dw.utility.AccessNumbers;
import dw.utility.CityProviderNumberInfo;
import dw.utility.Debug;

/**
 * Page 1c of the agent setup wizard. Prompts for dialup options.
 *
 * @author Rob Campbell
 * @author Geoff Atkin
 */
public class InstallWizardPage1c extends InstallFlowPage implements ActionListener {
	private static final long serialVersionUID = 1L;

	private JTextField hostSpecialDialField = null;
    private JTextField phoneNumberField = null;
    private JTextField usernameField = null;
    private JTextField passwordField = null;
    private MultiListComboBox<Country> countryBox;
    private StateListComboBox<CountrySubdivision> stateBox;
    private CityProviderNumberListComboBox cityProviderBox;
    private JCheckBox passThroughLogonBox;
    private JPanel passThroughUsernamePanel;
    private JPanel passThroughPasswordPanel;

    private JTextField passThroughUsernameField;
    private JTextField passThroughPasswordField;

    private JPanel modifyScriptPanel;
    private JCheckBox modifyScriptBox;
    
    private JPanel modemPanel;
    private MultiListComboBox<String> modemBox;


    /**
     * Constructor.
     */
    public InstallWizardPage1c(InstallTransaction tran,
            String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "install/AgentSetupPage1c.xml", name, pageCode, buttons);	
        collectComponents();

    }

    /**
     * Called before the enclosing container shows this page.
     */
    @Override
	public void start(int direction) {
        flowButtons.reset();
        
        hostSpecialDialField.requestFocus();
        
        ActionListener countryChangeListener = new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
            	AccessNumbers.updateStateList((Country) countryBox.getSelectedItem(), 
            			stateBox);
				AccessNumbers.updatecityProviderPhoneNumber((Country) countryBox.getSelectedItem(), (CountrySubdivision) stateBox.getSelectedItem(), 
						cityProviderBox);
				CityProviderNumberInfo cityProviderInfo = 
					(CityProviderNumberInfo)cityProviderBox.getSelectedItem();
				phoneNumberField.setText(cityProviderInfo.getNumber());				
            }};
        addActionListener("countryBox", countryChangeListener);   
        
        ActionListener stateChangeListener = new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
            	try {
				AccessNumbers.updatecityProviderPhoneNumber((Country) countryBox.getSelectedItem(), (CountrySubdivision) stateBox.getSelectedItem(), cityProviderBox);
				CityProviderNumberInfo cityProviderInfo = 
					(CityProviderNumberInfo)cityProviderBox.getSelectedItem();
				phoneNumberField.setText(cityProviderInfo.getNumber());				
            	}
            	catch(Exception ex) {
            		Debug.printStackTrace(ex);
            	}
            }};
        addActionListener("stateBox", stateChangeListener);   
        
        // code to handle the passthrough check
    	cityProviderBox.addActionListener(new ActionListener(){
    		@Override
			public void actionPerformed(ActionEvent ae){
        		CityProviderNumberInfo cityProviderInfo = 
        			(CityProviderNumberInfo)(
        				(CityProviderNumberListComboBox)ae.getSource()).getSelectedItem();
        		phoneNumberField.setText(cityProviderInfo.getNumber());
        		
    			if(cityProviderInfo.getScript().equalsIgnoreCase("None")){
    				passThroughLogonBox.setSelected(false);
    				if(passThroughLogonBox.isSelected()){
    					UnitProfile.getInstance().set("PRIMARY_DIALUP_SCRIPT_NAME",
    						"MCI", "S", false);
    				}else{
    					UnitProfile.getInstance().set("PRIMARY_DIALUP_SCRIPT_NAME",
    						"", "S", false);
    				}
    			}else{
    				passThroughLogonBox.setSelected(true);
					String str = cityProviderInfo.getScript();
					UnitProfile.getInstance().set("PRIMARY_DIALUP_SCRIPT_NAME",
							str.trim(), "S", false);
    			}
                setComponentEnabled(passThroughUsernamePanel, passThroughLogonBox.isSelected(), true);
                setComponentEnabled(passThroughPasswordPanel, passThroughLogonBox.isSelected(), true);
                setComponentEnabled(modifyScriptPanel, passThroughLogonBox.isSelected(), true);
    		}
    	});

		CityProviderNumberInfo cityProviderInfo = 
			(CityProviderNumberInfo)cityProviderBox.getSelectedItem();
		phoneNumberField.setText(cityProviderInfo.getNumber());
    	
    	passThroughLogonBox.addItemListener(new ItemListener() {
            @Override
			public void itemStateChanged(ItemEvent e) {
            	CityProviderNumberInfo cityProviderInfo = 
            	(CityProviderNumberInfo) cityProviderBox.getSelectedItem();
               	if(passThroughLogonBox.isSelected()){
               		if(cityProviderInfo.getScript().equalsIgnoreCase("None")){
                   		UnitProfile.getInstance().set("PRIMARY_DIALUP_SCRIPT_NAME",
                   			"MCI", "S", false);
               		}else{
               			UnitProfile.getInstance().set("PRIMARY_DIALUP_SCRIPT_NAME", 
               				cityProviderInfo.getScript(), "S", false);
               		}
               	}else{
               		if(cityProviderInfo.getScript().equalsIgnoreCase("None")){
                   		UnitProfile.getInstance().set("PRIMARY_DIALUP_SCRIPT_NAME",
                   			"", "S", false);
               		}
               	}
           	}
        });
        // ends here
        setComponentEnabled(passThroughUsernamePanel, passThroughLogonBox.isSelected(), true);
        setComponentEnabled(passThroughPasswordPanel, passThroughLogonBox.isSelected(), true);
        setComponentEnabled(modifyScriptPanel, passThroughLogonBox.isSelected(), true);

        ActionListener passThroughChangeListener = new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
                setComponentEnabled(passThroughUsernamePanel, passThroughLogonBox.isSelected(), true);
                setComponentEnabled(passThroughPasswordPanel, passThroughLogonBox.isSelected(), true);
                setComponentEnabled(modifyScriptPanel, passThroughLogonBox.isSelected(), true);
                // clear the pass through information if the checkbox isn't checked
                if (!passThroughLogonBox.isSelected()){
                    passThroughUsernameField.setText("");
                    passThroughPasswordField.setText("");
                    modifyScriptBox.setSelected(false);
                }
            }};
        addActionListener("passThroughLogonBox", passThroughChangeListener);   
        
        if (transaction.isCanDisplayModem()){
        	modemPanel.setVisible(true);
        	//modemBox.setEnabled(true);
        	modemBox.addList(transaction.getModemList(),"modemBox");
        } else 
        	modemPanel.setVisible(false);

    }

    
    
    /**
     * Called before the enclosing container hides this page.
     */
    @Override
	public void finish(int direction) {
        transaction.setUsingAlternateInfo(true);
        transaction.setHostSpecialDialParams(hostSpecialDialField.getText());
        transaction.setAlternatePhoneNumber(phoneNumberField.getText());
        transaction.setAlternateUsername(usernameField.getText());
        transaction.setAlternatePassword(passwordField.getText());
        transaction.setPassThroughLogin(passThroughLogonBox.isSelected());
        transaction.setPassThroughUserName(passThroughUsernameField.getText());
        transaction.setPassThroughPassword(passThroughPasswordField.getText());
        transaction.setModifyScript(modifyScriptBox.isSelected());
        transaction.setProviderAccessCountry("" + countryBox.getSelectedItem());
        transaction.setProviderAccessState("" + stateBox.getSelectedItem());
        transaction.setProviderAccessSelection("" 
        		+ cityProviderBox.getSelectedItem());
        if (transaction.isCanDisplayModem())
        	transaction.setModemName(modemBox.getSelectedItem().toString());
        
        if (direction == PageExitListener.NEXT) {
           JTextField[] fields = {phoneNumberField, usernameField, passwordField};
           if (transaction.isUsingAlternateInfo() && ! validateFields(fields))
               return;
        }

        PageNotification.notifyExitListeners(this, direction);
    }

    /**
     * Collects all the XML defined components.
     */
    private void collectComponents() {

        // collect the component references
        hostSpecialDialField = (JTextField) getComponent("hostSpecialDialField");
        phoneNumberField = (JTextField) getComponent("phoneNumberField");	
        usernameField = (JTextField) getComponent("usernameField");	
        passwordField = (JTextField) getComponent("passwordField");	
        countryBox = (MultiListComboBox<Country>) getComponent("countryBox");    
        modemPanel = (JPanel) getComponent ("modemPanel");
        modemBox = (MultiListComboBox<String>) getComponent ("modemBox");
        // change to get country, state, provider from AccessNumbers.xml 
        // instead of CodeTables
		countryBox.addList(CountryInfo.getAccessCountryList(), "countries");
		stateBox = (StateListComboBox<CountrySubdivision>) getComponent("stateBox");
		AccessNumbers.populateStateList(stateBox);		
		//AccessNumbers.updateStateList(countryBox.getSelectedItem(), stateBox);
		
        cityProviderBox = (CityProviderNumberListComboBox) getComponent("cityProviderBox");
		AccessNumbers.updatecityProviderPhoneNumber((Country) countryBox.getSelectedItem(), (CountrySubdivision) stateBox.getSelectedItem(), cityProviderBox);
		// change ends here
        passThroughLogonBox = (JCheckBox) getComponent("passThroughLogonBox"); 
        passThroughUsernamePanel = (JPanel) getComponent("passThroughUsernamePanel"); 
        passThroughUsernameField = (JTextField) getComponent("passThroughUsernameField");   
        passThroughPasswordPanel = (JPanel) getComponent("passThroughPasswordPanel"); 
        passThroughPasswordField = (JTextField) getComponent("passThroughPasswordField");   
        modifyScriptPanel = (JPanel) getComponent("modifyScriptPanel"); 
        modifyScriptBox = (JCheckBox) getComponent("modifyScriptBox"); 

    }

    /* (non-Javadoc)
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
	public void actionPerformed(ActionEvent e) {
        // Do nothing
    }
}
