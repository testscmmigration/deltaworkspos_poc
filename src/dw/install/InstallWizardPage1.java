package dw.install;

import java.io.File;

import javax.swing.JLabel;

import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.profile.UnitProfile;
import dw.utility.Debug;

/**
 * Page 1 of the agemt setup wizard.
 * @author Christopher Bartling
 */
public class InstallWizardPage1 extends InstallFlowPage {
	private static final long serialVersionUID = 1L;
	
    /**
     * Constructor.
     */
    public InstallWizardPage1(InstallTransaction tran,
                       String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "install/AgentSetupPage1.xml", name, pageCode, buttons);	
    }

    /**
     * Called before the enclosing container shows this page.
     */
    @Override
	public void start(int direction) {
        flowButtons.reset();
        flowButtons.getButton("next").requestFocus();	
        flowButtons.setEnabled("back", false);	
        JLabel versionLabel = (JLabel) getComponent("versionLabel");	
        versionLabel.setText(Messages.getString("InstallWizardPage1.1924") + UnitProfile.getInstance().getVersionNumber(true)); 
    }    

	/**
     * Called before the enclosing container hides this page.
     */
    @Override
	public void finish(int direction) {
        PageNotification.notifyExitListeners(this, direction);
    }

}
