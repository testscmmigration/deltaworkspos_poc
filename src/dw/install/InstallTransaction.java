package dw.install;

import java.util.Locale;

import dw.comm.MessageFacade;
import dw.comm.MessageFacadeError;
import dw.comm.MessageFacadeParam;
import dw.comm.MessageLogic;
import dw.framework.ClientTransaction;
import dw.io.State;
import dw.model.adapters.GetInitialSetupMsg;
import dw.profile.UnitProfile;
import dw.profile.UnitProfileInterface;
import dw.utility.DWValues;
import dw.utility.Debug;


/**
 * Encapsulates the POS install transaction functionality.
 * @author Christopher Bartling
 * @author Geoff Atkin
 */
public class InstallTransaction extends ClientTransaction {
	private static final long serialVersionUID = 1L;
    
    public static final int NETWORK = 0;
    public static final int NETWORK_WITH_PROXY = 1;
    public static final int DIALUP = 2;

    public static final String PROXY_TYPES[] = {"HTTP", "HTTPS", "SOCKS" };
            
    private String alternatePassword = "";
    private String alternatePhoneNumber = "";
    private String alternateUrl = "";        
    private String alternateUsername = "";
    private int connectionType = NETWORK;
    private String dispenserId;
    private String hostSpecialDialParams = "";
    private String dialupScriptName ="";
    private String origDialPassword;
    private String origDialPhoneNumber;
    private String origDialUsername;
    private String proxyPassword = "";
    private String proxyPort = "";
    private String proxyServer = "";
    private int proxyType = 0;
    private String proxyUsername = "";
    private String serviceTag;
    private boolean usingAlternateInfo = false;
    private int versionNumber;
    private boolean passThroughLogon = false;
    private String passThroughUserName = "";
    private String passThroughPassword = "";
    private String[] modemList;
    private boolean canDisplayModem = false;
    
    private String language = "";
    private String altLanguage = "";
    
    // Added for the new Timing Screen
    private boolean modifyScript = false;    
    private String delayAfterPhoneDial = "1";
    private String delayAfterPTUsername = "1";
    private String delayAfterPTPwEntry = "1";
    private String delayA = "1";
    private String delayB = "1";

    // Added for the profile
    private String providerAccessCountry = "";
    private String providerAccessState = "";
    private String providerAccessSelection = ""; 
    private String modemName = "";   
    /**
     * Create a new InstallTransaction.
     * Before calling this method, invoke UnitProfile.load().
     */
    public InstallTransaction(String tranName) {
        super(tranName, "");
        // Don't use the timeout or dispenser features.
        timeoutEnabled = false;
        dispenserNeeded = false;
        receiptPrinterNeeded = false;
        clear();
    }

    /**
     * Overrides ClientTransaction.clear() to make sure we have the current
     * values of some profile options. (Other transactions have this in a
     * separate method which is called from clear().)
     * 
     * The original dialup phone#, username, and password are preserved so we
     * can restore them if the user enters custom values, fails to get the
     * initial profile, hits the back button, and checks "use default config."
     * 
     * The current software version, dispenser unit number, and service tag
     * are preserved so we can restore them to the unit profile after the 
     * initial profile is downloaded from the middleware. 
     */
    @Override
	public void clear() {
        origDialPassword = UnitProfile.getInstance().get("PRIMARY_DUN_PASSWORD", "");
        origDialPhoneNumber = UnitProfile.getInstance().get("HOST_PRIMARY_PHONE", "");
        origDialUsername = UnitProfile.getInstance().get("PRIMARY_DUN_USER_ID", "");
        
        versionNumber = UnitProfile.getInstance().get("SOFTWARE_VERSION_ID", 0);
        dispenserId = UnitProfile.getInstance().getDispenserId();
        serviceTag = UnitProfile.getInstance().get("DEVICE_SERVICE_TAG", "");
        
        language = UnitProfile.getInstance().get("LANGUAGE", "");
        altLanguage = UnitProfile.getInstance().get("ALT_LANGUAGE", "");
        
//        Debug.println("Saving profile values: versionNumber=" + versionNumber
//            + " dispenserId=" + dispenserId + " serviceTag=" + serviceTag);

        Debug.println("Saving profile values: language=" + language
                + " altLanguage=" + altLanguage);       
        super.clear();
    }

    /** 
     * Writes SOFTWARE_VERSION_ID, dispenser UNIT_NUMBER, DEVICE_SERVICE_TAG,
     * AC_NETWORK_URL, AC_DIALUP_URL, POS_HOST_CONNECTION, HOST_SPECIAL_DIAL,
     * HOST_PRIMARY_PHONE, PRIMARY_DUN_USER_ID, PRIMARY_DUN_PASSWORD, PROXY*,
     * LANGUAGE, and ALT_LANGUAGE to the unit profile, using values entered
     * in this transaction or saved at the start of the transaction.
     */
    private void copySettingsToUnitProfile() {
        UnitProfile.getInstance().set("SOFTWARE_VERSION_ID", Integer.toString(versionNumber), 
            "V", false);
        UnitProfile.getInstance().setDispenserId(dispenserId);
        UnitProfile.getInstance().set("DEVICE_SERVICE_TAG", serviceTag, "V", false);
        
        if ((alternateUrl != null) && (! alternateUrl.equals(""))) {
            UnitProfile.getInstance().set("AC_NETWORK_URL", alternateUrl, "S", false);
            UnitProfile.getInstance().set("AC_DIALUP_URL", alternateUrl, "S", false);
        }
        
        if (! "".equals(altLanguage)) {
            UnitProfile.getInstance().set("ALT_LANGUAGE", altLanguage, "S", false);
        }

        if (! "".equals(language)) {
            UnitProfile.getInstance().set("LANGUAGE", language, "S", false);
        }

        if (connectionType == DIALUP) {
            UnitProfile.getInstance().set("POS_HOST_CONNECTION", "D", "S", false);
            UnitProfile.getInstance().set("HOST_SPECIAL_DIAL", hostSpecialDialParams, "S", false);
            //UnitProfile.getInstance().set("PRIMARY_DIALUP_SCRIPT_NAME", dialupScriptName, "S", false); 
            
            UnitProfile.getInstance().set("PRIMARY_SCRIPT_USER_NAME", passThroughUserName, "S", false); 
            UnitProfile.getInstance().set("PRIMARY_SCRIPT_PASSWORD", passThroughPassword, "S", false); 
            
            Debug.println("is this called : DIALUP");
            // Added for the profile values country, state, cityProvider
            UnitProfile.getInstance().set("PROVIDER_ACCESS_NUMBER_COUNTRY", 
            					providerAccessCountry, "P", false);
            UnitProfile.getInstance().set("PROVIDER_ACCESS_NUMBER_STATE", 
            					providerAccessState, "P", false);
            UnitProfile.getInstance().set("PROVIDER_ACCESS_NUMBER_SELECTION", 
            					providerAccessSelection, "P", false); 
            UnitProfile.getInstance().set("PRIMARY_SCRIPT_DELAY_PHONE_DIAL", 
            		delayAfterPhoneDial, "S", false);
            UnitProfile.getInstance().set("PRIMARY_SCRIPT_DELAY_PT_USERNAME", 
            		delayAfterPTUsername, "S", false);
			UnitProfile.getInstance().set("PRIMARY_SCRIPT_DELAY_PT_PASSWORD", 
					delayAfterPTPwEntry, "S", false); 
			UnitProfile.getInstance().set("PRIMARY_SCRIPT_DELAY_A", 
					delayA, "S", false);
			UnitProfile.getInstance().set("PRIMARY_SCRIPT_DELAY_B", 
					delayB, "S", false);
            UnitProfile.getInstance().set("MODEM_NAME", modemName, "P", false);
                       
            if (this.isPassThroughLogin())
                UnitProfile.getInstance().set("PASS_THROUGH_LOGON", "Y", "S",false);
            else 
                UnitProfile.getInstance().set("PASS_THROUGH_LOGON", "N", "S",false);
           
            if (isUsingAlternateInfo()) {
                UnitProfile.getInstance().set("HOST_PRIMARY_PHONE", alternatePhoneNumber, "S", false);
                UnitProfile.getInstance().set("PRIMARY_DUN_USER_ID", alternateUsername, "S", false);
                UnitProfile.getInstance().set("PRIMARY_DUN_PASSWORD", alternatePassword, "S", false);
            }
            else {
                UnitProfile.getInstance().set("HOST_PRIMARY_PHONE", origDialPhoneNumber, "H", false);
                UnitProfile.getInstance().set("PRIMARY_DUN_USER_ID", origDialUsername, "H", false);
                UnitProfile.getInstance().set("PRIMARY_DUN_PASSWORD", origDialPassword, "H", false);
            }
            
            // Setup Secondary Store
            UnitProfile.getInstance().set("SECONDARY_PROVIDER_ACCESS_NUMBER_COUNTRY", 
            		providerAccessCountry, "P", false);
            UnitProfile.getInstance().set("SECONDARY_PROVIDER_ACCESS_NUMBER_STATE", 
					providerAccessState, "P", false);
            UnitProfile.getInstance().set("SECONDARY_PROVIDER_ACCESS_NUMBER_SELECTION", 
					providerAccessSelection, "P", false); 
            UnitProfile.getInstance().set("SECONDARY_SCRIPT_DELAY_PHONE_DIAL", 
            		delayAfterPhoneDial, "S", false);
            UnitProfile.getInstance().set("SECONDARY_SCRIPT_DELAY_PT_USERNAME", 
            		delayAfterPTUsername, "S", false);
            UnitProfile.getInstance().set("SECONDARY_SCRIPT_DELAY_PT_PASSWORD", 
            		delayAfterPTPwEntry, "S", false); 
            UnitProfile.getInstance().set("SECONDARY_SCRIPT_DELAY_A", 
            		delayA, "S", false);
            UnitProfile.getInstance().set("SECONDARY_SCRIPT_DELAY_B", 
            		delayB, "S", false);
        }
        else if (connectionType == NETWORK_WITH_PROXY) {
            UnitProfile.getInstance().set("POS_HOST_CONNECTION", "N", "S", false);
            UnitProfile.getInstance().set("USE_PROXY", "Y", "S", false);
            UnitProfile.getInstance().set("PROXY_TYPE", PROXY_TYPES[proxyType], "S", false);
            UnitProfile.getInstance().set("PROXY_HOST", proxyServer, "S", false);
            UnitProfile.getInstance().set("PROXY_PORT", proxyPort, "S", false);
            UnitProfile.getInstance().set("PROXY_USER", proxyUsername, "S", false);
            UnitProfile.getInstance().set("PROXY_PASSWORD", proxyPassword, "S", false);
            
        }
        else if (connectionType == NETWORK) {
            UnitProfile.getInstance().set("POS_HOST_CONNECTION", "N", "S", false);
            UnitProfile.getInstance().set("USE_PROXY", "N", "S", false);
            UnitProfile.getInstance().set("PROXY_TYPE", "", "S", false);
            UnitProfile.getInstance().set("PROXY_HOST", "", "S", false);
            UnitProfile.getInstance().set("PROXY_PORT", "", "S", false);
            UnitProfile.getInstance().set("PROXY_USER", "", "S", false);
            UnitProfile.getInstance().set("PROXY_PASSWORD", "", "S", false);
        }
        
        UnitProfile.getInstance().updateProxyProperties();
    }
   
    public String getAlternatePassword() {
        return alternatePassword;
    }

    public String getAlternatePhoneNumber() {
        return alternatePhoneNumber;
    }

    public String getAlternateUsername() {
        return alternateUsername;
    }

    /**
     * @return One of NETWORK, NETWORK_WITH_PROXY, or DIALUP.
     */
    public int getConnectionType() {
        return connectionType;
    }

    /** Returns the URL to use as the initial value of the custom URL field. */
    public String getCurrentUrl() {
        if (isDialUpSelected()) {
            return UnitProfile.getInstance().get("AC_DIALUP_URL", DWValues.DW_AC_URL);
        }
        else {
            return UnitProfile.getInstance().get("AC_NETWORK_URL", DWValues.DW_AC_URL);
        }
    }
    
    /** Returns the URL to use as the initial value of the custom URL field. */
    public String getDemoModeUrl() {
    	return UnitProfile.getInstance().get("DEMO_MODE_URL", "http://localhost:4545/ac/services/");
    }

    /**
     * @return The string to be dialed to reach an outside line.
     */
    public String getHostSpecialDialParams() {
        return hostSpecialDialParams;
    }
    
    /**
     * @return The script name used to perform the double hop.
     */
    public String getDialupScriptName() {
        return dialupScriptName;
    }
    /**
     * @return The status on needing a double hop connection.
     */
    public boolean isPassThroughLogin() {
       return passThroughLogon;
    }
    
    /**
     * @return The script files user name used to connect with a double hop connection.
     */
    public String getPassThroughUserName() {
        return passThroughUserName;
    }

    /**
     * @return The script files password used to connect with a double hop connection.
     */
    public String setPassThroughPassword() {
        return passThroughPassword;
    }


    /**
     * Obtains the initial profile from the backend system.
     * @param unitNumber A String representing the POS client's
     * identification to the system.  The backend system will retrieve
     * the appropriate profile based on this identifier.
     * @param setupPassword A String representing the password to use for
     * authenticating the agent during the installation procedure.
     * @return A boolean representing the status of the profile installation.
     */
    public void getInitialProfile(final String unitNumber,
            final String setupPassword, final InstallFlowPage caller) {
        
        // moving this here otherwise, user entered alternate dialup information is not taking effect.        
        copySettingsToUnitProfile();
        
        final MessageFacadeParam param = 
            new MessageFacadeParam(MessageFacadeParam.INSTALL);

       // copySettingsToUnitProfile();
                            
        final MessageLogic logic = new MessageLogic() {
            @Override
			public Object run() {

                if (unitNumber.equalsIgnoreCase("DEMO") && (
                        setupPassword.equalsIgnoreCase("TEMG")
                        || setupPassword.equalsIgnoreCase("DEMO"))) {
                    return Boolean.TRUE;
                }

                try {
                	GetInitialSetupMsg message = null;
                    message = MessageFacade.getInstance().getInitialProfile(
                        param, unitNumber, setupPassword,versionNumber);

                    if (message == null) {
                        return Boolean.FALSE;
                    }
                      
                    // Pull out the return profile.
                    UnitProfile.getInstance().setInitialProfile(message.getReturnProfile());
                    
                    //set the locale to that of the JVM , otherwise it gets overridden by what is sent by the host.
                    Locale l = Locale.getDefault();
                    String s = l.toString();
                    UnitProfile.getInstance().set("LOCALE", s);    

                    // put back the values entered in this wizard
                    copySettingsToUnitProfile();

//                    // check if we have specified a conn. mode that is different from
//                    //  the one that came down in the profile
//                    if (UnitProfile.getInstance().isDialUpMode() != isDialUpSelected()) {
//                        Dialogs.showWarning("dialogs/DialogIncorrectConnectionMode.xml");	
//                        return Boolean.FALSE;
//                    }

                    // set the profile APPLICATION_MODE tag to the correct mode
                    UnitProfile.getInstance().setAppMode(UnitProfileInterface.NORMAL_MODE);
                    
                    // now we're done making changes to the unit profile, so save it
                    UnitProfile.getInstance().changesApplied();
                    UnitProfile.getInstance().saveChangesQuietly(); 
                    
                    long deviceId = 0L;
                    try {
                    	deviceId = Long.parseLong(UnitProfile.getInstance().get("DEVICE_ID", "-1"));
                    } catch (Exception e) {
                    }

                    long profileId = 0L;
                    try {
                    	profileId = Long.parseLong(UnitProfile.getInstance().get("PROFILE_ID", "-1"));
                    } catch (Exception e) {
                    }

                    try {
	                    State.getInstance().writeLong(deviceId, State.DEVICE_ID);
	                    State.getInstance().writeLong(profileId, State.PROFILE_ID);
                    } catch (Exception e) {
                    }

                    // We're here, so everything worked.
                    return Boolean.TRUE;
                }
                catch (MessageFacadeError mfe) {
                    return Boolean.FALSE;
                }
            }
        };
        MessageFacade.run(logic, caller, 0, Boolean.FALSE);
    }

    /**
     * @return
     */
    public String getProxyPassword() {
        return proxyPassword;
    }

    /**
     * @return
     */
    public String getProxyPort() {
        return proxyPort;
    }

    /**
     * @return
     */
    public String getProxyServer() {
        return proxyServer;
    }

    /**
     * @return
     */
    public int getProxyType() {
        return proxyType;
    }

    /**
     * @return
     */
    public String getProxyUsername() {
        return proxyUsername;
    }

    public boolean isDialUpSelected() {
        return getConnectionType() == DIALUP;
    }

    /**
     * This tran is not a product tran, and is automatic, so no login is
     *   necessary.
     */
    @Override
	public boolean isNamePasswordRequired() {
        return false;
    }

    /**
     * This tran is not a product tran, and is automatic, so no login is
     *   necessary.
     */
    @Override
	public boolean isPasswordRequired() {
        return false;
    }
    
	
    /** 
     * Returns true if this is the special demo mode used id to override the URL. 
     * This is just a comparison method; there are no side effects.
      */
    public boolean isDemoModeId(String p) {
        return ("demo".equalsIgnoreCase(p.trim())); 
    }
    
    /** 
     * Returns true if this is the special demo mode Password to override the URL. 
     * This is just a comparison method; there are no side effects.
      */
    public boolean isDemoModePassword(String p) {
        return ("demo".equalsIgnoreCase(p.trim())); 
    }

    /** 
     * Returns true if this is the special password to override the URL with 
     * extraDebug info on. This is just a comparison method; there are no side effects.
     * The special password is no big secret, just the string "demodebug" in
     * either upper or lower case.
     */
    public boolean isDemoModeDebugPassword(String p) {
        return ("demodebug".equalsIgnoreCase(p.trim())); 
    }
    
    /** 
     * Returns true if this is the special password to override the URL with 
     * extraDebug info on. This is just a comparison method; there are no side effects.
     * The special password is no big secret, just the string "url" in
     * either upper or lower case.
     */
    public boolean isUrlPassword(String p) {
        return ("url".equalsIgnoreCase(p.trim())); 
    }

    /** 
     * Returns true if this is the special password to override the URL with
     * extraDebug info on. This is just a comparison method; there are no side effects.
     * The special password is no big secret, just the string "urldebug" in
     * either upper or lower case.
     */
    public boolean isUrlDebugPassword(String p) {
        return ("urldebug".equalsIgnoreCase(p.trim())); 
    }
    
    
    public boolean isUsingAlternateInfo() {
        return usingAlternateInfo;
    }
 
    /**
     * Override of ClientTransaction method. There is only one type of this
     *   interface, so just default to wizard always.
     */
    @Override
	public boolean isUsingWizard() {
        return true;
    }

    public void setAlternatePassword(String newVal) {
        alternatePassword = newVal;
    }

    public void setAlternatePhoneNumber(String newVal) {
        alternatePhoneNumber = newVal;
    }

    public void setAlternateUrl(String string) {
        alternateUrl = string;
    }

    public void setAlternateUsername(String newVal) {
        alternateUsername = newVal;
    }

    /**
     * @param type One of NETWORK, NETWORK_WITH_PROXY, or DIALUP.
     */
    public void setConnectionType(int type) {
        connectionType = type;
    }

    /**
     * @param string The string to be dialed to reach an outside line.
     */
    public void setHostSpecialDialParams(String string) {
       hostSpecialDialParams = string;
    }
    
    /**
     * @param string The script files used to connect with a double hop connection.
     */
    public void setDialupScriptName(String string) {
       dialupScriptName = string;
    }
    
    /**
     * @param boolean The status on needing a double hop connection.
     */
    public void setPassThroughLogin(boolean b) {
       passThroughLogon = b;
    }
    
    /**
     * @param string The script files user name used to connect with a double hop connection.
     */
    public void setPassThroughUserName(String string) {
        passThroughUserName = string;
    }

    /**
     * @param string The script files password used to connect with a double hop connection.
     */
    public void setPassThroughPassword(String string) {
        passThroughPassword = string;
    }

    /**
     * @param string
     */
    public void setProxyPassword(String string) {
        proxyPassword = string;
    }

    /**
     * @param string
     */
    public void setProxyPort(String string) {
        proxyPort = string;
    }

    /**
     * @param string
     */
    public void setProxyServer(String string) {
        proxyServer = string;
    }

    /**
     * @param string
     */
    public void setProxyType(int i) {
        proxyType = i;
    }

    /**
     * @param string
     */
    public void setProxyUsername(String string) {
        proxyUsername = string;
    }

    public void setUsingAlternateInfo(boolean using) {
        usingAlternateInfo = using;
    }

	@Override
	public boolean isFinancialTransaction() {
		return false;
	}

    @Override
	public void setFinancialTransaction(boolean b)
    {
        // Do nothing.
    }

	@Override
	public boolean isCheckinRequired() {
		return false;
	}

    /**
     * to get the delayAfterPhoneDial
     */
    public String getDelayAfterPhoneDial() {
        return delayAfterPhoneDial;
    }

    /**
     * to get the delayAfterPhoneDial
     */
    public void setDelayAfterPhoneDial(String aDelayAfterPhoneDial) {
        delayAfterPhoneDial = aDelayAfterPhoneDial;
    }
	
    /**
     * to get the delayAfterPTUsername
     */
    public String getDelayAfterPTUsername() {
        return delayAfterPTUsername;
    }

    /**
     * to set the delayAfterPTUsername
     */
    public void setDelayAfterPTUsername(String aDelayAfterPTUsername) {
    	delayAfterPTUsername = aDelayAfterPTUsername;
    }

    /**
     * to get the delayAfterPTPwEntry
     */
    public String getDelayAfterPTPwEntry() {
        return delayAfterPTPwEntry;
    }

    /**
     * to set the delayAfterPTPwEntry
     */
    public void setDelayAfterPTPwEntry(String aDelayAfterPTPwEntry) {
    	delayAfterPTPwEntry = aDelayAfterPTPwEntry;
    }
    /**
     * to get the delayA
     */
    public String getDelayA() {
        return delayA;
    }

    /**
     * to set the delayA
     */
    public void setDelayA(String aDelayA) {
    	delayA = aDelayA;
    }
    
    /**
     * to get the providerAccessCountry
     */
    public String getProviderAccessCountry() {
        return providerAccessCountry;
    }

    /**
     * to set the providerAccessCountry
     */
    public void setProviderAccessCountry(String aProviderAccessCountry) {
        providerAccessCountry = aProviderAccessCountry;
    }
    /**
     * to get the modemName
     */
    public String getModemName() {
        return modemName;
    }

    /**
     * to set the modemName
     */
    public void setModemName(String modem) {
        modemName = modem;
    }

    /**
     * to get the modifyScript Flag
     */
    public boolean getModifyScript() {
        return modifyScript;
    }

    /**
     * to set the modifyScript Flag
     */
    public void setModifyScript(boolean aModifyScript) {
    	modifyScript = aModifyScript;
    }
    
    /**
     * to get the providerAccessState
     */
    public String getProviderAccessState() {
        return providerAccessState;
    }

    /**
     * to set the providerAccessState
     */
    public void setProviderAccessState(String aProviderAccessState) {
    	providerAccessState = aProviderAccessState;
    }
    
    
    /**
     * to get the providerAccessSelection
     */
    public String getProviderAccessSelection() {
        return providerAccessSelection;
    }

    /**
     * to set the providerAccessSelection
     */
    public void setProviderAccessSelection(String aProviderAccessSelection) {
    	providerAccessSelection = aProviderAccessSelection;
    }
    
    /**
     * to get the delayB
     */
    public String getDelayB() {
        return delayB;
    }

    /**
     * to set the delayB
     */
    public void setDelayB(String aDelayB) {
    	delayB = aDelayB;
    }    
	/**
	 * @return Returns the modemList.
	 */
	public String[] getModemList() {
		return modemList;
	}
	/**
	 * @param modemList The modemList to set.
	 */
	public void setModemList(String[] modemList) {
		this.modemList = modemList;
	}

	public boolean isCanDisplayModem() {
		return canDisplayModem;
	}

	public void setCanDisplayModem(boolean canDisplayModem) {
		this.canDisplayModem = canDisplayModem;
	}
} 

