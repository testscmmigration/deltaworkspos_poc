package dw.install;

import java.util.HashSet;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import dw.comm.AgentConnect;
import dw.comm.SOAPMessageHandler;
import dw.dwgui.DelimitedTextField;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.profile.Profile;
import dw.profile.ProfileAccessor;
import dw.profile.ProfileItem;
import dw.profile.UnitProfile;
import dw.profile.UnitProfileInterface;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.ErrorManager;

/**
 * Page 2 of the agent setup wizard.
 * @author Christopher Bartling
 */
public class InstallWizardPage2 extends InstallFlowPage {
	private static final long serialVersionUID = 1L;

	private int MONEY_ORDER_DOC_SEQ = 1;
    private int VENDOR_PAYMENT_DOC_SEQ = 2;
    private int MONEY_GRAM_SEND_DOC_SEQ = 3;
    private int MONEY_GRAM_RECEIVE_DOC_SEQ = 4;
    private int BILL_PAYMENT_DOC_SEQ = 5;
    
    private static String secondSendCurrency = "EUR";
    private static String primarySendCurrency = "USD";
    private static final int DEFAULT_SECOND_SEND_CURR_ID = 978;

    private JTextField unitNumberField = null;
    private JTextField setupPasswordField = null;
    private JTextField urlField = null;
    private JPanel urlPanel = null;
    private int demoCount = 0;
    private String enteredDemoString = null;
    private JPanel choicePanel = null;
    private JRadioButton domesticRadio = null;
    private JRadioButton internationalRadio = null;
    private JPanel domesticInfoPanel = null;
    private JPanel internationalInfoPanel = null;
    private JCheckBox superAgentOptions = null;
    private JCheckBox multiSendCurrencies = null;
	private JCheckBox mfaOption = null;
    private JLabel promoLabel = null;
	private DelimitedTextField promoCount = null;
    
    // domestic options
    private JCheckBox moneyOrderOptions = null;
    private JCheckBox vendorPaymentOptions = null;
    //private JCheckBox moneyGramSendOptions = null;
    private JCheckBox phoneLookupSendOptions = null;
    private JCheckBox formFreeSendOptions = null;
    private JCheckBox InlineRegSendOptions = null;
    private JCheckBox rewardsOptions = null;
    private JCheckBox sendReversalOptions = null;
    private JCheckBox amendOptions = null;
    private JCheckBox isiTransactionOptions = null;
    private JCheckBox isiLogOptions = null;
    
    private JCheckBox epOptions = null;
    private JCheckBox prepayOptions = null;
    private JCheckBox ubpOptions = null;
    private JCheckBox phoneLookupBPOptions = null;
    private JCheckBox formFreeBPOptions = null;
    
    private JCheckBox moneyGramReceiveEnable = null;
    private JComboBox receivePayoutType = null;
    private JCheckBox receiveToCard = null;
    private JComboBox receivePinpadType = null;
    private JComboBox receivePinType = null;
    private JCheckBox formFreeReceiveOptions = null;
    private JCheckBox receiveReversalOptions = null;
    
    // international options
//    private JCheckBox intMoneyGramSendOptions = null;
    private JCheckBox intPhoneLookupSendOptions = null;
//    private JCheckBox intFormFreeSendOptions = null;
    private JCheckBox intMoneyGramReceiveEnable = null;
    private JComboBox countryField = null;
    private JComboBox intReceivePayoutType = null;
    private JComboBox intReceivePinpadType = null;
    private JComboBox intReceivePinType = null;
    private boolean urlDebug = false;
    private HashSet<String> urlSet;
    private String[] validDebugUrls = {"tws2.moneygram.com/ac2",
    								   "dws2.moneygram.com/ac2",
    								   "ws.qa.moneygram.com/ac2",
    								   "ws2.qa.moneygram.com/ac2",
    								   "tws2.moneygram.com/ext",
    								   "extws2.moneygram.com/ext"};
    
    private class CountryItem {
    	private String name;
    	private String icoCode;
    	
    	private CountryItem(String name, String icoCode) {
    		this.name = name;
    		this.icoCode = icoCode;
    	}
    	
    	@Override
    	public String toString() {
    		return name;
    	}
    }
    
    /**
     * Constructor.
     */
    public InstallWizardPage2(InstallTransaction tran,
                       String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "install/AgentSetupPage2.xml", name, pageCode, buttons);	
        collectComponents();
        urlSet = new HashSet<String>();
        for (int i = 0; i < validDebugUrls.length; i++) {
        	urlSet.add(validDebugUrls[i]);
        }

    }

    /**
     * Called before the enclosing container shows this page.
     */
    @Override
	public void start(int direction) {
    	UnitProfile.getInstance().setNewDemoMode(false);
    	if (direction == 5){
	        addListeners();
	        UnitProfile.getInstance().setNewDemoMode(true);
	        unitNumberField.setText(enteredDemoString);
	        setupPasswordField.setText(enteredDemoString);
	        flowButtons.reset();
	        flowButtons.setEnabled("next", false);	
	        setButtonState();
	        flowButtons.getButton("next").requestFocus();
	        
	        // Default Domestic Settings.
	        moneyOrderOptions.setSelected(true);
	        vendorPaymentOptions.setSelected(true);
//	        moneyGramSendOptions.setSelected(true);
	        formFreeSendOptions.setSelected(false);
	        InlineRegSendOptions.setSelected(false);
	        phoneLookupSendOptions.setSelected(true);
	        rewardsOptions.setSelected(true);
	        sendReversalOptions.setSelected(true);
	        amendOptions.setSelected(true);
	        isiTransactionOptions.setSelected(true);
	        isiLogOptions.setSelected(true);
	        
	        epOptions.setSelected(true);
	        prepayOptions.setSelected(true);
	        ubpOptions.setSelected(true);
	        formFreeBPOptions.setSelected(false);
	        phoneLookupBPOptions.setSelected(true);
	        moneyGramReceiveEnable.setSelected(true);

	        formFreeReceiveOptions.setSelected(false);
	        receiveReversalOptions.setSelected(true);
	        
	        // setup receive types
	        receivePayoutType.addItem(Messages.getString("AgentSetupPage2.710"));
	        receivePayoutType.addItem(Messages.getString("AgentSetupPage2.711"));
	        receivePayoutType.addItem(Messages.getString("AgentSetupPage2.712"));
	        receivePinpadType.addItem(Messages.getString("AgentSetupPage2.None"));
	        receivePinpadType.addItem(Messages.getString("AgentSetupPage2.Ingenico_IPP320"));
	        receivePinpadType.addItem(Messages.getString("AgentSetupPage2.Ingenico_IPP350"));
	        receivePinType.addItem(Messages.getString("AgentSetupPage2.None"));
	        receivePinType.addItem(Messages.getString("AgentSetupPage2.4_Digit_Surname"));

	        // setup country list
	        countryField.addItem(new CountryItem("United States", "USA"));
	        countryField.addItem(new CountryItem("Mexico", "MEX"));
	        countryField.addItem(new CountryItem("France", "FRA"));
	        countryField.addItem(new CountryItem("Germany", "DEU"));
	        countryField.addItem(new CountryItem("India", "IND"));
	        countryField.addItem(new CountryItem("Malaysia", "MYS"));
	        countryField.addItem(new CountryItem("Morocco", "MAR"));
	        countryField.addItem(new CountryItem("Peru", "PER"));
	        countryField.addItem(new CountryItem("United Kingdom", "GBR"));
	        
	        // Default International Settings
//	        intMoneyGramSendOptions.setSelected(true);
//	        intFormFreeSendOptions.setSelected(false);
	        intPhoneLookupSendOptions.setSelected(true);
	        intMoneyGramReceiveEnable.setSelected(true);
	        
	        intReceivePayoutType.addItem(Messages.getString("AgentSetupPage2.710"));
	        intReceivePayoutType.addItem(Messages.getString("AgentSetupPage2.711"));
	        intReceivePayoutType.addItem(Messages.getString("AgentSetupPage2.712"));
	        intReceivePinpadType.addItem(Messages.getString("AgentSetupPage2.None"));
	        intReceivePinpadType.addItem(Messages.getString("AgentSetupPage2.Ingenico_IPP320"));
	        intReceivePinpadType.addItem(Messages.getString("AgentSetupPage2.Ingenico_IPP350"));
	        intReceivePinType.addItem(Messages.getString("AgentSetupPage2.None"));
	        intReceivePinType.addItem(Messages.getString("AgentSetupPage2.4_Digit_Surname"));

    	} else{
	        addListeners();
	        flowButtons.reset();
	        flowButtons.setEnabled("next", false);	
	        setButtonState();
	        unitNumberField.requestFocus();
    		
    	}
    }
    
    /**
     * Called before the enclosing container hides this page.
     */
    @Override
	public void finish(int direction) {
        if (direction == PageExitListener.NEXT) {

            // Are we going to manually override the URL?
            if (transaction.isUrlPassword(unitNumberField.getText())
                    || transaction.isUrlPassword(setupPasswordField.getText())) {
                urlPanel.setVisible(true);
                urlField.setText(transaction.getCurrentUrl());
                unitNumberField.setText("");	
                setupPasswordField.setText("");	
                start(0);
                return;
            }
            
            if (transaction.isUrlDebugPassword(unitNumberField.getText())
                    || transaction.isUrlDebugPassword(setupPasswordField.getText())) {
                urlPanel.setVisible(true);
                urlField.setText(transaction.getCurrentUrl());
                urlDebug = true;
                unitNumberField.setText("");	
                setupPasswordField.setText("");	
                start(0);
                return;
            }
            
            // Are we going to manually override the URL for DEMO Mode?
            if (transaction.isDemoModeId(unitNumberField.getText())
                    && transaction.isDemoModePassword(setupPasswordField.getText())
                    && demoCount == 0) {
                urlPanel.setVisible(true);
                choicePanel.setVisible(true);
                domesticInfoPanel.setVisible(true);
                superAgentOptions.setVisible(true);
                multiSendCurrencies.setVisible(true);
                mfaOption.setVisible(true);
                promoLabel.setVisible(true);
                promoCount.setVisible(true);
                promoCount.setText(Integer.toString(DWValues.getPromoCodeLimit()));
                this.rewardsOptions.setVisible(true);
                enteredDemoString = unitNumberField.getText();
                urlField.setText(transaction.getDemoModeUrl());
                unitNumberField.setText("");	
                setupPasswordField.setText("");	
                demoCount++;
                
                // Set the Money Order account number so that the old demo mode will still work.
                UnitProfile.getInstance().set("MO_ACCOUNT", "00000000000000");
                start(5);
                return;
            }
            
            // Are we going to manually override the URL for DEMO DEBUG Mode?
            if (transaction.isDemoModeDebugPassword(unitNumberField.getText())
                    && transaction.isDemoModeDebugPassword(setupPasswordField.getText())
                    && demoCount == 0) {
                urlPanel.setVisible(true);
                choicePanel.setVisible(true);
                urlDebug = true;
                domesticInfoPanel.setVisible(true);
                superAgentOptions.setVisible(true);
                multiSendCurrencies.setVisible(true);
                mfaOption.setVisible(true);
                this.rewardsOptions.setVisible(true);
                enteredDemoString = unitNumberField.getText();
                urlField.setText(transaction.getDemoModeUrl());
                unitNumberField.setText("");	
                setupPasswordField.setText("");	
                demoCount++;
                
                // Set the Money Order account number so that the old demo mode will still work.
                UnitProfile.getInstance().set("MO_ACCOUNT", "00000000000000");
                start(5);
                return;
            }

            if (transaction.isDemoModeId(unitNumberField.getText()) && choicePanel.isVisible()){
            	// Store the domestic information.
            	if (domesticRadio.isSelected()){
            		
            		setTranAuth(MONEY_ORDER_DOC_SEQ, moneyOrderOptions.isSelected());
            	    setTranAuth(VENDOR_PAYMENT_DOC_SEQ, vendorPaymentOptions.isSelected());
            	    setTranAuth(MONEY_GRAM_SEND_DOC_SEQ, formFreeSendOptions.isSelected() || phoneLookupSendOptions.isSelected());
            	    setTranAuth(MONEY_GRAM_RECEIVE_DOC_SEQ, moneyGramReceiveEnable.isSelected());
            	    setTranAuth(BILL_PAYMENT_DOC_SEQ, epOptions.isSelected() || ubpOptions.isSelected() || prepayOptions.isSelected());
            	    String type = receivePayoutType.getSelectedItem().toString().trim();

            	    ProfileAccessor instance=ProfileAccessor.getInstance(); 
            	    ProfileItem pItem=instance.getProductProfileOption("PRODUCT_VARIANTS",2,5);
            	    pItem.setValue(ubpOptions.isSelected());
            	    ProfileItem pItem2=instance.getProductProfileOption("PRODUCT_VARIANTS",1,5);
            	    pItem2.setValue(epOptions.isSelected());
            	    ProfileItem pItem3=instance.getProductProfileOption("PRODUCT_VARIANTS",3,5);
            	    pItem3.setValue(prepayOptions.isSelected());
            	    
            	    // setup FormFree, phone lookup	
            	    ProfileItem pItemFFSend=instance.getProductProfileOption("FORM_FREE_ENABLED",3);
            	    pItemFFSend.setValue(formFreeSendOptions.isSelected());
            	    ProfileItem pItemFFbp=instance.getProductProfileOption("FORM_FREE_ENABLED",5);
            	    pItemFFbp.setValue(formFreeBPOptions.isSelected());
            	    ProfileItem pItemFFReceive=instance.getProductProfileOption("FORM_FREE_ENABLED", 4);
            	    pItemFFReceive.setValue(formFreeReceiveOptions.isSelected());
            	    
            	    ProfileItem pInlineSend=instance.getProductProfileOption(
            	    		DWValues.DSR_INLINE, MONEY_GRAM_SEND_DOC_SEQ);
            	    pInlineSend.setValue(InlineRegSendOptions.isSelected());

            	    ProfileItem pItemPhoneSend=instance.getProductProfileOption("ENABLE_PHONE_NUMBER_LOOKUP",3);
            	    pItemPhoneSend.setValue(phoneLookupSendOptions.isSelected());
            	    ProfileItem pItemPhoneBp=instance.getProductProfileOption("ENABLE_PHONE_NUMBER_LOOKUP",5);
            	    pItemPhoneBp.setValue(phoneLookupBPOptions.isSelected());
            	    
            		if (moneyOrderOptions.isSelected() || 
            			vendorPaymentOptions.isSelected()||
            			type.equals(Messages.getString("AgentSetupPage2.710"))){
                		// Set dispenser ON
                	    UnitProfile.getInstance().set("MO_ACCOUNT", "00000000000000");
            		} else {
                		// Set dispenser OFF
                	    UnitProfile.getInstance().set("MO_ACCOUNT", "");
            		}
            	    
            	    if(type.equals(Messages.getString("AgentSetupPage2.710"))){
            	    	// Type is DeltaGram
            	    	UnitProfile.getInstance().set("MG_CHECKS", "DG");
            	    	UnitProfile.getInstance().set("USE_OUR_PAPER", "Y");
            	    } else if (type.equals(Messages.getString("AgentSetupPage2.711"))){
            	    	// Type is MTC
            	    	UnitProfile.getInstance().set("MG_CHECKS", "MTC");
            	    	UnitProfile.getInstance().set("USE_OUR_PAPER", "Y");            	    	
            	    } else {
            	    	// Type is CASH
            	    	UnitProfile.getInstance().set("MG_CHECKS", "MTC");
            	    	UnitProfile.getInstance().set("USE_OUR_PAPER", "N");            	    	           	    	
            	    }
            	    
       	        	setProductPinpadValue(MONEY_GRAM_RECEIVE_DOC_SEQ, 
       	        			receivePinpadType.getSelectedItem().toString().trim());
       	        	setProductPinValue(MONEY_GRAM_RECEIVE_DOC_SEQ,
       	        			receivePinType.getSelectedItem().toString().trim());

       	        	UnitProfile.getInstance().set("AGENT_COUNTRY_ISO_CODE", "USA");
       	        	UnitProfile.getInstance().set("AGENT_COUNTRY", "UNITED STATES");
    	        	UnitProfile.getInstance().set("AGENT_CURRENCY", "USD");
    	        	UnitProfile.getInstance().set("PROFILE_ID", "USA");

    	        	setProductMaxAmount(MONEY_GRAM_SEND_DOC_SEQ, "9099.99");
    	        	setProductMaxAmount(MONEY_GRAM_RECEIVE_DOC_SEQ, "9099.99");
    	        	setProductFaceEnable(MONEY_GRAM_RECEIVE_DOC_SEQ, "N");
    	        	setProductFeeEnable(MONEY_GRAM_RECEIVE_DOC_SEQ, "N");
    	        	setProductFxEnable(MONEY_GRAM_RECEIVE_DOC_SEQ, "N");
    	        	UnitProfile.getInstance().set(DWValues.RECV_OTHER_PAYOUT_TYPE, 
    	        			receiveToCard.isSelected()?DWValues.RECV_TO_CARD:"None");
					setProductReceiptFormat(MONEY_GRAM_SEND_DOC_SEQ, "mgsus49");
					// 2nd language selection for the hybrid receipt disallows "NONE"
					setSendReceiptAllow2ndLanguage(MONEY_GRAM_SEND_DOC_SEQ, false);
					
					
//			        getComponent("receiveReversalTransaction").setEnabled(getTranAuth(DWValues.MONEY_GRAM_RECEIVE_DOC_SEQ) && 
//			                "Y".equals(UnitProfile.getInstance().get("ALLOW_RECEIVE_REVERSALS", "N")));   
//			            getComponent("amendTransaction").setEnabled(getTranAuth(DWValues.MONEY_GRAM_SEND_DOC_SEQ) && 
//			                "Y".equals(UnitProfile.getInstance().get("ALLOW_AMENDS", "N")));   

            	    UnitProfile.getInstance().set("ALLOW_RECEIVE_REVERSALS", this.receiveReversalOptions.isSelected() ? "Y" : "N");
            	    UnitProfile.getInstance().set("ALLOW_SEND_CANCELS", this.sendReversalOptions.isSelected() ? "Y" : "N");
            	    UnitProfile.getInstance().set("ALLOW_AMENDS", this.amendOptions.isSelected() ? "Y" : "N");
            	    UnitProfile.getInstance().set("ISI_TRANSACTION", this.isiTransactionOptions.isSelected() ? "Y" : "N");
            	    UnitProfile.getInstance().set("ISI_LOG", this.isiLogOptions.isSelected() ? "Y" : "N");
            	}
            	
            	// Store the international information.
   	        	UnitProfileInterface unitProfile = UnitProfile.getInstance();
   	        	
   	        	unitProfile.set(DWValues.PROFILE_PROMO_MAX, promoCount.getText());
   	        	
            	if (internationalRadio.isSelected()){
//            	    setTranAuth(MONEY_GRAM_SEND_DOC_SEQ, intMoneyGramSendOptions.isSelected());
            	    setTranAuth(MONEY_GRAM_SEND_DOC_SEQ, intPhoneLookupSendOptions.isSelected());
            	    setTranAuth(MONEY_GRAM_RECEIVE_DOC_SEQ, intMoneyGramReceiveEnable.isSelected());
            		setTranAuth(MONEY_ORDER_DOC_SEQ, false);
             	    setTranAuth(VENDOR_PAYMENT_DOC_SEQ, false);
            	    setTranAuth(BILL_PAYMENT_DOC_SEQ, false);
            	    
            	    // Set dispenser off
            	    unitProfile.set("MO_ACCOUNT", "");

        	    	// Type is CASH
        	    	unitProfile.set("MG_CHECKS", "MTC");
        	    	unitProfile.set("USE_OUR_PAPER", "N");            
        	    	
       	        	setProductPinpadValue(MONEY_GRAM_RECEIVE_DOC_SEQ, 
       	        			receivePinpadType.getSelectedItem().toString().trim());
       	        	setProductPinValue(MONEY_GRAM_RECEIVE_DOC_SEQ,
       	        			receivePinType.getSelectedItem().toString().trim());

       	        	CountryItem ci = (CountryItem) countryField.getSelectedItem();
       	        	unitProfile.set("AGENT_COUNTRY", ci.name.toUpperCase());
       	        	unitProfile.set("AGENT_COUNTRY_ISO_CODE", ci.icoCode);
        	  
       	        	setProductFaceEnable(MONEY_GRAM_RECEIVE_DOC_SEQ, "N");
    	        	setProductFeeEnable(MONEY_GRAM_RECEIVE_DOC_SEQ, "N");
       	        	setProductFxEnable(MONEY_GRAM_RECEIVE_DOC_SEQ, "N");
       	        	unitProfile.set(DWValues.RECV_OTHER_PAYOUT_TYPE, 
    	        			receiveToCard.isSelected()?DWValues.RECV_TO_CARD:"None");
       	        	
        	    	if (countryField.getSelectedItem().equals("United States")){
        	        	setProductMaxAmount(MONEY_GRAM_SEND_DOC_SEQ, "9099.99");
        	        	setProductMaxAmount(MONEY_GRAM_RECEIVE_DOC_SEQ, "9099.99");
            	    	primarySendCurrency = "USD";
                		secondSendCurrency = "EUR";
        	        	UnitProfile.getInstance().set("PROFILE_ID", "USA");
						setProductReceiptFormat(MONEY_GRAM_SEND_DOC_SEQ, "_en");
						setProductReceiptFormat(MONEY_GRAM_RECEIVE_DOC_SEQ, "_en");
						// additional selection for the non-hybrid receipt allows "NONE"
						setSendReceiptAllow2ndLanguage(MONEY_GRAM_SEND_DOC_SEQ, true);
        	        } else if(countryField.getSelectedItem().equals("Mexico")){
        	        	setProductMaxAmount(MONEY_GRAM_SEND_DOC_SEQ, "99226.29");
        	        	setProductMaxAmount(MONEY_GRAM_RECEIVE_DOC_SEQ, "99226.29");        	        	
            	    	primarySendCurrency = "MXN";
                		secondSendCurrency = "USD";
        	        	UnitProfile.getInstance().set("PROFILE_ID", "MEX");
						setProductReceiptFormat(MONEY_GRAM_SEND_DOC_SEQ, "_es");
        	        } else if(countryField.getSelectedItem().equals("France")){
        	        	setProductMaxAmount(MONEY_GRAM_SEND_DOC_SEQ, "6433.69");
        	        	setProductMaxAmount(MONEY_GRAM_RECEIVE_DOC_SEQ, "6433.69");        	        	
            	    	primarySendCurrency = "EUR";
                		secondSendCurrency = "USD";
        	        	UnitProfile.getInstance().set("PROFILE_ID", "FRA");
						setProductReceiptFormat(MONEY_GRAM_SEND_DOC_SEQ, "_fr");
        	        } else if(countryField.getSelectedItem().equals("Germany")){
        	        	setProductMaxAmount(MONEY_GRAM_SEND_DOC_SEQ, "6506.49");
        	        	setProductMaxAmount(MONEY_GRAM_RECEIVE_DOC_SEQ, "6506.49");        	        	
            	    	primarySendCurrency = "EUR";
                		secondSendCurrency = "USD";
        	        	UnitProfile.getInstance().set("PROFILE_ID", "DEU");
						setProductReceiptFormat(MONEY_GRAM_SEND_DOC_SEQ, "_de");        	        	
        	        } else if(countryField.getSelectedItem().equals("India")){
        	        	setProductMaxAmount(MONEY_GRAM_SEND_DOC_SEQ, "382654.58");
        	        	setProductMaxAmount(MONEY_GRAM_RECEIVE_DOC_SEQ, "382654.58");        	        	
            	    	primarySendCurrency = "INR";
                		secondSendCurrency = "USD";
        	        	UnitProfile.getInstance().set("PROFILE_ID", "IND");
						setProductReceiptFormat(MONEY_GRAM_SEND_DOC_SEQ, "_en_intl");
        	        } else if(countryField.getSelectedItem().equals("Morocco")){
        	        	setProductMaxAmount(MONEY_GRAM_SEND_DOC_SEQ, "79479.31");
        	        	setProductMaxAmount(MONEY_GRAM_RECEIVE_DOC_SEQ, "79479.31");
            	    	primarySendCurrency = "MAD";
                		secondSendCurrency = "USD";
        	        	UnitProfile.getInstance().set("PROFILE_ID", "MAR");
        	        	// hybrid special case for Malaysia GST
						setProductReceiptFormat(MONEY_GRAM_SEND_DOC_SEQ, "mgsusca");
        	        } else if(countryField.getSelectedItem().equals("Malaysia")){
        	        	setProductMaxAmount(MONEY_GRAM_SEND_DOC_SEQ, "79479.31");
        	        	setProductMaxAmount(MONEY_GRAM_RECEIVE_DOC_SEQ, "79479.31");
            	    	primarySendCurrency = "MYR";
                		secondSendCurrency = "USD";
        	        	UnitProfile.getInstance().set("PROFILE_ID", "MYS");
        	        	// CIMB tax-exempt special case for Malaysia GST
						setProductReceiptFormat(MONEY_GRAM_SEND_DOC_SEQ, "_en_intl");
						setProductReceiptFormat(MONEY_GRAM_RECEIVE_DOC_SEQ, "_en_intl");
        	        } else if(countryField.getSelectedItem().equals("Peru")){
        	        	setProductMaxAmount(MONEY_GRAM_SEND_DOC_SEQ, "9099.99");
        	        	setProductMaxAmount(MONEY_GRAM_RECEIVE_DOC_SEQ, "9099.99");
        	        	primarySendCurrency = "USD";
                		secondSendCurrency = "EUR";
        	        	UnitProfile.getInstance().set("PROFILE_ID", "PER");
        	        	// hybrid special case for Malaysia GST
						setProductReceiptFormat(MONEY_GRAM_SEND_DOC_SEQ, "mgsus49");
        	        } else if(countryField.getSelectedItem().equals("United Kingdom")){
        	        	setProductMaxAmount(MONEY_GRAM_SEND_DOC_SEQ, "4468.09");
        	        	setProductMaxAmount(MONEY_GRAM_RECEIVE_DOC_SEQ, "4468.09");        	        	
        	        	primarySendCurrency = "GBP";
                		secondSendCurrency = "EUR";
        	        	UnitProfile.getInstance().set("PROFILE_ID", "GBR");
						setProductReceiptFormat(MONEY_GRAM_SEND_DOC_SEQ, "_en");
						setProductReceiptFormat(MONEY_GRAM_RECEIVE_DOC_SEQ, "_en");
        	        } 
        	    	
            	    // setup FormFree, phone lookup	
            	    ProfileAccessor instance=ProfileAccessor.getInstance(); 
            	    ProfileItem pItemFFSend=instance.getProductProfileOption("FORM_FREE_ENABLED",3);
            	    pItemFFSend.setValue(false);
            	    
            	    ProfileItem pItemPhoneSend=instance.getProductProfileOption("ENABLE_PHONE_NUMBER_LOOKUP",3);
            	    pItemPhoneSend.setValue(intPhoneLookupSendOptions.isSelected());
            	    UnitProfile.getInstance().set(DWValues.PROFILE_RCPT_LANG_ALLOW_ONE, "N");
            	}
            	else {
        	    	unitProfile.set("AGENT_COUNTRY_ISO_CODE", "USA");
        	    	unitProfile.set("AGENT_COUNTRY", "UNITED STATES");
            		primarySendCurrency = "USD";
            		secondSendCurrency = "EUR";
            	    UnitProfile.getInstance().set(DWValues.PROFILE_RCPT_LANG_ALLOW_ONE, "Y");
            	}
	        	unitProfile.set("AGENT_CURRENCY", primarySendCurrency);
	    		unitProfile.find("HDV_LIMIT").setCurr(primarySendCurrency);
	        	setCurrency(MONEY_GRAM_SEND_DOC_SEQ, primarySendCurrency);
    	    	
            	
            	if (rewardsOptions.isSelected())
            		unitProfile.set("MONEYSAVER_ENABLE", "Y");
            	else
            		unitProfile.set("MONEYSAVER_ENABLE", "N");
            	
            	if (superAgentOptions.isSelected()){
            		unitProfile.set("SUPERAGENT", "Y");
            		unitProfile.set("SUBAGENT_MODE", "N");
            	} else {
            		unitProfile.set("SUPERAGENT", "N");
            		unitProfile.set("SUBAGENT_MODE", "N");            		
            	}

            }
            
            try {
	            if (urlDebug && unitNumberField.getText().trim().equalsIgnoreCase("demodebug")) {
            		Debug.println("Demo debug used and set to ON");
                	AgentConnect.enableDebugEncryption();
                	SOAPMessageHandler.enableExtraDebug();
	            } else if (urlDebug && urlField.getText().trim().length() > 0) {
            		String[] urlText = urlField.getText().split("://");
	            	urlText = urlText[1].split("/services/");
	            	urlText = urlText[0].split("/");
	            	String url = null;
	            	String urlNew = null;
	            	if (urlText[1].length() > 2)            		
	            	    url = urlText[0] + "/" + urlText[1].substring(0, 3);
	            	else
	            		url = urlText[0] + "/" + urlText[1];
	            	if (url.length() > 2) {
	            		urlNew = url.substring(2);
	            	}
	            	if (urlSet.contains(url) || urlSet.contains(urlNew)) { 
	            		Debug.println("URL debug used and set to ON");
	            		Debug.println("url set to " + url);
	            		UnitProfile.getInstance().set("URL_EXTRADEBUG", "Y", "P", false);
	            		UnitProfile.getInstance().set("URL_DEBUGENCRYPTION", "Y", "P", false);
	            	}
	            	else {
	            		Debug.println("URL debug used and set to OFF");
	            		Debug.println("url set to " + url);
	            		UnitProfile.getInstance().set("URL_EXTRADEBUG", "N", "P", false);
	                	UnitProfile.getInstance().set("URL_DEBUGENCRYPTION", "N", "P", false);
		            }
	            } else {
	            	Debug.println("Attempted invalid URL debug...setting to OFF");
	                UnitProfile.getInstance().set("URL_EXTRADEBUG", "N", "P", false);
	            	UnitProfile.getInstance().set("URL_DEBUGENCRYPTION", "N", "P", false);
	            }
            }
            catch (Exception e) {
            	Debug.println("Invalid debug URL entered");
            }
            
            /*
             * If there is a second currency, delete it.  That way we can safely
             * add the correct second currency (if needed).
             */
        	Profile profInstance = UnitProfile.getInstance().getProfileInstance();
        	if (profInstance.findList("AGENT_CURRENCY").size() > 1) {
        		deleteSecondCurrency(profInstance);
        	}
        	
            if (multiSendCurrencies.isSelected()) {
            	/*
            	 * If already a second currency in profile, delete it, since it
            	 * may be the wrong one.
            	 */
            	ProfileItem prof =  new ProfileItem("ITEM", "AGENT_CURRENCY", 
            			"S", secondSendCurrency, null);
            	prof.setID(DEFAULT_SECOND_SEND_CURR_ID);
            	profInstance.add(prof);
            	
            	prof =  new ProfileItem("ITEM", "HDV_LIMIT","S","9975",
            			secondSendCurrency);
            	profInstance.add(prof);
            	
            	profInstance = (Profile) UnitProfile.getInstance().getProfileInstance().find(
                        "PRODUCT", MONEY_GRAM_SEND_DOC_SEQ);
            	
            	prof =  new ProfileItem("ITEM", "MAX_AMOUNT_PER_ITEM","S","9000",
            			secondSendCurrency);
            	profInstance.add(prof);
            	
            	prof =  new ProfileItem("ITEM", "FRAUD_LIMIT_CASH_WARNING","S","400",
            			secondSendCurrency);
            	profInstance.add(prof);
            	
            	prof =  new ProfileItem("ITEM", "FRAUD_LIMIT_PHOTO_ID","S","800",
            			secondSendCurrency);
            	profInstance.add(prof);
            	
            	prof =  new ProfileItem("ITEM", "FRAUD_LIMIT_LEGAL_ID","S","2500",
            			secondSendCurrency);
            	profInstance.add(prof);
            	
            	prof =  new ProfileItem("ITEM", "FRAUD_LIMIT_TEST_QUESTION","S","800",
            			secondSendCurrency);
            	profInstance.add(prof);
            	
            }
            if(mfaOption.isSelected()){
            	UnitProfile.getInstance().set("MFA_FLAG", "Y");   
            }else{
            	UnitProfile.getInstance().set("MFA_FLAG", "N");   
            }
            
            if (unitNumberField.getText().trim().length() == 0) {
                unitNumberField.requestFocus();
                return;
            }
            if (setupPasswordField.getText().trim().length() == 0) {
                setupPasswordField.requestFocus();
                return;
            }
            else {
                // Inquiry for the matching profile from the Gemstone system.
            	if((unitNumberField.getText().trim().equalsIgnoreCase("demo")) ||
            			(unitNumberField.getText().trim().equalsIgnoreCase("demodebug"))){
                    PageNotification.notifyExitListeners(this, direction);
            		
            	} else {
	                transaction.setAlternateUrl(urlField.getText().trim());
	                transaction.getInitialProfile(unitNumberField.getText().trim(),
	                        setupPasswordField.getText().trim(),
	                        this);
            	}
            }
        }
        else {
            // Direction is not NEXT page, so just allow to chain up through
            // the responsibility line.
            PageNotification.notifyExitListeners(this, direction);
        }
    }
    
    private void deleteSecondCurrency(Profile profInstance) {
    	ProfileItem prof = profInstance.find("AGENT_CURRENCY", 
    			DEFAULT_SECOND_SEND_CURR_ID);
    	String currency = prof.stringValue();
    	profInstance.remove(prof);
    	
    	prof = profInstance.findCurrencyValue("HDV_LIMIT", 
    			currency);
    	profInstance.remove(prof);
    	
    	profInstance = (Profile) UnitProfile.getInstance().getProfileInstance().find(
                "PRODUCT", MONEY_GRAM_SEND_DOC_SEQ);
    	
    	prof = profInstance.findCurrencyValue("MAX_AMOUNT_PER_ITEM", 
    			currency);
    	profInstance.remove(prof);
    	
    	prof = profInstance.findCurrencyValue("FRAUD_LIMIT_CASH_WARNING", 
    			currency);
    	profInstance.remove(prof);
    	
    	prof = profInstance.findCurrencyValue("FRAUD_LIMIT_PHOTO_ID", 
    			currency);
    	profInstance.remove(prof);
    	
    	prof = profInstance.findCurrencyValue("FRAUD_LIMIT_LEGAL_ID", 
    			currency);
    	profInstance.remove(prof);
    	
    	prof = profInstance.findCurrencyValue("FRAUD_LIMIT_TEST_QUESTION", 
    			currency);
    	profInstance.remove(prof);
    	
    }

    @Override
	public void commComplete(int commTag, Object returnValue) {
        boolean b = ((Boolean) returnValue).booleanValue();
        if (b) {
            PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
        }
        else {
            //replaceComponent("unitNumberField");	
            //replaceComponent("setupPasswordField");	
            start(0);
            unitNumberField.setText("");	
            unitNumberField.selectAll();
            setupPasswordField.setText("");	
            setupPasswordField.selectAll();
            unitNumberField.requestFocus();
            ErrorManager.handle
                       (new Exception(Messages.getString("InstallWizardPage2.1925")), 
                    		   "unsuccessfulGetInitialSetup", Messages.getString("ErrorMessages.unsuccessfulGetInitialSetup"), 
                        ErrorManager.ERROR,
                        true);
        }
    }

    /**
     * Collects all the XML defined components.
     */
    private void collectComponents() {
        // collect the component references
        unitNumberField = (JTextField) getComponent("unitNumberField");	
        setupPasswordField = (JTextField) getComponent("setupPasswordField");	
        urlPanel = (JPanel) getComponent("urlPanel");	
        urlField = (JTextField) getComponent("urlField");	
        choicePanel = (JPanel) getComponent("choicePanel");

        // Domestic options
        domesticInfoPanel = (JPanel) getComponent("domesticInfoPanel");
        domesticRadio = (JRadioButton) getComponent("domesticRadio");
        moneyOrderOptions = (JCheckBox) getComponent("moneyOrderOptions");
        vendorPaymentOptions = (JCheckBox) getComponent("vendorPaymentOptions");
        //moneyGramSendOptions = (JCheckBox) getComponent("moneyGramSendOptions");
        phoneLookupSendOptions = (JCheckBox) getComponent("phoneLookupSendOptions");
        formFreeSendOptions = (JCheckBox) getComponent("formFreeSendOptions");
        InlineRegSendOptions = (JCheckBox) getComponent("InlineRegSendOptions");
        rewardsOptions = (JCheckBox) getComponent("rewardsOptions");
        amendOptions = (JCheckBox) getComponent("amendOptions");
        isiTransactionOptions = (JCheckBox) getComponent("isiTransactionOptions");
        isiLogOptions = (JCheckBox) getComponent("isiLogOptions");
        sendReversalOptions = (JCheckBox) getComponent("sendReversalOptions");
            
        epOptions = (JCheckBox) getComponent("epOptions");
        prepayOptions = (JCheckBox) getComponent("prepayOptions");
        ubpOptions = (JCheckBox) getComponent("ubpOptions");
        
        phoneLookupBPOptions = (JCheckBox) getComponent("phoneLookupBPOptions");
        formFreeBPOptions = (JCheckBox) getComponent("formFreeBPOptions");        
        moneyGramReceiveEnable = (JCheckBox) getComponent("moneyGramReceiveEnable");
        receivePayoutType = (JComboBox) getComponent("receivePayoutType");
        receiveToCard = (JCheckBox) getComponent("receiveToCard");
        receivePinpadType = (JComboBox) getComponent("receivePinpadType");
        receivePinType = (JComboBox) getComponent("receivePinType");

        formFreeReceiveOptions = (JCheckBox) getComponent("formFreeReceiveOptions");       
        receiveReversalOptions = (JCheckBox) getComponent("receiveReversalOptions");
        
        
        // international options
        internationalInfoPanel = (JPanel) getComponent("internationalInfoPanel");
        internationalRadio = (JRadioButton) getComponent("internationalRadio");
//       intMoneyGramSendOptions = (JCheckBox) getComponent("intMoneyGramSendOptions");
        intPhoneLookupSendOptions = (JCheckBox) getComponent("intPhoneLookupSendOptions");
//        intFormFreeSendOptions = (JCheckBox) getComponent("intFormFreeSendOptions");
        intMoneyGramReceiveEnable = (JCheckBox) getComponent("intMoneyGramReceiveEnable");
        countryField = (JComboBox) getComponent("countryField");
        intReceivePayoutType = (JComboBox) getComponent("intReceivePayoutType");
        intReceivePinpadType = (JComboBox) getComponent("intReceivePinpadType");
        intReceivePinType = (JComboBox) getComponent("intReceivePinType");
        
        // super agent
        superAgentOptions = (JCheckBox) getComponent("superAgentOptions");

        multiSendCurrencies = (JCheckBox) getComponent("multiSendCurrencies");
        mfaOption = (JCheckBox) getComponent("mfaOption");
        promoLabel = (JLabel) getComponent("promoLabel");
        promoCount = (DelimitedTextField) getComponent("promoCount");
    }


    /**
     * Adds various event listeners to active components.
     */
    private void addListeners() {
        addActionListener("unitNumberField", this, "tryNext");	 
        addActionListener("setupPasswordField", this, "tryNext");	 
        addActionListener("urlField", this, "tryNext");
        addTextListener("unitNumberField", this, "setButtonState");	 
        addTextListener("setupPasswordField", this, "setButtonState");	 
        JComponent[] focusOrder = {unitNumberField, setupPasswordField,
            flowButtons.getButton("back"), flowButtons.getButton("next"),	 
            flowButtons.getButton("cancel")};	
        setFocusOrder(focusOrder);
        addActionListener("domesticRadio", this, "radioAction");
        addActionListener("internationalRadio", this, "radioAction");
    }

    /**
     * Event handler.
     */
    public void setButtonState() {
        flowButtons.getButton("next").setEnabled	
                                (setupPasswordField.getText().length() > 0);
    }

    /**
     * Event handler.
     */
    public void tryNext() {
        if (setupPasswordField.getText().length() > 0){
            flowButtons.getButton("next").requestFocus();	
            flowButtons.getButton("next").doClick();	
        }
        else if (unitNumberField.hasFocus())
            setupPasswordField.requestFocus();
        else if(urlField.hasFocus())                 
                   flowButtons.getButton("next").doClick();
        else
            unitNumberField.requestFocus();
    }
    /**
     * Event handler.
     */
    public void radioAction() {
        if (domesticRadio.isSelected()){
        	domesticInfoPanel.setVisible(true);
        	internationalInfoPanel.setVisible(false);
	        rewardsOptions.setSelected(true);
        }else if (internationalRadio.isSelected()){
        	domesticInfoPanel.setVisible(false);
        	internationalInfoPanel.setVisible(true);
	        rewardsOptions.setSelected(false);

        }
    }
 
    
    /**
     * Set the status of whether or not a certain product is authorized to be
     *   be sold by this agent based on the profile settings.
      */
    public void setTranAuth(int docSeq, boolean authorized) {
        Profile profile = UnitProfile.getInstance().getProfileInstance();
        ProfileItem item = profile.find("PRODUCT", docSeq); 
        if (authorized)
        	item.setStatus("A");
        else
        	item.setStatus("D");
    }

    
    /**
     *  Set the maximum amount per item to be sold by this agent
     *  by product.
     */
    public void setCurrency(int docSeq, String currency) {
        Profile profile = UnitProfile.getInstance().getProfileInstance();
        ProfileItem item = profile.find("PRODUCT", docSeq); 
        item.get("MAX_AMOUNT_PER_ITEM").setCurr(currency);
        item.get("FRAUD_LIMIT_CASH_WARNING").setCurr(currency);
        item.get("FRAUD_LIMIT_PHOTO_ID").setCurr(currency);
        item.get("FRAUD_LIMIT_LEGAL_ID").setCurr(currency);
        item.get("FRAUD_LIMIT_TEST_QUESTION").setCurr(currency);
    }
    
    /**
     *  Set the pinpad value per item to be sold by this agent
     *  by product.
     */
    public void setProductPinpadValue(int docSeq, String value) {
        Profile profile = UnitProfile.getInstance().getProfileInstance();
        ProfileItem item = profile.find("PRODUCT", docSeq); 
        item.get("PIN_DEVICE").setValue(value);
    }
    
    /**
     *  Set the pinpad value per item to be sold by this agent
     *  by product.
     */
    public void setProductPinValue(int docSeq, String value) {
        Profile profile = UnitProfile.getInstance().getProfileInstance();
        ProfileItem item = profile.find("PRODUCT", docSeq); 
        item.get("PIN_TYPE").setValue(value);
    }
 
    /**
     *  Set the maximum amount per item to be sold by this agent
     *  by product.
     */
    public void setProductMaxAmount(int docSeq, String amount) {
        Profile profile = UnitProfile.getInstance().getProfileInstance();
        ProfileItem item = profile.find("PRODUCT", docSeq); 
        item.get("MAX_AMOUNT_PER_ITEM").setValue(amount);
    }
 
    /**
     *  Set the Enable Face flag by product.
     */
    public void setProductFaceEnable(int docSeq, String value) {
        Profile profile = UnitProfile.getInstance().getProfileInstance();
        ProfileItem item = profile.find("PRODUCT", docSeq); 
        item.get("ENABLE_FACE_AMOUNT").setValue(value);
    }
    
    /**
     *  Set the Enable Fee flag by product.
     */
    public void setProductFeeEnable(int docSeq, String value) {
        Profile profile = UnitProfile.getInstance().getProfileInstance();
        ProfileItem item = profile.find("PRODUCT", docSeq); 
        item.get("ENABLE_FEE").setValue(value);
    }
    
    /**
     *  Set the Enable Face flag by product.
     */
    public void setProductFxEnable(int docSeq, String value) {
        Profile profile = UnitProfile.getInstance().getProfileInstance();
        ProfileItem item = profile.find("PRODUCT", docSeq); 
        item.get("ENABLE_FX_RATE").setValue(value);
    }
    
    /**
     *  Set the Enable Face flag by product.
     */
    public void setReceiveOption(String optionName, String value) {
        Profile profile = UnitProfile.getInstance().getProfileInstance();
        ProfileItem item = profile.find("PRODUCT", MONEY_GRAM_RECEIVE_DOC_SEQ); 
        item.get(optionName).setValue(value);
    }

    /**
     *  Set the receipt template format used by the given product
     */
    public void setProductReceiptFormat(int docSeq, String receiptFormat) {
        Profile profile = UnitProfile.getInstance().getProfileInstance();
        ProfileItem item = profile.find("PRODUCT", docSeq); 
        item.get("RECEIPT_FORMAT").setValue(receiptFormat);
    }

    /**
     *  Set the receipt secondary language list "NONE" list item used by the given product
     */
    public void setSendReceiptAllow2ndLanguage(int docSeq, boolean allow) {
        Profile profile = UnitProfile.getInstance().getProfileInstance();
        ProfileItem item = profile.find("PRODUCT", docSeq); 
		if (item != null) {
			ProfileItem allowItem = item
					.get(DWValues.PROFILE_RCPT_LANG_ALLOW_ONE);
			if (allowItem != null) {
				allowItem.setValue(allow ? "Y" : "N");
			}
		}
    }
}
