package dw.install;


import javax.swing.JLabel;

import dw.dialogs.Dialogs;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.main.DeltaworksMainPanel;
import dw.main.MainPanelDelegate;
import dw.profile.Profile;
import dw.profile.ProfileItem;
import dw.profile.UnitProfile;
import dw.utility.Debug;

/**
 * Page 3 of the agent setup wizard.
 * @author Christopher Bartling
 */
public class InstallWizardPage3 extends InstallFlowPage {
	private static final long serialVersionUID = 1L;

	private JLabel agentIdLabel = null;
    private JLabel agentNameLabel = null;
    private JLabel agentPhoneNumberLabel = null;
    private JLabel agentCountryLabel = null;
    private JLabel deviceIdLabel = null;

	/**
     * Constructor.
	 */
    public InstallWizardPage3(InstallTransaction tran,
                       String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "install/AgentSetupPage3.xml", name, pageCode, buttons);	
    }


    /**
     * Called before the enclosing container shows this page.
     */
    @Override
	public void start(int direction) {
        flowButtons.reset();
       // flowButtons.setAlternateText("next", "OK Enter");	 
        flowButtons.setAlternateText("next",Messages.getString("OKEnter"));     
        flowButtons.setMnemonic("next", 'O'); 
        flowButtons.setSelected("next");	
        flowButtons.getButton("cancel").setEnabled(false);	
        flowButtons.getButton("back").setEnabled(false);	
        collectComponents();
        displayAgentInformation();
    }

    /**
     * Called before the enclosing container hides this page.
     */
    @Override
	public void finish(int direction) {
        // User pressed the OK button, so the transaction has completed
        // successfully.
    	if(!UnitProfile.getInstance().isDemoMode()){
    		 Debug.println("Restarting to put Agent Time Zone in effect"); 	
    		 showRestartWarning("dialogs/DialogRestartAfterSetup.xml");
             MainPanelDelegate.tryExit(DeltaworksMainPanel.RESTART_EXIT);
    	}

        if (direction == PageExitListener.NEXT)
            direction = PageExitListener.COMPLETED;
        PageNotification.notifyExitListeners(this, direction);
    }
    
    /** Display a dialog box stating that the app will restart. The
     *  user has no choice, but wait five seconds so it
     *  is clear the app isn't crashing.
     */
    public void showRestartWarning(String xmlFilename) {
        Thread timer = new Thread(new Runnable() {
            @Override
			public void run() {
                try {
                    Thread.sleep(5000);
                    // cheat by using idle timeout feature to close the dialog
                    Dialogs.idleTimeout();
                }
                catch (InterruptedException e) {
                }
            }
        });
        timer.start();
        Dialogs.showWarning(xmlFilename);
        // dialog has exited on its own, so kill the thread
        if (timer.isAlive())
            timer.interrupt();
    }

    /**
        Obtain references to the various components on the form.
     */
    void collectComponents() {
    	agentIdLabel = (JLabel) getComponent("agentIdLabel");	
    	agentNameLabel = (JLabel) getComponent("agentNameLabel");	
    	agentPhoneNumberLabel = (JLabel) getComponent("agentPhoneNumberLabel");	
    	agentCountryLabel = (JLabel) getComponent("agentCountryLabel");	
        deviceIdLabel = (JLabel) getComponent("deviceIdLabel");	
    }


    /**
        Show agent information obtained through the transaction.
     */
    void displayAgentInformation() {
        Profile profile = UnitProfile.getInstance().getProfileInstance();

        ProfileItem agentId = profile.find("MO_AGENT_ID");	
        agentIdLabel.setText(agentId.stringValue());

        ProfileItem agentName = profile.find("AGENT_NAME");	
        agentNameLabel.setText(agentName.stringValue());

        ProfileItem agentPhoneNumber = profile.find("AGENT_PHONE");	
        agentPhoneNumberLabel.setText(agentPhoneNumber.stringValue());

        ProfileItem agentCountry = profile.find("AGENT_COUNTRY");	
        agentCountryLabel.setText(agentCountry.stringValue());

        ProfileItem agentUnit = profile.find("DEVICE_ID");	
        deviceIdLabel.setText(agentUnit.stringValue());
    }
}
