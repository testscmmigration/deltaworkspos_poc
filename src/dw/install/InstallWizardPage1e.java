package dw.install;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;

import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;

/**
 * Page 1c of the agent setup wizard. Prompts for dialup options.
 *
 * @author Rob Campbell
 * @author Geoff Atkin
 */
public class InstallWizardPage1e extends InstallFlowPage implements ActionListener {
	private static final long serialVersionUID = 1L;

	private JTextField delayAfterPhoneDialField = null;
    private JTextField delayAfterPTUsernameField = null;
    private JTextField delayAfterPTPasswordField = null;
    private JTextField delayAField = null;
    private JTextField delayBField = null;
    
    /**
     * Constructor.
     */
    public InstallWizardPage1e(InstallTransaction tran,
            String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "install/AgentSetupPage1e.xml", name, pageCode, buttons);
    }

    /**
     * Called before the enclosing container shows this page.
     */
    @Override
	public void start(int direction) {
        collectComponents();
       // add code to preserve the existing values
        delayAfterPhoneDialField.setText(
        		transaction.getDelayAfterPhoneDial());
        delayAfterPTUsernameField.setText(
        		transaction.getDelayAfterPTUsername());
        delayAfterPTPasswordField.setText(
        		transaction.getDelayAfterPTPwEntry());
        delayAField.setText(transaction.getDelayA());
        delayBField.setText(transaction.getDelayB());
        delayAfterPhoneDialField.requestFocus();
        flowButtons.reset();
    }

    /**
     * Called before the enclosing container hides this page.
     */
    @Override
	public void finish(int direction) {
    	
    	if (direction == PageExitListener.NEXT) {
   			JTextField[] fields = {delayAfterPhoneDialField, 
   										delayAfterPTUsernameField, 
										delayAfterPTPasswordField, 
										delayAField,
										delayBField};
            if  (! validateFields(fields))
                return;
    	}
            
    	transaction.setDelayAfterPhoneDial(
    			delayAfterPhoneDialField.getText());
    	transaction.setDelayAfterPTUsername(
    			delayAfterPTUsernameField.getText());
    	transaction.setDelayAfterPTPwEntry(
    			delayAfterPTPasswordField.getText());
    	transaction.setDelayA(
    			delayAField.getText());
    	transaction.setDelayB(
    			delayBField.getText());            
            
        PageNotification.notifyExitListeners(this, direction);
    }

    /**
     * Collects all the XML defined components.
     */
    private void collectComponents() {

        // collect the component references
        delayAfterPhoneDialField = (JTextField) getComponent("delayAfterPhoneDialField");
        delayAfterPTUsernameField = (JTextField) getComponent("delayAfterPTUserNameField");
        delayAfterPTPasswordField = (JTextField) getComponent("delayAfterPTUserPasswordField");
        delayAField = (JTextField) getComponent("delayAField");
        delayBField = (JTextField) getComponent("delayBField");
    }

    /* (non-Javadoc)
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
	public void actionPerformed(ActionEvent e) {
        // Do nothing
        
    }
}
