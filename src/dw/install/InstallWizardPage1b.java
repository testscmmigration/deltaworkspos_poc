package dw.install;

import javax.swing.JRadioButton;

import dw.comm.MessageFacade;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;

/**
 * Page 1b of the agent setup wizard. Prompts for the connection mode to be used.
 *
 * @author Rob Campbell
 * @author Geoff Atkin
 */
public class InstallWizardPage1b extends InstallFlowPage {
	private static final long serialVersionUID = 1L;

	private JRadioButton radioButtons[];

    /**
     * Constructor.
     */
    public InstallWizardPage1b(InstallTransaction tran,
                       String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "install/AgentSetupPage1b.xml", name, pageCode, buttons);	
        radioButtons = new JRadioButton[3];
    }

    /**
     * Called before the enclosing container shows this page.
     */
    @Override
	public void start(int direction) {
        collectComponents();
        flowButtons.reset();
    }

    /**
     * Called before the enclosing container hides this page.
     */
    @Override
	public void finish(int direction) {
        for (int i = 0; i < radioButtons.length; i++) {
            if (radioButtons[i].isSelected()) {
                transaction.setConnectionType(i);
            }
        }

        if (radioButtons[InstallTransaction.DIALUP].isSelected()) {
        	String modemList[] = MessageFacade.getInstance().getModemList();
        
        	if (modemList.length > 0) {
        		transaction.setModemList(modemList);
        		transaction.setCanDisplayModem(true);
        	    PageNotification.notifyExitListeners(this, direction);
        	    
        	}
        	else {
           		transaction.setCanDisplayModem(false);
        	    PageNotification.notifyExitListeners(this, direction);
//        		Dialogs.showError("dialogs/DialogNoModemAvailable.xml");
//            	flowButtons.reset();
//        		PageNotification.notifyExitListeners(this, PageExitListener.DONOTHING);
        	}
        }
        else
        	PageNotification.notifyExitListeners(this, direction);
    }

    /**
     * Collects all the XML defined components.
     */
    private void collectComponents() {
        radioButtons[InstallTransaction.DIALUP] = (JRadioButton) getComponent("dialupRadio");
        radioButtons[InstallTransaction.NETWORK_WITH_PROXY] = (JRadioButton) getComponent("proxyRadio");
        radioButtons[InstallTransaction.NETWORK] = (JRadioButton) getComponent("networkRadio");

//        if (transaction.isDialUpSelected()) {
//            dialUpButton.setSelected(true);
//            setDialUpOptionsEnabled(true);   
//        }
//        else {
//            networkButton.setSelected(true);
//            setDialUpOptionsEnabled(false);
//        }

        flowButtons.setSelected("next");	
    }
}
