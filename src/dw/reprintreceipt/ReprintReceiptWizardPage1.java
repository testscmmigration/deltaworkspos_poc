package dw.reprintreceipt;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.KeyMapManager;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;

public class ReprintReceiptWizardPage1  extends ReprintReceiptFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	private DWTextPane senderNameLabel;
    private JLabel senderPhoneLabel;
    private JLabel sentLabel;
    private DWTextPane receiverNameLabel;
    private JLabel messageLabel;
    private JLabel referenceLabel;
    private JLabel authCodeLabel;
    private JLabel authCode;
    private JLabel amountLabel;
    private JPanel screenPanel;
    
   public ReprintReceiptWizardPage1(ReprintReceiptTransaction tran, 
            String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "reprintreceipt/ReprintReceipt1.xml", name, pageCode, buttons);	
        collectComponents();
    }

	@Override
	public void commComplete(int commTag, Object returnValue) {
		PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
	}

	@Override
	public void finish(int direction) {
		PageNotification.notifyExitListeners(this, direction);
	}

    private void collectComponents() {
    	boolean authAvailable = auth.length() > 0;
        senderNameLabel = (DWTextPane) getComponent("senderNameLabel");	
        senderPhoneLabel = (JLabel) getComponent("senderPhoneLabel");	
        sentLabel = (JLabel) getComponent("sentLabel");	
        receiverNameLabel = (DWTextPane) getComponent("receiverNameLabel");	
        messageLabel = (JLabel) getComponent("messageLabel");	
        referenceLabel = (JLabel) getComponent("referenceLabel");	
        amountLabel = (JLabel) getComponent("amountLabel"); 
        authCodeLabel = (JLabel) getComponent("authCodeLabel");
        authCodeLabel.setEnabled(authAvailable);
        authCodeLabel.setVisible(authAvailable);
        authCode = (JLabel) getComponent("authCode");
      	authCode.setEnabled(authAvailable);
        authCode.setVisible(authAvailable);
        screenPanel = (JPanel) getComponent ("screenPanel");
        
        senderNameLabel.addListener(this, this);        
    }
    
    private void setupListeners() {
        addActionListener("reprintButton", this, "reprintReceipt");	 
        addKeyListener(new KeyAdapter() {
                @Override
				public void keyPressed(KeyEvent e) {
                    handleKeyPressedEvent(e);
                }
            });
    }

	@Override
	public void start(int direction) {
        flowButtons.reset();
        flowButtons.setVisible("expert", false);	
        flowButtons.setEnabled("expert", false);
        flowButtons.setVisible("back", false);
        flowButtons.setEnabled("back", false);
        flowButtons.setVisible("next", false);
        flowButtons.setEnabled("next", false);
        flowButtons.setVisible("cancel", true);
        flowButtons.setEnabled("cancel", true);

        setupListeners();
        
        KeyMapManager.mapComponent(this, KeyEvent.VK_F5, 0,
                getComponent("reprintButton"));	
        
        senderNameLabel.setText(senderName);
        //vAdjustNameDisplay(senderNameLabel, senderNameScrollPane);
        
        senderPhoneLabel.setText(senderPhone);
        sentLabel.setText(sentTime);
        receiverNameLabel.setText(recvName);
       // vAdjustNameDisplay(receiverNameLabel, receiverNameScrollPane);

        messageLabel.setText(message);
        referenceLabel.setText(refNbr);
        amountLabel.setText(totalAmt);
        authCode.setText(auth);
        
		addComponent(receiverNameLabel, "receiverName", "receiverNameScrollPane");  
		addComponent(senderNameLabel, "senderName", "senderNameScrollPane");  
		dwTextPaneResized();
	}

	/**
	 * Handles a key pressed KeyEvent.
	 * @param e A KeyEvent object.
	 */
	void handleKeyPressedEvent(KeyEvent e) {
	    e.getKeyCode();
	}
	
	void handleTableKeyEvent(KeyEvent e) {
	    if (e.getKeyCode() == KeyEvent.VK_TAB) {
            if (flowButtons.getButton("next").isEnabled()) {
            	flowButtons.getButton("next").requestFocus();
            }
            else {
            	flowButtons.getButton("cancel").requestFocus();
            }
	    }
	} // END_METHOD
	
	@Override
	public void reprintReceipt() {
		super.reprintReceipt();
	}

    @Override
	public void dwTextPaneResized() {
    	SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
		    	int width = screenPanel.getBounds().width - 120;
				senderNameLabel.setWidth(width);
				receiverNameLabel.setWidth(width);
			}
    	});
	}
}
