package dw.reprintreceipt;

import dw.framework.ClientTransaction;
import dw.framework.PageNameInterface;
import dw.framework.TransactionInterface;

public abstract class ReprintReceiptTransactionInterface extends TransactionInterface {
	private static final long serialVersionUID = 1L;

    protected ReprintReceiptTransaction transaction;
	
    /** 
     * Constructor.
     */
    public ReprintReceiptTransactionInterface(String fileName,
    							ReprintReceiptTransaction transaction,
                                PageNameInterface naming,
                                boolean sideBar) {
        super(fileName, naming, sideBar);	
        this.transaction = transaction;
    }

    @Override
	public ClientTransaction getTransaction() {
        return transaction;
    }
}
