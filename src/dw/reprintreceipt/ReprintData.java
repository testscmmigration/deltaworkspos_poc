package dw.reprintreceipt;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import dw.utility.TimeUtility;

/*
 *  Header data for the Reprint Receipt Environment file
 */
public class ReprintData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2521810437875709601L;
	public static final int REPRINT_SEND = 1;
	public static final int REPRINT_RECV = 2;
	public static final int REPRINT_UBP = 3;

	private int iRecptType;
	private long lTimeStamp;
	private String key;// referenceNumber
	private List<ReceiptInfo> printData;
	private boolean customerReceipt;

	public ReprintData(int recptType) {
		super();
		this.iRecptType = recptType;
		this.lTimeStamp = TimeUtility.currentTimeMillis();
	}

	public ReprintData(int recptType, String key) {
		super();
		this.iRecptType = recptType;
		this.lTimeStamp = TimeUtility.currentTimeMillis();
		this.key = key;
		this.printData = new ArrayList<ReceiptInfo>();
	}

	/**
	 * @return the iRecptType
	 */
	public int getiRecptType() {
		return iRecptType;
	}

	/**
	 * @return the lTimeStamp
	 */
	public long getlTimeStamp() {
		return lTimeStamp;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the printData
	 */
	public List<ReceiptInfo> getPrintData() {
		return printData;
	}

	/**
	 * @param printData
	 *            the printData to set
	 */
	public void setPrintData(List<ReceiptInfo> printData) {
		this.printData = printData;
	}

	/**
	 * @return the customerReceipt
	 */
	public boolean isCustomerReceipt() {
		return customerReceipt;
	}

	/**
	 * @param customerReceipt
	 *            the customerReceipt to set
	 */
	public void setCustomerReceipt(boolean customerReceipt) {
		this.customerReceipt = customerReceipt;
	}

	private void readObject(ObjectInputStream aInputStream) throws ClassNotFoundException, IOException {
		// always perform the default de-serialization first
		aInputStream.defaultReadObject();

	}

	/**
	 * This is the default implementation of writeObject. Customise if
	 * necessary.
	 */
	private void writeObject(ObjectOutputStream aOutputStream) throws IOException {
		// perform the default serialization for all non-transient, non-static
		// fields
		aOutputStream.defaultWriteObject();
	}

	class ReceiptInfo implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = -8789777312696071340L;
		/**
		 * 
		 */
		byte[] printData =null;

		private void readObject(ObjectInputStream aInputStream) throws ClassNotFoundException, IOException {
			// always perform the default de-serialization first
			aInputStream.defaultReadObject();

		}

		/**
		 * This is the default implementation of writeObject. Customise if
		 * necessary.
		 */
		private void writeObject(ObjectOutputStream aOutputStream) throws IOException {
			// perform the default serialization for all non-transient,
			// non-static
			// fields
			aOutputStream.defaultWriteObject();
		}
	}
}
