package dw.reprintreceipt;

import java.util.List;
import java.util.Map;

import com.moneygram.agentconnect.Response;

import dw.framework.CommCompleteInterface;
import dw.framework.DataCollectionPanel;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.FlowPage;
import dw.mgsend.MoneyGramClientTransaction;
import dw.model.adapters.ConsumerProfile;
import dw.model.adapters.CustomerInfo;

public class ReprintReceiptTransaction extends MoneyGramClientTransaction  {
	private static final long serialVersionUID = 1L;

    public ReprintReceiptTransaction(String tranName) {
        super(tranName, "");
        timeoutEnabled = true;
        dispenserNeeded = false;
        receiptPrinterNeeded = true;
        clear();
    }

    /**
     * Override of method in Client Transaction. Authentication is never
     *   needed for this transaction.
     */
    @Override
	public boolean isNamePasswordRequired() {
        return false;
    }

    /**
     * Override of method in Client Transaction. Authentication is never
     *   needed for this transaction.
     */
    @Override
	public boolean isPasswordRequired() {
        return false;
    }

    /**
     * Override of ClientTransaction method. There is only one type of this
     *   interface, so just default to wizard always.
     */
    @Override
	public boolean isUsingWizard() {
        return true;
    }

	@Override
	public boolean isCheckinRequired() {
		return false;
	}

	@Override
	public boolean isFinancialTransaction() {
		return false;
	}

	@Override
	public void setFinancialTransaction(boolean b) {
        // Do nothing.
	}
	
	@Override
	public void clear() {
		setStatus(IN_PROCESS);
	}
	
	// Data Collection support method (not used by this type of transaction)

	@Override
	public List<DataCollectionPanel> getSupplementalDataCollectionPanels() {
		return null;
	}

	@Override
	public void setValidationStatus(DataCollectionStatus status) {
	}

	@Override
	public void validation(FlowPage callingPage) {
	}

	@Override
	public String nextDataCollectionScreen(String currentScreen) {
		return null;
	}

	@Override
	public String getDestinationCountry() {
		return null;
	}

	@Override
	public void setScreensToShow() {
	}

	@Override
	public void applyHistoryData(CustomerInfo customerInfo) {
	}

	@Override
	public CustomerInfo getCustomerInfo() {
		return null;
	}

	@Override
	public void applyTransactionLookupData() {
	}

	@Override
	public void applyProfileData(ConsumerProfile profile) {
	}

	@Override
	public boolean createOrUpdateConsumerProfile(ConsumerProfile profile, ProfileStatus status) {
		return false;
	}

	@Override
	public void createOrUpdateConsumerProfile(ConsumerProfile profile, CommCompleteInterface cci) {
	}

	@Override
	public void getConsumerProfile(ConsumerProfile profile, String mgiSessionID, int rule, Map<String, String> currentValues, CommCompleteInterface cci) {
	}

	@Override
	public Response getConsumerProfileResponse() {
		return null;
	}

	@Override
	public void applyCurrentData() {
	}
}
