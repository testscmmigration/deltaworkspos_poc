package dw.reprintreceipt;

import java.awt.event.KeyEvent;

import dw.framework.PageExitListener;
import dw.framework.PageNameInterface;

public class ReprintReceiptWizard extends ReprintReceiptTransactionInterface {
	private static final long serialVersionUID = 1L;

	public ReprintReceiptWizard(
			ReprintReceiptTransaction transaction, PageNameInterface naming) {
		super("reprintreceipt/RRWizardPanel.xml", transaction, naming, true);
        createPages();
	}

	@Override
	protected void createPages() {
		addPage(ReprintReceiptWizardPage1.class, transaction, "reprintReceipt", "RRW01", buttons);
	}

    /**
     * Add the single keystroke mappings to the buttons. Use the function keys
     *   for back, next, cancel, etc.
     */
    private void addKeyMappings() {
        buttons.addKeyMapping("expert", KeyEvent.VK_F4, 0); 
        buttons.addKeyMapping("back", KeyEvent.VK_F11, 0); 
        buttons.addKeyMapping("next", KeyEvent.VK_F12, 0); 
        buttons.addKeyMapping("cancel", KeyEvent.VK_ESCAPE, 0); 
    }
    
    @Override
	public void start() {
        transaction.clear();
        super.start();
        addKeyMappings();
    }
    
    /**
     * Page traversal logic here.
     */
    @Override
	public void exit(int direction) {

        if (direction == PageExitListener.DONOTHING){
            return;
        }

        super.exit(direction);
    }

}
