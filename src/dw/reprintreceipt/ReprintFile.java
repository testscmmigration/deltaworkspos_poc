package dw.reprintreceipt;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Timer;
import java.util.TimerTask;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import dw.profile.ClientInfo;
import dw.profile.UnitProfile;
import dw.utility.Debug;
import dw.utility.ErrorManager;
import dw.utility.TimeUtility;

public class ReprintFile {
	private static final long   REPRINT_MAX_TIME = TimeUtility.HOUR;
	private static final String REPRINT_FILE = "scrnfx.dlr";
	
    private static Cipher                 cipher = null;
    private static boolean                deletionActive = false;
    private static Timer                  expireFileTimer;
    private static ExpireFileTimerTask    expireFileTimerTask;
    private static ObjectInputStream      in = null;
    private static ObjectOutputStream     out = null;
    private static SecretKeySpec          skeySpec;
    private static IvParameterSpec        ivspec;
    
    // Advanced Encryption Standard, Cipher Block Chaining mode, and padding 
    private static final String CIPHER_KEY_SPECIFICATION = "AES/CBC/PKCS5Padding";
    private static final String SECRET_KEY_SPECIFICATION = "AES";

    /*
     *  Task to delete the Reprint Environment file after it TTL expires.
     */
    private static class ExpireFileTimerTask extends TimerTask {
    	@Override
		public void run() {
    		deleteFile(true);				
    	}
    }
    
    /*
     *  Setup a timer to delete the environment file
     */
    private static void setupExpireTimer() {
        //
        // Get rid of any old timers
        //
        expireFileTimerTask.cancel();
        expireFileTimer.cancel();

        //
        // Setup expireFileTimerTask to delete the reprint environment file
        // after its expiration time.
        //
        expireFileTimer     = new Timer();
        expireFileTimerTask = new ExpireFileTimerTask();
        
        expireFileTimer.schedule(expireFileTimerTask, REPRINT_MAX_TIME);
    }
	
    /*
     * Open the Reprint Environment file, and setup its expiration
     */
    public static boolean prepOutputFile(int trxType) {
    	FileOutputStream   fileOut;
    	ReprintData        header = new ReprintData(trxType);

        try {
    		//
    		// Delete the file if it exists.
    		//
    		deleteFile(false);				

    		//
    		// Set the cipher to encryption mode.
    		//
    		try {
    			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivspec);
    		} catch (InvalidKeyException e) {
    			return false;
    		} catch (InvalidAlgorithmParameterException e) {
    			return false;
			}

    		//
    		// Setup the encrypted output stream
    		//
    		fileOut                = new FileOutputStream(REPRINT_FILE);
    		CipherOutputStream cos = new CipherOutputStream(fileOut, cipher);
    		out                    = new ObjectOutputStream(cos);
    		
    		//
    		// write out the header object
    		//
    		out.writeObject(header);

    		//
    		// Setup environment file to expire 
    		//
    		setupExpireTimer();
            
        	return true;
    		
    	} catch (FileNotFoundException e) {
            Debug.println("openFileForWrite() caught FileNotFoundException: " + e.getClass().getName() + ": " + e.getMessage());
    	} catch (IOException e) {
            Debug.println("openFileForWrite() caught IOExcepion: " + e.getClass().getName() + ": " + e.getMessage());
    	}
		return false;
    }
    
	/*
	 *  Save an object to a previously opened Reprint Environment file
	 */
    public static boolean saveObject(Object saveObject) {
    	if (out == null) {
    		return false;
    	}
		try {
			out.writeObject(saveObject);
			return true;
		} catch (IOException e) {
			Debug.println("saveObject caught IOExcepion: " + e.getClass().getName() + ": " + e.getMessage());
			deleteFile(false);
			return false;
		}
    }
	
    /*
     * Close the Output file containing a Reprint environment
     */
    public static boolean closeOutputFile() {
		try {
			if (out != null) {
				out.flush();
				out.close();
				out = null;
	            ErrorManager.updateReprintReceiptState();
			}
			else {
	            Debug.println("closeOutputFile() out == null");
			}
			return true;
		} catch (IOException e) {
			out = null;
            Debug.println("closeOutputFile() caught IOExcepion: " + e.getClass().getName() + ": " + e.getMessage());
			return false;
		}
    }
	
    /*
     * Allow access to the Reprint Environment file
     */
	public static boolean openFileForRead() {
		try {
			//
			// If reprint is not available, return a null.
			//
	        if (!reprintAvailable()) {
	        	return false;
	        }
			
    		//
    		// Set the cipher to decryption mode.
    		//
	        try {
	            cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivspec);
	        } catch (InvalidKeyException e) {
	            Debug.println("openFileForRead() caught InvalidKeyException: " + e.getClass().getName() + ": " + e.getMessage());
	            return false;
	        } catch (InvalidAlgorithmParameterException e) {
	            Debug.println("openFileForRead() caught InvalidAlgorithmParameterException: " + e.getClass().getName() + ": " + e.getMessage());
				return false;
			}
			
    		//
    		// Setup the encrypted input stream
    		//
	        FileInputStream   fileIn = new FileInputStream(REPRINT_FILE);
	        CipherInputStream cis    = new CipherInputStream(fileIn, cipher);
	        in                       = new ObjectInputStream(cis);

			return true;
			
		} catch (FileNotFoundException e1) {
            // ignore
		} catch (IOException e) {
            Debug.println("openFileForRead() caught IOExcepion: " + e.getClass().getName() + ": " + e.getMessage());
		}
		return false;
	}
	
	/*
	 *  Read an object from a previously opened Reprint Environment file
	 */
    public static Object readObject() {
    	if (in == null) {
    		return null;
    	}
    	
		try {
			return in.readObject();
		} catch (IOException e) {
            Debug.println("openFileForRead() caught IOExcepion: " + e.getClass().getName() + ": " + e.getMessage());
			return null;
		} catch (ClassNotFoundException e) {
            Debug.println("openFileForRead() caught ClassNotFoundException: " + e.getClass().getName() + ": " + e.getMessage());
			return null;
		}
    }
	
    /*
     * Close the Input file containing a Reprint environment
     */
    public static boolean closeInputFile() {
		try {
			if (in != null) {
				in.close();
	            ErrorManager.updateReprintReceiptState();
				in = null;
			}
			else {
	            Debug.println("closeInputFile() in == null");
			}
			return true;
		} catch (IOException e) {
			in = null;
            Debug.println("closeInputFile() caught IOExcepion: " + e.getClass().getName() + ": " + e.getMessage());
			return false;
		}
    }
	
	/*
	 * Delete the Reprint Environment file
	 */
	public static void deleteFile(boolean updateState) {
		/*
		 * Close any active streams.
		 */
		try {
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
		} catch (IOException e) {
			// Ignore
		}
		in  = null;
		out = null;

		if (!deletionActive) {
			deletionActive = true;
			File FileId = new File(REPRINT_FILE);
			if (FileId.delete()) {
				Debug.println("Deleted file " + REPRINT_FILE);
			} else {
				Debug.println("Deletion of file " + REPRINT_FILE + " failed");
			}
			if (updateState) {
				ErrorManager.updateReprintReceiptState();
			}
			deletionActive = false;
		}
	}
	
	/*
	 * Return true if a receipt reprint is possible
	 */
	public static boolean reprintAvailable() {
        /*
         *  If we have the environment open for output, reprint is not presently
         *  available
         */
        if (out != null) {
        	return false;
        }
        
		FileInputStream fileIn = null;

		try {
    		//
    		// Set the cipher to decryption mode.
    		//
	        try {
	            cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivspec);
	        } catch (InvalidKeyException e) {
	            Debug.println("reprintAvailable() caught InvalidKeyException: " + e.getClass().getName() + ": " + e.getMessage());
	            return false;
	        } catch (InvalidAlgorithmParameterException e) {
	            Debug.println("reprintAvailable() caught InvalidAlgorithmParameterException: " + e.getClass().getName() + ": " + e.getMessage());
	            return false;
			}
			
    		//
    		// setup the encrypted input stream
    		//
			fileIn                = new FileInputStream(REPRINT_FILE);
	        CipherInputStream cis = new CipherInputStream(fileIn, cipher);
			ObjectInputStream ois = new ObjectInputStream(cis);
			
			ReprintData  reprintHeader = (ReprintData)ois.readObject();
			
			//
			// Check to see if the file is still valid
			//
			if ( ( TimeUtility.currentTimeMillis() - reprintHeader.getlTimeStamp()) >
					REPRINT_MAX_TIME) {
				ois.close();
				deleteFile(true);				
				return false;
			}
			ois.close();

			/*
			 * Finally make sure receipts are not disabled.
			 */
		    return ! UnitProfile.getInstance().get("ENABLE_SERVER_SIDE_RECEIPTS", "Y").equals("N");
		    
		} catch (FileNotFoundException e) {
            // ignore
			
		} catch (StreamCorruptedException e) {
            Debug.println("reprintAvailable() caught StreamCorruptedException: " + e.getClass().getName() + ": " + e.getMessage());
			try {
				if (fileIn != null) {
					fileIn.close();
				}
				//
				// File is corrupted, or does not match the account number,
				// set it up for deletion.
				//
	    		setupExpireTimer();
			} catch (IOException e1) {
                // ignore
			}
			
		} catch (IOException e) {
            Debug.println("reprintAvailable() caught IOExcepion1: " + e.getClass().getName() + ": " + e.getMessage());
			try {
				if (fileIn != null) {
					fileIn.close();
				}
				//
				// File is bad or incorrect, set it up for deletion 
				//
	    		setupExpireTimer();
			} catch (IOException e1) {
                // ignore
			}
			
		} catch (ClassNotFoundException e) {
            Debug.println("reprintAvailable() caught ClassNotFoundException: " + e.getClass().getName() + ": " + e.getMessage());
			try {
				if (fileIn != null) {
					fileIn.close();
				}
			} catch (IOException e1) {
	            Debug.println("reprintAvailable() caught IOExcepion2: " + e.getClass().getName() + ": " + e.getMessage());
			}
		}
		return false;
	}
	
	/*
	 * Initialization required by ReprintFile
	 */
	static {
		/*
		 * Put some default characters in the key.  These should all get over
		 * written, but just in case one of the dynamic fields used to make
		 * the key is short we have something in each byte.
		 */
		byte[] baAesKeyData = { 0, 'M', 'G', 'I', 4, 'J', 'L', 'L', 8, 'M',
				'G', 'I', 12, 'J', 'L', 'L' };
		ivspec = new IvParameterSpec(baAesKeyData);

		expireFileTimer     = new Timer();
		expireFileTimerTask = new ExpireFileTimerTask();

		ClientInfo co = new ClientInfo();
		String profileId = co.getProfileID();

		//
		// Create key based on the profile ID and MGI Account Number
		//

		for (int ii = 1; (ii < profileId.length() && (ii <= 8)); ii++) {
			baAesKeyData[ii] = (byte) profileId.charAt(ii);
		}

		String acctNbr = co.getMGAccountNumber();

		for (int ii = 9; (ii < acctNbr.length() && (ii <= 15)); ii++) {
			baAesKeyData[ii] = (byte) acctNbr.charAt(ii);
		}

		//
		// create the secret key specification using our key
		//
		skeySpec = new SecretKeySpec(baAesKeyData, SECRET_KEY_SPECIFICATION);

		//
		// Instantiate the cipher
		//
		try {
			cipher = Cipher.getInstance(CIPHER_KEY_SPECIFICATION);
		} catch (NoSuchAlgorithmException e) {
			Debug.println("ReprintFile - caught NoSuchAlgorithmException: " + e.getClass().getName() + ": " + e.getMessage());
		} catch (NoSuchPaddingException e) {
			Debug.println("ReprintFile - caught NoSuchPaddingException: " + e.getClass().getName() + ": " + e.getMessage());
		}
	}
	
	/*
	 * Close the Output file containing a Reprint environment
	 */
	public static boolean closeOutputStream(ObjectOutputStream outStrm) {
		try {
			if (outStrm != null) {
				outStrm.flush();
				outStrm.close();
			} else {
				Debug.println("closeOutputFile() out == null");
			}
			return true;
		} catch (IOException e) {
			Debug.println("closeOutputFile() caught IOExcepion: " + e.getClass().getName() + ": " + e.getMessage());
			return false;
		}
	}


	/*
	 * Close the Input file containing a Reprint environment
	 */
	public static boolean closeInputStream(ObjectInputStream inStrm) {
		try {
			if (inStrm != null) {
				inStrm.close();
			} else {
				Debug.println("closeInputFile() in == null");
			}
			return true;
		} catch (IOException e) {
			Debug.println("closeInputFile() caught IOExcepion: " + e.getClass().getName() + ": " + e.getMessage());
			return false;
		}
	}


}
