package dw.reprintreceipt;

import java.util.Date;

import dw.billpayment.BPWizardPage17;
import dw.dialogs.Dialogs;
import dw.framework.DataCollectionSet;
import dw.framework.FieldKey;
import dw.framework.FlowPage;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.FormatSymbols;
import dw.mgreceive.MGRWizardPage9;
import dw.mgreceive.MoneyGramReceiveTransaction;
import dw.mgsend.MGSWizardPage14;
import dw.mgsend.MoneyGramSendTransaction;
import dw.model.adapters.PaidMoneyGramInfo40;
import dw.model.adapters.ReceiveDetailInfo;
import dw.model.adapters.SenderInfo;
import dw.pdfreceipt.PdfReceipt;
import dw.prtreceipt.PrtReceipt;
import dw.utility.DWValues;
import dw.utility.Debug;

public abstract class ReprintReceiptFlowPage extends FlowPage {
	private static final long serialVersionUID = 1L;

    private static MoneyGramReceiveTransaction recvTransaction=null;
	private static ReprintData                 reprintHeader=null;
    private static MoneyGramSendTransaction    sendTransaction=null;
    private static MoneyGramSendTransaction    ubpTransaction=null;
	
	protected ReprintReceiptTransaction transaction;
	
	protected static String auth;
	protected static String message;
	protected static String recvName;
	protected static String refNbr;
	protected static String senderName;
	protected static String senderPhone;
	protected static String sentTime;
	protected static String totalAmt;
	
	private static final int[] PDF_RCPTS_TO_PRINT = {
		PdfReceipt.PDF_AGENT_1,
		PdfReceipt.PDF_AGENT_2,
		PdfReceipt.PDF_CUST_1,
		PdfReceipt.PDF_CUST_2,
	};
	
	private static final int[] PRT_RCPTS_TO_PRINT = {
		PrtReceipt.PRT_AGENT_1,
		PrtReceipt.PRT_AGENT_2,
		PrtReceipt.PRT_CUST_1,
		PrtReceipt.PRT_CUST_2,
	};

	public ReprintReceiptFlowPage(ReprintReceiptTransaction transaction,
			String fileName, String name, String pageCode, PageFlowButtons buttons) {
		super(fileName, name, pageCode, buttons);
		this.transaction = transaction;
	}

	// do-nothing impl of commCompleteInterface...will be overridden by each
	//  specific page where comm is needed.
	@Override
	public void commComplete(int commTag, Object returnValue) {}

	/*
	 * Restores a saved transaction environment 
	 */
	public static boolean readTran() {

		if (!ReprintFile.openFileForRead()) {
			Debug.println("readTran() - openFileForRead() failed");
			return false;
		}

		reprintHeader = (ReprintData) ReprintFile.readObject();

		if (reprintHeader == null) {
			Debug.println("readTran() - Header not found");
			return false;
		}

		sentTime = FormatSymbols.formatDateTimeForLocaleWithTimeZone(
				new Date(reprintHeader.getlTimeStamp()));

		switch (reprintHeader.getiRecptType()) {

		case ReprintData.REPRINT_SEND: {
			sendTransaction = (MoneyGramSendTransaction)ReprintFile.readObject();
			if (sendTransaction == null) {
				Debug.println("readTran() - sendTransaction not found");
				return false;
			}
			SenderInfo detailLookupInfo = sendTransaction.getSender();
			if (detailLookupInfo == null) {
				Debug.println("readTran() - detailLookupInfo not found");
				return false;
			}

			DataCollectionSet data = sendTransaction.getDataCollectionData();
			senderName = DWValues.formatName(data.getFieldStringValue(FieldKey.SENDER_FIRSTNAME_KEY), data.getFieldStringValue(FieldKey.SENDER_MIDDLENAME_KEY), data.getFieldStringValue(FieldKey.SENDER_LASTNAME_KEY), data.getFieldStringValue(FieldKey.SENDER_LASTNAME2_KEY));
			senderPhone = sendTransaction.formatPhoneNumber(detailLookupInfo.getHomePhone());
			recvName = sendTransaction.getReceiver().getFullName(); 
			message = sendTransaction.getMessage1();
			refNbr = sendTransaction.getCommitReferenceNumber(); 
			totalAmt = sendTransaction.getTotalAmount().toString() 
			+ " (" +sendTransaction.getSendCurrency() + ")"; 
			auth = ""; 
			break;
		}
		case ReprintData.REPRINT_RECV: {
			ReceiveDetailInfo receiveDetailInfo = null;
			PaidMoneyGramInfo40 paidMoneyGramInfo40 = null;

			recvTransaction = (MoneyGramReceiveTransaction)ReprintFile.readObject();
			if (recvTransaction == null) {
				Debug.println("readTran() - recvTransaction not found");
				return false;
			}

			receiveDetailInfo = (ReceiveDetailInfo)ReprintFile.readObject();
			if (receiveDetailInfo == null) {
				Debug.println("readTran() - recvTransaction not found");
				return false;
			}
			recvTransaction.restoreReceiveDetailInfo(receiveDetailInfo);

			paidMoneyGramInfo40 =(PaidMoneyGramInfo40)ReprintFile.readObject(); 
			if (paidMoneyGramInfo40 == null) {
				Debug.println("readTran() - paidMoneyGramInfo40 not found");
				return false;
			}
			recvTransaction.restoreReceivePaidInfo(paidMoneyGramInfo40);

//			if (recvTransaction.refund) {
//				lookupDetail = (DetailLookupInfoMC)ReprintFile.readObject();
//				if (lookupDetail == null) {
//					Debug.println("readTran() - lookupDetail not found");
//					return false;
//				}
//			}
//			else {
//				lookupDetail = null;
//			}

			receiveDetailInfo = recvTransaction.getReceiveDetail();
			if (receiveDetailInfo == null) {
				Debug.println("readTran() - receiveDetailInfo not found");
				return false;
			}

			if (receiveDetailInfo.getSenderMiddleName().length() > 0) {
				senderName  = receiveDetailInfo.getSenderFirstName() + " " +
				receiveDetailInfo.getSenderMiddleName() + " " +
				receiveDetailInfo.getSenderLastName() + " " +
				receiveDetailInfo.getSenderLastName2();
			}
			else {
				senderName  = receiveDetailInfo.getSenderFirstName() + " " +
				receiveDetailInfo.getSenderLastName() + " " +
				receiveDetailInfo.getSenderLastName2();
			}
			senderPhone = receiveDetailInfo.getSenderHomePhone();
			recvName = recvTransaction.getReceiver().getFullName(); 
			message = receiveDetailInfo.getMessageField1();
			refNbr  = recvTransaction.getReferenceNumber();
			totalAmt = recvTransaction.getAmount().toString() 
			+ " (" +recvTransaction.getCurrency() + ")"; 
			auth = recvTransaction.getAuthorizationCode(); 
			break;
		}
		case ReprintData.REPRINT_UBP: {
			ubpTransaction = (MoneyGramSendTransaction)ReprintFile.readObject();
			if (ubpTransaction == null) {
				Debug.println("readTran() - ubpTransaction not found");
				return false;
			}

			SenderInfo detailLookupInfo = ubpTransaction.getSender();
			if (detailLookupInfo == null) {
				Debug.println("readTran() - detailLookupInfo not found");
				return false;
			}

			senderName  = detailLookupInfo.getFullName();
			senderPhone = ubpTransaction.formatPhoneNumber(detailLookupInfo.getHomePhone());
			recvName = ubpTransaction.getBillerInfo().getBillerName(); 
			message = ubpTransaction.getMessage1();
			refNbr = ubpTransaction.getCommitReferenceNumber(); 
			totalAmt = ubpTransaction.getTotalAmount().toString(); 
			auth = ""; 
			break;
		}
		}
		ReprintFile.closeInputFile();
		return true;
	}
    
	/*
	 * Reprint a receipt using the restored environment
	 */
    protected void reprintReceipt() {
    	
    	/*
    	 * Check for and use any PDF receipts first
    	 */
    	boolean bPdfAvailable = false;
    	for(int ii = 0; ii< PDF_RCPTS_TO_PRINT.length; ii++) {
    		if (PdfReceipt.isReprintReceiptFileAvailable(ii)) {
    			bPdfAvailable = true;
        		PdfReceipt pdfReceipt = new PdfReceipt(PDF_RCPTS_TO_PRINT[ii]);
				while (! pdfReceipt.print()) {
					if (Dialogs.showRetryCancel("dialogs/DialogReceiptReprintFailure.xml")== Dialogs.CANCEL_OPTION) {
						break;
					}
				}
    		}
    	}
    	/*
    	 * If there were PDF receipts found, exit now
    	 */
    	if (bPdfAvailable) {
    		return;
    	}

    	/*
    	 * Check for and use any PRT receipts next
    	 */
    	boolean bPrtAvailable = false;
    	for(int ii = 0; ii< PRT_RCPTS_TO_PRINT.length; ii++) {
    		if (PrtReceipt.isReprintReceiptFileAvailable(ii)) {
    			bPrtAvailable = true;
        		PrtReceipt prtReceipt = new PrtReceipt(PRT_RCPTS_TO_PRINT[ii]);
				while (!prtReceipt.print()) {
					if (Dialogs.showRetryCancel("dialogs/DialogReceiptReprintFailure.xml")== Dialogs.CANCEL_OPTION) {
						break;
					}
				}
    		}
    	}
    	/*
    	 * If there were RT receipts found, exit now
    	 */
    	if (bPrtAvailable) {
    		return;
    	}


    	switch (reprintHeader.getiRecptType()) {
	    	case ReprintData.REPRINT_SEND: {
	    		/*
	    		 * If the transaction is not present, a reprint has already
	    		 * been done, so read the transaction back in.
	    		 */
	    		if (sendTransaction == null) {
	    	        if (!ReprintReceiptFlowPage.readTran()) {
	    	        	ReprintFile.deleteFile(true);
	    	            Dialogs.showError("dialogs/DialogReprintUnavailable.xml");
	    	            PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
	    	            return;
	    	        }
	    		}
	    		MGSWizardPage14.reprint(sendTransaction);
	    		/*
	    		 * Don't leave the transaction laying around
	    		 */
	    		sendTransaction = null;
	    		break;
	    	}
	
	    	case ReprintData.REPRINT_RECV: {
	    		/*
	    		 * If the transaction is not present, a reprint has already
	    		 * been done, so read the transaction back in.
	    		 */
	    		if (recvTransaction == null) {
	    	        if (!ReprintReceiptFlowPage.readTran()) {
	    	        	ReprintFile.deleteFile(true);
	    	            Dialogs.showError("dialogs/DialogReprintUnavailable.xml");
	    	            PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
	    	            return;
	    	        }
	    		}
	    		MGRWizardPage9.reprint(recvTransaction);
	    		/*
	    		 * Don't leave the transaction laying around
	    		 */
	    		recvTransaction = null;
	    		break;
	    	}
	
	    	case ReprintData.REPRINT_UBP: {
	    		/*
	    		 * If the transaction is not present, a reprint has already
	    		 * been done, so read the transaction back in.
	    		 */
	    		if (ubpTransaction == null) {
	    	        if (!ReprintReceiptFlowPage.readTran()) {
	    	        	ReprintFile.deleteFile(true);
	    	            Dialogs.showError("dialogs/DialogReprintUnavailable.xml");
	    	            PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
	    	            return;
	    	        }
	    		}
	    		BPWizardPage17.reprint(ubpTransaction);
	    		/*
	    		 * Don't leave the transaction laying around
	    		 */
	    		ubpTransaction = null;
	    		break;
	    	}
    	}

    }
}
