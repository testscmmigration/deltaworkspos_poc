package dw.mgsend;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.moneygram.agentconnect.BPValidationResponse.Payload;
import com.moneygram.agentconnect.ProductVariantType;
import com.moneygram.agentconnect.SessionType;
import com.moneygram.agentconnect.StagedTransactionInfo;

import dw.accountdepositpartners.AccountDepositPartnersTransaction;
import dw.dwgui.DWTable;
import dw.dwgui.DWTable.DWTableListener;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.FieldKey;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;
import dw.profile.Profile;
import dw.profile.ProfileItem;
import dw.profile.UnitProfile;
import dw.utility.DWValues;

/**
 * "senderSelection"
 * Page 3 of the MoneyGram Send Wizard. This page displays a table of customers
 * that match the home phone number given. The user may choose zero or one of
 * the customers listed in the table and continue.
 */
public class MGSWizardPage2 extends MoneyGramSendFlowPage implements DWTextPaneListener, DWTableListener {
	private static final long serialVersionUID = 1L;

	private static final int MIN_SCREEN_WIDTH = 600;

	private DWTable table;
	private JScrollPane scrollPane;
    private JPanel panel;
    private DWTextPane tp1;
	private FormFreeTransactionTableModel tableModel = null;
	private StagedTransactionInfo stagedTransaction;
	private JLabel noTransactionsLabel;
	private JPanel transactionListPanel;
	private boolean mgsAuth;
	private boolean bpAuth;
	private int selectedTransactionIndex;
	private boolean initialValidationCallFlag;
	private SessionType transactionType;

	public MGSWizardPage2(AccountDepositPartnersTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		this((MoneyGramSendTransaction) tran, name, pageCode, buttons);
	}

	public MGSWizardPage2(MoneyGramSendTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super(tran, "moneygramsend/MGSWizardPage2.xml", name, pageCode, buttons); 

		scrollPane = (JScrollPane) this.getComponent("scrollpane1");
		panel = (JPanel) getComponent("panel");
		tp1 = (DWTextPane) getComponent("text1");
		table = (DWTable) getComponent("table");
		noTransactionsLabel = (JLabel) getComponent("noTransactionsLabel");
		transactionListPanel = (JPanel) getComponent("transactionListPanel");

		setupTable();
		table.addListener(this, this);
	}

	private void checkStatus() {
		int selectedRow = table.getTable().getSelectedRow();
		boolean enable = (selectedRow != -1);
		flowButtons.setEnabled("next", enable);
	}

	@Override
	public void dwTableResized() {
		dwTextPaneResized();

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.resizeTable();
			}
		});
	}

	@Override
	public void dwTextPaneResized() {
		int tableWidth = table.getTable().getPreferredSize().width;
		int screenWidth = scrollPane.getVisibleRect().width - 10;
		int actualWidth = Math.min(Math.max(tableWidth, MIN_SCREEN_WIDTH), screenWidth);

		tp1.setWidth(actualWidth);
	}

	@Override
	public void start(int direction) {
		flowButtons.reset();
		flowButtons.setVisible("expert", false); 
		flowButtons.setEnabled("back", false);

		transaction.setDataCollecionReview(false);
		noTransactionsLabel.setVisible(false);
		transactionListPanel.setVisible(false);
		checkStatus();

		table.setVisible(false);

		final JButton nextButton = flowButtons.getButton("next"); 
		final JButton backButton = flowButtons.getButton("back"); 
		final JButton cancelButton = flowButtons.getButton("cancel"); 

		KeyListener kl = new KeyAdapter() { 
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					e.consume();
					nextButton.doClick();
				}
				if (e.getKeyCode() == KeyEvent.VK_TAB) {
					e.consume();
					backButton.requestFocus();
				}
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					e.consume();
					cancelButton.doClick();
				}
				if (e.getKeyCode() == KeyEvent.VK_F2) {
					e.consume();
					transaction.f2SAR();
				}
			}
		};

		table.getTable().addKeyListener(kl);

		table.setListenerEnabled(true);

		transaction.clearFormFreeTransactionList();

		// Check if form free is enabled for sends and bill payments.

		transaction.setDocumentSequence(DWValues.MONEY_GRAM_SEND_DOC_SEQ);
        Profile profile = UnitProfile.getInstance().getProfileInstance();
        ProfileItem item = profile.find("PRODUCT", DWValues.MONEY_GRAM_SEND_DOC_SEQ);
        mgsAuth = item.getStatus().equalsIgnoreCase("A");
		boolean sendFormFree = (mgsAuth && transaction.isFormFreeEnabled());

        item = profile.find("PRODUCT", DWValues.BILL_PAYMENT_DOC_SEQ);
        bpAuth = item.getStatus().equalsIgnoreCase("A");
		transaction.setDocumentSequence(DWValues.BILL_PAYMENT_DOC_SEQ);
		boolean bpFormFree = (bpAuth && transaction.isFormFreeEnabled()); 
		
		transaction.clear(true);

		transactionType = null;
		if (sendFormFree && ! bpFormFree) {
			transactionType = SessionType.SEND;
		} else if (! sendFormFree && bpFormFree) {
			transactionType = SessionType.BP;
		}
		
		if (sendFormFree || bpFormFree) {
			transaction.searchStagedTransactions(this, transactionType, MoneyGramClientTransaction.SEARCH_STAGED_TRANSACTIONS);
		} else {
			displayTable();
		}
	}

	private class FormFreeTransactionTableModel extends DefaultTableModel {
		private static final long serialVersionUID = 1L;
		
		private List<StagedTransactionInfo> tableData;

		private FormFreeTransactionTableModel(String[] headers) {
			super(headers, 0);
		}

		private String getReceiverName(StagedTransactionInfo tran) {
			StringBuffer sb = new StringBuffer();
			if ((tran.getBillerName() != null) && (! tran.getBillerName().isEmpty())) {
				sb.append(tran.getBillerName());
				if (tran.getBillerAccountNumber() != null && ! tran.getBillerAccountNumber().isEmpty()) {
					sb.append(" (");
					sb.append(tran.getBillerAccountNumber());
					sb.append(")");
				}
			} else {
				if (tran.getReceiverLastName() != null) {
					sb.append(tran.getReceiverLastName());
				}
				if ((tran.getReceiverLastName2() != null) && (! tran.getReceiverLastName2().isEmpty())) {
					if (! sb.toString().isEmpty()) {
						sb.append(" ");
					}
					sb.append(tran.getReceiverLastName2());
				}
				if ((tran.getReceiverFirstName() != null) && (! tran.getReceiverFirstName().isEmpty())) {
					if (! sb.toString().isEmpty()) {
						sb.append(", ");
					}
					sb.append(tran.getReceiverFirstName());
				}
			}
			return sb.toString();
		}

		private void setData(List<StagedTransactionInfo> data) {
			int rowCount = this.getRowCount();
			for (int i = 0; i < rowCount; i++) {
				this.removeRow(0);
			}

			Collections.sort(data, new Comparator<StagedTransactionInfo>() {
				@Override
				public int compare(StagedTransactionInfo sti1, StagedTransactionInfo sti2) {
					int c = sti1.getSenderLastName().compareTo(sti2.getSenderLastName());
					if (c != 0) {
						return c;
					}
		
					String fn1 = sti1.getSenderFirstName() != null ? sti1.getSenderFirstName() : "";
					String fn2 = sti2.getSenderFirstName() != null ? sti2.getSenderFirstName() : "";
					c = fn1.compareTo(fn2);
					if (c != 0) {
						return c;
					}
		
					String name1 = getReceiverName(sti1);
					String name2 = getReceiverName(sti2);
					c = name1.compareTo(name2);
					if (c != 0) {
						return c;
					}
		
					return 0;
				}
			});
			tableData = data;

			for (StagedTransactionInfo tran : data) {
				
				// Sender Name
				
				StringBuffer sb = new StringBuffer();
				sb.append(tran.getSenderLastName());
				if ((tran.getSenderFirstName() != null) && (! tran.getSenderFirstName().isEmpty())) {
					if (! sb.toString().isEmpty()) {
						sb.append(", ");
					}
					sb.append(tran.getSenderFirstName());
				}
				String senderName = sb.toString();
				
				// Sender Location
				
				sb.setLength(0);
				if (tran.getSenderCity() != null) {
					sb.append(tran.getSenderCity());
				}
				if ((tran.getSenderCountrySubdivision() != null) && (! tran.getSenderCountrySubdivision().isEmpty())) {
					if (! sb.toString().isEmpty()) {
						sb.append(", ");
					}
					int i = tran.getSenderCountrySubdivision().indexOf("-") + 1;
					sb.append(tran.getSenderCountrySubdivision().substring(i));
				}
				if ((tran.getSenderCountry() != null) && (! tran.getSenderCountry().isEmpty())) {
					if (! sb.toString().isEmpty()) {
						sb.append(", ");
					}
					sb.append(tran.getSenderCountry());
				}
				String senderAddress = sb.toString();
				
				// Receiver Name
				
				String receiverName = getReceiverName(tran);
				
				// Destination Location

				sb.setLength(0);
				if ((tran.getDestinationCountrySubdivision() != null) && (! tran.getDestinationCountrySubdivision().isEmpty())) {
					int i = tran.getDestinationCountrySubdivision().indexOf("-") + 1;
					sb.append(tran.getDestinationCountrySubdivision().substring(i));
				}
				if ((tran.getDestinationCountry() != null) && (! tran.getDestinationCountry().isEmpty())) {
					if (! sb.toString().isEmpty()) {
						sb.append(", ");
					}
					sb.append(tran.getDestinationCountry());
				}
				String receiverAddress = sb.toString();
				
				this.addRow(new String[] {senderName, senderAddress, receiverName, receiverAddress});
			}
			this.fireTableDataChanged();
			dwTableResized();
		}

		public StagedTransactionInfo getTableRow(int i) {
			return tableData.get(i);
		}
	}

	private void setupTable() {

		String[] headers = {
				Messages.getString("MGSWizardPage2.2"),
				Messages.getString("MGSWizardPage2.3"),
				Messages.getString("MGSWizardPage2.4"),
				Messages.getString("MGSWizardPage2.5")
			};

		tableModel = new FormFreeTransactionTableModel(headers) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};

		table.getTable().setModel(tableModel);
		ListSelectionModel listSelectionModel = table.getTable().getSelectionModel();
		listSelectionModel.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				checkStatus();
			}
		});

		TableColumnModel model = table.getTable().getColumnModel();
		table.getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    	for (int i = 0; i < model.getColumnCount(); i++) {
    		TableColumn column = model.getColumn(i);
        	column.setCellRenderer(table.new DWTableCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));
    	}
	}

	@Override
	public void finish(int direction) {
		if (direction == PageExitListener.NEXT) {
			int selectedRow = table.getTable().getSelectedRow();
			if (selectedRow != -1) {
				stagedTransaction = tableModel.getTableRow(selectedRow);
				transaction.setFormFreeTransaction(stagedTransaction);
				transaction.getProfileValues();
				selectedTransactionIndex = selectedRow;

				if (stagedTransaction.getMgiSessionType() == null) {
					stagedTransaction.setMgiSessionType(transactionType);
				}
				
				String purposeOfLookup = null;
				if (stagedTransaction.getMgiSessionType().equals(SessionType.SEND)) {
					purposeOfLookup = DWValues.TRX_LOOKUP_SEND_COMP;
					transaction.setDocumentSequence(DWValues.MONEY_GRAM_SEND_DOC_SEQ);
				} else if (stagedTransaction.getMgiSessionType().equals(SessionType.BP)) {
					purposeOfLookup = DWValues.TRX_LOOKUP_BP_COMP;
					transaction.setDocumentSequence(DWValues.BILL_PAYMENT_DOC_SEQ);
				}
				if (purposeOfLookup != null) {
					transaction.transactionLookup(stagedTransaction.getConfirmationNumber(), this, purposeOfLookup, false);
				} else {
					PageNotification.notifyExitListeners(this, direction);
				}
			} else {
				selectedTransactionIndex = 0;
			}
		} else {
			PageNotification.notifyExitListeners(this, direction);
		}
		table.setListenerEnabled(false);
	}

	private void displayTable() {
		tableModel.setData(transaction.getFormFreeTransactionList());
		if (tableModel.getRowCount() > 0) {
			table.getTable().setRowSelectionInterval(0, 0);
			table.setVisible(true);
			table.getTable().requestFocus();
			transactionListPanel.setVisible(true);
			dwTextPaneResized();
			table.initializeTable(scrollPane, panel, MIN_SCREEN_WIDTH, 10);
			table.setColumnStaticWidth(3);
			dwTableResized();
			table.getTable().setRowSelectionInterval(selectedTransactionIndex, selectedTransactionIndex);
		} else {
			noTransactionsLabel.setVisible(true);
		}
	}

	@Override
	public void commComplete(int commTag, Object returnValue) {
		boolean b = ((Boolean) returnValue).booleanValue();
		if (commTag == MoneyGramClientTransaction.SEARCH_STAGED_TRANSACTIONS) {
			if (b) {
				displayTable();
			} else {
				if ((stagedTransaction == null) || (stagedTransaction.getMgiSessionType() == null) || (stagedTransaction.getMgiSessionType().equals(SessionType.SEND))) {
					transaction.setFormFreeStatus(MoneyGramClientTransaction.FORM_FREE_SEND_ERROR);
				} else if (stagedTransaction.getMgiSessionType().equals(SessionType.BP)) {
					transaction.setFormFreeStatus(MoneyGramClientTransaction.FORM_FREE_XPAY_ERROR);
				}
				flowButtons.setEnabled("cancel", true);
				PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
			}
				
		} else if (commTag == MoneyGramClientTransaction.TRANSACTION_LOOKUP) {
			if (b) {
				transaction.setMgiTransactionSessionId(transaction.getTransactionLookupResponsePayload().getMgiSessionID());
				BigDecimal amount = transaction.getTransactionLookupResponsePayload().getSendAmounts().getSendAmount();
				if (stagedTransaction.getMgiSessionType().equals(SessionType.SEND)) {
					transaction.setBillPaymentTransaction(false);
					String destinationCountry = stagedTransaction.getDestinationCountry();
					String deliveryOption = transaction.getDataCollectionData().getCurrentValue(FieldKey.SERVICEOPTION_KEY.getInfoKey());
					String sendCurrency = transaction.getTransactionLookupResponsePayload().getSendAmounts().getSendCurrency();
					String receiveCurrency = transaction.getTransactionLookupResponsePayload().getReceiveAmounts().getReceiveCurrency();
					initialValidationCallFlag = true;
					String receiveAgentId = transaction.getDataCollectionData().getCurrentValue(FieldKey.RECEIVE_AGENTID_KEY.getInfoKey());
			        boolean flag = transaction.isOKToPrintDF();
			        if (flag) {
						transaction.sendValidation(this, amount, destinationCountry, deliveryOption, sendCurrency, receiveCurrency, receiveAgentId, ValidationType.INITIAL_FORM_FREE);
			        } else {
						flowButtons.setButtonsEnabled(true);
			        	return;
			        }
				} else {
					transaction.setBillPaymentTransaction(true);
					transaction.setAmount(amount);
					transaction.setSendCurrency(transaction.getTransactionLookupResponsePayload().getSendAmounts().getSendCurrency());
					transaction.setReceiveCurrency(transaction.getTransactionLookupResponsePayload().getReceiveAmounts().getReceiveCurrency());
//					String destinationCountry = stagedTransaction.getDestinationCountry();
					String pv = transaction.getDataCollectionData().getCurrentValue(FieldKey.PRODUCTVARIANT_KEY.getInfoKey());
					transaction.setProductVariant(ProductVariantType.fromValue(pv));
					initialValidationCallFlag = true;
			        boolean flag = transaction.isOKToPrintDF();
			        if (flag) {
						transaction.bpValidation(this, ValidationType.INITIAL_FORM_FREE);
			        } else {
						flowButtons.setButtonsEnabled(true);
			        	return;
			        }
				}
			} else {
				flowButtons.setEnabled("cancel", true);
				PageNotification.notifyExitListeners(this, PageExitListener.DONOTHING);
			}
				
		} else if (commTag == MoneyGramClientTransaction.SEND_VALIDATE) {
			if (b) {
				if (initialValidationCallFlag && (! transaction.haveFieldsToCollect())) {
					initialValidationCallFlag = false;
					transaction.validation(this);
				} else {
					PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
				}
			} else {
				flowButtons.setEnabled("cancel", true);
				PageNotification.notifyExitListeners(this, PageExitListener.DONOTHING);
			}
				
		} else if (commTag == MoneyGramClientTransaction.BP_VALIDATE) {
			if (b) {
				Payload payload = transaction.getBpValidationResponsePayload();
				boolean isReadyForCommit = payload != null ? payload.isReadyForCommit() : false;
				if (initialValidationCallFlag && (! transaction.haveFieldsToCollect()) && (! isReadyForCommit)) {
					initialValidationCallFlag = false;
					transaction.validation(this);
				} else {
					PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
				}
			} else {
				flowButtons.setEnabled("cancel", true);
				PageNotification.notifyExitListeners(this, PageExitListener.DONOTHING);
			}
		}
	}
}
