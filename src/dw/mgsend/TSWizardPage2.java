package dw.mgsend;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JLabel;

import com.moneygram.agentconnect.TransactionLookupResponse.Payload;

import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.FieldKey;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.utility.DWValues;

/**
 * Page 2 of the Transaction Status. This page displays the details of the
 *   selected transaction.  
 */
public class TSWizardPage2 extends MoneyGramSendFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

    private JLabel authCodeLabel;
    private JLabel authCode;
	private List<DWTextPane> textPanes;
	private int tagWidth;

    public TSWizardPage2(MoneyGramSendTransaction tran,
                       String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "moneygramsend/TSWizardPage2.xml", name, pageCode, buttons);	
		textPanes = new ArrayList<DWTextPane>();
        authCodeLabel = (JLabel) getComponent("authCodeLabel");
        authCode = (JLabel) getComponent("authCode");
    }

	@Override
	public void dwTextPaneResized() {
		
		int w = FIELD_TOTAL_WIDTH - tagWidth;
		for (DWTextPane pane : textPanes) {
			pane.setWidth(w);
		}
	}

	@Override
	public void start(int direction) {
        flowButtons.reset();
        flowButtons.setVisible("expert", false);	
        flowButtons.setVisible("cancel", false);	
        flowButtons.getButton("cancel").setEnabled(false);	
        flowButtons.setAlternateText("next",Messages.getString("OKEnter"));     
        flowButtons.setMnemonic("next", 'O'); 
        flowButtons.setSelected("next");	

        Payload lookupResponse = transaction.getTransactionLookupResponsePayload();
        Map<String, String> currentValues = transaction.getDataCollectionData().getCurrentValueMap();
        
    	List<JLabel> tags = new ArrayList<JLabel>();
        
        String firstName = currentValues.get(FieldKey.SENDER_FIRSTNAME_KEY.getInfoKey());
        String middleName = currentValues.get(FieldKey.SENDER_MIDDLENAME_KEY.getInfoKey());
        String lastName1 = currentValues.get(FieldKey.SENDER_LASTNAME_KEY.getInfoKey());
        String lastName2 = currentValues.get(FieldKey.SENDER_LASTNAME2_KEY.getInfoKey());
        String name = DWValues.formatName(firstName, middleName, lastName1, lastName2);
		this.displayPaneItem("senderName", name, tags, textPanes);
        
        String phone = currentValues.get(FieldKey.SENDER_PRIMARYPHONE_KEY.getInfoKey());
		String tag = transaction.getTransactionLookupInfoLabel(FieldKey.SENDER_PRIMARYPHONE_KEY.getInfoKey());
        this.displayLabelItem("senderHomePhone", tag, phone, tags);

        String s = currentValues.get(FieldKey.DATETIMESENT_KEY.getInfoKey());  
		tag = transaction.getTransactionLookupInfoLabel("dateTimeSent");
        this.displayLabelItem("sent", tag, FormatSymbols.formatDateTimeForLocaleWithTimeZone(s), tags);
        
        firstName = currentValues.get(FieldKey.RECEIVER_FIRSTNAME_KEY.getInfoKey());
        middleName = currentValues.get(FieldKey.RECEIVER_MIDDLENAME_KEY.getInfoKey());
        lastName1 = currentValues.get(FieldKey.RECEIVER_LASTNAME_KEY.getInfoKey());
        lastName2 = currentValues.get(FieldKey.RECEIVER_LASTNAME2_KEY.getInfoKey());
        name = DWValues.formatName(firstName, middleName, lastName1, lastName2);
		this.displayPaneItem("receiverName", name, tags, textPanes);
        
        String m1 = currentValues.get(FieldKey.MESSAGEFIELD1_KEY.getInfoKey());
        String m2 = currentValues.get(FieldKey.MESSAGEFIELD2_KEY.getInfoKey());
        String message = ((m1 != null ? m1 : "") + " " + (m2 != null ? m2 : "").trim()); 
		this.displayPaneItem("message", message, tags, textPanes);

        BigDecimal sendAmount = lookupResponse.getSendAmounts().getSendAmount();
        String sendCurrency = lookupResponse.getSendAmounts().getSendCurrency();
        this.displayMoney("sendAmount", sendAmount, sendCurrency, tags);
        
        BigDecimal receiveAmount = lookupResponse.getReceiveAmounts().getReceiveAmount();
        String receiveCurrency = lookupResponse.getReceiveAmounts().getPayoutCurrency();  
        this.displayMoney("receiveAmount", receiveAmount, receiveCurrency, tags);
		
        this.displayLabelItem("reference", transaction.getTsReferenceNumber(), tags);
        
        String status = getTranactionStatus(lookupResponse.getTransactionStatus(), transaction.getDataCollectionData().getCurrentValueMap(), true, false);
        this.displayPaneItem("status", status, false, tags, textPanes); 
        
        String authorizationNumber = currentValues.get(FieldKey.FIRSTCHECKAUTH_KEY.getInfoKey());
        if ((authorizationNumber != null) && (! authorizationNumber.isEmpty())) {
        	authCodeLabel.setVisible(true);
			authCode.setVisible(true);
        	authCode.setText(authorizationNumber);
        } else {
        	authCodeLabel.setVisible(false);
			authCode.setVisible(false);
        }
        
		tagWidth = this.setTagLabelWidths(tags, MINIMUM_TAG_WIDTH);

        dwTextPaneResized(); 
    }

    @Override
	public void finish(int direction) {
        if (direction == PageExitListener.NEXT)
            direction = PageExitListener.COMPLETED;
        PageNotification.notifyExitListeners(this, direction);
    }
}
