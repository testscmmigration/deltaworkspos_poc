package dw.mgsend;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableModel;

import com.moneygram.agentconnect.FeeInfo;

import dw.accountdepositpartners.AccountDepositPartnersTransaction;
import dw.dialogs.Dialogs;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;
import dw.model.adapters.CountryInfo.Country;
import dw.model.adapters.ReceiverInfo;
import dw.model.adapters.SenderInfo;
import dw.profile.UnitProfile;
import dw.utility.DWValues;
import dw.utility.ServiceOption;

/**
 * "msfeeInformation"
 * Page 3 of the MoneyGram Send Wizard. This page displays a table of customers
 *   that match the home phone number given. The user may choose zero or one
 *   of the customers listed in the table and continue.
 */
public class MGSWizardPage5a extends MGSWizardAbstractFeeInformation {
	private static final long serialVersionUID = 1L;

	@Override
	protected ListSelectionListener getSharedListSelectionHandlerInstance(JTable table) {
		return new SharedListSelectionHandler(table);
	}

	class SharedListSelectionHandler implements ListSelectionListener {
		JTable table;
		
		public SharedListSelectionHandler(JTable table) {
			this.table = table;
		}

		@Override
		public void valueChanged(ListSelectionEvent e) {
			try {
				if (blockEvents) {
					// already handling a previous event
					return;
				}
				blockEvents = true;
				
				int newFeeTblIdx = updateSelection(table);
				blockEvents = false;
				
				if (newFeeTblIdx < 0) {
					blockEvents = false;
					return;
				}
				
				/*
				 * If there was a change
				 */
				if (newFeeTblIdx != lastSelectedIndex) {

					ServiceOption deliveryInfo = getDeliveryInfo(feeInfos.get(newFeeTblIdx));
				  	transaction.setHistoryQDO(deliveryInfo);
				  	
					if (transaction.getSelectedDeliveryInfo() != null && deliveryInfo != null) {
						boolean showDialog = false;
						SenderInfo sender = transaction.getSender();
					  	ReceiverInfo receiver = transaction.getReceiver();

					  	if (canDisplayServiceOptionChangeWarning && (! transaction.getSelectedDeliveryInfo().compareDeliveryInfo(deliveryInfo))) {
							if (sender != null && sender.getFirstName() != null && sender.getFirstName().length() > 0 && 
									receiver != null && receiver.getFirstName() != null && receiver.getFirstName().length() > 0) {
								if (! transaction.isServiceOptionChanged()) {
									showDialog = true;
								}
							} else {
								showDialog = true;
							}
					  	}
						
						if (showDialog) {							
							int option = showDialog();
							if (option == Dialogs.YES_OPTION) {
								canDisplayServiceOptionChangeWarning = false;
								transaction.setSelectedFeeTableIndex(newFeeTblIdx);
								selectTableItemBasedOnFeeInfoIdx(newFeeTblIdx);
								JButton nextButton = flowButtons.getButton("next"); 
								nextButton.doClick();
								return;
							} else if (option == Dialogs.NO_OPTION) {
								canDisplayServiceOptionChangeWarning = false;
								selectTableItemBasedOnFeeInfoIdx(transaction.getSelectedFeeTableIndex());
								JButton nextButton = flowButtons.getButton("next"); 
								nextButton.doClick();
								return;
							}
							selectTableItemBasedOnFeeInfoIdx(transaction.getSelectedFeeTableIndex());
							return;
						}
					}
					
					setupDeliveryOptionData();
					setRegisterAvailability();

					// for promo applicability
					setFeeDetailsEnableStatus();
					// for receive applicability
					setRecvInfoEnableStatus();
					lastSelectedIndex = newFeeTblIdx;
				}
				transaction.setFeeReturnInfo(feeInfos.get(newFeeTblIdx));
				transaction.setSelectedFeeTableIndex(newFeeTblIdx);
				
				String str = feeInfos.get(newFeeTblIdx).getServiceOptionDisplayDescription().trim();
				if (str != null && str.length() > 0 && messageTextPane != null) {
					messagePanel.setVisible(true);
					messageTextPane.setVisible(true);
					messageTextPane.setText(str);
				} else {
					messagePanel.setVisible(false);
					messageTextPane.setVisible(false);
					messageTextPane.setText("");
				}
				dwTextPaneResized();
			} finally {
				blockEvents = false;
			}
		}

		private ServiceOption getDeliveryInfo(FeeInfo feeMsgMC) {
//			ServiceOption deliveryInfo = new ServiceOption();
//			deliveryInfo.setServiceOption(feeMsgMC.getServiceOption());
//			deliveryInfo.setDestinationCountry(feeMsgMC.getDestinationCountry());
//			String receiveCurrency = feeMsgMC.getReceiveAmounts() != null ? feeMsgMC.getReceiveAmounts().getReceiveCurrency() : null;
//			deliveryInfo.setReceiveCurrency(receiveCurrency);
//			deliveryInfo.setReceiveAgentAbbreviation(feeMsgMC.getReceiveAgentAbbreviation());
//			deliveryInfo.setReceiveAgentID(feeMsgMC.getReceiveAgentID());
			ServiceOption deliveryInfo = new ServiceOption(feeMsgMC);
			return deliveryInfo;
		}

		private int updateSelection(JTable selectedTable) {
			int newFeeTblIdx;
			int tblRow = selectedTable.getSelectedRow();
			TableModel tableModel = selectedTable.getModel();
			if (tblRow < 0) {
				return tblRow;
			}
			selectedTable.requestFocus();
			
			clearTables(selectedTable);
			List<Integer> datalist = dataModelMap.get(tableModel);
			if (datalist != null) {
				newFeeTblIdx = (datalist.get(tblRow)).intValue();
				return newFeeTblIdx;
			}
			
			return -1;	
		}

		/**
		 * @return the table
		 */
		public JTable getTable() {
			return table;
		}

		/**
		 * @param table the table to set
		 */
		public void setTable(JTable table) {
			this.table = table;
		}
	}
    
	private boolean canDisplayServiceOptionChangeWarning;
    
	public MGSWizardPage5a(MoneyGramSendTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
        super(tran, name, pageCode, buttons);	
    }
    
	public MGSWizardPage5a(AccountDepositPartnersTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
    	super(tran, name, pageCode, buttons);	
	}
    
	/**
     *   Select the correct item from the correct table base on a FeeInfo
     *   index.
     */
	@Override
	protected void selectTableItemBasedOnFeeInfoIdx(int index) {
		super.selectTableItemBasedOnFeeInfoIdx(index);
		setupDeliveryOptionData();
		setRegisterAvailability();
	}

	private void setInitialFocus() {
		int tblRow;
		boolean isSelected = false;
		if (tableMap.size() > 0) {
			for (Map.Entry<Integer, JTable> entry : tableMap.entrySet()) {
				JTable table = tableMap.get(entry.getKey());
				if (!isSelected) {
					tblRow = table.getSelectedRow();
					if (tblRow >= 0) {
						table.requestFocus();
						isSelected = true;
					}
				}
			}
		} 
	}
    
	@Override
	public void start(int direction) {
		boolean currentBlockEvents = this.blockEvents;
		this.blockEvents = true;
		setupComponents();
		feeInfos = transaction.getFeeInfo();
		
		if (direction != PageExitListener.BACK) {
			if (transaction.isFeeQuery()) {
				lastSelectedIndex = transaction.getSelectedFeeTableIndex();  
			} else {
				lastSelectedIndex = -1;  //Force 1st selection to be 'a change'
			}
		
		} 

		if ((direction == PageExitListener.NEXT || direction == PageExitListener.BACK)) {
			populateFeeData();
			boolean flag = transaction.isFeeQuery() || transaction.isSendToAccount();
			populateFeeTable(flag, direction);
			transaction.setFeeReturnInfo(feeInfos.get(getFeeIdx()));
			setQdoInfo(direction);
		}

		cancelButton = flowButtons.getButton("cancel"); 
		flowButtons.reset();
		flowButtons.setVisible("expert", false); 

		addActionListener("feeDetailsButton", this, "feeDetails");
		addActionListener("recvDetailsButton", this, "recvDetails");

		destinationCountryLabel.setText(transaction.getDestination().toString().toUpperCase());
		String currency = transaction.getSendCurrency();
		sendCurrencyLabel.setText(currency);

		if (! UnitProfile.getInstance().isMultiAgentMode()) {
			subagentCreditLimitBorder.setVisible(false);
			subagentCreditLimitValueLabel.setVisible(false);
			subagentCreditLimitTextLabel.setVisible(false);
		} else {
			BigDecimal saLimit = transaction.getSaLimitAvailable();
			if (saLimit != null) {
				subagentCreditLimitValueLabel.setText(saLimit + " " + currency);
			}
		}

		// set up the enter key for the table

		setupDeliveryOptionData();

		transaction.setDestinationChanged(false);

		if (transaction.isAnyModifiedFees()
				|| (transaction.getReturnedPromoCodeCount() > 0)
				|| (transaction.getEnteredPromoCodeCount() > 0)) {
			feeDetailsPanel.setVisible(true);
			setFeeDetailsEnableStatus();
		} else {
			feeDetailsPanel.setVisible(false);
		}
		if (transaction.isAnyModifiedRecvAmts()) {
			recvDetailsPanel.setEnabled(true);
			recvDetailsPanel.setVisible(true);
			setRecvInfoEnableStatus();
		} else {
			recvDetailsPanel.setEnabled(false);
			recvDetailsPanel.setVisible(false);
		}
		setRegisterAvailability();

		setInitialFocus();
		dwTableResized();
		dwTextPaneResized();
		if (direction == PageExitListener.NEXT) {
			this.blockEvents = false;
			canDisplayServiceOptionChangeWarning = true;
		} else {
			this.blockEvents = currentBlockEvents;
		}
	}
	
	private void setQdoInfo(int direction) {
		int index = -1;
		ServiceOption qdo = transaction.getHistoryQDO();
		// Destination of "(New Receiver)" in sender list may be empty 
		if ((qdo != null) && (qdo.getDestinationCountry() != null) && (direction == PageExitListener.NEXT)) {
			for (int i = 0; i < feeInfos.size(); i++) {
				ServiceOption so = new ServiceOption(feeInfos.get(i));
				if (qdo.compareTo(so) == 0) {
					index = i;
					break;
				}
			}
			// match up the user specified delivery option
			selectTableItemBasedOnFeeInfoIdx(index);
		}
		// needed to update for "finish" selection
		if (index >= 0) {
			transaction.setSelectedFeeTableIndex(index);
		}
	}

	@Override
	public void finish(int direction) {
		if (direction == PageExitListener.NEXT) {
			
			transaction.setValidationStatus(DataCollectionStatus.VALIDATION_NOT_PERFORMED);

			int selectedRow = getFeeIdx();
			transaction.setMgiTransactionSessionId(transaction.getFeeInfo().get(selectedRow).getMgiSessionID());
			
			boolean hasErrorData = feeInfos.get(selectedRow).getErrorInfo() != null; 
			boolean isRecoverable = hasErrorData ? feeInfos.get(selectedRow).getErrorInfo().isRecoverableFlag() : false; 
			
			if (hasErrorData && !isRecoverable) {
				String lang = DWValues.sConvertLang2ToLang3(UnitProfile.getInstance().get("LANGUAGE", "en"));
				Dialogs.acErrorDialog(
						Messages.getString("DialogAcError.InvalidSelection"),
						""+feeInfos.get(selectedRow).getErrorInfo().getErrorCode(),
						getErrorMessageText(feeInfos.get(selectedRow), lang));
				flowButtons.setButtonsEnabled(true);
	            return;
			}

			if (!transaction.isInlineDSR()) {
				transaction.setRegistrationNumber("");
				transaction.setCustomerReceiveNumber("");
			}

			Country country = transaction.getDestination();
			transaction.setDestination(country);
			transaction.setDestOrDOChanged(true);

			ServiceOption historyQdo = transaction.getHistoryQDO();
			if (((historyQdo == null) || (!transaction.getHistoryQDO().equals(qdo)))) {
				transaction.setRegistrationNumber("");
				transaction.setCustomerReceiveNumber("");
			}
			transaction.setQualifiedDeliveryOption(qdo);
	        transaction.setReceiveCurrency(qdo.getPayoutCurrency());
	        boolean flag = transaction.isOKToPrintDF();
	        if (flag) {
				transaction.sendValidation(this, transaction.getAmount(), transaction.getDestinationCountry(), qdo.getServiceOption(), qdo.getReceiveAgentID(), ValidationType.INITIAL_NON_FORM_FREE);
	        } else {
				flowButtons.setButtonsEnabled(true);
	        	return;
	        }

		} else if (direction == PageExitListener.CANCELED) {
			PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
		} else if (direction == PageExitListener.BACK) {
			transaction.setRegistrationNumber("");
			transaction.setCustomerReceiveNumber("");
			transaction.setReceiveAgentID("");
			PageNotification.notifyExitListeners(this, PageExitListener.BACK);
		} else {
			PageNotification.notifyExitListeners(this, direction);
		}
	}

	@Override
	protected void populateFeeOptionsTableWithSelectedRow(FeeInfo fees) {
    	resetTable();    	
		String[] row = transaction.populateFeeOptionsTableWithSelectedRow(fees);
		if(row!=null)
		{
			List<String[]> rows = new ArrayList<String[]>();
			rows.add(row);
			setupLists(rows);
			setupTable();
			List<String> rowList = new ArrayList<String>();
			for (int j = 0; j < row.length; j++) {
				if (j >1 && row[j] != null) {
					
					rowList.add(row[j].trim());
				}
			}
			allTableModel.addRow(rowList.toArray());
		}
	}

	private void setRegisterAvailability() {
		int selectedRow = getFeeIdx();
		feeInfos = transaction.getFeeInfo();
		qdo = transaction.getQualifiedDeliveryOption();
		FeeInfo feeInfo = feeInfos.get(selectedRow);
		qdo.setPayoutCurrency(transaction.getValidReceiveCurrency(feeInfo));
		String receiveCurrency = feeInfo.getReceiveAmounts() != null ? feeInfo.getReceiveAmounts().getReceiveCurrency() : null;
		qdo.setReceiveCurrency(receiveCurrency);
		transaction.setIsDss(feeInfo.getServiceOptionCategoryId() != null
				? feeInfo.getServiceOptionCategoryId().equals("2") : false);
	}

	  /**
	   *This method is invoked when the Find Button is clicked.  It sets the flag to
	   *true in transaction to change the flow in MoneyGramSendWizard class.
	   */
	 public void registration() {
	 }

  /**
   * Callback from transaction after the comm is finished. If the comm was
   *   successful, go to the next page. Otherwise, go back to the card number
   *   field.
   */
	@Override
	public void commComplete(int commTag, Object returnValue) {
		boolean b = ((Boolean) returnValue).booleanValue();

		if (commTag == MoneyGramClientTransaction.SEND_VALIDATE) {
			if (b) {
				DataCollectionStatus validationStatus = transaction.getValidationStatus();
				String nextPage = transaction.nextDataCollectionScreen(MoneyGramSendWizard.MSW10_MG_SEND_FEE_INFORMATION);
				int businessErrorCount = transaction.getDataCollectionData().getBusinessErrors() != null ? transaction.getDataCollectionData().getBusinessErrors().size() : 0;
				int additionalDataFieldCount = (transaction.getDataCollectionData().getAdditionalDataCollectionFields() != null ? transaction.getDataCollectionData().getAdditionalDataCollectionFields().size() : 0);
				boolean rtc = transaction.getSendValidationResponsePayload() != null ? transaction.getSendValidationResponsePayload().isReadyForCommit() : false;

				if (validationStatus.equals(DataCollectionStatus.ERROR)) {
					PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
					flowButtons.setButtonsEnabled(true);
					flowButtons.setVisible("expert", false);
//				} else if (((! transaction.isAfterValidationAttempt()) && (nextPage.isEmpty())) || ((validationStatus.equals(DataCollectionStatus.VALIDATED)) && (nextPage.isEmpty()) && (businessErrorCount == 0) && (additionalDataFieldCount == 0))) {
				} else if ((nextPage.isEmpty()) && (businessErrorCount == 0) && (additionalDataFieldCount == 0) && (! rtc)) {
//					String receiveAgentId = transaction.getDataCollectionData().getCurrentValue(FieldKey.RECEIVE_AGENTID_KEY.getInfoKey());
					String receiveAgentId = qdo != null ? qdo.getReceiveAgentID() : null;
					String serviceOption = qdo != null ? qdo.getServiceOption() : null;
					transaction.sendValidation(this, transaction.getAmount(), transaction.getDestinationCountry(), serviceOption, transaction.getSendCurrency(), transaction.getReceiveCurrency(), receiveAgentId, ValidationType.SECONDARY);
//					transaction.setAfterValidationAttempt(true);
				} else {
					PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
				} 
			} else {
				transaction.setEditActive(true);
				PageNotification.notifyExitListeners(this, PageExitListener.DONOTHING);
				flowButtons.setButtonsEnabled(true);
				flowButtons.setVisible("expert", false);
			}
			
		} else {
			// start(PageExitListener.NEXT);
			PageNotification.notifyExitListeners(this, PageExitListener.DONOTHING);
			flowButtons.setButtonsEnabled(true);
			flowButtons.setVisible("expert", false);
		}
	}

  	private int showDialog() {
		int option = -1;
		option = Dialogs.showYesNoCancel("dialogs/DialogServiceOptionChanged.xml"); 	
		return option;
	}
}
