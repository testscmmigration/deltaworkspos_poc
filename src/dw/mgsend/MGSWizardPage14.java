package dw.mgsend;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import dw.accountdepositpartners.AccountDepositPartnersTransaction;
import dw.dialogs.Dialogs;
import dw.dialogs.FeeDetailsDialog;
import dw.dialogs.RecvDetailsDialog;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.KeyMapManager;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.utility.Debug;
import dw.utility.IdleBackoutTimer;

/**
 * "receiptPrinting"
 *  Page 14 of the MoneyGram Send Wizard. This page displays the results of the
 *   send info and prints the agent and customer receipts.
 */
/**  
 *  Modified to print both receipts on same screen as in expert mode.
 */
public class MGSWizardPage14 extends MoneyGramSendFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

    private JLabel receiveAmountLabel;
    private JLabel feeLabel;
    private JLabel newCardDirectionsLabel;
    private JButton reprintButton;
    private JLabel reprintLabel;
    private JLabel printingLabel;
    private JLabel instruction1Label;
    private JLabel instruction2Label;
    private JLabel instruction3Label;
    private JLabel rule3continued;
    private JLabel rule3continued2;
    private JLabel instruction4Label;
            
	private JPanel feeDetailsPanel;
	private DWTextPane feeDetailsTextpane;
	private JButton feeDetailsButton;

	private JPanel recvDetailsPanel;
	private DWTextPane recvDetailsTextpane;
	private JButton recvDetailsButton;
	
    private DWTextPane DoddFrankTextpane;
    private DWTextPane IdNoticeTextpane;
	
	private JScrollPane scrollPane;
	private int tagWidth;
	
	public MGSWizardPage14(MoneyGramSendTransaction tran,
	    String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "moneygramsend/MGSWizardPage14.xml", name, pageCode, buttons);	
        init();
    }
	
	public MGSWizardPage14(AccountDepositPartnersTransaction tran,
	    String name, String pageCode, PageFlowButtons buttons) {
		super(tran, "moneygramsend/MGSWizardPage14.xml", name, pageCode, buttons);	
	    init();
	}

	private void init() {
		receiveAmountLabel = (JLabel) getComponent("receiveAmountTagLabel");	
		feeLabel = (JLabel) getComponent("feeTagLabel");	
		newCardDirectionsLabel = (JLabel) getComponent("newCardDirectionsLabel");	
		reprintButton = (JButton) getComponent("reprintButton");
		reprintLabel = (JLabel) getComponent("reprintLabel");
		printingLabel = (JLabel) getComponent("printingLabel");
		instruction1Label = (JLabel) getComponent("instruction1Label");
		instruction2Label = (JLabel) getComponent("instruction2Label");
		instruction3Label = (JLabel) getComponent("instruction3Label");        
		rule3continued= (JLabel) getComponent("rule3continued");
		rule3continued2= (JLabel) getComponent("rule3continued2");
		instruction4Label = (JLabel) getComponent("instruction4Label");
		DoddFrankTextpane = (DWTextPane) getComponent("DoddFrankTextpane");
		DoddFrankTextpane.setForeground(Color.BLUE);
		
		IdNoticeTextpane = (DWTextPane) getComponent("IdNoticeTextpane");
		IdNoticeTextpane.setForeground(Color.BLUE);
	
		recvDetailsPanel = (JPanel) getComponent("recvDetailsPanel");
		recvDetailsButton = (JButton) getComponent("recvDetailsButton");
		recvDetailsTextpane = (DWTextPane) getComponent("recvDetailsTextpane");
		
		feeDetailsPanel = (JPanel) getComponent("feeDetailsPanel");
		feeDetailsButton = (JButton) getComponent("feeDetailsButton");
		feeDetailsTextpane = (DWTextPane) getComponent("feeDetailsTextpane");
		
		scrollPane = (JScrollPane) getComponent("scrollPane");
		scrollPane.setBorder(BorderFactory.createEmptyBorder());
		scrollPane.setViewportBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
	
		DoddFrankTextpane.addListener(this, this);
	}
    
	@Override
	public void dwTextPaneResized() {
		int width = getComponent("panel1").getPreferredSize().width;
		feeDetailsTextpane.setWidth(width - 110);
		recvDetailsTextpane.setWidth(width - 110);
		IdNoticeTextpane.setWidth(width);
		if (transaction.isDoddFrankTrx()) {
			DoddFrankTextpane.setWidth(width);
		} else {
			DoddFrankTextpane.setVisible(false);
			DoddFrankTextpane.setListenerEnabled(false);
		}
	}

    @Override
	public void start(int direction) {
        flowButtons.reset();
        flowButtons.setVisible("expert", false);	
        flowButtons.setEnabled("expert", false);	
        flowButtons.setEnabled("back", false);	
        flowButtons.setEnabled("next",false );	
        flowButtons.setVisible("back", true);	
        flowButtons.setVisible("next",true );	
        
        if (transaction.isReceiptReceived()) {
            reprintButton.setEnabled(true);
            reprintLabel.setEnabled(true);
            printingLabel.setText(Messages.getString("MGSWizardPage14.IntructionTitle"));
            instruction1Label.setText(Messages.getString("MGSWizardPage14.Instruction1")); 
            instruction2Label.setText(Messages.getString("MGSWizardPage14.Instruction2")); 
            instruction3Label.setText(Messages.getString("MGSWizardPage14.Instruction3")); 
            rule3continued.setText("     ".concat(Messages.getString("MGSWizardPage14.Instruction3b")));
            rule3continued2.setText("     ".concat(Messages.getString("MGSWizardPage14.Instruction3c")));
            instruction4Label.setText(Messages.getString("MGSWizardPage14.Instruction4")); 
            newCardDirectionsLabel.setText(Messages.getString("MGSWizardPage14.Instruction5"));
         }
        else {
            reprintButton.setEnabled(false);
            reprintLabel.setEnabled(false);
            printingLabel.setText(Messages.getString("MGSWizardPage14.IntructionTitleAlt"));
            instruction1Label.setText(Messages.getString("MGSWizardPage14.Instruction2_1")); 
            instruction2Label.setText(""); 
            instruction3Label.setText(""); 
            rule3continued.setText("");
            rule3continued2.setText("");
            instruction4Label.setText(""); 
            newCardDirectionsLabel.setText(Messages.getString("MGSWizardPage14.Instruction5_2"));
        }
        
        addActionListener("reprintButton", this, "reprintReceipt");	 
        
    	List<JLabel> tags = new ArrayList<JLabel>();
         
        String cardNumber = transaction.getAutoMoneySaver();
		// new fields for automated MoneySaver card creation.
        if(cardNumber==null || cardNumber.length()<=0){
        	cardNumber = transaction.getSender().getActiveFreqCustCardNumber();
        }
        
        String newCardValue = null;
		if ((! transaction.isFormFreeTransaction()) && (cardNumber != null) && (cardNumber.trim().length() != 0)) {
			newCardValue = cardNumber;
		} else if ((transaction.isFormFreeTransaction()) && (transaction.getFormFreeTransaction().getMgiRewardsNumber() != null) && (transaction.getFormFreeTransaction().getMgiRewardsNumber().length() > 0)) {
			newCardValue = transaction.getFormFreeTransaction().getMgiRewardsNumber();
		} else {
			newCardDirectionsLabel.setVisible(false);
		}
              
        KeyMapManager.mapComponent(this, KeyEvent.VK_F5, 0,
        getComponent("reprintButton"));	
        
        String sendCurrency = transaction.getSendCurrency();
        
        BigDecimal a = transaction.getAmount();
        BigDecimal fwt = transaction.getFeeSendTotalWithTax(false);
        if (fwt != null) {
        	a = a.add(fwt);
        }

        this.displayMoney("totalAmount", a, sendCurrency, tags);
        
        this.displayLabelItem("referenceNumber", transaction.getCommitReferenceNumber(), false, false, tags);
        
        this.displayLabelItem("callPIN", transaction.getPhoneCallPIN(), false, false, tags);
        
        this.displayLabelItem("orden", transaction.getOrdenNumber(), false, false, tags);
        
        this.displayLabelItem("newCard", newCardValue, false, false, tags);
        
        this.displayMoney("transferAmount", transaction.getAmount(), sendCurrency, tags);
        
        this.displayMoney("fee", fwt, sendCurrency, tags);
        
        this.displayLabelItem("exchangeRate", FormatSymbols.convertDecimal(transaction.getExchangeRate(false).toString()), false, false, tags);
        
		boolean bFeeModified = transaction.feeModified(
				transaction.getDiscSendTotal(),
				transaction.getTaxSendTotalAmt(),
				transaction.getFeeSendNonMgi());
		
		boolean bPromoCodesReturned = (transaction.getReturnedPromoCodeCount() > 0);
		
		feeDetailsTextpane.setEnabled(bFeeModified);
		
		if (bFeeModified || bPromoCodesReturned 
				|| (transaction.getEnteredPromoCodeCount() > 0)) {
			addActionListener("feeDetailsButton", this, "feeDetails");
			feeDetailsPanel.setVisible(true);
			feeDetailsButton.setEnabled(true);
		} else {
			feeDetailsPanel.setVisible(false);
			feeDetailsButton.setEnabled(false);
		}
		
		if (bFeeModified) {
			feeLabel.setText(Messages.getString("MGSWizardPage13.1170a"));
		} else {
			feeLabel.setText(Messages.getString("MGSWizardPage13.1170"));
		}
        
		BigDecimal trf = transaction.getSendValidationResponsePayload().getReceiveAmounts().getTotalReceiveFees();
		BigDecimal trt = transaction.getSendValidationResponsePayload().getReceiveAmounts().getTotalReceiveTaxes();
		boolean bRecvAmtModified = transaction.recvModified(trf, trt);
		
		BigDecimal receiveAmount;
		if (bRecvAmtModified) {
			receiveAmount = transaction.getReturnedFeeInfo().getFeeInfo().getReceiveAmounts().getTotalReceiveAmount();
			receiveAmountLabel.setText(Messages.getString("MGSWizardPage14.1191a"));
			recvDetailsPanel.setEnabled(true);
			recvDetailsPanel.setVisible(true);
			recvDetailsButton.setEnabled(true);
			addActionListener("recvDetailsButton", this, "recvDetails");
		} else {
			receiveAmount = transaction.getReceiveAmount();
			receiveAmountLabel.setText(Messages.getString("MGSWizardPage14.1191"));
			recvDetailsPanel.setEnabled(false);
			recvDetailsPanel.setVisible(false);
			recvDetailsButton.setEnabled(false);
		}
        
        this.displayMoney("receiveAmount", receiveAmount, transaction.getReceiveCurrency(), tags);
        
        tagWidth = this.setTagLabelWidths(tags, MINIMUM_TAG_WIDTH);
        
		IdNoticeTextpane.setVisible(true);
		
		flowButtons.disable();

		if (transaction.isReceiveAgentAndCustomerReceipts()) {
			// agent receipt
			printReceipt(false, false, null);
			/*
			 * Sleep 2.0 seconds, receipts are getting too big and printer may
			 * not be ready for second receipt.
			 */
			try {
				Thread.sleep(2500);
			} catch (InterruptedException e) {
				Debug.println("sleep2 between receipts caught InterruptedException: " + e.getClass().getName() + ": "
						+ e.getMessage());
			}
			if (!transaction.wasPrintFailure()) {
				// followed by customer receipt
				printReceipt(true, false, null);
			}
		} else if (transaction.isReceiveCustomerReceipt()) {
			printReceipt(true, false, null);
		} else if (transaction.isReceiveAgentReceipt()) {
			printReceipt(false, false, null);
		}
        if (transaction.wasPrintFailure()) {
		        Dialogs.showError("dialogs/DialogNoReceiptError.xml");
        }
        
        flowButtons.setEnabled("cancel", true);	
        flowButtons.setAlternateText("cancel",Messages.getString("OK"));  
        flowButtons.setSelected("cancel");	
         
        // last receipt has been printed, so allow backout timer again
        IdleBackoutTimer.start();
    }
        
    /** Send Agent and customer receipts to the Epson printer when user clicks 
     *  the reprint button
     * 
     */
    public void reprintReceipt() {
       	reprint(transaction);
    }
    
	/*
	 *  Reprint of send receipt(s).
	 */
	public static void reprint(MoneyGramSendTransaction tran) {
		tran.resetPrintFailure();
		if (tran.isReceiveAgentAndCustomerReceipts()) {
			tran.printReceipt(false, true);
			if (!tran.wasPrintFailure()) {
				tran.printReceipt(true, true);
			}
		} else if (tran.isReceiveCustomerReceipt()) {
			tran.printReceipt(true, true);
		} else if (tran.isReceiveAgentReceipt()) {
			tran.printReceipt(false, true);
		}
	}
    
    @Override
	public void finish(int direction) {
        flowButtons.disable();
        if (direction == PageExitListener.CANCELED)
            direction = PageExitListener.COMPLETED;
        PageNotification.notifyExitListeners(this, direction);
    }
	
	/**
	 * This method is invoked when the Fee Details Button is clicked.
	 */
	public void feeDetails() {

		FeeDetailsDialog dialog = new FeeDetailsDialog(transaction.getReturnedFeeInfo().getFeeInfo(),transaction.getEnteredPromoCodeArray());
		Dialogs.showPanel(dialog);
		if (dialog.isCanceled()) {
			finish(PageExitListener.CANCELED);
			return;
		} else if (!dialog.isAccepted()) {
			flowButtons.setButtonsEnabled(true);
			flowButtons.setSelected("next"); 
			return;
		}
	}

	/**
	 * This method is invoked when the Recv Info Button is clicked.
	 */
	public void recvDetails() {
		RecvDetailsDialog dialog = new RecvDetailsDialog(transaction.getReturnedFeeInfo().getFeeInfo());
		Dialogs.showPanel(dialog);
		if (dialog.isCanceled()) {
			finish(PageExitListener.CANCELED);
			return;
		} else if (!dialog.isAccepted()) {
			flowButtons.setButtonsEnabled(true);
			flowButtons.setSelected("next"); 
			return;
		}
	}
}
