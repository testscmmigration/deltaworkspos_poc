package dw.mgsend;

import dw.framework.ClientTransaction;
import dw.framework.PageNameInterface;
import dw.framework.TransactionInterface;

public abstract class MoneyGramSendTransactionInterface
                                                extends TransactionInterface {
	private static final long serialVersionUID = 1L;

    protected MoneyGramSendTransaction transaction;

    public MoneyGramSendTransactionInterface(String fileName,
                                 MoneyGramSendTransaction transaction,
                                 PageNameInterface naming,
                                 boolean sideBar) {
        super(fileName, naming, sideBar);
        this.transaction = transaction;
    }

    @Override
	public ClientTransaction getTransaction() {
        return transaction;
    }
}
