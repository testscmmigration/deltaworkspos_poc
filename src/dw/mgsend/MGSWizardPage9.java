package dw.mgsend;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import dw.accountdepositpartners.AccountDepositPartnersTransaction;
import dw.framework.DataCollectionField;
import dw.framework.DataCollectionScreenToolkit;
import dw.framework.DataCollectionSet;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;
import dw.utility.DWValues;
import dw.utility.ExtraDebug;

/**
 * "recipientDetail"
 * Page 9 of the MoneyGram Send Wizard. This page prompts the user for the
 * recipient information if this is not a selected existing recipient. The test
 * question and answer are disabled if not applicable based on profile.
 */
public class MGSWizardPage9 extends MoneyGramSendFlowPage {
	private static final long serialVersionUID = 1L;

	private JScrollPane scrollPane1;
	private JScrollPane scrollPane2;
	private JPanel panel2;
	private DataCollectionScreenToolkit toolkit = null;
	private Object dataCollectionSetChangeFlag = new Object();
	private JButton nextButton;

	public MGSWizardPage9(AccountDepositPartnersTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		this((MoneyGramSendTransaction) tran, name, pageCode, buttons); 
	}

	public MGSWizardPage9(MoneyGramSendTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super(tran, "moneygramsend/MGSWizardPage9.xml", name, pageCode, buttons); 
		scrollPane1 = (JScrollPane) getComponent("scrollPane1");
		scrollPane2 = (JScrollPane) getComponent("scrollPane2");
		panel2 = (JPanel) getComponent("panel2");
		nextButton = this.flowButtons.getButton("next");
	}
	
	@Override
	public void start(int direction) {
		
		// Make sure the screen has been built.
		
		setupComponents();
		
		// Setup the flow buttons at the bottom of the screen.
		
		flowButtons.reset();
		flowButtons.setVisible("expert", false); 
		flowButtons.setEnabled("expert", false); 
		
		// Populate the screen with any previously entered data.
		
		if (direction == PageExitListener.NEXT || direction == PageExitListener.BACK) {
			DataCollectionScreenToolkit.restoreDataCollectionFields(toolkit.getScreenFieldList(), transaction.getDataCollectionData());
		}
		
		// Move the cursor to 1) the first required item with no value or item 
		// with an invalid value or 2) the first optional item with no value.
		
		toolkit.moveCursor(toolkit.getScreenFieldList(), nextButton);
		
		// Enable and call the resize listener.
		
		toolkit.enableResize();
		toolkit.getComponentResizeListener().componentResized(null);
	}
	
	private void setupComponents() {
		
		// Check if the GFFP information has changed.
		
		DataCollectionSet data = this.transaction.getDataCollectionData();

		if (this.dataCollectionSetChangeFlag != this.transaction.getDataCollectionData().getChangeFlag()) {
	    	this.dataCollectionSetChangeFlag = this.transaction.getDataCollectionData().getChangeFlag();
	    	
	    	// Rebuild the data collection items for the screen.
	    	
	    	ExtraDebug.println("Rebuilding Screen " + getPageCode() + " from FTC data");
	    	
			int flags = DataCollectionScreenToolkit.SHORT_LABEL_VALUES_FLAG + DataCollectionScreenToolkit.DISPLAY_REQUIRED_FLAG;
			JButton backButton = this.flowButtons.getButton("back");
			this.toolkit = new DataCollectionScreenToolkit(this, this.scrollPane1, this, this.scrollPane2, this.panel2, flags, transaction.getReceiverNameScreenStatus().getDataCollectionPanels(data), this.transaction, backButton, nextButton, transaction.getDestination().getCountryCode());
		}
		
		// Check if the screen needs to be laid out
		
		if (transaction.getReceiverNameScreenStatus().needLayout(data)) {
			toolkit.populateDataCollectionScreen(panel2, transaction.getReceiverNameScreenStatus().getDataCollectionPanels(data), false);
		}
	}

	@Override
	public void finish(int direction) {
    	
    	// Disable the resize listener for this screen.
    	
		toolkit.disableResize();
        
    	// If the flow direction is to the next screen, check the validity of the
    	// data. If a value is invalid or a required item does not have a value,
    	// return to the screen.
    	
    	if (direction == PageExitListener.NEXT) {
        	String tag = toolkit.validateData(toolkit.getScreenFieldList(), DataCollectionField.DISPLAY_ERRORS_VALIDATION);
        	if (tag != null) {
				flowButtons.setButtonsEnabled(true);
				toolkit.enableResize();
        		return;
        	}
        }
    	
    	// If the flow direction is to the next screen or back to the previous
    	// screen, save the value of the current items in a data structure.

        if ((direction == PageExitListener.BACK) || (direction == PageExitListener.NEXT)) {
        	DataCollectionScreenToolkit.saveDataCollectionFields(toolkit.getScreenFieldList(), transaction.getDataCollectionData());
        }
    	
    	// Check if receiveValidation could be performed now or if another page should be displayed.
    	
    	if (direction == PageExitListener.NEXT) {
    		
        	// Check if transaction ready to be validated.
        	
			String nextPage = transaction.nextDataCollectionScreen(MoneyGramSendWizard.MSW60_RECIPIENT_DETAIL);
	    	if (nextPage.isEmpty() && transaction.getDataCollectionData().isAdditionalInformationScreenValid()) {
				transaction.sendValidation(this, ValidationType.SECONDARY);
				return;
			}
    	}
    	PageNotification.notifyExitListeners(this, direction);
	}
	
	/**
	 * Called by transaction after it receives response from middleware.
	 * 
	 * @param commTag
	 *            is function index.
	 * @param returnValue
	 *            is Boolean set to true if response is good. Otherwise, it is
	 *            set to false.
	 */
	@Override
	public void commComplete(int commTag, Object returnValue) {
		boolean b = ((Boolean) returnValue).booleanValue();
		
		if (b && commTag == MoneyGramClientTransaction.SEND_VALIDATE) {
			PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
		} else {
			start(PageExitListener.NEXT);
		}
	}
}
