/*
 * MGSWizardPage13.java
 * 
 * $Revision$
 *
 * Copyright (c) 2001-2012 MoneyGram International
 */
package dw.mgsend;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import com.moneygram.agentconnect.SessionType;

import dw.accountdepositpartners.AccountDepositPartnersTransaction;
import dw.dialogs.Dialogs;
import dw.dialogs.FeeDetailsDialog;
import dw.dialogs.FraudAlertDialog;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.dwgui.MoneyField;
import dw.framework.ClientTransaction;
import dw.framework.DataCollectionField;
import dw.framework.DataCollectionSet;
import dw.framework.FieldKey;
import dw.framework.KeyMapManager;
import dw.framework.MfaAuthorization;
import dw.framework.MfaFlowPageInterface;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.main.DeltaworksMainPanel;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.CountryInfo.Country;
import dw.model.adapters.CountryInfo.CountrySubdivision;
import dw.printing.ReceiptReport;
import dw.profile.UnitProfile;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.ServiceOption;

/**
 * "sendVerification"
 * Page 13 of the MoneyGram Send Wizard. This page displays a summary of the
 *   send information and allows the user to send the transaction.
 */
public class MGSWizardPage13 extends MoneyGramSendFlowPage implements DWTextPaneListener, MfaFlowPageInterface {
	private static final long serialVersionUID = 1L;

	private static int PRINT_OK = 0;
	private static int PRINT_CANCELED = 1;
	private static int PRINT_RETRY = 2;
	
	private JPanel scrollee;
    private boolean autoPrintConfirmMG;
    private boolean autoPrintConfirmationSlip = false;
    private boolean hasNotBeenPrinted = true;
	private boolean hasPrintBeenStarted = false;
    private Object hasNotBeenPrintedLock = new Object();

    
    //  calculation
    private JLabel feeTagLabel;
    private JLabel totalToCollectLabel;
    private JLabel printConfirmationLabel;
    private JButton printConfirmationButton;
    private MoneyField amountTenderedField;
    private MoneyField changeDueField;
    private BigDecimal amount;   
	private JButton editTransactionButton;

	private JPanel feeDetailsPanel;
	private JLabel feeDetailsText;
	private JButton feeDetailsButton;

    //Net send
    private JLabel subagentCreditLimitValueLabel;
    private JLabel subagentCreditLimitTextLabel;
    private JComponent subagentCreditLimitBorder;
    
    private boolean displayNewDS = false;
    private String  sSavedAcctNbr;

	private boolean reprint = false;
	private List<DWTextPane> textPanes;
	private int tagWidth;
	private DWTextPane text2;
	
	public MGSWizardPage13(MoneyGramSendTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "moneygramsend/MGSWizardPage13.xml", name, pageCode, buttons);	
        init();
    }
	
	public MGSWizardPage13(AccountDepositPartnersTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super(tran, "moneygramsend/MGSWizardPage13.xml", name, pageCode, buttons);	
		init();
	}
	
	private void init() {
		textPanes = new ArrayList<DWTextPane>();

        editTransactionButton = (JButton) getComponent("editTransactionButton");

        feeTagLabel = (JLabel) getComponent("feeTagLabel"); 
        totalToCollectLabel = (JLabel) getComponent("totalCollectAmountLabel");   
        amountTenderedField = (MoneyField) getComponent("tenderedAmountLabel");  
        changeDueField = (MoneyField) getComponent("changeDueAmountLabel");    
        printConfirmationLabel = (JLabel) getComponent("printConfirmationLabel");
        printConfirmationButton = (JButton) getComponent("printConfirmationButton");
         
        subagentCreditLimitValueLabel = (JLabel) getComponent("subagentCreditLimitValueLabel");
        subagentCreditLimitTextLabel  = (JLabel) getComponent("subagentCreditLimitTextLabel");
        subagentCreditLimitBorder = (JComponent)getComponent("subagentCreditLimitBorder");
        
		feeDetailsPanel = (JPanel) getComponent("feeDetailsPanel");
		feeDetailsButton = (JButton) getComponent("feeDetailsButton");
		feeDetailsText = (JLabel) getComponent("feeDetailsText");
		
		scrollee = (JPanel) getComponent("scrollee"); 
		text2 = (DWTextPane) getComponent("text2");	
		
        this.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						scrollpaneResized();
					}
				});
			}
        });
	}

	@Override
	public void dwTextPaneResized() {
		JScrollPane sp2 = (JScrollPane) this.getComponent("scrollPane2");
		int l1 = sp2.getViewport().getPreferredSize().width;
		int l2 = l1 - 195;
		int l3 = l1 - 20;
		DWTextPane text1 = (DWTextPane) getComponent("text1");	
		text1.setWidth(l1);
		text2.setWidth(l3);
		text2.setListenerEnabled(false);
		
		int w = FIELD_TOTAL_WIDTH - tagWidth;
		for (DWTextPane pane : textPanes) {
			pane.setWidth(w);
		}
	}
	
	private void scrollpaneResized() {
		
		dwTextPaneResized();
		
		JScrollPane sp1 = (JScrollPane) this.getComponent("scrollPane1");
		JScrollPane sp2 = (JScrollPane) this.getComponent("scrollPane2");
		JPanel panel2 = (JPanel) this.getComponent("scrollee");

		this.scrollpaneResized(sp1, sp2, panel2);
	}

    public void calculateChangeDue() {
        amount = new BigDecimal(amountTenderedField.getText());
        if ((amount.compareTo(BigDecimal.ZERO) != 0)
            && (amount.compareTo(transaction.getTotalToCollect()) > 0)) {
            BigDecimal changeDue = transaction.getChangeDue(FormatSymbols.convertDecimal(totalToCollectLabel.getText()), amountTenderedField.getText());
            changeDueField.setText(changeDue.toString());            
        }
        else
            changeDueField.setText("0");   
    }

    private String getFieldLabel(FieldKey key, String defaultValue) {
    	String label = null;
    	if (! transaction.isFormFreeTransaction()) {
    		label = transaction.getProfileObject().getLabel(key.getInfoKey());
    		if ((label == null) || (label.isEmpty())) {
    			DataCollectionField field = transaction.getDataCollectionData().getFieldByInfoKey(key.getInfoKey());
    			if (field != null) {
    				label = field.getLabel();
    			}
    		}
    	} else {
    		label = transaction.getTransactionLookupInfoLabel(key.getInfoKey());
    	}
		if ((label == null) || (label.isEmpty())) {
			label = defaultValue;
		}
		return label;
    }

    @Override
	public void start(int direction) {
        flowButtons.reset();
        flowButtons.setVisible("expert", false);	
        flowButtons.setAlternateText("next", Messages.getString("MGSWizardPage13.sendButtonText"));	 
        flowButtons.setAlternateKeyMapping("next", KeyEvent.VK_F5, 0);	
	     
		JScrollPane sp2 = (JScrollPane) this.getComponent("scrollPane2");
		setupAutoscroll(sp2, scrollee, new JComponent[] {
				amountTenderedField, 
				changeDueField
			});
		
        DataCollectionSet data = transaction.getDataCollectionData(); 
		
        String senderNameValue;
        String senderAddressValue;
    	String senderCountryValue;
    	String senderCity;
    	String senderStateCode;
    	String senderZipCode;
        String direction1Value;
        String direction2Value;
        String direction3Value;

        Country destinationCountry;
        CountrySubdivision destinationState;
        String deliveryOption;
        String receiverNameValue;        
        String receiveCurrency;
        String receiveAgentId;
        String receiverAddressValue;
		String receiverCityValue;
		String receiverStateValue;
		String receiverStateCode;
		String receiverZipCodeValue;
		String receiverPhoneNumberValue;
		String receiverCountryValue;
		
		String thirdParySenderNameValue;

		String accountNmber = null;
		boolean bFeeModified;
    	BigDecimal amountValue;
        BigDecimal f;
		
		if (! transaction.isFormFreeTransaction()) {
	        senderNameValue = DWValues.formatName(data.getValue(FieldKey.SENDER_FIRSTNAME_KEY), data.getValue(FieldKey.SENDER_MIDDLENAME_KEY), data.getValue(FieldKey.SENDER_LASTNAME_KEY), data.getValue(FieldKey.SENDER_LASTNAME2_KEY));
	        senderAddressValue = DWValues.getAddress(data.getValue(FieldKey.SENDER_ADDRESS_KEY), data.getValue(FieldKey.SENDER_ADDRESS2_KEY), data.getValue(FieldKey.SENDER_ADDRESS3_KEY), data.getValue(FieldKey.SENDER_ADDRESS4_KEY));
	    	senderCountryValue = data.getValue(FieldKey.SENDER_COUNTRY_KEY);
	        senderCity = data.getValue(FieldKey.SENDER_CITY_KEY);
	        senderStateCode = data.getValue(FieldKey.SENDER_COUNTRY_SUBDIVISIONCODE_KEY);
	        senderZipCode = data.getValue(FieldKey.SENDER_POSTALCODE_KEY);
	        direction1Value = data.getValue(FieldKey.DIRECTION1_KEY);
	        direction2Value = data.getValue(FieldKey.DIRECTION2_KEY);
	        direction3Value = data.getValue(FieldKey.DIRECTION3_KEY);
	        destinationCountry = CountryInfo.getCountry(transaction.getCurrentFeeInfo().getDestinationCountry());
	        destinationState = transaction.getDestinationState();
	        deliveryOption = transaction.getQualifiedDeliveryOption().getServiceOption();
	        receiverNameValue = DWValues.formatName(data.getValue(FieldKey.RECEIVER_FIRSTNAME_KEY), data.getValue(FieldKey.RECEIVER_MIDDLENAME_KEY), data.getValue(FieldKey.RECEIVER_LASTNAME_KEY), data.getValue(FieldKey.RECEIVER_LASTNAME2_KEY));        
	        receiveCurrency =  transaction.getQualifiedDeliveryOption().getPayoutCurrency();
	        receiveAgentId = transaction.getQualifiedDeliveryOption().getReceiveAgentID();
			receiverAddressValue = DWValues.getAddress(data.getValue(FieldKey.RECEIVER_ADDRESS_KEY), data.getValue(FieldKey.RECEIVER_ADDRESS2_KEY), data.getValue(FieldKey.RECEIVER_ADDRESS3_KEY), data.getValue(FieldKey.RECEIVER_ADDRESS4_KEY));
			receiverCityValue = data.getValue(FieldKey.RECEIVER_CITY_KEY);
			receiverStateCode = data.getValue(FieldKey.RECEIVER_COUNTRY_SUBDIVISIONCODE_KEY);
			receiverZipCodeValue = data.getValue(FieldKey.RECEIVER_POSTALCODE_KEY);
			receiverPhoneNumberValue = data.getValue(FieldKey.RECEIVER_PRIMARYPHONE_KEY);
			receiverCountryValue = data.getValue(FieldKey.RECEIVER_COUNTRY_KEY);
			if (transaction.isDss()) {
				accountNmber = ReceiptReport.getTextAccountNumberLabelFromDeliveryOption(transaction.getCurrentFeeInfo().getServiceOption()); 
			}
			bFeeModified = transaction.feeModified(transaction.getReturnedFeeInfo().getFeeInfo());

			thirdParySenderNameValue = data.getCurrentValue(FieldKey.THIRDPARTY_SENDER_ORGANIZATION_KEY.getInfoKey());
			if (thirdParySenderNameValue.isEmpty()) {
				thirdParySenderNameValue = DWValues.formatName(data.getCurrentValue(FieldKey.THIRDPARTY_SENDER_FIRSTNAME_KEY.getInfoKey()), data.getCurrentValue(FieldKey.THIRDPARTY_SENDER_MIDDLENAME_KEY.getInfoKey()), data.getCurrentValue(FieldKey.THIRDPARTY_SENDER_LASTNAME_KEY.getInfoKey()), data.getCurrentValue(FieldKey.THIRDPARTY_SENDER_LASTNAME2_KEY.getInfoKey()));
			}
			
			addActionListener("editTransactionButton", this, "editTransaction");
	        KeyMapManager.mapComponent(this, KeyEvent.VK_F6, 0, getComponent("editTransactionButton"));
			editTransactionButton.setEnabled(true);

	    	amountValue = transaction.getAmount();
	        f = transaction.getFeeSendTotalWithTax(true);
		} else {
	        senderNameValue = DWValues.formatName(data.getCurrentValue(FieldKey.SENDER_FIRSTNAME_KEY.getInfoKey()), data.getCurrentValue(FieldKey.SENDER_MIDDLENAME_KEY.getInfoKey()), data.getCurrentValue(FieldKey.SENDER_LASTNAME_KEY.getInfoKey()), data.getCurrentValue(FieldKey.SENDER_LASTNAME2_KEY.getInfoKey()));
	        senderAddressValue = DWValues.getAddress(data.getCurrentValue(FieldKey.SENDER_ADDRESS_KEY.getInfoKey()), data.getCurrentValue(FieldKey.SENDER_ADDRESS2_KEY.getInfoKey()), data.getCurrentValue(FieldKey.SENDER_ADDRESS3_KEY.getInfoKey()), data.getCurrentValue(FieldKey.SENDER_ADDRESS4_KEY.getInfoKey()));
	    	senderCountryValue = data.getCurrentValue(FieldKey.SENDER_COUNTRY_KEY.getInfoKey());
	    	senderCity = data.getValue(FieldKey.SENDER_CITY_KEY);
	        senderStateCode = data.getValue(FieldKey.SENDER_COUNTRY_SUBDIVISIONCODE_KEY);
	        senderZipCode = data.getValue(FieldKey.SENDER_POSTALCODE_KEY);
	        direction1Value = data.getCurrentValue(FieldKey.DIRECTION1_KEY.getInfoKey());
	        direction2Value = data.getCurrentValue(FieldKey.DIRECTION2_KEY.getInfoKey());
	        direction3Value = data.getCurrentValue(FieldKey.DIRECTION3_KEY.getInfoKey());
	        destinationCountry = CountryInfo.getCountry(data.getCurrentValue(FieldKey.DESTINATION_COUNTRY_KEY.getInfoKey()));
	        destinationState = destinationCountry != null ? destinationCountry.getCountrySubdivision(data.getCurrentValue(FieldKey.DESTINATION_COUNTRY_SUBDIVISIONCODE_KEY.getInfoKey())) : null;
	        deliveryOption = data.getCurrentValue(FieldKey.SERVICEOPTION_KEY.getInfoKey());
	        receiverNameValue = DWValues.formatName(data.getCurrentValue(FieldKey.RECEIVER_FIRSTNAME_KEY.getInfoKey()), data.getCurrentValue(FieldKey.RECEIVER_MIDDLENAME_KEY.getInfoKey()), data.getCurrentValue(FieldKey.RECEIVER_LASTNAME_KEY.getInfoKey()), data.getCurrentValue(FieldKey.RECEIVER_LASTNAME2_KEY.getInfoKey()));        
	        receiveCurrency =  transaction.getTransactionLookupResponsePayload().getReceiveAmounts().getReceiveCurrency();
	        receiveAgentId = data.getCurrentValue(FieldKey.RECEIVE_AGENTID_KEY.getInfoKey());
			receiverAddressValue = DWValues.getAddress(data.getCurrentValue(FieldKey.RECEIVER_ADDRESS_KEY.getInfoKey()), data.getCurrentValue(FieldKey.RECEIVER_ADDRESS2_KEY.getInfoKey()), data.getCurrentValue(FieldKey.RECEIVER_ADDRESS3_KEY.getInfoKey()), data.getCurrentValue(FieldKey.RECEIVER_ADDRESS4_KEY.getInfoKey()));
			receiverCityValue = data.getCurrentValue(FieldKey.RECEIVER_CITY_KEY.getInfoKey());
			receiverStateCode = data.getCurrentValue(FieldKey.RECEIVER_COUNTRY_SUBDIVISIONCODE_KEY.getInfoKey());
			receiverZipCodeValue = data.getCurrentValue(FieldKey.RECEIVER_POSTALCODE_KEY.getInfoKey());
			receiverPhoneNumberValue = data.getCurrentValue(FieldKey.RECEIVER_PRIMARYPHONE_KEY.getInfoKey());
			receiverCountryValue = data.getValue(FieldKey.RECEIVER_COUNTRY_KEY);
			accountNmber = data.getFieldStringValue(FieldKey.DISPLAY_ACCOUNT_ID_KEY).trim();
			bFeeModified = false;

			thirdParySenderNameValue = data.getCurrentValue(FieldKey.THIRDPARTY_SENDER_ORGANIZATION_KEY.getInfoKey());
			if (thirdParySenderNameValue.isEmpty()) {
				thirdParySenderNameValue = DWValues.formatName(data.getCurrentValue(FieldKey.THIRDPARTY_SENDER_FIRSTNAME_KEY.getInfoKey()), data.getCurrentValue(FieldKey.THIRDPARTY_SENDER_MIDDLENAME_KEY.getInfoKey()), data.getCurrentValue(FieldKey.THIRDPARTY_SENDER_LASTNAME_KEY.getInfoKey()), data.getCurrentValue(FieldKey.THIRDPARTY_SENDER_LASTNAME2_KEY.getInfoKey()));
			}
			
			editTransactionButton.setEnabled(false);

	    	amountValue = transaction.getTransactionLookupResponsePayload().getSendAmounts().getSendAmount();
	    	BigDecimal fee = transaction.getTransactionLookupResponsePayload().getSendAmounts().getTotalSendFees();
	    	BigDecimal tax = transaction.getTransactionLookupResponsePayload().getSendAmounts().getTotalSendTaxes();
			f = fee.add(tax);
		}
		
        Country senderCountryObject = senderCountryValue != null ? CountryInfo.getCountry(senderCountryValue) : null;
        CountrySubdivision senderStateObject = senderCountryObject != null ? senderCountryObject.getCountrySubdivision(senderStateCode) : null;
		
        Country receiverCountryObject = receiverCountryValue != null ? CountryInfo.getCountry(receiverCountryValue) : null;
        CountrySubdivision receiverStateObject = receiverCountryObject != null ? receiverCountryObject.getCountrySubdivision(receiverStateCode) : null;
        receiverStateValue = receiverStateObject != null ? receiverStateObject.getCountrySubdivisionName() : "";
		
		displayNewDS = transaction.isDss() && transaction.isInlineDSR();
		
		DWTextPane text1 = (DWTextPane) getComponent("text1");
		text1.setVisible(displayNewDS);
		
    	List<JLabel> tags = new ArrayList<JLabel>();
		
        // Sender Name
        
		this.displayPaneItem("senderName", senderNameValue, tags, textPanes);
		
		// Sender Address
		
		this.displayPaneItem("senderAddress", senderAddressValue, tags, textPanes);
		
		this.displayLabelItem("senderCity", getFieldLabel(FieldKey.SENDER_CITY_KEY, Messages.getString("MGSWizardPage13.receiverCityLabel")), senderCity, tags);
        
		this.displayLabelItem("senderState", getFieldLabel(FieldKey.SENDER_COUNTRY_SUBDIVISIONCODE_KEY, Messages.getString("MOWizardPage3aID.1360")), senderStateObject != null ? senderStateObject.getPrettyName() : "", tags);
        
		this.displayLabelItem("senderZip", getFieldLabel(FieldKey.SENDER_POSTALCODE_KEY, Messages.getString("MGRWizardPage3.958e")), senderZipCode, tags);
        
        this.displayLabelItem("senderCountry", getFieldLabel(FieldKey.SENDER_COUNTRY_KEY, Messages.getString("MGSWizardPage13.1161C")), senderCountryObject != null ? senderCountryObject.getPrettyName() : "", tags);
        
        // Amount
        
        String currency = transaction.getSendCurrency();
        this.displayMoney("amount", amountValue, currency, tags);
        
        this.displayMoney("fee", BigDecimal.ZERO, currency, tags);
        this.displayMoney("totalCollect", BigDecimal.ZERO, currency, tags);
        
        if (transaction.isAmountTenderedEnabled()) {
            amountTenderedField.requestFocus();
            this.displayMoney("tendered", BigDecimal.ZERO, currency, tags);
            this.displayMoney("changeDue", BigDecimal.ZERO, currency, tags);
        } else {
            flowButtons.setSelected("next");	
            this.displayMoney("tendered", null, currency, tags);
            this.displayMoney("changeDue", null, currency, tags);
            text2.setVisible(false);
        }
        
        // Destination 
        
        String location = null;
        if (destinationCountry != null) {
	        String countryCode = destinationCountry.getCountryCode();
	        location = destinationCountry.toString();
	        if ((destinationState != null) 
	        		&& (countryCode.equalsIgnoreCase("USA") || countryCode.equalsIgnoreCase("MEX") || countryCode.equalsIgnoreCase("CAN"))) {
	        	location += "  (" + destinationState.getPrettyName() +")";       	
	        }
        }
        
        this.displayPaneItem("destination", location, tags, textPanes);
        
        // Service Option
        
        ServiceOption so = null;
		if (destinationCountry != null) {
			so = ServiceOption.getServiceOption(deliveryOption, destinationCountry.getCountryCode(), receiveCurrency,
					receiveAgentId);
		}
        this.displayLabelItem("via", getFieldLabel(FieldKey.SERVICEOPTION_KEY, Messages.getString("AccountDepositPartnersWizardPage1.4")), so != null ? so.getServiceOptionDisplayName() : "", tags);
        
    	// Third Party
    	
		this.displayPaneItem("thirdParty", thirdParySenderNameValue, tags, textPanes);

		// Receiver Name
        
		this.displayPaneItem("receiverName", receiverNameValue, tags, textPanes);

		// Account Number
		
		String sAcct = null;
		if ((accountNmber != null) && (accountNmber.length() > 0)) {
			
			sAcct = transaction.getSendValidationResponsePayload() != null ? transaction.getSendValidationResponsePayload().getDisplayAccountID() : "";

			/*
			 * If account # is empty, assume partner managed and use RRN instead
			 */
			if ((sAcct == null) || sAcct.isEmpty()) {
				sAcct = transaction.getCustomerReceiveNumber();
			}
		}
		this.displayLabelItem("acctNumber", accountNmber, sAcct, tags);
        
        // Receiver Address
        
		this.displayPaneItem("receiverAddress", receiverAddressValue, tags, textPanes);
		
		// Receiver City
		
		this.displayLabelItem("receiverCity", receiverCityValue, tags);
		
		// Receiver State
		
		this.displayLabelItem("receiverState", receiverStateValue, tags);
		
		// Receiver Zip Code
		
		this.displayLabelItem("receiverZipCode", receiverZipCodeValue, tags);
		
		// Receiver Country
		
		this.displayLabelItem("receiverCountry", receiverCountryValue, tags);

        this.displayLabelItem("direction1", direction1Value, tags);
        this.displayLabelItem("direction2", direction2Value, tags);
        this.displayLabelItem("direction3", direction3Value, tags);

		// Receiver Phone
		
		this.displayLabelItem("receiverPhoneNumber", receiverPhoneNumberValue, tags);
        
    	// Fee
    	
        this.displayMoney("fee", f, currency, tags);

        // Total to Collect
        
        BigDecimal ttc = transaction.getSendAmounts() != null ? transaction.getSendAmounts().getTotalAmountToCollect() : null;
        totalToCollectLabel.setText(ttc != null ? FormatSymbols.convertDecimal(ttc.toString()) : "");
        
        amountTenderedField.setCurrency(currency);
        changeDueField.setCurrency(currency);
        
    	autoPrintConfirmMG = transaction.isMGConfirmationSlip();
        
		if (transaction.isReceiptReceived() && transaction.isReceiveSendConfirmation()) {
			printConfirmationButton.setVisible(true);
			printConfirmationLabel.setVisible(true);
			printConfirmationButton.setEnabled(true);
			printConfirmationLabel.setEnabled(true);
			autoPrintConfirmationSlip = autoPrintConfirmMG;
		} else {
			printConfirmationButton.setVisible(false);
			printConfirmationLabel.setVisible(false);
			autoPrintConfirmationSlip = false;
		}
        
        addFocusLostListener("tenderedAmountLabel", this,"calculateChangeDue");            
        
        addActionListener("printConfirmationButton", this, "printConfirmationReceipt");
        KeyMapManager.mapComponent(this, KeyEvent.VK_F10, 0,
                getComponent("printConfirmationButton"));
        
        amountTenderedField.addActionListener(new ActionListener(){
            @Override
			public void actionPerformed(ActionEvent e) {
                flowButtons.getButton("next").requestFocus();  
            }
        });
        
		if (!UnitProfile.getInstance().isMultiAgentMode()) {
			subagentCreditLimitBorder.setVisible(false);
			subagentCreditLimitValueLabel.setVisible(false);
			subagentCreditLimitTextLabel.setVisible(false);
		} else {
			BigDecimal saLimit = transaction.getSaLimitAvailable();
			if (saLimit != null) {
				subagentCreditLimitValueLabel.setText(FormatSymbols.convertDecimal(saLimit.toString()) + " " + currency);
			} else {
				subagentCreditLimitBorder.setVisible(false);
				subagentCreditLimitValueLabel.setVisible(false);
				subagentCreditLimitTextLabel.setVisible(false);
			}
		}
		
		boolean bPromoCodesReturned = (transaction.getReturnedPromoCodeCount() > 0);
		feeDetailsText.setEnabled(bFeeModified);
		
		if (bFeeModified || bPromoCodesReturned || (transaction.getEnteredPromoCodeCount() > 0)) {
			addActionListener("feeDetailsButton", this, "feeDetails");
			feeDetailsPanel.setVisible(true);
			feeDetailsButton.setEnabled(true);
			if (transaction.getReturnedFeeInfo().getSendValidationResponsePayload().getPromotionInfos() != null
					&& transaction.promoCodeErrors(transaction.getReturnedFeeInfo().getSendValidationResponsePayload()
							.getPromotionInfos().getPromotionInfo())) {
				feeDetailsButton.setForeground(Color.RED);
			} else {
				feeDetailsButton.setForeground(Color.BLACK);
			}
		} else {
			feeDetailsPanel.setVisible(false);
			feeDetailsButton.setEnabled(false);
		}
		
		if (bFeeModified) {
			feeTagLabel.setText(Messages.getString("MGSWizardPage13.1170a"));
		} else {
			feeTagLabel.setText(Messages.getString("MGSWizardPage13.1170"));
		}
		
        tagWidth = this.setTagLabelWidths(tags, MINIMUM_TAG_WIDTH);
        
		if (autoPrintConfirmationSlip && hasNotBeenPrinted) {
			// reentrant processing
			synchronized (hasNotBeenPrintedLock) {
				if (hasNotBeenPrinted && !hasPrintBeenStarted) {
					if (printConfirmationRcpt() == PRINT_CANCELED) {
						hasNotBeenPrinted = true;
						return;
					}
					hasNotBeenPrinted = false;
				}
			}
		}

        if (hasNotBeenPrinted) {
            printConfirmationButton.setText(Messages.getString("MGSWizardPage13.1177"));
        }

        transaction.doEarlyISI();
    }
    
    /**
     * Called by transaction after it receives response from middleware.
     * @param direction is a wizard direction.
     */
    @Override
	public void finish(int direction) {
        // call setButtonsEnabled to avoid duplicated transactions.
        flowButtons.setButtonsEnabled(false);
		editTransactionButton.setEnabled(false);
        amount = new BigDecimal(amountTenderedField.getText());
        if (direction == PageExitListener.NEXT) {
        //if amount tendered is less than total to collect or greater than daily limit don't allow to continue.    
            if ((amount.compareTo(BigDecimal.ZERO) != 0)&&
                (amount.compareTo(transaction.getTotalToCollect()) < 0)){ 
                Dialogs.showWarning("dialogs/DialogInsufficientAmount.xml");    
                amountTenderedField.requestFocus();
                amountTenderedField.setText("0"); 
                changeDueField.setText("0");   
                flowButtons.setButtonsEnabled(true);
                return;
            }else if(amount.compareTo(transaction.getDailyLimit())>0){
                java.awt.Toolkit.getDefaultToolkit().beep();
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
					public void run() {
                        amountTenderedField.requestFocus();
                    }
                });    
                amountTenderedField.setText("0"); 
                changeDueField.setText("0");   
                flowButtons.setButtonsEnabled(true);
                return;
            }   
          
            // first make sure the agent has the money
            
            String currency = (! transaction.isFormFreeTransaction()) ? transaction.getSendCurrency() : transaction.getTransactionLookupResponsePayload().getSendAmounts().getSendCurrency();
        	if (transaction.isFraudAlertNeeded(currency)) {
                FraudAlertDialog fraudDialog = new FraudAlertDialog();
                fraudDialog.showPanel();
                if (fraudDialog.isCanceled()) {
                    finish(PageExitListener.CANCELED);
                    return;
                }
                else if (! fraudDialog.isAccepted()){
                    flowButtons.setButtonsEnabled(true);
                    flowButtons.setSelected("next");    
                    return;
                }
            }
        	
            // Check if a MFA Token is required.
            doSend();
		} else {
			synchronized (hasNotBeenPrintedLock) {
				hasNotBeenPrinted = true;
				hasPrintBeenStarted = false;
			}
			PageNotification.notifyExitListeners(this, direction);
		}
	}
	    
    private void doSend() {
    	 if (UnitProfile.getInstance().isMfaEnabled()) {
         	MfaAuthorization ma = new MfaAuthorization();
         	ma.doMfaAuthorization(this);
         } else {
         	commitSend();
         }		
	}
		
    @Override
	public void commit() {
		commitSend();
    }
		
	public void commitSend() {
    	
        // all is good, send the transaction to the host
    	
    	/*
    	 * For non-pilot DS transactions, we do NOT want to populate the
    	 * new account number field. 
    	 */
    	if (transaction.isDss() && !transaction.isInlineDSR()) {
    		sSavedAcctNbr = transaction.getReceiverAccountNumber();
    		transaction.setReceiverAccountNumber("");
    	}
    	
    	transaction.completeSession(this, SessionType.SEND, null, transaction.getReferenceNumber());
		resetRetryCount();
    }
    
    @Override
	public void deauthorize() {
    	deauthorize(transaction);
		resetRetryCount();
    }

	@Override
	public void abort() {
        flowButtons.setButtonsEnabled(true);
		transaction.setStatus(ClientTransaction.FAILED);
		PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
		resetRetryCount();
	}

	@Override
	public void cancel() {
        flowButtons.setButtonsEnabled(true);
	}

    private int printConfirmationRcpt() {
    	hasPrintBeenStarted = true;
		int option;
		String cTran;
		do {
			super.printConfirmationReceipt();
			option = printDone();
			cTran = DeltaworksMainPanel.getMainPanel().getVisiblePageName();
		} while (option == PRINT_RETRY && !cTran.equalsIgnoreCase("mainMenu"));
		if (option == PRINT_OK) {
			hasNotBeenPrinted = false;
		} else {
			/*
			 * If disclosure was required, then on cancel abort transaction,
			 * otherwise basically ignore the cancel.
			 */
			if (autoPrintConfirmMG) {
				PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
				return PRINT_CANCELED;
			} else {
				return PRINT_OK;
			}
		}
        printConfirmationButton.setText(Messages.getString("MGSWizardPage13.1177a"));
        reprint = true;
        return PRINT_OK;
    }
    
    /**
     * Called by transaction after it receives response from middleware.
     * @param commTag is function index.
     * @param returnValue is Boolean set to true if response is good. 
     * Otherwise, it is set to false.
     */
    @Override
	public void commComplete(int commTag, Object returnValue) {
        if (returnValue.equals(Boolean.TRUE)) {
		    if (commTag == MoneyGramClientTransaction.SEND_VALIDATE) {
		    	if (printConfirmationRcpt() == PRINT_CANCELED) {
		    		return;
		    	}
		    } else {
	            PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
		    }
        } else {
            if (transaction.getStatus() == ClientTransaction.NEEDS_RECOMMIT) {
            	 doSend();
            }else if (transaction.getStatus() == ClientTransaction.CANCELED) {
            	if (transaction.isDss() && !transaction.isInlineDSR()) {
            		transaction.setReceiverAccountNumber(sSavedAcctNbr);
            	}
				start(PageExitListener.NEXT);
            }else {
				Debug.println("13CommComplete[" +commTag+ "],["+returnValue+"]");
				start(PageExitListener.NEXT);
            }
        }
    }

	/**
	 * This method is invoked when the Fee Details Button is clicked.
	 */
	public void feeDetails() {

		FeeDetailsDialog dialog = new FeeDetailsDialog(transaction.getReturnedFeeInfo().getFeeInfo(),transaction.getEnteredPromoCodeArray());
		Dialogs.showPanel(dialog);
		if (dialog.isCanceled()) {
			finish(PageExitListener.CANCELED);
			return;
		} else if (!dialog.isAccepted()) {
			flowButtons.setButtonsEnabled(true);
			flowButtons.setSelected("next"); 
			return;
		}
	}
	
	
	/**
	 * This method is invoked when the Edit Transaction Button is clicked.
	 */
	public void editTransaction() {

		finish(PageExitListener.EDIT_TRANSACTION);
		return;
	}
	
	private int printDone() {
		int bRetValue = PRINT_OK;
       	if (!transaction.wasDiscPrintSucessful()) {
       		if (cancelPrint()) {
       			bRetValue = PRINT_CANCELED;
       		} else {
       			bRetValue = PRINT_RETRY;
       		}
       	}
       	hasPrintBeenStarted = false;
		return bRetValue;
	}
	
	private boolean cancelPrint() {
		String sMessage, sMessage2, sMessage3, sMessage4;
		
		if (reprint) {
			if (autoPrintConfirmMG) {
				sMessage = Messages.getString("DialogReceiptReprintFailurePre.Text1");
				sMessage2 = Messages.getString("DialogReceiptReprintFailurePre.Text2");
				sMessage3 = Messages.getString("DialogReceiptReprintFailurePre.Text3");
				sMessage4 = Messages.getString("DialogReceiptReprintFailurePre.Text4");
			} else {
				sMessage = Messages.getString("DialogReceiptReprintFailurePreNR.Text1");
				sMessage2 = Messages.getString("DialogReceiptReprintFailurePreNR.Text2");
				sMessage3 = Messages.getString("DialogReceiptReprintFailurePreNR.Text3");
				sMessage4 = Messages.getString("DialogReceiptReprintFailurePreNR.Text4");
			}
		} else {
			if (autoPrintConfirmMG) {
				sMessage = Messages.getString("DialogReceiptPrintFailurePre.Text1");
				sMessage2 = Messages.getString("DialogReceiptPrintFailurePre.Text2");
				sMessage3 = Messages.getString("DialogReceiptPrintFailurePre.Text3");
				sMessage4 = Messages.getString("DialogReceiptPrintFailurePre.Text4");
			} else {
				sMessage = Messages.getString("DialogReceiptPrintFailure.Text1");
				sMessage2 = Messages.getString("DialogReceiptPrintFailure.Text2");
				sMessage3 = Messages.getString("DialogReceiptPrintFailure.Text3");
				sMessage4 = Messages.getString("DialogReceiptPrintFailure.Text4");
			}
		}

		boolean bRetValue = Dialogs.showPrintCancel("dialogs/BlankDialog3.xml",
				sMessage, sMessage2, sMessage3, sMessage4) != Dialogs.OK_OPTION;
		return bRetValue;
	}
}
