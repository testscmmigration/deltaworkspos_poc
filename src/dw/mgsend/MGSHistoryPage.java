package dw.mgsend;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/** 
 * Interface implemented by expert page 1 and wizard page 3, so that the 
 * populatePeopleTable method could be moved up. Kind of a kludge, but better 
 * than having populatePeopleTable copy-and-pasted in two places.
 */
public interface MGSHistoryPage {
    JTable getTable();
    DefaultTableModel getTableModel(); 
}
