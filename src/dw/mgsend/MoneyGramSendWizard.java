package dw.mgsend;

import java.awt.Component;
import java.awt.event.KeyEvent;

import dw.accountdepositpartners.AccountDepositPartnersWizardPage1;
import dw.billpayment.BPWizardPage16;
import dw.billpayment.BPWizardPage17;
import dw.billpayment.BPWizardPage18;
import dw.billpayment.BPWizardPage7;
import dw.billpayment.BPWizardPage7b;
import dw.billpayment.BillPaymentWizard;
import dw.framework.AdditionalInformationWizardPage;
import dw.framework.ClientTransaction;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.DataCollectionSet.DataCollectionType;
import dw.framework.FlowPage;
import dw.framework.FormFreeIdVerificationWizardPage;
import dw.framework.KeyMapManager;
import dw.framework.ProfileSearchWizardPage;
import dw.framework.ProfileSelectionWizardPage;
import dw.framework.ProfileReviewWizardPage;
import dw.framework.ProfileEditWizardPage1;
import dw.framework.ProfileEditWizardPage2;
import dw.framework.PageExitListener;
import dw.framework.PageNameInterface;
import dw.framework.SupplementalInformationWizardPage;
import dw.mgsend.MoneyGramClientTransaction.ProfileStatus;
import dw.model.adapters.ConsumerProfile;
import dw.model.adapters.ConsumerProfile.NotificationOptionDisplayStatus;

public class MoneyGramSendWizard extends MoneyGramSendTransactionInterface {
	private static final long serialVersionUID = 1L;

	private static final String FQW05_FEE_QUERY_ENTRY = "feeQueryEntry";
	private static final String FQW10_FEE_INFORMATION = "feeInformation";        
	static final String MSW06_CARD_NUMBER_ENTRY = "cardNumberEntry";
	static final String MSW12_PROFILE_SEARCH_RESULTS = "profileSearchResults";
	static final String MSW08_FORM_FREE_TRANSACTION_SELECTION = "formFreeTransactionSelection";
	private static final String MSW16_SENDER_SELECTION = "senderSelection";
	static final String MSW18_PROFILE_REVIEW = "profileReview";
	public static final String MSW20_PROFILE_CREATE_UPDATE_1 = "profileCreateUpdate1";
	static final String MSW21_PROFILE_CREATE_UPDATE_2 = "profileCreateUpdate2";
	private static final String MSW22_RECEIVER_SELECTION = "receiverSelection";
	private static final String MSW25_AMOUNT_ENTRY = "amountEntry";
	static final String MSW10_MG_SEND_FEE_INFORMATION = "msfeeInformation";
	static final String MSW30_IDENTIFICATION = "identification";
    public static final String MSW40_FORM_FREE_ID_VALIDATION = "formFreeIdValidation";
	static final String MSW50_THIRD_PARTY = "thirdParty";
	static final String MSW60_RECIPIENT_DETAIL = "recipientDetail";       
	static final String MSW61_DIRECTED_SEND_DETAIL = "directedSendDetail";
	static final String MSW63_DIRECTED_SEND_PARTNER_DETAIL = "directedSendPartnerDetail";
	static final String MSW64_SUPPLEMENTAL_SEND_INFORMATION = "supplementalSendInformation";
    static final String MRW67_ADDITIONAL_SEND_INFORMATION = "additionalSendInformation";
	static final String MSW65_SEND_VERIFICATION = "sendVerification";
	private static final String MSW70_RECEIPT_PRINTING = "sendReceiptPrinting";
	private static final String MSW75_ERROR_PAGE = "sendErrorPage";
	private static final String ADP01_ACCOUNT_DEPOSIT_PARTNERS_TABLE = "accountDepositPartnersTable";

	public MoneyGramSendWizard(MoneyGramSendTransaction transaction, PageNameInterface naming) {
		super("moneygramsend/MGSWizardPanel.xml", transaction, naming, false); 
		createPages();
	}

	@Override
	public void createPages() {
		addPage(MGFQWizardPage1.class,  transaction, FQW05_FEE_QUERY_ENTRY, "FQW05", buttons);  
		addPage(MGFQWizardPage2.class,  transaction, FQW10_FEE_INFORMATION, "FQW10", buttons);  
		addPage(ProfileSearchWizardPage.class,   transaction, MSW06_CARD_NUMBER_ENTRY, "MSW06", buttons);  
		addPage(MGSWizardPage2.class,   transaction, MSW08_FORM_FREE_TRANSACTION_SELECTION, "MSW08", buttons);  
		addPage(ProfileSelectionWizardPage.class,  transaction, MSW12_PROFILE_SEARCH_RESULTS, "MSW12", buttons);
		addPage(MGSWizardPage3.class,   transaction, MSW16_SENDER_SELECTION, "MSW16", buttons);  
		addPage(ProfileReviewWizardPage.class,   transaction, MSW18_PROFILE_REVIEW, "MSW18", buttons);  
		addPage(ProfileEditWizardPage1.class,   transaction, MSW20_PROFILE_CREATE_UPDATE_1, "MSW20", buttons);  
		addPage(ProfileEditWizardPage2.class,   transaction, MSW21_PROFILE_CREATE_UPDATE_2, "MSW21", buttons);  
		addPage(MGSWizardPage4.class,   transaction, MSW22_RECEIVER_SELECTION, "MSW22", buttons);  
		addPage(MGSWizardPage5.class,   transaction, MSW25_AMOUNT_ENTRY, "MSW25", buttons);  
		addPage(MGSWizardPage5a.class,  transaction, MSW10_MG_SEND_FEE_INFORMATION, "MSW10", buttons);  
		addPage(MGSWizardPage6.class,   transaction, MSW30_IDENTIFICATION, "MSW30", buttons);  
        addPage(FormFreeIdVerificationWizardPage.class, transaction, MSW40_FORM_FREE_ID_VALIDATION, "MSW40", buttons);	 
		addPage(MGSWizardPage8.class,   transaction, MSW50_THIRD_PARTY, "MSW50", buttons);  
		addPage(MGSWizardPage9.class,   transaction, MSW60_RECIPIENT_DETAIL, "MSW60", buttons);         
		addPage(MGSWizardPage9a.class,  transaction, MSW61_DIRECTED_SEND_DETAIL, "MSW61", buttons);  
		addPage(MGSWizardPage9b.class,  transaction, MSW63_DIRECTED_SEND_PARTNER_DETAIL, "MSW63", buttons);  
		addPage(SupplementalInformationWizardPage.class,  transaction, MSW64_SUPPLEMENTAL_SEND_INFORMATION, "MSW64", buttons);  
		addPage(AdditionalInformationWizardPage.class,  transaction, MRW67_ADDITIONAL_SEND_INFORMATION, "MSW67", buttons);  
		addPage(MGSWizardPage13.class,  transaction, MSW65_SEND_VERIFICATION, "MSW65", buttons);  
		addPage(MGSWizardPage14.class,  transaction, MSW70_RECEIPT_PRINTING, "MSW70", buttons);  
		addPage(MGSWizardPage15.class,  transaction, MSW75_ERROR_PAGE, "MSW75", buttons, true);  
		addPage(AccountDepositPartnersWizardPage1.class, transaction, ADP01_ACCOUNT_DEPOSIT_PARTNERS_TABLE, "ADP01", buttons); 
		
		// Bill Payment Pages
		
        addPage(BPWizardPage7.class, transaction, BillPaymentWizard.BPW25_SENDER_DETAIL, "BPW25", buttons);	 
        addPage(BPWizardPage7b.class, transaction, BillPaymentWizard.BPW30_COMPLIANCE, "BPW30", buttons);
        addPage(BPWizardPage16.class, transaction, BillPaymentWizard.BPW70_SEND_VERIFICATION, "BPW70", buttons);	 
        addPage(BPWizardPage17.class, transaction, BillPaymentWizard.BPW75_RECEIPT_PRINTING, "BPW75", buttons);	 
        addPage(BPWizardPage18.class, transaction, BillPaymentWizard.BPW80_ERROR_PAGE, "BPW80", buttons, true);	 
        addPage(SupplementalInformationWizardPage.class, transaction, BillPaymentWizard.BPW40_SUPPLEMENTAL_BP_INFORMATION, "BPW40", buttons);
        addPage(AdditionalInformationWizardPage.class, transaction, BillPaymentWizard.BPW50_ADDITIONAL_BP_INFORMATION, "BPW50", buttons);
	}

	/**
	 * Add the single keystroke mappings to the buttons. Use the function keys
	 * for back, next, cancel, etc.
	 */
	private void addKeyMappings() {
		KeyMapManager.mapMethod(this, KeyEvent.VK_F2, 0, "f2SAR"); 
		buttons.addKeyMapping("expert", KeyEvent.VK_F4, 0); 
		buttons.addKeyMapping("back", KeyEvent.VK_F11, 0); 
		buttons.addKeyMapping("next", KeyEvent.VK_F12, 0); 
		buttons.addKeyMapping("cancel", KeyEvent.VK_ESCAPE, 0); 
	}

	@Override
	public void start() {
		transaction.clear();
		if (transaction.isFeeQuery()) {
			super.start(FQW05_FEE_QUERY_ENTRY);
		} else if (transaction.isFormFreeSendEnabled()) {
			super.start(MSW08_FORM_FREE_TRANSACTION_SELECTION);
		} else if (transaction.isSendToAccount()) {
			super.start(ADP01_ACCOUNT_DEPOSIT_PARTNERS_TABLE);
		} else {
			super.start(MSW06_CARD_NUMBER_ENTRY);
		}
		addKeyMappings();
	}

	/**
	 * Page traversal logic here.
	 */
	@Override
	public void exit(int direction) {
		String currentPage = cardLayout.getVisiblePageName();
		String nextPage = null;
		
		switch (direction) {

			//=================================================================
	    	// Forward
	    	//=================================================================
    
			case PageExitListener.NEXT:
				if (currentPage.equals(MSW06_CARD_NUMBER_ENTRY)) {
					int profileCount = transaction.getSearchProfiles() != null ? transaction.getSearchProfiles().size() : 0;
					if (profileCount > 1) {
						nextPage = MSW12_PROFILE_SEARCH_RESULTS; 
					} else if (profileCount == 1) {
						nextPage = MSW18_PROFILE_REVIEW;
					} else if ((transaction.getUniqueCustomers() != null) && (! transaction.getUniqueCustomers().isEmpty())) {
						nextPage = MSW16_SENDER_SELECTION; 
					} else {
						nextPage = MSW20_PROFILE_CREATE_UPDATE_1;				
					}
					
				} else if (currentPage.equals(MSW08_FORM_FREE_TRANSACTION_SELECTION)) {
					if (transaction.getFormFreeStatus() == MoneyGramClientTransaction.FORM_FREE_SEND_ERROR) {
						nextPage = MSW75_ERROR_PAGE;
					} else if (transaction.getFormFreeStatus() == MoneyGramClientTransaction.FORM_FREE_XPAY_ERROR) {
						nextPage = BillPaymentWizard.BPW80_ERROR_PAGE;
					} else {
						nextPage = nextDataCollectionScreen(currentPage);
						if (nextPage.isEmpty()) {
							if (transaction.getFormFreeStatus() == MoneyGramClientTransaction.FORM_FREE_SEND_COMMIT) {
								nextPage = MSW65_SEND_VERIFICATION;
							} else if (transaction.getFormFreeStatus() == MoneyGramClientTransaction.FORM_FREE_XPAY_COMMIT) {
								nextPage = BillPaymentWizard.BPW70_SEND_VERIFICATION;
							} else {
								nextPage = MSW08_FORM_FREE_TRANSACTION_SELECTION;
							}
						}
					}
				
				} else if (currentPage.equals(MSW12_PROFILE_SEARCH_RESULTS)) {
					if (transaction.getProfileStatus().equals(ProfileStatus.CREATE_PROFILE)) {
						nextPage = MSW20_PROFILE_CREATE_UPDATE_1;
					} else {
						nextPage = MSW18_PROFILE_REVIEW;
					}
					
				} else if (currentPage.equals(MSW18_PROFILE_REVIEW)) {
					ProfileStatus profileStatus = transaction.getProfileStatus();
					if ((profileStatus == null) || (profileStatus.equals(ProfileStatus.USE_CURRENT_PROFILE))) {
						ConsumerProfile profile = transaction.getProfileObject();
						if (profile.getNotificationsDisplayed().equals(NotificationOptionDisplayStatus.NOT_DISPLAYED) && transaction.getProfileDataScreen2().hasDataToCollect()) {
							nextPage = MSW21_PROFILE_CREATE_UPDATE_2;
							profile.setNotificationsDisplayed(NotificationOptionDisplayStatus.DISPLAYING_IN_REVIEW);
						} else if (profile.getReceivers().size() > 0) {
							nextPage = MSW22_RECEIVER_SELECTION;
						} else {
							nextPage = MSW25_AMOUNT_ENTRY;
						}
					} else {
						nextPage = MSW20_PROFILE_CREATE_UPDATE_1;
					}
					
				} else if (currentPage.equals(MSW20_PROFILE_CREATE_UPDATE_1)) {
					if (transaction.getProfileDataScreen2().hasDataToCollect()) {
						nextPage = MSW21_PROFILE_CREATE_UPDATE_2;
						transaction.getProfileObject().setNotificationsDisplayed(NotificationOptionDisplayStatus.DISPLAYING_IN_EDIT);
					} else {
						nextPage = profileForwardPage();
					}

				} else if (currentPage.equals(MSW21_PROFILE_CREATE_UPDATE_2)) {
					ConsumerProfile profile = transaction.getProfileObject();
					nextPage = profileForwardPage();
					profile.setNotificationsDisplayed(NotificationOptionDisplayStatus.ALlREADY_DISPLAYED);
					
				} else if (currentPage.equals(MSW22_RECEIVER_SELECTION)) {
					nextPage = MSW25_AMOUNT_ENTRY;
					
				} else if (currentPage.equals(MSW16_SENDER_SELECTION)) {
					nextPage = MSW20_PROFILE_CREATE_UPDATE_1;
				
				} else if (currentPage.equals(MSW25_AMOUNT_ENTRY)) { 
					nextPage = MSW10_MG_SEND_FEE_INFORMATION; 
				
				} else if (currentPage.equals(MSW10_MG_SEND_FEE_INFORMATION)) { 
					if (transaction.getValidationStatus().equals(DataCollectionStatus.ERROR)) {
						nextPage = MSW75_ERROR_PAGE;
					} else {
						nextPage = this.nextSendDataCollectionScreen(currentPage);
					}
				
				} else if ((currentPage.equals(MSW30_IDENTIFICATION)) ||
						(currentPage.equals(MSW50_THIRD_PARTY)) ||
						(currentPage.equals(MSW60_RECIPIENT_DETAIL)) ||
						(currentPage.equals(MSW61_DIRECTED_SEND_DETAIL)) || 
						(currentPage.equals(MSW63_DIRECTED_SEND_PARTNER_DETAIL)) || 
						(currentPage.equals(MSW64_SUPPLEMENTAL_SEND_INFORMATION))) {
					nextPage = this.nextSendDataCollectionScreen(currentPage);
					if ((nextPage == null) || (nextPage.isEmpty())) {
			        	if (transaction.getValidationStatus().equals(DataCollectionStatus.NEED_MORE_DATA)) {
			        		nextPage = MRW67_ADDITIONAL_SEND_INFORMATION;
			        	} else if (transaction.getValidationStatus().equals(DataCollectionStatus.VALIDATED)) {
		            		nextPage = MSW65_SEND_VERIFICATION;
			        	} else {
			        		if (transaction.getDataCollectionData().isAdditionalInformationScreenValid()) {
			        			nextPage = MSW75_ERROR_PAGE;
			        		} else {
			        			nextPage = MRW67_ADDITIONAL_SEND_INFORMATION;
			        		}
			        	}
					}
					
		        } else if (currentPage.equals(MSW40_FORM_FREE_ID_VALIDATION)) {
		    		nextPage = transaction.isBillPayment() ? BillPaymentWizard.BPW70_SEND_VERIFICATION : MSW65_SEND_VERIFICATION;
					
		        } else if (currentPage.equals(MRW67_ADDITIONAL_SEND_INFORMATION)) {
	            	DataCollectionType type = transaction.getDataCollectionData().getDataCollectionType();
		        	if (transaction.getValidationStatus().equals(DataCollectionStatus.NEED_MORE_DATA)) {
		        		nextPage = MRW67_ADDITIONAL_SEND_INFORMATION;

		        	} else if (type.equals(DataCollectionType.VALIDATION)) {
			        	if (transaction.getValidationStatus().equals(DataCollectionStatus.VALIDATED)) {
			        		if (transaction.isFormFreeTransaction() && transaction.isFormFreeIdFields()) {
			        			nextPage = MSW40_FORM_FREE_ID_VALIDATION;
			        		} else {
			            		nextPage = MSW65_SEND_VERIFICATION;
			        		}
			        	} else {
			        		nextPage = MSW75_ERROR_PAGE;
			        	}
		        	} else {
	    				ProfileStatus profileStatus = transaction.getProfileStatus();
	    				DataCollectionStatus status = transaction.getDataCollectionData().getValidationStatus();
	    				if ((status.equals(DataCollectionStatus.VALIDATED)) || (profileStatus.equals(ProfileStatus.PROFILE_COMPLETED))) {
	    					if (transaction.getProfileObject().getReceivers().size() > 0) {
								nextPage = MSW22_RECEIVER_SELECTION;
							} else {
								nextPage = MSW25_AMOUNT_ENTRY;
							}
	    				} else {
			        		nextPage = MSW75_ERROR_PAGE;
	    				}
		        	}
		        	        	
		        } else if (currentPage.equals(BillPaymentWizard.BPW50_ADDITIONAL_BP_INFORMATION)) {
		        	if (transaction.getValidationStatus().equals(DataCollectionStatus.NEED_MORE_DATA)) {
		        		nextPage = BillPaymentWizard.BPW50_ADDITIONAL_BP_INFORMATION;
		        	} else if (transaction.getValidationStatus().equals(DataCollectionStatus.VALIDATED)) {
		        		if (transaction.isFormFreeTransaction() && transaction.isFormFreeIdFields()) {
		        			nextPage = MSW40_FORM_FREE_ID_VALIDATION;
		        		} else {
		            		nextPage = BillPaymentWizard.BPW70_SEND_VERIFICATION;
		        		}
		        	} else {
		        		nextPage = BillPaymentWizard.BPW80_ERROR_PAGE;
		        	}
				
				} else if (currentPage.equals(MSW65_SEND_VERIFICATION)) {
					if (transaction.getStatus() == ClientTransaction.COMPLETED) {
						nextPage = MSW70_RECEIPT_PRINTING;
					} else if (transaction.getStatus() == ClientTransaction.FAILED) {
						nextPage = MSW65_SEND_VERIFICATION; 
					}
				
				} else if (currentPage.equals(ADP01_ACCOUNT_DEPOSIT_PARTNERS_TABLE)) {
					nextPage = MSW06_CARD_NUMBER_ENTRY;
				
				} else if (currentPage.equals(BillPaymentWizard.BPW25_SENDER_DETAIL) || 
						currentPage.equals(BillPaymentWizard.BPW30_COMPLIANCE)) {
					nextPage = this.nextBPDataCollectionScreen(currentPage);
				}
				break;
			
        	//=================================================================
        	// Backward
        	//=================================================================
		        
			case PageExitListener.BACK:
				/*
				 * For Directed Sends the msFeeInformation screen can show up
				 * multiple times in history, force the back to go back to the 
				 * amount entry screen instead of back through the RRN find
				 * screens.
				 */
				
				if (currentPage.equals(FQW10_FEE_INFORMATION)) {
					nextPage = FQW05_FEE_QUERY_ENTRY;
				
				} else if (currentPage.equals(MSW06_CARD_NUMBER_ENTRY)) {
					if (transaction.isSendToAccount()) { 
						nextPage = ADP01_ACCOUNT_DEPOSIT_PARTNERS_TABLE;
					} else if (transaction.isFeeQuery()) {
						nextPage = FQW10_FEE_INFORMATION;
					}
				
				} else if (currentPage.equals(MSW10_MG_SEND_FEE_INFORMATION)) {
					popHistoryUntil(MSW25_AMOUNT_ENTRY);
					nextPage = MSW25_AMOUNT_ENTRY;
				/*
				 * If amount entry screen and edit is active, go back to the 
				 * senderDetail screen, so the sender info can be edited
				 */
				} else if (currentPage.equals(MSW25_AMOUNT_ENTRY)) {
					if (transaction.getProfileObject().getReceivers().isEmpty()) {
						nextPage = MSW18_PROFILE_REVIEW;
					} else {
						nextPage = MSW22_RECEIVER_SELECTION;
					}

				} else if (currentPage.equals(MSW12_PROFILE_SEARCH_RESULTS)) {
					nextPage = MSW06_CARD_NUMBER_ENTRY;
					
				} else if (currentPage.equals(MSW18_PROFILE_REVIEW)) {
					int profileCount = transaction.getSearchProfiles() != null ? transaction.getSearchProfiles().size() : 0;
					if (profileCount > 1) {
						popHistoryUntil(MSW12_PROFILE_SEARCH_RESULTS);
						nextPage = MSW12_PROFILE_SEARCH_RESULTS;
					} else {
						popHistoryUntil(MSW06_CARD_NUMBER_ENTRY);
						nextPage = MSW06_CARD_NUMBER_ENTRY;
					}
					
				} else if (currentPage.equals(MSW20_PROFILE_CREATE_UPDATE_1)) {
					if (transaction.getProfileObject().getCurrentValuesMap().isEmpty()) {
						nextPage = MSW06_CARD_NUMBER_ENTRY;
					} else {
						nextPage = MSW18_PROFILE_REVIEW;
					}
					
				} else if (currentPage.equals(MSW21_PROFILE_CREATE_UPDATE_2)) {
					nextPage = MSW20_PROFILE_CREATE_UPDATE_1;

				} else if (currentPage.equals(MSW22_RECEIVER_SELECTION)) {
					nextPage = MSW18_PROFILE_REVIEW;
					
				} else if ((currentPage.equals(MSW30_IDENTIFICATION)) || 
						(currentPage.equals(MSW50_THIRD_PARTY)) || 
						(currentPage.equals(MSW60_RECIPIENT_DETAIL)) || 
						(currentPage.equals(MSW61_DIRECTED_SEND_DETAIL)) || 
						(currentPage.equals(MSW63_DIRECTED_SEND_PARTNER_DETAIL)) || 
						(currentPage.equals(MSW64_SUPPLEMENTAL_SEND_INFORMATION)) || 
						(currentPage.equals(BillPaymentWizard.BPW25_SENDER_DETAIL)) || 
						(currentPage.equals(BillPaymentWizard.BPW30_COMPLIANCE)) ||
						(currentPage.equals(BillPaymentWizard.BPW40_SUPPLEMENTAL_BP_INFORMATION)) || 
						(currentPage.equals(BillPaymentWizard.BPW50_ADDITIONAL_BP_INFORMATION))) {
					transaction.setDataCollecionReview(true);
					nextPage = previousDataCollectionScreen(currentPage);
				
				} else if (currentPage.equals(MRW67_ADDITIONAL_SEND_INFORMATION)) {
	            	DataCollectionType type = transaction.getDataCollectionData().getDataCollectionType();
	            	if (type.equals(DataCollectionType.VALIDATION)) {
	    				transaction.setDataCollecionReview(true);
	    				nextPage = previousDataCollectionScreen(currentPage);
	            	} else if (transaction.getProfileDataScreen2().hasDataToCollect()) {
	   					nextPage = MSW21_PROFILE_CREATE_UPDATE_2;
	            	} else {
	            		nextPage = MSW20_PROFILE_CREATE_UPDATE_1;
	            	}
					
				} else if (currentPage.equals(MSW40_FORM_FREE_ID_VALIDATION)) {
					popHistoryUntil(MSW08_FORM_FREE_TRANSACTION_SELECTION);
		            super.exit(PageExitListener.CANCELED);
		            return;

				} else if (currentPage.equals(MSW65_SEND_VERIFICATION)) {
					transaction.setDataCollecionReview(true);
					if (! transaction.isFormFreeTransaction()) {
						popHistoryUntil(MSW25_AMOUNT_ENTRY);
						nextPage = MSW25_AMOUNT_ENTRY;
						transaction.setEditActive(true);
					} else {
						popHistoryUntil(MSW08_FORM_FREE_TRANSACTION_SELECTION);
						nextPage = MSW08_FORM_FREE_TRANSACTION_SELECTION;
					}

				} else if (currentPage.equals(BillPaymentWizard.BPW70_SEND_VERIFICATION)) {
					transaction.setDataCollecionReview(true);
					popHistoryUntil(MSW08_FORM_FREE_TRANSACTION_SELECTION);
					return;
					
				} else {
					super.exit(direction);
					return;
				}
				break;
		
        	//=================================================================
        	// Edit Transaction
        	//=================================================================
		        
			case PageExitListener.EDIT_TRANSACTION:
				transaction.setDataCollecionReview(true);
				transaction.setEditActive(true);
				transaction.getDataCollectionData().setAfterValidationAttempt(false);
				popHistoryUntil(MSW18_PROFILE_REVIEW);
				nextPage = MSW18_PROFILE_REVIEW;
				direction = NEXT;
				break;
				
        	//=================================================================
        	// Do Nothing
        	//=================================================================
			        
			case PageExitListener.DONOTHING:
				return;
				
        	//=================================================================
        	// Canceled
        	//=================================================================
			        
			case PageExitListener.CANCELED:
				transaction.escSAR();
				super.exit(direction);
				return;
				
        	//=================================================================
        	// Completed
        	//=================================================================
			        
			case PageExitListener.COMPLETED:
				transaction.f2NAG();
				super.exit(direction);
				return;

			default:
				super.exit(direction);
				return;
		}
		
		if (nextPage != null) {
			showPage(nextPage, direction);
		} else {
			super.exit(direction);
		}
	}

	public void f2SAR() {
		Component current = cardLayout.getVisiblePage();
		if (current instanceof FlowPage) {
			((FlowPage) current).updateTransaction();
		}
		transaction.f2SAR();
	}
	
	private String previousDataCollectionScreen(String currentPage) {
		String nextPage = transaction.previousDataCollectionScreen(currentPage);
		
		if (nextPage.isEmpty()) {
			if (! transaction.isFormFreeTransaction()) {
				nextPage = MSW10_MG_SEND_FEE_INFORMATION;
			} else {
				nextPage = MSW08_FORM_FREE_TRANSACTION_SELECTION;
			}
		}
		
		transaction.getDataCollectionData().setAfterValidationAttempt(false);
		return nextPage;
	}
	
	private String nextDataCollectionScreen(String currentPage) {
		if (! transaction.isBillPayment()) {
			return nextSendDataCollectionScreen(currentPage);
		} else {
			return nextBPDataCollectionScreen(currentPage);
		}
		
	}
	
	private String nextSendDataCollectionScreen(String currentPage) {
		
		String nextPage = transaction.nextDataCollectionScreen(currentPage);
		if ((! nextPage.isEmpty()) && (transaction.getDataCollectionData().isAfterValidationAttempt())) {
			nextPage = MRW67_ADDITIONAL_SEND_INFORMATION;
		}
		
		// If no more data needs to be collected, then a send validation should have been performed. 
		// Display the next screen based upon the status of the validation.
		
		if (nextPage.isEmpty()) {
			if ((transaction.getValidationStatus().equals(DataCollectionStatus.NEED_MORE_DATA)) || (transaction.getValidationStatus().equals(DataCollectionStatus.NOT_VALIDATED))) {
				nextPage = MRW67_ADDITIONAL_SEND_INFORMATION;
			} else if (transaction.isFormFreeTransaction() && (transaction.isFormFreeIdFields())) {
				nextPage = MSW40_FORM_FREE_ID_VALIDATION;
			} else if (transaction.getValidationStatus().equals(DataCollectionStatus.VALIDATED)) {
				nextPage = MSW65_SEND_VERIFICATION;
			} else if (transaction.getValidationStatus().equals(DataCollectionStatus.RETRY)) {
				nextPage = currentPage;
	    	}
		}
		
		transaction.getDataCollectionData().setAfterValidationAttempt(false);
		return nextPage;
	}

	private String nextBPDataCollectionScreen(String currentPage) {
		
		String nextPage = transaction.nextDataCollectionScreen(currentPage);
		if ((! nextPage.isEmpty()) && (transaction.getDataCollectionData().isAfterValidationAttempt())) {
			nextPage = BillPaymentWizard.BPW50_ADDITIONAL_BP_INFORMATION;
			transaction.getDataCollectionData().setAfterValidationAttempt(false);
		}
		
		// If no more data needs to be collected, then a send validation should have been performed. 
		// Display the next screen based upon the status of the validation.
		
		if (nextPage.isEmpty()) {
			if ((transaction.getValidationStatus().equals(DataCollectionStatus.NEED_MORE_DATA)) || (transaction.getValidationStatus().equals(DataCollectionStatus.NOT_VALIDATED))) {
				nextPage = BillPaymentWizard.BPW50_ADDITIONAL_BP_INFORMATION;
			} else if (transaction.isFormFreeTransaction() && (transaction.isFormFreeIdFields())) {
				nextPage = MSW40_FORM_FREE_ID_VALIDATION;
			} else if (transaction.getValidationStatus().equals(DataCollectionStatus.VALIDATED)) {
				nextPage = BillPaymentWizard.BPW70_SEND_VERIFICATION;
			} else if (transaction.getValidationStatus().equals(DataCollectionStatus.RETRY)) {
				nextPage = currentPage;
	    	} else {
				nextPage = BillPaymentWizard.BPW80_ERROR_PAGE;
	    	}
		}
		
		transaction.getDataCollectionData().setAfterValidationAttempt(false);
		return nextPage;
	}
	
	private String profileForwardPage() {
		String page;
		ProfileStatus profileStatus = transaction.getProfileStatus();
		DataCollectionStatus dataCollectionStatus = transaction.getDataCollectionData().getValidationStatus();
		if (dataCollectionStatus.equals(DataCollectionStatus.NEED_MORE_DATA)) {
			page = MRW67_ADDITIONAL_SEND_INFORMATION;
		} else if (dataCollectionStatus.equals(DataCollectionStatus.ERROR)) { 
			page = MSW75_ERROR_PAGE;
		} else if ((profileStatus.equals(ProfileStatus.PROFILE_COMPLETED)) || (dataCollectionStatus.equals(DataCollectionStatus.VALIDATED))) {
			if (transaction.getProfileObject().getReceivers().isEmpty()) {
				page = MSW25_AMOUNT_ENTRY;
			} else {
				page = MSW22_RECEIVER_SELECTION;
			}
		} else {
			page = MSW20_PROFILE_CREATE_UPDATE_1;
		}
		return page;
	}
}
