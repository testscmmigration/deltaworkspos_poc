package dw.mgsend;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableModel;

import com.moneygram.agentconnect.FeeInfo;

import dw.accountdepositpartners.AccountDepositPartnersTransaction;
import dw.dialogs.Dialogs;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.model.adapters.CountryInfo.Country;
import dw.profile.UnitProfile;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.ServiceOption;
import dw.utility.StringUtility;

/**
 * "feeInformation"
 * Page 3 of the MoneyGram Send Wizard. This page displays a table of customers
 *   that match the home phone number given. The user may choose zero or one
 *   of the customers listed in the table and continue.
 */
public class MGFQWizardPage2 extends MGSWizardAbstractFeeInformation {
	private static final long serialVersionUID = 1L;


	@Override
	protected ListSelectionListener getSharedListSelectionHandlerInstance(JTable table) {
		return new SharedListSelectionHandler(table);
	}

	class SharedListSelectionHandler implements ListSelectionListener {
		JTable table;
		
		
		public SharedListSelectionHandler(JTable table) {
			this.table=table;
		}

		@Override
		public void valueChanged(ListSelectionEvent e) {

			
			try {
				if (blockEvents) {
					// already handling a previous event
					return;
				}
				blockEvents = true;
				int newFeeTblIdx = updateSelection(table);
				
				blockEvents = false;
				
				if(newFeeTblIdx<0)
				{
					blockEvents = false;
					return;
				}
				
				
				/*
				 * If there was a change
				 */
				if (newFeeTblIdx != lastSelectedIndex) {

					setupDeliveryOptionData();
					// for promo applicability
					setFeeDetailsEnableStatus();
					// for receive applicability
					setRecvInfoEnableStatus();
					lastSelectedIndex = newFeeTblIdx;
				}
				transaction.setFeeReturnInfo(feeInfos.get(newFeeTblIdx));
				if(!transaction.isFeeQuery())
				{
					transaction.setSelectedFeeTableIndex(newFeeTblIdx);
				}
				
				String str = StringUtility.getNonNullString(feeInfos.get(newFeeTblIdx).getServiceOptionDisplayDescription()).trim();
				if (str.length() > 0 && messageTextPane != null) {
					messagePanel.setVisible(true);
					messageTextPane.setVisible(true);
					messageTextPane.setText(str);
				} else {
					messagePanel.setVisible(false);
					messageTextPane.setVisible(false);
					messageTextPane.setText("");
				}
				dwTextPaneResized();
			} finally {
				blockEvents = false;
			}
		}

		private int updateSelection(JTable selectedTable) {
			int newFeeTblIdx;
			int tblRow = selectedTable.getSelectedRow();
			TableModel tableModel = selectedTable.getModel();
			if (tblRow < 0) {
				return tblRow;
			}
			selectedTable.requestFocus();
			
			clearTables(selectedTable);
			List<Integer> datalist = dataModelMap.get(tableModel);
			if(datalist!=null)
			{
				newFeeTblIdx = (datalist.get(tblRow)).intValue();
				return newFeeTblIdx;
			}
			
			
			
			return -1;	
		}

		/**
		 * @return the table
		 */
		public JTable getTable() {
			return table;
		}

		/**
		 * @param table the table to set
		 */
		public void setTable(JTable table) {
			this.table = table;
		}

	}
	
	public MGFQWizardPage2(MoneyGramSendTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
        super(tran, name, pageCode, buttons);	
        tran.setSelectedFeeTableIndex(-1);
    }
	
    public MGFQWizardPage2(AccountDepositPartnersTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
    	super(tran, name, pageCode, buttons);	
	 	tran.setSelectedFeeTableIndex(-1);
	}

	@Override
	public void start(int direction) {
		setupComponents();

		if (direction == PageExitListener.BACK) {
			if (transaction.getFeeInfo()==null || transaction.getFeeInfo().size() == 0) {
				clearComponentTables();
			} else {
				feeInfos = transaction.getFeeInfo();
				populateFeeData();
				populateFeeTable(true, direction);
			}
			transaction.setSelectedFeeTableIndex(-1);
		}
		try {
			if (direction == PageExitListener.NEXT) {
				feeInfos = transaction.getFeeInfo();
				populateFeeData();
				populateFeeTable(true, direction);
				super.qdo = new ServiceOption(transaction.getReturnedFeeInfo().getFeeInfo());
			}
			flowButtons.reset();
			flowButtons.setVisible("expert", false); 
			
			FeeInfo returnInfo = transaction.getReturnedFeeInfo().getFeeInfo();
			boolean bRecvAmtModified = transaction.recvModified(
					returnInfo.getReceiveAmounts().getTotalReceiveFees(), returnInfo.getReceiveAmounts().getTotalReceiveTaxes());

			if (bRecvAmtModified) {
				recvDetailsPanel.setEnabled(true);
				recvDetailsPanel.setVisible(true);
				recvDetailsButton.setEnabled(true);
				addActionListener("recvDetailsButton", this, "recvDetails");
			} else {
				recvDetailsPanel.setEnabled(false);
				recvDetailsPanel.setVisible(false);
				recvDetailsButton.setEnabled(false);
			}
			cancelButton = flowButtons.getButton("cancel"); 
			
			destinationCountryLabel.setText(transaction.getDestination()
					.toString().toUpperCase());
			String currency = transaction.getSendCurrency();
			sendCurrencyLabel.setText(currency);
			if (!UnitProfile.getInstance().isMultiAgentMode()) {
				subagentCreditLimitBorder.setVisible(false);
				subagentCreditLimitValueLabel.setVisible(false);
				subagentCreditLimitTextLabel.setVisible(false);
			} else {
				 BigDecimal saLimit = transaction.getSaLimitAvailable();
				 if (saLimit != null) {
					 subagentCreditLimitValueLabel.setText(FormatSymbols.convertDecimal(saLimit.toString()) + " " + currency);
				 }
			}

			setupDeliveryOptionData();

		} catch (Exception e) {
			Debug.printStackTrace(e);
		}

		if (transaction.isAnyModifiedFees()
				|| (transaction.getReturnedPromoCodeCount() > 0)
				|| (transaction.getEnteredPromoCodeCount() > 0)) {
			feeDetailsPanel.setVisible(true);
			feeDetailsButton.setEnabled(true);
			setFeeDetailsEnableStatus();
			addActionListener("feeDetailsButton", this, "feeDetails");
		} else {
			if (feeDetailsPanel != null) {
				feeDetailsPanel.setVisible(false);
			}
			feeDetailsButton.setEnabled(false);
		}
		dwTableResized();
		dwTextPaneResized();
    }

	@Override
	public void finish(int direction) {
		if (direction == PageExitListener.NEXT) {

			int selectedRow = getFeeIdx();
			FeeInfo feeInfo = feeInfos.get(selectedRow); 
			if (feeInfo.getErrorInfo() != null && ! feeInfo.getErrorInfo().isRecoverableFlag()) {
				String lang = DWValues.sConvertLang2ToLang3(UnitProfile.getInstance().get("LANGUAGE", "en"));
				Dialogs.acErrorDialog(
						Messages.getString("DialogAcError.InvalidSelection"),
						feeInfo.getErrorInfo().getErrorCode().toString(),
						getErrorMessageText(feeInfo, lang));
				flowButtons.setButtonsEnabled(true);
				return;
			}
			Country country = transaction.getDestination();
			transaction.setDestination(country);

			transaction.setDestOrDOChanged(true);
			transaction.setQualifiedDeliveryOption(qdo);
			transaction.setAmount(feeInfo.getSendAmounts().getSendAmount(), feeInfo.getSendAmounts().getSendCurrency());
    		PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
		} else {
			PageNotification.notifyExitListeners(this, direction);
		}

		transaction.setSelectedFeeTableIndex(lastSelectedIndex);
	}

    @Override
	protected void populateFeeOptionsTableWithSelectedRow(FeeInfo fees) {
    	resetTable();    	
		String[] row = transaction.populateFeeOptionsTableWithSelectedRow(fees);
		if(row!=null)
		{
			List<String[]> rows = new ArrayList<String[]>();
			rows.add(row);
			setupLists(rows);
			setupTable();
			allTableModel.addRow(row);
		}
	}

    /**
	* Callback from transaction after the comm is finished. If the comm was
*   successful, go to the next page. Otherwise, go back to the card number
*   field.
*/

    @Override
	public void commComplete(int commTag, Object returnValue) {
    	boolean b = ((Boolean) returnValue).booleanValue();
    	if (b ) {
    		PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
    	} else {
    		PageNotification.notifyExitListeners(this, PageExitListener.DONOTHING);
    		flowButtons.setButtonsEnabled(true);
    		flowButtons.setVisible("expert", false);

    	}
    }
}
