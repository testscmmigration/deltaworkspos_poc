package dw.mgsend;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JComboBox;

import com.moneygram.agentconnect.BusinessError;
import com.moneygram.agentconnect.CategoryInfo;
import com.moneygram.agentconnect.ConsumerProfileSearchInfo;
import com.moneygram.agentconnect.FieldInfo;
import com.moneygram.agentconnect.InfoBase;
import com.moneygram.agentconnect.KeyValuePairType;
import com.moneygram.agentconnect.Request;
import com.moneygram.agentconnect.Response;
import com.moneygram.agentconnect.SearchConsumerProfilesResponse;
import com.moneygram.agentconnect.TransactionLookupRequest;
import com.moneygram.agentconnect.TransactionLookupResponse;
import com.moneygram.agentconnect.TransactionLookupResponse.Payload;

import dw.comm.MessageFacade;
import dw.comm.MessageFacadeError;
import dw.comm.MessageFacadeParam;
import dw.comm.MessageLogic;
import dw.dialogs.CommunicationDialog;
import dw.dwgui.MultiListComboBox;
import dw.dwgui.StateListComboBox;
import dw.framework.ClientTransaction;
import dw.framework.CommCompleteInterface;
import dw.framework.DataCollectionField;
import dw.framework.DataCollectionPanel;
import dw.framework.DataCollectionScreenToolkit;
import dw.framework.DataCollectionSet;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.DataCollectionSet.DataCollectionType;
import dw.framework.FieldKey;
import dw.framework.FlowPage;
import dw.io.report.AuditLog;
import dw.i18n.Messages;
import dw.mgreceive.MoneyGramReceiveTransaction;
import dw.mgsend.MoneyGramClientTransaction.DataCollectionScreenStatus.ScreenStatusType;
import dw.model.adapters.ConsumerProfile;
import dw.model.adapters.CustomerInfo;
import dw.model.adapters.CountryInfo.Country;
import dw.model.adapters.CountryInfo.CountrySubdivision;
import dw.utility.DWValues;
import dw.utility.ExtraDebug;

/**
 * MoneyGramClientTransaction contains some common info between MGS and MGR.
 */
public abstract class MoneyGramClientTransaction extends ClientTransaction {
	private static final long serialVersionUID = 1L;
	
	public static final int FORM_FREE_NO_STATUS = 0;
	public static final int FORM_FREE_SEND_COMMIT = 1;
	public static final int FORM_FREE_SEND_ERROR = 3;
	public static final int FORM_FREE_XPAY_COMMIT = 4;
	public static final int FORM_FREE_XPAY_ERROR = 6;
	
	// possible return codes for setting amounts
	public static final int OK = 0;
	public static final int MAX_ITEM_AMOUNT_EXCEEDED = 1;
	public static final int DAILY_LIMIT_EXCEEDED = 4;
	public static final int TRANSACTION_AMOUNT_EXCEEDED = 5; // (a.k.a. CTR)
	public static final int MAX_EMPLOYEE_SEND_LIMIT_EXCEEDED = 6;
	public static final int BILL_PAY_DAILY_LIMIT_EXCEEDED = 7;
	
	// AC communication complete tags
	
	public static final int AMEND_VALIDATION = 1;
	public static final int BILLER_SEARCH = 2;
	public static final int BP_VALIDATE = 3;
	public static final int COMPLETE_SESSION = 4;
	public static final int CONSUMER_HISTORY_LOOKUP = 5;
	public static final int CREATE_OR_UPDATE_PROFILES_RECEIVER = 6;
	public static final int CREATE_OR_UPDATE_PROFILES_SENDER = 7;
    public static final int DW_PROFILE = 8;
	public static final int FEE_LOOKUP = 9;
    public static final int GET_COUNTRY_INFO = 10;
	public static final int GET_COUNTRY_SUBDIVISION = 11;
	public static final int OPEN_OTP_LOGIN = 12;
	public static final int PROFILE_CHANGE = 13;
	public static final int RECEIPTS_FORMAT_DETAILS = 14;
    public static final int RECEIVE_REVERSAL_VALIDATION = 15;
	public static final int RECEIVE_VALIDATION = 16;
	public static final int REGISTER_HARD_TOKEN = 17;
    public static final int SAVE_SUBAGENTS = 18;
	public static final int SEARCH_STAGED_TRANSACTIONS = 19;
    public static final int SEND_REVERSAL_VALIDATION = 20;
	public static final int SEND_VALIDATE = 21;
    public static final int SUBAGENTS = 22;
	public static final int TRANSACTION_LOOKUP = 23;
	public static final int VERSION_MANIFEST = 24;
	
	
	// DeltaGram printing complete tags
    
	public static final int COMMIT_DONE = 31;
	public static final int COMMIT_ERROR = 32;
	public static final int COMMIT_COMMERROR = 33;
    
    protected boolean newCustomer;
    protected static boolean tranSuccessful;
	private int formFreeStatus;
	protected boolean formFreeIdFields;
	protected boolean clientStatus;
	protected TransactionLookupResponse transactionLookupResponse;
	protected Payload transactionLookupResponsePayload;
	private Request validationRequest;
	private Map<String, FieldInfo> transactionLookupInfos;
	private ConsumerProfile profile;
	protected DataCollectionSet dataCollectionData;
	private SearchConsumerProfilesResponse searchConsumerProfilesResponse;
	private ProfileStatus profileStatus;
	private DataCollectionSet searchProfilesData;
	private boolean isDataCollecionReview;
	private Set<String> excludePanelNames;
	protected enum idTypes {ID1, ID2} 
	
	protected DataCollectionScreenStatus profileDataScreen1 = new DataCollectionScreenStatus();
	protected DataCollectionScreenStatus profileDataScreen2 = new DataCollectionScreenStatus(this instanceof MoneyGramSendTransaction ? DataCollectionSet.SENDER_PROFILE_SCREEN_2_PANEL_NAMES : DataCollectionSet.RECEIVER_PROFILE_SCREEN_2_PANEL_NAMES, null);
	private List<DataCollectionPanel> supplementalDataCollectionPanels = new ArrayList<DataCollectionPanel>();
	private ValidationType validationType;
	
	public TransactionLookupResponse getTransactionLookupResponse() {
		return transactionLookupResponse;
	}

	public void setTransactionLookupResponse(TransactionLookupResponse transactionLookupResponse) {
		this.transactionLookupResponse = transactionLookupResponse;
	}

	public Payload getTransactionLookupResponsePayload() {
		return transactionLookupResponsePayload;
	}

	public void setTransactionLookupResponsePayload(Payload transactionLookupResponsePayload) {
		this.transactionLookupResponsePayload = transactionLookupResponsePayload;
	}

	public boolean isClientStatus() {
		return clientStatus;
	}

	public void setClientStatus(boolean clientStatus) {
		this.clientStatus = clientStatus;
	}

	public static class DataCollectionScreenStatus implements Serializable {
		private static final long serialVersionUID = 1L;
		
		private Set<String> includePanelNames;
		private Set<String> excludePanelNames;
		private Set<String> panelNames;
		private ScreenStatusType status;
		
		public enum ScreenStatusType {
			REQUIRED_FULLFILLED,
			REQUIRED_NEEDS_DATA,
			DO_NOT_DISPLAY
		}
		
		public DataCollectionScreenStatus() {
			status = ScreenStatusType.DO_NOT_DISPLAY;
		}
		
		public DataCollectionScreenStatus(List<String> includePanelNames, List<String> excludePanelNames) {
			status = ScreenStatusType.DO_NOT_DISPLAY;
			this.includePanelNames = new HashSet<String>();
			for (String panelName : includePanelNames) {
				this.includePanelNames.add(panelName);
			}
			this.excludePanelNames = new HashSet<String>();
			if (excludePanelNames != null) {
				for (String panelName : excludePanelNames) {
					this.excludePanelNames.add(panelName);
				}
			}
			this.panelNames = new HashSet<String>();
		}
		
		public void setScreenStatus(ScreenStatusType status) {
			this.status = status;
		}
		
		public boolean hasDataToCollect() {
			return status.equals(ScreenStatusType.REQUIRED_FULLFILLED) || status.equals(ScreenStatusType.REQUIRED_NEEDS_DATA);
		}
		
		public boolean requiredDataNotFullfilled() {
			return status.equals(ScreenStatusType.REQUIRED_NEEDS_DATA);
		}
		
		public ScreenStatusType getStatus() {
			return status;
		}
		
		public List<DataCollectionPanel> getDataCollectionPanels(DataCollectionSet data) {
			List<DataCollectionPanel> panels = new ArrayList<DataCollectionPanel>();
			for (String panelName : panelNames) {
				DataCollectionPanel dcp = data.getDataCollectionPanelMap().get(panelName);
		        if (dcp != null) {
		        	panels.add(dcp);
		        }
			}
			return panels;
		}
		
		public List<String> getDataCollectionPanelNames(DataCollectionSet data) {
			List<String> panels = new ArrayList<String>();
			for (String panelName : panelNames) {
				DataCollectionPanel dcp = data.getDataCollectionPanelMap().get(panelName);
		        if (dcp != null) {
		        	panels.add(dcp.getInfoKey());
		        }
			}
			return panels;
		}
		
		public boolean needLayout(DataCollectionSet data) {
			boolean flag = false;
			for (String panelName : panelNames) {
				DataCollectionPanel dcp = data.getDataCollectionPanelMap().get(panelName);
		        if ((dcp != null) && (dcp.isLayoutFlag())) {
		        	flag = true;
		        	break;
		        }
			}
			return flag;
		}

		public void calculateDataCollectionScreenStatus(DataCollectionSet data) {
			boolean required = false;
			boolean fullfilled = true;
			List<DataCollectionPanel> panels = getDataCollectionPanels(data);
			for (DataCollectionPanel panel : panels) {
				if (panel != null) {
					boolean r = DataCollectionScreenToolkit.hasDataFieldsToCollect(panel, data);
					required = required || r;
					boolean f = DataCollectionScreenToolkit.hasRequiredDataFieldsFulfilled(panel, data);
					fullfilled = fullfilled && f;
					if (required && ! fullfilled) {
						status = ScreenStatusType.REQUIRED_NEEDS_DATA;
						return;
					}
				}
			}
			if (required) {
				status = ScreenStatusType.REQUIRED_FULLFILLED;
			} else {
				status = ScreenStatusType.DO_NOT_DISPLAY;
			}
		}
		
		public Set<String> getPanelNames(DataCollectionSet data) {
			this.panelNames.clear();
			for (DataCollectionPanel dcp : data.getDataCollectionPanelMap().values()) {
				if (includePanelNames.contains(dcp.getInfoKey())) {
					addPanelNames(dcp);
				}
			}
			return panelNames;
		}
		
		public Set<String> getPanelNames() {
			return panelNames;
		}
		
		private void addPanelNames(DataCollectionPanel rootDcp) {
			this.panelNames.add(rootDcp.getInfoKey());
			if (rootDcp.getChildDataCollectionPanels() != null) {
				for (DataCollectionPanel childDcp: rootDcp.getChildDataCollectionPanels().values()) {
					if (! excludePanelNames.contains(childDcp.getInfoKey())) {
						addPanelNames(childDcp);
					}
				}
			}
		}
	}
	
    public MoneyGramClientTransaction(String tranName, String tranReceiptPrefix, int docSeq) {
        super(tranName, tranReceiptPrefix, docSeq);
        // set this value so we know to dial the host when we need to
        hostConnectionRequired = true;
    }

    public MoneyGramClientTransaction(String tranName, String tranReceiptPrefix) {
        this(tranName, tranReceiptPrefix, -1);
    }

	public DataCollectionSet getDataCollectionData() {
		return this.dataCollectionData;
	}
	
	public void setDataCollectionData(DataCollectionSet dataCollectionData) {
		this.dataCollectionData = dataCollectionData;
	}
	
	abstract public void setValidationStatus(DataCollectionStatus status);
	abstract public void validation(FlowPage callingPage);
	abstract public String nextDataCollectionScreen(String currentScreen);
	abstract public String getDestinationCountry();
	abstract public void setScreensToShow();
	abstract public void applyHistoryData(CustomerInfo customerInfo);
	abstract public void applyCurrentData();
	abstract public void applyProfileData(ConsumerProfile profile);
	abstract public void applyTransactionLookupData();
	abstract public CustomerInfo getCustomerInfo();
	abstract public void getConsumerProfile(final ConsumerProfile profile, String mgiSessionID, int rule, Map<String, String> currentValues, CommCompleteInterface cci);
	abstract public boolean createOrUpdateConsumerProfile(ConsumerProfile profile, ProfileStatus status);
	abstract public void createOrUpdateConsumerProfile(final ConsumerProfile profile, CommCompleteInterface cci);
	abstract public Response getConsumerProfileResponse();

   /**
     * Get a list of all the valid types for Government Issued IDs. Make the
     *   first item in the list the correct default type.
     */
    public String[] getLegalIDNames() {
        return DWValues.getLegalIdNames();
    }

    public String[] getPhotoIDNames() {
        return DWValues.getPhotoIdNames();
    }

    public void setNewCustomer(boolean newNewCustomer) {
        newCustomer = newNewCustomer;
    }

    public boolean isNewCustomer() {
        return newCustomer;
    }

	public int getFormFreeStatus() {
		return formFreeStatus;
	}

	public void setFormFreeStatus(int formFreeStatus) {
		this.formFreeStatus = formFreeStatus;
	}
	
	public String formatFullName(String firstName, String middleName, String lastName1, String lastName2) {
		StringBuffer sb = new StringBuffer();
		if (firstName != null) {
			sb.append(firstName);
		}
		if ((middleName != null) && (! middleName.equals(""))) {
			if (sb.length() > 0) {
				sb.append(" ");
			}
			sb.append(middleName);
		}
		if (lastName1 != null) {
			if (sb.length() > 0) {
				sb.append(" ");
			}
			sb.append(lastName1);
		}
		if ((lastName2 != null) && (! lastName2.equals(""))) {
			if (sb.length() > 0) {
				sb.append(" ");
			}
			sb.append(lastName2);
		}
		return sb.toString();
	}
	
	public enum ValidationType {
		INITIAL_NON_FORM_FREE,
		INITIAL_FORM_FREE,
		INITIAL_EXPEDITIED_LOAD,
		SECONDARY
	};
	
	public enum ProfileStatus {
		NO_PROFILE,
		USE_CURRENT_PROFILE,
		CREATE_PROFILE,
		EDIT_PROFILE,
		PROFILE_COMPLETED 
	}
	
	public void addCountryStateActionListener(DataCollectionField countryField, JComboBox countryBox, final DataCollectionField subdivisionField) {

		// Make sure the state drop down component for the subdivision has been created and
		// the current value.
		
		final CountrySubdivision defaultSubdivision;
		StateListComboBox<CountrySubdivision> subdivisionBox = subdivisionField.getDataComponent() instanceof StateListComboBox ? (StateListComboBox<CountrySubdivision>) subdivisionField.getDataComponent() : null;
		if (subdivisionBox == null) {
			//create state/country drop down, with no data
			subdivisionBox = (StateListComboBox<CountrySubdivision>) subdivisionField.createComponent("");
			subdivisionField.setDataComponent(subdivisionBox);
			defaultSubdivision = new CountrySubdivision();
		} else {
			CountrySubdivision currentSubdivision = (CountrySubdivision) subdivisionBox.getSelectedItem();
			defaultSubdivision = ((currentSubdivision != null) && (currentSubdivision.toString() != null)) ? currentSubdivision : new CountrySubdivision();
		}
		
		// Add a listener to change the values in the state drop down list when
		// the value of the country is changed.

        if ((countryBox != null) && (subdivisionBox != null)) {
        	final JComboBox cb = countryBox;
        	countryBox.addActionListener(new ActionListener() {
            	@Override
    			public void actionPerformed(ActionEvent e) {
                	updateStateList((Country) cb.getSelectedItem(), subdivisionField, defaultSubdivision);
            	}
            });
        }

        // Populate the subdivision drop down list and set it to it's current value.

        if (subdivisionBox != null) {
	        
	        subdivisionField.setDefaultValue(defaultSubdivision.getCountrySubdivisionCode());
	        subdivisionField.setParentFieldSpecified(true);
	        subdivisionField.setParentCountry(countryField);
        }
	}
	
	private void updateStateList(Country country, DataCollectionField stateField, CountrySubdivision agentState) {
		if (! (stateField.getDataComponent() instanceof MultiListComboBox)) {
			return;
		}
		
		if (stateField.isChildField()) {
			if (! stateField.isChildFieldVisible()) {
				if (Messages.getString("DWValues.Select").equalsIgnoreCase(country.getPrettyName()) 
						|| Messages.getString("DWValues.None").equalsIgnoreCase(country.getPrettyName())) {
					//If selected item of country drop down is '--None--' or '--Select --'
					//then state/province drop down should be disabled
					stateField.getDataComponent().setEnabled(false);
				}
				return;
			}
		}
		
		MultiListComboBox<CountrySubdivision> stateBox = (MultiListComboBox<CountrySubdivision>) stateField.getDataComponent(); 
		stateBox.removeAllItems();
		List<CountrySubdivision> countrySubdivisionList = country != null ? country.getCountrySubdivisionList() : null;
		if ((countrySubdivisionList != null) && (countrySubdivisionList.size() > 0)) {
			stateBox.reset();
			stateBox.setEnabled(true);
			stateBox.addList(countrySubdivisionList, "countrySubdivisions");
			// Added new Top item :  "--Select--",  or "-- None--"
			String displayValue = stateField.isRequired() ? Messages.getString("DWValues.Select") : Messages.getString("DWValues.None");
			CountrySubdivision cs = new CountrySubdivision();
			cs.setCountrySubdivisionCode("");
			cs.setPrettyName(displayValue);
			stateBox.insertItemAt(cs, 0);
			DataCollectionField.enablableMofifyTracking(false);
			stateBox.setSelectedIndex(0);
			DataCollectionField.enablableMofifyTracking(true);
		} else {
			stateBox.setEnabled(false);
		}
	}
	
	public ValidationType getValidationType() {
		return this.validationType;
	}
	
	public void processValidationResponse(ValidationType validationType, List<InfoBase> fieldsToCollectTagList, List<BusinessError> errors, boolean readyForCommit, List<String> excludedPanels) {
		
		this.validationType = validationType;
		
		// Clear any old business errors.

		dataCollectionData.clearBusinessErrorList();
		
        // Check if any fieldsToCollect information that was returned in the receiveValidation response.
		
		if ((fieldsToCollectTagList != null) && (fieldsToCollectTagList.size() > 0)) {
			
        	// Mark all current data collection items as unknown status.
			
			for (DataCollectionPanel dcp : dataCollectionData.getDataCollectionPanelMap().values()) {
				dcp.markFieldsCollected(DataCollectionField.SCREEN_COLLECTION_UNDEFINED);
			}
			
			// Process any fields to collect tags that come back in the response.
			
			int collectionStatus = (validationType.equals(ValidationType.INITIAL_NON_FORM_FREE) || validationType.equals(ValidationType.INITIAL_FORM_FREE)) ? DataCollectionField.SCREEN_COLLECTION_UNDEFINED : DataCollectionField.ERROR_SCREEN_COLLECTION;
			for (InfoBase fieldsToCollectTag : fieldsToCollectTagList) {
				if (fieldsToCollectTag instanceof CategoryInfo) {
					DataCollectionPanel.getDataCollectionPanel((CategoryInfo) fieldsToCollectTag, null, dataCollectionData, null, 0, collectionStatus, null, null);
				}
			}
			dataCollectionData.updated();
		}
		
		// Link and country and country subdivision fields.
		
		for (DataCollectionField countryField : dataCollectionData.getFieldMap().values()) {
			if (countryField.getDataType() == DataCollectionField.COUNTRY_FIELD_TYPE) {

				// Make sure the country drop down list component has been defined and
				// determine the current value.
				
				JComboBox countryBox = (JComboBox) countryField.getDataComponent();
				if (countryBox == null) {
					// Create Country Drop down with no item
					countryBox = (JComboBox) countryField.createComponent("");
					countryField.setDataComponent(countryBox);
				}
				
				// Remove any current action listeners from the country drop down list component.
				
				for (ActionListener l : countryBox.getActionListeners()) {
					countryBox.removeActionListener(l);
				}
				
				String countryKey = countryField.getInfoKey();
				String countryParentValue = countryField.getParentValue() != null ? countryField.getParentValue() : ""; 
				String subdivisionKey = countryKey + "_SubdivisionCode";
				List<DataCollectionField> subdivisionList = dataCollectionData.getFieldsByInfoKey(subdivisionKey);
				if ((countryParentValue != null) && (! countryParentValue.isEmpty())) {
					boolean countryVisible = countryField.isChildFieldVisible();
					for (DataCollectionField subdivisionField : subdivisionList) {
						boolean subdivisionVisible = subdivisionField.isChildField();
						String subdivisionParentValue = subdivisionField.getParentValue() != null ? subdivisionField.getParentValue() : ""; 
						
						if ((countryVisible && subdivisionVisible) || (countryParentValue.equals(subdivisionParentValue)) || (countryVisible && (subdivisionParentValue.isEmpty()))) {
							addCountryStateActionListener(countryField, countryBox, subdivisionField);
						}
					}
				} else {
					for (DataCollectionField subdivisionField : subdivisionList) {
				        addCountryStateActionListener(countryField, countryBox, subdivisionField);
					}
				}
			}
		}
		
//		//----------------------------------------------------------------------
//		// Additional Fields to Collect
//		//----------------------------------------------------------------------
//		
//		if ((fieldsToCollectTagList != null) && (fieldsToCollectTagList.size() > 0)) {
//			
//			// Get a list of dataCollectionItems to display on the error collection screen.
//			
//			dataCollectionData.setAdditionalDataCollectionFields(dataCollectionData.getAdditionalDataCollectionFields(excludedPanels));
//			Collections.sort(getDataCollectionData().getAdditionalDataCollectionFields());
//		} else {
//			getDataCollectionData().setAdditionalDataCollectionFields(new ArrayList<DataCollectionField>());
//		}
//			
//		// Reset the collection status of the fields to collect..
//			
//		setFieldCollectionStatus();
//			
//		ExtraDebug.println("Number of new fields = " + getDataCollectionData().getAdditionalDataCollectionFields().size());
		
		//----------------------------------------------------------------------
		// Business Error Processing
		//----------------------------------------------------------------------
		
		// Clear any previous business error messages from the data collection items.
		
		for (DataCollectionField item : dataCollectionData.getFieldMap().values()) {
			if (item.getBusinessErrors() != null) {
				item.getBusinessErrors().clear();
			}
			if (item.getErrorMessagePanel() != null) {
				item.getPanel().remove(item.getErrorMessagePanel());
				item.setErrorMessagePanel(null);
			}
		}

		// Check if errors were returned in the receiveValidation response.
		
		final List<String> errorsMsgs = new ArrayList<String>();
		ExtraDebug.println("Number of errors = " + (errors != null ? errors.size() : 0));
		
		if ((errors != null) && (errors.size() > 0)) {
			for (BusinessError error : errors) {
				String offendingField = null;
				if (error.getOffendingField() != null) {
					offendingField = error.getOffendingField();
					String errorCategory = error.getErrorCategory();
					ExtraDebug.println("Business Input Error = offendingField = " + offendingField + ", category = " + errorCategory);
					
					if ((offendingField != null) && (! offendingField.isEmpty())) {
						
						if (errorCategory.equals("CHECK_VERIFY") || errorCategory.equals("VALIDATION")) {
							DataCollectionField field = dataCollectionData.findVisibleFieldByInfoKey(offendingField);
							if (field != null) {
								if ((field.getVisibility() == DataCollectionField.REQUIRED) || (field.getVisibility() == DataCollectionField.OPTIONAL)) {
	
									field.addBusinessError(error);
									
									if (! dataCollectionData.getBusinessErrors().contains(field)) {
										dataCollectionData.getBusinessErrors().add(field);
										
//										// Add any child fields of this field to the addition fields to collect since
//										// a change it it's value may require other fields to be collected.
//										
//										if (field.getChildFieldList() != null) {
//											addChildFields(field, getDataCollectionData().getAdditionalDataCollectionFields());
//										}
										
										// If the field is a subdivision field, add the corresponding country field to the list.
										
										if ((field.getDataType() == DataCollectionField.STATE_FIELD_TYPE) && (field.getParentCountry() != null) && (! getDataCollectionData().getAdditionalDataCollectionFields().contains(field.getParentCountry()))) {
											getDataCollectionData().getAdditionalDataCollectionFields().add(field.getParentCountry());
										}
									}
									field.setCollectedStatus(DataCollectionField.ERROR_SCREEN_CORRECT_VALID);
								}
								getDataCollectionData().getAdditionalDataCollectionFields().remove(field);
								continue;
							}
						} 
					}
				}
				String errorMessage = MessageFacadeError.formatErrorMessageText(error.getMessageShort(), error.getMessage(), error.getDetailString(), error.getErrorCode(), error.getSubErrorCode(), offendingField);
				errorsMsgs.add(errorMessage);
			}
		}
		
		//----------------------------------------------------------------------
		// Additional Fields to Collect
		//----------------------------------------------------------------------
		
		if ((fieldsToCollectTagList != null) && (fieldsToCollectTagList.size() > 0)) {
			
			// Get a list of dataCollectionItems to display on the error collection screen.
			
			dataCollectionData.setAdditionalDataCollectionFields(dataCollectionData.getAdditionalDataCollectionFields(excludedPanels));
			Collections.sort(getDataCollectionData().getAdditionalDataCollectionFields());
		} else {
			getDataCollectionData().setAdditionalDataCollectionFields(new ArrayList<DataCollectionField>());
		}
			
		ExtraDebug.println("Number of new fields = " + getDataCollectionData().getAdditionalDataCollectionFields().size());
		
		// Determine the validation status.

		int numberAdditionalFields = getDataCollectionData().getAdditionalDataCollectionFields().size();
		int numberValidationFields = dataCollectionData.getBusinessErrors() != null ? dataCollectionData.getBusinessErrors().size() : 0;
		ExtraDebug.println("Number errors = " + errorsMsgs.size() + ", Business errors = " + (dataCollectionData.getBusinessErrors() != null ? dataCollectionData.getBusinessErrors().size() : 0) + ", Additional Fields = " + getDataCollectionData().getAdditionalDataCollectionFields().size());
		
		if (errorsMsgs.size() > 0) {
//			if (! CompileTimeFlags.isValidateAgainstWSDL()) {
			CommunicationDialog.getInstance().displayDialog(errorsMsgs);
//			}
			dataCollectionData.setValidationStatus(DataCollectionStatus.ERROR);
			return;
		} else if (readyForCommit) {
			dataCollectionData.setValidationStatus(DataCollectionStatus.VALIDATED);
		} else if ((numberAdditionalFields > 0) || (numberValidationFields > 0) || (! validationType.equals(ValidationType.SECONDARY))) {
			dataCollectionData.setValidationStatus(DataCollectionStatus.NEED_MORE_DATA);
		} else {
			dataCollectionData.setValidationStatus(DataCollectionStatus.ERROR);
		}
		
		ExtraDebug.println("Validation Status = " + dataCollectionData.getValidationStatus());
		
		// If this is the initial validation call, apply any data from history.
		
		if (dataCollectionData.getDataCollectionType().equals(DataCollectionType.PROFILE_EDIT)) {
			applyHistoryData(getCustomerInfo());
			
		} else if (dataCollectionData.getDataCollectionType().equals(DataCollectionType.VALIDATION)) {
			if (validationType.equals(ValidationType.INITIAL_NON_FORM_FREE) || validationType.equals(ValidationType.INITIAL_FORM_FREE) || validationType.equals(ValidationType.INITIAL_EXPEDITIED_LOAD)) {
				if ((this instanceof MoneyGramReceiveTransaction) || (validationType.equals(ValidationType.INITIAL_FORM_FREE))) {
					applyTransactionLookupData();
				}
				ConsumerProfile profile = this.getProfileObject();
				if (profile == null) {
					applyHistoryData(getCustomerInfo());
				} else {
					applyProfileData(profile);
				}
				applyCurrentData();
				dataCollectionData.setAfterValidationAttempt(false);
			} else {
				if (profile == null) {
					applyHistoryData(getCustomerInfo());
				} else {
					applyProfileData(profile);
				}
				dataCollectionData.setAfterValidationAttempt(true);
			}
		}

		// Determine the screens to show.
		
		DataCollectionType dataCollectionType = this.getDataCollectionData().getDataCollectionType();
		if (dataCollectionType.equals(DataCollectionType.PROFILE_EDIT) || dataCollectionType.equals(DataCollectionType.VALIDATION)) {
			setScreensToShow();
		}
	}
	
	public void transactionLookupCommon(TransactionLookupRequest transactionLookupRequest, String refNumber, String pin, CommCompleteInterface callingPage, String purposeOfLookup, boolean isInitial) {
		final MessageFacadeParam parameters = new MessageFacadeParam(MessageFacadeParam.REAL_TIME);

		clientStatus = Boolean.TRUE;
		try {
			AuditLog.writeReferenceLookupRecord(userID, refNumber);
			transactionLookupResponse = null;
			transactionLookupResponse = MessageFacade.getInstance().transactionLookup(transactionLookupRequest, callingPage, parameters, refNumber, getOperatorName(), pin, purposeOfLookup, isInitial);

			if ((transactionLookupResponse != null) && (transactionLookupResponse.getPayload() != null) && (transactionLookupResponse.getPayload().getValue() != null)) {

				transactionLookupResponsePayload = transactionLookupResponse.getPayload().getValue();
				
				// Save the current values returned by the transaction lookup.
				
				if (purposeOfLookup.equals(DWValues.TRX_LOOKUP_SEND_COMP) || purposeOfLookup.equals(DWValues.TRX_LOOKUP_BP_COMP)) {
					setDataCollectionData(new DataCollectionSet(DataCollectionType.VALIDATION));
				}

				getDataCollectionData().getCurrentValueMap().clear();
				for (KeyValuePairType objCurrentValues : transactionLookupResponsePayload.getCurrentValues().getCurrentValue()) {
					if (objCurrentValues != null) {
						String strXmlTag = objCurrentValues.getInfoKey();
						String strValue = objCurrentValues.getValue().getValue();
						FieldKey key = FieldKey.key(strXmlTag);
						getDataCollectionData().setFieldValue(key, strValue);
						getDataCollectionData().putCurrentValue(strXmlTag, strValue);
						getDataCollectionData().getDefaultValueMap().put(key, strValue);
					}
				}
				
				// Save the field info returned by the transaction lookup.
				
				transactionLookupInfos = new HashMap<String, FieldInfo>();
				if (transactionLookupResponsePayload.getInfos() != null) {
					processTransactionLookupInfo(transactionLookupResponsePayload.getInfos().getInfo());
				}
				
			} else {
				transactionLookupResponsePayload = null;
				if (getDataCollectionData() != null) {
					getDataCollectionData().getCurrentValueMap().clear();
					getDataCollectionData().getDefaultValueMap().clear();
				}
				clientStatus = Boolean.FALSE;
			}
		} catch (MessageFacadeError mfe) {
			clientStatus = Boolean.FALSE;
		}
	}
	
	private void processTransactionLookupInfo(List<InfoBase> infos) {
		if (infos != null) {
			for (InfoBase info : infos) {
				if (info instanceof CategoryInfo) {
					processTransactionLookupInfo(((CategoryInfo) info).getInfos().getValue().getInfo());
				} else if (info instanceof FieldInfo) {
					FieldInfo field = (FieldInfo) info;
					String key = field.getInfoKey();
					transactionLookupInfos.put(key, field);
				}
			}
		}
	}
	
	public String getTransactionLookupInfoLabel(String infoKey) {
		FieldInfo info = transactionLookupInfos.get(infoKey);
		return info != null ? info.getLabel() : "";
	}
	
	protected DataCollectionScreenStatus getDataCollectionScreenStatus(List<DataCollectionPanel> panels) {
		DataCollectionScreenStatus status = new DataCollectionScreenStatus();
		boolean required = false;
		boolean fullfilled = true;
		for (DataCollectionPanel panel : panels) {
			if (panel != null) {
				boolean r = DataCollectionScreenToolkit.hasDataFieldsToCollect(panel, this.getDataCollectionData());
				required = required || r;
				boolean f = DataCollectionScreenToolkit.hasRequiredDataFieldsFulfilled(panel, this.getDataCollectionData());
				fullfilled = fullfilled && f;
				if (required && ! fullfilled) {
					status.setScreenStatus(ScreenStatusType.REQUIRED_NEEDS_DATA);
					return status;
				}
			}
		}
		if (required) {
			status.setScreenStatus(ScreenStatusType.REQUIRED_FULLFILLED);
		} else {
			status.setScreenStatus(ScreenStatusType.DO_NOT_DISPLAY);
		}
		return status;
	}

	protected void setFormFreeIdFields(boolean flag) {
		this.formFreeIdFields = flag;
	}
	
	public boolean isFormFreeIdFields() {
		return this.formFreeIdFields;
	}

	public Request getValidationRequest() {
		return this.validationRequest;
	}
	
	public void setValidationRequest(Request request) {
		this.validationRequest = request;
	}
	
	public Map<String, FieldInfo> getTransactionLookupInfos() {
		return this.transactionLookupInfos;
	}

	public ConsumerProfile getProfileObject() {
		return this.profile;
	}

	public void setProfileObject(ConsumerProfile profile) {
		this.profile = profile;
	}

	private List<ConsumerProfile> searchProfiles;

	public List<ConsumerProfile> getSearchProfiles() {
		return this.searchProfiles;
	}
	
	public void searchConsumerProfiles(final DataCollectionSet data, final Map<String, String> values, final int rule, CommCompleteInterface cci) {
		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {
				searchConsumerProfilesResponse = MessageFacade.getInstance().searchConsumerProfiles(data, rule, values);
				
				List<ConsumerProfile> list = new ArrayList<ConsumerProfile>();
				if ((getSearchConsumerProfilesResponse() != null) && (getSearchConsumerProfilesResponse().getPayload() != null) && (getSearchConsumerProfilesResponse().getPayload().getValue() != null) && (getSearchConsumerProfilesResponse().getPayload().getValue().getConsumerProfileSearchInfos() != null)) {
					List<ConsumerProfileSearchInfo> profiles = getSearchConsumerProfilesResponse().getPayload().getValue().getConsumerProfileSearchInfos().getConsumerProfileSearch();
					for (ConsumerProfileSearchInfo info : profiles) {
						ConsumerProfile profile = new ConsumerProfile(info);
						list.add(profile);
					}
				}
				
				searchProfiles = list;
				
				return (searchConsumerProfilesResponse != null);
			}
		};
		MessageFacade.run(logic, cci, 0, Boolean.FALSE);
	}
	
	public SearchConsumerProfilesResponse getSearchConsumerProfilesResponse() {
		return this.searchConsumerProfilesResponse;
	}

	public ProfileStatus getProfileStatus() {
		return profileStatus;
	}

	public void setProfileStatus(ProfileStatus profileStatus) {
		this.profileStatus = profileStatus;
	}

	public DataCollectionSet getSearchProfilesData() {
		return searchProfilesData;
	}

	public void setSearchProfilesData(DataCollectionSet searchProfilesData) {
		this.searchProfilesData = searchProfilesData;
	}

	public void setSearchConsumerProfilesResponse(SearchConsumerProfilesResponse searchConsumerProfilesResponse) {
		this.searchConsumerProfilesResponse = searchConsumerProfilesResponse;
	}

	public boolean isDataCollecionReview() {
		return this.isDataCollecionReview;
	}

	public void setDataCollecionReview(boolean isDataCollecionReview) {
		this.isDataCollecionReview = isDataCollecionReview;
	}
	
	/*
	 * Return the best value from first the data collection set, and if nothing there
	 * then from the profile
	 */
	public String getBestDataValue(FieldKey tag) {
		String sDCValue = null;
		DataCollectionSet dcsDCSet = getDataCollectionData();
		
		if (dcsDCSet != null) {
			sDCValue = dcsDCSet.getValue(tag);
		}
		
		if ((sDCValue != null) && (sDCValue.length() > 0)){
			return sDCValue;
		} else {
			DataCollectionSet dcsSPD = getSearchProfilesData();
			if (dcsSPD != null) {
				return dcsSPD.getValue(tag);
			} else {
				return null;
			}
		}
	}
	
	/*
	 * Return the best Occupation (taking into account other) from first the data collection set, and if nothing there
	 * then from the profile
	 */
	public String getBestOccupationValue(FieldKey tag1, FieldKey tag2) {
		String sOcc = getBestDataValue(tag1);
		
		if ((sOcc == null) || (sOcc.compareToIgnoreCase("Other") != 0)) {
			return sOcc;
		} else {
			return getBestDataValue(tag2);
		}
	}
	
	protected void setProfileDataCollectionScreensToShow(List<String> excludeList) {
		
		// Determine which of the data collection screens need to be shown.
		
		Set<String> panelSet = new HashSet<String>(this instanceof MoneyGramSendTransaction ? DataCollectionSet.SENDER_PROFILE_SCREEN_2_PANEL_NAMES : DataCollectionSet.RECEIVER_PROFILE_SCREEN_2_PANEL_NAMES);
		panelSet.addAll(profileDataScreen2.getPanelNames(getDataCollectionData()));
		this.profileDataScreen2.calculateDataCollectionScreenStatus(getDataCollectionData());
		
		List<DataCollectionPanel> supplementalDataCollectionPanels = getDataCollectionData().getSupplementalDataCollectionPanels(panelSet);
		this.setExcludePanelNames(panelSet);
		setSupplementalDataCollectionPanels(supplementalDataCollectionPanels);
		this.profileDataScreen1 = getDataCollectionScreenStatus(supplementalDataCollectionPanels);

		ExtraDebug.println("Profile Data Screen 1 Status = " + profileDataScreen1.getStatus().toString());
		ExtraDebug.println("Profile Data Screen 2 Status = " + profileDataScreen2.getStatus().toString());
	}
	
	public DataCollectionScreenStatus getProfileDataScreen1() {
		return this.profileDataScreen1;
	}
	
	public DataCollectionScreenStatus getProfileDataScreen2() {
		return this.profileDataScreen2;
	}

	public void setSupplementalDataCollectionPanels(List<DataCollectionPanel> supplementalDataCollectionPanels) {
		this.supplementalDataCollectionPanels = supplementalDataCollectionPanels;
	}
	
	public List<DataCollectionPanel> getSupplementalDataCollectionPanels() {
		return this.supplementalDataCollectionPanels;
	}

	public void setExcludePanelNames(Set<String> excludePanelNames) {
		this.excludePanelNames = excludePanelNames;
	}

	public Set<String> getExcludePanelNames() {
		return this.excludePanelNames;
	}
	
	public boolean isProfileEdited() {
		for (DataCollectionField field : this.getDataCollectionData().getFieldMap().values()) {
			if (field.isModified()) {
				return true;
			}
		}
		return false;
	}
	
	protected String getOperatorName() {
		String operatorName = "";
		if (isPasswordRequired()) {
			operatorName = userProfile.find("NAME").stringValue();
			if ((operatorName != null) && (operatorName.length() > 7)) {
				operatorName = userProfile.find("NAME").stringValue().substring(0, 7);
			}
		}
		return operatorName;
	}
	
	/*
	 * Return the best ID Type (taking into ID Verification) from first the data collection set, and if nothing there
	 * then from the profile
	 */
	protected String getBestIdTypeValue(FieldKey fkChoice, FieldKey fkIdType) {
		String sIDType;
		String sIDChoice;
		
		sIDChoice = getBestDataValue(fkChoice);
		sIDType = getBestDataValue(fkIdType);
		if (!((sIDChoice == null) || (sIDChoice.compareToIgnoreCase("NEW") == 0))) {
			int typeLen = sIDChoice.length();
			if (typeLen >= 3) {
				sIDType = sIDChoice.substring(0, 3);
			} else if (typeLen > 0)  {
				sIDType = sIDChoice.substring(0, typeLen);
			} else {
				sIDType = "";
			}
		}
		return sIDType;
	}
	
	/*
	 * Return the best ID Number (taking into ID Verification) from first the data collection set, and if nothing there
	 * then from the profile
	 */
	protected String getBestIdNumber(FieldKey fkChoice, FieldKey fkIdNbr, FieldKey fkVerify) {
		String sIDNumber;
		String sIDChoice;
		
		sIDChoice = getBestDataValue(fkChoice);
		sIDNumber = getBestDataValue(fkIdNbr);
		if (!((sIDChoice == null) || (sIDChoice.compareToIgnoreCase("NEW") == 0))) {
			sIDNumber = getBestDataValue(fkVerify);
		}
		return sIDNumber;
	}
	
}
