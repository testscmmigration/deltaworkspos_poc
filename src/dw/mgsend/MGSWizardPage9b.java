package dw.mgsend;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import dw.accountdepositpartners.AccountDepositPartnersTransaction;
import dw.framework.DataCollectionField;
import dw.framework.DataCollectionScreenToolkit;
import dw.framework.DataCollectionSet;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;
import dw.utility.Debug;
import dw.utility.ExtraDebug;

public class MGSWizardPage9b extends MoneyGramSendFlowPage {
	private static final long serialVersionUID = 1L;

	private JScrollPane scrollPane1 = null;
	private JScrollPane scrollPane2 = null;
	private JPanel panel2 = null;
	private DataCollectionScreenToolkit toolkit = null;
	private Object dataCollectionSetChangeFlag = new Object();
	private JButton nextButton;

	public MGSWizardPage9b(AccountDepositPartnersTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		this((MoneyGramSendTransaction) tran, name, pageCode, buttons); 
	}

	public MGSWizardPage9b(MoneyGramSendTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super(tran, "moneygramsend/MGSWizardPage9b.xml", name, pageCode, buttons); 
		nextButton = this.flowButtons.getButton("next");
	}

	private void setupComponents() {
		
		// Check if the GFFP information has changed.
		
		DataCollectionSet data = this.transaction.getDataCollectionData();

		if (this.dataCollectionSetChangeFlag != this.transaction.getDataCollectionData().getChangeFlag()) {
	    	this.dataCollectionSetChangeFlag = this.transaction.getDataCollectionData().getChangeFlag();
	    	
	    	// Rebuild the data collection items for the screen.
	    	
	    	ExtraDebug.println("Rebuilding Screen " + getPageCode() + " from FTC Data");
	    	
			int flags = DataCollectionScreenToolkit.SHORT_LABEL_VALUES_FLAG + DataCollectionScreenToolkit.DISPLAY_REQUIRED_FLAG;
			JButton backButton = this.flowButtons.getButton("back");
			this.toolkit = new DataCollectionScreenToolkit(this, this.scrollPane1, this, this.scrollPane2, this.panel2, flags, transaction.getReceiverAccountInformation2ScreenStatus().getDataCollectionPanels(data), this.transaction, backButton, nextButton, transaction.getDestination().getCountryCode());
		}
		
		// Check if the screen needs to be laid out
		
		if (transaction.getReceiverAccountInformation2ScreenStatus().needLayout(data)) {
			toolkit.populateDataCollectionScreen(panel2, transaction.getReceiverAccountInformation2ScreenStatus().getDataCollectionPanels(data), false);
		}
	}

	@Override
	public void start(int direction) {
		scrollPane1 = (JScrollPane) getComponent("scrollPane1");
		scrollPane2 = (JScrollPane) getComponent("scrollPane2");
		panel2 = (JPanel) getComponent("panel2");
		
		try {
			flowButtons.reset();
			flowButtons.setVisible("expert", false); 
			flowButtons.setEnabled("expert", false); 
			
			// Make sure the screen has been built.
			
			setupComponents();
			
			transaction.setDestOrDOChanged(false);
		
			// Populate the screen with any previously entered data.
			
			if (direction == PageExitListener.NEXT || direction == PageExitListener.BACK) {
				DataCollectionScreenToolkit.restoreDataCollectionFields(toolkit.getScreenFieldList(), transaction.getDataCollectionData());
			}
			
			// Move the cursor to 1) the first required item with no value or item 
			// with an invalid value or 2) the first optional item with no value.
			
			toolkit.moveCursor(toolkit.getScreenFieldList(), nextButton);

			// Enable and call the resize listener.
			
			toolkit.enableResize();
			toolkit.getComponentResizeListener().componentResized(null);
		} catch (Exception e) {
			Debug.printStackTrace(e);
		}
	}
	
	@Override
	public void finish(int direction) {
		toolkit.disableResize();
		flowButtons.setButtonsEnabled(false);
		
    	// If the flow direction is to the next screen, check the validity of the
    	// data. If a value is invalid or a required item does not have a value,
    	// return to the screen.
    	
    	if (direction == PageExitListener.NEXT) {
        	String tag = toolkit.validateData(toolkit.getScreenFieldList(), DataCollectionField.DISPLAY_ERRORS_VALIDATION);
        	if (tag != null) {
				flowButtons.setButtonsEnabled(true);
				toolkit.enableResize();
        		return;
        	}
        }
    	
    	// If the flow direction is to the next screen or back to the previous
    	// screen, save the value of the current items in a data structure.

        if ((direction == PageExitListener.BACK) || (direction == PageExitListener.NEXT)) {
        	DataCollectionScreenToolkit.saveDataCollectionFields(toolkit.getScreenFieldList(), transaction.getDataCollectionData());
        }
		
		if ((direction == PageExitListener.NEXT)) {
			
			
			String nextPage = transaction.nextDataCollectionScreen(MoneyGramSendWizard.MSW63_DIRECTED_SEND_PARTNER_DETAIL);
	    	if (nextPage.isEmpty() && transaction.getDataCollectionData().isAdditionalInformationScreenValid()) {
				transaction.sendValidation(this, ValidationType.SECONDARY);
			} else {
				PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
			}
		
		} else if (direction == PageExitListener.CANCELED) {
			PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
		
		} else if (direction == PageExitListener.BACK) {
			transaction.setRegistrationNumber("");
			transaction.setCustomerReceiveNumber("");
			PageNotification.notifyExitListeners(this, PageExitListener.BACK);
		
		} else {
			PageNotification.notifyExitListeners(this, direction);
		}

		toolkit.enableResize();
	}
	
	@Override
	public void commComplete(int commTag, Object returnValue) {
		
		//----------------------------------------------------------------------
		// getBankDetailsByLevel
		//----------------------------------------------------------------------
		
		if (commTag == MoneyGramClientTransaction.SEND_VALIDATE) {
			PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
		}
	}
}