package dw.mgsend;

import java.math.BigDecimal;
import java.util.StringTokenizer;

import dw.dialogs.Dialogs;
import dw.dialogs.ManagerTranOverrideDialog;
import dw.framework.ClientTransaction;
import dw.framework.CommCompleteInterface;
import dw.framework.FlowPage;
import dw.framework.PageFlowButtons;
import dw.i18n.FormatSymbols;
import dw.printing.TEPrinter;
import dw.profile.UnitProfile;
import dw.reprintreceipt.ReprintData;
import dw.reprintreceipt.ReprintFile;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.ErrorManager;
import dw.utility.IdleBackoutTimer;
import dw.utility.StringUtility;

public abstract class MoneyGramSendFlowPage extends FlowPage {
	private static final long serialVersionUID = 1L;

    protected MoneyGramSendTransaction transaction;
    
    public MoneyGramSendFlowPage(MoneyGramSendTransaction transaction,
                    String fileName, String name, String pageCode, 
                    PageFlowButtons buttons, boolean displaySideBanner) {
        super(fileName, name, pageCode, buttons, displaySideBanner);
        this.transaction = transaction;
    }

    public MoneyGramSendFlowPage(MoneyGramSendTransaction transaction,
            String fileName, String name, String pageCode, 
            PageFlowButtons buttons) {
    	super(fileName, name, pageCode, buttons);
    	this.transaction = transaction;
    }

    // do-nothing impl of commCompleteInterface...will be overridden by each
    //  specific page where comm is needed.
    @Override
	public void commComplete(int commTag, Object returnValue) {}

    /**
     *  Saves the send transaction environment so a receipt can be reprinted 
     *  later
     */
    public void saveSendEnvironment(int tranType) {
    //  For now always save the environment just in case receipt get enabled and
    //  a reprint is needed, but keep this here in case requirements change.    	
    //
    	// Don't save the environment if receipts are not enabled.    	
//    	if (!transaction.isReceiptPrinterNeeded()) {
//    		return;
//    	}
 			if (!ReprintFile.prepOutputFile(tranType)) {
 			    Debug.println("saveSendEnvironment() - prepOutputFile() failed");
				return;
			} else {
				/*
				 * Don't need full account number for receipt reprint.
				 */
				String sAcctNbr = transaction.getBpAccountNumber();
				if ((sAcctNbr != null) && sAcctNbr.length() > 0) {
					transaction.setBpAccountNumber(StringUtility.stringToMaskedString(
							sAcctNbr));
				}
				if (!ReprintFile.saveObject(transaction)) {
	 			    Debug.println("saveSendEnvironment() - saveObject() failed");
					return;
				}
    			ReprintFile.closeOutputFile();
			}
    }

    public void printReceipt(boolean customerReceipt, boolean reprint,ReprintData reprintData) {
    	if (reprintData!=null) {
    		reprintData.setCustomerReceipt(customerReceipt);
    	}
        if ((TEPrinter.getPrinter() != null) && TEPrinter.getPrinter().isReady(this)) {
            transaction.printReceipt(customerReceipt, reprint);
        }
    }

    public void printConfirmationReceipt() {
        if (TEPrinter.getPrinter().isReady(this)) {
            transaction.printConfirmationReceipt();
        }
    }
    
    /**
     * Try to get the amount in the transaction and display an error to the user
     *    if this can not be done due to amount limits.
     */
    protected BigDecimal getAmount() {
        return transaction.getAmount();
    }
    /**
     * Try to set the amount in the transaction and display an error to the user
     *    if this can not be done due to amount limits.
     */
    protected boolean setAmount(BigDecimal amount) {
    	return setAmount(amount, transaction.agentBaseCurrency());
    }
    
    protected boolean setAmount(BigDecimal amount, String currency) {
    	transaction.setManagerOverrideFailed(false);
        int status = transaction.setAmount(amount, currency);
        if (status == MoneyGramClientTransaction.OK)
            return true;
        if (status == MoneyGramClientTransaction.MAX_ITEM_AMOUNT_EXCEEDED) {
            Dialogs.showWarning("dialogs/DialogMGMaxItemAmountExceeded.xml",	
                    transaction.getMaxItemAmount(currency).toString().concat(" "+currency));
        }
        else if (status == MoneyGramClientTransaction.DAILY_LIMIT_EXCEEDED) {
            String formattedDailyLimit = FormatSymbols.convertDecimal(transaction.getDailyLimit().toString());
            String formattedDailyTotal = FormatSymbols.convertDecimal(transaction.getDailyTotal().toString());
            Dialogs.showWarning("dialogs/DialogDailyLimitExceeded.xml",	
                                 formattedDailyLimit.concat(currencyLabel),
                                 formattedDailyTotal.concat(currencyLabel));
        }
        else if (status == MoneyGramClientTransaction.MAX_EMPLOYEE_SEND_LIMIT_EXCEEDED) {
        	transaction.setMaxEmployeeLimitExceeded(true);
        	boolean temp = getTransactionLimitOverride(currency);
        	if (!temp)
        		transaction.setManagerOverrideFailed(true);
        	return temp;       	
        }
        else if (status == MoneyGramClientTransaction.TRANSACTION_AMOUNT_EXCEEDED) {
            Debug.println("Tran limit exceeded.");	
        }
        else if(status == MoneyGramClientTransaction.BILL_PAY_DAILY_LIMIT_EXCEEDED){
            String formattedDailyBillPayLimit = FormatSymbols.convertDecimal(transaction.getDailyBillPayLimit().toString());
            String formattedDailyBillPayTotal = FormatSymbols.convertDecimal(transaction.getDailyBillPayTotal().toString());
            Dialogs.showWarning("dialogs/DailogBillPayDailyLimitExceeded.xml",	
                    formattedDailyBillPayLimit.concat(currency),
                    formattedDailyBillPayTotal.concat(currency));  
        }
        return false;
    }
    /**
     * Prompt the user for a manager override approval. If this transaction
     * required a name and password, the manager name and password must be
     * entered. Otherwise, just a manager password is required.
     * 
     * This method was originally written to give the user three tries to 
     * get it right, but there was no definition on what was supposed to
     * happen after the third try. As of 4.2 it loops until you get enter
     * a valid PIN or cancel the override dialog.
     * 
     * @return true if a valid mgr pin/password with the right permission
     * is entered.
     */
    public boolean getTransactionLimitOverride(String currency) {
        String name = null;
        String password = null;
        int userID = -1;
        int cnt = 0;

        while (true) {

            // ManagerTranOverrideDialog used to be a subclass of NoTimeoutDialog,
            // meaning it automatically paused the idle timer while displayed.
            // Dialogs.java doesn't support that, so the code asking for the 
            // dialog has to manage the timer.
        
            boolean timerPending = IdleBackoutTimer.isRunning();
           // If the agent is in multi agent mode, do not prompt for name/password.
           // set the transaction over ride flag to true and return true.
            if(UnitProfile.getInstance().isMultiAgentMode()){
                transaction.tranLimitOverridden = true;
                return true;
            }
            ManagerTranOverrideDialog dialog = new ManagerTranOverrideDialog(
                    transaction.getEmployeeMaxSendAmount(currency).toString(),
                    transaction.isNamePasswordRequired());
            Dialogs.showPanel(dialog);
            if (timerPending) IdleBackoutTimer.start();

            name = dialog.getName();
            password = dialog.getPassword();
            userID = ClientTransaction.getUserID(name, password, false);
            
            transaction.setManagerOverrideID(userID);

            if (dialog.isCanceled()) {
                return false;
            }
            else {      // ! dialog.isCanceled()
                // validate the manager's name and/or password
                if (name != null) {
                    if (transaction.overrideTransactionLimit(name, password)) {
                        
                        if(transaction.isUserLocked(name,password)){
                        	Dialogs.showWarning("dialogs/DialogAccountLocked.xml"); 
                        	ErrorManager.recordLoginAttempt(false, name);
                        	return false;
                        }
                        else{
                        	int user =transaction.isPasswordChangeRequired(name,password,userID);
                        	if(user > 0){
                        		ErrorManager.recordLoginAttempt(true, name);
                        return true;
                        	}
                        	else {
                        		ErrorManager.recordLoginAttempt(false, name);
                        		return false;
                        	}
                        }
                    }
                    else {
                        ErrorManager.recordLoginAttempt(false, name);
                        Dialogs.showError("dialogs/DialogNotAuthorized.xml");
                        //continue;
                    }
                }
                else {
                    if (transaction.overrideTransactionLimit(password)) {
                        ErrorManager.recordLoginAttempt(true, name);
                        if(transaction.isUserLocked(name,password)){
                        	Dialogs.showWarning("dialogs/DialogAccountLocked.xml"); 
                        	ErrorManager.recordLoginAttempt(false, name);
                        	return false;
                        }
                        else{
                        	int user =transaction.isPasswordChangeRequired(name,password,userID);
                        	if(user > 0){
                        		ErrorManager.recordLoginAttempt(true, name);
                            return true;
                        	}
                        	else {
                        		ErrorManager.recordLoginAttempt(false, name);
                        		return false;
                        	}
                        }
                    }
                    else {
                        ErrorManager.recordLoginAttempt(false, name);
                        Dialogs.showError("dialogs/DialogNotAuthorized.xml");
                        //continue;
                    }
                }
            }
           cnt++;
           if (cnt > 2)
            	return false;
        }
    }
    
//    public Object getSelectedObject(int count, String value, JComboBox component) {
//    	for (int i = 0; i < count; i++) {
//    		DWComboBoxItem codeNamePair = (DWComboBoxItem)component.getItemAt(i);
//			if (codeNamePair.getValue().equals(value)) {
//				return codeNamePair;
//			}
//		}
//    	return null;
//    }
    
    public String stripNumber(String number) {
		String out = "";
		// strip off dashes and parenthesis
		StringTokenizer tok = new StringTokenizer(number, "()-"); 
		while (tok.hasMoreTokens())
			out += tok.nextToken();
		return out;
	}
    
    protected boolean validPromoCodeCount(String sData) {
    	boolean bValid = true;
    	if (sData != null && sData.length() > 0) {
        	String delims = "[,; ]+";
        	String[] promoArray = sData.split(delims);
        	int iMaxCodes = DWValues.getPromoCodeLimit();
        	int iPromoArrayLen = promoArray.length;
        	if (iPromoArrayLen > iMaxCodes) {
    			Dialogs.showWarning("dialogs/DialogInvalidPromoCodeCount.xml", 
    					" " + iMaxCodes + ".");
        		bValid = false;
        	}
//        	String promoCodeArray[] = new PromoCode[iPromoArrayLen];
//        	for (int ii = 0; ii < iPromoArrayLen; ii++) {
//        		promoCodeArray[ii] = new PromoCode();
//        		promoCodeArray[ii].setPromoCodeValue(promoArray[ii]);
//        	}
//            transaction.setEnteredPromoCodeArray(promoCodeArray);
            transaction.setEnteredPromoCodeArray(promoArray);
            transaction.setEnteredPromoCodeCount(iPromoArrayLen);
    	} else {
            transaction.setEnteredPromoCodeArray(null);
            transaction.setEnteredPromoCodeCount(0);
    	}
    	return bValid;
    }
}
