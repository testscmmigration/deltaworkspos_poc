package dw.mgsend;

import dw.accountdepositpartners.AccountDepositPartnersTransaction;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;

/**
 * "errorPage"
 * Page 15 of the MoneyGram Send Wizard. This page displays instructions on
 *   what to do if the transaction fails.
 */
public class MGSWizardPage15 extends MoneyGramSendFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	private boolean wizard = false;
    public MGSWizardPage15(MoneyGramSendTransaction tran,
                       String name, String pageCode, PageFlowButtons buttons, boolean wizard) {
        super(tran, "moneygramsend/MGSWizardPage15.xml", name, pageCode, buttons);	
        this.wizard  = wizard;
        init(wizard);
    }
    public MGSWizardPage15(AccountDepositPartnersTransaction tran,
            String name, String pageCode, PageFlowButtons buttons, boolean wizard) {
	super(tran, "moneygramsend/MGSWizardPage15.xml", name, pageCode, buttons);	
	this.wizard  = wizard;
	init(wizard);
	}
	private void init(boolean wizard) {
		if (wizard)
            removeComponent("okButton");	
        
        DWTextPane text1 = (DWTextPane) getComponent("text1");	
        text1.addListener(this, this);
	}

	@Override
	public void dwTextPaneResized() {
        DWTextPane text1 = (DWTextPane) getComponent("text1");	
        text1.setWidth(400);
	}

	@Override
	public void start(int direction) {
        if (! wizard) {
            addActionListener("okButton", this);	
            getComponent("okButton").requestFocus();	
        }
        else {
            flowButtons.reset();
            flowButtons.setVisible("expert", false);	
            flowButtons.setEnabled("next", false);	
            flowButtons.setEnabled("back", false);	
           // flowButtons.setAlternateText("cancel", "OK");	 
           flowButtons.setAlternateText("cancel",Messages.getString("OK"));  
            flowButtons.setSelected("cancel");	
        }
    }

    public void okButtonAction() {
        finish(PageExitListener.NEXT);
    }

    @Override
	public void finish(int direction) {
        PageNotification.notifyExitListeners(this, PageExitListener.FAILED);
    }
}
