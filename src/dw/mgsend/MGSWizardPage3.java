package dw.mgsend;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import dw.accountdepositpartners.AccountDepositPartnersTransaction;
import dw.dialogs.Dialogs;
import dw.dwgui.DWTable;
import dw.dwgui.DWTable.DWTableListener;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.mgsend.MoneyGramClientTransaction.ProfileStatus;
import dw.model.adapters.ConsumerProfile;
import dw.model.adapters.CustomerInfo;
import dw.model.adapters.ReceiverInfo;
import dw.printing.ReceiptReport;
import dw.utility.DWValues;
import dw.utility.ServiceOption;

/**
 * "senderSelection"
 * Page 3 of the MoneyGram Send Wizard. This page displays a table of customers
 * that match the home phone number given. The user may choose zero or one of
 * the customers listed in the table and continue.
 */
public class MGSWizardPage3 extends MoneyGramSendFlowPage implements MGSHistoryPage, DWTextPaneListener, DWTableListener {
	private static final long serialVersionUID = 1L;

	private static final int SEND_FROM = 0;
	private static final int SEND_ADDRESS = 1;
	private static final int SEND_TO = 2;
	private static final int DEST = 3;
	private static final int QUALIFIED_DELIVERY_OPTION = 4;

	protected DWTable table = null;
	private ListSelectionModel listSelectionModel;
	private DefaultTableModel tableModel = null;
	private List<CustomerInfo> customers = null;
	private ServiceOption qdo;
    private JLabel extraInfoLabel0;
    private JLabel extraInfoLabel1;
    private DWTextPane extraInfoData1;
    private JLabel extraInfoLabel2;
    private DWTextPane extraInfoData2;
    private JLabel extraInfoLabel3;
    private DWTextPane extraInfoData3;
    private JLabel extraInfoLabel4;
    private DWTextPane extraInfoData4;
    private JScrollPane scrollPane;
    private JPanel panel;
    private boolean bViaNext;
    private DWTextPane tp1;
	private DWTextPane messageTextArea;
	private int MIN_SCREEN_WIDTH=600;
	private JPanel descPanel;
	private JPanel accIdPanel;
	private static int TAB_COL_REC_NAME=2;
	private static int DATA_OBJ_INDX=5;
	private boolean serviceOptionChanged=false;
	
	public MGSWizardPage3(MoneyGramSendTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super(tran, "moneygramsend/MGSWizardPage3.xml", name, pageCode, buttons); 
		init();
	}

	public MGSWizardPage3(AccountDepositPartnersTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super(tran, "moneygramsend/MGSWizardPage3.xml", name, pageCode, buttons); 
		init();
	}
	
	private void init() {
		panel = (JPanel) getComponent("panel");
		tp1 = (DWTextPane) getComponent("text1");
		extraInfoLabel0 = (JLabel) getComponent("extraInfoLabel0");
		extraInfoLabel1 = (JLabel) getComponent("extraInfoLabel1");
		extraInfoData1 = (DWTextPane) getComponent("extraInfoData1");
		extraInfoLabel2 = (JLabel) getComponent("extraInfoLabel2");
		extraInfoData2 = (DWTextPane) getComponent("extraInfoData2");
		extraInfoLabel3 = (JLabel) getComponent("extraInfoLabel3");
		extraInfoData3 = (DWTextPane) getComponent("extraInfoData3");
		extraInfoLabel4 = (JLabel) getComponent("extraInfoLabel4");
		extraInfoData4 = (DWTextPane) getComponent("extraInfoData4");
		messageTextArea = (DWTextPane) getComponent("messageTextArea"); 
		table = (DWTable) getComponent("table");
		scrollPane = (JScrollPane) this.getComponent("matchingCustomerScrollpane");
		descPanel = (JPanel)this.getComponent("descPanel");
		accIdPanel = (JPanel)this.getComponent("accIdPanel");
		descPanel.setVisible(false);
		accIdPanel.setVisible(false);
		setupTable();
		table.addListener(this, this);
	}
	
	@Override
	public void dwTableResized() {
		dwTextPaneResized();

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.resizeTable();
			} 
		});
	}

	@Override
	public void dwTextPaneResized() {
		int tableWidth = table.getTable().getPreferredSize().width;
		int screenWidth = scrollPane.getVisibleRect().width - 20;
		int actualWidth = Math.min(Math.max(tableWidth, MIN_SCREEN_WIDTH), screenWidth);
		
		tp1.setWidth(actualWidth);
		int width = actualWidth - 140;
		extraInfoData1.setWidth(width);
		extraInfoData2.setWidth(width);
		width = (actualWidth - 260) / 2;
		extraInfoData3.setWidth(width);
		extraInfoData4.setWidth(width);
		width = actualWidth - 20;
		messageTextArea.setWidth(width);
	}

	@Override
	public void start(int direction) {
		flowButtons.reset();
		flowButtons.setVisible("expert", false); 
		JLabel phoneLabel = (JLabel) getComponent("phoneLabel"); 
		if (direction == PageExitListener.NEXT) {
			serviceOptionChanged=false;
			customers = transaction.getUniqueCustomers();
			phoneLabel.setText(transaction.getCardPhoneNumber());
			populatePeopleTable(customers);
			bViaNext = true;
		} else {
			bViaNext = false;
		}
		setSelectedOption();
		table.getTable().requestFocus();
		// set up the enter key for the table
		final JButton nextButton = flowButtons.getButton("next"); 
		final JButton backButton = flowButtons.getButton("back"); 
		final JButton cancelButton = flowButtons.getButton("cancel"); 

		KeyListener kl = new KeyAdapter() { 
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					e.consume();
					nextButton.doClick();
				}
				if (e.getKeyCode() == KeyEvent.VK_TAB) {
					e.consume();
					backButton.requestFocus();
				}
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					e.consume();
					cancelButton.doClick();
				}
				if (e.getKeyCode() == KeyEvent.VK_F2) {
					e.consume();
					transaction.f2SAR();
				}
			}
		};
		
		table.getTable().addKeyListener(kl);

		table.getTable().setRowSelectionInterval(transaction.getSelectedCustomerIndex(),
				transaction.getSelectedCustomerIndex());
		
		table.setListenerEnabled(true);
	}
	
	public static void scrollToVisible(JTable table, int rowIndex, int vColIndex) {
        if (!(table.getParent() instanceof JViewport)) {
            return;
        }
        JViewport viewport = (JViewport)table.getParent();
        Rectangle rect = table.getCellRect(rowIndex, vColIndex, true);
        Point pt = viewport.getViewPosition();
        rect.setLocation(rect.x-pt.x, rect.y-pt.y);
        table.scrollRectToVisible(rect);
    }

	private void setupTable() {
		
		// String[] columnNames = {"First", "Middle", "Last"};
		String[] headers = {
				Messages.getString("MGSWizardPage3.1998"),
				Messages.getString("MGSWizardPage3.1999"),
				Messages.getString("MGSWizardPage3.2000"),
				Messages.getString("MGSWizardPage3.2001"),
				Messages.getString("MGFQWizardPage2.986"),
				""
				};    
		tableModel = new DefaultTableModel(headers, 0) {
			private static final long serialVersionUID = 1L;
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		
		table.getTable().setModel(tableModel);
        listSelectionModel = table.getTable().getSelectionModel();
        listSelectionModel.addListSelectionListener(new SharedListSelectionHandler());
        
		TableColumnModel model = table.getTable().getColumnModel();
		table.getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		table.getTable().getTableHeader().setReorderingAllowed(false);
		table.getTable().removeColumn(table.getTable().getColumnModel().getColumn(DATA_OBJ_INDX));

		TableColumn col = model.getColumn(SEND_FROM);
    	col.setCellRenderer(table.new DWTableCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));

		col = model.getColumn(SEND_ADDRESS);
    	col.setCellRenderer(table.new DWTableCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));
		
		col = model.getColumn(SEND_TO);
    	col.setCellRenderer(table.new DWTableCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));

		col = model.getColumn(DEST);
    	col.setCellRenderer(table.new DWTableCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));

		col = model.getColumn(QUALIFIED_DELIVERY_OPTION);
    	col.setCellRenderer(table.new DWTableCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));
	}
	
	private void populatePeopleTable(List<CustomerInfo> people) {
		table.getTable().getSelectionModel().clearSelection();
		clearComponent(table.getTable());
		transaction.populatePeopleTable(this, people);
		table.initializeTable(scrollPane, panel, MIN_SCREEN_WIDTH, 10);
		table.setColumnStaticWidth(3);
		table.setColumnStaticWidth(4);
	}
	
	private void setSelectedOption() {
		if (!serviceOptionChanged) {
			if (transaction.getSelectedDeliveryInfo() != null) {
				DefaultTableModel dtm = (DefaultTableModel) table.getTable().getModel();
				int nRow = dtm.getRowCount();
				int count = 0;
				int selectedIndx = -1;
				for (int i = 0; i < nRow; i++) {
					ServiceOption deliveryInfo = (ServiceOption) dtm.getValueAt(i, DATA_OBJ_INDX);
					if (deliveryInfo != null) {
						boolean isSame = deliveryInfo.compareDeliveryInfo(transaction.getSelectedDeliveryInfo());
						if (isSame) {
							count++;
							selectedIndx = i;
						}
					}
				}
				if (count == 1) {
					transaction.setSelectedCustomerIndex(selectedIndx);
					table.getTable().getSelectionModel().setSelectionInterval(selectedIndx, selectedIndx);
				} else {
					transaction.setSelectedCustomerIndex(0);
					transaction.setServiceOptionChanged(true);
					table.getTable().getSelectionModel().setSelectionInterval(0, 0);
					serviceOptionChanged=true;
				}
			} else {
				serviceOptionChanged=true;
				transaction.setSelectedCustomerIndex(0);
			}
		}
	}

	@Override
	public void finish(int direction) {
		int selectedRow = table.getTable().getSelectedRow();
		transaction.setSelectedCustomerIndex(selectedRow);
		if (selectedRow > 0) {
			String receiverName = (String) table.getTable().getValueAt(selectedRow, TAB_COL_REC_NAME);
			
			CustomerInfo customer = customers.get(selectedRow - 1);
			
			if (direction == PageExitListener.NEXT) {
				transaction.setCustomerInfo(customer);
				transaction.setEditActive(false);
				String sTempRRN = null;
				if (customer.getReceiver().getCustomerReceiveNumber() != null) {
					sTempRRN = customer.getReceiver().getCustomerReceiveNumber().trim();
				}
				transaction.setRegistrationNumber(sTempRRN);
				transaction.setCustomerReceiveNumber(sTempRRN);
				transaction.setNewCustomer(false);
				
				transaction.setDirection1(customer.getReceiver().getDirection1());
				transaction.setDirection2(customer.getReceiver().getDirection2());
				transaction.setDirection3(customer.getReceiver().getDirection3());
				if (transaction.isBillPayment() && !transaction.isDirectoryOfBillers()) {
					transaction.setReceivingCompany(
							transaction.getRecentCompanies()[table.getTable().getSelectedRow() - 1]);
				} else {
					// If the transaction is a fee query, the destination is
					// already set.
					
					if (transaction.isFeeQuery()) {
						if (!transaction.getOtherString().equals(receiverName)) {
							if (!(transaction.getDestination().getCountryCode().trim()
									.equalsIgnoreCase(customer.getReceiver().getDestCountry()))
									|| !(transaction.getDeliveryOption()
											.equals(customer.getReceiver().getDeliveryOption()))) {
								transaction.setSelectedFeeTableIndex(-1);
							}
							transaction.setDestination(customer.getReceiver().getDestCountry());
							transaction.setDeliveryOption(customer.getReceiver().getDeliveryOption());
							transaction.setSendCurrency(customer.getReceiver().getSendCurrency());
						}
					} else {
						
						/*
						 * Don't mark the destination as changed if we got 
						 * here via a next button, since we really did not have
						 * a destination.
						 */
						if (!bViaNext && !transaction.getDestination().getCountryCode().trim()
								.equalsIgnoreCase(customer.getReceiver().getDestCountry())) {
							transaction.setDestinationChanged(true);
						} else {
							transaction.setDestinationChanged(false);
						}
						transaction.setDestination(customer.getReceiver().getDestCountry());
						transaction.setDeliveryOption(customer.getReceiver().getDeliveryOption());
						transaction.setSendCurrency(customer.getReceiver().getSendCurrency());
						
						if (transaction.isSendToAccount() && transaction.getOtherString().equals(receiverName)) {
							ServiceOption deliveryInfo = transaction.getSelectedDeliveryInfo();
							
							transaction.setDestination(deliveryInfo.getDestinationCountry());
							transaction.setDeliveryOption(deliveryInfo.getServiceOption());
							transaction.setDestinationChanged(false);
							if (receiverName != null && receiverName.length() > 0) {
								transaction.setServiceOptionChanged(false);
							} else {
								transaction.setServiceOptionChanged(true);
							}
						}
					}
					
					if (transaction.isSendToAccount() && transaction.getOtherString().equals(receiverName)) {
						ServiceOption deliveryInfo = transaction.getSelectedDeliveryInfo();
						qdo = new ServiceOption();
						qdo.setServiceOption(deliveryInfo.getServiceOption());
						qdo.setPayoutCurrency(deliveryInfo.getPayoutCurrency());
						qdo.setReceiveAgentAbbreviation(deliveryInfo.getReceiveAgentAbbreviation());
						qdo.setReceiveAgentID(deliveryInfo.getReceiveAgentID());
					} else {
						int selCustomer = selectedRow - 1;
						qdo = new ServiceOption();
						qdo.setServiceOption(customers.get(selCustomer).getReceiver().getDeliveryOption());
						qdo.setReceiveCurrency(customers.get(selCustomer).getReceiver().getReceiveCurrency());
						qdo.setPayoutCurrency(customers.get(selCustomer).getReceiver().getPayoutCurrency());
						qdo.setReceiveAgentAbbreviation(customers.get(selCustomer).getReceiver().getReceiveAgentAbbreviation());
						qdo.setReceiveAgentID(customers.get(selCustomer).getReceiver().getReceiveAgentID());
					}
					
					transaction.setQualifiedDeliveryOption(qdo);
					transaction.setHistoryQDO(qdo);
				}
				transaction.setDataFromHistory(true);
				transaction.setDataCollecionReview(false);

				ConsumerProfile profile = new ConsumerProfile(customer);
				transaction.setProfileObject(profile);
    			boolean f = transaction.createOrUpdateConsumerProfile(profile, ProfileStatus.EDIT_PROFILE);
    			if (! f) {
					PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
    				return;
    			}
			}
		} else {
			ConsumerProfile profile = new ConsumerProfile("UCPID");
			transaction.setProfileObject(profile);
			transaction.setCustomerInfo(null);
			boolean f = transaction.createOrUpdateConsumerProfile(profile, ProfileStatus.CREATE_PROFILE);
			if (! f) {
				PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
				return;
			}
		}
		
		table.setListenerEnabled(false);
		PageNotification.notifyExitListeners(this, direction);
	}

	@Override
	public JTable getTable() {
		return table.getTable();
	}

	@Override
	public DefaultTableModel getTableModel() {
		return tableModel;
	}
	
	class SharedListSelectionHandler implements ListSelectionListener {
		@Override
		public void valueChanged(ListSelectionEvent e) {
			if (e.getValueIsAdjusting()) {
				return;
			}

			int selectedRow = table.getTable().getSelectedRow();
			
			if(selectedRow>0) {
				String receiverName=(String) table.getTable().getValueAt(selectedRow, TAB_COL_REC_NAME);
				if(!transaction.getOtherString().equals(receiverName))
				{
					ServiceOption deliveryInfo = (ServiceOption) getTableModel().getValueAt(selectedRow, DATA_OBJ_INDX);
					if(transaction.getSelectedDeliveryInfo()!=null && deliveryInfo!=null)
					{
						if(!transaction.getSelectedDeliveryInfo().compareDeliveryInfo(deliveryInfo) && !serviceOptionChanged && !transaction.isServiceOptionChanged())
						{
							
							int option = showDialog();
							if (option == Dialogs.YES_OPTION) {
								serviceOptionChanged=true;
								table.getTable().getSelectionModel().setSelectionInterval(selectedRow, selectedRow);
								JButton nextButton = flowButtons.getButton("next"); 
								nextButton.doClick();
								return;
							}else if (option == Dialogs.NO_OPTION) {
								//serviceOptionChanged=true;
								table.getTable().getSelectionModel().setSelectionInterval(transaction.getSelectedCustomerIndex(), transaction.getSelectedCustomerIndex());
								JButton nextButton = flowButtons.getButton("next"); 
								nextButton.doClick();
								return;
							}
							
							table.getTable().getSelectionModel().setSelectionInterval(transaction.getSelectedCustomerIndex(), transaction.getSelectedCustomerIndex());
							return;
							
						}
					}
				}
			}
			int iCustIdx = selectedRow - 1;

			String label0 = "";
			String label1 = "";
			String value1 = "";
			String label2 = "";
			String value2 = "";
			String label3 = "";
			String value3 = "";
			String label4 = "";
			String value4 = "";
			String str=null;
			
			if ((selectedRow > 0) && (customers.get(iCustIdx).getReceiver().getSendCurrency() != null)) {
				
				CustomerInfo customer = customers.get(iCustIdx);
				
				String sMRRN = customer.getReceiver().getCustomerReceiveNumber();
				
				ReceiverInfo receiver = customer.getReceiver();
				str = ServiceOption.getServiceOptionDisplayDescription(receiver.getDeliveryOption(), receiver.getCountry(), receiver.getReceiveCurrency(), receiver.getReceiveAgentID());
				
				ReceiverInfo ri = customer.getReceiver();
				String receiverName = DWValues.formatName(ri.getFirstName(), ri.getMiddleName(), ri.getLastName(), ri.getSecondLastName());
				
				label1 = Messages.getString("MGSWizardPage3.detailName");
				if ((sMRRN != null) && (! sMRRN.isEmpty())) {
					label0 = Messages.getString("MGSWizardPage3.detailPayTo");
					value1 = receiverName;
					label2 = ReceiptReport.getTextAccountNumberLabelFromDeliveryOption(customer.getReceiver().getDeliveryOption());
					value2 = customer.getReceiver().getReceiverAccountDisplayableID();
					
				} else {
					String sName = receiverName; 
					if ( sName != null && sName.length() > 0) {
						label0 = Messages.getString("MGSWizardPage3.detailPayTo");
						value1 = receiverName;
					}
				}
				
				label4 = Messages.getString("MGSWizardPage3.2005");
				value4 = customer.getReceiver().getSendCurrency();
				
				label3 = Messages.getString("MGSWizardPage3.2004");
				value3 = customer.getReceiver().getPayoutCurrency();
				tp1.setListenerEnabled(true);
				descPanel.setVisible(true);
			} else {
				descPanel.setVisible(false);
			}
			
			if (value2!=null && value2.length()>0) {
				accIdPanel.setVisible(true);
			} else {
				accIdPanel.setVisible(false);
			}
			
			if (extraInfoLabel0.getText().equals("")) {
				extraInfoLabel0.setVisible(false);
			} else {
				extraInfoLabel0.setVisible(true);
				if (! label0.equals(extraInfoLabel0.getText())) {
					extraInfoLabel0.setText(label0);
				}
			}

			if (! label1.equals(extraInfoLabel1.getText())) {
				extraInfoLabel1.setText(label1);
			}

			if (value1!=null && ! value1.equals(extraInfoData1.getText())) {
				extraInfoData1.setText(value1);
			}

			if (! label2.equals(extraInfoLabel2.getText())) {
				extraInfoLabel2.setText(label2);
			}
			
			if (value2!=null && ! value2.equals(extraInfoData2.getText())) {
				extraInfoData2.setText(value2);
			}

			if (! label3.equals(extraInfoLabel3.getText())) {
				extraInfoLabel3.setText(label3);
			}
			
			if (value3!=null && ! value3.equals(extraInfoData3.getText())) {
				extraInfoData3.setText(value3);
			} else {
				extraInfoData3.setText("");
			}
			
			if (extraInfoLabel4!=null) {
				if (! label4.equals(extraInfoLabel4.getText())) {
					extraInfoLabel4.setText(label4);
				}
			}

			if (extraInfoData4!=null) {
				if (value4!=null && ! value4.equals(extraInfoData4.getText())) {
					extraInfoData4.setText(value4);
				}
			}

			if (str!=null && str.length()>0) {
				messageTextArea.setVisible(true);
				messageTextArea.setText(str);
			} else {
				messageTextArea.setText("");
				messageTextArea.setVisible(false);
			}
			
			dwTableResized();
		}
	}
	
	private int showDialog() {
		int option = -1;
		option = Dialogs
				.showYesNoCancel("dialogs/DialogServiceOptionChanged.xml"); 	
		return option;

	}
}
