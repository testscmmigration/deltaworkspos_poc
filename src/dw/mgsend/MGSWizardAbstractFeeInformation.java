package dw.mgsend;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import com.moneygram.agentconnect.FeeInfo;
import com.moneygram.agentconnect.TextTranslationType;

import dw.accountdepositpartners.AccountDepositPartnersTransaction;
import dw.dialogs.Dialogs;
import dw.dialogs.FeeDetailsDialog;
import dw.dialogs.RecvDetailsDialog;
import dw.dwgui.DWTable;
import dw.dwgui.DWTable.DWTableCellRendererInterface;
import dw.dwgui.DWTable.DWTableListener;
import dw.dwgui.DWTextPane;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.i18n.Messages;
import dw.profile.UnitProfile;
import dw.utility.AgentCountryInfo;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.ServiceOption;

public abstract class MGSWizardAbstractFeeInformation extends MoneyGramSendFlowPage implements MGSHistoryPage, DWTableListener {
	private static final long serialVersionUID = 1L;

	private int MIN_SCREEN_WIDTH = 500;
	
	private DWTable allTable;
	protected JTable table1 = null;
    protected DefaultTableModel allTableModel = null;
    protected boolean blockEvents = false;
    protected DWTextPane messageTextPane;
    private Map<Integer, List<String[]>> dataMap = null;
	protected Map<Integer, JTable> tableMap = null;
	private Map<Integer, String> categoryNameMap = null;
	private Map<DefaultTableModel, JTable> tableModelMap = null;
	private Map<String[], Integer> rowIndexMap = null;
	protected Map<DefaultTableModel, List<Integer>> dataModelMap = null;
	private JPanel tablePanel = null;
	private JButton backButton = null;
	protected JButton cancelButton;
	protected int lastSelectedIndex = 0;
	protected List<FeeInfo> feeInfos = null;
	private DWTextPane deliveryOptionInfoArea;
	private List<String> currencyList;
	protected JPanel feeDetailsPanel;
	private DWTextPane feeDetailsText;
	protected JButton feeDetailsButton;
	protected JPanel recvDetailsPanel;
	private DWTextPane recvDetailsText;
	protected JButton recvDetailsButton;
	private JScrollPane matchingCustomerScrollpane;
	private JPanel contentPanel;
	private JScrollPane scrollPane2;
	protected ServiceOption qdo;
	protected JLabel destinationCountryLabel;
	protected JLabel sendCurrencyLabel;
	protected JLabel subagentCreditLimitValueLabel;
	protected JLabel subagentCreditLimitTextLabel;
	protected JComponent subagentCreditLimitBorder;
	protected JPanel messagePanel;

    private class DeliveryOptionsCellRenderer extends DefaultTableCellRenderer implements DWTableCellRendererInterface {
		private static final long serialVersionUID = 1L;

		private int margin = 0;
    	private int alignment;

    	public DeliveryOptionsCellRenderer(int margin, int alignment) {
    		this.margin = margin;
    		this.alignment = alignment;
    	}
    	
        @Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        	JComponent rend = (JComponent) super.getTableCellRendererComponent(table, value,
                                          isSelected, hasFocus, row, column);
            rend.setBorder(BorderFactory.createEmptyBorder(0, margin, 0, margin));
            if (rend instanceof JLabel) {
            	((JLabel) rend).setHorizontalAlignment(alignment);
            }
            return rend;
        }

		@Override
		public int getCellMargin() {
			return margin;
		}

		@Override
		public void setCellMargin(int margin) {
			this.margin = margin;
		}
    }
	
	private class TableKeyListener extends KeyAdapter
	{

		TableKeyListener(JTable previous, JTable current, JTable next)
		{
			this.next=next;
			this.current=current;
			this.previous=previous;
		}
		private JTable next;
		private JTable current;
		private JTable previous;
		
		@Override
		public void keyPressed(KeyEvent e)
				throws IllegalArgumentException {
			updateTabNavigation(backButton, e,previous,current,next);
		}
		
		private void updateTabNavigation(final JButton backButton, KeyEvent e, final JTable previousTable,	final JTable currTable,final JTable nextTable) {
			
			int keyCode = e.getKeyCode();
			// ExtraDebug.println("table1 Key["+keyCode+"]");
			if (keyCode == KeyEvent.VK_ENTER) {
				e.consume();
				tryNext();
			}else if (keyCode == KeyEvent.VK_TAB) {
				e.consume();
				backButton.requestFocus();
			} else if (keyCode == KeyEvent.VK_ESCAPE) {
				e.consume();
				cancelButton.doClick();
			}
			// key stroke down from the upper table

			if (nextTable != null) {
				if (keyCode == KeyEvent.VK_DOWN) {
					if ((currTable.getSelectedRow() == currTable.getRowCount() - 1)) {
						// generate a ListSelectionEvent
						currTable.getSelectionModel().clearSelection();
						try {
							Runnable runnable = new Runnable() {
								@Override
								public void run() {
									try {
										currTable.getSelectionModel().clearSelection();
										// also a ListSelectionEvent
										nextTable.requestFocus();
										nextTable.changeSelection(0, 0, false, false);
										/*
										 * table2.setRowSelection to -1 forces
										 * change and error condn
										 */
									} catch (IllegalArgumentException iaex) {
										// row index out of range error
									}
								}
							};
							SwingUtilities.invokeLater(runnable);
						} catch (Exception ex) {
							// to allow pending AWT-events to complete
						}

					}
				}
			}
			if (previousTable != null) {
				// key stroke up from the bottom table
				if (keyCode == KeyEvent.VK_UP) {
					if ((currTable.getSelectedRow() == 0)) {
						// generate a ListSelectionEvent
						currTable.getSelectionModel().clearSelection();
						try {
							Runnable runnable = new Runnable() {
								@Override
								public void run() {
									try {
										currTable.getSelectionModel().clearSelection();
										// also a ListSelectionEvent
										previousTable.requestFocus();
										previousTable.changeSelection(
												previousTable.getRowCount() - 1,
												previousTable.getColumnCount() - 1, false, false);
										previousTable.setRowSelectionInterval(
												previousTable.getRowCount(),
												previousTable.getRowCount());
									} catch (IllegalArgumentException iaex) {
										// row index out of range error
									}
								}
							};
							SwingUtilities.invokeLater(runnable);
						} catch (Exception ex) {
							// to allow pending AWT-events to complete
						}

					}
				}
			}
		}
	}
	
    public MGSWizardAbstractFeeInformation(MoneyGramSendTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
    	super(tran, "moneygramsend/MGFQWizardPage2.xml", name, pageCode, buttons);
    }

    public MGSWizardAbstractFeeInformation(AccountDepositPartnersTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
    	super(tran, "moneygramsend/MGFQWizardPage2.xml", name, pageCode, buttons);
    }
    
	protected void setupComponents() {
		allTable = new DWTable();
		tablePanel = (JPanel) getComponent("tablePanel");
		
		contentPanel= (JPanel) getComponent("contentPanel");
		tablePanel.removeAll();
		backButton = flowButtons.getButton("back"); 
		messageTextPane = (DWTextPane) getComponent("messageTextPane"); 
		if (messageTextPane != null) {
			messageTextPane.setVisible(false);	
		}
		dataMap = new TreeMap<Integer, List<String[]>>();
		tableMap= new TreeMap<Integer, JTable>();
		categoryNameMap = new TreeMap<Integer, String>();
		tableModelMap = new HashMap<DefaultTableModel, JTable>();
		rowIndexMap = new HashMap<String[], Integer>();
		dataModelMap = new HashMap<DefaultTableModel, List<Integer>>();
		scrollPane2 = (JScrollPane) this.getComponent("scrollPane2");
		matchingCustomerScrollpane = (JScrollPane) this.getComponent("matchingCustomerScrollpane");
		destinationCountryLabel = (JLabel) getComponent("destinationCountryLabel"); 
		sendCurrencyLabel = (JLabel) getComponent("sendCurrencyLabel"); 
		subagentCreditLimitBorder = (JComponent) getComponent("subagentCreditLimitBorder");
		subagentCreditLimitValueLabel = (JLabel) getComponent("subagentCreditLimitValueLabel");
		subagentCreditLimitTextLabel = (JLabel) getComponent("subagentCreditLimitTextLabel");
		currencyList = transaction.sendCurrencyList();
		feeDetailsPanel = (JPanel) getComponent("feeDetailsPanel");
		feeDetailsButton = (JButton) getComponent("feeDetailsButton");
		feeDetailsText = (DWTextPane) getComponent("feeDetailsText");
		recvDetailsPanel = (JPanel) getComponent("recvDetailsPanel");
		recvDetailsButton = (JButton) getComponent("recvDetailsButton");
		recvDetailsText = (DWTextPane) getComponent("recvDetailsText");
        deliveryOptionInfoArea = (DWTextPane) getComponent("DeliveryOptionInfoArea"); 
        messagePanel = (JPanel) getComponent("messagePanel");
        this.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				dwTableResized();
				dwTextPaneResized();
				contentPanel.validate();
			}
        });
	}

	@Override
	public void dwTableResized() {

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				dwTextPaneResized();
				
				allTable.resizeTablePanel(scrollPane2, tablePanel);
			} 
		});
	}
 
	public void dwTextPaneResized() {
		int width1 = scrollPane2.getVisibleRect().width - 20;
		int width2 = width1 - 100;
		deliveryOptionInfoArea.setWidth(width1);
		messageTextPane.setWidth(width1);
		feeDetailsText.setWidth(width2);
		recvDetailsText.setWidth(width2);
	}

	private DefaultTableModel createTableModel() {
		String[] headers = {Messages.getString("MGFQWizardPage2.986"),
				Messages.getString("MGFQWizardPage2.999"),				
				Messages.getString("MGFQWizardPage2.987"),
				Messages.getString("MGFQWizardPage2.988"),
				Messages.getString("MGFQWizardPage2.989"),
				Messages.getString("MGFQWizardPage2.990")};    
    	DefaultTableModel tableModel = new DefaultTableModel(headers, 0) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        return tableModel;
	}
	
	private void generateOptionTable(JTable optiontable, DefaultTableModel tableModel) {
		optiontable.setModel(tableModel);
        optiontable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        optiontable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

	protected void resetTable() {
		setupTable();
	}
	
	protected void setupTable() {
		// cash table is the default - with or without splitting into two tables
		allTableModel = createTableModel();
		allTable.addListener(this, this);
		
		generateOptionTable(allTable.getTable(), allTableModel);		
		TableColumnModel columnModel = allTable.getTable().getColumnModel();
		for (int column = 0; column < allTableModel.getColumnCount(); column++) {
			columnModel.getColumn(0).setCellRenderer(allTable.new DWTableCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));
			columnModel.getColumn(1).setCellRenderer(new DeliveryOptionsCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN, SwingConstants.CENTER));
			columnModel.getColumn(2).setCellRenderer(new DeliveryOptionsCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN, SwingConstants.CENTER));
			columnModel.getColumn(3).setCellRenderer(new DeliveryOptionsCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN, SwingConstants.CENTER));
			columnModel.getColumn(4).setCellRenderer(new DeliveryOptionsCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN, SwingConstants.CENTER));
			columnModel.getColumn(5).setCellRenderer(new DeliveryOptionsCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN, SwingConstants.CENTER));
		}
		if (tableMap != null && tableMap.size() > 0) {
			scrollPane2.setColumnHeaderView(allTable.getTable().getTableHeader());
		
			int gridy=0;

			List<Integer> sorted = new ArrayList<Integer>(tableMap.keySet());
			Collections.sort(sorted);

			tablePanel.removeAll();
			int tabIndx = 0;
			
			for (Map.Entry<Integer, JTable> entry : tableMap.entrySet()) {
				
				Integer indx = entry.getKey();
				JTable table = entry.getValue();
				tabIndx=indx.intValue();
				String categoryName = categoryNameMap.get(indx);
				if (categoryName == null || categoryName.length() == 0) {
					categoryName = " ";
				}
				JPanel panel = new JPanel(new BorderLayout());
				JLabel label= new JLabel(categoryName);
				panel.add(label, BorderLayout.CENTER);
				label.setHorizontalAlignment(SwingConstants.CENTER);
				GridBagConstraints gbc = new GridBagConstraints();
				gbc.gridx = 0;
				gbc.gridy = gridy;
				gbc.anchor = GridBagConstraints.CENTER;
				tablePanel.add(panel, gbc);
				gridy++;

				gbc = new GridBagConstraints();
				gbc.gridx = 0;
				gbc.gridy = gridy;
				gbc.anchor = GridBagConstraints.WEST;
				gbc.weightx = 1;
				gbc.fill = GridBagConstraints.HORIZONTAL;
				
				tablePanel.add(table, gbc);
				
				gridy++;
				
				DefaultTableModel tableModel = createTableModel();				
					
				table.setModel(tableModel);
				table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				 
				table.getTableHeader().setReorderingAllowed(false);
				table.getTableHeader().setResizingAllowed(true);
				table.getTableHeader().setResizingAllowed(false);
				table.setColumnModel(columnModel);

				ListSelectionModel selectionModel = table.getSelectionModel();
				selectionModel.addListSelectionListener(getSharedListSelectionHandlerInstance(table));
				table.setSelectionModel(selectionModel);				
				tableModelMap.put(tableModel, table);
				table.setVisible(true);
				Integer prevIdx = Integer.valueOf((tabIndx-1));
				Integer nextIdx = Integer.valueOf((tabIndx+1));
				JTable prevTab = tableMap.get(prevIdx);
				JTable nextTab = tableMap.get(nextIdx);
				
				TableKeyListener tableKeyListener = new TableKeyListener(prevTab,table,nextTab);
				table.addKeyListener(tableKeyListener);
			}
			
			tablePanel.invalidate();
			tablePanel.repaint();
		}
	}
	
	protected void clearTables(JTable selectedTable) {
		
		for (Map.Entry<Integer, JTable> entry : tableMap.entrySet()) {
			JTable table = entry.getValue();
			if(!table.equals(selectedTable))
			{
				table.getSelectionModel().clearSelection();
			}
		}
	}

    public void tryNext() {
    	flowButtons.getButton("next").doClick(); 
    }
    
    
    
	protected String getErrorMessageText(FeeInfo feeInfo, String lang) {
		// error message text
		String errorMessagesText = null;
		if (feeInfo.getErrorInfo() != null) {
			List<TextTranslationType> errorMessages = feeInfo.getErrorInfo().getErrorMessages().getErrorMessage();
			if (errorMessages != null) {
				for (TextTranslationType textTranslation : errorMessages) {
					String errorMessage = DWValues.handleSpecialChars(textTranslation.getTextTranslation());
					if (lang.compareToIgnoreCase(textTranslation.getLongLanguageCode()) == 0) {
						errorMessagesText = errorMessage;
						break;
					} else {
						StringBuffer sb = new StringBuffer();
						sb.append(errorMessage);
						// Add Error Code when response message has wrong
						// language
						sb.append(" (").append("AC " + Messages.getString("Error.1")).append(": ")
								.append(feeInfo.getErrorInfo().getErrorCode()).append(")");
						errorMessagesText = sb.toString();
					}
				}
			}
		}
		return errorMessagesText;
	}
    
    
    
    

	protected void setupDeliveryOptionData() {
		int selectedRow = getFeeIdx();
		feeInfos = transaction.getFeeInfo();
		transaction.setFeeReturnInfo(feeInfos.get(selectedRow));

		String deliveryOptionSpecificText = "";

		/*
		 * Add Error Info Text (if present)
		 */
		if (feeInfos.get(selectedRow).getErrorInfo()!=null && !feeInfos.get(selectedRow).getErrorInfo().isRecoverableFlag()) {
			String lang = DWValues.sConvertLang2ToLang3(UnitProfile.getInstance().get("LANGUAGE","en"));
			deliveryOptionInfoArea.setForeground(Color.RED);
			deliveryOptionSpecificText = getErrorMessageText(feeInfos.get(selectedRow), lang);
			deliveryOptionSpecificText = deliveryOptionSpecificText
			+ DWValues.CR_LF;
		} else {
			if (feeInfos.get(selectedRow).getErrorInfo()!=null) {
				String lang = DWValues.sConvertLang2ToLang3(UnitProfile.getInstance().get("LANGUAGE","en"));
				deliveryOptionSpecificText = getErrorMessageText(feeInfos.get(selectedRow), lang);
				deliveryOptionSpecificText = deliveryOptionSpecificText
				+ DWValues.CR_LF + DWValues.CR_LF;
			}
			deliveryOptionInfoArea.setForeground(Color.BLACK);
			deliveryOptionSpecificText = deliveryOptionSpecificText
			+ Messages.getString("MGFQWizardPage2.993")+ DWValues.CR_LF;

			boolean isValid = feeInfos.get(selectedRow).getReceiveAmounts() != null ? feeInfos.get(selectedRow).getReceiveAmounts().isValidCurrencyIndicator() : false;
			if (isValid) {
				if (!AgentCountryInfo.isUsAgent()) {
					deliveryOptionSpecificText = deliveryOptionSpecificText
					+ DWValues.CR_LF
					+ Messages.getString("MGFQWizardPage2.994")	+ DWValues.CR_LF
					+ Messages.getString("MGFQWizardPage2.995") + DWValues.CR_LF;
				}
			} else {
				deliveryOptionSpecificText = deliveryOptionSpecificText
				+ DWValues.CR_LF
				+ Messages.getString("MGFQWizardPage2.996")	+ DWValues.CR_LF
				+ Messages.getString("MGFQWizardPage2.997") + DWValues.CR_LF;
			}

	        if (currencyList.size() > 1) {
				deliveryOptionSpecificText = deliveryOptionSpecificText
				+ DWValues.CR_LF
				+ Messages.getString("MGFQWizardPage2.1000b")
				+ DWValues.CR_LF;
	        }
		}
		deliveryOptionInfoArea.setText(deliveryOptionSpecificText);
		
		// Display the delivery option description is specified.
		
		String str = feeInfos.get(selectedRow).getServiceOptionDisplayDescription();
		if (str != null && str.trim().length() > 0 && messageTextPane != null) {
			messagePanel.setVisible(true);
			messageTextPane.setVisible(true);
			messageTextPane.setText(str);
		} else {
			messagePanel.setVisible(false);
			messageTextPane.setVisible(false);
			messageTextPane.setText("");
		}
	}

	protected void setFeeDetailsEnableStatus() {
		int iIdx = getFeeIdx();
		boolean bFeeModified = transaction.feeModified(feeInfos.get(iIdx));
		boolean bPromoCodesReturned = (transaction.getReturnedPromoCodeCount() > 0);

		feeDetailsText.setEnabled(bFeeModified);
		feeDetailsButton.setEnabled(bFeeModified
				|| bPromoCodesReturned
				|| (transaction.getEnteredPromoCodeCount() > 0));
		if (feeDetailsButton.isEnabled()) {
			if (transaction.promoCodeErrors(feeInfos.get(iIdx).getPromotionInfos().getPromotionInfo())) {
				feeDetailsButton.setForeground(Color.RED);
			} else {
				feeDetailsButton.setForeground(Color.BLACK);
			}
		}
	}

	protected void setRecvInfoEnableStatus() {
		int iIdx = getFeeIdx();
		boolean bRecvAmtModified = transaction.isRecvAmtModified(feeInfos.get(iIdx));

		recvDetailsText.setEnabled(bRecvAmtModified);
		recvDetailsButton.setEnabled(bRecvAmtModified );
		if (recvDetailsButton.isEnabled()) {
			recvDetailsButton.setForeground(Color.BLACK);
		}
	}

	private int getCurrentIdx() {
		int FeeTblIdx = 0;
		int tblRow;

		for (Map.Entry<Integer, JTable> entry : tableMap.entrySet()) {
			JTable table = entry.getValue();
			tblRow = table.getSelectedRow();
			if (tblRow >= 0) {

				List<Integer> dataList = dataModelMap.get(table.getModel());
				FeeTblIdx = (dataList.get(tblRow)).intValue();
				break;
			}
		}

		return FeeTblIdx;
	}
	
	protected int getFeeIdx() {
		int feeTblIdx = getCurrentIdx();
		return feeTblIdx;
	}

	protected void populateFeeTable(boolean isFee, int direction) {

		if (dataMap.keySet() != null) {
			for (Map.Entry<Integer, List<String[]>> entry : dataMap.entrySet()) {
				Integer tabIdx = entry.getKey();
				List<String[]> dataSet = entry.getValue();
				List<Integer> dataIndexList = null;
				JTable table = tableMap.get(tabIdx);
				DefaultTableModel tableModel = null;
				if (table != null) {
					tableModel = (DefaultTableModel) table.getModel();
				}
				for (int i = 0; i < dataSet.size(); i++) {
					if (i == 0) {
						dataIndexList = new ArrayList<Integer>();
					}

					String[] row = dataSet.get(i);
					List<String> rowList = new ArrayList<String>();
					for (int j = 0; j < row.length; j++) {
						if (j > 1) {
							rowList.add(row[j]);
						}
					}

					Integer rowIdx = rowIndexMap.get(row);
					dataIndexList.add(rowIdx);
					allTableModel.addRow(rowList.toArray());

					if (tableModel != null) {
						tableModel.addRow(rowList.toArray());
					}
				}
				if (tableModel != null) {
					if (!dataModelMap.containsKey(tableModel)) {
						dataModelMap.put(tableModel, dataIndexList);
					}
				}
			}
		}

		allTable.initializeTable(matchingCustomerScrollpane, contentPanel, scrollPane2, tablePanel, MIN_SCREEN_WIDTH, 10);
		allTable.setColumnStaticWidth(0);

		// now set up the indices to match the feeInfo[] array
		if (direction == PageExitListener.BACK) {
			// populateFeeTable using lastSelectedRow
			selectTableItemBasedOnFeeInfoIdx(transaction.getSelectedFeeTableIndex());
		} else if (!isFee) {
			// default to first (assuming no history)
			if (table1.getModel().getRowCount() > 0) {
				table1.setRowSelectionInterval(0, 0);
			}
		} else {
			// promo code for fee changed after displaying discounted fees
			selectTableItemBasedOnFeeInfoIdx(transaction.getSelectedFeeTableIndex());
		}
	}

	/**
	 * This method is invoked when the Fee Details Button is clicked.
	 */
	public void feeDetails() {
		
		int lastSelIndex = getFeeIdx();
		FeeDetailsDialog dialog = new FeeDetailsDialog(
				feeInfos.get(lastSelIndex),
				transaction.getEnteredPromoCodeArray());
		try {
			Dialogs.showPanel(dialog);

			if (dialog.isCanceled()) {
				finish(PageExitListener.CANCELED);
				return;
			} else if (!dialog.isAccepted()) {
				flowButtons.setButtonsEnabled(true);
				flowButtons.setSelected("next"); 
				return;
			}
		} finally {
			reqFocus();
		}
	}

	/**
	 * This method is invoked when the Recv Info Button is clicked.
	 */
	public void recvDetails() {
		int iIdx = getFeeIdx();
		RecvDetailsDialog dialog = new RecvDetailsDialog(feeInfos.get(iIdx));

		try {
			Dialogs.showPanel(dialog);
			if (dialog.isCanceled()) {
				finish(PageExitListener.CANCELED);
				return;
			} else if (!dialog.isAccepted()) {
				flowButtons.setButtonsEnabled(true);
				flowButtons.setSelected("next"); 
				return;
			}
		} finally {
			reqFocus();
		}
	}

	private void reqFocus() {
		for (Map.Entry<Integer, JTable> entry : tableMap.entrySet()) {
			JTable table = entry.getValue();
			if (table.getSelectionModel() != null) {
				if (!table.getSelectionModel().isSelectionEmpty()) {
					table.requestFocus();
				}
			}
		}
	}
	
	@Override
	public JTable getTable() {
        return table1;
    }

    @Override
	public DefaultTableModel getTableModel() {
        return (DefaultTableModel) table1.getModel();
    }

    
    protected void clearComponentTables() {
    	clearComponent(allTable.getTable());
    	
		for (Map.Entry<Integer, JTable> entry : tableMap.entrySet()) {
			JTable table = entry.getValue();
			clearComponent(table);
			DefaultTableModel dfModel= (DefaultTableModel) table.getModel();
			if (dfModel != null) {
		        int itemCount;
		        synchronized (dfModel) {
		        	itemCount = dfModel.getRowCount();
		        	for (int i = 0; i < itemCount; i++) {
		        		dfModel.removeRow(0);
		        	}
		        }
			}
		}
    }
	
	protected void setupLists(List<String[]> rows) {
		/*
		 * sets the instance variables "depositOption"
		 */

		int tableIndex=0;
		Map<String, List<String[]>> categoryMap = new HashMap<String, List<String[]>>();
		for (int i = 0; i < rows.size(); i++) {
			String[] row = rows.get(i);
			List<String> rowList = new ArrayList<String>();
			for (int j = 0; j < row.length; j++) {
				if (j > 1) {
					rowList.add(row[j]);
				}
			}
			
			String category =row[0];
			String categoryName =row[1];
			List<String[]> data = categoryMap.get(category);
			if(data== null)
			{
				data = new ArrayList<String[]>();
				categoryMap.put(category, data);
				JTable table = new JTable();
				allTable.addSupplementalTable(table);
				table.setName("table"+(tableIndex+1));
				Integer tabIdx=  Integer.valueOf(tableIndex);
				tableMap.put(tabIdx, table);
				categoryNameMap.put(tabIdx, categoryName);
				dataMap.put(tabIdx, data);
				if(tableIndex==0)
				{
					table1=table;
				}
				tableIndex++;
			}
			data.add(row);
			rowIndexMap.put(row, Integer.valueOf(i));
		}
		
		this.repaint();
	}

	protected void populateFeeData() {

		try {
			List<String[]> rows = transaction.getTableRows(feeInfos);
			setupLists(rows);
			setupTable();
			clearComponentTables();

		} catch (Exception e) {
			Debug.printStackTrace(e);
		}
	}

	abstract protected void populateFeeOptionsTableWithSelectedRow(FeeInfo fees);
	
	abstract protected ListSelectionListener getSharedListSelectionHandlerInstance(JTable table);

    /**
     *   Select the correct item from the correct table base on a FeeInfo
     *   index.
     */
    protected void selectTableItemBasedOnFeeInfoIdx(int index) {
		Integer intObj = Integer.valueOf(index);
		JTable selectedTable = null;
		
		for (Map.Entry<DefaultTableModel, JTable> entry : tableModelMap.entrySet()) {
			DefaultTableModel tableModel = entry.getKey();
			JTable table = entry.getValue();
			List<Integer> dataList = dataModelMap.get(tableModel);
			if (dataList != null) {
				int tableIdx = dataList.indexOf(intObj);

				if (tableIdx != -1) {
					table.setRowSelectionInterval(tableIdx, tableIdx);
					table.requestFocus();
					transaction.setFeeReturnInfo(feeInfos.get(index));
					selectedTable = table;
					break;
				}
			}
		}
		if (selectedTable != null) {
			clearTables(selectedTable);
		} else {
			// delivery option match not found - use default
			if (table1.getModel().getRowCount() > 0) {
				table1.setRowSelectionInterval(0, 0);
				transaction.setFeeReturnInfo(feeInfos.get(0));
			} else {
				populateFeeOptionsTableWithSelectedRow(feeInfos.get(0));
			}
		}
	}
}
