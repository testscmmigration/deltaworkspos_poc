package dw.mgsend;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import dw.accountdepositpartners.AccountDepositPartnersTransaction;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.DataCollectionField;
import dw.framework.DataCollectionScreenToolkit;
import dw.framework.DataCollectionSet;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;
import dw.utility.ExtraDebug;

/**
 * "thirdParty"
 * Page 8 of the MoneyGram Send Wizard. This page prompts the user for the
 * recipient information if this is not a selected existing recipient. The test
 * question and answer are disabled if not applicable based on profile.
 */
public class MGSWizardPage8 extends MoneyGramSendFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	private JScrollPane scrollPane1;
	private JScrollPane scrollPane2;
	private JPanel panel2;
	private DWTextPane text1;
	private DataCollectionScreenToolkit toolkit = null;
	private Object dataCollectionSetChangeFlag = new Object();
	private JButton nextButton;

	public MGSWizardPage8(MoneyGramSendTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super(tran, "moneygramsend/MGSWizardPage8.xml", name, pageCode, buttons); 
		
		scrollPane1 = (JScrollPane) getComponent("scrollPane1");
		scrollPane2 = (JScrollPane) getComponent("scrollPane2");
		panel2 = (JPanel) getComponent("panel2");
		text1 = (DWTextPane) getComponent("text1");
		text1.addListener(this, this);
		nextButton = this.flowButtons.getButton("next");
	}
	
	public MGSWizardPage8(AccountDepositPartnersTransaction tran, String name, 	String pageCode, PageFlowButtons buttons) {
		this((MoneyGramSendTransaction) tran, name, pageCode, buttons); 
	}
	
	@Override
	public void start(int direction) {
		
		// Make sure the screen has been built.
		
		setupComponents();
		
		// Setup the flow buttons at the bottom of the screen.
		
		flowButtons.reset();
		flowButtons.setVisible("expert", false); 
		flowButtons.setEnabled("expert", false); 
		
		// Populate the screen with any previously entered data.
		
		if (direction == PageExitListener.NEXT || direction == PageExitListener.BACK) {
			DataCollectionScreenToolkit.restoreDataCollectionFields(toolkit.getScreenFieldList(), transaction.getDataCollectionData());
		}
		
		// Move the cursor to 1) the first required item with no value or item 
		// with an invalid value or 2) the first optional item with no value.
		
		toolkit.moveCursor(toolkit.getScreenFieldList(), nextButton);
		
		// Enable and call the resize listener.
		
		toolkit.enableResize();
    	text1.setListenerEnabled(true);
		toolkit.getComponentResizeListener().componentResized(null);
		dwTextPaneResized();
	}
	
	private void setupComponents() {
		
		// Check if the GFFP information has changed.
		
		DataCollectionSet data = this.transaction.getDataCollectionData();

		if (this.dataCollectionSetChangeFlag != this.transaction.getDataCollectionData().getChangeFlag()) {
	    	this.dataCollectionSetChangeFlag = this.transaction.getDataCollectionData().getChangeFlag();
	    	
	    	// Rebuild the data collection items for the screen.
	    	
	    	ExtraDebug.println("Rebuilding Screen " + getPageCode() + " from FTC data");
	    	
			int flags = DataCollectionScreenToolkit.SHORT_LABEL_VALUES_FLAG + DataCollectionScreenToolkit.DISPLAY_REQUIRED_FLAG;
			JButton backButton = this.flowButtons.getButton("back");
			this.toolkit = new DataCollectionScreenToolkit(this, this.scrollPane1, this, this.scrollPane2, this.panel2, flags, transaction.getSenderThirdPartyScreenStatus().getDataCollectionPanels(data), this.transaction, backButton, nextButton, transaction.getDestination().getCountryCode());
		}
		
		// Check if the screen needs to be laid out
		
		if (transaction.getSenderThirdPartyScreenStatus().needLayout(data)) {
			toolkit.populateDataCollectionScreen(panel2, transaction.getSenderThirdPartyScreenStatus().getDataCollectionPanels(data), false);
		}
	}

	@Override
	public void finish(int direction) {
    	
    	// Disable the resize listener for this screen.
    	
		toolkit.disableResize();
    	text1.setListenerEnabled(false);
        
    	// If the flow direction is to the next screen, check the validity of the
    	// data. If a value is invalid or a required item does not have a value,
    	// return to the screen.
    	
    	if (direction == PageExitListener.NEXT) {
        	String tag = toolkit.validateData(toolkit.getScreenFieldList(), DataCollectionField.DISPLAY_ERRORS_VALIDATION);
        	if (tag != null) {
				flowButtons.setButtonsEnabled(true);
				toolkit.enableResize();
        		return;
        	}
        }
    	
    	// If the flow direction is to the next screen or back to the previous
    	// screen, save the value of the current items in a data structure.

        if ((direction == PageExitListener.BACK) || (direction == PageExitListener.NEXT)) {
        	DataCollectionScreenToolkit.saveDataCollectionFields(toolkit.getScreenFieldList(), transaction.getDataCollectionData());
        }
    	
    	// Check if receiveValidation could be performed now or if another page should be displayed.
    	
    	if (direction == PageExitListener.NEXT) {
    		
			/*
			 * If all the data is available then do the validation, otherwise 
			 * move onto the next page to collect/edit the transaction data.
			 */
    		
    		String nextPage = transaction.nextDataCollectionScreen(MoneyGramSendWizard.MSW50_THIRD_PARTY);
	    	if (nextPage.isEmpty() && transaction.getDataCollectionData().isAdditionalInformationScreenValid()) {
	        	transaction.sendValidation(this, ValidationType.SECONDARY);
				return;
    		}
    	}
    	PageNotification.notifyExitListeners(this, direction);
	}

	/**
	 * Called by transaction after it receives response from middleware.
	 * 
	 * @param commTag
	 *            is function index.
	 * @param returnValue
	 *            is Boolean set to true if response is good. Otherwise, it is
	 *            set to false.
	 */
	@Override
	public void commComplete(int commTag, Object returnValue) {
		boolean b = ((Boolean) returnValue).booleanValue();
		if (b && commTag == MoneyGramClientTransaction.SEND_VALIDATE) {
			PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
		} else {
  			transaction.setEditActive(true);
  			PageNotification.notifyExitListeners(this, PageExitListener.DONOTHING);
  			flowButtons.setButtonsEnabled(true);
  			flowButtons.setVisible("expert", false);
 		}
	}
	
	@Override
	public void dwTextPaneResized() {
		int width = panel2.getPreferredSize().width;
		text1.setWidth(width);
	}
}