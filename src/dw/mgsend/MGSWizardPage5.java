/*
 * MGSWizardPage5.java
 * 
 * $Revision$
 *
 * Copyright (c) 2001-2012 MoneyGram International
 */
package dw.mgsend;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import dw.accountdepositpartners.AccountDepositPartnersTransaction;
import dw.dwgui.DelimitedTextField;
import dw.dwgui.MoneyField;
import dw.dwgui.MultiListComboBox;
import dw.framework.FieldKey;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.CountryInfo.Country;
import dw.model.adapters.CountryInfo.CountrySubdivision;
import dw.utility.AgentCountryInfo;
import dw.utility.DWValues;
import dw.utility.Guidelines;
import dw.utility.Restrictions;
import dw.utility.StringUtility;

/**
 * "amountEntry"
 * Page 5 of the MoneyGramSend Wizard. This page prompts the user to enter the
 *   amount and destination of the send, and the Mexico option if necessary.
 */
public class MGSWizardPage5 extends MoneyGramSendFlowPage {
	private static final long serialVersionUID = 1L;

	static private int MAXIMUM_MESSAGE_PANE_WIDTH = 800;
	
    protected MoneyField amountField;
    protected MultiListComboBox<Country> destinationComboBox;
    private JTextArea messageTextArea;
    private JLabel screenInstructions;
    private JLabel stateLabel;
    protected MultiListComboBox<CountrySubdivision> stateBox;
    private MultiListComboBox<String> currencyComboBox;
	private JLabel stateInfoTextLabel;
	private JLabel dummySpacerLabel;
	private String selectText;
	private String selectTextCurrency;
	private List<String> currencyList;
	private JLabel promoLabel;
	private DelimitedTextField promoField;

    public MGSWizardPage5(MoneyGramSendTransaction tran, String name,
            String pageCode, PageFlowButtons buttons) {
        super(tran, "moneygramsend/MGSWizardPage5.xml", name, pageCode, buttons); 
        init();
    }
    public MGSWizardPage5(AccountDepositPartnersTransaction tran, String name,
            String pageCode, PageFlowButtons buttons) {
        super(tran, "moneygramsend/MGSWizardPage5.xml", name, pageCode, buttons); 
        init();
    }
	private void init() {
		amountField = (MoneyField) getComponent("amountField"); 
        destinationComboBox = (MultiListComboBox<Country>) getComponent("destinationComboBox");        
        screenInstructions = (JLabel) getComponent("screenInstructions");
        messageTextArea = (JTextArea) getComponent("messageTextArea"); 
        destinationComboBox.addList(CountryInfo.getCountries(CountryInfo.KEY_RECV_ACTIVE, transaction.isInternational()), "countries"); 
        currencyList = transaction.sendCurrencyList();
        currencyComboBox =  (MultiListComboBox<String>) getComponent("currencyComboBox"); 
        promoLabel = (JLabel) getComponent("promoLabel"); 
        promoField = (DelimitedTextField) getComponent("promoField"); 
        
        if (currencyList.size() > 1) {
        	screenInstructions.setText(Messages.getString("MGSWizardPage5.1215a"));
        }
        
        if (transaction.getDestination() != null)
            destinationComboBox.setSelectedItem(transaction.getDestination());
        
        // State information
    	stateLabel = (JLabel) getComponent("stateLabel");	
		stateBox = (MultiListComboBox<CountrySubdivision>) getComponent("stateComboBox");
		stateInfoTextLabel = (JLabel) getComponent("stateInfoTextLabel");
		dummySpacerLabel  = (JLabel) getComponent("dummySpacerLabel");
		
        if (AgentCountryInfo.isStateRequired()){
        	stateInfoTextLabel.setVisible(true);
        	dummySpacerLabel.setVisible(false);
        	stateLabel.setEnabled(true);
        	stateLabel.setVisible(true);
        	//stateBox.setEnabled(true);
        	stateBox.setVisible(true);
	    	populateCodeTablesStateBox(stateBox, CountryInfo.getAgentCountry(), CountryInfo.getAgentCountrySubdivision());
	    	
        } else {
        	stateInfoTextLabel.setVisible(false);
        	dummySpacerLabel.setVisible(true);
        	stateLabel.setEnabled(false);
        	stateLabel.setVisible(false);        	
        	stateBox.setEnabled(false);
        	stateBox.setVisible(false);        	
        }

        selectTextCurrency = Messages.getString("MGSWizardPage5.item1"); 
		currencyComboBox.removeAllItems();
		currencyComboBox.reset();
        currencyComboBox.addList(currencyList.toArray(), "Currencies");
		
        this.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				dwTextPaneResized(false);
			}
        });
	}
    
	public void dwTextPaneResized(boolean newTextFlag) {
		JScrollPane sp = (JScrollPane) this.getComponent("scrollpane");
		JScrollPane messageScrollPane = (JScrollPane) this.getComponent("messageScrollPane");
		messageScrollPane.setPreferredSize(messageTextArea.getPreferredSize());
		messageScrollPane.invalidate();
		this.doLayout();
		
		JPanel panel = (JPanel) this.getComponent("main");
		
		int spWidth = sp.getVisibleRect().width;
		
		int newWidth = Math.min(MAXIMUM_MESSAGE_PANE_WIDTH, spWidth - 38);
		
		messageScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		Dimension dimension = new Dimension(newWidth, messageScrollPane.getPreferredSize().height);
		messageScrollPane.setPreferredSize(dimension);
		
		messageScrollPane.invalidate();
		this.validate();
		this.repaint();

		messageScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		
		int pHeight = panel.getPreferredSize().height;
		int spHeight = sp.getVisibleRect().height;
		
		if (pHeight > spHeight) {
			messageScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			
			int newHeight = messageScrollPane.getPreferredSize().height - pHeight + spHeight;
			int width = messageScrollPane.getPreferredSize().width;
			dimension = new Dimension(width, newHeight);
			messageScrollPane.setPreferredSize(dimension);
			if (newTextFlag) {
				messageScrollPane.getVerticalScrollBar().setValue(0);
				messageTextArea.setCaretPosition(0);
			}
		} else {
			messageScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		}
		
		messageScrollPane.invalidate();
		this.validate();
		this.repaint();
	}

    @Override
	public void start(int direction) {
        destinationComboBox.setSelectedItem(transaction.getDestination());
        amountField.requestFocus();
        amountField.selectAll();            
        flowButtons.reset();
        flowButtons.setVisible("expert", false); 
        flowButtons.setEnabled("expert", false); 

        if (direction == PageExitListener.NEXT) {

        	// Amount
        	
        	if (transaction.isDeductFeeFromSend()) {
//        		amountField.setValue(transaction.getTotalAmount());
        		BigDecimal sendAmount = transaction.getCurrentFeeInfo().getSendAmounts().getTotalAmountToCollect();
        		amountField.setValue(sendAmount);
        	} else {
        		amountField.setValue(transaction.getAmount());
        	}
        	
        	// Destination
        	
        	try {
        		String destination;
        		if ((! transaction.isFeeQuery()) && (! transaction.isSendToAccount())) {
        			destination = transaction.getProfileObject().getCurrentReceiver().getValues().get(FieldKey.RECEIVE_COUNTRY_KEY.getInfoKey());
        		} else {
        			destination = transaction.getDestination().getCountryCode();
        		}
        		if ((destination != null) && (! destination.isEmpty())) {
        			transaction.setDestination(CountryInfo.getCountry(destination));
        		}
        	} catch (Exception e) {
        	}
            
            if (transaction.getDestination() != null) {
            	destinationComboBox.setSelectedItem(transaction.getDestination());
            }
            destinationChanged();
		}
        
        promoField.setText(transaction.getEnteredPromoCodes());
        
        /*
         *  if multiple send currencies, then change the prompt to tell the
         *  agent to request a currency, and set the default to a historic
         *  send currency (if there is a valid one) or the select text.
         */
        if (currencyList.size() > 1) {
        	screenInstructions.setText(Messages.getString("MGSWizardPage5.1215a"));
        	String sendCurrency = StringUtility.getNonNullString(transaction.getSendCurrency());
        	if (sendCurrency.equals("")) {
            	currencyComboBox.getModel().setSelectedItem(selectTextCurrency);
        	}
            else {
				Iterator<String> enumeratedVals = currencyList.iterator();
				boolean found = false;
				/*
				 * search the currency list to make sure the transaction 
				 * send currency is valid for this agent.  If so, default to
				 * it, otherwise default to the select text.
				 */
				while (enumeratedVals.hasNext() && !found) {
					found = ((enumeratedVals.next()).compareToIgnoreCase(sendCurrency)==0);
				}
				if (found) {
                	currencyComboBox.getModel().setSelectedItem(sendCurrency);
                	amountField.setCurrency(sendCurrency);
                	amountField.reformat();
				}
				else {
                	currencyComboBox.getModel().setSelectedItem(selectTextCurrency);
                	transaction.setSendCurrency(transaction.agentBaseCurrency());
				}
            }
        	currencyComboBox.setEnabled(true);
        }
        else {
        	currencyComboBox.setEnabled(false);
        	/*
        	 * If we only have 1 currency, use it, otherwise we can have 
        	 * problems looking up the fraud limits.
        	 */
        	transaction.setSendCurrency(transaction.agentBaseCurrency());
        }
        addActionListener("currencyComboBox", this, "currencyChanged");      
        
        addActionListener("destinationComboBox", this, "destinationChanged");      
        Component[] ex1 = { destinationComboBox, stateBox}; 
        addReturnTabListeners(ex1, flowButtons.getButton("cancel")); 
        KeyListener nexter = new KeyAdapter() {
            @Override
			public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    e.consume();
                    destination();
                }
            }
        };

        addKeyListener("destinationComboBox", nexter); 
        addKeyListener("stateComboBox", nexter); 
        destinationComboBox.setSelectedItem(transaction.getDestination());
        
        stateBox.setEnabled(false);
        if (AgentCountryInfo.isStateRequired()){
        	stateLabel.setEnabled(true);
        	stateLabel.setVisible(true);        	
        	stateBox.setVisible(true);
	        updateCodeTableStateList((Country) destinationComboBox.getSelectedItem(), stateBox);
	        CountrySubdivision spiStateInfo = transaction.getDestinationState();
        	if ( spiStateInfo != null) {
        		/*
        		 * Skip item 0, since it is not the 'select' text not a state
        		 * item.
        		 */
        		for (int ii = 1; ii < stateBox.getItemCount(); ii++ ) {
        			CountrySubdivision spiListItem = (CountrySubdivision) stateBox.getItemAt(ii);
            		if (spiStateInfo.equals(spiListItem)) {
                		stateBox.setSelectedItem(spiListItem);
                		break;
            		}
        		}
        			
        	}
			boolean areStatesActive = (stateBox.getItemCount() > 1);
			stateInfoTextLabel.setVisible(areStatesActive);
			dummySpacerLabel.setVisible(!areStatesActive);
			stateBox.setEnabled(areStatesActive);
		} else {
			stateLabel.setEnabled(false);
        	stateLabel.setVisible(false);        	
        	
        	stateBox.setVisible(false);        	
        	stateInfoTextLabel.setVisible(false);
        	dummySpacerLabel.setVisible(true);
        }
        boolean arePromoCodesSupported = (DWValues.getPromoCodeLimit() > 0); 
        promoLabel.setEnabled(arePromoCodesSupported);
        promoLabel.setVisible(arePromoCodesSupported);
        promoField.setEnabled(arePromoCodesSupported);
        promoField.setVisible(arePromoCodesSupported);
    }

    public void destination() {
    	Country country = (Country) destinationComboBox.getSelectedItem();
		
		if (getComponent("destinationComboBox").hasFocus()) { 
			if (!transaction.getDestination().getCountryCode().trim()
					.equalsIgnoreCase(country.getCountryCode().trim()))
				transaction.setDestinationChanged(true);

			if (stateLabel.isVisible() && stateBox.isVisible() && stateBox.isEnabled())
				stateBox.requestFocus(true);
			else
				flowButtons.getButton("next").doClick(); 
		}else if (getComponent("stateComboBox").hasFocus()) { 
			flowButtons.getButton("next").doClick(); 
		}

	}

    /**
     *  Update MoneyField(s) to use new currency
     */
    public void currencyChanged() {
    	if (currencyComboBox.getSelectedIndex() != -1) {
    		String sCurrency = currencyComboBox.getSelectedItem().toString();
    		amountField.setCurrency(sCurrency);
    		amountField.reformat();
        	transaction.setSendCurrency(sCurrency);    		
    	}
     }

    public void destinationChanged() {
    	
      Country country = (Country) destinationComboBox.getSelectedItem(); 
      if (country == null) return;
      
      messageTextArea.setText(getCountryGuidelinesText(country));
      dwTextPaneResized(true);
      
      if (AgentCountryInfo.isStateRequired()) {
	      updateCodeTableStateList(country, stateBox);
	      boolean areStatesActive = (stateBox.getItemCount() > 1);
	      stateBox.setEnabled(areStatesActive);
	      stateInfoTextLabel.setVisible(areStatesActive);
	      dummySpacerLabel.setVisible(!areStatesActive);
      }
      
      if(!transaction.getDestination().getCountryCode().trim().equalsIgnoreCase(country.getCountryCode().trim()))
    	  transaction.setDestinationChanged(true);
    }
    
    /**
     * set the selectText for the country guidelines selectText 
     */
    private String getCountryGuidelinesText(Country country) {
        String countryCode = country.getCountryCode();
        String sendAndReceiveCapabilty = Messages.getString("SendReceiveCapability.1");
        
        String payoutCurrencyText = CountryInfo.getPayoutCurrenciesForCountry(countryCode);
		if (! payoutCurrencyText.isEmpty()) {
			if (payoutCurrencyText.length() > 3) {
				payoutCurrencyText = Messages.getString("PayoutCurrency.0") + " "
					+ payoutCurrencyText  + " "	
					+ Messages.getString("PayoutCurrency.1");
			} else {
				payoutCurrencyText = Messages.getString("PayoutCurrency.0") + " "
				+ payoutCurrencyText;
			}
		}
        String guidelineText = Guidelines.getInstance().getGuideLines(countryCode);
        String restrictionText = Restrictions.getInstance().getRestrictions(countryCode);
        
        boolean isSendCapability = CountryInfo.isSendCapability(countryCode);
        boolean isReceiveCapability = CountryInfo.isReceiveCapability(countryCode);

        if ((isSendCapability) && (isReceiveCapability)) {
            sendAndReceiveCapabilty = sendAndReceiveCapabilty + " " + Messages.getString("SendReceiveCapability.2");
        
        } else if ((isSendCapability) && (! isReceiveCapability)) {
            sendAndReceiveCapabilty = sendAndReceiveCapabilty + " " + Messages.getString("SendReceiveCapability.3");
        
        } else if ((! isSendCapability) && (isReceiveCapability)) {
            sendAndReceiveCapabilty = sendAndReceiveCapabilty + " " + Messages.getString("SendReceiveCapability.4");
        
        } else if ((! isSendCapability) && (! isReceiveCapability)) {
            sendAndReceiveCapabilty = sendAndReceiveCapabilty + " " + Messages.getString("SendReceiveCapability.5");
            payoutCurrencyText = "";
        }

        if (restrictionText == null) restrictionText = "  ";
        if (guidelineText == null) guidelineText = "   ";
        return sendAndReceiveCapabilty + "\n" + payoutCurrencyText
        		+ restrictionText + "\n" + guidelineText;
    }
    
    @Override
	public void finish(int direction) {
    	Country country = (Country) destinationComboBox.getSelectedItem();
        if (direction == PageExitListener.NEXT ) {
        	if (AgentCountryInfo.isStateRequired()){
        		if ((country.getCountryCode().equals("USA")|| country.getCountryCode().equals("CAN") )&& stateBox.getSelectedIndex()< 1){
                    stateBox.requestFocus();
                    flowButtons.setButtonsEnabled(true);
                    return;
        		}
        		if (stateBox.getSelectedIndex() > 0) {
        			transaction.setDestinationState((CountrySubdivision) stateBox.getSelectedItem());
        		} else {
        			transaction.setDestinationState(null);
        		}
        	}
        	
        	if (currencyComboBox.getModel().getSelectedItem().equals(selectText)){
        		currencyComboBox.requestFocus();
                flowButtons.setButtonsEnabled(true);
                return;
        	}

            if (amountField.getValue().compareTo(BigDecimal.ZERO) == 0) { 
                amountField.requestFocus();
                amountField.selectAll();
                flowButtons.setButtonsEnabled(true);
                return;
            }           

        	String sPromoCodes = promoField.getText();
       		if (! validPromoCodeCount(sPromoCodes)) {
                flowButtons.setButtonsEnabled(true);
                promoField.requestFocus();
                promoField.selectAll();
                return;
       		}

       		// check if phone number has changed when starting with a RRN number
       		// if so, reset the RRN number from the previous value
			
       		String cardPhoneNumber = transaction.getCardPhoneNumber() != null ? transaction.getCardPhoneNumber().trim() : "";
       		String homePhoneNumber = transaction.getSender().getHomePhone() != null ? transaction.getSender().getHomePhone().trim() : "";
       		boolean hasPhoneNumberChanged = false;
			boolean hasPhoneNumber = ((transaction.getSender() != null) && (! cardPhoneNumber.isEmpty()) ? true : false);
			if (hasPhoneNumber && (transaction.getHistoryQDO() != null) && (! cardPhoneNumber.equals(homePhoneNumber))) {
				hasPhoneNumberChanged = true;
			}
       		
            // check if a new amount has been entered.
            // if so, set the amount and reset the old values,
            // such as, fee, receiveamount, total, expectedreceive.
            
            BigDecimal d = null;
        	if (transaction.isDeductFeeFromSend()) {
        		d = transaction.getCurrentFeeInfo().getSendAmounts().getTotalAmountToCollect();
        	} else {
        		d = transaction.getAmount();
        	}
        	
            if (d != null && amountField.getValue().compareTo(d) != 0) {
                if (!(setAmount(amountField.getValue(), 
                		currencyComboBox.getSelectedItem().toString()))) {
                	if (transaction.isManagerOverrideFailed()){
                    	PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
                    	return;
                	} else {
	                    amountField.requestFocus();
	                    amountField.selectAll();
	                    flowButtons.setButtonsEnabled(true);
                        transaction.setAmount(BigDecimal.ZERO,
                       		 currencyComboBox.getSelectedItem().toString()); 
	                    return;
                	}
                } else {
                	if (transaction.isMaxEmployeeLimitExceeded()){
                        transaction.setAmount(amountField.getValue(), currencyComboBox.getSelectedItem().toString()); 
                	} 
                }
                	
                transaction.setDestination(country);
                transaction.setEnteredPromoCodes(sPromoCodes);
                transaction.setReceiveAgentID("");
                transaction.setSelectedFeeTableIndex(-1);
                if (transaction.isDestinationChanged()) {
                    transaction.setCustomerReceiveNumber("");
                    transaction.setRegistrationNumber("");
                }
                String mgiSessionID = transaction.getProfileObject() != null ? transaction.getProfileObject().getMgiSessionId() : null;
                transaction.getFeeCalculation(mgiSessionID, this);
            }
           //check whether destination country or Promo Code(s) changed
			else if (transaction.isDestinationChanged()
					|| hasPhoneNumberChanged
					|| sPromoCodes.compareTo(transaction
							.getEnteredPromoCodes()) != 0) {
                transaction.setSelectedFeeTableIndex(-1);
                transaction.setDestination(country);
                transaction.setEnteredPromoCodes(sPromoCodes);
                transaction.setAmount(amountField.getValue(),
                		currencyComboBox.getSelectedItem().toString()); 
                transaction.setReceiveAgentID("");
                transaction.setCustomerReceiveNumber("");
                transaction.setRegistrationNumber("");
                String mgiSessionID = transaction.getProfileObject() != null ? transaction.getProfileObject().getMgiSessionId() : null;
                transaction.getFeeCalculation(mgiSessionID, this);
                
            } else {
              	transaction.setDestinationChanged(false);
				if ((transaction.getFeeInfo() == null) || (transaction.getFeeInfo().isEmpty())) {
					transaction.setSelectedFeeTableIndex(-1);
					transaction.setDestination(country);
					transaction.setAmount(amountField.getValue(),
							currencyComboBox.getSelectedItem().toString());
					transaction.setReceiveAgentID("");
	                String mgiSessionID = transaction.getProfileObject() != null ? transaction.getProfileObject().getMgiSessionId() : null;
	                transaction.getFeeCalculation(mgiSessionID, this);
				} else if (! transaction.isFeeQuery()) {
                    transaction.setAmount(amountField.getValue(),
                   		 currencyComboBox.getSelectedItem().toString());
                    /*
                     * Always do fee lookup in order to get a new session ID,
                     * Otherwise may have problems with validation
                     */
                    String mgiSessionID = transaction.getProfileObject() != null ? transaction.getProfileObject().getMgiSessionId() : null;
                    transaction.getFeeCalculation(mgiSessionID, this);
            	} else {
                    PageNotification.notifyExitListeners(this, direction);
            	}
        	}
        }

        if (direction == PageExitListener.CANCELED) {
            PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
        } else if (direction == PageExitListener.BACK) {
        	if (currencyComboBox.getModel().getSelectedItem().equals(selectText)) {
        		transaction.setAmount(amountField.getValue());
        	} else {
        		transaction.setAmount(amountField.getValue(), currencyComboBox.getSelectedItem().toString()); 
        	}
            PageNotification.notifyExitListeners(this, PageExitListener.BACK);
        }
    }

    /**
     * Called by transaction after it receives response from middleware.
     * @param commTag is function index.
     * @param returnValue is Boolean set to true if response is good. 
     * Otherwise, it is set to false.
     */
    @Override
	public void commComplete(int commTag, Object returnValue) {
        boolean b = ((Boolean) returnValue).booleanValue();
        if (b && setAmount(transaction.getAmount(), transaction.getSendCurrency())) {
            PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
        } else {       	
        	 flowButtons.setButtonsEnabled(true);
        }
    }
}
