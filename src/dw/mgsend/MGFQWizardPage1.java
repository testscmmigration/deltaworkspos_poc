/*
 * MGFQWizardPage1.java
 * 
 * $Revision$
 *
 * Copyright (c) 2002-2005 MoneyGram International
 */

package dw.mgsend;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import dw.accountdepositpartners.AccountDepositPartnersTransaction;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.dwgui.DelimitedTextField;
import dw.dwgui.MoneyField;
import dw.dwgui.MultiListComboBox;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.CountryInfo.Country;
import dw.model.adapters.CountryInfo.CountrySubdivision;
import dw.utility.AgentCountryInfo;
import dw.utility.DWValues;
import dw.utility.StringUtility;

/**
 * "feeQueryEntry"
 * Page 1 of the MoneyGram Fee Query Wizard. This page prompts the user for the
 *   send of receive amount, whether fee are to be deducted from send amount, 
 *   the destination country, and the delivery option (if valid).  This information
 *   will be passed on to the MoneyGram Send pages.
 */
public class MGFQWizardPage1 extends MoneyGramSendFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	private MoneyField sendField;
    private MoneyField receiveField;
    private JLabel dummy1;
    private JLabel dummy2;
    private JLabel currencyLabel2;
    private JLabel oRLabel;
    private JPanel receivePanel;
    private MultiListComboBox<Country> destinationComboBox;
	private JPanel statePanel;
    private MultiListComboBox<CountrySubdivision> stateBox;
    private JCheckBox deductFeeFromSend;
    private MultiListComboBox<String> currencyComboBox;
    private List<String> currencyList;
    private String selectText;
    private String selectTextCurrency;
    private DWTextPane screenInstructions1;
    private JPanel promoPanel;
    private DelimitedTextField promoField;
    private BigDecimal bdTempRecvAmt = BigDecimal.ZERO;
    private Country lastSelectedCountry;
    public MGFQWizardPage1(MoneyGramSendTransaction tran,
                       String name,  String pageCode, PageFlowButtons buttons) {
        super(tran, "moneygramsend/MGFQWizardPage1.xml", name, pageCode, buttons);	
        init();
    }
    public MGFQWizardPage1(AccountDepositPartnersTransaction tran,
            String name,  String pageCode, PageFlowButtons buttons) {
	super(tran, "moneygramsend/MGFQWizardPage1.xml", name, pageCode, buttons);	
	init();
	}

	private void init() {
		collectComponents();
        setListBoxes();
        selectText = Messages.getString("MGFQWizardPage1.item1");
        selectTextCurrency = Messages.getString("MGFQWizardPage1.item1");
        promoPanel = (JPanel) getComponent("promoPanel"); 
        promoField = (DelimitedTextField) getComponent("promoField"); 
        // State information
		statePanel = (JPanel) getComponent("statePanel");
    	stateBox = (MultiListComboBox<CountrySubdivision>) getComponent("stateComboBox");	

        if (AgentCountryInfo.isStateRequired()){
        	statePanel.setEnabled(true);
        	statePanel.setVisible(true);
	    	populateCodeTablesStateBox(stateBox, CountryInfo.getAgentCountry(), CountryInfo.getAgentCountrySubdivision());
	    	
        } else {
        	statePanel.setEnabled(false);
        	statePanel.setVisible(false);        	
        }
        
        screenInstructions1.addListener(this, this);
	}
    
	@Override
	public void dwTextPaneResized() {
		
		int width = getComponent("panel1").getPreferredSize().width;
		screenInstructions1.setWidth(width);
		screenInstructions1.setListenerEnabled(false);
		int minsize=DWValues.DW_MINIMUM_WIDTH-50;
		screenInstructions1.setMinimumSize(new Dimension(minsize, screenInstructions1.getPreferredSize().height));
	}

	@Override
	public void start(int direction) {
        flowButtons.reset();
        flowButtons.setVisible("expert", false);	
        flowButtons.setEnabled("expert", false);	
        flowButtons.setEnabled("back", false);	
        receivePanel = (JPanel) getComponent("receivePanel");
        oRLabel = (JLabel) getComponent("oRLabel");
        addActionListener("destinationComboBox", this, "destinationChanged");	 
        addActionListener("currencyComboBox", this, "currencyChanged");	 
        //addActionListener("deliveryOptionComboBox", this, "deliveryChanged");	 
        addActionListener("deductFeeFromSend", this, "deductFeeFromSend");	 
        addTextListener("sendField", this, "amountFieldAction");	 
        addTextListener("receiveField", this, "amountFieldAction");	 
        
        dummy1.setText("         ");
        dummy2.setText("         ");
        currencyLabel2.setText("         ");
        
        transaction.setSelectedFeeTableIndex(-1);
        if (direction == PageExitListener.NEXT) {
            destinationChanged();
            destinationComboBox.requestFocus();
        } else{
            if (transaction.getDestination() != null) {
                destinationComboBox.setSelectedItem(transaction.getDestination());        	
            }
            else {
                destinationComboBox.setSelectedItem(CountryInfo.getAgentCountry().getCountryCode());
            }
        }

        deductFeeFromSend.setSelected(transaction.isDeductFeeFromSend());
        
        setBoxStates();
        if ((receiveField.getValue().compareTo(BigDecimal.ZERO) > 0) &&
                (transaction.getExpectedRecieveAmt().compareTo(BigDecimal.ZERO) >0)){
            receiveField.setText(transaction.getExpectedRecieveAmt().toString());
            sendField.setText(BigDecimal.ZERO.toString());
            if (direction != PageExitListener.NEXT) {
            	receiveField.requestFocus();
            }
        }
        else{
            receiveField.setText(BigDecimal.ZERO.toString());
//            if (transaction.isDeductFeeFromSend())
//                sendField.setText(transaction.getSendTotal().toString());
//            else
//                sendField.setText(transaction.getAmount().toString());
            if (direction != PageExitListener.NEXT) {
            	sendField.requestFocus();
            }
        }

        /*
         *  if multiple send currencies, then change the prompt to tell the
         *  agent to request a currency, and set the default to a historic
         *  send currency (if there is a valid one) or the select text.
         */
        if (currencyList.size() > 1) {
        	screenInstructions1.setText(Messages.getString("MGFQWizardPage1.977x"));
        	String sendCurrency = transaction.getSendCurrency();
        	if (StringUtility.getNonNullString(sendCurrency).isEmpty()) {
            	currencyComboBox.getModel().setSelectedItem(selectTextCurrency);
        	}
            else {
				Iterator<String> enumeratedVals = currencyList.iterator();
				boolean found = false;
				/*
				 * search the currency list to make sure the transaction 
				 * send currency is valid for this agent.  If so, default to
				 * it, otherwise default to the select text.
				 */
				while (enumeratedVals.hasNext() && !found) {
					found = ((enumeratedVals.next()).compareToIgnoreCase(sendCurrency)==0);
				}
				if (found) {
                	currencyComboBox.getModel().setSelectedItem(sendCurrency);
				}
				else {
                	currencyComboBox.getModel().setSelectedItem(selectTextCurrency);
				}
            }
        	currencyComboBox.setEnabled(true);
        }
        else {
        	currencyComboBox.setEnabled(false);
        }

        if (AgentCountryInfo.isStateRequired()){
        	statePanel.setEnabled(true);
        	statePanel.setVisible(true);
        	Country c = (Country) destinationComboBox.getSelectedItem();
	        updateCodeTableStateList(c, stateBox);
	        CountrySubdivision spiStateInfo = transaction.getDestinationState();
        	if ( spiStateInfo != null) {
        		/*
        		 * Skip item 0, since it is not the 'select' text not a state
        		 * item.
        		 */
        		for (int ii = 1; ii < stateBox.getItemCount(); ii++ ) {
        			CountrySubdivision spiListItem = (CountrySubdivision) stateBox.getItemAt(ii);
            		if (spiStateInfo.equals(spiListItem)) {
                		stateBox.setSelectedItem(spiListItem);
                		break;
            		}
        		}
        			
        	}
        } else {
        	statePanel.setEnabled(false);
        	statePanel.setVisible(false);
        }
        
        KeyListener nexter = new KeyAdapter() {
                    @Override
					public void keyPressed(KeyEvent e) {
                        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                            e.consume();
                            //destination();
                        }}};
        addKeyListener("destinationComboBox", nexter);	
        boolean arePromoCodesSupported = (DWValues.getPromoCodeLimit() > 0); 
        promoPanel.setEnabled(arePromoCodesSupported);
        promoPanel.setVisible(arePromoCodesSupported);
    }

    @Override
	public void finish(int direction) {
        flowButtons.disable();
        transaction.clearSender();
        
        Country country = (Country) destinationComboBox.getSelectedItem();
        if (direction == PageExitListener.NEXT) {
        	if (AgentCountryInfo.isStateRequired()){
        		if ((country.getCountryCode().equals("USA")|| country.getCountryCode().equals("CAN") )&& stateBox.getSelectedIndex()< 1){
                    stateBox.requestFocus();
                    flowButtons.setButtonsEnabled(true);
                    return;
        		}
        		if (stateBox.getSelectedIndex() > 0)
        			transaction.setDestinationState((CountrySubdivision) stateBox.getSelectedItem());
        		else
        			transaction.setDestinationState(null);
        	}
        	
        	if (currencyComboBox.getModel().getSelectedItem().equals(selectText)){
                flowButtons.reset();
                flowButtons.setVisible("expert", false);	
                flowButtons.setEnabled("expert", false);	
                flowButtons.setEnabled("back", false);	
        		currencyComboBox.requestFocus();
                return;
        	}
        	if ((sendField.getValue().compareTo(BigDecimal.ZERO) == 0) &&
        	   (receiveField.getValue().compareTo(BigDecimal.ZERO) == 0)){
                Toolkit.getDefaultToolkit().beep();
                flowButtons.reset();
                flowButtons.setVisible("expert", false);	
                flowButtons.setEnabled("expert", false);	
                flowButtons.setEnabled("back", false);	
                sendField.requestFocus();
                return;
        	}        	
        	else if (sendField.getValue().compareTo(BigDecimal.ZERO) > 0){
                if (setAmount(sendField.getValue(), 
                		currencyComboBox.getSelectedItem().toString())){
                    transaction.setExpectedRecieveAmt(null); 
                }
                else{
                	if (transaction.isManagerOverrideFailed()){
                    	PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
                    	return;
                	} else {
	                    flowButtons.reset();
	                    flowButtons.setVisible("expert", false);	
	                    flowButtons.setEnabled("expert", false);	
	                    flowButtons.setEnabled("back", false);	
	                    sendField.requestFocus();
	                    return;
                	}
                }
            }
            else {
                transaction.setAmount(BigDecimal.ZERO,
                		currencyComboBox.getSelectedItem().toString());	
                transaction.setExpectedRecieveAmt(receiveField.getValue());
            }
        	
        	String sTemp = promoField.getText();
       		if (!validPromoCodeCount(sTemp)) {
                flowButtons.reset();
                flowButtons.setVisible("expert", false);	
                flowButtons.setEnabled("expert", false);	
                flowButtons.setEnabled("back", false);	
                promoField.requestFocus();
                promoField.selectAll();
                return;
       		}
       		transaction.setEnteredPromoCodes(sTemp);

            transaction.setDestination(country);           
            country.getCountryCode();        
            transaction.getFeeCalculation(null, this);
        }
        else
        	PageNotification.notifyExitListeners(this, direction);
//           
//        
//        PageNotification.notifyExitListeners(this, direction);
    }

    /**
     * Callback from transaction after the comm is finished. If the comm was
     *   successful, go to the next page. Otherwise, go back to the card number
     *   field.
     */
    @Override
	public void commComplete(int commTag, Object returnValue) {
        boolean b = ((Boolean) returnValue).booleanValue();
        if (b && setAmount(transaction.getAmount(), 
        		transaction.getSendCurrency())) {
            PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
        }
      
        else {
            PageNotification.notifyExitListeners(this, PageExitListener.DONOTHING);
            if (sendField.isEnabled())
                sendField.requestFocus();
            else
                receiveField.requestFocus();
            flowButtons.reset();
            flowButtons.setVisible("expert", false);	
            flowButtons.setEnabled("expert", false);	
            flowButtons.setEnabled("back", false);	
        }
    }

    public void amountFieldAction() {
        setBoxStates();
    }
    
    public void deductFeeFromSend(){    	
        deductFeeFromSend.setSelected(!transaction.isDeductFeeFromSend());
        transaction.setDeductFeeFromSend(!transaction.isDeductFeeFromSend());
    }

	/**
	 * Update MoneyField(s) to use new currency
	 */
	public void currencyChanged() {
		if (currencyComboBox.getSelectedIndex() != -1) {
			sendField
					.setCurrency(currencyComboBox.getSelectedItem().toString());
			sendField.reformat();
		}
	}
    
    /**
     * Check the new destination. If it is mexico, toggle the options box.
     */
    public void destinationChanged() {
    	
    	Country country = (Country) destinationComboBox.getSelectedItem();
        if (country == null)
            return;
        if(lastSelectedCountry!=null && !lastSelectedCountry.equals(country))
        {
        	transaction.setSelectedFeeTableIndex(-1);
        }
        lastSelectedCountry = country;
        if (AgentCountryInfo.isStateRequired()) {
  	      updateCodeTableStateList(country, stateBox);
  	      boolean areStatesActive = (stateBox.getItemCount() > 1);
  	      stateBox.setEnabled(areStatesActive);
        }
        
        String indicative = CountryInfo.getIndicativeCountry(country.getCountryCode());
        if(indicative.equalsIgnoreCase("false")){
        	if (!receivePanel.isVisible()) {
        		receiveField.setValue(bdTempRecvAmt);
            	receivePanel.setVisible(true);
            	oRLabel.setVisible(true);
        	}
        }
        else{
        	if (receivePanel.isVisible()) {
        		bdTempRecvAmt = receiveField.getValue();
            	receivePanel.setVisible(false);
            	oRLabel.setVisible(false);
            	receiveField.setValue(BigDecimal.ZERO);
            	setBoxStates();
        	}
        }
     }

    private void setBoxStates() {
        // set the enabled state of the boxes
        setComponentEnabled(sendField,!(receiveField.getValue().compareTo(BigDecimal.ZERO) > 0), true);
        setComponentEnabled(receiveField,!(sendField.getValue().compareTo(BigDecimal.ZERO) > 0), true);
        if (receiveField.getValue().compareTo(BigDecimal.ZERO) > 0) {
        	deductFeeFromSend.setSelected(false);
        	transaction.setDeductFeeFromSend(false);
        	setComponentEnabled(deductFeeFromSend, false, true);       	
        }
        else {
        	deductFeeFromSend.setSelected(transaction.isDeductFeeFromSend());
        	setComponentEnabled(deductFeeFromSend, true, true);     
        }

   }

	private void setListBoxes() {
		// populate country box
		destinationComboBox.addList(CountryInfo.getCountries(CountryInfo.KEY_RECV_ACTIVE, transaction.isInternational()), transaction.getDestination(), "countries"); 
		// populate currency box
		currencyComboBox.removeAllItems();
		currencyComboBox.reset();
		currencyComboBox.addList(currencyList.toArray(), "Currencies");
	}
    
    /**
     * Collects all the XML defined components.
     */
    private void collectComponents() {
        // collect the component references
        screenInstructions1 = (DWTextPane) getComponent("screenInstructions1");
        sendField = (MoneyField) getComponent("sendField");	
        receiveField = (MoneyField) getComponent("receiveField");	
        dummy1 = (JLabel) getComponent("dummy1");
        dummy2 = (JLabel) getComponent("dummy2");
        currencyLabel2 = (JLabel) getComponent("currencyLabel2");
        destinationComboBox = (MultiListComboBox<Country>) getComponent("destinationComboBox");	
        deductFeeFromSend = (JCheckBox) getComponent("deductFeeFromSend");	
        currencyList = transaction.sendCurrencyList();
        currencyComboBox =  (MultiListComboBox<String>) getComponent("currencyComboBox"); 
    }
}
