package dw.mgsend;

import java.awt.event.KeyEvent;

import dw.framework.PageExitListener;
import dw.framework.PageNameInterface;

public class TSWizard extends MoneyGramSendTransactionInterface {
	private static final long serialVersionUID = 1L;

	public TSWizard(MoneyGramSendTransaction transaction, 
                                  PageNameInterface naming) {
        super("moneygramsend/MGSWizardPanel.xml", transaction, naming, true);	
        createPages();
    }

    @Override
	public void createPages() {
        addPage(TSWizardPage1.class, transaction, "tswizardpage1", "TSW05", buttons);	 
        addPage(TSWizardPage2.class, transaction, "tswizardpage2", "TSW10", buttons);	 
    }

    /**
     * Add the single keystroke mappings to the buttons. Use the function keys
     *   for back, next, cancel, etc.
     */
    private void addKeyMappings() {
        buttons.addKeyMapping("back", KeyEvent.VK_F11, 0);	
        buttons.addKeyMapping("next", KeyEvent.VK_F12, 0);	
        buttons.addKeyMapping("cancel", KeyEvent.VK_ESCAPE, 0);	
    }

    @Override
	public void start() {
        transaction.clear();
        super.start();
        addKeyMappings();
    }

    /**
     * Page traversal logic here.
     */
    @Override
	public void exit(int direction) {
        String currentPage = cardLayout.getVisiblePageName();
        String nextPage = null;
        
        if (direction != PageExitListener.NEXT ) {
            // default case
            super.exit(direction);
            return;
        }
        if (currentPage.equals("tswizardpage1")) {   
            nextPage = "tswizardpage2"; 
        } else if (currentPage.equals("tswizardpage2")) {   
            nextPage = null;
        }
        if (nextPage != null) {
            showPage(nextPage, direction);
            return;
        } else {
            super.exit(direction);
        }
    }
}