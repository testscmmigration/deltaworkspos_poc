package dw.mgsend;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;
import javax.swing.JTextField;

import com.moneygram.agentconnect.TransactionLookupResponse;
import com.moneygram.agentconnect.TransactionStatusType;

import dw.dialogs.Dialogs;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.framework.DataCollectionSet;
import dw.framework.DataCollectionSet.DataCollectionType;
import dw.i18n.Messages;
import dw.io.report.AuditLog;
import dw.utility.DWValues;


/**
 * Page 1 of the Transaction Status Wizard. This page prompts the user to enter
 *   reference number to search for a transaction.
 */
public class TSWizardPage1 extends MoneyGramSendFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

    private JTextField referenceField;
    private JPanel referencePanel;
    private DWTextPane line1Label;

    public TSWizardPage1(MoneyGramSendTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "framework/ReferenceNumberWizardPage.xml", name, pageCode,  buttons);	
        referenceField = (JTextField) getComponent("referenceNumber");	
        referencePanel = (JPanel) getComponent("referenceNumberPanel");	
        
        line1Label = (DWTextPane) getComponent("text");	
        line1Label.addListener(this, this);
	}

	@Override
	public void dwTextPaneResized() {
        DWTextPane line1Label = (DWTextPane) getComponent("text");	
		int l = referencePanel.getPreferredSize().width;
		line1Label.setWidth(l);
		line1Label.setListenerEnabled(false);
	}

    @Override
	public void start(int direction) {
        flowButtons.reset();
        flowButtons.setVisible("expert", false);	
        flowButtons.setEnabled("back", false);	
        referenceField.requestFocus();
        KeyListener nexter = new KeyAdapter() {
            @Override
			public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    e.consume();
                    finish(PageExitListener.NEXT);
                }
            }
        };

        addKeyListener("referenceNumber", nexter); 

        String text = Messages.getString("TSWizardPage1.1");
        line1Label.setText(text); 
        referenceField.requestFocus();
        referenceField.selectAll();
    }

     @Override
	public void finish(int direction) {
        // call setButtonsEnabled to avoid duplicated transactions.
        flowButtons.setButtonsEnabled(false);
        if (direction == PageExitListener.NEXT) {
            transaction.clear();
   			JTextField[] fields = {referenceField};
            if (! validateFields(fields)) {
                referenceField.requestFocus();
                flowButtons.reset();
                flowButtons.setVisible("expert", false);	
                flowButtons.setEnabled("back", false);	
                return;
            } else {
                transaction.setTsReferenceNumber(referenceField.getText());
                AuditLog.writeMgsTSRecord(transaction.getUserID(),referenceField.getText());
                transaction.setDataCollectionData(new DataCollectionSet(DataCollectionType.VALIDATION));
                transaction.transactionLookup(referenceField.getText(), this, DWValues.TRX_LOOKUP_STATUS, false);
            }
        } else {
            PageNotification.notifyExitListeners(this, direction);
        }
    }
  
    /**
     * Callback from transaction after the comm is finished. If the comm was
     *   successful, go to the next page and display the transaction status. 
     *  Otherwise, go back to the Reference Number Page
     */
    @Override
	public void commComplete(int commTag, Object returnValue) {
        boolean b = ((Boolean) returnValue).booleanValue();
        if (b) {
        	TransactionLookupResponse response = transaction.getTransactionLookupResponse();
        	TransactionStatusType status = ((response != null) && (response.getPayload() != null) && (response.getPayload().getValue() != null)) ? response.getPayload().getValue().getTransactionStatus() : null;
        	if (status != null) {
                PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
                return;
        	}
            Dialogs.showError("dialogs/DialogInvalidTransactionStatus.xml");
        }

    	flowButtons.setButtonsEnabled(true);
        flowButtons.setEnabled("back", false);	
    }
}
