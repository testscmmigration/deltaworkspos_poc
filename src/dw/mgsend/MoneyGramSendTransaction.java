package dw.mgsend;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeSet;

import com.moneygram.agentconnect.AmountInfo;
import com.moneygram.agentconnect.BPValidationRequest;
import com.moneygram.agentconnect.BPValidationResponse;
import com.moneygram.agentconnect.BillerInfo;
import com.moneygram.agentconnect.BillerSearchType;
import com.moneygram.agentconnect.BusinessError;
import com.moneygram.agentconnect.CompleteSessionRequest;
import com.moneygram.agentconnect.CompleteSessionResponse;
import com.moneygram.agentconnect.CompletionReceiptType;
import com.moneygram.agentconnect.ConsumerProfileIDInfo;
import com.moneygram.agentconnect.CreateOrUpdateProfileSenderResponse;
import com.moneygram.agentconnect.CurrentValuesType;
import com.moneygram.agentconnect.FeeInfo;
import com.moneygram.agentconnect.FeeLookupRequest;
import com.moneygram.agentconnect.FeeLookupRequest.PromoCodes;
import com.moneygram.agentconnect.FeeLookupResponse;
import com.moneygram.agentconnect.GetProfileSenderResponse;
import com.moneygram.agentconnect.InfoBase;
import com.moneygram.agentconnect.KeyValuePairType;
import com.moneygram.agentconnect.ObjectFactory;
import com.moneygram.agentconnect.ProductVariantType;
import com.moneygram.agentconnect.PromotionInfo;
import com.moneygram.agentconnect.ReceiptSegmentType;
import com.moneygram.agentconnect.ReceiveAmountInfo;
import com.moneygram.agentconnect.Request;
import com.moneygram.agentconnect.Response;
import com.moneygram.agentconnect.SearchStagedTransactionsResponse;
import com.moneygram.agentconnect.SendAmountInfo;
import com.moneygram.agentconnect.SendValidationRequest;
import com.moneygram.agentconnect.SendValidationResponse;
import com.moneygram.agentconnect.SendValidationResponse.Payload.ReceiptInfo;
import com.moneygram.agentconnect.SessionType;
import com.moneygram.agentconnect.StagedTransactionInfo;
import com.moneygram.agentconnect.TextTranslationType;
import com.moneygram.agentconnect.TransactionLookupRequest;
import com.moneygram.agentconnect.TransactionLookupResponse;

import dw.billpayment.BillPaymentWizard;
import dw.comm.AgentConnect;
import dw.comm.MessageFacade;
import dw.comm.MessageFacadeError;
import dw.comm.MessageFacadeParam;
import dw.comm.MessageLogic;
import dw.dialogs.Dialogs;
import dw.dialogs.Receipt1stLanguageDialog;
import dw.dialogs.Receipt2ndLanguageDialog;
import dw.framework.ClientTransaction;
import dw.framework.CommCompleteInterface;
import dw.framework.DataCollectionField;
import dw.framework.DataCollectionPanel;
import dw.framework.DataCollectionSet;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.DataCollectionSet.DataCollectionType;
import dw.framework.FieldKey;
import dw.framework.FlowPage;
import dw.framework.FormFreeIdVerificationWizardPage;
import dw.framework.WaitToken;
import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.io.DiagLog;
import dw.io.IsiReporting;
import dw.io.diag.MustTransmitDiagMsg;
import dw.io.info.BillPaymentDiagInfo;
import dw.io.info.MoneyGramSendDiagInfo;
import dw.io.report.AuditLog;
import dw.io.report.ReportLog;
import dw.main.DeltaworksStartup;
import dw.main.MainPanelDelegate;
import dw.mgsend.MoneyGramClientTransaction.DataCollectionScreenStatus.ScreenStatusType;
import dw.model.adapters.CachedACResponses;
import dw.model.adapters.CompanyInfo;
import dw.model.adapters.ConsumerProfile;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.CountryInfo.Country;
import dw.model.adapters.CountryInfo.CountrySubdivision;
import dw.model.adapters.CustomerInfo;
import dw.model.adapters.Data;
import dw.model.adapters.Data.DataValue;
import dw.model.adapters.MoneyGramInfo40;
import dw.model.adapters.MoneyGramSendInfo40;
import dw.model.adapters.PersonalInfo;
import dw.model.adapters.PromoCodeInfo;
import dw.model.adapters.PromotionalSlipInfo;
import dw.model.adapters.ReceiveDetailInfo;
import dw.model.adapters.ReceiverInfo;
import dw.model.adapters.SenderInfo;
import dw.pdfreceipt.PdfReceipt;
import dw.printing.Report;
import dw.profile.Profile;
import dw.profile.ProfileItem;
import dw.profile.UnitProfile;
import dw.profile.UnitProfileInterface;
import dw.prtreceipt.PrtReceipt;
import dw.utility.AgentCountryInfo;
import dw.utility.Currency;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.ExtraDebug;
import dw.utility.ServiceOption;
import dw.utility.StringUtility;
import dw.utility.TimeUtility;

/**
 * MoneyGramSendTransaction encapsulates the information needed to send a
 * MoneyGram from the POS. The message to the middleware contains a data object
 * (MoneyGramSendInfo) and returns data object (MoneyGramInfo). The accessors
 * for all the transaction values delegate to these objects so we don't need to
 * fill them in before sending them or get values from them after sending.
 */
public class MoneyGramSendTransaction extends MoneyGramClientTransaction {
	private static final long serialVersionUID = 1L;
	
	// Added as part of new API Changes - referenceNumberLookup to transactionLookup - bhl5 - 09/12/2016
	protected ReceiveDetailInfo detail;
	 protected WaitToken wait = new WaitToken();

	public static final int TRAN_ENTRY_UBP = 0;
	public static final int TRAN_ENTRY_PPC = 1;
    
	private Country destination = new Country();
	private CountrySubdivision destinationState;
	private PersonalInfo[] matchingCustomers = null;
	private List<CustomerInfo> customers;
	private TreeSet<CustomerInfo> uniqueCustomers;
	private List<CustomerInfo> customerHistory;
	private CustomerInfo customerInfo;
	private MoneyGramSendInfo40 sendInfo; // holds values to be sent to the
	private MoneyGramInfo40 returnInfo; // results from the transaction send
	private BigDecimal fraudLegalLimit;
	private BigDecimal thirdParyLimit;
	private BigDecimal billPayDailyLimit;
	private BigDecimal defaultInformationalFee;
	private boolean isDefaultMaxFee = false;
	private String internationalEnable = "NON"; 
	private boolean isDSS;
	private boolean destinationChanged = false;
	private boolean isServiceOptionChanged = false;
	private boolean bEditActive = false;
	private boolean billPayment = false;
	private CompanyInfo[] matchingCompanies = null;
	private BillerInfo[] matchingBillers = null;
	private FeeLookupResponse feeMsgResp = null;
	private List<BillerInfo> selectedBillers = null;
	private CompanyInfo[] recentCompanies = null;
	private CompanyInfo receivingCompany = new CompanyInfo();
	private BillerInfo billerInfo = new BillerInfo();
	private ArrayList<ArrayList<BillerInfo>> billerNameList;
	private ArrayList<BillerInfo> billerList;
	private int billerNameListIndex = 0;
	private int billerListIndex = 0;
	private boolean billerGroupingNeeded = false;
	private int selectedCustomerIndex = 0; // flag for saving user entered data
	private int selectedRecipientIndex = 0;
	private BigDecimal dailyTotal;
	private BigDecimal dailyBillPayTotal;
	private boolean feeQuery = false;
	private boolean isSendToAccount = false;
	private boolean financialTransaction = true;
	private boolean deductFeeFromSend = false;
	private boolean guaranteeInd = false;
	private boolean directoryOfBillers = false;
	private boolean homeDelivery = false;
	private String cardPhoneNumber = "";
	private boolean usingCard = false;
	private boolean receiverRegistration = false;
	private boolean accountDepositPartners = false;
	private boolean transactionStatus = false;
	private String tsReferenceNumber = "";
	private boolean isDestOrDOChanged = false;
	private BigDecimal employeeSendLimit;
	private boolean maxEmployeeLimitExceeded = false;
	private int managerOverrideID;
	private boolean managerOverrideFailed = false;
	private String countryOfBirth;
	private String nationalityOfBirth;
	private String currentNationality;
	private String secondIdIssuanceDate;
	private String secondIdIssuanceExpirationDate;
	private String idVerifiedStored;
	private String activeFreqCustCardNumber = "";
	private String freqCustCardNumber = "";
	private String consumerId = "";
	private String senderFirstName = "";
	private String senderLastName = "";
	private String billerSearchIndustryButton;
	private String agencyId;
	private int selectedFeeTableIndex = -1;
	private int tranEntryType;
	private String cardType;
	private boolean isEnrolled = false;
	private String rewardsFirstName;
	private String rewardsLastName;
	private boolean tempCard;
	private String  sAccountNickName;
	private String  sAccountNumberLabel;
	private String  sDynamicAccountLabel;
	private String  sRRNLabel;
	private String  sPhoneNumber;
	private String  sCountryDialCode;
	private boolean anyModifiedFees = false;
	private boolean anyModifiedRecvAmts = false;
	private int enteredPromoCodeCount = 0;
	private String enteredPromoCodes="";
	private static PromoCodeInfo[] badPromoInfo;
	private int returnedPromoCodeCount = 0;
    private int receipt1stLanguageIdx = 0;
    private int receipt2ndLanguageIdx = 0;
    private static String receipt1stLanguageMnemonic = DWValues.MNEM_ENG;
    private static String receipt2ndLanguageMnemonic = "";
	private static boolean bHasMultipleLanguages = true;
	private static boolean bHasSingleReceiptLanguage = false;
    private static boolean receiptAllow2ndLanguageNone = false;
	private static List<String> receipt1stLanguageMnemonicList = new ArrayList<String>();
	private static List<String> receipt2ndLanguageMnemonicList = new ArrayList<String>();
    private String  sSavedAcctNbr;
    private static PdfReceipt pdfReceiptDisc1;
    private static PdfReceipt pdfReceiptDisc2;
    private static PdfReceipt pdfReceiptAgent1;
    private static PdfReceipt pdfReceiptCust1;
    private static PdfReceipt pdfReceiptCust2;
    private static PrtReceipt prtReceiptDisc1;
    private static PrtReceipt prtReceiptDisc2;
    private static PrtReceipt prtReceiptAgent1;
    private static PrtReceipt prtReceiptCust1;
    private static PrtReceipt prtReceiptCust2;
    private boolean bPrintFailure = false;
    private ServiceOption selectedDeliveryInfo;
	private boolean formFreeSendEnabled = false;
	private List<StagedTransactionInfo> formFreeTransactionList;
	private boolean isDataFromHistory;
	private Map<BillerInfo, String> billerDisplayNameMap;
	private DataCollectionScreenStatus senderInformationScreenStatus = new DataCollectionScreenStatus(DataCollectionSet.MSW30_PANEL_NAMES, null);
	private DataCollectionScreenStatus senderThirdPartyScreenStatus = new DataCollectionScreenStatus(DataCollectionSet.MSW40_PANEL_NAMES, null);
	private DataCollectionScreenStatus receiverNameScreenStatus = new DataCollectionScreenStatus(DataCollectionSet.MSW60_PANEL_NAMES, null);
	private DataCollectionScreenStatus receiverAccountInformation1ScreenStatus = new DataCollectionScreenStatus(DataCollectionSet.MSW61_PANEL_NAMES, null);
	private DataCollectionScreenStatus receiverAccountInformation2ScreenStatus = new DataCollectionScreenStatus(DataCollectionSet.MSW63_PANEL_NAMES, null);
	private DataCollectionScreenStatus supplementalInformationScreenStatus = new DataCollectionScreenStatus();
	private SendValidationRequest sendValidationRequest;
	private SendValidationResponse sendValidationResponse;
	private SendValidationResponse.Payload sendValidationResponsePayload;
	private BPValidationResponse bpValidationResponse;
	private BPValidationResponse.Payload bpValidationResponsePayload;
	private boolean hasBillerData = false;
	
	private DataCollectionScreenStatus screen36Status = new DataCollectionScreenStatus(DataCollectionSet.BPW36_PANEL_NAMES, null);
	private DataCollectionScreenStatus screen38Status = new DataCollectionScreenStatus(DataCollectionSet.BPW38_PANEL_NAMES, null);
	private DataCollectionScreenStatus screen39Status = new DataCollectionScreenStatus(DataCollectionSet.BPW39_PANEL_NAMES, null);
	private DataCollectionScreenStatus screen25Status = new DataCollectionScreenStatus(DataCollectionSet.BPW25_PANEL_NAMES, null);
	private DataCollectionScreenStatus screen30Status = new DataCollectionScreenStatus(DataCollectionSet.BPW30_PANEL_NAMES, null);
	
	private String mgiTransactionSessionId;
	private FeeInfo feeInfo;
	private StagedTransactionInfo formFreeTransaction;
	private boolean afterValidationAttempt;
	private String referenceNumber;
	private CompleteSessionResponse completeSessionResponse;
	private GetProfileSenderResponse getProfileSenderResponse;
	private CreateOrUpdateProfileSenderResponse createOrUpdateProfileSenderResponse;
	
	public MoneyGramSendTransaction(String tranName, String tranReceiptPrefix) {
        super(tranName, tranReceiptPrefix, -1);
        dataCollectionData = new DataCollectionSet(DataCollectionType.VALIDATION);
		billerDisplayNameMap = new HashMap<BillerInfo, String>();
    }

	public MoneyGramSendTransaction(String tranName, String tranReceiptPrefix, int docSeq) {
		this(tranName, tranReceiptPrefix, docSeq, false);
	}

	public MoneyGramSendTransaction(String tranName, String tranReceiptPrefix, int docSeq, boolean billPay, boolean aReceiverRegistration) {
		super(tranName, tranReceiptPrefix, docSeq);
		dataCollectionData = new DataCollectionSet(DataCollectionType.VALIDATION);
		billerDisplayNameMap = new HashMap<BillerInfo, String>();
		clear(false);
		// to avoid an NPE.
		receiverRegistration = aReceiverRegistration;
		billPayment = billPay;
		dispenserNeeded = false;
		// getProfileValues();
		internationalEnable = "NON";
		billerDisplayNameMap.clear();
	}

	public MoneyGramSendTransaction(String tranName, String tranReceiptPrefix, int docSeq, boolean billPay) {
		super(tranName, tranReceiptPrefix, docSeq);
		dataCollectionData = new DataCollectionSet(DataCollectionType.VALIDATION);
		billerDisplayNameMap = new HashMap<BillerInfo, String>();
		billPayment = billPay;
		dispenserNeeded = false;
		clear();
	}

	public BigDecimal getDailyTotal() {
		return dailyTotal;
	}

	public BigDecimal getDailyBillPayTotal() {
		return dailyBillPayTotal;
	}

	public BigDecimal getDailyBillPayLimit() {
		return billPayDailyLimit;
	}
		
	public ReceiveDetailInfo getReceiveDetail() {
		return detail;
	}

	public void createNewReceiveDetailInfo() {
		detail = new ReceiveDetailInfo();
	}

	@Override
	public void clear() {
		clear(true);
		customerInfo = null;
		detail = null;
		wait.setWaitFlag(true);
		this.cardPhoneNumber = null;
		this.dataCollectionData = null;
	}

	public synchronized void clear(boolean getProfileValues) {
		// clear out all the transaction information
		super.clear();
//		Debug.println("SC["+tranName+"]"); 
		if (getProfileValues) {
			getProfileValues();
		}

		String defaultCountry = getDefaultCountry();
		// set defaults in send info object
		sendInfo = new MoneyGramSendInfo40();
		returnInfo = new MoneyGramInfo40();
		sendInfo.setSenderCountry(defaultCountry);
//		dataCollectionData.setFieldValue(FieldKey.SENDER_COUNTRY_KEY, defaultCountry);
		// getting discounted.
		setDestination(CountryInfo.getCountry(defaultCountry));
		setDestinationState(null);
		setEnteredPromoCodes("");
		setEnteredPromoCodeArray(null);
		setEnteredPromoCodeCount(0);
		/*
		 * Don't use setCardPhoneNumber since that spins off a new thread and
		 * in some cases clear() can be called a second time a second time in
		 * relatively quick succession and problems can ensue if the second
		 * call interrupts that thread.
		 */
		cardPhoneNumber = "";
		customers = null;
		newCustomer = true;
		recentCompanies = new CompanyInfo[0];
		clearSender();
		clearReceiver();

		// empty out lists
		matchingCustomers = new PersonalInfo[0];
		matchingCompanies = new CompanyInfo[0];
		recentCompanies = new CompanyInfo[0];
		setDeductFeeFromSend(false);
		// sendInfo.setDeliveryOption("OTHER");
		sendInfo.setDeliveryOption("WILL_CALL");

		if (dataCollectionData != null) {
			dataCollectionData.setFieldValue(FieldKey.SERVICEOPTION_KEY, "WILL_CALL");
			dataCollectionData.setFieldValue(FieldKey.ACCT_NBR_VERIFIED_KEY, "false");
		}

		countryOfBirth = "";
		nationalityOfBirth = "";
		currentNationality = "";
		secondIdIssuanceDate = "";
		secondIdIssuanceExpirationDate = "";
		idVerifiedStored = "";

		billerInfo = new BillerInfo();
		receivingCompany = new CompanyInfo();
		isEnrolled = false;
		freqCustCardNumber = "";

		sAccountNickName = "";
		sAccountNumberLabel = "";
		sDynamicAccountLabel = "";

		sPhoneNumber = "";
		sCountryDialCode = "";

		bEditActive = false;
		isServiceOptionChanged=false;

		setDataFromHistory(false);
		
		this.setDataCollecionReview(false);
		wait.setWaitFlag(true);

		setFormFreeTransaction(null);
		
//		Debug.println("EC["+tranName+"]"); 
	}

	/**
	 * Returns the 3-letter ISO code for the agent country. This can be used as
	 * the default sender country.
	 */
	private String getDefaultCountry() {
		Country ci = CountryInfo.getAgentCountry();
		if ((ci == null) || (ci.getCountryCode().trim().equals(""))) {
			return "USA";
		} else {
			return ci.getCountryCode();
		}
	}

	/**
	 * Retrieve some values from the profile interesting to this transaction
	 */
	public void getProfileValues() {

		if (billPayment) {
			try {
				fraudLegalLimit = getProductProfile().findCurrencyDecimalValue(
		        		"FRAUD_LIMIT_LEGAL_ID", agentBaseCurrency()); 
				thirdParyLimit = getProductProfile().findCurrencyDecimalValue(
		        		"FRAUD_LIMIT_THIRD_PARTY_ID", agentBaseCurrency()); 
				if (getProductProfileOption("DAILY_BILL_PAY_LIMIT") != null)
					billPayDailyLimit = getProductProfileBigDecimalOption(
							"DAILY_BILL_PAY_LIMIT"); 
				if (getProductProfileOption("UTILITY_BILL_PAY_INFO_FEE") != null) {
					defaultInformationalFee = getProductProfileBigDecimalOption(
							"UTILITY_BILL_PAY_INFO_FEE");

				}
				if (getProductProfileOption("UTILITY_BILL_PAY_MAX_FEE") != null) {
					isDefaultMaxFee = getProductProfileOption(
							"UTILITY_BILL_PAY_MAX_FEE").booleanValue();

				}

			} catch (NoSuchElementException e) {
				// Debug.println("Got to add --DAILY_BILL_PAY_LIMIT-- item in
				// profile");
			}
		}
		if ((!billPayment) && (!receiverRegistration) && (!accountDepositPartners)) {
			// testQuestionLimit = getProductProfileOption(
			// "FRAUD_LIMIT_TEST_QUESTION").bigDecimalValue(); 

			ProfileItem piTemp;

			piTemp = getProductProfileOption("RESTRICTION_CODE");
			if (piTemp != null) {
				internationalEnable = piTemp.stringValue(); 
			} else {
				internationalEnable = "NON"; 
			}

//			piTemp = getProductProfileOption(DWValues.DSR_INLINE);
//			if (piTemp != null) {
//				isInlineDSR = piTemp.booleanValue(); 
//			}
//			else {
//				isInlineDSR = false;
//			}
//
//			isInlineDSR=true;//Turning on Intra-Trn flag alway true as part of project#54351b
			/*
			 * Read list of languages for 2nd receipt from the profile.
			 */
			populateSendReceiptLanguageList();
		}
	}

	public boolean getReceiverRegistrationAllowed() {
		int MONEY_GRAM_RECEIVER_REGISTRATION = 9;
		Profile profile = UnitProfile.getInstance().getProfileInstance();
		ProfileItem item = profile.find(
				"PRODUCT", MONEY_GRAM_RECEIVER_REGISTRATION); 

		if ((null != item) && (item.getStatus().equalsIgnoreCase("A"))) {
			return true;
		} else {
			return false;
		}
	}

	public boolean getUtilityBillPayAllowed() {
		int UTILITY_PRODUCT_VARIANT = 2;

		ProfileItem item = getProductProfileOption("PRODUCT_VARIANTS",
				UTILITY_PRODUCT_VARIANT);

		if ((null != item) && (item.stringValue().equalsIgnoreCase("Y"))) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Check the amount of the send against limits.
	 */
	private int checkLimits(BigDecimal amount, String currency) {
		dailyTotal = UnitProfile.getInstance().getDailyTotal();
		getEmployeeMaxSendAmount(currency);

		BigDecimal maxItemAmt = getMaxItemAmount(currency);
		if (maxItemAmt.compareTo(ZERO) > 0
				&& amount.compareTo(maxItemAmt) > 0)
			return MAX_ITEM_AMOUNT_EXCEEDED;

		if (employeeSendLimit.compareTo(ZERO) > 0
				&& amount.compareTo(employeeSendLimit) > 0)
			return MAX_EMPLOYEE_SEND_LIMIT_EXCEEDED;

		return OK;
	}

	private int checkBillPayLimits(BigDecimal amount) {
		dailyBillPayTotal = UnitProfile.getInstance().getDailyBillPayTotal();
		BigDecimal newTotal = dailyBillPayTotal.add(amount);

		if (billPayDailyLimit == null) {
			if (getProductProfileOption("DAILY_BILL_PAY_LIMIT") != null) {
				billPayDailyLimit = getProductProfileBigDecimalOption("DAILY_BILL_PAY_LIMIT"); 
			} else {
				billPayDailyLimit = ZERO;
			}
		}

		if (billPayDailyLimit.compareTo(ZERO) > 0
				&& newTotal.compareTo(billPayDailyLimit) > 0) {
			return BILL_PAY_DAILY_LIMIT_EXCEEDED;
		}
		return OK;
	}

	public String getCardPhoneNumber() {
		return cardPhoneNumber;
	}

	public void resetCardPhoneNumber() {
		cardPhoneNumber = "";
	}

	private transient Comparator<CustomerInfo> comparator = new Comparator<CustomerInfo>() {
		@Override
		public int compare(CustomerInfo mg1, CustomerInfo mg2) {
			// get the extension number of the file

			int senderLastCompare = compareLastNames(mg1.getSender().getLastName(), mg2
					.getSender().getLastName());
			if (senderLastCompare != 0)
				return senderLastCompare;

			int senderFirstCompare = compareFirstNames(mg1.getSender().getFirstName(), mg2
					.getSender().getFirstName());
			if (senderFirstCompare != 0)
				return senderFirstCompare;

			int addressCityCompare = compareAddress(mg1.getSender().getCity(), mg2
					.getSender().getCity());
			if (addressCityCompare != 0)
				return addressCityCompare;
			int addressStateCompare = compareAddress(mg1.getSender().getState(), mg2
					.getSender().getState());
			if (addressStateCompare != 0)
				return addressStateCompare;

			int receiverLastCompare = compareLastNames(mg1.getReceiver().getLastName(),
					mg2.getReceiver().getLastName());
			if (receiverLastCompare != 0)
				return receiverLastCompare;

			int receiverFirstCompare = compareFirstNames(mg1.getReceiver().getFirstName(),
					mg2.getReceiver().getFirstName());
			if (receiverFirstCompare != 0)
				return receiverFirstCompare;

			int destinationCompare = compareDestination(mg1.getReceiver(), mg2.getReceiver());
			if (destinationCompare != 0)
				return destinationCompare;

			if (mg1.getCompanyInfo() != null && mg2.getCompanyInfo() != null) {

				ProductVariantType pv1 = mg1.getCompanyInfo().getProductVariant();
				ProductVariantType pv2 = mg2.getCompanyInfo().getProductVariant();
				
				if ((pv1 != null) && (pv2 != null)) {
					int variantCompare = pv1.compareTo(pv2);
					if (variantCompare != 0) {
						return variantCompare;
					}
				}
				int billerNameCompare = compareBillerNames(mg1.getCompanyInfo().getBillerName(), mg2.getCompanyInfo().getBillerName());
				if (billerNameCompare != 0) {
					return billerNameCompare;
				}
			} else if (mg1.getCompanyInfo() == null) {
				return 1;
			} else if (mg2.getCompanyInfo() == null) {
				return -1;
			}
			

			int recvCurrencyCompare = compareStrings(mg1.getReceiver().getReceiveCurrency(),
					mg2.getReceiver().getReceiveCurrency());
			if (recvCurrencyCompare != 0)
				return recvCurrencyCompare;

			String sRRN1 = mg1.getReceiver().getCustomerReceiveNumber();
			String sRRN2 = mg2.getReceiver().getCustomerReceiveNumber();
			int RRNCompare = compareStrings(sRRN1, sRRN2);
//            Debug.println("cRRN1["+sRRN1+ "], cRRN2["+sRRN2+ "]");
			if (RRNCompare != 0) {
				return RRNCompare;
			}

			sRRN1 = mg1.getReceiver().getCustomerReceiveNumber();
			sRRN2 = mg2.getReceiver().getCustomerReceiveNumber();
			RRNCompare = compareStrings(sRRN1, sRRN2);
//            Debug.println("mRRN1["+sRRN1+ "], mRRN2["+sRRN2+ "]");
			if (RRNCompare != 0)
				return RRNCompare;

			sRRN1 = mg1.getReceiver().getCustomerReceiveNumber();
			sRRN2 = mg2.getReceiver().getCustomerReceiveNumber();
			RRNCompare = compareStrings(sRRN1, sRRN2);
//            Debug.println("pRRN1["+sRRN1+ "], pRRN2["+sRRN2+ "]");
			if (RRNCompare != 0) {
				return RRNCompare;
			}

			return 0;
		}

		/*public boolean equals(Object o) {
			return false;
		}*/

		/** Return 0 if equal, -1 if p1 > p2, 1 if p1 < p2 . */
		public int compareFirstNames(String firstName1, String firstName2) {
			if ((firstName1 == null) && (firstName2 == null))
				return 0;
			else if (firstName1 == null)
				return 1;
			else if (firstName2 == null)
				return -1;
			else
				return firstName1.compareToIgnoreCase(
						firstName2);
		}

		/** Return 0 if equal, -1 if p1 > p2, 1 if p1 < p2 . */
		public int compareLastNames(String lastName1, String lastName2) {
			if ((lastName1 == null) && (lastName2 == null))
				return 0;
			else if (lastName1 == null)
				return 1;
			else if (lastName2 == null)
				return -1;
			else if (lastName1.equals("")
					&& lastName2.trim().length() > 0)
				return 1;
			else if (lastName2.equals("")
					&& lastName1.trim().length() > 0)
				return -1;
			else
				return lastName1.compareToIgnoreCase(
						lastName2);
		}

		/** Return 0 if equal, -1 if p1 > p2, 1 if p1 < p2 . */
		public int compareBillerNames(String p1, String p2) {
			if ((p1 == null) && (p2 == null))
				return 0;
			else if (p1 == null)
				return 1;
			else if (p2 == null)
				return -1;
			else if (p1.equals("") && p2.trim().length() > 0)
				return 1;
			else if (p2.equals("") && p1.trim().length() > 0)
				return -1;
			else
				return p1.compareToIgnoreCase(p2);
		}

		/** Return 0 if equal, -1 if p1 > p2, 1 if p1 < p2 . */
		public int compareAddress(String city1, String city2) {
			if ((city1 == null) && (city2 == null))
				return 0;
			else if (city1 == null)
				return 1;
			else if (city2 == null)
				return -1;
			else
				return city1.compareToIgnoreCase(city2);
		}
	
		/** Return 0 if equal, -1 if p1 > p2, 1 if p1 < p2 . */
		public int compareDestination(ReceiverInfo p1, ReceiverInfo p2) {
			if ((p1 == null) && (p2 == null))
				return 0;
			else if (p1 == null)
				return 1;
			else if (p2 == null)
				return -1;
			else if ((p1.getDestCountry() == null) && (p2.getDestCountry() == null))
				return 0;
			else if ((p1.getDestCountry() == null))
				return 1;
			else if (p2.getDestCountry() == null)
				return -1;
			else
				return p1.getDestCountry().compareToIgnoreCase(p2.getDestCountry());
		}

		/** Return 0 if equal, -1 if p1 > p2, 1 if p1 < p2 . */
		public int compareStrings(String p1, String p2) {
			if ((p1 == null) && (p2 == null))
				return 0;
			else if (p1 == null)
				return 1;
			else if (p2 == null)
				return -1;
			else
				return p1.compareToIgnoreCase(p2);
		}
	};

	/**
	 * Attempt to set the MoneySaver card # or Customer Phone # in this
	 * transaction. If this is a valid #, the corresponding senders and
	 * recipients information will be returned. The sender and recipient
	 * information will be returned from the RTS in an array of
	 * MoneyGramSendInfo40 objects.
	 *
	 * @param number
	 *            Card number or phone number
	 * @param card
	 *            If true, the above is a card number
	 * @param billPay
	 *            If true, we want EP history
	 */
	public void setCardPhoneNumber(final String number, final boolean card, final SessionType sessionType, final String filter, final CommCompleteInterface cci) {
		final MessageFacadeParam parameters = new MessageFacadeParam(
				MessageFacadeParam.REAL_TIME,
				MessageFacadeParam.MG_CUSTOMER_NOT_FOUND,
				MessageFacadeParam.MG_CARD_NOT_FOUND);
		// Custom comparator to sort transaction history return by the
		// getEnhancedTransactionHistory(request) message. Sort by
		// sender lastname, firstname and then receiver lastname.

		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {
				if (card) {
					setUsingCard(true);
				} else {
					setUsingCard(false);
				}

				cardPhoneNumber = number;
				if (number.equals("")) {
					clearCustomers();
					newCustomer = true;
					recentCompanies = new CompanyInfo[0];
					clearSender();
					clearReceiver();
					return Boolean.TRUE;
				}

				// card number was entered, see if there is a customer for it
				// but first make sure we have a different card #
				if (card && (getSender().getMoneySaverID() != null) && (getSender().getMoneySaverID().equals(number))) {
					return Boolean.TRUE;
				}

				// we are going to look for recents, but default to none
				clearReceiver();
				recentCompanies = new CompanyInfo[0];

				customers = null;
				try {
					int lookupType;
					if (card) {
						lookupType = DWValues.LOOKUP_CUST_ID;
					} else {
						lookupType = DWValues.LOOKUP_PHONE;
					}
					customers = MessageFacade.getInstance().consumerHistoryLookup(parameters, number, lookupType, sessionType, filter);
					uniqueCustomers = new TreeSet<CustomerInfo>(comparator);

					if ((customers != null) && (customers.size() > 0)) {
						for (int i = 0; i < customers.size(); i++) {
							CustomerInfo cust = customers.get(i);
							ReceiverInfo receiver = cust.getReceiver();
							receiver.setDestCountry(cust.getReceiver().getDestCountry());
							uniqueCustomers.add(cust);
						}
					} else if ((customers == null || customers.size() == 0) || (! isBillPayment() && customers.get(0).getSender().getLastName() == null)) {
						newCustomer = true;
						recentCompanies = new CompanyInfo[0];
						if (!isDirectoryOfBillers())
							setReceivingCompany(new CompanyInfo());
						clearSender();
						if (card) {
							getSender().setMoneySaverID(number);
						}
						clearReceiver();
						return Boolean.TRUE;
					}

				} catch (MessageFacadeError e) {
					if (e.hasErrorCode(MessageFacadeParam.MG_CUSTOMER_NOT_FOUND)) {
						if (!card) {
							Dialogs.showWarning(
											"dialogs/DialogCustomerNotFound.xml",
											Messages.getString("MoneyGramSendTransaction.2008"),
											Messages.getString("MoneyGramSendTransaction.2009"));
							// returning true will move to the next screen and
							// cause the
							// phone number to be carried over to the sender
							// detail screen.
							clearSender();
							// If a fee quote was done...the dest is already
							// set.
							if (!isFeeQuery()) {
								if(!isSendToAccount())
								{
									setDestination(getDefaultCountry());
								}
							}
							newCustomer = true;
							clearCustomers();
							return Boolean.TRUE;
						}
					} else if (e.hasErrorCode(MessageFacadeParam.MG_CARD_NOT_FOUND)) {
						Dialogs.showWarning(
										"dialogs/DialogCustomerNotFound.xml",
										Messages.getString("MoneyGramSendTransaction.2005"),
										Messages.getString("MoneyGramSendTransaction.2006"));
						// returning false will remain on card phone number
						// screen.
						
						return Boolean.FALSE;
					} else if (e.hasErrorCode(MessageFacadeParam.USER_CANCEL)) {
						// returning false will remain on card phone number
						// screen.

						return Boolean.FALSE;
					}
				}
				if ((customers == null || customers.size() == 0) || (! isBillPayment() && customers.get(0).getSender().getLastName() == null)) {
					newCustomer = true;
					recentCompanies = new CompanyInfo[0];
					if (!isDirectoryOfBillers()) {
						setReceivingCompany(new CompanyInfo());
					}
					clearSender();
					if (card) {
						getSender().setMoneySaverID(number);
					}
					clearReceiver();
					return Boolean.TRUE;
				}

				for (CustomerInfo customer : uniqueCustomers) {
					if (card) {
						customer.getSender().setMoneySaverID(number);
					}
					String sTemp = customer.getReceiver().getCustomerReceiveNumber();
					if (sTemp != null && ! sTemp.isEmpty()) {
						customer.getReceiver().setCustomerReceiveNumber(sTemp);
					}
				}

				// we have a customer...set them as the sender
				setSender(uniqueCustomers.first().getSender());

				// now retrieve their recent recip list
				if (isBillPayment()) {
					setSender(uniqueCustomers.first().getSender());
					recentCompanies = new CompanyInfo[uniqueCustomers.size()];
					int i = 0;
					for (CustomerInfo ci : uniqueCustomers) {
						if (ci.getCompanyInfo() != null) {
							recentCompanies[i] = new CompanyInfo();
							recentCompanies[i].setAgencyName(ci.getCompanyInfo().getBillerName());
							recentCompanies[i].setAgencyCity(ci.getCompanyInfo().getBillerCity());
							recentCompanies[i].setReceiveCode(ci.getCompanyInfo().getReceiveCode());
							i++;
						}
					}
				}

				return Boolean.TRUE;
			}
		};
    	ExtraDebug.println("setCardPhoneNumber-Thread:" + Thread.currentThread().getName());
		MessageFacade.run(logic, cci, CONSUMER_HISTORY_LOOKUP, Boolean.TRUE);
	}

	public boolean getGuaranteeInd() {
		return guaranteeInd;
	}

	public void setGuaranteeInd(String flag) {
		if (flag.equals("Y")) 
			guaranteeInd = true;
		else
			guaranteeInd = false;
	}

	public void setGuaranteeInd(boolean b) {
		guaranteeInd = b;
	}

	public boolean getHomeDelivery() {
		return homeDelivery;
	}

	public void setHomeDelivery(boolean flag) {
		this.homeDelivery = flag;
	}

	public void setDestOrDOChanged(boolean flag) {
		this.isDestOrDOChanged = flag;
	}

	public boolean getDestOrDOChanged() {
		return isDestOrDOChanged;
	}

	public int getSelectedCustomerIndex() {
		return selectedCustomerIndex;
	}

	public void setSelectedCustomerIndex(int index) {
		this.selectedCustomerIndex = index;
	}

	public void setSelectedReceiverIndex(int index) {
		this.selectedRecipientIndex = index;
	}

	public int getSelectedReceiverIndex() {
		return selectedRecipientIndex;
	}

	@Override
	public void setNewCustomer(boolean newCust) {
		super.setNewCustomer(newCust);
		if (newCust) {
			this.clearSender();
		}
	}

	/**
	 * Print a receipt for this transaction.
	 *
	 * @param customerReceipt
	 *            whether or not this is for the customer.
	 * @param reprint
	 *            whether or not this is a reprinted version.
	 */
	public void printReceipt(boolean customerReceipt, boolean reprint) {

		/*
		 * Setup receipt formats
		 */
		// needed for billpay and send agent & cust receipts
//		sendInfo.setRcptLangPrime(receipt1stLanguageMnemonic);

			if (returnInfo.getCommitTransactionInfoPayload() != null && returnInfo.getCommitTransactionInfoPayload().getReceipts() != null) {
				boolean bPrimaryAvailable;
				boolean bSecondaryAvailable = false;
				boolean pdfReceiptReceived = returnInfo.getCommitTransactionInfoPayload().getReceipts().getReceiptMimeType().indexOf("/pdf") > 0;
	
				if (!customerReceipt) {
					bPrimaryAvailable = isAgentPdfPrimaryAvailable() || isAgentPrtPrimaryAvailable();
					Debug.println("printReceipt: Is Agent receipt available: " + bPrimaryAvailable);
				} else {
					bPrimaryAvailable = isCustPdfPrimaryAvailable() || isCustPrtPrimaryAvailable();
					bSecondaryAvailable = isCustPdfSecondaryAvailable() || isCustPrtSecondaryAvailable();
					Debug.println("printReceipt: Is Customer receipt 1 available: " + bPrimaryAvailable);
					Debug.println("printReceipt: Is Customer receipt 2 available: " + bSecondaryAvailable);
				}
//				ExtraDebug.println("bPrimaryAvailable: " + bPrimaryAvailable);
//				ExtraDebug.println("bSecondaryAvailable: " + bSecondaryAvailable);
	
				// pdf/prt file availability to print
				if (bPrimaryAvailable || bSecondaryAvailable) {

					if (bPrimaryAvailable) {
						// primary language receipt format
						if (!customerReceipt) {
							// agent receipt
							if (pdfReceiptReceived) {
								// received Mime Data from server
								while (pdfReceiptAgent1 != null && !pdfReceiptAgent1.print()) {
									if (cancelPrint(reprint)) {
										bPrintFailure = true;
										break;
									}
								}
							} else {
								// thermal/generic text type stored as prt file
								while (prtReceiptAgent1 != null && !prtReceiptAgent1.print()) {
									if (cancelPrint(reprint)) {
										bPrintFailure = true;
										break;
									}
								}
							}
						} else {
							// customer receipt
							if (pdfReceiptReceived) {
								// received Mime Data from server
								while (pdfReceiptCust1 != null && !pdfReceiptCust1.print()) {
									if (cancelPrint(reprint)) {
										bPrintFailure = true;
										break;
									}
								}
							} else {
								// thermal/generic text type stored as prt file
								while (prtReceiptCust1 != null && !prtReceiptCust1.print()) {
									if (cancelPrint(reprint)) {
										bPrintFailure = true;
										break;
									}
								}
							}
						}
					}
					

					if (bSecondaryAvailable && !bPrintFailure) {
						// secondary language receipt
						if (customerReceipt) {
							// customer only
							if (pdfReceiptReceived) {
								// received Mime Data from server
								while (pdfReceiptCust2 != null && !pdfReceiptCust2.print()) {
									if (cancelPrint(reprint)) {
										bPrintFailure = true;
										break;
									}
								}
							} else {
								// thermal/generic text type stored as prt file
								while (prtReceiptCust1 != null && !prtReceiptCust2.print()) {
									if (cancelPrint(reprint)) {
										bPrintFailure = true;
										break;
									}
								}
							}
						}
					}
				}
			}
	}
	
	/**
	 * Print a receipt for this transaction.
	 *
	 * @param customerReceipt
	 *            whether or not this is for the customer.
	 * @param reprint
	 *            whether or not this is a reprinted version.
	 */
	public void printConfirmationReceipt() {
//	public void printConfirmationReceipt(String name, String code, String number, String qdoId) {
		
		if ((sendValidationResponsePayload != null && sendValidationResponsePayload.getReceipts() != null)
				|| (bpValidationResponsePayload != null && bpValidationResponsePayload.getReceipts() != null)) {
			boolean bPrimaryAvailable = isDiscPdfPrimaryAvailable() || isDiscPrtPrimaryAvailable();
			boolean bSecondaryAvailable = isDiscPdfSecondaryAvailable() || isDiscPrtSecondaryAvailable();
			ExtraDebug.println("bPrimaryAvailable: " + bPrimaryAvailable);
			ExtraDebug.println("bSecondaryAvailable: " + bSecondaryAvailable);	
			
			List<List<ReceiptSegmentType>> receiptList = new ArrayList<List<ReceiptSegmentType>>();
			String mimeType = "";
			if (billPayment && bpValidationResponsePayload.getReceipts().getDisclosure1MimeData() != null) {
				receiptList.add(
						bpValidationResponsePayload.getReceipts().getDisclosure1MimeData().getReceiptMimeDataSegment());
				if (bpValidationResponsePayload.getReceipts().getDisclosure2MimeData() != null) {
					receiptList.add(bpValidationResponsePayload.getReceipts().getDisclosure2MimeData()
							.getReceiptMimeDataSegment());
				}
				mimeType = bpValidationResponsePayload.getReceipts().getReceiptMimeType();
			} else if (sendValidationResponsePayload != null && sendValidationResponsePayload.getReceipts() != null
					&& sendValidationResponsePayload.getReceipts().getDisclosure1MimeData() != null) {
				receiptList.add(sendValidationResponsePayload.getReceipts().getDisclosure1MimeData()
						.getReceiptMimeDataSegment());
				if (sendValidationResponsePayload.getReceipts().getDisclosure2MimeData() != null) {
					receiptList.add(sendValidationResponsePayload.getReceipts().getDisclosure2MimeData()
							.getReceiptMimeDataSegment());
				}
				mimeType = sendValidationResponsePayload.getReceipts().getReceiptMimeType();
			}
			
			boolean pdfReceiptReceived = mimeType.indexOf("/pdf") > 0;
			if (receiptList != null && receiptList.size() > 0) {

				if (bPrimaryAvailable || bSecondaryAvailable) {
					if (bPrimaryAvailable) {
						// disclosure receipt in primary language
						if (pdfReceiptReceived) {
							if (pdfReceiptDisc1 != null)
								pdfReceiptDisc1.print();
						} else {
							if (prtReceiptDisc1 != null)
								prtReceiptDisc1.print();
						}
					}
					if (bSecondaryAvailable) {
						// another disclosure receipt in the secondary language
						if (pdfReceiptReceived) {
							if (pdfReceiptDisc2 != null)
								pdfReceiptDisc2.print();
						} else {
							if (prtReceiptDisc2 != null)
								prtReceiptDisc2.print();
						}
					}
				}
			}
		}
	}

	/**
	 * Query the host for a list of the last few companies this customer has
	 * sent payments to. If there already is a list, just return it instead of
	 * querying for a new one. List will be cleared when a new sender is set or
	 * the transaction is cleared.
	 *
	 * @return an array of the companies that were recently sent to.
	 */
	public CompanyInfo[] getRecentCompanies() {
		return recentCompanies;
	}
	
	public boolean hasBillerData() {
		return hasBillerData;
	}

	/**
	 * Search for all billers that match the given search criteria.
	 *
	 * @param searchType
	 *            valid types (CODE, NAME, IND, ID).
	 * @param productVar
	 *            the product variant that you are performing your search. (EP,
	 *            PREPAY, UBP)
	 * @param indId
	 *            the industry identification.
	 * @param recCode
	 *            the receive code of the biller
	 * @param billerName
	 *            the name of the biller
	 * @param billerId
	 *            the biller identification for the biller.
	 * @return whether or not we found any matching billers.
	 */
	public void searchForBillers(final BillerSearchType searchType, final ProductVariantType prodVar,
			final String indId, final String recCode, final String billerName,
			final String billerId, final String bin,
			final CommCompleteInterface cci) {

		final MessageFacadeParam parameters = new MessageFacadeParam(
				MessageFacadeParam.REAL_TIME);

		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {
				matchingBillers = new BillerInfo[0];
				billerGroupingNeeded = false;
				matchingBillers = MessageFacade.getInstance().getBillersList(
						parameters, searchType, prodVar, indId, recCode,
						billerName, billerId, bin,
						defaultInformationalFee,
						isDefaultMaxFee);

				if (matchingBillers != null && matchingBillers.length > 0) {
					ArrayList<String> groupNameList = new ArrayList<String>();
					// Determine if grouping is needed by looking at result set
					if (searchType.equals(BillerSearchType.IND)) {
						groupNameList = createGroupNameList(matchingBillers);
						billerGroupingNeeded = determineGroupingNeeds(
								groupNameList, matchingBillers);
					}
					// If grouping needed, sort by dma, group, and national
					// biller
					if (billerGroupingNeeded) {
						// ArrayList dmaBillers = new ArrayList();
						ArrayList<BillerInfo> groupBillers = new ArrayList<BillerInfo>();
						// ArrayList nationalBillers = new ArrayList();
						ArrayList<BillerInfo> nonGroupBillers = new ArrayList<BillerInfo>();
						for (int i = 0; i < matchingBillers.length; i++) {
							if (matchingBillers[i].getBillerGroupName().trim().length() > 0) {
								int count = 0;
								for (int i2 = 0; i2 < groupNameList.size(); i2++) {
									if ((groupNameList.get(i2)).equals(matchingBillers[i].getBillerGroupName()))
										count++;
								}
								if (count > 1) {
									billerDisplayNameMap.put(matchingBillers[i], matchingBillers[i].getBillerGroupName());
									groupBillers.add(matchingBillers[i]);
									continue;
								}
							}
							matchingBillers[i].setBillerGroupName("");
							billerDisplayNameMap.put(matchingBillers[i], matchingBillers[i].getBillerName());
							nonGroupBillers.add(matchingBillers[i]);
						}

						ArrayList<BillerInfo> allBillers = new ArrayList<BillerInfo>();
						for (int i = 0; i < groupBillers.size(); i++) {
							allBillers.add(groupBillers.get(i));
						}
						for (int i = 0; i < nonGroupBillers.size(); i++) {
							allBillers.add(nonGroupBillers.get(i));
						}

						BillerSearchComparator b1 = new BillerSearchComparator();
						Collections.sort(allBillers, b1);

						BillerInfo[] bi = new BillerInfo[allBillers.size()];
						// int biIndex = 0;
						for (int i = 0; i < allBillers.size(); i++) {
							bi[i] = allBillers.get(i);
						}

						rollupBillers(bi);
					} else {
						// If grouping not needed, sort by biller name
						BillerSearchComparator b = new BillerSearchComparator();
						ArrayList<BillerInfo> allBillers = new ArrayList<BillerInfo>();
						for (int i = 0; i < matchingBillers.length; i++) {
							allBillers.add(matchingBillers[i]);
						}
						Collections.sort(allBillers, b);
						BillerInfo[] bi = new BillerInfo[allBillers.size()];
						for (int i = 0; i < allBillers.size(); i++) {
							BillerInfo bi2 = allBillers.get(i);
							bi2.setBillerGroupName("");
							bi[i] = bi2;
						}
						rollupBillers(bi);
					}
					hasBillerData = true;
					return Boolean.TRUE;

				} else {
					matchingBillers = new BillerInfo[0];
					hasBillerData = false;
					return Boolean.FALSE;
				}
			}
		};
		// MessageFacade.run(logic, cci, BILLER_SEARCH, Boolean.TRUE);
		MessageFacade.run(logic, cci, BILLER_SEARCH, Boolean.FALSE);
	}

	/**
	 * Comparator used to sort biller inforamtion
	 */

	private class BillerSearchComparator implements Comparator<BillerInfo> {

		private BillerSearchComparator() {
		}

		@Override
		public int compare(BillerInfo biller1, BillerInfo biller2) {
			int result = 0;
			
			if (biller1 == null || biller2 == null) {
				return 0;
			}
			
			result = compareString(billerDisplayNameMap.get(biller1), billerDisplayNameMap.get(biller2));
			if (result == 0) {
				result = compareString(biller1.getBillerName(), biller2.getBillerName());
			}
			return result;
		}
		private int compareString(String obj1, String obj2) {
			if (obj1 == null) {
				return -1;
			} else if (obj2 == null) {
				return 1;
			} else {
				return obj1.compareTo(obj2);
			}
			
//			if (obj1 == null && obj2 == null) {
//				return -1;
//			}else if (obj1 == null && obj2 != null) {
//				return -1;
//			} else if (obj1 != null && obj2 == null) {
//				return 1;
//			} else {
//				return obj1.compareTo(obj2);
//			}
		}
	}

	/**
	 * createGroupNameList() used to populate an ArrayList with all the biller
	 * group names that were returned from the search.
	 */

	private ArrayList<String> createGroupNameList(BillerInfo[] billers) {

		ArrayList<String> groupNameList = new ArrayList<String>();
		for (int i = 0; i < billers.length; i++) {
			if (billers[i].getBillerGroupName().trim().length() > 0)
				groupNameList.add(billers[i].getBillerGroupName());
		}

		return groupNameList;
	}

	/**
	 * Determine if biller grouping is needed. If a group name is used for more
	 * than 1 biller.....grouping will be required.
	 */

	private boolean determineGroupingNeeds(ArrayList<String> groupNameList,
			BillerInfo[] billers) {

		int count = 0;
		for (int i = 0; i < billers.length; i++) {
			for (int i2 = 0; i2 < groupNameList.size(); i2++) {
				if ((groupNameList.get(i2)).equals(matchingBillers[i]
						.getBillerGroupName()))
					count++;
			}
			if (count > 1)
				return true;
			else
				count = 0;
		}
		return false;
	}

	/**
	 * Creat biller name list. This is used for display purposes in the screens.
	 */

	private void rollupBillers(BillerInfo[] billers) {

		billerNameList = new ArrayList<ArrayList<BillerInfo>>();
		String lastBillerGroupName = null;
		String billerGroupName = null;
		billerList = new ArrayList<BillerInfo>();
		boolean newGroupSeq = true;
		billerGroupingNeeded = false;
		billerListIndex = 0;
		billerNameListIndex = 0;

		if (billers == null)
			return;

		for (int i = 0; i < billers.length; i++) {
			billerGroupName = billers[i].getBillerGroupName().trim();
			if (billerGroupName.length() > 0) {
				if (newGroupSeq) {
					newGroupSeq = !newGroupSeq;
					lastBillerGroupName = billerGroupName;
				}
				if (billerGroupName.equals(lastBillerGroupName)) {
					billerList.add(billerListIndex, billers[i]);
				} else {
					addToBillerNameList(billerNameListIndex);
					billerList.add(billerListIndex, billers[i]);
					lastBillerGroupName = billerGroupName;
				}
				billerListIndex++;
			} else {
				if (billerList.size() > 0)
					addToBillerNameList(billerNameListIndex);

				billerList.add(billerListIndex, billers[i]);
				addToBillerNameList(billerNameListIndex);
				newGroupSeq = true;
			}

		}
		if (billerList.size() > 0)
			addToBillerNameList(billerNameListIndex);
	}

	private void addToBillerNameList(int index) {
		if (billerList.size() > 1)
			billerGroupingNeeded = true;
		billerNameList.add(index, billerList);
		billerNameListIndex++;
		billerListIndex = 0;
		billerList = new ArrayList<BillerInfo>();
	}

	public List<BillerInfo> getSelectedBillerInfo(int selectedRow) {
//		// int selectedRow = table.getSelectedRow();
//		int rowSize = billerNameList.get(selectedRow).size();
//		ArrayList<BillerInfo> selectedBillerList = billerNameList.get(selectedRow);
//		BillerInfo[] billerInfo = new BillerInfo[rowSize];
//		for (int i = 0; i < selectedBillerList.size(); i++) {
//			billerInfo[i] = (selectedBillerList.get(i));
//		}
//		return billerInfo;
		return billerNameList.get(selectedRow);
	}

	public List<BillerInfo> getFullBillerInfoList() {

		ArrayList<BillerInfo> allBillers = new ArrayList<BillerInfo>();
		int index = 0;
		for (int i = 0; i < billerNameList.size(); i++) {
			ArrayList<BillerInfo> billerList = billerNameList.get(i);
			for (int a = 0; a < billerList.size(); a++) {
				allBillers.add(index, (billerList.get(a)));
				index++;
			}
		}
		
		return allBillers;
	}

	/*
	 * Filter out all billers that don't match the supplied Class Of Trade and
	 * if expedited billers are present and it is a possible expedited trx, then
	 * filter out the non-expedited billers (non-expedited filter is no longer
	 * done).
	 */
	public List<BillerInfo> filterPpcBillerInfoList(String classOfTrade) {

		/*
		 * Filter out billers that have the wrong Class Of Trade
		 */
		for (int ii = billerNameList.size() - 1; ii >= 0; ii--) {
			ArrayList<BillerInfo> billerList = (billerNameList.get(ii));
			for (int aa = billerList.size() - 1; aa >= 0; aa--) {
				BillerInfo bi = billerList.get(aa);
				if (bi.getClassOfTradeCode().compareTo(classOfTrade) != 0) {
					billerList.remove(aa);
				}
			}
			if (billerList.size() == 0) {
				billerNameList.remove(ii);
			}
		}
		return getFullBillerInfoList();
	}

	public String getValidReceiveCurrency(FeeInfo feeInfo) {
		String sRetValue;
		boolean isPresent = feeInfo.getReceiveAmounts() != null;
		if (isPresent) {
			sRetValue = feeInfo.getReceiveAmounts().isValidCurrencyIndicator()
					? feeInfo.getReceiveAmounts().getReceiveCurrency()
					: feeInfo.getReceiveAmounts().getDisplayPayoutCurrency();
		} 
		else { 
			// At this point,  feeInfo.getReceiveAmounts() is null  
			// validReceiveCurrency is good enough to use
			sRetValue = feeInfo.getValidReceiveCurrency();
		}

		/*
		 * Default to USD if a receive currency was not found
		 */
		if (DWValues.nonEmpty(sRetValue)) {
			return sRetValue;
		} else {
			return "USD";
		}
	}
	
	private void addKeyPairValue(ObjectFactory objectFactory, BPValidationRequest.FieldValues values, FieldKey key) {
		String value = MoneyGramSendTransaction.this.getDataCollectionData().getCurrentValue(key.getInfoKey());
		if ((value != null) && (! value.isEmpty())) {
			KeyValuePairType pair = new KeyValuePairType();
			values.getFieldValue().add(pair);
			pair.setInfoKey(key.getInfoKey());
			pair.setValue(objectFactory.createKeyValuePairTypeValue(value));
		}
	}

	public void bpValidation(final FlowPage callingPage, final ValidationType validationType) {
		
		final MessageFacadeParam parameters = new MessageFacadeParam(
				MessageFacadeParam.REAL_TIME,
				MessageFacadeParam.USER_CANCEL
			);

		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {

				Boolean returnStatus = Boolean.TRUE;
				setStatus(ClientTransaction.IN_PROCESS);

				try {

					boolean isInitial = (validationType.equals(ValidationType.INITIAL_EXPEDITIED_LOAD) || validationType.equals(ValidationType.INITIAL_NON_FORM_FREE) || validationType.equals(ValidationType.INITIAL_FORM_FREE)); 
//					if (isInitial) {
//						dataCollectionData = new DataCollectionSet(DataCollectionType.VALIDATION);
//					}

					// Get the receipt languages - static primary language & dynamic secondary language selection
			        getDataCollectionData().setFieldValue(FieldKey.MF_VALID_LANG_RCPT_PRIME_KEY, DWValues.sConvertLang3ToLang5(getPrimaryReceiptSelectedLanguage()));
			        getDataCollectionData().setFieldValue(FieldKey.MF_VALID_LANG_RCPT_SECOND_KEY, DWValues.sConvertLang3ToLang5(getSecondaryReceiptSelectedLanguage()));

					//Delete receipt info for any previous transactions
					PdfReceipt.deletePdfFiles();
					PrtReceipt.deletePrtFiles();
					pdfReceiptDisc1 = null;
					pdfReceiptDisc2 = null;
					prtReceiptDisc1 = null;
					prtReceiptDisc2 = null;

					BPValidationRequest request = new BPValidationRequest();
					request.setMgiSessionID(mgiTransactionSessionId);
					setValidationRequest(request);
					
					if ((validationType.equals(ValidationType.INITIAL_EXPEDITIED_LOAD)) || (! isFormFreeTransaction())) {
						request.setSendAmount(sendInfo.getAmount());
						request.setSendCurrency(sendInfo.getSendCurrency());
						request.setFeeAmount(getDetailSendItemAmt(feeInfo.getSendAmounts(), 
								DWValues.MF_SEND_DETAIL_TOTAL_SEND_FEE_TAX));
						request.setReceiveCurrency(sendInfo.getReceiveCurrency());
						request.setDestinationCountry(sendInfo.getDestinationCountry());
						request.setProductVariant(getBillerInfo().getProductVariant());
						String receiveAgentId = MoneyGramSendTransaction.this.getBillerInfo().getReceiveAgentID();
						request.setReceiveAgentID(receiveAgentId);
					} else {
						request.setSendAmount(transactionLookupResponsePayload.getSendAmounts().getSendAmount());
						request.setSendCurrency(transactionLookupResponsePayload.getSendAmounts().getSendCurrency());
						request.setFeeAmount(getDetailSendItemAmt(transactionLookupResponsePayload.getSendAmounts(), 
								DWValues.MF_SEND_DETAIL_TOTAL_SEND_FEE_TAX));
						request.setReceiveCurrency(transactionLookupResponsePayload.getReceiveAmounts().getReceiveCurrency());
						request.setDestinationCountry(dataCollectionData.getCurrentValue(FieldKey.DESTINATION_COUNTRY_KEY.getInfoKey()));
						String pv = dataCollectionData.getCurrentValue(FieldKey.PRODUCTVARIANT_KEY.getInfoKey());
						ProductVariantType type = ! pv.isEmpty() ? ProductVariantType.fromValue(pv) : null;
						request.setProductVariant(type);
						request.setReceiveAgentID(dataCollectionData.getCurrentValue(FieldKey.RECEIVE_AGENTID_KEY.getInfoKey()));
					}
					
					// Add accountNumberRetryCount to the request.
					
	        		ObjectFactory objectFactory = new ObjectFactory();
					BPValidationRequest.FieldValues values = new BPValidationRequest.FieldValues();
	        		request.setFieldValues(values);
	        		
	        		if (! isInitial) {
		    			KeyValuePairType pair = new KeyValuePairType();
		    			values.getFieldValue().add(pair);
		    			pair.setInfoKey(FieldKey.ACCOUNTNUMBERRETRYCOUNT_KEY.getInfoKey());
		    			pair.setValue(objectFactory.createKeyValuePairTypeValue("1"));
	        		}

					// Add the card swipe flag value to the request as a pair value if the bill payment type is PREPAY.
					
					if (isPrepayType()) {

						KeyValuePairType pair = new KeyValuePairType();
		        		values.getFieldValue().add(pair);
		    			pair.setInfoKey(FieldKey.CARD_SWIPEDFLAG_KEY.getInfoKey());
						Boolean value = MoneyGramSendTransaction.this.getBankCardSwiped();
						pair.setValue(objectFactory.createKeyValuePairTypeValue(value.toString()));

						String accountNumber = getBpAccountNumber();
						if ((accountNumber != null) && (! accountNumber.isEmpty())) {
			        		pair = new KeyValuePairType();
			        		values.getFieldValue().add(pair);
			    			pair.setInfoKey(FieldKey.BILLER_ACCOUNTNUMBER_KEY.getInfoKey());
							pair.setValue(objectFactory.createKeyValuePairTypeValue(accountNumber));
						}
						
						request.setReceiveCode(billerInfo.getReceiveCode());
						addKeyPairValue(objectFactory, values, FieldKey.CARD_EXPIRATION_MONTH_KEY);
						addKeyPairValue(objectFactory, values, FieldKey.CARD_EXPIRATION_YEAR_KEY);

						if (getBankCardSwiped() && (! isTempCard()) && (getBillerInfo().isExpeditedEligibleFlag())) {
							addKeyPairValue(objectFactory, values, FieldKey.SENDER_FIRSTNAME_KEY);
							addKeyPairValue(objectFactory, values, FieldKey.SENDER_MIDDLENAME_KEY);
							addKeyPairValue(objectFactory, values, FieldKey.SENDER_LASTNAME_KEY);
						}
					} else {
			        	// Add the MGI Reward number to the request.

			        	if ((isUsingCard()) && (cardPhoneNumber != null) && (! cardPhoneNumber.isEmpty())) {
			        		KeyValuePairType pair = new KeyValuePairType();
			        		values.getFieldValue().add(pair);
			    			pair.setInfoKey(FieldKey.MGIREWARDSNUMBER_KEY.getInfoKey());
							pair.setValue(objectFactory.createKeyValuePairTypeValue(cardPhoneNumber));
			        	}
					}
					
					// Add transactionLookup currentValues to request if this is a form free transaction and is the initial validcation.
					
					if (isFormFreeTransaction() && (transactionLookupResponsePayload.getCurrentValues().getCurrentValue() != null)) {
//						BPValidationRequest.FieldValues values = request.getFieldValues();
//						if (values == null) {
//							values = new BPValidationRequest.FieldValues();
//			        		request.setFieldValues(values);
//						}
						
						for (KeyValuePairType value : transactionLookupResponsePayload.getCurrentValues().getCurrentValue()) {
							values.getFieldValue().add(value);
						}
					}

					int rule = MoneyGramSendTransaction.this.isFormFreeTransaction() ? AgentConnect.AUTOMATIC : AgentConnect.ALWAYS_LIVE;
					bpValidationResponse = MessageFacade.getInstance().bpValidation(callingPage, parameters, request, dataCollectionData, rule, getReferenceNumber(), isInitial, true);
					if (bpValidationResponse == null) {
						bpValidationResponsePayload = null;
						dataCollectionData.setValidationStatus(DataCollectionStatus.ERROR);
						returnInfo = null;
					} else {
						bpValidationResponsePayload = bpValidationResponse.getPayload() != null ? bpValidationResponse.getPayload().getValue() : null;
						List<InfoBase> fieldsToCollect = ((bpValidationResponsePayload != null) && (bpValidationResponsePayload.getFieldsToCollect() != null)) ? bpValidationResponsePayload.getFieldsToCollect().getFieldToCollect() : null;
						List<BusinessError> errors = (bpValidationResponse.getErrors() != null) ? bpValidationResponse.getErrors().getError() : null;
						boolean isReadyForCommit = bpValidationResponsePayload != null ? bpValidationResponsePayload.isReadyForCommit() : false;
						processValidationResponse(validationType, fieldsToCollect, errors, isReadyForCommit, DataCollectionSet.BP_BASE_EXCLUDE);
						returnInfo.setBPValidationResponsePayload(bpValidationResponsePayload);
						
						returnStatus = Boolean.TRUE;
						getDataCollectionData().setAfterValidationAttempt(validationType.equals(ValidationType.SECONDARY));
						
						// If this is a form free transaction, check for identification fields.
						
						if (isFormFreeTransaction()) {
							boolean b = FormFreeIdVerificationWizardPage.showSenderScreen(getDataCollectionData());
							setFormFreeIdFields(b);
						}

						if (bpValidationResponsePayload != null) {

							// Save the new mgiSessionId value.

							setMgiTransactionSessionId(bpValidationResponsePayload.getMgiSessionID());

							if (bpValidationResponsePayload.getReceipts() != null && bpValidationResponsePayload.getReceipts().getReceiptMimeType() != null) {
								try {
									if (bpValidationResponsePayload.getReceipts().getReceiptMimeType().indexOf("/pdf") > 0) {
										if (bpValidationResponsePayload.getReceipts().getDisclosure1MimeData() != null) {
											pdfReceiptDisc1 = new PdfReceipt(bpValidationResponsePayload.getReceipts().getDisclosure1MimeData().getReceiptMimeDataSegment(), PdfReceipt.PDF_DISC_1);
											if (bpValidationResponsePayload.getReceipts().getDisclosure2MimeData() != null) {
												pdfReceiptDisc2 = new PdfReceipt(bpValidationResponsePayload.getReceipts().getDisclosure2MimeData().getReceiptMimeDataSegment(), PdfReceipt.PDF_DISC_2);
											}
										}
									} else {
										if (bpValidationResponsePayload.getReceipts().getDisclosure1MimeData() != null) {
											prtReceiptDisc1 = new PrtReceipt(bpValidationResponsePayload.getReceipts().getDisclosure1MimeData().getReceiptMimeDataSegment(), PrtReceipt.PRT_DISC_1);
											if (bpValidationResponsePayload.getReceipts().getDisclosure2MimeData() != null) {
												prtReceiptDisc2 = new PrtReceipt(bpValidationResponsePayload.getReceipts().getDisclosure2MimeData().getReceiptMimeDataSegment(), PrtReceipt.PRT_DISC_2);
											}
										}
									}
								} catch (IOException e) {
									Debug.printException(e);
								}
							}
						}
					}
					
				} catch (MessageFacadeError mfe) {
					dataCollectionData.setValidationStatus(DataCollectionStatus.ERROR);

					if (mfe.hasErrorCode(MessageFacadeParam.USER_CANCEL)) {
						setStatus(ClientTransaction.FAILED);
						returnStatus = Boolean.FALSE;
					} else {
						setStatus(ClientTransaction.CANCELED);
		            	returnStatus = Boolean.FALSE;
					}
				}
				return returnStatus;
			}
		};
		MessageFacade.run(logic, callingPage, BP_VALIDATE, Boolean.FALSE);
	}
	
	public void sendValidation(final MoneyGramSendFlowPage callingPage, BigDecimal amount, String destinationCountry, String deliveryOption, String receiveAgentId, ValidationType validationType) {
		sendValidation(callingPage, amount, destinationCountry, deliveryOption, this.getSendCurrency(), getReceiveCurrency(), receiveAgentId, validationType);
	}
	
	public void sendValidation(final FlowPage callingPage, ValidationType validationType) {
		
		String serviceOption = null;
		BigDecimal sendAmount = null;
		String sendCurrency = null;
		String destinationCountry = null;
		String receiveCurrency = null;
		String receiveAgentId = null;
		if (! this.isFormFreeTransaction()) {
			serviceOption = this.feeInfo.getServiceOption();
			sendAmount = this.feeInfo.getSendAmounts().getSendAmount();
			sendCurrency = this.feeInfo.getSendAmounts().getSendCurrency();
			destinationCountry = this.feeInfo.getDestinationCountry();
			receiveCurrency = this.feeInfo.getReceiveAmounts().isValidCurrencyIndicator()
					? this.feeInfo.getReceiveAmounts().getReceiveCurrency()
					: this.feeInfo.getReceiveAmounts().getDisplayPayoutCurrency();
			receiveAgentId = this.feeInfo.getReceiveAgentID();
		} else {
			serviceOption = getDataCollectionData().getCurrentValue(FieldKey.SERVICEOPTION_KEY.getInfoKey());
			sendAmount = transactionLookupResponsePayload.getSendAmounts().getSendAmount();
			sendCurrency = transactionLookupResponsePayload.getSendAmounts().getSendCurrency();
			destinationCountry = getFormFreeTransaction().getDestinationCountry();
			receiveCurrency = transactionLookupResponsePayload.getReceiveAmounts().getReceiveCurrency();
			receiveAgentId = getDataCollectionData().getCurrentValue(FieldKey.RECEIVE_AGENTID_KEY.getInfoKey());
		}
		sendValidation(callingPage, sendAmount, destinationCountry, serviceOption, sendCurrency, receiveCurrency, receiveAgentId, validationType);
	}

	public void sendValidation(final FlowPage callingPage, final BigDecimal amount, final String destinationCountry, final String deliveryOption, final String sendCurrency, final String receiveCurrency, final String receiveAgentId, final ValidationType validationType) {
		this.setAmount(amount);
		this.setSendCurrency(sendCurrency);
		this.setReceiveCurrency(receiveCurrency);
		
		final MessageFacadeParam parameters = new MessageFacadeParam(
				MessageFacadeParam.REAL_TIME,
				MessageFacadeParam.USER_CANCEL,
				MessageFacadeParam.MG_FEE_RATE_CHANGED
			);
		
		if (validationType.equals(ValidationType.INITIAL_NON_FORM_FREE)) {
			Map<String, String> currentValuesMap = null;
			if (dataCollectionData.getDataCollectionType().equals(DataCollectionType.VALIDATION)) {
				currentValuesMap = dataCollectionData.getCurrentValueMap();
			}
			dataCollectionData = new DataCollectionSet(DataCollectionType.VALIDATION);
			if (currentValuesMap != null) {
				dataCollectionData.setCurrentValuesMap(currentValuesMap);
			}
		}

		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {

				Boolean returnStatus = Boolean.TRUE;
				setStatus(ClientTransaction.IN_PROCESS);

				try {
					
					//Delete receipt info for any previous transactions
					
					PdfReceipt.deletePdfFiles();
					PrtReceipt.deletePrtFiles();
					pdfReceiptDisc1 = null;
					pdfReceiptDisc2 = null;
					prtReceiptDisc1 = null;
					prtReceiptDisc2 = null;

					// Make the AC call to validate the send transaction.

					if (validationType.equals(ValidationType.INITIAL_NON_FORM_FREE) || validationType.equals(ValidationType.INITIAL_FORM_FREE)) {
						dataCollectionData.clear();
					}
		        	getDataCollectionData().setFieldValue(FieldKey.MF_VALID_LANG_RCPT_PRIME_KEY, DWValues.sConvertLang3ToLang5(getPrimaryReceiptSelectedLanguage()));
		        	getDataCollectionData().setFieldValue(FieldKey.MF_VALID_LANG_RCPT_SECOND_KEY, DWValues.sConvertLang3ToLang5(getSecondaryReceiptSelectedLanguage()));

		        	sendValidationRequest = new SendValidationRequest();
		        	sendValidationRequest.setMgiSessionID(mgiTransactionSessionId);
		        	sendValidationRequest.setSendAmount(amount);
		        	sendValidationRequest.setSendCurrency(sendCurrency);
		        	sendValidationRequest.setDestinationCountry(destinationCountry);
		        	sendValidationRequest.setServiceOption(deliveryOption);
		        	sendValidationRequest.setReceiveCurrency(receiveCurrency);
		        	if ((receiveAgentId != null) && (! receiveAgentId.isEmpty())) {
		        		sendValidationRequest.setReceiveAgentID(receiveAgentId);
		        	}

					ObjectFactory objectFactory = new ObjectFactory();
					boolean isInitial = (validationType.equals(ValidationType.INITIAL_NON_FORM_FREE) || validationType.equals(ValidationType.INITIAL_FORM_FREE)); 

					// Add the consumer profile id and consumer profile type to the request.
					
					if (getProfileObject() != null) {
						
		        		SendValidationRequest.FieldValues values = sendValidationRequest.getFieldValues();
		        		if (values == null) {
			        		values = new SendValidationRequest.FieldValues();
			        		sendValidationRequest.setFieldValues(values);
		        		}
		        		
		        		KeyValuePairType pair = new KeyValuePairType();
		        		values.getFieldValue().add(pair);
		    			pair.setInfoKey("sender_ProfileID");
						pair.setValue(objectFactory.createKeyValuePairTypeValue(getProfileObject().getProfileId()));
						
		        		pair = new KeyValuePairType();
		        		values.getFieldValue().add(pair);
		    			pair.setInfoKey("sender_ProfileID_Type");
						pair.setValue(objectFactory.createKeyValuePairTypeValue(getProfileObject().getProfileType()));
					}
		        	
		        	// Add any valid promo codes to the request.

					List<PromotionInfo> promoList = null;
					if (! isFormFreeTransaction()) {
			        	promoList = ((feeInfo != null) && (feeInfo.getPromotionInfos() != null)) ? feeInfo.getPromotionInfos().getPromotionInfo() : null;
					} else {
						promoList = getTransactionLookupResponsePayload() != null && getTransactionLookupResponsePayload().getPromotionInfos() != null ? getTransactionLookupResponsePayload().getPromotionInfos().getPromotionInfo() : null;
					}
		        	if ((promoList != null) && (! promoList.isEmpty())) {
			        	SendValidationRequest.PromoCodes promoCodes = new SendValidationRequest.PromoCodes();
		        		for (PromotionInfo promocode : promoList) {
		        			if (promocode.getPromotionErrorCode() == null) {
		        				promoCodes.getPromoCode().add(promocode.getPromotionCode());
		        			}
		        		}
		        		if (! promoCodes.getPromoCode().isEmpty()) {
		        			sendValidationRequest.setPromoCodes(promoCodes);
		        		}
		        	}
		        	
		        	// Add the receipt language to the request.
		        		
		        	sendValidationRequest.setPrimaryReceiptLanguage((String) getDataCollectionData().getFieldValue(FieldKey.MF_VALID_LANG_RCPT_PRIME_KEY));
		        	String secondaryLanguage = (String) getDataCollectionData().getFieldValue(FieldKey.MF_VALID_LANG_RCPT_SECOND_KEY);
		        	if (! StringUtility.getNonNullString(secondaryLanguage).isEmpty()) {
		            	sendValidationRequest.setSecondaryReceiptLanguage(secondaryLanguage);
		        	}
		        	
		        	// Add the MoneyGram directed send registration number to the request if exists.
		        	
		        	String customerReceiveNumber = null;
		        	if ((getProfileObject() != null) && (getProfileObject().getCurrentReceiver() != null)) {
		        		customerReceiveNumber = getProfileObject().getCurrentReceiver().getValues().get(FieldKey.CUSTOMERRECEIVENUMBER_KEY.getInfoKey());
		        		
		        		String feeReceiveAgentId = MoneyGramSendTransaction.this.feeInfo.getReceiveAgentID();
		        		String receiverReceiveAgentId = getProfileObject().getCurrentReceiver().getValues().get(FieldKey.RECEIVE_AGENTID_KEY.getInfoKey());
		        		
		        		boolean isSameReceiveAgentID = ((feeReceiveAgentId != null) && (receiverReceiveAgentId != null) && (feeReceiveAgentId.equals(receiverReceiveAgentId)));
		        		if (! isSameReceiveAgentID) {
		        			customerReceiveNumber = null;
		        		}
		        	} else if (isFormFreeTransaction()) {
		        		customerReceiveNumber = getDataCollectionData().getCurrentValue(FieldKey.CUSTOMERRECEIVENUMBER_KEY.getInfoKey());
		        	}
		        		
		        	if ((customerReceiveNumber != null) && (! customerReceiveNumber.isEmpty())) {
						
		        		SendValidationRequest.FieldValues values = sendValidationRequest.getFieldValues();
		        		if (values == null) {
			        		values = new SendValidationRequest.FieldValues();
			        		sendValidationRequest.setFieldValues(values);
		        		}
		        		
		        		KeyValuePairType pair = new KeyValuePairType();
		        		values.getFieldValue().add(pair);
		    			pair.setInfoKey(FieldKey.CUSTOMERRECEIVENUMBER_KEY.getInfoKey());
						pair.setValue(objectFactory.createKeyValuePairTypeValue(customerReceiveNumber));
		        	}
		        	
		        	// Add the destination country subdivision to the request.
		        	
		        	if (getDestinationState() != null) {
		        		SendValidationRequest.FieldValues values = sendValidationRequest.getFieldValues();
		        		if (values == null) {
			        		values = new SendValidationRequest.FieldValues();
			        		sendValidationRequest.setFieldValues(values);
		        		}
		        		
		        		KeyValuePairType pair = new KeyValuePairType();
		        		values.getFieldValue().add(pair);
		    			pair.setInfoKey(FieldKey.DESTINATION_COUNTRY_SUBDIVISIONCODE_KEY.getInfoKey());
						pair.setValue(objectFactory.createKeyValuePairTypeValue(getDestinationState().getCountrySubdivisionCode()));
		        	}
		        	
		        	// Add the MGI Reward number to the request.

		        	if ((isUsingCard()) && (cardPhoneNumber != null) && (! cardPhoneNumber.isEmpty())) {
		        		SendValidationRequest.FieldValues values = sendValidationRequest.getFieldValues();
		        		if (values == null) {
			        		values = new SendValidationRequest.FieldValues();
			        		sendValidationRequest.setFieldValues(values);
		        		}
		        		
		        		KeyValuePairType pair = new KeyValuePairType();
		        		values.getFieldValue().add(pair);
		    			pair.setInfoKey(FieldKey.MGIREWARDSNUMBER_KEY.getInfoKey());
						pair.setValue(objectFactory.createKeyValuePairTypeValue(cardPhoneNumber));
		        	}
		        	
					int rule = MoneyGramSendTransaction.this.isFormFreeTransaction() ? AgentConnect.AUTOMATIC : AgentConnect.ALWAYS_LIVE;
					sendValidationResponse = MessageFacade.getInstance().sendValidation(callingPage, parameters, sendValidationRequest, dataCollectionData, rule, getReferenceNumber(), isInitial, true);

					if (sendValidationResponse == null) {
						sendValidationResponsePayload = null;
						dataCollectionData.setValidationStatus(DataCollectionStatus.ERROR);
						returnInfo = new MoneyGramInfo40();
						returnStatus = Boolean.FALSE;

					} else {
						sendValidationResponsePayload = sendValidationResponse.getPayload() != null ? sendValidationResponse.getPayload().getValue() : null;
						
						// Process the FTC and errors returned in the response.
						
						List<InfoBase> fieldsToCollect = ((sendValidationResponsePayload != null) && (sendValidationResponsePayload.getFieldsToCollect() != null)) ? sendValidationResponsePayload.getFieldsToCollect().getFieldToCollect() : null;
						List<BusinessError> errors = sendValidationResponse.getErrors() != null ? sendValidationResponse.getErrors().getError() : null;
						boolean readyForCommit = sendValidationResponsePayload != null ? sendValidationResponsePayload.isReadyForCommit() : false;
						processValidationResponse(validationType, fieldsToCollect, errors, readyForCommit, DataCollectionSet.SEND_BASE_EXCLUDE);
						returnInfo.setSendValidationResponsePayload(sendValidationResponsePayload);
						
						getDataCollectionData().setAfterValidationAttempt(validationType.equals(ValidationType.SECONDARY));
						
						// If this is a form free transaction, check for identification fields.
						
						if (isFormFreeTransaction()) {
							boolean b = FormFreeIdVerificationWizardPage.showSenderScreen(getDataCollectionData());
							setFormFreeIdFields(b);
						}
						
						if (sendValidationResponsePayload != null) {
						
							// Promotion Information
							
							if (sendValidationResponsePayload.getPromotionInfos() != null) {
								setReturnedPromoCodeCount(sendValidationResponsePayload.getPromotionInfos().getPromotionInfo().size());
							}
	
							// If an RRN came back (registration), then save it in the senderInfo (for use by receipts)
	
							String sRRN = sendValidationResponsePayload.getCustomerReceiveNumber();
							if ((sRRN != null) && (! sRRN.isEmpty())) {
								dataCollectionData.setFieldValue(FieldKey.REGISTRATION_NUMBER_KEY, sRRN);
								dataCollectionData.setFieldValue(FieldKey.GFFP_TAG_RRN_KEY, sRRN);
								MoneyGramSendTransaction.this.setCustomerReceiveNumber(sRRN);
							}
							
							// Save the new mgiSessionId value.
							String sMgiSessionId = sendValidationResponsePayload.getMgiSessionID();
							setMgiTransactionSessionId(sMgiSessionId);
							dataCollectionData.setFieldValue(FieldKey.MGISESSIONID_KEY, sMgiSessionId);
							
							// If any receipt data came back in the response, save it so the receipts can be printed.
	
							if (sendValidationResponsePayload.getReceipts() != null
									&& sendValidationResponsePayload.getReceipts().getReceiptMimeType() != null) {
								try {
									if (sendValidationResponsePayload.getReceipts().getDisclosure1MimeData() != null) {
										if (sendValidationResponsePayload.getReceipts().getReceiptMimeType().toLowerCase(Locale.US)
												.contains("application/pdf")) {
											pdfReceiptDisc1 = new PdfReceipt(sendValidationResponsePayload.getReceipts()
													.getDisclosure1MimeData().getReceiptMimeDataSegment(),
													PdfReceipt.PDF_DISC_1);
											if (sendValidationResponsePayload.getReceipts()
													.getDisclosure2MimeData() != null) {
												pdfReceiptDisc2 = new PdfReceipt(sendValidationResponsePayload.getReceipts()
														.getDisclosure2MimeData().getReceiptMimeDataSegment(),
														PdfReceipt.PDF_DISC_2);
											}
										} else {
											prtReceiptDisc1 = new PrtReceipt(sendValidationResponsePayload.getReceipts()
													.getDisclosure1MimeData().getReceiptMimeDataSegment(),
													PrtReceipt.PRT_DISC_1);
											if (sendValidationResponsePayload.getReceipts()
													.getDisclosure2MimeData() != null) {
												prtReceiptDisc2 = new PrtReceipt(sendValidationResponsePayload.getReceipts()
														.getDisclosure2MimeData().getReceiptMimeDataSegment(),
														PrtReceipt.PRT_DISC_2);
											}
										}
									}
								} catch (IOException e) {
									Debug.printException(e);
								}
							}
						}
					} 

					// Restore Account Number

	            	if ((isDss()) && (! isInlineDSR())) {
	            		setReceiverAccountNumber(sSavedAcctNbr);
	            	}
				} catch (MessageFacadeError mfe) {
					dataCollectionData.setValidationStatus(DataCollectionStatus.ERROR);
					if (mfe.hasErrorCode(MessageFacadeParam.MG_FEE_RATE_CHANGED)) {
						int rr = Dialogs.showConfirm("dialogs/DialogRateChanged.xml");
						if (rr != Dialogs.OK_OPTION) {
							setStatus(ClientTransaction.CANCELED);
						} else {
							setStatus(ClientTransaction.NEEDS_RECOMMIT);
							dataCollectionData.setFieldValue(FieldKey.EXPECTED_RECEIVE_AMOUNT_KEY, "");
							dataCollectionData.setFieldValue(FieldKey.FEEAMOUNT_KEY, "");
						}
						returnStatus = Boolean.FALSE;

					} else if (mfe.hasErrorCode(MessageFacadeParam.USER_CANCEL)) {
						setStatus(ClientTransaction.FAILED);
						returnStatus = Boolean.FALSE;
					} else {
						setStatus(ClientTransaction.CANCELED);
						if ((isDss()) && (! isInlineDSR())) {
		            		setReceiverAccountNumber(sSavedAcctNbr);
		            	}
		            	returnStatus = Boolean.FALSE;
					}
				} catch (Exception e) {
					Debug.printException(e);
					Debug.printStackTrace(e);
	            	returnStatus = Boolean.FALSE;
				}
				return returnStatus;
			}
		};
		MessageFacade.run(logic, callingPage, SEND_VALIDATE, Boolean.FALSE);
	}
	
	@Override
	public void applyHistoryData(CustomerInfo consumrerInfo) {
		if (consumrerInfo != null) {
			List<Data> sets = new ArrayList<Data>();
			sets.add(consumrerInfo.getSender());
			sets.add(consumrerInfo.getReceiver());
			sets.add(consumrerInfo.getCompanyInfo());
			
			for (Data set : sets) {
				for (FieldKey key : set.getData().keySet()) {
					DataValue value = set.getData().get(key);
					
					if (key.getInfoKey().equals(FieldKey.BILLER_ACCOUNTNUMBER_KEY.getInfoKey())) {
						if ((value.getValue() != null) && (value.getValue().indexOf('*') >= 0)) {
							value = DataValue.value(getBpAccountNumber());
						}
					}
					
					List<DataCollectionField> fields = this.getDataCollectionData().getFieldsByInfoKey(key.getInfoKey());
					for (DataCollectionField field : fields) {
						field.setDefaultValue(value.getValue());
					}
				}
			}
		}
	}

//	@Override
//	protected void setFieldCollectionStatus() {
////		List<String> panels = new ArrayList<String>();
////		if (billPayment) {
////			if (this.isFormFreeTransaction() || (! this.getBillerInfo().getProductVariant().equals(ProductVariantType.PREPAY))) {
////				panels.addAll(this.screen25Status.getPanelNames());
////				panels.addAll(this.screen30Status.getPanelNames());
////			} else {
////				panels.addAll(this.screen36Status.getPanelNames());
////				panels.addAll(this.screen38Status.getPanelNames());
////				panels.addAll(this.screen39Status.getPanelNames());
////			}
////		} else {
////			if (! this.isDSS) {
////				panels.addAll(this.senderInformationScreenStatus.getPanelNames());
////				panels.addAll(this.senderThirdPartyScreenStatus.getPanelNames());
////				panels.addAll(this.receiverNameScreenStatus.getPanelNames());
////			} else {
////				panels.addAll(this.senderInformationScreenStatus.getPanelNames());
////				panels.addAll(this.senderThirdPartyScreenStatus.getPanelNames());
////				panels.addAll(this.receiverAccountInformation1ScreenStatus.getPanelNames());
////				panels.addAll(this.receiverAccountInformation2ScreenStatus.getPanelNames());
////			}
////		}
////		
////		for (String panel : panels) {
////			DataCollectionPanel dcp = dataCollectionData.getDataCollectionPanelMap().get(panel);
////			if (dcp != null) {
////				dcp.markFieldsCollected(DataCollectionField.BASE_SCREENS_COLLECTION);
////			}
////		}
//	}
	
	public boolean isPrepayType() {
		boolean isEP = (billerInfo != null) && (billerInfo.getProductVariant() != null) && billerInfo.getProductVariant().equals(ProductVariantType.EP);
		boolean isPREPAY = (billerInfo != null) && (billerInfo.getProductVariant() != null) && billerInfo.getProductVariant().equals(ProductVariantType.PREPAY);
		boolean isCotc51 = (billerInfo != null) && (billerInfo.getProductVariant() != null)
				&& (billerInfo.getClassOfTradeCode() != null) && billerInfo.getClassOfTradeCode().equals("51");
		
		return ((isEP && isCotc51) || isPREPAY) && (! this.isFormFreeTransaction());
	}

	private void setBpScreensToShow() {
		
		// Determine which of the data collection screens need to be shown.
		
		Set<String> panelSet = new HashSet<String>();
		if (! isPrepayType()) {
			panelSet.addAll(screen25Status.getPanelNames(this.dataCollectionData));
			panelSet.addAll(screen30Status.getPanelNames(this.dataCollectionData));
			this.screen25Status.calculateDataCollectionScreenStatus(getDataCollectionData());
			this.screen30Status.calculateDataCollectionScreenStatus(getDataCollectionData());
			this.screen36Status.setScreenStatus(ScreenStatusType.DO_NOT_DISPLAY);
			this.screen38Status.setScreenStatus(ScreenStatusType.DO_NOT_DISPLAY);
			this.screen39Status.setScreenStatus(ScreenStatusType.DO_NOT_DISPLAY);
		} else {
			panelSet.addAll(screen36Status.getPanelNames(this.dataCollectionData));
			panelSet.addAll(screen38Status.getPanelNames(this.dataCollectionData));
			panelSet.addAll(screen39Status.getPanelNames(this.dataCollectionData));
			this.screen25Status.setScreenStatus(ScreenStatusType.DO_NOT_DISPLAY);
			this.screen30Status.setScreenStatus(ScreenStatusType.DO_NOT_DISPLAY);
			this.screen36Status.calculateDataCollectionScreenStatus(getDataCollectionData());
			this.screen38Status.calculateDataCollectionScreenStatus(getDataCollectionData());
			this.screen39Status.calculateDataCollectionScreenStatus(getDataCollectionData());
		}
		
		List<DataCollectionPanel> supplementalDataCollectionPanels = getDataCollectionData().getSupplementalDataCollectionPanels(panelSet);
		this.setExcludePanelNames(panelSet);
		setSupplementalDataCollectionPanels(supplementalDataCollectionPanels);
		this.supplementalInformationScreenStatus = getDataCollectionScreenStatus(supplementalDataCollectionPanels);

		ExtraDebug.println("Screen 25 Data To Collect = " + screen25Status.getStatus().toString());
		ExtraDebug.println("Screen 30 Data To Collect = " + screen30Status.getStatus().toString());
		ExtraDebug.println("Screen 36 Data To Collect = " + screen36Status.getStatus().toString());
		ExtraDebug.println("Screen 38 Data To Collect = " + screen38Status.getStatus().toString());
		ExtraDebug.println("Screen 39 Data To Collect = " + screen39Status.getStatus().toString());
		ExtraDebug.println("Supplemental Data To Collect = " + supplementalInformationScreenStatus.getStatus().toString());
	}
	
	@Override
	public void setScreensToShow() {
		DataCollectionType dataCollectionType = this.getDataCollectionData().getDataCollectionType();
		if (dataCollectionType.equals(DataCollectionType.VALIDATION)) {
			if (! billPayment) {
				setSendScreensToShow();
			} else {
				setBpScreensToShow();
			}
		} else if (dataCollectionType.equals(DataCollectionType.PROFILE_EDIT)) {
			setProfileDataCollectionScreensToShow(DataCollectionSet.SENDER_PROFILE_SCREEN_2_PANEL_NAMES);
		}
	}
	
	private void setSendScreensToShow() {
		
		// Determine which of the data collection screens need to be shown.
		
		Set<String> panelSet = new HashSet<String>(DataCollectionSet.SEND_BASE_EXCLUDE);
		panelSet.addAll(senderInformationScreenStatus.getPanelNames(getDataCollectionData()));
		panelSet.addAll(senderThirdPartyScreenStatus.getPanelNames(getDataCollectionData()));
		if (this.isDSS) {
			panelSet.addAll(receiverAccountInformation1ScreenStatus.getPanelNames(getDataCollectionData()));
			panelSet.addAll(receiverAccountInformation2ScreenStatus.getPanelNames(getDataCollectionData()));
		} else {
			panelSet.addAll(receiverNameScreenStatus.getPanelNames(getDataCollectionData()));
		}
		this.senderInformationScreenStatus.calculateDataCollectionScreenStatus(getDataCollectionData());
		this.senderThirdPartyScreenStatus.calculateDataCollectionScreenStatus(getDataCollectionData());
		this.receiverNameScreenStatus.calculateDataCollectionScreenStatus(getDataCollectionData());
		this.receiverAccountInformation1ScreenStatus.calculateDataCollectionScreenStatus(getDataCollectionData());
		this.receiverAccountInformation2ScreenStatus.calculateDataCollectionScreenStatus(getDataCollectionData());
		
		// Supplemental Information Screen
		
		List<DataCollectionPanel> supplementalDataCollectionPanels = getDataCollectionData().getSupplementalDataCollectionPanels(panelSet);
		this.setExcludePanelNames(panelSet);
		setSupplementalDataCollectionPanels(supplementalDataCollectionPanels);
		this.supplementalInformationScreenStatus = getDataCollectionScreenStatus(supplementalDataCollectionPanels);
		
		ExtraDebug.println("Sender Information Screen Status = " + senderInformationScreenStatus.getStatus().toString());
		ExtraDebug.println("Sender Third Party Screen Status = " + senderThirdPartyScreenStatus.getStatus().toString());
		ExtraDebug.println("Receiver Information Screen Status = " + receiverNameScreenStatus.getStatus().toString());
		ExtraDebug.println("Receiver Account Information Screen 1 Status = " + receiverAccountInformation1ScreenStatus.getStatus().toString());
		ExtraDebug.println("Receiver Account Information Screen 2 Status = " + receiverAccountInformation2ScreenStatus.getStatus().toString());
		ExtraDebug.println("Receiver Supplemental Screen Status = " + supplementalInformationScreenStatus.getStatus().toString());
	}

	/**
	 * Commit this transaction. Send all the collected information to the host
	 * and handle the return.
	 *
	 * @param parentComponent
	 *            the component over which to display dialogs.
	 */

	public void completeSession(final MoneyGramSendFlowPage callingPage, final SessionType transactionType, final Request validationRequest, final String referenceNumber) {

		final MessageFacadeParam parameters = new MessageFacadeParam(
				MessageFacadeParam.REAL_TIME, MessageFacadeParam.USER_CANCEL);

		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {
				setStatus(ClientTransaction.IN_PROCESS);
				if (isPasswordRequired()) {
					String opName = userProfile.find("NAME").stringValue();
					if (opName != null && opName.length() > 7) {
						opName = userProfile.find("NAME").stringValue().substring(0, 7);
					}
					dataCollectionData.setFieldValue(FieldKey.OPERATORNAME_KEY, opName);
				}

				dataCollectionData.setFieldValue(FieldKey.DESTINATION_COUNTRY_KEY, destination.getCountryCode());  
				dataCollectionData.setFieldValue(FieldKey.DESTINATION_COUNTRY_KEY, destination.getCountryCode());
				String state = "";
				if (destinationState != null) {
					state = destinationState.getCountrySubdivisionCode();
				}
				dataCollectionData.setFieldValue(FieldKey.DESTINATION_COUNTRY_SUBDIVISIONCODE_KEY, state); 

				if (getSender().getConsumerId()==null || getSender().getConsumerId().isEmpty()) {
					getSender().setConsumerId("0");
					dataCollectionData.setFieldValue(FieldKey.SEND_CONSUMERID_KEY, "0");
				}

				boolean success = true;
				completeSessionResponse = null;

				try {
					// send message according to if we are an bill pay or not
					if (billPayment) {
						dataCollectionData.setFieldObject(FieldKey.MF_PROD_TYPE_KEY, SessionType.BP);
					} else {
						dataCollectionData.setFieldObject(FieldKey.MF_PROD_TYPE_KEY, SessionType.SEND);
					}

//					dataCollectionData.setFieldValue(FieldKey.MF_RCPT_VER_AG_PRIME_KEY, "0.00");
//					dataCollectionData.setFieldValue(FieldKey.MF_RCPT_VER_CU_PRIME_KEY, "0.00");
//
//					boolean hybridReceipt = ReceiptTemplates.getReceiptLanguageSend().indexOf("/") > 0
//							&& !receiptAllow2ndLanguageNone;
//
//					if ((hybridReceipt || receipt2ndLanguageMnemonic.length() > 0) && AgentCountryInfo.isUsAgent()) {
//						dataCollectionData.setFieldValue(FieldKey.MF_RCPT_VER_AG_SECOND_KEY, "0.00");
//						dataCollectionData.setFieldValue(FieldKey.MF_RCPT_VER_CU_SECOND_KEY, "0.00");
//					}
					/*
					 * Delete receipt info for any previous transactions
					 */
					PdfReceipt.deletePdfFiles();
					PrtReceipt.deletePrtFiles();
					pdfReceiptAgent1 = null;
					pdfReceiptCust1 = null;
					pdfReceiptCust2 = null;
					prtReceiptAgent1 = null;
					prtReceiptCust1 = null;
					prtReceiptCust2 = null;

					CompleteSessionRequest completeSessionRequest = new CompleteSessionRequest();
					completeSessionRequest.setMgiSessionID(mgiTransactionSessionId);
					completeSessionRequest.setMgiSessionType(transactionType);
					completeSessionResponse = MessageFacade.getInstance().completeSession(parameters, completeSessionRequest, validationRequest, referenceNumber);

					if (completeSessionResponse == null) {
						// the error page to show up
						// set the status to CANCELED instead.
						setStatus(ClientTransaction.CANCELED);
						return Boolean.FALSE;
					} else {
				        returnInfo.setCommitTransactionInfo(completeSessionResponse);
				        /*
				         * Add the commit response data to the validation response data
				         */

						try {
							CompletionReceiptType receipts = completeSessionResponse.getPayload().getValue().getReceipts();
							if (receipts != null) {
								if (receipts.getReceiptMimeType().indexOf("/pdf") > 0) {
									if (receipts.getAgentReceiptMimeData() != null) {
										pdfReceiptAgent1 = new PdfReceipt(receipts.getAgentReceiptMimeData().getReceiptMimeDataSegment(), PdfReceipt.PDF_AGENT_1);
									}
									if (receipts.getConsumerReceipt1MimeData() != null) {
										pdfReceiptCust1 = new PdfReceipt(receipts.getConsumerReceipt1MimeData().getReceiptMimeDataSegment(), PdfReceipt.PDF_CUST_1);
									}
									if (receipts.getConsumerReceipt2MimeData() != null) {
										pdfReceiptCust2 = new PdfReceipt(receipts.getConsumerReceipt2MimeData().getReceiptMimeDataSegment(), PdfReceipt.PDF_CUST_2);
									}
								} else {
									if (receipts.getAgentReceiptMimeData() != null) {
										prtReceiptAgent1 = new PrtReceipt(receipts.getAgentReceiptMimeData().getReceiptMimeDataSegment(), PrtReceipt.PRT_AGENT_1);
									}
									if (receipts.getConsumerReceipt1MimeData() != null) {
										prtReceiptCust1 = new PrtReceipt(receipts.getConsumerReceipt1MimeData().getReceiptMimeDataSegment(), PrtReceipt.PRT_CUST_1);
									}
									if (receipts.getConsumerReceipt2MimeData() != null) {
										prtReceiptCust2 = new PrtReceipt(receipts.getConsumerReceipt2MimeData().getReceiptMimeDataSegment(), PrtReceipt.PRT_CUST_2);
									}
								}
							}
						} catch (IOException e) {
							Debug.printException(e);
						}

						setupSendISI(ISI_RCPT);
						if (billPayment) {
							String sendCurrency = dataCollectionData.getFieldStringValue(FieldKey.SEND_CURRENCY_KEY);
							com.moneygram.agentconnect.CompleteSessionResponse.Payload payload = returnInfo.getCommitTransactionInfoPayload();
							String referenceNumber = payload != null ? payload.getReferenceNumber() : "";
							BigDecimal amount = getBpValidationResponsePayload().getSendAmounts().getSendAmount();
							dataCollectionData.setFieldValue(FieldKey.SEND_AMOUNT_KEY, amount);
							AuditLog.writeBpRecord(userID, referenceNumber, amount, getBillerInfo().getBillerName(), sendCurrency);
						} else {
							dataCollectionData.setFieldValue(FieldKey.SEND_AMOUNT_KEY,
									sendValidationResponsePayload.getSendAmounts().getSendAmount());
							AuditLog.writeMgsRecord(
									userID, 
									returnInfo.getCommitTransactionInfoPayload().getReferenceNumber(), 
									sendValidationResponsePayload.getSendAmounts().getSendAmount(), 
									sendValidationResponsePayload.getSendAmounts().getSendCurrency());
						}
						setStatus(ClientTransaction.COMPLETED);
					}
				} catch (MessageFacadeError mfe) {
					if (mfe.hasErrorCode(MessageFacadeParam.MG_FEE_RATE_CHANGED)) {
						int rr = Dialogs
								.showConfirm("dialogs/DialogRateChanged.xml");
						if (rr != Dialogs.OK_OPTION) {
							setStatus(ClientTransaction.CANCELED);
							return Boolean.FALSE;
						} else {
							setStatus(ClientTransaction.NEEDS_RECOMMIT);
							return Boolean.FALSE;
						}

					} else if (mfe.hasErrorCode(MessageFacadeParam.USER_CANCEL)) {
						setStatus(ClientTransaction.FAILED);
						// return Boolean.TRUE;
						return Boolean.FALSE;
					} else {
						setStatus(ClientTransaction.CANCELED);
						return Boolean.FALSE;
					}
				}
				// for diags and totals to be correct, set all void transactions
				// (training mode and demo mode) to have a success=false flag
				boolean isTrainingMode = UnitProfile.getInstance().isTrainingMode();
				boolean live = (!UnitProfile.getInstance().isDemoMode()) && (!isTrainingMode);

				if (!live) {
					success = false;
					if (isTrainingMode) {
					}
				}

				// add this item to the batch totals and counts, but not for
				// training mode
				if (live) {
					final int docType = billPayment ? UnitProfileInterface.DW_ID_TOTALS_MGRAM_XPAY : UnitProfileInterface.DW_ID_TOTALS_MGRAM_SEND;
					UnitProfile.getInstance().addTotalAndCount(getAmount(), 1, UnitProfileInterface.DNET_ID_APP_MGRAM, docType, !success);
					if (isBillPayment()) {
						UnitProfile.getInstance().addBillPayTotal(getAmount(), 1, UnitProfileInterface.DNET_ID_APP_MGRAM, docType, !success);
					}
				}

				// write ReportLog and
				// write out the diag saying we had a successful send
				MoneyGramSendDiagInfo mgsdi = new MoneyGramSendDiagInfo(new Date(TimeUtility.currentTimeMillis()), userID, userName, 1, success);
				mgsdi.setAmount(getAmount());
				mgsdi.setReferenceNumber(getCommitReferenceNumber());
				mgsdi.setMoneySaverNumber(getCardNumber());
				
				getDataCollectionData().setValidationStatus(DataCollectionStatus.COMPLETED);

				// leave at default for now, since MGSendDiagMsg is expecting an
				// integer code and the following code supplies a text code
				// mgsdi.setCurrency(getCurrency());

				if (billPayment) {
					mgsdi.setExchangeRate(new BigDecimal("1")); 
					ProductVariantType variant = getBillerInfo().getProductVariant();
					if (live && variant != null) {
						int type = 0;
						if (variant.equals(ProductVariantType.UBP)) {
							type = ReportLog.UTILITY_PAYMENT_TYPE;
						} else if (variant.equals(ProductVariantType.PREPAY)) {
							type = ReportLog.PREPAID_SERVICES_TYPE;
						} else if (variant.equals(ProductVariantType.EP)) {
							type = ReportLog.EXPRESS_PAYMENT_TYPE;
						} else {
							type = ReportLog.BILL_PAYMENT_TYPE;
						}
						ReportLog.logMoneyGramSend(type, dataCollectionData, mgsdi, returnInfo,billPayment);
					}

					BillPaymentDiagInfo exdi = new BillPaymentDiagInfo(new Date(TimeUtility.currentTimeMillis()), userID, userName, 1, success);
					try {
						if (! isFormFreeTransaction()) {
							exdi.setAccount(getBpAccountNumber());
							exdi.setAgencyName(getBillerInfo().getBillerName());
							exdi.setReceiveCode(getBillerInfo().getReceiveAgentID());
							exdi.setAmount(getAmount());
							exdi.setMoneySaverNumber(getCardNumber());
							exdi.setReferenceNumber(getCommitReferenceNumber());
							exdi.setSenderName(DWValues.formatName(dataCollectionData.getFieldStringValue(FieldKey.SENDER_FIRSTNAME_KEY), dataCollectionData.getFieldStringValue(FieldKey.SENDER_MIDDLENAME_KEY), dataCollectionData.getFieldStringValue(FieldKey.SENDER_LASTNAME_KEY), dataCollectionData.getFieldStringValue(FieldKey.SENDER_LASTNAME2_KEY)));
						} else {
							exdi.setAccount(getBpAccountNumber());
							exdi.setAgencyName(getDataCollectionData().getCurrentValue("receive_AgentName")); 
							exdi.setReceiveCode(getDataCollectionData().getCurrentValue("receiveAgentID")); 
							exdi.setAmount(getAmount());
							exdi.setMoneySaverNumber(getCardNumber());
							exdi.setReferenceNumber(getCommitReferenceNumber());
							exdi.setSenderName(DWValues.formatName(dataCollectionData.getFieldStringValue(FieldKey.SENDER_FIRSTNAME_KEY), dataCollectionData.getFieldStringValue(FieldKey.SENDER_MIDDLENAME_KEY), dataCollectionData.getFieldStringValue(FieldKey.SENDER_LASTNAME_KEY), dataCollectionData.getFieldStringValue(FieldKey.SENDER_LASTNAME2_KEY)));
						}
						DiagLog.getInstance().writeMGBillPayment(exdi);
					} catch (Exception e) {
						Debug.println("Error writing Bill Pay Diag..."); 
						Debug.printStackTrace(e);
					}
				} else {
					mgsdi.setExchangeRate(getExchangeRate(false));
					if (live) {
						ReportLog.logMoneyGramSend(ReportLog.MONEYGRAM_SEND_TYPE, dataCollectionData, mgsdi, returnInfo,billPayment);
					}
					DiagLog.getInstance().writeMGSend(mgsdi);
				}
				return Boolean.TRUE;
			}
		};
		MessageFacade.run(logic, callingPage, COMPLETE_SESSION, Boolean.FALSE);
	}

	/**
	 * Indicates whether amount tendered profile item is enabled.
	 *
	 * @return true if amount tendered profile item is enabled. if profile item
	 *         is disabled or missing, returns false.
	 */
	public boolean isAmountTenderedEnabled() throws NoSuchElementException {
		try {
			return super.isProductProfileOptionTrue("AMOUNT_TENDERED"); 
		} catch (Exception e) {
			Debug
					.println("AMOUNT_TENDERED profile Item is missing: " + e.getMessage()); 
			return false;
		}
	}

	/**
	 * Sets request information used for fee calculation, sends message to
	 * request for Fee calculation from middleware, sets sendInfo and returnInfo
	 * after it receives response.
	 *
	 * @param CommCompleteInterface
	 *            that is used to call back MGSWizardPage11 to bring up
	 *            MGSWizardPage13 after it receives response from middleware.
	 */
	public void getFeeCalculation(final String mgiSessionID, final CommCompleteInterface cci) {

		final MessageFacadeParam parameters = new MessageFacadeParam(
				MessageFacadeParam.REAL_TIME);

		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {
				FeeLookupRequest feeMsg = new FeeLookupRequest();
				feeMsg.setMgiSessionID(mgiSessionID);
				feeMsgResp = null;
				String sendCurrency = sendInfo.getSendCurrency();
				
				if (sendCurrency==null || sendCurrency.isEmpty()) {
					feeMsg.setSendCurrency(agentBaseCurrency());
					sendInfo.setSendCurrency(agentBaseCurrency());
				} else {
					feeMsg.setSendCurrency(sendCurrency);
				}
				
	        	if ((isUsingCard()) && (cardPhoneNumber != null) && (! cardPhoneNumber.isEmpty())) {
					feeMsg.setMgiRewardsNumber(cardPhoneNumber);
				}

				BigDecimal sendAmount = sendInfo.getAmount();
				if (sendAmount == null || sendAmount.equals(BigDecimal.ZERO)) {
					feeMsg.setReceiveAmount(getExpectedRecieveAmt());
				} else if (isDeductFeeFromSend()) {
					feeMsg.setAmountIncludingFee(sendInfo.getAmount());
				} else {
					feeMsg.setAmountExcludingFee(sendInfo.getAmount());
				}

				feeMsg.setDestinationCountry(destination.getCountryCode());
				CountrySubdivision cs = sendInfo.getDestinationState();
				String csc = cs != null ? cs.getCountrySubdivisionCode() : null;
				String state = (csc != null) && (! csc.isEmpty()) ? csc : null;
				feeMsg.setDestinationCountrySubdivisionCode(state);

				if (billPayment) {
					feeMsg.setServiceOption(sendInfo.getDeliveryOption());
					feeMsg.setAllOptions(false);
					feeMsg.setMgiSessionType(SessionType.BP);
					feeMsg.setReceiveCode(billerInfo.getReceiveCode());
					feeMsg.setProductVariant(sendInfo.getProductVariant());
					// feeMsg.setServiceOfferingID(sendInfo.getServiceOfferingID());
					feeMsg.setServiceOfferingID(billerInfo.getServiceOfferingID());
					feeMsg.setDefaultInformationalFee(getDefaultInformationalFee());
					feeMsg.setDefaultMaxFee(getIsDefaultMaxFee());
					feeMsg.setReceiveAgentID(billerInfo.getReceiveAgentID());
					//feeMsg.setReceiveCurrency("");
					// feeMsg.setAgencyID(sendInfo.getAgencyID());
				} else {
					feeMsg.setServiceOption(null);
					feeMsg.setReceiveCurrency(null);
					feeMsg.setAllOptions(true);
					feeMsg.setMgiSessionType(SessionType.SEND);
					feeMsg.setReceiveAgentID(sendInfo.getReceiveAgentID());
					feeMsg.setReceiveCurrency(sendInfo.getReceiveCurrency());
					if (getProfileObject() != null) {
						feeMsg.setMgiRewardsNumber(getProfileObject().getProfileValue(FieldKey.MGIREWARDSNUMBER_KEY.getInfoKey()));
					}
					String mgCustomerReceiveNumber = sendInfo.getMgCustomerReceiveNumber();
					if ((mgCustomerReceiveNumber != null) && (! mgCustomerReceiveNumber.isEmpty())) {
						feeMsg.setMgCustomerReceiveNumber(mgCustomerReceiveNumber);
					}
					
					if (sendInfo.getPromoCodes() != null) {
						PromoCodes promoCodesValues = feeMsg.getPromoCodes();
						if (promoCodesValues == null) {
							promoCodesValues = new PromoCodes();
						}
						for (String promocode : sendInfo.getPromoCodes()) {
							promoCodesValues.getPromoCode().add(promocode);
						}
						feeMsg.setPromoCodes(promoCodesValues);
					}
				}
				
				//feeMsg.setIssueFreqCustCard(getCreateMoneySaver());
				
				feeMsgResp = MessageFacade.getInstance().getFeeCalculation(feeMsg, parameters);

				if ((feeMsgResp == null) || (feeMsgResp.getPayload().getValue().getFeeInfos().getFeeInfo().size() == 0)) {
					return Boolean.FALSE;
				}
				FeeInfo feeInfo = (FeeInfo) getFeeInfo().get(0);
				setFeeReturnInfo(feeInfo);
				return Boolean.TRUE;
			}
		};
		MessageFacade.run(logic, cci, FEE_LOOKUP, Boolean.FALSE);
		// Debug.println("Complete fee calculation request");
	}

	public void setFeeReturnInfo(FeeInfo feeMsg) {
		this.feeInfo = feeMsg;
		returnInfo.setFeeInfo(feeMsg);
		if ((feeMsg.getErrorInfo() == null) || (feeMsg.getErrorInfo().isRecoverableFlag())) {
			sendInfo.setAmount(feeMsg.getSendAmounts().getSendAmount());
		}
		this.setMgiTransactionSessionId(feeMsg.getMgiSessionID());
		ServiceOption qdo = new ServiceOption(feeMsg);
		sendInfo.setQualifiedDeliveryOption(qdo, dataCollectionData);

		if (feeMsg.getReceiveAmounts() != null) {
		setGuaranteeInd(feeMsg.getReceiveAmounts().isValidCurrencyIndicator());
		}

		boolean isValid = feeMsg.getReceiveAmounts() != null ? feeMsg.getReceiveAmounts().isValidCurrencyIndicator() : false;
		if (isValid) { // used to be called "guaranteed"
			//returnInfo.setFeeInfo(feeMsg);
		} else { // used to be called "indicative"
			// don't set expected receive amount here because not guaranteed
			sendInfo.setExpectedReceiveAmount("");
		}

		// DW60-SCR#665 - Either way set the valid rate for confirm receipt.
		
		setReturnedPromoCodeCount(feeMsg.getPromotionInfos() != null ? feeMsg.getPromotionInfos().getPromotionInfo().size() : 0);
	}

	/**
	 * Gets sender information from the Account Number history.
	 *
	 * @param CommCompleteInterface
	 *            that is used to call back the current screen to bring up the
	 *            next screen after it receives response from middleware.
	 */
	public void getAccountNumberHistory(final CommCompleteInterface cci, final String accountNumber) {
		final MessageFacadeParam parameters = new MessageFacadeParam(MessageFacadeParam.REAL_TIME,
				MessageFacadeParam.USER_CANCEL);
		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {
				customerHistory = null;
				try {
					customerHistory = MessageFacade.getInstance().consumerHistoryLookup(parameters, accountNumber, DWValues.LOOKUP_ACCT, SessionType.BP, null);
					if (customerHistory != null) {
						if ((customerHistory.size() == 0)) {
							recentCompanies = new CompanyInfo[0];
							if (!isDirectoryOfBillers()) {
								setReceivingCompany(new CompanyInfo());
								clearCustomers();
							}
							return Boolean.TRUE;
						}
					}
				} catch (MessageFacadeError e) {
					return Boolean.FALSE;
				}
				if (customerHistory==null || customerHistory.size() == 0) {

					// This appears to be the case where the user clicked Cancel
					// in the comm dialog
					recentCompanies = new CompanyInfo[0];
					if (!isDirectoryOfBillers())
						setReceivingCompany(new CompanyInfo());
					return Boolean.FALSE;
				}

				if(customerHistory!=null && customerHistory.size()>0) {
					TreeSet<CustomerInfo> ts = new TreeSet<CustomerInfo>(comparator);
					for (int i = 0; i < customerHistory.size(); i++) {
						ts.add(customerHistory.get(i));
					}
					customerHistory = new ArrayList<CustomerInfo>();
					customerHistory.addAll(ts);
					// we have a customer...set them as the sender
					setSender(customerHistory.get(0).getSender());
					setCustomersToCustomerHistory();
				}else {

					/*
					 * If no valid history records, return false.
					 */
					return Boolean.FALSE;
				}
					
				// now retrieve their recent recip list
				if (isBillPayment()) {
					recentCompanies = new CompanyInfo[customerHistory.size()];
					for (int i = 0; i < customerHistory.size(); i++) {
						if (customerHistory.get(i).getCompanyInfo() != null) {
							recentCompanies[i] = new CompanyInfo();
							recentCompanies[i].setBillerName(customerHistory.get(i).getCompanyInfo().getBillerName());
							recentCompanies[i].setBillerCity(customerHistory.get(i).getCompanyInfo().getBillerCity());
							recentCompanies[i].setReceiveCode((customerHistory.get(i).getCompanyInfo().getReceiveCode()));
						}
					}
				}

				return Boolean.TRUE;
			}
		};
		MessageFacade.run(logic, cci, CONSUMER_HISTORY_LOOKUP, Boolean.TRUE);

	}

	public void setReceivingCompany(CompanyInfo company) {
		this.receivingCompany = company;
//		if (company != null) {
//			sendInfo.setAgencyID(company.getReceiveCode());
//			// the following line of code will pre-populate the customer's
//			// account number that was returned through transaction history.
//			// setEpAccountNumber(company.getAccountNum());
//		}
	}

	public void setBillerInfo(BillerInfo biller) {
		this.billerInfo = biller;
	}

	public CompanyInfo getReceivingCompany() {
		return this.receivingCompany;
	}

	public void setBankCardSwiped(boolean swiped) {
		sendInfo.setCardSwiped(swiped);
		dataCollectionData.setFieldValue(FieldKey.CARD_SWIPEDFLAG_KEY, (swiped ? "true" : "false"));
	}

	public boolean getBankCardSwiped() {
		return sendInfo.isCardSwiped();
	}

	public void setBankCardExpDateMonth(String expDate) {
		sendInfo.setCardExpDateMonth(expDate);
		dataCollectionData.setFieldValue(FieldKey.CARD_EXPIRATION_MONTH_KEY, expDate);
	}

	public String getBankCardExpDateMonth() {
		return sendInfo.getCardExpDateMonth();
	}

	public void setBankCardExpDateYear(String expDate) {
		sendInfo.setCardExpDateYear(expDate);
		dataCollectionData.setFieldValue(FieldKey.CARD_EXPIRATION_YEAR_KEY, expDate);
	}

	public String getBankCardExpDateYear() {
		return sendInfo.getCardExpDateYear();
	}

    public void setPpcExpeditedTran(boolean b) {
    	dataCollectionData.setFieldValue(FieldKey.PPC_EXP_TRAN_KEY, b ? "true" : "false");
    }

	public void setBillerAccountNumber(String accountNumber) {
		/*
		 * typically for a transactionLookup, but in this case from a successful
		 * biller search the ReceiveDetail is used for comparing the lookup acct
		 * number with a data entry value for the account number verification
		 */
		if (detail == null) {
			createNewReceiveDetailInfo();
		}
		detail.setBillerAccountNumber(accountNumber.trim());
	}

	public BillerInfo getBillerInfo() {
		return this.billerInfo;
	}

	public void setBpAccountNumber(String account) {
		sendInfo.setAccount(account);
		dataCollectionData.setFieldValue(FieldKey.BILLER_ACCOUNTNUMBER_KEY, account);
	}

	public String getBpAccountNumber() {
		return sendInfo.getAccount();
	}

	public void setBpPurposeOfFunds(String purpose) {
		sendInfo.setPurpose(purpose);
		dataCollectionData.setFieldValue(FieldKey.PURPOSEOFFUND_KEY, purpose);
	}

	public String getBpPurposeOfFunds() {
		return sendInfo.getPurpose();
	}

	public void setBpReceiverFirstName(String receiverFirstName) {
		sendInfo.setReceiverFirstName(receiverFirstName);
		dataCollectionData.setFieldValue(FieldKey.RECEIVER_FIRSTNAME_KEY, receiverFirstName);
	}

	public String getBpReceiverFirstName() {
		return sendInfo.getReceiverFirstName();
	}

	public void setBpReceiverMiddleName(String receiverMiddleName) {
		sendInfo.setReceiverMiddleName(receiverMiddleName);
		dataCollectionData.setFieldValue(FieldKey.RECEIVER_MIDDLENAME_KEY, receiverMiddleName);
	}

	public String getBpReceiverMiddleName() {
		return sendInfo.getReceiverMiddleName();
	}

	public void setBpReceiverLastName(String receiverLastName) {
		sendInfo.setReceiverLastName(receiverLastName);
		dataCollectionData.setFieldValue(FieldKey.RECEIVER_LASTNAME_KEY, receiverLastName);
	}

	public String getBpReceiverLastName() {
		return sendInfo.getReceiverLastName();
	}

	public void setBpSenderFirstName(String senderFirstName) {
		sendInfo.setSenderFirstName(senderFirstName);
		dataCollectionData.setFieldValue(FieldKey.SENDER_FIRSTNAME_KEY, senderFirstName);
	}

	public String getBpSenderFirstName() {
		return sendInfo.getSenderFirstName();
	}

	public void setBpSenderMiddleName(String senderMiddleName) {
		sendInfo.setSenderMiddleName(senderMiddleName);
		dataCollectionData.setFieldValue(FieldKey.SENDER_MIDDLENAME_KEY, senderMiddleName);
	}

	public String getBpSenderMiddleName() {
		return sendInfo.getSenderMiddleName();
	}

	public void setBpSenderLastName(String senderLastName) {
		sendInfo.setSenderLastName(senderLastName);
		dataCollectionData.setFieldValue(FieldKey.SENDER_LASTNAME_KEY, senderLastName);
	}

	public String getBpSenderLastName() {
		return sendInfo.getSenderLastName();
	}

	public void setValidateAccountNumber(String account) {
		sendInfo.setValidateAccountNumber(account);
		dataCollectionData.setFieldValue(FieldKey.VALIDATE_ACCOUNT_NBR_KEY, account);
	}

	public String getValidateAccountNumber() {
		return sendInfo.getValidateAccountNumber();
	}

	public boolean isBillPayment() {
		return billPayment;
	}

	public CompanyInfo[] getMatchingCompanies() {
		return matchingCompanies;
	}

	public BillerInfo[] getMatchingBillers() {
		return matchingBillers;
	}

	public List<FeeInfo> getFeeInfo() {
		if ((feeMsgResp == null) || (feeMsgResp.getPayload() == null) || (feeMsgResp.getPayload().getValue() == null) || (feeMsgResp.getPayload().getValue().getFeeInfos() == null)) {
			return null;
		} else {
			return feeMsgResp.getPayload().getValue().getFeeInfos().getFeeInfo();
		}
	}

	public void clearCustomers() {
		customers = null;
		uniqueCustomers = null;
	}

	public void setCustomersToCustomerHistory() {
		customers = customerHistory;
		uniqueCustomers = new TreeSet<CustomerInfo>(comparator);
		uniqueCustomers.addAll(customers);
	}

	public List<CustomerInfo> getUniqueCustomers() {
		return (uniqueCustomers == null ? customers : new ArrayList<CustomerInfo>(uniqueCustomers));
	}

	public List<CustomerInfo> getCustomers() {
		return customers;
	}

	public PersonalInfo[] getMatchingCustomers() {
		return matchingCustomers;
	}

	public boolean isFraudAlertNeeded(String currency) {
		return getAmount().compareTo(getProductProfile().findCurrencyDecimalValue(
        		"FRAUD_LIMIT_CASH_WARNING", currency))
        		>= 0;
	}

	public boolean isDss() {
		return isDSS;
	}
	
	public boolean isInlineDSR() {
		return true;///Turning on Intra-Trn flag alway true as part of project#54351b
		//return isInlineDSR;
	}

	public BigDecimal getPhotoIDLimit() {
		return getPhotoIDLimit(agentBaseCurrency());
    }

	public BigDecimal getPhotoIDLimit(String currency) {
		return getProductProfile().findCurrencyDecimalValue("FRAUD_LIMIT_PHOTO_ID",
				currency);
    }

	public BigDecimal getGovIDLimit() {
		return getGovIDLimit(agentBaseCurrency());
	}

	public BigDecimal getGovIDLimit(String currency) {
		return getProductProfile().findCurrencyDecimalValue("FRAUD_LIMIT_LEGAL_ID",
				currency);
    }

	public BigDecimal getThirdParyLimit() {
		return getThirdParyLimit(agentBaseCurrency());
	}

	public BigDecimal getThirdParyLimit(String currency) {
		return getProductProfile().findCurrencyDecimalValue("FRAUD_LIMIT_THIRD_PARTY_ID",
				currency);
    }

	public boolean isSecondIdNeeded() {
		return UnitProfile.getInstance().get("OPTIONAL_AGENT_FIELDS", "N")
				.equalsIgnoreCase("Y");
	}

	/**
	 * Check whether or not the Banamex bank branch and account numbers should
	 * be enabled for this transaction. Only for Next Day Pesos.
	 */
	public boolean isBanamexAccountAvailable() {
		return isBanamexTransaction()
				&& getDeliveryOption().equals("Next Day Service"); 
	}

	/**
	 * Check whether or not the Home Delivery option is available for this
	 * transaction.
	 */
	public boolean isSpecialServices() {
		return getHomeDelivery();
	}

	/**
	 * Check whether or not this is a Banamex transaction. Next Day Service is a
	 * Banamex services. Cambio Plus is not.
	 */
	public boolean isBanamexTransaction() {
		return getDestination().getCountryCode().equals("MX") &&  // Need to be changed if not correct
				getDeliveryOption().equals("Next Day Service"); 
	}

	public void setDestination(Country newDestination) {
		if (newDestination == null) {
			return;
		}
		destination = newDestination;

		// Not sure why destination country is in two places, but it
		// seems reasonable to update the other one.
		sendInfo.setDestinationCountry(destination.getCountryCode());
		if (dataCollectionData != null) {
			dataCollectionData.setFieldValue(FieldKey.MF_RECV_COUNTRY_KEY, destination.getCountryCode());
			dataCollectionData.setFieldValue(FieldKey.DESTINATION_COUNTRY_KEY, destination.getCountryCode());
		}
	}

	public void setDestination(String newDestination) {
		if (newDestination == null || newDestination.equals(""))
			setDestination(CountryInfo.getCountry(getDefaultCountry()));
		else
			setDestination(CountryInfo.getCountry(newDestination));
	}

	public Country getDestination() {
		return destination;
	}

	@Override
	public String getDestinationCountry() {
		return destination.getCountryCode();
	}

	public CountrySubdivision getDestinationState() {
		return destinationState;
	}

	public void setDestinationState(CountrySubdivision state) {
		destinationState = state;
		if (state != null) {
			sendInfo.setDestinationState(destinationState);
//			dataCollectionData.setFieldValue(FieldKey.DESTINATION_COUNTRY_SUBDIVISIONCODE_KEY, destinationState.getCountrySubdivisionCode());
		}
	}

	public String getCommitReferenceNumber() {
		if(returnInfo.getCommitTransactionInfo()!=null) {
			return returnInfo.getCommitTransactionInfoPayload().getReferenceNumber();
		}
		return null;
	}

	public String getPhoneCallPIN() {
		if(returnInfo.getCommitTransactionInfo()!=null) {
			return returnInfo.getCommitTransactionInfoPayload().getFreePhoneCallPIN();
		}
		return null;
	}

	public String getOrdenNumber() {
		String number = null;
		if ((this.completeSessionResponse != null) && (this.completeSessionResponse.getPayload() != null) && (this.completeSessionResponse.getPayload().getValue() != null)) {
			number = this.completeSessionResponse.getPayload().getValue().getPartnerConfirmationNumber();
		}
		return number;
	}

	@Override
	public BigDecimal getMaxItemAmount() {
		return getMaxItemAmount(agentBaseCurrency());
	}

	@Override
	public BigDecimal getMaxItemAmount(String currency) {
		BigDecimal miAmt;

		try {
				miAmt = getProductProfile().findCurrencyDecimalValue(
						"MAX_AMOUNT_PER_ITEM", currency);
		} catch (NoSuchElementException e) {
			miAmt = new BigDecimal(0.00);
		}
		return miAmt;
	}

	public BigDecimal getFraudLegalLimit() {
		return this.fraudLegalLimit;
	}

	public BigDecimal getThirdPartyLimit() {
		return this.thirdParyLimit;
	}

	/**
	 * Return the exchange rate for this transaction. The rate will be returned
	 * from the middleware with an implied 6 decimal places, so adjust the point
	 * accordingly.
	 */
	public BigDecimal getExchangeRate(boolean isBillPay) {
		BigDecimal retValue=null;
		if (isBillPay) {
			retValue = bpValidationResponsePayload.getExchangeRateApplied();
		} else {
			retValue = sendValidationResponsePayload.getExchangeRateApplied();
		}
		if (retValue == null) {
			return new BigDecimal(applyDoddFrank("1.000000")); 
		} else {
			return retValue;
		}
	}

	/**
	 * The currency for the send will be determined based on the destination
	 * country. Since we do not get back the exchange rate from the host, we
	 * have it stored in our local country table.
	 */
	public String getCurrency() {
		return getDestination().getBaseReceiveCurrency();
	}

	public String getReceiveCurrency() {
		return sendInfo.getReceiveCurrency();
	}

	public void setFiAccountNumber(String fiAccountNumber) {
		sendInfo.setSenderBankAccountNumber(fiAccountNumber);
		dataCollectionData.setFieldValue(FieldKey.INTERNATIONALBANKACCOUNTNUMBER_KEY, fiAccountNumber); 
	}

	public String getFiAccountNumber() {
		return sendInfo.getSenderBankAccountNumber();
	}

	public void setDeductFeeFromSend(boolean on) {
		deductFeeFromSend = on;
	}

	public boolean isDeductFeeFromSend() {
		return deductFeeFromSend;
	}

	public void setFeeQuery(boolean on) {
		feeQuery = on;
	}

	public void setDirectoryOfBillers(boolean on) {
		directoryOfBillers = on;
	}


	public boolean isFeeQuery() {
		return feeQuery;
	}

	public boolean isDirectoryOfBillers() {
		return directoryOfBillers;
	}

	public boolean isCardNumberEntered() {
		String cardNumber = getCardNumber();
		return cardNumber != null && cardNumber.length() > 0;
	}

	public String getCardNumber() {
		return getSender().getMoneySaverID();
	}

	public void setUsingCard(boolean b) {
		usingCard = b;
	}

	public boolean isUsingCard() {
		return usingCard;
	}

	/**
	 * get the auto Money saver card number
	 *
	 */
	public String getAutoMoneySaver() {
		// return returnInfo.getAutoMoneySaver();
		return returnInfo.getAutoEnrollMoneySaverID();
	}

	/**
	 * profile option for associate discount. If associate card is available,
	 * returns true
	 */

	public boolean isAssociateDiscountAvailable() {
		return isProductProfileOptionTrue(DWValues.DISCOUNT_OPTION); 
	}

	public void setDeliveryOption(String deliveryOption) {
		sendInfo.setDeliveryOption(deliveryOption);
		dataCollectionData.setFieldValue(FieldKey.SERVICEOPTION_KEY, deliveryOption);
	}

	public String getDeliveryOption() {
		return sendInfo.getDeliveryOption();
	}

	public void setReceiveAgentAbbreviation(String receiveAgentAbbreviation) {
		dataCollectionData.setFieldValue(FieldKey.RECEIVE_AGENTABBREVIATION_KEY, receiveAgentAbbreviation);
	}

	public void setDeliveryOptionID(String deliveryOptionID) {
		sendInfo.setDeliveryOptionID(deliveryOptionID);
		dataCollectionData.setFieldValue(FieldKey.DELIVERYOPTIONID_KEY, deliveryOptionID);
	}

	public String getDeliveryOptionID() {
		return sendInfo.getDeliveryOptionID();
	}

	public void setReceiveAgentID(String receiveAgentID) {
		dataCollectionData.setFieldValue(FieldKey.RECEIVE_AGENTID_KEY, receiveAgentID);
	}

	public String getReceiveAgentID() {
		return sendInfo.getReceiveAgentID();
	}

	public void setConfirmationNumber(String confirmationNumber) {
		sendInfo.setConfirmationNumber(confirmationNumber);
		dataCollectionData.setFieldValue(FieldKey.CONFIRMATION_NBR_KEY, confirmationNumber);
	}

	public String getConfirmationNumber() {
		return sendInfo.getConfirmationNumber();
	}

	public void setReceiveCurrency(String pm_sReceiveCurrency) {
		sendInfo.setReceiveCurrency(pm_sReceiveCurrency);
		dataCollectionData.setFieldValue(FieldKey.RECEIVE_CURRENCY_KEY, pm_sReceiveCurrency);
	}

	public void setValidReceiveCurrency(String pm_sPayoutCurrency) {
		dataCollectionData.setFieldValue(FieldKey.VALID_RECEIVE_CURRENCY_KEY, pm_sPayoutCurrency);
	}

	public String getSendCurrency() {
		return sendInfo.getSendCurrency();
	}

	public void setSendCurrency(String sendCurrency) {
		sendInfo.setSendCurrency(sendCurrency);
		dataCollectionData.setFieldValue(FieldKey.SEND_CURRENCY_KEY, sendCurrency);
	}

	public void setTestQuestion(String question) {
		sendInfo.setQuestion(question);
	}

	public void setMessage1(String msg) {
		sendInfo.setMessage1(msg);
		dataCollectionData.setFieldValue(FieldKey.MESSAGEFIELD1_KEY, msg);
	}

	public String getMessage1() {
		return sendInfo.getMessage1();
	}

	public void setMessage2(String msg) {
		sendInfo.setMessage1(msg);
		dataCollectionData.setFieldValue(FieldKey.MESSAGEFIELD2_KEY, msg);
	}

	public String getMessage2() {
		return sendInfo.getMessage2();
	}

	public String getTestQuestion() {
		return sendInfo.getQuestion();
	}

	public void setTestAnswer(String answer) {
		sendInfo.setAnswer(answer);
		dataCollectionData.setFieldValue(FieldKey.TESTANSWER_KEY, answer);
	}

	public String getTestAnswer() {
		return sendInfo.getAnswer();
	}

	public PersonalInfo getThirdParty() {
		return sendInfo.getThirdParty();
	}

	public ReceiverInfo getReceiver() {
		return sendInfo.getReceiver();
	}

	public void setReceiver(ReceiverInfo receiver) {
		sendInfo.setReceiver(receiver, dataCollectionData);
	}

	public void setReceiverEP(ReceiverInfo receiver) {
		sendInfo.setReceiverEP(receiver, dataCollectionData);
	}

	/** Replaces setReceiver(new PersonalInfo()). */
	public void clearReceiver() {
		sendInfo.clearSender(dataCollectionData);
	}

	public SenderInfo getSender() {
		return sendInfo.getSender();
	}

	/**
	 * Set the sender for this transaction. Clear out the recent companies and
	 * recent recipients at this time since we have them cached for the last
	 * sender.
	 */
	public void setSender(SenderInfo sender) {
		if (dataCollectionData != null) {
			sendInfo.setSender(sender, dataCollectionData);
		}
	}

	/** Replaces setSender(new PersonalInfo()). */
	public void clearSender() {
		if (sendInfo != null) {
			sendInfo.clearSender(dataCollectionData);
			sendInfo.setSenderCountry(getDefaultCountry());
			if (dataCollectionData != null) {
				dataCollectionData.setFieldValue(FieldKey.SENDER_COUNTRY_KEY, getDefaultCountry());
			}
			sendInfo.setSenderCountry(getDefaultCountry());
		}
		else {
			Debug.println("Why is there no sendInfo["+tranName+"]");
		}
		
		if (dataCollectionData != null) {
			dataCollectionData.setFieldValue(FieldKey.SENDER_COUNTRY_KEY, getDefaultCountry());
		}
		this.setRewardsFirstName("");
		this.setRewardsLastName("");
	}

	public void clearBiller() {
		billerInfo = new BillerInfo();
	}

	/**
	 * Set the expected receive amount of the transaction. This will be use by
	 * Fee Query in the MC4A message.
	 */
	public void setExpectedRecieveAmt(BigDecimal amount) {
		if (amount == null) {
			sendInfo.setExpectedReceiveAmount("");
//			dataCollectionData.setFieldValue(FieldKey.EXPECTED_RECEIVE_AMOUNT_KEY, "");
		} else {
			sendInfo.setExpectedReceiveAmount(amount.toString());
//			dataCollectionData.setFieldValue(FieldKey.EXPECTED_RECEIVE_AMOUNT_KEY, amount.toString());
		}
	}

	public BigDecimal getExpectedRecieveAmt() {
		return StringUtility.stringToBigDecimal(sendInfo
				.getExpectedReceiveAmount());
	}

	/**
	 * Set the amount of the transaction. Check the limits for this transaction
	 * and return a code indicating the results of the amount set.
	 */

	public int setAmount(BigDecimal amount) {
		int status = -1;
		if (this.billPayment) {
			status = checkBillPayLimits(amount);
			if (status == BILL_PAY_DAILY_LIMIT_EXCEEDED)
				return status;
		}

		status = this.checkLimits(amount, agentBaseCurrency());
		if (status == OK || status == MAX_EMPLOYEE_SEND_LIMIT_EXCEEDED) {
			sendInfo.setAmount(amount);
			dataCollectionData.setFieldObject(FieldKey.SEND_AMOUNT_KEY, amount);
			if (sendInfo.getSendCurrency()==null || sendInfo.getSendCurrency().isEmpty()) {
				sendInfo.setReceiveCurrency(agentBaseCurrency());
				dataCollectionData.setFieldValue(FieldKey.SEND_CURRENCY_KEY, agentBaseCurrency());
			}
		}
		return setAmount(amount, agentBaseCurrency());

	}

	public int setAmount(BigDecimal amount, String currency) {
		int status = -1;
		if (this.billPayment) {
			status = checkBillPayLimits(amount);
			if (status == BILL_PAY_DAILY_LIMIT_EXCEEDED)
				return status;
		}

		status = this.checkLimits(amount, currency);
		if (status == OK || status == MAX_EMPLOYEE_SEND_LIMIT_EXCEEDED) {
			sendInfo.setAmount(amount);
			sendInfo.setSendCurrency(currency);
			sendInfo.setReceiveCurrency(currency);
		}
		return status;

	}

	public BigDecimal getAmount() {
		if (sendInfo.getAmount() == null) {
			return BigDecimal.ZERO;
		}
		return sendInfo.getAmount();
	}

	public BigDecimal getTotalAmount() {
		return getSendTotal();
	}

	public BigDecimal getReceiveAmount() {
		return sendValidationResponsePayload.getReceiveAmounts().getReceiveAmount();
	}

	public BigDecimal getSendTotal() {
		if (this.billPayment) {
			if (bpValidationResponsePayload == null)
				return null;
			return bpValidationResponsePayload.getSendAmounts() != null
					? bpValidationResponsePayload.getSendAmounts().getTotalAmountToCollect() : null;
		} else {
			if (sendValidationResponsePayload == null)
				return null;
			return sendValidationResponsePayload.getSendAmounts() != null
					? sendValidationResponsePayload.getSendAmounts().getTotalAmountToCollect() : null;
		}
	}
    
	public BigDecimal getSaLimitAvailable() {
	    BigDecimal retValue = UnitProfile.getInstance().get("SA_LIMIT", (BigDecimal) null);
	    if (retValue != null) {
	    	if (retValue.compareTo(BigDecimal.ZERO) < 0) {
	    		retValue = BigDecimal.valueOf(0, Currency.getCurrencyPrecision(sendInfo.getSendCurrency()));
	    	}
	    }
		return retValue;
	}

	public boolean doesSALimitExist() {
		return !(returnInfo.getSaLimitAvailable()==null);
	}

	public void setRegistrationNumber(String registrationNumber) {
		sendInfo.setRegistrationNumber(registrationNumber);
		dataCollectionData.setFieldValue(FieldKey.REGISTRATION_NUMBER_KEY, registrationNumber);
	}

	public String getRegistrationNumber() {
		return sendInfo.getRegistrationNumber() != null ? sendInfo.getRegistrationNumber().trim() : "";
	}

	public void setProductVariant(ProductVariantType productVariant) {
		sendInfo.setProductVariant(productVariant);
	}

	public ProductVariantType getProductVariant() {
		return sendInfo.getProductVariant();
	}

	public String getCustomerReceiveNumber() {
		return sendInfo.getCustomerReceiveNumber();
	}

	public void setCustomerReceiveNumber(String receiveNumber) {
		sendInfo.setCustomerReceiveNumber(receiveNumber);
		dataCollectionData.setFieldValue(FieldKey.GFFP_TAG_RRN_KEY, receiveNumber);
		if (receiveNumber!=null && receiveNumber.toUpperCase(Locale.US).startsWith("MG")
				|| (receiveNumber ==null || receiveNumber.isEmpty())) {
			sendInfo.setMgCustomerReceiveNumber(receiveNumber);
			dataCollectionData.setFieldValue(FieldKey.MG_CUST_RECV_NBR_KEY, receiveNumber);
		}
	}

	public BigDecimal getFeeSendTotalWithTaxBp(boolean isFeeInfo) {
		return returnInfo != null ? returnInfo.getFeeSendTotalWithTax(true,isFeeInfo) : null;
	}
	
	public BigDecimal getFeeSendTotalWithTax(boolean isFeeInfo) {
		return returnInfo != null ? returnInfo.getFeeSendTotalWithTax(false,isFeeInfo) : null;
	}

	public BigDecimal getTaxSendTotalAmt() {
		return sendValidationResponsePayload.getSendAmounts().getTotalSendTaxes();
	}

	public BigDecimal getDiscSendTotal() {
		return sendValidationResponsePayload.getSendAmounts().getTotalSendTaxes();
	}

    /**
     * Gets the Non-MGI imposed fee amount charged for sending the transaction.
     */
    public BigDecimal getFeeSendNonMgi() {
		return returnInfo.getFeeSendNonMgi(false,false);
    }

	public BigDecimal getDefaultInformationalFee() {
		return this.defaultInformationalFee;
	}

	public boolean getIsDefaultMaxFee() {
		return this.isDefaultMaxFee;
	}

	/**
	 * Adds up amount to send and fee.
	 *
	 * @return amount to send plus fee.
	 */
	public BigDecimal getTotalToCollect() {
		return getSendTotal();
	}

	/**
	 * subtracts total to collect from amount tendered.
	 *
	 * @param totalToCollect
	 *            total to collect from customer.
	 * @param amountTendered
	 *            amount collected from customer
	 * @return amount tendered minus total to collect.
	 */
	public BigDecimal getChangeDue(String totalToCollect, String amountTendered) {
		BigDecimal totalAmtToCollect = StringUtility
				.stringToBigDecimal(totalToCollect);
		BigDecimal amtTendered = StringUtility
				.stringToBigDecimal(amountTendered);
		return amtTendered.subtract(totalAmtToCollect);
	}

	/**
	 * Test if message is available on the screen.
	 *
	 * @return true if message is available, returns false otherwise.
	 */
	public boolean isMessageAvailable() {
		if (isBancomerDeliveryOption())
			return false;

		return true;
	}

	/**
	 * Test if Middle Initial is available on the screen.
	 *
	 * @return true if Middle Initial is available, returns false otherwise.
	 */
	public boolean isMiddleNameAvailable() {
		if (isBancomerDeliveryOption())
			return false;

		return true;
	}

	/**
	 * Test if Cambio Plus Bancomer delivery option is used
	 *
	 * @return true if Cambio Plus Bancomer delivery option is used, returns
	 *         false otherwise.
	 */
	public boolean isBancomerDeliveryOption() {
		if (getDeliveryOption() != null
				&& getDeliveryOption().equals("Cambio Plus - Bancomer Only")) 
			return true;

		return false;
	}

	public void clearReceiverAddress() {
		sendInfo.getReceiver().setAddress(""); 
		sendInfo.getReceiver().setAddress2(""); 
		sendInfo.getReceiver().setAddress3(""); 
		sendInfo.getReceiver().setAddress4(""); 
		sendInfo.getReceiver().setCity(""); 
		sendInfo.getReceiver().setState(""); 
		sendInfo.getReceiver().setZip(""); 
		sendInfo.getReceiver().setHomePhone(""); 
		clearDirections();
	}

	public void clearBanamex() {
		sendInfo.setAccount(""); 
		dataCollectionData.setFieldValue(FieldKey.OTHER_PAYOUT_ID_NBR_KEY, "");
		// sendInfo.setBankBranch(""); 
		clearDirections();
	}

	public void clearDirections() {
		sendInfo.getReceiver().setColonia(""); 
		sendInfo.getReceiver().setMunicipio(""); 
	}

	public void clearTestQuestionAnswer() {
		sendInfo.setQuestion(""); 
		dataCollectionData.setFieldValue(FieldKey.TESTQUESTION_KEY, "");
		sendInfo.setAnswer(""); 
		dataCollectionData.setFieldValue(FieldKey.TESTANSWER_KEY, "");
	}

	public void clearBancomer() {
		sendInfo.getReceiver().setMiddleName(""); 
		dataCollectionData.setFieldValue(FieldKey.RECEIVER_MIDDLENAME_KEY, "");
		clearTestQuestionAnswer();
		sendInfo.setMessage1(""); 
		dataCollectionData.setFieldValue(FieldKey.MESSAGEFIELD1_KEY, "");
		sendInfo.setMessage1(""); 
		dataCollectionData.setFieldValue(FieldKey.MESSAGEFIELD2_KEY, "");
		clearDirections();
	}

	public void clearHomeDelivery() {
		clearTestQuestionAnswer();
		sendInfo.setMessage1(""); 
		dataCollectionData.setFieldValue(FieldKey.MESSAGEFIELD2_KEY, "");
		clearBanamex();
		sendInfo.getReceiver().setAddress(""); 
		sendInfo.getReceiver().setAddress2(""); 
		sendInfo.getReceiver().setAddress3(""); 
		sendInfo.getReceiver().setAddress4(""); 
		sendInfo.getReceiver().setState(""); 
		sendInfo.getReceiver().setZip(""); 
	}

	public String formatPhoneNumber(String number) {
		String temp = ""; 
		// Format the phone number to the US format of x-xxx-xxx-xxx. If
		// the input string has 11 numbers passed in convert, otherwise
		// pass back the original number.
		if (number.length() == 11) {
			temp = number.substring(0, 1);
			temp = temp.concat("-"); 
			temp = temp.concat(number.substring(1, 4));
			temp = temp.concat("-"); 
			temp = temp.concat(number.substring(4, 7));
			temp = temp.concat("-"); 
			temp = temp.concat(number.substring(7, 11));
			number = temp;
		}
		return number;
	}

	public void doEarlyISI() {
		try {
			setupSendISI(ISI_CONF);
	        if (MainPanelDelegate.getIsiRequireRcptTextData()){
	            if (MainPanelDelegate.getIsiExe2ProgramArguments() != null ){
	                String programName = MainPanelDelegate.getIsiExe2ProgramArguments().trim();
	                if (programName.length() > 0 && !(programName.startsWith("-EXE2"))) {
	                	MainPanelDelegate.executeIsiProgram(programName);
	                }
	            }
	        }
		} catch (Exception e) {
			Debug.printException(e);
		}
	}

	private static final int ISI_CONF = 0;
	private static final int ISI_RCPT = 1;

	private Object[] addDfAmts() {
		SendAmountInfo sendAmountInfo = sendValidationResponsePayload.getSendAmounts();
		ReceiveAmountInfo receiveAmount = sendValidationResponsePayload.getReceiveAmounts();
		ReceiptInfo receiptInfo = sendValidationResponsePayload.getReceiptInfo();
		
		Object[] sendAmtItems = new Object[] {};
		
		try {
			sendAmtItems = new Object[] {
				sendAmountInfo.getSendAmount(),
				sendAmountInfo.getSendCurrency(),
				sendAmountInfo.getTotalSendFees(),
				sendAmountInfo.getTotalDiscountAmount(),
				sendAmountInfo.getTotalSendTaxes(),
				sendAmountInfo.getTotalAmountToCollect(),
				returnInfo.getFeeSendMgiOrigNoTax(false,false),
				returnInfo.getFeeSendNonMgi(false,false),
				receiveAmount.getTotalReceiveTaxes(),
				returnInfo.getFeeSendTotalOrigNoTax(false,false),
				returnInfo.getFeeSendTotalFinalNoTax(false,false),
				returnInfo.getTaxSendMgi(false,false),
				returnInfo.getTaxSendNonMgi(false,false),
				returnInfo.getFeeSendMgiCollectedAndTax(false,false),
				returnInfo.getTotalAmountMgi(false,false),
				returnInfo.getFeeSendTotalWithTax(false,false),
				returnInfo.getFeeSendNonMgiWithTax(false,false),
				receiveAmount.getReceiveAmount(),
				receiveAmount.getReceiveCurrency(),
				receiveAmount.isValidCurrencyIndicator()?"true":"false",
				receiveAmount.getPayoutCurrency(),
				receiveAmount.getTotalReceiveFees(),
				receiveAmount.getTotalReceiveTaxes(),
				receiveAmount.getTotalReceiveAmount(),
				returnInfo.getFeeRecvMgi(false,false),
				returnInfo.getFeeRecvOther(false,false),
				returnInfo.getTaxRecvMgi(false,false),
				returnInfo.getTaxRecvOther(false,false),
				returnInfo.getForeignExchange(),
				receiptInfo != null ? receiptInfo.isReceiveFeeDisclosureText() : false,
				receiptInfo != null ? receiptInfo.isReceiveTaxDisclosureText() : false,
				receiveAmount.isReceiveFeesAreEstimated() ? "true" : "false",
				receiveAmount.isReceiveTaxesAreEstimated() ? "true" : "false"
			};
		} catch (Exception e) {
			Debug.printException(e);
			Debug.printStackTrace(e);
		}
		
		return sendAmtItems;
	}
	
	private String getDisclosureText(String language, List<TextTranslationType> list) {
		String text = "";
		for (TextTranslationType ttt : list) {
			if (ttt.getLongLanguageCode().equalsIgnoreCase(language)) {
				text = ttt.getTextTranslation();
				break;
			}
		}
		return text;
	}
	
	private Object[] addDfText1() {
		String dt1 = "";
		String dt2 = "";
		
		if (this.getSendValidationResponsePayload() != null) {
			if (this.getSendValidationResponsePayload().getReceiptInfo() != null) {
				if (this.getSendValidationResponsePayload().getReceiptInfo().getDisclosureTexts() != null) {
					List<TextTranslationType> list = this.getSendValidationResponsePayload().getReceiptInfo()
							.getDisclosureTexts().getDisclosureText();
					dt1 = getDisclosureText(receipt1stLanguageMnemonic, list);
					dt2 = getDisclosureText(receipt2ndLanguageMnemonic, list);
				}
			}
		}
	
		Object[] oaDfText1 = {
				receipt1stLanguageMnemonic,
				dt1,
				receipt2ndLanguageMnemonic,
				dt2
		};
		return oaDfText1;
	}

	private Object[] addDfText2() {
		String dt1 = "";
		String dt2 = "";
		
		if (this.completeSessionResponse != null) {
			if (this.completeSessionResponse.getPayload() != null) {
				if (this.completeSessionResponse.getPayload().getValue() != null) {
					if (this.completeSessionResponse.getPayload().getValue().getReceiptInfo() != null) {
						if (this.completeSessionResponse.getPayload().getValue().getReceiptInfo().getReceiptTextInfos() != null) {
							List<TextTranslationType> list = this.completeSessionResponse.getPayload().getValue()
									.getReceiptInfo().getReceiptTextInfos().getReceiptTextInfo();
							dt1 = getDisclosureText(receipt1stLanguageMnemonic, list);
							dt2 = getDisclosureText(receipt2ndLanguageMnemonic, list);
						}
					}
				}
			}
		}
	
		Object[] oaDfText2 = new Object[] {};
		try {
			oaDfText2 = new Object[] {
					receipt1stLanguageMnemonic,
					dt1,
					receipt2ndLanguageMnemonic,
					dt2
			};
		} catch (Exception e) {
			Debug.printException(e);
		}
		return oaDfText2;
	}

	private List<String> createIsiRecord(Object[] pm_oaData, String pm_recordType) {
		List<String> fields = new ArrayList<String>();
		fields.add(pm_recordType);
		getIsiHeader(fields);
		for (int i = 0; i < pm_oaData.length; i++) {
			fields.add(nullToSpace(pm_oaData[i]));
		}
		return fields;
	}

	private void setupSendISI(int pm_iType) {
		boolean bSavedAppendState = MainPanelDelegate.getIsiAppendMode();
		String overrideID = "";
		String receiveCode = "";

		if (getManagerOverrideID() > 0)
			overrideID = String.valueOf(getManagerOverrideID());

		if ((billerInfo.getProductVariant() != null) && (billerInfo.getProductVariant().equals(ProductVariantType.EP))) {
			receiveCode = billerInfo.getReceiveCode();
		} else {
			receiveCode = "";
		}

		List<PromotionInfo> returnedPromoCodeInfo = null;
		
		if (isBillPayment()) {
			
			if ((bpValidationResponsePayload != null) && (bpValidationResponsePayload.getPromotionInfos() != null)) {
				returnedPromoCodeInfo = bpValidationResponsePayload.getPromotionInfos().getPromotionInfo();	
			}
		} else {
			if ((sendValidationResponsePayload != null) && (sendValidationResponsePayload.getPromotionInfos() != null)) {
				returnedPromoCodeInfo = sendValidationResponsePayload.getPromotionInfos().getPromotionInfo();	
			}
		}
		
		int iPromoCodeCount=0;
		ArrayList<PromotionInfo> promos = new ArrayList<PromotionInfo>();

		if (returnedPromoCodeInfo != null) {
			for (PromotionInfo promoInfo : returnedPromoCodeInfo) {
				if ((promoInfo.getPromotionErrorMessages() == null) ||(promoInfo.getPromotionErrorMessages().getPromotionErrorMessage().isEmpty())) {
					iPromoCodeCount++;
					promos.add(promoInfo);
				}
			}
		}

		List<String> fields = new ArrayList<String>();
		IsiReporting isi = new IsiReporting();
		if (isBillPayment()) {
			
			Object[] billPaymentItems = {
					sendInfo.getDestinationCountry(),
					getBestDataValue(FieldKey.SENDER_FIRSTNAME_KEY),  
					getBestDataValue(FieldKey.SENDER_MIDDLENAME_KEY), 
					getBestDataValue(FieldKey.SENDER_LASTNAME_KEY), 
					getBestDataValue(FieldKey.SENDER_PRIMARYPHONE_KEY), 
					getBestDataValue(FieldKey.SENDER_ADDRESS_KEY), 
					getBestDataValue(FieldKey.SENDER_CITY_KEY), 
					DWValues.convertIsoSubdivToState(getBestDataValue(FieldKey.SENDER_COUNTRY_SUBDIVISIONCODE_KEY)), 
					getBestDataValue(FieldKey.SENDER_POSTALCODE_KEY), 
					getBestDataValue(FieldKey.SENDER_COUNTRY_KEY), 
					FormatSymbols.formatDateNoSeperatorCCYYMMDD(FormatSymbols.convertCCYYMMDDToDate(getBestDataValue(FieldKey.SENDER_DOB_KEY))),
		            getBestIdTypeValue(idTypes.ID1),
		            getBestIdNumber(idTypes.ID1),
					DWValues.convertIsoSubdivToState(getBestDataValue(FieldKey.SENDER_PERSONALID1_COUNTRY_SUBDIVISIONCODE_KEY)), 
					getBestDataValue(FieldKey.SENDER_PERSONALID1_ISSUE_COUNTRY_KEY), 
		            getBestIdTypeValue(idTypes.ID2),
		            getBestIdNumber(idTypes.ID2),
					maskString(getBestDataValue(FieldKey.BILLER_ACCOUNTNUMBER_KEY)),
					getBestOccupationValue(FieldKey.SENDER_OCCUPATION_KEY, FieldKey.SENDER_OCCUPATIONOTHER_KEY), 
					receiveCode,
					maskString(sendInfo.getAccount()),
					billerInfo.getBillerName(),
					"", // MADE " " for Bill payment.
					getBestDataValue(FieldKey.MESSAGEFIELD1_KEY), 
					getBestDataValue(FieldKey.MESSAGEFIELD2_KEY), 
					getBestDataValue(FieldKey.THIRDPARTY_SENDER_FIRSTNAME_KEY),
					getBestDataValue(FieldKey.THIRDPARTY_SENDER_MIDDLENAME_KEY),
					getBestDataValue(FieldKey.THIRDPARTY_SENDER_LASTNAME_KEY),
					getBestDataValue(FieldKey.THIRDPARTY_SENDER_ADDRESS_KEY),
					getBestDataValue(FieldKey.THIRDPARTY_SENDER_CITY_KEY),
					DWValues.convertIsoSubdivToState(getBestDataValue(FieldKey.THIRDPARTY_SENDER_COUNTRY_SUBDIVISIONCODE_KEY)),
					getBestDataValue(FieldKey.THIRDPARTY_SENDER_COUNTRY_KEY),
					getBestDataValue(FieldKey.THIRDPARTY_SENDER_POSTALCODE_KEY),
					getBestDataValue(FieldKey.THIRDPARTY_SENDER_PERSONALID1_TYPE_KEY),
					getBestDataValue(FieldKey.THIRDPARTY_SENDER_PERSONALID1_NUMBER_KEY),
					FormatSymbols.formatDateNoSeperatorCCYYMMDD(FormatSymbols.convertCCYYMMDDToDate(getBestDataValue(FieldKey.THIRDPARTY_SENDER_DOB_KEY))),
					getBestOccupationValue(FieldKey.THIRDPARTY_SENDER_OCCUPATION_KEY, FieldKey.THIRDPARTY_SENDER_OCCUPATIONOTHER_KEY),
					getBestDataValue(FieldKey.THIRDPARTY_SENDER_ORGANIZATION_KEY),
					bpValidationResponsePayload.getMgiRewardsNumber(),
					returnInfo.getCommitTransactionInfo() != null ? returnInfo.getCommitTransactionInfo().getPayload().getValue().getReferenceNumber() : "",
					bpValidationResponsePayload.getSendAmounts() != null ? bpValidationResponsePayload.getSendAmounts().getSendAmount() : "",
					returnInfo.getFeeSendTotalWithTax(true, false), 
					bpValidationResponsePayload.getSendAmounts().getTotalAmountToCollect(),
					bpValidationResponsePayload.getSendAmounts().getSendCurrency(), 
					bpValidationResponsePayload.getExchangeRateApplied() != null ? applyDoddFrank(bpValidationResponsePayload.getExchangeRateApplied().toString()) : "",
					DWValues.getProductVariantName(sendInfo.getProductVariant()),
					billerInfo.getReceiveAgentID(),
					getBestDataValue(FieldKey.RECEIVER_FIRSTNAME_KEY),
					getBestDataValue(FieldKey.RECEIVER_MIDDLENAME_KEY), 
					getBestDataValue(FieldKey.RECEIVER_LASTNAME_KEY), 
					sendInfo.getPurpose()
				};
			
			fields.add(MainPanelDelegate.clBillPayment);
			getIsiHeader(fields);
			for (int i = 0; i < billPaymentItems.length; i++) {
				fields.add(nullToSpace(billPaymentItems[i]));
			}
		} else {

			Object[] sendItems = {
					sendInfo.getDestinationCountry(),
					getBestDataValue(FieldKey.SENDER_FIRSTNAME_KEY),  
					getBestDataValue(FieldKey.SENDER_MIDDLENAME_KEY), 
					getBestDataValue(FieldKey.SENDER_LASTNAME_KEY), 
					getBestDataValue(FieldKey.SENDER_PRIMARYPHONE_KEY), 
					getBestDataValue(FieldKey.SENDER_ADDRESS_KEY), 
					getBestDataValue(FieldKey.SENDER_CITY_KEY), 
					DWValues.convertIsoSubdivToState(getBestDataValue(FieldKey.SENDER_COUNTRY_SUBDIVISIONCODE_KEY)), 
					getBestDataValue(FieldKey.SENDER_POSTALCODE_KEY), 
					getBestDataValue(FieldKey.SENDER_COUNTRY_KEY), 
					FormatSymbols.formatDateNoSeperatorCCYYMMDD(FormatSymbols.convertCCYYMMDDToDate(getBestDataValue(FieldKey.SENDER_DOB_KEY))),
					getBestDataValue(FieldKey.SENDER_BIRTH_CITY_KEY),
					getBestDataValue(FieldKey.SENDER_BIRTH_COUNTRY_KEY),
					getBestDataValue(FieldKey.SENDER_PERSONALID2_ISSUE_CITY_KEY),
					getBestDataValue(FieldKey.SENDER_PERSONALID2_ISSUE_COUNTRY_KEY),
					formatIsiDateField(getBestDataValue(FieldKey.SENDER_PERSONALID2_ISSUE_YEAR_KEY)),
		            getBestIdTypeValue(idTypes.ID1),
		            getBestIdNumber(idTypes.ID1),
					DWValues.convertIsoSubdivToState(getBestDataValue(FieldKey.SENDER_PERSONALID1_ISSUE_COUNTRY_SUBDIVISIONCODE_KEY)), 
					getBestDataValue(FieldKey.SENDER_PERSONALID1_ISSUE_COUNTRY_KEY), 
		            getBestIdTypeValue(idTypes.ID2),
		            getBestIdNumber(idTypes.ID2),
					maskString(getBestDataValue(FieldKey.INTERNATIONALBANKACCOUNTNUMBER_KEY)),
					getBestOccupationValue(FieldKey.SENDER_OCCUPATION_KEY, FieldKey.SENDER_OCCUPATIONOTHER_KEY), 
					getBestDataValue(FieldKey.RECEIVER_FIRSTNAME_KEY),
					getBestDataValue(FieldKey.RECEIVER_MIDDLENAME_KEY), 
					getBestDataValue(FieldKey.RECEIVER_LASTNAME_KEY), 
					getBestDataValue(FieldKey.RECEIVER_LASTNAME2_KEY), 
					getBestDataValue(FieldKey.RECEIVER_PRIMARYPHONE_KEY), 
					getBestDataValue(FieldKey.RECEIVER_ADDRESS_KEY), 
					getBestDataValue(FieldKey.DIRECTION1_KEY), 
					getBestDataValue(FieldKey.DIRECTION2_KEY), 
					getBestDataValue(FieldKey.DIRECTION3_KEY), 
					getBestDataValue(FieldKey.COLONIA_KEY), 
					getBestDataValue(FieldKey.MUNICIPIO_KEY), 
					getBestDataValue(FieldKey.RECEIVER_CITY_KEY), 
					DWValues.convertIsoSubdivToState(getBestDataValue(FieldKey.RECEIVER_COUNTRY_SUBDIVISIONCODE_KEY)), 
					getBestDataValue(FieldKey.RECEIVER_POSTALCODE_KEY), 
					getBestDataValue(FieldKey.TESTQUESTION_KEY), 
					getBestDataValue(FieldKey.TESTANSWER_KEY), 
					getBestDataValue(FieldKey.MESSAGEFIELD1_KEY), 
					getBestDataValue(FieldKey.MESSAGEFIELD2_KEY), 
					getBestDataValue(FieldKey.THIRDPARTY_SENDER_FIRSTNAME_KEY),
					getBestDataValue(FieldKey.THIRDPARTY_SENDER_MIDDLENAME_KEY),
					getBestDataValue(FieldKey.THIRDPARTY_SENDER_LASTNAME_KEY),
					getBestDataValue(FieldKey.THIRDPARTY_SENDER_ADDRESS_KEY),
					getBestDataValue(FieldKey.THIRDPARTY_SENDER_CITY_KEY),
					DWValues.convertIsoSubdivToState(getBestDataValue(FieldKey.THIRDPARTY_SENDER_COUNTRY_SUBDIVISIONCODE_KEY)),
					getBestDataValue(FieldKey.THIRDPARTY_SENDER_COUNTRY_KEY),
					getBestDataValue(FieldKey.THIRDPARTY_SENDER_POSTALCODE_KEY),
					getBestDataValue(FieldKey.THIRDPARTY_SENDER_PERSONALID2_TYPE_KEY),
					getBestDataValue(FieldKey.THIRDPARTY_SENDER_PERSONALID2_NUMBER_KEY),
					FormatSymbols.formatDateNoSeperatorCCYYMMDD(FormatSymbols.convertCCYYMMDDToDate(getBestDataValue(FieldKey.THIRDPARTY_SENDER_DOB_KEY))),
					getBestOccupationValue(FieldKey.THIRDPARTY_SENDER_OCCUPATION_KEY, FieldKey.THIRDPARTY_SENDER_OCCUPATIONOTHER_KEY),
					getBestDataValue(FieldKey.THIRDPARTY_SENDER_ORGANIZATION_KEY),
					(! this.isFormFreeTransaction()) ? this.feeInfo.getServiceOption() : getDataCollectionData().getCurrentValue(FieldKey.SERVICEOPTION_KEY.getInfoKey()),
					getBestDataValue(FieldKey.MGIREWARDSNUMBER_KEY),
					returnInfo.getCommitTransactionInfo() != null ? returnInfo.getCommitTransactionInfo().getPayload().getValue().getReferenceNumber() : "",
					returnInfo.getCommitTransactionInfo() != null ? returnInfo.getCommitTransactionInfo().getPayload().getValue().getFreePhoneCallPIN() : "",
					returnInfo.getCommitTransactionInfo() != null ? returnInfo.getCommitTransactionInfo().getPayload().getValue().getTollFreePhoneNumber() : "",
					sendValidationResponsePayload.getSendAmounts() != null ? sendValidationResponsePayload.getSendAmounts().getSendAmount() : "",
					returnInfo.getFeeSendTotalWithTax(false,false),
					"",// Discount Amount, reserved for future use.
					"", // Discount Type, reserved for future use.
					getTotalToCollect(), sendValidationResponsePayload.getSendAmounts().getSendCurrency(),
					sendValidationResponsePayload.getReceiveAmounts().getReceiveAmount(), 
					sendValidationResponsePayload.getReceiveAmounts().getPayoutCurrency(),
					sendValidationResponsePayload.getExchangeRateApplied() != null ? applyDoddFrank(sendValidationResponsePayload.getExchangeRateApplied().toString()) : -1,
					returnInfo.getCommitTransactionInfo() != null ? returnInfo.getCommitTransactionInfo().getPayload().getValue().getPartnerConfirmationNumber() : "",
					sendInfo.getReceiver().getRegistrationNumber(),
					returnInfo.getCommitTransactionInfo() != null ? TimeUtility.toStringDate(returnInfo.getCommitTransactionInfo().getPayload().getValue().getExpectedDateOfDelivery()).subSequence(0, 10) : "", 
					overrideID,
					returnInfo.getFeeSendMgiOrigNoTax(false,false),
					sendValidationResponsePayload.getSendAmounts().getTotalDiscountAmount(),
					sendValidationResponsePayload.getSendAmounts().getTotalSendFees(),
					sendValidationResponsePayload.getSendAmounts().getTotalSendTaxes(),
					Integer.toString(iPromoCodeCount),
					getBestDataValue(FieldKey.RECEIVER_COUNTRY_KEY),
					getBestDataValue(FieldKey.SENDER_LASTNAME2_KEY) 
				};
			
			fields.add(MainPanelDelegate.clMoneyGramSend);
			getIsiHeader(fields);
			for (int i = 0; i < sendItems.length; i++) {
				fields.add(nullToSpace(sendItems[i]));
			}
		}

		if (pm_iType == ISI_RCPT) {
			isi.print(fields, IsiReporting.ISI_LOG);
			if (MainPanelDelegate.getIsiExeCommand())
				isi.print(fields, IsiReporting.EXE_LOG);
		} else if (MainPanelDelegate.getIsiExe2Command()){
			isi.print(fields, IsiReporting.ISI2_LOG);
			isi.print(fields, IsiReporting.EXE2_LOG);
		}

		/*
		 * Add PromoCode records
		 */
		if (!isBillPayment()) {
			MainPanelDelegate.setIsiAppendMode(true);
			for (int ii = 0; ii < iPromoCodeCount; ii++) {
				fields = new ArrayList<String>();
				fields.add(MainPanelDelegate.clPromoCodeData);
				PromotionInfo pci = promos.get(ii);
				fields.add(pci.getPromotionCode());
				fields.add(pci.getPromotionCategoryId());
				fields.add(pci.getPromotionDiscountId());
				fields.add(pci.getPromotionDiscount() != null ?  pci.getPromotionDiscount().toString() : "0.00");
				fields.add(pci.getPromotionDiscount() != null ? pci.getPromotionDiscountAmount().toString() : "0.00");
				if (pm_iType == ISI_RCPT) {
					isi.print(fields, IsiReporting.ISI_LOG);
					if (MainPanelDelegate.getIsiExeCommand())
						isi.print(fields, IsiReporting.EXE_LOG);
				} else if (MainPanelDelegate.getIsiExe2Command()) {
					isi.print(fields, IsiReporting.ISI2_LOG);
					isi.print(fields, IsiReporting.EXE2_LOG);
				}
			}
			if ("Y".equals(UnitProfile.getInstance().get(DWValues.PROFILE_ISI_DF_AMTS, "N"))) {
				fields = createIsiRecord(addDfAmts(), MainPanelDelegate.clMGSendDetailedAmounts);
				if (pm_iType == ISI_RCPT) {
					isi.print(fields, IsiReporting.ISI_LOG);
					isi.print(fields, IsiReporting.EXE_LOG);
				} else if (MainPanelDelegate.getIsiExe2Command()){
					isi.print(fields, IsiReporting.ISI2_LOG);
					isi.print(fields, IsiReporting.EXE2_LOG);
				}
			}
			if ("Y".equals(UnitProfile.getInstance().get(DWValues.PROFILE_ISI_DF_TEXT, "N"))) {
				fields = createIsiRecord(addDfText1(), MainPanelDelegate.clSendReceiptText1);
				if (pm_iType == ISI_RCPT) {
					isi.print(fields, IsiReporting.ISI_LOG);
					isi.print(fields, IsiReporting.EXE_LOG);
					fields = createIsiRecord(addDfText2(), MainPanelDelegate.clSendReceiptText2);
					isi.print(fields, IsiReporting.ISI_LOG);
					isi.print(fields, IsiReporting.EXE_LOG);
				} else {
					if (MainPanelDelegate.getIsiExe2Command()){
						isi.print(fields, IsiReporting.ISI2_LOG);
						isi.print(fields, IsiReporting.EXE2_LOG);
					}
				}
			}
			if (pm_iType != ISI_RCPT) {
				MainPanelDelegate.setIsiAppendMode(bSavedAppendState);
			}
		}
	}

	@Override
	public boolean isFinancialTransaction() {
		return financialTransaction;
	}

	@Override
	public void setFinancialTransaction(boolean b) {
		financialTransaction = b;
	}

	@Override
	public boolean isCheckinRequired() {
		return !(DeltaworksStartup.getInstance().haveConnected());
	}

	public void setReceiverAccountNumber(String string) {
		sendInfo.setReceiverAccountNumber(string);
		dataCollectionData.setFieldValue(FieldKey.GFFP_TAG_ACCT_NBR_KEY, string);
	}

	public String getReceiverAccountNumber() {
		return sendInfo.getReceiverAccountNumber();
	}

	/**
	 * For DR Home delivery sends don't populate Address field in out going
	 * message.
	 */
	public void setDirection1(String string) {
		sendInfo.setDirection2(string);
		dataCollectionData.setFieldValue(FieldKey.DIRECTION1_KEY, string);
	}

	public String getDirection1() {
		return sendInfo.getDirection1();
	}

	public void setDirection2(String string) {
		sendInfo.setDirection2(string);
		dataCollectionData.setFieldValue(FieldKey.DIRECTION2_KEY, string);
	}

	public String getDirection2() {
		return sendInfo.getDirection2();
	}

	public void setDirection3(String string) {
		sendInfo.setDirection2(string);
		dataCollectionData.setFieldValue(FieldKey.DIRECTION3_KEY, string);
	}

	public String getDirection3() {
		return sendInfo.getDirection3();
	}
	/**
	 * Put the matching customers in the table. Also add an empty entry to allow
	 * agent to select none of the existing customers.
	 */
	public void populatePeopleTable(MGSHistoryPage page, List<CustomerInfo> people) {
		String senderName = null;
		String senderAddress = null;
		String recipientName = null;
		String country = null;

		if (people == null || people.size() == 0) {
			page.getTable().setEnabled(false);
			return;
		}

		String[] emptyRow = { getNoneOfAboveString(), "", "" };
		page.getTableModel().addRow(emptyRow);
		
		for (int i = 0; i < people.size(); i++) {

			String firstName = people.get(i).getSender().getFirstName();
			String lastName = people.get(i).getSender().getLastName();
			
			StringBuffer sb = new StringBuffer();
			if (lastName != null) {
				sb.append(lastName);
			}
			if ((firstName != null) && (! firstName.isEmpty())) {
				if (sb.length() > 0) {
					sb.append(", ");
				}
				sb.append(firstName);
			}
			senderName = sb.toString();
			
			String senderCity = people.get(i).getSender().getCity();
			String senderState = people.get(i).getSender().getState();
			senderAddress = senderCity + (senderState != null ? ", " + senderState : "");

			String receiverLastName1 = people.get(i).getReceiver().getLastName();
			String agentName = people.get(i).getCompanyInfo().getAgencyName();
			if ((receiverLastName1 == null) && (agentName == null)) { 
				recipientName = getOtherString();
				country = "";
			} else {
				String first = people.get(i).getReceiver().getFirstName() != null ? people.get(i).getReceiver().getFirstName() : "";
				String last1 = people.get(i).getReceiver().getLastName() != null ? people.get(i).getReceiver().getLastName() : "";
				String last2 = people.get(i).getReceiver().getSecondLastName() != null ? people.get(i).getReceiver().getSecondLastName() : "";
				recipientName = last1 + (! last2.isEmpty() ? " " : "") + last2  + ", " + first;
				country = people.get(i).getReceiver().getDestCountry();
			}

			ReceiverInfo receiver = people.get(i).getReceiver();
			ServiceOption serviceOption = ServiceOption.getServiceOption(receiver.getDeliveryOption(), receiver.getValue(FieldKey.DESTINATION_COUNTRY_KEY), receiver.getPayoutCurrency(), receiver.getReceiveAgentID());
			String serviceOptionDisplayName = serviceOption != null ? serviceOption.getServiceOptionDisplayName() : "";

			Object[] row = { senderName, senderAddress, recipientName, country, serviceOptionDisplayName, serviceOption};
			page.getTableModel().addRow(row);
		}

		page.getTable().setEnabled(true);
		page.getTable().getSelectionModel().setSelectionInterval(0, 0);
	}

	/**
	 * Put the matching customers in the table. Also add an empty entry to allow
	 * agent to select none of the existing customers.
	 */
	public void populateBillPaymentPeopleTable(MGSHistoryPage page, List<CustomerInfo> people, boolean addOther) {
		String senderName = null;
		String senderAddress = null;
		String billerName = null;
		String productVariant = null;

		if (people == null || people.size() == 0) {
			page.getTable().setEnabled(false);
			return;
		}

		String[] emptyRow = { getNoneOfAboveString(), "", "" };
		page.getTableModel().addRow(emptyRow);

		for (int i = 0; i < people.size(); i++) {
			
			String senderFirstName = people.get(i).getSender().getFirstName();
			String senderLastName = people.get(i).getSender().getLastName();

			if (senderLastName != null) {
				senderName = senderLastName + ", " + senderFirstName;
			} else {
				senderName = "";
			}
			
			String senderCity = people.get(i).getSender().getCity();
			String senderState = people.get(i).getSender().getState();

			if (senderCity != null) {
				senderAddress = senderCity + (senderState != null ? ", " + senderState : "");
			} else {
				senderAddress = "";
			}

			if (people.get(i).getCompanyInfo() != null) {
				billerName = people.get(i).getCompanyInfo().getValue(FieldKey.BILLER_NAME_KEY);
				productVariant = DWValues.getProductVariantName(people.get(i).getCompanyInfo().getProductVariant());
			}
			
			if (billerName == null || billerName.trim().isEmpty()) {
				if (! addOther) {
					continue;
				}
				billerName = Messages.getString("DWValues.BILLER_OTHER");
			}
			
			String[] row = { senderName, senderAddress, billerName, productVariant };
			page.getTableModel().addRow(row);
		}

		page.getTable().setEnabled(true);
		page.getTable().getSelectionModel().setSelectionInterval(0, 0);
	}

	public boolean isMoneySaverEnabled() {
		if ("Y".equals(UnitProfile.getInstance().get("MONEYSAVER_ENABLE", "Y"))) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isAutoEnrollEnabled() {
		if ("Y"
				.equals(UnitProfile.getInstance()
						.get("AUTO_ENROLL_OPTION", "N")))
			return true;
		else
			return false;
	}

	public boolean isRewardsActivationEnabled() {
		if ("Y"
				.equals(UnitProfile.getInstance()
						.get(DWValues.PROFILE_REWARDS_ACTIVATION, "N")))
			return true;
		else
			return false;
	}

	private BigDecimal getDetailSendItemAmt(SendAmountInfo sendAmountInfo, String amountType) {
		
		if (sendAmountInfo != null) {
			SendAmountInfo.DetailSendAmounts detailSendAmounts = sendAmountInfo.getDetailSendAmounts();
			if ( detailSendAmounts != null) {
				List<AmountInfo> amounts = detailSendAmounts.getDetailSendAmount();
				if (amounts != null) {
					for (AmountInfo amountInfo : amounts) {
						if (amountInfo.getAmountType().equalsIgnoreCase(amountType)) {
							return amountInfo.getAmount();
						}
					}
				}
			}
		}
		return null;
	}

	private BigDecimal getDetailSendItemAmt(FeeInfo feeInfo, String amountType) {
		if ((feeInfo) != null && (feeInfo.getSendAmounts() != null)) {
			return getDetailSendItemAmt(feeInfo.getSendAmounts(), amountType);
		}
		return null;
	}

	public boolean feeModified(FeeInfo fees) {
		BigDecimal discSendTotal = fees.getSendAmounts() != null ? fees.getSendAmounts().getTotalDiscountAmount() : null;
		BigDecimal taxSendTotal = fees.getSendAmounts() != null ? fees.getSendAmounts().getTotalSendTaxes() : null;
		return feeModified(discSendTotal, taxSendTotal, getDetailSendItemAmt(fees, DWValues.MF_SEND_DETAIL_OTHER_SEND_FEE));
	}

	public boolean feeModified(BigDecimal pm_sDisc, BigDecimal pm_sTax, BigDecimal pm_sFee) {
		return (((pm_sDisc!=null) && (pm_sDisc.compareTo(ZERO) != 0))
				|| ((pm_sTax!=null)	&& (pm_sTax.compareTo(ZERO) != 0))
				|| ((pm_sFee!=null) && (pm_sFee.compareTo(ZERO) != 0)));
	}

	public boolean recvModified(BigDecimal pm_sFees, BigDecimal pm_sTax) {
		return ((pm_sFees!=null) && (pm_sFees.compareTo(ZERO) != 0)
				|| (pm_sTax!=null) && (pm_sTax.compareTo(ZERO) != 0));
	}

	public List<String[]> getTableRows(List<FeeInfo> feeInfos) {
		List<String[]> rows = new ArrayList<String[]>();
		String delveryOption = null;
		String sendAmount = null;
		String fee = null;
		String fxrate = null;
		String receiveAmt = null;
		anyModifiedFees = false;
		anyModifiedRecvAmts = false;

		String serviceOptionCategoryCode;
		String serviceOptionCategoryName;

		if (feeInfo != null) {
		// handle null ptr with return empty list
		for (FeeInfo feeInfo : feeInfos) {
//			ServiceOption qdo = new ServiceOption();
//			qdo.setServiceOption(feeInfo.getServiceOption());
//
//			qdo.setReceiveAgentID(feeInfo.getReceiveAgentID());
//			qdo.setReceiveAgentAbbreviation(feeInfo.getReceiveAgentAbbreviation());
			ServiceOption qdo = new ServiceOption(feeInfo);
			serviceOptionCategoryCode = feeInfo.getServiceOptionCategoryId();
			serviceOptionCategoryName = feeInfo.getServiceOptionCategoryDisplayName();
			delveryOption = feeInfo.getServiceOptionDisplayName();

			boolean hasErrorData = feeInfo.getErrorInfo() != null; 
			boolean isRecoverable = hasErrorData ? feeInfo.getErrorInfo().isRecoverableFlag() : true; 
			if (isRecoverable) {
				String receiveCurrency = null;
				if (feeInfo.getReceiveAmounts() != null) {
						receiveCurrency = feeInfo.getReceiveAmounts().isValidCurrencyIndicator()
								? feeInfo.getReceiveAmounts().getReceiveCurrency()
								: feeInfo.getReceiveAmounts().getDisplayPayoutCurrency();				
				}
				qdo.setReceiveCurrency(receiveCurrency);
				qdo.setPayoutCurrency(getValidReceiveCurrency(feeInfo));

				sendAmount = feeInfo.getSendAmounts().getSendAmount().toString();
				fee = getDetailSendItemAmt(feeInfo, DWValues.MF_SEND_DETAIL_TOTAL_SEND_FEE_TAX).toString();

				boolean bFeeModified = feeModified(feeInfo);

				if (bFeeModified) {
					anyModifiedFees = true;
				}

				boolean isValid = feeInfo.getReceiveAmounts() != null ? feeInfo.getReceiveAmounts().isValidCurrencyIndicator() : false;
				if (isValid) {
					fxrate = feeInfo.getValidExchangeRate().toString();
					receiveAmt = feeInfo.getReceiveAmounts().getTotalReceiveAmount().toString() ;
				} else {
					fxrate = feeInfo.getEstimatedExchangeRate().toString();
					receiveAmt = feeInfo.getEstimatedReceiveAmount().toString();
				}

				if (isRecvAmtModified(feeInfo)) {
					receiveAmt = receiveAmt + "**";
					anyModifiedRecvAmts = true;
				}

				String[] row = {
						serviceOptionCategoryCode,
						serviceOptionCategoryName,
						delveryOption,
						qdo.getPayoutCurrency(),
						FormatSymbols.convertDecimal(sendAmount),
						FormatSymbols.convertDecimal(fee) + (bFeeModified ? "*" : ""),
						FormatSymbols.convertDecimal(fxrate),
						FormatSymbols.convertDecimal(receiveAmt) };
				rows.add(row);
			} else {
				qdo.setPayoutCurrency(feeInfo.getValidReceiveCurrency());
				qdo.setReceiveCurrency(feeInfo.getValidReceiveCurrency());
				sendAmount = getAmount().toString();

				String[] row = { serviceOptionCategoryCode, serviceOptionCategoryName, delveryOption, qdo.getPayoutCurrency(),
						FormatSymbols.convertDecimal(sendAmount), "", "", "" };
				rows.add(row);
			}
		}
		}
		return rows;
	}

	public String[] populateFeeOptionsTableWithSelectedRow(FeeInfo fees) {

		String delveryOption = null;
		String sendAmount = null;
		String fee = null;
		String fxrate = null;
		String receiveAmt = null;
		String[] row = null;
		String serviceOptionCategoryName =null;
		delveryOption = fees.getServiceOptionDisplayName();
		serviceOptionCategoryName = fees.getServiceOptionCategoryDisplayName();
		String serviceOptionCategoryCode = fees.getServiceOptionCategoryId();
		boolean hasErrorData = fees.getErrorInfo() != null; 
		boolean isRecoverable = hasErrorData ? fees.getErrorInfo().isRecoverableFlag() : isFeeQuery(); 
		if (isRecoverable) {
		fee = getDetailSendItemAmt(fees, DWValues.MF_SEND_DETAIL_TOTAL_SEND_FEE_TAX).toString();
		boolean isValid = fees.getReceiveAmounts() != null ? fees.getReceiveAmounts().isValidCurrencyIndicator() : false;
    	if (isValid) {
    		fxrate = MoneyGramSendTransaction.applyDoddFrank(fees.getValidExchangeRate().toString());
    	} else {
    		fxrate = MoneyGramSendTransaction.applyDoddFrank(fees.getEstimatedExchangeRate().toString());
    	}
		
		receiveAmt = fees.getValidReceiveAmount().toString();

		sendAmount = fees.getSendAmounts().getSendAmount().toString();

		boolean bFeeModified = feeModified(fees);

		if (bFeeModified) {
			anyModifiedFees = true;
		}

		if (isDoddFrankTrx()) {
			if (isRecvAmtModified(fees))
			receiveAmt = receiveAmt + "**";
		}
		
		row = new String[] { 
				serviceOptionCategoryCode,
				serviceOptionCategoryName,
				delveryOption, 
				getValidReceiveCurrency(fees),
				FormatSymbols.convertDecimal(sendAmount),
				FormatSymbols.convertDecimal(fee) + (bFeeModified ? "*" : ""),
				FormatSymbols.convertDecimal(fxrate),
				FormatSymbols.convertDecimal(receiveAmt) };
		} else {
			sendAmount =getAmount().toString();
			row =  new String[]{serviceOptionCategoryCode,serviceOptionCategoryName, delveryOption, getValidReceiveCurrency(fees),
					FormatSymbols.convertDecimal(sendAmount),
					"",
					"",
					"" };
		}
		return row;
	}

	public void setTsReferenceNumber(String aTsReferenceNumber) {
		tsReferenceNumber = aTsReferenceNumber;
	}

	public int getTranEntryType() {
		return tranEntryType;
	}

	public void setTranEntryType(int tranEntryType) {
		this.tranEntryType = tranEntryType;
	}

	public String getTsReferenceNumber() {
		return tsReferenceNumber;
	}

	public void setReceiverRegistration(boolean aReceiverRegistration) {
		receiverRegistration = aReceiverRegistration;
	}

	public boolean isReceiverRegistration() {
		return receiverRegistration;
	}

	public void setTransactionStatus(boolean aTransactionStatus) {
		transactionStatus = aTransactionStatus;
	}

	public boolean isTransactionStatus() {
		return transactionStatus;
	}

	public void setQualifiedDeliveryOption(ServiceOption qdo) {
		sendInfo.setQualifiedDeliveryOption(qdo, dataCollectionData);
	}

	public ServiceOption getQualifiedDeliveryOption() {
		return sendInfo.getQualifiedDeliveryOption();
	}


	public void setHistoryQDO(ServiceOption qdo) {
		sendInfo.setHistoryQDO(qdo);
	}


	public ServiceOption getHistoryQDO() {
		return sendInfo.getHistoryQDO();
	}

	/**
	 * @return Returns the registerClicked.
	 */
	public boolean isDestinationChanged() {
		return destinationChanged;
	}

	/**
	 * This method returns a flag which is set and reset when the register
	 * button is Clicked.
	 *
	 * @param registerClicked
	 *            The registerClicked to set.
	 */
	public void setDestinationChanged(boolean destinationChanged) {
		this.destinationChanged = destinationChanged;
	}

	public void clearFormFreeSendLookupInfo() {
		this.dataCollectionData.getFieldValueMap().clear();
	}

	public void clearFormFreeBPLookupInfo() {
		this.dataCollectionData.getFieldValueMap().clear();
	}

	public void setFormFreeSendEnabled(boolean b) {
		this.formFreeSendEnabled = b;
	}

	public boolean isFormFreeSendEnabled() {
		return this.formFreeSendEnabled;
	}
	
	public boolean isFormFreeTransaction() {
		return this.formFreeTransaction != null;
	}

	public BigDecimal getEmployeeMaxSendAmount() {

		return getEmployeeMaxSendAmount(agentBaseCurrency());
	}

	public BigDecimal getEmployeeMaxSendAmount(String currency) {

		try {
			if (getUserID() != 0)
				employeeSendLimit = userProfile
						.findCurrencyDecimalValue(DWValues.E_MAX_SEND_AMOUNT,
								currency); 
			else
				employeeSendLimit = new BigDecimal(0.00);
		} catch (NoSuchElementException e) {
			employeeSendLimit = new BigDecimal(0.00);
		} finally {
			if (employeeSendLimit.equals(new BigDecimal(0.00)))
				employeeSendLimit = getMaxItemAmount(currency);
		}
		return employeeSendLimit;
	}

	public boolean isMaxEmployeeLimitExceeded() {
		return maxEmployeeLimitExceeded;
	}

	public void setMaxEmployeeLimitExceeded(boolean maxEmployeeLimitExceeded) {
		this.maxEmployeeLimitExceeded = maxEmployeeLimitExceeded;
	}

	public int getManagerOverrideID() {
		return managerOverrideID;
	}

	public void setManagerOverrideID(int managerOverrideID) {
		this.managerOverrideID = managerOverrideID;
	}

	public String getAgentUseSendData() {
		// the five values will be concatenated and put into one string.
		StringBuffer buf = new StringBuffer(55);
		for (int i = 0; i < 55; i++)
			buf.append(" ");

		buf.replace(0, countryOfBirth.length(), countryOfBirth + ",");
		buf.replace(6, 6 + nationalityOfBirth.length(), nationalityOfBirth
				+ ",");
		buf.replace(13, 13 + currentNationality.length(), currentNationality
				+ ",");
		buf.replace(19, 19 + secondIdIssuanceDate.length(),
				secondIdIssuanceDate + ",");
		buf.replace(34, 34 + secondIdIssuanceExpirationDate.length(),
				secondIdIssuanceExpirationDate + ",");
		buf.replace(49, 49 + idVerifiedStored.length(), idVerifiedStored);
		return buf.toString();
	}

	public String getSecondIdIssuanceDate() {
		return secondIdIssuanceDate;
	}

	public void setSecondIdIssuanceDate(String secondIdIssuanceDate) {
		this.secondIdIssuanceDate = secondIdIssuanceDate;
	}

	public String getSecondIdIssuanceExpirationDate() {
		return secondIdIssuanceExpirationDate;
	}

	public void setSecondIdIssuanceExpirationDate(
			String secondIdIssuanceExpirationDate) {
		this.secondIdIssuanceExpirationDate = secondIdIssuanceExpirationDate;
	}

	/**
	 * @return Returns the countryOfBirth.
	 */
	public String getCountryOfBirth() {
		return countryOfBirth;
	}

	/**
	 * @param countryOfBirth
	 *            The countryOfBirth to set.
	 */
	public void setCountryOfBirth(String countryOfBirth) {
		this.countryOfBirth = countryOfBirth;
	}

	/**
	 * @return Returns the currentNationality.
	 */
	public String getCurrentNationality() {
		return currentNationality;
	}

	/**
	 * @param currentNationality
	 *            The currentNationality to set.
	 */
	public void setCurrentNationality(String currentNationality) {
		this.currentNationality = currentNationality;
	}

	/**
	 * @return Returns the idVerifiedStored.
	 */
	public String getIdVerifiedStored() {
		return idVerifiedStored;
	}

	/**
	 * @param idVerifiedStored
	 *            The idVerifiedStored to set.
	 */
	public void setIdVerifiedStored(String idVerifiedStored) {
		this.idVerifiedStored = idVerifiedStored;
	}

	/**
	 * @return Returns the nationalityOfBirth.
	 */
	public String getNationalityOfBirth() {
		return nationalityOfBirth;
	}

	/**
	 * @param nationalityOfBirth
	 *            The nationalityOfBirth to set.
	 */
	public void setNationalityOfBirth(String nationalityOfBirth) {
		this.nationalityOfBirth = nationalityOfBirth;
	}

	public void setSelectedBillerSearchButton(String selectedSearch) {
		this.billerSearchIndustryButton = selectedSearch;
	}

	public String getSelectedBillerSearchButton() {
		return billerSearchIndustryButton;
	}

	public void setSelectedBillerName(List<BillerInfo> selectedBillers) {
		this.selectedBillers = selectedBillers;
	}

	public List<BillerInfo> getSelectedBillerName() {
		return selectedBillers;
	}

	public String getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(String agencyId) {
		this.agencyId = agencyId;
	}

	public boolean isManagerOverrideFailed() {
		return managerOverrideFailed;
	}

	public void setManagerOverrideFailed(boolean managerOverrideFailed) {
		this.managerOverrideFailed = managerOverrideFailed;
	}

	/**
	 * Returns the billerNameList.
	 */
	public ArrayList<ArrayList<BillerInfo>> getBillerNameList() {
		return billerNameList;
	}

	/**
	 * The billerNameList to set.
	 */
	public void setBillerNameList(ArrayList<ArrayList<BillerInfo>> billerNameList) {
		this.billerNameList = billerNameList;
	}

	/**
	 * Returns the billerGroupingNeeded.
	 */
	public boolean isBillerGroupingNeeded() {
		return billerGroupingNeeded;
	}

	public void setSelectedFeeTableIndex(int index) {
		this.selectedFeeTableIndex = index;
	}

	public int getSelectedFeeTableIndex() {
		return this.selectedFeeTableIndex;
	}

	public void setMoneySaverID(String id) {
		this.freqCustCardNumber = id;
	}

	public String getMoneySaverID() {
		return freqCustCardNumber;
	}

	public void setActiveFreqCustCardNumber(String s) {
		this.activeFreqCustCardNumber = s;
	}

	public String getActiveFreqCustCardNumber() {
		return activeFreqCustCardNumber;
	}

	public String getConsumerId() {
		return consumerId;
	}

	public void setConsumerId(String id) {
		this.consumerId = id;
	}

	/**
	 * @return Returns the senderFirstName.
	 */
	public String getSenderFirstName() {
		return senderFirstName;
	}

	/**
	 * @param senderFirstName
	 *            The senderFirstName to set.
	 */
	public void setSenderFirstName(String senderFirstName) {
		this.senderFirstName = senderFirstName;
	}

	/**
	 * @return Returns the senderLastName.
	 */
	public String getSenderLastName() {
		return senderLastName;
	}

	/**
	 * @param senderLastName
	 *            The senderLastName to set.
	 */
	public void setSenderLastName(String senderLastName) {
		this.senderLastName = senderLastName;
	}

	public CustomerInfo getHistoricCustomerInfo() {
        if ((customerHistory != null) && (customerHistory.size() > 0)) {
			return customerHistory.get(0);
		}
		else{
			return null;
		}
	}

	public void setSmsPhoneNumber(String phoneNumber) {
		this.sPhoneNumber = phoneNumber;
	}

	public String getSmsPhoneNumber() {
		return sPhoneNumber;
	}

	public void setSmsCountryDialCode(String dialCode) {
		this.sCountryDialCode = dialCode;
	}

	public String getSmsCountryDialCode() {
		return sCountryDialCode;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public boolean isEnrolled() {
		return isEnrolled;
	}

	public void setEnrolled(boolean isEnrolled) {
		this.isEnrolled = isEnrolled;
	}

	public String getRewardsFirstName() {
		return rewardsFirstName;
	}

	public void setRewardsFirstName(String rewardsFirstName) {
		this.rewardsFirstName = rewardsFirstName;
	}

	public String getRewardsLastName() {
		return rewardsLastName;
	}

	public void setRewardsLastName(String rewardsLastName) {
		this.rewardsLastName = rewardsLastName;
	}

	public boolean isMatchingName(String actual, String supplied) {

		String first3CharsActual = "";
		String first3CharsSupplied = "";
		if (actual.length() >= 3)
			first3CharsActual = actual.substring(0, 3);
		else
			first3CharsActual = actual;

		if (supplied.length() >= 3)
			first3CharsSupplied = supplied.substring(0, 3);
		else
			first3CharsSupplied = supplied;

		return first3CharsActual.equalsIgnoreCase(first3CharsSupplied);
	}

	public boolean isTempCard() {
		return tempCard;
	}

	public void setTempCard(boolean tempCard) {
		this.tempCard = tempCard;
	}

	/**
	 * @return the dynamicAccountLabel
	 */
	public String getDynamicAccountLabel() {
		return sDynamicAccountLabel;
	}

	/**
	 * @param pm_sDynamicAccountLabel the dynamicAccountLabel to set
	 */
	public void setDynamicAccountLabel(String pm_sDynamicAccountLabel) {
		sDynamicAccountLabel = pm_sDynamicAccountLabel;
	}

	/**
	 * @return the sAccountNickName
	 */
	public String getAccountNickName() {
		return sAccountNickName;
	}

	/**
	 * @param pm_sAccountNickName the sAccountNickName to set
	 */
	public void setAccountNickName(String pm_sAccountNickName) {
		this.sAccountNickName = pm_sAccountNickName;
	}

	/**
	 * @return the sAccountNumberLabel
	 */
	public String getAccountNumberLabel() {
		String sTemp = this.sAccountNumberLabel;
		if ((sTemp == null) || (sTemp.length() == 0)) {
			sTemp = Messages.getString("Global.AcctNbrLabel");
		}
        if (sTemp.substring(sTemp.length() - 1).indexOf(":") == -1 ) {
        	sTemp = sTemp + ":";
        }
		return sTemp;
	}

	/**
	 * @param pm_sAccountNumberLabel the sAccountNumberLabel to set
	 */
	public void setAccountNumberLabel(String pm_sAccountNumberLabel) {
		this.sAccountNumberLabel = pm_sAccountNumberLabel;
	}

	/**
	 * @return the RRNLabel
	 */
	public String getRRNLabel() {
		String sTemp = this.sRRNLabel;
		if ((sTemp == null) || (sTemp.length() == 0)) {
			sTemp = Messages.getString("MGSWizardPage9a.RNPrompt");
		}
        if (sTemp.substring(sTemp.length() - 1).indexOf(":") == -1 ) {
        	sTemp = sTemp + ":";
        }
		return sTemp;
	}

	/**
	 * @param pm_sRRNLabel the RRNLabel to set
	 */
	public void setRRNLabel(String pm_sRRNLabel) {
		this.sRRNLabel = pm_sRRNLabel;
	}

	/**
	 * @return the anyModifiedFees
	 */
	public boolean isAnyModifiedFees() {
		return anyModifiedFees;
	}

	/**
	 * @return the anyModifiedRecvAmts state
	 */
	public boolean isAnyModifiedRecvAmts() {
		return anyModifiedRecvAmts;
	}

	/**
	 * @return the enteredPromoCodeCount
	 */
	public int getEnteredPromoCodeCount() {
		return enteredPromoCodeCount;
	}

	/**
	 * @param enteredPromoCodeCount the enteredPromoCodeCount to set
	 */
	public void setEnteredPromoCodeCount(int enteredPromoCodeCount) {
		this.enteredPromoCodeCount = enteredPromoCodeCount;
	}

	/**
	 * @return the enteredPromoCodes
	 */
	public String getEnteredPromoCodes() {
		return enteredPromoCodes;
	}

	/**
	 * @param promoCodes the enteredPromoCodes to set
	 */
	public void setEnteredPromoCodes(String promoCodes) {
		this.enteredPromoCodes = promoCodes;
	}

	/**
	 * @param pm_promoArray
	 *            The entered promotional codes as an array of String
	 */

	/**
	 * @param enteredPromoCodes
	 *            The entered promotional codes as an array of String
	 */
	public void setEnteredPromoCodeArray(String[] pm_promoArray) {
		sendInfo.setEnteredPromoArray(pm_promoArray);
		sendInfo.setPromoCodes(pm_promoArray);
	}

	/**
	 * @return the entered Promotional Codes as a PromoCode array
	 */
	public String[] getEnteredPromoCodeArray() {
		return sendInfo.getEnteredPromoArray();
	}

    /**
     * Gets the loyalty discount applied for the transaction.
     */
	public static BigDecimal getLoyaltyDiscAmt(List<PromotionInfo> promoCodeInfo) {

		BigDecimal bdLoyaltyDisc = BigDecimal.ZERO;
		boolean bFound = false;

		for (int ii = 0; ii < promoCodeInfo.size(); ii++) {
			final String iCatId = promoCodeInfo.get(ii).getPromotionCategoryId();
			if (iCatId.equals(DWValues.PROMO_CAT_LOYALTY_DISC)
					||iCatId.equals(DWValues.PROMO_CAT_WM_EMP_DISC)){
				BigDecimal sPromoDisc = promoCodeInfo.get(ii).getPromotionDiscountAmount();
				if (sPromoDisc!=null) {
					bdLoyaltyDisc = bdLoyaltyDisc.add(sPromoDisc);
					bFound = true;
				}
			}
		}

		if (!bFound) {
			return null;
		} else {
			return bdLoyaltyDisc;
		}
	}

    /**
     * Gets an array of valid (non-old loyalty) promotional codes
     */
	public static PromoCodeInfo[] getValidPromoCodes(List<PromotionInfo> promotionInfos) {
		PromoCodeInfo[] promoCodeInfo = new PromoCodeInfo[0];

		int iIdx = 0;
		int iBadIdx = 0;
		int[] iValidIndicies = new int[promoCodeInfo.length];
		int[] iErrorIndicies = new int[promoCodeInfo.length];

		for (int ii = 0; ii < promotionInfos.size(); ii++) {
			final String sErrCode = promotionInfos.get(ii).getPromotionErrorCode();
			if (!Report.notNull(sErrCode)) {
				final String iCatId = promotionInfos.get(ii).getPromotionCategoryId();
				if ((iCatId.equals(DWValues.PROMO_CAT_LOYALTY_DISC))
						&& (iCatId.equals(DWValues.PROMO_CAT_WM_EMP_DISC))){
					iValidIndicies[iIdx++] = ii;
				}
			} else {
				iErrorIndicies[iBadIdx++] = ii;
			}
		}
		PromoCodeInfo[] validPromoCodeInfo = new PromoCodeInfo[iIdx];
		for (int ii=0; ii < iIdx; ii++) {
			validPromoCodeInfo[ii] = promoCodeInfo[iValidIndicies[ii]];
		}

		badPromoInfo = new PromoCodeInfo[iBadIdx];
		for (int ii=0; ii < iBadIdx; ii++) {
			badPromoInfo[ii] = promoCodeInfo[iErrorIndicies[ii]];
		}

		return validPromoCodeInfo;
	}

	public MoneyGramInfo40 getReturnedFeeInfo(){
		return returnInfo;
	}

	/**
	 * @return the returnedPromoCodeCount
	 */
	public int getReturnedPromoCodeCount() {
		return returnedPromoCodeCount;
	}

	/**
	 * @param returnedPromoCodeCount the returnedPromoCodeCount to set
	 */
	public void setReturnedPromoCodeCount(int returnedPromoCodeCount) {
		this.returnedPromoCodeCount = returnedPromoCodeCount;
	}

	boolean promoCodeErrors(List<PromotionInfo> promoCodeInfo) {
		boolean bRetValue = false;

		for (int ii = 0; ii < promoCodeInfo.size(); ii++) {
			final String sErrCode = promoCodeInfo.get(ii).getPromotionErrorCode();
			if (Report.notNull(sErrCode)) {
				bRetValue = true;
			}
		}
		return bRetValue;

	}

	/**
	 * @return the bEditActive
	 */
	public boolean isEditActive() {
		return bEditActive;
	}

	/**
	 * @param bEditActive the bEditActive to set
	 */
	public void setEditActive(boolean editActive) {
		this.bEditActive = editActive;
	}

    /**
     * For Dodd-Frank, if it is a US agent, modify the exchange rate so that
     * it has exactly 4 decimal places.
     */
    public static String applyDoddFrank(String pm_sFxRate) {
    	String sFxRate = pm_sFxRate;
    	if (AgentCountryInfo.isUsAgent()) {
    		int iDecimaPt = sFxRate.lastIndexOf('.');
    		if (iDecimaPt > 0) {
    			int iDecDigits = sFxRate.length() - iDecimaPt;
    			if (iDecDigits > 4) {
        			sFxRate = sFxRate.substring(0,iDecimaPt+5);
    			} else if (iDecDigits < 4) {
    				for (int ii = 0; ii <= (4-iDecDigits); ii++){
    					sFxRate += "0";
    				}
    			}

    		} if (iDecimaPt < 0){
    			sFxRate = sFxRate +".0000";
    		}
    	}
        return sFxRate;
    }

    protected boolean isDoddFrankTrx() {
    	return (AgentCountryInfo.isUsAgent()
    			&& !DWValues.isUsOrUsTerr(destination.getCountryCode())
    			&& !billPayment);
    }

	public static void populateSendReceiptLanguageList() {
		Profile profile = UnitProfile.getInstance().getProfileInstance();

		try {
			bHasSingleReceiptLanguage = (profile.find(DWValues.PROFILE_RCPT_LANG_NUMBER).stringValue()
					.compareToIgnoreCase("singleLanguage") == 0);
		} catch (NoSuchElementException e) {
			bHasSingleReceiptLanguage = true;
		} catch (Exception e) {
			Debug.printException(e);
			Debug.dumpStack(e.getMessage());
		}

 		if (bHasSingleReceiptLanguage) {
 			// non-hybrid - so only one language specified to use for the receipt
 			bHasMultipleLanguages = false;
 			// primary language only - use list for selection
			receipt1stLanguageMnemonic = "";

 			// build list
			int iNbr1stLanguages = 0;
			try {
				List<String> receipt1stLanguageList = profile.findList(DWValues.PROFILE_RCPT_LANG);

				receipt1stLanguageMnemonicList = new ArrayList<String>();
				receipt2ndLanguageMnemonic = "";
				Iterator<String> i = receipt1stLanguageList.iterator();
				boolean skipFirst = true;  // formerly "eng" was reserved as first listed item				
				while (i.hasNext()) {
					String sLan = (i.next()).toUpperCase(Locale.US);
					if (skipFirst) {
						skipFirst = false;
						continue;
					}
					if ((sLan != null) && (sLan.compareToIgnoreCase(DWValues.PROFILE_RCPT_LANG_NONE) != 0)) {
						receipt1stLanguageMnemonicList.add(sLan);
						iNbr1stLanguages++;
					}
				}
			} catch (Exception e) {
				iNbr1stLanguages = 0;
				Debug.printException(e);
				Debug.dumpStack(e.getMessage());
			}

			if (iNbr1stLanguages == 0) {
				receipt1stLanguageMnemonicList.add(DWValues.MNEM_ENG);
				iNbr1stLanguages++;
			}

			if (iNbr1stLanguages == 1) {
				receipt1stLanguageMnemonic = receipt1stLanguageMnemonicList.get(0);
			}
 		} else {
 			// multiple language for hybrid receipts - primary is specified
 			int iNbr2ndLanguages = 0;

 			try {
 				receipt1stLanguageMnemonic = profile
 						.find(DWValues.PROFILE_RCPT_LANG_PRIMARY, 0).stringValue()
 						.toUpperCase(Locale.US);
 	       }
 	        catch(NoSuchElementException e){
 	        	receipt1stLanguageMnemonic = DWValues.MNEM_ENG;
 			} catch (Exception e) {
 				Debug.printException(e);
 				Debug.dumpStack(e.getMessage());
 			}

 			// "NONE" may be a valid selection for the additional language list
 			try {
 				receiptAllow2ndLanguageNone = (profile
 						.get(DWValues.PROFILE_RCPT_LANG_ALLOW_ONE).stringValue()
 						.compareToIgnoreCase("Y") == 0);
 	        }
 	        catch(NoSuchElementException e) {
 	        	receiptAllow2ndLanguageNone = false;
 			} catch (Exception e) {
 				Debug.printException(e);
 				Debug.dumpStack(e.getMessage());
 			}

 			try {
				List<String> receipt2ndLanguageList = profile.findList(DWValues.PROFILE_RCPT_LANG);

 				receipt2ndLanguageMnemonicList = new ArrayList<String>();
 				receipt2ndLanguageMnemonic = "";
 				Iterator<String> i = receipt2ndLanguageList.iterator();
				boolean skipFirst = true;  // formerly "eng" was reserved as first listed item
				while (i.hasNext()) {
					String sLan = (i.next()).toUpperCase();
					if (skipFirst) {
						skipFirst = false;
						continue;
					}
 					if ((sLan != null)
 							&& (sLan.compareToIgnoreCase(DWValues.PROFILE_RCPT_LANG_NONE) != 0)
 							&& (sLan.compareToIgnoreCase(receipt1stLanguageMnemonic) != 0)) {
 						receipt2ndLanguageMnemonicList.add(sLan);
 						iNbr2ndLanguages++;
 					}
 				}
 			} catch (Exception e) {
 				Debug.printException(e);
 				Debug.dumpStack(e.getMessage());
 			}

			bHasMultipleLanguages = ((iNbr2ndLanguages > 1) || (iNbr2ndLanguages == 1 && receiptAllow2ndLanguageNone));
			if (iNbr2ndLanguages == 1) {
				receipt2ndLanguageMnemonic = receipt2ndLanguageMnemonicList.get(0);
			}
		}
	}

	public static boolean allow2ndLangNone() {
		return receiptAllow2ndLanguageNone;
	}

	public static List<String> getSendReceiptAllLanguageList() {
		List<String> lRetValue = new ArrayList<String>();
		lRetValue.add(receipt1stLanguageMnemonic);
		Iterator<String> itr = receipt2ndLanguageMnemonicList.iterator();
		while (itr.hasNext()) {
			lRetValue.add(itr.next());
		}
		return lRetValue;
	}

    public static List<String> getSendReceipt2ndLanguageList(){
    	List<String> lRetList = new ArrayList<String>(receipt2ndLanguageMnemonicList);
    	return lRetList;
    }

    public int getSendReceipt2ndLanguageIdx(){
    	return receipt2ndLanguageIdx;
    }

    public static List<String> getSendReceipt1stLanguageList(){
    	if (receipt1stLanguageMnemonicList != null) {
	    	List<String> lRetList = new ArrayList<String>(receipt1stLanguageMnemonicList);
	    	return lRetList;
    	} else {
    		return new ArrayList<String>();
    	}
    }

    public String getSendReceiptend2ndLanguageMnemonic(){
    	return receipt2ndLanguageMnemonic;
    }

	public boolean isOKToPrintDF() {
		boolean bReturnValue = true;
		
		if (this.isReceiptReceived()) {

			// are there multiple selections available for primary language?
			List<String> pLanguageList = getSendReceipt1stLanguageList();
	 		if (bHasSingleReceiptLanguage && pLanguageList.size() > 1) {
				// dialog box for selection
				Receipt1stLanguageDialog dialog = new Receipt1stLanguageDialog(pLanguageList, receipt1stLanguageIdx);
				Dialogs.showPanel(dialog);
				if (dialog.isAccepted()) {
					receipt1stLanguageIdx = dialog.getLanguageIdx();
					receipt1stLanguageMnemonic = dialog.getLanguageMnemonic();
				} else {
					bReturnValue = false;
				}
			}
	
			// multiple selections available for secondary language
			if (bHasMultipleLanguages && bReturnValue) {
				// no Dodd-Frank constraint
				List<String> lLanguageList = getSendReceipt2ndLanguageList();
	
				if (allow2ndLangNone()) {
					lLanguageList.add(DWValues.MNEM_NONE);
				}
	
				Receipt2ndLanguageDialog dialog = new Receipt2ndLanguageDialog(lLanguageList, receipt2ndLanguageIdx);
				Dialogs.showPanel(dialog);
				if (dialog.isAccepted()) {
					receipt2ndLanguageIdx = dialog.getLanguageIdx();
					receipt2ndLanguageMnemonic = dialog.getLanguageMnemonic();
				} else {
					bReturnValue = false;
				}
	    	}
		}
    	return bReturnValue;
    }

	protected String getPrimaryReceiptSelectedLanguage() {
		return receipt1stLanguageMnemonic;
	}

	protected String getSecondaryReceiptSelectedLanguage() {
		return receipt2ndLanguageMnemonic;
	}

    protected boolean isRecvAmtModified(FeeInfo pm_feeInfo) {
    	if ((pm_feeInfo.getReceiveAmounts() == null) || (pm_feeInfo.getReceiveAmounts().getReceiveAmount() == null) || (pm_feeInfo.getReceiveAmounts().getTotalReceiveAmount() == null)) {
    		return false;
    	} else {
    		return (! pm_feeInfo.getReceiveAmounts().getReceiveAmount().equals(pm_feeInfo.getReceiveAmounts().getTotalReceiveAmount()));
    	}
    }

    /**
     * Does this MGS transaction need an auto confirmation slip.
     */
	public boolean isMGConfirmationSlip() {
    	/*
    	 * For Dodd-Frank the confirmation slip MUST be printed for international
    	 * transactions originating in the US unless Enable Receipts is set to Off (for DWAF).
    	 */
        return this.isAutoPrintSendConfirmation() || (isDoddFrankTrx() && this.isReceiveSendConfirmation());
    }

    protected boolean hasMultipleAdditonalLanguages() {
    	return isDoddFrankTrx() && bHasMultipleLanguages;
    }

	/**
	 * @return the receipt1stLanguageMnemonic
	 */
	public String getReceipt1stLanguageMnemonic() {
		return receipt1stLanguageMnemonic;
	}

	/**
	 * @return the receipt2ndLanguageMnemonic
	 */
	public String getReceipt2ndLanguageMnemonic() {
		return receipt2ndLanguageMnemonic;
	}


	public boolean wasDiscPrintSucessful() {
		boolean bRetValue = true;
		if (isDiscPdfPrimaryAvailable()) {
			if (!pdfReceiptDisc1.getLastPrintStatus()) {
				bRetValue = false;
			} else if (isDiscPdfSecondaryAvailable()) {
				if (!pdfReceiptDisc2.getLastPrintStatus()) {
					bRetValue = false;
				}
			}
		} else if (isDiscPrtPrimaryAvailable()) {
			if (!prtReceiptDisc1.getLastPrintStatus()) {
				bRetValue = false;
			} else if (isDiscPrtSecondaryAvailable()) {
				if (!prtReceiptDisc2.getLastPrintStatus()) {
					bRetValue = false;
				}
			}
		}
		return bRetValue;
	}

	private boolean isDiscPrtPrimaryAvailable() {
		return (prtReceiptDisc1 != null);
	}

	private boolean isDiscPrtSecondaryAvailable() {
		return (prtReceiptDisc2 != null);
	}

	private boolean isAgentPrtPrimaryAvailable() {
		return (prtReceiptAgent1 != null);
	}

	private boolean isCustPrtPrimaryAvailable() {
		return (prtReceiptCust1 != null);
	}

	private boolean isCustPrtSecondaryAvailable() {
		return (prtReceiptCust2 != null);
	}

	private boolean isDiscPdfPrimaryAvailable() {
		return (pdfReceiptDisc1 != null);
	}

	private boolean isDiscPdfSecondaryAvailable() {
		return (pdfReceiptDisc2 != null);
	}

	private boolean isAgentPdfPrimaryAvailable() {
		return (pdfReceiptAgent1 != null);
	}

	private boolean isCustPdfPrimaryAvailable() {
		return (pdfReceiptCust1 != null);
	}

	private boolean isCustPdfSecondaryAvailable() {
		return (pdfReceiptCust2 != null);
	}

	/**
	 * @return true if there was a failure printing receipts
	 */
	public void resetPrintFailure() {
		bPrintFailure = false;
	}

	/**
	 * @return true if there was a failure printing receipts
	 */
	public boolean wasPrintFailure() {
		return bPrintFailure;
	}

	private boolean cancelPrint(boolean pm_bReprint) {
		String sMessage, sMessage2, sMessage3, sMessage4;

		if (pm_bReprint) {
			sMessage = Messages.getString("DialogReceiptReprintFailurePost.Text1");
			sMessage2 = Messages.getString("DialogReceiptReprintFailurePost.Text2");
			sMessage3 = Messages.getString("DialogReceiptReprintFailurePost.Text3");
			sMessage4 = Messages.getString("DialogReceiptReprintFailurePost.Text4");
		} else {
			sMessage = Messages.getString("DialogReceiptPrintFailurePost.Text1");
			sMessage2 = Messages.getString("DialogReceiptPrintFailurePost.Text2");
			sMessage3 = Messages.getString("DialogReceiptPrintFailurePost.Text3");
			sMessage4 = Messages.getString("DialogReceiptPrintFailurePost.Text4");
		}

		boolean bRetValue = Dialogs.showPrintCancel("dialogs/BlankDialog3.xml",
				sMessage, sMessage2, sMessage3, sMessage4) != Dialogs.OK_OPTION;
		return bRetValue;
	}

	/**
	 * @return the isSendToAccount
	 */
	public boolean isSendToAccount() {
		return isSendToAccount;
	}

	/**
	 * @param isSendToAccount the isSendToAccount to set
	 */
	public void setSendToAccount(boolean isSendToAccount) {
		this.isSendToAccount = isSendToAccount;
	}

	/**
	 * @param accountDepositPartners the accountDepositPartners to set
	 */
	public void setAccountDepositPartners(boolean accountDepositPartners) {
		this.accountDepositPartners = accountDepositPartners;
	}

	public void setSelectedDeliveryInfo(ServiceOption deliveryInfo) {
		selectedDeliveryInfo=deliveryInfo;
	}

	public ServiceOption getSelectedDeliveryInfo() {
		return selectedDeliveryInfo;
	}

	/**
	 * @return the isServiceOptionChanged
	 */
	public boolean isServiceOptionChanged() {
		return isServiceOptionChanged;
	}

	/**
	 * @param isServiceOptionChanged the isServiceOptionChanged to set
	 */
	public void setServiceOptionChanged(boolean isServiceOptionChanged) {
		this.isServiceOptionChanged = isServiceOptionChanged;
	}

	public DataCollectionStatus getValidationStatus() {
		return dataCollectionData != null ? dataCollectionData.getValidationStatus() : DataCollectionStatus.NOT_VALIDATED;
	}

	@Override
	public void setValidationStatus(DataCollectionStatus status) {
		dataCollectionData.setValidationStatus(status);
	}

	public StagedTransactionInfo getFormFreeTransaction() {
		return this.formFreeTransaction;
	}

	public void setFormFreeTransaction(StagedTransactionInfo formFreeTransaction) {
		this.formFreeTransaction = formFreeTransaction;
	}

	public void searchStagedTransactions(MoneyGramSendFlowPage callingPage, final SessionType transactionType, final int commTag) {
		final MessageFacadeParam parameters = new MessageFacadeParam(
				MessageFacadeParam.REAL_TIME,
				MessageFacadeParam.USER_CANCEL);

		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {
				try {
					SearchStagedTransactionsResponse result = MessageFacade.getInstance().searchStagedTransactions(parameters, transactionType);
					if (result == null) {
						return Boolean.FALSE;
					}

					processFormFreeTransactions(result);
				} catch (MessageFacadeError mfe) {
					return Boolean.FALSE;
				} catch (Exception e) {
					Debug.printException(e);
					return Boolean.FALSE;
				}

				return Boolean.TRUE;
			}
		};
		MessageFacade.run(logic, callingPage, commTag, Boolean.FALSE);
	}

	public void clearFormFreeTransactionList() {
		if (this.formFreeTransactionList == null) {
			this.formFreeTransactionList = new ArrayList<StagedTransactionInfo>();
		} else {
			this.formFreeTransactionList.clear();
		}
	}

	public List<StagedTransactionInfo> getFormFreeTransactionList() {
		return this.formFreeTransactionList;
	}

	private void processFormFreeTransactions(SearchStagedTransactionsResponse response) {
		
		if ((response != null) && (response.getPayload() != null) && (response.getPayload().getValue() != null) && (response.getPayload().getValue().getStagedTransactionInfos() != null)) {
			this.formFreeTransactionList = response.getPayload().getValue().getStagedTransactionInfos().getStagedTransactionInfo();
		}
	}


	public Map<FieldKey, Object> getFormFreeSendLookupInfo() {
		return this.getDataCollectionData().getFieldValueMap();
	}


	public void setBillPaymentTransaction(boolean flag) {
		this.billPayment = flag;
	}

	public boolean isDataFromHistory() {
		return isDataFromHistory;
	}

	public void setDataFromHistory(boolean isDataFromHistory) {
		this.isDataFromHistory = isDataFromHistory;
	}

	public boolean isInternational() {
		return internationalEnable.equals("NON");
	}
	
	public String previousDataCollectionScreen(String currentPage) {
		if (! billPayment) {
			return previousSendDataCollectionScreen(currentPage);
		} else {
			return previousBpDataCollectionScreen(currentPage);
		}
	}

	private String previousSendDataCollectionScreen(String currentPage) {
		
		String nextPage = "";

		// Determine which screens can be shown based upon the current screen.
		
		boolean showSenderInformationScreen = false;
		boolean showSenderThirdPartyScreen = false;
		boolean showReceiverNameScreen = false;
		boolean showReceiverAccountInformation1Screen = false;
		boolean showReceiverAccountInformation2Screen = false;
		boolean showSupplementalInformationScreen = false;
		
		if (currentPage.equals(MoneyGramSendWizard.MSW30_IDENTIFICATION)) {
			
		} else if (currentPage.equals(MoneyGramSendWizard.MSW50_THIRD_PARTY)) {
			showSenderInformationScreen = senderInformationScreenStatus.hasDataToCollect();
			
		} else if (currentPage.equals(MoneyGramSendWizard.MSW60_RECIPIENT_DETAIL)) {
			showSenderInformationScreen = senderInformationScreenStatus.hasDataToCollect();
			showSenderThirdPartyScreen = senderThirdPartyScreenStatus.hasDataToCollect();

		} else if (currentPage.equals(MoneyGramSendWizard.MSW61_DIRECTED_SEND_DETAIL)) {
			showSenderInformationScreen = senderInformationScreenStatus.hasDataToCollect();
			showSenderThirdPartyScreen = senderThirdPartyScreenStatus.hasDataToCollect();
			
		} else if (currentPage.equals(MoneyGramSendWizard.MSW63_DIRECTED_SEND_PARTNER_DETAIL)) {
			showSenderInformationScreen = senderInformationScreenStatus.hasDataToCollect();
			showSenderThirdPartyScreen = senderThirdPartyScreenStatus.hasDataToCollect();
			showReceiverAccountInformation1Screen = senderInformationScreenStatus.hasDataToCollect();
		
		} else if (currentPage.equals(MoneyGramSendWizard.MSW64_SUPPLEMENTAL_SEND_INFORMATION)) {
			showSenderInformationScreen = senderInformationScreenStatus.hasDataToCollect();
			showSenderThirdPartyScreen = senderThirdPartyScreenStatus.hasDataToCollect();
			if (this.isDss()) {
				showReceiverAccountInformation1Screen = receiverAccountInformation1ScreenStatus.hasDataToCollect();
				showReceiverAccountInformation2Screen = receiverAccountInformation2ScreenStatus.hasDataToCollect();
			} else {
				showReceiverNameScreen = receiverNameScreenStatus.hasDataToCollect();
			}
			
		} else if (currentPage.equals(MoneyGramSendWizard.MRW67_ADDITIONAL_SEND_INFORMATION) || currentPage.equals(MoneyGramSendWizard.MSW65_SEND_VERIFICATION)) {
			showSenderInformationScreen = senderInformationScreenStatus.hasDataToCollect();
			showSenderThirdPartyScreen = senderThirdPartyScreenStatus.hasDataToCollect();
			showSupplementalInformationScreen = supplementalInformationScreenStatus.hasDataToCollect();
			if (this.isDss()) {
				showReceiverAccountInformation1Screen = receiverAccountInformation1ScreenStatus.hasDataToCollect();
				showReceiverAccountInformation2Screen = receiverAccountInformation2ScreenStatus.hasDataToCollect();
			} else {
				showReceiverNameScreen = receiverNameScreenStatus.hasDataToCollect();
			}
		}
		
		// Determine the previous data collection screen.
		
		if (this.isDss()) {
			if (showSupplementalInformationScreen) {
				nextPage = MoneyGramSendWizard.MSW64_SUPPLEMENTAL_SEND_INFORMATION;
				
			} else if (showReceiverAccountInformation2Screen) {
				nextPage = MoneyGramSendWizard.MSW63_DIRECTED_SEND_PARTNER_DETAIL;
			
			} else if (showReceiverAccountInformation1Screen) {
				nextPage = MoneyGramSendWizard.MSW61_DIRECTED_SEND_DETAIL; 

			} else if (showSenderThirdPartyScreen) {
				nextPage = MoneyGramSendWizard.MSW50_THIRD_PARTY;
					
			} else if (showSenderInformationScreen) {
				nextPage = MoneyGramSendWizard.MSW30_IDENTIFICATION; 
			}
		} else {
			if (showSupplementalInformationScreen) {
				nextPage = MoneyGramSendWizard.MSW64_SUPPLEMENTAL_SEND_INFORMATION;

			} else if (showReceiverNameScreen) {
				nextPage = MoneyGramSendWizard.MSW60_RECIPIENT_DETAIL;
				
			} else if (showSenderThirdPartyScreen) {
				nextPage = MoneyGramSendWizard.MSW50_THIRD_PARTY;
				
			} else if (showSenderInformationScreen) {
				nextPage = MoneyGramSendWizard.MSW30_IDENTIFICATION; 
			}
		}

		return nextPage;
	}

	private String previousBpDataCollectionScreen(String currentPage) {
		
		String nextPage = "";

		boolean showScreen25 = false;
		boolean showScreen30 = false;
		boolean showScreen36 = false;
		boolean showScreen38 = false;
		boolean showScreen39 = false;
		boolean showSupplementalInformationScreen = false;
		
		if (currentPage.equals(BillPaymentWizard.BPW30_COMPLIANCE)) {
			showScreen25 = this.screen25Status.hasDataToCollect();
			
		} else if (currentPage.equals(BillPaymentWizard.BPW38_PPC_THIRD_PARTY)) {
			showScreen36 = this.screen36Status.hasDataToCollect();
			
		} else if (currentPage.equals(BillPaymentWizard.BPW39_PPC_COMPLIANCE)) {
			showScreen36 = this.screen36Status.hasDataToCollect();
			showScreen38 = this.screen38Status.hasDataToCollect();

		} else if (currentPage.equals(BillPaymentWizard.BPW40_SUPPLEMENTAL_BP_INFORMATION)) {
			showScreen25 = this.screen25Status.hasDataToCollect();
			showScreen30 = this.screen30Status.hasDataToCollect();
			showScreen36 = this.screen36Status.hasDataToCollect();
			showScreen38 = this.screen38Status.hasDataToCollect();
			showScreen39 = this.screen39Status.hasDataToCollect();

		} else if (currentPage.equals(BillPaymentWizard.BPW50_ADDITIONAL_BP_INFORMATION) || currentPage.equals(BillPaymentWizard.BPW70_SEND_VERIFICATION)) {
			showScreen25 = this.screen25Status.hasDataToCollect();
			showScreen30 = this.screen30Status.hasDataToCollect();
			showScreen36 = this.screen36Status.hasDataToCollect();
			showScreen38 = this.screen38Status.hasDataToCollect();
			showScreen39 = this.screen39Status.hasDataToCollect();
			showSupplementalInformationScreen = supplementalInformationScreenStatus.hasDataToCollect();
		} 
		
		if (! isPrepayType()) {
			if (showSupplementalInformationScreen) {
				nextPage = BillPaymentWizard.BPW40_SUPPLEMENTAL_BP_INFORMATION;
			} else if (showScreen30) {
				nextPage = BillPaymentWizard.BPW30_COMPLIANCE;
			} else if (showScreen25) {
				nextPage = BillPaymentWizard.BPW25_SENDER_DETAIL;
			}  
		} else {
			if (showSupplementalInformationScreen) {
				nextPage = BillPaymentWizard.BPW40_SUPPLEMENTAL_BP_INFORMATION;
			} else if (showScreen39) {
				nextPage = BillPaymentWizard.BPW39_PPC_COMPLIANCE;
			} else if (showScreen38) {
				nextPage = BillPaymentWizard.BPW38_PPC_THIRD_PARTY;
			} else if (showScreen36) {
				nextPage = BillPaymentWizard.BPW36_PPC_SENDER_DETAIL;
			}
		}
		
		return nextPage;
	}
	
	@Override
	public String nextDataCollectionScreen(String currentPage) {
		if (! billPayment) {
			return nextSendDataCollectionScreen(currentPage);
		} else {
			return nextBpDataCollectionScreen(currentPage);
		}
	}

	private String nextSendDataCollectionScreen(String currentPage) {
		
		String nextPage = "";

		// Determine which screens can be shown based upon the current screen.
		
		boolean showSenderInformationScreen = false;
		boolean showSenderThirdPartyScreen = false;
		boolean showReceiverNameScreen = false;
		boolean showReceiverAccountInformationScreen = false;
		boolean showReceiverAccountIdentificationScreen = false;
		boolean showSupplementalInformationScreen = false;
		
		if (currentPage.equals(MoneyGramSendWizard.MSW10_MG_SEND_FEE_INFORMATION) || currentPage.equals(MoneyGramSendWizard.MSW08_FORM_FREE_TRANSACTION_SELECTION)) {
			showSenderInformationScreen = senderInformationScreenStatus.hasDataToCollect();
			showSenderThirdPartyScreen = senderThirdPartyScreenStatus.hasDataToCollect();
			if (! this.isDss()) {
				showReceiverNameScreen = receiverNameScreenStatus.hasDataToCollect();
			} else {
				showReceiverAccountInformationScreen = senderInformationScreenStatus.hasDataToCollect();
				showReceiverAccountIdentificationScreen = receiverAccountInformation2ScreenStatus.hasDataToCollect();
			}
			showSupplementalInformationScreen = supplementalInformationScreenStatus.hasDataToCollect();

		} else if (currentPage.equals(MoneyGramSendWizard.MSW30_IDENTIFICATION)) {
			showSenderThirdPartyScreen = senderThirdPartyScreenStatus.hasDataToCollect();
			if (! this.isDss()) {
				showReceiverNameScreen = receiverNameScreenStatus.hasDataToCollect();
			} else {
				showReceiverAccountInformationScreen = senderInformationScreenStatus.hasDataToCollect();
				showReceiverAccountIdentificationScreen = receiverAccountInformation2ScreenStatus.hasDataToCollect();
			}
			showSupplementalInformationScreen = supplementalInformationScreenStatus.hasDataToCollect();
			
		} else if (currentPage.equals(MoneyGramSendWizard.MSW50_THIRD_PARTY)) {
			if (! this.isDss()) {
				showReceiverNameScreen = receiverNameScreenStatus.hasDataToCollect();
			} else {
				showReceiverAccountInformationScreen = senderInformationScreenStatus.hasDataToCollect();
				showReceiverAccountIdentificationScreen = receiverAccountInformation2ScreenStatus.hasDataToCollect();
			}
			showSupplementalInformationScreen = supplementalInformationScreenStatus.hasDataToCollect();
			
		} else if (currentPage.equals(MoneyGramSendWizard.MSW60_RECIPIENT_DETAIL)) {
			showSupplementalInformationScreen = supplementalInformationScreenStatus.hasDataToCollect();

		} else if (currentPage.equals(MoneyGramSendWizard.MSW61_DIRECTED_SEND_DETAIL)) {
			showReceiverAccountIdentificationScreen = receiverAccountInformation2ScreenStatus.hasDataToCollect();
			showSupplementalInformationScreen = supplementalInformationScreenStatus.hasDataToCollect();
			
		} else if (currentPage.equals(MoneyGramSendWizard.MSW63_DIRECTED_SEND_PARTNER_DETAIL)) {
			showSupplementalInformationScreen = supplementalInformationScreenStatus.hasDataToCollect();
		}
		
		// If history data is been used to populate the data collection items and 
		// the user has not clicked a back button during the data collection process,
		// eliminate screens that shown not be shown if all the required data for 
		// the screen can be found in the history data.
		
		if (! this.isDataCollecionReview()) {
			showSenderInformationScreen = showSenderInformationScreen && senderInformationScreenStatus.requiredDataNotFullfilled();
			showSenderThirdPartyScreen = showSenderThirdPartyScreen && senderThirdPartyScreenStatus.requiredDataNotFullfilled();
			showReceiverNameScreen = showReceiverNameScreen && receiverNameScreenStatus.requiredDataNotFullfilled();
			showReceiverAccountInformationScreen = showReceiverAccountInformationScreen && receiverAccountInformation1ScreenStatus.requiredDataNotFullfilled();
			showReceiverAccountIdentificationScreen = showReceiverAccountIdentificationScreen && receiverAccountInformation2ScreenStatus.requiredDataNotFullfilled();
			showSupplementalInformationScreen = showSupplementalInformationScreen && supplementalInformationScreenStatus.requiredDataNotFullfilled();
		}
		
		// Check if more data needs to be collection and which screen needs to be displayed.
		
		if (this.isDss()) {
			if (showSenderInformationScreen) {
				nextPage = MoneyGramSendWizard.MSW30_IDENTIFICATION; 
				
			} else if (showSenderThirdPartyScreen) {
				nextPage = MoneyGramSendWizard.MSW50_THIRD_PARTY;
					
			} else if (showReceiverAccountInformationScreen) {
				nextPage = MoneyGramSendWizard.MSW61_DIRECTED_SEND_DETAIL; 
			
			} else if (showReceiverAccountIdentificationScreen) {
				nextPage = MoneyGramSendWizard.MSW63_DIRECTED_SEND_PARTNER_DETAIL;
			
			} else if (showSupplementalInformationScreen) {
				nextPage = MoneyGramSendWizard.MSW64_SUPPLEMENTAL_SEND_INFORMATION;
			}
		} else {
			if (showSenderInformationScreen) {
				nextPage = MoneyGramSendWizard.MSW30_IDENTIFICATION; 
				
			} else if (showSenderThirdPartyScreen) {
				nextPage = MoneyGramSendWizard.MSW50_THIRD_PARTY;
				
			} else if (showReceiverNameScreen) {
				nextPage = MoneyGramSendWizard.MSW60_RECIPIENT_DETAIL;
				
			} else if (showSupplementalInformationScreen) {
				nextPage = MoneyGramSendWizard.MSW64_SUPPLEMENTAL_SEND_INFORMATION;
			}
		}

		return nextPage;
	}

	private String nextBpDataCollectionScreen(String currentPage) {
		
		String nextPage = "";
		
		// Determine which screen can be displayed based upon the current
		// screen and which screens has data to be collected.

		boolean showScreen25 = false;
		boolean showScreen30 = false;
		boolean showScreen36 = false;
		boolean showScreen38 = false;
		boolean showScreen39 = false;
		boolean showSupplementalInformationScreen = false;
		
		if (currentPage.equals(BillPaymentWizard.BPW14_DB_MATCHING_COMPANIES_3) || currentPage.equals(BillPaymentWizard.BPW16_SENDER_SELECTION) || currentPage.equals(MoneyGramSendWizard.MSW08_FORM_FREE_TRANSACTION_SELECTION)) {
			showScreen25 = this.screen25Status.hasDataToCollect();
			showScreen30 = this.screen30Status.hasDataToCollect();
			showSupplementalInformationScreen = supplementalInformationScreenStatus.hasDataToCollect();

		} else if (currentPage.equals(BillPaymentWizard.BPW25_SENDER_DETAIL)) {
			showScreen30 = this.screen30Status.hasDataToCollect();
			showSupplementalInformationScreen = supplementalInformationScreenStatus.hasDataToCollect();
			
		} else if (currentPage.equals(BillPaymentWizard.BPW30_COMPLIANCE)) {
			showSupplementalInformationScreen = supplementalInformationScreenStatus.hasDataToCollect();
			
		} else if (currentPage.equals(BillPaymentWizard.BPW34_PPC_VERIFY)) {
			showScreen36 = this.screen36Status.hasDataToCollect();
			showScreen38 = this.screen38Status.hasDataToCollect();
			showScreen39 = this.screen39Status.hasDataToCollect();
			showSupplementalInformationScreen = supplementalInformationScreenStatus.hasDataToCollect();
			
		} else if (currentPage.equals(BillPaymentWizard.BPW36_PPC_SENDER_DETAIL)) {
			showScreen38 = this.screen38Status.hasDataToCollect();
			showScreen39 = this.screen39Status.hasDataToCollect();
			showSupplementalInformationScreen = supplementalInformationScreenStatus.hasDataToCollect();
			
		} else if (currentPage.equals(BillPaymentWizard.BPW38_PPC_THIRD_PARTY)) {
			showScreen39 = this.screen39Status.hasDataToCollect();
			showSupplementalInformationScreen = supplementalInformationScreenStatus.hasDataToCollect();
			
		} else if (currentPage.equals(BillPaymentWizard.BPW39_PPC_COMPLIANCE)) {
			showSupplementalInformationScreen = supplementalInformationScreenStatus.hasDataToCollect();
		}
		
		// If history data was used and the flow is not in data review, check
		// if any of the screens can be skipped.
		
		if ((this.isDataFromHistory() || this.isFormFreeTransaction()) && (! this.isDataCollecionReview())) {
			if (showScreen25 && isFormFreeTransaction()) {
				showScreen25 = this.screen25Status.requiredDataNotFullfilled();
			}
			if (showScreen30) {
				showScreen30 = this.screen30Status.requiredDataNotFullfilled();
			}
			if (showScreen36) {
				showScreen36 = this.screen36Status.requiredDataNotFullfilled();
			}
			if (showScreen38) {
				showScreen38 = this.screen38Status.requiredDataNotFullfilled();
			}
			if (showScreen39) {
				showScreen39 = this.screen39Status.requiredDataNotFullfilled();
			}
			if (showSupplementalInformationScreen) {
				showSupplementalInformationScreen = this.supplementalInformationScreenStatus.requiredDataNotFullfilled();
			}
		}
		
		// Determine which screen should be displayed next.
		
		if (! isPrepayType()) {
			if (showScreen25) {
				nextPage = BillPaymentWizard.BPW25_SENDER_DETAIL;
			} else if (showScreen30) {
				nextPage = BillPaymentWizard.BPW30_COMPLIANCE;
			} else if (showSupplementalInformationScreen) {
				nextPage = BillPaymentWizard.BPW40_SUPPLEMENTAL_BP_INFORMATION;
			}
		} else {
			if (showScreen36) {
				nextPage = BillPaymentWizard.BPW36_PPC_SENDER_DETAIL;
			} else if (showScreen38) {
				nextPage = BillPaymentWizard.BPW38_PPC_THIRD_PARTY;
			} else if (showScreen39) {
				nextPage = BillPaymentWizard.BPW39_PPC_COMPLIANCE;
			} else if (showSupplementalInformationScreen) {
				nextPage = BillPaymentWizard.BPW40_SUPPLEMENTAL_BP_INFORMATION;
			}
		}

		return nextPage;
	}
	
	public SendValidationResponse getSendValidationResponse() {
		return this.sendValidationResponse;
	}
	
	public SendValidationResponse.Payload getSendValidationResponsePayload() {
		return ((this.sendValidationResponse != null) && (this.sendValidationResponse.getPayload() != null)) ? this.sendValidationResponse.getPayload().getValue() : null;
	}
	
	public SendAmountInfo getSendAmounts() {
		return (this.getSendValidationResponsePayload() != null) ? this.getSendValidationResponsePayload().getSendAmounts() : null;
	}
	
	public ReceiveAmountInfo getReceiveAmounts() {
		return (this.getSendValidationResponsePayload() != null) ? this.getSendValidationResponsePayload().getReceiveAmounts() : null;
	}

	@Override
	public void validation(FlowPage callingPage) {
		if (billPayment) {
			this.bpValidation(callingPage, ValidationType.SECONDARY);
		} else {
			this.sendValidation(callingPage, ValidationType.SECONDARY);
		}
	}

	@Override
	public CustomerInfo getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(CustomerInfo customerInfo) {
		this.customerInfo = customerInfo;
	}
	
	public BPValidationResponse.Payload getBpValidationResponsePayload() {
		return this.bpValidationResponsePayload;
	}

	public String getMgiTransactionSessionId() {
		return this.mgiTransactionSessionId;
	}
	
	public void setMgiTransactionSessionId(String mgiSessionId) {
		this.mgiTransactionSessionId = mgiSessionId;
	}
	
	public void transactionLookup(final String refNumber, final CommCompleteInterface callingPage, final String purposeOfLookup, final boolean isInitial) {

		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {
		    	TransactionLookupRequest transactionLookupRequest = new TransactionLookupRequest();
				transactionLookupCommon(transactionLookupRequest, refNumber, null, callingPage, purposeOfLookup, isInitial);
				if ((purposeOfLookup.equals(DWValues.TRX_LOOKUP_SEND_COMP)) || (purposeOfLookup.equals(DWValues.TRX_LOOKUP_BP_COMP))) {
					String referenceNumber = transactionLookupRequest.getReferenceNumber();
					MoneyGramSendTransaction.this.setReferenceNumber(referenceNumber);
				}
				boolean status = isClientStatus();
				transactionLookupResponsePayload = null;
				if (getTransactionLookupResponse().getPayload() != null) {
					transactionLookupResponsePayload = getTransactionLookupResponse().getPayload().getValue();
				}

				if (transactionLookupResponsePayload != null) {
					String mgiTransactionSessionID = transactionLookupResponsePayload.getMgiSessionID();
					MoneyGramSendTransaction.this.setMgiTransactionSessionId(mgiTransactionSessionID);
				}

				// Save all of the values returned by the transactionLookup call
				// in the data collection set.

				createNewReceiveDetailInfo();
				// Save all of the values returned by the
				// transactionLookup call in the data collection set.
				if (detail != null && transactionLookupResponsePayload != null) {
					for (KeyValuePairType objCurrentValues : transactionLookupResponsePayload.getCurrentValues()
							.getCurrentValue()) {
						if (objCurrentValues != null) {
							String strXmlTag = objCurrentValues.getInfoKey();
							String strValue = objCurrentValues.getValue().getValue();

							if ((strXmlTag != null) && (strValue != null)) {
								if (FieldKey.RECEIVER_FIRSTNAME_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
									detail.setReceiverFirstName(strValue.trim());
								else if (FieldKey.RECEIVER_LASTNAME_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
									detail.setReceiverLastName(strValue.trim());
								else if (FieldKey.RECEIVER_MIDDLENAME_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
									detail.setReceiverMiddleName(strValue.trim());
								else if (FieldKey.RECEIVER_LASTNAME2_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
									detail.setReceiverLastName2(strValue.trim());
								else if (FieldKey.SENDER_FIRSTNAME_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
									detail.setSenderFirstName(strValue.trim());
								else if (FieldKey.SENDER_LASTNAME_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
									detail.setSenderLastName(strValue.trim());
								else if (FieldKey.SENDER_MIDDLENAME_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
									detail.setSenderMiddleName(strValue.trim());
								else if (FieldKey.SENDER_LASTNAME2_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
									detail.setSenderLastName2(strValue.trim());
								else if (FieldKey.SENDER_PRIMARYPHONE_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
									detail.setSenderHomePhone(strValue.trim());
								else if ("dateTimeSent".equalsIgnoreCase(strXmlTag))
									detail.setDateTimeSent(strValue.trim());
								else if (FieldKey.MESSAGEFIELD1_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
									detail.setMessageField1(strValue.trim());
								else if (FieldKey.MESSAGEFIELD2_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
									detail.setMessageField2(strValue.trim());
								else if (FieldKey.BILLER_ACCOUNTNUMBER_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
									detail.setBillerAccountNumber(strValue.trim());
							}

						}
					}
					detail.setTransactionStatus(transactionLookupResponsePayload.getTransactionStatus());
				}
				return status;
			}
		};
		MessageFacade.run(logic, callingPage, TRANSACTION_LOOKUP, Boolean.FALSE);
	}	

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	
	public String getReferenceNumber() {
		return this.referenceNumber;
	}

	public void completeSession(final FlowPage callingPage, final SessionType productType, final Request validationRequest, final String referenceNumber) {
		
		final MessageFacadeParam parameters = new MessageFacadeParam(MessageFacadeParam.REAL_TIME, MessageFacadeParam.USER_CANCEL);

		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {
				dataCollectionData.setFieldValue(FieldKey.MGISESSIONID_KEY, getTransactionLookupResponse().getPayload().getValue().getMgiSessionID());
				dataCollectionData.setFieldValue(FieldKey.MF_PROD_TYPE_KEY, productType);

				PdfReceipt.deletePdfFiles();
				PrtReceipt.deletePrtFiles();

				CompleteSessionRequest completeSessionRequest = new CompleteSessionRequest();
				completeSessionRequest.setMgiSessionID(getTransactionLookupResponse().getPayload().getValue().getMgiSessionID());
				completeSessionRequest.setMgiSessionType(productType);
				CompleteSessionResponse commitResponse = MessageFacade.getInstance().completeSession(parameters, completeSessionRequest, validationRequest, referenceNumber);
				if (returnInfo != null ) {
					returnInfo.setCommitTransactionInfo(commitResponse);
				}

				if (commitResponse == null) {
					tranSuccessful = false;
				} else {
					tranSuccessful = true;
					
					// Process and receipt data returned in the response.

					try {
						CompleteSessionResponse.Payload payload = commitResponse.getPayload().getValue();
						if ((payload.getReceipts() != null) && (payload.getReceipts().getReceiptMimeType() != null)) {
							if (payload.getReceipts().getReceiptMimeType().indexOf("/pdf") > 0) {
								if (payload.getReceipts().getAgentReceiptMimeData() != null) {
									pdfReceiptAgent1 = new PdfReceipt(payload.getReceipts().getAgentReceiptMimeData().getReceiptMimeDataSegment(), PdfReceipt.PDF_AGENT_1);
								}
								if (payload.getReceipts().getConsumerReceipt1MimeData() != null) {
									pdfReceiptCust1 = new PdfReceipt(payload.getReceipts().getConsumerReceipt1MimeData().getReceiptMimeDataSegment(), PdfReceipt.PDF_CUST_1);
								}
								if (payload.getReceipts().getConsumerReceipt2MimeData() != null) {
									pdfReceiptCust2 = new PdfReceipt(payload.getReceipts().getConsumerReceipt2MimeData().getReceiptMimeDataSegment(), PdfReceipt.PDF_CUST_2);
								}
								prtReceiptAgent1 = null;
								prtReceiptCust1 = null;
								prtReceiptCust2 = null;
							} else {
								if (payload.getReceipts().getAgentReceiptMimeData() != null) {
									prtReceiptAgent1 = new PrtReceipt(payload.getReceipts().getAgentReceiptMimeData().getReceiptMimeDataSegment(), PrtReceipt.PRT_AGENT_1);
								}
								if (payload.getReceipts().getConsumerReceipt1MimeData() != null) {
									prtReceiptCust1 = new PrtReceipt(payload.getReceipts().getConsumerReceipt1MimeData().getReceiptMimeDataSegment(), PrtReceipt.PRT_CUST_1);
								}
								if (payload.getReceipts().getConsumerReceipt2MimeData() != null) {
									prtReceiptCust2 = new PrtReceipt(payload.getReceipts().getConsumerReceipt2MimeData().getReceiptMimeDataSegment(), PrtReceipt.PRT_CUST_2);
								}
								pdfReceiptAgent1 = null;
								pdfReceiptCust1 = null;
								pdfReceiptCust2 = null;
							}
						}
						
					} catch (IOException e) {
						Debug.printException(e);
					}
					
					// Add an entry for the transaction in the ISI log file.
					
					logISIEntry();
					
					dataCollectionData.setValidationStatus(DataCollectionStatus.COMPLETED);
				}

				endOfSend();
				return commitResponse != null ? Boolean.TRUE : Boolean.FALSE;
			}
		};
		MessageFacade.run(logic, callingPage, COMPLETE_SESSION, Boolean.FALSE);
	}
	
	public void endOfSend() {
        synchronized (wait) {
			wait.setWaitFlag(false);
			wait.notifyAll();
        }
	}
	
	protected void logISIEntry() {
    	String checkOneType="";
    	String checkTwoType="";
		    	
//    	if (modf1 != null && getProfileMoneyGramChecks().equalsIgnoreCase("DG")){
//    		checkOneType = modf1.getDeltagramFlag();
//    	}
//    	
//    	if (modf2 != null && getProfileMoneyGramChecks().equalsIgnoreCase("DG")) {
//    		checkTwoType = modf2.getDeltagramFlag();
//    	}
    	
        Object[] receiveItems = {
         //  paidInfo.getReferenceNumber(),
        //    paidInfo.getAgentCheckNumber(),
        //    paidInfo.getAgentAmount(), 
        //    getAuthorizationCode(),
        //    paidInfo.getCustomerCheckNumber(),
        //    paidInfo.getCustomerAmount(),
        //    paidInfo.getCurrency(),
            getDataCollectionData().getCurrentValue("senderFirstName"),
            getDataCollectionData().getCurrentValue("senderMiddleName"),
            getDataCollectionData().getCurrentValue("senderLastName"),
            getDataCollectionData().getCurrentValue("receiverFirstName"),
            getDataCollectionData().getCurrentValue("receiverMiddleName"),
            getDataCollectionData().getCurrentValue("receiverLastName"),
            getDataCollectionData().getCurrentValue("receiverLastName2"),
            getDataCollectionData().getCurrentValue("receiverPrimaryPhone"),
            getDataCollectionData().getCurrentValue("receiverAddress"),
            getDataCollectionData().getCurrentValue("receiverCity"),
            getDataCollectionData().getCurrentValue("receiverCountrySubdivisionCode"),
            getDataCollectionData().getCurrentValue("receiverPostalCode"),
            getDataCollectionData().getCurrentValue("receiverPersonalId1Type"),
            getDataCollectionData().getCurrentValue("receiverPersonalId1Number"),
            getDataCollectionData().getCurrentValue("receiverPersonalId1CountrySubdivisionCode"),
            getDataCollectionData().getCurrentValue("receiverPersonalId1IssueCountry"),
            getDataCollectionData().getCurrentValue("receiverPersonalId2Type"),
            getDataCollectionData().getCurrentValue("receiverPersonalId2Number"),
            convertCCYYMMDDToDateYYYYMMDD(getDataCollectionData().getCurrentValue("receiverDOB")), 
            getDataCollectionData().getFieldStringValue("receiverOccupation"),
            getDataCollectionData().getCurrentValue("thirdPartyReceiverFirstName"),
            getDataCollectionData().getCurrentValue("thirdPartyReceiverLastName"),
            getDataCollectionData().getCurrentValue("thirdPartyReceiverAddress"),		
            getDataCollectionData().getCurrentValue("thirdPartyReceiverCity"),
            getDataCollectionData().getCurrentValue("thirdPartyReceiverCountrySubdivisionCode"),
            getDataCollectionData().getCurrentValue("thirdPartyReceiverPostalCode"),
            getDataCollectionData().getCurrentValue("thirdPartyReceiverCountry"),
            getDataCollectionData().getCurrentValue("thirdPartyReceiverPersonalId2Type"),
            getDataCollectionData().getCurrentValue("thirdPartyReceiverPersonalId2Number"),
            convertCCYYMMDDToDateYYYYMMDD(getDataCollectionData().getCurrentValue("thirdPartyReceiverDOB")),
            getDataCollectionData().getCurrentValue("thirdPartyReceiverOccupation"),
            getDataCollectionData().getCurrentValue("thirdPartyReceiverOrganization"),
            getDataCollectionData().getCurrentValue("cityOfBirth"),   
            getDataCollectionData().getCurrentValue("countryOfBirth"),
        //    paidInfo.getIdDateOfIssue() != null ? formatIsiDateField(paidInfo.getIdDateOfIssue()) : "",
        //    paidInfo.getIdCityOfIssue(),  
        //    paidInfo.getIdCountryOfIssue(),
            getDataCollectionData().getCurrentValue("originatingCountry"),
            checkOneType,
            checkTwoType,            
     //       this.isSendAmtNeeded() ? detail.getRedirectInfo().getOriginalSendAmount() : "",
     //       this.isSendFeeNeeded() ? detail.getRedirectInfo().getOriginalSendFee() : "",
     //       this.isSendAmtNeeded() || this.isSendFeeNeeded() ? detail.getRedirectInfo().getOriginalSendCurrency() : "",
     //       this.isFxNeeded() ? detail.getRedirectInfo().getOriginalExchangeRate() : "",
     //  		paidInfo.getOtherPayoutType(),
      // 		paidInfo.getOtherPayoutAmount(),
     //  		paidInfo.isOtherPayoutSwiped() ? "Y" : "N",
	//		getOtherPayoutIdLast4().substring(4),
			getDataCollectionData().getCurrentValue("senderAddress"),
			getDataCollectionData().getCurrentValue("senderCity"),
			getDataCollectionData().getCurrentValue("senderCountrySubdivisionCode"),
			getDataCollectionData().getCurrentValue("senderPostalCode"),
			getDataCollectionData().getCurrentValue("senderCountry"),
			getDataCollectionData().getCurrentValue("senderLastName2"),
			getDataCollectionData().getCurrentValue("senderPrimaryPhone"),
			getDataCollectionData().getCurrentValue("messageField1"),
			getDataCollectionData().getCurrentValue("destinationCountry")
        };

		List<String> fields = new ArrayList<String>();
		IsiReporting isi = new IsiReporting();
		fields.add(MainPanelDelegate.clMoneyGramReceive);
		getIsiHeader(fields);
		for (int i = 0; i < receiveItems.length; i++) {
			fields.add(nullToSpace(receiveItems[i]));
		}

		isi.print(fields, IsiReporting.ISI_LOG);
		if (MainPanelDelegate.getIsiExeCommand()) {
			isi.print(fields, IsiReporting.EXE_LOG);
		}
	}

	private String convertCCYYMMDDToDateYYYYMMDD(String date) {
		if ((date != null) && (date.length() == 8)) {
			return date.substring(0, 4) + date.substring(5, 6) + date.substring(7, 8);
		} else {
			return "";
		}
	} 
	
	public FeeInfo getCurrentFeeInfo() {
		return this.feeInfo;
	}
	
	private TransactionLookupResponse.Payload transactionLookupResponsePayload;

	@Override
	public TransactionLookupResponse.Payload getTransactionLookupResponsePayload() {
		return transactionLookupResponsePayload;
	}

	@Override
	public void applyTransactionLookupData() {
		for (DataCollectionField field : this.getDataCollectionData().getFieldMap().values()) {
			String infoKey = field.getInfoKey();
			String value = this.getDataCollectionData().getCurrentValue(infoKey);
			if ((value != null) && (! value.isEmpty())) {
				field.setDefaultValue(value);
				field.setValue(value);
			}
		}
	}
	
	public boolean haveFieldsToCollect() {
		if (! billPayment) {
			return senderInformationScreenStatus.requiredDataNotFullfilled() || senderThirdPartyScreenStatus.requiredDataNotFullfilled() || receiverNameScreenStatus.requiredDataNotFullfilled() || receiverAccountInformation1ScreenStatus.requiredDataNotFullfilled() || receiverAccountInformation2ScreenStatus.requiredDataNotFullfilled() || supplementalInformationScreenStatus.requiredDataNotFullfilled();
		} else {
			return screen25Status.requiredDataNotFullfilled() || screen30Status.requiredDataNotFullfilled() || supplementalInformationScreenStatus.requiredDataNotFullfilled();
		}
	}
	
	public boolean isAfterValidationAttempt() {
		return afterValidationAttempt;
	}

	public void setAfterValidationAttempt(boolean afterValidationAttempt) {
		this.afterValidationAttempt = afterValidationAttempt;
	}

	public void setIsDss(boolean isDss) {
		this.isDSS = isDss;
	}

	public DataCollectionScreenStatus getSenderInformationScreenStatus() {
		return senderInformationScreenStatus;
	}
	
	public DataCollectionScreenStatus getSenderThirdPartyScreenStatus() {
		return senderThirdPartyScreenStatus;
	}
	
	public DataCollectionScreenStatus getReceiverAccountInformation1ScreenStatus() {
		return receiverAccountInformation1ScreenStatus;
	}

	public DataCollectionScreenStatus getReceiverNameScreenStatus() {
		return receiverNameScreenStatus;
	}

	public DataCollectionScreenStatus getReceiverAccountInformation2ScreenStatus() {
		return receiverAccountInformation2ScreenStatus;
	}

	public DataCollectionScreenStatus getScreen25Status() {
		return screen25Status;
	}

	public DataCollectionScreenStatus getScreen30Status() {
		return screen30Status;
	}

	public DataCollectionScreenStatus getScreen36Status() {
		return screen36Status;
	}

	public DataCollectionScreenStatus getScreen38Status() {
		return screen38Status;
	}

	public DataCollectionScreenStatus getScreen39Status() {
		return screen39Status;
	}
	
	public SendValidationRequest getSendValidationRequest() {
		return this.sendValidationRequest;
	}
	
	@Override
	public void getConsumerProfile(final ConsumerProfile profile, String mgiSessionID, final int rule, final Map<String, String> currentValues, CommCompleteInterface cci) {
		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {
				getProfileSenderResponse = MessageFacade.getInstance().getProfileSender(profile, rule);
				
				if (getProfileSenderResponse != null) {
					GetProfileSenderResponse.Payload payload = getProfileSenderResponse.getPayload() != null ? getProfileSenderResponse.getPayload().getValue() : null; 
					if (payload != null) {
						profile.getInfoMap().clear();
						profile.addInfoValues(payload.getInfos() != null ? payload.getInfos().getInfo() : null);
						
						// Save away the response values if the AC call returned live data.
						
						boolean live = (rule == AgentConnect.ALWAYS_LIVE) || (! UnitProfile.getInstance().isTrainingMode());
						if (live) {
							String mgiSessionId = payload.getMgiSessionID();
							profile.setMgiSessionId(mgiSessionId);
							profile.addValues(payload.getCurrentValues().getCurrentValue());
							
							if (payload.getReceivers() != null) {
								profile.getReceivers().clear();
								profile.getOriginalReceivers().clear();
								for (CurrentValuesType entry : payload.getReceivers().getReceiver()) {
									profile.addReceiver(entry.getCurrentValue());
								}
							}
							
							if (payload.getBillers() != null) {
								profile.getBillers().clear();
								profile.getOriginalBillers().clear();
								for (CurrentValuesType entry : payload.getBillers().getBiller()) {
									profile.addBiller(entry.getCurrentValue());
								}
							}
						} else if (UnitProfile.getInstance().isTrainingMode() && (currentValues != null)) {
							profile.setCurrentValuesMap(currentValues);
						}
						if (getProfileObject() != null) {
							getProfileObject().setModified(false);
						}
					}
				}
				return (getProfileSenderResponse != null);
			}
		};
		MessageFacade.run(logic, cci, 0, Boolean.FALSE);
	}
	
	public GetProfileSenderResponse getProfileSenderResponse() {
		return this.getProfileSenderResponse;
	}
	
	@Override
	public boolean createOrUpdateConsumerProfile(ConsumerProfile profile, ProfileStatus status) {
		setProfileStatus(status);
		if (profile == null) {
			setProfileObject(new ConsumerProfile("UCPID"));
		}
		createOrUpdateProfileSenderResponse = CachedACResponses.getCreateOrUpdateProfileSenderResponse();
		if (createOrUpdateProfileSenderResponse != null) {
			List<BusinessError> errors = createOrUpdateProfileSenderResponse.getErrors() != null ? createOrUpdateProfileSenderResponse.getErrors().getError() : null;
			List<InfoBase> fieldsToCollectTagList = null;
			CreateOrUpdateProfileSenderResponse.Payload payload = createOrUpdateProfileSenderResponse.getPayload() != null ? createOrUpdateProfileSenderResponse.getPayload().getValue() : null;
			
			if (payload != null) {
				fieldsToCollectTagList = payload.getFieldsToCollect() != null ? payload.getFieldsToCollect().getFieldToCollect() : null;
			}
				
			this.setDataCollectionData(new DataCollectionSet(DataCollectionType.PROFILE_EDIT));
			processValidationResponse(ValidationType.INITIAL_NON_FORM_FREE, fieldsToCollectTagList, errors, false, null);
			return true;
		} else {
			Debug.println("Cached createOrUpdateProfileSender response invalid. Must transmit to refresh cache.");
			CachedACResponses.deleteObjectFile();
			DiagLog.getInstance().writeMustTransmit(MustTransmitDiagMsg.AWOL_LIMIT);
			UnitProfile.getInstance().setForceTransmit(true);
			Dialogs.showRequest("dialogs/DialogCannotCreateUpdateCustomerInformation.xml");
			return false;
		}
	}
	
	@Override
	public void createOrUpdateConsumerProfile(final ConsumerProfile profile, CommCompleteInterface cci) {
		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {
				boolean flag = getProfileStatus().equals(ProfileStatus.EDIT_PROFILE);
				createOrUpdateProfileSenderResponse = MessageFacade.getInstance().createOrUpdateProfileSender(profile, getDataCollectionData(), flag);
				
				if (createOrUpdateProfileSenderResponse != null) {
					List<BusinessError> errors = createOrUpdateProfileSenderResponse.getErrors() != null ? createOrUpdateProfileSenderResponse.getErrors().getError() : null;
					List<ConsumerProfileIDInfo> profileList = null;
					List<InfoBase> fieldsToCollectTagList = null;
					CreateOrUpdateProfileSenderResponse.Payload payload = createOrUpdateProfileSenderResponse.getPayload() != null ? createOrUpdateProfileSenderResponse.getPayload().getValue() : null;
					if (payload != null) {
						fieldsToCollectTagList = payload.getFieldsToCollect() != null ? payload.getFieldsToCollect().getFieldToCollect() : null;
						profileList = payload.getConsumerProfileIDs() != null ? payload.getConsumerProfileIDs().getConsumerProfileIDInfo() : null;
						
						if (profileList != null) {
							for (ConsumerProfileIDInfo info : profileList) {
								String id = info.getConsumerProfileID();
								String type = info.getConsumerProfileIDType();
								if (type.equals("TRANSIENT")) {
									profile.setProfileId(id);
									break;
								}
							}
						}
					}
					
					boolean isComplete = ((profileList != null) && (! profileList.isEmpty()));
					processValidationResponse(ValidationType.SECONDARY, fieldsToCollectTagList, errors, isComplete, null);

					if ((profileList != null) && (! profileList.isEmpty())) {
						setProfileStatus(ProfileStatus.PROFILE_COMPLETED);
					}
					
					for (DataCollectionField field : getDataCollectionData().getFieldMap().values()) {
						String value = field.getValue();
						if ((value == null) || (value.isEmpty())) {
							value = field.getDefaultValue();
						}
						if ((value != null) && (! value.isEmpty())) {
							profile.setValue(field.getInfoKey(), value);
						}
					}
				}
				
				return (createOrUpdateProfileSenderResponse != null);
			}
		};
		MessageFacade.run(logic, cci, CREATE_OR_UPDATE_PROFILES_SENDER, Boolean.FALSE);
	}
	
	public CreateOrUpdateProfileSenderResponse getCreateOrUpdateProfileSenderResponse() {
		return this.createOrUpdateProfileSenderResponse;
	}
	
	@Override
	public void applyProfileData(ConsumerProfile profile) {
		for (DataCollectionField field : this.getDataCollectionData().getFieldMap().values()) {
			String value = profile.getProfileValue(field.getInfoKey());
			if ((value != null) && (! value.isEmpty())) {
				field.setDefaultValue(value);
			}
		}
		
		if (profile.getCurrentReceiver() != null) {
			for (DataCollectionField field : this.getDataCollectionData().getFieldMap().values()) {
				String value = profile.getCurrentReceiver().getValues().get(field.getInfoKey());
				if ((value != null) && (! value.isEmpty())) {
					field.setDefaultValue(value);
				}
			}
		}
	}

	@Override
	public Response getConsumerProfileResponse() {
		return this.getProfileSenderResponse;
	}

	public void setValidationData(DataCollectionSet data) {
		this.dataCollectionData = data;
	}

	@Override
	public void applyCurrentData() {
		for (DataCollectionField field : this.getDataCollectionData().getFieldMap().values()) {
			String key = field.getInfoKey();
			String value = this.getDataCollectionData().getCurrentValue(key);
			if ((value != null) && (! value.isEmpty())) {
				field.setDefaultValue(value);
			}
		}
	}
	
	@Override
	public boolean isTransmitRequired(boolean checkCachedResponses) {
		boolean flag = super.isTransmitRequired(checkCachedResponses);
		if ((! flag) && (checkCachedResponses)) {
			flag = CachedACResponses.isSendResponsesInvalid();
		}
		return flag;
	}
	
	/*
	 * Return the best ID Type (taking into ID Verification) from first the data collection set, and if nothing there
	 * then from the profile
	 */
	private String getBestIdTypeValue(idTypes idNbr) {
		FieldKey sIDChoice;
		FieldKey fkIDType;
		
		if (idNbr == idTypes.ID1 ) {
			sIDChoice = FieldKey.SENDER_PERSONALID1_CHOICE_TYPE_KEY;
			fkIDType = FieldKey.SENDER_PERSONALID1_TYPE_KEY;
		} else {
			sIDChoice = FieldKey.SENDER_PERSONALID2_CHOICE_TYPE_KEY;
			fkIDType = FieldKey.SENDER_PERSONALID2_TYPE_KEY;
		}
		
		return getBestIdTypeValue(sIDChoice, fkIDType);
	}
	
	/*
	 * Return the best ID Number (taking into ID Verification) from first the data collection set, and if nothing there
	 * then from the profile
	 */
	private String getBestIdNumber(idTypes idNbr) {
		FieldKey fkIDNumber;
		FieldKey fkIDChoice;
		FieldKey fkVerify;
		
		if (idNbr == idTypes.ID1 ) {
			fkIDChoice = FieldKey.SENDER_PERSONALID1_CHOICE_TYPE_KEY;
			fkIDNumber = FieldKey.SENDER_PERSONALID1_NUMBER_KEY;
			fkVerify = FieldKey.SENDER_PERSONALID1_CHOICE_VERIFY_KEY;
		} else {
			fkIDChoice = FieldKey.SENDER_PERSONALID2_CHOICE_TYPE_KEY;
			fkIDNumber = FieldKey.SENDER_PERSONALID2_NUMBER_KEY;
			fkVerify = FieldKey.SENDER_PERSONALID2_CHOICE_VERIFY_KEY;
		}
		return getBestIdNumber(fkIDChoice, fkIDNumber, fkVerify);
	}
}
