package dw.mgsend;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.math.BigDecimal;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import dw.accountdepositpartners.AccountDepositPartnersTransaction;
import dw.dwgui.DWTable;
import dw.dwgui.DWTable.DWTableListener;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.DataCollectionSet.DataCollectionType;
import dw.framework.FieldKey;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.model.adapters.ConsumerProfile;
import dw.model.adapters.ConsumerProfile.EntryInfo;
import dw.utility.ServiceOption;

public class MGSWizardPage4 extends MoneyGramSendFlowPage implements MGSHistoryPage, DWTextPaneListener, DWTableListener {
	private static final long serialVersionUID = 1L;

	private static final int MIN_SCREEN_WIDTH = 500;
	
	private DWTable table = null;
	private DefaultTableModel tableModel = null;
    private JScrollPane scrollPane;
    private JPanel panel;
    private DWTextPane text1;
    private int lastSelectedItem = 0;
	private JButton nextButton;
	private JButton backButton;
	private JButton cancelButton;
	private JPanel informationPanel;
	private DWTextPane nameValue;
	private JLabel addressTag;
	private DWTextPane addressValue;
	private JLabel directionsTag;
	private DWTextPane directionsValue;
	private JLabel serviceOptionValue;
	private JLabel serviceOptionTag;
	private JLabel receiveCountryValue;
	private JLabel receiveCurrencyValue;
	private JLabel accountIdTag;
	private JLabel accountIdValue;
	private JLabel nameTag;
	private JLabel receiveCountryTag;
	private JLabel cityTag;
	private JLabel cityValue;

	public MGSWizardPage4(MoneyGramSendTransaction transaction, String name, String pageCode, PageFlowButtons buttons) {
		super(transaction, "moneygramsend/MGSWizardPage4.xml", name, pageCode, buttons);
		init();
	}

	public MGSWizardPage4(AccountDepositPartnersTransaction transaction, String name, String pageCode, PageFlowButtons buttons) {
		super(transaction, "moneygramsend/MGSWizardPage4.xml", name, pageCode, buttons);
		init();
	}
	
	private void init() {
		table = (DWTable) this.getComponent("table");
		scrollPane = (JScrollPane) this.getComponent("matchingCustomerScrollpane");
		panel = (JPanel) getComponent("panel");
		text1 = (DWTextPane) getComponent("text1");
		nextButton = flowButtons.getButton("next"); 
		backButton = flowButtons.getButton("back"); 
		cancelButton = flowButtons.getButton("cancel"); 
		informationPanel = (JPanel) getComponent("informationPanel");
		nameValue = (DWTextPane) getComponent("nameValue");
		serviceOptionValue = (JLabel) getComponent("serviceOptionValue");
		serviceOptionTag = (JLabel) getComponent("serviceOptionTag");
		receiveCountryValue = (JLabel) getComponent("receiveCountryValue");
		receiveCurrencyValue = (JLabel) getComponent("receiveCurrencyValue");
		accountIdTag = (JLabel) getComponent("accountIdTag");
		accountIdValue = (JLabel) getComponent("accountIdValue");
		nameTag = (JLabel) getComponent("nameTag");
		receiveCountryTag = (JLabel) getComponent("receiveCountryTag");
		addressTag = (JLabel) getComponent("addressTag");
		addressValue = (DWTextPane) getComponent("addressValue");
		cityTag = (JLabel) getComponent("cityTag");
		cityValue = (JLabel) getComponent("cityValue");
		directionsTag = (JLabel) getComponent("directionsTag");
		directionsValue = (DWTextPane) getComponent("directionsValue");
		
		setupTable();
		table.addListener(this, this);
	}

	@Override
	public void dwTableResized() {
		dwTextPaneResized();

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.resizeTable();
			} 
		});
	}

	@Override
	public void dwTextPaneResized() {
		int tableWidth = table.getTable().getPreferredSize().width;
		int screenWidth = scrollPane.getVisibleRect().width - 20;
		int actualWidth = Math.min(Math.max(tableWidth, MIN_SCREEN_WIDTH), screenWidth);
		
		text1.setWidth(actualWidth);
		
		int w1 = nameTag.getPreferredSize().width;
		int w2 = receiveCountryTag.getPreferredSize().width;
		int w3 = accountIdTag.getPreferredSize().width;
		int w4 = serviceOptionTag.getPreferredSize().width;
		int w5 = addressTag.getPreferredSize().width;
		int w6 = directionsTag.getPreferredSize().width;
		int w = actualWidth - Math.max(Math.max(w1, w2), Math.max(Math.max(w3, w4), Math.max(w5, w6))) - 15;
		nameValue.setWidth(w);
		addressValue.setWidth(w);
		directionsValue.setWidth(w);
	}

	@Override
	public JTable getTable() {
		return null;
	}

	@Override
	public DefaultTableModel getTableModel() {
		return null;
	}

	private void setupTable() {
		
		String[] headers = {
				Messages.getString("GeneralProfileOptions.1411"),
				Messages.getString("MGSWizardPage2.5"),
				Messages.getString("AccountDepositPartnersWizardPage1.4"),
				Messages.getString("MGSWizardPage4a.AccountNumber")
			}; 
		tableModel = new DefaultTableModel(headers, 0) {
			private static final long serialVersionUID = 1L;
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		
		table.getTable().setModel(tableModel);
        
		TableColumnModel model = table.getTable().getColumnModel();
		table.getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		table.getTable().getTableHeader().setReorderingAllowed(false);

		TableColumn col = model.getColumn(0);
    	col.setCellRenderer(table.new DWTableCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));

		col = model.getColumn(1);
    	col.setCellRenderer(table.new DWTableCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));

		col = model.getColumn(2);
    	col.setCellRenderer(table.new DWTableCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));

		col = model.getColumn(3);
    	col.setCellRenderer(table.new DWTableCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));
		
		KeyListener kl = new KeyAdapter() { 
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					e.consume();
					nextButton.doClick();
				}
				if (e.getKeyCode() == KeyEvent.VK_TAB) {
					e.consume();
					backButton.requestFocus();
				}
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					e.consume();
					cancelButton.doClick();
				}
				if (e.getKeyCode() == KeyEvent.VK_F2) {
					e.consume();
					transaction.f2SAR();
				}
			}
		};
		
		table.getTable().addKeyListener(kl);
		
		ListSelectionListener lsl = new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (e.getValueIsAdjusting()) {
					return;
				}

				int selectedRow = table.getTable().getSelectedRow();
				if (selectedRow > 0) {
					informationPanel.setVisible(true);
					
					ConsumerProfile profile = transaction.getProfileObject();
					EntryInfo entry = profile.getReceivers().get(selectedRow - 1);
					
					String name = entry.getReceiverName();
					nameValue.setText(name);
					
					String address = entry.getAddress();
					if (! address.isEmpty()) {
						addressTag.setVisible(true);
						addressValue.setVisible(true);
						addressValue.setText(address);
					} else {
						addressTag.setVisible(false);
						addressValue.setVisible(false);
					}
					
					String city = entry.getCity();
					if ((city != null) && (! city.isEmpty())) {
						cityTag.setVisible(true);
						cityValue.setVisible(true);
						cityValue.setText(city);
					} else {
						cityTag.setVisible(false);
						cityValue.setVisible(false);
					}
					
					String directions = entry.getDirections();
					if ((directions != null) && (! directions.isEmpty())) {
						directionsTag.setVisible(true);
						directionsValue.setVisible(true);
						directionsValue.setText(directions);
					} else {
						directionsTag.setVisible(false);
						directionsValue.setVisible(false);
					}
					
					String receiveCountry = entry.getValues().get("receive_Country");
					receiveCountryValue.setText(receiveCountry);
					
					String receiveCurrency = entry.getValues().get("receiveCurrency");
					receiveCurrencyValue.setText(receiveCurrency);
					
					String displayAccountID = entry.getValues().get("displayAccountID");
					if ((displayAccountID != null) && (! displayAccountID.isEmpty())) { 
						accountIdTag.setVisible(true);
						accountIdValue.setVisible(true);
						accountIdValue.setText("****" + displayAccountID);
					} else {
						accountIdTag.setVisible(false);
						accountIdValue.setVisible(false);
					}

//					String serviceOption = entry.getServiceOptionDescription();
					String serviceOption = entry.getServiceOption();
					if (! serviceOption.isEmpty()) {
						serviceOptionTag.setVisible(true);
						serviceOptionValue.setVisible(true);
						serviceOptionValue.setText(serviceOption);
					} else {
						serviceOptionTag.setVisible(false);
						serviceOptionValue.setVisible(false);
					}
					
					dwTextPaneResized();
				} else {
					informationPanel.setVisible(false);
				}
				dwTableResized();
			}
		};
		
        ListSelectionModel listSelectionModel = table.getTable().getSelectionModel();
        listSelectionModel.addListSelectionListener(lsl);
		
	}
	
	private void resetButtons() {
		flowButtons.reset();
		flowButtons.setVisible("expert", false);
	}
	
	@Override
	public void start(int direction) {
		resetButtons();
		
		if (direction == PageExitListener.NEXT) {

			table.getTable().getSelectionModel().clearSelection();
			clearComponent(table.getTable());
			
			tableModel.addRow(new String[] {Messages.getString("ClientTransaction.1922"), "", "", ""});
			ConsumerProfile profile = transaction.getProfileObject();
			List<EntryInfo> entryList = profile.getReceivers();
			for (EntryInfo entry : entryList) {
				String name = entry.getReceiverName();
				String destination = entry.getDestination();
				String serviceOption = entry.getServiceOption();
				String accountNumber = entry.getAccountNumber();
				tableModel.addRow(new String[] {name, destination, serviceOption, accountNumber});
			}
			
			table.initializeTable(scrollPane, panel, MIN_SCREEN_WIDTH, 10);
			table.setColumnStaticWidth(1);
			table.setColumnStaticWidth(2);
			table.setColumnStaticWidth(3);
			dwTableResized();
			table.getTable().getSelectionModel().setSelectionInterval(0, 0);
		} else {
			table.getTable().getSelectionModel().setSelectionInterval(lastSelectedItem, lastSelectedItem);
		}
		table.getTable().requestFocus();
	}

	@Override
	public void finish(int direction) {
		if (direction == PageExitListener.NEXT) {
			int selectedRow = table.getTable().getSelectedRow();
			lastSelectedItem = selectedRow;
			
			if (transaction.getDataCollectionData().getDataCollectionType().equals(DataCollectionType.VALIDATION)) {
				transaction.getDataCollectionData().getCurrentValueMap().clear();
			}
			
			if (selectedRow == 0) {
				transaction.getProfileObject().setReceiver(null);
				if (! transaction.isFeeQuery()) {
					transaction.setAmount(BigDecimal.ZERO);
				}
			} else {
				transaction.getProfileObject().setReceiverIndex(selectedRow - 1);
				
				if ((! transaction.isFeeQuery()) && (! transaction.isSendToAccount())) {
					EntryInfo receiver = transaction.getProfileObject().getCurrentReceiver();
					
					ServiceOption so = new ServiceOption();
					so.setServiceOption(receiver.getValues().get(FieldKey.SERVICEOPTION_KEY.getInfoKey()));
					so.setDestinationCountry(receiver.getValues().get(FieldKey.RECEIVE_COUNTRY_KEY.getInfoKey()));
					so.setReceiveCurrency(receiver.getValues().get(FieldKey.RECEIVE_CURRENCY_KEY.getInfoKey()));
					so.setReceiveAgentID(receiver.getValues().get(FieldKey.RECEIVE_AGENTID_KEY.getInfoKey()));
					
					transaction.setHistoryQDO(so);
				}
			}
		}
		PageNotification.notifyExitListeners(this, direction);
	}
}
