package dw.moneyorder;

import java.awt.Dimension;
import java.math.BigDecimal;
import java.text.MessageFormat;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import com.moneygram.agentconnect.MoneyOrderInfo;
import com.moneygram.agentconnect.MoneyOrderPrintStatusType;

import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;

/**
 * Page 2 of the Money Order Wizard.
 * This page prompts the user for additional money order amounts. When an amount
 *  is entered, this page will display again asking for another money order.
 *  When an amount of 0 is finally entered, the review page will be displayed.
 *  Based on a profile setting, the user may be able to specify whether or not a
 *  fee is to be charged for this money order.
 */
public class MOWizardPage2 extends MoneyOrderFlowPage {
	private static final long serialVersionUID = 1L;

    private int currentIndex = 1;

    private JTextField amountField = null;
    private JTextField totalField = null;
    private JRadioButton feeButton = null;
    private JRadioButton noFeeButton = null;
    private JPanel feePanel = null;
    private JLabel typeLabel = null;
    private String labelMainText;
    private String typeText;

    public MOWizardPage2(
        MoneyOrderTransaction40 tran,
        String name,
        String pageCode,
        PageFlowButtons buttons) {
        this((MoneyOrderTransaction) tran, name, pageCode, buttons);
    }

    public MOWizardPage2(
        MoneyOrderTransaction tran,
        String name,
        String pageCode,
        PageFlowButtons buttons) {
        super(tran, "moneyorder/MOWizardPage2.xml", name, pageCode, buttons);	
        // collect the component references
        labelMainText = Messages.getString("MOWizardPage2.1339")+"{0} {1}"+Messages.getString("MOWizardPage2.1340");
        amountField = (JTextField) getComponent("amountField");	
        totalField = (JTextField) getComponent("totalField");	
        noFeeButton = (JRadioButton) getComponent("noFeeButton");	
        feeButton = (JRadioButton) getComponent("feeButton");	
        feePanel = (JPanel) getComponent("feePanel");	
        typeLabel = (JLabel) getComponent("typeLabel");	

        // set up this page for VP vs MO
		if (transaction.isVendorPayment()) {
			removeComponent("feePanel");            
			typeText = Messages.getString("MOWizardPage2.vendorPayment");
		} else {
			typeText = Messages.getString("MOWizardPage2.moneyOrder");
		}
    }

    /**
     * Override of clear() in XmlDefinedPanel. Reset the index back to 1 to
     *   make sure we start with the correct index for additional mos.
     */
    @Override
	public void clear() {
        super.clear();
        currentIndex = 1;
    }
    
    private String getLabelText(int index) {
    	switch (index) {
    	case 1: return Messages.getString("MOWizardPage2.second");
    	case 2: return Messages.getString("MOWizardPage2.third");
    	case 3: return Messages.getString("MOWizardPage2.fourth");
    	case 4: return Messages.getString("MOWizardPage2.fifth");
    	case 5: return Messages.getString("MOWizardPage2.sixth");
    	case 6: return Messages.getString("MOWizardPage2.seventh");
    	case 7: return Messages.getString("MOWizardPage2.eighth");
    	case 8: return Messages.getString("MOWizardPage2.ninth");
    	case 9: return Messages.getString("MOWizardPage2.tenth");
    	case 10: return Messages.getString("MOWizardPage2.eleventh");
    	case 11: return Messages.getString("MOWizardPage2.twelfth");
    	case 12: return Messages.getString("MOWizardPage2.thirteenth");
    	case 13: return Messages.getString("MOWizardPage2.fourteenth");
    	case 14: return Messages.getString("MOWizardPage2.fifteenth");
    	default: return Messages.getString("MOWizardPage2.additional");
    	}
    }
    
    @Override
	public void start(int direction) {
        int numOrders = ((MoneyOrderTransaction40) transaction).getMoneyOrderList().size();

        totalField.setFocusable(false);

        // if we have just split a money order, make sure we jump past the extra
        //   generated MOs to the next empty one.
        if (transaction.isLastEntrySplit()) {
            currentIndex = numOrders;
            transaction.unsetLastEntrySplit();
        }
        // following check is for max forms reached after the first screen

        if (currentIndex >= transaction.getMaxFormsPerSale()) {
            currentIndex = 1;
            PageNotification.notifyExitListeners(this, direction);
            return;
        }

        flowButtons.reset();
        flowButtons.setVisible("expert", false);	
        flowButtons.setEnabled("expert", false);	

        SwingUtilities.invokeLater(new Runnable() {
            @Override
			public void run() {
                amountField.requestFocus();
            }
        });

        if (direction == PageExitListener.BACK) {
            currentIndex = numOrders == 1 ? 1 : numOrders - 1;
        }
        String boldTxt ="<b>" + getLabelText(currentIndex) + "</b>";
        
        String label = MessageFormat.format(labelMainText, boldTxt, typeText);
        String labelHtml = "<html>"+label+"</html>";
        Dimension dm = ((JPanel) getComponent("feePanel")).getSize();
        JPanel amtPanel = (JPanel) getComponent("amount");
        if (amtPanel.getSize().width > dm.width) {
        	dm.width = amtPanel.getSize().width;
        }
        if (amtPanel.getSize().height > dm.height) {
        	dm.height = 3 * amtPanel.getSize().height;
        }

        typeLabel.setPreferredSize(dm);
        typeLabel.setText(labelHtml);	
        // totalField.setText(transaction.getTotal(
        //                        MoneyOrderTransaction.ALL, true).toString());
        totalField.setText(transaction.getTotal(MoneyOrderPrintStatusType.PROPOSED, true).toString());
        MoneyOrderInfo mo = transaction.getMoneyOrderInfo(currentIndex);
        amountField.setText(mo.getItemAmount().toString());
        //Renuka - if we do this then the MO has to be created with a fee >0.00
        //use the product level profile option FEE_ENABLED insted.
        //     if (new BigDecimal(mo.getItemFee()).compareTo(ZERO) == 0)
        if(transaction.isFeeEnabled()){
            if(mo.getItemAmount().equals(BigDecimal.ZERO))
                feeButton.setSelected(true);
            else{
                if(mo.getItemFee().equals(BigDecimal.ZERO))
                    noFeeButton.setSelected(true);
                else
                    feeButton.setSelected(true);
            }
        }
        else
            noFeeButton.setSelected(true);

        if (transaction.isVendorPayment())
            setComponentEnabled(feePanel, false);
        else
            setComponentEnabled(feePanel, transaction.isNoFeeAvailable());

        addActionListener("amountField", this, "amountFieldAction");	 

    }

    @Override
	public void finish(int direction) {
        // get the info from the components and update the money order info
        BigDecimal amount = new BigDecimal(amountField.getText());

        if (direction == PageExitListener.NEXT) {
            String payeeName =
                transaction.isVendorPayment() ? transaction.getSelectedVendor() : null;
            if (!setInTransaction(amount, feeButton.isSelected(), payeeName, currentIndex)) {
                flowButtons.setButtonsEnabled(true);
                // since setintransaction failed, set focus back to the amount field.
                amountField.requestFocus();
                return;
            }

            if (amount.compareTo(BigDecimal.ZERO) != 0) {
                // value is present, re-enter page again with index++
                this.cleanup(); // dont want multiple listeners
                currentIndex++;
                if (((MoneyOrderTransaction40) transaction).getMoneyOrderList().size()
                    < transaction.getMaxFormsPerSale()) {
                    start(PageExitListener.NEXT);
                    return;
                }
            }
        } else if (direction == PageExitListener.BACK) {
            if (currentIndex > 1) {
                // move index back and re-enter page
                this.cleanup(); // dont want multiple listeners
                currentIndex--;
                start(PageExitListener.NEXT);
                return;
            }
        }
        currentIndex = 1;
        PageNotification.notifyExitListeners(this, direction);
    }

    public void amountFieldAction() {
        flowButtons.getButton("next").doClick();	
    }
}
