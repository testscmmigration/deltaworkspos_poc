package dw.moneyorder;

import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import dw.dialogs.Dialogs;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;

/**
 * Page 4 of the Money Order Wizard.
 * This page displays the amounts of the money orders, their fees, and the
 *   total amount to collect from the customer. Based on a profile setting,
 *   the user may enter the amount tendered in order to get a change amount.
 *   When the print button is activated, the money orders will print and a
 *   progress window will display to show the status of the operation.
 */
public class MOWizardPage4 extends MoneyOrderFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

    private JTextField totalField = null;
    private JTextField tenderedField = null;
    private JTextField changeField = null;
    private JPanel tenderedInnerPanel = null;
    private JPanel tenderedPanel = null;
    // private static boolean tenderedFlag = true;
    private static boolean tenderedFlag = false;
    private JLabel instrLabel = null;

    public MOWizardPage4(
        MoneyOrderTransaction40 tran,
        String name,
        String pageCode,
        PageFlowButtons buttons) {
        this((MoneyOrderTransaction) tran, name, pageCode, buttons);
    }

    public MOWizardPage4(
        MoneyOrderTransaction tran,
        String name,
        String pageCode,
        PageFlowButtons buttons) {
        super(tran, "moneyorder/MOWizardPage4.xml", name, pageCode, buttons);	
        totalField = (JTextField) getComponent("collectField");	
        tenderedField = (JTextField) getComponent("tenderedField");	
        changeField = (JTextField) getComponent("changeField");	
        tenderedPanel = (JPanel) getComponent("tenderedPanel");	
        tenderedInnerPanel = (JPanel) getComponent("tenderedInnerPanel");	
        instrLabel = (JLabel) getComponent("text1");	

		DWTextPane tp2 = (DWTextPane) getComponent("text2");
		tp2.addListener(this, this);
    }

	@Override
	public void dwTextPaneResized() {
		DWTextPane tp2 = (DWTextPane) getComponent("text2");
		JPanel panel = (JPanel) getComponent("totalToCollect");
		int offset=45;
		tp2.setWidth((panel.getSize().width+offset));
		tp2.setListenerEnabled(false);
	}

    @Override
	public void start(int direction) {
        flowButtons.reset();
        flowButtons.setVisible("expert", false);	
        flowButtons.setEnabled("expert", false);	
        flowButtons.setAlternateText("next", Messages.getString("MOWizardPage4.printText"));	 
        flowButtons.setAlternateKeyMapping("next", KeyEvent.VK_F5, 0);	
        totalField.setText(transaction.getTotal(null, true).toString());
        BigDecimal tenderedAmount = transaction.getAmountTendered();
        tenderedField.setText(tenderedAmount.toString());
        changeField.setFocusable(false);
        totalField.setFocusable(false);

        StringBuffer sb = new StringBuffer(Messages.getString("MOWizardPage4.1364"));
        String beforeTxt = Messages.getString("MOWizardPage4.1365");
        String boldTxt ="<b>"+beforeTxt+"</b>";
        sb.append(boldTxt);
        sb.append(Messages.getString("MOWizardPage4.1366"));
        String htmlText = "<html>"+sb.toString().trim()+"</html>";
        Dimension dm = ((JPanel) getComponent("totalToCollect")).getSize();
        dm.height = 2 * dm.height;
        instrLabel.setPreferredSize(dm);
        instrLabel.setText(htmlText);	

        if (!transaction.isAmountTenderedAvailable())
            tenderedPanel.remove(tenderedInnerPanel);

        changeField.setText("0");	

        if (transaction.isAmountTenderedAvailable()) {
            tenderedFlag = true;
            SwingUtilities.invokeLater(new Runnable() {
                @Override
				public void run() {
                    tenderedField.requestFocus(); //OK
                }
            });
        } else {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
				public void run() {
                    flowButtons.setSelected("next"); //OK (sets focus)	
                }
            });
        }

       //  addFocusLostListener("tenderedField", this);   	
        addFocusLostListener("tenderedField", this,"calculateChangeDue");	 

        addKeyListener("tenderedField", new KeyAdapter() {	
            @Override
			public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_TAB || e.getKeyCode() == KeyEvent.VK_ENTER) {
                    e.consume();
                    if (e.getKeyCode() == KeyEvent.VK_TAB) {
                        flowButtons.setSelected("back");	
                    } else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        // flowButtons.setSelected("next");	
                        flowButtons.getButton("next").requestFocus();	
                    }
                }
            }
        });

    }

    /**
     * If amount tendered is on,check if the insufficient amount tendered 
     * error dialog should be shown.Else everything is fine.print Money orders.
     */
    @Override
	public void finish(int direction) {
        if (direction == PageExitListener.NEXT) {
            if (tenderedFlag) {
                BigDecimal amount = new BigDecimal(tenderedField.getText());
                if ((amount.compareTo(BigDecimal.ZERO) != 0)
                    &&(amount.compareTo(transaction.getTotal(null, true))
                        < 0)){
                    Dialogs.showWarning("dialogs/DialogInsufficientAmount.xml");	
                    tenderedField.requestFocus();
                    tenderedField.setText("0");	
                    changeField.setText("0");	
                    flowButtons.setButtonsEnabled(true);
                    return;
                }else if(amount.compareTo(transaction.getDailyLimit())>0){
                    java.awt.Toolkit.getDefaultToolkit().beep();
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
						public void run() {
                            tenderedField.requestFocus();
                        }
                    });    
                    tenderedField.setText("0"); 
                    changeField.setText("0");   
                    flowButtons.setButtonsEnabled(true);
                    return;
                 }   
                 else {

                    flowButtons.disable();
                    printMoneyOrders();
                }
            }

            // try to print and handle results.Tendered flag is false. 
            else {
                flowButtons.disable();
                printMoneyOrders();
            }
        }

        PageNotification.notifyExitListeners(this, direction);
    }

    public void calculateChangeDue() {
        BigDecimal amount = new BigDecimal(tenderedField.getText());
        if ((amount.compareTo(BigDecimal.ZERO) != 0)
            && (amount.compareTo(transaction.getTotal(null, true)) > 0)) {
            transaction.setAmountTendered(amount);
            changeField.setText(transaction.getChangeAmount().toString());
        }
        else
            changeField.setText("0");	
    }
}
