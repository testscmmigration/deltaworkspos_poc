package dw.moneyorder;

import java.math.BigDecimal;

import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;

import com.moneygram.agentconnect.MoneyOrderInfo;

import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.dwgui.MoneyField;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.main.MainPanelDelegate;
import dw.profile.UnitProfile;

/**
 * Page 1 of the MoneyOrderWizard.
 * This page prompts the user for an amount for the first money order. The
 *   amount must be more than 0 for the user to be allowed to go to the next
 *   page of the wizard. Based on a profile setting, the user may be able to
 *   specify whether or not a fee is to be charged for this money order.
 */
public class MOWizardPage1 extends MoneyOrderFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

    private MoneyField amountField = null;
    private JRadioButton feeButton = null;
    private JRadioButton noFeeButton = null;
    private JPanel feePanel = null;
    private boolean discountOpt = false;

	public MOWizardPage1(MoneyOrderTransaction40 tran, String name, String pageCode, PageFlowButtons buttons) {
		this((MoneyOrderTransaction) tran, name, pageCode, buttons);
	}
	
    public MOWizardPage1(MoneyOrderTransaction tran,
                       String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "moneyorder/MOWizardPage1.xml", name, pageCode, buttons);	
        // collect the component references
        amountField = (MoneyField) getComponent("amountField");	
        noFeeButton = (JRadioButton) getComponent("noFeeButton");	
        feeButton = (JRadioButton) getComponent("feeButton");	
        feePanel = (JPanel) getComponent("feePanel");	

        // set up this page for MO vs VP
        if (transaction.isVendorPayment()) {
            removeComponent("moInstructionPanel");	
            removeComponent("discountInstructionPanel");	
            removeComponent("feePanel");	
        }
        else if (transaction.isAssociateDiscountAvailable()) {
            removeComponent("moInstructionPanel");	
            removeComponent("vpInstructionPanel");	
        }
        else {
            removeComponent("discountInstructionPanel");	
            removeComponent("vpInstructionPanel");	
        }
        
		DWTextPane tp = (DWTextPane) getComponent("text1");
		tp.addListener(this, this);
    }

	@Override
	public void dwTextPaneResized() {
		DWTextPane tp = (DWTextPane) getComponent("text1");
		JPanel panel = (JPanel) getComponent("moInstructionPanel");
		int offset=45;
		tp.setWidth((panel.getSize().width+offset));
		tp.setListenerEnabled(false);
	}
	
    @Override
	public void start(int direction) {

		if (("Y".equals(UnitProfile.getInstance().get("ISI_ENABLE", "N"))) &&
		    ("N".equals(transaction.getKeyboardIssue()))){ 
    		MainPanelDelegate.setIsiAllowDataCollection(true);
    		MainPanelDelegate.setIsiSkipRecord(false);
    	}
    	
    	flowButtons.reset();
        //flowButtons.setEnabled("back", false);	

        // ask transaction if discount option is enabled
        discountOpt = transaction.isAssociateDiscountAvailable();

        flowButtons.setEnabled("back", (transaction.isVendorPayment() || discountOpt));	
        flowButtons.setVisible("expert", (! transaction.isVendorPayment() && !discountOpt));	
        amountField.requestFocus(); //OK
        // set the money order info for this page
        MoneyOrderInfo mo = transaction.getMoneyOrderInfo(0);
        // BigDecimal fee = mo.getFee();
        //BigDecimal fee = new BigDecimal(mo.getItemFee());
        amountField.setText(mo.getItemAmount().toString());
                           
        //Renuka - if we do this then the MO has to be created with a fee >0.00
        //use the product level profile option FEE_ENABLED insted.
        //  if (fee.compareTo(DWValues.ZERO) == 0)
        if(transaction.isFeeEnabled()){
            if(mo.getItemAmount().equals(BigDecimal.ZERO))
                feeButton.setSelected(true);
            else
                if(mo.getItemFee().equals(BigDecimal.ZERO))
                    noFeeButton.setSelected(true);
                else
                    feeButton.setSelected(true);
        }
        else
            noFeeButton.setSelected(true);

        if (transaction.isVendorPayment())
            setComponentEnabled(feePanel, false);
        else
            setComponentEnabled(feePanel, transaction.isNoFeeAvailable());

        addActionListener("amountField", this, "amountFieldAction");	 
    }

    @Override
	public void finish(int direction) {
        // get the info from the components and update the money order info
        BigDecimal amount = new BigDecimal(amountField.getText());
        // do not go to the next page if there is no money order entered
        if (direction == PageExitListener.NEXT) {
            if (amount.compareTo(BigDecimal.ZERO) == 0) {
                amountField.selectAll();
                flowButtons.setButtonsEnabled(true);
                flowButtons.setEnabled("back", (transaction.isVendorPayment() || discountOpt));	
                // buttons are not setting focus correctly without the invokerLater
                SwingUtilities.invokeLater(new Runnable() {@Override
				public void run() {
                    amountField.requestFocus();
                }});
                return;
            }
            String payeeName = transaction.isVendorPayment() ?
                         transaction.getSelectedVendor() : null;
            if (! setInTransaction(amount, feeButton.isSelected(), payeeName, 0)){
                flowButtons.setButtonsEnabled(true);
                flowButtons.setEnabled("back", (transaction.isVendorPayment() || discountOpt));	
                // since setintransaction failed, set focus back to the amount field.
                amountField.requestFocus();
                return;
            }
        }
        flowButtons.setButtonsEnabled(true);
        flowButtons.setEnabled("back", (transaction.isVendorPayment() || discountOpt));	
        PageNotification.notifyExitListeners(this, direction);
    }

    public void amountFieldAction() {
        flowButtons.getButton("next").doClick();	
    }
}
