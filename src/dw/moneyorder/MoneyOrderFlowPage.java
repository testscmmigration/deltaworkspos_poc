package dw.moneyorder;

import java.math.BigDecimal;
import java.util.List;

import com.moneygram.agentconnect.MoneyOrderInfo;

import dw.dialogs.Dialogs;
import dw.dialogs.ManagerTranOverrideDialog;
import dw.dispenser.DispenserFactory;
import dw.dispenser.DispenserInterface;
import dw.framework.ClientTransaction;
import dw.framework.FlowPage;
import dw.framework.PageFlowButtons;

//import dw.io.*;

import dw.i18n.FormatSymbols;
import dw.profile.UnitProfile;
import dw.utility.ErrorManager;
import dw.utility.IdleBackoutTimer;

public abstract class MoneyOrderFlowPage extends FlowPage {
	private static final long serialVersionUID = 1L;

	protected MoneyOrderTransaction transaction;

    public MoneyOrderFlowPage(MoneyOrderTransaction tran, String fileName,
                                       String name, String pageCode, PageFlowButtons buttons) {
        super(fileName, name, pageCode, buttons);
        transaction = tran;
    }
    public MoneyOrderFlowPage(MoneyOrderTransaction40 tran, String fileName,
                                       String name, String pageCode, PageFlowButtons buttons) {
        super(fileName, name, pageCode, buttons);
        transaction = tran;
    }
    
//  do-nothing impl of commCompleteInterface...will be overridden by each
     //  specific page where comm is needed.
     @Override
	public void commComplete(int commTag, Object returnValue) {}

  
    /**
     * Prompt the user for a manager override approval. If this transaction
     * required a name and password, the manager name and password must be
     * entered. Otherwise, just a manager password is required.
     * 
     * This method was originally written to give the user three tries to 
     * get it right, but there was no definition on what was supposed to
     * happen after the third try. As of 4.2 it loops until you get enter
     * a valid PIN or cancel the override dialog.
     * 
     * @return true if a valid mgr pin/password with the right permission
     * is entered.
     */
    public boolean getTransactionLimitOverride() {
        String name = null;
        String password = null;
        int userID = -1;

        while (true) {

            // ManagerTranOverrideDialog used to be a subclass of NoTimeoutDialog,
            // meaning it automatically paused the idle timer while displayed.
            // Dialogs.java doesn't support that, so the code asking for the 
            // dialog has to manage the timer.
        
            boolean timerPending = IdleBackoutTimer.isRunning();
           // If the agent is in multi agent mode, do not prompt for name/password.
           // set the transaction over ride flag to true and return true.
            if(UnitProfile.getInstance().isMultiAgentMode()){
                transaction.tranLimitOverridden = true;
                return true;
            }
            ManagerTranOverrideDialog dialog = new ManagerTranOverrideDialog(
                    transaction.getMaxTransactionAmount().toString(),
                    transaction.isNamePasswordRequired());
            Dialogs.showPanel(dialog);
            if (timerPending) IdleBackoutTimer.start();

            name = dialog.getName();
            password = dialog.getPassword();
            userID = ClientTransaction.getUserID(name, password, false);
            if (dialog.isCanceled()) {
                return false;
            }
            else {      // ! dialog.isCanceled()
                // validate the manager's name and/or password
                if (name != null) {
                    if (transaction.overrideTransactionLimit(name, password)) {
                    	if(transaction.isUserLocked(name,password)){
                        	Dialogs.showWarning("dialogs/DialogAccountLocked.xml"); 
                        	ErrorManager.recordLoginAttempt(false, name);
                        	return false;
                        }
                        else{
                        	int user =transaction.isPasswordChangeRequired(name,password,userID);
                        	if(user > 0){
                        		ErrorManager.recordLoginAttempt(true, name);
                        return true;
                        	}
                        	else {
                        		ErrorManager.recordLoginAttempt(false, name);
                        		return false;
                        	}
                        }
                    }
                  
                    else {
                        ErrorManager.recordLoginAttempt(false, name);
                        Dialogs.showError("dialogs/DialogNotAuthorized.xml");
                        //continue;
                    }
                }
                else {
                    if (transaction.overrideTransactionLimit(password)) {
                    	if(transaction.isUserLocked(null,password)){
                        	Dialogs.showWarning("dialogs/DialogAccountLocked.xml"); 
                        	ErrorManager.recordLoginAttempt(false, name);
                        	return false;
                        }
                        else{
                        	int user =transaction.isPasswordChangeRequired(name,password,userID);
                        	if(user > 0){
                        		ErrorManager.recordLoginAttempt(true, name);
                        return true;
                        	}
                        	else {
                        		ErrorManager.recordLoginAttempt(false, name);
                        		return false;
                        	}
                        }
                    }
                    else {
                        ErrorManager.recordLoginAttempt(false, name);
                        Dialogs.showError("dialogs/DialogNotAuthorized.xml");
                        //continue;
                    }
                }
            }
        }
    }

    /**
     * Set the values of a particular money order in the transaction. Handle the
     *   results of the attempted add with the appropriate dialog box if
     *   necessary.
     */
    public boolean setInTransaction(BigDecimal amount, boolean chargeFee, //BigDecimal fee,
                                       String payeeName, int index) {
    	int status = transaction.setMoneyOrder(index, amount, chargeFee, payeeName);
        switch (status) {
            case MoneyOrderTransaction.TRANSACTION_AMOUNT_EXCEEDED :
                if (getTransactionLimitOverride()) {
                    // limit should be overridden, so we can try the add again
                    this.requestFocus();
                    return setInTransaction(amount, chargeFee,payeeName, index);
                } else {
                    transaction.setTranLimitOverridden(false);
                    return false;
                }
            case MoneyOrderTransaction.MAX_FORMS_PER_SALE_EXCEEDED :
                Dialogs.showWarning("dialogs/DialogMaxPerSaleReached.xml");	
                /*  3/8/2001 PatO, this reported the error that the Max Forms per Sale was exceeded and
                *   would force the user to enter a zero dollar amount inorder to process the money orders.
                *   "setMoneyOrder" was changed to process the money order and to exit to the summary screen.
                *   By returning true to the calling object, normal processing will take place.
                */
                //  return false;
                return true;
            case MoneyOrderTransaction.DAILY_LIMIT_EXCEEDED :
                Dialogs.showWarning("dialogs/DialogDailyLimitExceeded.xml",	
                        FormatSymbols.convertDecimal(transaction.getDailyLimit().toString()),
                        FormatSymbols.convertDecimal(transaction.getDailyTotal().toString()));
                return false;
            case MoneyOrderTransaction.REMAINING_ITEMS_EXCEEDED :
                Dialogs.showWarning("dialogs/DialogFormsExceeded.xml");	
                return false;
            case MoneyOrderTransaction.MAX_ITEM_AMOUNT_EXCEEDED :
                Dialogs.showWarning("dialogs/DialogMaxItemAmountExceeded.xml",	
                        FormatSymbols.convertDecimal(transaction.getMaxItemAmount().toString()));
                return false;
            case MoneyOrderTransaction.MAX_BATCH_COUNT_EXCEEDED :
                Dialogs.showWarning("dialogs/DialogBatchCountExceeded.xml", 	
                        String.valueOf(transaction.getMaxBatchCount()));
                return false;
            case MoneyOrderTransaction.MAX_BATCH_AMOUNT_EXCEEDED :
                Dialogs.showWarning("dialogs/DialogBatchLimitExceeded.xml",	
                        FormatSymbols.convertDecimal(transaction.getMaxBatchAmount().toString()));
                return false;
            case MoneyOrderTransaction.MAX_ITEM_AND_FORMS_EXCEEDED :
                Dialogs.showWarning("dialogs/DialogMaxPerSaleExceeded.xml");	
                return false;

        }
        return true;
    }

    protected boolean setInTransaction(BigDecimal amount, boolean chargeFee,
            String payeeName) {
        
        return setInTransaction(amount, chargeFee, payeeName,
            transaction.getMoneyOrderCount());
    }

    /**
     * Set values for a new money order.
     */
    protected boolean setInTransaction(BigDecimal amount, boolean chargeFee) { //BigDecimal fee) {
        return setInTransaction(amount, chargeFee, null);
    }

    /**
     * Try to add the moneyOrder to the transaction. If it can not be added for
     *   any reason, display an error dialog to the user indicating the problem.
     */
    protected boolean setInTransaction(BigDecimal amount, boolean chargeFee, //BigDecimal fee,
                                                              int index) {
        return setInTransaction(amount, chargeFee, null, index);
    }

    /**
     * Fire off the print process by calling into the transaction and showing
     *   the printing dialog. Since this thread will block when modal dialog is
     *   shown, create a new thread to call the transaction print process before
     *   showing the dialog
     */
    protected void printMoneyOrders() {
     //not required to check this since at this point the list cannot be empty.
       /* if ((((MoneyOrderTransaction40)transaction).getMoneyOrderList().size() == 0)||
        (transaction.getMoneyOrders().size() == 0)) 
        return; */   
        // make sure the dispenser is still ready to go
        DispenserInterface dispenser = DispenserFactory.getDispenser(1);
        if (dispenser.queryStatus() != DispenserInterface.OK) {
            // notify user and cancel transaction
            Dialogs.showError("dialogs/DialogDispenserUnavailable.xml");	
            transaction.setStatus(ClientTransaction.CANCELED);
            transaction.setTransactionPrinted(true);
            return;
        }

        if (!dispenser.isOn() && UnitProfile.getInstance().isDemoMode()){
            List<MoneyOrderInfo> orders = ((MoneyOrderTransaction40)transaction).getMoneyOrderList();

            for (int i = 0; i < orders.size(); i++) {
            	MoneyOrderInfo order = orders.get(i);
            
                if (!DispenserFactory.getDispenser(1).isOn() && UnitProfile.getInstance().isDemoMode()){
                    MoneyOrderTransaction40 tran40 = (MoneyOrderTransaction40)transaction;
                    tran40.setupISI(order); 
                }
            }
            return;
        }


        final MOPrintingDialog printDialog = new MOPrintingDialog();
        // dialog will block this thread, so create a new one to do the printing
        Thread printThread = new Thread(new Runnable(){
            @Override
			public void run(){
                IdleBackoutTimer.pause();
                transaction.commit(printDialog);
                IdleBackoutTimer.start();
//                TempLog.commitTempLog();
            }
        });
        printThread.start();
        Dialogs.showPanel(printDialog);
        
   }
   
}
