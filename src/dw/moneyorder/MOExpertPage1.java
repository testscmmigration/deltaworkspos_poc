/*
 * MOExpertPage1.java
 * 
 * $Revision$
 *
 * Copyright (c) 2000-2005 MoneyGram International
 */

package dw.moneyorder;

import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.moneygram.agentconnect.ComplianceTransactionRequest;
import com.moneygram.agentconnect.CustomerComplianceInfo;
import com.moneygram.agentconnect.CustomerComplianceTypeCodeType;
import com.moneygram.agentconnect.MoneyOrderInfo;
import com.moneygram.agentconnect.PhotoIdType;

import dw.dialogs.Dialogs;
import dw.dwgui.CustomTableCellRenderer;
import dw.dwgui.DWTable;
import dw.dwgui.DWTable.DWTableListener;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.dwgui.MoneyField;
import dw.dwgui.MultiListComboBox;
import dw.dwgui.RowSelectCellRenderer;
import dw.dwgui.StateListComboBox;
import dw.dwgui.UneditableTableModel;
import dw.framework.ClientTransaction;
import dw.framework.DataCollectionScreenToolkit;
import dw.framework.KeyMapManager;
import dw.framework.PageExitListener;
import dw.framework.PageNotification;
import dw.framework.DataCollectionScreenToolkit.DWComboBoxItem;
import dw.i18n.Messages;
import dw.main.MainPanelDelegate;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.CountryInfo.Country;
import dw.model.adapters.CountryInfo.CountrySubdivision;
import dw.profile.UnitProfile;
import dw.utility.DWValues;
import dw.utility.TimeUtility;

/**
 * Page 1 (and only page) of the Money Order Expert interface.
 * This page allows the user to enter multiple money orders. Each money order
 *   entered will be displayed in the money order table. If the up arrow key
 *   is pressed at any time on this page, the last item in the money order
 *   table will be highlighted and the amount will be populated in the amount
 *   field. The amount may be edited at this time and will then be updated in
 *   the table. When all money orders have been entered, The user may activate
 *   print button to print the money orders. After the print process is finished
 *   the interface will exit back to the main menu. If printing fails, a dialog
 *   window will be displayed with details of what printed and what didn't.
 *   Based on a profile setting, the user may have the option of issuing a money
 *   order with no fee, and also may have the option of entering the amount
 *   tendered to get the change amount due.
 * 
 *   DW4.0 - The expert page supports the collection of data to be used for OFAC checking.
 */
public class MOExpertPage1 extends MoneyOrderFlowPage implements DWTextPaneListener, DWTableListener {
	private static final long serialVersionUID = 1L;

	// component references
	private MoneyField amountField = null;

	private MoneyField collectField = null;

	private MoneyField tenderedField = null;

	private MoneyField changeField = null;

	private JTextField quantityField = null;

	private JButton cancelButton = null;

	private JButton totalButton = null;

	private JButton printButton = null;

	// components for adding / hiding optional functions
	private JButton noFeeButton = null;

	private JPanel tenderedChangePanel = null;

	private JPanel tenderedChangeFieldPanel = null;

	private DWTextPane discountText = null;

	private JPanel photoIdPanel;

	// table information
	private DWTable moneyOrderTable = null;

	private DefaultTableModel tableModel = null;

	private int tableRowSelected = -1;

	// previous amount, fee and quantity for repeat functionality
	private String previousAmount = "0.00"; 

	private boolean previousFee = true;

	//Components for photoid information.
	private JTextField firstNameField;

	private JTextField middleInitialField;

	private JTextField lastNameField;

	private MultiListComboBox<String> pidTypeBox;

	private JTextField pidNumberField;

	private MultiListComboBox<Country> pidCountryBox;

	private StateListComboBox<CountrySubdivision> pidStateBox;

	private CustomerComplianceInfo cci;

	private ComplianceTransactionRequest requestInfo;
	
	private JScrollPane scrollpane1;

	private JPanel column1;

	public MOExpertPage1(MoneyOrderTransaction40 tran, String name, String pageCode) {
		this((MoneyOrderTransaction) tran, name, pageCode);
	}

	public MOExpertPage1(MoneyOrderTransaction tran, String name, String pageCode) {
		super(tran, "moneyorder/MOExpertPage1.xml", name, pageCode, null); 
		collectComponents();
	}

	private void collectComponents() {
		amountField = (MoneyField) getComponent("amountField"); 
		tenderedField = (MoneyField) getComponent("tenderedField"); 
		collectField = (MoneyField) getComponent("collectField"); 
		changeField = (MoneyField) getComponent("changeField"); 
		quantityField = (JTextField) getComponent("quantityField"); 
		discountText = (DWTextPane) getComponent("discountText"); 
		cancelButton = (JButton) getComponent("cancelButton"); 
		totalButton = (JButton) getComponent("totalButton"); 
		printButton = (JButton) getComponent("printButton"); 
		noFeeButton = (JButton) getComponent("noFeeButton"); 
		tenderedChangePanel = (JPanel) getComponent("tenderedChangePanel"); 
		tenderedChangeFieldPanel = (JPanel) getComponent("tenderedChangeFieldPanel"); 
		// all the photoid information.
		photoIdPanel = (JPanel) getComponent("photoIdPanel"); 
		firstNameField = (JTextField) getComponent("firstNameField"); 
		middleInitialField = (JTextField) getComponent("middleInitialField"); 
		lastNameField = (JTextField) getComponent("lastNameField"); 
		pidTypeBox = (MultiListComboBox<String>) getComponent("pidTypeBox"); 
		pidNumberField = (JTextField) getComponent("pidNumberField"); 
		pidCountryBox = (MultiListComboBox<Country>) getComponent("pidCountryBox"); 
		pidStateBox = (StateListComboBox<CountrySubdivision>) getComponent("pidStateBox"); 
		moneyOrderTable = (DWTable) this.getComponent("moneyOrderTable");
		scrollpane1 = (JScrollPane) getComponent("scrollpane1");
		column1 = (JPanel) getComponent("column1");

		//load up the static combo boxes
        List<DWComboBoxItem> photoIdTypeList = DataCollectionScreenToolkit.toComboBoxItemsList(transaction.getPhotoIDNames(), DWValues.getPhotoIdTypes());
		pidTypeBox.addList(photoIdTypeList, DWValues.PHOTO_ID_DEFAULT, "types"); 

		List<Country> countries = CountryInfo.getCountries(CountryInfo.OK_FOR_IDENTIFICATION);
		pidCountryBox.addList(countries, "countries"); 

		populateStateBox(pidStateBox, CountryInfo.getAgentCountry(), CountryInfo.getAgentCountrySubdivision());

		setupTable();
		
		moneyOrderTable.addListener(this, this);
		moneyOrderTable.setListenerEnabled(false);
		discountText.addListener(this, this);
	}

	@Override
	public void dwTextPaneResized() {
		JPanel panel1 = (JPanel) getComponent("panel1");
		JPanel panel2 = (JPanel) getComponent("helpBox");
		int width = panel1.getPreferredSize().width - panel2.getPreferredSize().width;
		discountText.setWidth(width);
		discountText.setListenerEnabled(false);
	}
	
	@Override
	public void dwTableResized() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				moneyOrderTable.resizeTable();
				column1.revalidate();
				scrollpane1.repaint();
			} 
		});
	}

	private void setupListeners() {
		addActionListener("wizardButton", this); 
		addActionListener("cancelButton", this); 
		addActionListener("amountField", this); 
		addActionListener("noFeeButton", this); 
		addActionListener("tenderedField", this); 
		addActionListener("repeatButton", this); 
		addActionListener("removeButton", this); 
		addActionListener("totalButton", this); 
		addActionListener("printButton", this); 
		addActionListener("quantityField", this); 

		addFocusLostListener("firstNameField", this, "validatePurchaserInfo");  
		addFocusLostListener("middleInitialField", this, "validatePurchaserInfo");  
		addFocusLostListener("lastNameField", this, "validatePurchaserInfo");  
		addFocusLostListener("pidNumberField", this, "validatePurchaserInfo");  

		addFocusGainedListener("amountField", this, "disablePrintButton");  
		addFocusGainedListener("quantityField", this, "qtyFieldFocusGained");  
		addFocusLostListener("tenderedField", this, "setTendered");
		KeyMapManager.mapMethod(this, KeyEvent.VK_UP, 0, "upArrowAction"); 
		KeyMapManager.mapMethod(this, KeyEvent.VK_DOWN, 0, "downArrowAction"); 
		KeyMapManager.mapComponent(this, KeyEvent.VK_ESCAPE, 0, cancelButton);
		moneyOrderTable.getTable().setRowSelectionAllowed(true);
		moneyOrderTable.getTable().getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {
					@Override
					public void valueChanged(ListSelectionEvent e) {
						selectTableRow(moneyOrderTable.getTable().getSelectedRow());
					}
				});
//		//add action listener to detect a country change and load state accordingly.              
//		//if stateBox is empty,gray it out.
//		ActionListener countryChangeListener = new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				CountryInfo ci = (CountryInfo) pidCountryBox.getSelectedItem();
//			}
//		};
//		addActionListener("pidCountryBox", countryChangeListener); 
		
        ActionListener countryChangeListener = new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
               	Country country = (Country) pidCountryBox.getSelectedItem();
               	updateStateList(country, pidStateBox);
                
            }};
        addActionListener(pidCountryBox, countryChangeListener);
	}

	/**
	 * Create and set upp the money order table. Add the table to the scrollpane
	 *   defined in the XML.
	 */
	private void setupTable() {
		String[][] data = {};
		String[] colNames = {
				Messages.getString("MOExpertPage1.itemText"), Messages.getString("MOExpertPage1.feeText"), Messages.getString("MOExpertPage1.amountText") };   
		tableModel = new UneditableTableModel(data, colNames);
		moneyOrderTable.getTable().setModel(tableModel);
		RowSelectCellRenderer.setAsDefaultRenderer(moneyOrderTable.getTable());
		TableColumnModel tcm = moneyOrderTable.getTable().getColumnModel();

		TableColumn itemCol = tcm.getColumn(0);
		TableColumn feeCol = tcm.getColumn(1);
		TableColumn amountCol = tcm.getColumn(2);

		TableCellRenderer tcr = new CustomTableCellRenderer(CustomTableCellRenderer.RIGHT_CELL, DWTable.DEFAULT_COLUMN_MARGIN);
		itemCol.setCellRenderer(tcr);
		tcr = new CustomTableCellRenderer(CustomTableCellRenderer.LEFT_HEADER, DWTable.DEFAULT_COLUMN_MARGIN);
		itemCol.setHeaderRenderer(tcr);
		
		tcr = new CustomTableCellRenderer(CustomTableCellRenderer.MONETARY_CELL, DWTable.DEFAULT_COLUMN_MARGIN);
		feeCol.setCellRenderer(tcr);
		tcr = new CustomTableCellRenderer(CustomTableCellRenderer.MONETARY_HEADER, DWTable.DEFAULT_COLUMN_MARGIN);
		feeCol.setHeaderRenderer(tcr);

		tcr = new CustomTableCellRenderer(CustomTableCellRenderer.MONETARY_CELL, DWTable.DEFAULT_COLUMN_MARGIN);
		amountCol.setCellRenderer(tcr);
		tcr = new CustomTableCellRenderer(CustomTableCellRenderer.MONETARY_HEADER, DWTable.DEFAULT_COLUMN_MARGIN);
		amountCol.setHeaderRenderer(tcr);

		moneyOrderTable.getTable().setEnabled(false);
		
		moneyOrderTable.getTable().getTableHeader().setReorderingAllowed(false);
		moneyOrderTable.getTable().getTableHeader().setResizingAllowed(false);
		moneyOrderTable.getTable().setFocusable(true);
		moneyOrderTable.getTable().setRowSelectionAllowed(true);
		moneyOrderTable.getTable().setEnabled(true);
	}

	@Override
	public void start(int direction) {
    	Country defaultCountry = CountryInfo.getAgentCountry();
    	pidCountryBox.setSelectedComboBoxItem(defaultCountry.getCountryCode());
    	
    	CountrySubdivision defaultState = CountryInfo.getAgentCountrySubdivision();
    	pidStateBox.setSelectedComboBoxItem(defaultState.getCountrySubdivisionCode());
    	
		pidTypeBox.setSelectedComboBoxItem(DWValues.PHOTO_ID_DEFAULT);
		
		if (("Y".equals(UnitProfile.getInstance().get("ISI_ENABLE", "N")))
				&& ("N".equals(transaction.getKeyboardIssue()))) {
			MainPanelDelegate.setIsiAllowDataCollection(true);
			MainPanelDelegate.setIsiSkipRecord(false);
		}
		setButtonsEnabled(true);
		previousAmount = "0.00"; 
		previousFee = false;
		amountField.setValue(BigDecimal.ZERO);
		setupListeners();
		collectField.setValue(BigDecimal.ZERO); 
		collectField.setFocusable(false);
		changeField.setFocusable(false);
		quantityField.setText("1"); 
		tenderedField.setValue(transaction.getAmountTendered());
		tenderedFieldAction();
		refreshTable();
		disablePrintButton();
		noFeeButton.setEnabled(transaction.isNoFeeAvailable());
		if (!transaction.isAmountTenderedAvailable()) {
			tenderedChangePanel.remove(tenderedChangeFieldPanel);
		}
		// show discount verbiage if fee discount option is enabled
		if (!transaction.isAssociateDiscountAvailable()) {
			discountText.setVisible(false);
		}

		this.tenderedFieldAction();

		amountField.requestFocus();

		//  photoIdPanel.setVisible(false);
		if (transaction.isAggCheckEnable()) {
			photoIdPanel.setVisible(true);
			setComponentEnabled(photoIdPanel, false, true);
		} else {
			photoIdPanel.setVisible(false);
		}

		moneyOrderTable.setListenerEnabled(true);
	}

	public void qtyFieldFocusGained() {
		quantityField.selectAll();
		printButton.setEnabled(false);
	}

	public void disablePrintButton() {
		printButton.setEnabled(false);
	}

	public void validatePurchaserInfo() {
		if ((this.firstNameField.getText().length() > 0)
				&& (this.lastNameField.getText().length() > 0)
				&& (this.pidNumberField.getText().length() > 0)) {
			printButton.setEnabled(true);
		} else {
			printButton.setEnabled(false);
		}
	}

	/**
	 * Get the money order info from the transaction and populate the table.
	 */
	private void refreshTable() {
		int rowCount = tableModel.getRowCount();
		// clear out the table
		for (int i = 0; i < rowCount; i++) {
			tableModel.removeRow(0);
		}
		List<MoneyOrderInfo> moneyOrderList = transaction.getMoneyOrderList();
		// add all the transaction's money orders to the table
		for (int i = 0; i < moneyOrderList.size(); i++) {
			MoneyOrderInfo moneyOrder = moneyOrderList.get(i);
			addToTable(moneyOrder);
		}
		tableRowSelected = -1;
		amountField.requestFocus();
		// update the total and change due
		collectField.setValue(transaction.getTotal(null, true));
		checkPhotoIdPanel();
		moneyOrderTable.initializeTable(scrollpane1, column1, 250, 0);
        dwTableResized();
		scrollToRow(tableModel.getRowCount() - 1);
	}

	/** 
	 * check if photoId is required or not and populate the fields. 
	 */
	private void checkPhotoIdPanel() {
		if (transaction.isPhotoIdNeeded()) {
			setComponentEnabled(photoIdPanel, true, true);
		} else {
			setComponentEnabled(photoIdPanel, false, true);
		}
	}

	private void scrollToRow(final int row) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				if (! moneyOrderTable.getVerticalScrollBar().isShowing()) {
					return;
				}
				
				int pos = moneyOrderTable.getTable().getBounds().height - moneyOrderTable.getBounds().height;
				moneyOrderTable.getViewport().setViewPosition(new Point(0, pos));
			}
		});
	}

	private void removeMoneyOrder(int index) {
		transaction.deleteMoneyOrder(index);
		tableModel.removeRow(index);
	}

	/**
	 * Set the values of a particular money order. Update the values in the
	 *   transaction and refresh the table.
	 */
	private boolean setMoneyOrder(String amt, boolean chargeFee, int index) {
		BigDecimal amount = new BigDecimal(amt);
		if (setInTransaction(amount, chargeFee, index)) {
			refreshTable();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Add money order(s) to the transaction and refresh the table. The number
	 *   of money orders to add is based on the value in the quantity field. If
	 *   money order amount was 0, return false
	 */
	private boolean addMoneyOrders(String amt, boolean chargeFee, int qty) {
		BigDecimal amount = new BigDecimal(amt);
		boolean result = true;
		if (amount.compareTo(BigDecimal.ZERO) == 0) {
			result = false;
			return result;
		}
		for (int i = 0; i < qty; i++) {
			if (!setInTransaction(amount, chargeFee)) {
				result = false;
				break;
			} else {
				if (transaction.getMoneyOrderCount() >= transaction.getMaxFormsPerSale()) {
					result = false;
					break;
				}
			}
		}
		refreshTable();
		return result;
	}

	/**
	 * Add a money order to the money order table. Update the total and change
	 *   due fields accordingly.
	 */
	private void addToTable(MoneyOrderInfo moneyOrder) {
		int seqNum = moneyOrderTable.getTable().getRowCount() + 1;
		String[] data = {String.valueOf(seqNum), String.valueOf(moneyOrder.getItemFee()), String.valueOf(moneyOrder.getItemAmount()) };
		
		tableModel.addRow(data);
		// clear out the amount field
		amountField.setValue(BigDecimal.ZERO);
		quantityField.setText("1"); 
		amountField.requestFocus();
	}

	/**
	 * Just notify my listeners that I am done.
	 */
	@Override
	public void finish(int direction) {
		PageNotification.notifyExitListeners(this, direction);
		moneyOrderTable.setListenerEnabled(false);
	}

	/**
	 * Attempt to add money orders. If the money orders were not added, return
	 *   false. This indicates that there was no amount entered.
	 */
	private boolean doAddAction(boolean chargeFee) {
		int quantity = 1;
		boolean added;
		String pa = amountField.getText();
		try {
			quantity = Integer.parseInt(quantityField.getText());
		} catch (NumberFormatException e) {
		}

		if (tableRowSelected > -1) {
			if (setMoneyOrder(amountField.getText(), chargeFee,
					tableRowSelected)) {
				if (((MoneyOrderTransaction40) transaction).getMoneyOrderList()
						.size() < transaction.getMaxFormsPerSale()) {
					added = true;
				} else {
					added = false;
				}
			} else {
				added = true;
			}
		} else {
			if (addMoneyOrders(amountField.getText(), chargeFee, quantity)) {
				if (transaction.getMoneyOrderCount() < transaction.getMaxFormsPerSale()) {
					added = true;
				} else {
					added = false;
				}
			} else {
				added = false;
			}
		}
		if (added) {
			previousAmount = pa;
			previousFee = chargeFee;
		}
		return added;
	}

	public void quantityFieldAction() {
		amountField.requestFocus();
	}

	/**
	 * Add a money order(s) to the table if the amount in the field is not 0. If
	 *   it is 0, shift focus to the amount tendered field. If it was not 0,
	 *   shift focus back to the amount field.
	 */
	public void amountFieldAction() {
		if (doAddAction(true)) {
			amountField.requestFocus();
		} else {
			amountField.setValue(BigDecimal.ZERO);
			totalButton.doClick();
		}
	}

	/**
	 * Repeat the last action. Add the same quantity, amount and fee of the last
	 *   money order action entered.
	 */
	public void repeatButtonAction() {
		if (tableRowSelected > -1) {
			addMoneyOrders(amountField.getText(), previousFee, 1);
		} else {
			addMoneyOrders(previousAmount, previousFee, 1);
		}
		amountField.requestFocus();
	}

	/**
	 * Add money order(s) for the given amount, but not charge a fee.
	 */
	public void noFeeButtonAction() {
		doAddAction(false);
		amountField.requestFocus();
	}

	/**
	 * Calculate the amount of change due and populate the change due field.
	 */
	public void tenderedFieldAction() {
		if (setTendered()) {
			getComponent("printButton").setEnabled(true); 
			getComponent("printButton").requestFocus(); 
		}
	}

	/**
	 * Set the tendered amount field to the given value. If the value is 0, do
	 *   not update the change due field.
	 */
	public boolean setTendered() {
		BigDecimal tendered = new BigDecimal(tenderedField.getText());
		if ((tendered.compareTo(BigDecimal.ZERO) != 0)
				&& (tendered.compareTo(transaction.getTotal(
						null, true)) > 0)) {
			transaction.setAmountTendered(tendered);
			changeField.setValue(transaction.getChangeAmount());
		} else {
			changeField.setText("0");
		}
		return true;
	}

	/**
	 * Delete the money order that is highlighted in the table.
	 */
	public void removeButtonAction() {
		if (tableRowSelected > -1) {
			removeMoneyOrder(tableRowSelected);
			refreshTable();
			amountField.setValue(BigDecimal.ZERO);
			amountField.requestFocus();
		}
	}

	/**
	 * Notify my listeners that the user wishes to go the wizard interface. Make
	 *   sure to notify the user that they will lose their entered data.
	 */
	public void wizardButtonAction() {
		if (transaction.getMoneyOrderCount() > 0) {
			int result = Dialogs.showConfirm("dialogs/DialogModeSwitch.xml"); 
			if (result != Dialogs.YES_OPTION) {
				return;
			}
		}
		finish(PageExitListener.WIZARD);
	}

	/**
	 * Notify listeners that the user wishes to cancel the transaction.
	 */
	public void cancelButtonAction() {
		transaction.setStatus(ClientTransaction.CANCELED);
		finish(PageExitListener.CANCELED);
	}

	/**
	 * Try to print the money orders. If not all the money orders printed, show
	 *   a dialog with the details of the print. Exit back to the main menu.
	 */
	public synchronized void printButtonAction() {
		// disable the buttons to remove multiple-activations
		setButtonsEnabled(false);

		// try to add a money order if there is one in the amount field
		doAddAction(true);
		//Renuka - check if the insufficient amount tendered dialog should be shown.
		//if so don't allow to continue.
		if (transaction.isAmountTenderedAvailable()) {
			BigDecimal tendered = new BigDecimal(tenderedField.getText());
			if (tendered.compareTo(BigDecimal.ZERO) != 0) {
				if (tendered.compareTo(transaction.getTotal(null, true)) < 0) {
					Dialogs.showWarning("dialogs/DialogInsufficientAmount.xml"); 
					tenderedField.requestFocus();
					tenderedField.selectAll();
					setButtonsEnabled(true);
					noFeeButton.setEnabled(transaction.isNoFeeAvailable());
					return;
				} else if (tendered.compareTo(transaction.getDailyLimit()) > 0) {
					java.awt.Toolkit.getDefaultToolkit().beep();
					SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							tenderedField.requestFocus();
						}
					});
					tenderedField.setText("0"); 
					changeField.setText("0"); 
					setButtonsEnabled(true);
					noFeeButton.setEnabled(transaction.isNoFeeAvailable());
					return;
				}
			}
		}

		//set the required photoid fields, set the cci to what is entered and
		//send request for aggregate check.
		if (transaction.isPhotoIdNeeded()) {
			// JTextField[] fields = { firstNameField, lastNameField, pidNumberField };
			JComponent[] fields = { firstNameField, lastNameField, pidNumberField, pidStateBox };
			if (validateFields(fields)) {
				if (!validatePhotoIdNumber()) {
					Toolkit.getDefaultToolkit().beep();
					pidNumberField.requestFocus();
					pidNumberField.selectAll();
					Dialogs.showWarning("dialogs/DialogInvalidData.xml", Messages.getString("MOExpertPage1.2018"));
					setButtonsEnabled(true);
					printButton.setEnabled(false);
					noFeeButton.setEnabled(transaction.isNoFeeAvailable());
					return;
				} else {
					setCci();
					transaction.getAggregationCheck(requestInfo, this);
				}
			} else {
				noFeeButton.setEnabled(transaction.isNoFeeAvailable());
			}

		// try the print and handle the results
		} else {
			printMoneyOrders();
			if (transaction.getStatus() == ClientTransaction.COMPLETED) {
				finish(PageExitListener.COMPLETED);
			} else {
				Dialogs.showPanel(new PrintCanceledDialogMO(transaction, false));
				cancelButtonAction();
			}
		}
	}

	/**
	 * Add a money order with the amount the is currently entered in the amount
	 *   field and then shift focus to the tendered field if it is visible, or
	 *   the print button if not.
	 */
	public void totalButtonAction() {
		if (tableModel.getRowCount() == 0
				&& amountField.getValue().compareTo(BigDecimal.ZERO) == 0) {
			amountField.requestFocus();
			return;
		}
		if (amountField.getValue().compareTo(BigDecimal.ZERO) != 0) {
			doAddAction(true);
		}
		// if the photoid panel is visible move the focus to the first field in the photoid panel.    
		if (((MoneyOrderTransaction40) transaction).isPhotoIdNeeded()) {
			firstNameField.requestFocus();
			validatePurchaserInfo();
		} else if (transaction.isAmountTenderedAvailable()) {
			tenderedField.requestFocus();
			if (setTendered()) {
				printButton.setEnabled(true);
			}
		} else {
			printButton.setEnabled(true);
			printButton.requestFocus();
		}
	}

	/**
	 * If there is not a currently selected money order in the table, select the
	 *   last one. If there is one selected, just move the selection up one (or
	 *   wrap around if necessary).
	 */
	public void upArrowAction() {
		if (tableRowSelected <= 0) {
			selectTableRow(tableModel.getRowCount() - 1);
		} else {
			selectTableRow(tableRowSelected - 1);
		}
		amountField.requestFocus();
	}

	/**
	 * If a money order is currently selected in the table, move the selection
	 *   down one (or wrap if necessary). If there is none selected, do nothing.
	 */
	public void downArrowAction() {
		if (tableRowSelected > -1) {
			selectTableRow((tableRowSelected + 1) % tableModel.getRowCount());
		}
		amountField.requestFocus();
	}

	/**
	 * Select a row in the money order table. Get the amount value for the money
	 *   order selected and place it in the amount field.
	 */
	private void selectTableRow(int row) {
		if (row < 0 || row >= tableModel.getRowCount()) {
			tableRowSelected = -1;
			return;
		}
		tableRowSelected = row;
		ListSelectionModel tableSelectionModel = moneyOrderTable.getTable().getSelectionModel();
		tableSelectionModel.setSelectionInterval(row, row);
		amountField.setText((String) moneyOrderTable.getTable().getValueAt(row, 2));
		previousAmount = (String) moneyOrderTable.getTable().getValueAt(row, 2);
		previousFee = !"0.00".equals(moneyOrderTable.getTable().getValueAt(row, 1)); 
	}

	/**
	 * Populate the CustomerComplianceInfo with the values entered on screen.
	 */
	private void setCci() {
		cci = new CustomerComplianceInfo();
		cci.setFirstName(firstNameField.getText());
		cci.setLastName(lastNameField.getText());
		cci.setMiddleInitial(middleInitialField.getText());
		cci.setPhotoIdNumber(pidNumberField.getText());
        cci.setPhotoIdType(PhotoIdType.fromValue(((DWComboBoxItem) pidTypeBox.getSelectedItem()).getValue()));
		String value = ((CountrySubdivision) pidStateBox.getSelectedItem()).getCountrySubdivisionCode();
		cci.setState(((value != null) && (value.length() == 5)) ? value.substring(3, 5) : null);
		cci.setCountry(((Country) pidCountryBox.getSelectedItem()).getCountryCode());
		cci.setTypeCode(CustomerComplianceTypeCodeType.OFAC_OR_AGGREGATION);
		cci.setLocalDateTime(TimeUtility.toXmlCalendar(TimeUtility.currentTime()));
		requestInfo = new ComplianceTransactionRequest();
		requestInfo.setCci(cci);
		transaction.saveCci(cci);
	}

	/**
	 * validate that the photoId number entered on screen 
	 * contains atleast one letter or digit.
	 */
	private boolean validatePhotoIdNumber() {
		String number = pidNumberField.getText();
		char c;
		boolean result = true;
		for (int i = 0; i < number.length(); i++) {
			c = number.charAt(i);
			if ((Character.isLetterOrDigit(c))) {
				result = true;
				break;
			} else {
				result = false;
			}
		}
		return result;
	}

	/**
	 * Called by transaction after it receives response from middleware.
	 * ReturnValue is Boolean set to true if response is good. 
	 * Otherwise, it is set to false.
	 * If any of the name or photoId information id was changed as determined
	 * by NEEDS_RECOMMIT then send the message to the RTS requesting for Aggregate/OFAC
	 * check. 
	 * If the transaction failed for OFAC reasons, print the receipts and return to main menu. 
	 */
	@Override
	public void commComplete(int commTag, Object returnValue) {
		if (returnValue.equals(Boolean.TRUE)) {
			printMoneyOrders();
			if (transaction.getStatus() == ClientTransaction.COMPLETED) {
				finish(PageExitListener.COMPLETED);
			} else {
				Dialogs.showPanel(new PrintCanceledDialogMO(transaction, false));
				cancelButtonAction();
			}
		} else if (transaction.getStatus() == ClientTransaction.NEEDS_RECOMMIT) {
			transaction.getAggregationCheck(requestInfo, this);
		} else if (transaction.getStatus() == ClientTransaction.FAILED) {
			String sourceNumber = ((MoneyOrderTransaction40) transaction).sourceNumber;
			if (((MoneyOrderTransaction40) transaction).checkPrinterReady(this)) {
				((MoneyOrderTransaction40) transaction).printOfacErrorReceipt(true, sourceNumber);
				((MoneyOrderTransaction40) transaction).printOfacErrorReceipt(false, sourceNumber);
			}
			PageNotification.notifyExitListeners(this, PageExitListener.FAILED);
			return;
		} else {
			firstNameField.requestFocus();
			PageNotification.notifyExitListeners(this, PageExitListener.DONOTHING);
			setButtonsEnabled(true);
			noFeeButton.setEnabled(transaction.isNoFeeAvailable());
		}
	}
}
