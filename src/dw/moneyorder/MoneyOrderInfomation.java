package dw.moneyorder;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import dw.profile.UnitProfile;
import dw.utility.TimeUtility;

/**
 * Information about a single money order or vendor payment. Amount, fee,
 *  creation time, printed time, status, vendor name.
 *
 * @author Erik S. Steinmetz
 * @author Geoff Atkin
 */
public class MoneyOrderInfomation implements java.io.Serializable {
    static final long serialVersionUID = 1076106817850107299L;
    private transient boolean attempted = false;

    public static final short DISPENSER_VOID = 1; // dispenser error handler
    public static final short DEMO_VOID = 2; // demo mode
    public static final short TRAINING_VOID = 3; // training mode
    public static final short TEST_VOID = 4; // "print test void" on the menu bar
    public static final short LOADING_VOID = 5; // void remaining forms during load
    public static final short LAST_VOID = 6; // last (unprintable) item in pack

    // fast convert from int to string representation of void reason code
    public static String reasonCode[] = { "0", "1", "2", "3", "4", "5", "6" };       
    // fast convert from above reason codes to old codes used by dnet
    public static short fDocVoidDiagReasonCode[] = { 0, 0, 0, 0, 0, 254, 255 };

    private BigInteger serialNumber;	
    private BigDecimal amount = BigDecimal.ZERO;
    private BigDecimal fee = BigDecimal.ZERO;
    private String payeeName = "";	
    private long creationDateTime = 0;
    private byte period = 0;
    private long dateTimePrinted = 0;
    private boolean printed = false;
    private boolean completed = false;
    private boolean voidFlag = false;
    private String voidReason = "";	
    private boolean trainingFlag = false;
    private String documentType = "";	
    private BigInteger documentNumber = null;	
    private int reportDocumentType;
    private String dispenserNumber = "";	
    private String employeeID = "00";	
    private String saleIssuanceTag = "0";	
    private String multiIssueIndicator = "N";	
    private String limitIndicator = "N";	
    private String documentName = "";	
    private String deltagramFlag = "";	
    private BigInteger payeeID = null;	
    private String taxID = "";	
    private String docName[] = { "", "MO", "VP", "GIFT", "DG" };     
    private BigDecimal discountAmount = BigDecimal.ZERO;
    private BigDecimal discountPercentage = BigDecimal.ZERO;

    /* Used by DiagReport to format Message to look like Plutto output. */
    public String toStringPrnt() {
        StringBuffer sb = new StringBuffer();

        // document ID
        sb.append(toStringDocID());

        // Dispenser ID
        sb.append("Disp" + (getIntDispenserNumber() + 1)); 

        // Serial Number
        sb.append("; Serial #" + serialNumber); 

        return sb.toString();
    }

    /* Used by DiagReport to format Message to look like Plutto output. */
    public String toStringMO() {
        StringBuffer sb = new StringBuffer();

        // amount
        sb.append(" Amount " + amount); 

        // fee amount
        sb.append("; Fee " + fee); 

        // void flag
        if (voidFlag)
            sb.append("; VOID"); 

        // Dispenser ID
        sb.append("; Disp" + (getIntDispenserNumber() + 1)); 

        // employee ID
        sb.append("; Employee " + employeeID); 

        // payee ID if Vendor Payment
        if (isVendorPayment())
            sb.append("; Payee=" + payeeID);	
        //            sb.append("; Payee=" + payeeName);	

        // Serial #
        sb.append("; Serial #" + serialNumber); 

        return sb.toString();
    }

    /* Used by DiagReport to format Message to look like Plutto output. */
    public String toStringDocID() {
        StringBuffer sb = new StringBuffer();

        // document Number
        sb.append("DOC" + documentNumber + "-");	 

        // document Type
        int i = getIntDocumentType();
        if (i < docName.length)
            sb.append(docName[i]);
        else
            sb.append("Unknown Doc Type #" + documentType); 
        sb.append(": ");	

        return sb.toString();
    }

    @Override
	public String toString() {
        return "MoneyOrderInfo { "	
            + "\r\n    serialNumber = "	
            + serialNumber
            + "\r\n    amount = "	
            + amount
            + "\r\n    fee = "	
            + fee
            + "\r\n    payeeName = "	
            + payeeName
            + "\r\n    creationDateTime = "	
            + creationDateTime
            + "\r\n    period = "	
            + period
            + "\r\n    dateTimePrinted = "	
            + dateTimePrinted
            + "\r\n    attempted = "	
            + attempted
            + "\r\n    printed = "	
            + printed
            + "\r\n    completed = "	
            + completed
            + "\r\n    voidFlag = "	
            + voidFlag
            + "\r\n    voidReason = "	
            + voidReason
            + "\r\n    trainingFlag = "	
            + trainingFlag
            + "\r\n    documentType = "	
            + documentType
            + "\r\n    documentNumber = "	
            + documentNumber
            + "\r\n    reportDocumentType = "	
            + reportDocumentType
            + "\r\n    dispenserNumber = "	
            + dispenserNumber
            + "\r\n    employeeID = "	
            + employeeID
            + "\r\n    documentName = "	
            + documentName
            + "\r\n    deltagramFlag = "	
            + deltagramFlag
            + "\r\n    payeeID = "	
            + payeeID
            + "\r\n    taxID = "	
            + taxID
            + "\r\n    discountAmount = "	
            + discountAmount
            + "\r\n    discountPercentage = "	
            + discountPercentage
            + "\r\n} MoneyOrderInfo\n";	
    }

    /**
     * Create a new MoneyOrderInfo.
     */
    public MoneyOrderInfomation(
        final int rdt,
        final int userId,
        final BigDecimal amount,
        final BigDecimal fee) {
        this(rdt, userId, amount, "", fee);	
    }

    /** Creates a new MoneyOrderInfo.
    */
    public MoneyOrderInfomation(
        final int rdt,
        final int userId,
        final BigDecimal amount,
        final String payeeName) {
        this(rdt, userId, amount, payeeName, BigDecimal.ZERO);
    }

    /** Creates a new MoneyOrderInfo.
    */
    private MoneyOrderInfomation(
        final int rdt,
        final int userId,
        final BigDecimal amount,
        final String payeeName,
        final BigDecimal fee) {
        setReportDocumentType(rdt);
        setEmployeeID(userId);
        setAmount(amount);
        setFee(fee);
        setPayeeName(payeeName);
        creationDateTime = TimeUtility.currentTimeMillis();
        period = UnitProfile.getInstance().getCurrentPeriod();
    }

    public boolean validate() {
        return true;
    }

    /** Returns the serial number of this money order.
    */
    public BigInteger getSerialNumber() {
        return (serialNumber);
    }
    public void setSerialNumber(BigInteger number) {
        serialNumber = number;
    }

    /** Returns serial number of this money order, including check digit. */
    public String getSerialNumberWithCheckDigit() {
        return ""+serialNumber + checkDigit(serialNumber.longValue());
    }

    /**
     * Computes check digit. 
     */
    private static long checkDigit(long serial) {
        return (serial % 11) % 10;
    }

    /**
     * Get the date/time that this money order was created.
     */
    public long getCreationDateTime() {
        return creationDateTime;
    }

    /**
     * Get the amount of this money order.
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Get the fee of this money order.
     */
    public BigDecimal getFee() {
        return fee;
    }

    /**
     * Indicates if there is a fee
     */
//    public boolean hasFee() {
//       return (getFee().compareTo(BigDecimal.ZERO) > 0);
//    }

    /**
     * Get the name of the vendor this payment is for.
     */
    public String getPayeeName() {
        return payeeName;
    }

    /**
     * Get the ID of the vendor this payment is for.
     */
    public BigInteger getPayeeID() {
        return payeeID;
    }

    /**
     * Get the discount amount of an associate discount.
     */
    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    /**
     * Get the discount percentage of an associate discount.
     */
    public BigDecimal getDiscountPercentage() {
        return discountPercentage;
    }

    /**
     * Get whether or not this money order has been printed.
     * Note: A return value of false means either this item
     * was never sent to the dispenser or this item is in
     * limbo.
     */
    public boolean isPrinted() {
        return printed;
    }

    /**
     * Returns true if it can not be determined whether this
     * money order really printed or not. "Limbo" means the
     * dispenser was sent the command to print the item, but
     * we didn't get a definitive response back as to whether
     * it printed successfully. Since printing status isn't
     * stored here, the return value is deduced from
     * circumstancial evidence.
     */
    public boolean isLimbo() {
        return (!printed && (serialNumber!=null));	
    }

    /**
     * Indicates if the money order is in limbo.
     * Note this method just returns <code>!isPrinted()</code>.
     */
    public boolean inLimbo() {
        return !isPrinted();
    }

    /** Sets the printed status. This is similar to setPrinted,
      * except the argument is inverted and the dateTimePrinted
      * is not changed by this method.
      */
    public void setLimbo(boolean limbo) {
        this.printed = !limbo;
    }

    /**
     * Set the printed status of this money order.
     */
    public void setPrinted(boolean printed) {
        this.printed = printed;
        dateTimePrinted = TimeUtility.currentTimeMillis();
    }

    /**
     * Set the amount of this money order
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * Set the fee for this money order.
     */
    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    /**
     * Set the name of the payee for this money order.
     */
    public void setPayeeName(String payeeName) {
        if (payeeName != null) {
            this.payeeName = payeeName;
        }
        else {
            this.payeeName = "";	
        }
    }

    /**
     * Set the ID of the payee for this money order.
     */
    public void setPayeeID(BigInteger payeeID) {
        this.payeeID = payeeID;
    }

    /**
     * Returns the payee ID in integer form
     */
    public int getIntPayeeID() {
    	return getPayeeID().intValue();
    }

    /**
     * Set the discount amount of an associate discount.
     */
    public void setDiscountAmount(BigDecimal da) {
        if (da != null) {
            discountAmount = da;
        }
        else {
            discountAmount = BigDecimal.ZERO;
        }
    }

    /**
     * Set the discount percentage of an associate discount.
     */
    public void setDiscountPercentage(BigDecimal dp) {
        if (dp != null) {
            discountPercentage = dp;
        }
        else {
            discountPercentage = BigDecimal.ZERO;
        }
    }

    /**
     * Get the date/time that this money order was printed.
     */
    public long getDateTimePrinted() {
        return dateTimePrinted;
    }

    /**
     * Set the date/time that this money order was created.
     * This variable is used when a new MO Info object is created.
     * The ability to change the variable is needed when commiting
     * the TempLog.
     */
    public void setCreationDateTime(Date dtPrinted) {
        this.creationDateTime = dtPrinted.getTime();
    }

    public void setAttempted(boolean attempted) {
        this.attempted = attempted;
    }

    public boolean wasAttempted() {
        return attempted;
    }

    /**
     * Set the completion status of this money order. True means it has been
     *   printed and sent to the host successfully.
     */
    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    /**
     * Get the completion status of this money order. True means that it has
     *   been printed and sent to the host successfully.
     */
    public boolean getCompleted() {
        return completed;
    }

    /** Returns whether or not the void flag is set
    */
    public boolean getVoidFlag() {
        return voidFlag;
    }
    public void setVoidFlag(boolean flag) {
        voidFlag = flag;
        voidReason = "";	
    }
    public void setVoidFlag(boolean flag, int reason) {
        voidFlag = flag;
        voidReason = reasonCode[reason];
    }

    /** Returns the reason code for a void.
    */
    public String getVoidReason() {
        return (voidReason);
    }
    public void setVoidReason(String vReason) {
        voidReason = vReason;
    }

    /** Returns whether or not the training flag is set.
    */
    public boolean getTrainingFlag() {
        return (trainingFlag);
    }
    public void setTrainingFlag(boolean torf) {
        trainingFlag = torf;
    }

    /** Returns the document type string.
        This should be 1 for MO, 2 for VP, 3 for GC, or 4 for DG.
    */
    public String getDocumentType() {
        return (documentType);
    }
    public void setDocumentType(String docType) {
        documentType = docType;
    }

    /**
     * Returns the document type in integer form
     */
    public int getIntDocumentType() {
        try {
            final String documentType = getDocumentType();
            return Integer.parseInt(documentType);
        }
        catch (NumberFormatException nfe) {
            return 0;
        }
    }

    /**
     * Indicates if this is a vendor payment
     */
    public boolean isVendorPayment() {
        return getDocumentType().equals("2");	
    }

    /** Returns the document number string.
    */
    public BigInteger getDocumentNumber() {
        return (documentNumber);
    }
    public void setDocumentNumber(BigInteger docNumber) {
        documentNumber = docNumber;
    }

    /** Returns the report document type.
    */
    public int getReportDocumentType() {
        return reportDocumentType;
    }
    public void setReportDocumentType(final int rdt) {
        reportDocumentType = rdt;
    }

    /**
     * Returns the document type in integer form
     */
    public int getIntDocumentNumber() {
    	return getDocumentNumber().intValue();
    }

    /** Returns the Dispenser ID number, where '1' is DISP1 and '2' is DISP2.
    */
    public String getDispenserNumber() {
        return (dispenserNumber);
    }
    public void setDispenserNumber(String disp) {
        dispenserNumber = disp;
    }

    /**
     * Returns the dispenser number in int form
     */
    public int getIntDispenserNumber() {
        try {
            final String dispenserNumber = getDispenserNumber();
            return Integer.parseInt(dispenserNumber);
        }
        catch (NumberFormatException nfe) {
            return 0;
        }
    }

    /** Returns the employee ID.
    */
    public String getEmployeeID() {
        return (employeeID);
    }
    public void setEmployeeID(int empID) {
        if ((empID < 0) || (empID > 99)) {
            this.setEmployeeID("00");	
        }
        else if (empID < 10) {
            this.setEmployeeID("0" + String.valueOf(empID));	
        }
        else {
            this.setEmployeeID(String.valueOf(empID));
        }
    }
    public void setEmployeeID(String empID) {
        if ((empID != null) && (empID.length() == 2)) {
            employeeID = empID;
        }
    }

    /**
     * Returns the employee ID in int form
     */
    public int getIntEmployeeID() {
        try {
            String employeeID = getEmployeeID();
            if (employeeID == null)
                return 0;
            if ((employeeID.length() > 1) && (employeeID.charAt(0) == '0'))
                employeeID = employeeID.substring(1);
            return Integer.parseInt(employeeID);
        }
        catch (NumberFormatException nfe) {
            return 0;
        }
    }

    /** Returns the sale issuance tag string.
        This is a '1' or '0'.  Each complete sale flips between these
        two values.
    */
    public String getSaleIssuanceTag() {
        return (saleIssuanceTag);
    }
    public void setSaleIssuanceTag(String sitag) {
        saleIssuanceTag = sitag;
    }

    /** Returns the multi-issue indicator. Y means this item is part of a
      * larger group, N means this item was sold by itself.
      */
    public String getMultiIssueIndicator() {
        return multiIssueIndicator;
    }
    public void setMultiIssueIndicator(String s) {
        multiIssueIndicator = s;
    }

    /** Returns the limit indicator. Y means the CTR limit was overridden,
      * N means it wasn't.
      */
    public String getLimitIndicator() {
        return limitIndicator;
    }
    public void setLimitIndicator(String s) {
        limitIndicator = s;
    }

    /** Returns the document name. Such as "MONEY ORDER" or "VENDOR PAYMENT".
      */
    public String getDocumentName() {
        return documentName;
    }
    public void setDocumentName(String s) {
        documentName = s;
    }

    /** Returns the DeltaGram flag. Such as "AG" or "CU".	 
      */
    public String getDeltagramFlag() {
        return deltagramFlag;
    }
    public void setDeltagramFlag(String s) {
        deltagramFlag = s;
    }

    /** Returns the period when the MO was issued */
    public byte getPeriod() {
        return period;
    }

    /** Returns the tax ID, which we use for reference number */
    public String getTaxID() {
        return taxID;
    }
    public void setTaxID(String s) {
        taxID = s;
    }

    /**
     * Indicates if this is remote, meaning issued by device attached to the
     * DELTA NETWORK terminal, hardcoded to false
     */
    public boolean isRemote() {
        return false;
    }
}
