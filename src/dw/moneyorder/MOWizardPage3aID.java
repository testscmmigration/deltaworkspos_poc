/*
 * Created on Nov 5, 2003
 */
package dw.moneyorder;

import java.awt.Component;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.moneygram.agentconnect.ComplianceTransactionRequest;
import com.moneygram.agentconnect.CustomerComplianceInfo;
import com.moneygram.agentconnect.CustomerComplianceTypeCodeType;
import com.moneygram.agentconnect.PhotoIdType;

import dw.dialogs.Dialogs;
import dw.dwgui.MultiListComboBox;
import dw.dwgui.StateListComboBox;
import dw.framework.ClientTransaction;
import dw.framework.DataCollectionScreenToolkit;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.framework.DataCollectionScreenToolkit.DWComboBoxItem;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.CountryInfo.Country;
import dw.model.adapters.CountryInfo.CountrySubdivision;
import dw.utility.DWValues;
import dw.utility.TimeUtility;
/**
 * @author Renuka Easwar
 *
 * This screen prompts the customer to enter required photoId information
 * and sends request to the RTS for aggregation and OFAC check on the supplied
 * name and photoID information.
 */
public class MOWizardPage3aID extends MoneyOrderFlowPage {
	private static final long serialVersionUID = 1L;

	private JTextField firstNameField;
    private JTextField middleInitialField;
    private JTextField lastNameField;
    private MultiListComboBox<DWComboBoxItem> typeBox;
    private JTextField numberField;
    private MultiListComboBox<Country> countryBox;
    private StateListComboBox<CountrySubdivision> stateBox;
    private ComplianceTransactionRequest requestInfo;
    private JLabel amountLabel;

    public MOWizardPage3aID(
        MoneyOrderTransaction40 tran,
        String name,
        String pageCode,
        PageFlowButtons buttons) {
        this((MoneyOrderTransaction) tran, name, pageCode, buttons);
    }

    public MOWizardPage3aID(MoneyOrderTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "moneyorder/MOWizardPage3aID.xml", name, pageCode, buttons);	
        
        firstNameField = (JTextField) getComponent("firstNameField");	
        middleInitialField = (JTextField) getComponent("middleInitialField");	
        lastNameField = (JTextField) getComponent("lastNameField");	
        typeBox = (MultiListComboBox<DWComboBoxItem>) getComponent("typeBox");	
        numberField = (JTextField) getComponent("numberField");	
        countryBox = (MultiListComboBox<Country>) getComponent("countryBox");	
        stateBox = (StateListComboBox<CountrySubdivision>) getComponent("stateBox");	
        amountLabel = (JLabel) getComponent("amountLabel"); 

        List<DWComboBoxItem> photoIdTypeList = DataCollectionScreenToolkit.toComboBoxItemsList(transaction.getPhotoIDNames(), DWValues.getPhotoIdTypes());
        typeBox.addList(photoIdTypeList, DWValues.PHOTO_ID_DEFAULT, "types"); 

        List<Country> countries = CountryInfo.getCountries(CountryInfo.OK_FOR_ADDRESS);
		countryBox.addList(countries, "countries"); 

        populateStateBox(stateBox, CountryInfo.getAgentCountry(), CountryInfo.getAgentCountrySubdivision());
    }

	@Override
    public void start(int direction) {
        flowButtons.reset();
        flowButtons.setVisible("expert", false);	
        flowButtons.setEnabled("expert", false);	
        
    	Country defaultCountry = CountryInfo.getAgentCountry();
    	countryBox.setSelectedComboBoxItem(defaultCountry.getCountryCode());
    	
    	updateStateList(defaultCountry, stateBox);
    	CountrySubdivision defaultState = CountryInfo.getAgentCountrySubdivision();
    	stateBox.setSelectedComboBoxItem(defaultState.getCountrySubdivisionCode());
    	
		typeBox.setSelectedComboBoxItem(DWValues.PHOTO_ID_DEFAULT);

        firstNameField.requestFocus();
        amountLabel.setText(String.valueOf(((MoneyOrderTransaction40)transaction).getAggLimit()));

        Component[] ex = { stateBox };
        addReturnTabListeners(ex, flowButtons.getButton("cancel"));	

        ActionListener countryChangeListener = new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
               	Country country = (Country) countryBox.getSelectedItem();
               	updateStateList(country, stateBox);
                
            }};
        addActionListener(countryBox, countryChangeListener);
        countryBox.setDoNotHandleEnter();
        
        //handle enter key on countryBox ourself. 

        KeyListener nexter2 = new KeyAdapter() {
			@Override
            public void keyPressed(KeyEvent e2) {
                if (e2.getKeyCode() == KeyEvent.VK_ENTER) {
                    e2.consume();
                    if (stateBox.isEnabled()) {
                        stateBox.requestFocus();
                    } else {
                        flowButtons.getButton("next").requestFocus();	
                    }
                }
            }
        };
        addKeyListener("countryBox", nexter2);	

        //DW4.0 tracker #683 - pressing enterkey while selecting a state brings up the comm dialog 
        //even though the combo box is not closed.
        addKeyListener("stateBox", new KeyAdapter() {	
			@Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    e.consume();
                    //  flowButtons.getButton("next").doClick();	
                    flowButtons.getButton("next").requestFocus();	
                }
            }
        });

        firstNameField.requestFocus();
    }

    /**  
     * Validate required fields. Send request to host for Aggegate/OFAC check. 
     */
	@Override
    public void finish(int direction) {
        if (direction == PageExitListener.NEXT) {
            JComponent[] fields = { firstNameField, lastNameField, numberField, stateBox };
            if (!validateFields(fields)) {
                return;
            }
            if (!validatePhotoIdNumber()) {
                Toolkit.getDefaultToolkit().beep();
                numberField.requestFocus();
                numberField.selectAll();
                Dialogs.showWarning ("dialogs/DialogInvalidData.xml", Messages.getString("MOWizardPage3aID.2024"));
                flowButtons.setButtonsEnabled(true);
                return;
            } else {
                setCci();
                transaction.getAggregationCheck(requestInfo, this);
            }
        } else {
            PageNotification.notifyExitListeners(this, direction);
        }
    }
    /**
     * Populate the CustomerComplianceInfo with the values entered on screen.
     */
    private void setCci() {
    	CustomerComplianceInfo cci = new CustomerComplianceInfo();
        cci.setFirstName(firstNameField.getText());
        cci.setMiddleInitial(middleInitialField.getText());
        cci.setLastName(lastNameField.getText());
        cci.setPhotoIdType(PhotoIdType.fromValue(((DWComboBoxItem) typeBox.getSelectedItem()).getValue()));
        cci.setPhotoIdNumber(numberField.getText());
        cci.setCountry(((Country) countryBox.getSelectedItem()).getCountryCode());
		String value = ((CountrySubdivision) stateBox.getSelectedItem()).getCountrySubdivisionCode();
        cci.setState(((value != null) && (value.length() == 5)) ? value.substring(3, 5) : null);
        cci.setTypeCode(CustomerComplianceTypeCodeType.OFAC_OR_AGGREGATION);
        cci.setLocalDateTime(TimeUtility.toXmlCalendar(TimeUtility.currentTime()));
        requestInfo = new ComplianceTransactionRequest();
        requestInfo.setCci(cci);
        transaction.saveCci(cci);
    }

    /**
     * validate that the photoId number entered on screen 
     * contains atleast one letter or digit.
     */
    private boolean validatePhotoIdNumber() {
        String number = numberField.getText();
        char c;
        boolean result = true;
        for (int i = 0; i < number.length(); i++) {
            c = number.charAt(i);
            if ((Character.isLetterOrDigit(c))) {
                result = true;
                break;
            } else {
                result = false;
            }
        }
        return result;
    }

    /**
         * Called by transaction after it receives response from middleware.
         * ReturnValue is Boolean set to true if response is good. 
         * Otherwise, it is set to false.
         * If any of the name or photoId information id was changed as determined
         * by NEEDS_RECOMMIT then send the message to the RTS requesting for Aggregate/OFAC
         * check. 
         * If the transaction failed for OFAC reasons, print the receipts and return to main menu. 
         */
	@Override
    public void commComplete(int commTag, Object returnValue) {
	    String sourceNumber;
	    if (returnValue.equals(Boolean.TRUE)) {
	        PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
	    } else if (transaction.getStatus() == ClientTransaction.NEEDS_RECOMMIT) {
	        transaction.getAggregationCheck(requestInfo, this);
	    } else if (transaction.getStatus() == ClientTransaction.FAILED) {
	        sourceNumber = ((MoneyOrderTransaction40) transaction).sourceNumber;
	        if(((MoneyOrderTransaction40) transaction).checkPrinterReady(this)){
	            ((MoneyOrderTransaction40) transaction).printOfacErrorReceipt(true, sourceNumber);
	            ((MoneyOrderTransaction40) transaction).printOfacErrorReceipt(false, sourceNumber);
	        }
	        PageNotification.notifyExitListeners(this, PageExitListener.FAILED);
	        return;
	    } else {
	        flowButtons.setButtonsEnabled(true);
	        PageNotification.notifyExitListeners(this, PageExitListener.DONOTHING);
	    }
    }
}
