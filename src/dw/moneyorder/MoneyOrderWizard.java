package dw.moneyorder;

import java.awt.Component;
import java.awt.event.KeyEvent;

import dw.framework.ClientTransaction;
import dw.framework.FlowPage;
import dw.framework.KeyMapManager;
import dw.framework.PageExitListener;
import dw.framework.PageNameInterface;

/**
 * The Wizard Interface for issuing Money Orders.
 */
public class MoneyOrderWizard extends MoneyOrderTransactionInterface {
	private static final long serialVersionUID = 1L;

	public MoneyOrderWizard(MoneyOrderTransaction transaction, PageNameInterface naming) {
        super("moneyorder/MOWizardPanel.xml", transaction, naming, true);	
        createPages();
    }

    /**
     * Add custom page traversal logic here. If no custom logic needed, just
     *   call super.exit(direction) to do default traversal.
     */
    @Override
	public void exit(int direction) {
        String currentPage = cardLayout.getVisiblePageName();
        
        
 //       if ((direction == PageExitListener.TIMEOUT || direction == PageExitListener.CANCELED)
 //           && currentPage.equals("additionalAmounts")) {	
 //           MOWizardPage2 p2 = (MOWizardPage2) cardLayout.getVisiblePage();
 //       }

        
        if (direction == PageExitListener.CANCELED) {
            transaction.escSAR();
            ((MoneyOrderTransaction40) transaction).unlockCciLog();
            super.exit(direction);
            return;
        }
        if (direction == PageExitListener.DONOTHING)
           return;         

        if (direction == PageExitListener.COMPLETED) {
            transaction.f2NAG();
            ((MoneyOrderTransaction40) transaction).unlockCciLog();
            super.exit(direction);
            return;
        }

        if (direction == PageExitListener.TIMEOUT) {
            ((MoneyOrderTransaction40) transaction).unlockCciLog();
            super.exit(direction);
            return;
        }

        if (direction == PageExitListener.FAILED) {
            ((MoneyOrderTransaction40) transaction).unlockCciLog();
        }

        if (direction != PageExitListener.NEXT) {
            super.exit(direction);
            return;
        }

        if ((currentPage.equals("orderTable"))	
            && ((MoneyOrderTransaction40) transaction).isPhotoIdNeeded()) {
            ((MoneyOrderTransaction40) transaction).lockCciLog();
            showPage("PhotoID", PageExitListener.NEXT); 
            return;
        }
        if ((currentPage.equals("orderTable"))	
            && !((MoneyOrderTransaction40) transaction).isPhotoIdNeeded()) {
            ((MoneyOrderTransaction40) transaction).lockCciLog();
            showPage("summary", PageExitListener.NEXT); 
            return;
        }
        if (currentPage.equals("summary")	
            && transaction.getStatus() != ClientTransaction.COMPLETED
            && transaction.isTransactionPrinted()) {
            showPage("printCanceled", PageExitListener.NEXT); 
            return;
        }
        // default traversal if no special case
        super.exit(direction);

    }

    @Override
	protected void createPages() {
        addPage(MOWizardDiscPage1.class, transaction, "cardNumberEntry", "MOW03", buttons);	 
        addPage(MOWizardPage1.class, transaction, "firstAmount", "MOW05", buttons);	 
        addPage(MOWizardPage2.class, transaction, "additionalAmounts", "MOW10", buttons);	 
        addPage(MOWizardPage3.class, transaction, "orderTable", "MOW15", buttons);	 
        addPage(MOWizardPage3aID.class, transaction, "PhotoID", "MOW18", buttons);	 
        addPage(MOWizardPage4.class, transaction, "summary", "MOW20", buttons);	 
        addPage(MOWizardPage5.class, transaction, "printSuccessful", "MOW25", buttons);	 
        addPage(MOWizardPage6.class, transaction, "printCanceled", "MOW30", buttons);	 
    }

    /**
     * Add the single keystroke mappings to the buttons. Use the function keys
     *   for back, next, cancel, etc.
     */
    private void addKeyMappings() {
        KeyMapManager.mapMethod(this, KeyEvent.VK_F2, 0, "f2SAR");	
        buttons.addKeyMapping("expert", KeyEvent.VK_F4, 0);	
        buttons.addKeyMapping("back", KeyEvent.VK_F11, 0);	
        buttons.addKeyMapping("next", KeyEvent.VK_F12, 0);	
        buttons.addKeyMapping("cancel", KeyEvent.VK_ESCAPE, 0);	
    }

    @Override
	public void start() {
        transaction.clear();

        // ask transaction if fee discount option is enabled
        if (transaction.isAssociateDiscountAvailable())
            super.start();
        else
            super.start(1);
        addKeyMappings();
    }

    public void f2SAR() {
        Component current = cardLayout.getVisiblePage();
        if (current instanceof FlowPage) {
            ((FlowPage) current).updateTransaction();
        }
        transaction.f2SAR();
    }
}
