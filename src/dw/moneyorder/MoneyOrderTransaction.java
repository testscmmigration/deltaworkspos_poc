package dw.moneyorder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Vector;

import com.moneygram.agentconnect.ComplianceTransactionRequest;
import com.moneygram.agentconnect.CustomerComplianceInfo;
import com.moneygram.agentconnect.MoneyOrderInfo;
import com.moneygram.agentconnect.MoneyOrderPrintStatusType;

import dw.dispenser.DispenserFactory;
import dw.dispenser.DispenserInterface;
import dw.dispenser.MoneyOrderDisplayFields;
import dw.framework.ClientTransaction;
import dw.framework.CommCompleteInterface;
import dw.framework.DataCollectionSet;
import dw.framework.PrintingProgressInterface;
import dw.framework.DataCollectionSet.DataCollectionType;
import dw.i18n.Messages;
import dw.io.report.ReportLog;
import dw.profile.FeeTable;
import dw.profile.Profile;
import dw.profile.ProfileItem;
import dw.profile.UnitProfile;
import dw.utility.Debug;
import dw.utility.ErrorManager;

/**
 * MoneyOrderTransaction encapsulates the information about a group of money
 *   orders and the communication between the client and the host. It allows
 *   the user to populate information about the money orders and send them to
 *   the server when finished.
 * Note : This transaction is also used for Vendor Payments. With a vendor
 *   payment, there is simply the addition of a payee on each money order.
 *
 */
public abstract class MoneyOrderTransaction extends ClientTransaction {
	private static final long serialVersionUID = 1L;

	//return codes for addMoneyOrder(MoneyOrderInfo)
    public static final int OK = 0;
    public static final int MAX_ITEM_AMOUNT_EXCEEDED = 1;
    public static final int MAX_FORMS_PER_SALE_EXCEEDED = 2;
    public static final int REMAINING_ITEMS_EXCEEDED = 3;
    public static final int DAILY_LIMIT_EXCEEDED = 4;
    public static final int TRANSACTION_AMOUNT_EXCEEDED = 5; // (a.k.a. CTR)
    public static final int MAX_BATCH_AMOUNT_EXCEEDED = 6;
    public static final int MAX_BATCH_COUNT_EXCEEDED = 7;
    public static final int MAX_ITEM_AND_FORMS_EXCEEDED = 8;
    private BigDecimal amountTendered;
    protected BigDecimal total;
    
    protected FeeTable feeTable = null;

    private String referenceNumber = null;

    // additional info for vendor payment transaction
    private String selectedVendor = null;

    // flag to help determine if a cancel was during a print or before printing
    protected boolean transactionPrinted = false;

    /** The ever-toggling Sale Issuance Tag.
    */
    protected static String currentSaleIssuanceTag = "0";	

    // tag whether or not this is a transaction for a vendor payment
    private boolean vendorPayment = false;

    protected HashMap<String, String> vendorMap;

    // tag whether or not the last entered money order was split up
    //changing visibilty to use in MOT40
    protected boolean lastEntrySplit = false;

    // keep this for querying
    protected BigDecimal dailyTotal;

    // Associate Card Number from MOWizardDiscPage1
    private String discountCardNumber;
    
    //  create Vector of MoneyOrderInfo40s to use in MoneyOrderTransaction40
    public List<MoneyOrderInfo> moItemData;
    public List<MoneyOrderDisplayFields> modfItemData;

	protected CustomerComplianceInfo cci;

	/** Toggles the current state of the Sale Issuance Tag.
    */
    protected synchronized void toggleSaleIssuanceTag() {
        if( currentSaleIssuanceTag.equalsIgnoreCase("0")) {	
            currentSaleIssuanceTag = "1";	
        }
        else {
            currentSaleIssuanceTag = "0";	
        }
    }

    public MoneyOrderTransaction(String tranName, String tranReceiptPrefix, 
                                 int documentSequence, boolean vendorPayment) {
        super(tranName, tranReceiptPrefix, documentSequence);
        this.vendorPayment = vendorPayment;
//        moneyOrderList = new ArrayList();
        documentName = getProductProfileOption("DOCUMENT_NAME").stringValue();	
        requiredFormCount = 1;
        receiptPrinterNeeded = false;
        moItemData = new ArrayList<MoneyOrderInfo>();
        modfItemData = new Vector<MoneyOrderDisplayFields>();
        clear();
    }

    public BigDecimal getDailyTotal() {
        return dailyTotal;
    }

    public boolean isLastEntrySplit() {
        return lastEntrySplit;
    }

    public void unsetLastEntrySplit() {
        lastEntrySplit = false;
    }
    
    /**
     * Clear out all information that is currently in this transaction.
     */
    @Override
	public void clear() {
        super.clear();
//        moneyOrderList.clear();
        moItemData.clear();
        modfItemData.clear();
        total = ZERO;
        amountTendered = ZERO;
        feeTable = null;
        tranLimitOverridden = false;
        transactionPrinted = false;
        discountCardNumber=null;
        cci = null;
    }

    public void setReferenceNumber(String reference){
        this.referenceNumber = reference;
    }
    
    public String getReferenceNumber(){
        return referenceNumber;
    }
       
    /**
     * Has this transaction tried to print? This will distinguish print canceled
     *   case from just normal cancel.
     */
    public boolean isTransactionPrinted() {
        return transactionPrinted;
    }

    public void setTransactionPrinted(boolean printed) {
        transactionPrinted = printed;
    }

    public boolean isVendorPayment() {
        return vendorPayment;
    }

    public String getSelectedVendor() {
        return selectedVendor;
    }

    public void setSelectedVendor(String vendor) {
        this.selectedVendor = vendor;
    }

    public String[] getDemoVendors() {
        String[] vendors = {"Joe's Crunchy Munchies", "ACME Beer Co.",  
                            "Pepsi Co.", "Coca-Cola Co.", "JW Distribution Co."};   
        vendorMap = new HashMap<String, String>();
        for (int i = 0; i < vendors.length; i++)
            vendorMap.put(vendors[i], pad2(i));
        return vendors;
    }

    /**
     * Obtain a list of vendors from the profile for the first time. If there
     * are no vendors, return null.
     * @return An array of Strings representing vendors/payees.  If no vendors
     * exist, null will be returned.
     */
    public String[] getVendorList() {
        if (UnitProfile.getInstance().isDemoMode())
            return getDemoVendors();
        Profile payeeTable = null;
        try {
            payeeTable = (Profile) UnitProfile.getInstance().getProfileInstance().find("PAYEE_TABLE");	
        }
        catch (NoSuchElementException x) {
            // Could not find the PAYEE_TABLE item in the profile, so return
            // an empty list of payees
            ErrorManager.handle(x, "missingPayeeTableError", Messages.getString("ErrorMessages.missingPayeeTableError"), ErrorManager.ERROR, 	
                                                                        false);
            return null;
        }


        Vector<String> vec = new Vector<String>();
        vendorMap = new HashMap<String, String>();
        for (Iterator<ProfileItem> it = payeeTable.iterator(); it.hasNext(); ) {
            ProfileItem payee = it.next();
            if (payee.getStatus().equals("A")) {	
                vec.add(payee.stringValue());
                vendorMap.put(payee.stringValue(), pad2(payee.getID()));
            }
        }
        if (vec.size() == 0)
            return null;
        else
            return vec.toArray(new String[0]);
    }

     /**
     *     
     */     
     public String getDiscountCardNumber(){
        return discountCardNumber;
     }
     
     /**     
     *     
     */
     public void setDiscountCardNumber(String cardNumber){
        discountCardNumber = cardNumber;    
     }
    

    /**
     * Returns true if the amount tendered field should be shown.
     */
    public boolean isAmountTenderedAvailable() {
          return isProductProfileOptionTrue("AMOUNT_TENDERED");	
    }
   
    /**
      * Returns true if the Aggregation check is enabled 
      */
    public boolean isAggCheckEnable() {
        return isProductProfileOptionTrue("AGG_ENABLE");      	
    }


    /**
     * Returns true if the print purchaser line field should be shown.
     */
    public boolean isPrintPurchaserAvailable() {
          return isProductProfileOptionTrue("PRINT_PURCHASER");	
    }

    /**
     * Returns true if fees need to be computed.
     */
    public boolean isFeeEnabled() {
        return isProductProfileOptionTrue("FEE_ENABLED");	
    }

    /**
     * Returns true if the user should have the option to not charge a fee. If
     *   there is no user for this transaction, then just use the product's
     *   no fee option.
     */
    public boolean isNoFeeAvailable() {
        if (isPasswordRequired())
              return isProductProfileOptionTrue("NO_FEE_OPTION")	
                        && isUserProfileOptionTrue("NO_FEE_ISSUE");	
        else
            return isProductProfileOptionTrue("NO_FEE_OPTION");	
    }
    
    /** 
     * Returns true if the GUI should show extra screens and verbiage for 
     * the Wal Mart associate discount.
     */
    public boolean isAssociateDiscountAvailable() {
        return isProductProfileOptionTrue("DISCOUNT_OPTION");	
    }
    

    /**
     * Return the discount option card prefix, or an empty string
     * if it isn't set in the profile.
     */
     public String getDiscountOptionCardPrefix(){
        String prefix = "";	
        ProfileItem item = getProductProfileOption("DISCOUNT_PREFIX");	
        if (item != null){
            prefix = item.stringValue();
            }
            return prefix;
        }

    /**
     *Return the discount percentage, or zero if it isn't set in the profile.
     */
    public BigDecimal getDiscountOptionPercentage(){
        BigDecimal discount = ZERO;
        ProfileItem item = getProductProfileOption("DISCOUNT_PERCENTAGE");	
        if (item != null){
            discount = item.bigDecimalValue();
        }
        return discount;
    }

    /** Return true if an associate discount should be applied. */
    protected boolean shouldApplyAssociateDiscount() {
        return (isAssociateDiscountAvailable() 
                && (getDiscountCardNumber() != null) 
                && (getDiscountCardNumber().length() > 0));
    }

    private static BigDecimal ONE_HUNDRED = new BigDecimal("100");	

    /**
     * Return the fee after applying an associate discount to the given fee. 
     * Implmentation note: Rounding 0.005 down before the subtraction is the 
     * same as rounding it up after the subtraction.
     */
    protected static BigDecimal computeDiscountedFee(BigDecimal original, BigDecimal percentage) {
        return original.subtract(original.multiply(percentage).divide(ONE_HUNDRED, 2, BigDecimal.ROUND_HALF_DOWN));
    }
    
    /**
     * Check the fee schedule to get the fee amount for the given MO amount.
     * Return BigDecimal(0) if fees are disabled.
     */
    public BigDecimal computeFee(BigDecimal amount) {
         
          if (! isFeeEnabled())
              return BigDecimal.ZERO;
          
          try {
              feeTable = (FeeTable) getProductProfileOption("FEE_TABLE");	
              //feeTable = (FeeTable) productProfile.find("FEE_TABLE");	
          }
          catch (NoSuchElementException e) {
              return BigDecimal.ZERO;
          }
           
          return feeTable.getFee(amount);
    }

    /**
     * Check the requested money order against limits. Checks that adding
     * count items will not exceed the number of forms per sale or the
     * number of remaining items. Checks that adding an item of newAmount
     * does not exceed the per-item limit. Checks that adding an item of
     * newAmount in place of an item of oldAmount does not exceed the per-
     * transaction limit or the daily limit. Returns a status code.
     */
    protected abstract int checkLimits(int count, BigDecimal newAmount,
            BigDecimal oldAmount);
    
    /**
     * Plain money order. Send null as the payee name.
     */
    public int setMoneyOrder(int index, BigDecimal amount, boolean chargeFee) {//BigDecimal fee) {
        return setMoneyOrder(index, amount, chargeFee, null);
    }

    /**
     * Set the values of the money order at the particular index. If a value
     * of 0 is set for the money order, remove the money order from the list.
     * If the index is greater than the current money order count, attempt to
     * create a new money order and add it. If the Deltagram flag is true, then
     * any money order over the max item amount will not be split into multiple
     * money orders.
     * Return a code from above to indicate status of the set.
     */
    public abstract int setMoneyOrder(int index, BigDecimal amount, 
            boolean chargeFee, 
            String payeeName);

   /**
     * Delete multiple money orders from this transaction. Ignore any indexes
     * given that are beyond the end of the list.
     */
    public void deleteMoneyOrders(int[] indexes) {
        for (int i = 0; i < indexes.length; i++) {
            deleteMoneyOrder(indexes[i]);
        }
    }

    /**
     * Delete a money order from this transaction. Ignore any indexes that
     * are beyond the end of the list.
     */
    public abstract void deleteMoneyOrder(int index);
    
//    public void deleteMoneyOrder(int index) {
//          MoneyOrderInfo mo;
//        if (index < moneyOrderList.size()) {
//            mo = (MoneyOrderInfo) moneyOrderList.get(index);
//            total = total.subtract(mo.getAmount());
//            moneyOrderList.remove(index);
//        }
//    }

//    /**
//     * Get the list of money orders. List contains MoneyOrderInfos.
//     * @deprecated Probably wrong, because nothing populates this list. 
//     */
//    public List getMoneyOrders() {
//        return moneyOrderList;
//    }
    
    public abstract List<MoneyOrderInfo> getMoneyOrderList();
    public abstract int getMoneyOrderCount();

//    /**
//     * Get the money order at a given index. If the index is past the range
//     * of the current money orders, create a new empty one and return it.
//     * Note: this method no longer adds such a newly created empty money order
//     * to the list.
//     * Note : the money order being returned has a default fee of zero on it.
//     *   This allows the GUI to default the fee button to being selected. The
//     *   fee will just be overwritten anyway.
//     */
//    public MoneyOrderInfo getMoneyOrder(int index) {
//        MoneyOrderInfo anMOI = null;
//        if (index >= moneyOrderList.size()) {
//            int reportDocumentType;
//            if (documentType == 1)
//                reportDocumentType = ReportLog.MONEY_ORDER_TYPE;
//            else
//                reportDocumentType = ReportLog.VENDOR_PAYMENT_TYPE;
//            anMOI = new MoneyOrderInfo(reportDocumentType,userID,ZERO,new BigDecimal("0.01"));	
//            anMOI.setDocumentType( String.valueOf(this.documentType));
//        }
//        else
//            anMOI = (MoneyOrderInfo) moneyOrderList.get(index);
//        return( anMOI);
//    }
        
    /**
     * Set the amount tendered for this transaction.
     */
    public void setAmountTendered(BigDecimal tendered) {
        amountTendered = tendered;
    }

    /**
     * Get the amount tendered for this transaction.
     */
    public BigDecimal getAmountTendered() {
        return amountTendered;
    }

//    /**
//     * Get a list of the money orders that match the specified criteria. 
//     * Filters are ALL, PRINTED, or UNPRINTED.
//     * Changed by Geoff: Limbo items now count as having been printed.
//     */
//    public List getMoneyOrders(int filter) {
//        if (filter == FILTER_ALL)
//            return moneyOrderList;
//        ArrayList filteredList = new ArrayList();
//        for (int i = 0; i < moneyOrderList.size(); i++) {
//            MoneyOrderInfo mo = (MoneyOrderInfo) moneyOrderList.get(i);
//            if ((filter == FILTER_PRINTED && (mo.isPrinted() || mo.isLimbo())) ||
//                    (filter == FILTER_UNPRINTED && !mo.isPrinted() && !mo.isLimbo()))
//            {
//                filteredList.add(mo);
//            }
//        }
//        return filteredList;
//    }

//    /**
//     * Get the total of all the money orders that match the specified criteria
//     *   of ALL, PRINTED, or UNPRINTED. Include the fees in the total if specified
//     */
//    public BigDecimal getTotal(int filter, boolean includeFees) {
//        BigDecimal total = new BigDecimal(0);
//        for (Iterator i = getMoneyOrders(filter).iterator(); i.hasNext(); ) {
//            MoneyOrderInfo mo = (MoneyOrderInfo) i.next();
//            total = total.add(mo.getAmount());
//            if (includeFees)
//                total = total.add(mo.getFee());
//        }
//        return total;
//    }

    public abstract BigDecimal getTotal(MoneyOrderPrintStatusType filter, boolean includeFees);
    
//    /**
//     * Computes the total fee amount of all the money orders.
//     */
//    public BigDecimal getFeeTotal() {
//        BigDecimal total = new BigDecimal(0);
//        for (Iterator i = getMoneyOrders().iterator(); i.hasNext(); ) {
//            MoneyOrderInfo mo = (MoneyOrderInfo) i.next();
//            total = total.add(computeFee(mo.getAmount()));
//        }
//        return total;
//    }

    /**
     * recalculateFees is used to calculate the new associate discount fee
     * for each item.  The discount is applied to the total of the original
     * fees and the distributed equally amoung the item.  Any underage is
     * applied to the last item.
     */
    public abstract void recalculateFees();
//            if (shouldApplyAssociateDiscount()) {
//            
//            BigDecimal originalFee = new BigDecimal(0);
//            BigDecimal runningFeeTotal = new BigDecimal(0);
//            BigDecimal discountPercentage = new BigDecimal(0);
//            BigDecimal runningFeeTotalDiscounted = new BigDecimal(0);
//            BigDecimal discountedFee = new BigDecimal(0);
//            BigDecimal previousRunningFeeTotalDiscounted = new BigDecimal(0);
//
//            for (Iterator i = getMoneyOrders().iterator(); i.hasNext(); ) {
//                MoneyOrderInfo mo = (MoneyOrderInfo) i.next();
//
//                // get the non-discounted fee and discount percentage                
//                originalFee = computeFee(mo.getAmount());
//                discountPercentage = getDiscountOptionPercentage();
//
//                // runningFeeTotal is a running sum of the non-discounted
//                // fees as it iterates through the MoneyOrder list.
//                runningFeeTotal = runningFeeTotal.add(originalFee);
//
//                // compute the discount on the runningFeeTotal.
//                runningFeeTotalDiscounted = computeDiscountedFee(runningFeeTotal, discountPercentage);
//                
//                // the discount fee is the difference between the runningFeeTotalDiscounted 
//                // and the previousRunningFeeTotalDiscounted.
//                discountedFee = runningFeeTotalDiscounted.subtract(previousRunningFeeTotalDiscounted);
//                // after computing the discout fee, save the runFeeTotalDiscounted
//                // to previousRunningFeeTotalDiscounted.
//                previousRunningFeeTotalDiscounted = runningFeeTotalDiscounted;
//
//                mo.setFee(discountedFee);
//                mo.setDiscountAmount(originalFee.subtract(discountedFee));
//                mo.setDiscountPercentage(discountPercentage);
//            }
//        }
//    }

    /**
     * Get the number of money orders that have been printed.
     */
    public abstract int getPrintedCount();
// Really seems like this implementation makes more sense than the overriding
// implementation in MoneyOrderTransaction40, but I'm not going there now.
// Commented out because it is over-ridden in the sole subclass of this one.
    
//    public int getPrintedCount() {
//       return getMoneyOrders(FILTER_PRINTED).size();
//    }

    /**
     * Get the amount of change due for this transaction.
     */
    public BigDecimal getChangeAmount() {
        return amountTendered.subtract(getTotal(null, true));
    }

//    /**
//     * Get the moneyorder list size.
//     * @deprecated Probably wrong, because nothing populates this list.
//     */
//    public int getListSize() {
//        return moneyOrderList.size();
//    }
    
    /**
      * Returns the number of items remaining in the selected dispenser.
      */
    public int getRemainingItems() {
        DispenserInterface dispenser = 
            DispenserFactory.getDispenser(selectedDispenser);
        return dispenser.getRemainingCount();
    }

    public String getNextSerialNumber() {
        DispenserInterface dispenser = 
            DispenserFactory.getDispenser(selectedDispenser);
        return dispenser.getNextSerialNumber();
    }

    /** Returns the dispenser selected for this transaction. */
    public DispenserInterface getDispenser() {
        return DispenserFactory.getDispenser(selectedDispenser);
    }
    
    /** Returns the status of the given dispenser. */
    public int getDispenserStatus() {

        DispenserInterface aDispenser = this.getDispenser();
        int deviceStatus = aDispenser.queryStatus();
        int dispenserStatus = MoneyOrderTransaction.OK;
        switch( deviceStatus) {
              case DispenserInterface.OK:
                  break;// do nothing special
              case DispenserInterface.NOT_INITIALIZED:
              case DispenserInterface.NO_CTS:
              case DispenserInterface.WAITING:
              case DispenserInterface.ACK:
              case DispenserInterface.EOF:
              case DispenserInterface.BAD_RESPONSE:
              case DispenserInterface.ERROR:
              case DispenserInterface.VOIDED:
              case DispenserInterface.LOAD_REQUIRED:
                    dispenserStatus = ClientTransaction.LOAD_REQUIRED;
                    break;
              case DispenserInterface.VERIFY_REQUIRED:
                    dispenserStatus = ClientTransaction.VERIFY_REQUIRED;
                    break;
              default:
                    // This shouldn't happen.
                    dispenserStatus = ClientTransaction.LOAD_REQUIRED;
                    break;
           }
        return dispenserStatus;
    }

    public abstract void commit(final PrintingProgressInterface progressBar);
    
    public static void addToReportLog(final MoneyOrderInfomation mo, final boolean isNotDeltaGram) {
        if (isNotDeltaGram)
            ReportLog.logMoneyOrder(mo);
        }

    /**
      Go through the list of money orders and send them to the host for
      processing. If they all come back ok, then set the transaction status
      to COMPLETE, otherwise, set it to FAILED.
      The basic algorithm here is:
      <pre>
      Check the device
      Prepare the progress bar
      For each money order in the moneyOrderList:
        Get the next serial number from the device (exit if failure)
        Update progress bar and check for user cancellation
        Fill in the money order information fields
        Write to the ItemLog
        Actually print the item (exit if failure, write Item &amp;
        DiagLogs if voided)
        Mark the money order complete (for the POS information)
        Mark the money order printed (for the Middleware limbo message)
        Write to the ItemLog
        Write to the DiagnosticLog
      Remove the progress bar
      </pre>
     */
    public abstract void commit(final PrintingProgressInterface progressBar,
                                boolean isNotDeltaGram);

    
    private String pad2(int i) {
        if ((i < 0) || (i > 99)) {
            return "00";	
        }
        else if (i < 10) {
            return "0" + String.valueOf(i);	
        }
        else {
            return String.valueOf(i);
        }
    }
        
    
    /** For testing. */
    private static void testDiscount(String orig) {
        BigDecimal original = new BigDecimal(orig);
        BigDecimal discounted = computeDiscountedFee(original, new BigDecimal("10"));	
        Debug.println("original = " + orig 	
                + " discounted = " + discounted	
                + " discount amt = " + original.subtract(discounted));	
    }

    /** For testing. */
    public static void main(String args[]) {
        try {
            testDiscount("0.40");	
            testDiscount("0.41");	
            testDiscount("0.42");	
            testDiscount("0.43");	
            testDiscount("0.44");	
            testDiscount("0.45");	
            testDiscount("0.46");	
            testDiscount("0.47");	
            testDiscount("0.48");	
            testDiscount("0.49");	
            testDiscount("0.50");	
        }
        catch (Exception e) {
			Debug.printStackTrace(e);
        }
    }

    public abstract boolean isPhotoIdNeeded();
   
    public abstract String[] getPhotoIDNames();
    
    public abstract void getAggregationCheck(final ComplianceTransactionRequest ctri,
            final CommCompleteInterface callingPage);
    
    public abstract MoneyOrderInfo getMoneyOrderInfo(int i);

    private DataCollectionSet gffpDataCollectionSet = new DataCollectionSet(DataCollectionType.VALIDATION);

	public DataCollectionSet getGffpDataCollectionSet() {
		return gffpDataCollectionSet;
	}

	public void saveCci(CustomerComplianceInfo cci) {
		this.cci = cci;
	}

	public void setTranLimitOverridden(boolean b) {
		this.tranLimitOverridden = b;
	}
}
