package dw.moneyorder;

import java.awt.Color;
import java.awt.Component;
import java.awt.SystemColor;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import com.moneygram.agentconnect.MoneyOrderInfo;
import com.moneygram.agentconnect.MoneyOrderPrintStatusType;

import dw.dwgui.DWTable;
import dw.dwgui.DWTable.DWTableCellRendererInterface;
import dw.dwgui.DWTable.DWTableListener;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.dwgui.MoneyField;
import dw.framework.KeyMapManager;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;

/**
 * Page 3 of the Money Order Wizard.
 * This page displays a list of the money orders entered in a table and allows
 *   the user to edit the amounts in the table before continuing.
 */
public class MOWizardPage3 extends MoneyOrderFlowPage implements DWTextPaneListener, DWTableListener {
	private static final long serialVersionUID = 1L;

    private JTextField totalField = null;
    private DWTable table = null;
    private DefaultTableModel tableModel = null;
    private final int ITEM_COLUMN = 0;
    private final int FEE_COLUMN = 1;
    private final int AMOUNT_COLUMN = 2;
    
    private final int SCREEN_WIDTH = 350;
    
	private JScrollPane scrollpane;
	private JPanel panel;

	public MOWizardPage3(MoneyOrderTransaction40 tran, String name, String pageCode, PageFlowButtons buttons) {
		this((MoneyOrderTransaction) tran, name, pageCode, buttons);
	}
	
    public MOWizardPage3(MoneyOrderTransaction tran,
                       String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "moneyorder/MOWizardPage3.xml", name, pageCode, buttons);	
        totalField = (JTextField) getComponent("totalField");	

        table = (DWTable) this.getComponent("moneyOrderScrollpane");
        setupTable();
        
        table.addListener(this, this);
        table.setListenerEnabled(false);
        
        DWTextPane tp = (DWTextPane) getComponent("text1");
		scrollpane = (JScrollPane) this.getComponent("scrollpane");	
		panel = (JPanel) this.getComponent("main");
		tp.addListener(this, this);
    }
    
	@Override
	public void dwTableResized() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.resizeTable();
			} 
		});
	}
	
	@Override
	public void dwTextPaneResized() {
		DWTextPane tp = (DWTextPane) getComponent("text1");
		tp.setWidth(SCREEN_WIDTH);
		tp.setListenerEnabled(false);
	}

	private void setupTable() {
        // get the column names from the XML params
        String[] colNames = {Messages.getString("MOWizardPage3.itemText") , Messages.getString("MOWizardPage3.feeText"),	 
        		Messages.getString("MOWizardPage3.amountText")};	
        tableModel = new MoneyOrderTableModel(colNames, 10, AMOUNT_COLUMN,
                                              transaction);
        table.getTable().setModel(tableModel);
        TableColumnModel colModel = table.getTable().getColumnModel();
        colModel.getColumn(AMOUNT_COLUMN).setCellEditor(
                                     new DefaultCellEditor(new MoneyField()));
        colModel.getColumn(ITEM_COLUMN).setCellRenderer(new MoneyOrderCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));
        colModel.getColumn(FEE_COLUMN).setCellRenderer(new MoneyOrderCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));
        colModel.getColumn(AMOUNT_COLUMN).setCellRenderer(new MoneyOrderCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));
        
		table.getTable().getTableHeader().setReorderingAllowed(false);
		table.getTable().getTableHeader().setResizingAllowed(false);
    }
    
    @Override
	public void start(int direction) {
        flowButtons.reset();
        flowButtons.setVisible("expert", false);	
        flowButtons.setEnabled("expert", false);	

        refreshTable();
        KeyMapManager.mapMethod(this, KeyEvent.VK_UP, 0, "upArrowAction");	

        final JButton nextButton = flowButtons.getButton("next");	

        nextButton.requestFocus(); //OK
        
        table.setListenerEnabled(true);
    }

    /**
     * Populate the table with values from the transaction's money orders.
     */
    public void refreshTable() {
        updateTotal();
       // java.util.List orders = transaction.getMoneyOrders();
        List<MoneyOrderInfo> orders = ((MoneyOrderTransaction40)transaction).getMoneyOrderList();
        int rowCount = tableModel.getRowCount();
        for (int i = 0; i < rowCount; i++) {
            tableModel.removeRow(0);
        }
        tableModel.fireTableRowsDeleted(0, rowCount - 1);
        for (int i = 0; i < orders.size(); i++) {
        	MoneyOrderInfo order = orders.get(i);
            String[] row = {"" + (i+1), order.getItemFee().toString(),	
                                           order.getItemAmount().toString()};
            tableModel.addRow(row);
        }
        tableModel.fireTableRowsInserted(0, orders.size() - 1);
        table.initializeTable(scrollpane, panel, SCREEN_WIDTH, 10);
		dwTableResized();
    }

    private void updateTotal() {
        totalField.setText(((MoneyOrderTransaction40) transaction).
                             getTotal(MoneyOrderPrintStatusType.PROPOSED,true).
                             toString());                         
    }

    @Override
	public void finish(int direction) {
        table.getTable().editingStopped(new javax.swing.event.ChangeEvent(this));
        PageNotification.notifyExitListeners(this, direction);
        
        table.setListenerEnabled(false);
    }

    /**
     * Put selection in the last amount field of the table.
     */
    public void upArrowAction() {
        table.getTable().requestFocus();
        table.getTable().setRowSelectionInterval(table.getTable().getRowCount() - 1,
                                      table.getTable().getRowCount() - 1);
        table.getTable().setColumnSelectionInterval(AMOUNT_COLUMN, AMOUNT_COLUMN);
    }

    /**
     * Custom TableModel to allow editing only of specified columns.
     */
    class SemiEditableTableModel extends DefaultTableModel {
		private static final long serialVersionUID = 1L;
		int[] editableCols;

        public SemiEditableTableModel(Object[] colNames, int numRows,
                                      int[] editableCols) {
            super(colNames, numRows);
            this.editableCols = editableCols;
        }

        public SemiEditableTableModel(Object[] colNames, int numRows,
                                      int editableCol) {
            this(colNames, numRows, null);
            int[] editCols = {editableCol};
            editableCols = editCols;
        }

        @Override
		public boolean isCellEditable(int row, int col) {
            for (int i = 0; i < editableCols.length; i++)
                if (editableCols[i] == col)
                    return true;
            return false;
        }
    }

    class MoneyOrderTableModel extends SemiEditableTableModel {
		private static final long serialVersionUID = 1L;
        MoneyOrderTransaction transaction;

        public MoneyOrderTableModel(Object[] colNames, int numRows,
                                    int amountCol, MoneyOrderTransaction tran) {
            super(colNames, numRows, amountCol);
            this.transaction = tran;
        }

        /**
         * When a value is set in the model, update the corresponding money
         *   order in the transaction. If a value of 0 is set, ignore the change
         *   and revert back to the previous value.
         */
        @Override
		public void setValueAt(Object value, int row, int col) {
            BigDecimal newAmount = new BigDecimal((String) value);
            if (newAmount.compareTo(BigDecimal.ZERO) == 0)
                return;
           // if this MO has a fee already, it should get one again
           // MoneyOrderInfo moInfo = transaction.getMoneyOrder(row);
            MoneyOrderInfo moInfo = transaction.getMoneyOrderInfo(row);
            BigDecimal currentFee = moInfo.getItemFee();
            boolean chargeFee = currentFee.compareTo(BigDecimal.ZERO) != 0;
            //BigDecimal newFee = transaction.getFee(newAmount);
            if (setInTransaction(newAmount, chargeFee, row)) {
                super.setValueAt(value, row, col);
                
            }
            // after changing table, refresh and repaint.
            refreshTable();
        }
    }

    /**
     * Custom TableCellRenderer for the money order table. When selecting the
     *   item or fee columns, do not highlight the cell and make the text gray
     *   to indicate that it is not editable. If it is the amount column, make
     *   the cell highlighted to indicate it may be edited.
     */
    class MoneyOrderCellRenderer extends DefaultTableCellRenderer implements DWTableCellRendererInterface {
		private static final long serialVersionUID = 1L;
    	
    	private int margin = 0;

    	private MoneyOrderCellRenderer(int margin) {
    		this.margin = margin;
    	}
    	
        @Override
		public Component getTableCellRendererComponent(JTable table,
                                                       Object value,
                                                       boolean isSelected,
                                                       boolean hasFocus,
                                                       int row,
                                                       int column) {
            Component rend = super.getTableCellRendererComponent(table, value,
                                          isSelected, hasFocus, row, column);
            if (column != ITEM_COLUMN) {
                MoneyField mf = new MoneyField(value.toString());
                mf.setBorder(BorderFactory.createEmptyBorder());
                rend = mf;
            } else {
            	((JLabel) rend).setHorizontalAlignment(SwingConstants.RIGHT);
            }
            if (isSelected && hasFocus) {
                rend.setBackground(Color.white);
                rend.setForeground(Color.black);
                ((JComponent)rend).setBorder(BorderFactory.createLineBorder(
                                                    SystemColor.textHighlight));
            }
            else if (isSelected) {
                    rend.setBackground(SystemColor.textHighlight);
                    rend.setForeground(Color.white);
            }
            else {
                rend.setBackground(Color.white);
                rend.setForeground(Color.black);
            }
            
            JComponent c = (JComponent) rend;
            c.setBorder(BorderFactory.createEmptyBorder(0, margin, 0, margin));
            return rend;
        }

		@Override
		public int getCellMargin() {
			return margin;
		}

		@Override
		public void setCellMargin(int margin) {
			this.margin = margin;
		}
    }
}
