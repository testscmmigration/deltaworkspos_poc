package dw.moneyorder;

import javax.swing.JComponent;
import javax.swing.JTextField;

import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;

/**
 * Page 5 of the Money Order Wizard.
 * This page will be displayed after a successful print of the monety orders.
 *   It displays the total and the user may press OK to return to the main
 *   menu.
 */
public class MOWizardPage5 extends MoneyOrderFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

    private JTextField totalField = null;

	public MOWizardPage5(MoneyOrderTransaction40 tran, String name, String pageCode, PageFlowButtons buttons) {
		this((MoneyOrderTransaction) tran, name, pageCode, buttons);
	}
	
    public MOWizardPage5(MoneyOrderTransaction tran,
                       String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "moneyorder/MOWizardPage5.xml", name, pageCode, buttons);	
        totalField = (JTextField) getComponent("totalField");	

        DWTextPane tp = (DWTextPane) getComponent("text1");
		tp.addListener(this, this);
    }

	@Override
	public void dwTextPaneResized() {
		DWTextPane tp1 = (DWTextPane) getComponent("text1");
		DWTextPane tp2 = (DWTextPane) getComponent("text2");
		DWTextPane tp3 = (DWTextPane) getComponent("text3");
		int width = 350;
		tp1.setWidth(width);
		tp2.setWidth(width);
		tp3.setWidth(width);
		tp1.setListenerEnabled(false);
	}

    @Override
	public void start(int direction) {
        flowButtons.reset();
        flowButtons.setEnabled("back", false);	
        flowButtons.setEnabled("next", false);	
        flowButtons.setVisible("expert", false);	
        flowButtons.setEnabled("expert", false);	
        flowButtons.setText("cancel", Messages.getString("MOWizardPage5.okText"));	 
        flowButtons.setSelected("cancel");	
        totalField.setText(transaction.getTotal(
                                null, true).toString());
        JComponent[] focusOrder = {flowButtons.getButton("cancel")};	
        setFocusOrder(focusOrder);

    }

    // can only exit this page by being complete
    @Override
	public void finish(int direction) {
        PageNotification.notifyExitListeners(this, PageExitListener.COMPLETED);
    }
}
