package dw.moneyorder;

import java.awt.Component;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

import com.moneygram.agentconnect.MoneyOrderPrintStatusType;

import dw.dwgui.CustomTableCellRenderer;
import dw.dwgui.DWTable.DWTableCellRendererInterface;
import dw.dwgui.MoneyField;
import dw.i18n.Messages;

/**
 * @author Renuka.
 * The PrintCanceledTableMO is specific to MoneyOrders and uses the MOI40s.
 */
public class PrintCanceledTableMO extends JTable {
	private static final long serialVersionUID = 1L;

	private MoneyOrderTransaction transaction;
    private static String[] colNames = {Messages.getString("PrintCanceledTableMO.2029"), Messages.getString("PrintCanceledTableMO.2030")};  
    private static final int NOT_PRINTED_ROW = 2;
    private static final int COUNT_COLUMN = 0;
    private static final int TOTAL_COLUMN = 1;

    public PrintCanceledTableMO(MoneyOrderTransaction transaction) {
        super(new DefaultTableModel(colNames, 2));
        this.transaction = transaction;
        setEnabled(false);
        setRowHeight(23);
        TableColumnModel columnModel = getColumnModel();
        TableCellRenderer renderer = new PrintCancelRenderer();
        columnModel.getColumn(COUNT_COLUMN).setCellRenderer(renderer);
        columnModel.getColumn(TOTAL_COLUMN).setCellRenderer(renderer);
        columnModel.getColumn(TOTAL_COLUMN).setHeaderRenderer(
                                    new CustomTableCellRenderer(
                                      CustomTableCellRenderer.MONETARY_HEADER, 10));
    }

    public void setTableValues() {
        DefaultTableModel tableModel = (DefaultTableModel) getModel();
        int rowCount = tableModel.getRowCount();
        for (int i = 0; i < rowCount; i++) {
            tableModel.removeRow(0);
        }
        String[] row1 = {"" + ((MoneyOrderTransaction40)transaction).getMoneyOrderList().size(),	
             transaction.getTotal(null, false).toString()};
        tableModel.addRow(row1);
        String[] row2 = {"" + transaction.getPrintedCount(),	
         transaction.getTotal(MoneyOrderPrintStatusType.CONFIRMED_PRINTED, false).toString()};
        tableModel.addRow(row2);
        String[] row3 = {"" + (((MoneyOrderTransaction40)transaction).getMoneyOrderList().size() -	
                              transaction.getPrintedCount()),
         transaction.getTotal(MoneyOrderPrintStatusType.ATTEMPTED, false).toString()};
        tableModel.addRow(row3);
        dw.utility.Debug.println("PrintCanceledTableMO.setTableValues() getTotal(PRINTED)=" +
            transaction.getTotal(MoneyOrderPrintStatusType.CONFIRMED_PRINTED, false).toString());
    }

    /**
     * Custom table cell renderer that renders the NOT_PRINTED items in bold.
     */
    class PrintCancelRenderer extends DefaultTableCellRenderer implements DWTableCellRendererInterface {
		private static final long serialVersionUID = 1L;

		private int margin;
        @Override
		public Component getTableCellRendererComponent(JTable table,
                                                       Object value,
                                                       boolean isSelected,
                                                       boolean hasFocus,
                                                       int row,
                                                       int column) {
        	
            JTextField rend;
            if (column == TOTAL_COLUMN)
                rend = new MoneyField();
            else
                rend = new JTextField();
            rend.setBorder(BorderFactory.createEmptyBorder());
            rend.setText(value.toString());

            if (row == NOT_PRINTED_ROW) {
                Font f = rend.getFont();
                rend.setFont(new Font(f.getName(), Font.BOLD, f.getSize()));
            }
            return rend;
        }

		@Override
		public int getCellMargin() {
			return margin;
		}

		@Override
		public void setCellMargin(int margin) {
			this.margin = margin;
		}
    }
}
