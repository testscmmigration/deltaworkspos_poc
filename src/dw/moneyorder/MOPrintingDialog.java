package dw.moneyorder;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;

import dw.dialogs.Dialogs;
import dw.dwgui.DWTextField;
import dw.framework.PrintingProgressInterface;
import dw.framework.XmlDialogPanel;
import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.model.adapters.CountryInfo;

/**
 * MOPrintingDialog shows the current status of the money orders being printed.
 *   It calls the MoneyOrderTransaction to intiate printing of the money orders,
 *   and displays the status of the prints with a progress bar.
 */
public class MOPrintingDialog extends XmlDialogPanel
        implements PrintingProgressInterface {

    private JLabel printingLabel = null;
    private JLabel currencyLabel = null;
    private JProgressBar printProgressBar = null;
    private JTextField serialField = null;
    private DWTextField subtotalField = null;
    private JButton cancelButton = null;
    private boolean canceled = false;
    private boolean allowCancel = false;
    private int totalItems = 0;
    private int lastItem = 0;

    public MOPrintingDialog(boolean allowCancel) {
        super("moneyorder/MOPrinting.xml");	
        this.title = Messages.getString("MOPrintingDialog.2019"); 
        this.allowCancel = allowCancel;

        printingLabel = (JLabel) mainPanel.getComponent("printingLabel");	
        printProgressBar = (JProgressBar) mainPanel.getComponent("printProgressBar");	
        serialField = (JTextField) mainPanel.getComponent("serialTextField");	
        currencyLabel = (JLabel) mainPanel.getComponent("currencyLabel");
        subtotalField = (DWTextField)mainPanel.getComponent("subtotalTextField");	
        cancelButton = (JButton) mainPanel.getComponent("cancelButton");	
        cancelButton.addActionListener(new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
                canceled = true;
            }
        });
        
        cancelButton.addKeyListener(new KeyAdapter() {   
            @Override
			public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    e.consume();
                    canceled = true;
                }
            }});
       
        cancelButton.setEnabled(allowCancel);
        cancelButton.setVisible(allowCancel);

    }

    public MOPrintingDialog() {
        this(true);
    }

    public boolean getSuccess() {
        return ! canceled;
    }

    /**
    * Eliminates the printing progress bar.
    */
    @Override
	public void done() {
        if (lastItem < totalItems)
            canceled = true;
        closeDialog(Dialogs.CLOSED_OPTION);
    }

    /** Begins a printing progress bar.
     *  This should not actually display the progress bar until the first
     *  begin() call is made.
     *  @param totalNumberToPrint The total number of items to print.
     */
    @Override
	public void begin(int totalNumberToPrint) {
        canceled = false;
        totalItems = totalNumberToPrint;
        lastItem = 0;
        printProgressBar.setMinimum(0);
        printProgressBar.setMaximum(totalNumberToPrint);
        printProgressBar.setValue(0);
        printingLabel.setText(Messages.getString("MOPrintingDialog.2020")); 
        currencyLabel.setText(CountryInfo.getAgentCountry().getBaseReceiveCurrency());
        final JButton cb = cancelButton;
        // can't cancel the first one, so don't frustrate them with a button
        if (totalNumberToPrint == 1) {
            cancelButton.setVisible(false);
        }
        else if (allowCancel) {
            cancelButton.setEnabled(false);
            Thread t = new Thread(new Runnable() {
                    @Override
					public void run() {
                        try {
                            Thread.sleep(3000);
                            cb.setEnabled(allowCancel);
                            cb.requestFocus();
                        }
                        catch (InterruptedException e) {}
                    }});
            t.start();
        }
    }

    /** Updates the printing progress bar. Sets visible true on the progress dialog
     *  if not already visible.
     *  @param itemOrdinal The number of the current item (out of the total number to print).
     *  @param serialNumber The serial number of the currently printing item.
     *  @param totalSoFar The amount printed through the current item, including fees.
     *  @return Whether or not it is OK to proceed with the item.  False if the user has cancelled the transaction.
     */
    @Override
	public boolean item( int itemOrdinal, String serialNumber,
                                                BigDecimal totalSoFar) {
        lastItem = itemOrdinal;
        serialField.setText(serialNumber);
        subtotalField.setText(FormatSymbols.convertDecimal(totalSoFar.toString()));
        printingLabel.setText(Messages.getString("MOPrintingDialog.2021") + itemOrdinal + Messages.getString("MOPrintingDialog.2022") + totalItems +  
                                                         Messages.getString("MOPrintingDialog.2023")); 
        printProgressBar.setValue(itemOrdinal - 1); //items will come in 1-based
        return ! canceled;
    }
}
