package dw.moneyorder;

import java.awt.Component;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import com.moneygram.agentconnect.MoneyOrderPrintStatusType;

import dw.dwgui.CustomTableCellRenderer;
import dw.dwgui.DWTable;
import dw.dwgui.DWTable.DWTableCellRendererInterface;
import dw.dwgui.DWTable.DWTableListener;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.dwgui.MoneyField;
import dw.framework.ClientTransaction;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;

/**
 * Page 6 of the Money Order Wizard.
 * This page is an alternate page to page 5, and will be displayed if the print
 *   operation was canceled or failed for some reason. This page displays a
 *   table indicating to the user how many money orders were requested, how
 *   many printed, and how many did not print.
 */
public class MOWizardPage6 extends MoneyOrderFlowPage implements DWTextPaneListener, DWTableListener {
	private static final long serialVersionUID = 1L;

    private static String[] colNames = {Messages.getString("PrintCanceledTableMO.2029"), Messages.getString("PrintCanceledTableMO.2030")};  
    private final int NOT_PRINTED_ROW = 2;
    private final int COUNT_COLUMN = 0;
    private final int TOTAL_COLUMN = 1;
    
    private DWTable table;
    private JScrollPane scrollpane;
    private JPanel panel;
    
    class PrintCancelRenderer extends DefaultTableCellRenderer implements DWTableCellRendererInterface {
		private static final long serialVersionUID = 1L;

		private int margin;
        @Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            JTextField rend;
            if (column == TOTAL_COLUMN) {
                rend = new MoneyField();
            } else {
                rend = new JTextField();
            }
            rend.setText(value.toString());

            if (row == NOT_PRINTED_ROW) {
                Font f = rend.getFont();
                rend.setFont(new Font(f.getName(), Font.BOLD, f.getSize()));
            }
            
            rend.setBorder(BorderFactory.createEmptyBorder(0, margin, 0, margin));
            return rend;
        }

		@Override
		public int getCellMargin() {
			return margin;
		}

		@Override
		public void setCellMargin(int margin) {
			this.margin = margin;
		}
    }

	public MOWizardPage6(MoneyOrderTransaction40 tran, String name, String pageCode, PageFlowButtons buttons) {
		this((MoneyOrderTransaction) tran, name, pageCode, buttons);
		scrollpane = (JScrollPane) this.getComponent("scrollpane1");	
		panel = (JPanel) this.getComponent("panel1");
	}
	
    public MOWizardPage6(MoneyOrderTransaction tran,
                       String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "moneyorder/MOWizardPage6.xml", name, pageCode, buttons);	
		
		table = (DWTable) this.getComponent("printTableScrollpane");
		table.addListener(this, this);
		table.setListenerEnabled(false);

		DWTextPane tp = (DWTextPane) getComponent("text1");
		tp.addListener(this, this);
		
		setupTable();
    }

	@Override
	public void dwTextPaneResized() {
		DWTextPane tp1 = (DWTextPane) getComponent("text1");
		DWTextPane tp2 = (DWTextPane) getComponent("text2");
		JPanel panel1 = (JPanel) getComponent("panel1");
		int width = panel1.getPreferredSize().width;
		tp1.setWidth(width);
		tp2.setWidth(width);
		tp1.setListenerEnabled(false);
	}
	
	@Override
	public void dwTableResized() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.resizeTable();
			} 
		});
	}
	
	private class PrintCancelTableModel extends DefaultTableModel {
		private static final long serialVersionUID = 1L;

		PrintCancelTableModel(Object[] colNames, int numRows) {
			super(colNames, numRows);
		}
	}
    
    private void setupTable() {

        // set up this page for MO vs VP
        
        table.getTable().setModel(new PrintCancelTableModel(colNames, 2));
        table.getTable().setEnabled(false);
        TableColumnModel columnModel = table.getTable().getColumnModel();
        columnModel.getColumn(COUNT_COLUMN).setCellRenderer(new PrintCancelRenderer());
        columnModel.getColumn(TOTAL_COLUMN).setCellRenderer(new PrintCancelRenderer());
        columnModel.getColumn(TOTAL_COLUMN).setHeaderRenderer(new CustomTableCellRenderer(CustomTableCellRenderer.MONETARY_HEADER, 0));
        
        if (transaction.isVendorPayment()) {
            removeComponent("moInstructionsPanel");	
        } else {
            removeComponent("vpInstructionsPanel");        	
        }
		
        table.getTable().getTableHeader().setReorderingAllowed(false);
		table.getTable().getTableHeader().setResizingAllowed(false);
    }

    @Override
	public void start(int direction) {
        flowButtons.reset();
        flowButtons.setEnabled("back", false);	
        flowButtons.setEnabled("next", false);	
        flowButtons.setVisible("expert", false);	
        flowButtons.setText("cancel", Messages.getString("MOWizardPage6.okText"));	 
        flowButtons.setSelected("cancel");	

        // set the current values in the table
        setTableValues();
        // transaction at this point should be canceled 
        transaction.setStatus(ClientTransaction.CANCELED);

        JComponent[] focusOrder = {flowButtons.getButton("cancel")};	
        setFocusOrder(focusOrder);
		table.setListenerEnabled(true);
    }
    
    private void setTableValues() {
        DefaultTableModel tableModel = (DefaultTableModel) table.getTable().getModel();
        int rowCount = tableModel.getRowCount();
        for (int i = 0; i < rowCount; i++) {
            tableModel.removeRow(0);
        }
        tableModel.fireTableRowsDeleted(0, rowCount - 1);
        String[] row1 = {"" + ((MoneyOrderTransaction40)transaction).getMoneyOrderList().size(),	
             transaction.getTotal(null, false).toString()};
        tableModel.addRow(row1);
        String[] row2 = {"" + transaction.getPrintedCount(),	
         transaction.getTotal(MoneyOrderPrintStatusType.CONFIRMED_PRINTED, false).toString()}; 
        tableModel.addRow(row2);
        String[] row3 = {"" + (((MoneyOrderTransaction40)transaction).getMoneyOrderList().size() -	
                              transaction.getPrintedCount()),
         transaction.getTotal(MoneyOrderPrintStatusType.ATTEMPTED, false).toString()};
        tableModel.addRow(row3);
        tableModel.fireTableRowsInserted(0, 2);
        dw.utility.Debug.println("PrintCanceledTableMO.setTableValues() getTotal(PRINTED)=" +
            transaction.getTotal(MoneyOrderPrintStatusType.CONFIRMED_PRINTED, false).toString());

        table.initializeTable(scrollpane, panel, 350, 10);
    }

    @Override
	public void finish(int direction) {
    	table.setListenerEnabled(false);
        PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
    }
}
