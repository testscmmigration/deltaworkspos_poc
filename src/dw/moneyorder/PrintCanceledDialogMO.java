package dw.moneyorder;

import dw.dialogs.Dialogs;
import dw.framework.XmlDialogPanel;
import dw.i18n.Messages;

public class PrintCanceledDialogMO extends XmlDialogPanel {

    public PrintCanceledDialogMO(MoneyOrderTransaction transaction, 
            boolean vendorPayment) {

        super("moneyorder/DialogPrintingCanceled.xml");	
        this.title = Messages.getString("PrintCanceledDialogMO.2026"); 

        if (vendorPayment) {
            mainPanel.removeComponent("moInstructionsPanel");	
        }
        else {
            mainPanel.removeComponent("vpInstructionsPanel");	
        }
        PrintCanceledTableMO table = new PrintCanceledTableMO(transaction);
        table.setTableValues();
        mainPanel.addComponent(table, "printTable", "printTableScrollpane");	 

        mainPanel.addActionListener("okButton", this, "okButtonAction");	 
    }

    public void okButtonAction() {
        closeDialog(Dialogs.OK_OPTION);
    }
}

