package dw.moneyorder;

import java.awt.event.KeyEvent;

import dw.framework.ClientTransaction;
import dw.framework.KeyMapManager;
import dw.framework.PageNameInterface;
import dw.framework.TransactionInterface;

public abstract class MoneyOrderTransactionInterface
                                                extends TransactionInterface {
	private static final long serialVersionUID = 1L;
	
	protected MoneyOrderTransaction transaction;

    public MoneyOrderTransactionInterface(String fileName,
                                 MoneyOrderTransaction transaction,
                                 PageNameInterface naming,
                                 boolean sideBar) {
        super(fileName, naming, sideBar);
        this.transaction = transaction;
    }

    @Override
	public ClientTransaction getTransaction() {
        return transaction;
    }

    @Override
	public void start() {
        setF2KeyMapping();
        super.start();
    }
    
    protected void setF2KeyMapping(){
        KeyMapManager.mapMethod(this, KeyEvent.VK_F2, 0, "f2SAR");
    }

}
