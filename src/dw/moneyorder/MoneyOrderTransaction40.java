/*
 * Created on Nov 10, 2003
 */
package dw.moneyorder;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.JComponent;

import com.moneygram.agentconnect.ComplianceTransactionRequest;
import com.moneygram.agentconnect.ComplianceTransactionRequestType;
import com.moneygram.agentconnect.ComplianceTransactionResponse;
import com.moneygram.agentconnect.CustomerComplianceInfo;
import com.moneygram.agentconnect.CustomerComplianceTypeCodeType;
import com.moneygram.agentconnect.MoneyOrderInfo;
import com.moneygram.agentconnect.MoneyOrderPrintStatusType;
import com.moneygram.agentconnect.MoneyOrderVoidReasonCodeType;

import dw.comm.MessageFacade;
import dw.comm.MessageFacadeError;
import dw.comm.MessageFacadeParam;
import dw.comm.MessageLogic;
import dw.dialogs.Dialogs;
import dw.dispenser.DispenserFactory;
import dw.dispenser.DispenserInterface;
import dw.dispenser.MoneyOrderDisplayFields;
import dw.framework.CommCompleteInterface;
import dw.framework.PrintingProgressInterface;
import dw.i18n.Messages;
import dw.io.CCILog;
import dw.io.IsiReporting;
import dw.io.report.ReportLog;
import dw.main.MainPanelDelegate;
import dw.printing.ReceiptReport;
import dw.printing.TEPrinter;
import dw.profile.ClientInfo;
import dw.profile.ProfileItem;
import dw.profile.UnitProfile;
import dw.profile.UnitProfileInterface;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.ErrorManager;
import dw.utility.ExtraDebug;
import dw.utility.IdleBackoutTimer;
import dw.utility.TimeUtility;

/**
 * @author Renuka Easwar
 *
 * Subclass of the existing MoneyOrderTransaction to keep track of the 
 * aggregate limits and all the compliance information.This class also encapsulates 
 * the new real time messages between the POS and the RTS. This class uses the new
 * MoneyOrderInfo40s.  
 * 
 */
public class MoneyOrderTransaction40 extends MoneyOrderTransaction {
	private static final long serialVersionUID = 1L;
	private static BigInteger DELTAGRAM = BigInteger.valueOf(4);
	
	private static final BigDecimal ONE = new BigDecimal(1.00);

	protected ComplianceTransactionResponse responseInfo;
    public String sourceNumber;

    public MoneyOrderDisplayFields modf;
	
    public MoneyOrderTransaction40(String tranName, String tranReceiptPrefix, int documentSequence,boolean vendorPayment) {
        super(tranName, tranReceiptPrefix, documentSequence,vendorPayment);
    }
    
    public MoneyOrderTransaction40(String tranName, String tranReceiptPrefix, int documentSequence) {
            this(tranName, tranReceiptPrefix, documentSequence,false);
        }

    /**
      * check if photo ID is required for this transaction.
      */
    @Override
	public boolean isPhotoIdNeeded() {
        if ((isAggCheckEnable() == true)
            && (getTotal(MoneyOrderPrintStatusType.PROPOSED, false).compareTo(getAggLimit()) >= 0))
            return true;
        else
            return false;
    }

    /**   
      * Returns true if the Aggregation check is enabled 
      */
    @Override
	public boolean isAggCheckEnable() {
        return isProductProfileOptionTrue("AGG_ENABLE");	
    }

    /**
      * Get a list of all the valid types for Government Issued IDs. Make the
      * first item in the list the correct default type. 
      */
    @Override
	public String[] getPhotoIDNames() {
        return DWValues.getPhotoIdNames();
    }

    /**
      * Returns the threshold for aggregation.  
      */
    public BigDecimal getAggLimit() {
        ProfileItem item = getProductProfileOption("FRAUD_LIMIT_PHOTO_ID");	
        if (item == null) {
            return new BigDecimal("99999.99");	
        } else {
            return item.bigDecimalValue();
        }
    }

    /**
     * Sends the request to the RTS for a OFAC check. All the Money Orders in the
     * Vector are in a proposed print status. Set the CTR request type to 1,lock the CCI since 
     * it is the first write and write to the CCI Log. 
     * If the RTS returns that no further information is required, set the print status for each MoneyOrder
     * in the Vector to 1. Set the CTR request type to 3 and write to the CCI log and 
     * return true to the calling page.
     * Else if the RTS returns that more compliance information is required,
     * show the requested compliance dialog. If any of the name and photoId information is changed,
     * set the CTR request type to 2,set the transaction status to NEEDS_RECOMMIT and return false to the calling page.
     * Else if the RTS indicates that transaction failed for OFAC reasons,
     * set the print status for each MoneyOrder in the Vector to not printed.
     * Write to the CCI Log, set the transaction status to failed, 
     * show the OFAC error dialog, return false to the calling page.
     */
    @Override
	public void getAggregationCheck(
        final ComplianceTransactionRequest requestInfo,
        final CommCompleteInterface callingPage) {
        requestInfo.getMoneyOrder().clear();
        requestInfo.getMoneyOrder().addAll(moItemData);
        //set the request type to 2 before writing to the log.
        requestInfo.setRequestType(ComplianceTransactionRequestType.COMPLETED_WITH_CHANGES);
        this.setComplianceTransactionRequestInfo(requestInfo);
        writeToCCI(this.complianceTransactionRequestInfo);
        //reset request type to 1 to request host for OFAC check on name.
        requestInfo.setRequestType(ComplianceTransactionRequestType.PROPOSED);
        final MessageFacadeParam parameters =
            new MessageFacadeParam(
                MessageFacadeParam.REAL_TIME,
                MessageFacadeParam.MO_OFAC_CHECK_FAILED);
        final MessageLogic logic = new MessageLogic() {
            @Override
			public Object run() {
                try {
                    Debug.println("Sending request for aggreagate/OFAC check");	
                    responseInfo =
                        MessageFacade.getInstance().checkOFAC(parameters, requestInfo);
                    if (responseInfo != null) {
                        MessageFacade.hideDialog(0);
                        // For 4.0 reduced scope, disable unreachable code
                        // FUTURE Put this back if 4.0 design is put back in scope
                        //if (responseInfo.isComplianceInfoRequired()) {
//                        if (false) {
//                            ComplianceDialog complianceDialog =
//                                new ComplianceDialog(responseInfo.getCci());
//                            Dialogs.showPanel(complianceDialog);
//                            if (complianceDialog.isSaved()) {
//                                requestInfo.setCci(complianceDialog.getModifiedCCI());
//                            }
//                            requestInfo.setRequestType(2);
//                            setStatus(NEEDS_RECOMMIT);
//                            return Boolean.FALSE;
//                        } else {
                            requestInfo.setRequestType(ComplianceTransactionRequestType.COMPLETED_WITHOUT_CHANGES);
                            //iterate through the vector of Money Orders and for each item in Vector 
                            //update print status for each money order to not attempted.
                            for (Iterator<MoneyOrderInfo> i = getMoneyOrderList().iterator(); i.hasNext();) {
                            	MoneyOrderInfo mo = i.next();
                                mo.setPrintStatus(MoneyOrderPrintStatusType.NOT_ATTEMPTED);
                            }
                            setComplianceTransactionRequestInfo(requestInfo);
                            writeToCCI(complianceTransactionRequestInfo);
//                        }
                    } else {

                        return Boolean.FALSE;
                    }

                } catch (MessageFacadeError mfe) {
                    if (mfe.hasErrorCode(MessageFacadeParam.MO_OFAC_CHECK_FAILED)) {
                        requestInfo.setRequestType(ComplianceTransactionRequestType.COMPLETED_WITH_CHANGES); 
                        sourceNumber = mfe.getMessage();
                        Dialogs.showError("dialogs/DialogOfacError.xml", sourceNumber);	

                        for (Iterator<MoneyOrderInfo> i = getMoneyOrderList().iterator(); i.hasNext();) {
                        	MoneyOrderInfo mo = i.next();
                            mo.setPrintStatus(MoneyOrderPrintStatusType.CONFIRMED_NOT_PRINTED);
                        }
                        setComplianceTransactionRequestInfo(requestInfo);
                        writeToCCI(requestInfo);
                        setStatus(FAILED);
                    }
                    return Boolean.FALSE;
                }
                return Boolean.TRUE;
            }

        };
        MessageFacade.run(logic, callingPage, 0, Boolean.FALSE);
    }

    /**
     * check receipt printer status,and return true or false to the calling page.
     */
    public boolean checkPrinterReady(JComponent parent){
        if (isReceiptReceived()) {
            if (TEPrinter.getPrinter().isReady(parent))
                return true;  
        }
        return false;
    }
 
    /**
      *  Print a receipt for this Ofac error. 
      */

    public void printOfacErrorReceipt(
        boolean isCustomerReceipt,
        String sourceNumber) {
        
        String receiptFormat = getReceiptFormat();
               
         try {
            ReceiptReport.printMoOfacErrorReceipt(
                receiptFormat,
                isCustomerReceipt,
                sourceNumber);
        } catch (Exception e) {
            ErrorManager.handle(e, "printReceiptError", Messages.getString("ErrorMessages.printReceiptError"), ErrorManager.ERROR, true);	
        }
    }

    /**
     * Get the MoneyOrderInfo40 at a given index. Create a new MOI40 if the index is past
     * the range of MoneyOrders, create a new one and set all the fields. 
     */

    @Override
	public MoneyOrderInfo getMoneyOrderInfo(int index) {
    	MoneyOrderInfo mo = null;
        ClientInfo co = new ClientInfo();
        if (index >= moItemData.size()) {
            mo = new MoneyOrderInfo();
            mo.setPrintStatus(MoneyOrderPrintStatusType.PROPOSED);
            mo.setMoAccountNumber(co.getAccountNumber());
            String value = UnitProfile.getInstance().get("DEVICE_ID", "");
            mo.setDeviceID(((value != null) && (value.length() > 8)) ? value.substring(0, 8) : value);
//            mo.setTransType("IT");  
            mo.setItemAmount( BigDecimal.ZERO);
            mo.setDateTimePrinted(TimeUtility.toXmlCalendar(TimeUtility.currentTime()));
            mo.setItemFee(BigDecimal.ZERO);  
            mo.setPeriodNumber(new BigInteger(String.valueOf(UnitProfile.getInstance().getCurrentPeriod())));
            mo.setDocumentSequenceNbr(new BigInteger(String.valueOf(this.documentNumber)));
            mo.setDocumentType(BigInteger.valueOf(documentType));
            //does not make sense here -commenting out.
//            if(mo.getDocumentType().equals("4"))
//                mo.setTaxID(this.getReferenceNumber());
            //if an associate discount card number was entered save it into the taxId field.
            if (shouldApplyAssociateDiscount())
                mo.setTaxID(this.getDiscountCardNumber());
            mo.setEmployeeID(BigInteger.valueOf(userID));
            if (MainPanelDelegate.getIsiTransactionInProgress())
                mo.setRemoteIssuanceFlag(true);
            else
                mo.setRemoteIssuanceFlag(false);           
            mo.setSaleIssuanceTag(new BigInteger(currentSaleIssuanceTag));
            
            mo.setDiscountPercentage(mo.getDiscountPercentage());
            mo.setDiscountAmount(mo.getDiscountAmount());
            mo.setAccountingStartDay(new BigInteger(co.getAccountingStartDay()));
        } else
            mo = moItemData.get(index);
        return (mo);
    }
    
    /** 
     * Return the MODF at a given index from the modfItemdataVector.
     */
     
    public MoneyOrderDisplayFields getMoneyOrderDisplayFields(int index){
        ClientInfo co = new ClientInfo();
        if(index >= modfItemData.size()){
            modf = new MoneyOrderDisplayFields();
            modf.setDocumentName(documentName);
            modf.setLimitIndicator((tranLimitOverridden) ? "Y" : "N");  
            modf.setAccountNumber(co.getAccountNumber());
            modf.setUnitNumber(co.getUnitNumber());
            if (isPrintPurchaserAvailable())
                 modf.setPurchaserName(UnitProfile.getInstance().getGenericDocumentStoreName());
            else
                 modf.setPurchaserName("");    
        }
        else
            modf = modfItemData.get(index);

        return modf;
    }

    public void addNewDeltaGramToMODF(int index, MoneyOrderDisplayFields modf){
           modfItemData.add(index,modf); //add a modf to the modfItemData vector for every MO added to the moItemData vector.        }
    }
    
    public void addNewDeltaGramToMO(int index, MoneyOrderInfo mo){
        moItemData.add(index,mo); //add the mo to the moItemData vector 
    }
    

    /**
     * Override the setMoneyOrder in MoneyOrderTransaction to use the 
     * MoneyOrderInfo40 for all MoneyOrder Transactions.
     */

    @Override
	public int setMoneyOrder(int index, BigDecimal amount, boolean chargeFee, String payeeName) {
        int result = 0;
        lastEntrySplit = false;
        boolean newMoneyOrder = false;
        MoneyOrderInfo mo;
        BigDecimal itemAmount;
        
        // reset the idle timer here...to make sure expert int don't timeout
        IdleBackoutTimer.checkIn();
        mo = getMoneyOrderInfo(index);
        modf = getMoneyOrderDisplayFields(index);

        itemAmount = mo.getItemAmount();
        //If we are setting a new money order, temporarily add it to the list
        if (index >= moItemData.size()) {
            moItemData.add(index, mo);
            if (!mo.getDocumentType().equals(DELTAGRAM))
                modfItemData.add(index, modf); //add a modf to the modfItemData vector for every MO added to the moItemData vector. 
            newMoneyOrder = true;
        }
        if (amount.compareTo(ZERO) == 0) {
            total = total.subtract(itemAmount);
            moItemData.remove(mo);
            modfItemData.remove(modf);
        } else {
        	BigDecimal maxItemAmount = getMaxItemAmount();
        	BigDecimal div = amount.divide(maxItemAmount,2,RoundingMode.HALF_UP);
            // count should be 0 if it is exactly the same as maxItemAmount
            int count = div.equals(ONE) ? 0 : div.intValue();
            // make sure we will be able to add the extra ones to the transaction
            result = checkLimits(count, amount, itemAmount);
            if (result == MAX_ITEM_AMOUNT_EXCEEDED) {
                // split off the max item amount and remainder
                if (newMoneyOrder) {
                    if ((moItemData.size() - 1) + div.doubleValue() > maxFormsPerSale) {
                        moItemData.remove(mo);
                        modfItemData.remove(modf);
                        return MAX_ITEM_AND_FORMS_EXCEEDED;
                    }
                } else if ((moItemData.size() - 1) + div.doubleValue() > maxFormsPerSale)
                    return MAX_ITEM_AND_FORMS_EXCEEDED;
                BigDecimal remainder = amount.subtract(maxItemAmount);
                // we should not issue an extra mo for less than 1.00, so round
                BigDecimal aBuck = new BigDecimal("1.00");	
                if (remainder.compareTo(aBuck) < 0)
                    remainder = aBuck;
                amount = amount.subtract(remainder);
                setMoneyOrder(index, amount, chargeFee, payeeName);
                // recurse with the remainder...add it to the end of the tran
                setMoneyOrder(moItemData.size(), remainder, chargeFee, payeeName);
                lastEntrySplit = true;
            }
            //check for MaxFormsPerSaleExceeded, move to summary screen after the dialog message box is cleared.
            else if (result == MAX_FORMS_PER_SALE_EXCEEDED) {
                total = total.subtract(itemAmount);
                total = total.add(amount);
                mo.setItemAmount(amount);
                applyFee(mo, chargeFee);
                modf.setPayeeName(""+mo.getVendorNumber());
                if (isVendorPayment()) {
                    mo.setVendorNumber(new BigInteger(vendorMap.get(payeeName)));
                }
                if (newMoneyOrder)
                    if ((moItemData.size() - 1) + div.doubleValue() > maxFormsPerSale){
                        moItemData.remove(mo);
                        modfItemData.remove(modf);
                    }
                return result;
            } else if (result != OK) {
                // remove the temporary money order if not a successful add
                if (newMoneyOrder){
                    moItemData.remove(mo);
                    modfItemData.remove(modf);
                }
                return result;
            } else {
                total = total.subtract(itemAmount);
                total = total.add(amount);
                mo.setItemAmount(amount);
                applyFee(mo, chargeFee);
                modf.setPayeeName(payeeName);
                if (isVendorPayment()) {
                    mo.setVendorNumber(new BigInteger(vendorMap.get(payeeName)));
                }
            }
        }

        recalculateFees();

        if (maxFormsPerSale > 0 && moItemData.size() >= maxFormsPerSale) {
            return MAX_FORMS_PER_SALE_EXCEEDED;
        }
        return OK;

    }
                
    /**
     * Override the checkLimits in MoneyOrderTransaction to use the Vector of
     * MoneyOrderInfo40s.
     */
    @Override
	protected int checkLimits(int count, BigDecimal newAmount, BigDecimal oldAmount) {
        // acts differently for multiple adds...adjust for 1 already added
    	if (count > 0)
            count--;

        BigDecimal newTotal = total.add(newAmount).subtract(oldAmount);

        dailyTotal = UnitProfile.getInstance().getDailyTotal();
        //Debug.println("**** mot40.checkLimits() dailyTotal = " + dailyTotal.toString());
        BigDecimal dailyTotalNew = dailyTotal.add(newTotal);

        if (dailyLimit.compareTo(ZERO) > 0 && dailyTotalNew.compareTo(dailyLimit) > 0)
            return DAILY_LIMIT_EXCEEDED;
        BigDecimal tranLimit = getMaxTransactionAmount();
        if (documentSequence != 4 && tranLimit.compareTo(ZERO) > 0
            && newTotal.compareTo(tranLimit) > 0
            && !tranLimitOverridden)
            return TRANSACTION_AMOUNT_EXCEEDED;

        BigDecimal batchTotal = UnitProfile.getInstance().getBatchTotal(documentType);
        batchTotal = batchTotal.add(newTotal);
//        if (maxBatchAmount.compareTo(ZERO) > 0 && batchTotal.compareTo(maxBatchAmount) > 0)
        if (documentType < 4 && maxBatchAmount.compareTo(ZERO) > 0 && batchTotal.compareTo(maxBatchAmount) > 0)
            return MAX_BATCH_AMOUNT_EXCEEDED;

        int batchCount = UnitProfile.getInstance().getBatchCount(documentType);
        batchCount = batchCount + moItemData.size() + count;
//        if (maxBatchCount > 0 && batchCount > maxBatchCount)
        if (documentType < 4 && maxBatchCount > 0 && batchCount > maxBatchCount)
            return MAX_BATCH_COUNT_EXCEEDED;

        if (moItemData.size() + count > getRemainingItems())
            return REMAINING_ITEMS_EXCEEDED;

    	BigDecimal maxItemAmount = getMaxItemAmount();
        if (maxItemAmount.compareTo(ZERO) > 0 && newAmount.compareTo(maxItemAmount) > 0)
            return MAX_ITEM_AMOUNT_EXCEEDED;

        // 3/1/01 PatO, changed check to ">=" from just ">".	 
        if (maxFormsPerSale > 0 && moItemData.size() + count >= maxFormsPerSale)
            return MAX_FORMS_PER_SALE_EXCEEDED;

        return OK;
    }

    /**
     * Overriding the applyFee method in MOT. The fee being charged will be 
     * updated in MoneyOrderInfo40. 
     */
    private void applyFee(MoneyOrderInfo mo, boolean chargeFee) {
        mo.setItemFee(ZERO);
        mo.setDiscountAmount(ZERO);
        mo.setDiscountPercentage(ZERO);
        if (!chargeFee)
            return;
        mo.setItemFee(computeFee(mo.getItemAmount()));
    }

    /**
     * Returns the Vector of money orders. List contains MoneyOrderInfo40s.
     */
    @Override
	public List<MoneyOrderInfo> getMoneyOrderList() {
        return moItemData;
    }

    @Override
	public int getMoneyOrderCount() {
        return moItemData.size();
    }
    
    /**
     * Delete a money order from this transaction. Ignore any indexes that
     * are beyond the end of the list. Overriding from MOTranascation
     */
    @Override
	public void deleteMoneyOrder(int index) {
    	MoneyOrderInfo mo;
        if (index < moItemData.size()) {
            mo = moItemData.get(index);
            total = total.subtract(mo.getItemAmount());
            moItemData.remove(index);
            modfItemData.remove(index);
        }
    }

    /**
      * Get the total of all the money orders that match the specified criteria.
      * Include the fees in the total if specified
      */
    @Override
	public BigDecimal getTotal(MoneyOrderPrintStatusType printStatus, boolean includeFees) {
        BigDecimal total = BigDecimal.ZERO;
        for (Iterator<MoneyOrderInfo> i = getFilteredMoneyOrders(printStatus).iterator(); i.hasNext();) {
        	MoneyOrderInfo mo = i.next();
            total = total.add(mo.getItemAmount());
            if (includeFees)
                total = total.add(mo.getItemFee());
        }
        return total;
    }

    public List<MoneyOrderInfo> getFilteredMoneyOrders(MoneyOrderPrintStatusType filter) {
    	if(filter == null || MoneyOrderPrintStatusType.PROPOSED.equals(filter))
    	{
    		 return moItemData;
    	}
        Vector<MoneyOrderInfo> filteredList = new Vector<MoneyOrderInfo>();
        for (int i = 0; i < moItemData.size(); i++) {
        	MoneyOrderInfo mo = moItemData.get(i);
			if ((MoneyOrderPrintStatusType.CONFIRMED_PRINTED.equals(filter) && (MoneyOrderPrintStatusType.CONFIRMED_PRINTED
					.equals(mo.getPrintStatus()) || MoneyOrderPrintStatusType.ATTEMPTED.equals(mo.getPrintStatus())))
					|| (MoneyOrderPrintStatusType.CONFIRMED_NOT_PRINTED.equals(filter) && (MoneyOrderPrintStatusType.NOT_ATTEMPTED
							.equals(mo.getPrintStatus()) || MoneyOrderPrintStatusType.CONFIRMED_NOT_PRINTED.equals(mo
							.getPrintStatus())))) {
				filteredList.add(mo);
			}
        }
        return filteredList;
    }

    /** overriding the recalculateFees in MOT to use the MoneyOrderInfo40
      */
    @Override
	public void recalculateFees() {
        if (shouldApplyAssociateDiscount()) {

            BigDecimal runningFeeTotal = BigDecimal.ZERO;
            BigDecimal previousRunningFeeTotalDiscounted = BigDecimal.ZERO;

            for (Iterator<MoneyOrderInfo> i = getMoneyOrderList().iterator(); i.hasNext();) {
            	MoneyOrderInfo mo = i.next();

                // get the non-discounted fee and discount percentage
                if(!mo.getItemFee().equals(ZERO)){
                	BigDecimal originalFee = computeFee(mo.getItemAmount());
                    BigDecimal discountPercentage = getDiscountOptionPercentage();
    
                    // runningFeeTotal is a running sum of the non-discounted
                    // fees as it iterates through the MoneyOrder list.
                    runningFeeTotal = runningFeeTotal.add(originalFee);
    
                    // compute the discount on the runningFeeTotal.
                    BigDecimal runningFeeTotalDiscounted =
                        computeDiscountedFee(runningFeeTotal, discountPercentage);
    
                    // the discount fee is the difference between the runningFeeTotalDiscounted 
                    // and the previousRunningFeeTotalDiscounted.
                    BigDecimal discountedFee =
                        runningFeeTotalDiscounted.subtract(previousRunningFeeTotalDiscounted);
                    // after computing the discout fee, save the runFeeTotalDiscounted
                    // to previousRunningFeeTotalDiscounted.
                    previousRunningFeeTotalDiscounted = runningFeeTotalDiscounted;
    
                    mo.setItemFee(discountedFee);
                    mo.setDiscountAmount(originalFee.subtract(discountedFee));
                    mo.setDiscountPercentage(discountPercentage);
                }
            }
        }
    }
    /**
      * Get the number of money orders that have been printed.
      * Overrirding method in MOT
      */
    @Override
	public int getPrintedCount() {
        return getFilteredMoneyOrders(MoneyOrderPrintStatusType.CONFIRMED_PRINTED).size();
    }
    
    @Override
	public synchronized void commit(final PrintingProgressInterface progressBar){
        commit(progressBar, true);
        //Debug.println("MOT.commit() completed, moneyOrderList = " + moneyOrderList);  
    }
    /**
     * Go through the list of MoneyOrders,set the printStatus to Not Attempted,get the serial numbers
     * and write to the CCILog.Query the status of the dispenser.
     * For each item, set the print status to limbo,fill in other MoneyOrderInfo40 fields
     * and begin printing of the items.
     * Update the print status as each item is printed and write to the CCILog and ReportLog.
     * If everything goes fine,mark the transaction as COMPLETE.
     */
    @Override
	public synchronized void commit(
        final PrintingProgressInterface progressBar,
        boolean isNotDeltaGram) {
        // ClientInfo co = new ClientInfo();
        //Get the dispenser and check to make sure it's on-line.
        DispenserInterface aDispenser = DispenserFactory.getDispenser(selectedDispenser);
        toggleSaleIssuanceTag();

        /* At this point everything is good with the money orders.
         * Before printing all the moneyorder Items,update all MoneyOrderInfos 
         * in moItemData Vector with the amounts and serial numbers. 
         * Set the Moneyorders to not Attempted print Status
         * Write to CCI log. 
         */
        boolean increment = true;
        for (int i = 0; i < moItemData.size(); i++) {
        	MoneyOrderInfo mo = getMoneyOrderInfo(i);
            if (mo.getPrintStatus() == MoneyOrderPrintStatusType.PROPOSED)
                mo.setPrintStatus(MoneyOrderPrintStatusType.NOT_ATTEMPTED);
            long serialNumber = 0;
            try {
            	serialNumber = Long.parseLong(getNextSerialNumber());
            } catch (Exception e) {
            }
            long nextSerialNumber = serialNumber + i;
            if (increment)
                mo.setSerialNumber(BigInteger.valueOf(serialNumber));
            else
                mo.setSerialNumber(BigInteger.valueOf(nextSerialNumber));
            increment = false;
        }
        
        // set CCI information.
        
        if (this.cci != null) {
        	complianceTransactionRequestInfo.setCci(this.cci);
        } else {
        	CustomerComplianceInfo cci = new CustomerComplianceInfo();
        	cci.setTypeCode(CustomerComplianceTypeCodeType.EMPTY_OR_NONE);
    		cci.setLocalDateTime(TimeUtility.toXmlCalendar(TimeUtility.currentTime()));
        	complianceTransactionRequestInfo.setCci(cci);
        }

        //set the CTR request type to 3 as we are done with all the checks at this point.
        this.complianceTransactionRequestInfo.getMoneyOrder().clear();
        this.complianceTransactionRequestInfo.getMoneyOrder().addAll(moItemData);
        this.complianceTransactionRequestInfo.setRequestType(ComplianceTransactionRequestType.COMPLETED_WITHOUT_CHANGES);
        writeToCCI(this.complianceTransactionRequestInfo);

        transactionPrinted = true;
        String multiIssueFlag = "N";	
        if (moItemData.size() > 1) {
            multiIssueFlag = "Y";	
        }

        progressBar.begin(moItemData.size());

        BigDecimal progBarTotal = new BigDecimal(0.0);
        // Now print each item. 
        for (int i = 0; i < moItemData.size(); i++) {
        	MoneyOrderInfo mo = getMoneyOrderInfo(i);
               
            String serialNum = getNextSerialNumber();
            //Update a progress bar and allow the user to cancel if they want.
            progBarTotal = progBarTotal.add(mo.getItemAmount());
            progBarTotal = progBarTotal.add(mo.getItemFee());

            boolean okToProceed = progressBar.item(i + 1, serialNum, progBarTotal);
            boolean dispenserStillOK = aDispenser.queryStatus() == DispenserInterface.OK;

            if (!okToProceed || !dispenserStillOK) {
                // The user has canceled the transaction.
                ExtraDebug.println("okToProceed["+okToProceed+"], dispenserStillOK["+dispenserStillOK+"]"); 
                ExtraDebug.println("aDispenser.queryStatus()["+aDispenser.queryStatus()+"]");
                this.setStatus(CANCELED);
                // tell the progress bar we are done
                progressBar.done();
                return;
            }
            mo.setSerialNumber(new BigInteger(serialNum));

            /* the item is about to go to Limbo, write to the CCI,send item to dispenser.
             * If item printed OK,set the MO print status to 3,write to CCI.
             * if the item did not print ,set the MO print Status to 4, write to the CCI
             * else if dispenser voided the last item, set the void Indicators and write to the CCI.
             * */
            mo.setPrintStatus(MoneyOrderPrintStatusType.ATTEMPTED);
            mo.setDateTimePrinted(TimeUtility.toXmlCalendar(TimeUtility.currentTime()));
            // this.complianceTransactionRequestInfo.setMoneyOrders(moItemData);
            // writeToCCI(this.complianceTransactionRequestInfo);

            //if in Training or Demo modes,set the void flag to true,
            // and set reason code to demo mode. this is 2 as per MOI40. 
            if (UnitProfile.getInstance().isDemoMode()) {
                mo.setVoidFlag(true);
                mo.setVoidReasonCode(MoneyOrderVoidReasonCodeType.DEMO_MODE);
            }

            // add count and amount to the batch totals. this is done with limbo flag set.
            UnitProfile.getInstance().addBatchCount(1, mo.getDocumentType().intValue());
            UnitProfile.getInstance().addBatchTotal(
                (mo.getItemAmount()),
                mo.getDocumentType().intValue());
            
            //          Print It!!!
            int itemStatus = 0;

            //set the MoneyOrder Display fields for printing
            //Get the modf from the modfitemDataVector at the same index as the MoneyOrder. 
            modf = getMoneyOrderDisplayFields(i); 
            modf.setMultiIssueIndicator(multiIssueFlag);
            if (modf.getPayeeName()==null || modf.getPayeeName().trim().equalsIgnoreCase(getEmptyItemText()))
                modf.setPayeeName("");  
            setTrainingFlags(mo, modf);

            // moved from up above
            this.complianceTransactionRequestInfo.getMoneyOrder().clear();
            this.complianceTransactionRequestInfo.getMoneyOrder().addAll(moItemData);
            // limbo should be true, void flag is true for training else false.
            writeToCCI(this.complianceTransactionRequestInfo);
            /*
             * Previously instead of backing up, DW did not write the MO info
             * to the report log until after the print was done, so it the item
             * did not print there was not an entry printed int the report for 
             * it.  However this can have some fraud implications if the agent
             * disables DW when the print starts (so the item does not get added
             * into the report log.  Instead, mark the location in the report log
             * and if the item is not printed, then later on rewind back to this
             * point.
             */
            markReportLog();
            writeToReportLog(mo, modf); // limbo

            /*
             * Print MO
             */
            itemStatus = aDispenser.printItem(mo, modf);

            Debug.println("itemStatus = " + itemStatus + " "  
                + DispenserInterface.STATUS_TEXT[itemStatus]);
                
            /* the report log looks for a limbo/non-limbo pair for a item that printed sucessfully or was voided by the dispenser.
               if the item did not print and has a print status 4, then no record should be written to the reportLog.
               For this reason, look at the item status. For ITEM_OK and ITEM_VOIDED write the record with print status 2(limbo) 
               then write again to the report log with print status 3(printed).  
               For ITEM_LIMBO,write to reportLog.
             */
            switch (itemStatus) {
                case DispenserInterface.ITEM_OK :  // 0
                    // limbo is still true, void is true if training else false.
//                    writeToReportLog(mo, modf); // limbo
                    mo.setPrintStatus(MoneyOrderPrintStatusType.CONFIRMED_PRINTED);
                    this.complianceTransactionRequestInfo.getMoneyOrder().clear();
                    this.complianceTransactionRequestInfo.getMoneyOrder().addAll(moItemData);
                    writeToCCI(this.complianceTransactionRequestInfo);
                    // add count and amount to daily and grand totals, this is done on non-limbo item.
                    if (isNotDeltaGram){
	                    UnitProfile.getInstance().addTotalAndCount(
	                        mo.getItemAmount(),
	                        1,
	                        UnitProfileInterface.DNET_ID_APP_DSPN,
	                        documentType,
	                        mo.isVoidFlag());
                    }
                    unMarkReportLog();
                    writeToReportLog(mo, modf); // nonlimbo
                    setupISI(mo);
                    break;
                case DispenserInterface.ITEM_VOIDED :   // 9
                    BigInteger savDocType = mo.getDocumentType();
                    if (!isNotDeltaGram){
                        Debug.println("MOT40.commit(), item voided, setting DocType=1 on "+mo.getSerialNumber());
                        mo.setDocumentType(BigInteger.valueOf(1L));    
                    }
//                    writeToReportLog(mo, modf); // limbo
                    mo.setVoidFlag(true);
                    mo.setVoidReasonCode(MoneyOrderVoidReasonCodeType.DISPENSER_ERROR);
                    mo.setPrintStatus(MoneyOrderPrintStatusType.CONFIRMED_PRINTED);
                    this.complianceTransactionRequestInfo.getMoneyOrder().clear();
                    this.complianceTransactionRequestInfo.getMoneyOrder().addAll(moItemData);
                    writeToCCI(this.complianceTransactionRequestInfo);
                    unMarkReportLog();
                    writeToReportLog(mo, modf); // nonlimbo
                    setupISI(mo);
                    if (!isNotDeltaGram){
                        mo.setDocumentType(savDocType);
                        Debug.println("MOT40.commit(), restoring DocType=" + savDocType + " on "+mo.getSerialNumber());
                    }
                    this.setStatus(FAILED);
                    progressBar.done();
                    return;
                case DispenserInterface.ITEM_NOT_PRINTED :  // 10
                	rewindReportLog();
                    // no write to report log for items not printed
                    // Pat is commenting out the following line because 99 is used by temp log processing.
                    // mo.setDocumentType("99");	
                    mo.setSerialNumber(new BigInteger(serialNum));
                    mo.setPrintStatus(MoneyOrderPrintStatusType.CONFIRMED_NOT_PRINTED);
                    Debug.println(
                        "the MO item " 
                            + mo.getSerialNumber()
                            + " has print status " 
                            + mo.getPrintStatus());
                    this.complianceTransactionRequestInfo.getMoneyOrder().clear();
                    this.complianceTransactionRequestInfo.getMoneyOrder().addAll(moItemData);
                    writeToCCI(this.complianceTransactionRequestInfo);
                    this.setStatus(FAILED);
                    progressBar.done();
                    return;
                case DispenserInterface.ITEM_LIMBO :    // 3
                    if(mo.getDocumentType().equals(DELTAGRAM)) {
                        Debug.println("MOT40.commit() case DI.ITEM_LIMBO setting doctype=1 on " + mo.getSerialNumber());
                        mo.setDocumentType(BigInteger.valueOf(1L));
                    }
                    unMarkReportLog();
//                    writeToReportLog(mo, modf); // limbo record
                    this.complianceTransactionRequestInfo.getMoneyOrder().clear();
                    this.complianceTransactionRequestInfo.getMoneyOrder().addAll(moItemData);
                    writeToCCI(this.complianceTransactionRequestInfo);
                    setupISI(mo);
                    // if in training mode, write a second record to the 
                    // reportlog to indicate a void.
                    if (UnitProfile.getInstance().isTrainingMode()) {
                        mo.setPrintStatus(MoneyOrderPrintStatusType.CONFIRMED_PRINTED);
                        writeToReportLog(mo, modf); // Write non-limbo record
                    }

                    this.setStatus(LOAD_REQUIRED);
                    progressBar.done();
                    return;
            }
        }
        if (isNotDeltaGram) {
            voidIfLastForm(aDispenser);
        }

        this.setStatus(COMPLETED);
        progressBar.done();

    }

    /**
     * check for training mode and set void flag and
     *  void reason code = 3 as specified in MOI40.
     */
    private void setTrainingFlags(MoneyOrderInfo mo, MoneyOrderDisplayFields modf) {
        if (UnitProfile.getInstance().isTrainingMode() 
        		|| UnitProfile.getInstance().isDemoMode()) {
            mo.setVoidFlag(true);
            mo.setVoidReasonCode(MoneyOrderVoidReasonCodeType.TRAINING_MODE);
            modf.setTrainingFlag(true);
        } else {
            mo.setVoidFlag(false);
            mo.setVoidReasonCode(null);
            modf.setTrainingFlag(false);
        }
   }
    /**
     * Lock the CCILog before the first write.
     */
    @Override
	public void lockCciLog() {
       // Debug.println("Locking the CCI before first write");	
        try {
            CCILog.getInstance().getDetailFile().lock();
        } catch (IOException e) {
            Debug.println("Error locking the CCI");	
			Debug.printStackTrace(e);
        }
    }

    /**
     * Write the ComplianceTransactionRequestInfo to the CCI. It is difficult for
     * MOT40 to keep track of whether a write has been done to the CCILog yet.
     * Therefore follow the sequence of rewind and write.
     */
    private void writeToCCI(ComplianceTransactionRequest ctri) {
        if (UnitProfile.getInstance().isDemoMode()) { // skip all report log writes in demo mode
		    Debug.println("Skipping writeToCCI for demo mode");	
            return;
        }

        //  Debug.println("writing the CTRI to the CCILog");	
        try {
            CCILog cciLog = CCILog.getInstance();
            cciLog.getDetailFile().rewind();
            cciLog.write(ctri);
        } catch (IOException e) {
            Debug.println("Error writing to the CCI");	
			Debug.printStackTrace(e);
        }
    }

    /**
      * Unlock the CCILog after all the write to the log is done.
      */
    @Override
	public void unlockCciLog() {
       // Debug.println("Unlock CCILog, done writing to it.");	
        try {
            CCILog.getInstance().getDetailFile().unLock();
        } catch (IOException e1) {
            Debug.println("Error unlocking the CCI");	
            Debug.printStackTrace(e1);
        }
    }

    /** 
     * Write the MoneyOrderInfo40 Vector to the ReportLog.
     * MoneyOrders no longer write to the templog for reporting purpose.      
     */
    public void writeToReportLog(MoneyOrderInfo mo, MoneyOrderDisplayFields modf) {
        if (UnitProfile.getInstance().isDemoMode()) // skip all report log writes in demo mode
            return;
        ReportLog.logMoneyOrder(mo, modf);
    }
    
    public void markReportLog() {
        if (UnitProfile.getInstance().isDemoMode()) // skip all report log writes in demo mode
            return;
        ReportLog.markLogLocation();
    }
    
    public void unMarkReportLog() {
        if (UnitProfile.getInstance().isDemoMode()) // skip all report log writes in demo mode
            return;
        ReportLog.unMarkLogLocation();
    }
    
    public void rewindReportLog() {
        if (UnitProfile.getInstance().isDemoMode()) // skip all report log writes in demo mode
            return;
        ReportLog.rewindLogToMark();
    }
    
    public String getPayeeName(String vendorNumber){
        Map.Entry<String, String> e;
        String vendorName = null;
        Iterator<Map.Entry<String, String>> i = vendorMap.entrySet().iterator();
        while (i.hasNext()) {
            e = i.next();
            if(e.getValue().equals(vendorNumber))
                vendorName = e.getKey();
         }
        return vendorName;
      }

    // Write entry for each MoneyOrder into the MGRAM.TRN file. Unlike
    // the setupISI functions in MoneyGramSendTransaction and 
    // MoneyGramReceiveTransaction no records are written to the DWISI.TRN
    // or DWISI.CSV files.
    public void setupISI(MoneyOrderInfo mo) {
        Object[] moItems = {
            mo.getDocumentType(),          // Document Type
            mo.getDocumentSequenceNbr(),   // Document Number
            mo.isVoidFlag() ? "V" : "",   // Void Flag
            MoneyOrderPrintStatusType.ATTEMPTED.equals(mo.getPrintStatus()) ? "L" : "",    // Limbo Flag
            mo.getPeriodNumber(),          // Period
            mo.getSerialNumber(),          // Serial Number
            mo.getItemAmount(),            // Item Face Amount
            mo.getItemFee(),               // Item Fee
            mo.isRemoteIssuanceFlag(),    // Remote Issuance Flag
            mo.getSaleIssuanceTag(),       // Sales Issuance Tag
            "",                            // Reserved for future use
            mo.getDiscountAmount(),        // Fee Discount Amount
            "",                            // Reserved for future use
            mo.getDiscountPercentage(),    // Fee Discount Percentage
            (mo.getVendorNumber()==null || mo.getVendorNumber().equals(BigInteger.ZERO)) ?  "":mo.getVendorNumber() ,      // Vendor Payment, Payee Number
            (mo.getVendorNumber()==null || mo.getVendorNumber().equals(BigInteger.ZERO)) ?  "" : getPayeeName(""+mo.getVendorNumber()) , // Vendor Payment, Payee name
            mo.getDocumentType().equals(BigInteger.ONE) ? "" : UnitProfile.getInstance().getGenericDocumentStoreName() , // Purchaser
            mo.getDocumentType().equals(DELTAGRAM) ?  getReferenceNumber() : null        // Reference Number
        };

        List<String> fields = new ArrayList<String>();
        IsiReporting isi = new IsiReporting();

        fields.add("DOC");     // Record Type
        getIsiHeader(fields);

        for(int i=0; i<moItems.length; i++){
            fields.add(nullToSpace(moItems[i]));
        }

        isi.print(fields, isi.ISI_LOG);
        if (MainPanelDelegate.getIsiExeCommand())
            isi.print(fields, isi.EXE_LOG);
    }

    @Override
	public boolean isFinancialTransaction() {
		return true;
	}
    
    @Override
	public void setFinancialTransaction(boolean b)
    {
        // Do nothing.
    }
    
    /** Moved code from MainPanel. Verify if a check in is required for this transaction
     *  and allow offline money orders and VendorPayments.
     */
    @Override
	public boolean isCheckinRequired() {
        if (UnitProfile.getInstance().isDemoMode())
            return false;

        if (!UnitProfile.getInstance().isPosAuthorized())
            return true;
        
        long lastCheckin = UnitProfile.getInstance().getLastCheckInTime().getTime();
        long currentTime = TimeUtility.currentTimeMillis();
  
     //   Compute difference between last check in time and current time
     //   and check if difference is more than 24 hours
        long timeDifference = Math.abs(lastCheckin - currentTime);
        
        if (timeDifference > 86400000)
            return true;

        else          
            return false;
    }
  

}
