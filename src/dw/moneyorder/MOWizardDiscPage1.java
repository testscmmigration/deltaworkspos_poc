package dw.moneyorder;

import javax.swing.JPanel;
import javax.swing.JTextField;

import dw.dialogs.Dialogs;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;

/**
 * Based on a Profile Item, this may be Page 1 of the MoneyOrder Wizard. 
 *   This page prompts the user for an employee discount card number from
 *   the customer. Currently, this page is set up for WalMart employee
 *   discount cards, with a length of 16 or 17 and starting with 600643.
 *   If the field is left blank, they will continue.
 */
public class MOWizardDiscPage1 extends MoneyOrderFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	private JTextField cardNumberField;
    
    public MOWizardDiscPage1(MoneyOrderTransaction40 tran, String name, String pageCode, PageFlowButtons buttons) {
            this((MoneyOrderTransaction) tran, name, pageCode, buttons);
    }
    
    public MOWizardDiscPage1(MoneyOrderTransaction transaction,
                       String name,  String pageCode, PageFlowButtons buttons) {
        super(transaction, "moneyorder/MOWizardDiscPage1.xml", name, pageCode, buttons);	
        cardNumberField = (JTextField) getComponent("cardNumberField");	
		
		DWTextPane tp = (DWTextPane) getComponent("text1");
		tp.addListener(this, this);
    }

	@Override
	public void dwTextPaneResized() {
		DWTextPane tp1 = (DWTextPane) getComponent("text1");
		DWTextPane tp2 = (DWTextPane) getComponent("text2");
		DWTextPane tp3 = (DWTextPane) getComponent("text3");
		JPanel panel1 = (JPanel) getComponent("panel1");
		int width = panel1.getPreferredSize().width;
		tp1.setWidth(width);
		tp2.setWidth(width);
		tp3.setWidth(width);
		tp1.setListenerEnabled(false);
	}

	@Override
	public void start(int direction) {
        collectComponents();
        
        flowButtons.reset();
        flowButtons.setEnabled("back", false);	

        cardNumberField.requestFocus();
        String cardNumber = transaction.getDiscountCardNumber();
        cardNumberField.setText(cardNumber == null ? "" : cardNumber);	
        addActionListener("cardNumberField", this);	
    }

    public void cardNumberFieldAction() {
        // click the next button
        flowButtons.getButton("next").doClick();	
    }

    /**
     * Get the discount card number from the field and validate it if it has
     *   been entered. If it is not valid, display an error to the user.
     */

    @Override
	public void finish(int direction) {
        // call setButtonsEnabled to avoid duplicated transactions.
        flowButtons.setButtonsEnabled(false);
        if (direction == PageExitListener.NEXT) {
            if (validateCardNumber()){
                String cardNumber = cardNumberField.getText().trim();
                transaction.setDiscountCardNumber(cardNumber);
                PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
            }
            else {
                Dialogs.showWarning("dialogs/DialogInvalidAssociateCard.xml"); 	
                flowButtons.reset();
                flowButtons.setEnabled("back", false);	
                cardNumberField.requestFocus();
                cardNumberField.selectAll();
            }
        }
        else {
            PageNotification.notifyExitListeners(this, direction);
        }
    }

    /**
     * Collects all the XML defined components.
     */
    private void collectComponents() {
        // collect the component references
        cardNumberField = (JTextField) getComponent("cardNumberField");	
    }

    private boolean validateCardNumber(){
        boolean result = false;
        if (cardNumberField.getText().length() == 0)
            result = true;
        else if (cardNumberField.getText().length() >= 16 && 
            cardNumberField.getText().length() <= 17) {
                String prefix = transaction.getDiscountOptionCardPrefix();
                if (prefix.length() > 0 && cardNumberField.getText().startsWith(prefix))
                    result = true;
        }
        return result;
    }
}
