package dw.moneyorder;

import java.awt.Component;
import java.awt.event.KeyEvent;

import dw.framework.FlowPage;
import dw.framework.KeyMapManager;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNameInterface;

public class MoneyOrderExpert extends MoneyOrderTransactionInterface {
	private static final long serialVersionUID = 1L;

	public MoneyOrderExpert(MoneyOrderTransaction transaction, PageNameInterface naming) {
        super("EmptyPanel.xml", transaction, naming, false);	
        createPages();
    }

    @Override
	public void createPages() {
        addPage(MOExpertPage1.class, transaction, "orderentry", "MOX05");	 
    }

    // override for default wizard buttons...create an empty set
    @Override
	public PageFlowButtons createButtons() {
        javax.swing.JButton[] buttons = {
        };
        String[] names = {
        };
        return new PageFlowButtons(buttons, names, this);
    }

    @Override
	public void start() {
        super.start();
        ((MoneyOrderTransaction40) transaction).lockCciLog();
        addKeyMappings();
    }

    public void f2SAR() {
        Component current = cardLayout.getVisiblePage();
        if (current instanceof FlowPage) {
            ((FlowPage) current).updateTransaction();
        }
        transaction.f2SAR();
    }

    private void addKeyMappings() {
        KeyMapManager.mapMethod(this, KeyEvent.VK_F2, 0, "f2SAR");	
    }

    /**
     * Money Order Expert is only one screen, so just call super.exit();
     */
    @Override
	public void exit(int direction) {
        if (direction == PageExitListener.CANCELED) {
            transaction.escSAR();
            ((MoneyOrderTransaction40) transaction).unlockCciLog();
            super.exit(direction);
            return;
        }

        if (direction == PageExitListener.COMPLETED) {
            transaction.f2NAG();
            ((MoneyOrderTransaction40) transaction).unlockCciLog();
            super.exit(direction);
            return;
        }
        if (direction == PageExitListener.DONOTHING)
                   return;    

        if (direction == PageExitListener.TIMEOUT || direction == PageExitListener.FAILED) {
            ((MoneyOrderTransaction40) transaction).unlockCciLog();
            super.exit(direction);
            return;
        }

        super.exit(direction);
    }
}
