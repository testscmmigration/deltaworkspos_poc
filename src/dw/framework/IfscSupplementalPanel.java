package dw.framework;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;

import com.moneygram.agentconnect.AttributeType;
import com.moneygram.agentconnect.CategoryInfo;
import com.moneygram.agentconnect.GetBankDetailsByLevelResponse;
import com.moneygram.agentconnect.GetBankDetailsResponse;
import com.moneygram.agentconnect.HierarchyLevelInfo;
import com.moneygram.agentconnect.InfoBase;
import com.moneygram.agentconnect.KeyValuePairType;

import dw.comm.MessageFacade;
import dw.comm.MessageFacadeError;
import dw.comm.MessageLogic;
import dw.dwgui.DWTextPane;
import dw.dwgui.MultiListComboBox;
import dw.framework.DataCollectionScreenToolkit.DWComboBoxItem;
import dw.framework.DataCollectionScreenToolkit.EnumeratedValue;
import dw.i18n.Messages;

public class IfscSupplementalPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	public static final Border TITLED_BORDER = BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(""), BorderFactory.createEmptyBorder(0, 4, 0, 4));
	public static final String BANK_ADDRESS_INFO_KEY = "bankBranchAddress";
			
	private DataCollectionField field;
	private JLabel addressLabel;
	private DWTextPane addressValue;
	private DWTextPane helpTextPane;
	private DataCollectionScreenToolkit dataCollectionScreenToolkit;
	private DataCollectionScreenToolkit validationScreenToolkit;
	private GetBankDetailsByLevelResponse getBankDetailsByLevelResponse;
	private GetBankDetailsResponse getBankDetailsResponse;
	private String receiveCountry;
	private boolean actionEnabled = true;
	private String lastIfscCodeValue = "";
	private List<IfscLevel> levels;
	private Dimension labelDimension;
	private JPanel searchPanel;
	private CodeStatus codeStatus;
	private SearchPanelStatus searchPanelStatus;
	private JButton expansionButton;
	
	private enum SearchPanelStatus {
		CLOSED,
		EXPANDED
	}
	
	private enum CodeStatus {
		NOT_SPECIFIED,
		INVALID_FORMAT,
		VALID_FORMAT,
		INVALID_CODE,
		VALID_CODE
	}

	private class IfscLevel implements Comparable<IfscLevel> {
		String infoKey;
		String tag;
		String value;
		Integer displayOrder;
		JLabel label;
		MultiListComboBox<DWComboBoxItem> box;
		
		@Override
		public int compareTo(IfscLevel il) {
			return this.displayOrder.compareTo(il.displayOrder);
		}
	}
	
	private class IfscItemValue extends EnumeratedValue {
		private static final long serialVersionUID = 1L;

		private Map<String, String> attributes;
		
		public IfscItemValue(String identifier, String labelShort, String label) {
			super(identifier, labelShort, label);
		}
	}
	
	private class IfscItemListener implements ItemListener {
		
		private int level;
		private MultiListComboBox<DWComboBoxItem> changedBox;

		private IfscItemListener(int level, MultiListComboBox<DWComboBoxItem> changedBox) {
			this.level = level;
			this.changedBox = changedBox;
		}
		
		@Override
		public void itemStateChanged(ItemEvent event) {
			if (actionEnabled && (event.getStateChange() == ItemEvent.SELECTED)) {
				
				if (levels.size() > level) {
					for (int i = level; i < levels.size(); i++) {
						IfscLevel ifscLevel = levels.get(i);
						ifscLevel.box.setSelectedIndex(0);
						ifscLevel.box.setEnabled(false);
					}
				}
				
				if (changedBox.getSelectedIndex() != 0) {
					int value = 0;
					try {
						DWComboBoxItem item = (DWComboBoxItem) changedBox.getSelectedItem();
						value = Integer.parseInt(item.getValue());
					} catch (Exception e) {
					}
					
					actionEnabled = false;
					getBankDetailsByLevel(level + 1, value, new CommCompleteInterface() {
						@Override
						public void commComplete(int commTag, Object returnValue) {

							boolean b = returnValue instanceof Boolean ? ((Boolean) returnValue).booleanValue() : false;
							
							if (b && (getBankDetailsByLevelResponse != null) && (getBankDetailsByLevelResponse.getPayload() != null)) {
								
								int l = getBankDetailsByLevelResponse.getPayload().getValue().getHierarchyLevelNumber();
								String labelText = getBankDetailsByLevelResponse.getPayload().getValue().getHierarchyLevelLabel();

								IfscLevel ifscLevel;
								if (levels.size() > level) {
									ifscLevel = levels.get(l - 1);
									updateLevel(ifscLevel, labelText, getBankDetailsByLevelResponse.getPayload().getValue().getHierarchyLevelInfos(), true);
								} else {
									ifscLevel = addLevel(l, labelText, getBankDetailsByLevelResponse.getPayload().getValue().getHierarchyLevelInfos(), true);
									levels.add(ifscLevel);
								}
							}
							actionEnabled = true;
						}
					});
				}
				
				((JTextField) field.getDataComponent()).setText("");
				lastIfscCodeValue = "";
				addressValue.setText("");
				codeStatus = CodeStatus.NOT_SPECIFIED;
				
				resize();
			}
		}
	}
	
	private class SetIfscBankDetailsListener implements ItemListener {

		private MultiListComboBox<DWComboBoxItem> changedBox;
		
		private SetIfscBankDetailsListener(MultiListComboBox<DWComboBoxItem> changedBox) {
			this.changedBox = changedBox;
		}
		
		@Override
		public void itemStateChanged(ItemEvent event) {
			if (actionEnabled && (event.getStateChange() == ItemEvent.SELECTED)) {
				if (changedBox.getSelectedIndex() == 0) {
					((JTextField) field.getDataComponent()).setText("");
					lastIfscCodeValue = "";
					addressValue.setText("");
					codeStatus = CodeStatus.NOT_SPECIFIED;
				} else {
					IfscItemValue item = (IfscItemValue) event.getItem();
					String ifscCode = item.attributes.get("IFSC");
					String address = item.attributes.get("ADDRESS");
					((JTextField) field.getDataComponent()).setText(ifscCode);
					lastIfscCodeValue = ifscCode;
					addressValue.setText(address);
					codeStatus = CodeStatus.VALID_CODE;
					addressValue.setForeground(Color.BLACK);
					field.getDataComponent().setForeground(Color.BLACK);
				}
				resize();
			}
		}
	}
	
	private class GetIfscBankDetailsListener implements FocusListener {
		private JTextField ifscCodeField;
		private DWTextPane addressValue;
		private Map<String, IfscLevel> infoObjectMap;
		
		private GetIfscBankDetailsListener(JTextField ifscCodeField, DWTextPane addressValue) {
			this.ifscCodeField = ifscCodeField;
			this.addressValue = addressValue;
		}

		@Override
		public void focusLost(FocusEvent e) {
			final String ifscCode = ifscCodeField.getText();

			if (actionEnabled && (! ifscCode.equals(lastIfscCodeValue))) {
				lastIfscCodeValue = ifscCode;
				
				if (ifscCode.isEmpty()) {
					codeStatus = CodeStatus.NOT_SPECIFIED;
					searchPanel.removeAll();
					levels.clear();
					addressValue.setText("");
					expansionButton.setEnabled(true);
					resize();
				} else if (field.validate(DataCollectionField.SILENT_VALIDATION) != null) {
					codeStatus = CodeStatus.INVALID_FORMAT;
					searchPanel.removeAll();
					levels.clear();
					addressValue.setText("");
					expansionButton.setEnabled(true);
					resize();
				} else {
					codeStatus = CodeStatus.VALID_FORMAT;
				
					actionEnabled = false;
					lastIfscCodeValue = ifscCode;
					getBankDetails("ifscCode", ifscCode, new CommCompleteInterface() {
						@Override
						public void commComplete(int commTag, Object returnValue) {
	
							boolean b = returnValue instanceof Boolean ? ((Boolean) returnValue).booleanValue() : false;
	
							if (b && (getBankDetailsResponse != null) && (getBankDetailsResponse.getPayload() != null)) {
								Map<String, String> values = new HashMap<String, String>();
								for (KeyValuePairType pair : getBankDetailsResponse.getPayload().getValue().getCurrentValues().getCurrentValue()) {
									values.put(pair.getInfoKey(), pair.getValue().getValue());
								}
								
								List<InfoBase> list = getBankDetailsResponse.getPayload().getValue().getInfos().getInfo();
								infoObjectMap = new HashMap<String, IfscLevel>();
								processInfoObjectList(list, values);
								
								// Remove previous items.
								
								searchPanel.removeAll();
								
								String address = values.get("bankBranchAddress");
								
								if ((ifscCode != null) && (address != null)) {
									setFieldValues(ifscCode, address);
									codeStatus = CodeStatus.VALID_CODE;
									addressValue.setForeground(Color.BLACK);
									ifscCodeField.setForeground(Color.BLACK);
	
									List<IfscLevel> items = new ArrayList<IfscLevel>();
									for (IfscLevel item : infoObjectMap.values()) {
										if (item.infoKey.equals(BANK_ADDRESS_INFO_KEY)) {
											continue;
										}
										String value = values.get(item.infoKey);
										if ((value != null) && (! value.isEmpty())) {
											items.add(item);
										}
									}
									Collections.sort(items);
									
									int c = 1;
									levels.clear();
									for (IfscLevel level : items) {
										List<HierarchyLevelInfo> valueList = new ArrayList<HierarchyLevelInfo>();
										HierarchyLevelInfo value = new HierarchyLevelInfo();
										value.setHierarchyLevelValue(level.value);
										value.setHierarchyLevelElementNumber(c);
										valueList.add(value);
										value.setHierarchyLevelElementNumber(0);
										value.setHierarchyLevelValue(level.value);
										addLevel(c, level.tag, valueList, false);
										c++;
									}
	
								} else {
									setFieldValues(null, Messages.getString("DataCollectionField.InvalidIfscCode"));
									codeStatus = CodeStatus.INVALID_CODE;
									addressValue.setForeground(Color.RED);
									ifscCodeField.setForeground(Color.RED);
	
								}
								
							} else {
								setFieldValues(null, Messages.getString("DataCollectionField.InvalidIfscCode"));
								addressValue.setForeground(Color.RED);
								ifscCodeField.setForeground(Color.RED);
								expansionButton.setEnabled(true);
								searchPanel.removeAll();
								levels.clear();
	
							}
							
							resize();
						}
					});
				}
			}
		}

		@Override
		public void focusGained(FocusEvent e) {
		}
		
		private void processInfoObjectList(List<InfoBase> infoObjectList, Map<String, String> values) {
			
			for (InfoBase infoBase : infoObjectList) {
				if (infoBase instanceof CategoryInfo) {
					processInfoObjectList(((CategoryInfo) infoBase).getInfos().getValue().getInfo(), values);
				} else {
					IfscLevel level = new IfscLevel();
					level.infoKey = infoBase.getInfoKey();
					level.tag = infoBase.getLabel();
					level.displayOrder = infoBase.getDisplayOrder();
					level.value = values.get(level.infoKey);
					if (level.displayOrder == null) {
						level.displayOrder = 0;
					}
					infoObjectMap.put(level.infoKey, level);
				}
			}
		}
		
	}
	
	private void setFieldValues(String ifscCode, String address) {
		actionEnabled = false;
		
		if ((ifscCode != null) && (! ((JTextField) field.getDataComponent()).getText().equals(ifscCode))) {
			((JTextField) field.getDataComponent()).setText(ifscCode);
			lastIfscCodeValue = ifscCode;
		}
		
		if (! addressValue.getText().equals(address)) {
			addressValue.setText(address);
			addressValue.setForeground(Color.BLACK);
			field.getDataComponent().setForeground(Color.BLACK);
			resize();
		}
		actionEnabled = true;
	}
	
	public IfscSupplementalPanel(DataCollectionField field, String receiveCountry, DataCollectionScreenToolkit toolkit) {
		
		levels = new ArrayList<IfscLevel>();
		this.field = field;
		this.receiveCountry = receiveCountry;
		this.dataCollectionScreenToolkit = toolkit;
		this.codeStatus = CodeStatus.NOT_SPECIFIED;
		
		this.setLayout(new GridBagLayout());
		
		GridBagConstraints gbc = new GridBagConstraints();
		
		// Help Text
		
		if ((field.getShortHelpText() != null) && (! field.getShortHelpText().isEmpty())) {
			helpTextPane = new DWTextPane();
			helpTextPane.setText(field.getShortHelpText());
			gbc.gridx = 0;
			gbc.gridy = 0;
			gbc.gridwidth = 2;
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.weightx = 1.0;
			gbc.insets = DataCollectionScreenToolkit.INSET_TOP_BOTTOM;
			this.add(helpTextPane, gbc);
		} 
		
		// Expansion Icon
		
		expansionButton = new JButton(Messages.getString("IfscSupplementalPanel.Find"));
		expansionButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				clickEvent();
			}
		});
		gbc.gridx = 2;
		gbc.gridy = 0;
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.NONE;
		gbc.weightx = 0.0;
		gbc.insets = DataCollectionScreenToolkit.INSET_LEFT;
		this.add(expansionButton, gbc);
		
		// Search Panel
		
		this.searchPanel = new JPanel();
		searchPanel.setLayout(new GridBagLayout());
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 3;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		gbc.insets = DataCollectionScreenToolkit.INSET_TOP;
		this.add(searchPanel, gbc);
		searchPanelStatus = SearchPanelStatus.CLOSED;
		
		// Bank Address
		
		addressLabel = new JLabel(Messages.getString("DataCollectionField.Address"));
		gbc.anchor = GridBagConstraints.NORTHWEST;
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth = 1;
		gbc.fill = GridBagConstraints.NONE;
		gbc.weightx = 0.0;
		gbc.insets = DataCollectionScreenToolkit.INSET_BOTTOM;
		this.add(addressLabel, gbc);
		
		addressValue = new DWTextPane();
		gbc.gridx = 1;
		gbc.gridy = 2;
		gbc.gridwidth = 2;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		gbc.insets = DataCollectionScreenToolkit.INSET_BOTTOM;
		this.add(addressValue, gbc);
		
		field.getDataComponent().addFocusListener(new GetIfscBankDetailsListener(((JTextField) field.getDataComponent()), addressValue));
	}

	public void clickEvent() {
		if (actionEnabled && expansionButton.isEnabled()) {
			actionEnabled = false;
			this.expansionButton.setEnabled(false);
	
			int level1ItemCount = 0;
			if (! levels.isEmpty()) {
				level1ItemCount = levels.get(0).box.getItemCount();
			}
			
			if (! codeStatus.equals(CodeStatus.VALID_CODE)) {
				if (level1ItemCount <= 1) { 
					levels.clear();
	
					this.getBankDetailsByLevel(1, 0, new CommCompleteInterface() {
						@Override
						public void commComplete(int commTag, Object returnValue) {
		
							boolean b = returnValue instanceof Boolean ? ((Boolean) returnValue).booleanValue() : false;
		
							if (b && (getBankDetailsByLevelResponse != null) && (getBankDetailsByLevelResponse.getPayload() != null)) {
		
								String labelText = getBankDetailsByLevelResponse.getPayload().getValue().getHierarchyLevelLabel();
								
								IfscLevel level = addLevel(1, labelText, getBankDetailsByLevelResponse.getPayload().getValue().getHierarchyLevelInfos(), true);
								levels.add(level);
							}
							actionEnabled = true;
						}
					});
					return;
					
				} else {
					boolean first = true;
					for (IfscLevel level : levels) {
						if (first) {
							level.box.setVisible(true);
							level.box.setSelectedIndex(0);
							first = false;
						} else {
							level.label.setVisible(false);
							level.box.setVisible(false);
						}
					}
				}
			}
			
			this.field.setValue("");
			this.addressValue.setText("");
	
			resize();
			actionEnabled = true;
		}
	}
	
	private void resize() {
		if (dataCollectionScreenToolkit != null) {
			dataCollectionScreenToolkit.getComponentResizeListener().componentResized(null);
		}
		if (validationScreenToolkit != null) {
			validationScreenToolkit.getComponentResizeListener().componentResized(null);
		}
	}
	
	private IfscLevel addLevel(final int level, String labelText, List<HierarchyLevelInfo> items, boolean isEnabled) {
		
		final IfscLevel ifscLevel = new IfscLevel();
	
		ifscLevel.label = new JLabel(labelText);
		ifscLevel.label.setPreferredSize(labelDimension);
		ifscLevel.label.setVisible(true);
		
		ifscLevel.box = new MultiListComboBox<DWComboBoxItem>();
		ifscLevel.box.setVisible(true);
		
		boolean addItemListener = updateLevel(ifscLevel, labelText, items, isEnabled);
		
		if (addItemListener) {
			ifscLevel.box.addItemListener(new IfscItemListener(level, ifscLevel.box));
		} else {
			ifscLevel.box.addItemListener(new SetIfscBankDetailsListener(ifscLevel.box));			
		}

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				GridBagConstraints gbc = new GridBagConstraints();
				gbc.gridx = 0;
				gbc.gridy = level - 1;
				gbc.gridwidth = 1;
				gbc.fill = GridBagConstraints.NONE;
				gbc.weightx = 0.0;
				gbc.insets = DataCollectionScreenToolkit.INSET_BOTTOM;
				searchPanel.add(ifscLevel.label, gbc);

				gbc.gridx = 1;
				gbc.gridy = level - 1;
				gbc.gridwidth = 2;
				gbc.fill = GridBagConstraints.HORIZONTAL;
				gbc.weightx = 1.0;
				gbc.insets = DataCollectionScreenToolkit.INSET_BOTTOM;
				searchPanel.add(ifscLevel.box, gbc);
				
				ifscLevel.label.invalidate();
				ifscLevel.box.invalidate();
				searchPanel.repaint();
				resize();
			}
		});
		
		return ifscLevel;
	}
	
	private boolean updateLevel(IfscLevel ifscLevel, String labelText, List<HierarchyLevelInfo> hierarchyLevelInfos, boolean isEnabled) {
		
		ifscLevel.box.removeAllItems();
		ifscLevel.box.setPreferredSize(null);
		
		if (isEnabled) {
			String l = Messages.getString("DWValues.Select");
			IfscItemValue ev = new IfscItemValue("", l, l);
			ifscLevel.box.addItem(ev);
		}

		boolean addItemListener = true;
		for (HierarchyLevelInfo levelInfo : hierarchyLevelInfos) {
			long elementNumber = levelInfo.getHierarchyLevelElementNumber();
//			String value = levelInfo.getHierarchyLevelValue().trim();
			String value = levelInfo.getHierarchyLevelValue();
			IfscItemValue ev = new IfscItemValue(String.valueOf(elementNumber), value, value);
			ifscLevel.box.addItem(ev);
			
			if (! levelInfo.getAttributes().isEmpty()) {
				for (AttributeType attribute : levelInfo.getAttributes()) {
					String attributeName = attribute.getAttributeLabel();
					String attributeValue = attribute.getAttributeValue();
					if ((attributeName != null) && (! attributeName.isEmpty()) && (attributeValue != null) && (! attributeValue.isEmpty())) {
						if (ev.attributes == null) {
							ev.attributes = new HashMap<String, String>();
						}
						ev.attributes.put(attributeName, attributeValue);
						addItemListener = false;
					}
				}
			}
		}
		
		ifscLevel.box.setEnabled(isEnabled);
		
		Dimension d = ifscLevel.box.getPreferredSize();
		
		if (dataCollectionScreenToolkit != null) {
			dataCollectionScreenToolkit.updateMaximumComponentWidth(d.width);
			dataCollectionScreenToolkit.getComponentResizeListener().componentResized(null);
		}
		if (validationScreenToolkit != null) {
			validationScreenToolkit.updateMaximumComponentWidth(d.width);
			validationScreenToolkit.getComponentResizeListener().componentResized(null);
		}
		
		return addItemListener && isEnabled;
	}
	

	public void setLabelWidth(Dimension labelDimension) {
		addressLabel.setPreferredSize(labelDimension);
		this.labelDimension = labelDimension;
	}

	private void getBankDetailsByLevel(final int hierarchyLevelNumber, final int previousLevelElementNumber, CommCompleteInterface cci) {
		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {
				try {
					getBankDetailsByLevelResponse = null;
					getBankDetailsByLevelResponse = MessageFacade.getInstance().getBankDetailsByLevel(receiveCountry, hierarchyLevelNumber, previousLevelElementNumber);
					return Boolean.TRUE;
				} catch (MessageFacadeError e) {
					return Boolean.FALSE;
				}
			}
		};
		MessageFacade.run(logic, cci, 0, Boolean.TRUE);
	}
	
	private void getBankDetails(final String infoKey, final String value, CommCompleteInterface cci) {
		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {
				try {
					getBankDetailsResponse = null;
					getBankDetailsResponse = MessageFacade.getInstance().getBankDetails(receiveCountry, infoKey, value);
					return Boolean.TRUE;
				} catch (MessageFacadeError e) {
					return Boolean.FALSE;
				}
			}
		};
		MessageFacade.run(logic, cci, 0, Boolean.TRUE);
	}

	public void setWidth(Dimension labelDimension, Dimension valueDimension) {
		this.addressLabel.setPreferredSize(labelDimension);
		this.addressValue.setWidth(valueDimension.width);
		
		if (helpTextPane != null) {
			int width = labelDimension.width + valueDimension.width - this.expansionButton.getPreferredSize().width - DataCollectionScreenToolkit.COMPONENT_SPACING;
			this.helpTextPane.setWidth(width);
		}
		for (IfscLevel level : levels) {
			level.label.setPreferredSize(labelDimension);
			level.box.setPreferredSize(valueDimension);
		}
	}
	
	public void setValidationScreenToolkit(DataCollectionScreenToolkit validationScreenToolkit) {
		this.validationScreenToolkit = validationScreenToolkit;
	}

	public JLabel getAddressLabel() {
		return addressLabel;
	}

	public int getPanelWidth() {
		int width = 0;
		for (IfscLevel level : levels) {
			width = Math.max(width, level.box.getPreferredSize().width);
		}
		return width;
	}
}
