package dw.framework;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import dw.NonObfuscatable;
import dw.dialogs.Dialogs;
import dw.utility.Debug;

/** 
 * Superclass for dialogs too complicated to be a simple JOptionPane. 
 * Together with Dialogs.java, this class replaces XmlDefinedDialog.
 * In a way, this class is a throwback to the original concept of 
 * XmlDefinedDialog, but without all the complicated baggage that 
 * class wound up with. 
 * 
 * Instances of this class (and subclasses) are not actually dialogs.
 * In MVC terms, they're really controllers, in charge of configuration
 * and event handling. The actual JDialogs and/or JOptionPanes are
 * created by Dialogs.java
 */
public class XmlDialogPanel implements NonObfuscatable {
    protected XmlDefinedPanel mainPanel = null;
    protected String title = Dialogs.TITLE;
    protected JOptionPane optionPane;
    protected JDialog dialog;

    /** For debugging. */
    @Override
	public String toString() {
        return this.getClass().getName() + "(" + ((mainPanel == null) ? "" : mainPanel.xmlFileName);	 
    }
    
    public XmlDialogPanel() {
    }

    public XmlDialogPanel(String filename) {
        mainPanel = new XmlDefinedPanel(filename);
    }
    
    public XmlDefinedPanel getMainPanel() {
        return mainPanel;
    }

    /** 
     * Used by Dialogs.java to determine if title should be shown in debug log.
     * Subclasses can override to return false for dialogs that don't count
     * (like login) or are special cases (like communication dialog).
     */
    public boolean showTitleInDebug() {
        return true;
    }

    /** Used by Dialogs.java to set the title in the JDialog. */
    public String getTitle() {
        return title;
    }

    /** Used by Dialogs.java so that subclasses can close the dialog. */
    public void setOptionPane(JOptionPane pane) {
        optionPane = pane;
    }
    
    /**
     * Used by subclasses to close the dialog. Probably better to 
     * call closeDialog() instead.
     */
    public JOptionPane getOptionPane() {
        return optionPane;
    }

    /** Used by Dialogs.java */
    public void setDialog(JDialog dialog) {
        this.dialog = dialog;
    }
    public JDialog getDialog() {
        return dialog;
    }

    /** 
     * Closes the associated dialog. This implementation is one of 
     * two that were used before this class was written. It is kind 
     * a roundabout way to do it, but avoids leaking the JDialog
     * reference from Dialogs.java. That class is responsible for
     * disposing the JDialog and calling our cleanup method.
     *
     * @param option the value to return to the code which posted the
     * dialog, such as JOptionPane.CANCEL_OPTION or CLOSED_OPTION.
     * Note: Dialogs.showPanel does not currently return the supplied
     * value to the code which posted the dialog.
     */
    public void closeDialog(int option) {
        if (optionPane == null) {
            Debug.println("XmlDialogPanel.closeDialog() optionPane=null");
        }
        else {
            optionPane.setValue(Integer.valueOf(option));
        }
    }

    /** 
     * Closes the associated dialog. See closeDialog(int) for details. 
     */
    public void closeDialog() {
        closeDialog(Dialogs.CLOSED_OPTION);
    }
    
    /** Subclasses can override if they have additional cleanup. */
    public void cleanup() {
        if (mainPanel != null) {
            mainPanel.cleanup();
        }
    }

    /**
     * Used by Dialogs to determine if the communication dialog
     * should be reparented while the dialog associated with 
     * this object is displayed. Subclasses should override 
     * this method to return true if they perform any 
     * messaging. Such subclasses should be rare; the MG report
     * dialog may be the only one. 
     */
    public boolean usesComm() {
        return false;
    }
    
    /** 
     * Show a dialog containing this panel. For compatibility. 
     * New code should call Dialogs.showPanel() instead.
     */
    public void show() {
        Dialogs.showPanel(this);
    }

    /** Returns true if the dialog is currently displayed on screen. */
    public boolean isShowing() {
        return mainPanel.isShowing();
    }
        

}

