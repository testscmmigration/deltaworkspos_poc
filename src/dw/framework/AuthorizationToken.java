package dw.framework;

public interface AuthorizationToken {
	
	final public static int VALID_TOKEN = 0;
	final public static int DIFFERENT_TOKEN_GENERATOR = 1;
	final public static int INVALID_NUMBER_CHARACTERS = 2;
	final public static int INVALID_TOKEN_CHARACTERS = 3;
	final public static int INVALID_TOKEN_GENERATOR = 4;
	
	public String getValue();
	public int checkToken(String profileTokenId);
	public String getTokenId();
	public boolean isValidTokenID(String tokenId);
	public long toLongValue(String modHexValue);
}
