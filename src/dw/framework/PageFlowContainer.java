package dw.framework;

import java.awt.Component;
import java.awt.KeyboardFocusManager;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import dw.comm.MessageFacade;
import dw.dialogs.CommunicationDialog;
import dw.dwgui.CustomCardLayout;
import dw.main.DeltaworksMainMenu;
import dw.main.MainPanelDelegate;
import dw.profile.UnitProfile;
import dw.utility.Debug;
import dw.utility.IdleBackoutTimer;

/**
 * An Xml Defined Panel that contains a main area for a
 *   card layout. It contains a standard set of buttons used to traverse through
 *   the pages and cancel the flow. Methods are provided to show / get a
 *   page by name, add pages, and traverse pages.
 */
public abstract class PageFlowContainer extends XmlDefinedPanel
                                        implements PageExitListener {
	private static final long serialVersionUID = 1L;

	protected PageFlowButtons buttons = null;
    protected CustomCardLayout cardLayout = null;
    protected JPanel cardPanel = null;        // stores the flow of pages.
    private Stack<String> pageHistory = new Stack<String>();  // of page names
    private PageNameInterface naming = null;
    private ArrayList<NamedCard> cards = new ArrayList<NamedCard>();
    protected JPanel sideBannerPanel;
    


    /**
     * Create from a given XML file, which must contain the named
     *   CardLayout panel. A default set of buttons will be created. The XML
     *   DefinedPanel must have a CARDLAYOUT panel component with the name of
     *   'pageFlowPanel'.
     * @param xmlFileName the XML file that defines this container
     * @param sideBar indicates if the panel has a sidebar
     */
    public PageFlowContainer(String xmlFileName, boolean sideBar) {
        super(xmlFileName);
        cardPanel = (JPanel) getComponent("pageFlowPanel");	
        cardLayout = (CustomCardLayout) cardPanel.getLayout();
        cardLayout.setDelegate(this);
        buttons = createButtons();
        if (sideBar) {
            sideBannerPanel = (JPanel) getComponent("sideBanner");
        } else {
        	sideBannerPanel = null;
        }
    }

    /**
     * Create from a given XML file, which must contain the named
     *   CardLayout panel. A default set of buttons will be created. The XML
     *   DefinedPanel must have a CARDLAYOUT panel component with the name of
     *   'pageFlowPanel'.
     * @param xmlFileName the XML file that defines this container
     * @param naming an instance of the callback interface for page naming
     */
    public PageFlowContainer(String xmlFileName, PageNameInterface naming, 
    		boolean sideBar) {
        this(xmlFileName, sideBar);
        this.naming = naming;
    }

    /**
     * Create a default set of flow buttons. Override this in subclass to create
     *   a custom set of buttons. Default buttons will contain the standard
     *   wizard style buttons (BACK, NEXT, CANCEL, EXPERT).
     */
    protected PageFlowButtons createButtons() {
        JButton[] buttons = {(JButton) getComponent("expertButton"),	
                             (JButton) getComponent("backButton"),	
                             (JButton) getComponent("nextButton"),	
                             (JButton) getComponent("cancelButton")
                             //,(JButton) getComponent("errorButton")
                             };	
        String[] buttonNames = {"expert", "back", "next", "cancel"};    
        //String[] buttonNames = {"expert", "back", "next", "cancel","errors"};    
        return new PageFlowButtons(buttons, buttonNames, this);
    }

    /**
     * Must be overridden to create pages and add them to the flow.
     */
    protected abstract void createPages();

    /**
     * Adds a FlowPage to the card layout.
     * Use of this method is discouraged. Transaction interfaces should
     * use one of the variants of addPage which takes the page class as
     * the first parameter. This method is retained for compatibility 
     * with ProfileWizard, which is not worth converting. Eventually,
     * ProfileWizard should be obsoleted by SimplePMTransactionInterface.
     * 
     * @param page the FlowPage to add to the layout
     */
    public void addPage(FlowPage page) {
        addPage(page, page.getName());
    }

    /**
     * Adds a component to the card layout.
     * This method is used by DeltaworksMainPanel to add transaction
     * interfaces to an overall page flow container. Use of this method
     * by anyone else is discouraged. Transaction interfaces should use
     * one of the variants of addPage which takes the page class as
     * the first parameter. 
     * @param page the transaction interface to add to the layout.
     * @param pageName the name of the page in the layout
     */
    public void addPage(Component page, String pageName) {
        cardPanel.add(page, pageName);
        cards.add(new NamedCard(pageName, page));
        PageNotification.addPageExitListener(this, page);
    }

    /**
     * Deferred add of a flowpage to the card layout. Records the class 
     * and constructor arguments so we can construct the page later.
     * @param pageClass the class of the flowpage to be constructed.
     * @param transaction the associated transaction logic object.
     * @param pageName identifier for later use in a call to showPage.
     * @param pageCode screen number which appears in the title bar.
     */
    public void addPage(Class<?> pageClass, ClientTransaction transaction,
            String pageName, String pageCode) {
        Class<?>[] params = new Class[] { transaction.getClass(), String.class, 
                    String.class}; 
        Object[] args = new Object[] {transaction, pageName, pageCode};
        cards.add(new NamedCard(pageClass, args, params));
    }

    /**
     * Same as above with an extra parameter.
     * @param buttons set of buttons which control page traversal.
     */
    public void addPage(Class<?> pageClass, ClientTransaction transaction,
            String pageName, String pageCode, PageFlowButtons buttons) {
        Class<?>[] params = new Class[] { transaction.getClass(), String.class, 
                    String.class, PageFlowButtons.class }; 
        Object[] args = new Object[] {transaction, pageName, pageCode, buttons};
        cards.add(new NamedCard(pageClass, args, params));
    }

    /**
     * Same as above with an extra parameter.
     * @param b additional flag supported by some flow pages to indicate role
     */
    public void addPage(Class<?> pageClass, ClientTransaction transaction,
            String pageName, String pageCode, PageFlowButtons buttons, boolean b) {
        Class<?>[] params = new Class[] { transaction.getClass(), String.class, 
                    String.class, PageFlowButtons.class, Boolean.TYPE }; 
        Object[] args = new Object[] {transaction, pageName, pageCode, buttons, Boolean.valueOf(b)};
        cards.add(new NamedCard(pageClass, args, params));
    }


    /** Instantiate a page whose creation was deferred. */
    public Component constructPage(String pageName) {
        Component page = null;
        NamedCard nc = null;
        try {
            for (Iterator<NamedCard> cardIter = cards.iterator(); cardIter.hasNext(); ) {
                nc = cardIter.next();
                if ((nc != null) && (nc.cardName.equals(pageName))) {
                    // essentially return new cardClass(args)
                    nc.card = (Component) 
                        nc.cardClass.getConstructor(nc.params).newInstance(nc.args);
                    page = nc.card;

                    // For compatibility with old logic, call clear on the new
                    // page. Otherwise state boxes don't get the right default,
                    // and there may be other initializations that get missed.
                    if (page instanceof XmlDefinedPanel)
                        ((XmlDefinedPanel) page).clear();
                    
                    break;
                }
            }
            if (page == null) {
                Debug.println("constructPage(" + pageName+ ") failed to find page");	 
            }
            else {
                cardPanel.add(page, pageName);
                PageNotification.addPageExitListener(this, page);
            }
        }
        catch (NoSuchMethodException e1) {
            Debug.printStackTrace(e1);
        }
        catch (InstantiationException e2) {
            if (nc != null) {
                StringBuffer debugbuf = new StringBuffer("<init>(");
                String sep = "";
                for (int i = 0; i < nc.params.length; i++) {
                    debugbuf.append(sep);
                    debugbuf.append(nc.params[i].getName());
                    sep = ",";
                }
                debugbuf.append(")");
                Debug.println(debugbuf.toString());
            }
            Debug.printStackTrace(e2);
        }
        catch (IllegalAccessException e3) {
            Debug.printStackTrace(e3);
        }
        catch (InvocationTargetException e4) {
            Debug.printStackTrace(e4);
        }
            
        return page;
    }
     

    /**
     * Get a page from the container by name.
     * @param pageName the name of the page to retrieve
     * @return the page with the given name
     */
    public Component getPage(String pageName) {
        return this.getCardComponent(pageName);
    }

    /**
     * Clear out the contents of all the components in all the contained pages.
     * Called by DeltaworksMainPanel right before starting a new transaction,
     * to make sure leftover data isn't displayed.
     */
    @Override
	public void clear() {
        for (Iterator<Component> i = this.getAllPages().iterator(); i.hasNext(); ) {
            Object page = i.next();
            if (page instanceof XmlDefinedPanel)
                ((XmlDefinedPanel) page).clear();
        }
    }

    /**
     * Unload pages that can be recreated later, and clear the rest.
     * Called by DeltaworksMainPanel after a transaction is done, to 
     * reclaim memory.
     */
    public void unloadPages() {
        Iterator<NamedCard> i = cards.iterator();
        Component page = null;
        while (i.hasNext()) {
            NamedCard nc = i.next();
            page = nc.card;
            // ignore pages that aren't loaded
            if (page != null) {
                // if we know the class, we can reconstruct it later
                if (nc.cardClass != null) {
                    // reverse the operations in constructPage
                    PageNotification.removePageExitListener(this, page);
                    cardPanel.remove(page);
                    nc.card = null;
                }
                else if (page instanceof XmlDefinedPanel) {
                    ((XmlDefinedPanel) page).clear();
                }
            }
        }
    }

    /**
     * Show a particular page in the layout.
     * @param pageName the name of the page in the layout to show
     * @param direction the direction we are going in the flow...
     * @param starting are we just starting the flow?
     */
    public void showPage(final String pageName, final int direction,
           final boolean starting) {
    	/*
    	 * Issue: A possible, outstanding dialog popup from the previous 
    	 * page might remain open because its close logic is delayed (it 
    	 * waits or sleeps) to allow the user to view it; however, that 
    	 * delay might be enough to cause its close to occur after the 
    	 * previous page is closed and this page opened. In that event, 
    	 * the close would be ignored and cause the popup to remain open 
    	 * until the user manually closed it, which is the reported "blank
    	 * popup" issue. So this fix always does an automatic close to get 
    	 * rid of it, if it's still open. If not, the close is just ignored.
    	 */
    	MessageFacade.hideDialogNow();
    	
        SwingUtilities.invokeLater(new Runnable() {
            @Override
			public void run() {
                // Workaround for Sun bug 4722671 and 478813. There's a race
                // condition in native code called from Sun's focus manager
                // when an async focus retarget tries to find a new component
                // to assign focus, and the application is adding and removing
                // components at the same time. So give it time to settle
                // down before we start explicitly setting focus ourselves.
                try { Thread.sleep(100); } catch (InterruptedException e) { }
                showPageImpl(pageName, direction, starting);
            }
        });
    }

    private void showPageImpl(String pageName, final int direction, boolean starting) {
        try {
            Component currentPage = null;
            // get the current page and clean it up
            String currentPageName = this.getVisiblePageName();

            //Debug.println("showPage(" + pageName+","+direction+","+starting
            //        +"): currentPageName = " + currentPageName);	
            if (currentPageName != null) {
                currentPage = this.getCardComponent(currentPageName);
                
                if (currentPage instanceof FlowPage) {
                    ((FlowPage) currentPage).cleanup();
                }
            }
            // get the page that will be displayed next
            Component nextPage = this.getCardComponent(pageName);

            if (nextPage == null) {
                nextPage = constructPage(pageName);
            }
            
            // If we are going forward a page, make the current page history
            if ((! starting) && (nextPage != currentPage)
                    && (direction == PageExitListener.NEXT) && (currentPageName != null)) {
                pageHistory.push(currentPageName);
//            	ExtraDebug.println("pageHistory.push["+pageHistory+"]");
            }

            // empty the event queue of any pending key and mouse events that
            //   may have been activated while we were waiting for something
            emptyEventQueue();

            if (nextPage == null)
                return;

            // Before we try to show the page, update the page name display
            // so any errors appear after it in the debug log.
            if (nextPage instanceof FlowPage) {
                if (naming != null)
                    naming.setCurrentPageName(((FlowPage) nextPage).getPageCode());
            }
            
            if (sideBannerPanel != null ) {
                sideBannerPanel.setVisible(((FlowPage) nextPage).displaySideBanner());
            }
            
            // show the next page
            cardLayout.show(cardPanel, pageName);

            if (nextPage instanceof FlowPage) {
                // start the page 
                ((FlowPage) nextPage).start(direction);

                if(!(nextPage instanceof DeltaworksMainMenu)){
                    // add the idle focus listeners so we check in more often
                    addIdleFocusListeners((XmlDefinedPanel) nextPage);
                }
            }
            validate();

            // reset the idle timer
            IdleBackoutTimer.checkIn();
        }
        catch (Exception e) {
            Debug.println("show page failed : " + e);	
            Debug.printStackTrace(e);
        }
    }

    /**
     * Show a particular page in the card layout.
     * @param direction the direction that the page flow moved in to arrive at
     *   this page.
     * @param pageName the page to show in the layout
     */
    public void showPage(String pageName, int direction) {
        showPage(pageName, direction, false);
    }

    /**
     * Add focus listeners to all the components that check in with the idle
     *   timer, so we don't timeout on a page that requires a lot of entry.
     * @param panel the XmlDefinelPanel on which to install the idle listeners
     */
    private void addIdleFocusListeners(final XmlDefinedPanel panel) {
        Collection<Object> c = panel.getAllComponents();
        // create a focus listener that will reset the idle timer on focus event
        
        FocusListener resetter = new FocusAdapter() {
                @Override
				public void focusLost(FocusEvent e) {
                     dw.utility.IdleBackoutTimer.checkIn();
                }};
        // install it on all the components in the panel
        for (Iterator<Object> i = c.iterator(); i.hasNext(); ) {
            Object o = i.next();
            if (o instanceof Component) {
                Component comp = (Component) o;
                addFocusListener(comp, resetter);
            }
        }
    }

    /**
     * Start up the container and show the first page.
     */
    public void start(int p) {
        // clear out the page history
        pageHistory.clear();

        // install the appropriate listeners on the flow buttons
        if (buttons.getButton("back") != null)	
            addActionListener("backButton", this, "back");	 
        if (buttons.getButton("next") != null)	
            addActionListener("nextButton", this, "next");	 
        if (buttons.getButton("cancel") != null)	
            addActionListener("cancelButton", this, "cancel");	 
        if (buttons.getButton("expert") != null)	
            addActionListener("expertButton", this, "expert");	 
        if (buttons.getButton("errors") != null)	
            addActionListener("errorButton", this, "errors");	 
        // show the first page, but do not let it get pushed on the history
        showPage(this.getComponentName(p), PageExitListener.NEXT, true);
    }
    
    public void start(String startPage) {
		int i = getCardIndex(startPage);
    	start(i);
    }
    
    public void start() {
        start(0);
    }


    /**
     * Implementation of PageExitListener. Will be called when a page that has
     *  been registered exits. For BACK and NEXT, show the corresponding page
     *  in the layout. For CANCEL, COMPLETE, and FAIL, notify my listeners that
     *  I am done.
     * @param direction in which the flow is going
     */
    @Override
	public void exit(int direction) {
        int visibleIndex = this.getVisibleIndex();

        // Before we show the next page, remove focus from current page. This helps
        // work around Sun bug 4722671 where focus manager looks for another component
        // to transfer focus to, but by the time the event gets there the chosen
        // component isn't there any more.
        KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();

        // Show the next page in the layout if there is one
        if (direction == PageExitListener.NEXT) {
            if ((visibleIndex + 1) < this.getCardCount())
                showPage(this.getComponentName(visibleIndex + 1), direction);
            else
                return;
        }

        // show the previous page in the layout if there is one
        else if (direction == PageExitListener.BACK) {
            // use the history to determine what the previous page was
            if (pageHistory.empty())
                return;
            String previousPageName = pageHistory.pop();
//        	ExtraDebug.println("pageHistory.pop["+pageHistory+"]");
            showPage(previousPageName, direction);
        }

        // flow is finished, clean up and notify my listeners that I am done
        else {
            buttons.disable();
            buttons.removeKeyMappings();   // clean up button key mappings
            cleanup();                     // remove mappings and listeners
            Component currentPage = this.getVisiblePage();
            // clean up the current page
            if (currentPage instanceof FlowPage)
                ((FlowPage) currentPage).cleanup();
            // notify listeners
            PageNotification.notifyExitListeners(this, direction);
            if(direction == PageExitListener.TIMEOUT){
            if(UnitProfile.getInstance().isSubAgent()|| UnitProfile.getInstance().isTakeOverSubagentprofile()){
        		MainPanelDelegate.changeToSuperAgent(true);
        		// getTransaction().setStatus(ClientTransaction.TIMED_OUT);
        	}
            }
        	
           
        }
    }

    /**
     * Tell the current page that we wish to go to the next page.
     */
    public void next() {
        pageExit(PageExitListener.NEXT);
    }
    
    /**
     * Pops the page history until we get to the specified page, this allows
     * the back to skip pages when needed.
     */
    public void popHistoryUntil(String sPageName) {
        // use the history to determine what the previous page was
        String previousPageName;
    	do {
            if (pageHistory.empty())
                return;
            previousPageName = pageHistory.pop();
//        	ExtraDebug.println("pageHistory.pop["+pageHistory+"]");
    	} while (sPageName.compareToIgnoreCase(previousPageName) != 0);
        showPage(previousPageName, PageExitListener.BACK);
    }
    
    public void popHistoryUntil(List<String> pageNames) {
    	String page = null;
    	while (true) {
    		try {
    			page = pageHistory.pop();
    		} catch (Exception e) {
    			return;
    		}
    		if (pageNames.contains(page)) {
    			break;
    		}
    	}
    	if (page != null) {
    		showPage(page, PageExitListener.BACK);
    	}
    }

    /**
     * Tell the current page that we wish to go back to the previous page.
     */
    public void back() {
        pageExit(PageExitListener.BACK);
    }

    /**
     * Tell the current page that we wish to cancel out.
     */
    public void cancel() {
        pageExit(PageExitListener.CANCELED);
    }

    /**
     * Tell the current page that we wish to switch to the expert interface.
     */
    public void expert() {
        pageExit(PageExitListener.EXPERT);
    }

    
    public void errors() {
       // pageExit(PageExitListener.CANCELED);
    	CommunicationDialog.getInstance().showErrors(null, true);
    }

    /**
     * Exit the current page in the layout. Notify it of the direction we wish
     *   to exit in.
     * @param direction in which the flow is going
     */
    protected void pageExit(int direction) {
        Component currentPage = this.getCardComponent(
                this.getVisiblePageName());
        // tell the current page we wish to exit
        if (currentPage instanceof FlowPage) {
            buttons.disable();
            ((FlowPage) currentPage).finish(direction);
        }
        else {
            exit(direction);
        }
    }

    /**
     * Data class to store a name / card pair.
     */
    private class NamedCard {
        public String cardName = null;
        public Component card = null;
        public Class<?> cardClass = null;
        public Object args[];
        public Class<?> params[];

      
        public NamedCard(String cardName, Component card) {
            this.cardName = cardName;
            this.card = card;
        }

/*        public NamedCard(Class pageClass, ClientTransaction transaction,
                String pageName, String pageCode, PageFlowButtons buttons) {
            
            params = new Class[] { transaction.getClass(), String.class, 
                    String.class, PageFlowButtons.class }; 

            cardName = pageName;
            card = null;
            cardClass = pageClass;
            args = new Object[] {transaction, pageName, pageCode, buttons};
        }
*/
        public NamedCard(Class<?> pageClass, Object[] objArr, Class<?>[] p) {
            this(pageClass, objArr, p, 1);
            
//            cardName = (String) objArr[1];
//            card = null;
//            cardClass = pageClass;
//            params = p;
//            args = objArr;
        }

        public NamedCard(Class<?> pageClass, Object[] objArr, Class<?>[] p, int index) {
            cardName = (String) objArr[index];
            card = null;
            cardClass = pageClass;
            params = p;
            args = objArr;

//            // For fine-tuning the obsfucator
//            StringBuffer debugbuf = new StringBuffer("<init>(");
//            String sep = "";
//            for (int i = 0; i < p.length; i++) {
//                debugbuf.append(sep);
//                debugbuf.append(p[i].getName());
//                sep = ",";
//            }
//            debugbuf.append(")");
//            Debug.track(debugbuf.toString());            

        }
}

    /**
     * Get the name of the currently showing card in the layout.
     * Pages which aren't loaded are obviously not visible.
     */
    public String getVisiblePageName() {
        for (int i = 0; i < cards.size(); i++) {
            NamedCard nameCard = cards.get(i);
            if ((nameCard.card != null) && nameCard.card.isVisible()) {
                return nameCard.cardName;
            }
        }
        return null;
    }

    /**
     * Get a particular card in the layout by name.
     * Refactored from CustomCardLayout.getComponent().
     * Returns null if the card hasn't been loaded yet.
     */
    public Component getCardComponent(String componentName) {
        for (Iterator<NamedCard> cardIter = cards.iterator(); cardIter.hasNext(); ) {
            NamedCard nameCard = cardIter.next();
            if (nameCard.cardName.equals(componentName))
                return nameCard.card;
        }
        return null;
    }

    /** Returns all currently loaded pages. */
    public Collection<Component> getAllPages() {
        ArrayList<Component> pages = new ArrayList<Component>();
        for (Iterator<NamedCard> i = cards.iterator(); i.hasNext(); ) {
            Component c = (i.next()).card;
            if (c != null) {
                pages.add(c);
            }
        }
        return pages;
    }

    /**
     * Get the name of the component at the given index.
     * Note that unloaded cards are not returned, but that's ok.
     */
    public String getComponentName(int index) {
        if (index < cards.size() && index >= 0) {
            NamedCard nameCard = cards.get(index);
            return nameCard.cardName;
        }
        else
            return null;
    }

    /**
     * Get the index of the currently showing card in the layout.
     * Cards that are not loaded are obviously not visible.
     */
    public int getVisibleIndex() {
        for (int i = 0; i < cards.size(); i++) {
            NamedCard nameCard = cards.get(i);
            if ((nameCard.card != null) && nameCard.card.isVisible()) {
                return i;
            }
        }
        return 0;
    }

    /**
     * Get the number of cards in this card layout.
     * Includes cards that are not loaded, because that's what the caller needs.
     */
    public int getCardCount() {
        return cards.size();
    }

    /**
     * Get the currently showing card in the layout.
     */
    public Component getVisiblePage() {
        return getCardComponent(getVisiblePageName());
    }

    public int getCardIndex(String name) {
    	for (int i = 0; i < cards.size(); i++) {
    		NamedCard card = cards.get(i);
    		if (card.cardName.equals(name)) {
    			return i;
    		}
    	}
    	return -1;
    }
}
