package dw.framework;

import java.io.Serializable;

public class WaitToken implements Serializable {
	private static final long serialVersionUID = 1L;

	private boolean waitFlag;
	
	public WaitToken() {
		waitFlag = true;
	}

	public boolean isWaitFlag() {
		return waitFlag;
	}

	public void setWaitFlag(boolean waitFlag) {
		this.waitFlag = waitFlag;
	}
}
