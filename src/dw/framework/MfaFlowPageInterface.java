package dw.framework;

public interface MfaFlowPageInterface {
	public void commit();
	public void deauthorize();
	public void abort();
	public void cancel();
    public int getRetryCount();
    public void incrementRetryCount();
}
