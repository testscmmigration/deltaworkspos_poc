/*
 * MfaAuthorization.java
 *
 * Copyright (c) 2004-2014 MoneyGram International
 */
package dw.framework;

import java.util.Locale;

import com.moneygram.agentconnect.OpenOTPLoginRequest;
import com.moneygram.agentconnect.OpenOTPLoginResponse;
import com.moneygram.agentconnect.RegisterHardTokenRequest;
import com.moneygram.agentconnect.RegisterHardTokenResponse;

import dw.comm.MessageFacade;
import dw.comm.MessageFacadeError;
import dw.comm.MessageFacadeParam;
import dw.comm.MessageLogic;
import dw.dialogs.Dialogs;
import dw.dialogs.MfaHardTokenDialog;
import dw.io.DiagLog;
import dw.io.diag.MustTransmitDiagMsg;
import dw.main.DeltaworksStartup;
import dw.main.MainPanelDelegate;
import dw.mgsend.MoneyGramClientTransaction;
import dw.model.XmlUtil;
import dw.model.adapters.CountryInfo;
import dw.profile.Profile;
import dw.profile.ProfileItem;
import dw.profile.UnitProfile;
import dw.profile.UnitProfileInterface;
import dw.utility.Debug;

public class MfaAuthorization {

	private static final int NUMBER_RETRY_ATTEMPTS = 3;
	private static boolean doRegistration = false;
	private static int retryCount = 0;
	private boolean regOnly;
	
    private AuthorizationToken authorizationToken;
    private int status;
    
	private class MfaCommComplete implements CommCompleteInterface {

    	private boolean successful;
    	private MfaFlowPageInterface flowPage;   	

		public MfaCommComplete(MfaFlowPageInterface flowPage) {
    		this.flowPage = flowPage;
    	}
    	
    	public void setSuccessful(boolean successful) {
    		this.successful = successful;
    	}
    	
    	public boolean isSuccessful() {
    		return successful;
    	}
    	
		@Override
		public void commComplete(int commTag, Object returnValue) {
						
			switch (commTag) {
				case MoneyGramClientTransaction.REGISTER_HARD_TOKEN:
					if ((returnValue instanceof Boolean) && (returnValue.equals(Boolean.FALSE))) {
						 if (getStatus() == ClientTransaction.NEEDS_RECOMMIT) {
			            	 doMfaAuthorization(flowPage, false, regOnly);
						 }
						 return;
					}else {
						if (isSuccessful()) {							
							ProfileItem item = UnitProfile.getInstance().find(UnitProfileInterface.MFA_TOKEN);
							String temp = item.getMode();
							/*
							 * if this is a subagent, the mode will be V (view) so
							 * temporarily change it to S (store) so it will get
							 * updated in MGT
							 */
							item.setMode("S");
					        updateMfaProfileItem(flowPage, getProfileItemXML(item));
							item.setMode(temp);
						} else {
							deauthorize(flowPage);
						}
					}
					return;
			
				case MoneyGramClientTransaction.PROFILE_CHANGE:
					if ((returnValue instanceof Boolean) && (returnValue.equals(Boolean.FALSE))) {
						 flowPage.cancel();
						 return;
					}
					if (!regOnly ){
						openOTPLogin(flowPage, authorizationToken.getValue());						
					}
					return;
			
				case MoneyGramClientTransaction.OPEN_OTP_LOGIN:
				if ((returnValue instanceof Boolean) && (returnValue.equals(Boolean.FALSE))) {
					if (getStatus() == ClientTransaction.NEEDS_RECOMMIT) {
						doMfaAuthorization(flowPage, false, regOnly);
					}
					return;
				} else {
					if (isSuccessful()) {						
						flowPage.commit();						
					}else if (doRegistration && (retryCount < NUMBER_RETRY_ATTEMPTS && getStatus() != ClientTransaction.CANCELED)) {
							retryCount = retryCount++;
							Debug.println("**Retrying login since we had just done a registration ");
							try {
								Thread.sleep(2000);
							} catch (InterruptedException e) {
								Debug.println("commComplete(): received InterruptedException: " + e);
							}
							openOTPLogin(flowPage, authorizationToken.getValue());
						} else {
							deauthorize(flowPage);
						}
					}
				}
					return;
			}
		}    
	
	public void doMfaAuthorization(MfaFlowPageInterface flowPage) {
		doMfaAuthorization(flowPage, true, false);
	}
	
    public void doMfaAuthorization(MfaFlowPageInterface flowPage,boolean doChangeRetryCount, boolean regOnly) {
    	
    	Debug.println("MFA Enabled");
    	this.regOnly = regOnly;
    	
    	// Loop until a valid token is entered or the retry attempt count is exceeded.
    	
        boolean firstFlag = true;
    	loop: while (true) {
    		
    		// Check if the retry attempt count has been exceeded.
    		
        	if (flowPage.getRetryCount() >= NUMBER_RETRY_ATTEMPTS) {
        		Debug.println("Retry Count :" + flowPage.getRetryCount());
				deauthorize(flowPage);
    			return;
        	}
        	
        	// Display the dialog to enter a token.    		
            MfaHardTokenDialog mfaDialog = new MfaHardTokenDialog(firstFlag, regOnly);
       		mfaDialog.show();
       		firstFlag = false;
       	       		
       		// If the user clicks the cancel button, cancel the transaction.            
            if (mfaDialog.isCanceled()) {
            	flowPage.cancel();
            	return;
            }
            
            if(doChangeRetryCount) {
            	flowPage.incrementRetryCount();
            }
    		
    		// Create an object for the token and get the tokenID from the profile.
        	authorizationToken = new YubicoToken(mfaDialog.getToken().toUpperCase(Locale.US), UnitProfile.getInstance().isDemoMode());
    		String profileTokenId = UnitProfile.getInstance().getMfaTokenId();
    		Debug.println("Profile Token ID = " + profileTokenId);
    		
    		// Check the validity of the token.
    		
        	int status = authorizationToken.checkToken(profileTokenId);
        	switch (status) {
        	
        		// If the token is good, then register the token if the token generator has not been registered, or check authorization if 
        		// the token generator has been registered.
        	
        		case AuthorizationToken.VALID_TOKEN:
        			if (UnitProfile.getInstance().isTrainingMode()) {
        				flowPage.commit();
        			} else if ((profileTokenId == null) || (profileTokenId.length() == 0)) {
                		Debug.println("Valid Token entered. Attempting to register token Id");
                		doRegistration = true;                		
        				registerHardToken(flowPage, authorizationToken.getTokenId());        				
        			} else {
                		Debug.println("Valid Token entered. Attempting to log into OpenOTP");
                		doRegistration = false;
        				openOTPLogin(flowPage, authorizationToken.getValue()); // Success - Call Success Page         
        			}   
        			return;
	        	case AuthorizationToken.DIFFERENT_TOKEN_GENERATOR:
            		Debug.println("Different Token Generator - Abort transaction");
    				Dialogs.showError("dialogs/DialogMfaDifferentTokenGenerator.xml");
            		flowPage.cancel();
            		return;
	        	case AuthorizationToken.INVALID_NUMBER_CHARACTERS:
	        		Debug.println("if not Successful RegOnly :" + regOnly + "get Retry Count: "+ flowPage.getRetryCount());
					if(regOnly && flowPage.getRetryCount() >= NUMBER_RETRY_ATTEMPTS){
						Dialogs.showError("dialogs/DialogMfaErrorMessage.xml");	
						flowPage.commit();
						setStatus(ClientTransaction.FAILED);
					}
					else if (regOnly && flowPage.getRetryCount() < NUMBER_RETRY_ATTEMPTS){
						Dialogs.showError("dialogs/DialogMfaRetryErrorMessage.xml");
					}
            		Debug.println("Invalid number of characters in token - " + authorizationToken.getValue().length());
            		continue loop;
	        	case AuthorizationToken.INVALID_TOKEN_CHARACTERS:
	        		Debug.println("if not Successful RegOnly - INVALID_TOKEN_CHARACTERS:" + regOnly + "get Retry Count: "+ flowPage.getRetryCount());
	        		if(regOnly && flowPage.getRetryCount() >= NUMBER_RETRY_ATTEMPTS){
						Dialogs.showError("dialogs/DialogMfaErrorMessage.xml");	
						flowPage.commit();
						setStatus(ClientTransaction.FAILED);
					}
					else if (regOnly && flowPage.getRetryCount() < NUMBER_RETRY_ATTEMPTS){
						Dialogs.showError("dialogs/DialogMfaRetryErrorMessage.xml");
					}
            		Debug.println("Invalid characters in token - Deauthorize DeltaWorks");            		
            		deauthorize(flowPage);
            		return;
	        	case AuthorizationToken.INVALID_TOKEN_GENERATOR:
            		Debug.println("Invalid token generator - Token Id = " + authorizationToken.getTokenId());
            		deauthorize(flowPage); 
            		return;
        	}
    	}
	}
    
    private void registerHardToken(final MfaFlowPageInterface flowPage, final String token) {
		final MfaCommComplete cci = new MfaCommComplete(flowPage);
		
		MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {
				RegisterHardTokenRequest request = new RegisterHardTokenRequest();
				RegisterHardTokenResponse response = null;
				
				long longValue = authorizationToken.toLongValue(token);
				request.setHardToken(String.valueOf(longValue));
				
				try {
					response = MessageFacade.getInstance().registerHardToken(request);
				} catch (Exception e) {
					Debug.println("Exception: " + e.getMessage());
					
					// Handle an exception from MessageFacade call:
					//    If instance of MessageFacadeError
					//       ErrorCode = 1: commit
					//		 ErrorCode = 49: abort
					//     All other exceptions or ErrorCode values: deauthorize
					
					if (e instanceof MessageFacadeError) {
						MessageFacadeError mfe = (MessageFacadeError) e;
						if (mfe.hasErrorCode(950) || mfe.hasErrorCode(951)) {
							Debug.println("Call to OpenOTP or webADM failed. Transaction will be committed without authorization.");
								Debug.println("Call to OpenOTP or webADM failed. Transaction will be committed without authorization.");
								if (regOnly) {
									Dialogs.showWarning("dialogs/DialogMfaOtpNotAvailable.xml");
								}
								flowPage.commit();
								setStatus(ClientTransaction.FAILED);
								return Boolean.FALSE;
							}
						else if (mfe.hasErrorCode(MessageFacadeParam.OTHER_ERROR)) {
							Debug.println("Agent Connect call failed. Transaction will be aborted.");
							//flowPage.abort();
							setStatus(ClientTransaction.NEEDS_RECOMMIT);
							return Boolean.FALSE;
						} else if (mfe.hasErrorCode(MessageFacadeParam.USER_CANCEL)) {
							flowPage.cancel();
							setStatus(ClientTransaction.CANCELED);
							return Boolean.FALSE;
						}
						
						Debug.println("Registration failed. Transaction will be aborted and DeltaWorks will be deauthorized.");
						deauthorize(flowPage);
						return Boolean.FALSE;
					}
					setStatus(ClientTransaction.NEEDS_RECOMMIT);
					return Boolean.FALSE;
				}
					
				cci.setSuccessful(response.getPayload().getValue().isRegistrationSuccessful());
				
				if (cci.isSuccessful()) {
					Debug.println("Registration Successful");
					String tokenId = token.substring(0, Math.min(token.length(), 12));
					UnitProfile.getInstance().set(UnitProfileInterface.MFA_TOKEN, tokenId, "S", true);
					Debug.println("if Successful RegOnly :" + regOnly);
					if(regOnly)							
						Dialogs.showInformation("dialogs/DialogMfaRegistrationSuccess.xml");
					flowPage.commit();
					setStatus(ClientTransaction.COMPLETED);
					Debug.println("Setting profile MFA token Id = " + tokenId);
			        try {
			            UnitProfile.getInstance().changesApplied();
			            UnitProfile.getInstance().saveChanges();
			        } catch (java.io.IOException e) {
			            Debug.println("saveProfileChanges(): ignoring unrecoverable exception: " + e);
			        }
			    }				
				return Boolean.TRUE;
			}
		};
		MessageFacade.run(logic, cci, MoneyGramClientTransaction.REGISTER_HARD_TOKEN, Boolean.FALSE);
	}
    
    private void openOTPLogin(final MfaFlowPageInterface flowPage, final String token) {
    	
    	Debug.println("Logging into OpenOTP...");
		final MfaCommComplete cci = new MfaCommComplete(flowPage);

		MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {
				OpenOTPLoginRequest request = new OpenOTPLoginRequest();
				OpenOTPLoginResponse response = null;

				request.setPassword(token);

				try {
					response = MessageFacade.getInstance().openOTPLogin(request);
				} catch (Exception e) {
					Debug.println("Exception: " + e.getMessage());
					
					// Handle an exception from MessageFacade call:
					// If instance of MessageFacadeError
					// ErrorCode = 2: commit
					// ErrorCode = 49: abort
					// All other exceptions or ErrorCode values: deauthorize

					if (e instanceof MessageFacadeError) {
						MessageFacadeError mfe = (MessageFacadeError) e;
						if (mfe.hasErrorCode(950) || mfe.hasErrorCode(951)) {
							Debug.println("Call to OpenOTP or webADM failed. Transaction will be committed without authorization.");
							flowPage.commit();
							setStatus(ClientTransaction.FAILED);
							return Boolean.FALSE;
						} else if (mfe.hasErrorCode(MessageFacadeParam.OTHER_ERROR)) {
							Debug.println("Agent Connect call failed. Transaction will be aborted.");
							//flowPage.abort();
							setStatus(ClientTransaction.NEEDS_RECOMMIT);
							return Boolean.FALSE;
						} else if (mfe.hasErrorCode(MessageFacadeParam.USER_CANCEL)) {
							flowPage.cancel();
							setStatus(ClientTransaction.CANCELED);
							return Boolean.FALSE;
						}
					}
					Debug.println("OpenOTP login failed. Transaction will be aborted and DeltaWorks will be deauthorized.");
					deauthorize(flowPage);
					return Boolean.FALSE;
				}
				cci.setSuccessful(response.getPayload().getValue().isLoginSuccessful());
				
				return Boolean.TRUE;
			}
		};
		MessageFacade.run(logic, cci, MoneyGramClientTransaction.OPEN_OTP_LOGIN, Boolean.FALSE);
	}
    
    private void updateMfaProfileItem(final MfaFlowPageInterface flowPage, final String token) {
    	
		final MfaCommComplete cci = new MfaCommComplete(flowPage);

		MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {
				
				try {
					Boolean success = MessageFacade.getInstance().updateAgentProfile(token);
	                if (success.equals(Boolean.FALSE)) {
	                	Debug.println("updateMfaProfileItem - FAILED");
	                }
				} catch (Exception e) {
					Debug.println("Exception: " + e.getMessage());
					
					return Boolean.FALSE;
				}
				cci.setSuccessful(true);

				return Boolean.TRUE;
			}
		};
		MessageFacade.run(logic, cci, MoneyGramClientTransaction.PROFILE_CHANGE, Boolean.FALSE);
	}
    
    private void deauthorize(MfaFlowPageInterface flowPage) {
    	DeltaworksStartup.getInstance().setConnected(false);
		UnitProfile.getInstance().setLastCheckInTime(UnitProfileInterface.MFA_DEAUTHORIZATION);
		MainPanelDelegate.setDeauthMessage(true);
		DiagLog.getInstance().writeMustTransmit(MustTransmitDiagMsg.SECURITY_BREACH);
		Thread thread = new Thread() {
			@Override
			public void run() {
				DeltaworksStartup.getInstance().checkInWithHost(false, CountryInfo.MANUAL_TRANSMIT_RETERIVAL);
			}
		};
		thread.start();
		if(regOnly && retryCount >= flowPage.getRetryCount())	{
			Debug.println("Retry Count :" + flowPage.getRetryCount());
			Dialogs.showError("dialogs/DialogMfaDeauthorization.xml");
			setStatus(ClientTransaction.FAILED);
			flowPage.deauthorize();						
		}
		else{
			Dialogs.showError("dialogs/DialogMfaDeauthorization.xml");
		}
		flowPage.deauthorize();
	}

    /**
     * Builds the profile change XML for a single profile item.
     * @param profileItem A ProfileItem object.
     * @return A String representing this Profile's changed item and other 
     * important attributes necessary for updating the profile in the 
     * middleware.
     */ 
    private String getProfileItemXML(ProfileItem profileItem) {
        
        StringBuffer buf = new StringBuffer();
        Profile profile = UnitProfile.getInstance().getProfileInstance();
        
        buf.append("<");	
        buf.append(profile.getTag());
        if (profile.getID() != 0) {
            // Note: The value of id in the ProfileItem class ought to be
            // initialized to some well known value so it can be tested for 
            // existence in the XML message.
            buf.append(" id=\""); 
            buf.append(profile.getID());
            buf.append("\""); 
        }
        
        if (profile.getMode() != null && !(profile.getMode().equals(""))) {	
            buf.append(" mode=\""); 
            buf.append(profile.getMode());
            buf.append("\""); 
        }
        
        buf.append(">\n"); 

        buf.append("\t<ITEM tag=\""); 
        buf.append(profileItem.getTag());
        buf.append("\""); 
        
        // Now determine what else goes on into the ITEM element
        if (profileItem.getID() != 0) {
            // Note: The value of id in the ProfileItem class ought to be
            // initialized to some well known value so it can be tested for 
            // existence in the XML message.
            buf.append(" id=\""); 
            buf.append(profileItem.getID());
            buf.append("\""); 
        }
        
        if (profileItem.getStatus() != null && 
                !(profileItem.getStatus().equals(""))) {	
            buf.append(" status=\""); 
            buf.append(profileItem.getStatus());
            buf.append("\""); 
        }
        
        if (profileItem.getCurr() != null &&
                !(profileItem.getCurr().equals(""))) {	
            buf.append(" curr=\""); 
            buf.append(profileItem.getCurr());
            buf.append("\""); 
        }
        
        if (profileItem.getMode() != null &&
                !(profileItem.getMode().equals(""))) {	
            buf.append(" mode=\""); 
            buf.append(profileItem.getMode());
            buf.append("\""); 
        }
        
        buf.append(">");	
        XmlUtil.sanitizeXml(buf, profileItem.stringValue());
        buf.append("</ITEM>\n"); 
        
        buf.append("</");	
        buf.append(profile.getTag());
        buf.append(">\n"); 
        
        return buf.toString();
    } // END-METHOD

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
}
