package dw.framework;

/**
 * This interface is used by a transaction to perform a callback to the calling
 *   GUI page. This is necessary so the GUI caller can put up and dialog and
 *   not block on the call to the middleware.
 */
public interface CommCompleteInterface {
    // callback to the gui to indicate we are done with our host comm. Use a
    // generic object for the return val, because transaction methods all have
    // different types of return values. Caller will have to cast it
    public void commComplete(int commTag, Object returnValue);
}
