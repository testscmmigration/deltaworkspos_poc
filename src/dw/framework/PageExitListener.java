package dw.framework;

/**
 * PageExitListener is the callback interface for a page contained in a
 *   PageFlowContainer to notify the container that it is finished.
 */
public interface PageExitListener {
   // all possible directions that the page can exit in
    public static final int BACK = 0;
    public static final int NEXT = 1;
    public static final int CANCELED = 2;
    public static final int COMPLETED = 3;
    public static final int FAILED = 4;
    public static final int EXPERT = 5;
    public static final int WIZARD = 6;
    public static final int TIMEOUT = 7;
    public static final int EDIT_TRANSACTION = 10;

    public static final int DONOTHING = 99;

    public void exit(int direction);
} 
