package dw.framework;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.moneygram.agentconnect.Response;

import dw.accountdepositpartners.AccountDepositPartnersTransaction;
import dw.comm.AgentConnect;
import dw.dwgui.DWTable;
import dw.dwgui.DWTable.DWTableListener;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.i18n.Messages;
import dw.mgreceive.MoneyGramReceiveTransaction;
import dw.mgsend.MGSHistoryPage;
import dw.mgsend.MoneyGramClientTransaction;
import dw.mgsend.MoneyGramClientTransaction.ProfileStatus;
import dw.mgsend.MoneyGramSendTransaction;
import dw.model.adapters.ConsumerProfile;

public class ProfileSelectionWizardPage extends FlowPage implements MGSHistoryPage, DWTextPaneListener, DWTableListener {
	private static final long serialVersionUID = 1L;

	private static final int MIN_SCREEN_WIDTH = 450;
	
	private DWTable table = null;
	private ListSelectionModel listSelectionModel;
	private DefaultTableModel tableModel = null;
    private JScrollPane scrollPane;
    private JPanel panel;
    private DWTextPane text1;
	private MoneyGramClientTransaction transaction;
	private List<ConsumerProfile> profileList;
	private int lastSelectedItem = 0;
	private JButton nextButton;
	private JButton backButton;
	private JButton cancelButton;

	public ProfileSelectionWizardPage(MoneyGramSendTransaction transaction, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/ProfileSelectionWizardPage.xml", name, pageCode, buttons);
		this.transaction = transaction;
		init();
	}

	public ProfileSelectionWizardPage(AccountDepositPartnersTransaction transaction, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/ProfileSelectionWizardPage.xml", name, pageCode, buttons);
		this.transaction = transaction;
		init();
	}
	
	public ProfileSelectionWizardPage(MoneyGramReceiveTransaction transaction, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/ProfileSelectionWizardPage.xml", name, pageCode, buttons);
		this.transaction = transaction;
		init();
	}
	
	private void init() {
		table = (DWTable) this.getComponent("table");
		scrollPane = (JScrollPane) this.getComponent("matchingCustomerScrollpane");
		panel = (JPanel) getComponent("panel");
		text1 = (DWTextPane) getComponent("text1");
		String text = Messages.getString("ProfileSelectionWizardPage.Text1");
		if (transaction instanceof MoneyGramSendTransaction) {
			text += Messages.getString("ProfileSelectionWizardPage.Text2");
		}
		text1.setText(text);
		nextButton = flowButtons.getButton("next"); 
		backButton = flowButtons.getButton("back"); 
		cancelButton = flowButtons.getButton("cancel"); 
		setupTable();
		table.addListener(this, this);
	}

	@Override
	public void dwTableResized() {
		dwTextPaneResized();

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.resizeTable();
			} 
		});
	}

	@Override
	public void dwTextPaneResized() {
		int tableWidth = table.getTable().getPreferredSize().width;
		int screenWidth = scrollPane.getVisibleRect().width - 20;
		int actualWidth = Math.min(Math.max(tableWidth, MIN_SCREEN_WIDTH), screenWidth);
		
		text1.setWidth(actualWidth);
	}

	@Override
	public JTable getTable() {
		return null;
	}

	@Override
	public DefaultTableModel getTableModel() {
		return null;
	}

	private void setupTable() {
		
		String[] headers = {
				Messages.getString("GeneralProfileOptions.1411"),
				Messages.getString("MGSWizardPage2.3"),
				Messages.getString("ProfileSelectionWizardPage.PhoneNumber"),
				Messages.getString("MGSWizardPage2a.BirthDate"),
				""
			}; 
		tableModel = new DefaultTableModel(headers, 0) {
			private static final long serialVersionUID = 1L;
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		
		table.getTable().setModel(tableModel);
        listSelectionModel = table.getTable().getSelectionModel();
        
		TableColumnModel model = table.getTable().getColumnModel();
		table.getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		table.getTable().getTableHeader().setReorderingAllowed(false);
		table.getTable().removeColumn(table.getTable().getColumnModel().getColumn(4));

		TableColumn col = model.getColumn(0);
    	col.setCellRenderer(table.new DWTableCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));

		col = model.getColumn(1);
    	col.setCellRenderer(table.new DWTableCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));

		col = model.getColumn(2);
    	col.setCellRenderer(table.new DWTableCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));
		
		col = model.getColumn(3);
    	col.setCellRenderer(table.new DWTableCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));
		
		KeyListener kl = new KeyAdapter() { 
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					e.consume();
					nextButton.doClick();
				}
				if (e.getKeyCode() == KeyEvent.VK_TAB) {
					e.consume();
					backButton.requestFocus();
				}
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					e.consume();
					cancelButton.doClick();
				}
				if (e.getKeyCode() == KeyEvent.VK_F2) {
					e.consume();
					transaction.f2SAR();
				}
			}
		};
		
		table.getTable().addKeyListener(kl);
	}
	
	private void resetButtons() {
		flowButtons.reset();
		flowButtons.setVisible("expert", false);
	}
	
	@Override
	public void start(int direction) {
		resetButtons();
		
		if (direction == PageExitListener.NEXT) {
			table.getTable().getSelectionModel().clearSelection();
			clearComponent(table.getTable());
			
			tableModel.addRow(new String[] {Messages.getString("ClientTransaction.1922"), "", "", ""});
			profileList = transaction.getSearchProfiles();
			
			for (ConsumerProfile profile : profileList) {
				String name = profile.getName();
				String address = profile.getAddress();
				String phoneNumber = profile.getPhoneNumber();
				String birthDate = profile.getBirthDate();
				tableModel.addRow(new String[] {name, address, phoneNumber, birthDate});
			}
			
			table.initializeTable(scrollPane, panel, MIN_SCREEN_WIDTH, 10);
			table.setColumnStaticWidth(2);
			table.setColumnStaticWidth(3);
			dwTableResized();
			table.getTable().getSelectionModel().setSelectionInterval(0, 0);
		} else {
			table.getTable().getSelectionModel().setSelectionInterval(lastSelectedItem, lastSelectedItem);
		}
		table.getTable().requestFocus();
	}
	
	@Override
	public void finish(int direction) {
		if (direction == PageExitListener.NEXT) {
			int selectedRow = table.getTable().getSelectedRow();
			lastSelectedItem = selectedRow;
			
			if (selectedRow > 0) {
				final ConsumerProfile profile = profileList.get(selectedRow - 1);
				String mgiSessionID = transaction instanceof MoneyGramReceiveTransaction && transaction.getTransactionLookupResponsePayload() != null ? transaction.getTransactionLookupResponsePayload().getMgiSessionID() : null;
				int rule = transaction instanceof MoneyGramSendTransaction ? AgentConnect.ALWAYS_LIVE : AgentConnect.AUTOMATIC;
				transaction.getConsumerProfile(profile, mgiSessionID, rule, null, new CommCompleteInterface() {
					@Override
					public void commComplete(int commTag, Object returnValue) {
						boolean b = ((returnValue != null) && (returnValue instanceof Boolean)) ? ((Boolean) returnValue) : false;
						if (b) {
							Response response = transaction.getConsumerProfileResponse();
							if (response != null) {
								transaction.setProfileObject(profile);
								transaction.setProfileStatus(ProfileStatus.USE_CURRENT_PROFILE);
								PageNotification.notifyExitListeners(ProfileSelectionWizardPage.this, PageExitListener.NEXT);
							}
						} else {
							resetButtons();
							PageNotification.notifyExitListeners(ProfileSelectionWizardPage.this, PageExitListener.DONOTHING);
						}
					}
				});
				return;
			} else {
				boolean f = transaction.createOrUpdateConsumerProfile(null, ProfileStatus.CREATE_PROFILE);
				if (f) {
					PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
				} else {
					PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
				}
			}
		} else {
			PageNotification.notifyExitListeners(this, direction);
		}
	}
}
