package dw.framework;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import javax.swing.JComponent;

import com.moneygram.agentconnect.ComplianceTransactionRequest;
import com.moneygram.agentconnect.CustomerComplianceInfo;
import com.moneygram.agentconnect.DwPasswordRequest;
import com.moneygram.agentconnect.DwPasswordResponse;
import com.moneygram.agentconnect.MoneyOrderVoidReasonCodeType;

import dw.comm.MessageFacade;
import dw.comm.MessageFacadeParam;
import dw.comm.MessageLogic;
import dw.dialogs.ChangePasswordDialog;
import dw.dialogs.ComplianceDialog;
import dw.dialogs.Dialogs;
import dw.dialogs.NamePasswordDialog;
import dw.dispenser.DispenserInterface;
import dw.i18n.Messages;
import dw.io.CCILog;
import dw.io.DiagLog;
import dw.io.diag.SecurityDiagMsg;
import dw.main.DeltaworksMainPanel;
import dw.main.MainPanelDelegate;
import dw.mgreceive.MoneyGramReceiveTransaction;
import dw.model.adapters.CountryInfo;
import dw.printing.TEPrinter;
import dw.profile.Profile;
import dw.profile.ProfileItem;
import dw.profile.UnitProfile;
import dw.reprintreceipt.ReprintData;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.ErrorManager;
import dw.utility.TimeUtility;

/**
 * ClientTransaction
 * Contains a transceiver to communicate with the Deltaworks servlet. All client
 *   side transactions will extend this.
 */
public abstract class ClientTransaction  implements Serializable{
	private static final long serialVersionUID = 1L;

	// possible client transaction statuses
    
	public static final int IN_PROCESS = 0;
    public static final int COMPLETED = 1;
    public static final int CANCELED = 2;
    public static final int FAILED = 3;
    public static final int LOAD_REQUIRED = 4;
    public static final int VERIFY_REQUIRED = 5;
    public static final int TIMED_OUT = 6;
    public static final int NEEDS_RECOMMIT = 7;
    protected int status = COMPLETED;
    public static final int VALIDATE_PIN = 12;
    public static final int NO_FORMS = 90;
    public static final int COMM_ERROR = 91;
    public static final int CANNOT_COMPLETE_AS_DEFINED = 92;
    public static final int INVALID_USERID = -100;
    
    // Values used for setting the tranReceiptPrefix.
    public static final String RECEIPT_PREFIX_BILL_PAYMENT = "exp";
    public static final String RECEIPT_PREFIX_SEND = "mgs";
    public static final String RECEIPT_PREFIX_RECEIVE = "mgr";
    public static final String RECEIPT_PREFIX_REVERSAL = "mgstr";
    public static final String RECEIPT_PREFIX_MONEY_ORDER = "ofacError";
    public static final String RECEIPT_PREFIX_CARD = "mgc";
    
    public static final int VENDOR_PAYMENT_TYPE =2;
    public static final int MONEY_ORDER_TYPE =1;

    //is this transaction set to wizard? This is for tran, not user
    protected boolean usingWizard = false;
    protected String tranName;     // logical name of the transaction
    protected String tranReceiptPrefix;
    protected long dateTime = 0;
    protected int documentSequence = 0; // GUI's sequence num, which Tandem doesn't want
    protected int documentType = 0;	// 1=MO, 2=VP, 4=MGR, 90=MGS, 91=EP
    protected int documentNumber = 0;  // Tandem's document sequence number
    protected String documentName = "";	
    protected boolean productTransaction = true;
    protected int userID = 0;   // user who is currently logged in for this tran
    protected String userName = null;
    protected int tranStarterUserID = 0;   // user who is currently logged in for super Agent tran
    protected Profile userProfile = null;
    protected int selectedDispenser;
    protected int maxFormsPerSale;
    protected int maxBatchCount;
    protected BigDecimal maxBatchAmount;

    // these values should be set to non-default by subclasses if necessary
    protected int requiredFormCount = 0;
    protected boolean timeoutEnabled = true;
    protected boolean dispenserNeeded = true;
    protected boolean receiptPrinterNeeded = true;
    protected boolean managerApprovalRequired = false;
    protected boolean hostConnectionRequired = false;

    protected String emptyItemText = Messages.getString("ClientTransaction.1919"); 
    protected String[] noneOfAboveArray = {Messages.getString("ClientTransaction.1920"), Messages.getString("ClientTransaction.1921")};  
    protected String noneOfFollowingString = Messages.getString("ClientTransaction.1922"); 
    protected String otherString = Messages.getString("ClientTransaction.1"); 


    protected BigDecimal dailyLimit;
    protected boolean receiveSendConfirmation = false;
    protected boolean receiveBPConfirmation = false;
    protected boolean custAgtReceiptsReceived = false;
    protected boolean custReceiptsReceived = false;
    protected boolean agtReceiptsReceived = false;
    protected boolean autoPrintSendConfirmation = false;
    protected boolean autoPrintBPConfirmation = false;
    protected boolean useAgentPrinter = false;
    public boolean tranLimitOverridden = false;
    protected boolean financialInstitution = false;
    protected String agentBaseCurrency;
    ChangePasswordDialog cDialog =null;
    
    protected ComplianceTransactionRequest complianceTransactionRequestInfo;
    private DwPasswordResponse response = null;

    public static final BigDecimal ZERO = BigDecimal.ZERO;
    private boolean idleTimeoutShort=false;
    private int attempt = 0;
    private int type=0;
    
	//FUTURE add boolean class variable to denote CTR dialog display
    
    /**
      * Show object contents for debugging.
      */
    @Override
	public String toString() {
        return "ClientTransaction {" +	
            "\n    status = " + status +	
            "\n    usingWizard = " + usingWizard +	
            "\n    tranName = " + tranName +    
            "\n    tranReceiptPrefix = " + tranReceiptPrefix +    
            "\n    dateTime = " + dateTime +	
            "\n    documentSequence = " + documentSequence +	
            "\n    documentType = " + documentType +	
            "\n    documentNumber = " + documentNumber +	
            "\n    documentName = " + documentName +	
            "\n    productTransaction = " + productTransaction +	
            "\n    userID = " + userID +	
            "\n    userName = " + userName +	
            "\n} ClientTransaction\n";	
    }

    /**
     * Get rid of the comm dialog and return a value to the caller.
     */
    protected void finishComm(int commTag, CommCompleteInterface cci, Object returnVal) {
        if (cci != null)
            cci.commComplete(commTag, returnVal);
    }

    /**
     * Creates a new client transaction with the specified transaction name.
     * Before calling this method, invoke UnitProfile.getUPInstance().load(). The Deltaworks
     * document sequence number is used to load product-specfic profile items.
     * @param tranName logical name of this transaction
     * @param tranNameReceiptPrefix receipt prefix of this transaction.
     * @param docSeq Deltaworks document sequence number for this product.
     * @throws java.util.NoSuchElementException if a product profile can
     * not be loaded for the given document sequence number, or if key items
     * in the profile are not found.
     */
    public ClientTransaction(String tranName, String tranReceiptPrefix, int docSeq) {
        this(tranName, tranReceiptPrefix);
        documentSequence = docSeq;
        if (docSeq < 0)
            productTransaction = false;
        else {
            getProductProfileValues();
            productTransaction = true;
        }
        agentBaseCurrency = CountryInfo.getAgentCountry().getBaseReceiveCurrency();
    }

    /**
     * Creates a new client transaction with the specified transaction name.
     * Before calling this method, invoke UnitProfile.getUPInstance().load(). c profile items.
     * @param tranName logical name of this transaction
     * @param tranNameReceiptPrefix receipt prefix of this transaction.
     * @throws java.util.NoSuchElementException if key items in the profile
     *   are not found.
     */
    public ClientTransaction(String tranName, String tranReceiptPrefix) {
        this.tranName = tranName;
        this.tranReceiptPrefix = tranReceiptPrefix;
        dateTime = TimeUtility.currentTimeMillis();
        getProfileValues();
        productTransaction = false;
		complianceTransactionRequestInfo = new ComplianceTransactionRequest();
		complianceTransactionRequestInfo.setCci(new CustomerComplianceInfo());
        agentBaseCurrency = CountryInfo.getAgentCountry().getBaseReceiveCurrency();
    }

    public List<String> currencyForTransferCheck(){
    	return UnitProfile.getInstance().getProfileInstance().findList(
        "RECEIVE_CURRENCY");
    }

    public List<String> sendCurrencyList(){
    	return UnitProfile.getInstance().getProfileInstance().findList(
        "AGENT_CURRENCY");
    }

    public String agentBaseCurrency(){
    	return agentBaseCurrency;
    }

    /**
     * Does this transaction need receipts received rather than generated.
     */
    public boolean isReceiptReceived() {
        return (custAgtReceiptsReceived ||custReceiptsReceived ||agtReceiptsReceived);
    }

    public boolean isReceiveAgentAndCustomerReceipts() {
        return custAgtReceiptsReceived;
    }
    
    public boolean isReceiveCustomerReceipt() {
        return custReceiptsReceived;
    }
    
    public boolean isReceiveAgentReceipt() {
        return agtReceiptsReceived;
    }

    /**
     * Does this MGS transaction need an auto confirmation slip.
     */
    
    public boolean isReceiveSendConfirmation() {
    	return receiveSendConfirmation;
    }
    
    public boolean isAutoPrintSendConfirmation() {
        return autoPrintSendConfirmation;
    }
     
    public boolean isReceiveBPConfirmation() {
    	return receiveBPConfirmation;
    }
    
    /**
     * Does this EP transaction need an auto confirmation slip.
     */
    public boolean isAutoPrintBPConfirmation() {
        return autoPrintBPConfirmation;
    }
    
    /**
     * Does this transaction use agent printer.
     */
    public boolean isUseAgentPrinter() {
        return useAgentPrinter;
    }
    
    /**
     * Does this transaction need to be time-limited? Default is yes.
     */
    public boolean isTimeoutEnabled() {
        return timeoutEnabled;
    }

    public int getMaxFormsPerSale() {
        return maxFormsPerSale;
    }

    /**
     * Does this transaction need to use the dispenser?
     */
    public boolean isDispenserNeeded() {
        return dispenserNeeded;
    }

    /**
     * Does this transaction need to use the receipt printer?
     */
    public boolean isReceiptPrinterNeeded() {
    	String value = UnitProfile.getInstance().get("ENABLE_SERVER_SIDE_RECEIPTS", "N");
    	return ((value.equals("Y") || value.equals("C") || value.equals("A")) && receiptPrinterNeeded);
    }

    public void setMaxBatchAmount(BigDecimal maxBatchAmt) {
    	maxBatchAmount = maxBatchAmt;
    }

    public BigDecimal getMaxBatchAmount() {
        return maxBatchAmount;
    }

    public int getMaxBatchCount() {
        return maxBatchCount;
    }

    protected Profile getProductProfile() {
        Profile productProfile = null;
        try {
            productProfile = (Profile) UnitProfile.getInstance().getProfileInstance().find(
                                              "PRODUCT", documentSequence);	
        }
        catch (NoSuchElementException e) {
            Debug.println("Could not find product profile for doc seq : " +	
                                       documentSequence);
        }
        return productProfile;
    }

    protected Profile getDispenserProfile() {
        Profile profile = null;
        try {
            profile = (Profile) UnitProfile.getInstance().getProfileInstance().find(
                                           "DISPENSER", selectedDispenser);	
        }
        catch (NoSuchElementException e) {
            Debug.println("Could not find dispenser profile...");	
        }
        return profile;
    }

    private void getProductProfileValues() {
    	
        Profile productProfile = getProductProfile();
        documentType = productProfile.find("PRODUCT_TYPE").intValue();	
        if (documentType == 1 || documentType == 2) {  // grab the values for MO and VP
            maxBatchAmount = productProfile.findDecimal("MAX_BATCH_AMOUNT");	
            maxBatchCount = productProfile.findInt("MAX_BATCH_COUNT");	
        }
        if (documentType == 96 || documentType == 97) {  // grab the values for Card Reload and Card Purchase
        	maxBatchCount = productProfile.findInt("MAX_BATCH_COUNT");	
        }
        if (documentType == 99) {  // grab the values for Card General
        	maxBatchAmount = productProfile.findDecimal("MAX_BATCH_AMOUNT");	
        }
        if (documentType <= 4) {
            documentNumber =
                productProfile.findInt("DOCUMENT_NUMBER");	
            documentName =
                productProfile.findString("DOCUMENT_NAME");	
            selectedDispenser =
                productProfile.findInt("DISPENSER_NUMBER");	
            maxFormsPerSale =
                getDispenserProfile().findInt("MAX_FORMS_PER_SALE");	
        }
    }

    /**
     * Return the minumum number of forms that must be in the dispenser to be
     *   able to start this transaction. Default is 0. (no forms needed).
     */
    public int getRequiredFormCount() {
        return requiredFormCount;
    }
    private void getProfileValues() {
    	Profile prof = UnitProfile.getInstance().getProfileInstance(); 
      	dailyLimit = prof.findDecimal("DAILY_LIMIT");	
  	
        financialInstitution = prof.find("FINANCIAL_INSTITUTION").booleanValue();	

        String serverSideReceipts = UnitProfile.getInstance().get("ENABLE_SERVER_SIDE_RECEIPTS", "Y");
        custAgtReceiptsReceived = serverSideReceipts.equals("Y");
        custReceiptsReceived = serverSideReceipts.equals("C");
        agtReceiptsReceived = serverSideReceipts.equals("A");
		
        receiveSendConfirmation = UnitProfile.getInstance().get("MG_ENABLE_PRE_RECEIPTS", "Y").equals("Y");
        autoPrintSendConfirmation = UnitProfile.getInstance().get("PRINT_CONFIRMATION_SLIP_MG", "Y").equals("Y");
        
        receiveBPConfirmation = UnitProfile.getInstance().get("ENABLE_PREPAYMENT_DISC_BP", "Y").equals("Y");
        autoPrintBPConfirmation = UnitProfile.getInstance().get("PRINT_CONFIRMATION_SLIP_EP", "Y").equals("Y");
        
        useAgentPrinter = UnitProfile.getInstance().get("USE_AGENT_PRINTER", "Y").equals("Y");
    }

    public BigDecimal getMaxTransactionAmount() {
        return getMaxTransactionAmount(agentBaseCurrency());
    }

	public BigDecimal getMaxTransactionAmount(String currency) {
		return UnitProfile.getInstance().getProfileInstance()
				.findCurrencyDecimalValue("HDV_LIMIT", currency);
	}

    public BigDecimal getMaxItemAmount() {
        return getMaxItemAmount(agentBaseCurrency());
    }

    public BigDecimal getMaxItemAmount(String currency) {
        if (documentSequence != 9 && documentSequence != 99) {
        return getProductProfile().findCurrencyDecimalValue(
        		"MAX_AMOUNT_PER_ITEM", currency);
        }
        else {
        	return MoneyGramReceiveTransaction.MAX_MO;
        }
    }

    public BigDecimal getDailyLimit() {
        return dailyLimit;
    }

    public boolean isFinancialInstitution() {
        return financialInstitution;
    }

    /** NOTE:  doesn't handle expired passwords correctly */
    public static boolean isManager(String userName, String password) {
        int managerID = getUserID(userName, password, true);
        return (managerID > 0);
    }

    public static boolean isManager(int userID) {
        Profile userProfile = null;
        try {
            userProfile = (Profile)
                                 UnitProfile.getInstance().getProfileInstance().find("USER", userID);	
        }
        catch (NoSuchElementException e) {
            return false;
        }
        if (userProfile.find("TYPE").stringValue().equals("M"))	 
            return true;
        else
            return false;
    }

    /**
     * Get the user who is currently logged in to this transaction.
     */
    public int getUserID() {
        return userID;
    }
    
    /**
     * Set the user who is currently logged in to this transaction.
     */
    public void setUserID(int id) {
        userID = id;
    }

    /** Checks the supplied user name and password. Returns the matching
      * user ID. If userName is null, the match is done on pin only.
      * Returns 0 if no user account matches. Returns a negative user ID
      * if the user account exists but the pin/password has expired.
      * If managerRequired is true, additionally check that the user account
      * indicates manager privileges; return 0 if it doesn't.
      */
	public static int getUserID(String userName, String password, boolean managerRequired) {
		Profile userProfile = null;

		try {
			userProfile = UnitProfile.getInstance().getProfileInstance().find(userName, password);
		} catch (NoSuchElementException e) {
			return 0; // this is not a valid username/pwd combo
		}

		int userID = userProfile.find("USER_ID").intValue(); 

		if (! managerRequired) {
			return userID;
		}
		String type = userProfile.find("TYPE").stringValue(); 
		if (type.equalsIgnoreCase("M")) { 
			return userID;
		} else {
			return 0; // this is a valid user, but not a manager
		}
	}

    /**
     * Validate given name and password, return true if this is a manager
     * with CTR override privilege. Also set flag in this transaction that
     * the CTR has been overridden.
     */
    public boolean overrideTransactionLimit(String managerName,
            String managerPassword) {

        try {
            Profile mgrProfile = UnitProfile.getInstance().getProfileInstance().find(
                managerName, managerPassword);
            if (isManager(managerName, managerPassword)) {
                try {
                    tranLimitOverridden =
                        mgrProfile.find("HDV_OVERRIDE").booleanValue();	
                }
                catch (NoSuchElementException e) {
                    tranLimitOverridden =
                        mgrProfile.find("CTR_OVERRIDE").booleanValue();	
                }
            }
            else {
                tranLimitOverridden = false;
            }
            
            if (tranLimitOverridden) {
                DiagLog.getInstance().writeSecurity(
                    SecurityDiagMsg.HDV_LIMIT_OVERRIDE,
                    (short) mgrProfile.find("USER_ID").intValue());	
            }
        }
        catch (NoSuchElementException e) {
	    tranLimitOverridden = false;
        }
        return tranLimitOverridden;
    }

    public boolean overrideTransactionLimit(String managerPassword) {
	return overrideTransactionLimit(null, managerPassword);
    }

    /**
     * Set the receipt prefix of this transaction.
     */
    public void setTransactionReceiptPrefix(String name) {
        tranReceiptPrefix = name;
    }

    /**
     * Get the receipt prefix of the transaction.
     */
    public String getTransactionReceiptPrefix() {
        return tranReceiptPrefix;
    }


    /**
     * Set the logical name of this transaction.
     */
    public void setTransactionName(String name) {
        tranName = name;
    }

    /**
     * Get the logical name of the transaction. 
     * @return WARNING:: THE RETURNED STRING IS CONVERTED TO THE LANGUAGE THAT DELTAWORKS IS RUNNING IN. 
     * 
     * 
     */
    public String getTransactionName() {
        return tranName;
    }

    /**
     * Set the status of this transaction.
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * Get the status of this transaction.
     */
    public int getStatus() {
        return status;
    }

    /**
     * Get the receipt format from the profile and determines if a different
     * format (English only, German, French, Spanish) is necessary.  It will also
     * determine if a special receipt format is necessary and set the format to the 
     * special receipt format.
     */

    protected String getReceiptFormat(){
         
        String finalReceiptFormat = "";
        ProfileItem profileFormat = getProductProfileOption("RECEIPT_FORMAT");                    
        String tranReceiptPrefix = getTransactionReceiptPrefix().trim();
           
        if ((profileFormat != null) && (profileFormat.stringValue().startsWith("_"))) {
            Messages.setCurrentReceiptLocale(profileFormat.stringValue());
            if (!isReceiptFormatSpecial("xml/receipts/", tranReceiptPrefix + profileFormat.stringValue())){
                finalReceiptFormat = tranReceiptPrefix;
                if (profileFormat.stringValue().endsWith("intl"))
                {
                	finalReceiptFormat = "mgrintl";           	
                    if (tranReceiptPrefix.equals(ClientTransaction.RECEIPT_PREFIX_REVERSAL)){
                        finalReceiptFormat = "mgstr";        
                    }
                }
            }
            else
                finalReceiptFormat = tranReceiptPrefix + profileFormat.stringValue();

        }
        else if (tranReceiptPrefix.equals(ClientTransaction.RECEIPT_PREFIX_REVERSAL)){
            finalReceiptFormat = "mgstr49";        }
        else if (tranReceiptPrefix.equals(ClientTransaction.RECEIPT_PREFIX_MONEY_ORDER))
            finalReceiptFormat = "ofacFail";            
        else
            finalReceiptFormat = profileFormat.stringValue();
         
        return finalReceiptFormat;
    }
    
    
    /**
     * Determine if the current receipt needed is a special case.  Construct the 
     * xmlFileName from the receipt name passed to this method and see if that xml
     * file exists.
     */    
      
    private boolean isReceiptFormatSpecial(String xmlPath, String receiptName){
        
        String xmlFileName = new StringBuffer(xmlPath.length() + receiptName.length() + 6)
            .append(xmlPath).append(receiptName).append("cu.xml").toString();
        URL fileUrl = ClassLoader.getSystemResource(xmlFileName);
        if (fileUrl != null)
            return true;
        else 
            return false;    
    }

    /**
     * Set whether this transaction is set to use wizard. If this transaction
     *   currently has a user associated, update the user's wizard preference
     *   for this transaction in the profile.
     */
    public void setUsingWizard(boolean using) {
        usingWizard = using;
        if (isPasswordRequired()) {
            ProfileItem wizardItem = getUserProductProfileOption("WIZARD");	
            //Debug.println(wizardItem.dumpObject());
            if (wizardItem != null) 
                wizardItem.setValue(using);
        }
    }

    /**
     * Get whether this transaction is set to use wizard.
     */
    public boolean isUsingWizard() {
        return usingWizard;
    }
    /**
     * Check the validity of the name and password given. If the user is valid,
     * return the userID of the user. If not, return 0.
     * If the password has expired, return the negative of the user ID.
     */
    public int isNamePasswordValid(String name, String password) {
    	int userID=0;
       // boolean expired = false;
       // String str = null;
       // int superAgentUserID = 0;
//        boolean graceLoginUsed = false;
//        Calendar today = Calendar.getInstance();
//  	    today.setTime(new Date());
//  	    today.set(Calendar.HOUR, 0);
//  	    today.set(Calendar.MINUTE, 0);
//  	    today.set(Calendar.SECOND, 0);
  	    //String.valueOf(today.getTimeInMillis())
  	    
        try {
            userProfile = UnitProfile.getInstance().getProfileInstance().find(name, password);
            userID = userProfile.getID();   
         
        }
        catch (NoSuchElementException e) {
              Debug.println(e.getMessage());
              userID =0;
        }
      
        if(this.idleTimeoutShort){
        if(!isTransactionOwnerUser(userID)){
        	int option = -1;
        	  if(attempt <=1){
        	  	if(UnitProfile.getInstance().isSubAgent())
        	  		 option = Dialogs.showConfirm("dialogs/DialogUserAuthirizationInSubAgentMode.xml");  
        	     else 
        	  	     option = Dialogs.showConfirm("dialogs/DialogIdleTimeOutShortRetry.xml"); 
        	    if(option == Dialogs.OK_OPTION){
        		   userID=0;
        	       return userID;
        	    }
        	    else{
        	    	Dialogs.closeAllDialogs();
        		 	userID = INVALID_USERID;
        	    }
        	  }
        	 if(attempt <=2){
        		 if(UnitProfile.getInstance().isTakeOverSubagentprofile() ||UnitProfile.getInstance().isSubAgent()){
        	    	Dialogs.closeAllDialogs();
        		    MainPanelDelegate.changeToSuperAgent(true);
        		 }
        		 else{
        		 	Dialogs.closeAllDialogs();
        		 	userID = INVALID_USERID;
        		 }
        	}
        	
        	
        }
      }
        
        if(!this.idleTimeoutShort && UnitProfile.getInstance().isSubAgent()){
            if(!isSubAgentTakeOverUserID(userID)){
            	  if(attempt <=2){
            	  int option = Dialogs.showConfirm("dialogs/DialogUserAuthirizationInSubAgentMode.xml");  
            	  if(option == Dialogs.OK_OPTION){
            		Dialogs.closeAllDialogs();
      		 	    userID = 0;
            	  }
            	  else{
            	  	Dialogs.closeAllDialogs();
            	  	userID = INVALID_USERID;
        		    MainPanelDelegate.changeToSuperAgent(true);
            	  
            	  }
            	  }
            	 
            	
            	
            }
          }
       
        return userID;
    }
    
    public int isNamePasswordValid(String name, String password, boolean dualControl) {
    	int userID=0;
         	    
        try {
            userProfile = UnitProfile.getInstance().getProfileInstance().find(name, password);
            userID = userProfile.getID();   
         
        }
        catch (NoSuchElementException e) {
              Debug.println(e.getMessage());
              userID =0;
        }
                                
        return userID;
    }
    
  
    public boolean isTransactionOwnerUser(int userID){
    	 if(this.idleTimeoutShort){
  	      ClientTransaction info = DeltaworksMainPanel.getMainPanel().getCurrentTransaction();
  	      if(info==null)
  	      	return true;
  	      tranStarterUserID = info.getTranStarterUserID();
  	      if(tranStarterUserID == userID){
  	      return true;
  	      
  	      }
  	     else return false;
           }
    	 return false;
    }
    public boolean isSubAgentTakeOverUserID(int userID){
//    	String loginType = getGlobalLoginTypeRequired();
//        if (loginType.equals("N")) {
//        	return true;
//        }
   	 if(UnitProfile.getInstance().getSubAgentTakeOverUserID() == userID)
   	 	return true;
		else
			return false;
   }
    
    public boolean hasPasswordExpired(String name, String password){
        int userID =0;
        boolean expired = false;
        String str = null;
        boolean result = false;
        boolean graceLoginUsed = false;
        Calendar today = Calendar.getInstance();
  	    today.setTime(new Date());
  	    today.set(Calendar.HOUR, 0);
  	    today.set(Calendar.MINUTE, 0);
  	    today.set(Calendar.SECOND, 0);
  	    //String.valueOf(today.getTimeInMillis())
        try {
            userProfile = UnitProfile.getInstance().getProfileInstance().find(name, password);
            userID = userProfile.getID();     
            try {
                if(isSixCharPinType()){
                Profile userProfile = (Profile) UnitProfile.getInstance().getProfileInstance().find("USER", userID);
                insertFields(userID);               
      	        str = userProfile.find("PSWD_CHANGE_DATE").stringValue();   
      	                         
                if((today.getTimeInMillis() - 2*(TimeUtility.MONTH)) >= stringToLong(str)){
                 try{
                 	
                        userProfile.find("PIN_EXPIRED").setValue("Y");  
                        expired = true;    
                       
                       
                  }
                    catch(NoSuchElementException e){
                        Debug.println("No element :PIN_EXPIRED ");
                    }
               }
                             
                  try{
                      graceLoginUsed =userProfile.find("GRACE_LOGIN_USED").booleanValue();
                  }
                  catch(NoSuchElementException e){
                     Debug.println("No element :GRACE_LOGIN_USED");
                  }
          
                  if (expired && !graceLoginUsed){
                      try{
                          userProfile.find("GRACE_TIME").setValue(String.valueOf(today.getTimeInMillis()+TimeUtility.DAY));
                          userProfile.find("GRACE_LOGIN_USED").setValue("Y");
                      }
                      catch(NoSuchElementException e){
                          Debug.println("No element :GRACE_TIME ");
                      }
                      
                  }
                  graceLoginUsed =userProfile.find("GRACE_LOGIN_USED").booleanValue();
            	 if(expired && graceLoginUsed){
            	     result= true; // pin has expired, they have to change it
                }
                }
              
            }
            catch (NoSuchElementException e) {    
                result = false;
            }
         
      
        }
        catch (NoSuchElementException e) {
            Debug.println(e.getMessage());
            result=  false;
        }
        return result;
    
        
    }

    /**
     * Check the validity of the password given. If the user is valid,return
     * the userID of the user. If not, return -1
     */
    public int isPasswordValid(String password) {
	return isNamePasswordValid(null, password);
    }

    /**
     * Set the user of this transaction. If the user does not have access to
     *   this transaction, return false; If a -1 is entered, this indicates that
     *   the transaction is being performed without a login.
     */
    public boolean setUser(int userID) {
    
        this.userID = userID;
        if (userID == 0){
            this.userID = userID;
            this.userName = "";
            return true;
        }
        else{
            if (userID == -1 && isManagerApprovalRequired() && ! productTransaction)
                // no user is OK in this case
                return true;
        }
        try {
            userProfile = (Profile) UnitProfile.getInstance().getProfileInstance().find("USER", userID);	
            this.userName = userProfile.find("NAME").stringValue();	
            if (isManagerApprovalRequired()) {
                String typeFlag = userProfile.find("TYPE").stringValue();	
                if (! typeFlag.equals("M"))	
                    return false;
            }
            if (productTransaction) {
                if (! isUserProductProfileOptionTrue("ISSUE_DOC_TYPE"))	
                    return false;
            }
        }
        catch (NoSuchElementException e) {
            return false;
        }
        return true;
    }

    protected ProfileItem getProfileOption(String tag) {
        ProfileItem item = null;
        try {
            item = UnitProfile.getInstance().getProfileInstance().find(tag);
        }
        catch (NoSuchElementException e) {
            //Debug.println("No Value for profile tag : " + tag);	
        }
        return item;
    }

    protected boolean isProfileOptionTrue(String tag) {
        ProfileItem item = getProfileOption(tag);
        if (item == null)
            return false;
        else
            return item.booleanValue();
    }

    //protected ProfileItem getProductProfileOption(String tag) {
    public ProfileItem getProductProfileOption(String tag) {
        ProfileItem item = null;
        Profile productProfile = getProductProfile();
        try {
            item = productProfile.find(tag);
        }
        catch (NoSuchElementException e) {
//            Debug.println("No Value for profile tag : " + tag + " on document : " + documentSequence);	
        }
        return item;
    }
    public BigDecimal getProductProfileBigDecimalOption(String tag) {
        Profile productProfile = getProductProfile();
        try {
            return productProfile.findDecimal(tag);
        }
        catch (NoSuchElementException e) {
//            Debug.println("No Value for profile tag : " + tag + " on document : " + documentSequence);	
        }
        return BigDecimal.ZERO;
    }
    
    /**
     * return the profile item for the given tag and currency Code
     * @param tag
     * @param currencyCode
     * @return
     */
    protected ProfileItem getProductProfileOption(String tag,String currencyCode) {
        ProfileItem item = null;
        Profile productProfile = getProductProfile();
        try {
            item = productProfile.findCurrencyValue(tag,currencyCode);
        }
        catch (NoSuchElementException e) {       	
            Debug.println("No Value for profile tag : " + tag 
            		+ " with currency : " + currencyCode);	
        }
        return item;
    }
    
    /**
     * return the BigDeciaml value of the item for the given tag and 
     * currency Code
     * @param tag
     * @param currencyCode
     * @return
     */
    protected BigDecimal getProductProfileDecimalOption(String tag,String currencyCode) {
        ProfileItem item = null;
        Profile productProfile = getProductProfile();
        try {
            item = productProfile.findCurrencyValue(tag,currencyCode);
            return item.bigDecimalValue();
        }
        catch (NoSuchElementException e) {       	
            Debug.println("No Value for profile tag : " + tag 
            		+ " with currency : " + currencyCode);	
            return new BigDecimal(0.00);
        }
    }
    
    /**
     * return the profile item for the given tag and product variant
     * @param tag
     * @param product Id
     * @return
     */
  
    public ProfileItem getProductProfileOption(String tag, int prodVariant) {
        ProfileItem item = null;
        Profile productProfile = getProductProfile();
        try {
            item = productProfile.find(tag, prodVariant);
        }
        catch (NoSuchElementException e) {       	
//            Debug.println("No Value for profile tag : " + tag + " on document : " + documentSequence);	
        }
        return item;
    }
    
    /**
     * Return a specific boolean product profile setting. If the profile
     * option can not be found, return false.
     */
    protected boolean isProductProfileOptionTrue(String tag) {
        ProfileItem item = getProductProfileOption(tag);
        if (item == null)
            return false;
        else
            return item.booleanValue();
    }

	protected ProfileItem getUserProfileOption(String tag) {
		ProfileItem item = null;
		try {
			if ((userProfile == null) || (userProfile.getID() != userID)) {
				userProfile = (Profile) UnitProfile.getInstance()
						.getProfileInstance().find("USER", userID); 
			}
			item = userProfile.find(tag);
			return item;
		} catch (NoSuchElementException e) {
			//Debug.println("No value for : " + tag + " for user " + userID);	
		}
		return item;
	}

    /**
     * Return a specific boolean user profile setting. If the profile
     * option can not be found, return false.
     */
    protected boolean isUserProfileOptionTrue(String tag) {
        ProfileItem item = getUserProfileOption(tag);
        if (item == null)
            return false;
        else
            return item.booleanValue();
    }

    /**
     * Return a specific boolean profile setting that applies to both the user
     * and the product. If the profile option can not be found, return false.
     */
    protected boolean isUserProductProfileOptionTrue(String tag) {
        ProfileItem item = getUserProductProfileOption(tag);
        if (item == null)
            // if part has been added for the case where the product profile 
            // item is not found in the user profile items
            if ("ISSUE_DOC_TYPE".equals(tag)) {
                return true;
            } else {
                return false;
            }
        else
            return item.booleanValue();
    }

    protected ProfileItem getUserProductProfileOption(String tag) {
        ProfileItem item = null;
        try {
            if ((userProfile == null)
                    || (userProfile.getID() != userID)) {
                userProfile = (Profile) UnitProfile.getInstance().getProfileInstance().find("USER", userID);	
            }
           	
            item = userProfile.find(tag, documentType);

        }
        catch (NoSuchElementException e) {
            if ((userProfile != null) && (tag != null) && (tag.equals("WIZARD"))){
                // add the wizard mode P profile item for a product if missing.
                Debug.println("No WIZARD in profile..." +  
                        "Creating profile item."); 
                item = ProfileItem.create("ITEM");  
                item.setTag("WIZARD"); 
                item.setMode("P");                  
                item.setID(documentType);
                item.setValue("Y");
                userProfile.add(item);
            } else {            
                Debug.println("element : " + tag + " not found for user : " + userID);	 
            }
        }
        return item;
    }

    /**
     * Get the default text to use in lists when there is a 'none of the above'
     *   function available.
     */
    public String getEmptyItemText() {
        return emptyItemText;
    }

    public String getOtherString() {
        return otherString;
    }

    public String getNoneOfAboveString() {
        return noneOfFollowingString;
    }

    public String[] getNoneOfAboveArray() {
        return noneOfAboveArray;
    }

    public boolean isManagerApprovalRequired() {
        return managerApprovalRequired;
    }

    public boolean isHostConnectionRequired() {
        return hostConnectionRequired;
    }

	public boolean isIsiEnabled() {
		return ("Y".equals(UnitProfile.getInstance().get("ISI_ENABLE", "N")) ? true : false);   
	}

    // To process the ISI commands you need the following. 
    // ISI_ENABLE == 'Y' and product level KEYBOARD_ISSUE == 'N' and
    // a valaid -RP command line or no -RP command line and the 
    // -EXE command line for the transaction.  
    
	public boolean isIsiCommandValid() {
		boolean result = true;

		MainPanelDelegate.setIsiAllowDataCollection(false);
		MainPanelDelegate.setIsiRequireRcptTextData(false);
		if (UnitProfile.getInstance().get("ISI_ENABLE", "N").equals("Y") && isFinancialTransaction() && getKeyboardIssue().equals("N")) { 
			if (MainPanelDelegate.getIsiRpCommandValid() && MainPanelDelegate.getIsiExeCommand() && (! MainPanelDelegate.getIsiTransactionInProgress())) {
				// Allow the user to perform the transaction
				MainPanelDelegate.setIsiTransactionInProgress(true);
				MainPanelDelegate.setIsiAllowDataCollection(true);
				if (UnitProfile.getInstance().get(DWValues.PROFILE_ISI_DF_TEXT, "N").equals("Y")) {
					MainPanelDelegate.setIsiRequireRcptTextData(true);
				}
				result = true;
			} else {
				// do not allow the user to connect. Display a dialog.
				failedNoIsiInitiation();
				MainPanelDelegate.initializeIsiFields();
				result = false;
			}
		}
		return result;
	}

	  public boolean isUserAuthorized(boolean isDispenserLoad,boolean idleTimeoutShort ) {
       this.idleTimeoutShort = idleTimeoutShort;
       
      
	  boolean result = false;
	  type =
	          isManagerApprovalRequired()
	              ? NamePasswordDialog.MANAGER_LOGIN
	              : NamePasswordDialog.USER_LOGIN;
	  if(idleTimeoutShort || UnitProfile.getInstance().isTakeOverSubagentprofile() ||UnitProfile.getInstance().isSubAgent()){
       	DeltaworksMainPanel.getMainPanel().getCurrentTransaction().setTranStarterUserID(
       			DeltaworksMainPanel.getMainPanel().getCurrentTransaction().getUserID());
	  }
	  attempt = 0;
		// invalid command and ISI enabled using command-line-interpreter rather than keyboard issued
		if (!isIsiCommandValid() && isFinancialTransaction()) {
			// keyboard entry is disallowed
			if (isIsiEnabled() && "N".equals(getKeyboardIssue()))
				return false;
			// command line entry to initiate the user input is disallowed
			else if (MainPanelDelegate.getIsiFrontCommandOccurred())
				if ("Y".equals(getKeyboardIssue()))
					return false;
		}
	  else {
		 // if (!isDispenserLoad)
			  MainPanelDelegate.setIsiAppendMode(false);
	  }
	
	  if (!isPasswordRequired()) {
		  result = true; // no authorization necessary.
	  }
	  else {
		  boolean nameRequired = isNamePasswordRequired();
		  
	
          // prompt for the user's name and password
          NamePasswordDialog dialog = Dialogs.showNamePasswordDialog(type, nameRequired, false);
          
          if (dialog.isCanceled()) {
        	  // make sure we don't have a pending transaction if we are doing DL
        	  result = false;
        	  MainPanelDelegate.setupCancelISI("01");
        	  if ((MainPanelDelegate.getIsiAllowDataCollection() && 
        		   MainPanelDelegate.getIsiExeProgramArguments() != null ) ||
        		  (isDispenserLoad && ("Y".equals(UnitProfile.getInstance().get("ISI_ENABLE", "N"))))){
        		  String programName = "";
        		  if (MainPanelDelegate.getIsiExeProgramArguments() != null)
        			  programName = MainPanelDelegate.getIsiExeProgramArguments().trim();
        		  if (programName.length() > 0 && !(programName.startsWith("-EXE"))) {
  	  	  			  MainPanelDelegate.executeIsiProgram(programName);
        		  }
        	  }
              if (MainPanelDelegate.getIsiNfProgramArguments() != null  && 
              		"Y".equals(UnitProfile.getInstance().get("ISI_ENABLE", "N"))){
                String programName = MainPanelDelegate.getIsiNfProgramArguments().trim();
                if (programName.length() > 0 && !(programName.startsWith("-NF"))) {
                	MainPanelDelegate.executeIsiProgram(programName);
                }
                MainPanelDelegate.setIsiNfCommand(false);
                MainPanelDelegate.setIsiNfProgramArguments("");
              }

        	  MainPanelDelegate.setIsiTransactionInProgress(false);
        	  MainPanelDelegate.setIsiExeCommand(false);
        	  MainPanelDelegate.setIsiExe2Command(false);
        	  MainPanelDelegate.setIsiExeProgramArguments("");
        	  MainPanelDelegate.setIsiRpCommandValid(false);
          }
          else {   //  dilaog is not cancelled , go ahead          
              String userName = dialog.getName();
     	      String pswd = dialog.getPassword();     	   
        	  if(isUserLocked(userName,pswd)){
                  Dialogs.showWarning("dialogs/DialogAccountLocked.xml");                                
              }
        	 else{
        	     attempt++;  
      	         userID = isNamePasswordValid(userName, pswd);
      	         if(userID == INVALID_USERID)
      	          return false;
      	         else
      	          if(userID == -200)
        	          return false;
                if (userID == 0 || !setUser(userID)) {
            	  ErrorManager.recordLoginAttempt(false, userName);
                  // bad name/pwd...show an error dialog indicating this
                  while (attempt < 3) {
                	  dialog = Dialogs.showNamePasswordDialog(type, nameRequired, true);
                	 
                      if (dialog.isCanceled()) {
                    	  result = false;
                          MainPanelDelegate.setupCancelISI("01");
                          break;
                      }
                                       
                      else {
                    	  // try to log in again                         
                          userName = dialog.getName();
                          pswd = dialog.getPassword();
                      	if(isUserLocked(userName,pswd)){
                            Dialogs.showWarning("dialogs/DialogAccountLocked.xml"); 
                            return false;
                        }
                          userID =
                                isNamePasswordValid(
                                    userName,
                                    dialog.getPassword());
                          if(userID == INVALID_USERID)
                	          return false;
                          else
                	          if(userID == -200)
                  	          return false;
                          if (userID == 0 || !setUser(userID))
                        	  ErrorManager.recordLoginAttempt(false, userName);                         
                          else { // login is good
                              if(hasPasswordExpired(userName,pswd)){  
                                  handleExpired(userName,pswd);
                                  cDialog = Dialogs.showChangePasswordDialog(userID, dialog.getPassword(),this);
	                    		  if (cDialog.isCanceled()){
	                    		      if(cDialog.isCommunicationError()&& ((this.getDocumentSequence()==ClientTransaction.VENDOR_PAYMENT_TYPE)
	                    		              ||(this.getDocumentSequence()==ClientTransaction.MONEY_ORDER_TYPE))){
	                    		          Dialogs.showInformation("dialogs/DialogUnableToUpdatePin.xml");
	                    		          result= true;
	                    		          break;
	                    		      }
	                    		      else {
	                    			  // Return to the mainpanel.
	                    			 return false;	                    			 
	                    		      }	                    		    
	                    		  }	   
                              }
                        	  if (isPasswordChangeRequired(userID)){
	                    		  cDialog = Dialogs.showChangePasswordDialog(userID, dialog.getPassword(),this);
	                    		  if (cDialog.isCanceled()){
	                    		      if(cDialog.isCommunicationError()&& ((this.getDocumentSequence()==ClientTransaction.VENDOR_PAYMENT_TYPE)
	                    		              ||(this.getDocumentSequence()==ClientTransaction.MONEY_ORDER_TYPE))){
	                    		          Dialogs.showInformation("dialogs/DialogUnableToUpdatePin.xml");
	                    		          result= true;
	                    		          break;
	                    		      }
	                    		      else {
	                    			  // Return to the mainpanel.
	                    			 return false;	                    			 
	                    		      }	                    		    
	                    		  }	                    		
                          	  }
                         
                    		  ErrorManager.recordLoginAttempt(true, userName);
                              // check if we need a dual control login also		
                              result = dualControlLogin(nameRequired, userID);
                             
                              break;
                          }
                      }
                      attempt++;
                      if(attempt==3){
                          if(isSixCharPinType()&& !idleTimeoutShort){
                          lockUser(userName,pswd);
                          }
                  }
                  }
            
              }
              else { 
                   if(hasPasswordExpired(userName,pswd)){
                       handleExpired(userName,pswd);
                       cDialog = Dialogs.showChangePasswordDialog(userID, dialog.getPassword(),this);
  	        		 
  	        		  if (cDialog.isCanceled()){
  	        		      if(cDialog.isCommunicationError()&& ((this.getDocumentSequence()==ClientTransaction.VENDOR_PAYMENT_TYPE)
              		              ||(this.getDocumentSequence()==ClientTransaction.MONEY_ORDER_TYPE))){
              		          Dialogs.showInformation("dialogs/DialogUnableToUpdatePin.xml");
              		          result= true;
              		      }
  	        		   else{
  	        		       // Return to the mainpanel.
  	        		     return false;
  	        		   }
  	        		  }
  	        		 
  	        		

              	  }                         
            	      if (isPasswordChangeRequired(userID)) {    
	        		  cDialog = Dialogs.showChangePasswordDialog(userID, dialog.getPassword(),this);
	        		 
	        		  if (cDialog.isCanceled()){
	        		      if(cDialog.isCommunicationError()&& ((this.getDocumentSequence()==ClientTransaction.VENDOR_PAYMENT_TYPE)
            		              ||(this.getDocumentSequence()==ClientTransaction.MONEY_ORDER_TYPE))){
            		          Dialogs.showInformation("dialogs/DialogUnableToUpdatePin.xml");
            		          result= true;
            		      }
	        		   else{
	        		       // Return to the mainpanel.
	        		     return false;
	        		   }
	        		  }
	        		 
	        		

            	  }   
               
            	 ErrorManager.recordLoginAttempt(true, userName);
            	 result = dualControlLogin(nameRequired, userID);
            	 
            	
            	  
              }
          }
	  }
	  }
      return result;
  }
  public void lockUser(String name,String password){
//      boolean result = false; 
      Profile userProf = null;
      if(isSixCharPinType()){
   
      try {
           
            if (name!=null) {
            	userProf = UnitProfile.getInstance().getProfileInstance().findName(name); 
            } else { 
            	userProf = UnitProfile.getInstance().getProfileInstance().find(null, password);
            }
            
            if(name!=null){
            try{
               
                    if(userProf.get("USER_ID_LOCKED")!=null
                            &&  !userProf.get("USER_ID_LOCKED").booleanValue())
                    {
                        userProf.get("USER_ID_LOCKED").setValue("Y");	
                Dialogs.showWarning("dialogs/DialogAccountLocked.xml"); 
                    }
                
            }
            catch(NoSuchElementException e){
                Debug.println("No element : " + "USER_ID_LOCKED" + " in user profile in client transaction.");  
            }
            }
       
         
      }
      catch (NoSuchElementException e) {
          Debug.println(e.getMessage());  
        
      }
      }
     
      
  }

  public boolean isUserLocked(String name,String password){
      boolean result = false; 
      Profile userProf = null;
      if(isSixCharPinType()){
   
      try {
           
            if (name !=null) {
            	userProf = UnitProfile.getInstance().getProfileInstance().findName(name); 
            } else { 
            	userProf  = UnitProfile.getInstance().getProfileInstance().find(null, password);
            }
            try {
            	result = (userProf.find("USER_ID_LOCKED").booleanValue());
           
            } catch(NoSuchElementException e) {
                Debug.println("No element : " + "USER_ID_LOCKED" + " in user profile in client transaction.");  
            }
         
      }
      catch (NoSuchElementException e) {
          Debug.println(e.getMessage());  
          return false;
      }
      }
      return result;
  }
  
  public void insertFields(int userID){
      Profile userProfile = null;
      try{
          if(isSixCharPinType()){              
          userProfile = (Profile) UnitProfile.getInstance().getProfileInstance().find("USER", userID);                   
          try{              
               userProfile.find("GRACE_TIME");
          }
          catch(NoSuchElementException e){
              Debug.println("No element :GRACE_TIME when querying ");
              ProfileItem pi = new ProfileItem("ITEM", "GRACE_TIME", "P", "0", null);
	            userProfile.insertOrUpdate("GRACE_TIME", pi);	          
          }
          try{ 
              userProfile.find("PSWD_CHANGE_DATE");
          }
          catch(NoSuchElementException e){
              Debug.println("No element :PSWD_CHANGE_DATE when querying ");
              ProfileItem pi = new ProfileItem("ITEM", "PSWD_CHANGE_DATE", "P", "1107237600953", null);
	            userProfile.insertOrUpdate("PSWD_CHANGE_DATE", pi);
	            //str = userProfile.find("PSWD_CHANGE_DATE").stringValue();   	          
          }
          try{
              userProfile.find("PIN_EXPIRED");                  
          }
          catch(NoSuchElementException e){
              Debug.println("No element :PIN_EXPIRED not found when seeting expiry to N");
              ProfileItem pi = new ProfileItem("ITEM", "PIN_EXPIRED", "P", "N", null);
  	       userProfile.insertOrUpdate("PIN_EXPIRED", pi);
          }
        
          try{
             userProfile.find("GRACE_LOGIN_USED");
          }
          catch(NoSuchElementException e){
             Debug.println("No element :GRACE_LOGIN_USED");
              ProfileItem pi = new ProfileItem("ITEM", "GRACE_LOGIN_USED","P" , "N", null);
              userProfile.insertOrUpdate("GRACE_LOGIN_USED", pi); 
          }
          }
         
          saveProfileChanges();
        
      }
      catch(NoSuchElementException e){
          }
  }


	public void validatePin(final int userID, final String newPass) {

		final WaitToken wait = new WaitToken();
		final MessageFacadeParam parameters = new MessageFacadeParam(MessageFacadeParam.REAL_TIME);
		final CommCompleteInterface cci = new CommCompleteInterface() {
			@Override
			public void commComplete(int commTag, Object returnValue) {
				wait.setWaitFlag(false);
				if (returnValue instanceof DwPasswordResponse) {
					setReturnValue((DwPasswordResponse) returnValue);

				}
				synchronized (wait) {
					wait.notify();

				}
			}
		};
		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {
				DwPasswordRequest request = new DwPasswordRequest();
				request.setNewPassword(newPass);
				request.setEmployeeNumber(String.valueOf(userID));
				return MessageFacade.getInstance().validatePin(cci, parameters, request);
			}
		};

		wait.setWaitFlag(true);
		MessageFacade.run(logic, cci, 0, null);
		synchronized (wait) {
			try {
				while (wait.isWaitFlag()) {
					wait.wait();
				}
			} catch (InterruptedException ie) {
				Debug.println(" caught InterruptedException"); 
			}
		}

	}

     public DwPasswordResponse getValidatePinResponse(){
         return response;
       }
     public void setReturnValue(DwPasswordResponse t){
        response=t; 
        if(Dialogs.getInstance()!=null)
        Dialogs.getInstance().setValidatePinResponse(response);
       
  }

  /**
   * If the USER profile item PSWD_CHANGE_DATE doesn't exist, set it 
   * to the number of millis from January 1, 2005.  Returns true if
   * the PSWD_CHANGE_DATE exceeds 30 days from today.
   */
   public boolean isPasswordChangeRequired(int id){
      Calendar today =null;
      String str=null;
      try {
          if("3DIGIT".equalsIgnoreCase(UnitProfile.getInstance().get("USER_PIN_TYPE","3DIGIT"))){
        	  return false;
          } else {
		  	  Profile userProfile = (Profile) UnitProfile.getInstance().getProfileInstance().find("USER", id);	  	
		  	  str = userProfile.find("PSWD_CHANGE_DATE").stringValue(); 	  
			  today = Calendar.getInstance();
			  today.setTime(new Date());
			  today.set(Calendar.HOUR, 0);
			  today.set(Calendar.MINUTE, 0);
			  today.set(Calendar.SECOND, 0);
          }
    	  return ((today.getTimeInMillis() - 2*(TimeUtility.MONTH)) >= stringToLong(str) || userProfile.get("PASSWORD").stringValue().length() < 8);
      } catch(Exception e){
    	  return false;
      }	       	  
  }

  public static long stringToLong(String s) {
      try { return Long.parseLong(s); }
      catch (NumberFormatException e) { return 0; }
  }

 
  /**
   * Prompt the user to enter a second manager's name/pwd for dual control
   *   purposes if needed.
   */
  public boolean dualControlLogin(boolean nameRequired, int userID) {
      int attempt = 0;
      if (!isManagerApprovalRequired()
          || !isDualControlRequired())
          return true;
      while (attempt++ < 3) {
          NamePasswordDialog dialog =
              Dialogs.showNamePasswordDialog(
                  NamePasswordDialog.DUAL_CONTROL_LOGIN,
                  nameRequired,
                  false);
          if (dialog.isCanceled())
              return false;
          String name = dialog.getName();
          String pwd = dialog.getPassword();
          // try to log in a manager
          int user2 = isNamePasswordValid(name, pwd,true);
          if(isUserLocked(name,pwd)){
            Dialogs.showWarning("dialogs/DialogAccountLocked.xml"); 
            return false;
        }

          if (user2 == 0) { // not even a valid user
              ErrorManager.recordLoginAttempt(false, name);
          }
          else {
              // is a real user, but is it a manager?
              if (ClientTransaction.isManager(name, pwd)) {
                  // this is a manager, but is it the same user again?
                  ErrorManager.recordLoginAttempt(userID == user2, name);
                  if (userID != user2) {
                  	
                  	int user =isPasswordChangeRequired(name,pwd,user2);
                  	if(user == -1)
                  		return false;
                  	else{
                      DiagLog.getInstance().writeSecurity(
                          SecurityDiagMsg.DUAL_CONTROL,
                          (short) user2);

                      return true;
                  	}
                  }
              }
              else {
                  ErrorManager.recordLoginAttempt(false, name);
              }
          }
         
      }
      return false;
  }
  public int isPasswordChangeRequired(String name, String pwd, int userID){

	 if(hasPasswordExpired(name,pwd)){
	 	handleExpired(name,pwd);
      cDialog = Dialogs.showChangePasswordDialog(userID, pwd,this);
      if (cDialog.isCanceled()){
		      return -1;	                    			 
		      }	
      else
      	 return userID;
		  }
	 else
	 {
	 	if(isPasswordChangeRequired(userID)){
	 	 cDialog = Dialogs.showChangePasswordDialog(userID,pwd,this);
       if (cDialog.isCanceled()){
		      return -1;	                    			 
		      }	
       else
       	 return userID;
	 }
	else
		return userID;
		
	 }
	
}

  public int handleExpired(String name, String password) {
      
//      boolean graceLoginUsed = false;
      int userID =0;
      Calendar today = Calendar.getInstance();
	      today.setTime(new Date());
	      today.set(Calendar.HOUR, 0);
	      today.set(Calendar.MINUTE, 0);
	      today.set(Calendar.SECOND, 0);
      try {
          userProfile = UnitProfile.getInstance().getProfileInstance().find(name, password);
          userID = userProfile.getID();
          try {
              String str = userProfile.find("GRACE_TIME").stringValue();                           
              if (stringToLong(str) - today.getTimeInMillis() < 0){          
                  userProfile.get("USER_ID_LOCKED").setValue("Y");
                  userProfile.find("GRACE_LOGIN_USED").setValue("N");                  
                  Dialogs.showWarning("dialogs/DialogAccountLocked.xml");   
                 userID = 0 - userID; // pin has expired, they have to change it
                 return userID;
              }

          }
          catch (NoSuchElementException e) {
           Debug.println("No such element GRACE_TIME ");
          }
      }
      catch(NoSuchElementException e){
         return 0;
      }
     
      Dialogs.showError("dialogs/DialogPasswordExpired.xml");
      return userID;
  }


    /**
     * Determine whether or not a name and password is required for this
     * transaction.
     */
    public boolean isNamePasswordRequired() {
//Commented by Prem for  SUBAGENT_MODE taken over by super Agent   	
//        if (UnitProfile.getInstance().get("SUBAGENT_MODE", "N").equals("Y")) {
//            return false;
//        }
       String requiredValue = getProductProfileOption(
                                       "NAME_PIN_REQUIRED").stringValue();                              	
       if (requiredValue.equals("Y"))	
           return true;
       else
           return false;
    }

    /**
      * return the NAME_PIN_REQUIRED setting for the current transaction
      */
     public String getNamePinRequired() {
        return getProductProfileOption("NAME_PIN_REQUIRED").stringValue();                              	

     }
 
    /**
      * return the KEYBOARD_ISSUE setting for the current transaction
      */
     public String getKeyboardIssue() {
        return getProductProfileOption("KEYBOARD_ISSUE").stringValue();                                  

     }

     public boolean isAcctNumberLookupEnabled() {

		String enabled = "Y";

		ProfileItem p = getProductProfileOption("ENABLE_ACCOUNT_NUMBER_LOOKUP");
		if (p != null)
			enabled = getProductProfileOption("ENABLE_ACCOUNT_NUMBER_LOOKUP")
					.stringValue();

		return enabled.equalsIgnoreCase("Y");
	}

     public boolean isPhoneNumberLookupEnabled() {

    	 String enabled = "Y"; 
    	 
    	 ProfileItem p = getProductProfileOption("ENABLE_PHONE_NUMBER_LOOKUP");
    	  if (p != null)
    		  enabled = getProductProfileOption("ENABLE_PHONE_NUMBER_LOOKUP").stringValue();
    	  
           return enabled.equalsIgnoreCase("Y");
     }

     /**
      * return the FORM_FREE_ENABLED setting for the current transaction
      */
     public boolean isFormFreeEnabled() {
    	 String enabled = "N"; 
    	 
    	 ProfileItem p = getProductProfileOption("FORM_FREE_ENABLED");
    	  if (p != null)
    		  enabled = getProductProfileOption("FORM_FREE_ENABLED").stringValue();
    	  
           return enabled.equalsIgnoreCase("Y");
        }

     
    /**
     * Determine whether or not a password is required for this transaction.
     */
    public boolean isPasswordRequired() {
//    	Commented by Prem for  SUBAGENT_MODE taken over by super Agent 
//        if (UnitProfile.getInstance().get("SUBAGENT_MODE", "N").equals("Y")) {
//            return false;
//        }
        String requiredValue = getProductProfileOption(
                                        "NAME_PIN_REQUIRED").stringValue();	
       
        if (requiredValue.equals("N")) {	
//        	/*
//        	 * Check pinRequired, some transactions (Send Reversals, 
//        	 * Receive Reversals and Amends) are based on transactions that can
//        	 * have PINs disabled, but these transactions should always ask for
//        	 * at least a PIN.
//        	 */
//        	if (this.pinRequired){
//        		return true;
//        	}
            return false;
        }
        else
            return true;
    }

    /**
     * Determine whether or not dual entry is required for this transaction. If
     *   the profile indicates dual control, but there is only one manager,
     *   then we can not use dual control.
     */
    public boolean isDualControlRequired() {
        boolean profileOptionTrue = isProfileOptionTrue("DUAL_CONTROL");	
        if (! profileOptionTrue)
            return false;

        // find the number of managers this agent has
        int managerCount = 0;
        for (Iterator<ProfileItem> it = UnitProfile.getInstance().getProfileInstance().iterator(); it.hasNext();){
            ProfileItem item = it.next();
            if (item.getTag().equalsIgnoreCase("USER") &&	
                           isManager(item.getID()) && ++managerCount > 1)
                break;
        }
        return managerCount > 1;
    }

    /**
     * Determine whether or not the user set by setUser chose the
     * wizard interface last time.
     */
    public boolean isUserWizard() {
        if(UnitProfile.getInstance().isMultiAgentMode())
        	return isUsingWizard();
        else if (productTransaction)
    	      return isUserProductProfileOptionTrue("WIZARD");	
        else
            return isUsingWizard();
    }

    /**
     * Determine from the profile value whether or not this user is allowed to
     *   load the dispenser.
     */
    public static boolean canLoadDispenser(int userID) {
        try {
            Profile userProfile = (Profile) 
                UnitProfile.getInstance().getProfileInstance().find("USER", userID);	
            return userProfile.find("LOAD_FORMS").booleanValue();	
        }
        catch (NoSuchElementException e) {
            return false;
        }
    }

    /** 
     * Voids the last (unprintable) form in the dispenser. Checks to see if the 
     * last printable form was just used. This method should only be called following
     * a successful item print.
     */
    public void voidIfLastForm(DispenserInterface dispenser) {
        //Debug.println("voidIfLastForm, remainingCount = " 	
        //        + dispenser.getRemainingCount());
        if (dispenser.getRemainingCount() == 0) {
            dispenser.fullVoidForm(userID, true, MoneyOrderVoidReasonCodeType.LAST_ITEM);
            dispenser.ejectForm();

        }
    }

	public ComplianceTransactionRequest getComplianceTransactionRequestInfo() {
		return complianceTransactionRequestInfo;
	}
	
	public void setComplianceTransactionRequestInfo(ComplianceTransactionRequest ctri) {
		complianceTransactionRequestInfo = ctri;
	}
	
	public void setCustomerComplianceInfo(CustomerComplianceInfo cci) {
		complianceTransactionRequestInfo.setCci(cci);
	}

    /**
     * Clear out the contents of the transaction.
     */
    public void clear() {
        dateTime = TimeUtility.currentTimeMillis();
        tranLimitOverridden = false;
        setStatus(IN_PROCESS);
        // get the profile values again in case the profile has changed
        getProfileValues();
        agentBaseCurrency = CountryInfo.getAgentCountry().getBaseReceiveCurrency();
        if (productTransaction)
            getProductProfileValues();
            
        setCustomerComplianceInfo(new CustomerComplianceInfo());
    }

    /**
     * Lock the CCILog before the first write.
     */
    public void lockCciLog() {
        Debug.println("Locking the CCI before first write");  
        try {
            CCILog.getInstance().getDetailFile().lock();
        } catch (IOException e) {
            Debug.println("Error locking the CCI"); 
            Debug.printStackTrace(e);
        }
    }

    /**
      * Unlock the CCILog after all the write to the log is done.
      */
    public void unlockCciLog() {
        Debug.println("Unlock CCILog, done writing to it.");  
        try {
            CCILog.getInstance().getDetailFile().unLock();
        } catch (IOException e1) {
            Debug.println("Error unlocking the CCI");   
            Debug.printStackTrace(e1);
        }
    }

    public void f2SAR() {
        // For this build of Deltaworks we don't want the code to use profile
        // "SAR_ENABLE" so it was changed to "SAR_ENABLE_ni" 	
        String result = UnitProfile.getInstance().get("SAR_ENABLE_ni", "N");  
        if (result.equalsIgnoreCase("Y")) { 
            ComplianceDialog dialog =
                new ComplianceDialog(getComplianceTransactionRequestInfo(), true);
            Dialogs.showPanel(dialog);
        }
    }

    public void escSAR() {
        // For this build of Deltaworks we don't want the code to use profile
        // "SAR_ENABLE" so it was changed to "SAR_ENABLE_ni" 	
        String result = UnitProfile.getInstance().get("SAR_ENABLE_ni", "N");  
        if (result.equalsIgnoreCase("Y")) { 
            if (everDisplayedCTR()) {
                ComplianceDialog dialog =
                    new ComplianceDialog(getComplianceTransactionRequestInfo(), true);
                Dialogs.showPanel(dialog);
            }
        }
    }
    public void f2NAG() {
        // For this build of Deltaworks we don't want the code to use profile
        // "SAR_ENABLE" and "SAR_NAG" so it was changed to "SAR_ENABLE_ni" 
        // and "SAR_NAG_ni"
        String resultNAG = UnitProfile.getInstance().get("SAR_NAG_ni", "N");  
        String resultSAR = UnitProfile.getInstance().get("SAR_ENABLE_ni", "N");  
        if (resultNAG.equalsIgnoreCase("Y") && 
        resultSAR.equalsIgnoreCase("Y")) { 
            if (Dialogs.showConfirm("dialogs/DialogSARQuestion.xml") == Dialogs.YES_OPTION) { 
                f2SAR();
            }
        }
    }
    public boolean everDisplayedCTR() {
    	//FUTURE implement this to implement original design of DW 4.0
    	return false;
    }
    
    /**
    * Called when a transaction has been attempted to start manually, but 
    * requires a call from the DGISI.EXE program first. Show the failed 
    * dialog and hide this dialog.
    */
    public static void failedNoIsiInitiation() {
        Dialogs.showError("dialogs/DialogNeedIsiInitiation.xml"); 
    }

    
    public void getIsiHeader(List<String> fields){
        String[] headerItems = {(UnitProfile.getInstance().isTrainingMode() ? "T" : (UnitProfile.getInstance().isDemoMode() ? "D" : "P")),
        		new SimpleDateFormat(TimeUtility.ISI_DATE_TIME_FORMAT).format(TimeUtility.currentTime()),
            Integer.toString(userID<0 ? 0 : userID),
            userName==null ? "" : userName,
            UnitProfile.getInstance().get("MG_ACCOUNT","NO_MG_ACCOUNT"),
            UnitProfile.getInstance().get("AGENT_ID","NO_AGENT_ID")};

        for(int i=0; i<headerItems.length; i++){
            fields.add(headerItems[i]);
        }

        return;
    }
   

    public String formatIsiDateField(String stringToFormat){
        String dob = "";
        if(stringToFormat != null){
            if(stringToFormat.trim().length()!=0){
                Date dt;
                try {
                    dt = new SimpleDateFormat(TimeUtility.DATE_MONTH_DAY_SHORT_YEAR_NO_DELIMITER_FORMAT).parse(stringToFormat);
                    dob = new SimpleDateFormat(TimeUtility.DATE_LONG_YEAR_MONTH_DAY_FORMAT).format(dt);
                }
                catch (ParseException e) {
                    Debug.println("ParseException in formatIsiDateField: "+e);
                }
            }
        }
        return dob;
    }

    public String maskString(String s){
        s = nullToSpace(s);
        StringBuffer sb = new StringBuffer(s);
        for(int i=0; i<sb.length()-4; i++){
            sb.replace(i,i+1,"X");
        }
        s = sb.toString();
        return s;
    }

    public String nullToSpace(Object data) {
        return data != null ? data.toString() : " ";
    }
    
    public boolean withinStoreHours(){ 
        if (!UnitProfile.getInstance().isWithinBusinessHours()) {
            Dialogs.showError("dialogs/DialogOutsideBusinessHours.xml", 
            UnitProfile.getInstance().getBusinessStartTime(), UnitProfile.getInstance().getBusinessEndTime());
            MainPanelDelegate.initializeIsiFields();
            return false;
        }
        return true;
    }
  
    abstract public boolean isFinancialTransaction();
    abstract public void setFinancialTransaction(boolean b);
 
    /** 
     * Verify if a checkin is required for this transaction.
	 * All sub classes of client transaction would implement this method.
     * All non-financial transactions would return false. 
	 */
	  
    abstract public boolean isCheckinRequired();
    
    // Note: Do not remove parameter checkCachedResponses from this method. Although the parameter is not
    //       used by the method in this super class, the method is overridden in some subclasses and the
    //       parameter is used in those methods. Thus, the parameter must be present in this method so a
    //       subclass can override it.
    
    public boolean isTransmitRequired(boolean checkCachedResponses) {
    	return (UnitProfile.getInstance().checkTransmitConditions() || isCheckinRequired());
    }
    
	/**
	 * Set the documentSequence number.
	 */
	public void setDocumentSequence(int documentSequence) {
		this.documentSequence = documentSequence;
	}
	public int getDocumentSequence(){
	    return this.documentSequence;
	}
	
	/**
	 * Set the documentSequence number.
	 */
	public void setDocumentType(int documentType) {
		this.documentType = documentType;
	}
	
	/**
	 * Set the documentSequence number.
	 */
	public int getDocumentType() {
		return this.documentType;
	}
	public boolean isSixCharPinType(){
	    if("6CHAR".equalsIgnoreCase(UnitProfile.getInstance().get("USER_PIN_TYPE","3DIGIT")))
	        return true;
	        else return false;
	    
	}
	public void saveProfileChanges() {
        try {
            UnitProfile.getInstance().changesApplied();
            UnitProfile.getInstance().saveChanges();
        }
        catch (java.io.IOException e) {
            Debug.println("saveProfileChanges(): " +	
                    "ignoring unrecoverable exception: " + e); 
        }
    }   

	/**
	 * @return Returns the tranStarterUserID.
	 */
	public int getTranStarterUserID() {
		return tranStarterUserID;
	}
	/**
	 * @param superAgentUserID The tranStarterUserID to set.
	 */
	public void setTranStarterUserID(int tranStarterUserID) {
		this.tranStarterUserID = tranStarterUserID;
	}
	 public String getGlobalLoginTypeRequired() {
        return UnitProfile.getInstance().get("GLOBAL_NAME_PIN_REQ", "P");
    }
	
     /**
      * return the ENABLE_FACE_AMOUNT setting for the current transaction
      */
     public String getSendAmtFlag() {
        return getProductProfileOption("ENABLE_FACE_AMOUNT").stringValue();                                  

     }
     
     /**
      * return the ENABLE_FEE setting for the current transaction
      */
     public String getSendFeeFlag() {
        return getProductProfileOption("ENABLE_FEE").stringValue();                                  

     }
     
     /**
      * return the ENABLE_FX_RATE setting for the current transaction
      */
     public String getFxRateFlag() {
        return getProductProfileOption("ENABLE_FX_RATE").stringValue();                                  

     }
 	
    public void printReceipt(boolean customerReceipt, boolean reprint, ReprintData reprintData, JComponent screen) {
    	if (reprintData != null) {
    		reprintData.setCustomerReceipt(customerReceipt);
    	}
        if (TEPrinter.getPrinter().isReady(screen)) {
            printReceipt(customerReceipt, reprint, reprintData);
        }
    }
    
    protected void printReceipt(boolean customerReceipt, boolean reprint, ReprintData reprintData) {
    	
    }

}
