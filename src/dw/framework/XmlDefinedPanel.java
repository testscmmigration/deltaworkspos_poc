/*
 * XmlDefinedPanel.java
 * 
 * $Revision$
 *
 * Copyright (c) 1999-2005 MoneyGram International
 */

package dw.framework;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Point;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;

import dw.dwgui.DWComponentInterface;
import dw.dwgui.MultiListComboBox;
import dw.gui.Gui;
import dw.gui.GuiException;
import dw.gui.ImageIconCache;
import dw.i18n.Messages;
import dw.main.DeltaworksStartup;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.TimeUtility;

/**
 * XMLDefinedPanel is a JPanel component that constructs itself from an XML
 * definition. It reads the XML file and creates the component hierarchy
 * defined. XmlDefinedPanel allows for extra components to be added to
 * containers defined in the XML, as well as getting components by name.
 * @author Rob Campbell
 */
public class XmlDefinedPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	// default directory to look for xml files
    protected static String xmlPath = "xml/"; 

    // print stream to display GuiExceptions on
    private static PrintStream guiExceptionStream = null;

    // name of the file this panel should be generated from
    protected String xmlFileName;

    // collection of generated gui components from XML definition
    private Gui gui;

    // collections of listeners...to remove them later for cleanup
    // map contains an ArrayList of listeners installed on the component
    private Map<Component, List<EventListener>> actionListenerMap = new HashMap<Component, List<EventListener>>();
    private Map<Component, List<EventListener>> focusListenerMap = new HashMap<Component, List<EventListener>>();
    private Map<Component, List<EventListener>> textListenerMap = new HashMap<Component, List<EventListener>>();
    private Map<Component, List<EventListener>> keyListenerMap = new HashMap<Component, List<EventListener>>();
    private Map<Component, List<EventListener>> selectionListenerMap = new HashMap<Component, List<EventListener>>();
    private Map<Component, List<EventListener>> itemListenerMap = new HashMap<Component, List<EventListener>>();

    // store the focus order
    // removed when we started using JDK 1.4
    //private HashMap focusOrderMap = new HashMap();

    /**
     * Create a new XmlDefinedPanel by parsing the file specified. Will first
     *   look for the file as a system resource, then if not found, will look
     *   for it as a file.
     * @param fileName the name of the xml file describing the gui layout
     */
    public XmlDefinedPanel(String fileName) {
        setLayout(new BorderLayout());
        xmlFileName = new StringBuffer(xmlPath.length() + fileName.length())
            .append(xmlPath).append(fileName).toString();
        // Try to get XML file as a system resource
        try {
	        URL fileUrl = ClassLoader.getSystemResource(xmlFileName);
	        // create the gui from the xml definition

	        createFromXml(fileUrl);
        }
        catch (GuiException e) {
            if (guiExceptionStream == null) {
                Debug.println(e.toString());
                System.exit(1);
            }
            guiExceptionStream.println("GuiException caught : " + e);	
		}
    }

    public static void setGuiExceptionStream(PrintStream stream) {
        guiExceptionStream = stream;
    }

    /**
     * Parse the XML URL specified and create the component hierarchy. For the
     *   component named "main" in the XML, add it to the center of this
     *   panel's layout.
     * @param url the URL object containing the xml gui definition
     */
    private void createFromXml(java.net.URL url) throws GuiException {
        try {
            gui = new Gui(url);
        }
        catch (GuiException e) {
            throw e;
        }
    	catch (Exception e) {
	        Debug.println("Error: couldn't read XML file " + xmlFileName);	
            Debug.println(e.toString());
	        System.exit(0);
      	}
	    if (gui.containsKey("main")) {	
	        JComponent content = (JComponent) gui.get("main");	
            add(BorderLayout.CENTER, content);
	    }
    }

   /**
     * Go through the system event queue and pull off any pending key or mouse
     *   events that are waiting to occur. This will be called whenever we are
     *   about to show a page, so any extraneous keystrokes or multiple
     *   activations will not occur while we are waiting for something to happen
     *   in the GUI.
     */
    public static void emptyEventQueue() {
        EventQueue q = Toolkit.getDefaultToolkit().getSystemEventQueue();
        AWTEvent event = null;
        Vector<AWTEvent> preserve = new Vector<AWTEvent>();
        // go through the whole event queue and pull all events out of it
        while ((event = q.peekEvent()) != null) {
            try {
                event = q.getNextEvent();
                // only need to kill mouse and key events...save all others
                if ( (! (event instanceof KeyEvent)) &&
                                  (! (event instanceof MouseEvent)))
                    preserve.add(event);
            }
            catch (InterruptedException e) {}
        }
        // now re-post all events that we want to keep
        for (Iterator<AWTEvent> it = preserve.iterator(); it.hasNext(); )
            q.postEvent(it.next());
    }

    /**
     * Test the given fields for values. If there is nothing in a field,
     *   focus it and return false.
     * @param fields an array of JTextFields to validate
     * @return whether or not all the fields passed validation
     */
    protected boolean validateFieldItems(JTextField[] fields) {
        for (int i = 0; i < fields.length; i++) {
            if (fields[i].getText().trim().equals("") ||	
                        fields[i].getText().trim().equals(getRequiredText())) {
                fields[i].setText(getRequiredText());
                fields[i].select(0, fields[i].getText().length());
                setFieldFocus(fields[i]);
                Toolkit.getDefaultToolkit().beep();
                return false;
            }
        }
        return true;
    }
     
     


    /**
     * Set the focus to the field in error.  Invoke later is used so
     * a conflict in focus is avoided.
     */

    protected void setFieldFocus(final JTextField j) {
    
        SwingUtilities.invokeLater(new Runnable() {
            @Override
			public void run() {
              j.requestFocus();
         }
        });
    
    }
    
    /**
     * Test the given fields for values. If there is nothing in a field,
     *   focus it and return false.
     * @param boxes an array of JComboBoxes to validate
     * @return whether or not all the fields passed validation
     */
    protected boolean validateFieldItems(JComboBox[] boxes) {
        for (int i = 0; i < boxes.length; i++) {
            //if (boxes[i].getItemCount() > 0) // must have a value
            //    continue;
            if ((boxes[i].getSelectedItem())==null || (boxes[i].getSelectedItem()).toString().trim().equals("") ||	
            		(boxes[i].getSelectedItem()).toString().trim().equals(Messages.getString("MGSWizardPage5.item1")) ||
                        (boxes[i].getSelectedItem()).toString().trim().equals(getRequiredText())) {
                boxes[i].setSelectedItem(getRequiredText());
                final JComboBox jcb = boxes[i];
                // grab the editor and highlight the text
                boxes[i].getEditor().selectAll();
                SwingUtilities.invokeLater(new Runnable() 
                    { @Override
					public void run() {
                        jcb.requestFocus();
                     }});
                // boxes[i].requestFocus();
                Toolkit.getDefaultToolkit().beep();
                return false;
            }
        }
        return true;
    }

    
    protected boolean validateFieldItems(JCheckBox[] checkBoxes) {
        for (int i = 0; i < checkBoxes.length; i++) {
            //if (boxes[i].getItemCount() > 0) // must have a value
            //    continue;
            if (!(checkBoxes[i].isSelected())) {
            	checkBoxes[i].setSelected(Boolean.valueOf(getRequiredText()).booleanValue());
                final JCheckBox jcb = checkBoxes[i];
                // grab the editor and highlight the text
                SwingUtilities.invokeLater(new Runnable() 
                    { @Override
					public void run() {
                        jcb.requestFocus();
                     }});
                Toolkit.getDefaultToolkit().beep();
                return false;
            }
        }
        return true;
    }

    /**
     * Test the given components for values. If there is nothing in a field,
     *   focus it and return false.
     * @param components an array of JComponents to validate
     * @return whether or not all the fields passed validation
     */
    protected boolean validateFieldItems(JComponent[] components) {
        // collect text fields and combo boxes from the list..separating them
        //   into separate lists to validate
        ArrayList<JTextField> texts = new ArrayList<JTextField>();
        ArrayList<JComboBox> boxes = new ArrayList<JComboBox>();
        ArrayList<JCheckBox> checkBoxes = new ArrayList<JCheckBox>();
        for (int i = 0; i < components.length; i++)  {
            if (components[i] instanceof JTextField)
                texts.add((JTextField) components[i]);
            else if (components[i] instanceof JComboBox)
                boxes.add((JComboBox) components[i]);
            else if (components[i] instanceof JCheckBox)
            	checkBoxes.add((JCheckBox) components[i]);
        }

        // validate each of the lists
        if (validateFieldItems(texts.toArray(new JTextField[0])))
            return( validateFieldItems(boxes.toArray(new JComboBox[0])));
            	//	return validateFieldItems((JCheckBox[]) checkBoxes.toArray(new JCheckBox[0]));
        return false;
    }

    /**
     * Validate all the given fields. If one is not valid, display the required
     *   field text and send focus back to that field.
     * @param fields an ArrayList of the fields to be validated
     * @return whether or not all the fields passed validation
     */
    protected boolean validateFieldItems(ArrayList<JComponent> fields) {
        JComponent[] components = fields.toArray(new JComponent[0]);
        return validateFieldItems(components);
    }

    /**
     * Clear out all components contained on this page that accept user input.
     *   Text fields, drop lists, etc.
     */
    protected void clear() {
    	for (Object comp : getAllComponents()) {
            if (comp instanceof JTextField)
                ((JTextField) comp).setText("");	
            else if (comp instanceof MultiListComboBox)
                ((MultiListComboBox) comp).reset();
        }
    }

    /**
     * Get an icon object with the specified name.
     * @param iconName the name of the icon to retrieve.
     * @return the Icon object for the desired icon.
     */
    public Icon getIcon(String iconName) {
        return ImageIconCache.getImageIcon(iconName);
    }

    /**
     * Convenience method for blocking input from all buttons on a panel.
     *   GlassPane seems to only handle mouse input blocking well.
     * @param enabled the status to set on the buttons
     */
    public synchronized void setButtonsEnabled(boolean enabled) {
    	for (Object obj : getAllComponents()) {
            if (obj instanceof JButton)
                ((JButton) obj).setEnabled(enabled);
        }
    }

    /**
     * Get a reference to the component specified by the given name.
     * @param componentName the name of the component in the hierarchy. Matches
     *   the 'name' property value of the element in the XML definition.
     * @return a Component object.
     */
    public Component getComponent(String componentName) {
    	return getComponent(componentName, true);
    }
    
    
    public Component getComponent(String componentName, boolean displayWarning) {
    	if (gui == null) {
    		return null;
    	}
        Object obj = gui.get(componentName);
        if ((obj == null) && (displayWarning)) {
            Debug.println("Warning : could not retrieve component '" +	
                            componentName + "' on panel '" + xmlFileName + "'");	 
        }
        return (Component) obj;
    }

    /**
     * Get a collection of all the components defined in this panel.
     * @return a Collection object containing all the defined components.
     */
    public Collection<Object> getAllComponents() {
        return gui.getAllComponents();
    }

    /**
     * Set the base path to search for XML files.
     * @param path the relative path to search for XML files.
     */
    public static void setXmlPath(String path) {
        xmlPath = path;
    }

    /**
     * Get the base path to locate xml files.
     * @return the base path to locate XML files.
     */
    public static String getXmlPath() {
        return xmlPath;
    }

    /**
     * Add a component to the specified container.
     * @param comp the component to add to the container.
     * @param componentName the name of the new component to add.
     * @param containerName the name of the container component to add the new
     *    component to.
     */
    public void addComponent(Component comp, String componentName,
                                        String containerName) {
        gui.addComponent(comp, componentName, containerName);
    }

    /**
     * Find the parent frame for this panel, and add any frame-level properties
     *   to it that were defined in the XML, such as title, menubar, size, etc.
     *   This method is called after the XMLDefinedPanel is added to a frame.
     */
    public void setupFrameComponents() {
    	Object o = JOptionPane.getFrameForComponent(this);
    	JFrame frame = (o instanceof JFrame) ? (JFrame) JOptionPane.getFrameForComponent(this) : null;
        if (frame == null) {
            Debug.println("Frame properties can not be accessed before the"	
                       + " panel is added to a frame."); 
            return;
        }
        if (gui.containsKey("title")) {	
            StringBuffer title = (StringBuffer) gui.get("title");	
            frame.setTitle(title.toString());
        }
        if (gui.containsKey("menubar")) {	
            JMenuBar menubar = (JMenuBar) gui.get("menubar");	
            frame.setJMenuBar(menubar);
        }
        if (gui.containsKey("width") && gui.containsKey("height")) {	 
            try {
                frame.setSize(
                        Integer.parseInt(gui.get("width").toString()),	
                        Integer.parseInt(gui.get("height").toString()));	
            }
            catch (NumberFormatException e) {
                Debug.println("Ignoring parameters width = "	
                        + gui.get("width") + " height = " + gui.get("height"));   
            }
        }
        else {
            frame.pack();
        }
    }

    /**
     * Try to locate this panel's parent frame and set it's title.
     * @param title the title to place in the frame's title bar.
     */
    public void setTitle(String title) {
        Frame f = JOptionPane.getFrameForComponent(this);
        //Debug.println("f = " + f + "\n f.getClass().getName() = " + f.getClass().getName());
        
        if (f instanceof JFrame) {
            JFrame frame = (JFrame) f; 
            frame.setTitle(title);
           
            // ENH-1133 moved here by GEA
            Debug.println("===== " + title + " " + TimeUtility.currentTime());	 
            //Exception e = new Exception("Stack trace");
            //e.printStackTrace();
        }
        else {
            Debug.println("ignored setTitle(\"" + title + "\") called from");
            Debug.dumpStack();
        }
    }
    
    /**
     * Refresh Menu bar when the MianPanel is refreshed when language toggle  
     * is selected.
     */
    public void refreshMenuBar() {
        JFrame frame = (JFrame) JOptionPane.getFrameForComponent(this);
        JMenuBar menubar = (JMenuBar) gui.get("menubar");   
        frame.setJMenuBar(menubar);
    }

    /**
     * Shift the focus to the component that follows the specified component in
     *   the focus order.
     * @param componentName the name of the component to shift focus from.
     */
    public void focusNextComponent(String componentName) {
        Component component = getComponent(componentName);
        if (component == null)
            return;
        focusNextComponent(component);
    }

    /**
     * Shift the focus to the component that follows the specified component in
     *   the focus order.
     * @param component the component to shift focus from.
     */
    public void focusNextComponent(Component component) {
//        javax.swing.FocusManager.getCurrentManager().focusNextComponent(component);
        java.awt.KeyboardFocusManager.getCurrentKeyboardFocusManager().focusNextComponent(component);
    }

    /**
     * Shift the focus to the component that precedes the specified component in
     *   the focus order.
     * @param componentName the name of the component to shift focus from.
     */
    public void focusPreviousComponent(String componentName) {
        Component component = getComponent(componentName);
        if (component == null)
            return;
        focusPreviousComponent(component);
    }

    /**
     * Shift the focus to the component that precedes the specified component in
     *   the focus order.
     * @param component the component to shift focus from.
     */
    public void focusPreviousComponent(Component component) {
//        javax.swing.FocusManager.getCurrentManager().focusPreviousComponent(component);
        java.awt.KeyboardFocusManager.getCurrentKeyboardFocusManager().focusPreviousComponent(component);
    }

    /**
     * Add an item listener to the given component. Assumes the component is a
     *   JCheckBox.
     * @param componentName the name that specifies the component to add listener on.
     * @param listener the ItemListener to add to the component
     */
    public void addItemListener(String componentName,
                                            ItemListener listener) {
        Component component = getComponent(componentName);
        if (component == null)
            return;
        if (component instanceof JCheckBox)
            ((JCheckBox) component).addItemListener(listener);
        else
            return;
        addToListenerList(itemListenerMap, component, listener);
    }

    /**
     * Remove item listeners from all components.
     */
    public void removeAllItemListeners() {
    	for (Map.Entry<Component, List<EventListener>> entry : itemListenerMap.entrySet()) {
    		Component component = entry.getKey();
            List<EventListener> list = entry.getValue();
            for (EventListener listener : list) {
                if (component instanceof JCheckBox && listener instanceof ItemListener)
                    ((JCheckBox) component).removeItemListener((ItemListener) listener);
                else continue;
            }
        }
        itemListenerMap.clear();
    }

//    /**
//     * Add a list selection listener to the given component. Assumes the
//     *   component is a JTable or a JList.
//     * @param componentName the name of the component to add listener to.
//     * @param listener the ListSelectionListener to add to the component.
//     */
//    public void addSelectionListener(String componentName,
//                                            ListSelectionListener listener) {
//        Component component = getComponent(componentName);
//        if (component == null)
//            return;
//        if (component instanceof JTable)
//            ((JTable) component).getSelectionModel().addListSelectionListener(
//                                                                     listener);
//        else if (component instanceof JList)
//            ((JList) component).addListSelectionListener(listener);
//        else
//            return;
//        addToListenerList(selectionListenerMap, component, listener);
//    }

    /**
     * Remove selection listeners from all components.
     */
    public void removeAllSelectionListeners() {
    	for (Map.Entry<Component, List<EventListener>> entry : selectionListenerMap.entrySet()) {
            JComponent component = (JComponent) entry.getKey();
            List<EventListener> list = entry.getValue();
            for (Iterator<EventListener> j = list.iterator(); j.hasNext(); ) {
            	EventListener listener = j.next();
                if (component instanceof JTable && listener instanceof ListSelectionListener)
                    ((JTable) component).getSelectionModel().removeListSelectionListener((ListSelectionListener) listener);
                else if (component instanceof JList)
                    ((JList) component).removeListSelectionListener((ListSelectionListener) listener);
                else continue;
            }
        }
        selectionListenerMap.clear();
    }

    /**
     * Add a key listener to the given component.
     * @param componentName the name of the component to add the listener to.
     * @param listener the KeyListener to add to the component.
     */
    public void addKeyListener(String componentName, KeyListener listener) {
        Component component = getComponent(componentName);
        addKeyListener(component, listener);
    }

    /**
     * Add a key listener to the given component.
     * @param component the component to add the listener to.
     * @param listener the KeyListener to add to the component;
     */
    public void addKeyListener(Component component, KeyListener listener) {
        if (component == null)
            return;
        component.addKeyListener(listener);
        addToListenerList(keyListenerMap, component, listener);
    }

    /**
     * Remove a KeyListener from a given component.
     * @param componentName the name of the component to pull listener from.
     */
    public void removeKeyListener(String componentName) {
        Component component = getComponent(componentName);
        if (component != null) {
            KeyListener listener = (KeyListener) keyListenerMap.get(component);
            if (listener != null) {
                component.removeKeyListener(listener);
                keyListenerMap.remove(component);
            }
        }
    }

    /**
     * Remove key listeners from all components.
     */
    public void removeAllKeyListeners() {
    	for (Map.Entry<Component, List<EventListener>> entry : keyListenerMap.entrySet()) {
            JComponent component = (JComponent) entry.getKey();
            List<EventListener> list = entry.getValue();
            for (Iterator<EventListener> j = list.iterator(); j.hasNext(); ) {
                KeyListener listener = (KeyListener) j.next();
                component.removeKeyListener(listener);
            }
        }
        keyListenerMap.clear();
    }

    /**
     * Add action listeners to a group of components that invoke a callback
     *   method the same as the component name. Convenience method to slim down
     *   some tedious action listener generation code.
     * ex : component "reverseSetup" calls reverseSetup() on target.
     * @param componentNames an array of Strings containing component names.
     * @param target the object to invoke the actions methods on.
     */
    public void addSameNameActions(String[] componentNames, Object target) {
        for (int i = 0; i < componentNames.length; i++)
            addActionListener(componentNames[i], target, componentNames[i]);
    }

    /**
     * Add an action listener to the given component. The action
     *   listener will call the method <componentName>Action() on the target.
     * ex: component named 'cancelButton' will invoke cancelButtonAction().
     * @param componentName the name of the component to add the listener to.
     * @param invokeTarget the target object on which to call the generated
     *   callback method on.
     */
    public void addActionListener(String componentName,
                                         final Object invokeTarget) {
        addActionListener(componentName, invokeTarget,
                                        componentName + "Action"); 
    }

    /**
     * For the given component name, attempt to locate it on the XML panel,
     *   then add an action listener that calls the given method on the
     *   target specified.
     * @param componentName the name of the component to add the listener to.
     * @param invokeTarget the target object on which to call the generated.
     * @param methodName the name of the method to invoke on the target.
     */
    public void addActionListener(String componentName,
            final Object invokeTarget,
            final String methodName)
    {        
        Component component = getComponent(componentName);
        if (component == null)
            return;

        if (! Callbacks.known(methodName + "()")) {
            return;
        }
        
        try {
            // assuming all 'no-args' methods
            final Method method = invokeTarget.getClass().getMethod(methodName);
            //Debug.track(method);
            ActionListener listener = new ActionListener() {
                // avoid multiple invocations
                private boolean activating = false;
                @Override
				public void actionPerformed(ActionEvent e) {
                    if (activating)
                        return;
                    try {
                        activating = true;
                        method.invoke(invokeTarget);
                    }
                    catch (Exception exc) {
                        Debug.println("Could not invoke method \"" + methodName + "\" on : "  
                                + invokeTarget + " : " + e);	
                        Debug.printStackTrace(exc);
                    }
                    finally {
                        activating = false;
                    }
                }
            };
            addActionListener(componentName, listener);
        }
        catch (Exception ex) {
            Debug.println(ex.toString());
        }
    }

    /**
      * Add a specific action listener to the component with the specified name.
      * @param componentName the name of the component to add the listener to.
      * @param listener the ActionListener to add to the component.
      */
    public void addActionListener(String componentName,
                                                ActionListener listener){
        Component component = getComponent(componentName);
        if (component == null)
            return;
        addActionListener(component, listener);
    }

    /**
     * Add the given action listener to the given component. Store the component
     *   to listener mapping for later removal. Assumes the component is an
     *   AbstractButton, JTextField, JMenuItem, or JComboBox.
     * @param component the component to add the listener to.
     * @param listener the ActionListener to add to the component.
     */
    public void addActionListener(Component component, ActionListener listener){
        // add the action listener to the component
        if (component instanceof AbstractButton)
            ((AbstractButton) component).addActionListener(listener);
        else if (component instanceof JTextField)
            ((JTextField) component).addActionListener(listener);
        else if (component instanceof JMenuItem)
            ((JMenuItem) component).addActionListener(listener);
        else if (component instanceof JComboBox)
            ((JComboBox) component).addActionListener(listener);
        else
            return;
        // store it off for later retrieval and removal
        addToListenerList(actionListenerMap, component, listener);
    }

    /**
     * Add a component / listener mapping to a HashMap of listeners, to be able
     *   to retrieve it later for removal from the component.
     * @param map the HashMap to add the mapping to.
     * @param comp the component to map
     * @param listener the listener object to map to the component.
     */
    private void addToListenerList(Map<Component, List<EventListener>> map, Component comp, EventListener listener) {
        List<EventListener> list = map.get(comp);
        if (list == null) {
            list = new ArrayList<EventListener>();
            map.put(comp, list);
        }
        list.add(listener);
    }

    /**
     * Remove all the action listeners that may have been added to a component.
     *   This will remove only the listeners added with the supplied add methods
     * @param component the component from which to remove all action listeners.
     */
    public void removeActionListeners(Component component) {
        if (component == null)
            return;
        // get the list of action listeners that have been added to the component
        List<EventListener> list = actionListenerMap.get(component);
        if (list == null)
            return;
        // go through the list, removing all the listeners
        for (Iterator<EventListener> i = list.iterator(); i.hasNext(); ) {
            ActionListener listener = (ActionListener) i.next();
            // pull it off of the component
            if (component instanceof AbstractButton)
                ((AbstractButton) component).removeActionListener(listener);
            if (component instanceof JTextField)
                ((JTextField) component).removeActionListener(listener);
            if (component instanceof JMenuItem)
                ((JMenuItem) component).removeActionListener(listener);
            if (component instanceof JComboBox)
                ((JComboBox) component).removeActionListener(listener);
        }
        // get rid of the list and mapping
        actionListenerMap.remove(component);
    }

    /**
     * Remove all the action listeners from all the components.
     */
    public void removeAllActionListeners() {
        ArrayList<Component> keys = new ArrayList<Component>();
        for (Iterator<Component> i = actionListenerMap.keySet().iterator(); i.hasNext();) {
            keys.add(i.next());
        }
        for (Iterator<Component> i = keys.iterator(); i.hasNext(); ) {
            removeActionListeners(i.next());
        }
    }

    /**
     * Add a listener to the text component to determine when text has changed.
     *   When a text change is detected, the method
     *   <componentName>TextValueChanged() will be invoked on the given target.
     * @param componentName the name of the component to add the listener to.
     * @param invokeTarget the targetObject to invoke the callback on
     */
    public void addTextListener(String componentName,
                                         final Object invokeTarget) {
        addTextListener(componentName, invokeTarget,
                                componentName + "TextValueChanged"); 
    }

    /**
     * Add a listener to the text component to determine when text has changed.
     *   When a text change is detected, the method
     *   <componentName>TextValueChanged() will be invoked on the given target.
     * @param methodName the name of the method to be invoked on the target
     * @param componentName the name of the component to add the listener to.
     * @param invokeTarget the targetObject to invoke the callback on
     */
    public void addTextListener(String componentName,
                                final Object invokeTarget, String methodName) {
        Component component = getComponent(componentName);
        if (component == null ||
                 ! (component instanceof JTextComponent))
            return;
        addTextListener((JTextComponent) component, invokeTarget, methodName);
    }

    /**
     * Add a listener to the given named component that will detect when its
     *   text has changed and call the given method on the given target.
     * @param methodName the name of the method to be invoked on the target
     * @param component the component to add the listener to.
     * @param invokeTarget the targetObject to invoke the callback on
     */
    public void addTextListener(JTextComponent component,
            final Object invokeTarget, String methodName) {
        
        if (! Callbacks.known(methodName + "()")) {
            return;
        }
        
        try {
            final Method method = invokeTarget.getClass().getMethod(methodName);
            //Debug.track(method);
            // create an old-school text listener (now deprecated) by using
            //  the new document listener.
            DocumentListener listener = new MyDocumentListener(invokeTarget,
                                                                    method);
            component.getDocument().addDocumentListener(listener);
            addToListenerList(textListenerMap, component, listener);
        }
        catch (Exception ex) {
            Debug.println(ex.toString());
        }
    }

    /**
     * Remove all the text listeners from the mapped text components.
     */
    public void removeAllTextListeners() {
    	for (Map.Entry<Component, List<EventListener>> entry : textListenerMap.entrySet()) {
            JTextComponent component = (JTextComponent) entry.getKey();
            List<EventListener> list = entry.getValue();
            for (Iterator<EventListener> j = list.iterator(); j.hasNext(); ) {
                DocumentListener listener = (DocumentListener) j.next();
                component.getDocument().removeDocumentListener(listener);
            }
        }
        textListenerMap.clear();
    }

    /**
     * Add a listener to determine when focus is gained by the given named
     *   component. When it is, invoke the method <componentName>FocusGained on
     *   the target.
     * @param componentName the name of the component to add the listener to.
     * @param invokeTarget the targetObject to invoke the callback on
     */
    public void addFocusGainedListener(String componentName,
                          Object invokeTarget) {
        addFocusListener(componentName, invokeTarget,
                               componentName + "FocusGained", true); 
    }

    /**
     * Add a listener to determine when focus is gained by the given named
     *   component. When it is, invoke the given method on the target.
     * @param methodName the name of the method to be invoked on the target
     * @param componentName the name of the component to add the listener to.
     * @param invokeTarget the targetObject to invoke the callback on
     */
    public void addFocusGainedListener(String componentName,
                          Object invokeTarget, String methodName) {
        addFocusListener(componentName, invokeTarget, methodName, true);
    }

    /**
     * Add a listener to determine when focus is lost by the given named
     *   component. When it is, invoke the given method on the target.
     * @param methodName the name of the method to be invoked on the target
     * @param componentName the name of the component to add the listener to.
     * @param invokeTarget the targetObject to invoke the callback on
     */
    public void addFocusLostListener(String componentName,
                          Object invokeTarget, String methodName) {
        addFocusListener(componentName, invokeTarget, methodName, false);
    }

    /**
     * Add a focus listener to the component with the specified name. When focus
     *   is gained or lost (depending on gained flag), the given method will be
     *   invoked on the given target.
     * @param methodName the name of the method to be invoked on the target
     * @param componentName the name of the component to add the listener to.
     * @param invokeTarget the targetObject to invoke the callback on
     * @param gained whether or not to create a focus gained listener
     */
    private void addFocusListener(String componentName,
            final Object invokeTarget, String methodName, boolean gained) {

        if (! Callbacks.known(methodName + "()")) {
            return;
        }

        try {
            final Method method = invokeTarget.getClass().getMethod(methodName);
            //Debug.track(method);
            FocusListener listener;
            if (gained)  // create listener to listen for focus gained only
                listener = new FocusAdapter() {
                             @Override
							public void focusGained(FocusEvent e) {
                                 try {
                                     method.invoke(invokeTarget);
                                 }
                                 catch (Exception exc) {
                                     Debug.println(
                                              "Could not invoke method on : " 
                                              + invokeTarget + " : " + e);	
                                     Debug.printStackTrace(exc);
                                 }
                             }};
            else        // create listener to listen for focus lost only
                listener = new FocusAdapter() {
                             @Override
							public void focusLost(FocusEvent e) {
                                 try {
                                     method.invoke(invokeTarget);
                                 }
                                 catch (Exception exc) {
                                     Debug.println(
                                              "Could not invoke method on : " 
                                              + invokeTarget + " : " + e);	
                                     Debug.printStackTrace(exc);
                                 }
                             }};
            addFocusListener(componentName, listener);
        }
        catch (Exception ex) {
            Debug.println(ex.toString());
        }
    }

    /**
     * Add the given FocusListener to the component named by componentName.
     * @param componentName the name of the component to add the listener to.
     * @param listener the listener to add
     */
    public void addFocusListener(String componentName, FocusListener listener){
        Component component = getComponent(componentName);
        if (component == null)
            return;
        addFocusListener(component, listener);
    }

    /**
     * Add the given FocusListener to the given component. Store a mapping
     *   between the two for later removal.
     * @param component the component to add the listener to.
     * @param listener the listener to add
     */
    public void addFocusListener(Component component, FocusListener listener) {
        component.addFocusListener(listener);
        this.addToListenerList(focusListenerMap, component, listener);
    }

    /**
     * Remove FocusListeners from all components that have been mapped to them.
     */
    public void removeAllFocusListeners() {
    	for (Map.Entry<Component, List<EventListener>> entry : focusListenerMap.entrySet()) {
//        for (Iterator<Component> i = focusListenerMap.keySet().iterator(); i.hasNext(); ){
            Component component = entry.getKey();
            List<EventListener> list = entry.getValue();
            for (Iterator<EventListener> j = list.iterator(); j.hasNext(); ) {
                FocusListener listener = (FocusListener) j.next();
                component.removeFocusListener(listener);
            }
        }
        focusListenerMap.clear();
    }

    /**
     * Add the return tab listeners to the panel, excluding the ones listed.
     *   Also add the escape key listeners to the comboboxes. Return tab listeners
     *   allow the return key to act like the tab key for focus traversal. The
     *   escapeMethod and escapeButton are mutually exclusive
     * @param exceptions an array of components that should not have return tab
     *   listeners added to them
     * @param escapeButton the button to activate when ESC is pressed
     * @param escapeTarget the target object to invoke escapeMethod on
     * @param escapeMethod the method to invoke when ESC is pressed
     * @param escapeArg the argument to pass to the escape method when called
     */
    private void addReturnTabListeners(Component[] exceptions,
                                     JButton escapeButton,
                                     Object escapeTarget, String escapeMethod,
                                     int escapeArg) {
        ArrayList<Component> exceptionList = new ArrayList<Component>();
        for (int i = 0; i < exceptions.length; i++)
            exceptionList.add(exceptions[i]);

        for (Object next : getAllComponents()) {

            // Add a ESC key listener to any combobox
            if (next instanceof JComboBox)
                if (escapeButton != null)
                    addKeyListener((Component) next,
                                        new EscapeKeyListener(escapeButton));
                else if (escapeTarget != null)
                    addKeyListener((Component) next,
                             new EscapeKeyListener(escapeTarget, escapeMethod,
                                                   escapeArg));
            if (next instanceof DWComponentInterface) {
                final DWComponentInterface dwComponent = (DWComponentInterface)next;
                if ( exceptionList.contains(next) ){
                    dwComponent.setDoNotHandleEnter();
                   
                }    
                else {
                    dwComponent.setHandleEnter();
                }    
            }
        }
    }

    /**
     * Add return-tab listeners to all components, excluding a group.
     * @param exceptions the group of components to not add them to.
     */
    public void addReturnTabListeners(Component[] exceptions) {
        addReturnTabListeners(exceptions, null, null, null, 0);
    }

    /**
     * Set up the return key to tab through components on the panel.
     */
    public void addReturnTabListeners() {
        addReturnTabListeners(new Component[0]);
    }

    /**
     * Add return-tab listeners to all components, excluding a group.
     * @param exceptions the group of components to not add them to.
     * @param escapeButton the button to activate when escape is pressed.
     */
    public void addReturnTabListeners(Component[] exceptions,
                                                        JButton escapeButton) {
        addReturnTabListeners(exceptions, escapeButton, null, null, 0);

    }

    /**
     * Add return-tab listeners to all components, excluding a group.
     * @param exceptions the group of components to not add them to.
     * @param escapeTarget the target object to invoke escapeMethod on
     * @param escapeMethod the method to invoke when ESC is pressed.
     * @param methodArg the arg to pass to the escape method when called.
     */
    public void addReturnTabListeners(Component[] exceptions,
                     Object escapeTarget, String escapeMethod, int methodArg) {
        addReturnTabListeners(exceptions, null, escapeTarget, escapeMethod,
                                                              methodArg);
    }

    /**
     * Remove all listeners from components that have been mapped to them.
     *   Focus, action, text, key, selection, and item listeners.
     */
    public void removeAllListeners() {
        removeAllFocusListeners();
        removeAllTextListeners();
        removeAllKeyListeners();
        removeAllSelectionListeners();
        removeAllItemListeners();
        removeAllActionListeners();
    }

    /**
     * Clean up listeners and key mappings that were added to this page. Also
     *   unset the focus order that was set.
     */
    public void cleanup() {
        removeAllListeners();
        // pull the keystroke mappings that were added to this page
        KeyMapManager.removeMappings(this);
        // unset any components set in the focus order
        unsetFocusOrder();
    }

    /**
     * Get the location to place this component at so it will be centered on
     *   the screen.
     * @param comp the component to place in the center
     */
    public static Point getLocationForCenter(Component comp) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Point centerLocation = new Point((screenSize.width / 2) -
                                           (comp.getSize().width / 2),
                                         (screenSize.height / 2) -
                                           (comp.getSize().height / 2));
        return centerLocation;
    }

    /**
     * Get the location to place this component at so it will be centered on
     *   its parent component.
     * @param parent the parent component to center the component on
     * @param child the component to place in the center
     */
    public static Point getLocationForCenter(Component parent, Component child){
        if (parent == null)
            return getLocationForCenter(child);
        Point parentCenter = new Point(parent.getLocation().x +
                                               (parent.getSize().width / 2),
                                       parent.getLocation().y +
                                               (parent.getSize().height / 2));
        Point childCenterLocation = new Point(parentCenter.x -
                                                 (child.getSize().width / 2),
                                              parentCenter.y - (
                                                  child.getSize().height / 2));
        return childCenterLocation;
    }

    /**
     * Set the order in which the components should be traversed.
     * @param componentNames an array of the components names in the order to
     *   be traversed
     */
    // removed when we started using JDK 1.4
    public void setFocusOrder(String[] componentNames) {
    /*    JComponent[] components = new JComponent[componentNames.length];
        for (int i = 0; i < componentNames.length; i++) {
            JComponent component = (JComponent) getComponent(componentNames[i]);
            if (component == null) {
                Debug.println("setFocusOrder() component not found : " +	
                   componentNames[i]);
                return;
            }
            components[i] = (JComponent) getComponent(componentNames[i]);
        }
        setFocusOrder(components);
    */
    }

    /**
     * Try to locate the TitledBorder of this panel if it has one.
     * @param panelName the name of the panel to get the titled border from
     * @return the TitledBorder of the panel
     */
    protected TitledBorder getTitledBorder(String panelName) {
        JPanel panel = (JPanel) getComponent(panelName);
        if (panel == null)
            return null;
        Border border = panel.getBorder();
        if (border instanceof CompoundBorder)
            border = ((CompoundBorder) border).getOutsideBorder();
        if (border instanceof TitledBorder)
            return (TitledBorder) border;
        return null;
    }

    /**
     * Set the enabled property of a given component.
     * @param component the component to set the property for.
     * @param enabled whether or not the component should be enabled.
     */
    public void setComponentEnabled(Component component, boolean enabled) {
        setComponentEnabled(component, enabled, false);
    }

    /**
     * Set the enabled status of a component on this page. Also set the same
     *   enabled status of all the subcomponents of this one. If there happens
     *   to be a titledborder on a panel, gray it out also. For a panel, all
     *   child components in it will also be enabled/disabled.
     * @param component the component to set the property for.
     * @param enabled whether or not the component should be enabled.
     * @param grayFieldBackgrounds for disabled text fields, should the background
     *   of the field also change to gray?
     */
    public void setComponentEnabled(Component component, boolean enabled,
                                                 boolean grayFieldBackgrounds) {
        Color titleColor = enabled ? Color.black : Color.gray;
        
        component.setEnabled(enabled);
         // darken the background of the text field if desired
        if (component instanceof JTextField && grayFieldBackgrounds)
            if (enabled)
                component.setBackground(Color.white);
            else
                component.setBackground(SystemColor.control);

        // set the border attributes for a JPanel
        if (component instanceof JPanel) {
            JPanel panel = (JPanel) component;
            Border border = panel.getBorder();
            if (border instanceof CompoundBorder)
                border = ((CompoundBorder) border).getOutsideBorder();
            if (border instanceof TitledBorder)
                ((TitledBorder) border).setTitleColor(titleColor);
        }

        // recurse with container's children
        if (component instanceof Container) {
            Component[] children = ((Container) component).getComponents();
            for (int i = 0; i < children.length; i++)
                setComponentEnabled(children[i], enabled, grayFieldBackgrounds);
        }
    }

    /**
     * Clear out the contents of a given component.
     * @param the component for which to clear the contents.
     */
    protected void clearComponent(Component component) {
        int itemCount;
        // for combo box, remove all items
        /*
        if (component instanceof JComboBox) {
            JComboBox box = (JComboBox) component;
            itemCount = box.getItemCount();
            for (int i = 0; i < itemCount; i++)
                box.removeItemAt(0);
        }
        // for text component, clear the text to ""	
        else if (component instanceof TextComponent)
            ((TextComponent) component).setText("");	
        // for list, remove all items
        else if (component instanceof JList) {
            JList list = (JList) component;
            itemCount = list.getModel().getSize();
            for (int i = 0; i < itemCount; i++)
                list.remove(0);
        }
        // for table, remove all rows
        else 
        */
        if (component instanceof JTable) {
            DefaultTableModel model = (DefaultTableModel)
                             ((JTable) component).getModel();
            itemCount = model.getRowCount();
            for (int i = 0; i < itemCount; i++)
                model.removeRow(0);
        }
    }

    /**
     * Set the focus order of the given components. Store them in a hashmap for
     *   later cleanup by restoring their original focus order. Start the focus
     *   order by focusing the first component.
     * @param components the components in the order they should be traversed
     */

    // removed when we started using JDK 1.4
    public void setFocusOrder(JComponent[] components) {
    /*    for (int i = 0; i < components.length; i++) {
            // store the original next component to restore at cleanup
            while (i+1 < components.length && !components[i+1].isFocusable())
                i++;
            Component next = components[i];
            focusOrderMap.put(components[i], next);
        }
        // start the focus order
        components[0].requestFocus();
    */
    }

    /**
     * Restore the original focus order of all components set in focus order.
     */
    // removed when we started using JDK 1.4
    public void unsetFocusOrder() {
    /*    for (Iterator i = focusOrderMap.keySet().iterator(); i.hasNext(); ) {
            JComponent comp = (JComponent) i.next();
            Component nextComp = (Component) focusOrderMap.get(comp);
            comp.setNextFocusableComponent(nextComp);
        }
        focusOrderMap.clear();
    */
    }

    /**
     * Remove a component from its parent container.
     * @param componentName the component to remove from the parent container
     */
    public void removeComponent(String componentName) {
        Container container = null;
        Component comp = getComponent(componentName);
        
        if (comp != null)
            container = comp.getParent();
        
        if (container != null)
            container.remove(getComponent(componentName));
    }
    
    /**
     * Replace component in the container. This method will remove old component, 
     * create new one and put new component into the same location on the panel where
     * old one were. New component will be of the same class as old one was.
     * @param oldComponentKey is the key use by xmlDefinedPanel to reference component.
     * Note: This method is tested only with text fields. It is created to work around
     * dual focus problem/lost focus problem on a PageFlow(s).
     */
    public void replaceComponent(String oldComponentKey){
        gui.replaceComponent(oldComponentKey);   
        this.paintComponents(this.getGraphics());
    }    
    
    /**
     * DocumentListener that detects when text has changed. When a change is
     *   detected, will call the registered method on the given target.
     */
    private class MyDocumentListener implements DocumentListener {
        private Method method;
        private Object target;
        public MyDocumentListener(Object target, Method method) {
            this.method = method;
            this.target = target;
        }
        @Override
		public void removeUpdate(DocumentEvent e) {
            invoke();
        }
        @Override
		public void insertUpdate(DocumentEvent e) {
            invoke();
        }
        @Override
		public void changedUpdate(DocumentEvent e) {}
        private void invoke() {
            try {
                method.invoke(target);
            }
            catch (Exception ex) {
                Debug.println("Could not invoke method on : " + target);	
                Debug.printStackTrace(ex);
            }
        }
    }

    /**
     * Key listener to be installed on a component to listen for an ESC key and
     *   invoke the given method or button.
     */
    private class EscapeKeyListener extends KeyAdapter {
        private Object invokeTarget = null;
        private Method method = null;
        private int arg = 0;
        private JButton button = null;

        public EscapeKeyListener(final Object invokeTarget, String method,
                int arg) {

            if (! Callbacks.known(method + "(int)")) {
                return;
            }
            
            this.invokeTarget = invokeTarget;
            this.arg = arg;
            try {
                this.method = invokeTarget.getClass().getMethod(method, Integer.TYPE);
                //Debug.track(this.method);
            }
            catch (Exception e) {
                Debug.println("Could not create ESCAPE method : " + method);	
            }
        }

        public EscapeKeyListener(JButton button) {
            this.button = button;
        }

        @Override
		public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                if (method != null) {
                    try {
                        Object[] args = {Integer.valueOf(arg)};
                        method.invoke(invokeTarget, args);
                    }
                    catch (Exception x) {
                        Debug.println("Could not invoke method : " +	
                             method + " on ESCAPE key"); 
                    }
                }
                else
                    button.doClick();

            }
        }
    }

    /**
     * Create a new XMLDefinedPanel and display it in a JFrame.
     */
    public static void main(String[] args) {
        xmlPath = "";	
        Debug.setDebug(true);
        if (args.length < 1) {
            Debug.println("Usage : XmlDefinedPanel <xmlFileName>");	
            return;
        }
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (Exception e) {}

        DeltaworksStartup.fixFonts();
        Messages.setCurrentLocale("en");
        JFrame f = new JFrame();
        f.getContentPane().setLayout(new BorderLayout());
        XmlDefinedPanel panel = new XmlDefinedPanel(args[0]);
        if (args.length > 1) {
            XmlDefinedPanel subpanel = new XmlDefinedPanel(args[1]);
            JPanel p = (JPanel) panel.getComponent("pageFlowPanel");
            p.add(subpanel, "");
        }
        f.getContentPane().add(BorderLayout.CENTER, panel);
        panel.setupFrameComponents();
        f.setSize(DWValues.DW_MINIMUM_WIDTH, DWValues.DW_MINIMUM_HEIGHT);
        f.setTitle("Prototype - " + args[args.length - 1]);
        f.setVisible(true);

        f.addWindowListener(new BasicWindowAdapter());
    }

	public static String getRequiredText() {
		return Messages.getString("XmlDefinedPanel.1923");
	}

    public static class BasicWindowAdapter extends WindowAdapter {
        @Override
		public void windowClosing(WindowEvent e) {
            System.exit(0);
        }
    }
}



