package dw.framework;

import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import com.moneygram.agentconnect.SessionType;

import dw.accountdepositpartners.AccountDepositPartnersTransaction;
import dw.amend.AmendTransaction;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.i18n.Messages;
import dw.mgreceive.MoneyGramReceiveTransaction;
import dw.mgreversal.ReceiveReversalTransaction;
import dw.mgreversal.SendReversalTransaction;
import dw.mgsend.MoneyGramClientTransaction;
import dw.mgsend.MoneyGramSendTransaction;
import dw.model.adapters.SendReversalType;
import dw.profile.UnitProfile;
import dw.utility.DWValues;
import dw.utility.ExtraDebug;

public class SupplementalInformationWizardPage extends FlowPage implements MfaFlowPageInterface, DWTextPaneListener {

	private static final long serialVersionUID = 1L;

	private DataCollectionScreenToolkit toolkit;
	private Object dataCollectionSetChangeFlag;
	private DWTextPane line1Tp = null;
	private JScrollPane scrollPane1;
	private JScrollPane scrollPane2;
	private JPanel panel2;
	private List<DataCollectionPanel> supplementalDataCollectionPanels;
	private MoneyGramClientTransaction transaction;
	private JButton nextButton;

	public SupplementalInformationWizardPage(MoneyGramSendTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/SupplementalInformationWizardPage.xml", name, pageCode, buttons);
		this.transaction = tran;
		init();
	}

	public SupplementalInformationWizardPage(AccountDepositPartnersTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/SupplementalInformationWizardPage.xml", name, pageCode, buttons);
		this.transaction = tran;
		init();
	}

	public SupplementalInformationWizardPage(MoneyGramReceiveTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/SupplementalInformationWizardPage.xml", name, pageCode, buttons);
		this.transaction = tran;
		init();
	}

	public SupplementalInformationWizardPage(SendReversalTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/SupplementalInformationWizardPage.xml", name, pageCode, buttons);
		this.transaction = tran;
		init();
	}
	
	
	public SupplementalInformationWizardPage(AmendTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/SupplementalInformationWizardPage.xml", name, pageCode, buttons);
		this.transaction = tran;
		init();
	}
	
	public SupplementalInformationWizardPage(ReceiveReversalTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/SupplementalInformationWizardPage.xml", name, pageCode, buttons);
		this.transaction = tran;
		init();
	}	

	private void init() {
    	scrollPane1 = (JScrollPane) getComponent("scrollPane1");
    	scrollPane2 = (JScrollPane) getComponent("scrollPane2");
    	panel2 = (JPanel) getComponent("scrollee"); 
        line1Tp = (DWTextPane) getComponent("line1");
    	scrollPane2 = (JScrollPane) getComponent("scrollPane2");
        line1Tp.addListener(this, this);
		nextButton = this.flowButtons.getButton("next");
	}

	@Override
	public void dwTextPaneResized() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				int width = scrollPane2.getViewport().getPreferredSize().width;
				line1Tp.setWidth(width);
			}
		});
	}

    private void setupComponents() {

		String line1Text = Messages.getString("MGRWizardPage4.932");
		DWTextPane tp = (DWTextPane) getComponent("line1");
		tp.setText(line1Text);

		// Check if the GFFP information has changed.
		
		if (dataCollectionSetChangeFlag != transaction.getDataCollectionData().getChangeFlag()) {
			
			// Save a reference to the GFFP information.
			
			dataCollectionSetChangeFlag = transaction.getDataCollectionData().getChangeFlag();
	    	
	    	// Rebuild the data collection items for the screen.
	    	
	    	ExtraDebug.println("Rebuilding Screen " + getPageCode() + " from GFFP");
			int flags = DataCollectionScreenToolkit.SHORT_LABEL_VALUES_FLAG + DataCollectionScreenToolkit.DISPLAY_REQUIRED_FLAG;
			supplementalDataCollectionPanels = transaction.getSupplementalDataCollectionPanels();
			JButton backButton = this.flowButtons.getButton("back");
			toolkit = new DataCollectionScreenToolkit(this, scrollPane1, this, scrollPane2, panel2, flags, supplementalDataCollectionPanels, transaction, backButton, nextButton, transaction.getDestinationCountry());
		}
		
		if (supplementalDataCollectionPanels == null) {
			return;
		}
		
		// Check if the screen needs to be laid out
		
		for (DataCollectionPanel dcp : supplementalDataCollectionPanels) {
			if (dcp.isLayoutFlag()) {
			
				// Display the data collection panels on the screen.
				
				toolkit.populateDataCollectionScreen(panel2, supplementalDataCollectionPanels, transaction.getExcludePanelNames(), false);
				break;
			}
		}
    }

	@Override
    public void start(int direction) {
	
		// Make sure the screen has been built.
		
		setupComponents();
		
		// Setup the flow buttons at the bottom of the screen.
		
		flowButtons.reset();
		flowButtons.setVisible("expert", false); 
		flowButtons.setEnabled("expert", false); 
		
		// Populate the screen with any previously entered data.
		
		if ((toolkit != null) && (direction == PageExitListener.NEXT || direction == PageExitListener.BACK)) {
			List<DataCollectionField> fieldList = toolkit.getScreenFieldList();
			DataCollectionScreenToolkit.restoreDataCollectionFields(fieldList, transaction.getDataCollectionData());
		}
		
		if (transaction instanceof AmendTransaction) {
			flowButtons.getButton("next").setText(Messages.getString("ATWizardPage2.1278"));
		} else if ((transaction instanceof SendReversalTransaction) || (transaction instanceof ReceiveReversalTransaction)) {
			flowButtons.getButton("next").setText(Messages.getString("RRTWizardPage2.revBut"));
		}
		
		// Move the cursor to 1) the first required item with no value or item 
		// with an invalid value or 2) the first optional item with no value.
		
		if (toolkit != null) {
			toolkit.moveCursor(toolkit.getScreenFieldList(), nextButton);
		}
		
		// Enable and call the resize listener.
		
		if (toolkit != null) {
			toolkit.enableResize();
			toolkit.getComponentResizeListener().componentResized(null);
		}
	}
	
	@Override
	public void finish(int direction) {
		
		// Disable the resize listener for this screen.
		
		toolkit.disableResize();
		
    	// If the flow direction is to the next screen, check the validity of the
    	// data. If a value is invalid or a required item does not have a value,
    	// return to the screen.
    	
    	if (direction == PageExitListener.NEXT) {
        	String tag = toolkit.validateData(toolkit.getScreenFieldList(), DataCollectionField.DISPLAY_ERRORS_VALIDATION);
        	if (tag != null) {
				flowButtons.setButtonsEnabled(true);
				toolkit.enableResize();
        		return;
        	}
        }
    	
    	// If the flow direction is to the next screen or back to the previous
    	// screen, save the value of the current items in a data structure.

        if ((direction == PageExitListener.BACK) || (direction == PageExitListener.NEXT)) {
        	DataCollectionScreenToolkit.saveDataCollectionFields(toolkit.getScreenFieldList(), transaction.getDataCollectionData());
        }
		
		// Check if receiveValidation could be performed now or if another page should be displayed.
		
		if (direction == PageExitListener.NEXT) {
			
			if (transaction instanceof SendReversalTransaction) {
				SendReversalTransaction srTransaction = (SendReversalTransaction) transaction; 
		    	SendReversalType reversalType = srTransaction.getSendReversalType();
		    	boolean payoutRequired = reversalType != null ? reversalType.equals(SendReversalType.R) : false;
		    	
		    	if (payoutRequired) {
		            FieldKey key = FieldKey.key("payout1Type");
		            boolean payoutInformationCollected = transaction.getDataCollectionData().getFieldMap().get(key) != null;
			    	int payoutType = srTransaction.getPayoutMethod();
			    	
			    	if ((! payoutInformationCollected) && (payoutType != MoneyGramReceiveTransaction.PAYOUT_CASH)) {
			    		payoutInformationCollected = true;
			    		PageNotification.notifyExitListeners(this, direction);
			    		return;
			    	}
		    	}
			}
			
	    	String nextPage = transaction.nextDataCollectionScreen(this.getPageCode());
	    	if (nextPage.isEmpty() && transaction.getDataCollectionData().isAdditionalInformationScreenValid()) {
				transaction.validation(this);
				return;
			}
		}
		PageNotification.notifyExitListeners(this, direction);
	}
	
	@Override
	public void commComplete(int commTag, Object returnValue) {
		boolean b = returnValue != null && returnValue instanceof Boolean ? ((Boolean) returnValue).booleanValue() : false;
		if (commTag == MoneyGramClientTransaction.TRANSACTION_LOOKUP) {
		} else if ((commTag == MoneyGramClientTransaction.SEND_VALIDATE) || 
				(commTag == MoneyGramClientTransaction.BP_VALIDATE) || 
				(commTag == MoneyGramClientTransaction.RECEIVE_VALIDATION)) {
			flowButtons.setButtonsEnabled(true);
	        PageNotification.notifyExitListeners(this, PageExitListener.NEXT);

		} else if (commTag == MoneyGramClientTransaction.SEND_REVERSAL_VALIDATION) {
			if (b) {
				SendReversalTransaction srTransaction =  (SendReversalTransaction) transaction;
		    	SendReversalType reversalType = srTransaction.getSendReversalType();
		    	boolean payoutRequired = reversalType != null ? reversalType.equals(SendReversalType.R) : false;
				if ((! payoutRequired) && (transaction.getDataCollectionData().getValidationStatus().equals(DataCollectionStatus.VALIDATED))) {
					srTransaction.completeSession(this, SessionType.SREV, transaction.getValidationRequest(), srTransaction.getReferenceNumber());
					return;
				}
			}

		} else if (commTag == MoneyGramClientTransaction.AMEND_VALIDATION) {
			if (b) {
				AmendTransaction amendTransaction =  (AmendTransaction) transaction;
				if (transaction.getDataCollectionData().getValidationStatus().equals(DataCollectionStatus.VALIDATED)) {
					if (UnitProfile.getInstance().isMfaEnabled()) {
			        	MfaAuthorization ma = new MfaAuthorization();
			        	ma.doMfaAuthorization(this);
			        } else {
			        	amendTransaction.completeSession(this, SessionType.AMD, transaction.getValidationRequest(), amendTransaction.getAmendReferenceNumber());
			       }	
					return;
				}
			}

		} else if (commTag == MoneyGramClientTransaction.RECEIVE_REVERSAL_VALIDATION) {
			if (b) {
				ReceiveReversalTransaction receiveReversalTransaction =  (ReceiveReversalTransaction) transaction;
				if (transaction.getDataCollectionData().getValidationStatus().equals(DataCollectionStatus.VALIDATED)) {					
					receiveReversalTransaction.completeSession(this, SessionType.RREV, transaction.getValidationRequest(), receiveReversalTransaction.getReferenceNumber());
					return;
				}
			}

		} else if (commTag == MoneyGramClientTransaction.COMPLETE_SESSION) {
			if (b) {
				if (transaction instanceof MoneyGramReceiveTransaction) {
					MoneyGramReceiveTransaction srTransaction =  (MoneyGramReceiveTransaction) transaction;
					boolean bCancel;
					boolean bSendReversal = transaction instanceof SendReversalTransaction;
					
					if (bSendReversal) {
						bCancel = srTransaction.isCancelAllowed();
					} else if (transaction instanceof ReceiveReversalTransaction) {
						bCancel = true;
					} else {
						bCancel = false;
					}
					srTransaction.print(this, bCancel);
					if ((! UnitProfile.getInstance().isDemoMode()) && (! UnitProfile.getInstance().isTrainingMode())) {
						if (bSendReversal) {
							((SendReversalTransaction) transaction).logTransaction();
						} else if (transaction instanceof ReceiveReversalTransaction) {
							((ReceiveReversalTransaction) transaction).logTransaction();
						}
					}
					return;
				} else if (transaction instanceof AmendTransaction) {
					AmendTransaction amendTransaction =  (AmendTransaction) transaction;
					amendTransaction.printReceipts();
				}
			}
		
		} else if ((commTag == MoneyGramClientTransaction.COMMIT_DONE) || (commTag == MoneyGramClientTransaction.COMMIT_ERROR) || (commTag == MoneyGramClientTransaction.COMMIT_COMMERROR)) {
			if (transaction instanceof MoneyGramReceiveTransaction) {
				MoneyGramReceiveTransaction srTransaction =  (MoneyGramReceiveTransaction) transaction;
				srTransaction.transactionLookup(srTransaction.getReferenceNumber(), null, this, DWValues.TRX_LOOKUP_STATUS, false);
			} else if (transaction instanceof AmendTransaction) {
				AmendTransaction amendTransaction =  (AmendTransaction) transaction;
				amendTransaction.transactionLookup(amendTransaction.getAmendReferenceNumber(), this, DWValues.TRX_LOOKUP_STATUS, false);
			}
			return;
		}
			
		flowButtons.setButtonsEnabled(true);
	    PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
	}

	@Override
	public void commit() {
		if(transaction instanceof AmendTransaction) {
		AmendTransaction amendTransaction =  (AmendTransaction) transaction;
		amendTransaction.completeSession(this, SessionType.AMD, transaction.getValidationRequest(), amendTransaction.getAmendReferenceNumber());
		}		
	}

	@Override
	public void deauthorize() {
		if(transaction instanceof AmendTransaction) {
			AmendTransaction amendTransaction =  (AmendTransaction) transaction;
			deauthorize(amendTransaction);
	        flowButtons.disable();
	        flowButtons.getButton("cancel").setEnabled(true);
		}
	}

	@Override
	public void abort() {
	}

	@Override
	public void cancel() {
	}
}
