package dw.framework;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import com.moneygram.agentconnect.Response;
import com.moneygram.agentconnect.SessionType;

import dw.accountdepositpartners.AccountDepositPartnersTransaction;
import dw.amend.AmendTransaction;
import dw.comm.AgentConnect;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.DataCollectionPanel.PanelType;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.DataCollectionSet.DataCollectionType;
import dw.i18n.Messages;
import dw.mgreceive.MoneyGramReceiveTransaction;
import dw.mgreceive.MoneyGramReceiveWizard;
import dw.mgreversal.ReceiveReversalTransaction;
import dw.mgreversal.SendReversalTransaction;
import dw.mgsend.MoneyGramClientTransaction;
import dw.mgsend.MoneyGramSendTransaction;
import dw.mgsend.MoneyGramClientTransaction.ProfileStatus;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;
import dw.model.adapters.ConsumerProfile;
import dw.model.adapters.SendReversalType;
import dw.profile.UnitProfile;
import dw.utility.DWValues;
import dw.utility.ExtraDebug;

public class AdditionalInformationWizardPage extends FlowPage implements MfaFlowPageInterface, DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	private JPanel panel2;
	private JScrollPane scrollPane1;
	private JScrollPane scrollPane2;
	private DataCollectionScreenToolkit toolkit;
	private DWTextPane text1;
	private MoneyGramClientTransaction transaction;

	public AdditionalInformationWizardPage(MoneyGramSendTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/AdditionalInformationWizardPage.xml", name, pageCode, buttons);
		this.transaction = tran;
		init();
	}

	public AdditionalInformationWizardPage(AccountDepositPartnersTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/AdditionalInformationWizardPage.xml", name, pageCode, buttons);
		this.transaction = tran;
		init();
	}

	public AdditionalInformationWizardPage(MoneyGramReceiveTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/AdditionalInformationWizardPage.xml", name, pageCode, buttons);
		this.transaction = tran;
		init();
	}

	public AdditionalInformationWizardPage(SendReversalTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/AdditionalInformationWizardPage.xml", name, pageCode, buttons);
		this.transaction = tran;
		init();
	}
	
	public AdditionalInformationWizardPage(AmendTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/AdditionalInformationWizardPage.xml", name, pageCode, buttons);
		this.transaction = tran;
		init();
	}
	
	public AdditionalInformationWizardPage(ReceiveReversalTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/AdditionalInformationWizardPage.xml", name, pageCode, buttons);
		this.transaction = tran;
		init();
	}	

	@Override
	public void dwTextPaneResized() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				int width = scrollPane2.getPreferredSize().width - (DataCollectionScreenToolkit.BORDER_WIDTH * 2);
				text1.setWidth(width);
			}
		});
	}
	
	private void init() {
		panel2 = (JPanel) this.getComponent("panel2");
		scrollPane1 = (JScrollPane) this.getComponent("scrollPane1");
		scrollPane2 = (JScrollPane) this.getComponent("scrollPane2");
		text1 = (DWTextPane) this.getComponent("text1");
        text1.addListener(this, this);
	}

	@Override
	public void start(int direction) {
		
		// Set up the flow buttons at the bottom of the screen.
		
        flowButtons.reset();
        flowButtons.setVisible("expert", false);
		flowButtons.setEnabled("expert", false); 
        
        // Get the data collection items returned by the sendValidation AC call.
        
        DataCollectionPanel checkVerifyPanel = new DataCollectionPanel((DataCollectionPanel) null, PanelType.VALICATION);
        checkVerifyPanel.setStandAloneLabel(Messages.getString("MGRWizardPage8a.2"));
        DataCollectionPanel validationPanel = new DataCollectionPanel((DataCollectionPanel) null, PanelType.VALICATION);
        validationPanel.setStandAloneLabel(Messages.getString("MGRWizardPage8a.3"));
        DataCollectionPanel additionalItemsPanel = new DataCollectionPanel((DataCollectionPanel) null, PanelType.DATA_COLLECTION);
        additionalItemsPanel.setStandAloneLabel(Messages.getString("MGRWizardPage8a.4"));
        
		if (transaction.getDataCollectionData().getBusinessErrors() != null) {
			for (DataCollectionField field : transaction.getDataCollectionData().getBusinessErrors()) {
				FieldKey key = field.getKey();
				if (field.getMaximumBusinessErrorCategory() == DataCollectionField.BUSINESS_ERROR_VALIDATION) {
					validationPanel.getChildDataCollectionFields().put(key, field);
				} else {
					checkVerifyPanel.getChildDataCollectionFields().put(key, field);
				}
			}
		}
        
        if (transaction.getDataCollectionData().getAdditionalDataCollectionFields() != null) {
	        for (DataCollectionField field : transaction.getDataCollectionData().getAdditionalDataCollectionFields()) {
	        	if (field.isRequired()) {
	        		additionalItemsPanel.getChildDataCollectionFields().put(field.getKey(), field);
	        	}
	        }
        }
        
		if ((toolkit != null) && (toolkit.getComponentResizeListener() != null)) {
			this.removeComponentListener(toolkit.getComponentResizeListener());
		}
        
    	// Rebuild the data collection items for the screen.
    	
    	ExtraDebug.println("Rebuilding Screen " + getPageCode() + " from GFFP");
		int flags = DataCollectionScreenToolkit.DISPLAY_BUSINESS_ERRORS_FLAG + DataCollectionScreenToolkit.LONG_LABEL_VALUES_FLAG + DataCollectionScreenToolkit.DISPLAY_REQUIRED_FLAG; 
		JButton backButton = this.flowButtons.getButton("back");
		JButton nextButton = this.flowButtons.getButton("next");

		if (transaction instanceof AmendTransaction) {
			flowButtons.getButton("next").setText(Messages.getString("ATWizardPage2.1278"));
		} else if ((transaction instanceof SendReversalTransaction) || (transaction instanceof ReceiveReversalTransaction)) {
			flowButtons.getButton("next").setText(Messages.getString("RRTWizardPage2.revBut"));
		}
		
		List<DataCollectionPanel> panels = new ArrayList<DataCollectionPanel>();
		panels.add(checkVerifyPanel);
		panels.add(validationPanel);
		panels.add(additionalItemsPanel);
		
		toolkit = new DataCollectionScreenToolkit(this, scrollPane1, this, scrollPane2, panel2, flags, panels, transaction, backButton, nextButton, null);
		toolkit.setValidationToolkit();
        
		// Populate the screen with each data collection item.
		
		toolkit.populateDataCollectionScreen(panel2, panels, true);
		toolkit.checkPanelVisibility(panels);

		transaction.getDataCollectionData().setAdditionalInformationScreenFields(toolkit.getFieldList());
		
		// Populate the data collection fields with any existing data.
		
		DataCollectionScreenToolkit.restoreDataCollectionFields(toolkit.getScreenFieldList(), transaction.getDataCollectionData());
		for (DataCollectionField item : toolkit.getScreenFieldList()) {
			item.setOldValue(item.getValue());
		}
		
		// Update the data collection screens since new fields could have been 
		// added by the last validation call.
		
		transaction.setScreensToShow();
		
		// Reset the modify flag on any field on the screen.
		
		transaction.getDataCollectionData().resetModifyFlags();
		
		// Update flag so any previous screen will be completely redrawn.
		
		transaction.getDataCollectionData().updated();
		
		// Call the resize listener.
		
		toolkit.enableResize();
		toolkit.getComponentResizeListener().componentResized(null);
		
		toolkit.moveCursor(toolkit.getScreenFieldList(), nextButton);
	}

	@Override
    public void finish(int direction) {
		flowButtons.setButtonsEnabled(false);

    	// Disable the screen resize listener.
    	
		toolkit.disableResize();

		DataCollectionSet dataSet = transaction.getDataCollectionData();

        // Save the data collected by this screen.
        
        DataCollectionScreenToolkit.saveDataCollectionFields(toolkit.getScreenFieldList(), dataSet);
    	
        if (direction == PageExitListener.NEXT) {
            
        	// Validate the data entered on the screen.
            
        	List<DataCollectionField> list = new ArrayList<DataCollectionField>(toolkit.getFieldList());
        	Collections.sort(list);
        	String tag = toolkit.validateData(list, DataCollectionField.DISPLAY_ERRORS_VALIDATION);
            if (tag != null) {
    			flowButtons.setButtonsEnabled(true);
    	    	toolkit.enableResize();
    			return;
            }
            
        	// Save the list of fields validated.
        	
        	dataSet.getValidationTagList().clear();
        	if (dataSet.getBusinessErrors() != null) {
				for (DataCollectionField item : dataSet.getBusinessErrors()) {
					if (item.getMaximumBusinessErrorCategory() == DataCollectionField.BUSINESS_ERROR_CHECK_VERIFY) {
						if (item.isValidated()) {
							dataSet.getVerifiedFieldsSet().add(item.getInfoKey());
						}
					}
				}
			}
        	
        	DataCollectionType type = transaction.getDataCollectionData().getDataCollectionType();
        	
        	if (type.equals(DataCollectionType.PROFILE_EDIT)) {
        		transaction.createOrUpdateConsumerProfile(transaction.getProfileObject(), this);
        	} else {
        		transaction.validation(this);
        	}
        } else {
        	PageNotification.notifyExitListeners(this, direction);
        }
    }
	
	@Override
	public void commComplete(int commTag, Object returnValue) {
		boolean b = returnValue != null && returnValue instanceof Boolean ? ((Boolean) returnValue).booleanValue() : false;
		if ((commTag == MoneyGramClientTransaction.TRANSACTION_LOOKUP) ||
				(commTag == MoneyGramClientTransaction.SEND_VALIDATE) || 
				(commTag == MoneyGramClientTransaction.BP_VALIDATE) || 
				(commTag == MoneyGramClientTransaction.RECEIVE_VALIDATION)) {
		} else if (commTag == MoneyGramClientTransaction.SEND_REVERSAL_VALIDATION) {
			if (b) {
				SendReversalTransaction srTransaction =  (SendReversalTransaction) transaction;
		    	SendReversalType reversalType = srTransaction.getSendReversalType();
		    	boolean payoutRequired = reversalType != null ? reversalType.equals(SendReversalType.R) : false;
				if ((! payoutRequired) && (transaction.getDataCollectionData().getValidationStatus().equals(DataCollectionStatus.VALIDATED))) {
					srTransaction.completeSession(this, SessionType.SREV, transaction.getValidationRequest(), srTransaction.getReferenceNumber());
					return;
				}
			}
		} else if (commTag == MoneyGramClientTransaction.AMEND_VALIDATION) {
			if (b) {
				AmendTransaction amendTransaction =  (AmendTransaction) transaction;
				if (transaction.getDataCollectionData().getValidationStatus().equals(DataCollectionStatus.VALIDATED)) {
					if (UnitProfile.getInstance().isMfaEnabled()) {
			        	MfaAuthorization ma = new MfaAuthorization();
			        	ma.doMfaAuthorization(this);
			        } else {
			        	amendTransaction.completeSession(this, SessionType.AMD, transaction.getValidationRequest(), amendTransaction.getAmendReferenceNumber());
			       }
					return;
				}
			}
		} else if (commTag == MoneyGramClientTransaction.RECEIVE_REVERSAL_VALIDATION) {
			if (b) {
				ReceiveReversalTransaction receiveReversalTransaction =  (ReceiveReversalTransaction) transaction;
				if (transaction.getDataCollectionData().getValidationStatus().equals(DataCollectionStatus.VALIDATED)) {					
					receiveReversalTransaction.completeSession(this, SessionType.RREV, transaction.getValidationRequest(), receiveReversalTransaction.getReferenceNumber());
					return;
				}
			}
		} else if (commTag == MoneyGramClientTransaction.COMPLETE_SESSION) {
			if (b) {
				if (transaction instanceof MoneyGramReceiveTransaction) {
					MoneyGramReceiveTransaction srTransaction = (MoneyGramReceiveTransaction) transaction;
					srTransaction.print(this, false);
					if ((! UnitProfile.getInstance().isDemoMode()) && (! UnitProfile.getInstance().isTrainingMode())) {
						if (transaction instanceof SendReversalTransaction) {
							((SendReversalTransaction) transaction).logTransaction();
						} else if (transaction instanceof ReceiveReversalTransaction) {
							((ReceiveReversalTransaction) transaction).logTransaction();
						}
					}
					return;
				} else if (transaction instanceof AmendTransaction) {
					AmendTransaction amendTransaction =  (AmendTransaction) transaction;
					amendTransaction.printReceipts();
				}
			}
			
		} else if ((commTag == MoneyGramClientTransaction.COMMIT_DONE) || (commTag == MoneyGramClientTransaction.COMMIT_ERROR) || (commTag == MoneyGramClientTransaction.COMMIT_COMMERROR)) {
			if (transaction instanceof MoneyGramReceiveTransaction) {
				MoneyGramReceiveTransaction srTransaction = (MoneyGramReceiveTransaction) transaction;
				srTransaction.transactionLookup(srTransaction.getReferenceNumber(), null, this, DWValues.TRX_LOOKUP_STATUS, false);
			} else if (transaction instanceof AmendTransaction) {
				AmendTransaction amendTransaction =  (AmendTransaction) transaction;
				amendTransaction.transactionLookup(amendTransaction.getAmendReferenceNumber(), this, DWValues.TRX_LOOKUP_STATUS, false);
			}
			return;
		
		} else if ((commTag == MoneyGramClientTransaction.CREATE_OR_UPDATE_PROFILES_SENDER) || (commTag == MoneyGramClientTransaction.CREATE_OR_UPDATE_PROFILES_RECEIVER)) {
			if (transaction.getDataCollectionData().getValidationStatus().equals(DataCollectionStatus.VALIDATED)) {
				getConsumerProfile();
				return;
			}
		}
		
		flowButtons.setButtonsEnabled(true);
        PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
	}

	@Override
	public void commit() {
		if(transaction instanceof AmendTransaction) {
		AmendTransaction amendTransaction =  (AmendTransaction) transaction;
		amendTransaction.completeSession(this, SessionType.AMD, transaction.getValidationRequest(), amendTransaction.getAmendReferenceNumber());
		}		
	}

	@Override
	public void deauthorize() {
		if(transaction instanceof AmendTransaction) {
			AmendTransaction amendTransaction =  (AmendTransaction) transaction;
			deauthorize(amendTransaction);
	        flowButtons.disable();
	        flowButtons.getButton("cancel").setEnabled(true);
		}
	}

	@Override
	public void abort() {
	}

	@Override
	public void cancel() {
	}
	
	private void getConsumerProfile() {
		final ConsumerProfile profile = transaction.getProfileObject();
		String mgiSessionID = profile.getMgiSessionId();
		transaction.getConsumerProfile(profile, mgiSessionID, AgentConnect.AUTOMATIC, null, new CommCompleteInterface() {
			@Override
			public void commComplete(int commTag, Object returnValue) {
				boolean b = ((returnValue != null) && (returnValue instanceof Boolean)) ? ((Boolean) returnValue) : false;
				if (b) {
					Response response = transaction.getConsumerProfileResponse();
					if (response != null) {
						transaction.setProfileObject(profile);
						transaction.setProfileStatus(ProfileStatus.USE_CURRENT_PROFILE);
						
						if (transaction instanceof MoneyGramReceiveTransaction) {
							receiveValidation1();
						} else {
							PageNotification.notifyExitListeners(AdditionalInformationWizardPage.this, PageExitListener.NEXT);
						}
					}
				} else {
			        flowButtons.disable();
			        flowButtons.getButton("cancel").setEnabled(true);
				}
			}
		});
	}
	
	private void receiveValidation1() {
		final MoneyGramReceiveTransaction receiveTransaction = (MoneyGramReceiveTransaction) transaction;
		
		CommCompleteInterface cci = new CommCompleteInterface() {
			@Override
			public void commComplete(int commTag, Object returnValue) {
				boolean b = ((returnValue != null) && (returnValue instanceof Boolean)) ? ((Boolean) returnValue) : false;
				if (b) {
					
					// This is the first receiveValidation call from this screen. If the there are no data collection screens to show, and the status is not NEED_MORE_DATE or VALIDATED,
					// then make a second receiveValidation call.
					
    				String pseudoPage;
    				if (transaction.getValidationType().equals(ValidationType.SECONDARY)) {
    					pseudoPage = MoneyGramReceiveWizard.MRW59_ADDITIONAL_RECEIVER_INFORMATION;
    				} else {
    					pseudoPage = MoneyGramReceiveWizard.MRW20_RECEIVE_DETAIL;
    				}
					String nextPage = receiveTransaction.nextDataCollectionScreen(pseudoPage);
					
					if ((nextPage.isEmpty()) && 
							(receiveTransaction.getPayoutMethod() == MoneyGramReceiveTransaction.PAYOUT_CASH) && 
							(! transaction.getDataCollectionData().getValidationStatus().equals(DataCollectionStatus.VALIDATED)) && 
							(! transaction.getDataCollectionData().getValidationStatus().equals(DataCollectionStatus.ERROR))) {
						receiveValidation2();
						return;
					}
					
//			    	transaction.getDataCollectionData().clearFieldValueMap();
				}
				PageNotification.notifyExitListeners(AdditionalInformationWizardPage.this, PageExitListener.NEXT);
			}
		};
		
		boolean flag = receiveTransaction.isOKToPrint();
		if (flag) {
			receiveTransaction.receiveValidation(cci, ValidationType.INITIAL_NON_FORM_FREE);
		} else {
			flowButtons.setButtonsEnabled(true);
		}
	}

	private void receiveValidation2() {
		MoneyGramReceiveTransaction receiveTransaction = (MoneyGramReceiveTransaction) transaction;
		
		CommCompleteInterface cci = new CommCompleteInterface() {
			@Override
			public void commComplete(int commTag, Object returnValue) {
				PageNotification.notifyExitListeners(AdditionalInformationWizardPage.this, PageExitListener.NEXT);
			}
		};
		
		receiveTransaction.receiveValidation(cci, ValidationType.SECONDARY);
	}
}
