package dw.framework;

import java.io.Serializable;

public class FieldKey implements Serializable {
	private static final long serialVersionUID = 1L;
	
	// Rewards Enrollment Information Fields
	
	public static final FieldKey CONSUMER_ADDRESS_KEY = FieldKey.key("consumer_Address");
	public static final FieldKey CONSUMER_CITY_KEY = FieldKey.key("consumer_City");
	public static final FieldKey CONSUMER_COUNTRY_KEY = FieldKey.key("consumer_Country");
	public static final FieldKey CONSUMER_DOB_KEY = FieldKey.key("consumer_DOB");
	public static final FieldKey CONSUMER_FIRSTNAME_KEY = FieldKey.key("consumer_FirstName"); 
	public static final FieldKey CONSUMER_HOMEPHONE_KEY = FieldKey.key("consumer_PrimaryPhone");
	public static final FieldKey CONSUMER_LASTNAME2_KEY = FieldKey.key("consumer_LastName2");
	public static final FieldKey CONSUMER_LASTNAME_KEY = FieldKey.key("consumer_LastName");
	public static final FieldKey CONSUMER_MIDDLENAME_KEY = FieldKey.key("consumer_MiddleName");
	public static final FieldKey CONSUMER_NAMESUFFIX_KEY = FieldKey.key("consumer_NameSuffix");
	public static final FieldKey CONSUMER_NAMESUFFIXOTHER_KEY = FieldKey.key("consumer_NameSuffixOther");
	public static final FieldKey CONSUMER_POSTALCODE_KEY = FieldKey.key("consumer_PostalCode");
	public static final FieldKey CONSUMER_REFERENCENUMBER_KEY = FieldKey.key("consumer_ReferenceNumber");
	
	// Documented in getAllFields
	
	public static final FieldKey ACCOUNTNUMBERRETRYCOUNT_KEY = FieldKey.key("accountNumberRetryCount");
	public static final FieldKey BILLER_ACCOUNTNUMBER_KEY = FieldKey.key("biller_AccountNumber");
	public static final FieldKey CARD_EXPIRATION_MONTH_KEY = FieldKey.key("card_Expiration_Month");
	public static final FieldKey CARD_EXPIRATION_YEAR_KEY = FieldKey.key("card_Expiration_Year");
	public static final FieldKey CARD_SWIPEDFLAG_KEY = FieldKey.key("card_SwipedFlag");
	public static final FieldKey CUSTOMERRECEIVENUMBER_KEY = FieldKey.key("customerReceiveNumber");
	public static final FieldKey DATETIMESENT_KEY = FieldKey.key("dateTimeSent");
	public static final FieldKey DELIVERYOPTIONID_KEY = FieldKey.key("deliveryOptionId");
	public static final FieldKey DESTINATION_COUNTRY_KEY = FieldKey.key("destinationCountry");
	public static final FieldKey DESTINATION_COUNTRY_SUBDIVISIONCODE_KEY = FieldKey.key("destination_Country_SubdivisionCode");
	public static final FieldKey DIRECTION1_KEY = FieldKey.key("direction1");
	public static final FieldKey DIRECTION2_KEY = FieldKey.key("direction2");
	public static final FieldKey DIRECTION3_KEY = FieldKey.key("direction3");
	public static final FieldKey FEEAMOUNT_KEY = FieldKey.key("feeAmount");
	public static final FieldKey FIRSTCHECKAUTH_KEY = FieldKey.key("firstCheckAuth");
	public static final FieldKey INTERNATIONALBANKACCOUNTNUMBER_KEY = FieldKey.key("internationalBankAccountNumber");
	public static final FieldKey MESSAGEFIELD1_KEY = FieldKey.key("messageField1");
	public static final FieldKey MESSAGEFIELD2_KEY = FieldKey.key("messageField2");
	public static final FieldKey MGCUSTOMERRECEIVENUMBER_KEY = FieldKey.key("mgCustomerReceiveNumber");
	public static final FieldKey MGIREWARDSNUMBER_KEY = FieldKey.key("mgiRewardsNumber");
	public static final FieldKey MGISESSIONID_KEY = FieldKey.key("mgiSessionID");
	public static final FieldKey NOTOKFORPICKUPREASONCODE_KEY = FieldKey.key("notOkForPickupReasonCode");
	public static final FieldKey OKFORPICKUPFLAG_KEY = FieldKey.key("okForPickupFlag");
	public static final FieldKey OPERATORNAME_KEY = FieldKey.key("operatorName");
	public static final FieldKey ORIGINATINGCOUNTRY_KEY = FieldKey.key("originatingCountry");
	public static final FieldKey OTHERPAYOUTAMOUNT_KEY = FieldKey.key("otherPayoutAmount");
	public static final FieldKey OTHERPAYOUTTYPE_KEY = FieldKey.key("otherPayoutType");
	public static final FieldKey PAYOUT1_CHECKAMOUNT_KEY = FieldKey.key("payout1_CheckAmount");
	public static final FieldKey PAYOUT1_CHECKNUMBER_KEY = FieldKey.key("payout1_CheckNumber");
	public static final FieldKey PAYOUT1_TYPE_KEY = FieldKey.key("payout1_Type");
	public static final FieldKey PAYOUT2_CHECKAMOUNT_KEY = FieldKey.key("payout2_CheckAmount");
	public static final FieldKey PAYOUT2_CHECKNUMBER_KEY = FieldKey.key("payout2_CheckNumber");
	public static final FieldKey PAYOUT2_TYPE_KEY = FieldKey.key("payout2_Type");
	public static final FieldKey PRODUCTVARIANT_KEY = FieldKey.key("productVariant");
	public static final FieldKey PURPOSEOFFUND_KEY = FieldKey.key("purposeOfFund");
	public static final FieldKey RECEIVE_AGENTABBREVIATION_KEY = FieldKey.key("receive_AgentAbbreviation");
	public static final FieldKey RECEIVE_AGENTID_KEY = FieldKey.key("receiveAgentID");
	public static final FieldKey RECEIVE_AGENTNAME_KEY = FieldKey.key("receive_AgentName");
	public static final FieldKey RECEIVE_CODE_KEY = FieldKey.key("receiveCode");
	public static final FieldKey RECEIVE_CURRENCY_KEY = FieldKey.key("receiveCurrency");
	public static final FieldKey RECEIVER_ADDRESS_KEY = FieldKey.key("receiver_Address");
	public static final FieldKey RECEIVER_ADDRESS2_KEY = FieldKey.key("receiver_Address2");
	public static final FieldKey RECEIVER_ADDRESS3_KEY = FieldKey.key("receiver_Address3");
	public static final FieldKey RECEIVER_BIRTH_CITY_KEY = FieldKey.key("receiver_Birth_City");
	public static final FieldKey RECEIVER_BIRTH_COUNTRY_KEY = FieldKey.key("receiver_Birth_Country");
	public static final FieldKey RECEIVER_CITY_KEY = FieldKey.key("receiver_City");
	public static final FieldKey RECEIVER_COUNTRY_KEY = FieldKey.key("receiver_Country");
	public static final FieldKey RECEIVER_COUNTRY_SUBDIVISIONCODE_KEY = FieldKey.key("receiver_Country_SubdivisionCode");
	public static final FieldKey RECEIVER_DOB_KEY = FieldKey.key("receiver_DOB");
	public static final FieldKey RECEIVER_EMAIL_KEY = FieldKey.key("receiver_EmailAddress");
	public static final FieldKey RECEIVER_TITLE_KEY = FieldKey.key("receiver_Title");
	public static final FieldKey RECEIVER_FIRSTNAME_KEY = FieldKey.key("receiver_FirstName");
	public static final FieldKey RECEIVER_LASTNAME_KEY = FieldKey.key("receiver_LastName");
	public static final FieldKey RECEIVER_LASTNAME2_KEY = FieldKey.key("receiver_LastName2");
	public static final FieldKey RECEIVER_MIDDLENAME_KEY = FieldKey.key("receiver_MiddleName");
	public static final FieldKey RECEIVER_NAMESUFFIX_KEY = FieldKey.key("receiver_NameSuffix");
	public static final FieldKey RECEIVER_NAMESUFFIXOTHER_KEY = FieldKey.key("receiver_NameSuffixOther");
	public static final FieldKey RECEIVER_OCCUPATION_KEY = FieldKey.key("receiver_Occupation");
	public static final FieldKey RECEIVER_OCCUPATIONOTHER_KEY = FieldKey.key("receiver_OccupationOther");
	public static final FieldKey RECEIVER_PERSONALID1_CHOICE_TYPE_KEY = FieldKey.key("receiver_PersonalId1_Choice");
	public static final FieldKey RECEIVER_PERSONALID1_CHOICE_VERIFY_KEY = FieldKey.key("receiver_PersonalId1_VerificationStr");
	public static final FieldKey RECEIVER_PERSONALID1_COUNTRY_SUBDIVISIONCODE_KEY = FieldKey.key("receiver_PersonalId1_Country_SubdivisionCode");
	public static final FieldKey RECEIVER_PERSONALID1_EXPIRATION_DAY_KEY = FieldKey.key("receiver_PersonalId1_Expiration_Day");
	public static final FieldKey RECEIVER_PERSONALID1_EXPIRATION_MONTH_KEY = FieldKey.key("receiver_PersonalId1_Expiration_Month");
	public static final FieldKey RECEIVER_PERSONALID1_EXPIRATION_YEAR_KEY = FieldKey.key("receiver_PersonalId1_Expiration_Year");
	public static final FieldKey RECEIVER_PERSONALID1_ISSUE_AUTHORITY_KEY = FieldKey.key("receiver_PersonalId1_Issue_Authority");
	public static final FieldKey RECEIVER_PERSONALID1_ISSUE_CITY_KEY = FieldKey.key("receiver_PersonalId1_Issue_City");
	public static final FieldKey RECEIVER_PERSONALID1_ISSUE_COUNTRY_KEY = FieldKey.key("receiver_PersonalId1_Issue_Country");
	public static final FieldKey RECEIVER_PERSONALID1_ISSUE_COUNTRY_SUBDIVISIONCODE_KEY = FieldKey.key("receiver_PersonalId1_Issue_Country_SubdivisionCode");
	public static final FieldKey RECEIVER_PERSONALID1_ISSUE_DAY_KEY = FieldKey.key("receiver_PersonalId1_Issue_Day");
	public static final FieldKey RECEIVER_PERSONALID1_ISSUE_MONTH_KEY = FieldKey.key("receiver_PersonalId1_Issue_Month");
	public static final FieldKey RECEIVER_PERSONALID1_ISSUE_YEAR_KEY = FieldKey.key("receiver_PersonalId1_Issue_Year");
	public static final FieldKey RECEIVER_PERSONALID1_NUMBER_KEY = FieldKey.key("receiver_PersonalId1_Number");
	public static final FieldKey RECEIVER_PERSONALID1_TYPE_KEY = FieldKey.key("receiver_PersonalId1_Type");
	public static final FieldKey RECEIVER_PERSONALID2_CHOICE_TYPE_KEY = FieldKey.key("receiver_PersonalId2_Choice");
	public static final FieldKey RECEIVER_PERSONALID2_CHOICE_VERIFY_KEY = FieldKey.key("receiver_PersonalId2_VerificationStr");
	public static final FieldKey RECEIVER_PERSONALID2_EXPIRATION_DAY_KEY = FieldKey.key("receiver_PersonalId2_Expiration_Day");
	public static final FieldKey RECEIVER_PERSONALID2_EXPIRATION_MONTH_KEY = FieldKey.key("receiver_PersonalId2_Expiration_Month");
	public static final FieldKey RECEIVER_PERSONALID2_EXPIRATION_YEAR_KEY = FieldKey.key("receiver_PersonalId2_Expiration_Year");
	public static final FieldKey RECEIVER_PERSONALID2_ISSUE_AUTHORITY_KEY = FieldKey.key("receiver_PersonalId2_Issue_Authority");
	public static final FieldKey RECEIVER_PERSONALID2_ISSUE_CITY_KEY = FieldKey.key("receiver_PersonalId2_Issue_City");
	public static final FieldKey RECEIVER_PERSONALID2_ISSUE_COUNTRY_KEY = FieldKey.key("receiver_PersonalId2_Issue_Country");
	public static final FieldKey RECEIVER_PERSONALID2_ISSUE_COUNTRY_SUBDIVISIONCODE_KEY = FieldKey.key("receiver_PersonalId2_Issue_Country_SubdivisionCode");
	public static final FieldKey RECEIVER_PERSONALID2_ISSUE_DAY_KEY = FieldKey.key("receiver_PersonalId2_Issue_Day");
	public static final FieldKey RECEIVER_PERSONALID2_ISSUE_MONTH_KEY = FieldKey.key("receiver_PersonalId2_Issue_Month");
	public static final FieldKey RECEIVER_PERSONALID2_ISSUE_YEAR_KEY = FieldKey.key("receiver_PersonalId2_Issue_Year");
	public static final FieldKey RECEIVER_PERSONALID2_NUMBER_KEY = FieldKey.key("receiver_PersonalId2_Number");
	public static final FieldKey RECEIVER_PERSONALID2_TYPE_KEY = FieldKey.key("receiver_PersonalId2_Type");
	public static final FieldKey RECEIVER_POSTALCODE_KEY = FieldKey.key("receiver_PostalCode");
	public static final FieldKey RECEIVER_PRIMARYPHONE_KEY = FieldKey.key("receiver_PrimaryPhone");
	public static final FieldKey RECEIVER_PRIMARYPHONECOUNTRYCODE_KEY = FieldKey.key("receiver_PrimaryPhoneCountryCode");
	public static final FieldKey RECEIVER_SECONDARYPHONE_KEY = FieldKey.key("receiver_SecondaryPhone");
	public static final FieldKey RECEIVER_SECONDARYPHONECOUNTRYCODE_KEY = FieldKey.key("receiver_SecondaryPhoneCountryCode");
//	private static final FieldKey SEND_AMOUNT_KEY = FieldKey.key("send_Amount");
	public static final FieldKey SEND_AMOUNT_KEY = FieldKey.key("sendAmount");
	public static final FieldKey SEND_CONSUMERID_KEY = FieldKey.key("send_ConsumerId");
	public static final FieldKey SEND_CURRENCY_KEY = FieldKey.key("sendCurrency");
	public static final FieldKey SENDER_ADDRESS_KEY = FieldKey.key("sender_Address");
	public static final FieldKey SENDER_ADDRESS2_KEY = FieldKey.key("sender_Address2");
	public static final FieldKey SENDER_ADDRESS3_KEY = FieldKey.key("sender_Address3");
	public static final FieldKey SENDER_BIRTH_CITY_KEY = FieldKey.key("sender_Birth_City");
	public static final FieldKey SENDER_BIRTH_COUNTRY_KEY = FieldKey.key("sender_Birth_Country");
	public static final FieldKey SENDER_CITY_KEY = FieldKey.key("sender_City");
	public static final FieldKey SENDER_COUNTRY_KEY = FieldKey.key("sender_Country");
	public static final FieldKey SENDER_COUNTRY_SUBDIVISIONCODE_KEY = FieldKey.key("sender_Country_SubdivisionCode");
	public static final FieldKey SENDER_DOB_KEY = FieldKey.key("sender_DOB");
	public static final FieldKey SENDER_EMAIL_KEY = FieldKey.key("sender_EmailAddress");
	public static final FieldKey SENDER_FIRSTNAME_KEY = FieldKey.key("sender_FirstName");
	public static final FieldKey SENDER_LASTNAME_KEY = FieldKey.key("sender_LastName");
	public static final FieldKey SENDER_LASTNAME2_KEY = FieldKey.key("sender_LastName2");
	public static final FieldKey SENDER_MIDDLENAME_KEY = FieldKey.key("sender_MiddleName");
	public static final FieldKey SENDER_NAMESUFFIX_KEY = FieldKey.key("sender_NameSuffix");
	public static final FieldKey SENDER_NAMESUFFIXOTHER_KEY = FieldKey.key("sender_NameSuffixOther");
	public static final FieldKey SENDER_OCCUPATION_KEY = FieldKey.key("sender_Occupation");
	public static final FieldKey SENDER_OCCUPATIONOTHER_KEY = FieldKey.key("sender_OccupationOther");
	public static final FieldKey SENDER_PERSONALID1_CHOICE_TYPE_KEY = FieldKey.key("sender_PersonalId1_Choice");
	public static final FieldKey SENDER_PERSONALID1_CHOICE_VERIFY_KEY = FieldKey.key("sender_PersonalId1_VerificationStr");
	public static final FieldKey SENDER_PERSONALID1_COUNTRY_SUBDIVISIONCODE_KEY = FieldKey.key("sender_PersonalId1_Country_SubdivisionCode");
	public static final FieldKey SENDER_PERSONALID1_EXPIRATION_DAY_KEY = FieldKey.key("sender_PersonalId1_Expiration_Day");
	public static final FieldKey SENDER_PERSONALID1_EXPIRATION_MONTH_KEY = FieldKey.key("sender_PersonalId1_Expiration_Month");
	public static final FieldKey SENDER_PERSONALID1_EXPIRATION_YEAR_KEY = FieldKey.key("sender_PersonalId1_Expiration_Year");
	public static final FieldKey SENDER_PERSONALID1_ISSUE_AUTHORITY_KEY = FieldKey.key("sender_PersonalId1_Issue_Authority");
	public static final FieldKey SENDER_PERSONALID1_ISSUE_CITY_KEY = FieldKey.key("sender_PersonalId1_Issue_City");
	public static final FieldKey SENDER_PERSONALID1_ISSUE_COUNTRY_KEY = FieldKey.key("sender_PersonalId1_Issue_Country");
	public static final FieldKey SENDER_PERSONALID1_ISSUE_COUNTRY_SUBDIVISIONCODE_KEY = FieldKey.key("sender_PersonalId1_Issue_Country_SubdivisionCode");
	public static final FieldKey SENDER_PERSONALID1_ISSUE_DAY_KEY = FieldKey.key("sender_PersonalId1_Issue_Day");
	public static final FieldKey SENDER_PERSONALID1_ISSUE_MONTH_KEY = FieldKey.key("sender_PersonalId1_Issue_Month");
	public static final FieldKey SENDER_PERSONALID1_ISSUE_YEAR_KEY = FieldKey.key("sender_PersonalId1_Issue_Year");
	public static final FieldKey SENDER_PERSONALID1_NUMBER_KEY = FieldKey.key("sender_PersonalId1_Number");
	public static final FieldKey SENDER_PERSONALID1_TYPE_KEY = FieldKey.key("sender_PersonalId1_Type");
	public static final FieldKey SENDER_PERSONALID2_CHOICE_TYPE_KEY = FieldKey.key("sender_PersonalId2_Choice");
	public static final FieldKey SENDER_PERSONALID2_CHOICE_VERIFY_KEY = FieldKey.key("sender_PersonalId2_VerificationStr");
	public static final FieldKey SENDER_PERSONALID2_EXPIRATION_DAY_KEY = FieldKey.key("sender_PersonalId2_Expiration_Day");
	public static final FieldKey SENDER_PERSONALID2_EXPIRATION_MONTH_KEY = FieldKey.key("sender_PersonalId2_Expiration_Month");
	public static final FieldKey SENDER_PERSONALID2_EXPIRATION_YEAR_KEY = FieldKey.key("sender_PersonalId2_Expiration_Year");
	public static final FieldKey SENDER_PERSONALID2_ISSUE_AUTHORITY_KEY = FieldKey.key("sender_PersonalId2_Issue_Authority");
	public static final FieldKey SENDER_PERSONALID2_ISSUE_CITY_KEY = FieldKey.key("sender_PersonalId2_Issue_City");
	public static final FieldKey SENDER_PERSONALID2_ISSUE_COUNTRY_KEY = FieldKey.key("sender_PersonalId2_Issue_Country");
	public static final FieldKey SENDER_PERSONALID2_ISSUE_COUNTRY_SUBDIVISIONCODE_KEY = FieldKey.key("sender_PersonalId2_Issue_Country_SubdivisionCode");
	public static final FieldKey SENDER_PERSONALID2_ISSUE_DAY_KEY = FieldKey.key("sender_PersonalId2_Issue_Day");
	public static final FieldKey SENDER_PERSONALID2_ISSUE_MONTH_KEY = FieldKey.key("sender_PersonalId2_Issue_Month");
	public static final FieldKey SENDER_PERSONALID2_ISSUE_YEAR_KEY = FieldKey.key("sender_PersonalId2_Issue_Year");
	public static final FieldKey SENDER_PERSONALID2_NUMBER_KEY = FieldKey.key("sender_PersonalId2_Number");
	public static final FieldKey SENDER_PERSONALID2_TYPE_KEY = FieldKey.key("sender_PersonalId2_Type");
	public static final FieldKey SENDER_POSTALCODE_KEY = FieldKey.key("sender_PostalCode");
	public static final FieldKey SENDER_PRIMARYPHONE_KEY = FieldKey.key("sender_PrimaryPhone");
	public static final FieldKey SENDER_PRIMARYPHONECOUNTRYCODE_KEY = FieldKey.key("sender_PrimaryPhoneCountryCode");
	public static final FieldKey SENDER_SECONDARYPHONE_KEY = FieldKey.key("sender_SecondaryPhone");
	public static final FieldKey SENDER_SECONDARYPHONECOUNTRYCODE_KEY = FieldKey.key("sender_SecondaryPhoneCountryCode");
	public static final FieldKey SEND_PURPOSEOFTRANSACTION_KEY = FieldKey.key("send_PurposeOfTransaction");
	public static final FieldKey SEND_PURPOSEOFTRANSACTIONOTHER_KEY = FieldKey.key("send_PurposeOfTransactionOther");
	public static final FieldKey SERVICEOPTION_KEY = FieldKey.key("serviceOption");
	public static final FieldKey TESTANSWER_KEY = FieldKey.key("testAnswer");
	public static final FieldKey TESTQUESTION_KEY = FieldKey.key("testQuestion");
	public static final FieldKey THIRDPARTY_RECEIVER_ADDRESS_KEY = FieldKey.key("thirdParty_Receiver_Address");
	public static final FieldKey THIRDPARTY_RECEIVER_CITY_KEY = FieldKey.key("thirdParty_Receiver_City");
	public static final FieldKey THIRDPARTY_RECEIVER_COUNTRY_KEY = FieldKey.key("thirdParty_Receiver_Country");
	public static final FieldKey THIRDPARTY_RECEIVER_COUNTRY_SUBDIVISIONCODE_KEY = FieldKey.key("thirdParty_Receiver_Country_SubdivisionCode");
	public static final FieldKey THIRDPARTY_RECEIVER_DOB_KEY = FieldKey.key("thirdParty_Receiver_DOB");
	public static final FieldKey THIRDPARTY_RECEIVER_FIRSTNAME_KEY = FieldKey.key("thirdParty_Receiver_FirstName");
	public static final FieldKey THIRDPARTY_RECEIVER_LASTNAME_KEY = FieldKey.key("thirdParty_Receiver_LastName");
	public static final FieldKey THIRDPARTY_RECEIVER_OCCUPATION_KEY = FieldKey.key("thirdParty_Receiver_Occupation");
	public static final FieldKey THIRDPARTY_RECEIVER_OCCUPATIONOTHER_KEY = FieldKey.key("thirdParty_Receiver_OccupationOther");
	public static final FieldKey THIRDPARTY_RECEIVER_ORGANIZATION_KEY = FieldKey.key("thirdParty_Receiver_Organization");
	public static final FieldKey THIRDPARTY_RECEIVER_PERSONALID2_NUMBER_KEY = FieldKey.key("thirdParty_Receiver_PersonalId2_Number");
	public static final FieldKey THIRDPARTY_RECEIVER_PERSONALID2_TYPE_KEY = FieldKey.key("thirdParty_Receiver_PersonalId2_Type");
	public static final FieldKey THIRDPARTY_RECEIVER_POSTALCODE_KEY = FieldKey.key("thirdParty_Receiver_PostalCode");
	public static final FieldKey THIRDPARTY_SENDER_ADDRESS_KEY = FieldKey.key("thirdParty_Sender_Address");
	public static final FieldKey THIRDPARTY_SENDER_CITY_KEY = FieldKey.key("thirdParty_Sender_City");
	public static final FieldKey THIRDPARTY_SENDER_COUNTRY_KEY = FieldKey.key("thirdParty_Sender_Country");
	public static final FieldKey THIRDPARTY_SENDER_COUNTRY_SUBDIVISIONCODE_KEY = FieldKey.key("thirdParty_Sender_Country_SubdivisionCode");
	public static final FieldKey THIRDPARTY_SENDER_DOB_KEY = FieldKey.key("thirdParty_Sender_DOB");
	public static final FieldKey THIRDPARTY_SENDER_FIRSTNAME_KEY = FieldKey.key("thirdParty_Sender_FirstName");
	public static final FieldKey THIRDPARTY_SENDER_LASTNAME_KEY = FieldKey.key("thirdParty_Sender_LastName");
	public static final FieldKey THIRDPARTY_SENDER_LASTNAME2_KEY = FieldKey.key("thirdParty_Sender_LastName2");
	public static final FieldKey THIRDPARTY_SENDER_MIDDLENAME_KEY = FieldKey.key("thirdParty_Sender_MiddleName");
	public static final FieldKey THIRDPARTY_SENDER_OCCUPATION_KEY = FieldKey.key("thirdParty_Sender_Occupation");
	public static final FieldKey THIRDPARTY_SENDER_OCCUPATIONOTHER_KEY = FieldKey.key("thirdParty_Sender_OccupationOther");
	public static final FieldKey THIRDPARTY_SENDER_ORGANIZATION_KEY = FieldKey.key("thirdParty_Sender_Organization");
	public static final FieldKey THIRDPARTY_SENDER_PERSONALID1_NUMBER_KEY = FieldKey.key("thirdParty_Sender_PersonalId1_Number");
	public static final FieldKey THIRDPARTY_SENDER_PERSONALID1_TYPE_KEY = FieldKey.key("thirdParty_Sender_PersonalId1_Type");
	public static final FieldKey THIRDPARTY_SENDER_PERSONALID2_NUMBER_KEY = FieldKey.key("thirdParty_Sender_PersonalId2_Number");
	public static final FieldKey THIRDPARTY_SENDER_PERSONALID2_TYPE_KEY = FieldKey.key("thirdParty_Sender_PersonalId2_Type");
	public static final FieldKey THIRDPARTY_SENDER_POSTALCODE_KEY = FieldKey.key("thirdParty_Sender_PostalCode");

	// Undocumented in getAllFields
	
	public static final FieldKey RECEIVER_ADDRESS4_KEY = FieldKey.key("receiver_Address4");
	public static final FieldKey SENDER_ADDRESS4_KEY = FieldKey.key("sender_Address4");
	public static final FieldKey COLONIA_KEY = FieldKey.key("colonia");
	public static final FieldKey MUNICIPIO_KEY = FieldKey.key("municipio");
	public static final FieldKey MF_VALID_LANG_RCPT_PRIME_KEY = FieldKey.key("primaryReceiptLanguage");
	public static final FieldKey MF_VALID_LANG_RCPT_SECOND_KEY = FieldKey.key("secondaryReceiptLanguage");
	public static final FieldKey SMS_COUNTRY_DIAL_CODE_KEY = FieldKey.key("smsCountryDialCode");
	public static final FieldKey SMS_PHONE_NUMBER_KEY = FieldKey.key("smsPhoneNumber");
	public static final FieldKey REGISTRATION_NUMBER_KEY = FieldKey.key("registrationNumber");
	public static final FieldKey EXPECTED_RECEIVE_AMOUNT_KEY = FieldKey.key("expectedReceiveAmount");
	public static final FieldKey MF_PROD_TYPE_KEY = FieldKey.key("productType");
	public static final FieldKey AGENT_CITY = FieldKey.key("agent_city");
	public static final FieldKey ACCT_NBR_VERIFIED_KEY = FieldKey.key("accountNumberVerified");
	public static final FieldKey ACTIVE_FREQ_CUST_CARD_NUMBER_KEY = FieldKey.key("activeFreqCustCardNumber");
	public static final FieldKey BILLER_NAME_KEY = FieldKey.key("biller_Name");
	public static final FieldKey BILLER_CITY_KEY = FieldKey.key("biller_City");
	public static final FieldKey VALIDATE_ACCOUNT_NBR_KEY = FieldKey.key("validateAccountNumber");
	public static final FieldKey VALID_RECEIVE_CURRENCY_KEY = FieldKey.key("validReceiveCurrency");
	public static final FieldKey SENDER_ACTIVE_FREQ_CUST_CARD_NUMBER = FieldKey.key("activeFreqCustCardNumber");
	public static final FieldKey SENDER_ORGANIZATION = FieldKey.key("sender_organization");
	public static final FieldKey MF_RCPT_VER_CU_SECOND_KEY = FieldKey.key("secondaryConsumerReceiptVersion");
	public static final FieldKey MF_RECV_COUNTRY_KEY = FieldKey.key("receiveCountry");
	public static final FieldKey GFFP_TAG_RRN_KEY = FieldKey.key("customerReceiveNumber");
	public static final FieldKey MF_RCPT_VER_AG_PRIME_KEY = FieldKey.key("primaryAgentReceiptVersion");
	public static final FieldKey MF_RCPT_VER_AG_SECOND_KEY = FieldKey.key("secondaryAgentReceiptVersion");
	public static final FieldKey MF_RCPT_VER_CU_PRIME_KEY = FieldKey.key("primaryConsumerReceiptVersion");
	public static final FieldKey DELIVERY_OPTION_DISPLAY_NAME_KEY = FieldKey.key("deliveryOptionDisplayName");
	public static final FieldKey CONFIRMATION_NBR_KEY = FieldKey.key("confirmationNumber");
	public static final FieldKey DISPLAY_ACCOUNT_ID_KEY = FieldKey.key("displayAccountID");
	public static final FieldKey GFFP_TAG_ACCT_NBR_KEY = FieldKey.key("accountNumber");
	public static final FieldKey MG_CUST_RECV_NBR_KEY = FieldKey.key("mgCustomerReceiveNumber");
	public static final FieldKey OTHER_PAYOUT_ID_NBR_KEY = FieldKey.key("billerAccountNumber");
	public static final FieldKey PAYOUT_CURRENCY_KEY = FieldKey.key("payoutCurrency");
	public static final FieldKey PPC_EXP_TRAN_KEY = FieldKey.key("expeditedTransaction");
	public static final FieldKey RECEIVE_COUNTRY_KEY = FieldKey.key("receive_Country");
	public static final FieldKey RECEIVER_ORGANIZATION_KEY = FieldKey.key("receiver_organization");
	public static final FieldKey SEARCH_CRITERIANAME_KEY = FieldKey.key("search_CriteriaName");

	public static final FieldKey SENDER_GENDER_KEY = FieldKey.key("sender_Gender");
	public static final FieldKey RECEIVER_GENDER_KEY = FieldKey.key("receiver_Gender");
	public static final FieldKey ISTRANSACTIONSTAGED_KEY = FieldKey.key("isTransactionStaged");
	
	public static final FieldKey REFERENCE_NUMBER_TEXT = FieldKey.key("referenceNumberText");

	private final String infoKey;
	private final String parentValue;
	
	private FieldKey(String infoKey, String parentValue) {
		this.infoKey = infoKey;
		this.parentValue = parentValue;
	}
	
	public static FieldKey key(String infoKey) {
		return new FieldKey(infoKey, null);
	}
	
	public static FieldKey key(String infoKey, String parentValue) {
		return new FieldKey(infoKey, parentValue);
	}
	
	@Override
	public boolean equals(Object o) {
		if (! (o instanceof FieldKey)) {
			return false;
		}
		FieldKey fi = (FieldKey) o;
		if (parentValue == null) {
			return this.infoKey.equals(fi.infoKey);
		} else {
			return this.infoKey.equals(fi.infoKey) && this.parentValue.equals(fi.parentValue);
		}
	}
	
	@Override
	public int hashCode() {
		if (parentValue == null) {
			return infoKey.hashCode();
		} else {
			return infoKey.hashCode() | parentValue.hashCode();
		}
	}
	
	@Override
	public String toString() {
		String s = infoKey + (parentValue != null ? ("." + parentValue) : "");
		return s;
	}
	
	public String getInfoKey() {
		return this.infoKey;
	}
	
	public String getParentValue() {
		return this.parentValue;
	}
}
