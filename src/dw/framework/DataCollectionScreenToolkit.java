package dw.framework;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;

import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextString;
import dw.dwgui.MultiListComboBox;
import dw.framework.DataCollectionField.BusinessErrorMessage;
import dw.framework.DataCollectionPanel.PanelType;
import dw.framework.FieldKey;
import dw.i18n.Messages;
import dw.main.DeltaworksMainPanel;
import dw.mgsend.MoneyGramClientTransaction;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.CountryInfo.Country;
import dw.utility.ExtraDebug;

public class DataCollectionScreenToolkit implements Serializable{
	private static final long serialVersionUID = 1L;

	public static final int DISPLAY_REQUIRED_FLAG = 1;
	public static final int SHOW_ALL_CHILD_FIELDS = 2;
	public static final int HIDE_UNUSED_CHILD_FIELDS = 4;
	public static final int SHORT_LABEL_VALUES_FLAG = 16;
	public static final int LONG_LABEL_VALUES_FLAG = 32;
	public static final int DISPLAY_BUSINESS_ERRORS_FLAG = 256;
	
	public static final int COMPONENT_SPACING = 5;
	public static final int COMPONENT_HEIGHT = 23;
	public static final int BORDER_WIDTH = 10;
	public static final char ELEMENT_SEPARATOR = '_';
	private static final int MINIMUM_SCREEN_WIDTH = 440;
	
	public static final Insets INSET_NONE = new Insets(0, 0, 0, 0);
	public static final Insets INSET_RIGHT = new Insets(0, 0, 0, COMPONENT_SPACING);
	public static final Insets INSET_LEFT = new Insets(0, COMPONENT_SPACING, 0, 0);
	public static final Insets INSET_TOP = new Insets(COMPONENT_SPACING, 0, 0, 0);
	public static final Insets INSET_BOTTOM = new Insets(0, 0, COMPONENT_SPACING, 0);
	public static final Insets INSET_TOP_BOTTOM = new Insets(COMPONENT_SPACING, 0, COMPONENT_SPACING, 0);
	public static final Insets INSET_LEFT_BOTTOM = new Insets(0, COMPONENT_SPACING, COMPONENT_SPACING, 0);
	public static final Insets INSET_RIGHT_BOTTOM = new Insets(0, 0, COMPONENT_SPACING, COMPONENT_SPACING);

	private boolean displayRequiredFlag = false;
	private boolean longLabelValueFlag = false;
	private boolean displayBusinessErrorsFlag;
	private boolean showAllChildFields = false;
	
	private transient List<DataCollectionField> screenFieldList;
	private JPanel panel1;
	private JPanel panel2;
	private transient JScrollPane scrollPane1;
	private transient JScrollPane scrollPane2;
	private boolean scrollpaneCheckFlag;
	private transient FlowPage page;
	private int margin;
	private boolean resizeEnabled = false;
	
	private int maximumCategoryLevel;
	private int maximumLabelWidth;
	private int maximumComponentWidth;
	private ComponentResizeListener componentResizeListener;
	private Map<FieldKey, DataCollectionField> screenFieldMap;
	private Map<String, List<DataCollectionField>> searchFieldMap;
	private DataCollectionField headSearchField;
	private Set<DataCollectionField> fieldList; 
	
	public abstract static class DWComboBoxItem {
		
		public DWComboBoxItem() {
		}

		public abstract String getLabel();
		public abstract String getValue();
		public abstract String getLabelShort();
		public abstract String getIdentifier();
	}
	
	public interface DataCollectionComponent extends Comparable<DataCollectionComponent> {
		public static final DecimalFormat SORT_ORDER_FORMAT = new DecimalFormat("00000");
		
		abstract Integer getDisplayOrder();
		abstract String getInfoKey();
		abstract JPanel getPanel();
		abstract DataCollectionComponent getParentField();
		abstract Set<DataCollectionComponent> getChildFieldList();
		abstract void setCollectedStatus(int collectionStatus);
		abstract boolean isChildFieldVisible();
		abstract String getSortString();
	}
	
	public interface DataCollectionFieldValidator extends Serializable {
		public DataCollectionField isValid(int mode);
	}
	
	public static class EnumeratedValue extends DWComboBoxItem implements Comparable<EnumeratedValue>, Serializable {
		private static final long serialVersionUID = 1L;
		protected String label;
		protected String labelShort;
		protected String value;
		
		public EnumeratedValue() {
		}
		
		public EnumeratedValue(String label, String value) {
			this.label = label;
			this.value = value;
		}
		
		public EnumeratedValue(String identifier, String labelShort, String label) {
			this.value = identifier;
			this.label = label;
			this.labelShort = labelShort;
		}
		
		@Override
		public String toString() {
			return this.label;
		}
		
		@Override
		public String getLabel() {
			return this.label;
		}
		
		@Override
		public String getValue() {
			return this.value;
		}
		
		public static EnumeratedValue getInitialEnumeratedValue() {
			String label = Messages.getString("MGSWizardPage5.item1");
			String value = "";
			return new EnumeratedValue(label, value);
		}

		@Override
		public int compareTo(EnumeratedValue ev) {
			return this.label.compareTo(ev.label);
		}

		@Override
		public String getLabelShort() {
			return this.labelShort;
		}

		@Override
		public String getIdentifier() {
			return this.value;
		}
	}
	
	public class ComponentResizeListener extends ComponentAdapter implements Serializable {
		private static final long serialVersionUID = 1L;
		
		@Override
		public void componentResized(ComponentEvent e) {
			if (resizeEnabled) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						DataCollectionScreenToolkit.this.componentResized();
					}
				});
			}
		}
	}
	
	public DataCollectionScreenToolkit(FlowPage page, JScrollPane scrollPane1, JPanel panel1, JScrollPane scrollPane2, JPanel panel2, int flags, List<DataCollectionPanel> dataCollectionPanelList, MoneyGramClientTransaction transaction, JButton backButton, JButton nextButton, String receiveCountry) {
		this.scrollPane1 = scrollPane1;
		this.panel1 = panel1;
		this.scrollPane2 = scrollPane2;
		this.panel2 = panel2;
		this.scrollpaneCheckFlag = true;
		this.page = page;
		
		this.displayRequiredFlag = (flags & DISPLAY_REQUIRED_FLAG) != 0;
		this.longLabelValueFlag = (flags & LONG_LABEL_VALUES_FLAG) != 0;
		this.displayBusinessErrorsFlag = (flags & DISPLAY_BUSINESS_ERRORS_FLAG) != 0;
		this.showAllChildFields = (flags & SHOW_ALL_CHILD_FIELDS) != 0;
		
		// Set the base category level for the panels.
		
		for (DataCollectionPanel panel : dataCollectionPanelList) {
			if (panel != null) {
				if (dataCollectionPanelList.contains(panel.getParentDcp())) {
					continue;
				}
			
				panel.setBaseCategoryLevel(panel.getCategoryLevel());
			}
		}
		
		// Initialize the fields that will be displayed on the screen.
		
		screenFieldMap = new HashMap<FieldKey, DataCollectionField>();
		initializeDataCollectionScreen(scrollPane2, panel2, transaction, dataCollectionPanelList, screenFieldMap, backButton, nextButton, receiveCountry);
		this.screenFieldList = sortScreenFields(dataCollectionPanelList);
		
		// Set up the field labels for the screen.
		
		fieldList = new HashSet<DataCollectionField>(this.screenFieldList);
		for (DataCollectionPanel panel : dataCollectionPanelList) {
			if (panel.getPanelType().equals(PanelType.VALICATION)) {
				for (DataCollectionComponent parentField : panel.getChildDataCollectionFields().values()) {
					if (parentField instanceof DataCollectionField && ((DataCollectionField) parentField).getRelatedFields() != null) {
						fieldList.add((DataCollectionField)parentField);
						fieldList.addAll(((DataCollectionField) parentField).getRelatedFields()); 
					}
				}
			}
		}
		setupLabels(fieldList);
		
		// Calculate a margin to use when laying out the screen.
		
		this.margin = ((this.maximumCategoryLevel + 2) * BORDER_WIDTH);
		
		// Check for any orphan country subdivision fields.

		checkForOrphanStateFields(dataCollectionPanelList);
		
		// Add a listener to resize the screen.
		
		this.componentResizeListener = getResizeListener();
		page.addComponentListener(this.componentResizeListener);
	}
	
	private void checkForOrphanStateFields(List<DataCollectionPanel> dataCollectionPanelList) {
		for (DataCollectionPanel panel : dataCollectionPanelList) {
			if (panel != null) {
				if (panel.getChildDataCollectionPanels().size() > 0) {
					checkForOrphanStateFields(new ArrayList<DataCollectionPanel>(panel.getChildDataCollectionPanels().values()));
				}
				for (DataCollectionField field : panel.getChildDataCollectionFields().values()) {
					if ((field.getDataType() == DataCollectionField.STATE_FIELD_TYPE) && (! field.getParentFieldSpecified())) {
						ExtraDebug.println("Found Orphan country subdivision field - " + field.getInfoKey());
						JComponent oldComponent = field.getDataComponent();
						JComponent newComponent = new JTextField();
						GridBagLayout gbl = (GridBagLayout) field.getPanel().getLayout();
						if (oldComponent != null) {
							GridBagConstraints gbc = gbl.getConstraints(oldComponent);
							field.getPanel().remove(oldComponent);
							field.getPanel().add(newComponent, gbc);
						}
						field.setDataComponent(newComponent);
					}
				}
			}
		}
	}

	private void setupLabels(Set<DataCollectionField> fieldList) {
		setFieldLabelText(fieldList);
		this.maximumCategoryLevel = getMaximumCategoryLevel();
		setLabelWidth(fieldList); 
	}
	
	public ComponentResizeListener getResizeListener() {
		return new ComponentResizeListener();
	}
	
	private int getMaximumCategoryLevel() {
		int maxCategoryLevel = 0;
		for (DataCollectionField field : this.screenFieldList) {
			if ((field.getLayoutType() == DataCollectionField.PAIR_COMPONENT_LAYOUT_FORMAT) && (field.isVisible()) && (field.getLabelComponent() != null)) {
				maxCategoryLevel = Math.max(maxCategoryLevel, field.getCategoryLevel());
			}
		}
		return maxCategoryLevel;
	}
	
	private void setLabelWidth(Set<DataCollectionField> fieldList) {
		this.maximumLabelWidth = 0;
		for (DataCollectionField field : fieldList) {
			if ((field.getLayoutType() == DataCollectionField.PAIR_COMPONENT_LAYOUT_FORMAT || field.getLayoutType() == DataCollectionField.IFSC_LAYOUT_FORMAT) && (field.isVisible()) && (field.getLabelComponent() != null)) {
				DWTextString label = field.getLabelComponent();
				int width = label.getPreferredSize().width + (field.getCategoryLevel() * BORDER_WIDTH);
				if (this.displayRequiredFlag  && (field.isRequired())) {
					width += field.getRequiredLabel().getPreferredSize().width + COMPONENT_SPACING;
				}
				this.maximumLabelWidth = Math.max(this.maximumLabelWidth, width);
			}
		}
		
		for (DataCollectionField field : fieldList) {
			if ((field.getLayoutType() == DataCollectionField.PAIR_COMPONENT_LAYOUT_FORMAT || field.getLayoutType() == DataCollectionField.IFSC_LAYOUT_FORMAT) && (field.getLabelComponent() != null)) {
				int width = this.maximumLabelWidth - (field.getCategoryLevel() * BORDER_WIDTH);
				if (this.displayRequiredFlag  && (field.isRequired())) {
					width -= (field.getRequiredLabel().getPreferredSize().width + COMPONENT_SPACING);
				}
				if (field.getLayoutType() == DataCollectionField.IFSC_LAYOUT_FORMAT) {
					width -= BORDER_WIDTH;
				}
				
				Dimension d = new Dimension(width, COMPONENT_HEIGHT);
				DWTextString label = field.getLabelComponent();
				label.setPreferredSize(d);
				if (field.getValueTypeLabel() != null) {
					if (field.isRequired()) {
						d = new Dimension(width + field.getRequiredLabel().getPreferredSize().width + COMPONENT_SPACING, COMPONENT_HEIGHT);
					}
					field.getValueTypeLabel().setPreferredSize(d);
				}
				label.invalidate();
				
				if (field.getDataType() == DataCollectionField.IFSC_FIELD_TYPE) {
					IfscSupplementalPanel supplementalPanel = field.getIfscSupplementalPanel();
					width += (field.getRequiredLabel().getPreferredSize().width + COMPONENT_SPACING + COMPONENT_SPACING);
					supplementalPanel.setLabelWidth(new Dimension(width, COMPONENT_HEIGHT));
				}
			}
		}
    }
	
	private int getMaximumnComponentWidth(Set<DataCollectionField> fieldList) {
		int width = 0;
		for (DataCollectionField field : fieldList) {
			if (field.isVisible()) {
				int dataComponentWidth = field.getDataComponentWidth();
				int helpIconWidth = (field.getHelpIconComponent() != null) ? field.getHelpIconComponent().getPreferredSize().width + COMPONENT_SPACING : 0;
				int margin = field.getCategoryLevel() * BORDER_WIDTH;
				width = Math.max(width, dataComponentWidth + helpIconWidth + margin);
				
				if ((field.getDataType() == DataCollectionField.IFSC_FIELD_TYPE) && (field.getIfscSupplementalPanel() != null)) {
					width = Math.max(width, field.getIfscSupplementalPanel().getPanelWidth());
				}
			}
		}
		return width;
	}
	
	private void componentResized() {
		if (resizeEnabled) {
			this.scrollPane2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
			this.panel2.revalidate();
			
			int screenWidth = this.scrollPane1.getSize().width - (BORDER_WIDTH * 2);
	
			int scrollBarWidth = this.scrollPane2.getVerticalScrollBar().getPreferredSize().width;
			int verticalLayoutWidth = this.maximumLabelWidth + this.maximumComponentWidth;
			if (verticalLayoutWidth < MINIMUM_SCREEN_WIDTH) {
				this.maximumComponentWidth = MINIMUM_SCREEN_WIDTH - this.maximumLabelWidth;
			}
	
			int componentWidth = 0;
			
			if (! this.displayBusinessErrorsFlag) {
				
				if ((screenWidth - this.margin) >= verticalLayoutWidth) {
					componentWidth = this.maximumComponentWidth;
				} else {
					componentWidth = screenWidth - this.margin - this.maximumLabelWidth;
				}
				
				// Loop through each field that is displayed on the screen.
				
				for (DataCollectionField field : this.fieldList) {
					
					// Determine how the item should be laid out on the screen.
					
					int l = DataCollectionField.LAYOUT_HORIZONTALLY;
					field.layout(l, this.displayRequiredFlag);
					
					// Check if the field has a DWTextPane component. The DWTextPane component can be
					// either the label or the data component. If it does, determine the width of that component.
					
					if (field.getFieldLabel() instanceof DWTextPane) {
						DWTextPane tp = (DWTextPane) field.getFieldLabel();
						int w = Math.min((screenWidth - this.margin), (this.maximumLabelWidth + this.maximumComponentWidth));
						if (field.getLayoutType() == DataCollectionField.BOOLEAN_LAYOUT_FORMAT) {
							w -= (field.getDataComponent().getPreferredSize().width + DataCollectionScreenToolkit.COMPONENT_SPACING);
						}
						if (field.getHelpIconLabel() != null) {
							w -= (field.getHelpIconLabel().getPreferredSize().width + DataCollectionScreenToolkit.COMPONENT_SPACING);
						}
						tp.setWidth(w);
						if (field.getDataType() == DataCollectionField.BOOLEAN_FIELD_TYPE) {
							Dimension d = new Dimension(1, ((JComponent) field.getFieldLabel()).getPreferredSize().height);
							field.getFiller().setPreferredSize(d);
							field.getFiller().revalidate();
							((JComponent) field.getFieldLabel()).validate();
						}
						tp.revalidate();
					} else if (field.getDataComponent() instanceof DWTextPane) {
						DWTextPane tp = (DWTextPane) field.getDataComponent();
						tp.setWidth(componentWidth);
						Dimension d = tp.getPreferredSize(componentWidth);
						d = new Dimension(d.width, Math.max((COMPONENT_HEIGHT + COMPONENT_SPACING), d.height));
						field.getPanel().setPreferredSize(d);
						field.getPanel().revalidate();
						tp.repaint();
					} else if (field.getDataType() == DataCollectionField.IFSC_FIELD_TYPE) {
						int w = field.getFieldLabel().getPreferredSize().width + field.getRequiredLabel().getPreferredSize().width + COMPONENT_SPACING + COMPONENT_SPACING;
						Dimension labelDimension = new Dimension(w, field.getFieldLabel().getPreferredSize().height);
						w = field.getDataComponent().getPreferredSize().width + field.getSuffixAndHelpIconWidth();
						Dimension componentDimension = new Dimension(componentWidth, COMPONENT_HEIGHT);
						field.getIfscSupplementalPanel().setWidth(labelDimension, componentDimension);
					} 
					
					if ((field.getLayoutType() == DataCollectionField.PAIR_COMPONENT_LAYOUT_FORMAT) && (! (field.getDataComponent() instanceof DWTextPane))) {
						JComponent component = field.getDataComponent();
						int width = componentWidth - field.getSuffixAndHelpIconWidth(); 
						component.setPreferredSize(new Dimension(width, COMPONENT_HEIGHT));
						component.invalidate();
					} else if (field.getLayoutType() == DataCollectionField.BOOLEAN_LAYOUT_FORMAT) {
						int width = componentWidth - field.getSuffixAndHelpIconWidth() - field.getDataComponent().getPreferredSize().width - DataCollectionScreenToolkit.COMPONENT_SPACING;
						field.getLabelComponent().setPreferredSize(new Dimension(width, COMPONENT_HEIGHT));
					} else if (field.getLayoutType() == DataCollectionField.IFSC_LAYOUT_FORMAT) {
						JComponent component = field.getDataComponent();
						int width = componentWidth - field.getSuffixAndHelpIconWidth(); 
						component.setPreferredSize(new Dimension(width, COMPONENT_HEIGHT));
						component.invalidate();
					}
				}
				
				// Check if a vertical scroll bar needs to be displayed.
				
				boolean scrollBarFlag = this.page.scrollpaneResized(this.scrollPane1, this.scrollPane2, this.panel2, false);
				
				if (scrollBarFlag) {
					for (DataCollectionField field : this.screenFieldList) {
						JComponent component = field.getDataComponent();
						int width = componentWidth - field.getSuffixAndHelpIconWidth() - scrollBarWidth; 
						if (field.getLayoutType() == DataCollectionField.PAIR_COMPONENT_LAYOUT_FORMAT) {
							component.setPreferredSize(new Dimension(width, COMPONENT_HEIGHT));
							component.invalidate();
						} else if (field.getLayoutType() == DataCollectionField.BOOLEAN_LAYOUT_FORMAT) {
							width -= (field.getDataComponent().getPreferredSize().width + DataCollectionScreenToolkit.COMPONENT_SPACING);
							field.getLabelComponent().setPreferredSize(new Dimension(width, COMPONENT_HEIGHT));
						}
					}
				}
			} else {
				
				if ((screenWidth - this.margin) >= verticalLayoutWidth) {
					componentWidth = this.maximumComponentWidth;
				} else {
					componentWidth = screenWidth - this.margin - this.maximumLabelWidth - 10;
				}
				
				// Loop through each field that is displayed on the screen.
				
				for (DataCollectionField field : this.fieldList) {
//				for (DataCollectionField field : this.screenFieldList) {
					
					// Determine how the item should be laid out on the screen.
					
					int l = DataCollectionField.LAYOUT_HORIZONTALLY;
					int errorCategory = field.getMaximumBusinessErrorCategory();
					if (errorCategory == DataCollectionField.BUSINESS_ERROR_VALIDATION) {
						l = DataCollectionField.LAYOUT_CHECK_VALIDATION;
					} else if (errorCategory == DataCollectionField.BUSINESS_ERROR_CHECK_VERIFY) {
						l = DataCollectionField.LAYOUT_VERIFY;
					}
					
					field.layout(l, this.displayRequiredFlag);
					
					// Check if the field has a DWTextPane component. The DWTextPane component can be
					// either the label or the data component. If it does, determine the width of that component.
					
					if (field.getFieldLabel() instanceof DWTextPane) {
						DWTextPane tp = (DWTextPane) field.getFieldLabel();
						int w = Math.min((screenWidth - this.margin), (this.maximumLabelWidth + this.maximumComponentWidth));
						if (field.getLayoutType() == DataCollectionField.BOOLEAN_LAYOUT_FORMAT) {
							w -= (field.getDataComponent().getPreferredSize().width + DataCollectionScreenToolkit.COMPONENT_SPACING);
						}
						if (field.getHelpIconLabel() != null) {
							w -= (field.getHelpIconLabel().getPreferredSize().width + DataCollectionScreenToolkit.COMPONENT_SPACING);
						}
						tp.setWidth(w);
						if (field.getDataType() == DataCollectionField.BOOLEAN_FIELD_TYPE) {
							Dimension d = new Dimension(1, ((JComponent) field.getFieldLabel()).getPreferredSize().height);
							field.getFiller().setPreferredSize(d);
							field.getFiller().revalidate();
							((JComponent) field.getFieldLabel()).validate();
						}
						tp.revalidate();
					} else if (field.getDataComponent() instanceof DWTextPane) {
						DWTextPane tp = (DWTextPane) field.getDataComponent();
						tp.setWidth(componentWidth);
						Dimension d = tp.getPreferredSize(componentWidth);
						d = new Dimension(d.width, Math.max((COMPONENT_HEIGHT + COMPONENT_SPACING), d.height));
						field.getPanel().setPreferredSize(d);
						field.getPanel().revalidate();
						tp.repaint();
					} else if (field.getDataType() == DataCollectionField.IFSC_FIELD_TYPE) {
						int w = field.getValueTypeLabel().getPreferredSize().width + COMPONENT_SPACING;
						Dimension labelDimension = new Dimension(w, field.getValueTypeLabel().getPreferredSize().height);
						w = screenWidth - field.getIfscSupplementalPanel().getAddressLabel().getPreferredSize().width - (3 * BORDER_WIDTH);
						w = field.getDataComponent().getPreferredSize().width + field.getSuffixAndHelpIconWidth();
						Dimension componentDimension = new Dimension(w, COMPONENT_HEIGHT);
						field.getIfscSupplementalPanel().setWidth(labelDimension, componentDimension);
					} 
					
					// Set the width of any error messages.
					
					if (field.getErrorMessagePanel() != null) {
						int width = this.maximumLabelWidth + componentWidth - (BORDER_WIDTH * 2);
						for (BusinessErrorMessage be : field.getBusinessErrors()) {
							be.getErrorMessageLabel().setWidth(width);
							be.getErrorMessageLabel().revalidate();
						}
					}
	
					if ((field.getLayoutType() == DataCollectionField.PAIR_COMPONENT_LAYOUT_FORMAT) && (! (field.getDataComponent() instanceof DWTextPane))) {
						JComponent component = field.getDataComponent();
						int width = componentWidth - field.getSuffixAndHelpIconWidth() - BORDER_WIDTH; 
						component.setPreferredSize(new Dimension(width, COMPONENT_HEIGHT));
						component.invalidate();
					} else if (field.getLayoutType() == DataCollectionField.BOOLEAN_LAYOUT_FORMAT) {
						int width = componentWidth - field.getSuffixAndHelpIconWidth() - field.getDataComponent().getPreferredSize().width - DataCollectionScreenToolkit.COMPONENT_SPACING;
						field.getLabelComponent().setPreferredSize(new Dimension(width, COMPONENT_HEIGHT));
					} else if (field.getLayoutType() == DataCollectionField.IFSC_LAYOUT_FORMAT) {
						JComponent component = field.getDataComponent();
						int width = componentWidth - field.getSuffixAndHelpIconWidth() - BORDER_WIDTH; 
						component.setPreferredSize(new Dimension(width, COMPONENT_HEIGHT));
						component.invalidate();
					}
				}
				
				// Check if a vertical scroll bar needs to be displayed.
				
				boolean scrollBarFlag = this.page.scrollpaneResized(this.scrollPane1, this.scrollPane2, this.panel2, false);
				
				if (scrollBarFlag) {
					for (DataCollectionField field : this.screenFieldList) {
						JComponent component = field.getDataComponent();
						int width = componentWidth - field.getSuffixAndHelpIconWidth() - scrollBarWidth - BORDER_WIDTH; 
						if (field.getLayoutType() == DataCollectionField.PAIR_COMPONENT_LAYOUT_FORMAT) {
							component.setPreferredSize(new Dimension(width, COMPONENT_HEIGHT));
							component.invalidate();
						} else if (field.getLayoutType() == DataCollectionField.BOOLEAN_LAYOUT_FORMAT) {
							width -= (field.getDataComponent().getPreferredSize().width + DataCollectionScreenToolkit.COMPONENT_SPACING);
							field.getLabelComponent().setPreferredSize(new Dimension(width, COMPONENT_HEIGHT));
						}
					}
				}
			}
	
			this.panel2.revalidate();
			this.panel1.revalidate();
				
			boolean scrollBarFlag = this.page.scrollpaneResized(this.scrollPane1, this.scrollPane2, this.panel2, false);
			if (this.scrollpaneCheckFlag && scrollBarFlag) {
				this.page.scrollpaneResized(this.scrollPane1, this.scrollPane2, this.panel2);
			} else {
				this.panel2.setSize(this.panel2.getPreferredSize());
				this.panel2.invalidate();
				this.scrollPane2.setPreferredSize(this.scrollPane2.getViewport().getPreferredSize());
				this.scrollPane2.invalidate();
				this.page.revalidate();
				this.page.doLayout();
				this.page.repaint();
			}
		}
	}

	private void setMaximumComponentWidth(int maximumComponentWidth) {
		this.maximumComponentWidth = maximumComponentWidth;
	}
	
	public void updateMaximumComponentWidth(int width) {
		this.maximumComponentWidth = Math.max(this.maximumComponentWidth, width);
	}
	
	/**
	 * insert item at top of combo box, and select it
	 */
	private void addFirstSelectItem(MultiListComboBox<? extends DWComboBoxItem> combox, DataCollectionField field) {
		if ((field.getDefaultValue() == null) || (field.getDefaultValue().isEmpty())) {
			DWComboBoxItem firstItem = (DWComboBoxItem) combox.getItemAt(0);
			String firstItemValue = firstItem != null ? firstItem.getValue() : "";
			if (! firstItemValue.isEmpty()) {
				String displayValue = field.isRequired() ? Messages.getString("DWValues.Select") : Messages.getString("DWValues.None");
				Country c = new Country("", displayValue);
				combox.insertItemAt(c, 0);
				DataCollectionField.enablableMofifyTracking(false);
				combox.setSelectedIndex(0);
				DataCollectionField.enablableMofifyTracking(true);
			}
		}
	}

	/**
	 * response event
	 * @param childFields
	 */
	private void eventResponse(Set<DataCollectionComponent>  childFields) {
		checkFieldVisibility(childFields);
		componentResized();
	}

	private void initializeDataCollectionScreen(final JScrollPane scrollPane, final JPanel panel, MoneyGramClientTransaction transaction, List<DataCollectionPanel> panelList, Map<FieldKey, DataCollectionField> screenFieldMap, JButton backButton, JButton nextButton, String receiveCountry) {
		
		// Initialize all of the data collection fields on the screen.
		
		initializeDataCollectionFields(this.page, scrollPane, panel, transaction, panelList, screenFieldMap, backButton, nextButton, receiveCountry);
		
		// Loop through all of the fields on the screen and set up any dependencies.
		
		for (final DataCollectionField field : screenFieldMap.values()) {
			
			// If country type, set up the country list.
			
			if ((field != null) && (field.getDataType() == DataCollectionField.COUNTRY_FIELD_TYPE)) {
				MultiListComboBox<DWComboBoxItem> cb = (MultiListComboBox<DWComboBoxItem>) field.getDataComponent();
				if (cb.getItemCount() == 0) {
					List<Country> countries = CountryInfo.getCountries(field.getLookupKey());
					if (countries != null && countries.size() > 0) {
						DataCollectionField.enablableMofifyTracking(false);
						cb.addList(countries, "");
						DataCollectionField.enablableMofifyTracking(true);
					}
				}
				addFirstSelectItem(cb, field);
			}
			
			if ((field != null) && (field.getDataType() == DataCollectionField.DIAL_CODE_FIELD_TYPE)) {
				MultiListComboBox<DWComboBoxItem> cb = (MultiListComboBox<DWComboBoxItem>) field.getDataComponent();
				if (cb.getItemCount() == 0) {
					DataCollectionField.enablableMofifyTracking(false);
					cb.addList(CountryInfo.getCountryDialCodesList(), "");
					if ((field.getDefaultValue() != null) && (! field.getDefaultValue().isEmpty())) {
				        cb.setSelectedComboBoxItem(field.getDefaultValue());
					} else {
						addFirstSelectItem(cb, field);
					}
					DataCollectionField.enablableMofifyTracking(false);
				}
			}
			
			/*
			 *  For Enum fields set the default value in the list (if one is provided.)
			 */
			if ((field != null) && (field.getDataType() == DataCollectionField.ENUM_FIELD_TYPE)) {
				MultiListComboBox<DWComboBoxItem> cb = (MultiListComboBox<DWComboBoxItem>) field.getDataComponent();
				String sDefaultValue = field.getDefaultValue();
				if (sDefaultValue != null) {
			        cb.setSelectedComboBoxItem(sDefaultValue);
				}
			}
			
			// If the item has children items, add a value change listener.
			
			if ((field != null) && (field.getChildFieldList() != null)) {
				JComponent dataComponent = field.getDataComponent();
				if (dataComponent instanceof JTextField) {
					((JTextField) dataComponent).addKeyListener(new KeyAdapter() {
						@Override
						public void keyTyped(KeyEvent event) {
							eventResponse(field.getChildFieldList());
						}
					});
				} else if (dataComponent instanceof JComboBox) {
					((JComboBox) dataComponent).addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent event) {
							eventResponse(field.getChildFieldList());
						}
					});
				} else if (dataComponent instanceof JCheckBox) {
					((JCheckBox) dataComponent).addItemListener(new ItemListener() {
						@Override
						public void itemStateChanged(ItemEvent event) {
							eventResponse(field.getChildFieldList());
						}
					});
				}
				checkFieldVisibility(field.getChildFieldList());
			}
			
	        // Setup auto scrolling for the item.

	        if ((field != null) && (field.getDataComponent() != null) && (field.isVisible()) && (field.getDataType() != DataCollectionField.TEXT_FIELD_TYPE) && (field.getDataType() != DataCollectionField.VALUE_FIELD_TYPE)) {
	        	field.getDataComponent().addFocusListener(new FocusAdapter() {
					@Override
					public void focusGained(FocusEvent e) {
						Component c = e.getOppositeComponent();
						if ((c != null) && (! (c instanceof JFrame))) {
			        		SwingUtilities.invokeLater(new Runnable() {
			        			@Override
								public void run() {
									field.focus(scrollPane, panel, false);
								}
			        		});
						}
					}
				});
			}
		}
	}

	public void checkPanelVisibility(List<DataCollectionPanel> panels) {
		for (DataCollectionPanel panel : panels) {
			checkFieldVisibility(new HashSet<DataCollectionComponent>(panel.getChildDataCollectionFields().values()));
			checkPanelVisibility(new ArrayList<DataCollectionPanel>(panel.getChildDataCollectionPanels().values()));
		}
	}
	
	private void checkFieldVisibility(Set<DataCollectionComponent> components) {
		for (DataCollectionComponent component : components) {
			if (component.getPanel() == null) {
				continue;
			}
			if (component.getParentField() != null) {
				boolean show = showAllChildFields || component.isChildFieldVisible();
				component.getPanel().setVisible(show);
			} else {
				component.getPanel().setVisible(true);
			}
			if (component.getChildFieldList() != null) {
				checkFieldVisibility(component.getChildFieldList());
			}
		}
	}
	
	private void initializeDataCollectionFields(FlowPage page, final JScrollPane scrollPane, final JPanel panel, MoneyGramClientTransaction transaction, List<DataCollectionPanel> panelList, Map<FieldKey, DataCollectionField> screenFieldMap, JButton backButton, JButton nextButton, String receiveCountry) {
		for (DataCollectionPanel dcp : panelList) {
			if (dcp != null) {
				List<DataCollectionPanel> childPanelList = new ArrayList<DataCollectionPanel>(dcp.getChildDataCollectionPanels().values());
				initializeDataCollectionFields(page, scrollPane, panel, transaction, childPanelList, screenFieldMap, backButton, nextButton, receiveCountry);
				
				for (DataCollectionField field : dcp.getChildDataCollectionFields().values()) {
	
			        // Setup the field so it can display on the screen.
		        
					Container c = DeltaworksMainPanel.getMainPanel().getRootPane();
					c.setFocusCycleRoot(true);
			        field.setupDataCollectionField(screenFieldMap, null, dcp, backButton, nextButton, receiveCountry, this);

					if (dcp.getPanelType().equals(PanelType.VALICATION)) {
						initializeChildFields(field, dcp, screenFieldMap, backButton, nextButton, receiveCountry);
					}
				}
			}
		}
	}
	
	private void initializeChildFields(DataCollectionComponent parentField, DataCollectionPanel dcp, Map<FieldKey, DataCollectionField> screenFieldMap, JButton backButton, JButton nextButton, String receiveCountry) {
		if ((parentField != null) && (parentField.getChildFieldList() != null)) {
			for (DataCollectionComponent childComponent : parentField.getChildFieldList()) {
				if (childComponent instanceof DataCollectionField) {
					DataCollectionField childField = (DataCollectionField) childComponent;
					screenFieldMap.put(childField.getKey(), childField);
					childField.setupDataCollectionField(screenFieldMap, null, dcp, backButton, nextButton, receiveCountry, this);
				} 
	
				if (childComponent.getChildFieldList() != null) {
					initializeChildFields(childComponent, dcp, screenFieldMap, backButton, nextButton, receiveCountry);
				}
			}
		}
	}
	
	public void populateSearchScreen(JPanel panel, DataCollectionPanel dataCollectionPanel) {

		// Clear the screen of all previous panels.
		
		panel.removeAll();
		searchFieldMap = new HashMap<String, List<DataCollectionField>>();
		headSearchField = null;
		
		dataCollectionPanel.setPanel(panel);
		
		for (DataCollectionField field : dataCollectionPanel.getChildDataCollectionFields().values()) {
			String parentValue = field.getParentValue();
			if (parentValue == null) {
				headSearchField = field;
			} else {
				List<DataCollectionField> list = searchFieldMap.get(parentValue);
				if (list == null) {
					list = new ArrayList<DataCollectionField>();
					searchFieldMap.put(parentValue, list);
				}
				list.add(field);
			}
		}
		
		int y1 = 0;
		
		List<EnumeratedValue> list = headSearchField.getEnumeratedValueInfoList(true);
		for (EnumeratedValue ev : list) {
			final String parentValue = ev.getValue();
			
			List<DataCollectionField> list1 = searchFieldMap.get(parentValue);
			if (list1 == null) {
				continue;
			}

			if (y1 != 0) {
				JPanel panel1 = new JPanel();
				panel1.setLayout(new GridBagLayout());
				GridBagConstraints gbc = new GridBagConstraints();
				gbc.gridx = 0;
				gbc.gridy = y1;
				y1++;
				gbc.weightx = 1.0;
				gbc.fill = GridBagConstraints.HORIZONTAL;
				gbc.insets = DataCollectionScreenToolkit.INSET_BOTTOM;
				panel.add(panel1, gbc);

				JLabel label = new JLabel(Messages.getString("BPWizardPage5.1133b"));
				gbc = new GridBagConstraints();
				gbc.gridx = 0;
				gbc.gridy = 0;
				y1++;
				gbc.fill = GridBagConstraints.HORIZONTAL;
				gbc.insets = DataCollectionScreenToolkit.INSET_BOTTOM;
				panel1.add(label, gbc);
			}
			
			JPanel panel1 = new JPanel();
			panel1.setLayout(new GridBagLayout());
			panel1.setBorder(getCompoundTitleBorder(""));
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.gridx = 0;
			gbc.gridy = y1;
			y1++;
			gbc.weightx = 1.0;
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.insets = DataCollectionScreenToolkit.INSET_BOTTOM;
			panel.add(panel1, gbc);
			
			DataCollectionPanel dcp = new DataCollectionPanel(dataCollectionPanel, PanelType.DATA_COLLECTION);
			dcp.setPanel(panel1);
			for (final DataCollectionField field : list1) {
				dcp.getChildDataCollectionFields().put(field.getKey(), field);
				
				JComponent dataComponent = field.getDataComponent();
				if (dataComponent instanceof JTextField) {
					((JTextField) dataComponent).addKeyListener(new KeyAdapter() {
						@Override
						public void keyReleased(KeyEvent event) {
							checkSearchComponents(parentValue);
						}
					});
				} else if (dataComponent instanceof JComboBox) {
					((JComboBox) dataComponent).addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent event) {
							checkSearchComponents(parentValue);
						}
					});
				} else if (dataComponent instanceof JCheckBox) {
					((JCheckBox) dataComponent).addItemListener(new ItemListener() {
						@Override
						public void itemStateChanged(ItemEvent event) {
							checkSearchComponents(parentValue);
						}
					});
				}
			}
			
			populateDataCollectionPanel(dcp, null, false);
		}

		// Set the size of the components on the screen.
		
		setMaximumComponentWidth(getMaximumnComponentWidth(fieldList));
	}
	
	private void checkSearchComponents(String parentValue) {
		
		boolean groupHasValues = false;
		List<DataCollectionField> list = searchFieldMap.get(parentValue);
		for (DataCollectionField field : list) {
			String value = field.getDataComponent() instanceof JTextField ? ((JTextField) field.getDataComponent()).getText() : field.getValue();
			if ((value != null) && (! value.isEmpty())) {
				groupHasValues = true;
				break;
			}
		}
		
		if (headSearchField != null) {
			if (groupHasValues) {
				headSearchField.setValue(parentValue);
			} else {
				headSearchField.setValue("");
			}
		}
		for (Map.Entry<String, List<DataCollectionField>> entry : searchFieldMap.entrySet()) {
			List<DataCollectionField> list1 = entry.getValue();
			boolean enabled = ! groupHasValues || (parentValue.equals(entry.getKey()));
			for (DataCollectionField field1 : list1) {
				field1.getDataComponent().setEnabled(enabled);
			}
		}
	}
	
	public void populateDataCollectionScreen(JPanel panel, List<DataCollectionPanel> includedPanels, boolean invalidateLayoutFlag) {
		populateDataCollectionScreen(panel, includedPanels, null, invalidateLayoutFlag);
	}
	
	public void populateDataCollectionScreen(JPanel panel, List<DataCollectionPanel> includedPanels, Set<String> excludedPanels, boolean invalidateLayoutFlag) {
		
		// Clear the screen of all previous panels.
		
		panel.removeAll();
		
		// Loop through each panel on the screen.
		
		int y = 0;
		
		Collections.sort(includedPanels);
		
		for (DataCollectionPanel dcp : includedPanels) {
			if (dcp != null) {

				// Check if panel is a child of another panel on the screen. If it is, skip the panel.
				
				if (includedPanels.contains(dcp.getParentDcp())) {
					continue;
				}
				
				ExtraDebug.println("Populate panel - " + dcp.getInfoKey());
				
				// Check if there are an visible fields in the panel. If there are
				// none, do not add the panel to the screen.
	
				if (! dcp.isVisible()) {
					continue;
				}
					
				// Create the panel and place it on the screen.
				
				dcp.setPanel(new JPanel(new GridBagLayout()));
				GridBagConstraints gbc = new GridBagConstraints();
				gbc.gridx = 0;
				gbc.gridy = y;
				y++;
				gbc.weightx = 1.0;
				gbc.fill = GridBagConstraints.HORIZONTAL;
				gbc.insets = DataCollectionScreenToolkit.INSET_BOTTOM;
				panel.add(dcp.getPanel(), gbc);
				
				// Place a border around the panel.
				
				dcp.getPanel().setBorder(getCompoundTitleBorder(dcp.getStandAloneLabel()));
				
				// Add the data collection fields to the panel.
				
				populateDataCollectionPanel(dcp, excludedPanels, invalidateLayoutFlag);
			}
		}

		// Set the size of the components on the screen.
		
		setMaximumComponentWidth(getMaximumnComponentWidth(fieldList));
	}
	
	private void populateDataCollectionPanel(DataCollectionPanel parentDcp, Set<String> excludedPanels, boolean invalidateLayoutFlag) {
		
		// Clear the panel of all previous fields.
		
		parentDcp.getPanel().removeAll();
		int y = 0;
		
		// Sort a list of all sub panels and fields on the panel.
		
		List<DataCollectionComponent> list = new ArrayList<DataCollectionComponent>();
		list.addAll(parentDcp.getChildDataCollectionPanels().values());
		list.addAll(parentDcp.getChildDataCollectionFields().values());
		Collections.sort(list);
		
		// Loop through each component.
		
		for (DataCollectionComponent dcc : list) {
			
			if (dcc instanceof DataCollectionPanel) {
				
				DataCollectionPanel childDcp = (DataCollectionPanel) dcc;
				
				if ((! childDcp.isVisible()) || ((excludedPanels != null) && (excludedPanels.contains(childDcp.getInfoKey())))) {
					continue;
				}
				
				ExtraDebug.println("Populate panel - " + childDcp.getInfoKey());
	
				// Create the swing panel and place it on the screen.
				
				childDcp.setPanel(new JPanel(new GridBagLayout()));
				GridBagConstraints gbc = new GridBagConstraints();
				gbc.gridx = 0;
				gbc.gridy = y;
				y++;
				gbc.weightx = 1.0;
				gbc.fill = GridBagConstraints.HORIZONTAL;
				gbc.insets = DataCollectionScreenToolkit.INSET_BOTTOM;
				parentDcp.getPanel().add(childDcp.getPanel(), gbc);
				
				// Place a border around the panel.
				
				Border b1 = BorderFactory.createTitledBorder(childDcp.getStandAloneLabel());
				Border b2 = BorderFactory.createEmptyBorder(0, 4, 0, 4);
				Border b = BorderFactory.createCompoundBorder(b1, b2);
				childDcp.getPanel().setBorder(b);
				
				// Add the data collection fields to the panel.
				
				populateDataCollectionPanel(childDcp, excludedPanels, invalidateLayoutFlag);
			
			} else if (dcc instanceof DataCollectionField) {
				
				DataCollectionField field = (DataCollectionField) dcc;
				
				ExtraDebug.println("Add field to panel - " + field.getInfoKey() + ", " + field.getPanel());
	
				if ((field.getPanel() == null) || (! field.isDisplay())) {
					continue;
				}
				
				GridBagConstraints gbc = new GridBagConstraints();
				gbc.gridx = 0;
				gbc.gridy = y;
				y++;
				gbc.weightx = 1.0;
				gbc.fill = GridBagConstraints.HORIZONTAL;
				gbc.insets = DataCollectionScreenToolkit.INSET_BOTTOM;
				
				parentDcp.getPanel().add(field.getPanel(), gbc);
				
				// Set to default value is a default value specified and there is no current value.
				
				String currentValue = field.getValue();
				if (currentValue.isEmpty()) {
					if ((field.getDefaultValue() != null) && (! field.getDefaultValue().isEmpty())) {
						field.setValue(field.getDefaultValue());
					}
				}
				
				// Check if the data collection panel should be marked as needing to be relaid out in the original screen.
				
				if (invalidateLayoutFlag) {
					invalidateDataCollectionPanel(field.getDataCollectionPanel());
				}
			}
		}
	}
	
	private void invalidateDataCollectionPanel(DataCollectionPanel dcp) {
		dcp.setLayoutFlag(true);
		if (dcp.getParentDcp() != null) {
			invalidateDataCollectionPanel(dcp.getParentDcp());
		}
	}

	private void setFieldLabelText(Set<DataCollectionField> fieldList) {
		for (DataCollectionField field : fieldList) {
			if ((field.isVisible()) && (field.getFieldLabel() != null)) {
				String text = "";
				
				if (this.longLabelValueFlag && (field.getStandAloneLabel() != null) && (! field.getStandAloneLabel().isEmpty())) {
					text = field.getStandAloneLabel();
				} else if ((field.getLabel() != null) && (! field.getLabel().isEmpty()) && (! field.getLabel().equals(field.getInfoKey()))) {
					text = field.getLabel();
				}
				
				if ((field.getLayoutType() == DataCollectionField.PAIR_COMPONENT_LAYOUT_FORMAT) && (text != null) && (! text.isEmpty()) && (! text.endsWith(":"))) {
					text += ":";
				}
		
				field.getFieldLabel().setPreferredSize(null);
				field.getFieldLabel().setText(text); 
			}
		}
	}
	
	public static void saveDataCollectionFields(List<DataCollectionField> fieldList, DataCollectionSet dataSet) {
		Map<FieldKey, Object> dataMap = dataSet.getFieldValueMap();
		for (DataCollectionField field : fieldList) {
			if ((field != null) && (field.isVisible()) && (field.getDataComponent() != null)) {
				FieldKey key = field.getKey();
				Object oldValue = dataMap.get(key);
				Object newValue = field.getValue();
				if ((oldValue == null) || (! oldValue.equals(newValue))) {
					dataMap.put(key, newValue);
				}
				if (field.isModified()) {
					field.setDefaultValue(newValue.toString());
				}
				dataSet.getCurrentValueMap().put(key.getInfoKey(), newValue.toString());
			}
		}
	}

	private static String getValue(DataCollectionSet dataCollectionSet, DataCollectionField field) {
		Object obj = dataCollectionSet.getFieldValueMap().get(field.getKey());
		String value = getObjectStrValue(obj);
		if ((value == null) || (value.equals(""))) {
			obj = field.getDefaultValue();
			value = getObjectStrValue(obj);
		}
		return value;
	}

	private static String getObjectStrValue(Object obj) {
		String value = "";
		if (obj != null) {
			if (obj instanceof String) {
				value = (String) obj;
			} else {
				value = obj.toString();
			}
		}
		return value;
	}

	public static void restoreDataCollectionFields(List<DataCollectionField> fieldList, DataCollectionSet dataCollectionSet) {
		for (DataCollectionField field : fieldList) {
			if ((field != null) && (field.getDataComponent() != null) && (field.isVisible())) {
				String value = getValue(dataCollectionSet, field);
				field.setValue(value);
				if (! field.isFieldEnabled()) {
					field.setDataFieldEnabled(false);
				}
				field.fireValidationKeyListener();
			}
		}
	}

	public static boolean hasDataFieldsToCollect(DataCollectionPanel panel, DataCollectionSet dataSet) {
		
		if (panel == null) {
			return false;
		}
		
		for (DataCollectionField field : panel.getChildDataCollectionFields().values()) {
			if ((field != null) && ((field.getVisibility() == DataCollectionField.REQUIRED) || (field.getVisibility() == DataCollectionField.OPTIONAL) || (field.getVisibility() == DataCollectionField.READ_ONLY))) {
				return true;
			}
		}
		
		for (DataCollectionPanel childPanel : panel.getChildDataCollectionPanels().values()) {
			boolean b = hasDataFieldsToCollect(childPanel, dataSet);
			if (b) {
				return true;
			}
		}
		
		return false;
	}

	public static boolean hasRequiredDataFieldsFulfilled(DataCollectionPanel panel, DataCollectionSet dataSet) {
		
		if (panel == null) {
			return true;
		}
		
		for (DataCollectionField field : panel.getChildDataCollectionFields().values()) {
			if ((field != null) && (field.isFieldRequired()) && field.isChildFieldVisible()) {
				FieldKey key = field.getKey();
				Object value = field.getDefaultValue();
				if ((value == null) || (value.toString().isEmpty())) {
					value = dataSet.getFieldValue(key);
				}
				if ((value == null) || (value.toString().isEmpty())) {
					ExtraDebug.println("Required Field Missing: " + field.getInfoKey());
					return false;
				}
			}
		}
		
		for (DataCollectionPanel childPanel : panel.getChildDataCollectionPanels().values()) {
			boolean b = hasRequiredDataFieldsFulfilled(childPanel, dataSet);
			if (! b) {
				return false;
			}
		}
		
		return true;
	}
	
	public String validateData(List<DataCollectionField> dataCollectionFieldList, int mode) {
		DataCollectionField invalidItem = null;
		for (DataCollectionField field : dataCollectionFieldList) {
			if ((field != null) && (field.isVisible()) && (field.getDataComponent() != null) && (field.getDataComponent().isEnabled()) && (field.isValidatable())) {
				invalidItem = field.validate(mode);
				if (invalidItem != null) {
					Toolkit.getDefaultToolkit().beep();
					invalidItem.focus(this.scrollPane2, this.panel2, true);
					return invalidItem.getInfoKey();
				}
			}
		}
		return null;
	}
	
	public static List<DWComboBoxItem> toComboBoxItemsList(Object[] oList, Map<String, String> values) {
		List<DWComboBoxItem> cbiList = new ArrayList<DWComboBoxItem>();
		for (int i = 0; i < oList.length; i++) {
			String label = oList[i].toString();
			
			String value = label;
			if (values != null) {
				for (Map.Entry<String, String> entry : values.entrySet()) {
		            String l = entry.getValue();
		            if (l.equalsIgnoreCase(label)) {
		                value = entry.getKey();
		            }
		        }
			}
			
			if (value == null) {
				value = label;
			}
			cbiList.add(new EnumeratedValue(label, value));
		}
		return cbiList;
	}

	public void moveCursor(List<DataCollectionField> dataCollectionFieldList, JButton nextButton) {
		
		// If there are no invalid fields on the screen, move the cursor to the 
		// first field that does not have a value.
		
		for (DataCollectionField field : dataCollectionFieldList) {
			if ((field.isFocusable()) && (field.getValue().equals("")) && (field.getDataComponent().isShowing()) && (field.getDataComponent().isEnabled())) {
        		field.focus(this.scrollPane2, this.panel2, true);
        		return;
        	}
        }
		
		// All of the fields have a valid value. Move the focus to the Next button.
		
		nextButton.requestFocusInWindow();
	}
	
	public static Border getCompoundTitleBorder(String title) {
		Border b1 = BorderFactory.createTitledBorder(title);
		Border b2 = BorderFactory.createEmptyBorder(0, 4, 0, 4);
		return BorderFactory.createCompoundBorder(b1, b2);
	}

	private List<DataCollectionField> sortScreenFields(List<DataCollectionPanel> panelList) {
		List<DataCollectionField> dciList = new ArrayList<DataCollectionField>();
		Collections.sort(panelList);
		for (DataCollectionPanel panel : panelList) {
			if (panel != null) {
				List<DataCollectionComponent> dccList = new ArrayList<DataCollectionComponent>();
				dccList.addAll(panel.getChildDataCollectionPanels().values());
				dccList.addAll(panel.getChildDataCollectionFields().values());
				Collections.sort(dccList);
				for (DataCollectionComponent dcc : dccList) {
					if (dcc instanceof DataCollectionPanel) {
						List<DataCollectionPanel> dcpList = new ArrayList<DataCollectionPanel>();
						dcpList.add((DataCollectionPanel) dcc);
						dciList.addAll(sortScreenFields(dcpList));
					} else {
						dciList.add((DataCollectionField) dcc);
					}
				}
			}
		}
		return dciList;
	}

	public List<DataCollectionField> getScreenFieldList() {
		return this.screenFieldList;
	}
	
	public ComponentResizeListener getComponentResizeListener() {
		return this.componentResizeListener;
	}
	
	public DataCollectionField getScreenField(FieldKey key) {
		DataCollectionField field = null;
		if (screenFieldMap != null) {
			field = screenFieldMap.get(key);
		}
		return field;
	}

	public void setValidationToolkit() {
		for (DataCollectionField field : screenFieldList) {
			if (field.getDataType() == DataCollectionField.IFSC_FIELD_TYPE) {
				field.getIfscSupplementalPanel().setValidationScreenToolkit(this);
			}
		}
	}
	
	public void enableResize() {
		this.resizeEnabled = true;
	}
	
	public void disableResize() {
		this.resizeEnabled = false;
	}
	
	public Set<DataCollectionField> getFieldList() {
		return this.fieldList;
	}

	public List<DataCollectionField> getOrderedScreenFieldList(DataCollectionPanel dataCollectionPanel) {
		List<DataCollectionField> list = new ArrayList<DataCollectionField>();
		for (Entry<FieldKey, DataCollectionField> e : dataCollectionPanel.getChildDataCollectionFields().entrySet()) {
			DataCollectionField field = e.getValue();
			String parentValue = field.getParentValue();
			if (parentValue != null) {
				list.add(field);
			}
		}
		return list;
	}
}
