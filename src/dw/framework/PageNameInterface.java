package dw.framework;

/**
 * Callback interface used to notify containers that a current page name is
 *   being used. Will be used to notify the main app container (a JFrame) that
 *   it should put the screen name in the title bar
 */
public interface PageNameInterface {
    public void setCurrentPageName(String pageName);
}
