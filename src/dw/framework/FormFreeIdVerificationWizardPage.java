package dw.framework;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

import com.moneygram.agentconnect.EnumeratedIdentifierInfo;
import com.moneygram.agentconnect.FieldInfo;
import com.moneygram.agentconnect.KeyValuePairType;

import dw.i18n.Messages;
import dw.mgreceive.MoneyGramReceiveTransaction;
import dw.mgsend.MoneyGramClientTransaction;
import dw.mgsend.MoneyGramSendTransaction;
import dw.accountdepositpartners.AccountDepositPartnersTransaction;
import dw.framework.DataCollectionSet.DataCollectionStatus;

public class FormFreeIdVerificationWizardPage extends FlowPage {
	private static final long serialVersionUID = 1L;

	private static final List<FieldKey> SENDER_ID1_KEYS = Arrays.asList(new FieldKey[] {
		FieldKey.SENDER_PERSONALID1_TYPE_KEY,
		FieldKey.SENDER_PERSONALID1_NUMBER_KEY,
		FieldKey.SENDER_PERSONALID1_ISSUE_YEAR_KEY,
		FieldKey.SENDER_PERSONALID1_ISSUE_MONTH_KEY,
		FieldKey.SENDER_PERSONALID1_ISSUE_DAY_KEY,
		FieldKey.SENDER_PERSONALID1_EXPIRATION_YEAR_KEY,
		FieldKey.SENDER_PERSONALID1_EXPIRATION_MONTH_KEY,
		FieldKey.SENDER_PERSONALID1_EXPIRATION_DAY_KEY,
		FieldKey.SENDER_PERSONALID1_ISSUE_COUNTRY_KEY,
		FieldKey.SENDER_PERSONALID1_ISSUE_COUNTRY_SUBDIVISIONCODE_KEY,
		FieldKey.SENDER_PERSONALID1_ISSUE_CITY_KEY,
		FieldKey.SENDER_PERSONALID1_ISSUE_AUTHORITY_KEY
	});
	
	private static final List<FieldKey> SENDER_ID2_KEYS = Arrays.asList(new FieldKey[] {
		FieldKey.SENDER_PERSONALID2_TYPE_KEY,
		FieldKey.SENDER_PERSONALID2_NUMBER_KEY,
		FieldKey.SENDER_PERSONALID2_ISSUE_YEAR_KEY,
		FieldKey.SENDER_PERSONALID2_ISSUE_MONTH_KEY,
		FieldKey.SENDER_PERSONALID2_ISSUE_DAY_KEY,
		FieldKey.SENDER_PERSONALID2_EXPIRATION_YEAR_KEY,
		FieldKey.SENDER_PERSONALID2_EXPIRATION_MONTH_KEY,
		FieldKey.SENDER_PERSONALID2_EXPIRATION_DAY_KEY,
		FieldKey.SENDER_PERSONALID2_ISSUE_COUNTRY_KEY,
		FieldKey.SENDER_PERSONALID2_ISSUE_COUNTRY_SUBDIVISIONCODE_KEY,
		FieldKey.SENDER_PERSONALID2_ISSUE_CITY_KEY,
		FieldKey.SENDER_PERSONALID2_ISSUE_AUTHORITY_KEY
	});
	
	private static final List<FieldKey> RECEIVER_ID1_KEYS = Arrays.asList(new FieldKey[] {
		FieldKey.RECEIVER_PERSONALID1_TYPE_KEY,
		FieldKey.RECEIVER_PERSONALID1_NUMBER_KEY,
		FieldKey.RECEIVER_PERSONALID1_ISSUE_YEAR_KEY,
		FieldKey.RECEIVER_PERSONALID1_ISSUE_MONTH_KEY,
		FieldKey.RECEIVER_PERSONALID1_ISSUE_DAY_KEY,
		FieldKey.RECEIVER_PERSONALID1_EXPIRATION_YEAR_KEY,
		FieldKey.RECEIVER_PERSONALID1_EXPIRATION_MONTH_KEY,
		FieldKey.RECEIVER_PERSONALID1_EXPIRATION_DAY_KEY,
		FieldKey.RECEIVER_PERSONALID1_ISSUE_COUNTRY_KEY,
		FieldKey.RECEIVER_PERSONALID1_ISSUE_COUNTRY_SUBDIVISIONCODE_KEY,
		FieldKey.RECEIVER_PERSONALID1_ISSUE_CITY_KEY,
		FieldKey.RECEIVER_PERSONALID1_ISSUE_AUTHORITY_KEY
	});
	
	private static final List<FieldKey> RECEIVER_ID2_KEYS = Arrays.asList(new FieldKey[] {
		FieldKey.RECEIVER_PERSONALID2_TYPE_KEY,
		FieldKey.RECEIVER_PERSONALID2_NUMBER_KEY,
		FieldKey.RECEIVER_PERSONALID2_ISSUE_YEAR_KEY,
		FieldKey.RECEIVER_PERSONALID2_ISSUE_MONTH_KEY,
		FieldKey.RECEIVER_PERSONALID2_ISSUE_DAY_KEY,
		FieldKey.RECEIVER_PERSONALID2_EXPIRATION_YEAR_KEY,
		FieldKey.RECEIVER_PERSONALID2_EXPIRATION_MONTH_KEY,
		FieldKey.RECEIVER_PERSONALID2_EXPIRATION_DAY_KEY,
		FieldKey.RECEIVER_PERSONALID2_ISSUE_COUNTRY_KEY,
		FieldKey.RECEIVER_PERSONALID2_ISSUE_COUNTRY_SUBDIVISIONCODE_KEY,
		FieldKey.RECEIVER_PERSONALID2_ISSUE_CITY_KEY,
		FieldKey.RECEIVER_PERSONALID2_ISSUE_AUTHORITY_KEY
	});
	
	private JPanel panel2;
	
	private MoneyGramClientTransaction transaction;

	public FormFreeIdVerificationWizardPage(MoneyGramSendTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/FormFreeIdValidationWizardPage.xml", name, pageCode, buttons);
		this.transaction = tran;
		init();
	}

	public FormFreeIdVerificationWizardPage(AccountDepositPartnersTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/FormFreeIdValidationWizardPage.xml", name, pageCode, buttons);
		this.transaction = tran;
		init();
	}

	public FormFreeIdVerificationWizardPage(MoneyGramReceiveTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/FormFreeIdValidationWizardPage.xml", name, pageCode, buttons);
		this.transaction = tran;
		init();
	}

	private void init() {
		panel2 = (JPanel) getComponent("panel2");
	}
	
//	private static boolean showPanel(List<FieldKey> keys, DataCollectionSet validationData) {
//		for (FieldKey key : keys) {
//			List<DataCollectionField> fieldList = validationData.getFieldsByInfoKey(key.getInfoKey());
//			
//			for (DataCollectionField field : fieldList) {
//				if (field.isChildFieldVisible()) {
//					String value = field.getValue();
//					if ((value == null) || (value.isEmpty())) {
//						value = field.getDefaultValue();
//					}
//					if ((value != null) && (! value.isEmpty())) {
//						return true;
//					}
//				}
//			}
//		}
//		return false;
//	}
	
	private static boolean showPanel(List<FieldKey> keys, DataCollectionSet validationData) {
		for (FieldKey key : keys) {
			String value = validationData.getCurrentValue(key.getInfoKey());
			if ((value != null) && (! value.isEmpty())) {
				return true;
			}
		}
		return false;
	}
	
	private static boolean hasFields(List<FieldKey> keys, DataCollectionSet validationData, List<KeyValuePairType> currentValues) {
		for (FieldKey key : keys) {
			String value = validationData.getCurrentValue(key.getInfoKey());
			if ((value != null) && (! value.isEmpty())) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean showSenderScreen(DataCollectionSet validationData) {
		return showPanel(SENDER_ID1_KEYS, validationData) || showPanel(SENDER_ID2_KEYS, validationData);
	}
	
	public static boolean showReceiverScreen(DataCollectionSet validationData) {
		return showPanel(RECEIVER_ID1_KEYS, validationData) || showPanel(RECEIVER_ID2_KEYS, validationData);
	}
	
	public static boolean haveReceiverIdFields(DataCollectionSet validationData, List<KeyValuePairType> currentValues) {
		return hasFields(RECEIVER_ID1_KEYS, validationData, currentValues) || hasFields(RECEIVER_ID2_KEYS, validationData, currentValues);
	}
	
	@Override
	public void start(int direction) {
		
		// Setup the flow buttons at the bottom of the screen.
		
		this.flowButtons.reset();
		this.flowButtons.setVisible("expert", false); 
		this.flowButtons.setEnabled("expert", false); 
		this.flowButtons.getButton("back").setText(Messages.getString("Dialogs.No"));
		this.flowButtons.getButton("next").setText(Messages.getString("Dialogs.Yes"));
		
		int maximumLabelWidth = 0;
		List<JLabel> labels = new ArrayList<JLabel>();
		
		List<List<FieldKey>> idPanelList = new ArrayList<List<FieldKey>>();
		List<KeyValuePairType> currentValues = transaction.getTransactionLookupResponsePayload().getCurrentValues().getCurrentValue();
		if (transaction instanceof MoneyGramReceiveTransaction) {
			idPanelList.add(RECEIVER_ID1_KEYS);
			idPanelList.add(RECEIVER_ID2_KEYS);
		} else {
			idPanelList.add(SENDER_ID1_KEYS);
			idPanelList.add(SENDER_ID2_KEYS);
		}
		
		panel2.removeAll();
		
		int gridy = 0;
		for (List<FieldKey> idKeyList : idPanelList) {
			if (hasFields(idKeyList, transaction.getDataCollectionData(), currentValues)) {
				
				JPanel collectionPanel = new JPanel();
				collectionPanel.setLayout(new GridBagLayout());
				
				GridBagConstraints gbc = new GridBagConstraints();
				gbc.gridx = 1;
				gbc.gridy = gridy;
				gridy++;
				gbc.weightx = 1.0;
				gbc.fill = GridBagConstraints.HORIZONTAL;
				gbc.insets = DataCollectionScreenToolkit.INSET_BOTTOM;
				panel2.add(collectionPanel, gbc);
				Border b1 = BorderFactory.createTitledBorder("");
				Border b2 = BorderFactory.createEmptyBorder(0, 4, 0, 4);
				Border b = BorderFactory.createCompoundBorder(b1, b2);
				collectionPanel.setBorder(b);
				
				int fieldCount = 0;
				
				for (FieldKey key : idKeyList) {
					
					String value = transaction.getDataCollectionData().getCurrentValue(key.getInfoKey());
					FieldInfo info = transaction.getTransactionLookupInfos().get(key.getInfoKey());
					String labelText = transaction.getTransactionLookupInfoLabel(key.getInfoKey());
				
					if ((value != null) && (! value.isEmpty())) {
						
						if ((labelText != null) && (! labelText.endsWith(":"))) {
							labelText += ":";
						}
						JLabel label = new JLabel(labelText);
						gbc.gridx = 0;
						gbc.gridy = fieldCount;
						gbc.weightx = 0.0;
						gbc.fill = GridBagConstraints.NONE;
						gbc.anchor = GridBagConstraints.WEST;
						gbc.insets = DataCollectionScreenToolkit.INSET_RIGHT_BOTTOM;
						collectionPanel.add(label, gbc);
						
						int width = label.getPreferredSize().width + DataCollectionScreenToolkit.COMPONENT_SPACING;
						maximumLabelWidth = Math.max(maximumLabelWidth, width);
						labels.add(label);
						
						if ((info != null) && (info.getEnumeration() != null) && (info.getEnumeration().getEnumeratedItems() != null)) {
							for (EnumeratedIdentifierInfo item : info.getEnumeration().getEnumeratedItems().getEnumeratedItem()) {
								if (item.getIdentifier().equals(value)) {
									value = item.getLabel();
									break;
								}
							}
						}
						
						JTextField tf = new JTextField();
						tf.setEditable(false);
							
						tf.setText(value);
						gbc.gridx = 1;
						gbc.gridy = fieldCount;
						gbc.weightx = 1.0;
						gbc.fill = GridBagConstraints.HORIZONTAL;
						gbc.anchor = GridBagConstraints.WEST;
						gbc.insets = DataCollectionScreenToolkit.INSET_BOTTOM;
						collectionPanel.add(tf, gbc);
						
						fieldCount++;
					}
				}
			}
		}
		
		if (! labels.isEmpty()) {
			Dimension d = new Dimension(maximumLabelWidth, labels.get(0).getPreferredSize().height);
			for (JLabel label : labels) {
				label.setPreferredSize(d);
			}
		}
	}

	@Override
	public void finish(int direction) {
    	if ((direction == PageExitListener.NEXT) && (! transaction.getDataCollectionData().getValidationStatus().equals(DataCollectionStatus.VALIDATED))) {
        	transaction.setValidationStatus(DataCollectionStatus.VALIDATION_NOT_PERFORMED);
   			transaction.validation(this);
   		} else {
   			PageNotification.notifyExitListeners(this, direction);
   		}
	}

	@Override
	public void commComplete(int commTag, Object returnValue) {
		if (returnValue != null) {
			PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
		}
	}
}