package dw.framework;

import java.awt.Color;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.moneygram.agentconnect.Response;

import dw.accountdepositpartners.AccountDepositPartnersTransaction;
import dw.comm.AgentConnect;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.mgreceive.MoneyGramReceiveTransaction;
import dw.mgreceive.MoneyGramReceiveWizard;
import dw.mgsend.MoneyGramClientTransaction.ProfileStatus;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;
import dw.mgsend.MoneyGramClientTransaction;
import dw.mgsend.MoneyGramSendTransaction;
import dw.model.adapters.ConsumerProfile;

public class ProfileEditWizardPage1 extends FlowPage {
	private static final long serialVersionUID = 1L;
	
	private static final List<String> RECEIVER_NAME_FIELD_LIST = Arrays.asList(FieldKey.RECEIVER_TITLE_KEY.getInfoKey(), FieldKey.RECEIVER_FIRSTNAME_KEY.getInfoKey(), FieldKey.RECEIVER_MIDDLENAME_KEY.getInfoKey(), FieldKey.RECEIVER_LASTNAME_KEY.getInfoKey(), FieldKey.RECEIVER_LASTNAME2_KEY.getInfoKey(), FieldKey.RECEIVER_NAMESUFFIX_KEY.getInfoKey(), FieldKey.RECEIVER_NAMESUFFIXOTHER_KEY.getInfoKey());
	private static final Map<String, String> SENDER_FIELD_MAP = new HashMap<String, String>();
	private static final Map<String, String> RECEIVER_FIELD_MAP = new HashMap<String, String>();

	private DataCollectionScreenToolkit toolkit;
	private MoneyGramClientTransaction transaction;
	private Object dataCollectionSetChangeFlag = new Object();
	private JScrollPane scrollPane1;
	private JScrollPane scrollPane2;
	private JPanel panel2;
	private int flags;
	private JButton backButton;
	private JButton nextButton;
	
	static {
		SENDER_FIELD_MAP.put(FieldKey.CONSUMER_FIRSTNAME_KEY.getInfoKey(), FieldKey.SENDER_FIRSTNAME_KEY.getInfoKey()); 
		SENDER_FIELD_MAP.put(FieldKey.CONSUMER_LASTNAME_KEY.getInfoKey(), FieldKey.SENDER_LASTNAME_KEY.getInfoKey());
		SENDER_FIELD_MAP.put(FieldKey.CONSUMER_LASTNAME2_KEY.getInfoKey(), FieldKey.SENDER_LASTNAME2_KEY.getInfoKey());
		SENDER_FIELD_MAP.put(FieldKey.CONSUMER_DOB_KEY.getInfoKey(), FieldKey.SENDER_DOB_KEY.getInfoKey()); 
		SENDER_FIELD_MAP.put(FieldKey.CONSUMER_HOMEPHONE_KEY.getInfoKey(), FieldKey.SENDER_PRIMARYPHONE_KEY.getInfoKey()); 
		
		RECEIVER_FIELD_MAP.put(FieldKey.CONSUMER_DOB_KEY.getInfoKey(), FieldKey.RECEIVER_DOB_KEY.getInfoKey());
	}

	public ProfileEditWizardPage1(AccountDepositPartnersTransaction transaction, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/ProfileEditWizardPage.xml", name, pageCode, buttons); 
		this.transaction = transaction;
		init();
	}

	public ProfileEditWizardPage1(MoneyGramSendTransaction transaction, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/ProfileEditWizardPage.xml", name, pageCode, buttons); 
		this.transaction = transaction;
		init();
	}
	
	public ProfileEditWizardPage1(MoneyGramReceiveTransaction transaction, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/ProfileEditWizardPage.xml", name, pageCode, buttons); 
		this.transaction = transaction;
		init();
	}
	
	private void init() {
		scrollPane1 = (JScrollPane) this.getComponent("scrollPane1");
		scrollPane2 = (JScrollPane) this.getComponent("scrollPane2");
		panel2 = (JPanel) this.getComponent("container2");
		flags = DataCollectionScreenToolkit.SHORT_LABEL_VALUES_FLAG + DataCollectionScreenToolkit.DISPLAY_REQUIRED_FLAG;
		backButton = this.flowButtons.getButton("back");
		nextButton = this.flowButtons.getButton("next");
	}

	private void resetButtons() {
		flowButtons.reset();
		flowButtons.setVisible("expert", false);
	}
	
	@Override
	public void start(int direction) {
		resetButtons();
		
		if (this.dataCollectionSetChangeFlag != this.transaction.getDataCollectionData().getChangeFlag()) {
	    	this.dataCollectionSetChangeFlag = this.transaction.getDataCollectionData().getChangeFlag();
			
			List<DataCollectionPanel> list = transaction.getSupplementalDataCollectionPanels();
			toolkit = new DataCollectionScreenToolkit(this, scrollPane1, this, scrollPane2, panel2, flags, list, this.transaction, backButton, nextButton, null);
		
			toolkit.populateDataCollectionScreen(panel2, list, transaction.getExcludePanelNames(), false);
			
			if (transaction instanceof MoneyGramReceiveTransaction) {
				for (DataCollectionField field : transaction.getDataCollectionData().getFieldMap().values()) {
					String value = transaction.getSearchProfilesData().getCurrentValue(field.getInfoKey());
//					if (((value != null) && (! value.isEmpty()) && (field.getDataComponent() != null)) || (RECEIVER_NAME_FIELD_LIST.contains(field.getInfoKey()))) {
					if (RECEIVER_NAME_FIELD_LIST.contains(field.getInfoKey())) {
						field.getDataComponent().setEnabled(false);
					}
				}
			}
		}

		// Populate the screen with any previously entered data.
		
		if (direction == PageExitListener.NEXT || direction == PageExitListener.BACK) {
			DataCollectionScreenToolkit.restoreDataCollectionFields(this.toolkit.getScreenFieldList(), this.transaction.getDataCollectionData());
		}

		if (direction == PageExitListener.NEXT) {
			ConsumerProfile profile = transaction.getProfileObject();
			
			if (transaction.getProfileStatus().equals(ProfileStatus.NO_PROFILE) || transaction.getProfileStatus().equals(ProfileStatus.CREATE_PROFILE)) {
				
				// Clear the profile of any old entries.
				
				if (profile == null) {
					profile = new ConsumerProfile("UCPID");
					transaction.setProfileObject(profile);
				} else {
					profile.setCurrentReceiver(null);
					profile.setCurrentBiller(null);
					profile.getReceivers().clear();
					profile.getOriginalReceivers().clear();
					profile.getBillers().clear();
					profile.getOriginalBillers().clear();
				}

				// Add any values entered for the profile search.
				
				if (transaction.getDataCollectionData() != null) {
					Map<String, String> map = transaction instanceof MoneyGramSendTransaction ? SENDER_FIELD_MAP : RECEIVER_FIELD_MAP;
					for (DataCollectionField field1 : transaction.getSearchProfilesData().getFieldMap().values()) {
						String infoKey = map.get(field1.getInfoKey());
						if (infoKey != null) {
							DataCollectionField field2 = transaction.getDataCollectionData().getFieldMap().get(FieldKey.key(infoKey));
							if (field2 != null) {
								String value = field1.getValue();
								if ((value != null) && (! value.isEmpty())) {
									field2.setValue(value);
									field2.setDefaultValue(value);
									field2.getDataComponent().setForeground(Color.BLACK);
									field2.validate(DataCollectionField.SILENT_VALIDATION);
								}
							}
						}
					}
				}
				
				// If the transaction is a receive transaction, add any values obtained from the transactionLookup response.

				if (transaction instanceof MoneyGramReceiveTransaction) {
					for (DataCollectionField field : toolkit.getFieldList()) {
						String value = transaction.getSearchProfilesData().getCurrentValue(field.getInfoKey());
						if ((value != null) && (! value.isEmpty())) {
							field.setValue(value);
							field.setDefaultValue(value);
						}
					}
				}
				
			} else {
				for (DataCollectionField field : toolkit.getFieldList()) {
					String value = profile.getProfileValue(field.getInfoKey());
					if ((value != null) && (! value.isEmpty())) {
						field.setValue(value);
						field.setDefaultValue(value);
					}
				}
			}
		}
			
		toolkit.moveCursor(toolkit.getScreenFieldList(), nextButton);
		toolkit.enableResize();
		toolkit.getComponentResizeListener().componentResized(null);
	}
	
	@Override
	public void finish(int direction) {
		flowButtons.setButtonsEnabled(false);
		toolkit.disableResize();
		
    	if (direction == PageExitListener.NEXT) {
        	String tag = toolkit.validateData(toolkit.getScreenFieldList(), DataCollectionField.DISPLAY_ERRORS_VALIDATION);
        	if (tag != null) {
				flowButtons.setButtonsEnabled(true);
				toolkit.enableResize();
        		return;
        	}
        }

    	DataCollectionScreenToolkit.saveDataCollectionFields(toolkit.getScreenFieldList(), transaction.getDataCollectionData());

    	if (transaction.isProfileEdited()) {
    		transaction.getProfileObject().setModified(true);
    	}
    	
    	if ((direction == PageExitListener.NEXT) && (! transaction.getProfileDataScreen2().hasDataToCollect())) { 
    			
    		if (transaction.getProfileObject().isModified()) {
				transaction.createOrUpdateConsumerProfile(transaction.getProfileObject(), new CommCompleteInterface() {
					@Override
					public void commComplete(int commTag, Object returnValue) {
						boolean b = ((returnValue != null) && (returnValue instanceof Boolean)) ? ((Boolean) returnValue) : false;
						if (b) {
							transaction.getProfileObject().updateValues(transaction.getDataCollectionData().getFieldMap());
							if (transaction.getDataCollectionData().getValidationStatus().equals(DataCollectionStatus.VALIDATED)) {
								getConsumerProfile();
							} else {
								PageNotification.notifyExitListeners(ProfileEditWizardPage1.this, PageExitListener.NEXT);
							}
						} else {
							resetButtons();
							PageNotification.notifyExitListeners(ProfileEditWizardPage1.this, PageExitListener.DONOTHING);
						}
					}
				});
    		} else if (transaction instanceof MoneyGramReceiveTransaction) {
				receiveValidation1();
			} else {
				transaction.getDataCollectionData().setValidationStatus(DataCollectionStatus.VALIDATED);
				PageNotification.notifyExitListeners(this, direction);
			}
		} else {
			PageNotification.notifyExitListeners(this, direction);
		}
	}
	
	private void getConsumerProfile() {
		final ConsumerProfile profile = transaction.getProfileObject();
		String mgiSessionID = profile.getMgiSessionId();
		transaction.getConsumerProfile(profile, mgiSessionID, AgentConnect.AUTOMATIC, transaction.getDataCollectionData().getCurrentValueMap(), new CommCompleteInterface() {
			@Override
			public void commComplete(int commTag, Object returnValue) {
				boolean b = ((returnValue != null) && (returnValue instanceof Boolean)) ? ((Boolean) returnValue) : false;
				if (b) {
					Response response = transaction.getConsumerProfileResponse();
					if (response != null) {
						transaction.setProfileObject(profile);
						transaction.setProfileStatus(ProfileStatus.USE_CURRENT_PROFILE);
						if (transaction instanceof MoneyGramReceiveTransaction) {
							receiveValidation1();
						} else {
							PageNotification.notifyExitListeners(ProfileEditWizardPage1.this, PageExitListener.NEXT);
						}
					}
				} else {
					resetButtons();
				}
			}
		});
	}
	
	private void receiveValidation1() {
		final MoneyGramReceiveTransaction receiveTransaction = (MoneyGramReceiveTransaction) transaction;
		
		CommCompleteInterface cci = new CommCompleteInterface() {
			@Override
			public void commComplete(int commTag, Object returnValue) {
				boolean b = ((returnValue != null) && (returnValue instanceof Boolean)) ? ((Boolean) returnValue) : false;
				if (b) {
					
					// This is the first receiveValidation call from this screen. If the there are no data collection screens to show, and the status is not NEED_MORE_DATE or VALIDATED,
					// then make a second receiveValidation call.
					
					String nextPage = receiveTransaction.nextDataCollectionScreen(MoneyGramReceiveWizard.MRW20_RECEIVE_DETAIL);
					
					if ((nextPage.isEmpty()) && 
							(receiveTransaction.getPayoutMethod() == MoneyGramReceiveTransaction.PAYOUT_CASH) && 
							(! transaction.getDataCollectionData().getValidationStatus().equals(DataCollectionStatus.VALIDATED)) && 
							(! transaction.getDataCollectionData().getValidationStatus().equals(DataCollectionStatus.ERROR))) {
						receiveValidation2();
						return;
					}
					
//			    	transaction.getDataCollectionData().clearFieldValueMap();
				}
				PageNotification.notifyExitListeners(ProfileEditWizardPage1.this, PageExitListener.NEXT);
			}
		};
		
		boolean flag = receiveTransaction.isOKToPrint();
		if (flag) {
			receiveTransaction.receiveValidation(cci, ValidationType.INITIAL_NON_FORM_FREE);
		} else {
			flowButtons.setButtonsEnabled(true);
		}
	}

	private void receiveValidation2() {
		MoneyGramReceiveTransaction receiveTransaction = (MoneyGramReceiveTransaction) transaction;
		
		CommCompleteInterface cci = new CommCompleteInterface() {
			@Override
			public void commComplete(int commTag, Object returnValue) {
				PageNotification.notifyExitListeners(ProfileEditWizardPage1.this, PageExitListener.NEXT);
			}
		};
		
		receiveTransaction.receiveValidation(cci, ValidationType.SECONDARY);
	}
}
