/*
 * Copyright (c) 2001-2012 MoneyGram International
 */
package dw.framework;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import com.moneygram.agentconnect.BusinessError;
import com.moneygram.agentconnect.InfoBase;
import com.moneygram.agentconnect.Response;
import com.moneygram.agentconnect.SearchConsumerProfilesResponse;
import com.moneygram.agentconnect.SessionType;
import com.moneygram.agentconnect.TransactionLookupResponse;

import dw.accountdepositpartners.AccountDepositPartnersTransaction;
import dw.comm.AgentConnect;
import dw.dialogs.Dialogs;
import dw.dialogs.RecvRedirectDialog;
import dw.dwgui.CardSwipeTextField;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.dwgui.PinpadTextField;
import dw.dwgui.PinpadTextField.PinpadTextFieldListener;
import dw.dwgui.PinpadTextField.ReturnStatus;
import dw.framework.DataCollectionScreenToolkit.EnumeratedValue;
import dw.framework.DataCollectionSet.DataCollectionType;
import dw.i18n.Messages;
import dw.io.DiagLog;
import dw.io.diag.MustTransmitDiagMsg;
import dw.main.MainPanelDelegate;
import dw.mgreceive.MoneyGramReceiveTransaction;
import dw.mgsend.MoneyGramClientTransaction;
import dw.mgsend.MoneyGramSendTransaction;
import dw.mgsend.MoneyGramClientTransaction.ProfileStatus;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;
import dw.model.adapters.CachedACResponses;
import dw.model.adapters.ConsumerProfile;
import dw.model.adapters.ReceiveDetailInfo;
import dw.pinpad.Pinpad;
import dw.profile.UnitProfile;
import dw.utility.DWValues;
import dw.utility.Debug;

/**
 * "cardNumberEntry"
 * Page 1 of the MoneyGram Send Wizard. This page prompts the user for the
 * MoneySaver card from the customer. If the field is left blank, they will
 * continue and the home phone page will be shown. Modified code to show the
 * associate discount instructions to user depending upon profile option
 */
public class ProfileSearchWizardPage extends FlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;
	
//	public static final List<String> SEND_BLACK_LIST = Arrays.asList("NAME_AC_NUM_SEARCH", "REF_NUM_DOB_SEARCH");
	public static final List<String> SEND_WHITE_LIST = Arrays.asList("PLUS_NUM_SEARCH", "NAME_DOB_SEARCH", "NAME_PHONE_SEARCH", "PHONE_DOB_SEARCH", "ID_SEARCH");
//	public static final List<String> RECEIVE_BLACK_LIST = Arrays.asList("PLUS_NUM_SEARCH", "NAME_AC_NUM_SEARCH", "NAME_DOB_SEARCH", "NAME_PHONE_SEARCH", "PHONE_DOB_SEARCH", "ID_SEARCH");
	public static final List<String> RECEIVE_WHITE_LIST = Arrays.asList("REF_NUM_DOB_SEARCH"); 
	
	private DataCollectionScreenToolkit toolkit;
    private DWTextPane text1;
    private JScrollPane scrollPane1;
    private JScrollPane scrollPane2;
	private JPanel panel;
	private MoneyGramClientTransaction transaction;
	private PinpadTextField referenceNumberField;
	private boolean enableBackBotton;

	public ProfileSearchWizardPage(MoneyGramSendTransaction transaction, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/ProfileSearchWizardPage.xml", name, pageCode, buttons); 
		this.transaction = transaction;
		init();
	}
	
	public ProfileSearchWizardPage(AccountDepositPartnersTransaction transaction, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/ProfileSearchWizardPage.xml", name, pageCode, buttons); 
		this.transaction = transaction;
		init();
	}
	
	public ProfileSearchWizardPage(MoneyGramReceiveTransaction transaction, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/ProfileSearchWizardPage.xml", name, pageCode, buttons);
		this.transaction = transaction;
		init();
	}
	
	private void init() {
		text1 = (DWTextPane) getComponent("text1");
		scrollPane1 = (JScrollPane) this.getComponent("scrollPane1");
		scrollPane2 = (JScrollPane) this.getComponent("scrollPane2");
		panel = (JPanel) this.getComponent("panel2");
		
		text1.addListener(panel, this);
		
		enableBackBotton = false;
		if ((transaction instanceof MoneyGramSendTransaction) && ((((MoneyGramSendTransaction) transaction).isFeeQuery()) || ((MoneyGramSendTransaction) transaction).isSendToAccount())) {
			enableBackBotton = true;
		}

		if (transaction instanceof MoneyGramSendTransaction) {
			text1.setText(Messages.getString("MGSWizardPage1.Text1"));
		} else if (transaction instanceof MoneyGramReceiveTransaction) {
			text1.setText(Messages.getString("MGSWizardPage1.Text2"));
		}
	}

	@Override
	public void dwTextPaneResized() {
		int width = scrollPane2.getVisibleRect().width - 20;
		text1.setWidth(width);
	}
	
	private void resetButtons() {
		flowButtons.reset();
		flowButtons.setVisible("expert", false);
		flowButtons.setEnabled("back", enableBackBotton); 
	}

	@Override
	public void start(int direction) {
		resetButtons();
		text1.setListenerEnabled(true);

		if (transaction instanceof MoneyGramSendTransaction) {
			((MoneyGramSendTransaction) transaction).setManagerOverrideID(-1);
			((MoneyGramSendTransaction) transaction).setMaxEmployeeLimitExceeded(false);
			((MoneyGramSendTransaction) transaction).clearCustomers();

		} else if (transaction instanceof MoneyGramReceiveTransaction) {
			((MoneyGramReceiveTransaction) transaction).clear();
			if (("Y".equals(UnitProfile.getInstance().get("ISI_ENABLE", "N"))) && ("N".equals(transaction.getKeyboardIssue()))) {
	    		MainPanelDelegate.setIsiAllowDataCollection(true);
	    		MainPanelDelegate.setIsiSkipRecord(false);
		    }
		}
		transaction.setSearchConsumerProfilesResponse(null);
		transaction.setProfileObject(null);
		transaction.setProfileStatus(ProfileStatus.NO_PROFILE);
 
		if ((direction == PageExitListener.NEXT) || (direction == PageExitListener.TIMEOUT)) {
			SearchConsumerProfilesResponse response = CachedACResponses.getSearchConsumerProfilesResponse();
			
			if (response != null) { 
				List<BusinessError> errors = response.getErrors() != null ? response.getErrors().getError() : null;
				List<InfoBase> fieldsToCollectTagList = null;
				if (response.getPayload() != null) {
					fieldsToCollectTagList = response.getPayload().getValue().getFieldsToCollect().getFieldToCollect();
				}
				DataCollectionSet data = new DataCollectionSet(DataCollectionType.PROFILE_SEARCH);
				transaction.setDataCollectionData(data);
				transaction.setSearchProfilesData(data);
				transaction.processValidationResponse(ValidationType.INITIAL_NON_FORM_FREE, fieldsToCollectTagList, errors, false, null);
				
				int flags = DataCollectionScreenToolkit.SHORT_LABEL_VALUES_FLAG + DataCollectionScreenToolkit.DISPLAY_REQUIRED_FLAG + DataCollectionScreenToolkit.SHOW_ALL_CHILD_FIELDS;
				JButton backButton = this.flowButtons.getButton("back");
				final JButton nextButton = this.flowButtons.getButton("next");
				
				final ArrayList<DataCollectionPanel> list1 = new ArrayList<DataCollectionPanel>(transaction.getDataCollectionData().getDataCollectionPanelMap().values());
				
				List<String> whiteList = transaction instanceof MoneyGramSendTransaction ? SEND_WHITE_LIST : RECEIVE_WHITE_LIST;
				
				List<String> definedParentValues = new ArrayList<String>();
				DataCollectionField searchCriteriaNameField = transaction.getDataCollectionData().getFieldByInfoKey(FieldKey.SEARCH_CRITERIANAME_KEY.getInfoKey());
				if (searchCriteriaNameField != null) {
					for (EnumeratedValue value : searchCriteriaNameField.getEnumeratedValueInfoList(false)) {
						definedParentValues.add(value.getValue());
					}
				}
				
				for (DataCollectionPanel dcp : list1) {
					List<DataCollectionField> list2 = new ArrayList<DataCollectionField>(dcp.getChildDataCollectionFields().values());
					for (DataCollectionField field : list2) {
						if ((field.getParentValue() != null) && ((! definedParentValues.contains(field.getParentValue())) || (! whiteList.contains(field.getParentValue())))) {
							FieldKey key = field.getKey();
							dcp.getChildDataCollectionFields().remove(key);
						} else if (field.getInfoKey().equals(FieldKey.MGIREWARDSNUMBER_KEY.getInfoKey())) {
							final CardSwipeTextField dc = new CardSwipeTextField();
							dc.addKeyListener(new KeyAdapter() {
								@Override
								public void keyPressed(KeyEvent e) {
									if ((e.getKeyCode() == KeyEvent.VK_ENTER) && (! dc.isNormalInputMethod())) {
										searchConsumerProfiles(AgentConnect.ALWAYS_LIVE);
									}
								}
							});
							field.setDataComponent(dc);
						} else if (field.getInfoKey().equals(FieldKey.CONSUMER_REFERENCENUMBER_KEY.getInfoKey())) {
							referenceNumberField = new PinpadTextField();
							field.setDataComponent(referenceNumberField);
							referenceNumberField.setFocusListenerEnabled(true);
							referenceNumberField.setPinpadListener(new PinpadTextFieldListener() {
								@Override
								public void callback(ReturnStatus returnStatus) {
									if (returnStatus.equals(ReturnStatus.GOOD)) {
					    				finish(PageExitListener.NEXT);
									} else if (returnStatus.equals(ReturnStatus.BAD)) {
					    				finish(PageExitListener.DONOTHING);
									} else if (returnStatus.equals(ReturnStatus.CANCELLED)) {
					    				finish(PageExitListener.CANCELED);
									}  
								}
							});
						}
					}
				}
				
				toolkit = new DataCollectionScreenToolkit(this, scrollPane1, this, scrollPane2, panel, flags, list1, this.transaction, backButton, nextButton, null);
				
				if (transaction instanceof MoneyGramReceiveTransaction) {
					DataCollectionField field = toolkit.getScreenField(FieldKey.SEARCH_CRITERIANAME_KEY);
					if (field != null) {
						field.setValidatable(false);
					}
				}
				
				if (toolkit.getScreenFieldList().size() <= 1) {
					Debug.println("### No Valid Search Types ###");
					cancelTransaction();
					return;
				}
				
				toolkit.populateSearchScreen(panel, list1.get(0));
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						dwTextPaneResized();
						toolkit.moveCursor(toolkit.getOrderedScreenFieldList(list1.get(0)), nextButton);
						toolkit.enableResize();
						toolkit.getComponentResizeListener().componentResized(null);
					}
				});
			} else {
				cancelTransaction();
				return;
			}
		} else if (direction == PageExitListener.BACK) {
			transaction.setDataCollectionData(transaction.getSearchProfilesData());
		} else {
			if (toolkit != null) {
				toolkit.enableResize();
			}
		}
	}
	
	private void cancelTransaction() {
		Debug.println("Cached searchConsumerProfiles response invalid. Must transmit to refresh cache.");
		CachedACResponses.deleteObjectFile();
		DiagLog.getInstance().writeMustTransmit(MustTransmitDiagMsg.AWOL_LIMIT);
		UnitProfile.getInstance().setForceTransmit(true);
		Dialogs.showRequest("dialogs/DialogCannotInitiateTransaction.xml");
		PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
	}

	@Override
	public void finish(int direction) {
		flowButtons.setButtonsEnabled(false);
		text1.setListenerEnabled(false);
		toolkit.disableResize();
    	
    	// Disable the resize listener for this screen.
    	
		if (direction == PageExitListener.NEXT) {
			if (transaction.getSearchProfiles() != null) {
				transaction.getSearchProfiles().clear();
			}
			if (transaction instanceof MoneyGramSendTransaction) {
				((MoneyGramSendTransaction) transaction).setDataFromHistory(false);
				((MoneyGramSendTransaction) transaction).setCustomerInfo(null);
				((MoneyGramSendTransaction) transaction).resetCardPhoneNumber();
			}
			
			// Check if the required data has been enter on the screen 
			
	    	String tag = toolkit.validateData(toolkit.getScreenFieldList(), DataCollectionField.DISPLAY_ERRORS_VALIDATION);
	    	if (tag != null) {
	    		
	    		// Check if the "search_CriteriaName" field has a value. If it 
	    		// does not, then start this transaction by creating a new profile
	    		// for the customer. Otherwise, return the user back to this screen
	    		// to complete entry of required data.
	    		
	    		if (tag.equals(FieldKey.SEARCH_CRITERIANAME_KEY.getInfoKey())) {
	    			if (transaction instanceof MoneyGramReceiveTransaction) {
						resetButtons();
	    			} else {
	    				transaction.setDataCollectionData(null);
	    				boolean f = transaction.createOrUpdateConsumerProfile(null, ProfileStatus.CREATE_PROFILE);
	    				if (f) {
	    					PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
	    				} else {
	    					PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
	    				}
	    			}
	    		} else {
	        		flowButtons.reset();
	        		flowButtons.setVisible("expert", false);
	        		flowButtons.setEnabled("back", false); 
					toolkit.enableResize();
	    		}
	    		return;
	    	}
	    	
			// If this is a receive transaction, do a transactionLookup on the reference number.
			
			if (transaction instanceof MoneyGramReceiveTransaction) {
				DataCollectionField field = toolkit.getScreenField(FieldKey.key(FieldKey.CONSUMER_REFERENCENUMBER_KEY.getInfoKey(), "REF_NUM_DOB_SEARCH"));
				if ((field != null) && (field.getDataComponent() != null) && (field.getDataComponent() instanceof PinpadTextField)) {
					PinpadTextField pptf = (PinpadTextField) field.getDataComponent();
					String referenceNumber = pptf.getReferenceNumber();
					String pin = pptf.getPin();
					((MoneyGramReceiveTransaction) transaction).setReferenceNumber(referenceNumber);
					
			        if (referenceNumber != null) {
			        	CommCompleteInterface cci = new CommCompleteInterface() {
							@Override
							public void commComplete(int commTag, Object returnValue) {
								boolean b = ((returnValue != null) && (returnValue instanceof Boolean)) ? ((Boolean) returnValue) : false;
								if (b) {
									checkReceiveTransaction();
								} else {
									if (Pinpad.INSTANCE.getIsPinpadRequired()) {
										Pinpad.INSTANCE.sendDisplayMessageRequest(Messages.getString("MGRWizardPage1.PinpadIngenico_TransactionCannotBeReceived"));
									}
									resetButtons();
								}
							}
			        	};
			        	
			        	String purposeOfLookup = transaction.isFormFreeEnabled() ? DWValues.TRX_LOOKUP_RECV_COMP : DWValues.TRX_LOOKUP_RECV;
			        	((MoneyGramReceiveTransaction) transaction).transactionLookup(referenceNumber.trim(), pin, cci, purposeOfLookup, true);
			        	return;
			        }
				}
			} else {
				searchConsumerProfiles(AgentConnect.ALWAYS_LIVE);
				return;
			}
			
		} else {
			if (toolkit != null) {
				toolkit.disableResize();
			}
		}
		PageNotification.notifyExitListeners(this, direction);
	}
	
	private void checkReceiveTransaction() {

		MoneyGramReceiveTransaction receiveTransaction = (MoneyGramReceiveTransaction) transaction;
		
        ReceiveDetailInfo receive = receiveTransaction.getReceiveDetail();
        TransactionLookupResponse transResponse = transaction.getTransactionLookupResponse();

        if (! receiveTransaction.isFormFreeTransaction() && (referenceNumberField.getText().isEmpty()) && (! receive.isOkForAgent())) {
            Dialogs.showError("dialogs/DialogInvalidReceiveData.xml");
            repaint();
            flowButtons.setButtonsEnabled(true);
            flowButtons.setEnabled("back", false);	
            return;
        }

        if (! referenceNumberField.getText().isEmpty()) {
            if ((! receiveTransaction.isOverDailyLimit()) && (! receiveTransaction.isOverReceiveLimit())) {
                // determine if this receive has been redirected to a new country
                if ((! receiveTransaction.isFormFreeTransaction()) && receive.isRedirectIndicator())  {
            		RecvRedirectDialog dialog = new RecvRedirectDialog(transResponse);
            		Dialogs.showPanel(dialog);
                    if (dialog.isCanceled()) {
                        repaint();
                        flowButtons.setButtonsEnabled(true);
                        flowButtons.setEnabled("back", false);
                        return;
                    }
                }
            } else {
                repaint();
                flowButtons.setButtonsEnabled(true);
                return;
            }
        }
		PageNotification.notifyExitListeners(ProfileSearchWizardPage.this, PageExitListener.NEXT);
	}
	
	private void searchConsumerProfiles(int rule) {
		
		Map<String, String> transactionLookupValues = transaction instanceof MoneyGramReceiveTransaction ? transaction.getDataCollectionData().getCurrentValueMap() : null;
		Map<String, String> values = new HashMap<String, String>();
		if (transactionLookupValues != null) {
			String value = transactionLookupValues.get(FieldKey.RECEIVER_FIRSTNAME_KEY.getInfoKey());
			values.put(FieldKey.CONSUMER_FIRSTNAME_KEY.getInfoKey(), value);
			value = transactionLookupValues.get(FieldKey.RECEIVER_MIDDLENAME_KEY.getInfoKey());
			values.put(FieldKey.CONSUMER_MIDDLENAME_KEY.getInfoKey(), value);
			value = transactionLookupValues.get(FieldKey.RECEIVER_LASTNAME_KEY.getInfoKey());
			values.put(FieldKey.CONSUMER_LASTNAME_KEY.getInfoKey(), value);
			value = transactionLookupValues.get(FieldKey.RECEIVER_LASTNAME2_KEY.getInfoKey());
			values.put(FieldKey.CONSUMER_LASTNAME2_KEY.getInfoKey(), value);
			value = transactionLookupValues.get(FieldKey.RECEIVER_NAMESUFFIX_KEY.getInfoKey());
			values.put(FieldKey.CONSUMER_NAMESUFFIX_KEY.getInfoKey(), value);
			value = transactionLookupValues.get(FieldKey.RECEIVER_NAMESUFFIXOTHER_KEY.getInfoKey());
			values.put(FieldKey.CONSUMER_NAMESUFFIXOTHER_KEY.getInfoKey(), value);
		}
		
		if (transaction.getDataCollectionData() != null) {
			String value = transaction.getDataCollectionData().getValue(FieldKey.CONSUMER_DOB_KEY);
			values.put(FieldKey.CONSUMER_DOB_KEY.getInfoKey(), value);
		}
		
		transaction.searchConsumerProfiles(transaction.getDataCollectionData(), values, rule, new CommCompleteInterface() {
			@Override
			public void commComplete(int commTag, Object returnValue) {
				boolean b = ((returnValue != null) && (returnValue instanceof Boolean)) ? ((Boolean) returnValue) : false;
				if (b) {
					checkSearchConsumerProfilesResults();
					if (toolkit != null) {
						toolkit.disableResize();
					}
				} else {
					resetButtons();
				}
			}
		});
	}

	private void checkSearchConsumerProfilesResults() {
		DataCollectionScreenToolkit.saveDataCollectionFields(toolkit.getScreenFieldList(), transaction.getDataCollectionData());
		int profileCount = transaction.getSearchProfiles() != null ? transaction.getSearchProfiles().size() : 0;
		if (profileCount == 0) {
			String searchType = (String) transaction.getDataCollectionData().getFieldValue(FieldKey.key("search_CriteriaName"));
			if (transaction instanceof MoneyGramSendTransaction) {
				
				if (searchType.equals("NAME_PHONE_SEARCH") && transaction.isPhoneNumberLookupEnabled()) {
					
					
					String lastName = null;
					DataCollectionSet data = transaction.getDataCollectionData();
					if (data != null) {
						DataCollectionField field = data.getFieldByInfoKey(FieldKey.CONSUMER_LASTNAME_KEY.getInfoKey());
						if (field != null) {
							lastName = field.getValue();
						}
					}
					
					String phoneNumber = (String) transaction.getDataCollectionData().getFieldValue(FieldKey.key("consumer_PrimaryPhone", searchType));
					((MoneyGramSendTransaction) transaction).setCardPhoneNumber(phoneNumber, false, SessionType.SEND, lastName, new CommCompleteInterface() {
						@Override
						public void commComplete(int commTag, Object returnValue) {
							boolean b = ((returnValue != null) && (returnValue instanceof Boolean)) ? ((Boolean) returnValue) : false;
							if (b) {
								boolean f = true;
								if ((((MoneyGramSendTransaction) transaction).getCustomers() == null) || (((MoneyGramSendTransaction) transaction).getCustomers().isEmpty())) {
									f = transaction.createOrUpdateConsumerProfile(null, ProfileStatus.CREATE_PROFILE);
								}
								if (f) {
									PageNotification.notifyExitListeners(ProfileSearchWizardPage.this, PageExitListener.NEXT);
								} else {
									PageNotification.notifyExitListeners(ProfileSearchWizardPage.this, PageExitListener.CANCELED);
								}
							} else {
								resetButtons();
							}
						}
					});
					return;
				} else if (searchType.equals("PLUS_NUM_SEARCH") && ((MoneyGramSendTransaction) transaction).isMoneySaverEnabled()) {
					String plusNumber = (String) transaction.getDataCollectionData().getFieldValue(FieldKey.key("mgiRewardsNumber", searchType));
					((MoneyGramSendTransaction) transaction).setCardPhoneNumber(plusNumber, true, SessionType.SEND, null, new CommCompleteInterface() {
						@Override
						public void commComplete(int commTag, Object returnValue) {
							boolean b = ((returnValue != null) && (returnValue instanceof Boolean)) ? ((Boolean) returnValue) : false;
							if (b) {
								if ((((MoneyGramSendTransaction) transaction).getCustomers() == null) || (((MoneyGramSendTransaction) transaction).getCustomers().isEmpty())) {
									transaction.createOrUpdateConsumerProfile(null, ProfileStatus.CREATE_PROFILE);
								}
								PageNotification.notifyExitListeners(ProfileSearchWizardPage.this, PageExitListener.NEXT);
							} else {
								((MoneyGramSendTransaction) transaction).resetCardPhoneNumber();
								resetButtons();
							}
						}
					});
					return;
				} else {
					Dialogs.showWarning(
							"dialogs/DialogCustomerNotFound.xml",
							Messages.getString("MoneyGramSendTransaction.2005"),
							Messages.getString("MoneyGramSendTransaction.2010"));
				}
			} else if (transaction instanceof MoneyGramReceiveTransaction) {
				Map<String, String> dataMap = transaction.getDataCollectionData().getCurrentValueMap();
				boolean f = transaction.createOrUpdateConsumerProfile(transaction.getProfileObject(), ProfileStatus.CREATE_PROFILE);
				if (! f) {
					PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
					return;
				}
				for (DataCollectionField field : transaction.getDataCollectionData().getFieldMap().values()) {
					String value = dataMap.get(field.getInfoKey());
					if ((value != null) && (! value.isEmpty())) {
						field.setDefaultValue(value);
					}
				}
			}
			boolean f = transaction.createOrUpdateConsumerProfile(null, ProfileStatus.CREATE_PROFILE);
			if (! f) {
				PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
				return;
			}
		} else if (profileCount == 1) {
			final ConsumerProfile profile = transaction.getSearchProfiles().get(0);
			int rule = transaction instanceof MoneyGramSendTransaction ? AgentConnect.ALWAYS_LIVE : AgentConnect.AUTOMATIC;
			String mgiSessionID = transaction instanceof MoneyGramReceiveTransaction && transaction.getTransactionLookupResponsePayload() != null ? transaction.getTransactionLookupResponsePayload().getMgiSessionID() : null;
			transaction.getConsumerProfile(profile, mgiSessionID, rule, null, new CommCompleteInterface() {
				@Override
				public void commComplete(int commTag, Object returnValue) {
					boolean b = ((returnValue != null) && (returnValue instanceof Boolean)) ? ((Boolean) returnValue) : false;
					if (b) {
						Response response = transaction.getConsumerProfileResponse();
						if (response != null) {
							transaction.setProfileObject(profile);
							transaction.setProfileStatus(ProfileStatus.USE_CURRENT_PROFILE);
							PageNotification.notifyExitListeners(ProfileSearchWizardPage.this, PageExitListener.NEXT);
						}
					} else {
						resetButtons();
					}
				}
			});
			return;
		}
		PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
	}
}
