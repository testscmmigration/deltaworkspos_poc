package dw.framework;


import dw.dialogs.Dialogs;
import dw.utility.IdleBackoutTimer;
import dw.utility.IdleTimeoutListener;


/**
 * Abstract base class for all transaction interfaces. Contains a reference to
 *   to the ClientTransaction object being acted on. Also handles a timeout of
 *   the transaction.
 */
public abstract class TransactionInterface extends PageFlowContainer implements IdleTimeoutListener {
	private static final long serialVersionUID = 1L;

    public TransactionInterface(String fileName, PageNameInterface naming, 
    		boolean sideBar) {
        super(fileName, naming, sideBar);
    }

    @Override
	public void cancel() {
        getTransaction().setStatus(ClientTransaction.CANCELED);
        super.cancel();
    }

    /**
     * Callback from the IdleBackoutTimer. This will be called if there is no
     *   user activity for more than the allowed time period. This will cancel
     *   the transaction.
     */
    @Override
	public void idleTimeoutLong() {
        // let Dialogs.java remove any dialogs it's managing
        
        Dialogs.idleTimeout();
        
        if (getTransaction().getStatus() == ClientTransaction.COMPLETED)
            exit(PageExitListener.COMPLETED);
        else {
            getTransaction().setStatus(ClientTransaction.TIMED_OUT);
            exit(PageExitListener.TIMEOUT);
        }
    }
    
    @Override
	public void idleTimeoutShort() {
                   
          if (!getTransaction().isUserAuthorized(false,true)) {
             IdleBackoutTimer.stopLong();
             idleTimeoutLong();    
          }
          else { 
             IdleBackoutTimer.resetExpiredShort();
             IdleBackoutTimer.checkIn();
          }
    }

    @Override
	public String getNamePinRequiredValue() {
        return getTransaction().getNamePinRequired();
    }

   

    public abstract ClientTransaction getTransaction();
}
