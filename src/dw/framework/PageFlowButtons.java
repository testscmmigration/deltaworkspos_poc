package dw.framework;

import java.awt.Dimension;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.JButton;

import dw.NonObfuscatable;
import dw.utility.Debug;

/**
 * Collection of buttons mapped by name. These buttons are installed on the
 *   container to facilitate the traversal and cancellation functionality. The
 *   buttons stored will be accessible by the name they are mapped to. Certain
 *   button states may be set by name also.
 */
public class PageFlowButtons implements NonObfuscatable {
    private HashMap<String, MappedButton> buttonMap = new HashMap<String, MappedButton>();  // of name/MappedButton mappings
    private Object parent;

    /**
     * Create the PageFlowButtons with a group of buttons and the corresponding
     *   symbolic names.
     * @param buttons an array of JButtons that makes up the flow buttons
     * @param buttonNames an array of the names the buttons are mapped to
     * @parent the parent of the buttons.
     */
    public PageFlowButtons(JButton[] buttons, String[] buttonNames,
                           Object parent) {
        this.parent = parent;
        for (int i = 0; i < buttons.length; i++) {
            if (buttons[i] != null)
                buttonMap.put(buttonNames[i], new MappedButton(buttons[i], parent));
        }
    }

    /**
     * Set an alternate text to be shown on the specified button. The button may
     *   revert()ed later to restore its original text.
     * @param buttonName the name of the button on which we are setting text
     * @param altText the new text to show on the button
     */
    public void setAlternateText(String buttonName, String altText) {
        MappedButton mapped = getMappedButton(buttonName);
        if (mapped != null)
            mapped.setAlternateText(altText);
    }

    /**
     * Set an alternate key mapping for the specified button. The button may be
     *   revert()ed to restore its original key mapping.
     * @param buttonName the name of the button to remap
     * @param keyCode the key to map the button to
     * @param modifier the key modifier to use for the key mapping
     */
    public void setAlternateKeyMapping(String buttonName, int keyCode,
                                              int modifier) {
        MappedButton mapped = getMappedButton(buttonName);
        if (mapped != null)
            mapped.setAlternateKeyCode(keyCode, modifier);
    }

    /**
     * Add a key mapping to a specified button.
     * @param buttonName the name of the button to map
     * @param keyCode the key to map to the button
     * @param modifier the key modifier to use for the key mapping
     */
    public void addKeyMapping(String buttonName, int keyCode,
                                             int modifier) {
        MappedButton mapped = getMappedButton(buttonName);
        if (mapped != null)
            mapped.setKeyCode(keyCode, modifier);
    }

    /**
     * Remove key mappings from all buttons.
     */
    public void removeKeyMappings() {
        KeyMapManager.removeMappings(parent);
    }

    /**
     * Revert a specfic button to its original text and key mapping.
     * @param buttonName the name of the button to reset.
     */
    public void reset(String buttonName) {
        MappedButton mapped = getMappedButton(buttonName);
        if (mapped != null) {
            mapped.reset();
            mapped.getButton().setEnabled(true);
            mapped.getButton().setVisible(true);
        }
    }

    /**
     * Set the size of a particular button.
     * @param buttonName the name of the button to set the size of.
     * @param width width of the button
     * @param height height of the button
     */
    public void setSize(String buttonName, int width, int height) {
        JButton button = getButton(buttonName);
        if (button != null)
            button.setSize(new Dimension(width, height));
    }

    /**
     * Get a reference to a specific button.
     * @param buttonName the name of the button to retrieve
     * @return the button requested
     */
    public JButton getButton(String buttonName) {
        MappedButton mapped = getMappedButton(buttonName);
        if (mapped != null) {
            return mapped.getButton();
        }
        return null;
    }

    /**
     * Get the MappedButton instance that represents the name button.
     * @param buttonName the name of the button retrieve
     * @return the MappedButton requested
     */
    private MappedButton getMappedButton(String buttonName) {
        return buttonMap.get(buttonName);
    }

    /**
     * Set a specific button's enabled status.
     * @param buttonName the button to set status for
     * @param enabled the enabled status to set
     */
    public void setEnabled(String buttonName, boolean enabled) {
        JButton button = getButton(buttonName);
        if (button != null)
            button.setEnabled(enabled);
    }

    /**
     * Set a specific button's visible status. Assumption is that this button is
     *   not needed for the particular screen, so disable it as well to avoid
     *   unexpected button actions.
     * @param buttonName the name of the button to set visible status for
     * @param visible the visible status to set
     */
    public void setVisible(String buttonName, boolean visible) {
        JButton button = getButton(buttonName);
        if (button != null) {
            button.setVisible(visible);
            button.setEnabled(visible);
        }
    }

    /**
     * Set the text of a specified button.
     * @param buttonName the name of the button to set the text of
     * @param text the text to set on the button
     */
    public void setText(String buttonName, String text) {
        JButton button = getButton(buttonName);
        if (button != null)
            button.setText(text);
    }

    /**
     * Set the mnemonic of a specific button.
     * @param buttonName the name of the button to set the text of
     * @param key the key to set a button mnemonic with
     */
    public void setMnemonic(String buttonName, char key) {
        JButton button = getButton(buttonName);
        if (button != null) {
            button.setMnemonic(key);
        }
    }

    /**
     * Set a particular button to be selected.
     * @param buttonName the name of the button to select
     */
    public void setSelected(String buttonName) {
        JButton button = getButton(buttonName);
        if (button != null) {
            button.requestFocus();
        }
        else {
            Debug.println("setSelected(" + buttonName + ") no such button");	 
        }
    }

    /** For debugging. */
    @Override
	public String toString() {
        String sep="";	
        StringBuffer sb = new StringBuffer("[");	
        Iterator<String> i = buttonMap.keySet().iterator();
        while (i.hasNext()) {
            sb.append(sep);
            sb.append(i.next());
            sep = ",";	
        }
        sb.append("]");	
        return sb.toString();
    }

    /**
     * Reset all the buttons to their original text and key mappings.
     */
    public void reset() {
        
        for (Iterator<String> i = buttonMap.keySet().iterator(); i.hasNext(); ) {
            String b = i.next();
            reset(b);
            //if (b == "next"){
            //    setSelected(b);
            //}
        }
    }

    /**
     * Reset all the buttons to their original text and key mappings.
     */
    public void setButtonsEnabled(boolean enable) {
        for (Iterator<String> i = buttonMap.keySet().iterator(); i.hasNext(); ) {
            setEnabled(i.next(), enable);
        }
    }

    /**
     * Disable the entire button set.
     */
    public void disable() {
        for (Iterator<String> i = buttonMap.keySet().iterator(); i.hasNext(); ) {
            JButton button = getButton(i.next());
            button.setEnabled(false);
        }
    }
}

/**
 * Store a button along with its original configuration so it can be changed
 *   and restored.
 */
class MappedButton {
    private Object parent;   // need this to map components with
    private JButton button;
    private String originalText;
    private int originalKeyCode;
    private int originalModifier = 0;

    /**
     * Create a MappedButton with a specified button mapped to a specified
     *   target
     */
    public MappedButton(JButton button, Object parent) {
         this.button = button;
         this.parent = parent;
         this.originalText = button.getText();
    }

    /**
     * Set an alternate key mnemonic for this button.
     */
    public void setAlternateKeyCode(int keyCode, int modifier) {
        KeyMapManager.remapComponent(parent, keyCode, modifier, button);
    }

    /**
     * Set an alternate label for this button.
     */
    public void setAlternateText(String text) {
        button.setText(text);
    }

    /**
     * Set the original key code for this button.
     */
    public void setKeyCode(int keyCode, int modifier) {
        this.originalKeyCode = keyCode;
        this.originalModifier = modifier;
        KeyMapManager.remapComponent(parent, keyCode, modifier, button);
    }

    /**
     * Set the original text for this button.
     */
    public void setText(String text) {
        button.setText(text);
        originalText = text;
    }

    /**
     * Get a reference to the mapped button.
     */
    public JButton getButton() {
        return button;
    }

    /**
     * Restore the original settings for this button. (Text and key mapping)
     */
    public void reset() {
        button.setText(originalText);
        KeyMapManager.remapComponent(parent, originalKeyCode,
                                                 originalModifier, button);
    }
}
