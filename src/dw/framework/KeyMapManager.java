package dw.framework;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

import dw.utility.Debug;

/**
 * KeyMapManager handles the mapping of keystrokes to components. Each
 *  component mapping that is added is registered on a main parent component
 *  via registerKeyboardAction().
 * 10/31/2000 Removed System.exit(1) call in actionPerformed catch block
 */
public class KeyMapManager {
    private static JComponent mapComponent; // parent component to register with
    private static HashMap<Object, ArrayList<MapActionListener>> targetMap = new HashMap<Object, ArrayList<MapActionListener>>();    // Object/ArrayList

    private KeyMapManager() {} // no instances of this class allowed

    /**
     * Set the main component that will have keystrokes registered with it.
     */
    public static void setMainMapComponent(JComponent comp) {
         mapComponent = comp;
    }

    /**
     * Clear all keyboard actions from the map component.
     */
    public static void clear() {
        if (mapComponent == null)
            return;
        KeyStroke[] strokes = mapComponent.getRegisteredKeyStrokes();
        for (int i = 0; i < strokes.length; i++) {
            mapComponent.unregisterKeyboardAction(strokes[i]);
        }
    }

    /**
     * Map a group of components to their specified keystrokes for a particular
     *   target object.
     */
    public static void mapComponents(Object target, int[] keys,
                                            int[] modifiers,
                                            Component[] components) {
        if (keys.length != components.length ||
            keys.length != modifiers.length) {
            Debug.println("Could not map components to keys.");	
            return;
        }
        for (int i = 0; i < keys.length; i++) {
            mapComponent(target, keys[i], modifiers[i], components[i]);
        }
    }

    /**
     * Map a given component to a keystroke for a particular target object.
     */
    public static void mapComponent(Object target, int key, int modifier,
                                    Component component) {
        if (mapComponent == null) {
            Debug.println("Could not register keystroke on null parent.");	
            return;
        }
        MapActionListener listener = new MapActionListener(mapComponent, target,
                                                      key, modifier, component);
        ArrayList<MapActionListener> listenerList = targetMap.get(target);
        if (listenerList == null) {
            listenerList = new ArrayList<MapActionListener>();
            targetMap.put(target, listenerList);
        }
        listenerList.add(listener);
    }

    /**
     * Map a given method to a keystroke for a particular target object.
     */
    public static void mapMethod(Object target, int key, int modifier,
                                 String methodName) {
        if (mapComponent == null) {
            Debug.println("Could not register keystroke on null parent.");	
            return;
        }
        MapActionListener listener = new MapActionListener(mapComponent, target,
                                                  key, modifier, methodName);
        ArrayList<MapActionListener> listenerList = targetMap.get(target);
        if (listenerList == null) {
            listenerList = new ArrayList<MapActionListener>();
            targetMap.put(target, listenerList);
        }
        listenerList.add(listener);
    }

    /**
     * Remove all the key mappings that belong to a given target object.
     */
    public static void removeMappings(Object target) {
        ArrayList<MapActionListener> listenerList = targetMap.get(target);
        if (listenerList == null)
            return;
        for (Iterator<MapActionListener> i = listenerList.iterator(); i.hasNext(); )
            i.next().unregister();
        targetMap.remove(target);
    }


	/**
	 * Remove all key mappings for panel refresh
	 *
	 */
	public static void removeAllMappings(){
		targetMap.clear();	
	}
	
    /**
     * Find the given component and remap it to the new key.
     */
    public static void remapComponent(Object target, int newKeyCode,
                                    int modifier, Component component) {
        // find and remove any mappings to this component
        for (Iterator<ArrayList<MapActionListener>> i = targetMap.values().iterator(); i.hasNext(); ) {
        	ArrayList<MapActionListener> listenerList = i.next();
            for (Iterator<MapActionListener> j = listenerList.iterator(); j.hasNext(); ) {
                MapActionListener listener = j.next();
                if (listener.getComponent() == component) {
                    listener.unregister();
                    listenerList.remove(listener);
                    break;
                }
            }
        }
        // add a new mapping to this component
        mapComponent(target, newKeyCode, modifier, component);
    }
    
 public static void unregisterComponent(Component component) {
    // find and remove any mappings to this component
          for (Iterator<ArrayList<MapActionListener>> i = targetMap.values().iterator(); i.hasNext(); ) {
              ArrayList<MapActionListener> listenerList = i.next();
                 for (Iterator<MapActionListener> j = listenerList.iterator(); j.hasNext(); ) {
                     MapActionListener listener = j.next();
                     if (listener.getComponent() == component) {
                         listener.unregister();
                         listenerList.remove(listener);                       
                         break;
	                  }
                 }
          }
          targetMap.remove(component);
 }  
    
}

/**
 * An action listener that stores some state information about the target object
 *   it was created by and the keystroke that it is registered to. This state
 *   information can be used to de-register the key mappings.
 */
class MapActionListener implements ActionListener {
    Object[] emptyParms = {};
    Object target = null;
    Component component = null;
    JComponent parent = null;
    KeyStroke stroke = null;
    Method method = null;

    private MapActionListener(JComponent parent, Object target,
                                int keyCode, int modifier) {
        this.parent = parent;
        this.target = target;
        stroke = KeyStroke.getKeyStroke(keyCode, modifier);
        parent.registerKeyboardAction(this, stroke,
                                    JComponent.WHEN_IN_FOCUSED_WINDOW);
    }

    public MapActionListener(JComponent parent, Object target,
                                int keyCode, int modifier,
                                String methodName) {
        this(parent, target, keyCode, modifier);
        if (! Callbacks.known(methodName + "()")) {
            Debug.println("No such method on " + target.getClass().getName());
//            Debug.dumpStack();
        }
        try {
            method = target.getClass().getMethod(methodName);
        }
        catch (Exception e) {
            Debug.println("Mapping action listener failed : ");	
            Debug.printStackTrace(e);
        }
    }

    public MapActionListener(JComponent parent, Object target,
                             int keyCode, int modifier,
                                 Component component) {
        this(parent, target, keyCode, modifier);
        this.component = component;
    }

    public Component getComponent() {
        return component;
    }

    public Object getTarget() {
        return target;
    }

    /**
     * Remove this listener from the key it was mapped to.
     */
    public void unregister() {
        parent.unregisterKeyboardAction(stroke);
    }

    /**
     * Activate the component/method that I am mapped to.
     */
    @Override
	public void actionPerformed(ActionEvent e) {
        if (component != null) {
            if (component instanceof JButton)
                ((JButton) component).doClick();
        }
        else if (method != null) {
            try {
                method.invoke(target, emptyParms);
            }
            catch (Exception ex) {
                Debug.println("Could not invoke method on target.");	
                Debug.printStackTrace(ex);
            }
        }
    }
}
