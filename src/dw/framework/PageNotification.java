package dw.framework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import dw.utility.Debug;

/**
 * PageNotification is a central Mapping of objects to the PageExitListeners
 *   that are listening to them.
 */
public class PageNotification {
    // map of object -> ArrayList of PageExitListeners
    private static HashMap<Object, ArrayList<PageExitListener>> listenerMap = new HashMap<Object, ArrayList<PageExitListener>>();

    // do not allow instance of this class to be created
    private PageNotification() {}

    /**
     * Register a PageExitListener with a given target.
     */
    public static void addPageExitListener(PageExitListener listener,
                                           Object target) {
        ArrayList<PageExitListener> list = listenerMap.get(target);
        if (list == null) {
            list = new ArrayList<PageExitListener>();
            listenerMap.put(target, list);
        }
        if (! list.contains(listener))
            list.add(listener);
    }

    /**
     * De-Register a PageExitListener from a given target object.
     */
    public static void removePageExitListener(PageExitListener listener,
                                              Object target) {
        ArrayList<PageExitListener> list = listenerMap.get(target);
        if (list != null) {
            list.remove(listener);
            if (list.size() == 0)
                listenerMap.remove(target);
        }
    }

    /**
     * De-Register all PageExitListeners from a given target object.
     */
    public static void removePageExitListeners(Object target) {
        listenerMap.remove(target);
    }

	/**
	 * Remove all PageExitListeners for ALL targets
	 * @author T007
	 *
	 * To change the template for this generated type comment go to
	 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
	 */
	public static void removeAllPageExitListeners(){
		listenerMap.clear();
	}
	
    /**
     * Notify all the interested parties of the given target object that it has
     *   generated an exit() event.
     */
    public static void notifyExitListeners(Object target, int direction) {
        List<PageExitListener> listeners = listenerMap.get(target);
        if (listeners == null)
            return;

        if (listeners.size() == 1) {
            // Normal case: the containing page is the only listener.
            (listeners.get(0)).exit(direction);
        }
        else {
            // General case: any number of listeners.
            // Have to copy the array so that unloadPages can remove
            // exit listeners when a transaction exits.
            Debug.println("notifyExitListeners() target " 	
                    + target.getClass().getName() + " has "  
                    + listeners.size() + " exit listeners"); 
            ArrayList<PageExitListener> copy = new ArrayList<PageExitListener>(listeners);  
            for (Iterator<PageExitListener> i = copy.iterator(); i.hasNext(); ) {
                (i.next()).exit(direction);
            }
        }
    }
}
