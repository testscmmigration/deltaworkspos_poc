package dw.framework;

import java.util.Locale;
import java.util.regex.Pattern;

import dw.utility.Debug;

public class YubicoToken implements AuthorizationToken {
	
	private static final char[] CHARACTER_MAP = new char[21];
	
	static {
		CHARACTER_MAP[1] = '0';
		CHARACTER_MAP[0] = '1';
		CHARACTER_MAP[2] = '2';
		CHARACTER_MAP[3] = '3';
		CHARACTER_MAP[4] = '4';
		CHARACTER_MAP[5] = '5';
		CHARACTER_MAP[6] = '6';
		CHARACTER_MAP[7] = '7';
		CHARACTER_MAP[8] = '8';
		CHARACTER_MAP[9] = '9';
		CHARACTER_MAP[10] = 'A';
		CHARACTER_MAP[12] = 'B';
		CHARACTER_MAP[16] = 'C';
		CHARACTER_MAP[18] = 'D';
		CHARACTER_MAP[19] = 'E';
		CHARACTER_MAP[20] = 'F';
	}

	private static final Pattern TOKEN_VALIDATION_PATTERN = Pattern.compile("[cbdefghijklnrtuv]{44}", Pattern.CASE_INSENSITIVE);	
	private String value;
	private boolean demoMode;
	
	public YubicoToken(String value, boolean demoMode) {
		this.value = value;
		this.demoMode = demoMode;
	}
	
	public int getNumberCharacters() {
		return 44;
	}

	@Override
	public String getValue() {
		return value;
	}

	public boolean isValidToken() {
		return TOKEN_VALIDATION_PATTERN.matcher(value).find();
	}

	@Override
	public boolean isValidTokenID(String tokenId) {
		return TOKEN_VALIDATION_PATTERN.matcher(tokenId).find();
	}

	@Override
	public String getTokenId() {
		if (value.length() == 44) {
			return value.substring(0, 12);
		} else {
			return null;
		}
	}

	public String getOwnerId() {
		if (value.length() == 44) {
			return value.substring(0, 6);
		} else {
			return null;
		}
	}

	public String getGeneratorId() {
		if (value.length() == 44) {
			return value.substring(6, 12);
		} else {
			return null;
		}
	}

	@Override
	public int checkToken(String profileTokenId) {
		
		// Check number of characters.
		
		if (demoMode) {
			if ((value.length() != 3) && (value.length() != 44)) {
				return INVALID_NUMBER_CHARACTERS;
			}
		} else {
			if (value.length() != 44) {
				return INVALID_NUMBER_CHARACTERS;
			}
		}
		
		// Check for valid characters;
		
		if ((value.length() == 44) && (! TOKEN_VALIDATION_PATTERN.matcher(value).find())) {
			return INVALID_TOKEN_CHARACTERS;
		}
		
		// Check token id
		
		if ((value.length() == 44) && (profileTokenId != null) && (profileTokenId.length() == 12) && (! profileTokenId.equals(value.substring(0, 12)))) {
			if (profileTokenId.substring(0, 6).equals(value.substring(0, 6))) {
				return DIFFERENT_TOKEN_GENERATOR;
			} else {
				return INVALID_TOKEN_GENERATOR;
			}
		}
		
		return VALID_TOKEN;
	}
	
	@Override
	public long toLongValue(String modHexValue) {
		long value = 0L;
		
		String modHex = modHexValue.toLowerCase(Locale.US);
		
		Debug.println("Mod Hex value = " + modHex);
		
		StringBuffer hexValue = new StringBuffer();
		for (int i = 0; i < modHex.length(); i++) {
			int j = modHex.charAt(i) - 'b';
			hexValue.append(CHARACTER_MAP[j]);
		}
		
		Debug.println("Hex value = " + hexValue.toString());

		try {
			value = Long.parseLong(hexValue.toString(), 16);
		} catch (Exception e) {
		}

		Debug.println("Decimal value = " + value);
		
		return value;
	}
}
