package dw.framework;

/**
 * Pause Semaphore is a flag that is sent to a time-consuming process. The
 *   process then calls waitUntilFree() at desired intervals to allow it to
 *   be paused.
 */

public class PauseSemaphore {
    private boolean paused = false;

    public void waitUntilFree() {
        while (paused) {
            try {
                Thread.sleep(1000);
            }
            catch (InterruptedException e) {}
        }
    }
    public void waitUntilInterrupted() throws InterruptedException {
        while (paused) {
        	Thread.sleep(1000);
            throw new InterruptedException();
        }
    }
    public synchronized void pause() {
        paused = true;
    }

    public synchronized void resume() {
        paused = false;
    }

    public boolean isPaused() {
        return paused;
    }
    
    
}
