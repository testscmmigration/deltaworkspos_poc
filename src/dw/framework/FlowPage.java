package dw.framework;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;

import com.moneygram.agentconnect.TransactionStatusType;

import dw.dwgui.DWTextPane;
import dw.dwgui.MultiListComboBox;
import dw.dwgui.StateListComboBox;
import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.io.DiagLog;
import dw.io.diag.MustTransmitDiagMsg;
import dw.main.DeltaworksStartup;
import dw.main.MainPanelDelegate;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.CountryInfo.Country;
import dw.model.adapters.CountryInfo.CountrySubdivision;
import dw.profile.UnitProfile;
import dw.profile.UnitProfileInterface;
import dw.utility.DWValues;

/**
 * FlowPage is a single page in a PageFlowContainer.
 */
public abstract class FlowPage extends XmlDefinedPanel implements CommCompleteInterface {
	private static final long serialVersionUID = 1L;
	
	protected int FIELD_VALUE_WIDTH = 250;
	protected int FIELD_TOTAL_WIDTH = 400;
	protected int MINIMUM_TAG_WIDTH = 160;

	private String name = null;

	private String pageCode = null;

	private boolean displaySideBanner = true;

	private boolean stateBoxIsEmpty = false;

	protected PageFlowButtons flowButtons = null;

	protected String currencyLabel = " ";

    private int retryCount;

    /**
	 * Create a new FlowPage from the given XML file name.
	 *
	 * @param xmlFileName
	 *            the name of the XML file this flow page is created from
	 * @param name
	 *            the name of this flow page
	 * @param pageCode
	 *            the code of the page...displayed in the title bar.
	 * @param buttons
	 *            a set of PageFlowButtons that is being used for a flow
	 * @param displaySideBanner
	 *            true if the page should display a side banner
	 */
	public FlowPage(String xmlFileName, String name, String pageCode,
			PageFlowButtons buttons, boolean displaySideBanner) {
		super(xmlFileName);
		this.name = name;
		flowButtons = buttons;
		this.pageCode = pageCode; // code to display on title bar for
									// reference
		this.displaySideBanner = displaySideBanner;
		currencyLabel = currencyLabel + CountryInfo.getAgentCountry().getBaseReceiveCurrency();
	}

	/**
	 * Create a new FlowPage from the given XML file name.
	 *
	 * @param xmlFileName
	 *            the name of the XML file this flow page is created from
	 * @param name
	 *            the name of this flow page
	 * @param pageCode
	 *            the code of the page...displayed in the title bar.
	 * @param buttons
	 *            a set of PageFlowButtons that is being used for a flow
	 */
	public FlowPage(String xmlFileName, String name, String pageCode,
			PageFlowButtons buttons) {
		super(xmlFileName);
		this.name = name;
		flowButtons = buttons;
		this.pageCode = pageCode; // code to display on title bar for
									// reference
		this.displaySideBanner = true;
		currencyLabel = currencyLabel + CountryInfo.getAgentCountry().getBaseReceiveCurrency();
	}

	/**
	 * For a given country, update the corresponding state list.
	 *
	 * @param country
	 *            the country whose list is being updated
	 * @param stateBox
	 *            the combobox to update
	 */
	public void updateStateList(Country country, MultiListComboBox<CountrySubdivision> stateBox) {
		stateBox.removeAllItems();
		List<CountrySubdivision> countrySubdivisionList = country != null ? country.getCountrySubdivisionList() : null;
		if ((countrySubdivisionList != null) && (countrySubdivisionList.size() > 0)) {
			stateBox.reset();
			stateBox.setEnabled(true);
			stateBox.addList(countrySubdivisionList, "countrySubdivisions");
			stateBoxIsEmpty = false;
		} else {
			stateBoxIsEmpty = true;
			stateBox.setEnabled(false);
		}
	}

	/**
	 * For a given country, update the corresponding state list.
	 *
	 * @param country
	 *            the country whose list is being updated
	 * @param stateBox
	 *            the combobox to update
	 */
	public void updateCodeTableStateList(Country country, MultiListComboBox<CountrySubdivision> stateBox) {
		stateBox.removeAllItems();
		
		String[] stateListCountries = DWValues.getCodeTableStateListCountries();
		List<String> stateCountryList = Arrays.asList(stateListCountries);
		if(!stateCountryList.contains(country.getCountryCode())) {
			return;
		}
		List<CountrySubdivision> si = country.getCountrySubdivisionList();
		
		if (country.getCountryCode().equals("MEX")) {
			stateBoxIsEmpty = true;
			stateBox.setEnabled(false);
		} else if (si.size() > 0) {
			stateBox.reset();
			stateBox.setEnabled(true);
			stateBox.setCurrentList(country.getCountryCode());
			String text = Messages.getString("MGSWizardPage5.item1");
			CountrySubdivision blankState = new CountrySubdivision();
			blankState.setPrettyName(text);
			blankState.setCountrySubdivisionCode("");
			stateBox.insertItemAt(blankState, 0);
			stateBox.setSelectedIndex(0);

			stateBoxIsEmpty = false;
		} else {
			stateBoxIsEmpty = true;
			stateBox.setEnabled(false);
		}
	}

	/**
	 * Put all the distinct state lists in the given multilist combo box.
	 *
	 * @param stateBox
	 *            the box to populate with state lists
	 * @param agentCountry
	 *            the country the agent is located in..for default
	 * @param agentState
	 *            the state the agent is located in...for default
	 */
	public void populateCodeTablesStateBox(MultiListComboBox<CountrySubdivision> stateBox, Country agentCountry, CountrySubdivision agentState) {
		List<Country> stateCountryList = CountryInfo.getCodeTableStateListCountries();
		for (Country country : stateCountryList) {
			List<CountrySubdivision> states = country.getCountrySubdivisionList();
			/*
			 * Add a dummy list just in case code tables did not have anything
			 * (avoid a null ptr exception)
			 */
			if (states == null) {
				states = new ArrayList<CountrySubdivision>();
			}
			if (states.size() == 0) {
				CountrySubdivision cs = new CountryInfo.CountrySubdivision();
				cs.setPrettyName(Messages.getString("DWValues.2049"));
				cs.setCountrySubdivisionCode(Messages.getString("DWValues.2049"));
				cs.setCountrySubdivisionName(Messages.getString("DWValues.2049"));
				states.add(cs);
			}
			stateBox.addList(states, country.getCountryCode());
		}
		stateBox.addItem(null);
	}

	/**
	 * Put all the distinct state lists in the given multilist combo box.
	 *
	 * @param stateBox
	 *            the box to populate with state lists
	 * @param defaultCountry
	 *            the default country to use (which state list) 
	 * @param defaultState
	 *            the state to default to
	 */
	public void populateStateBox(MultiListComboBox<CountrySubdivision> stateBox, Country defaultCountry, CountrySubdivision defaultState) {
		/*
		 * Get List of Countries that have states lists
		 */
		List<Country> stateListCountries = CountryInfo.getCodeTableStateListCountries();
		/*
		 * Go through the list of countries that have states, adding all of the
		 * state lists to the stateBox, then defaulting to the agent's state (if
		 * applicable)
		 */
		for (Country country : stateListCountries) {
			List<CountrySubdivision> states =null;
			
			if (country != null) {
				states = country.getCountrySubdivisionList();
				if (states != null && states.size() > 0) {
					stateBox.addList(states, country.getCountryCode()); // Need to be tweaked if not correct as part of AGG3
				}
			}

		}
		
		/*
		 * Done finding all the state lists, setup the default (if available)
		 */
    	if (defaultState != null && defaultState.getCountrySubdivisionCode().length() > 0) {
    		stateBox.setCurrentList(defaultCountry.getCountryCode());
			stateBox.setSelectedComboBoxItem(defaultState.getCountrySubdivisionCode());
		} else {
			stateBox.setCurrentList(null);
			stateBox.setSelectedComboBoxItem(null);
		}
	}

	/**
	 * Override of addReturnTabs to include the addition of the finish(CANCEL)
	 * method or the cancel button click.
	 *
	 * @param cancelButton
	 *            the button to be clicked to cancel. If this is null, use the
	 *            finish(CANCELED) method instead.
	 * @param exceptions
	 *            a list of components who should not receive the listeners
	 */
	@Override
	public void addReturnTabListeners(Component[] exceptions,
			JButton cancelButton) {
		if (cancelButton != null)
			super.addReturnTabListeners(exceptions, cancelButton);
		else
			super.addReturnTabListeners(exceptions, this, "finish", 
					PageExitListener.CANCELED);
	}

	/**
	 * Return the page code for this page. This will be displayed in the title
	 * bar of the application for reference and customer support.
	 *
	 * @return the code for the page
	 */
	public String getPageCode() {
		return pageCode;
	}

	/**
	 *Return true if the side banner is to be displayed (normal case)
	 *
	 * @return the code for the page
	 */
	public boolean displaySideBanner() {
		return displaySideBanner;
	}

	/**
	 * Get the symbolic name of this page.
	 *
	 * @return the page name
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * Will be called before the enclosing container shows this page
	 *
	 * @param direction
	 *            specifies which way the flow is going to start this page
	 */
	public abstract void start(int direction);

	/**
	 * Will be called before the enclosing container hides this page
	 *
	 * @param direction
	 *            specifies which way the flow is going to exit this page
	 */
	public abstract void finish(int direction);

	/**
	 * Test the given components for values. If there is nothing in a field,
	 * focus it and return false.
	 *
	 * @param components
	 *            an array of JComponents to validate
	 * @return whether or not all the fields passed validation
	 */
	public boolean validateFields(JComponent[] components) {
		JComponent[] items = new JComponent[components.length];

		/*
		 * if (components instanceof JComboBox[]){ items = (JComboBox[])
		 * components; } else if (components instanceof JTextField[]){ items =
		 * (JTextField[]) components; } else{ items = components; }
		 */

		for (int i = 0; i < components.length; i++) {
			if ((components[i] instanceof StateListComboBox)
					&& (stateBoxIsEmpty))
				continue;
			items[i] = components[i];
		}
		if (!super.validateFieldItems(items)) {
			if (flowButtons != null)
				flowButtons.setButtonsEnabled(true);
			else
				setButtonsEnabled(true);
			return false;
		} else
			return true;
	}

	public void updateTransaction() {
	}

	public boolean validateEmailAddress(String emailAddress) {
		// Input the string for validation
		// String email = "xyz@hotmail.com";

		// Set the email pattern string
		Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
		if (!emailAddress.equals("")) {
			// Match the given string with the pattern
			Matcher m = p.matcher(emailAddress);

			// check whether match is found
			boolean matchFound = m.matches();

			if (matchFound)
				return true;
			else
				return false;
		} else
			return true;

	}

	public void deauthorize(ClientTransaction transaction) {
    	DeltaworksStartup.getInstance().setConnected(false);
		UnitProfile.getInstance().setLastCheckInTime(UnitProfileInterface.MFA_DEAUTHORIZATION);
		MainPanelDelegate.setDeauthMessage(true);
		DiagLog.getInstance().writeMustTransmit(MustTransmitDiagMsg.SECURITY_BREACH);
        flowButtons.setButtonsEnabled(true);
		transaction.setStatus(ClientTransaction.FAILED);
		PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
    }

    public int getRetryCount() {
    	return retryCount;
    }

    public void resetRetryCount() {
    	retryCount = 0;
    }

    public void incrementRetryCount() {
    	retryCount++;
    }

	protected void scrollpaneResized(JScrollPane sp1, JScrollPane sp2, JPanel panel2) {
		scrollpaneResized(sp1, sp2, panel2, true);
	}

	protected boolean scrollpaneResized(JScrollPane scrollPane1, JScrollPane scrollPane2, JPanel panel2, boolean addScrollBarFlag) {
		scrollPane2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		scrollPane2.setPreferredSize(panel2.getPreferredSize());

		panel2.invalidate();
		this.doLayout();

		int height1 = scrollPane1.getPreferredSize().getSize().height;
		int height2 = scrollPane1.getVisibleRect().height;

		boolean flag = (height1 > height2);
		if (addScrollBarFlag) {
			if (flag) {

				if (scrollPane1.getSize().width < 0) {
					return flag;
				}

				scrollPane2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

				int newHeight = scrollPane2.getPreferredSize().height - height1 + height2;
				int width1 = scrollPane2.getPreferredSize().width;
				int width2 = width1 + scrollPane2.getVerticalScrollBar().getPreferredSize().width;
				Dimension dimension = new Dimension(width2, newHeight);
				scrollPane2.setPreferredSize(dimension);
				dimension = new Dimension(width1, newHeight);
				panel2.setSize(dimension);
			}
			panel2.invalidate();
			scrollPane2.invalidate();
			this.validate();
			this.repaint();
		}
		return flag;
	}

	/** Scroll the viewport only if needed to make the given component visible. */
	protected void scrollToComponent(final JScrollPane scrollPane, final JPanel panel, final JComponent f) {
		Runnable task = new Runnable() {
			@Override
			public void run() {
				Rectangle scrollBounds = new Rectangle();
				Point scrollPoint = new Point();
				f.getBounds(scrollBounds);
				Rectangle convertedRectangle = SwingUtilities.convertRectangle(f.getParent(), scrollBounds, panel);
				Rectangle viewRect = scrollPane.getViewport().getViewRect();
				viewRect.x = 0;
				convertedRectangle.x = 0;
				if (!viewRect.contains(convertedRectangle)) {
					scrollPoint.x = 0;
					scrollPoint.y = convertedRectangle.y;
					scrollPane.getViewport().setViewPosition(scrollPoint);
				}

			}
		};
		SwingUtilities.invokeLater(task);
	}

	/** Set each component to trigger an automatic scroll on focus. */
	protected void setupAutoscroll(final JScrollPane scrollPane, final JPanel panel, JComponent fields[]) {
		for (int i = 0; i < fields.length; i++) {
			final JComponent c = fields[i];
			addFocusListener(c, new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent e) {
					scrollToComponent(scrollPane, panel, c);
				}
			});
		}
	}

    @Override
	public void commComplete(int commTag, Object returnValue) {
    }
    
    protected void displayLabelItem(String prefix, String value, List<JLabel> tags) {
    	displayLabelItem(prefix, value, true, true, tags);
    }

    protected void displayLabelItem(String prefix, String value, boolean uc, List<JLabel> tags) {
    	displayLabelItem(prefix, value, uc, true, tags);
    }
    
    protected void displayLabelItem(String prefix, String value, boolean uc, boolean displayWarning, List<JLabel> tags) {
    	JPanel panel = (JPanel) this.getComponent(prefix + "Panel", displayWarning);
    	JLabel tag = (JLabel) this.getComponent(prefix + "Tag");
    	JLabel label = (JLabel) this.getComponent(prefix + "Label");
    	if ((value == null) || value.isEmpty()) {
    		if (panel != null) {
    			panel.setVisible(false);
    		} else {
    			tag.setVisible(false);
    			label.setVisible(false);
    		}
    	} else {
    		if (panel != null) {
    			panel.setVisible(true);
    		} else {
    			tag.setVisible(true);
    			label.setVisible(true);
    		}
    		tags.add(tag);
    		label.setText(uc ? value.toUpperCase() : value);
    	}
    }
    
    protected void displayLabelItem(String prefix, String tag, String value, List<JLabel> tags) {
    	JPanel panel = (JPanel) this.getComponent(prefix + "Panel");
    	JLabel label1 = (JLabel) this.getComponent(prefix + "Tag");
    	JLabel label2 = (JLabel) this.getComponent(prefix + "Label");
    	if ((value == null) || value.isEmpty()) {
    		panel.setVisible(false);
    	} else {
    		panel.setVisible(true);
    		if (! tag.endsWith(":")) {
    			tag += ':';
    		}
    		label1.setText((tag != null) ? tag : ""); 
    		tags.add(label1);
    		label2.setText(value.toUpperCase());
    	}
    }

    protected void displayPaneItem(String prefix, String value, List<JLabel> tags, List<DWTextPane> panes) {
    	displayPaneItem(prefix, value, true, tags, panes);
    }    
    
    protected void displayPaneItem(String prefix, String value, boolean uc, List<JLabel> tags, List<DWTextPane> panes) {
    	JPanel panel = (JPanel) this.getComponent(prefix + "Panel");
    	if ((value == null) || value.isEmpty()) {
    		panel.setVisible(false);
    	} else {
        	JLabel tag = (JLabel) this.getComponent(prefix + "Tag");
        	DWTextPane pane = (DWTextPane) this.getComponent(prefix + "Pane");
    		panel.setVisible(true);
    		tags.add(tag);
    		pane.setText(uc ? value.toUpperCase() : value);
    		panes.add(pane);
    	}
    }

    protected void displayPaneItem(String prefix, String tag, String value, List<JLabel> tags, List<DWTextPane> panes) {
    	JPanel panel = (JPanel) this.getComponent(prefix + "Panel");
    	JLabel label = (JLabel) this.getComponent(prefix + "Tag");
    	DWTextPane pane = (DWTextPane) this.getComponent(prefix + "Pane");
    	if ((value == null) || value.isEmpty()) {
    		panel.setVisible(false);
    	} else {
    		panel.setVisible(true);
    		label.setText((tag != null) && (! tag.isEmpty()) && (! tag.endsWith(":")) ? (tag + ":") : tag); 
    		tags.add(label);
    		pane.setText(value.toUpperCase());
    		panes.add(pane);
    	}
    }
    
    protected void displayMoney(String prefix, BigDecimal amount, String currency, List<JLabel> tags) {
    	JPanel panel = (JPanel) this.getComponent(prefix + "Panel", false);
    	JLabel tagLabel = (JLabel) this.getComponent(prefix + "TagLabel");
    	JComponent amountComponent = (JComponent) this.getComponent(prefix + "AmountLabel");
    	JLabel currencyLabel = (JLabel) this.getComponent(prefix + "CurrencyLabel");
    	if ((amount != null) && (currency != null) && (! currency.isEmpty())) {
    		if (panel != null) {
    			panel.setVisible(true);
    		} else {
    			tagLabel.setVisible(true);
    			amountComponent.setVisible(true);
    			currencyLabel.setVisible(true);
    		}
    		tags.add(tagLabel);
    		if (amountComponent instanceof JLabel) {
    			((JLabel) amountComponent).setText(FormatSymbols.convertDecimal(amount.toString()));
    		} else if (amountComponent instanceof JTextField) {
    			((JTextField) amountComponent).setText(FormatSymbols.convertDecimal(amount.toString()));
    		}
    		currencyLabel.setText(currency);
    	} else {
    		if (panel != null) {
    			panel.setVisible(false);
    		} else {
    			tagLabel.setVisible(false);
    			amountComponent.setVisible(false);
    			currencyLabel.setVisible(false);
    		}
    	}
    }
    
    protected int setTagLabelWidths(List<JLabel> tags, int minimumWidth) {
    	int width = 0;
    	for (JLabel label : tags) {
    		label.setPreferredSize(null);
    		width = Math.max(width, label.getPreferredSize().width);
    	}
    	
    	width = Math.max(width + DataCollectionScreenToolkit.COMPONENT_SPACING, minimumWidth);
    	
    	Dimension d = new Dimension(width, 20);

    	for (JLabel label : tags) {
    		label.setPreferredSize(d);
    	}
    	
    	return width;
    }
    
    protected String getTranactionStatus(TransactionStatusType status, Map<String, String> values, boolean isPrevious, boolean isFormFree) {
    	String text = null;
    	
    	
		if (status != null) {
			switch (status) {
			case AVAIL:
				text = Messages.getString("DWValues.2047");
				break;
			case CANCL:
				text = Messages.getString("DWValues.2046");
				break;
			case RECVD:
				if (isPrevious) {
					text = Messages.getString("DWValues.2045");
				} else {
	    			text = Messages.getString("DWValues.2072");
				}
				break;
			case REFND:
				if (isPrevious) {
					text = Messages.getString("DWValues.2048");
				} else {
	    			text = Messages.getString("DWValues.2073");
				}
				break;
			case AFR:
				text = Messages.getString("DWValues.2048a");
				break;
			case UNCOMMITED:
				if (isFormFree) {
					text = Messages.getString("DWValues.2047b");
				} else {
					text = Messages.getString("DWValues.2047");
				}
				break;
			case PRCSS:
		    	if (values != null) {
		    		text = values.get(FieldKey.REFERENCE_NUMBER_TEXT.getInfoKey());
		    	}
		    	if ((text == null) || (text.isEmpty())) {
		    		text = Messages.getString("DWValues.2076");
		    	}
				break;
			}
		}
    	
    	if (text == null) {
    		text = Messages.getString("DWValues.2049");
    	}
    	
    	return "<B>" + text + "</B>";
    }
}
