package dw.framework;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.EventListener;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.FocusManager;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.moneygram.agentconnect.BusinessError;
import com.moneygram.agentconnect.CategoryInfo;
import com.moneygram.agentconnect.ChildField;
import com.moneygram.agentconnect.DataType;
import com.moneygram.agentconnect.EnumeratedIdentifierInfo;
import com.moneygram.agentconnect.EnumeratedTypeInfo;
import com.moneygram.agentconnect.FieldInfo;
import com.moneygram.agentconnect.FieldToCollectInfo;
import com.moneygram.agentconnect.InfoBase;

import dw.comm.MessageFacadeError;
import dw.dwgui.DWLabel;
import dw.dwgui.DWTextField;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextString;
import dw.dwgui.DelimitedTextField;
import dw.dwgui.DwSimpleDateFormat;
import dw.dwgui.HelpText;
import dw.dwgui.MultiListComboBox;
import dw.dwgui.PinpadTextField;
import dw.dwgui.StateListComboBox;
import dw.framework.DataCollectionScreenToolkit.DWComboBoxItem;
import dw.framework.DataCollectionScreenToolkit.DataCollectionComponent;
import dw.framework.DataCollectionScreenToolkit.DataCollectionFieldValidator;
import dw.framework.DataCollectionScreenToolkit.EnumeratedValue;
import dw.i18n.Messages;
import dw.main.DeltaworksStartup;
import dw.model.adapters.CountryInfo.Country;
import dw.model.adapters.CountryInfo.CountrySubdivision;
import dw.pinpad.Pinpad;
import dw.profile.UnitProfile;
import dw.utility.Debug;
import dw.utility.ExtraDebug;
import dw.utility.TimeUtility;

public class DataCollectionField implements Serializable, DataCollectionComponent {
	private static final long serialVersionUID = 1L;
	
	public static final int BOOLEAN_FIELD_TYPE = 1;
	public static final int COUNTRY_FIELD_TYPE = 2;
	public static final int STATE_FIELD_TYPE = 3;
	public static final int DATE_FIELD_TYPE = 4;
	public static final int DATETIME_FIELD_TYPE = 5;
	public static final int DECIMAL_FIELD_TYPE = 6;
	public static final int ENUM_FIELD_TYPE = 7;
	public static final int INT_FIELD_TYPE = 8;
	public static final int STRING_FIELD_TYPE = 10;
	public static final int TEXT_FIELD_TYPE = 11;
	public static final int TIME_FIELD_TYPE = 12;
	public static final int VALUE_FIELD_TYPE = 13;
	public static final int IFSC_FIELD_TYPE = 14;
	public static final int DIAL_CODE_FIELD_TYPE = 15;
	public static final int GDAY_FIELD_TYPE = 16;
	public static final int GMONTH_FIELD_TYPE = 17;
	public static final int GYEAR_FIELD_TYPE = 18;

	public static final int LAYOUT_UNDEFINED = 0;
	public static final int LAYOUT_HORIZONTALLY = 1;
	public static final int LAYOUT_CHECK_VALIDATION = 2;
	public static final int LAYOUT_VERIFY = 3;
	
	public static final int PAIR_COMPONENT_LAYOUT_FORMAT = 1;
	public static final int BOOLEAN_LAYOUT_FORMAT = 2;
	public static final int TEXT_LAYOUT_FORMAT = 3;
	public static final int JUSTIFY_LAYOUT_FORMAT = 4;
	public static final int IFSC_LAYOUT_FORMAT = 7;
	
	public static final int REQUIRED = 1;
	public static final int OPTIONAL = 2;
	public static final int READ_ONLY = 6;
	public static final int HIDDEN = 7;
	
	public static final int BUSINESS_ERROR_CHECK_VERIFY = 1;
	public static final int BUSINESS_ERROR_VALIDATION = 2;
	
	public static final Color ENABLE_COLOR = (new JTextField()).getBackground();
	public static final Color DISABLE_COLOR = (new JLabel()).getBackground();
	
	public static final int SILENT_VALIDATION = 1;
	public static final int DISPLAY_ERRORS_VALIDATION = 2;
	
	private static final int FIRST_CHARACTER_WIDTH;
	private static final int ADDITIONAL_CHARACTER_WIDTH;
	
	private static final String DATE_TEMPLATE;
//	private static final String TIME_TEMPLATE;
//	private static final String DATETIME_TEMPLATE;
	private static final FtcDateFormat DATE_FORMAT;
	private static final NumberFormat NUMBER_FORMAT;
	private static final Calendar CALENDAR = new GregorianCalendar();
	
	public static final int SCREEN_COLLECTION_UNDEFINED = 0;
	public static final int BASE_SCREENS_COLLECTION = 1;
	public static final int UPGRADED_REQUIREMENTS = 2;
	public static final int ERROR_SCREEN_COLLECTION = 3;
	public static final int ERROR_SCREEN_CORRECT_VALID = 4;

    public static final String LOOKUP_DATA_SOURCE_COUNTRY = "countryInfo";
    public static final String LOOKUP_DATA_SOURCE_COUNTRY_SUBDIVISION = "countrySubDivInfo";
    public static final String LOOKUP_DATA_SOURCE_COUNTRY_PHONE_CODE = "countryPhoneCodeInfo";
    public static final String LOOKUP_DATA_SOURCE_IFSC = "bankBranchInfo";
    public static final String LOOKUP_DATA_SOURCE_STATE = "stateProvinceInfo";

    private static final boolean NON_CHECKBOX_VERIFY_CHECK = true;
    
	private static String dateCharacters;
	private static String timeCharacters;
	private static String dateTimeCharacters;

	private FieldKey key;
	private String label;
	private Integer displayOrder;
	private Long fieldMin = 0L;
	private Long fieldMax = 0L;
	private int dataType;
	private List<EnumeratedValue> enumeratedValueInfoList;
	private boolean display;
	private String exampleFormat;
	private String shortHelpText;
	private String extendedHelpText;
	private String defaultValue;
	private String validationRegEx;
	private boolean enumerated;
	private boolean parentFieldSpecified; 
	private String lookupKey;
	
	private String standAloneLabel;
	private Boolean readOnly;
	private Boolean required;
	
	private int visibility;
	
	private Set<DataCollectionComponent> childFieldList;
	private DataCollectionField parentField;
	private DataCollectionField parentCountry;

	private boolean gffpEnabled;
	private String suffixText;
	private int maximumFieldLength;
	private int currentLayoutType;
	private boolean fieldEnabled;
	private int layoutFormat;
	private Pattern pattern;
	private KeyListener validationKeyListener;
	private DataCollectionFieldValidator validator;
	private DataCollectionPanel dataCollectionPanel;
	private int collectionStatus;
	private String oldValue;
	private String sortString;
	private boolean validatable;

	private DataCollectionPanel parentPanel;
	private DataCollectionSet dataSet;

	private JPanel panel;
	private JLabel requiredLabel;
	private DWTextString fieldLabel;
	private JComponent dataComponent;
	private HelpText helpIconLabel;
	private boolean validationFlag;
	private JLabel valueTypeLabel;
	private JPanel errorMessagePanel;
	private List<BusinessErrorMessage> businessErrors;
	private JLabel filler;
	private IfscSupplementalPanel supplementalPanel;
	private JCheckBox validationCheckBox;
	private JLabel validationMessageLabel;
	private EventListener validationChangeListener;
	private boolean modified;
	static private boolean enablableMofifyTracking = true;
	private JPanel relatedFieldsPanel;
	private List<DataCollectionField> relatedFields;
	
	static {
		JTextField tf1 = new JTextField("W");
		JTextField tf2 = new JTextField("WW");
		FIRST_CHARACTER_WIDTH = tf1.getPreferredSize().width;
		ADDITIONAL_CHARACTER_WIDTH = tf2.getPreferredSize().width - FIRST_CHARACTER_WIDTH;
		
		CALENDAR.setLenient(false);
        boolean isKorea = (UnitProfile.getInstance().get("AGENT_COUNTRY_ISO_CODE", "USA").equalsIgnoreCase("KOR"));
		DateFormat df = isKorea ? new SimpleDateFormat(TimeUtility.KOREAN_DATE_YEAR_MONTH_DAY_FORMAT_2) : DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault());
		Calendar calendar = df.getCalendar();
		calendar.set(3333, 10, 22);
		Date date = calendar.getTime();
		DATE_TEMPLATE = df.format(date).replaceAll("3333", "33").replaceAll("33", "3333");
		DwSimpleDateFormat dateFormat = new DwSimpleDateFormat(DATE_TEMPLATE.replaceAll("11", "MM").replaceAll("22", "dd").replaceAll("3333", "yyyy"));
		dateFormat.setLenient(false);
		dateCharacters = "0123456789";
		DATE_FORMAT = new FtcDateFormat();
		DATE_FORMAT.length = DATE_TEMPLATE.length();
		DATE_FORMAT.yearIndex = DATE_TEMPLATE.indexOf("3333");
		DATE_FORMAT.monthIndex = DATE_TEMPLATE.indexOf("11");
		DATE_FORMAT.dayIndex = DATE_TEMPLATE.indexOf("22");
		for (int i = 0; i < DATE_TEMPLATE.length(); i++) {
			char c = DATE_TEMPLATE.charAt(i);
			if (dateCharacters.indexOf(c) == -1) {
				dateCharacters += c;
			}
			if ((c != '1') && (c != '2') && (c != '3')) {
				DATE_FORMAT.extraCharacters.add(i);
			}
		}
		
		NUMBER_FORMAT = NumberFormat.getIntegerInstance();
		NUMBER_FORMAT.setMinimumIntegerDigits(2);
	}
	
	private static class FtcDateFormat {
		int length;
		int yearIndex;
		int monthIndex;
		int dayIndex;
		List<Integer> extraCharacters;
		
		private FtcDateFormat() {
			extraCharacters = new ArrayList<Integer>();
		}
	}

	private class DefaultNumberValidator implements DataCollectionFieldValidator, Serializable {
		private static final long serialVersionUID = 1L;

		private DataCollectionField numberField;
		private long minValue;
		private long maxValue;
		
		private DefaultNumberValidator(final DataCollectionField integerItem, long minValue, long maxValue) {
			this.numberField = integerItem;
			this.minValue = minValue;
			this.maxValue = maxValue;
			this.numberField.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						integerItem.getDataComponent().transferFocus();
					}
				}
				
				@Override
				public void keyReleased(KeyEvent e) {
					if (isValid(DataCollectionField.SILENT_VALIDATION) == null) {
						DefaultNumberValidator.this.numberField.getDataComponent().setForeground(Color.BLACK);
					} else {
						DefaultNumberValidator.this.numberField.getDataComponent().setForeground(Color.RED);
					}
				}
			});
		}
		
		@Override
		public DataCollectionField isValid(int mode) {
			String text = ((JTextField) this.numberField.getDataComponent()).getText();
			if ((text == null) || (text.length() == 0)) {
				return null;
			} 

			DataCollectionField tag = null;
			try {
				long value = 0;
				if (this.numberField.dataType == DataCollectionField.INT_FIELD_TYPE) {
					value = Long.parseLong(text);
				} else if (this.numberField.dataType == DataCollectionField.DECIMAL_FIELD_TYPE) {
					BigDecimal bd = new BigDecimal(text);
					value = bd.longValue();
				}
				if ((value < this.minValue) || (value > this.maxValue)) {
					tag = this.numberField;
				}
			} catch (Exception e) {
				tag = this.numberField;
			}
			return tag;
		}
	}
	
//	/** @deprecated */
//	public class DefaultIdValidator implements DataCollectionFieldValidator, Serializable {
//		private static final long serialVersionUID = 1L;
//
//		private JComboBox<DWComboBoxItem> idTypeBox;
//		private JTextField idNumberField;
//
//		public DefaultIdValidator(DataCollectionField idTypeField) {
//			this.idTypeBox = (JComboBox<DWComboBoxItem>) idTypeField.getDataComponent();
//			this.idNumberField = (JTextField) DataCollectionField.this.getDataComponent();
//
//			this.idTypeBox.addActionListener(this.actionListener);
//			this.idNumberField.addKeyListener(this.keyListener);
//			
//			idTypeField.setLinkField(DataCollectionField.this);
//			idTypeField.setLinkType(DataCollectionField.ID_NUMBER_LINK);
//			DataCollectionField.this.setLinkField(idTypeField);
//			DataCollectionField.this.setLinkType(DataCollectionField.ID_TYPE_LINK);
//		}
//
//		@Override
//		public DataCollectionField isValid(int mode) {
//			DataCollectionField field = null;
//			String idType = ((DWComboBoxItem) this.idTypeBox.getSelectedItem()).getValue();
//	        if ((idType.equals("SSN")) || (idType.equals("TAX"))) {
//				Matcher matcher = DataCollectionField.this.ID_VALUE_PATTERN.matcher(DataCollectionField.this.getValue());
//				if (matcher.matches()) {
//					this.idNumberField.setForeground(Color.BLACK);
//				} else {
//					this.idNumberField.setForeground(Color.RED);
//					field = DataCollectionField.this; 
//				}
//	        } else {
//	        	this.idNumberField.setForeground(Color.BLACK);
//	        }
//	        return field;
//		}
//		
//		private KeyListener keyListener = new DataFieldKeyListener(this);
//		
//		private ActionListener actionListener = new DataFieldActionListener(this, keyListener);
//	}
	
//	private class DataFieldKeyListener extends KeyAdapter implements Serializable {
//		private static final long serialVersionUID = 1L;
//
//		private DefaultIdValidator idValidator;
//		
//		private DataFieldKeyListener(DefaultIdValidator idValidator) {
//			this.idValidator = idValidator;
//		}
//		
//		@Override
//		public void keyPressed(KeyEvent e) {
//			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
//				idValidator.idNumberField.transferFocus();
//			}
//		}
//		
//		@Override
//		public void keyReleased(KeyEvent e) {
//			DWComboBoxItem field = (DWComboBoxItem) idValidator.idTypeBox.getSelectedItem();
//			String value = field.getValue();
//	        if ((value.equals("SSN")) || (value.equals("TAX"))) {
//				Matcher matcher = DataCollectionField.this.ID_VALUE_PATTERN.matcher(idValidator.idNumberField.getText());
//				if (matcher.matches()) {
//					idValidator.idNumberField.setForeground(Color.BLACK);
//				} else {
//					idValidator.idNumberField.setForeground(Color.RED);
//				}
//	        } else {
//	        	idValidator.idNumberField.setForeground(Color.BLACK);
//	        }
//		}
//	}
	
//	private class DataFieldActionListener implements ActionListener, Serializable {
//		private static final long serialVersionUID = 1L;
//
//		private DefaultIdValidator idValidator;
//		private KeyListener keyListener;
//		
//		private DataFieldActionListener(DefaultIdValidator idValidator, KeyListener keyListener) {
//			this.idValidator = idValidator;
//			this.keyListener = keyListener;
//		}
//		
//		@Override
//		public void actionPerformed(ActionEvent e) {
//			DWComboBoxItem field = (DWComboBoxItem) idValidator.idTypeBox.getSelectedItem();
//			String value = field.getValue();
//	        if ((value.equals("SSN")) || (value.equals("TAX"))) {
//	        	keyListener.keyReleased(null);
//	        } else {
//	        	idValidator.idNumberField.setForeground(Color.BLACK);
//	        }
//		}
//	}
	
	private class DefaultDateRangeValidator implements DataCollectionFieldValidator, Serializable {
		private static final long serialVersionUID = 1L;

		private DataCollectionField field;
		private int minValue;
		private int maxValue;
		private int maxLength;

		private DefaultDateRangeValidator(final DataCollectionField field, int minValue, int maxValue, int maxLength) {
			this.field = field;
			this.minValue = minValue;
			this.maxValue = maxValue;
			this.maxLength = maxLength;
			
			field.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						field.getDataComponent().transferFocus();
					}
				}
				
				@Override
				public void keyReleased(KeyEvent e) {
					if (isValid(DataCollectionField.SILENT_VALIDATION) == null) {
						field.getDataComponent().setForeground(Color.BLACK);
					} else {
						field.getDataComponent().setForeground(Color.RED);
					}
				}
			});
		}
		
		@Override
		public DataCollectionField isValid(int mode) {
			String text = ((JTextField) this.field.getDataComponent()).getText();
			if ((text != null) && (text.length() > maxLength)) {
				return field;
			}
			int value = 0;
			try {
				value = Integer.parseInt(text);
			} catch (Exception e) {
				return field;
			}
			if ((value < minValue) || (value > maxValue)) {
				return field;
			} 
			return null;
		}
	}
	
	private class DefaultDateValidator implements DataCollectionFieldValidator, Serializable {
		private static final long serialVersionUID = 1L;

		private DataCollectionField dateField;
		private long startTime;
		private long endTime;

		private DefaultDateValidator(final DataCollectionField dateItem, long startTime, long endTime) {
			this.dateField = dateItem;
			this.startTime = startTime;
			this.endTime = endTime;
			dateItem.addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						dateItem.getDataComponent().transferFocus();
					}
				}
				
				@Override
				public void keyReleased(KeyEvent e) {
					if (isValid(DataCollectionField.SILENT_VALIDATION) == null) {
						dateItem.getDataComponent().setForeground(Color.BLACK);
					} else {
						dateItem.getDataComponent().setForeground(Color.RED);
					}
				}
			});
		}
		
		@Override
		public DataCollectionField isValid(int mode) {
			String dateText = ((JTextField) this.dateField.getDataComponent()).getText();
			if ((dateText == null) || (dateText.isEmpty())) {
				return null;
			} else if (dateText.length() != DATE_FORMAT.length) {
				return this.dateField;
			} else {
				DataCollectionField tag = null;
				try {
					setCalendarDate(CALENDAR, dateText);
					if ((CALENDAR.getTimeInMillis() < startTime) || (CALENDAR.getTimeInMillis() > endTime)) {
						tag = dateField;
					}
				} catch (Exception e) {
					tag = dateField;
				}
				return tag;
			}
		}
		
		public String getTemplate() {
			String dd = Messages.getString("DataCollectionItem.Day");
			String mm = Messages.getString("DataCollectionItem.Month");
			String yyyy = Messages.getString("DataCollectionItem.Year");		
			return DATE_TEMPLATE.replaceAll("11", mm).replaceAll("22", dd).replaceAll("3333", yyyy);
		}
	}
	
	private void setCalendarDate(Calendar c, String dateText) throws Exception {
		for (int i : DATE_FORMAT.extraCharacters) {
			if (DATE_TEMPLATE.charAt(i) != dateText.charAt(i)) {
				throw new Exception();
			}
		}
		
		int y = Integer.parseInt(dateText.substring(DATE_FORMAT.yearIndex, DATE_FORMAT.yearIndex + 4));
		int m = Integer.parseInt(dateText.substring(DATE_FORMAT.monthIndex, DATE_FORMAT.monthIndex + 2)) - 1;
		int d = Integer.parseInt(dateText.substring(DATE_FORMAT.dayIndex, DATE_FORMAT.dayIndex + 2));
		c.set(y, m, d);
	}
	
	public class BusinessErrorMessage implements Serializable {
		private static final long serialVersionUID = 1L;

		private int businessErrorCategory;
		private DWTextPane errorMessageLabel;
		
		public DWTextPane getErrorMessageLabel() {
			return errorMessageLabel;
		}
	}
	
	private DataCollectionField(FieldKey key, DataCollectionSet dataSet, DataCollectionPanel parentPanel) {
		this.key = key;
		this.parentPanel = parentPanel;
		this.dataSet = dataSet;
		this.fieldEnabled = true;
		parentPanel.getChildDataCollectionFields().put(this.getKey(), this);
		dataSet.getFieldMap().put(key, this);
		this.parentFieldSpecified = false;
		this.validationFlag = false;
		this.sortString = "";
		this.validatable = true;
		this.modified = false;
	}
	
	public DataCollectionField(FieldKey key) {
		this.key = key;
		this.displayOrder = Integer.valueOf(0);
		this.fieldEnabled = true;
		this.sortString = "";
		this.validatable = true;
		this.modified = false;
	}

	public int getLayoutType() {
		return this.layoutFormat;
	}
	
	public DWTextString getLabelComponent() {
		return this.fieldLabel;
	}
	
	public JLabel getHelpIconComponent() {
		return this.helpIconLabel;
	}
	
	public JComponent getDataComponent() {
		return this.dataComponent;
	}
	
	public boolean isVisible() {
		return (this.getVisibility() == DataCollectionField.REQUIRED) || (this.getVisibility() == DataCollectionField.OPTIONAL) || (this.getVisibility() == DataCollectionField.READ_ONLY);
	}
	
	public boolean isRequired() {
		return this.visibility == DataCollectionField.REQUIRED;
	}

	public String getLabel() {
		return this.label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public int getMaximumFieldLength() {
		return this.maximumFieldLength;
	}

	public void setFieldMax(Long fieldMax) {
		this.fieldMax = fieldMax;
	}

	public int getDataType() {
		return this.dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	public List<EnumeratedValue> getEnumeratedValueInfoList(boolean removeNullValueField) {
		if (removeNullValueField) {
			List<EnumeratedValue> list = new ArrayList<EnumeratedValue>(this.enumeratedValueInfoList);
			for (int i = 0; i < list.size(); i++) {
				EnumeratedValue item = list.get(i);
				if (item.getValue().isEmpty()) {
					list.remove(i);
					return list;
				}
			}
		}
		return this.enumeratedValueInfoList;
	}

	public void setEnumeratedValueInfoList(Vector<EnumeratedValue> enumeratedValueInfoList) {
		this.enumeratedValueInfoList = enumeratedValueInfoList;
	}

	public boolean isDisplay() {
		return this.display;
	}

	public void setDisplay(boolean display) {
		this.display = display;
	}

	public void setExampleFormat(String exampleFormat) {
		this.exampleFormat = exampleFormat;
	}

	public void setExtendedHelpText(String extendedHelpText) {
		this.extendedHelpText = extendedHelpText;
	}
	
	public String getShortHelpText() {
		return this.shortHelpText;
	}

	public String getDefaultValue() {
		return this.defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public void setValidationRegEx(String validationRegEx) {
		this.validationRegEx = validationRegEx;
	}

	public void setCurrentLayoutType(int currentLayoutType) {
		this.currentLayoutType = currentLayoutType;
	}

	public Pattern getPattern() {
		return this.pattern;
	}

	public JLabel getRequiredLabel() {
		return this.requiredLabel;
	}

	public DWTextString getFieldLabel() {
		return this.fieldLabel;
	}

	public HelpText getHelpIconLabel() {
		return this.helpIconLabel;
	}

	public void setVisibility(int visibility) {
		this.visibility = visibility;
	}

	public void setType(int type) {
		this.layoutFormat = type;
	}

	public int getSuffixAndHelpIconWidth() {
		int w = 0;
		w += (this.helpIconLabel != null) ? (this.helpIconLabel.getPreferredSize().width + DataCollectionScreenToolkit.COMPONENT_SPACING): 0;
		return w;
	}

	public FieldKey getKey() {
		return this.key;
	}
	
	@Override
	public String getInfoKey() {
		return this.key.getInfoKey();
	}

	@Override
	public JPanel getPanel() {
		return this.panel;
	}

	public void layout(int newLayoutType, boolean displayRequiredFlag) {
		if (newLayoutType != this.getCurrentLayoutType()) {
			if (this.panel == null) {
				this.panel = new JPanel(new GridBagLayout());
			}
			GridBagLayout gbl = (GridBagLayout) this.panel.getLayout();
			GridBagConstraints requiredLabelGbc = (((this.requiredLabel != null) && displayRequiredFlag)) ? gbl.getConstraints(this.requiredLabel) : null;
			GridBagConstraints labelGbc = (this.fieldLabel != null) ? gbl.getConstraints((JComponent) this.fieldLabel) : null;
			GridBagConstraints dataComponentGbc = (this.dataComponent != null) ? gbl.getConstraints(this.dataComponent) : null;
			GridBagConstraints helpIconGbc = (this.helpIconLabel != null) ? gbl.getConstraints(this.helpIconLabel) : null;
			GridBagConstraints valueLabelGbc = (this.valueTypeLabel != null) ? gbl.getConstraints(this.valueTypeLabel) : null;
			GridBagConstraints errorMessageGbc = (this.errorMessagePanel != null) ? gbl.getConstraints(this.errorMessagePanel) : null;
			GridBagConstraints fillerGbc = (this.filler != null) ? gbl.getConstraints(this.filler) : null;
			GridBagConstraints supplementalGbc = (this.supplementalPanel != null) ? gbl.getConstraints(this.supplementalPanel) : null;
			GridBagConstraints validationCheckBoxGbc = (this.validationCheckBox != null) ? gbl.getConstraints(this.validationCheckBox) : null;
			GridBagConstraints validationMessageGbc = (this.validationMessageLabel != null) ? gbl.getConstraints(this.validationMessageLabel) : null;
			GridBagConstraints relatedFiedldsGbc = (this.relatedFieldsPanel != null) ? gbl.getConstraints(this.relatedFieldsPanel) : null;
			
			if (this.layoutFormat == DataCollectionField.BOOLEAN_LAYOUT_FORMAT) {
				labelGbc.gridx = 2;
				labelGbc.gridy = 0;
				labelGbc.weightx = 1.0;
				labelGbc.weighty = 1.0;
				labelGbc.anchor = GridBagConstraints.NORTHWEST;
				labelGbc.fill = GridBagConstraints.BOTH;
				labelGbc.insets = DataCollectionScreenToolkit.INSET_RIGHT;
				
				if ((this.requiredLabel != null) && displayRequiredFlag) {
					requiredLabelGbc.gridx = 0;
					requiredLabelGbc.gridy = 0;
					requiredLabelGbc.anchor = GridBagConstraints.NORTH;
					requiredLabelGbc.gridwidth = 1;
					requiredLabelGbc.fill = GridBagConstraints.NONE;
					requiredLabelGbc.insets = DataCollectionScreenToolkit.INSET_RIGHT;
				}
				
				dataComponentGbc.gridx = 1;
				dataComponentGbc.gridy = 0;
				dataComponentGbc.anchor = GridBagConstraints.NORTHWEST;
				dataComponentGbc.gridwidth = 1; 
				dataComponentGbc.weightx = 0.0;
				dataComponentGbc.fill = GridBagConstraints.NONE;
				dataComponentGbc.insets = DataCollectionScreenToolkit.INSET_RIGHT;
				
				if (this.helpIconLabel != null) {
					helpIconGbc.gridx = 4;
					helpIconGbc.gridy = 0;
					helpIconGbc.anchor = GridBagConstraints.NORTH;
					helpIconGbc.gridwidth = 1;
					helpIconGbc.fill = GridBagConstraints.NONE;
					helpIconGbc.insets = DataCollectionScreenToolkit.INSET_LEFT;
				}
				
				fillerGbc.gridx = 5;
				fillerGbc.gridy = 0;
				fillerGbc.weighty = 1.0;
				fillerGbc.fill = GridBagConstraints.VERTICAL;

				if ((newLayoutType == LAYOUT_CHECK_VALIDATION) || (NON_CHECKBOX_VERIFY_CHECK && (newLayoutType == LAYOUT_VERIFY))) {
					this.panel.setBorder(DataCollectionScreenToolkit.getCompoundTitleBorder(this.getStandAloneLabel()));
					
					errorMessageGbc.gridx = 0;
					errorMessageGbc.gridy = 2;
					errorMessageGbc.gridwidth = 5;
					errorMessageGbc.anchor = GridBagConstraints.WEST;
					errorMessageGbc.weightx = 1.0;
					errorMessageGbc.insets = DataCollectionScreenToolkit.INSET_BOTTOM;
					errorMessageGbc.fill = GridBagConstraints.HORIZONTAL;
					
					valueLabelGbc.gridx = 2;
					valueLabelGbc.gridy = 0;
					valueLabelGbc.weightx = 1.0;
					valueLabelGbc.weighty = 1.0;
					valueLabelGbc.anchor = GridBagConstraints.NORTHWEST;
					valueLabelGbc.fill = GridBagConstraints.BOTH;
					valueLabelGbc.insets = DataCollectionScreenToolkit.INSET_RIGHT;
					
					if (relatedFiedldsGbc != null) {
						relatedFiedldsGbc.gridx = 0;
						relatedFiedldsGbc.gridy = 1;
						relatedFiedldsGbc.gridwidth = 5;
						relatedFiedldsGbc.weightx = 1.0;
						relatedFiedldsGbc.insets = DataCollectionScreenToolkit.INSET_BOTTOM;
						relatedFiedldsGbc.fill = GridBagConstraints.HORIZONTAL;
					}
					
					if (this.fieldLabel != null) {
						this.fieldLabel.setVisible(false);
					}
					
					if (validationCheckBox != null) {
						validationCheckBox.setVisible(false);
					}
					
					if (validationMessageLabel != null) {
						validationMessageLabel.setVisible(false);
					}

					if (this.valueTypeLabel != null) {
						this.valueTypeLabel.setVisible(true);
					}
				} else {
					this.panel.setBorder(null);
					
					if (this.fieldLabel != null) {
						this.fieldLabel.setVisible(true);
					}
					
					if (this.errorMessagePanel != null) {
						this.errorMessagePanel.setVisible(false);
					}

					if (this.valueTypeLabel != null) {
						this.valueTypeLabel.setVisible(false);
					}
					
					if (this.relatedFieldsPanel != null) {
						this.relatedFieldsPanel.setVisible(false);
					}
				}
				
			} else if (this.layoutFormat == DataCollectionField.TEXT_LAYOUT_FORMAT) {
				labelGbc.gridx = 0;
				labelGbc.gridy = 0;
				labelGbc.gridwidth = 1; 
				labelGbc.weightx = 1.0;
				labelGbc.anchor = GridBagConstraints.NORTHWEST;
				labelGbc.fill = GridBagConstraints.HORIZONTAL;
				labelGbc.insets = DataCollectionScreenToolkit.INSET_NONE;
				
				if (this.requiredLabel != null) {
					this.requiredLabel.setVisible(false);
				}
				if (this.fieldLabel != null) {
					this.fieldLabel.setVisible(true);
				}
				if (this.helpIconLabel != null) {
					this.helpIconLabel.setVisible(false);
				}
			
			} else if (this.layoutFormat == DataCollectionField.JUSTIFY_LAYOUT_FORMAT) {
				labelGbc.gridx = 0;
				labelGbc.gridy = 0;
				labelGbc.gridwidth = 1; 
				labelGbc.weightx = 1.0;
				labelGbc.anchor = GridBagConstraints.WEST;
				labelGbc.fill = GridBagConstraints.HORIZONTAL;
				labelGbc.insets = DataCollectionScreenToolkit.INSET_NONE;
				
				dataComponentGbc.gridx = 1;
				dataComponentGbc.gridy = 0;
				dataComponentGbc.gridwidth = 0; 
				dataComponentGbc.ipadx = DataCollectionScreenToolkit.COMPONENT_SPACING * 2;
				dataComponentGbc.anchor = GridBagConstraints.EAST;
				dataComponentGbc.insets = DataCollectionScreenToolkit.INSET_NONE;
				
				this.panel.removeAll();
				this.panel.add((JComponent) this.fieldLabel, labelGbc);
				this.panel.add(this.dataComponent, dataComponentGbc);
				
			} else if (this.layoutFormat == DataCollectionField.IFSC_LAYOUT_FORMAT) {
				if (newLayoutType == LAYOUT_HORIZONTALLY) {
					int gridX = 0;
					if (this.valueTypeLabel != null) {
						this.valueTypeLabel.setVisible(false);
					}
					this.panel.setBorder(IfscSupplementalPanel.TITLED_BORDER);
					if (this.fieldLabel != null) {
						if ((this.requiredLabel != null) && displayRequiredFlag) {
							this.requiredLabel.setVisible(true);
							requiredLabelGbc.gridx = gridX;
							requiredLabelGbc.gridy = 0;
							requiredLabelGbc.gridwidth = 1;
							requiredLabelGbc.insets = DataCollectionScreenToolkit.INSET_RIGHT;
							gridX++;
						}
						this.fieldLabel.setVisible(true);
						labelGbc.gridx = gridX;
						labelGbc.gridy = 0;
						labelGbc.gridwidth = 1;
						labelGbc.weightx = 0.0;
						labelGbc.anchor = GridBagConstraints.NORTHWEST;
						labelGbc.fill = GridBagConstraints.NONE;
						labelGbc.insets = DataCollectionScreenToolkit.INSET_RIGHT;
						gridX++;
					}
					
					dataComponentGbc.gridx = gridX;
					dataComponentGbc.gridy = 0;
					dataComponentGbc.gridwidth = 1; 
					dataComponentGbc.weightx = 1.0;
					dataComponentGbc.fill = GridBagConstraints.HORIZONTAL;
					gridX++;
					
					if (this.helpIconLabel != null) {
						helpIconGbc.gridx = gridX;
						helpIconGbc.gridy = 0;
						helpIconGbc.gridwidth = 1;
						helpIconGbc.insets = DataCollectionScreenToolkit.INSET_LEFT;
						gridX++;
					}
					
					supplementalGbc.gridx = 0;
					supplementalGbc.gridy = 1;
					supplementalGbc.gridwidth = gridX;
					supplementalGbc.fill = GridBagConstraints.HORIZONTAL;
					
					if (errorMessagePanel != null) {
						errorMessagePanel.setVisible(false);
					}
					
					if (this.relatedFieldsPanel != null) {
						this.relatedFieldsPanel.setVisible(false);
					}
				} else if (newLayoutType == LAYOUT_CHECK_VALIDATION) {

					this.fieldLabel.setVisible(false);
					if (this.requiredLabel != null) {
						this.requiredLabel.setVisible(false);
					}
					this.errorMessagePanel.setVisible(true);
					
					this.panel.setBorder(DataCollectionScreenToolkit.getCompoundTitleBorder(this.getStandAloneLabel()));

					this.valueTypeLabel.setVisible(true);
					valueLabelGbc.gridx = 0;
					valueLabelGbc.gridy = 0;
					valueLabelGbc.insets = DataCollectionScreenToolkit.INSET_RIGHT_BOTTOM;
					
					dataComponentGbc.gridx = 1;
					dataComponentGbc.gridy = 0;
					dataComponentGbc.gridwidth = 1; 
					dataComponentGbc.weightx = 1.0;
					dataComponentGbc.fill = GridBagConstraints.HORIZONTAL;
					dataComponentGbc.insets = DataCollectionScreenToolkit.INSET_BOTTOM;
					
					if (this.helpIconLabel != null) {
						helpIconGbc.gridx = 3;
						helpIconGbc.gridy = 0;
						helpIconGbc.gridwidth = 1;
						helpIconGbc.insets = DataCollectionScreenToolkit.INSET_LEFT_BOTTOM;
					}
					
					supplementalGbc.gridx = 0;
					supplementalGbc.gridy = 1;
					supplementalGbc.gridwidth = 4;
					supplementalGbc.anchor = GridBagConstraints.WEST;
					supplementalGbc.weightx = 1.0;
					supplementalGbc.insets = DataCollectionScreenToolkit.INSET_BOTTOM;
					supplementalGbc.fill = GridBagConstraints.HORIZONTAL;
					this.supplementalPanel.setVisible(true);
					
					errorMessageGbc.gridx = 0;
					errorMessageGbc.gridy = 2;
					errorMessageGbc.gridwidth = 4;
					errorMessageGbc.anchor = GridBagConstraints.WEST;
					errorMessageGbc.weightx = 1.0;
					errorMessageGbc.insets = DataCollectionScreenToolkit.INSET_BOTTOM;
					errorMessageGbc.fill = GridBagConstraints.HORIZONTAL;
				}
			} else if (this.layoutFormat == DataCollectionField.PAIR_COMPONENT_LAYOUT_FORMAT) {
				if (newLayoutType == LAYOUT_HORIZONTALLY) {
					if (this.valueTypeLabel != null) {
						this.valueTypeLabel.setVisible(false);
					}
					if (this.fieldLabel != null) {
						this.fieldLabel.setVisible(true);
						labelGbc.gridx = 1;
						labelGbc.gridy = 0;
						labelGbc.gridwidth = 1;
						labelGbc.weightx = 0.0;
						labelGbc.anchor = GridBagConstraints.NORTHWEST;
						labelGbc.fill = GridBagConstraints.NONE;
						labelGbc.insets = DataCollectionScreenToolkit.INSET_RIGHT;
						if ((this.requiredLabel != null) && displayRequiredFlag) {
							this.requiredLabel.setVisible(true);
							requiredLabelGbc.gridx = 0;
							requiredLabelGbc.gridy = 0;
							requiredLabelGbc.gridwidth = 1;
							requiredLabelGbc.insets = DataCollectionScreenToolkit.INSET_RIGHT;
						}
					}
					
					dataComponentGbc.gridx = 2;
					dataComponentGbc.gridy = 0;
					dataComponentGbc.gridwidth = 1; 
					dataComponentGbc.weightx = 1.0;
					dataComponentGbc.fill = GridBagConstraints.HORIZONTAL;
					
					if (this.helpIconLabel != null) {
						helpIconGbc.gridx = 4;
						helpIconGbc.gridy = 0;
						helpIconGbc.gridwidth = 1;
						helpIconGbc.insets = DataCollectionScreenToolkit.INSET_LEFT;
					}
					this.panel.setBorder(BorderFactory.createEmptyBorder());
					if (errorMessagePanel != null) {
						errorMessagePanel.setVisible(false);
					}

					if (validationCheckBox != null) {
						validationCheckBox.setVisible(false);
					}
					
					if (validationMessageLabel != null) {
						validationMessageLabel.setVisible(false);
					}
				} else if ((newLayoutType == LAYOUT_CHECK_VALIDATION) || (NON_CHECKBOX_VERIFY_CHECK && (newLayoutType == LAYOUT_VERIFY))) {

					this.fieldLabel.setVisible(false);
					if (this.requiredLabel != null) {
						this.requiredLabel.setVisible(false);
					}
					this.errorMessagePanel.setVisible(true);
//					this.validationFlag = true;
					
					this.panel.setBorder(DataCollectionScreenToolkit.getCompoundTitleBorder(this.getStandAloneLabel()));

					this.valueTypeLabel.setVisible(true);
					valueLabelGbc.gridx = 0;
					valueLabelGbc.gridy = 0;
					valueLabelGbc.insets = DataCollectionScreenToolkit.INSET_RIGHT_BOTTOM;
					
					dataComponentGbc.gridx = 1;
					dataComponentGbc.gridy = 0;
					dataComponentGbc.gridwidth = 1; 
					dataComponentGbc.weightx = 1.0;
					dataComponentGbc.fill = GridBagConstraints.HORIZONTAL;
					dataComponentGbc.insets = DataCollectionScreenToolkit.INSET_BOTTOM;
					
					if (this.helpIconLabel != null) {
						helpIconGbc.gridx = 3;
						helpIconGbc.gridy = 0;
						helpIconGbc.gridwidth = 1;
						helpIconGbc.insets = DataCollectionScreenToolkit.INSET_LEFT_BOTTOM;
					}
					
					if (relatedFiedldsGbc != null) {
						relatedFiedldsGbc.gridx = 0;
						relatedFiedldsGbc.gridy = 1;
						relatedFiedldsGbc.gridwidth = 5;
						relatedFiedldsGbc.weightx = 1.0;
						relatedFiedldsGbc.insets = DataCollectionScreenToolkit.INSET_BOTTOM;
						relatedFiedldsGbc.fill = GridBagConstraints.HORIZONTAL;
					}
					
					errorMessageGbc.gridx = 0;
					errorMessageGbc.gridy = 2;
					errorMessageGbc.gridwidth = 4;
					errorMessageGbc.anchor = GridBagConstraints.WEST;
					errorMessageGbc.weightx = 1.0;
					errorMessageGbc.insets = DataCollectionScreenToolkit.INSET_BOTTOM;
					errorMessageGbc.fill = GridBagConstraints.HORIZONTAL;
					
					if (validationCheckBox != null) {
						validationCheckBox.setVisible(false);
					}
					
					if (validationMessageLabel != null) {
						validationMessageLabel.setVisible(false);
					}
//				} else if (newLayoutType == LAYOUT_VERIFY) {
//					
//					this.fieldLabel.setVisible(false);
//					if (this.requiredLabel != null) {
//						this.requiredLabel.setVisible(false);
//					}
//					this.errorMessagePanel.setVisible(true);
//					this.validationCheckBox.setVisible(true);
//					if (validationMessageLabel != null) {
//						validationMessageLabel.setVisible(true);
//					}
//					if (valueTypeLabel != null) {
//						valueTypeLabel.setVisible(true);
//					}
//					
//					this.panel.setBorder(DataCollectionScreenToolkit.getCompoundTitleBorder(this.getStandAloneLabel()));
//
//					validationCheckBoxGbc.gridx = 0;
//					validationCheckBoxGbc.gridy = 0;
//					validationCheckBoxGbc.gridwidth = 1;
//					validationCheckBoxGbc.insets = DataCollectionScreenToolkit.INSET_RIGHT;
//					
//					validationMessageGbc.gridx = 1;
//					validationMessageGbc.gridy = 0;
//					validationMessageGbc.gridwidth = 4;
//					validationMessageGbc.weightx = 1.0;
//					validationMessageGbc.fill = GridBagConstraints.HORIZONTAL;
//					
////					if (fieldLabel != null) {
////						gbc2.gridx = 2;
////						gbc2.gridy = 0;
////						gbc2.gridwidth = 1;
////						gbc2.weightx = 0.0;
////						gbc2.anchor = GridBagConstraints.NORTHWEST;
////						gbc2.fill = GridBagConstraints.NONE;
////						gbc2.insets = DataCollectionScreenToolkit.INSET_RIGHT;
////					}
//					
//					valueLabelGbc.gridx = 0;
//					valueLabelGbc.gridy = 1;
//					valueLabelGbc.gridwidth = 2;
//					valueLabelGbc.insets = DataCollectionScreenToolkit.INSET_RIGHT_BOTTOM;
//					
//					dataComponentGbc.gridx = 2;
//					dataComponentGbc.gridy = 1;
//					dataComponentGbc.gridwidth = 1; 
//					dataComponentGbc.weightx = 1.0;
//					dataComponentGbc.fill = GridBagConstraints.HORIZONTAL;
//					dataComponentGbc.insets = DataCollectionScreenToolkit.INSET_BOTTOM;
//					
//					if (this.helpIconLabel != null) {
//						helpIconGbc.gridx = 4;
//						helpIconGbc.gridy = 1;
//						helpIconGbc.gridwidth = 1;
//						helpIconGbc.insets = DataCollectionScreenToolkit.INSET_LEFT_BOTTOM;
//					}
//					
//					errorMessageGbc.gridx = 0;
//					errorMessageGbc.gridy = 2;
//					errorMessageGbc.gridwidth = 5;
//					errorMessageGbc.anchor = GridBagConstraints.WEST;
//					errorMessageGbc.weightx = 1.0;
//					errorMessageGbc.insets = DataCollectionScreenToolkit.INSET_BOTTOM;
//					errorMessageGbc.fill = GridBagConstraints.HORIZONTAL;
				}
			}

			if (this.fieldLabel != null) {
				if (this.requiredLabel != null) {
					if (displayRequiredFlag) {
						gbl.setConstraints(this.requiredLabel, requiredLabelGbc);
					} else {
						this.requiredLabel.setVisible(false);
					}
				}			
				gbl.setConstraints((JComponent) this.fieldLabel, labelGbc);
			}
			if (this.dataComponent != null) {
				gbl.setConstraints(this.dataComponent, dataComponentGbc);
			}
			if (this.helpIconLabel != null) {
				gbl.setConstraints(this.helpIconLabel, helpIconGbc);
			}
			if (this.filler != null) {
				gbl.setConstraints(this.filler, fillerGbc);
			}
			if (this.valueTypeLabel != null) {
				gbl.setConstraints(this.valueTypeLabel, valueLabelGbc);
			}
			if (this.errorMessagePanel != null) {
				gbl.setConstraints(this.errorMessagePanel, errorMessageGbc);
			}
			if (this.supplementalPanel != null) {
				gbl.setConstraints(this.supplementalPanel, supplementalGbc);
			}
			if (this.validationCheckBox != null) {
				gbl.setConstraints(this.validationCheckBox, validationCheckBoxGbc);
			}
			if (this.validationMessageLabel != null) {
				gbl.setConstraints(this.validationMessageLabel, validationMessageGbc);
			}
			if (this.relatedFieldsPanel != null) {
				gbl.setConstraints(this.relatedFieldsPanel, relatedFiedldsGbc);
			}
			
			this.setCurrentLayoutType(newLayoutType); 
		}
	}

	public int getVisibility() {
		return this.visibility;
	}

	public JComponent createComponent(String defaultListValue) {
		JComponent dataComponent = null;
//		if (dataComponentType != null) {
//			if (dataComponentType.equals(StateListComboBox.class)) {
//				StateListComboBox<DWComboBoxField> cb = new StateListComboBox<DWComboBoxField>();
//				dataComponent = cb;
//			} else if (dataComponentType.equals(MultiListComboBox.class)) {
//				MultiListComboBox<DWComboBoxField> cb = new MultiListComboBox<DWComboBoxField>();
//				dataComponent = cb;
//			} else if (dataComponentType.equals(DelimitedTextField.class)) {
//				DelimitedTextField cb = new DelimitedTextField();
//				dataComponent = cb;
//				this.maximumFieldLength = this.fieldMax.intValue();
//			}

		if (this.dataType == ENUM_FIELD_TYPE) {
			MultiListComboBox<DWComboBoxItem> cb = new MultiListComboBox<DWComboBoxItem>();
			dataComponent = cb;
			if ((this.enumeratedValueInfoList != null) && (this.enumeratedValueInfoList.size() > 0)) {
				cb.addList(this.enumeratedValueInfoList.toArray(), "values");
			}
			if (this.getVisibility() == DataCollectionField.READ_ONLY) {
				cb.setEditable(false);
				cb.setEnabled(false);
			}
		
		} else if (this.dataType == STRING_FIELD_TYPE) {
			if (((this.enumeratedValueInfoList != null) && (this.enumeratedValueInfoList.size() > 0)) || this.enumerated) {
				MultiListComboBox<DWComboBoxItem> cb = new MultiListComboBox<DWComboBoxItem>();
				dataComponent = cb;
				if (this.enumeratedValueInfoList.size() > 0) {
					cb.addList(this.enumeratedValueInfoList.toArray(), "values");
				}
				if (this.getVisibility() == DataCollectionField.READ_ONLY) {
					cb.setEditable(false);
					cb.setEnabled(false);
				}
			} else if (! this.display) {
				JPasswordField pf = new JPasswordField();
				dataComponent = pf;
				pf.setEchoChar('*');
				if (this.getVisibility() == DataCollectionField.READ_ONLY) {
					pf.setEditable(false);
					pf.setEnabled(false);
				}
			} else {
				DelimitedTextField tf = new DelimitedTextField();
				dataComponent = tf;
				if ((this.fieldMax != null) && (this.fieldMax > 0)) {
					tf.setMaxFieldLength(this.fieldMax.intValue());
					this.maximumFieldLength = this.fieldMax.intValue();
				}
				if (this.getVisibility() == DataCollectionField.READ_ONLY) {
					tf.setEditable(false);
					tf.setEnabled(false);
				}
			}
			
		} else if (this.dataType == BOOLEAN_FIELD_TYPE) {
			JCheckBox cb = new JCheckBox();
			dataComponent = cb;
			DataCollectionField.enablableMofifyTracking(false);
			cb.setSelected((this.defaultValue != null) && (this.defaultValue.equalsIgnoreCase("true")));
			DataCollectionField.enablableMofifyTracking(true);
			if (this.getVisibility() == DataCollectionField.READ_ONLY) {
				cb.setEnabled(false);
			}
		
		} else if (this.dataType == COUNTRY_FIELD_TYPE) {
			MultiListComboBox<Country> cb = new MultiListComboBox<Country>(); 
			dataComponent = cb;
			if (this.getVisibility() == DataCollectionField.READ_ONLY) {
				cb.setEditable(false);
				cb.setEnabled(false);
			}
		
		} else if (this.dataType == STATE_FIELD_TYPE) {
			MultiListComboBox<CountrySubdivision> cb = new StateListComboBox<CountrySubdivision>(); 
			dataComponent = cb;
			if (this.getVisibility() == DataCollectionField.READ_ONLY) {
				cb.setEditable(false);
				cb.setEnabled(false);
			}
		
		} else if (this.dataType == INT_FIELD_TYPE) {
			DelimitedTextField tf = new DelimitedTextField();
			dataComponent = tf;
			tf.setValidChars("+-0123456789");
			if (this.fieldMax > 0) {
				this.maximumFieldLength = String.valueOf(this.fieldMax).length() + 1;
				tf.setMaxFieldLength(this.maximumFieldLength);
			}
			if (this.getVisibility() == DataCollectionField.READ_ONLY) {
				tf.setEditable(false);
				tf.setEnabled(false);
			}
		
		} else if (this.dataType == DIAL_CODE_FIELD_TYPE) {
			MultiListComboBox<Country> cb = new MultiListComboBox<Country>(); 
			dataComponent = cb;
			if (this.getVisibility() == DataCollectionField.READ_ONLY) {
				cb.setEditable(false);
				cb.setEnabled(false);
			}
		
		} else if (this.dataType == DECIMAL_FIELD_TYPE) {
			DelimitedTextField tf = new DelimitedTextField();
			dataComponent = tf;
			tf.setValidChars("0123456789.-");
			if (this.fieldMax > 0) {
				this.maximumFieldLength = String.valueOf(this.fieldMax).length() + 3;
				tf.setMaxFieldLength(this.maximumFieldLength);
			}
			if (this.getVisibility() == DataCollectionField.READ_ONLY) {
				tf.setEditable(false);
				tf.setEnabled(false);
			}
		
		} else if (this.dataType == DATE_FIELD_TYPE) {
			DelimitedTextField tf = new DelimitedTextField();
			tf.setValidChars(dateCharacters);
			dataComponent = tf;
			if (this.getVisibility() == DataCollectionField.READ_ONLY) {
				tf.setEditable(false);
				tf.setEnabled(false);
			}
		
		} else if (this.dataType == TIME_FIELD_TYPE) {
			DelimitedTextField tf = new DelimitedTextField();
			tf.setValidChars(timeCharacters);
			dataComponent = tf;
			if (this.getVisibility() == DataCollectionField.READ_ONLY) {
				tf.setEditable(false);
				tf.setEnabled(false);
			}
		
		} else if (this.dataType == DATETIME_FIELD_TYPE) {
			DelimitedTextField tf = new DelimitedTextField();
			tf.setValidChars(dateTimeCharacters);
			dataComponent = tf;
			if (this.getVisibility() == DataCollectionField.READ_ONLY) {
				tf.setEditable(false);
				tf.setEnabled(false);
			}
			
		} else if ((this.dataType == GDAY_FIELD_TYPE) || (this.dataType == GMONTH_FIELD_TYPE) || (this.dataType == GYEAR_FIELD_TYPE)) {
			DelimitedTextField tf = new DelimitedTextField();
			dataComponent = tf;
			tf.setValidChars("0123456789");
			if (this.getVisibility() == DataCollectionField.READ_ONLY) {
				tf.setEditable(false);
				tf.setEnabled(false);
			}
			
		} else if (this.dataType == VALUE_FIELD_TYPE) {
			dataComponent = new DWTextPane();
		
		} else if (this.dataType == IFSC_FIELD_TYPE) {
			dataComponent = new DelimitedTextField();
		}
		
		// If the component type is a combo box and a list of items have been
		// specified for the combo box, populate the combo box.
		
//		if ((dataComponent != null) && (dataComponent instanceof MultiListComboBox) && (listItems != null)) {
//			MultiListComboBox<DWComboBoxField> cb = (MultiListComboBox<DWComboBoxField>) dataComponent;
//			if (cb.getItemCount() == 0) {
//				cb.addList(listItems, "");
//				cb.setSelectedComboBoxItem(defaultListValue);
//				if (defaultListValue != null) {
//			        cb.setSelectedComboBoxItem(defaultListValue);
//				}
//			}
//		}

		return dataComponent;
	}
	
	public void setupDataCollectionField(Map<FieldKey, DataCollectionField> screenFields, String defaultListValue, DataCollectionPanel dataCollectionPanel, final JButton backButton, final JButton nextButton, String receiveCountry, final DataCollectionScreenToolkit toolkit) {
		
        // Add the field to the screen item map.
        
        if (screenFields != null) {
        	screenFields.put(getKey(), this);
        }
        
        this.setCurrentLayoutType(LAYOUT_UNDEFINED);
        
        this.dataCollectionPanel = dataCollectionPanel;

		// Panel
		
        if (this.panel == null) {
        	this.panel = new JPanel(new GridBagLayout());
        }
		this.panel.setVisible(true);
		
		// Required Label 
		
		if ((this.isRequired()) && (this.dataType != DataCollectionField.BOOLEAN_FIELD_TYPE) && (requiredLabel == null)) {
			this.requiredLabel = new JLabel("*");
			this.requiredLabel.setForeground(Color.RED);
			this.panel.add(this.requiredLabel);
		}
		
		// Data Component
		
		if (this.dataComponent == null) {
			this.dataComponent = createComponent(defaultListValue);
		}
		if (this.dataComponent != null) {
			this.panel.add(this.dataComponent);
		}
		
		// Default value
		
		if (defaultListValue != null) {
			this.defaultValue = defaultListValue;
		}
		
		// If a value of the field is validated by a regular expression,
		// add an listener to check if the value is valid.
		
		if (this.dataComponent instanceof JTextField) {
			
			// If a regular express is specified in the GFFP and the data type is not a date, time or datetime, 
			// then use that expression. Do not use a regular express for a date since the
			// values needs to parsed from the text entered and just the regular express do not provide that capability.
			
			if ((this.validationRegEx != null) && (! this.validationRegEx.isEmpty()) && (this.dataType != DATE_FIELD_TYPE) && (this.dataType != TIME_FIELD_TYPE) && (this.dataType != DATETIME_FIELD_TYPE)) {
				this.pattern = Pattern.compile(this.validationRegEx);
				setValidation(this.pattern);

			} else {
				if (this.dataType == DATE_FIELD_TYPE) {
					long startTime = new Date().getTime() + Math.round(-125 * 365.25 * 24 * 3600 * 1000);
					long endTime = new Date().getTime() + Math.round(50 * 365.25 * 24 * 3600 * 1000);
					DefaultDateValidator validator = new DefaultDateValidator(this, startTime, endTime);
					this.suffixText = validator.getTemplate();
					setValidator(validator);
					this.maximumFieldLength = this.suffixText.length();
//				} else if (this.dataType == TIME_ITEM_TYPE) {
//					DefaultTimeValidator validator = new DefaultTimeValidator(this);
//					this.suffixText = validator.getTemplate();
//					setValidator(validator);
//					this.maximumFieldLength = this.suffixText.length();
//				} else if (this.dataType == DATETIME_ITEM_TYPE) {
//					long startTime = new Date().getTime() + Math.round(-125 * 365.25 * 24 * 3600 * 1000);
//					long endTime = new Date().getTime() + Math.round(50 * 365.25 * 24 * 3600 * 1000);
//					DefaultDateTimeValidator validator = new DefaultDateTimeValidator(this, startTime, endTime);
//					this.suffixText = validator.getTemplate();
//					setValidator(validator);
//					this.maximumFieldLength = this.suffixText.length();
				} else if ((this.dataType == INT_FIELD_TYPE) || (this.dataType == DECIMAL_FIELD_TYPE)) {
					if ((fieldMin == 0L) && (fieldMax == 0L)) {
						fieldMax = Long.MAX_VALUE;
					}
					DataCollectionFieldValidator validator = new DefaultNumberValidator(this, this.fieldMin, this.fieldMax);
					setValidator(validator);
				} else if (this.dataType == GDAY_FIELD_TYPE) {
					setValidator(new DefaultDateRangeValidator(this, 1, 31, 2));
				} else if (this.dataType == GMONTH_FIELD_TYPE) {
					setValidator(new DefaultDateRangeValidator(this, 1, 12, 2));
				} else if (this.dataType == GYEAR_FIELD_TYPE) {
					Calendar c = new GregorianCalendar();
					int year = c.get(Calendar.YEAR);
					setValidator(new DefaultDateRangeValidator(this, year - 125, year + 50, 4));
				}
			}
			
			if ((this.dataType == STRING_FIELD_TYPE)) {
				if (fieldMin == 0L) {
					fieldMin = 1L;
				} 
				if (fieldMax == 0L) {
					fieldMax = Long.MAX_VALUE;
				}
			}
		}
		
		// Add a listener to check if the field value has been modified.
		
		if (dataComponent instanceof JTextField) {
			((JTextField) dataComponent).addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					modified = true;
				}
			});
		} else if (dataComponent instanceof JComboBox) {
			((JComboBox) dataComponent).addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (enablableMofifyTracking) {
						modified = true;
					}
				}
			});
		} else if (dataComponent instanceof JCheckBox) {
			((JCheckBox) dataComponent).addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (enablableMofifyTracking) {
						modified = true;
					}
				}
			});
			((JCheckBox) dataComponent).addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent e) {
					if (e.getKeyChar() == KeyEvent.VK_ENTER) {
						dataComponent.transferFocus();
					}
				}
			});
		}
		
		if (this.dataType == DataCollectionField.BOOLEAN_FIELD_TYPE) {
			this.filler = new JLabel();
			this.panel.add(this.filler);
		}
		
		//----------------------------------------------------------------------
		
		if (this.dataType == DataCollectionField.IFSC_FIELD_TYPE) {
			this.layoutFormat = DataCollectionField.IFSC_LAYOUT_FORMAT;
		} else if (this.dataComponent instanceof JCheckBox) {
			this.layoutFormat = DataCollectionField.BOOLEAN_LAYOUT_FORMAT;
		} else if (this.dataComponent == null) {
			this.layoutFormat = DataCollectionField.TEXT_LAYOUT_FORMAT;
		} else {
			this.layoutFormat = DataCollectionField.PAIR_COMPONENT_LAYOUT_FORMAT;
		}
		
		// Field Label
		
		if (this.fieldLabel == null) {
			if (this.dataType == BOOLEAN_FIELD_TYPE) {
				this.fieldLabel = new DWTextPane();
			} else if (this.dataType == TEXT_FIELD_TYPE) {
				this.fieldLabel = new DWTextPane();
				String text = this.getStandAloneLabel();
				if ((text == null) || (text.isEmpty())) {
					text = this.getLabel();
				}
				this.fieldLabel.setText(text);
			} else {
				this.fieldLabel = new DWLabel();
			}
			this.panel.add((JComponent) this.fieldLabel);
		}
		
		// Suffix Label
		
		if (this.dataType == DataCollectionField.IFSC_FIELD_TYPE) {
			if (this.supplementalPanel == null) {
				this.supplementalPanel = new IfscSupplementalPanel(this, receiveCountry, toolkit);
			}
			this.panel.add(this.supplementalPanel);
		}
		
		// Placeholder text
		
		if (this.dataType == DATE_FIELD_TYPE) {
			String text = ((DefaultDateValidator) this.validator).getTemplate();
			((DWTextField) this.dataComponent).setPlaceholder(text, DWTextField.GRAY_PLACEHOLDER_COLOR);
		} else if ((this.dataComponent instanceof DWTextField) && (this.exampleFormat != null) && (this.exampleFormat.length() > 0)) {
			((DWTextField) this.dataComponent).setPlaceholder(this.exampleFormat, DWTextField.GRAY_PLACEHOLDER_COLOR);
		}
		
		// Help Icon
		
		if (this.helpIconLabel == null) {
			if ((this.extendedHelpText != null) && (! this.extendedHelpText.isEmpty())) {
				this.helpIconLabel = new HelpText();
				this.helpIconLabel.setHelpText(this.extendedHelpText);
				this.panel.add(this.helpIconLabel);
			}
		}
		
		// Key Listener
		
		KeyListener kl = new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					if (e.getComponent() instanceof JTextField) {
						nextFocus(backButton, nextButton);
					} 
				}
			}
		};
		
		if (this.dataComponent != null) {
			if (this.dataComponent instanceof JComboBox) {
				
				JComboBox cb = (JComboBox) this.dataComponent;
				
				Action action = new AbstractAction() {
					private static final long serialVersionUID = 1L;
	
					@Override
					public void actionPerformed(ActionEvent e) {
						nextFocus(backButton, nextButton);
					}
				};
				
				cb.getInputMap().put(KeyStroke.getKeyStroke((char) 10), "EnterKey");
				cb.getActionMap().put("EnterKey", action);
			} else {
				this.dataComponent.addKeyListener(kl);
			}
		}
	}
	
	private void nextFocus(final JButton backButton, final JButton nextButton) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				Component nextComponent = FocusManager.getCurrentManager().getDefaultFocusTraversalPolicy().getComponentAfter(DeltaworksStartup.getInstance().getContentPane(), dataComponent);
				boolean flag = nextComponent == backButton;
				if (flag) {
					nextButton.requestFocus();
				} else {
					dataComponent.transferFocus();
				}
			}
		});
	}
	
	public DataCollectionField validate(int mode) {
		
		// If the panel is not visible, then do not validate the field.
		
		if (! this.panel.isVisible()) {
			return null;
		}
		
		// If it is a required field, check if a value has been specified.
		
		if (this.isRequired() && (this.getValue().isEmpty())) {
			if ((this.dataType != STATE_FIELD_TYPE) || (this.getDataComponent().isEnabled())) {
				if ((this.dataComponent instanceof DWTextField) && (mode == DataCollectionField.DISPLAY_ERRORS_VALIDATION)) {
					Runnable task = new Runnable() {
						@Override
						public void run() {
							((DWTextField) DataCollectionField.this.dataComponent).setPlaceholder(Messages.getString("XmlDefinedPanel.1923"), DWTextField.RED_PLACEHOLDER_COLOR);
							((DWTextField) DataCollectionField.this.dataComponent).repaint();
						}
					};
					SwingUtilities.invokeLater(task);
				}
				return this;
			}
		}
		
		// If the field is a text field, check if minimum and maximum length.

		if ((this.dataComponent instanceof JTextField) && (this.dataType == STRING_FIELD_TYPE)) {
			int tfl = ((JTextField) this.dataComponent).getText().length();
			if (tfl != 0) {
				if (tfl < fieldMin) {
					return this;
				}
				
				if ((fieldMax > 0) && (tfl > fieldMax)) {
					return this;
				}
			}
		}
		
		// If a validation regular expression has been specified.
		
		if ((this.pattern != null) && (this.dataComponent instanceof JTextField)) {
			String text = ((JTextField) this.dataComponent).getText();
			if (this.isRequired() || (! text.isEmpty())) {
				Matcher matcher = this.pattern.matcher(text);
				if (! matcher.matches()) {
					return this;
				}
			}
		}
		
		// If a validation object has been specified.
		
		if (this.validator != null) {
			String text = this.getValue();
			if (this.isRequired() || (! text.isEmpty())) {
				DataCollectionField field = this.validator.isValid(mode);
				if (field != null) {
					return field;
				}
			}
		}
		
		// If a pinpad entry, check if a pin value has been entered if required.
		
		if ((this.dataComponent instanceof PinpadTextField) && (Pinpad.INSTANCE.getIsPinRequired())) {
			String pin = ((PinpadTextField) this.dataComponent).getPin();
			if ((pin == null) || (pin.isEmpty())) {
				return this;
			}
		}
		
		// All validation checked passed, return null to indicate the data is good.
		
		return null;
	}
	
	public synchronized void focus(JScrollPane scrollPane, JPanel scrollPanePanel, boolean unconditionalMove) {
		if ((this.dataCollectionPanel != null) && (this.dataComponent != null)) {
			this.dataComponent.requestFocusInWindow();
			if (this.dataComponent instanceof JTextField) {
				((JTextField) this.dataComponent).selectAll();
			} 
			
			// Window location
			
			final JViewport viewport = scrollPane.getViewport();
			int windowY1 = viewport.getViewPosition().y;
			int windowY2 = windowY1 + viewport.getSize().height; 
			int windowHeight = windowY2 - windowY1;
			
			// Panel location

			Point p1 = new Point(1, 1);
			SwingUtilities.convertPointToScreen(p1, scrollPane.getViewport());
			Point p2 = new Point(1, 1);
			SwingUtilities.convertPointToScreen(p2, this.getDataCollectionPanel().getPanel());
			int panelY1 = p2.y - p1.y + windowY1;
			
			// Item location
		
			p2 = new Point(1, 1);
			SwingUtilities.convertPointToScreen(p2, this.dataComponent);
			int itemY1 = p2.y - p1.y + windowY1;
			int itemY2 = itemY1 +  this.panel.getBounds().height;
			
			// Check if screen window needs to be moved.
			
			boolean isItemVisible = ((windowY1 <= itemY1) && (windowY2 >= itemY2));
			final int y;
			if (unconditionalMove) {
				boolean isPanelTopVisible = (windowY1 <= panelY1);
				if (isPanelTopVisible && isItemVisible) {
					return;
				}
				
				if (itemY2 > (panelY1 + windowHeight)) {
					y = itemY2 - windowHeight;
				} else {
					y = panelY1;
				}
			} else {
				if (isItemVisible) {
					return;
				}
				
				if (windowY1 > itemY1) {
					y = itemY1;
				} else {
					y = itemY2 - windowHeight;
				}
			}
			
			// The window needs to be moved. Schedule that move.

			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					viewport.setViewPosition(new Point(0, y));
				}
			});
		}
	}
	
	@Override
	public int compareTo(DataCollectionComponent dcc) {

		if (dcc != null) {
			return this.getSortString().compareTo(dcc.getSortString());
		} else {
			return -1;
		}
	}

	private void setValidation(final Pattern pattern) {
		this.pattern = pattern;
		if (this.pattern != null) {
			
			if (this.dataComponent instanceof JTextField) {
				final JTextField tf = (JTextField) this.dataComponent;
				
				this.validationKeyListener = new JTextFieldValidationKeyListener(tf);
				
//				this.validationKeyListener = new KeyAdapter() {
//					@Override
//					public void keyPressed(KeyEvent e) {
//						if (e.getKeyCode() == KeyEvent.VK_ENTER) {
//							tf.transferFocus();
//						}
//					}
//					
//					@Override
//					public void keyReleased(KeyEvent e) {
//						Matcher matcher = pattern.matcher(tf.getText());
//						if (matcher.matches()) {
//							tf.setForeground(Color.BLACK);
//						} else {
//							tf.setForeground(Color.RED);
//						}
//					}
//				};
				
				tf.addKeyListener(this.validationKeyListener);
			}
		}
	}
	
	private class JTextFieldValidationKeyListener extends KeyAdapter implements Serializable {
		private static final long serialVersionUID = 1L;
		private JTextField tf;

		private JTextFieldValidationKeyListener(JTextField tf) {
			this.tf = tf;
		}
		
		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				tf.transferFocus();
			}
		}
		
		@Override
		public void keyReleased(KeyEvent e) {
			int tfl = tf.getText().length();
			boolean isValidLength = tfl >= fieldMin && tfl <= fieldMax;
			Matcher matcher = pattern.matcher(tf.getText());
			if (isValidLength && matcher.matches()) {
				tf.setForeground(Color.BLACK);
			} else {
				tf.setForeground(Color.RED);
			}
		}
	}
	
	public void fireValidationKeyListener() {
		if (this.validationKeyListener != null) {
			this.validationKeyListener.keyReleased(null);
		}
	}
	
	public String getValue() {
		String value = "";
		if (this.dataComponent != null) {
			if (this.dataComponent instanceof JTextField) {
				JTextField tf = (JTextField) this.dataComponent; 
				if (this.dataType == DataCollectionField.DATE_FIELD_TYPE) {
					try {
						setCalendarDate(CALENDAR, tf.getText());
						StringBuffer sb = new StringBuffer();
						sb.append(CALENDAR.get(Calendar.YEAR));
						sb.append("-");
						sb.append(NUMBER_FORMAT.format(CALENDAR.get(Calendar.MONTH) + 1));
						sb.append("-");
						sb.append(NUMBER_FORMAT.format(CALENDAR.get(Calendar.DAY_OF_MONTH)));
				        value = sb.toString();
					} catch (Exception e) {
						value = "";
					}
//				} else if ((this.linkField != null) && (this.linkType == DataCollectionField.ID_TYPE_LINK)) {
//					value = ((JTextField) this.dataComponent).getText().trim();
//					String idType = this.linkField.getValue();
//			        if ((idType.equals("SSN")) || (idType.equals("TAX"))) {
//			        	value = value.replaceAll("-", "");
//			        }
				} else {
					value = ((JTextField) this.dataComponent).getText().trim();
				}
			} else if (this.dataComponent instanceof JComboBox) {
				JComboBox cb = (JComboBox) this.dataComponent;
				if ((cb.isEnabled()) && (cb.getSelectedItem() != null) && (cb.getSelectedItem() instanceof DWComboBoxItem)) {
					String v = ((DWComboBoxItem) cb.getSelectedItem()).getValue();
					if (v != null) {
						value = v.trim();
					}
				}
			} else if (this.dataComponent instanceof JCheckBox) {
				value = ((JCheckBox) this.dataComponent).isSelected() ? "true" : "false";
			}
		}
		return value;
	}
	
	private static String formatDate(String value) {
		String text;
        try {
        	text = DATE_TEMPLATE.replace("11", value.substring(5, 7)).replace("22", value.substring(8, 10)).replace("3333", value.substring(0, 4));
        } catch (Exception e) {
        	text = "";
        }
        return text;
	}
	
	public void setValue(String value) {
		if (this.dataType == DataCollectionField.TEXT_FIELD_TYPE) {
			((DWTextPane) this.fieldLabel).setText(value);
		} else if (this.dataComponent != null) {
			if (this.dataComponent instanceof JTextField) {
				JTextField tf = (JTextField) this.dataComponent; 
				if ((value != null) && (this.dataType == DataCollectionField.DATE_FIELD_TYPE)) {
//		        	String dateText;
//			        try {
//			    		dateText = DATE_TEMPLATE.replace("11", value.substring(5, 7)).replace("22", value.substring(8, 10)).replace("3333", value.substring(0, 4));
//			        } catch (Exception e) {
//			        	dateText = "";
//			        }
		        	tf.setText(formatDate(value));
//				} else if ((this.linkField != null) && (this.linkType == DataCollectionField.ID_TYPE_LINK)) {
//					String formattedValue = value;
//					String idType = this.linkField.getValue();
//			        if (((idType.equals("SSN")) || (idType.equals("TAX"))) && (value != null) && (value.length() == 9)) {
//			        	StringBuffer sb = new StringBuffer();
//			        	sb.append(value.substring(0, 3));
//			        	sb.append("-");
//			        	sb.append(value.substring(3, 5));
//			        	sb.append("-");
//			        	sb.append(value.substring(5, 9));
//			        	formattedValue = sb.toString();
//			        }
//					tf.setText(formattedValue);
				} else {
					tf.setText(value);
				}
			} else if (this.dataComponent instanceof JComboBox) {
				JComboBox cb = (JComboBox) this.dataComponent;
				int c = cb.getItemCount();
				
//				String stateValue = this.linkType == DataCollectionField.STATE_LINK ? this.linkField.getValue() : "";
				
				for (int i = 0; i < c; i++) {
					DWComboBoxItem cbi = (DWComboBoxItem) cb.getItemAt(i);
					if (cbi.getValue().equals(value)) {
						DataCollectionField.enablableMofifyTracking(false);
						cb.setSelectedIndex(i);
						DataCollectionField.enablableMofifyTracking(true);
						break;
					}
				}
				
//				if (! stateValue.isEmpty()) {
//					this.linkField.setValue(stateValue);
//				}
			} else if (this.dataComponent instanceof JCheckBox) {
				DataCollectionField.enablableMofifyTracking(false);
				((JCheckBox) this.dataComponent).setSelected((value != null) ? value.toLowerCase(Locale.US).equals("true") : false);
				DataCollectionField.enablableMofifyTracking(true);
			} else if (this.dataComponent instanceof DWTextPane) {
				((DWTextPane) this.dataComponent).setText((value != null) ? value : "");
			}
		}
	}

	public void setEnumerated(boolean enumerated) {
		this.enumerated = enumerated;
	}

	public int getDataComponentWidth() {
		if (this.dataComponent instanceof JTextField) {
			return FIRST_CHARACTER_WIDTH + ((this.maximumFieldLength - 1) * ADDITIONAL_CHARACTER_WIDTH);
		} else if (this.dataComponent instanceof JComboBox) {
			return this.dataComponent.getPreferredSize().width; 
		} else {
			return 0;
		}
	}
	
	private void setDataComponentEnabled(boolean b) {
		if (this.dataComponent != null) {
			this.dataComponent.setEnabled(b);
			if ((this.dataComponent instanceof JTextField) || (this.dataComponent instanceof JComboBox)) {
				if (b) {
					this.dataComponent.setBackground(ENABLE_COLOR);
				} else {
					this.dataComponent.setBackground(DISABLE_COLOR);
				}
			}
		}
	}

	public void setDataFieldEnabled(boolean b) {
		if (this.fieldLabel != null) {
			this.fieldLabel.setEnabled(b);
		}
		setDataComponentEnabled(b);
		this.fieldEnabled = b;
	}

	public boolean isFieldEnabled() {
		return this.fieldEnabled;
	}

	public void setGffpEnabled(boolean gffpEnabled) {
		this.gffpEnabled = gffpEnabled;
	}
	
	public boolean isGffpEnabled() {
		return this.gffpEnabled;
	}

	private void setValidator(DataCollectionFieldValidator validator) {
		this.validator = validator;
	}

	private void addKeyListener(KeyListener listener) {
		if ((this.dataComponent != null) && (this.dataComponent instanceof JTextField)) {
			((JTextField) this.dataComponent).addKeyListener(listener);
		}
	}

	private void setFieldMin(Long fieldMin) {
		this.fieldMin = fieldMin;
	}

	@Override
	public Integer getDisplayOrder() {
		return this.displayOrder;
	}

	public DataCollectionPanel getDataCollectionPanel() {
		return this.dataCollectionPanel;
	}

	public JLabel getFiller() {
		return this.filler;
	}

	private void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}

	private void setRequired(Boolean required) {
		this.required = required;
	}
	
	private void setStandAloneLabel(String standAloneLabel) {
		this.standAloneLabel = standAloneLabel;
	}

	private void setEnumerationList(Vector<EnumeratedValue> enumerationList) {
		this.enumeratedValueInfoList = enumerationList;
	}

	public boolean isFocusable() {
		return (this.isVisible() && (this.getDataType() != DataCollectionField.TEXT_FIELD_TYPE) && (this.getDataType() != DataCollectionField.VALUE_FIELD_TYPE));
	}

	public int getCollectedStatus() {
		return this.collectionStatus;
	}

	@Override
	public void setCollectedStatus(int collectionStatus) {
		this.collectionStatus = collectionStatus;
	}

	public List<BusinessErrorMessage> getBusinessErrors() {
		return this.businessErrors;
	}

	public void setEditable(boolean flag) {
		if (this.dataComponent instanceof JTextField) {
			((JTextField) this.dataComponent).setEditable(flag);
		} else if (this.dataComponent instanceof JComboBox) {
			((JComboBox) this.dataComponent).setEnabled(flag);
		}
	}
	
	public void addBusinessError(BusinessError errorTag) {
        if (this.panel == null) {
        	this.panel = new JPanel(new GridBagLayout());
        }
        
		BusinessErrorMessage be = new BusinessErrorMessage();
		if (this.businessErrors == null) {
			this.businessErrors = new ArrayList<BusinessErrorMessage>();
		}
		this.businessErrors.add(be);
		
		// Update data collection field with components to display the business error. 
		
		// Value Label
		
		if (this.valueTypeLabel == null) {
			this.valueTypeLabel = new JLabel(Messages.getString("DataCollectionItem.OldValue"));
			this.valueTypeLabel.setPreferredSize(DataCollectionField.getValueLabelSize());
		}
		this.panel.add(this.valueTypeLabel);
		
		if ((this.getChildFieldList() != null) && (! this.getChildFieldList().isEmpty())) {
			this.relatedFieldsPanel = new JPanel();
			this.panel.add(this.relatedFieldsPanel);
			this.relatedFieldsPanel.setLayout(new GridBagLayout());
			this.relatedFields = this.getAllChildFields();
			Collections.sort(this.relatedFields);
			
			GridBagConstraints gbc = new GridBagConstraints();
			
			int y = 0;
			for (DataCollectionField relatedField : this.relatedFields) {
				gbc.gridx = 0;
				gbc.gridy = y;
				gbc.insets = DataCollectionScreenToolkit.INSET_BOTTOM;
		        if (relatedField.panel == null) {
		        	relatedField.panel = new JPanel(new GridBagLayout());
		        }
				this.relatedFieldsPanel.add(relatedField.panel, gbc);
				
				y++;
			}
		}
		
		// Error Message
		
		String errorMessage = MessageFacadeError.formatErrorMessageText(errorTag.getMessageShort(), errorTag.getMessage(), errorTag.getDetailString(), errorTag.getErrorCode(), errorTag.getSubErrorCode(), null);
		be.errorMessageLabel = new DWTextPane();
		
		if (this.errorMessagePanel == null) {
			this.errorMessagePanel = new JPanel();
			this.errorMessagePanel.setLayout(new GridLayout(0, 1, 0, 5));
			this.panel.add(this.errorMessagePanel);
		}
		
		this.errorMessagePanel.add(be.errorMessageLabel);
		be.errorMessageLabel.setText(errorMessage);
		be.errorMessageLabel.setForeground(Color.RED);
		
		final String errorCategory = errorTag.getOffendingField() != null  ? errorTag.getErrorCategory() : "";
		if (errorTag.getOffendingField() != null) {
			if (errorCategory.equals("CHECK_VERIFY")) {
				be.businessErrorCategory = DataCollectionField.BUSINESS_ERROR_CHECK_VERIFY;
				
				// Validation Check Box
				
				if (this.validationCheckBox == null) {
					this.validationCheckBox = new JCheckBox();
					this.validationCheckBox.addChangeListener(new ChangeListener() {
						@Override
						public void stateChanged(ChangeEvent e) {
							if (validationCheckBox.isSelected()) {
								setEditable(false);
								dataComponent.setFocusable(false);
								setValue(oldValue);
								valueTypeLabel.setText(Messages.getString("DataCollectionItem.OldValue"));
								if (errorCategory.equals("CHECK_VERIFY")) {
									validationFlag = true;
								}
							} else {
								setEditable(true);
								dataComponent.setFocusable(true);
								valueTypeLabel.setText(Messages.getString("DataCollectionItem.OldValue"));
								if (errorCategory.equals("CHECK_VERIFY")) {
									validationFlag = false;
								}
							}
						}
					});
				}
				this.panel.add(this.validationCheckBox);
				this.validationCheckBox.setSelected(false);
				
				// Validation Message Label
				
				if (this.validationMessageLabel == null) {
					this.validationMessageLabel = new JLabel(Messages.getString("DataCollectionItem.ValidationMessage"));
				}
				this.panel.add(this.validationMessageLabel);
	
			} else {
				be.businessErrorCategory = DataCollectionField.BUSINESS_ERROR_VALIDATION;
			}
		}
		
		if (this.dataComponent instanceof JTextField) {
			if (validationChangeListener == null) {
				KeyListener keyListener = new KeyAdapter() {
					@Override
					public void keyReleased(KeyEvent arg0) {
						String newValue = ((JTextField) DataCollectionField.this.dataComponent).getText();
						if (newValue.equalsIgnoreCase(DataCollectionField.this.oldValue)) {
							DataCollectionField.this.valueTypeLabel.setText(Messages.getString("DataCollectionItem.OldValue"));
						} else {
							DataCollectionField.this.valueTypeLabel.setText(Messages.getString("DataCollectionItem.NewValue"));
						}
					}
				};
				validationChangeListener = keyListener;
				((JTextField) this.dataComponent).addKeyListener(keyListener);
			}
		} else if (this.dataComponent instanceof JCheckBox) {
			if (validationChangeListener == null) {
				ActionListener actionListener = new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						String newValue = String.valueOf(((JCheckBox) DataCollectionField.this.dataComponent).isSelected());
						if (newValue.equalsIgnoreCase(DataCollectionField.this.oldValue)) {
							DataCollectionField.this.valueTypeLabel.setText(Messages.getString("DataCollectionItem.OldValue"));
						} else {
							DataCollectionField.this.valueTypeLabel.setText(Messages.getString("DataCollectionItem.NewValue"));
						}
					}
				};
				validationChangeListener = actionListener;
				((JCheckBox) this.dataComponent).addActionListener(actionListener);
			}
		} else if (this.dataComponent instanceof JComboBox) {
			if (validationChangeListener == null) {
				ItemListener itemListener = new ItemListener() {
					@Override
					public void itemStateChanged(ItemEvent e) {
						String newValue = String.valueOf(((JComboBox) DataCollectionField.this.dataComponent).getSelectedItem());
						if (newValue.equalsIgnoreCase(DataCollectionField.this.oldValue)) {
							DataCollectionField.this.valueTypeLabel.setText(Messages.getString("DataCollectionItem.OldValue"));
						} else {
							DataCollectionField.this.valueTypeLabel.setText(Messages.getString("DataCollectionItem.NewValue"));
						}
					}
				};
				validationChangeListener = itemListener;
				((JComboBox) this.dataComponent).addItemListener(itemListener);
			}
		}
		
		this.setCurrentLayoutType(LAYOUT_UNDEFINED);
	}
	
	private List<DataCollectionField> getAllChildFields() {
		List<DataCollectionField> list = new ArrayList<DataCollectionField>();
		getChildFields(this, list);
		return list;
	}
	
	private void getChildFields(DataCollectionField field, List<DataCollectionField> list) {
		if (field.getChildFieldList() != null) {
			for (DataCollectionComponent component : field.getChildFieldList()) {
				if (component instanceof DataCollectionField) {
					DataCollectionField child = (DataCollectionField) component;
					if (! list.contains(child)) {
						list.add(child);
						if (child.getChildFieldList() != null) {
							getChildFields(child, list);
						}
					}
				} else if (component instanceof DataCollectionPanel) {
					DataCollectionPanel panel = (DataCollectionPanel) component;
					getPanelFields(panel, list);
				}
			}
		}
	}
	
	private void getPanelFields(DataCollectionPanel panel, List<DataCollectionField> list) {
		for (DataCollectionField field : panel.getChildDataCollectionFields().values()) {
			if (! list.contains(field)) {
				list.add(field);
				if (field.getChildFieldList() != null) {
					getChildFields(field, list);
				}
			}
		}
		for (DataCollectionPanel childPanel : panel.getChildDataCollectionPanels().values()) {
			getPanelFields(childPanel, list);
		}
	}
	
	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}

	public String getStandAloneLabel() {
		return this.standAloneLabel;
	}

	public int getCategoryLevel() {
		return this.getDataCollectionPanel().getCategoryLevel() - this.getDataCollectionPanel().getBaseLevel() + 1;
	}
	
	private static Dimension getValueLabelSize() {
		JLabel l1 = new JLabel(Messages.getString("DataCollectionItem.OldValue"));
		JLabel l2 = new JLabel(Messages.getString("DataCollectionItem.NewValue"));
		JLabel l3 = new JLabel(Messages.getString("DataCollectionField.Address"));
		int w = Math.max(Math.max(l1.getPreferredSize().width, l2.getPreferredSize().width), l3.getPreferredSize().width);
		return new Dimension(w, l1.getPreferredSize().height);
	}

	public boolean isValidated() {
		if (NON_CHECKBOX_VERIFY_CHECK) {
			return this.getValue().equals(oldValue);
		} else {
			return this.validationFlag;
		}
	}
	
	public static DataCollectionField getDataCollectionField(FieldToCollectInfo tag, List<EnumeratedTypeInfo> enumerations, DataCollectionSet dataSet, DataCollectionPanel dcp, int collectionStatus, DataCollectionField parentField, String parentValue, int categoryLevel) {

		// Get the xml tag.

		String s = tag.getInfoKey();
		String infoKey = (s != null) ? s.trim() : null;

		ExtraDebug.println("Processing FieldToCollectInfo - " + infoKey);

		// Check if the field has already been defined.
		
		FieldKey key1 = FieldKey.key(infoKey, parentValue);
		DataCollectionField field = dataSet.getFieldMap().get(key1);
		
		if (field == null) {
			
			// If a parent value is present (the field is a child), check if the
			// field was defined before as a regular field.
			
			if (parentValue != null) {
				FieldKey key2 = FieldKey.key(infoKey);
				field = dataSet.getFieldMap().get(key2);
				if (field != null) {
					field.setKey(key1);
					dataSet.getFieldMap().remove(key2);
					dataSet.getFieldMap().put(key1, field);
					parentField.getChildFieldList().add(field);
				}
				
			// If no parent value is present, check if the field was specified
			// as a regular field but was defined as a child field in the past.
				
			} else {
				field = dataSet.findFieldByInfoKey(infoKey); 
				if (field != null) {
					parentField = field.getParentField();
					key1 = FieldKey.key(infoKey, field.getParentValue());
				}
			}
		}
		
		// If the field has already been defined, remove it and any child fields.
		
		if (field != null) {
			dataSet.getFieldMap().remove(field.getKey());
			if (field.getDataCollectionPanel() != null) {
				field.getDataCollectionPanel().getChildDataCollectionFields().remove(field.getKey());
			}
			for (DataCollectionField child : field.getAllChildFields()) {
				dataSet.getFieldMap().remove(child.getKey());
				if (child.getDataCollectionPanel() != null) {
					child.getDataCollectionPanel().getChildDataCollectionFields().remove(child.getKey());
				}
			}
			if (field.getChildFieldList() != null) {
				field.getChildFieldList().clear();
			}
		}
		
		// Create a new DataCollectionItem instance for the field.
		
		field = new DataCollectionField(key1, dataSet, dcp);
		
		if (parentField != null) {
			parentField.childFieldList.add(field);
		}
		field.parentField = parentField;
		field.dataCollectionPanel = dcp;

		// label

		s = tag.getLabel();
		if ((s != null) && (s.length() > 0)) {
			field.setLabel(s);
		}

		// standAloneLabel

		s = tag.getLabelStandAlone();
		if ((s != null) && (s.length() > 0)) {
			field.setStandAloneLabel(s);
		}

		// displayOrder
		
		field.setDisplayOrder(tag.getDisplayOrder());
		if (field.getDisplayOrder() == null) {
			field.setDisplayOrder(Integer.valueOf(Integer.MAX_VALUE));
		}
		field.sortString = "F" + SORT_ORDER_FORMAT.format(field.getDisplayOrder());
		DataCollectionField parent = field.getParentField();
		while (parent != null) {
			field.sortString = "F" + SORT_ORDER_FORMAT.format(parent.getDisplayOrder()) + "." + field.sortString;
			parent = parent.getParentField();
		}
		DataCollectionPanel panel = field.getDataCollectionPanel();
		while (panel != null) {
			field.sortString = "P" + SORT_ORDER_FORMAT.format(panel.getDisplayOrder()) + "." + field.sortString;
			panel = panel.getParentDcp();
		}
		
		// shortHelpText
		
		s = tag.getHelpTextShort();
		if ((s != null) && (s.length() > 0)) {
			field.shortHelpText = s;
		}
		
		// extendedHelpText

		s = tag.getHelpTextLong();
		if ((s != null) && (s.length() > 0)) {
			field.setExtendedHelpText(s);
		}
		
		// hidden
		
		processFieldToCollect(tag, enumerations, field, dcp, collectionStatus, categoryLevel);
		processFieldInfo(tag, field, collectionStatus);

		// Check if the item was defined or if it was only a reference to child fields.
		// If the label and data type are defined, then update the collection status.

		return field;
	}
	
	@Override
	public Set<DataCollectionComponent> getChildFieldList() {
		return this.childFieldList;
	}

	public String getParentValue() {
		return this.key.getParentValue();
	}

	public boolean isParentFieldVisible() {
		if (this.parentField == null) {
			return true;
		} else if (this.key.getParentValue() != null) {
			if (this.parentField.getParentValueOfChildren().equals(this.key.getParentValue())) {
				return this.parentField.isParentFieldVisible();
			} else {
				return false;
			}
		} else {
			return this.parentField.isParentFieldVisible();
		}
	}
	
	public String getParentValueOfChildren() {
		String value = this.getValue();
		if (value.isEmpty()) {
			value = this.getDefaultValue() != null ? this.getDefaultValue() : "";
		}
		if (this.getParentField() != null) {
			value = this.getParentField().getParentValueOfChildren() + "." + value;
		}
		return value;
	}
	
	@Override
	public boolean isChildFieldVisible() {
		boolean flag = true;
		if (this.getParentField() != null) {
			String parentValueOfChildren = this.getParentField().getParentValueOfChildren();
			flag = this.isParentFieldVisible() && (this.getParentField() != null) && (parentValueOfChildren != null) && (this.getParentField().getParentValueOfChildren()).equals(this.getParentValue());
		}
		return flag;
	}

	private boolean isParentFieldRequired() {
		if (this.parentField == null) {
			return true;
		} else  if (parentField.getVisibility() != DataCollectionField.REQUIRED) {
			return false;
		} else {
			return this.parentField.isParentFieldRequired();
		}
	}
	
	public boolean isFieldRequired() {
		boolean b2 = this.getVisibility() == DataCollectionField.REQUIRED;
		if (this.isChildField()) {
			boolean b1 = this.isParentFieldRequired();
			return b1 && b2;
		} else {
			return b2;
		}
	}
	
	public boolean isFieldOptional() {
		boolean b2 = this.getVisibility() == DataCollectionField.OPTIONAL;
		if (this.isChildField()) {
			boolean b1 = this.isParentFieldRequired();
			return b1 && b2;
		} else {
			return b2;
		}
	}
	
	public boolean isChildField() {
		return (this.parentField != null);
	}

	@Override
	public DataCollectionField getParentField() {
		return this.parentField;
	}
	
	private static void processFieldToCollect(InfoBase tag, List<EnumeratedTypeInfo> enumerations, DataCollectionField field, DataCollectionPanel parentDcp, int collectionStatus, int categoryLevel) {
		FieldToCollectInfo fieldToCollectInfo = (FieldToCollectInfo) tag;
		
		field.setDefaultValue(fieldToCollectInfo.getDefaultValue());
		
		// readOnly

		Boolean readOnly = fieldToCollectInfo.isReadOnly();
		field.setReadOnly(readOnly != null ? readOnly : false);

		// required

		Boolean required = fieldToCollectInfo.isRequired();
		if ((field.required != null) && (field.required == false) && (required != null) && (required == true)) {
			field.setCollectedStatus(DataCollectionField.UPGRADED_REQUIREMENTS);
		}
		field.setRequired(required != null ? required : false);

		// fieldMax

		Long fieldMax = fieldToCollectInfo.getFieldMax();
		if (fieldMax != null) {
			field.setFieldMax(fieldMax);
		}

		// fieldMin
		
		Long fieldMin = fieldToCollectInfo.getFieldMin();
		if (fieldMin != null) {
			field.setFieldMin(fieldMin);
		}

		// validationRegEx

		String validationRegEx = fieldToCollectInfo.getValidationRegEx();
		if (validationRegEx != null) {
			field.setValidationRegEx(validationRegEx);
		}
		
		// Lookup Key
		
		field.lookupKey = fieldToCollectInfo.getLookupKey();

		// exampleFormat
		
		String exampleFormat = fieldToCollectInfo.getExampleFormat();
		if (exampleFormat != null) {
			field.setExampleFormat(exampleFormat);
		}
		
		// enumerationTypeName
		
		String etn  = fieldToCollectInfo.getEnumeration() != null ? fieldToCollectInfo.getEnumeration().getName() : null;
		if ((etn != null) && (enumerations != null)) {
			for (EnumeratedTypeInfo eti : enumerations) {
				if (eti.getName().equals(etn)) {
					Vector<EnumeratedValue> enumeratedValueInfoList = new Vector<EnumeratedValue>();

					for (EnumeratedIdentifierInfo eii : eti.getEnumeratedItems().getEnumeratedItem()) {
						String identifier = eii.getIdentifier();
						String labelShort = eii.getLabelShort();
						String labelLong = eii.getLabel();
						EnumeratedValue ev = new EnumeratedValue(identifier, labelShort, labelLong);
						enumeratedValueInfoList.add(ev);
					}
					field.setEnumerationList(enumeratedValueInfoList);
					break;
				}
			}
		}

		// childFields

		List<ChildField> childFieldTags = fieldToCollectInfo.getChildFields() != null ? fieldToCollectInfo.getChildFields().getChildField() : null;
		if ((childFieldTags != null) && (childFieldTags.size() > 0)) {
			if (field.childFieldList == null) {
				field.childFieldList = new HashSet<DataCollectionComponent>();
			}
			for (ChildField childFieldTag : childFieldTags) {
				String pv = (field.getParentValue() != null ? field.getParentValue() + "." : "") + childFieldTag.getFieldValue();
				List<InfoBase> infoTags = childFieldTag.getInfos().getInfo();
				for (InfoBase childInfoTag : infoTags) {
					if (childInfoTag instanceof CategoryInfo) {
						DataCollectionPanel childDcp = DataCollectionPanel.getDataCollectionPanel((CategoryInfo) childInfoTag, enumerations, field.dataSet, field.parentPanel, categoryLevel + 1, collectionStatus, field, pv);
						childDcp.setParentField(field);
						parentDcp.getChildDataCollectionPanels().put(childDcp.getInfoKey(), childDcp);
						field.dataSet.getDataCollectionPanelMap().put(childDcp.getInfoKey(), childDcp);
						
					} else if (childInfoTag instanceof FieldToCollectInfo) {
						DataCollectionField childItem = DataCollectionField.getDataCollectionField((FieldToCollectInfo) childInfoTag, enumerations, field.dataSet, field.parentPanel, collectionStatus, field, pv, categoryLevel);
						childItem.parentField = field;
						childItem.collectionStatus = collectionStatus;
					}
				}
			}
		}
	}

	private static void processFieldInfo(InfoBase tag, DataCollectionField field, int collectionStatus) {
		FieldInfo fieldInfo = (FieldInfo) tag;
		FieldToCollectInfo fieldToCollectInfo = (FieldToCollectInfo) tag;
		boolean b = fieldInfo.isDisplay() != null ? fieldInfo.isDisplay().booleanValue() : true;
		field.setDisplay(b);
		
		// visibility 

		int visibility = 0;
		if (field.display) {
			if (field.readOnly) {
				visibility = DataCollectionField.READ_ONLY;
			} else if (field.required) {
				visibility = DataCollectionField.REQUIRED;
			} else {
				visibility = DataCollectionField.OPTIONAL;
			}
		} else {
			visibility = DataCollectionField.HIDDEN;
		}
		field.setVisibility(visibility);

		// dataType

		DataType dataTypeCode = fieldInfo.getDataType();
		String lookupSource = fieldToCollectInfo.getLookupDataSource();
		
		int dataType = 0;
		switch (dataTypeCode) {
			case BOOLEAN:
				dataType = BOOLEAN_FIELD_TYPE;
				break;
		
			case INT:
			case LONG:
				dataType = INT_FIELD_TYPE;
				break;
				
			case DECIMAL:
				dataType = DECIMAL_FIELD_TYPE;
				break;
				
			case DATE:
				dataType = DATE_FIELD_TYPE;
				break;
				
			case DATETIME:
				dataType = DATETIME_FIELD_TYPE;
				break;
				
			case TIME:
				dataType = TIME_FIELD_TYPE;
				break;
				
			case ENUM:
				dataType = ENUM_FIELD_TYPE;
				break;
				
			case G_YEAR:
				dataType = GYEAR_FIELD_TYPE;
				break;

			case G_MONTH:
				dataType = GMONTH_FIELD_TYPE;
				break;
				
			case G_DAY:
				dataType = GDAY_FIELD_TYPE;
				break;
				
			case STRING:
				if (lookupSource != null) {
					//
					//  IF/When we use JRE 8 (or seven) this can change to a switch statement
					//
					if (lookupSource.equals(LOOKUP_DATA_SOURCE_COUNTRY)) {
						dataType = COUNTRY_FIELD_TYPE;
						ExtraDebug.println(fieldInfo.getInfoKey() + ": Change String Type to Country");
					} else if (lookupSource.equalsIgnoreCase(LOOKUP_DATA_SOURCE_COUNTRY_SUBDIVISION) || lookupSource.equalsIgnoreCase(LOOKUP_DATA_SOURCE_STATE)) {
						dataType = STATE_FIELD_TYPE;
						ExtraDebug.println(fieldInfo.getInfoKey() + ": Change String Type to State");
					} else if (lookupSource.equalsIgnoreCase(LOOKUP_DATA_SOURCE_IFSC)) {
						dataType = IFSC_FIELD_TYPE;
						ExtraDebug.println(fieldInfo.getInfoKey() + ": Change String Type to IFSC");
					} else if (lookupSource.equalsIgnoreCase(LOOKUP_DATA_SOURCE_COUNTRY_PHONE_CODE)) {
						dataType = DIAL_CODE_FIELD_TYPE;
						ExtraDebug.println(fieldInfo.getInfoKey() + ": Change String Type to Country Dial Code");

					} else {
						dataType = STRING_FIELD_TYPE;
						Debug.println(fieldInfo.getInfoKey() + ": Unknown lookupSource" + "[" + lookupSource + "]");
					}
				} else {
					dataType = STRING_FIELD_TYPE;
				}
				break;
				
			default:
			break;
		}
		field.setDataType(dataType);

		if ((fieldInfo.getEnumeration() != null) && (fieldInfo.getEnumeration().getEnumeratedItems() != null)) {
			// enumeration
			Vector<EnumeratedValue> enumeratedValueInfoList = new Vector<EnumeratedValue>();
			List<EnumeratedIdentifierInfo> identifiersTagList = fieldInfo.getEnumeration().getEnumeratedItems().getEnumeratedItem();
			
			boolean defaultValueSpecified = (field.defaultValue != null) && (! field.defaultValue.isEmpty());
			boolean mustChoice = ((! defaultValueSpecified) && (identifiersTagList.size() > 1)) || field.isFieldOptional();
			if (mustChoice) {
				String l = field.isRequired() ? Messages.getString("DWValues.Select") : Messages.getString("DWValues.None");
				EnumeratedValue ev = new EnumeratedValue("", l, l);
				enumeratedValueInfoList.add(ev);
			}
			for (EnumeratedIdentifierInfo identifiersTag : identifiersTagList) {
				String identifier = identifiersTag.getIdentifier();
				String labelShort = identifiersTag.getLabelShort();
				String labelLong = identifiersTag.getLabel();
				EnumeratedValue ev = new EnumeratedValue(identifier, labelShort, labelLong);
				enumeratedValueInfoList.add(ev);
			}
			field.setEnumerationList(enumeratedValueInfoList);
		} else if (dataType == ENUM_FIELD_TYPE) {
			Debug.println("Field " + field.getInfoKey() + " is defined as type 'enum' by contains no enumerated values");
		}

		String label = tag.getLabel();
		if ((label != null) && (label.length() > 0) && (dataTypeCode != null)) {
			field.collectionStatus = collectionStatus;
		}
	}

	public void setKey(FieldKey key) {
		this.key = key;
	}

	public void setParentFieldSpecified(boolean b) {
		this.parentFieldSpecified = b;
	}

	public boolean getParentFieldSpecified() {
		return this.parentFieldSpecified;
	}

	public void setDataComponent(JComponent component) {
		this.dataComponent = component;
	}
	
	public int getMaximumBusinessErrorCategory() {
		int category = 0;
		if (this.businessErrors != null) {
			for (BusinessErrorMessage be : this.businessErrors) {
				category = Math.max(category, be.businessErrorCategory);
			}
		}
		return category;
	}
	
	public JPanel getErrorMessagePanel() {
		return this.errorMessagePanel;
	}

	public void setErrorMessagePanel(JPanel panel) {
		this.errorMessagePanel = panel;
	}
	
	public IfscSupplementalPanel getIfscSupplementalPanel() {
		return this.supplementalPanel;
	}
	
	public JLabel getValueTypeLabel() {
		return this.valueTypeLabel;
	}
	
	@Override
	public String getSortString() {
		return this.sortString;
	}
	
	public void setSortString(String sortString) {
		this.sortString = sortString;
	}

	public int getCurrentLayoutType() {
		return currentLayoutType;
	}

	public DataCollectionField getParentCountry() {
		return parentCountry;
	}

	public void setParentCountry(DataCollectionField parentCountry) {
		this.parentCountry = parentCountry;
	}

	public void setLookupKey(String lookupKey) {
		this.lookupKey = lookupKey;
	}
	
	public String getLookupKey() {
		return this.lookupKey;
	}

	public boolean isValidatable() {
		return validatable;
	}

	public void setValidatable(boolean validatable) {
		this.validatable = validatable;
	}
	
	public boolean isModified() {
		return this.modified;
	}

	public List<DataCollectionField> getRelatedFields() {
		return relatedFields;
	}
	
	public static void enablableMofifyTracking(boolean flag) {
		enablableMofifyTracking = flag;
	}

	public void setModified(boolean modified) {
		this.modified = modified;
	}
}
