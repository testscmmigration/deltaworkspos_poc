//////////////////////////////////////////////////
//
//    PrintingProgressInterface.java
//
//    Copyright 2000, MoneyGram International
//    
//    20 April, 2000 - Created, ESS
//
//////////////////////////////////////////////////


package dw.framework;

import java.math.BigDecimal;

/** The callback interface for a progress bar which entertains the user while the
    dispenser is printing.
*/
public interface PrintingProgressInterface {

    /** Begins a printing progress bar.
     *  This should not actually display the progress bar until the first
     *  begin() call is made.
     *  @param totalNumberToPrint The total number of items to print.
     */
    public void begin( int totalNumberToPrint);
    
    /** Updates the printing progress bar. Sets visible true on the progress dialog
     *  if not already visible.
     *  @param itemOrdinal The number of the current item (out of the total number to print).
     *  @param serialNumber The serial number of the currently printing item.
     *  @param totalSoFar The amount printed through the current item, including fees.
     *  @return Whether or not it is OK to proceed with the item.  False if the user has cancelled the transaction.
     */
    public boolean item( int itemOrdinal, String serialNumber, BigDecimal totalSoFar);
    
    /** Eliminates the printing progress bar.
    */
    public void done();
}
