package dw.framework;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JPanel;

import com.moneygram.agentconnect.CategoryInfo;
import com.moneygram.agentconnect.EnumeratedTypeInfo;
import com.moneygram.agentconnect.FieldToCollectInfo;
import com.moneygram.agentconnect.InfoBase;

import dw.framework.FieldKey;
import dw.framework.DataCollectionScreenToolkit.DataCollectionComponent;
import dw.utility.ExtraDebug;

public class DataCollectionPanel implements Serializable, Comparable<DataCollectionComponent>, DataCollectionComponent {
	private static final long serialVersionUID = 1L;
	
	private static int counter;
	
	private String infoKey;
	private FieldKey key;
	private String standAloneLabel;
	private Integer displayOrder;
	
	private JPanel panel;
	private Map<String, DataCollectionPanel> childDataCollectionPanels;
	private Map<FieldKey, DataCollectionField> childDataCollectionFields;
	private boolean layoutFlag;
	private DataCollectionPanel parentDcp;
	private List<DataCollectionPanel> parentDcpList;
	private int categoryLevel;
	private int baseLevel;
	private DataCollectionField parentField;
	private String parentValue;
	private String sortString;
	private PanelType panelType;
	
	public enum PanelType {
		DATA_COLLECTION,
		VALICATION
	}
	
	public DataCollectionPanel(DataCollectionPanel parentDcp, PanelType panelType) {
		this.childDataCollectionPanels = new HashMap<String, DataCollectionPanel>();
		this.childDataCollectionFields = new LinkedHashMap<FieldKey, DataCollectionField>();
		this.parentDcp = parentDcp;
		this.layoutFlag = true;
		this.panelType = panelType;
		
		parentDcpList = new ArrayList<DataCollectionPanel>();
		parentDcpList.add(this);
		if (parentDcp != null) {
			parentDcpList.addAll(parentDcp.parentDcpList);
		}
		this.sortString = "";
	}
	
	public static DataCollectionPanel getDataCollectionPanel(CategoryInfo tag, List<EnumeratedTypeInfo> enumerations, DataCollectionSet dataSet, DataCollectionPanel parentPanel, int categoryLevel, int collectionStatus, DataCollectionField parentField, String parentValue) {

		// Get the xml tag for the panel

		String s = tag.getInfoKey();
		String infoKey = (s != null) ? s.trim() : null;

		// Check if the panel has already been defined. If not, create a new instance.

		ExtraDebug.println("Processing CategoryInfo - " + infoKey);

		DataCollectionPanel dcp = dataSet.getDataCollectionPanelMap().get(infoKey);
		if (dcp == null) {
			dcp = new DataCollectionPanel(parentPanel, PanelType.DATA_COLLECTION);
			dcp.setInfoKey(infoKey);
			dcp.setCategoryLevel(categoryLevel);
		}
		
		if (parentField != null) {
			parentField.getChildFieldList().add(dcp);
		}
		dcp.parentField = parentField;
		dcp.parentValue = parentValue;
		
		dcp.key = FieldKey.key(infoKey, parentValue);
	
		// standAloneLabel tag
		
		s = tag.getLabelStandAlone();
		if ((s != null) && (s.length() > 0)) {
			dcp.setStandAloneLabel(s);
		}
		
		// displayOrder tag
		
		Integer displayOrder = tag.getDisplayOrder();
		if (displayOrder != null) {
			dcp.setDisplayOrder(tag.getDisplayOrder());
		} else  if (dcp.getDisplayOrder() == null) {
			counter++;
			displayOrder = counter;
			dcp.setDisplayOrder(displayOrder);
		}
		dcp.sortString = "P" + SORT_ORDER_FORMAT.format(dcp.getDisplayOrder());
		DataCollectionPanel panel = dcp.getParentDcp();
		while (panel != null) {
			dcp.sortString = "P" + SORT_ORDER_FORMAT.format(panel.getDisplayOrder()) + "." + dcp.sortString;
			panel = panel.getParentDcp();
		}
		
		// infos tag

		CategoryInfo cInfo = tag;
		CategoryInfo.Infos info = cInfo.getInfos().getValue();
		List<InfoBase> infos = info.getInfo();
		if (infos != null) {
			for (InfoBase infoBaseTag : infos) {
				if (infoBaseTag instanceof CategoryInfo) {
					DataCollectionPanel childDcp = DataCollectionPanel.getDataCollectionPanel((CategoryInfo) infoBaseTag, enumerations, dataSet, dcp, categoryLevel + 1, collectionStatus, parentField, parentValue);
					dcp.childDataCollectionPanels.put(childDcp.getInfoKey(), childDcp);
					dataSet.getDataCollectionPanelMap().put(childDcp.getInfoKey(), childDcp);
				} else if (infoBaseTag instanceof FieldToCollectInfo) {
					DataCollectionField field = DataCollectionField.getDataCollectionField((FieldToCollectInfo) infoBaseTag, enumerations, dataSet, dcp, collectionStatus, parentField, parentValue, categoryLevel);
					dataSet.getFieldMap().put(field.getKey(), field);
				} else {
					ExtraDebug.println("Invalid tag class - " + tag.getClass().toString());
				}
			}
		}

		if (parentPanel == null) {
			dataSet.getDataCollectionPanelMap().put(dcp.infoKey, dcp);
		} else {
			parentPanel.childDataCollectionPanels.put(dcp.infoKey, dcp);
		}
		return dcp;
	}

	public boolean isVisible() {
		for (DataCollectionPanel childDcp : this.getChildDataCollectionPanels().values()) {
			boolean flag = childDcp.isVisible();
			if (flag) {
				return true;
			}
		}
		for (DataCollectionField field : this.getChildDataCollectionFields().values()) {
			if ((field.getVisibility() == DataCollectionField.REQUIRED) || (field.getVisibility() == DataCollectionField.OPTIONAL) || (field.getVisibility() == DataCollectionField.READ_ONLY)) {
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public JPanel getPanel() {
		return this.panel;
	}

	public void setPanel(JPanel panel) {
		this.panel = panel;
	}

	@Override
	public String getInfoKey() {
		return this.infoKey;
	}

	public void setInfoKey(String infoKey) {
		this.infoKey = infoKey;
	}

	public String getStandAloneLabel() {
		return this.standAloneLabel;
	}

	public void setStandAloneLabel(String standAloneLabel) {
		this.standAloneLabel = standAloneLabel;
	}

	@Override
	public Integer getDisplayOrder() {
		return this.displayOrder;
	}

	private void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public Map<String, DataCollectionPanel> getChildDataCollectionPanels() {
		return this.childDataCollectionPanels;
	}

	public Map<FieldKey, DataCollectionField> getChildDataCollectionFields() {
		return this.childDataCollectionFields;
	}

	@Override
	public int compareTo(DataCollectionComponent dcc) {
		if (dcc != null) {
			return this.getSortString().compareTo(dcc.getSortString());
		} else {
			return -1;
		}
	}
	
	public void markFieldsCollected(int collectionStatus) {
		for (DataCollectionPanel dcp : this.getChildDataCollectionPanels().values()) {
			dcp.markFieldsCollected(collectionStatus);
		}
		for (DataCollectionField field : this.getChildDataCollectionFields().values()) {
			field.setCollectedStatus(collectionStatus);
			if (field.getChildFieldList() != null) {
				for (DataCollectionComponent child : field.getChildFieldList()) {
					child.setCollectedStatus(collectionStatus);
				}
			}
		}
	}

	public void getSupplementalDataCollectionPanels(List<DataCollectionPanel> supplementalDataCollectionPanelList, List<String> excludedCategories) {
		for (DataCollectionPanel dcp : this.getChildDataCollectionPanels().values()) {
			if (excludedCategories != null) {
				if (excludedCategories.contains(dcp.getInfoKey())) {
					continue;
				}
			}
			if (dcp.getChildDataCollectionPanels().isEmpty()) {
				supplementalDataCollectionPanelList.add(dcp);
			} else {
				dcp.getSupplementalDataCollectionPanels(supplementalDataCollectionPanelList, excludedCategories);
			}
		}
	}
	
	public void getAdditionalDataCollectionFields(List<DataCollectionField> uncollectedDataCollectionFieldList) {
		for (DataCollectionField field : this.getChildDataCollectionFields().values()) {
			if (((field.getCollectedStatus() == DataCollectionField.ERROR_SCREEN_COLLECTION) || (field.getCollectedStatus() == DataCollectionField.UPGRADED_REQUIREMENTS)) && (field.getVisibility() == DataCollectionField.REQUIRED) && field.isChildFieldVisible()) {
				uncollectedDataCollectionFieldList.add(field);
				
				if ((field.getDataType() == DataCollectionField.STATE_FIELD_TYPE) && (field.getParentCountry() != null) && (! uncollectedDataCollectionFieldList.contains(field.getParentCountry()))) {
					uncollectedDataCollectionFieldList.add(field.getParentCountry());
				}
				
				if (field.getChildFieldList() != null) {
					addChildComponents(field.getChildFieldList(), uncollectedDataCollectionFieldList);
				}
			}
		}
	}
	
	private void addChildComponents(Set<DataCollectionComponent> components, List<DataCollectionField> uncollectedDataCollectionFieldList) {
		for (DataCollectionComponent component : components) {
			if (component instanceof DataCollectionField) {
				DataCollectionField field = (DataCollectionField) component;
				uncollectedDataCollectionFieldList.add(field);
			}
			if (component.getChildFieldList() != null) {
				addChildComponents(component.getChildFieldList(), uncollectedDataCollectionFieldList);
			}
		}
	}

	public boolean isLayoutFlag() {
		return this.layoutFlag;
	}

	public void setLayoutFlag(boolean layoutFlag) {
		this.layoutFlag = layoutFlag;
	}

	public DataCollectionPanel getParentDcp() {
		return this.parentDcp;
	}

	public boolean isContainedWithin(String infoKey) {
		DataCollectionPanel p = this;
		while (true) {
			if (p.getInfoKey().equals(infoKey)) {
				return true;
			}
			p = p.getParentDcp();
			if (p == null) {
				break;
			}
		}
		return false;
	}

	public int getCategoryLevel() {
		return categoryLevel;
	}

	private void setCategoryLevel(int categoryLevel) {
		this.categoryLevel = categoryLevel;
	}

	public int getBaseLevel() {
		return baseLevel;
	}

	private void setBaseLevel(int baseLevel) {
		this.baseLevel = baseLevel;
	}

	public void setBaseCategoryLevel(int level) {
		this.setBaseLevel(level);
		for (DataCollectionPanel panel : this.getChildDataCollectionPanels().values()) {
			panel.setBaseCategoryLevel(level);
		}
	}

	@Override
	public DataCollectionField getParentField() {
		return this.parentField;
	}
	
	public void setParentField(DataCollectionField field) {
		this.parentField = field;
	}

	@Override
	public Set<DataCollectionComponent> getChildFieldList() {
		return new HashSet<DataCollectionComponent>();
	}

	@Override
	public void setCollectedStatus(int collectionStatus) {
	}

	@Override
	public boolean isChildFieldVisible() {
		boolean b1 = this.isParentFieldVisible();
		boolean b2 = (this.getParentValue() == null) || (this.getParentField().getParentValueOfChildren().equals(this.getParentValue()));
		return b1 && b2;
	}
	
	private String getParentValue() {
		return this.parentValue;
	}

	private boolean isParentFieldVisible() {
		if (this.parentField == null) {
			return true;
		} else if (this.key.getParentValue() != null) {
			if (this.parentField.getParentValueOfChildren().equals(this.key.getParentValue())) {
				return this.parentField.isParentFieldVisible();
			} else {
				return false;
			}
		} else {
			return this.parentField.isParentFieldVisible();
		}
	}

	@Override
	public String getSortString() {
		return this.sortString;
	}

	public PanelType getPanelType() {
		return this.panelType;
	}
}
