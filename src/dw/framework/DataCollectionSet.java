package dw.framework;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import dw.framework.FieldKey;

public class DataCollectionSet implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public enum DataCollectionType {
		PROFILE_SEARCH,
		PROFILE_EDIT,
		VALIDATION
	}
	
	public enum DataCollectionStatus {
		VALIDATION_NOT_PERFORMED,
		NEED_MORE_DATA,
		NOT_VALIDATED,
		VALIDATED,
		COMPLETED,
		ERROR,
		RETRY
	}
	
	// Profile Create or Update
	
	private static final String SENDER_NOTIFICATIONS = "senderNotificationUpdateinfoSet";
	private static final String SENDER_PLUS_PROGRAM = "senderNotificationPlusUpdateinfoSet";
	private static final String RECEIVER_NOTIFICATIONS = "receiverNotificationUpdateinfoSet";
	private static final String RECEIVER_PLUS_PROGRAM = "receiverNotificationPlusUpdateinfoSet";
	public static final List<String> SENDER_PROFILE_SCREEN_2_PANEL_NAMES = Collections.unmodifiableList(Arrays.asList(SENDER_NOTIFICATIONS, SENDER_PLUS_PROGRAM));
	public static final List<String> RECEIVER_PROFILE_SCREEN_2_PANEL_NAMES = Collections.unmodifiableList(Arrays.asList(RECEIVER_NOTIFICATIONS, RECEIVER_PLUS_PROGRAM));
	
	// Send Transaction
	
	private static final String SENDER_INFORMATION = "mtSenderInformationSet";
	private static final String SENDER_GENERAL = "mtTransactionInfoGeneralSet";
	private static final String SENDER_COMPLIANCE = "mtTransactionInfoComplianceSet";
	private static final String SENDER_THIRD_PARTY = "mtTPSenderInformationSet";
	private static final String RECEIVE_NAME = "mtReceiverInformationSet";
	private static final String RECEIVE_ACCOUNT_INFORMATION = "mtTransactionInfoAccountDepositSet";
	private static final String RECEIVE_ACCOUNT_IDENTIFICATION = "receiverAccountIdentificationSet";

	public static final List<String> MSW30_PANEL_NAMES = Collections.unmodifiableList(Arrays.asList(SENDER_INFORMATION, SENDER_GENERAL, SENDER_COMPLIANCE, "mtSenderTransactionInfoSet"));
	public static final List<String> MSW40_PANEL_NAMES = Collections.unmodifiableList(Arrays.asList(SENDER_THIRD_PARTY));
	public static final List<String> MSW60_PANEL_NAMES = Collections.unmodifiableList(Arrays.asList(RECEIVE_NAME));
	public static final List<String> MSW61_PANEL_NAMES = Collections.unmodifiableList(Arrays.asList(RECEIVE_NAME));
	public static final List<String> MSW63_PANEL_NAMES = Collections.unmodifiableList(Arrays.asList(RECEIVE_ACCOUNT_IDENTIFICATION, RECEIVE_ACCOUNT_INFORMATION));

	public static final List<String> SEND_BASE_EXCLUDE = Collections.unmodifiableList(Arrays.asList("mtTransactionInfoDestAmountSet"));
	
	// Bill Pay Transaction
	
	private static final String BP_SENDER_INFORMATION = "billPaySenderInformationSet";
	private static final String BP_PAYEE_INFORMATION = "billPayReceiveInformationSet";
	private static final String BP_COMPLIANCE_INFORMATION = "billPayComplianceSecondarySet";
	private static final String BP_THIRD_PARTY_INFORMATION = "billPayTPSenderInformationSet";
	private static final String BP_THIRD_PARTY_ID_INFORMATION = "bpThirdPartyIdInformationSet";
	private static final String BP_ACCOUNT_INFORMATION = "billPayTransactionInfoSet";
	private static final String BP_TRANSACTION_INFORMATION = "billPayTransactionInfoBillerSet";

	public static final List<String> BPW25_PANEL_NAMES = Collections.unmodifiableList(Arrays.asList(BP_SENDER_INFORMATION, BP_PAYEE_INFORMATION, BP_COMPLIANCE_INFORMATION, BP_ACCOUNT_INFORMATION, BP_TRANSACTION_INFORMATION));
	public static final List<String> BPW30_PANEL_NAMES = Collections.unmodifiableList(Arrays.asList(BP_THIRD_PARTY_INFORMATION, BP_THIRD_PARTY_ID_INFORMATION));
	public static final List<String> BPW36_PANEL_NAMES = Collections.unmodifiableList(Arrays.asList(BP_SENDER_INFORMATION));
	public static final List<String> BPW38_PANEL_NAMES = Collections.unmodifiableList(Arrays.asList(BP_THIRD_PARTY_INFORMATION));
	public static final List<String> BPW39_PANEL_NAMES = Arrays.asList();

	public static final List<String> BP_BASE_EXCLUDE = Collections.unmodifiableList(Arrays.asList("billPayPayoutSet", "billPayReceiveAgentSet"));
	
	// Receive Transaction
	
	private static final String RECEIVE_ADDRESS = "receiveReceiverInformationSet"; 
	private static final String RECEIVE_COMPLIANCE = "receiveTransactionInfoComplianceSet"; 
	private static final String RECEIVE_THIRD_PARTY = "receiveTPReceiverInformationSet";
	
	public static final List<String> MRW30_PANEL_NAMES = Collections.unmodifiableList(Arrays.asList(RECEIVE_ADDRESS, RECEIVE_COMPLIANCE));
	public static final List<String> MRW35_PANEL_NAMES = Collections.unmodifiableList(Arrays.asList(RECEIVE_THIRD_PARTY));
	
	public static final List<String> RECEIVE_BASE_EXCLUDE = Collections.unmodifiableList(Arrays.asList("receiveReceiverPayoutSet", "receiverTransactionInfo")); 
	
	// Send Reversal Transaction 
	
	public static final List<String> SEND_REVERSAL_EXCLUDE = Collections.unmodifiableList(Arrays.asList("sendReversalPayoutSet"));
	
	// Amend Transaction
	
	public static final List<String> AMEND_EXCLUDE =  Collections.unmodifiableList(Arrays.asList("AmendPayoutSet"));

	private DataCollectionStatus validationStatus;
	private DataCollectionType dataCollectionType;
	
	private Map<FieldKey, DataCollectionField> fieldMap;
	private Map<String, List<DataCollectionField>> categoryMap;
	private Map<FieldKey, Object> fieldValueMap;
	private Map<FieldKey, Object> defaultValueMap;
	private Map<String, DataCollectionPanel> dataCollectionPanelMap;
	private Map<String, String> currentValueMap;
	private List<FieldKey> validationTagList;
	private List<DataCollectionField> businessErrorList;
	private List<DataCollectionField> additionalDataCollectionFields;
	private Set<String> verifiedFieldsSet;
	private Set<DataCollectionField> additionalInformationScreenFields;
	private boolean afterValidationAttempt;
	private Object changeFlag;
	private int validationErrorCode;
	private int retryCount;
	
	public DataCollectionSet(DataCollectionType type) {
		this.dataCollectionType = type;
		this.fieldMap = new HashMap<FieldKey, DataCollectionField>();
		this.categoryMap = new HashMap<String, List<DataCollectionField>>();
		this.fieldValueMap = new HashMap<FieldKey, Object>();
		this.defaultValueMap = new HashMap<FieldKey, Object>();
		this.validationTagList = new ArrayList<FieldKey>();
		this.verifiedFieldsSet = new HashSet<String>();
		this.dataCollectionPanelMap = new HashMap<String, DataCollectionPanel>();
		this.currentValueMap = new HashMap<String, String>();
		afterValidationAttempt = false;
	}
	
	public Map<FieldKey, DataCollectionField> getFieldMap() {
		return this.fieldMap;
	}
	
	public void setFieldMap(Map<FieldKey, DataCollectionField> fieldMap) {
		this.fieldMap = fieldMap;
	}

	public Map<String, List<DataCollectionField>> getGffpCategoryMap() {
		return this.categoryMap;
	}
	
	public void setGffpCategoryMap(Map<String, List<DataCollectionField>> categoryMap) {
		this.categoryMap = categoryMap;
	}
	
	public void clear() {
		if (this.fieldMap != null) {
			this.fieldMap.clear();
		}
		if (this.categoryMap != null) {
			this.categoryMap.clear();
		}
		this.fieldValueMap.clear();
		this.validationTagList.clear();
		this.verifiedFieldsSet.clear();
		this.dataCollectionPanelMap.clear();
		validationStatus = DataCollectionStatus.VALIDATION_NOT_PERFORMED;
	}
	
	public Map<FieldKey, Object> getFieldValueMap() {
		return this.fieldValueMap;
	}
	
	public void clearFieldValueMap() {
		this.fieldValueMap.clear();
	}
	
	public Map<FieldKey, Object> getDefaultValueMap() {
		return this.defaultValueMap;
	}

	public void setFieldValue(FieldKey key, Object value) {
		this.fieldValueMap.put(key, value);
	}
	
	public String getValue(FieldKey key) {
		String value = null;
		List<DataCollectionField> list = getFieldsByInfoKey(key.getInfoKey());
		if (! list.isEmpty()) {
			for (DataCollectionField field : list) {
				if (field.isChildFieldVisible()) {
					value = field.getValue();
					if ((value == null) || (value.isEmpty())) {
						value = field.getDefaultValue();
					}
					if ((value == null) || (value.isEmpty())) {
						value = currentValueMap.get(key.getInfoKey());
					}
				}
			}
		} else {
			value = currentValueMap.get(key.getInfoKey());
		}
		return value;
	}

	public void setValidationErrorCode(int validationErrorCode) {
		this.validationErrorCode = validationErrorCode;
	}
	
	public int getValidationErrorCode() {
		return this.validationErrorCode;
	}
	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}

	public void incrementRetryCount() {
		this.retryCount++;
	}

	public void setValidationStatus(DataCollectionStatus validationStatus) {
		this.validationStatus = validationStatus;
	}
	
	public DataCollectionStatus getValidationStatus() {
		return this.validationStatus;
	}

	public int getRetryCount() {
		return this.retryCount;
	}

	public Object getFieldValue(FieldKey tag) {
		return this.fieldValueMap.get(tag);
	}
	
	public String getFieldStringValue(FieldKey key) {
		Object value = getFieldValue(key);
		return value != null ? value.toString() : "";
	}

	public String getFieldStringValue(String tag) {
		FieldKey key = FieldKey.key(tag);
		return getFieldStringValue(key);
	}

	public List<FieldKey> getValidationTagList() {
		return this.validationTagList;
	}

	public Set<String> getVerifiedFieldsSet() {
		return this.verifiedFieldsSet;
	}
	
	public Map<String, DataCollectionPanel> getDataCollectionPanelMap() {
		return this.dataCollectionPanelMap;
	}
	
	public List<DataCollectionPanel> getSupplementalDataCollectionPanels(Set<String> excludedPanels) {
		List<DataCollectionPanel> supplementalDataCollectionPanelList = new ArrayList<DataCollectionPanel>();
		for (DataCollectionPanel dcp : this.getDataCollectionPanelMap().values()) {
			if ((excludedPanels != null) && (excludedPanels.contains(dcp.getInfoKey()))) {
				continue;
			}
			if (! dcp.getChildDataCollectionFields().isEmpty()) {
				supplementalDataCollectionPanelList.add(dcp);
			}
		}
		return supplementalDataCollectionPanelList;
	}
	
	public List<DataCollectionField> getAdditionalDataCollectionFields(List<String> excludedPanels) {
		List<DataCollectionField> additionalDataCollectionFieldList = new ArrayList<DataCollectionField>();
		loop1: for (DataCollectionPanel dcp : this.getDataCollectionPanelMap().values()) {
			
			// Check if panel should be excluded.
			
			if (excludedPanels != null) {
				for (String infoKey : excludedPanels) {
					if (dcp.isContainedWithin(infoKey)) {
						continue loop1;
					}
				}
			}
			
			// Get any field that needs to be collected.
			
			dcp.getAdditionalDataCollectionFields(additionalDataCollectionFieldList);
		}
		return additionalDataCollectionFieldList;
	}
	
	public void clearBusinessErrorList() {
		if (this.businessErrorList == null) {
			this.businessErrorList = new ArrayList<DataCollectionField>();
		} else {
			this.businessErrorList.clear();
		}
	}

	public List<DataCollectionField> getBusinessErrors() {
		return this.businessErrorList;
	}

	public void updated() {
		this.changeFlag = new Object();
	}
	
	public Object getChangeFlag() {
		return this.changeFlag;
	}

	public Object getFieldObject(FieldKey key) {
		return this.fieldValueMap.get(key);
	}
	
	public void setFieldObject(FieldKey key, Object value) {
		this.fieldValueMap.put(key, value);
	}
	
	public DataCollectionField findFieldByInfoKey(String infoKey) {
		for (DataCollectionField field : this.getFieldMap().values()) {
			if (field.getInfoKey().equals(infoKey)) {
				if ((field.getParentValue() == null) || field.isChildFieldVisible()) {
					return field;
				}
			}
		}
		return null;
	}
	
	public DataCollectionField findVisibleFieldByInfoKey(String infoKey) {
		for (DataCollectionField field : this.getFieldMap().values()) {
			if (field.getInfoKey().equals(infoKey)) {
				if ((field.getParentValue() == null) || field.isChildFieldVisible()) {
					return field;
				}
			}
		}
		return null;
	}

	public DataCollectionField getFieldByInfoKey(String infoKey) {
		for (DataCollectionField field : fieldMap.values()) {
			if (infoKey.equals(field.getInfoKey())) {
				if ((! field.isChildField()) || field.isChildFieldVisible()) {
					return field;
				}
			}
		}
		return null;
	}

	public List<DataCollectionField> getFieldsByInfoKey(String infoKey) {
		List<DataCollectionField> list = new ArrayList<DataCollectionField>();
		for (DataCollectionField field : fieldMap.values()) {
			if (infoKey.equals(field.getInfoKey())) {
				list.add(field);
			}
		}
		return list;
	}
	
	public Map<String, String> getCurrentValueMap() {
		return this.currentValueMap;
	}
	
	public void putCurrentValue(String infoKey, String value) {
		this.currentValueMap.put(infoKey, value);
	}
	
	public String getCurrentValue(String infoKey) {
		String value = this.currentValueMap.get(infoKey);
		return value != null ? value : "";
	}
	
	public void clearCurrentValues() {
		this.currentValueMap.clear();
	}

	public void setAdditionalDataCollectionFields(List<DataCollectionField> additionalDataCollectionFields) {
		this.additionalDataCollectionFields = additionalDataCollectionFields;
	}
	
	public List<DataCollectionField> getAdditionalDataCollectionFields() {
		return this.additionalDataCollectionFields;
	}

	public boolean isAfterValidationAttempt() {
		return afterValidationAttempt;
	}

	public void setAfterValidationAttempt(boolean afterValidationAttempt) {
		this.afterValidationAttempt = afterValidationAttempt;
	}

	public DataCollectionType getDataCollectionType() {
		return dataCollectionType;
	}

	public void setDataCollectionType(DataCollectionType dataCollectionType) {
		this.dataCollectionType = dataCollectionType;
	}

	public void setCurrentValuesMap(Map<String, String> currentValuesMap) {
		this.currentValueMap = currentValuesMap;
	}
	
	public boolean isAdditionalInformationScreenValid() {
		boolean flag = true;
		
    	if (this.getAdditionalInformationScreenFields() != null) {
			for (DataCollectionField field : this.getAdditionalInformationScreenFields()) {
				if (field != null) {
					if ((field.isVisible()) && (field.getDataComponent() != null) && (field.getDataComponent().isEnabled()) && (field.isValidatable()) && (field.validate(DataCollectionField.SILENT_VALIDATION) != null)) {
						flag = false;
						break;
					}
					if ((field.getBusinessErrors() != null) && (! field.getBusinessErrors().isEmpty()) && (! field.isModified()) && (field.validate(DataCollectionField.SILENT_VALIDATION) != null)) {
						flag = false;
						break;
					}
				}
			}
    	}
		
		return flag;
	}
	
	public void resetModifyFlags() {
		for (DataCollectionField field : this.getAdditionalInformationScreenFields()) {
			field.setModified(false);
		}
	}

	public Set<DataCollectionField> getAdditionalInformationScreenFields() {
		return additionalInformationScreenFields;
	}

	public void setAdditionalInformationScreenFields(Set<DataCollectionField> additionalInformationScreenFields) {
		this.additionalInformationScreenFields = additionalInformationScreenFields;
	}
}
