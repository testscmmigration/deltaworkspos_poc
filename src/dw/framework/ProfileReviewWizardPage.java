package dw.framework;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import dw.accountdepositpartners.AccountDepositPartnersTransaction;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.mgreceive.MoneyGramReceiveTransaction;
import dw.mgsend.MoneyGramClientTransaction.ProfileStatus;
import dw.mgsend.MoneyGramClientTransaction;
import dw.mgsend.MoneyGramSendTransaction;
import dw.model.adapters.ConsumerProfile;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.ConsumerProfile.NotificationOptionDisplayStatus;
import dw.model.adapters.CountryInfo.Country;
import dw.model.adapters.CountryInfo.CountrySubdivision;
import dw.utility.DWValues;

public class ProfileReviewWizardPage extends FlowPage implements DWTextPaneListener {

	private static final long serialVersionUID = 1L;
	
	private static final int SCREEN_WIDTH = 475;

	private JPanel panel;
    private JButton editProfileButton;
    private JButton newProfileButton;
	private MoneyGramClientTransaction transaction;
	private DWTextPane text1;
	private List<DWTextPane> textPanes;
	private int tagWidth;
	
	public ProfileReviewWizardPage(AccountDepositPartnersTransaction transaction, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/ProfileReviewWizardPage.xml", name, pageCode, buttons);
		this.transaction = transaction;
		init();
	}

	public ProfileReviewWizardPage(MoneyGramSendTransaction transaction, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/ProfileReviewWizardPage.xml", name, pageCode, buttons);
		this.transaction = transaction;
		init();
	}

	public ProfileReviewWizardPage(MoneyGramReceiveTransaction transaction, String name, String pageCode, PageFlowButtons buttons) {
		super("framework/ProfileReviewWizardPage.xml", name, pageCode, buttons);
		this.transaction = transaction;
		init();
	}

	@Override
	public void dwTextPaneResized() {
		int width1 = SCREEN_WIDTH - 20;
		int width2 = width1 - tagWidth;
		text1.setWidth(width1);
		for (DWTextPane pane : textPanes) {
			pane.setWidth(width2);
		}
	}
	
	private void init() {
		panel = (JPanel) this.getComponent("panel2");
		editProfileButton = (JButton) this.getComponent("editProfileButton");
		newProfileButton = (JButton) this.getComponent("newProfileButton");
		text1 = (DWTextPane) this.getComponent("text1");
		text1.addListener(panel, this);
		text1.setListenerEnabled(false);
		textPanes = new ArrayList<DWTextPane>();
		
		editProfileButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				editProfile(ProfileStatus.EDIT_PROFILE);
			}
		});
		newProfileButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				newProfile();
			}
		});
	}

	@Override
	public void start(int direction) {
		flowButtons.reset();
		flowButtons.setVisible("expert", false);
        KeyMapManager.mapComponent(this, KeyEvent.VK_F6, 0, editProfileButton);
        KeyMapManager.mapComponent(this, KeyEvent.VK_F7, 0, newProfileButton);
        flowButtons.getButton("next").requestFocus();
		text1.setListenerEnabled(true);
		transaction.setProfileStatus(ProfileStatus.USE_CURRENT_PROFILE);
		String key;
		
		ConsumerProfile profile = transaction.getProfileObject();
		textPanes.clear();
		
    	List<JLabel> tags = new ArrayList<JLabel>();
    	String nameTag = Messages.getString("MGSWizardPage13.1161");
		
		// Name
		
		String firstName;
		String middleName;
		String lastName1;
		String lastName2;
		if (transaction instanceof MoneyGramSendTransaction) {
			firstName = profile.getProfileValue(FieldKey.SENDER_FIRSTNAME_KEY.getInfoKey());
			middleName = profile.getProfileValue(FieldKey.SENDER_MIDDLENAME_KEY.getInfoKey());
			lastName1 = profile.getProfileValue(FieldKey.SENDER_LASTNAME_KEY.getInfoKey());
			lastName2 = profile.getProfileValue(FieldKey.SENDER_LASTNAME2_KEY.getInfoKey());
		} else {
			firstName = profile.getProfileValue(FieldKey.RECEIVER_FIRSTNAME_KEY.getInfoKey());
			middleName = profile.getProfileValue(FieldKey.RECEIVER_MIDDLENAME_KEY.getInfoKey());
			lastName1 = profile.getProfileValue(FieldKey.RECEIVER_LASTNAME_KEY.getInfoKey());
			lastName2 = profile.getProfileValue(FieldKey.RECEIVER_LASTNAME2_KEY.getInfoKey());
		}
		String name = DWValues.formatName(firstName, middleName, lastName1, lastName2);
		this.displayPaneItem("name", nameTag, name, tags, textPanes);
		
		// Gender
		
		key = transaction instanceof MoneyGramSendTransaction ? FieldKey.SENDER_GENDER_KEY.getInfoKey() : FieldKey.RECEIVER_GENDER_KEY.getInfoKey();
		this.displayLabelItem("gender", profile.getLabel(key), profile.getProfileValue(key), tags);
		
		// Address
		
		String address1;
		String address2;
		String address3;
		String address4;
		if (transaction instanceof MoneyGramSendTransaction) {
			address1 = profile.getProfileValue(FieldKey.SENDER_ADDRESS_KEY.getInfoKey());
			address2 = profile.getProfileValue(FieldKey.SENDER_ADDRESS2_KEY.getInfoKey());
			address3 = profile.getProfileValue(FieldKey.SENDER_ADDRESS3_KEY.getInfoKey());
			address4 = profile.getProfileValue(FieldKey.SENDER_ADDRESS4_KEY.getInfoKey());
		} else {
			address1 = profile.getProfileValue(FieldKey.RECEIVER_ADDRESS_KEY.getInfoKey());
			address2 = profile.getProfileValue(FieldKey.RECEIVER_ADDRESS2_KEY.getInfoKey());
			address3 = profile.getProfileValue(FieldKey.RECEIVER_ADDRESS3_KEY.getInfoKey());
			address4 = profile.getProfileValue(FieldKey.RECEIVER_ADDRESS4_KEY.getInfoKey());
		}
		StringBuffer sb = new StringBuffer();
		if ((address1 != null) && (! address1.isEmpty())) {
			sb.append(address1);
		}
		if ((address2 != null) && (! address2.isEmpty())) {
			sb.append(", ").append(address2);
		}
		if ((address3 != null) && (! address3.isEmpty())) {
			sb.append(", ").append(address3);
		}
		if ((address4 != null) && (! address4.isEmpty())) {
			sb.append(", ").append(address4);
		}
		this.displayPaneItem("address", Messages.getString("MGRWizardPage9.958b"), sb.toString(), tags, textPanes);
		
		// City
		
		key = transaction instanceof MoneyGramSendTransaction ? FieldKey.SENDER_CITY_KEY.getInfoKey() : FieldKey.RECEIVER_CITY_KEY.getInfoKey();
		this.displayLabelItem("city", profile.getLabel(key), profile.getProfileValue(key), tags);
		
		String countryName = null;
		String stateName = null;
		String countryKey = transaction instanceof MoneyGramSendTransaction ? FieldKey.SENDER_COUNTRY_KEY.getInfoKey() : FieldKey.RECEIVER_COUNTRY_KEY.getInfoKey();
		String countryCode = profile.getProfileValue(countryKey);
		String stateKey = transaction instanceof MoneyGramSendTransaction ? FieldKey.SENDER_COUNTRY_SUBDIVISIONCODE_KEY.getInfoKey() : FieldKey.RECEIVER_COUNTRY_SUBDIVISIONCODE_KEY.getInfoKey();
		if ((countryCode != null) && (! countryCode.isEmpty())) {
			Country countryObject = CountryInfo.getCountry(countryCode);
			if (countryObject != null) {
				countryName = countryObject.getCountryName() + " (" + countryCode + ")";
				String stateCode = profile.getProfileValue(stateKey);
				if ((stateCode != null) && (!stateCode.isEmpty())) {
					CountrySubdivision stateObject = countryObject.getCountrySubdivision(stateCode);
					if (stateObject != null) {
						stateName = stateObject.getCountrySubdivisionName();
					}
				}
			}
		}
		
		// Country Subdivision
		
		this.displayLabelItem("state", profile.getLabel(stateKey), stateName, tags);
		
		// Country
		
		this.displayLabelItem("country", profile.getLabel(countryKey), countryName, tags);
		
		// Postal Code
		
		key = transaction instanceof MoneyGramSendTransaction ? FieldKey.SENDER_POSTALCODE_KEY.getInfoKey() : FieldKey.RECEIVER_POSTALCODE_KEY.getInfoKey();
		this.displayLabelItem("postalCode", profile.getLabel(key), profile.getProfileValue(key), tags);
		
		// Primary Phone Number
		
		key = transaction instanceof MoneyGramSendTransaction ? FieldKey.SENDER_PRIMARYPHONECOUNTRYCODE_KEY.getInfoKey() : FieldKey.RECEIVER_PRIMARYPHONECOUNTRYCODE_KEY.getInfoKey();
		String cc = profile.getProfileValue(key);
		key = transaction instanceof MoneyGramSendTransaction ? FieldKey.SENDER_PRIMARYPHONE_KEY.getInfoKey() : FieldKey.RECEIVER_PRIMARYPHONE_KEY.getInfoKey();
		String pn = profile.getProfileValue(key);
		sb = new StringBuffer();
		if ((cc != null) && (! cc.isEmpty())) {
			sb.append("+").append(cc).append(" ");
		}
		if ((pn != null) && (! pn.isEmpty())) {
			sb.append(pn);
		}
		this.displayLabelItem("primaryPhone", profile.getLabel(key), sb.toString(), tags);
		
		// Secondary Phone Number
		
		key = transaction instanceof MoneyGramSendTransaction ? FieldKey.SENDER_SECONDARYPHONECOUNTRYCODE_KEY.getInfoKey() : FieldKey.RECEIVER_SECONDARYPHONECOUNTRYCODE_KEY.getInfoKey();
		cc = profile.getProfileValue(key);
		key = transaction instanceof MoneyGramSendTransaction ? FieldKey.SENDER_SECONDARYPHONE_KEY.getInfoKey() : FieldKey.RECEIVER_SECONDARYPHONE_KEY.getInfoKey();
		pn = profile.getProfileValue(key);
		sb = new StringBuffer();
		if ((cc != null) && (! cc.isEmpty())) {
			sb.append("+").append(cc).append(" ");
		}
		if ((pn != null) && (! pn.isEmpty())) {
			sb.append(pn);
		}
		this.displayLabelItem("secondaryPhone", profile.getLabel(key), sb.toString(), tags);
		
		// Date of Birth
		
		key = transaction instanceof MoneyGramSendTransaction ? FieldKey.SENDER_DOB_KEY.getInfoKey() : FieldKey.RECEIVER_DOB_KEY.getInfoKey();
		String dob = profile.getProfileValue(key);
		String value = dob != null ? FormatSymbols.formatDateForLocale(FormatSymbols.convertCCYYMMDDToDate(dob)) : null;
		this.displayLabelItem("dob", profile.getLabel(key), value, tags); 
		
		// Country of Birth
		
		key = transaction instanceof MoneyGramSendTransaction ? FieldKey.SENDER_BIRTH_COUNTRY_KEY.getInfoKey() : FieldKey.RECEIVER_BIRTH_COUNTRY_KEY.getInfoKey();
		countryName = null;
		countryCode = profile.getProfileValue(key);
		if ((countryCode != null) && (! countryCode.isEmpty())) {
			Country countryObject = CountryInfo.getCountry(countryCode);
			if (countryObject != null) {
				countryName = countryObject.getCountryName() + " (" + countryCode + ")";
			}
		}
		this.displayLabelItem("cob", profile.getLabel(key), countryName, tags); 
		
		// Email
		
		key = transaction instanceof MoneyGramSendTransaction ? FieldKey.SENDER_EMAIL_KEY.getInfoKey() : FieldKey.RECEIVER_EMAIL_KEY.getInfoKey();
		this.displayPaneItem("email", profile.getLabel(key), profile.getProfileValue(key), tags, textPanes);
		
		// Occupation
		
		String key1 = transaction instanceof MoneyGramSendTransaction ? FieldKey.SENDER_OCCUPATION_KEY.getInfoKey() : FieldKey.RECEIVER_OCCUPATION_KEY.getInfoKey();
		String v1 = profile.getProfileValue(key1);
		String key2 = transaction instanceof MoneyGramSendTransaction ? FieldKey.SENDER_OCCUPATIONOTHER_KEY.getInfoKey() : FieldKey.RECEIVER_OCCUPATIONOTHER_KEY.getInfoKey();
		String v2 = profile.getProfileValue(key2);
		this.displayPaneItem("occupation", profile.getLabel(key1), profile.getEnumValue(key1, v1, v2), tags, textPanes);
 
			// MGI Rewards Number

		key = FieldKey.MGIREWARDSNUMBER_KEY.getInfoKey();
		this.displayLabelItem("rewardsNumber", profile.getLabel(key), profile.getProfileValue(key), tags);
		
		tagWidth = this.setTagLabelWidths(tags, MINIMUM_TAG_WIDTH);
		
		dwTextPaneResized();
	}

	@Override
	public void finish(int direction) {
		transaction.getProfileObject().restoreOriginalValues();
		text1.setListenerEnabled(false);
		
		if ((direction == PageExitListener.NEXT) && (transaction.getProfileObject().getNotificationsDisplayed().equals(NotificationOptionDisplayStatus.NOT_DISPLAYED))) {
			this.editProfile(ProfileStatus.USE_CURRENT_PROFILE);
		} else {
			PageNotification.notifyExitListeners(this, direction);
		}
	}
	
	private void editProfile(final ProfileStatus profileStatus) {
		Thread t = new Thread() {
			@Override
			public void run() {
				boolean f = transaction.createOrUpdateConsumerProfile(transaction.getProfileObject(), ProfileStatus.EDIT_PROFILE);
				if (f && (! transaction.getDataCollectionData().getValidationStatus().equals(DataCollectionStatus.ERROR))) {
					for (DataCollectionField field : transaction.getDataCollectionData().getFieldMap().values()) {
						String value = transaction.getProfileObject().getProfileValue(field.getInfoKey());
						field.setDefaultValue(value);
					}
					transaction.getProfileObject().restoreOriginalValues();
					text1.setListenerEnabled(false);
					transaction.setProfileStatus(profileStatus);
					PageNotification.notifyExitListeners(ProfileReviewWizardPage.this, PageExitListener.NEXT);
				} else {
					transaction.setDataCollectionData(transaction.getSearchProfilesData());
					PageNotification.notifyExitListeners(ProfileReviewWizardPage.this, PageExitListener.CANCELED);
				}
			}
		};
		t.start();
	}
	
	private void newProfile() {
		Thread t = new Thread() {
			@Override
			public void run() {
				boolean f = transaction.createOrUpdateConsumerProfile(transaction.getProfileObject(), ProfileStatus.CREATE_PROFILE);
				if (f && (! transaction.getDataCollectionData().getValidationStatus().equals(DataCollectionStatus.ERROR))) {
					transaction.getProfileObject().clearCurrentValues();
					transaction.getProfileObject().getReceivers().clear();
					transaction.getProfileObject().getOriginalReceivers().clear();
					transaction.getProfileObject().getBillers().clear();
					transaction.getProfileObject().getOriginalBillers().clear();
					text1.setListenerEnabled(false);
					PageNotification.notifyExitListeners(ProfileReviewWizardPage.this, PageExitListener.NEXT);
				} else {
					transaction.setDataCollectionData(transaction.getSearchProfilesData());
					PageNotification.notifyExitListeners(ProfileReviewWizardPage.this, PageExitListener.CANCELED);
				}
			}
		};
		t.start();
	}
}
