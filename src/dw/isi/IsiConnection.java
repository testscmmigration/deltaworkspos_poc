package dw.isi;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;

//import dw.main.DeltaworksMainMenu;
import dw.main.DeltaworksMainPanel;
import dw.main.MainPanelDelegate;
import dw.profile.ProfileItem;
import dw.profile.UnitProfile;
import dw.utility.Debug;

/**
 * Handles a single connection to an ISI client. The run method in this 
 * class is intended to be the main line of execution for a thread 
 * established when an incoming connection is made. 
 *
 */
public class IsiConnection implements Runnable {
    private Socket socket;
    private BufferedReader input;

    private Socket dataSocket;
    private boolean dataOpen = false;
    private DataOutputStream dataOutput; 
    private boolean hasExeCommand = false;  // Flag to determine if a Exe command is supplied before the -NF command.
    private boolean hasNfCommand =false;


    /** 
     * The command send by the client. Leading and trailing whitespace is
     * trimmed, and the command is converted to upper case.
     */
    protected String command;

    /** 
     * Constructs a connection to serve the client on the given socket. 
     */
    public IsiConnection(Socket socket) {
        this.socket = socket;
        Debug.println(" connected to  " + socket.getInetAddress()	
                + " port " + socket.getPort()); 
        Debug.println(" local addr is " + socket.getLocalAddress()	
                + " port " + socket.getLocalPort()); 
    }


    /** 
     * Opens the data connection.  In active mode, initiate a connection
     * from our data port to the client's data port. Our data port is
     * always port L-1 where L is the control connection port. The client
     * data port is the same as the client control connection port unless
     * the PORT command has been specified. See RFC 959 section 5.2.
     * 
     * In passive mode, the data connection should already be open and 
     * this method should not be called.
     * 
     * @throws IOException if the connection can't be established.
     */
    protected void openData() throws IOException {
        InetAddress remoteAddress, localAddress;
        int remotePort, localPort;

//        if (host == null) {
            remoteAddress = socket.getInetAddress();
            remotePort = socket.getPort();
//        }
//        else {
//            remoteAddress = InetAddress.getByName(host);
//            remotePort = port;
//        }

        localAddress = socket.getLocalAddress(); 
        localPort = socket.getLocalPort() - 1;
        dataSocket = new Socket(remoteAddress, remotePort, 
                localAddress, localPort);

        Debug.println(" connecting to " + dataSocket.getInetAddress()	
                + " port " + dataSocket.getPort()); 
        Debug.println(" local addr is " + dataSocket.getLocalAddress()	
                + " port " + dataSocket.getLocalPort()); 
        
        dataOutput = new DataOutputStream(
                new BufferedOutputStream(dataSocket.getOutputStream()));

        dataOpen = true;
    }
    
    /**
     * Sends a line of data over the data connection.
     */
    protected void sendData(String s) throws IOException {
        if (! dataOpen) 
            openData();

        dataOutput.writeBytes(s);
        dataOutput.writeBytes("\r\n");	
    }

    /**
     * Closes the data connection..
     */
    protected void closeData() {

        if (dataOutput != null) {
            try { 
                dataOutput.flush();
            } 
            catch (IOException ignore) {
            }
        }
  
        if (dataSocket != null) {
            try { 
                dataSocket.close(); 
            } 
            catch (IOException ignore) {
            }
        }

        dataOpen = false;
    }
    
   /**
     * Take the string of ISI command line arguments.  Find each argument that starts
     * with the character '-'. Compare each command found in the string with the valid
     * ISI commands and ignore invalid command lines.  The valid ISI commands are:
     *      -RP <password> 
     *      -FRONT
     *      -BACK
     *      -EXE program.exe <arg1> <arg2> <arg3> ...
     * @param data The command line arguments passed from the C program DWISI.EXE
     */
    private void parseCommands(String data){
        String command;
        StringBuffer sb = new StringBuffer();
        int startPoint, endPoint;
        boolean requiresRpCommand = false;
        boolean rpCommandExecutedOnce = false;
        boolean startTransactionNoLogin = true;
        
        hasExeCommand = false;
        hasNfCommand = false;
        
        String profilePassword = UnitProfile.getInstance().get("ISI_PASSWORD", "NOPASWD").trim();
        if(profilePassword.equals("NOPASWD") || profilePassword.equals("")){
        	requiresRpCommand = false;
            MainPanelDelegate.setIsiRpCommandValid(true);
        }else{        	
        	requiresRpCommand = true;
            MainPanelDelegate.setIsiRpCommandValid(false);
        }
        sb.append(data);
        endPoint = sb.length();
        


        while ((startPoint = sb.lastIndexOf("-",endPoint)) != -1) {
            command = sb.substring(startPoint, endPoint);
            endPoint = startPoint - 1;
            
            if (command.startsWith("-RP")){   
            	commandRp(command); 
            	rpCommandExecutedOnce = true;
            }
        }
        
        endPoint = sb.length();        
        
        
        // Check to see if we have both -EXE and -NF command which is not valid and will give a error code "02"
        while ((startPoint = sb.lastIndexOf("-",endPoint)) != -1) {
            command = sb.substring(startPoint, endPoint);
            endPoint = startPoint - 1;
            
            if (command.startsWith("-EXE")) hasExeCommand = true;
            if (command.startsWith("-NF")) hasNfCommand = true;
        }
        
        endPoint = sb.length();
        
        while ((startPoint = sb.lastIndexOf("-",endPoint)) != -1) {
            command = sb.substring(startPoint, endPoint);
            endPoint = startPoint - 1;
            if (!(hasExeCommand && hasNfCommand)){
	            if (command.startsWith("-EXE"))   {
	                String argument = command.replaceFirst("-EXE ", "").trim();
	                // Find the -EXE code word to execute.
	                startTransactionNoLogin = !doesCodeWordRequireLogin(getCodeWord(argument));
	            }
            }
        }
        
        endPoint = sb.length();

        
        // Set the RP command to true in case the -RP command line
        // is not supplied.  Method commandRp contains the business
        // logic for validation of the -RP command if a command is supplied.
        MainPanelDelegate.setIsiFrontCommandOccurred(false);
        while ((startPoint = sb.lastIndexOf("-",endPoint)) != -1) {
            command = sb.substring(startPoint, endPoint);
            endPoint = startPoint - 1;
            
            if (command.startsWith("-FRONT"))   {
            	// Only run the command Front if you don't login to the transaction.
            	if (startTransactionNoLogin){
            		commandFront();    
            	}
                MainPanelDelegate.setIsiFrontCommandOccurred(true);
            }
        }
        
        endPoint = sb.length();

        while ((startPoint = sb.lastIndexOf("-",endPoint)) != -1) {
            command = sb.substring(startPoint, endPoint);
            endPoint = startPoint - 1;
            
            if (command.startsWith("-RP")){   
            	// Do nothing handled above 
            }
            else if (command.startsWith("-FRONT")){
            	// Do nothing handled above 
            }
            else if (command.startsWith("-BACK"))   commandBack();    
            else if (command.startsWith("-EXE2"))   commandExe2(command);    
            else if (command.startsWith("-EXE"))    commandExe(command);    
            else if (command.startsWith("-NF"))     commandNf(command);
            else unknown(command);
        }
        
        if (requiresRpCommand && !rpCommandExecutedOnce)
        	commandRp("");
    }

    /**
     * Loops over input read from the control connection, and
     * processes the commands. 
     */
    @Override
	public void run() {
        try {
            input = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
          
            // Get the command line
            while (true) {
                command = input.readLine();
                if (command == null)
                    break;
                
                // Remove extra spaces and parse the commands.
                command = command.trim();
                Debug.println("ISI Request:["+command+"]");
                parseCommands(command);
            }
        }
        catch (IOException e) {
            Debug.println("IsiConnection: " + e);	
        }
        finally {
            // close the connections
            closeData();
            
            try {
                socket.close();
            }
            catch (IOException ignore) {
            }
        }
    }


    /**
     * -RP ISI command line functionality.  Strip the command argument -RP 
     * from the optional password string.  Then compare this password with
     * the profile value for ISI_PASSWORD.  If the -RP command and its password
     * are valid, set the valid flag to true.  If the -RP command doesn't 
     * include a password and the profile item ISI_PASSWORD is not defined,
     * the valid flag is set to true.  For all other cases teh valid flag 
     * is set to false.  
     * @param password - The option password supplied by the command line
     */
    protected void commandRp(String password){
       String profilePswd = UnitProfile.getInstance().get("ISI_PASSWORD", "NOPASWD").trim();
       String pswd = password.replaceFirst("-RP ","").trim();
         if (!MainPanelDelegate.getIsiTransactionInProgress()){            
            if (profilePswd.equals("NOPASWD") && password.trim().equals("-RP"))
                MainPanelDelegate.setIsiRpCommandValid(true);
            else if (pswd.equals(profilePswd))
                MainPanelDelegate.setIsiRpCommandValid(true);
            else if (pswd.trim().equals("-RP")&& profilePswd.trim().equals(""))
                MainPanelDelegate.setIsiRpCommandValid(true);
            else
                MainPanelDelegate.setIsiRpCommandValid(false);
        }
    }
    
    /**
     * -FRONT ISI command line functionality.  This command line option brings the
     * running version of DeltaWorks to the foreground.
     */
    protected void commandFront() {
        MainPanelDelegate.executeIsiFrontCommand();
    }    
    
    /**
     * -BACK ISI command line functionality.  This command line option minimizes brings the
     * running version of DeltaWorks to the background.
     */
    protected void commandBack() {
        MainPanelDelegate.executeIsiBackCommand();
    }    

    /**
     * -EXE ISI command line functionality.  This command line allows a single
     * transaction to be started. Data for this transaction will be written to 
     * the DWISI.CSV file and the program will be executed if all the business
     * logic is satisfied.
     *  
     * @param command - Contains the program.exe arg1 arg2.... to execute once
     *                  the data is written to the log.
     */
    protected void commandExe(String command) {
    	// If there is a NF command do not run.
    	if (hasNfCommand && hasExeCommand)
    		return;
    	
        // Command -EXE program.exe arg1 arg2... specific code
        // Strip command argument -EXE from the program arguments.
        if (!MainPanelDelegate.getIsiTransactionInProgress()){            
            MainPanelDelegate.setIsiExeCommand(true);
            String argument = command.replaceFirst("-EXE ", "").trim();
            String exeArgument;
            // Find the -EXE code word to execute.
            String exeCode = getCodeWord(argument);
            if (exeCode.length() > 0)
            	exeArgument = argument.replaceFirst(exeCode, "").trim();
            else	
            	exeArgument = argument;
            MainPanelDelegate.setIsiExeProgramArguments(exeArgument);
            
            if (MainPanelDelegate.getIsiRpCommandValid())
            	MainPanelDelegate.setupControlledLaunchISI(exeCode);
            else {
        		MainPanelDelegate.setIsiAppendMode(false);
            	MainPanelDelegate.setupErrorISI("04");
             	String programName = "";
        		if (MainPanelDelegate.getIsiExeProgramArguments() != null)
        			programName = MainPanelDelegate.getIsiExeProgramArguments().trim();
        		
        		if (programName.length() > 0 && !(programName.startsWith("-EXE"))) {
	  	  			MainPanelDelegate.executeIsiProgram(programName);
        		}
          	  	MainPanelDelegate.setIsiTransactionInProgress(false);
          	  	MainPanelDelegate.setIsiExeCommand(false);
          	  	MainPanelDelegate.setIsiExeProgramArguments("");
          	  	MainPanelDelegate.setIsiRpCommandValid(false);
            }
        } else {
        	MainPanelDelegate.setupErrorISI("01");
        	Debug.println("YOU ARE HERE WITHOUT RUNNING THE -EXE COMMAND");
        }
    }
    /**
     * -EXE2 ISI command line functionality.  Data for this transaction will be 
     * written to the DWISI.CSV file and the program will be executed if all 
     * the business logic is satisfied.
     *  
     * @param command - Contains the program.exe arg1 arg2.... to execute once
     *                  the data is written to the log.
     */
    protected void commandExe2(String command) {
    	// If there is a NF command do not run.
    	if (hasNfCommand && hasExeCommand)
    		return;
    	
        // Command -EXE2 program.exe arg1 arg2... specific code
        // Strip command argument -EXE2 from the program arguments.
        if (!MainPanelDelegate.getIsiTransactionInProgress()){            
            MainPanelDelegate.setIsiExe2Command(true);
            String argument = command.replaceFirst("-EXE2 ", "").trim();
            MainPanelDelegate.setIsiExe2ProgramArguments(argument);
        } else {
        	MainPanelDelegate.setupErrorISI("01");
        	Debug.println("YOU ARE HERE WITHOUT RUNNING THE -EXE2 COMMAND");
        }
    }
    /**
     * -NF ISI command line functionality.  This command line allows non financial
     * transaction to be started. Data for this transaction will be written to 
     * the DWISI.CSV file and the program will be executed if all the business
     * logic is satisfied.
     *  
     * @param command - Contains the program.exe arg1 arg2.... to execute once
     *                  the data is written to the log.
     */
    protected void commandNf(String command) {
    	
    
    	if (!MainPanelDelegate.getIsiRpCommandValid()) {
            MainPanelDelegate.setIsiNfProgramArguments(command.replaceFirst("-NF", "").trim());
    		MainPanelDelegate.setIsiAppendMode(false);
	    	MainPanelDelegate.setupErrorISI("04");
	     	String programName = "";
			if (MainPanelDelegate.getIsiNfProgramArguments() != null)
				programName = MainPanelDelegate.getIsiNfProgramArguments().trim();
			
			if (programName.length() > 0 && !(programName.startsWith("-NF"))) {
  	  			MainPanelDelegate.executeIsiProgram(programName);
			}
        	MainPanelDelegate.setIsiNfCommand(false);
        	MainPanelDelegate.setIsiNfProgramArguments("");
      	  	MainPanelDelegate.setIsiTransactionInProgress(false);
      	  	MainPanelDelegate.setIsiExeCommand(false);
      	  	MainPanelDelegate.setIsiExe2Command(false);
      	  	MainPanelDelegate.setIsiExeProgramArguments("");
      	  	MainPanelDelegate.setIsiRpCommandValid(false);
    	}
    	
    	
    	// Command -NF program.exe arg1 arg2... specific code
        // Strip command argument -NF from the program arguments.
        if (!MainPanelDelegate.getIsiTransactionInProgress() && MainPanelDelegate.getIsiRpCommandValid()){            
            MainPanelDelegate.setIsiNfCommand(true);
            MainPanelDelegate.setIsiAppendMode(false);
            MainPanelDelegate.setIsiNfProgramArguments(command.replaceFirst("-NF", "").trim());
            if (hasExeCommand && hasNfCommand){
            	MainPanelDelegate.setupErrorISI("03");
             	String programName = "";
        		if (MainPanelDelegate.getIsiNfProgramArguments() != null)
        			programName = MainPanelDelegate.getIsiNfProgramArguments().trim();
        		
        		if (programName.length() > 0 && !(programName.startsWith("-NF"))) {
	  	  			MainPanelDelegate.executeIsiProgram(programName);
        		}
        		
            	MainPanelDelegate.setIsiNfCommand(false);
            	MainPanelDelegate.setIsiNfProgramArguments("");
          	  	MainPanelDelegate.setIsiTransactionInProgress(false);
          	  	MainPanelDelegate.setIsiExeCommand(false);
          	  	MainPanelDelegate.setIsiExeProgramArguments("");
          	  	MainPanelDelegate.setIsiRpCommandValid(false);
            }     
            
           else  if (!MainPanelDelegate.isValidateNfLaunchPage()){
            	MainPanelDelegate.setupErrorISI("02");
             	String programName = "";
        		if (MainPanelDelegate.getIsiNfProgramArguments() != null)
        			programName = MainPanelDelegate.getIsiNfProgramArguments().trim();
        		
        		if (programName.length() > 0 && !(programName.startsWith("-NF"))) {
	  	  			MainPanelDelegate.executeIsiProgram(programName);
        		}
        		
            	MainPanelDelegate.setIsiNfCommand(false);
            	MainPanelDelegate.setIsiNfProgramArguments("");
          	  	MainPanelDelegate.setIsiTransactionInProgress(false);
          	  	MainPanelDelegate.setIsiExeCommand(false);
          	  	MainPanelDelegate.setIsiExeProgramArguments("");
          	  	MainPanelDelegate.setIsiRpCommandValid(false);
            }      
            
        }
    }


    /**
     * Compare the word after the -EXE command to see if it is a KEYWORD
     * The valid KEYWORDS are:
     * 		MGS - MoneyGram Send
     * 		EXP - Bill Payment	
     * 		MOD	- MoneyOrder
     * 		MGR - MoneyGram Receive
     * 		RLD - MoneyGram Prepaid Card Reload
     * 		PUR - MoneyGram Prepaid Card Purchase
     * 		VPD - Vendor Payment	
     * 		MGQ - Transaction Quote
     * 		DOB - Directory of Billers
     * 		REV - Transaction Reversal
     *      AMD - Amend Transaction
     *      RRT - Receive Reversal Transaction
     * 		
     *      -EXE program.exe <keyword> <arg1> <arg2> <arg3> ...
     * @param data The command line arguments passed from the C program DWISI.EXE
     */
    private String getCodeWord(String data){
    	String retVal = "";
    	
    	if (data.length() > 0){
    		if (data.startsWith(MainPanelDelegate.clMoneyGramSend))
    			retVal = MainPanelDelegate.clMoneyGramSend;
    		else if (data.startsWith(MainPanelDelegate.clBillPayment)) 
    			retVal = MainPanelDelegate.clBillPayment;
    		else if (data.startsWith(MainPanelDelegate.clMoneyOrder)) 
    			retVal = MainPanelDelegate.clMoneyOrder;
    		else if (data.startsWith(MainPanelDelegate.clMoneyGramReceive)) 
    			retVal = MainPanelDelegate.clMoneyGramReceive;
    		else if (data.startsWith(MainPanelDelegate.clCardReload)) 
    			retVal = MainPanelDelegate.clCardReload;
    		else if (data.startsWith(MainPanelDelegate.clCardPurchase)) 
    			retVal = MainPanelDelegate.clCardPurchase;
    		else if (data.startsWith(MainPanelDelegate.clVendorPayment)) 
    			retVal = MainPanelDelegate.clVendorPayment;
    		else if (data.startsWith(MainPanelDelegate.clTranQuote)) 
    			retVal = MainPanelDelegate.clTranQuote;
    		else if (data.startsWith(MainPanelDelegate.clDirectoryBillers)) 
    			retVal = MainPanelDelegate.clDirectoryBillers;
    		else if (data.startsWith(MainPanelDelegate.clMoneyGramSendReversal)) 
    			retVal = MainPanelDelegate.clMoneyGramSendReversal;
    		else if (data.startsWith(MainPanelDelegate.clMoneyGramReceiveReversal)) 
    			retVal = MainPanelDelegate.clMoneyGramReceiveReversal;
    		else if (data.startsWith(MainPanelDelegate.clAmend)) 
    			retVal = MainPanelDelegate.clAmend;
    		else if (data.startsWith(MainPanelDelegate.clFormFreeSend)) 
    			retVal = MainPanelDelegate.clFormFreeSend;
    		else if (data.startsWith(MainPanelDelegate.clAccountDeposit)) 
    			retVal = MainPanelDelegate.clAccountDeposit;
    	}
        return retVal;
     }
   
    private boolean doesCodeWordRequireLogin(String type){
    	int product = 0;
    	boolean retVal = false;
    	
    	if (type.length() > 0){
    		if (type.equals(MainPanelDelegate.clMoneyGramSend))
    			product = 3;
    		else if (type.equals(MainPanelDelegate.clBillPayment)) 
    			product = 5;
    		else if (type.equals(MainPanelDelegate.clMoneyOrder)) 
    			product = 1;
    		else if (type.equals(MainPanelDelegate.clMoneyGramReceive)) 
    			product = 4;
    		else if (type.equals(MainPanelDelegate.clCardReload)) 
    			product = 97;
    		else if (type.equals(MainPanelDelegate.clCardPurchase)) 
    			product = 96;
    		else if (type.equals(MainPanelDelegate.clVendorPayment)) 
    			product = 2;
    		else if (type.equals(MainPanelDelegate.clTranQuote)) 
    			product = 3;
    		else if (type.equals(MainPanelDelegate.clDirectoryBillers)) 
    			product = 5;
    		else if (type.equals(MainPanelDelegate.clMoneyGramSendReversal)) 
    			product = 100;
    		else if (type.equals(MainPanelDelegate.clMoneyGramReceiveReversal)) 
    			product = 101;
    		else if (type.equals(MainPanelDelegate.clAmend)) 
    			product = 102;
    		else if (type.equals(MainPanelDelegate.clFormFreeSend)) 
    			product = 3;
    		else if (type.equals(MainPanelDelegate.clAccountDeposit)) 
    			product = 3;
    	}
    	
    	if (product > 0){
    		if (product == 100){
    			if (DeltaworksMainPanel.getMainPanel().getTranAuth(3) 
    					&& "Y".equals(UnitProfile.getInstance().get("ALLOW_SEND_CANCELS", "N")) ){
    	        	ProfileItem p = UnitProfile.getInstance().getProfileInstance().get("PRODUCT[3]/NAME_PIN_REQUIRED");
    	        	if (p.stringValue().equals("N"))
    	        		retVal = false;
    	        	else
    	        		retVal = true;    				
    			}
    		}
    		else if (product == 101){
    			if (DeltaworksMainPanel.getMainPanel().getTranAuth(4) 
    					&& "Y".equals(UnitProfile.getInstance().get("ALLOW_RECEIVE_REVERSALS", "N")) ){
    	        	ProfileItem p = UnitProfile.getInstance().getProfileInstance().get("PRODUCT[4]/NAME_PIN_REQUIRED");
    	        	if (p.stringValue().equals("N"))
    	        		retVal = false;
    	        	else
    	        		retVal = true;    				
    			}
    		}
    		else if (product == 102){
    			if (DeltaworksMainPanel.getMainPanel().getTranAuth(3) 
    					&& "Y".equals(UnitProfile.getInstance().get("ALLOW_AMENDS", "N")) ){
    	        	ProfileItem p = UnitProfile.getInstance().getProfileInstance().get("PRODUCT[3]/NAME_PIN_REQUIRED");
    	        	if (p.stringValue().equals("N"))
    	        		retVal = false;
    	        	else
    	        		retVal = true;    				
    			}
    		}
    		// check if product is authorized
    		else if (DeltaworksMainPanel.getMainPanel().getTranAuth(product)){    		
	        	ProfileItem p = UnitProfile.getInstance().getProfileInstance().get("PRODUCT["+product+"]/NAME_PIN_REQUIRED");
	        	if (p.stringValue().equals("N"))
	        		retVal = false;
	        	else
	        		retVal = true;
    		} else
    			retVal = false;
    	}
    	
        return retVal;
     }

    /**
     * Invalid ISI command line functionality.  This allows for the recording of
     * all invalid command line arguments that are passed to the ISI parser.
     * @param command - Contains the invalid ISI command line supplied to DeltaWorks.
     */
    protected void unknown(String command) {
        Debug.println ("INVALID: ISI command line: " + command);
    }


}
        
