/* //////////////////////////////////////////////////////////////////////////

    System:     In Store Integration (ISI) Simulator
    Module:     Implementaiton fo the CAsyncClient Class

    Author:     Chris Macgowan
    Date:       19 December 2002
    Version:    1.00
    Filename:   AsyncClient.cpp


    System Description:

    The In Store Integration (ISI) Simulator is used to demonstrate command
    data format, communincation and other parameters between the DNet Terminal
    and a PC based client.  MoneyGram provides a development kit with
    command structure and communincation parameters to customers that would
    like to integrate DNet functionality into thier POS software.  This ISI
    Simulator Application is provided as an aid for software developers.

    Module Description: 

    Interface for the CAsyncClient Class
    

    Source Maintanice History 
    Date         Author
    ---------------------------------------------------------------------------
    27 Mar 2003  Macgowan 
    Converting the CAsyncClient Class to C Language. 

    19 Dec 2002  Macgowan 
    Today we begin.  We have begun with AsyncClientWnd from the AsyncClient
    project.  We will attempt to create the application without CWnd. 

////////////////////////////////////////////////////////////////////////// */



// AsyncClientWnd.h - Declares the window and other things the basic
//      async I/O Winsock client uses.

#if !defined _ASYNC_CLIENT_H_
#define _ASYNC_CLIENT_H_


#include <winsock2.h>       // support the WSA Event Methods

const int   IPCM_WS_LOOKUP       = WM_USER+1; 
const int   IPCM_WS_NOTIFICATION = WM_USER+2; 



// These are the attributes that we win the CAsyncClient Class
// We have moved these to the sglobals.h and externs.h

// This timeout if used to expire a socket transaction with 
// no response.  
// int m_nWSTimerID;
// int m_nWSTimeout;		// milliseconds

// union 
// {
//     hostent he_;
//     char acBuffer_[MAXGETHOSTSTRUCT];
// } LookupResult_;


// bool bIgnoreStart_;
// sockaddr_in sin_;
// SOCKET sd_;
// WSAEVENT m_hEvents[2];
// HWND hWnd;



// Prototypes

// CAsyncClient();
// virtual ~CAsyncClient();

// Connect to server
bool    ACStart(const char* addr, int port);

// Close the network connection.  Return true if we can do it
// immediately or if the conn is already down.  Return false if
// we have to wait for the conn to come down: we'll send nMessage
// to pNotifyWnd when the conn comes down.
// virtual bool Stop(CWnd* pNotifyWnd, int nMessage);
// bool    ACStop(int nMessage);
void    ACCloseSocket();
int     ACSendMessage(char *szMessage);
int     ACSendChar(char nChar); 
char*   ACGetMessage(void); 

// void    SetClientSocket(SOCKET sData) {sd_ = sData;}
// SOCKET  GetClientSocket(void) {return sd_;}

bool ACLookupAddress(const char* pcHost);
bool ACEstablishConnection();


#endif // _ASYNC_CLIENT_H_
