// DWISI.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <winsock2.h>
#include <io.h>
#include <time.h>
#include <process.h>
#include <sys/types.h>
#include <sys/timeb.h>
#include <string.h>


// Set the receive buffer size.  
const int nBufferSize = 1024;

// define LookupResult_ to support a call to WSAAsyncGetHostByName() to 
// resolve the host name using DNS
union 
{
    hostent he_;
    char acBuffer_[MAXGETHOSTSTRUCT];
} LookupResult_;

int m_nWSTimerID;
int m_nWSTimeout;		// milliseconds

bool bIgnoreStart_;
sockaddr_in sin_;
SOCKET sd_;
WSAEVENT m_hEvents[2];
HWND hWnd;

// These variabled are used to contain the host name (or ip address) 
// and the port number
char g_pcHost[256];
int   g_nPort; 

// These are the default tcp/ip port number and the defualt log 
// level.  These are valued from the registry.  If they are not defined 
// in the registry then we use the defaults defined in the define.h 
// file.  These constants support tcp/ip communication
int  g_nDefaultPortNumber; 

char g_szIPAddDisplay[256];

// These are the variables that we added to support TCP/IP 
DWORD g_nThreadid = 0; 
int   g_nCommMode; 

const int   IPCM_WS_LOOKUP       = WM_USER+1; 
const int   IPCM_WS_NOTIFICATION = WM_USER+2; 

char* portFile = "port.txt";
char* exeBatchFile = "isiexe.bat";

int socketError = 1;

/* ////////////////////////////////////////////////////////////////////////////

    Function:       ClearExit()
    
    Description:    This will clear the standard output and exit the
	                application with the supplied exit code.  See the
					description of the main() function to see what 
					each exit code represents.
                    


    Parameters:     exitCode the code to return back to the calling program. 
    Return:         None

//////////////////////////////////////////////////////////////////////////// */
void ClearExit(int exitCode)
{
	system("CLS");
    
	exit(exitCode);
}


/* ////////////////////////////////////////////////////////////////////////////

    Function:       getPort()
    
    Description:    This function will get the port value that DeltaWorks 
	                is using for ISI.

    Parameters:     None 
    Return:         int port value

//////////////////////////////////////////////////////////////////////////// */
int getPort()
{
	int port = 90;
	char data[20];
	FILE *fp;

	// Open for read
	if( (fp  = fopen( portFile, "r" )) != NULL ){
		if (fgets(data, 20, fp) != NULL) {
			port = atoi(data);
			printf(" The port from file port.txt is %d\n", port);
		} 

		/* Close stream */
		fclose( fp );
	}
	

   return port;
}



/* //////////////////////////////////////////////////////////////////////////

    Method:         CloseSocket()

    Description:    Close the socket and mark it as invalid.  Only to be 
                    called from within the class.

    Parameters:     None
    Return:         None

////////////////////////////////////////////////////////////////////////// */
void ACCloseSocket()
{
	if (sd_ != INVALID_SOCKET) 
    {
		closesocket(sd_);
		sd_ = INVALID_SOCKET;
	}
}



/* //////////////////////////////////////////////////////////////////////////

    Method:         LookupAddress()

    Description:    Given an address string, determine if it's a dotted-quad
                    IP address or a domain address.  If the latter, ask 
                    DNS to resolve it.  If we can resolve the address 
                    immediately, we return true, else we return false. 
                    A message will come along later when Winsock finishes
                    resolving the address.
    
    Parameters:     pcHost      Server ip address or domain address
    Return:         bool        true if we resolve the address
                                false if we can not resolve the address

////////////////////////////////////////////////////////////////////////// */
bool ACLookupAddress(const char* pcHost)
{
    bool bReturn = FALSE; 
    int  nIPAddr1 = 0; 
    int  nIPAddr2 = 0; 
    int  nIPAddr3 = 0; 
    int  nIPAddr4 = 0; 

    struct hostent* pH;


	u_long nRemoteAddr = inet_addr(pcHost);
	if (nRemoteAddr != INADDR_NONE) 
    {
		sin_.sin_addr.s_addr = nRemoteAddr;

        printf("Got address using IP\n");
		bReturn = TRUE; 
	}
	else 
    {
		// pcHost isn't a dotted IP, so resolve it through DNS
        printf("Resolving IP address from host name\n");

        // This is the blocking version ... testing                         
        if (pH = gethostbyname(pcHost))
        {

            sin_.sin_addr = *((in_addr *)pH->h_addr); //Set IP

            // Put the ipaddress in here so we can display it in the 
            // ip configuration function 

            nIPAddr1 = sin_.sin_addr.S_un.S_un_b.s_b1;
            nIPAddr2 = sin_.sin_addr.S_un.S_un_b.s_b2;
            nIPAddr3 = sin_.sin_addr.S_un.S_un_b.s_b3;
            nIPAddr4 = sin_.sin_addr.S_un.S_un_b.s_b4;

            sprintf(g_szIPAddDisplay, "%d.%d.%d.%d", nIPAddr1, nIPAddr2, nIPAddr3, nIPAddr4);

            // Returned successful.  This does not imply that we have the 
            // address yet.  When WSAAsyncGetHostByName is done getting 
            // the address from the host name it will post a message to 
            // the window.  We can get this message 

			m_nWSTimerID = SetTimer(NULL, NULL, m_nWSTimeout, 0);
            printf("gethostbyname() completed successfully\n");

            //printf("Got IP address for %s IP:%s\n", pcHost, g_szIPAddDisplay);
    		bReturn = TRUE; 
		}
		else 
        {
            printf("WSAAsyncGetHostByName() failed\n");
    		bReturn = FALSE; 
			socketError = 1;

			//ClearExit(-2);
			
        }
	}

    return bReturn; 
}



/* //////////////////////////////////////////////////////////////////////////

    Method:         Threadproc()

    Description:    This thread will be used to mamaneg the network events

    Parameters:     None
    Return:         None

////////////////////////////////////////////////////////////////////////// */
DWORD WINAPI Threadproc(LPVOID lpParameter)
{
    // We will handle the network events in here

    bool bEventLoop = true;
    int nRet = 0; 
    int EventRet = 0; 
    int EventError = 0; 

    WSANETWORKEVENTS wsaEvent;

    while (bEventLoop)
    {
        // Test for Network events
        // We might want to put this in a thread if we are waiting !! 
        // DWORD retVal = WaitForMultipleObjects(2,hEvents,FALSE,INFINITE);

        printf("Waiting for an event\n");

        DWORD lEventFlagged = WSAWaitForMultipleEvents(2,               // total events
                                                       m_hEvents,       // event array    
                                                       FALSE,           // return on any event    
                                                       WSA_INFINITE,    // timeout disabled
                                                       FALSE);          // disable completation routine
        switch(lEventFlagged)
        {
            case WAIT_OBJECT_0: // Network Event
        
                nRet = WSAEnumNetworkEvents(sd_,
                                            m_hEvents[0],
                                            &wsaEvent); 

                if (nRet == SOCKET_ERROR) 
                {
                    printf("WSAEnumNetworkEvents() failed\n");
                    bEventLoop = false;
                    break;
                }

                // Test for error codes
                EventError = wsaEvent.iErrorCode[FD_READ_BIT];
                if (EventError != 0)
                { 
                    printf("Read event error occured (iErrorCode): %d\n", EventError);
                }

                EventError = wsaEvent.iErrorCode[FD_WRITE_BIT];
                if (EventError != 0)
                { 
                    printf("Write event error occured (iErrorCode): %d\n", EventError);
                }

                // Process the events
                EventRet = wsaEvent.lNetworkEvents;
                switch(EventRet)
                {
                    case FD_ACCEPT:
                        printf("Event received: Accept\n");
                        break;
                                    
                    case FD_READ:

                        // This event will happen when there is data on the socket 
                        // to read.  We will call GetMessageTCPIPComm() to 
                        // process the data. 
                        printf("Event received: Read\n");
                        break;
        
                    case FD_CLOSE:
                        printf("Event received: Close\n");
						socketError = 1;
                        break;
        
                    case FD_CONNECT:
                        printf("Event received: Connect\n");
						socketError = 1;
                        break;
        
                    case FD_WRITE:
                        printf("Event received: Write\n");
						socketError = 1;
                        break;
        
                    case FD_OOB:
                        printf("Event received: Out of band data (OOB)\n");
                        break;
        
                    case FD_QOS:
                        printf("Event received: QOS\n");
                        break;
        
                    case FD_GROUP_QOS:
                        printf("Event received: Group QOS\n");
                        break;
        
                    case FD_ADDRESS_LIST_CHANGE:
                        printf("Event received: FD_ADDRESS_LIST_CHANGE\n");
                        break;
        
                    case FD_ROUTING_INTERFACE_CHANGE:
                        printf("Event received: FD_ROUTING_INTERFACE_CHANGE\n");
						socketError = 1;
                        break; 
        
                    default:
                        printf("Event received: not defined: 0x%X\n", EventRet);
						socketError = 0;
                        break;
                }
                break;
        
            case WAIT_OBJECT_0+1: // Stop Event
                printf("Event received: Stop\n");

                lEventFlagged = false;
                ResetEvent(m_hEvents[1]);
                break;
                
            default:
                break;
        }
    }
    return 0;
}



/* //////////////////////////////////////////////////////////////////////////

    Method:         EstablishConnection()

    Description:    The server address has been resolved, we will connect 
                    to the server. 
    
    Parameters:     None
    Return:         bool        true  Connection successful
                                false Connection attempt failed

////////////////////////////////////////////////////////////////////////// */
bool ACEstablishConnection()
{
	// Create a stream socket
    int nReturn = 0; 
    int nLastError = 0; 


	sd_ = socket(AF_INET, SOCK_STREAM, 0);
	if (sd_ == INVALID_SOCKET) 
    {
        printf("Failed to create the socket\n");
		return false;
	}

    // Create the events
    m_hEvents[0] = WSACreateEvent();
    m_hEvents[1] = WSACreateEvent();

    // Create an event to be used
    nReturn = WSAEventSelect(sd_, m_hEvents[0], FD_READ | FD_WRITE | FD_CONNECT | FD_CLOSE);


    if (nReturn == SOCKET_ERROR) 
    { 
        nLastError = WSAGetLastError(); 
        printf("WSAEventSelect failed.  Error code = %d\n", nLastError);

        switch(nLastError)
        { 
            case WSANOTINITIALISED:
                printf("WSAEventSelect failed.  WSANOTINITIALISED\n");
                break; 

            case WSAENETDOWN:
                printf("WSAEventSelect failed.  WSAENETDOWN\n");
                break; 

            case WSAEINVAL:
                printf("WSAEventSelect failed.  WSAEINVAL\n");
                break; 

            case WSAEINPROGRESS:
                printf("WSAEventSelect failed.  WSAEINPROGRESS\n");
                break; 

            case WSAENOTSOCK:
                printf("WSAEventSelect failed.  WSAENOTSOCK\n");
                break; 

            default:
                printf("WSAEventSelect failed.  Unknown\n");
                break; 
        }
		//exit (-2);
	    socketError = 1;

		return false;
    }


	// Begin the connection attempt.  This will almost certainly
	// not complete immediately.
	if (connect(sd_, (sockaddr*)&sin_, sizeof(sockaddr_in)) == SOCKET_ERROR) 
    {
        // Error has occured
		int nError = WSAGetLastError();

		if (nError == WSAEWOULDBLOCK) 
        {
            printf("Asynchronous connection attempt started\n");

			m_nWSTimerID = SetTimer(NULL, NULL, m_nWSTimeout, 0);
    		PostMessage(NULL, IPCM_WS_NOTIFICATION, sd_, MAKELPARAM(FD_CONNECT, 0));
		}
		else 
        {
			ACCloseSocket();
            printf("Asynchronous connection failed\n");
			socketError = 1;
			//ClearExit(-2);
			return false;
		}
	}
	else 
    {
		// We'll have to fake an FD_CONNECT message to ourselves.
        printf("Asynchronous connect immediately without blocking\n");
		PostMessage(NULL, IPCM_WS_NOTIFICATION, sd_, MAKELPARAM(FD_CONNECT, 0));
	}

	return true;
}



/* //////////////////////////////////////////////////////////////////////////

    Method:         ACStart()

    Description:    Connect to the server
    
    Parameters:     addr        Ip address of the server 
                    port        Port number of the server

    Return:         bool        true / false

////////////////////////////////////////////////////////////////////////// */
bool ACStart(const char* addr, int port)
{
	memset(LookupResult_.acBuffer_, 0, sizeof(LookupResult_.acBuffer_));

	sin_.sin_family = AF_INET;
	sin_.sin_port = 0;
	sin_.sin_addr.s_addr = INADDR_NONE;
    // Intiialize IP Stack

    m_nWSTimerID = 42;
    m_nWSTimeout = 3000;		// milliseconds


    //sAddress_ = addr;
	sin_.sin_port = htons(port);
	if (ACLookupAddress(addr)) 
    {
	    // Lookup happened immediately, so bring up the connection.
		ACEstablishConnection();
	}
    return true;         
}



/* ////////////////////////////////////////////////////////////////////////////

    Function:       ACInitializeSocket()
    
    Description:    This function will initialize the TCP/IP communication

    Parameters:     None 
    Return:         None

//////////////////////////////////////////////////////////////////////////// */
int ACInitializeSocket(char *szHost)
{

    int             nCode;
    bool            bMessageLoop = true; 

    // These timers will control the program 
    unsigned int    nTimerId1 = 0; 
    int             nTimerCount1 = 0; 
	int port = getPort();


    MSG     *pMsg = NULL; 
    pMsg = new MSG; 

    HWND hWnd;


	printf("Starting initialize.....\n");
    // We need the window handle to use the WSAAsyncSelect function.  We will 
    // get the window handle of the console using the following code. 
    TCHAR pszWindowTitle[1000]; 
    GetConsoleTitle(pszWindowTitle, 1000); 
    hWnd = FindWindow(NULL, pszWindowTitle); 

    // Start Winsock up
    WSAData wsaData;
    if ((nCode = WSAStartup(MAKEWORD(2, 2), &wsaData)) != 0) 
    {
        printf("WSAStartup() returned error code: %d\n");
		//ClearExit(-2);
		socketError = 1;
    }

    printf("Start ip client\n");

    printf("Server:  ip:%s  port:%d\n", szHost, port);

    // start the winsock object using the CAsyncClient Class
    ACStart(szHost, port); 

    // Start the threat that will handle the events
    HANDLE Threadhandle;
    DWORD Threadid;

    g_nThreadid = GetCurrentThreadId();

    Threadhandle = CreateThread(NULL, 0, Threadproc, NULL, 0, &Threadid);

    // sleep some while thread initialize
    Sleep(1000);

    return 1; 
} 



/* //////////////////////////////////////////////////////////////////////////

    Method:         SendChar()

    Description:    Sends a single character to the server.  We will take 
                    the single char and set the first byte off the 8 
                    byte buffer. 

    Parameters:     nChar           Character to send
    Return:         int             1 for success 0 for fail

////////////////////////////////////////////////////////////////////////// */
int ACSendChar(char nChar)
{
    int nReturn = 0; 
	char szMessage[8];

	
	printf("in ACSendChar sending %c\n", nChar);  // Do not remove this print line
    // Clear and set the first byte of the message to the char
    memset(szMessage, 0x00, sizeof(szMessage)); 
    szMessage[0] = nChar; 

    // send the data on the socket
	if (send(sd_, szMessage, 1, 0) != SOCKET_ERROR) 
    {
        m_nWSTimerID = SetTimer(NULL, NULL, m_nWSTimeout, 0);
        nReturn = 1; 
	}

    return nReturn; 
}




/* ////////////////////////////////////////////////////////////////////////////

    Function:       SendMessage()
    
    Description:    This function will send command passed to this function 
                    to the terminal.  The message needs to have a Carage Return
					and Line Feed added to the end of the message so that the Java
					command line BufferedReader.readLine() will work otherwise the 
					Java command will throw an Exception.
                    


    Parameters:     None 
    Return:         None

//////////////////////////////////////////////////////////////////////////// */
int SendMessage(char *szMessage)
{
    unsigned char lrc = 0;
    

    while (*szMessage)
    {
         printf("%c",*szMessage);
		 ACSendChar(*szMessage);
         lrc ^= *szMessage++;
    }

	ACSendChar ('\r');	
	ACSendChar ('\n');	

	printf("<\n");
    return 1; 
}

/* ////////////////////////////////////////////////////////////////////////////

    Function:       writeDnsRecord()
    
    Description:    This function appends the "Did Not Start" record to 
	                the dwisi.csv file.  The timestamp is converted to the
					standard DeltaWorks ISI timestamps.
                    


    Parameters:     None 
    Return:         None

//////////////////////////////////////////////////////////////////////////// */
void  writeDnsRecord()
{
	FILE *fp;
	char timebuf[20];

	char tmpbuf[20];

	_strdate(tmpbuf);

	if (strlen(tmpbuf) > 7){
		strcpy(timebuf, "20");
		strncat(timebuf, &tmpbuf[6], 2);
		strncat(timebuf, &tmpbuf[3], 2);
		strncat(timebuf, tmpbuf, 2);
	} else
		strcpy(timebuf, tmpbuf);

	_strtime(tmpbuf);
	strcat(timebuf, tmpbuf);

	if( (fp  = fopen( "dwisi.csv", "w" )) != NULL ){
		fprintf(fp, "\"DNS\",\"%s\"\n", timebuf);
		fclose( fp );
	}
}

/* ////////////////////////////////////////////////////////////////////////////

    Function:       waitForDeltaWorksToStart()
    
    Description:    This function starts DeltaWorks and waits for the port.txt file
	                to be created.
                    


    Parameters:     None 
    Return:         TRUE if DeltaWorks started
	                FALSE if DeltaWorks doesn't start

//////////////////////////////////////////////////////////////////////////// */
bool  waitForDeltaWorksToStart(char * exeCommandLine)
{
	struct _finddata_t c_file;
	long hFile;
	bool retVal = FALSE;
	int SLEEP_TIME = 250;  // 1000 miliseconds
	int START_ERROR_TIME = 120000;   // 2 minutes
	int timesToLoop = int(START_ERROR_TIME/SLEEP_TIME);
	int cnt = 0;
	FILE *fp;

	system("CLS"); 

	//remove the files
	remove( portFile );
	remove( exeBatchFile);

	// Spawn a process to run DeltaWorks without waiting for it to return.
	_spawnl(_P_NOWAIT, "deltaworks.bat", "deltaworks.bat", "_spawnl", "two", NULL);
	
	while (true){
		// Find the port.txt file in the current directory
		if ((hFile = _findfirst(portFile, &c_file)) == -1L){
			if (cnt > timesToLoop){
				// Write the Did Not Start record to the csv file and exit.
				writeDnsRecord();

				if (strlen(exeCommandLine) > 1){
					//create the batch file.
					if( (fp  = fopen( exeBatchFile, "w" )) != NULL ){
						fprintf(fp, "%s\n", exeCommandLine);

						fclose( fp );
					}
				
					// run -EXE command line arguments.
					_spawnl(_P_NOWAIT, exeBatchFile, exeBatchFile, "_spawnl", "two", NULL);
				}

				ClearExit(-4);

			}
			cnt++;
			Sleep(SLEEP_TIME);
		} else { 
			retVal = TRUE;
			_findclose(hFile);
			Sleep(300);
			break;
		}
	}

	return retVal; 
}



/* ////////////////////////////////////////////////////////////////////////////

    Function:       main()
    
    Description:    Send the program arguments to the DeltaWorks java application
	                which parses and validates the program arguments.
               

    Parameters:     None 
    Return:          0 = Successful
					-1 = Failure - Problem openning or reading the port file (port.txt)
					-2 = Failure - Unable to connect to the port
					-3 = Failure - Unable to write data to the port (NO LONGER USED)
					-4 = Failure - Unable to start DeltaWorks in 2 minutes.

//////////////////////////////////////////////////////////////////////////// */
int main(int argc, char* argv[])
{
	int i;
	char commandLine[1024] =" ";
	char exeCommandLine[1024] = " ";
	int exeCommandLocation = 1000;

	if (argc > 1) { 
		for (i=1; i < argc; i++) {
			strcat(commandLine, argv[i]);
			strcat (commandLine, " ");
		}

		// store the exe command line to execute if delteworks
		// is unable to start
		for (i=1; i < argc; i++) {
			if (!strcmp(argv[i], "-EXE"))
				exeCommandLocation = i;
			
			if (i > exeCommandLocation) {
				if (!strcmp(argv[i], "-FRONT") ||
					!strcmp(argv[i], "-BACK") ||
					!strcmp(argv[i], "-RP") )
				{
					exeCommandLocation = 1000;
				} else {
					strcat(exeCommandLine, argv[i]);
					strcat(exeCommandLine, " ");
				}
			}
		}
		
		while(true){
			ACInitializeSocket("localhost");

			SendMessage(commandLine);

			if (socketError){
				waitForDeltaWorksToStart(exeCommandLine);
				socketError = false;
			}
			else
				break;
		}

		ACCloseSocket();

		ClearExit(0);

	}
	return 0;
}
 