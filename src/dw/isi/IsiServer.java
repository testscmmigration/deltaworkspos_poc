package dw.isi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

import dw.utility.Debug;
import dw.utility.ExtraDebug;

/**
 *  Implements a ISI server. 
 * */
public class IsiServer implements Runnable {

    /**
     * Creates a server to listen on the given port.
     */
    public IsiServer() {
    }

    /** 
     * Listens for incoming connections. When a new one comes in,
     * starts a thread to handle the connection.
     */
    @Override
	public void run() {
        ServerSocket serverSocket;
        Socket clientSocket;
        Thread t;
        try {
            serverSocket = new ServerSocket();
            serverSocket.bind(null);
            serverSocket.getLocalPort();
            writePortFile(serverSocket.getLocalPort());
            while (true) {
                clientSocket = serverSocket.accept();
                InetSocketAddress localHost = new InetSocketAddress("localhost", 0);
                
                ExtraDebug.println("ISI clientSocket Host:"+clientSocket.getInetAddress().getHostAddress());
                ExtraDebug.println("ISI localHost Host:"+localHost.getAddress().getHostAddress());
                
                /*
                 * Only process the input if it came from the same PC that
                 * DW is running on.
                 */
                if (clientSocket.getLocalAddress().getHostAddress().equals(localHost.getAddress().getHostAddress())) {
                	ExtraDebug.println("ISI COnnected ");
                    t = new Thread(new IsiConnection(clientSocket));
                    t.start();
                } else {
                	ExtraDebug.println("Closing clientSocket");
                	clientSocket.close();
                }
            }
        }
        catch (IOException e) {
            Debug.println("IsiServer: " + e);	
        }
    }
    
    
    /**
     * This function writes the port the current ISI server is using
     * to a text file PORT.TXT.  This allows the C program client to
     * know what port number to use to connect to the server.
     * @param port - The port number that the ISI Server is using.
     */
    private void writePortFile(int port){
        File file;
        FileOutputStream fos = null;
        String fileName = "port.txt";
        String data = String.valueOf(port);
        file = new File(fileName);
        try {
            fos = new FileOutputStream(file, false);
        } catch (FileNotFoundException e) {
            Debug.printStackTrace(e);
        }
        for (int i=0; i < data.length(); i++){
            try {
                fos.write(data.charAt(i));
            } catch (IOException e1){
                Debug.printStackTrace(e1);
            }
        }
        try {
        	if (fos != null) {
        		fos.close();
        	}
        } catch (IOException e2) {
			Debug.printStackTrace(e2);
        }
    }    
}
