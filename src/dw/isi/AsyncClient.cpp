/* //////////////////////////////////////////////////////////////////////////

    System:     In Store Integration (ISI) Simulator
    Module:     Implementaiton fo the CAsyncClient Class

    Author:     Chris Macgowan
    Date:       19 December 2002
    Version:    1.00
    Filename:   AsyncClient.cpp


    System Description:

    The In Store Integration (ISI) Simulator is used to demonstrate command
    data format, communication and other parameters between the DNet Terminal
    and a PC based client.  MoneyGram provides a development kit with
    command structure and communication parameters to customers that would
    like to integrate DNet functionality into thier POS software.  This ISI
    Simulator Application is provided as an aid for software developers.

    Module Description: 

    Implementation for the CAsyncClient Class.  The CAsyncClient Class will 
    provide Asycrounous Sockets communication support using Windows Sockets. 
    

    Source Maintanice History 
    Date         Author
    ---------------------------------------------------------------------------
    27 Mar 2003  Macgowan 
    Converting the CAsyncClient Class to C Language. 

    19 Dec 2002  Macgowan 
    Today we begin.  We have begun with AsyncClientWnd from the AsyncClient
    project.  We will attempt to create the application without CWnd. 


////////////////////////////////////////////////////////////////////////// */


#include "stdafx.h"

#include <iostream>
#include <string.h>

//#include <winsock2.h>       // support the WSA Event Methods
//#include "MessageLog.h"     // support message logging

//#include "defines.h"
//#include "externs.h"

#include "AsyncClient.h"



// Set the receive buffer size.  
const int nBufferSize = 1024;

// define LookupResult_ to support a call to WSAAsyncGetHostByName() to 
// resolve the host name using DNS
union 
{
    hostent he_;
    char acBuffer_[MAXGETHOSTSTRUCT];
} LookupResult_;


bool ACStart(const char* addr, int port)
/* //////////////////////////////////////////////////////////////////////////

    Method:         Start()

    Description:    Connect to the server
    
    Parameters:     addr        Ip address of the server 
                    port        Port number of the server

    Return:         bool        true / false

////////////////////////////////////////////////////////////////////////// */
{
	memset(LookupResult_.acBuffer_, 0, sizeof(LookupResult_.acBuffer_));

	sin_.sin_family = AF_INET;
	sin_.sin_port = 0;
	sin_.sin_addr.s_addr = INADDR_NONE;
    // Intiialize IP Stack

    m_nWSTimerID = 42;
    m_nWSTimeout = 3000;		// milliseconds

    // We are going to test our logging here 
    WriteMessageLog(g_nLog, "Start winsock client", MESSAGE_LEVEL_INFO);

    //sAddress_ = addr;
	sin_.sin_port = htons(port);
	if (ACLookupAddress(addr)) 
    {
	    // Lookup happened immediately, so bring up the connection.
		ACEstablishConnection();
	}
    return true;         
}




bool ACStart(const char* addr, int port)
/* //////////////////////////////////////////////////////////////////////////

    Method:         Start()

    Description:    Connect to the server
    
    Parameters:     addr        Ip address of the server 
                    port        Port number of the server

    Return:         bool        true / false

////////////////////////////////////////////////////////////////////////// */
{
	memset(LookupResult_.acBuffer_, 0, sizeof(LookupResult_.acBuffer_));

	sin_.sin_family = AF_INET;
	sin_.sin_port = 0;
	sin_.sin_addr.s_addr = INADDR_NONE;
    // Intiialize IP Stack

    m_nWSTimerID = 42;
    m_nWSTimeout = 3000;		// milliseconds

    // We are going to test our logging here 
    WriteMessageLog(g_nLog, "Start winsock client", MESSAGE_LEVEL_INFO);

    //sAddress_ = addr;
	sin_.sin_port = htons(port);
	if (ACLookupAddress(addr)) 
    {
	    // Lookup happened immediately, so bring up the connection.
		ACEstablishConnection();
	}
    return true;         
}





bool ACLookupAddress(const char* pcHost)
/* //////////////////////////////////////////////////////////////////////////

    Method:         LookupAddress()

    Description:    Given an address string, determine if it's a dotted-quad
                    IP address or a domain address.  If the latter, ask 
                    DNS to resolve it.  If we can resolve the address 
                    immediately, we return true, else we return false. 
                    A message will come along later when Winsock finishes
                    resolving the address.
    
    Parameters:     pcHost      Server ip address or domain address
    Return:         bool        true if we resolve the address
                                false if we can not resolve the address

////////////////////////////////////////////////////////////////////////// */
{
    char szLogMessage[MESSAGELOG_BUFFER]; 
    bool bReturn = FALSE; 
    int  nIPAddr1 = 0; 
    int  nIPAddr2 = 0; 
    int  nIPAddr3 = 0; 
    int  nIPAddr4 = 0; 

//    hostent hten; 
    struct hostent* pH;

	u_long nRemoteAddr = inet_addr(pcHost);
	if (nRemoteAddr != INADDR_NONE) 
    {
		sin_.sin_addr.s_addr = nRemoteAddr;

        sprintf(szLogMessage, "Got address using IP");
        WriteMessageLog(g_nLog, szLogMessage, MESSAGE_LEVEL_DEBUG);
		bReturn = TRUE; 
	}
	else 
    {
		// pcHost isn't a dotted IP, so resolve it through DNS
        sprintf(szLogMessage, "Resolving IP address from host name");
        WriteMessageLog(g_nLog, szLogMessage, MESSAGE_LEVEL_DEBUG);

        // This is the blocking version ... testing                         
        if (pH = gethostbyname(pcHost))
        {

            sin_.sin_addr = *((in_addr *)pH->h_addr); //Set IP

            // Put the ipaddress in here so we can display it in the 
            // ip configuration function 

            nIPAddr1 = sin_.sin_addr.S_un.S_un_b.s_b1;
            nIPAddr2 = sin_.sin_addr.S_un.S_un_b.s_b2;
            nIPAddr3 = sin_.sin_addr.S_un.S_un_b.s_b3;
            nIPAddr4 = sin_.sin_addr.S_un.S_un_b.s_b4;

            sprintf(g_szIPAddDisplay, "%d.%d.%d.%d", nIPAddr1, nIPAddr2, nIPAddr3, nIPAddr4);

            // Returned successful.  This does not imply that we have the 
            // address yet.  When WSAAsyncGetHostByName is done getting 
            // the address from the host name it will post a message to 
            // the window.  We can get this message 

			m_nWSTimerID = SetTimer(NULL, NULL, m_nWSTimeout, 0);
            sprintf(szLogMessage, "gethostbyname() completed successfully");
            WriteMessageLog(g_nLog, szLogMessage, MESSAGE_LEVEL_DEBUG);

            sprintf(szLogMessage, "Got IP address for %s IP:%s", pcHost, g_szIPAddDisplay);
            WriteMessageLog(g_nLog, szLogMessage, MESSAGE_LEVEL_DEBUG);
    		bReturn = TRUE; 
		}
		else 
        {
            sprintf(szLogMessage, "WSAAsyncGetHostByName() failed");
            WriteMessageLog(g_nLog, szLogMessage, MESSAGE_LEVEL_DEBUG);
    		bReturn = FALSE; 
        }
	}

    return bReturn; 
}





bool ACEstablishConnection()
/* //////////////////////////////////////////////////////////////////////////

    Method:         EstablishConnection()

    Description:    The server address has been resolved, we will connect 
                    to the server. 
    
    Parameters:     None
    Return:         bool        true  Connection successful
                                false Connection attempt failed

////////////////////////////////////////////////////////////////////////// */
{
	// Create a stream socket
    int nReturn = 0; 
    int nLastError = 0; 

    char szLogMessage[MESSAGELOG_BUFFER]; 


    // Create the socket connection
    sprintf(szLogMessage, "Create client socket");
    WriteMessageLog(g_nLog, szLogMessage, MESSAGE_LEVEL_DEBUG);

	sd_ = socket(AF_INET, SOCK_STREAM, 0);
	if (sd_ == INVALID_SOCKET) 
    {
        sprintf(szLogMessage, "Failed to create the socket");
        WriteMessageLog(g_nLog, szLogMessage, MESSAGE_LEVEL_ERROR);
		return false;
	}

    // Create the events
    m_hEvents[0] = WSACreateEvent();
    m_hEvents[1] = WSACreateEvent();

    // Create an event to be used
    nReturn = WSAEventSelect(sd_, m_hEvents[0], FD_READ | FD_WRITE | FD_CONNECT | FD_CLOSE);


    if (nReturn == SOCKET_ERROR) 
    { 
        nLastError = WSAGetLastError(); 
        sprintf(szLogMessage, "WSAEventSelect failed.  Error code = %d", nLastError);
        WriteMessageLog(g_nLog, szLogMessage, MESSAGE_LEVEL_ERROR);

        switch(nLastError)
        { 
            case WSANOTINITIALISED:
                sprintf(szLogMessage, "WSAEventSelect failed.  WSANOTINITIALISED");
                break; 

            case WSAENETDOWN:
                sprintf(szLogMessage, "WSAEventSelect failed.  WSAENETDOWN");
                break; 

            case WSAEINVAL:
                sprintf(szLogMessage, "WSAEventSelect failed.  WSAEINVAL");
                break; 

            case WSAEINPROGRESS:
                sprintf(szLogMessage, "WSAEventSelect failed.  WSAEINPROGRESS");
                break; 

            case WSAENOTSOCK:
                sprintf(szLogMessage, "WSAEventSelect failed.  WSAENOTSOCK");
                break; 

            default:
                sprintf(szLogMessage, "WSAEventSelect failed.  Unknown");
                break; 
        }

        WriteMessageLog(g_nLog, szLogMessage, MESSAGE_LEVEL_ERROR);
		return false;
    }

    sprintf(szLogMessage, "WSAEventSelect successful");
    WriteMessageLog(g_nLog, szLogMessage, MESSAGE_LEVEL_DEBUG);

	// Begin the connection attempt.  This will almost certainly
	// not complete immediately.
	if (connect(sd_, (sockaddr*)&sin_, sizeof(sockaddr_in)) == SOCKET_ERROR) 
    {
        // Error has occured
		int nError = WSAGetLastError();

		if (nError == WSAEWOULDBLOCK) 
        {
            sprintf(szLogMessage, "Asynchronous connection attempt started");
            WriteMessageLog(g_nLog, szLogMessage, MESSAGE_LEVEL_DEBUG);

			m_nWSTimerID = SetTimer(NULL, NULL, m_nWSTimeout, 0);
    		PostMessage(NULL, IPCM_WS_NOTIFICATION, sd_, MAKELPARAM(FD_CONNECT, 0));
		}
		else 
        {
			ACCloseSocket();
            sprintf(szLogMessage, "Asynchronous connection failed");
            WriteMessageLog(g_nLog, szLogMessage, MESSAGE_LEVEL_ERROR);
			return false;
		}
	}
	else 
    {
		// We'll have to fake an FD_CONNECT message to ourselves.
        sprintf(szLogMessage, "Asynchronous connect immediately without blocking");
        WriteMessageLog(g_nLog, szLogMessage, MESSAGE_LEVEL_DEBUG);
		PostMessage(NULL, IPCM_WS_NOTIFICATION, sd_, MAKELPARAM(FD_CONNECT, 0));
	}

	return true;
}








int ACSendMessage(char *szMessage)
/* //////////////////////////////////////////////////////////////////////////

    Method:         SendMessage()

    Description:    Sends the passed message to the server.  
     
                    Warning: This method is smart enough to send data in 
                    multiple parts.  It assumes it can send the entire 
                    message in one pass.  We will work on this later. 

    Parameters:     szMessage       pointe to message text
    Return:         int             1 for success 0 for fail

////////////////////////////////////////////////////////////////////////// */
{
    // WSARecvfrom
	// Send the string to the server

    int nMessageLen = 0 ; 
    int nReturn = 0; 

    char szLogMessage[MESSAGELOG_BUFFER]; 

    nMessageLen = strlen(szMessage); 

	if (send(sd_, szMessage, nMessageLen, 0) != SOCKET_ERROR) 
    {
        sprintf(szLogMessage, "Message send successful");
        WriteMessageLog(g_nLog, szLogMessage, MESSAGE_LEVEL_DEBUG);

        m_nWSTimerID = SetTimer(NULL, NULL, m_nWSTimeout, 0);
        nReturn = 1; 
	}
	else 
    {
		int nError = WSAGetLastError();
		if (nError == WSAEWOULDBLOCK) 
        {
            sprintf(szLogMessage, "Message send failed, send() would block");
            WriteMessageLog(g_nLog, szLogMessage, MESSAGE_LEVEL_ERROR);
		}
		else 
        {
            sprintf(szLogMessage, "Message send failed, send() failed! ");
            WriteMessageLog(g_nLog, szLogMessage, MESSAGE_LEVEL_ERROR);
		}
        nReturn = 0; 
	}

    return nReturn; 
}







int ACSendChar(char nChar)
/* //////////////////////////////////////////////////////////////////////////

    Method:         SendChar()

    Description:    Sends a single character to the server.  We will take 
                    the single char and set the first byte off the 8 
                    byte buffer. 

    Parameters:     nChar           Character to send
    Return:         int             1 for success 0 for fail

////////////////////////////////////////////////////////////////////////// */
{
    // WSARecvfrom
	// Send the string to the server

    int nReturn = 0; 

    char szLogMessage[MESSAGELOG_BUFFER]; 
	char szMessage[8];

    // Clear and set the first byte of the message to the char
    memset(szMessage, 0x00, sizeof(szMessage)); 
    szMessage[0] = nChar; 


    // send the data on the socket
	if (send(sd_, szMessage, 1, 0) != SOCKET_ERROR) 
    {
        sprintf(szLogMessage, "Character send successful");
        WriteMessageLog(g_nLog, szLogMessage, MESSAGE_LEVEL_DEBUG);

        m_nWSTimerID = SetTimer(NULL, NULL, m_nWSTimeout, 0);
        nReturn = 1; 
	}
	else 
    {
		int nError = WSAGetLastError();
		if (nError == WSAEWOULDBLOCK) 
        {
            sprintf(szLogMessage, "Message send failed, send() would block");
            WriteMessageLog(g_nLog, szLogMessage, MESSAGE_LEVEL_ERROR);
		}
		else 
        {
            sprintf(szLogMessage, "Message send failed, send() failed! ");
            WriteMessageLog(g_nLog, szLogMessage, MESSAGE_LEVEL_ERROR);
		}
        nReturn = 0; 
	}

    return nReturn; 
}








char * ACGetMessage(void)
/* //////////////////////////////////////////////////////////////////////////

    Method:         GetMessage()

    Description:    Get data from the server. 
                    This method used recv() and will block until there is 
                    data on the socket.  

    Parameters:     None
    Return:         None

////////////////////////////////////////////////////////////////////////// */
{
	char szReadBuffer[nBufferSize];
	int nTotalBytes = 0;

    char szLogMessage[MESSAGELOG_BUFFER]; 

    memset(szReadBuffer, 0x00, nBufferSize); 

    sprintf(szLogMessage, "Get message from socket");
    WriteMessageLog(g_nLog, szLogMessage, MESSAGE_LEVEL_DEBUG);

	while (nTotalBytes < nBufferSize) 
    {
		int nNewBytes = recv(sd_,                           // socket
                             szReadBuffer + nTotalBytes,    // buffer for read data
				             nBufferSize - nTotalBytes,     // length of buffer
                             0);                            // flag

		if (nNewBytes == SOCKET_ERROR) 
        {
			int nError = WSAGetLastError();

			if (nError == WSAEWOULDBLOCK) 
            {
                // We got the data !!
                sprintf(szLogMessage, "Bytes received from server: %d", nTotalBytes);
                WriteMessageLog(g_nLog, szLogMessage, MESSAGE_LEVEL_DEBUG);
				break;
			}
			else 
            {
                *szReadBuffer = 0; 
                sprintf(szLogMessage, "GetMessage failed, recv() failed");
                WriteMessageLog(g_nLog, szLogMessage, MESSAGE_LEVEL_ERROR);
			}
		}
		else if (nNewBytes == 0) 
        {
            *szReadBuffer = 0; 
            sprintf(szLogMessage, "Connection closed by the peer");
            WriteMessageLog(g_nLog, szLogMessage, MESSAGE_LEVEL_DEBUG);
			ACCloseSocket();
		}

		nTotalBytes += nNewBytes;
	}

    return szReadBuffer; 
}








void ACCloseSocket()
/* //////////////////////////////////////////////////////////////////////////

    Method:         CloseSocket()

    Description:    Close the socket and mark it as invalid.  Only to be 
                    called from within the class.

    Parameters:     None
    Return:         None

////////////////////////////////////////////////////////////////////////// */
{
	if (sd_ != INVALID_SOCKET) 
    {
		closesocket(sd_);
		sd_ = INVALID_SOCKET;
	}
}

