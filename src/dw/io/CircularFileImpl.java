package dw.io;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;

/**
 * Persistent circular buffer used for local storage. This is a first-in,
 * first-out data structure implemented with a fixed-length file. Reads and
 * writes that extend past the end of the file transparently continue at the 
 * beginning. 
 *
 * Writes are performed using the methods inherited from OutputStream.
 * Reads are performed using the object returned by getInputStream.
 * The CircularFile slowly fills up as data is written. Calling the reset 
 * method invalidates old data, making space available.
 *
 * Within the circular file, valid data begins at a position named start,
 * and continues to the byte before the position named end. Read operations
 * begin at start, and write operations begin at end. When start == end, the
 * circular file is empty.
 * @author Geoff Atkin
 * 10/4/2000 modified by Francis Fung to change private field length to protected
 * 10/5/2000
 * added a reset method for use when synchronizing one CircularFile to another
 * removed final from length to allow reset
 * 10/11/2000 erased the changes of 10/5/2000
 * added a throw AuthenticationException if the start or end markers exceed 
 * the physical length of the underlying file.
 * 10/18/2000 changed isNearlyFull to return true when free space is less than
 * 20% of physical file length
 */
public class CircularFileImpl extends CircularFile{
    
    private static final long FULL_THRESHHOLD = 300000;

    protected SimpleRandomAccessFile file;
    protected long start;
    protected long end;
    protected final long length;
    protected StateListener listener;

    protected boolean lock = false;
    protected long lockPointer = 0;


    /** Gets the start pointer for debugging. */
    @Override
	public synchronized long getStart() {return start;}

    /** Gets the end pointed for debugging. */
    @Override
	public long getEnd() {return end;}

    /** 
     * Creates a circular file to read from and write to the given file.
     * @param file the underlying file
     * @param start the offset into the file where existing data begins
     * @param end the offset into the file where new data can be written
     * @throws IOException if the length of the file can not be determined
     * @throws AuthenticationException if the start or end markers are invalid
     */ 
    public CircularFileImpl(SimpleRandomAccessFile file, long start, long end)
            throws IOException {
        this.file = file;
        this.start = start;
        this.end = end;
        this.length = file.length();
        this.listener = null;
	if (start < 0 || end < 0 || start >= this.length || end > this.length) {
	    throw new AuthenticationException();
	}
    }
	
    /**
     * Writes a string to the file in UTF8 format.
     * @param s the string to write
     * @throws EOFException if the string length exceeds the available space
     * @throws IOException if the string can not be written for another reason
     */
    @Override
	public void write(String s) throws IOException {
        this.write(s.getBytes("UTF8")); 
    }

    /**
     * Writes a single byte to the file.
     * @param b the byte to be written
     * @throws EOFException if there is no available space
     * @throws IOException if the byte can not be written for another reason
     */
    @Override
	public void write(int b) throws IOException {
        byte[] temp = new byte[1];
        temp[0] = (byte) b;
        this.write(temp, 0, 1);
    }
    
    /**
     * Writes b.length bytes to the file.
     * @param b the array of bytes to be written
     * @throws EOFException if b.length exceeds the available space
     * @throws IOException if the array can not be written for another reason
     */
    @Override
	public void write(byte[] b) throws IOException {
        this.write(b, 0, b.length);
    }

    /**
     * Writes len bytes from the given byte array starting at offset off
     * to the file.
     * @param b the array of bytes to be written
     * @param off the start offset in the array
     * @param len the number of bytes to write
     * @throws EOFException if len exceeds the available space
     * @throws IOException if the bytes can not be written for another reason
     */
    @Override
	public synchronized void write(byte[] b, int off, int len) 
        throws IOException {

        if (len == 0) {
            return;
        }
    
        if (len > freeSpace()) {
            throw new EOFException("insufficient free space"); 
        }
    
        if (end + len < length) {
            file.write(b, off, len, end);
            end = end + len;
        }
        else {
            int midpoint = (int) (length - end);
            file.write(b, off, midpoint, end);
            file.write(b, off + midpoint, len - midpoint, 0);
            end = len - midpoint;
        }
    }

    /** 
     * Returns the amount of available space remaining in the circular file.
     * If the file is empty, the available space is one less than the length 
     * of the underlying random access file. If the file is not empty, the 
     * length is reduced by the amount of data between the start and end
     * pointers.
     */
    @Override
	public synchronized long freeSpace() {
        if (end >= start)
               return length - end + start - 1;
        else
            return start - end - 1;
    }


    /** Returns true if this circular file is empty. */
    @Override
	public boolean isEmpty() {
        return (start == end);
    }

    /** Returns true if this circular file is locked. */
    @Override
	public boolean isLocked() {
        return lock;
    }

    // Returns the threshhold used for determining if nearly full.
    @Override
	protected long getFullThreshhold() {
        return FULL_THRESHHOLD;
    }
    
    // Indicates that the available space is below the threshhold
    //10/18/2000 changed from static amount of free space to 20% free
    @Override
	public boolean isNearlyFull() {
	return ( freeSpace() < 0.2 * length);
        // return freeSpace() < getFullThreshhold();
    }
    
    /**
     * Flushes any buffers to disk. 
     */      
    @Override
	public void flush() throws IOException {
        file.flush();
        if (listener != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(16);
            DataOutputStream out = new DataOutputStream(baos);
            out.writeLong(start);
            out.writeLong(end);
            listener.stateChanged(baos.toByteArray(), 0, out.size());
            out.close();
        }
        else {
            //Debug.println("No state listener");	
        }
    }
    
    /**
     * Closes this circular file.
     */
    @Override
	public void close() throws IOException {
        this.flush();
        file.close();
    }

    /**
     * Sets the start pointer to the given position. This method should
     * probably not be called directly. Space used by data before the
     * specified position becomes available for future calls to write.
     */
    @Override
	public synchronized void setStart(long newPosition) throws IOException {
        start = newPosition;
    }

    /**
     * Lock will set a lock flag denoting that the CCILog is being used
     * and should not be used by another caller.  Lock will save the end
     * point of the circular file to be used by the rewind method.
     * 
     * @return boolean if lock was successful
     */
    @Override
	public boolean lock(){
        if (!lock){
            lockPointer = getEnd();
            lock = true;
            return true;
        }
        return false;
    }
    
    /**
     * unLock will set the lock flag to false and reset the 
     * start pointer (detailStart) to 0;
     * 
     */
    @Override
	public void unLock(){
        lockPointer = 0;
        lock = false;
    }

    /**
     * rewind will set the circular file's start point to the
     * lockPointer value saved by the lock method.  If the lock
     * flag is not set, rewind will return false.  
     *   
     * @return lock
     */
    @Override
	public boolean rewind(){
        if (lock){
            end = lockPointer;
        }
        return lock;
    }
     

    /**
      * Deletes all data in the circular file by setting the start pointer
      * to the position of the end pointer. 
      */
    @Override
	public void clear() throws IOException {
        setStart(end); // note that SecureCircularFile overrides setStart
        this.flush();
    }
    

    /**
     * Returns a CircularInputStream which can be used to read the data
     * contained in this circular file. A call to the clearToMark method
     * of this stream changes the start pointer of this circular file.
     * The available method returns the number of bytes that can be read
     * before end of file, but makes no guarantee about blocking.
     * Otherwise, CircularInputStream is just like any other InputStream.
     */
    @Override
	public CircularInputStream getInputStream() {
        return new CircularInputStreamImpl();
    }

    /**
     * Registers the StateListener which will record our start and end
     * pointers. Only one state listener is currently supported, so the 
     * given listener will replace any existing listener.
     */
    public void addStateListener(StateListener listener) {
        this.listener = listener;
    }
    
    /**
     * Implementation of CircularInputStream returned by getInputStream.
     * This is a member class (a non-static inner class) of CircularFile
     * and therefore has direct access to other members of CircularFile.
     */
    private class CircularInputStreamImpl extends CircularInputStream {
        
        private long current; @Override
		public long getCurrent() {return current;} // getter for debugging
        private long mark;    @Override
		public long getMark   () {return    mark;} // getter for debugging

        /**
         * Creates a CircularInputStreamImpl.
         */
        public CircularInputStreamImpl() {
            current = start;
            mark = start;
        }
        
        /**
         * Computes the number of bytes available for reading. This is the
         * amount of data between the current position and the end of the 
         * data. However, there is no guarantee that a read will not block.
         */
        @Override
		public int available() {
            if (long_available() > Integer.MAX_VALUE)
                return Integer.MAX_VALUE;
            else
                return (int) long_available();
        }
        
        /**
         * Computes the number of bytes available for reading. This is the
         * amount of data between the current position and the end of the 
         * data. However, there is no guarantee that a read will not block.
         */
        private long long_available() {
            if (end >= current)
                return end - current;
            else
                return length - current + end;
        }
        
        /**
         * Closes this input stream. The current implementation does nothing.
         */     
        @Override
		public void close() {
        }
        
        /**
         * Sets a bookmark at the current position.
         */
        @Override
		public void mark(int readLimit) {
            mark = current;
        }
        
        /**
         * Indicates whether the mark method is implemented. Returns true.
         */
        @Override
		public boolean markSupported() {
            return true;
        }
        
        /**
         * Sets the current position to the previously bookmarked position.
         */
        @Override
		public void reset() {
            current = mark;
        }
        
        /** 
         * Tells the underlying CircularFile to move the start pointer to
         * the bookmarked position. Should only be called when we are done with
         * the data read so far. 
         */
        @Override
		public void clearToMark() throws IOException {
            clearTo(mark);
        }
        
        @Override
		public void clear() throws IOException {
            current = end;
            mark = end;
            CircularFileImpl.this.setStart(end);
            CircularFileImpl.this.flush();
        }
        
        @Override
		public void clearTo(long position) throws IOException {
            CircularFileImpl.this.setStart(position);
            CircularFileImpl.this.flush();        
        }

        @Override
		public void reopen() {
            current = start;
            mark = start;
        }


        /**
         * Reads a single byte.
         * @return the byte read, or -1 if at end of file.
         */
        @Override
		public int read() throws IOException {
            byte[] temp = new byte[1];
            int n = this.read(temp, 0, 1);
            if (n < 1)
                return -1;
            else
                return temp[0] & 0xff;
        }
        
        /**
         * Reads an array of bytes.
         * @return the number of bytes read, or -1 if at end of file.
         */
        @Override
		public int read(byte[] b) throws IOException {
            return this.read(b, 0, b.length);
        }
        
        /**
         * Reads len bytes into an array of bytes.
         *
         * Since this is an InputStream, attempts to read more data than
         * available should read as much as possible, then return the number
         * of bytes read. This is different from RandomAccessFile, which
         * throws EOFException. This method handles the case where a read
         * has to loop around from the end of the file to the beginning,
         * so an EOFException will not be thrown unless there is an error
         * in the implementation.
         * 
         * If len is 0, nothing is done and 0 is returned.
         * If no data is available, -1 is returned.
         * 
         * However, if some other condition prevents the expected amount
         * of data from being read, this method throws an IOException.
         */
        @Override
		public int read(byte[] b, int off, int len) throws IOException {
            int n1, n2;
            int midlength;
            
            if (len == 0) 
                return 0;
            
            if (available() == 0)
                return -1;
            
            if (len > available()) 
                len = available();
            
            if (current + len < length) {
                n1 = file.read(b, off, len, current);
                n2 = 0;
                current = current + len;
            }
            else {        // have to read to EOF then loop around
                midlength = (int) (length - current);
                n1 = file.read(b, off, midlength, current);
                n2 = file.read(b, off + midlength, len - midlength, 0);
                current = len - midlength;
            }
            
            if ((n1 == -1) || (n2 == -1))
                throw new IOException("truncated read"); 
            
            return n1 + n2;
        }
        
        /**
         * Attempts to skip over n bytes of input. May skip fewer bytes
         * if fewer bytes are available. Never throws an exception.
         * If n is negative, no bytes are skipped.
         * @return the number of bytes skipped.
         */
        @Override
		public long skip(long n) {
            if (n < 0)
                n = 0;
            
            if (n > long_available())
                n = long_available();
            
            current += n;
            
            if (current >= length)
                current -= length;
            
            return n;
        }
    }
}
