package dw.io.diag;
import dw.io.info.MoneyGramReceiveDiagInfo;

public class MGReceiveDiagMsg extends DiagMessage {
	private static final long serialVersionUID = 1L;

	private MoneyGramReceiveDiagInfo mgRecvInfo;

    public MGReceiveDiagMsg(final MoneyGramReceiveDiagInfo mgRecvInfo) {
        super(DW_TT_TLOG_FDOC_MG_RECV);
        this.mgRecvInfo = mgRecvInfo;
    }

    /* Used by DiagReport to format Message to look like Plutto output. */
    @Override
	public String toString() {
        StringBuffer sb = new StringBuffer();
        
        // date/time stamp
        sb.append(super.toString());
        // label
        sb.append(" MG Receive "); 
        // MG info
        sb.append(mgRecvInfo);
        
        return sb.toString();
    }

    /** Encodes a byte array (buffer) of a MG Receive transaction
        suitable for logging to the transaction log file.
        @param mgRecvInfo        The MG Receive info to be included.
        @param bufLen            The returned lenght of the buffer to be logged.

      *
      *  Transaction Log Diagnostic - MG Receive
      * 
      *  NOTE:  This is an EXACT bit/byte layout linearly (left-to-right) and
      *         not a 'C' layout (e.g., like the Tandem mainframe sees it).
      *
      struct tag_TL_DIAG_MG_RECV
      {
         TL_HEADER  tl_h;


         struct
         {
           UBIT     format_version     : 2;
           UBIT     is_success         : 1;
           UBIT     remote_flag        : 1;  * future...not in phase 1 DW *
           UBIT     nbr_attempts       : 4;

           UBIT     currency_id        : 3;
           UBIT     amount_len_from_1  : 2;
           UBIT     _bit_filler_1      : 3;

           UBIT     _bit_filler_2      : 3;
           UBIT     emp_name_len       : 5;

           UBIT     emp_id_x           : 8;     * emp id (expanded to 256).

         } bf;


         char      ref_nbr[ 8 ];
  
         UBYTE     amount[ variable ];    * length based on amount_len_from_1 (1 to 4)*

         char      emp_name[ variable ];  * length based on emp_name_len (0 to 31)*

         UBYTE     data[ 80 ];                  * contains repeats of DIAG_DW_MG_RECV_INFO *

      }


      typedef struct tag_DIAG_DW_MG_RECV_INFO
      {
         struct
         {
           UBIT     field_id       : 3;
           UBIT     info_len       : 5; * does NOT include the size of 'h_bf' *
         } h_bf;

         union
         {
            struct
            {
               byte      discount_percentage;
            } d_p;

            struct
            {
               struct
               {
                 UBIT    scale            : 4;
                 UBITE   _bit_filler_1    : 4;
               } b_f;

               UBYTE     exchange_rate[ 3 ];
            } e_r;

            struct
            {
               struct
               {
                 UBIT    is_packed        : 1;
                 UBITE   _bit_filler_1    : 7;
               } b_f;

               char      money_saver_nbr[ variable ];  * size is info_len - 1 *
            } m_s;

            UBYTE        data[ 31 ]; * anticipated maximum data field...based on info_len *

         } info;
      }

    */

//    public byte[] getTandemByteArray() {
//        final byte[] buf = new byte[ DNET_LEN_TLOG_HDR + 128 ];
//        int ii, jj, tempInt, byte_1_ii;
//        String tempStr;
//        BigDecimal tempBD;
//        final String userName = StringUtility.getNonNullString (mgRecvInfo.getUserName());
//        int userNameLen =  Math.min ( userName.length(), 31); 
//
//        // Encode the log data now.
//        ii = DNET_LEN_TLOG_HDR;
//
//        buf[ ii++ ] =  (byte)( 0x00   //version 0
//                             | (mgRecvInfo.isSuccessful() ? 0x20 : 0x00)
//                             | ((mgRecvInfo.getNumberOfAttempts() < 15
//                                   ? mgRecvInfo.getNumberOfAttempts() : 15) & 0x0F) );
//
//        byte_1_ii   =  ii;
//        buf[ ii++ ] =  (byte)( (Integer.parseInt( StringUtility.getNonNullZeroString (mgRecvInfo.getCurrency())) 
//                                                                            & 0x07) << 5 );
//
//        buf[ ii++ ] =  (byte)( userNameLen );
//
//        buf[ ii++ ] =  (byte)( mgRecvInfo.getEmployeeNumber() & 0xFF );
//
//        tempStr = StringUtility.getNonNullString (mgRecvInfo.getReferenceNumber());
//        for ( jj = 0; jj < 8 && jj < tempStr.length(); jj++ )
//          buf[ ii++ ] = (byte)tempStr.charAt( jj );
//
//        for ( ; jj < 8; jj++ )
//          buf[ ii++ ] = 0x20;
//
//        tempInt = getNonNullBigDecimal (mgRecvInfo.getAmount()).unscaledValue().intValue();
//        jj      = 0;
//
//        if ( tempInt > 0x00FFFFFF ) {
//          buf[ ii++ ] = (byte)( (tempInt >>> 24) & 0xFF );
//          jj++;
//        }
//
//        if ( tempInt > 0x0000FFFF ) {
//          buf[ ii++ ] = (byte)( (tempInt >>> 16) & 0xFF );
//          jj++;
//        }
//
//        if ( tempInt > 0x000000FF ) {
//          buf[ ii++ ] = (byte)( (tempInt >>>  8) & 0xFF );
//          jj++;
//        }
//
//        buf[ ii++ ] = (byte)( (tempInt      ) & 0xFF );
//
//        buf[ byte_1_ii ] = (byte)( buf[ byte_1_ii ]
//                                 | (jj << 3) );
//
//        for ( jj = 0; jj < userNameLen; jj++ )
//          buf[ ii++ ] = (byte)userName.charAt( jj );
//
//        // Encode the optional log data fields now.
//        final BigDecimal discountPercentage = getNonNullBigDecimal (mgRecvInfo.getDiscountPercentage());
//        if (discountPercentage.compareTo( new BigDecimal( 0 ) )
//               != 0 ) {
//          buf[ ii++ ] = (byte)( (DW_ID_TLOG_MG_SEND_DISCOUNT_P << 5)
//                              | 1 );
//
//          buf[ ii++ ] = (byte)(discountPercentage.unscaledValue().intValue()
//                                  & 0x7F );
//        }
//
//        final BigDecimal exchangeRate = getNonNullBigDecimal (mgRecvInfo.getExchangeRate());
//        if ( exchangeRate.compareTo( new BigDecimal( 1 ) )
//               != 0 ) {
//          buf[ ii++ ] = (byte)( (DW_ID_TLOG_MG_SEND_XCHG_RATE << 5)
//                              | 4 );
//
//          buf[ ii++ ] = (byte)( (exchangeRate.scale() & 0x0F) << 4 );
//
//          tempInt     = discountPercentage.unscaledValue().intValue();
//
//          buf[ ii++ ] = (byte)( (tempInt >>> 16) & 0xFF );
//          buf[ ii++ ] = (byte)( (tempInt >>>  8) & 0xFF );
//          buf[ ii++ ] = (byte)( (tempInt       ) & 0xFF );
//        }
//
//        tempStr = StringUtility.getNonNullString (mgRecvInfo.getMoneySaverNumber());
//        if ( tempStr.length() > 0 ) {
//
//          if ( _strIsNumeric( tempStr ) ) {
//            tempInt = Math.min( (tempStr.length() + 1) / 2, 30 );
//
//            buf[ ii++ ] = (byte)( (DW_ID_TLOG_MG_SEND_MS_NBR << 5)
//                                | (( tempInt + 1) & 0x1F) );
//
//            buf[ ii++ ] = (byte)( 0x80 );  // is packed.
//
//            packNumberToNibbles( buf, ii, tempInt, tempStr );
//            ii += tempInt;
//          }
//          else {
//            tempInt = Math.min( tempStr.length(), 30 );
//
//            buf[ ii++ ] = (byte)( (DW_ID_TLOG_MG_SEND_MS_NBR << 5)
//                                | (( tempInt + 1) & 0x1F) );
//
//            buf[ ii++ ] = (byte)( 0x00 );  // NOT is packed.
//
//            for ( jj = 0; jj < tempInt; jj++ )
//              buf[ ii++ ] = (byte)tempStr.charAt( jj );
//          }
//        }
//
//        encodeHeader(buf,ii);
//
//        // Return the encoded buffer.
//        return ByteArray.subArray(buf,ii);
//    }
}
