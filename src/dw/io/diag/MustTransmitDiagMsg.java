package dw.io.diag;

public class MustTransmitDiagMsg extends DiagMessage {
	private static final long serialVersionUID = 1L;

    public static final short SECURITY_BREACH        =  10;
    public static final short AWOL_LIMIT             =  11;

    private short reasonCode;
    private short xrefID;
    
    private String reasonCodeText[] = { "",	
                                   "Host Buffer Pointer (1)", 
                                   "Host Buffer Pointer (2)", 
                                   "Remote Buffer Pointer (1)", 
                                   "Remote Buffer Pointer (2)", 
                                   "",	
                                   "$ Limit Exceeded", 
                                   "Item Limit Exceeded", 
                                   "",	
                                   "B Command Reset BillPaymentd", 
                                   "Security Breach", 
                                   "",	
                                   "Totals Reset", 
                                   "Transmission Failures", 
                                   "",	
                                   "",	
                                   "",	
                                   "",	
                                   "Checksum Failure" 
                                  };

    public MustTransmitDiagMsg(final short reasonCode, final short xrefID) {
        super(DNET_TT_TLOG_DIAG_MUST_XMIT);
        this.reasonCode = reasonCode;
        this.xrefID = xrefID;
    }

    /* Used by DiagReport to format Message to look like Plutto output. */
    @Override
	public String toString() {
        StringBuffer sb = new StringBuffer();
        
        // date/time stamp
        sb.append(super.toString());
        // label
        sb.append(" Must Transmit: "); 
        
        // reason code text
        String s;
        if (reasonCode <= (reasonCodeText.length))
            s = reasonCodeText[reasonCode];
        else if (reasonCode == 255)
            s = reasonCodeText[18];
        else 
            s = "Unknown Reason Code " + reasonCode; 
        sb.append(s);
        
        // xref ID
        if (xrefID > 0)
            sb.append("; XRef ID " + xrefID); 
            
        return sb.toString();
    }

    /** Encodes a byte array (buffer) of a Must Transmit event.
        @param reasonCode   The reason code for the must xmit condition.
        @param xrefID       The crossref ID for the must xmit condition.

      *
      *  Transaction Log Diagnostic - Must Transmit
      * 
      *  NOTE:  This is an EXACT bit/byte layout linearly (left-to-right) and
      *         not a 'C' layout (e.g., like the Tandem mainframe sees it).
      *
      struct tag_TL_DIAG_MUST_XMIT
      {
         TL_HEADER  tl_h;

         UBYTE      reason_code;

         struct
         {
            UBIT  _bit_filler_1  :  4;
            UBIT  xref_id        :  4;
         } bf;

      }
    */
    @Override
	public byte[] getTandemByteArray() {
        final byte[] buf = new byte[ DNET_LEN_TLOG_HDR + 2 ];
        encodeHeader(buf);
        int ii;

        // Encode the log data now.
        ii = DNET_LEN_TLOG_HDR;
        buf[ ii++ ] = (byte)( reasonCode & 0xFF );
        buf[ ii ] = (byte)( xrefID     * 0x0F );

        // Return the encoded buffer.
        return( buf );
    }
}
