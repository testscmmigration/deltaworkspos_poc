package dw.io.diag;

public class DispenserStatusDiagMsg extends DiagMessage {
	private static final long serialVersionUID = 1L;

	private short dispenserID;
    private int   dispenserStatusBits;
    private short dispenserItemCount;
    
    // bits are reversed because Plutto/Tandem expect them in the
    // reverse order of DW
    private String[] extstatus = {
            "NLG7",     //0 
            "S2", 		//1   
	        "S1",		//2  
	        "NLG6",		//3  
	        "LB",	    //4 	
	        "RAM",		//5  	
	        "ROM",      //6	
	        "PU",		//7  	
	        "RS",	    //8  	
	        "NLG5",	    //9  
	        "NLG4",	    //10   
	        "NLG3",  	//11   
	        "NLG2",		//12   
	        "NLG1",	    //13   
	        "PD",		//14 	
	        "MID",		//15 	
	        "OPN",		//16 	
	        "MEM",      //17	
	        "VOID",     //18	
	        "NIF",      //19	
	        "TEST",     //20	
	        "UNL",		//21	
	        "3/4",	    //22 	
	        "PWR",		//23 	
	        "FAIL",		//24  	
	        "GL",		//25 	
            "LS",       //26 	
            "AMT",      //27	
	        "PASS",		//28 	
	        "TOF",      //29	
	        "JAM",      //30	
	        ""};	    //31 	

    public DispenserStatusDiagMsg(final short dispenserID,
                                  final int   dispenserStatusBits,
                                  final short dispenserItemCount) {
        super(DNET_TT_TLOG_DIAG_DSPN_STATUS);
        this.dispenserID = dispenserID;
        this.dispenserStatusBits = dispenserStatusBits;
        this.dispenserItemCount = dispenserItemCount;
    }

    /* Used by DiagReport to format Message to look like Plutto output. */
    @Override
	public String toString() {
        StringBuffer sb = new StringBuffer();
        
        // date/time stamp
        sb.append(super.toString());
        // label
        sb.append(" Dispenser Status - "); 
        
        // dispenser ID
        sb.append("Disp" + dispenserID); 
        // dispenser count
        sb.append("; Item Count=" + dispenserItemCount);	
        // dispenser Flags label
        sb.append("; Flags: "); 
        //  dispenser Flags
        sb.append(" " + getStatusText(dispenserStatusBits));	
        
        return sb.toString();
    }
    
    // Returns the dispenser status text, based on status input.
    private String getStatusText(int status) {
		StringBuffer sb = new StringBuffer();
		for (int j = 0; j < 32; j++) {
		    if ((status & (1 << j)) != 0) {
			    sb.append(extstatus[j] + " ");	
			}
		}
		return sb.toString();
    }

    /** Encodes a byte array (buffer) of a Dispenser Status event.
        @param dispenserID  The dispenser ID opened (0 or 1).
        @param dispenserStatusBits  The dispenser's 32-bit status.
        @param dispenserItemCount   The item counter from the dispenser.

      *
      *  Transaction Log Diagnostic - Dispenser Status
      * 
      *  NOTE:  This is an EXACT bit/byte layout linearly (left-to-right) and
      *         not a 'C' layout (e.g., like the Tandem mainframe sees it).
      *
      struct tag_TL_DIAG_DISPENSER_STATUS
      {
         TL_HEADER  tl_h;

         UBYTE      item_count[ 2 ];

         union
         {                                    * four bytes of status bits * 
           struct
           {
             UBIT    offline              :  1;     * software determined * 
             UBIT    jam                  :  1;
             UBIT    missed_tof           :  1;
             UBIT    invalid_pass         :  1;
             UBIT    invalid_amount       :  1;
             UBIT    left_sensor          :  1;
             UBIT    graphics_loaded      :  1;
             UBIT    printer_failed       :  1;

             UBIT    power_fail           :  1;
             UBIT    third_fourth_markfail:  1;
             UBIT    printer_opened       :  1;
             UBIT    test_mode            :  1;
             UBIT    not_idle_fail        :  1;     * Power failed during command * 
             UBIT    voided_last_doc      :  1;
             UBIT    mem_init             :  1;
             UBIT    printer_is_open      :  1;

             UBIT    midmark_fail         :  1;
             UBIT    plunger_down         :  1;
             UBIT    not_logged_1         :  1;
             UBIT    not_logged_2         :  1;
             UBIT    not_logged_3         :  1;
             UBIT    not_logged_4         :  1;
             UBIT    not_logged_5         :  1;
             UBIT    right_sensor_tof     :  1;

             UBIT    plunger_up           :  1;
             UBIT    rom_fail             :  1;
             UBIT    ram_fail             :  1;
             UBIT    third_markfail       :  1;
             UBIT    not_logged_6         :  1;
             UBIT    switch_one_failed    :  1;
             UBIT    switch_two_failed    :  1;
             UBIT    not_logged_7         :  1;
           } bits;

           UBYTE  bytes[ 4 ];
         } st;

         struct
         {
           UBIT         dispenser_id             :  1;
           UBIT         _bit_filler_1            :  7;
         } bf;

      }
    */
    @Override
	public byte[] getTandemByteArray() {
        final byte[] buf = new byte[ DNET_LEN_TLOG_HDR + 7 ];
        encodeHeader(buf);
        int ii;

        // Encode the log data now.
        ii = DNET_LEN_TLOG_HDR;

        buf[ ii++ ] = (byte)(( dispenserItemCount  >>>  8 ) & 0xFF);
        buf[ ii++ ] = (byte)(( dispenserItemCount         ) & 0xFF);

        buf[ ii++ ] = (byte)(( dispenserStatusBits >>> 24 ) & 0xFF);
        buf[ ii++ ] = (byte)(( dispenserStatusBits >>> 16 ) & 0xFF);
        buf[ ii++ ] = (byte)(( dispenserStatusBits >>>  8 ) & 0xFF);
        buf[ ii++ ] = (byte)(( dispenserStatusBits        ) & 0xFF);

        buf[ ii++ ] = (byte)(( dispenserID & 0x01) << 7);

        // Return the encoded buffer.
        return( buf );
    }
}
