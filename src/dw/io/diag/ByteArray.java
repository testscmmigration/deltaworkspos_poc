package dw.io.diag;

public class ByteArray {
    
    protected byte[] array;
    private int appendPos = 0;
    
    public ByteArray(final int length) {
        array = new byte[length];
    }
    
    public ByteArray(final byte[] array) {
        this.array = array;
    }
    
    public void append(final byte b) {
        array[appendPos++] = b;
    }
    
    public void append(final byte[] byteArray) {
        for (int i = 0; i < byteArray.length; i++)
            append(byteArray[i]);
    }
    
    public void append(final ByteArray byteArray) {
        append(byteArray.array);
    }
    
    public boolean canHold(final int length) {
        return (array.length - appendPos) >= length;
    }
    
    public boolean cantHold(final int length) {
        return !(canHold(length));
    }
    
    public void clear() {
        appendPos = 0;
    }
    
    public boolean empty() {
        return (appendPos == 0);
    }
    
    public boolean full() {
        return cantHold(1);
    }
    
    public byte[] getArray() {
        return array;
    }
    
    // Returns a copy of the utilized portion of the array
    public byte[] subArray() {
        return subArray(appendPos);
    }
    
    // Returns a copy of the specified length of the beginning of the array
    public byte[] subArray(final int length) {
        final int actualLength = Math.min(length,array.length);
        final byte[] copy = new byte[actualLength];
        for (int i = 0; i < actualLength; i++)
            copy[i] = array[i];
        return copy;
    }
    
    // Returns a copy of the specified length of the beginning of the specified array
    public static byte[] subArray(final byte[] array, final int length) {
        return new ByteArray(array).subArray(length);
    }

    /**
     * Returns a string listing the bytes in hex format
     */
    public static String hex(byte[] data) {
	String result = "";	
	int i;

	for (i = 0; i < data.length; i++) {
	    if (i > 0)
	        if ((i % 20) == 0)
                    result += "\r\n";	
	        else
                    result += " ";	
	    result += Integer.toHexString((data[i] >> 4) & 0x0F);
	    result += Integer.toHexString(data[i] & 0x0F);
	}
	return result;
    }
}
