package dw.io.diag;

public class PeriodChangeDiagMsg extends DiagMessage {
	private static final long serialVersionUID = 1L;

    private short appID = DiagMessage.APPLICATION_DISPENSER;
    private short periodNbr;
    private boolean flagAutoChange;
    private short employeeID;
    private String appIDText[] = { "Term", "Disp", "Util", "Card", "MGram" };     

    public PeriodChangeDiagMsg(final short periodNbr,
                               final boolean flagAutoChange,
                               final short employeeID) {
        super(DNET_TT_TLOG_DIAG_PERIOD_CH);
        this.periodNbr = periodNbr;
        this.flagAutoChange = flagAutoChange;
        this.employeeID = employeeID;
    }

    /* Used by DiagReport to format Message to look like Plutto output. */
    @Override
	public String toString() {
        StringBuffer sb = new StringBuffer();
        
        // date/time stamp
        sb.append(super.toString());
        // label
        sb.append(" Period Change - "); 
        
        // change method
        String xmitProcess;
        if (flagAutoChange)
            xmitProcess = "Auto"; 
        else
            xmitProcess = "Manual"; 
        sb.append(xmitProcess + "; ");	
        
        // app id 
        String appIDTxt;
        if (appID < appIDText.length)
            appIDTxt = appIDText[appID];
        else
            appIDTxt = "Unknown App #" + appID; 
        sb.append(appIDTxt + " App; "); 
        
        // period number
        sb.append("Period " + periodNbr); 
        
        // employee ID
        sb.append("; Employee " + employeeID); 
        
        return sb.toString();
    }

    /** Encodes a byte array (buffer) of a accounting Period Change
        suitable for logging to the transaction log file.
        @param appID             The application ID for this Period ID.
        @param periodNbr         The new Period #.
        @param flagAutoChange    Auto. Accounting change? (not manual change)
        @param employeeID        Employee ID who performed the change (manual).

      *
      *  Transaction Log Diagnostic - Period Change
      * 
      *  NOTE:  This is an EXACT bit/byte layout linearly (left-to-right) and
      *         not a 'C' layout (e.g., like the Tandem mainframe sees it).
      *
      struct tag_TL_DIAG_PERIOD_CHANGE
      {
         TL_HEADER  tl_h;

         struct
         {
           UBIT     change_method  : 1;
           UBIT     app_id         : 4;
           UBIT     period_nbr     : 3;

           UBIT     _bit_filler_1  : 2;
           UBIT     emp_id         : 6;
         } bf;
      }
    */
    @Override
	public byte[] getTandemByteArray() {
        final byte[] buf = new byte[ DNET_LEN_TLOG_HDR + 2 ];
        encodeHeader(buf);
        int ii;

        // Encode the log data now.
        ii = DNET_LEN_TLOG_HDR;

        buf[ ii++ ] =  (byte)( ( flagAutoChange ? 0x80 : 0 )
                               | ((appID & 0x0F) << 3)
                               | ( periodNbr & 0x07 ) );

        buf[ ii ] =  (byte)( employeeID & 0x3F );

        // Return the encoded buffer.
        return( buf );
    }
}
