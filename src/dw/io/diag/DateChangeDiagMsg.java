package dw.io.diag;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import dw.utility.TimeUtility;

public class DateChangeDiagMsg extends DiagMessage {
	private static final long serialVersionUID = 1L;

	private Date changeTime;

    public DateChangeDiagMsg(final Date changeTime) {
        super(DNET_TT_TLOG_DIAG_DATE_CH);
        this.changeTime = changeTime;
    }
    /* Used by DiagReport to format Message to look like Plutto output. */
    @Override
	public String toString() {
        StringBuffer sb = new StringBuffer();
        
        // date/time stamp
        sb.append(super.toString());
        // label
        sb.append(" Date Change - "); 
        // formatted date change
        sb.append(new SimpleDateFormat(TimeUtility.DATE_MONTH_DAY_LONG_YEAR_FORMAT).format(changeTime));
        
        return sb.toString();
    }

    /** Encodes a byte array (buffer) of a Date Change event.
        @param aCalendar  The Calendar for this date change.

      *
      *  Transaction Log Diagnostic - Data Change
      * 
      *  NOTE:  This is an EXACT bit/byte layout linearly (left-to-right) and
      *         not a 'C' layout (e.g., like the Tandem mainframe sees it).
      *
      struct tag_TL_DIAG_DATE_CHANGE
      {
         TL_HEADER  tl_h;

         UBYTE jul_date[ 2 ];

      }
    */
    @Override
	public byte[] getTandemByteArray() {
        final byte[] buf = new byte[ DNET_LEN_TLOG_HDR + 2 ];
        encodeHeader(buf);
        int ii;
        Calendar aCalendar = getCalendar();
        short julDate = computeJulianDate( aCalendar );

        // Encode the log data now.
        ii = DNET_LEN_TLOG_HDR;

        buf[ ii++ ] = (byte)(( julDate >>> 8) & 0xFF );
        buf[ ii ] = (byte)(( julDate      ) & 0xFF );

        // Return the encoded buffer.
        return( buf );
    }
}
