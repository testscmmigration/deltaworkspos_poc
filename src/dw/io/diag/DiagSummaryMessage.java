package dw.io.diag;


// Represents the summary header of the diag log transmit
public class DiagSummaryMessage {    
    // Document type codes
    public static final int DIAG_SUMMARY_DOC_TYPE_TOTAL      = 0;
    public static final int DIAG_SUMMARY_DOC_TYPE_MO         = 1;
    public static final int DIAG_SUMMARY_DOC_TYPE_VP         = 2;
    public static final int DIAG_SUMMARY_DOC_TYPE_MGS        = 3;
    public static final int DIAG_SUMMARY_DOC_TYPE_MGR        = 4;
    public static final int DIAG_SUMMARY_DOC_TYPE_DG         = 5;
    public static final int DIAG_SUMMARY_DOC_TYPE_XP         = 6;
    public static final int DIAG_SUMMARY_DOC_TYPE_VOID       = 7;
    public static final int DIAG_SUMMARY_DOC_TYPE_LIMBO      = 8;
}
