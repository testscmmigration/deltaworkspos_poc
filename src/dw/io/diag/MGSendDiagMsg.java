package dw.io.diag;
import java.math.BigDecimal;

import dw.io.info.MoneyGramSendDiagInfo;
import dw.utility.StringUtility;

public class MGSendDiagMsg extends DiagMessage {
	private static final long serialVersionUID = 1L;

	private MoneyGramSendDiagInfo mgSendInfo;

    public MGSendDiagMsg(final MoneyGramSendDiagInfo mgSendInfo) {
        super(DW_TT_TLOG_FDOC_MG_SEND);
        this.mgSendInfo = mgSendInfo;
    }

    /* Used by DiagReport to format Message to look like Plutto output. */
    @Override
	public String toString() {
        StringBuffer sb = new StringBuffer();
        
        // date/time stamp
        sb.append(super.toString());
        // label
        sb.append(" MG Send "); 
        // MG info
        sb.append(mgSendInfo);
        
        return sb.toString();
    }

    /** Encodes a byte array (buffer) of a MG Send transaction
        suitable for logging to the transaction log file.
        @param mgSendInfo        The MG Send info to be included.
        @param bufLen            The returned lenght of the buffer to be logged.

      *
      *  Transaction Log Diagnostic - MG Send
      * 
      *  NOTE:  This is an EXACT bit/byte layout linearly (left-to-right) and
      *         not a 'C' layout (e.g., like the Tandem mainframe sees it).
      *
      struct tag_TL_DIAG_MG_SEND
      {
         TL_HEADER  tl_h;


         struct
         {
           UBIT     format_version     : 2;
           UBIT     is_success         : 1;
           UBIT     remote_flag        : 1;  * future...not in phase 1 DW *
           UBIT     nbr_attempts       : 4;

           UBIT     currency_id        : 3;
           UBIT     amount_len_from_1  : 2;
           UBIT     _bit_filler_1      : 3;

           UBIT     _bit_filler_2      : 3;
           UBIT     emp_name_len       : 5;

           UBIT     emp_id_x           : 8;     * emp id (expanded to 256).

         } bf;


         char      ref_nbr[ 8 ];
  
         UBYTE     amount[ variable ];    * length based on amount_len_from_1 (1 to 4)*

         char      emp_name[ variable ];  * length based on emp_name_len (0 to 31)*

         UBYTE     data[ 80 ];                  * contains repeats of DIAG_DW_MG_SEND_INFO *

      }


      typedef struct tag_DIAG_DW_MG_SEND_INFO
      {
         struct
         {
           UBIT     field_id       : 3;
           UBIT     info_len       : 5; * does NOT include the size of 'h_bf' *
         } h_bf;

         union
         {
            struct
            {
               byte      discount_percentage;
            } d_p;

            struct
            {
               struct
               {
                 UBIT    scale            : 4;
                 UBITE   _bit_filler_1    : 4;
               } b_f;

               UBYTE     exchange_rate[ 3 ];
            } e_r;

            struct
            {
               struct
               {
                 UBIT    is_packed        : 1;
                 UBITE   _bit_filler_1    : 7;
               } b_f;

               char      money_saver_nbr[ variable ];  * size is info_len - 1 *
            } m_s;

            UBYTE        data[ 31 ]; * anticipated maximum data field...based on info_len *

         } info;
      }

    */
    @Override
	public byte[] getTandemByteArray() {
        final byte[] buf = new byte[ DNET_LEN_TLOG_HDR + 128 ];
        int ii, jj, tempInt, byte_1_ii;
        String tempStr;
        final String userName = StringUtility.getNonNullString(mgSendInfo.getUserName());
        final int userNameLength = userName.length();
        int empNameLen =  ( userNameLength > 31 ? 31 : userNameLength ); 

        // Encode the log data now.
        ii = DNET_LEN_TLOG_HDR;

        buf[ ii++ ] =  (byte)( 0x00   //version 0
                             | (mgSendInfo.isSuccessful() ? 0x20 : 0x00)
                             | ((mgSendInfo.getNumberOfAttempts() < 15
                                   ? mgSendInfo.getNumberOfAttempts() : 15) & 0x0F) );

        byte_1_ii   =  ii;
        buf[ ii++ ] =  (byte)( (Integer.parseInt( mgSendInfo.getCurrency() ) & 0x07) << 5 );

        buf[ ii++ ] =  (byte)( empNameLen );

        buf[ ii++ ] =  (byte)( mgSendInfo.getEmployeeNumber() & 0xFF );

        tempStr = StringUtility.getNonNullString(mgSendInfo.getReferenceNumber());
        for ( jj = 0; jj < 8 && jj < tempStr.length(); jj++ ) 
          buf[ ii++ ] = (byte)tempStr.charAt( jj );

        for ( ; jj < 8; jj++ )
          buf[ ii++ ] = 0x20;

        tempInt = getNonNullBigDecimal(mgSendInfo.getAmount()).unscaledValue().intValue();
        jj      = 0;

        if ( tempInt > 0x00FFFFFF ) {
          buf[ ii++ ] = (byte)( (tempInt >>> 24) & 0xFF );
          jj++;
        }

        if ( tempInt > 0x0000FFFF ) {
          buf[ ii++ ] = (byte)( (tempInt >>> 16) & 0xFF );
          jj++;
        }

        if ( tempInt > 0x000000FF ) {
          buf[ ii++ ] = (byte)( (tempInt >>>  8) & 0xFF );
          jj++;
        }

        buf[ ii++ ] = (byte)( (tempInt      ) & 0xFF );

        buf[ byte_1_ii ] = (byte)( buf[ byte_1_ii ]
                                 | (jj << 3) );

        for ( jj = 0; jj < empNameLen; jj++ )
          buf[ ii++ ] = (byte)userName.charAt( jj );

        // Encode the optional log data fields now.
        final BigDecimal discountPercentage = getNonNullBigDecimal(mgSendInfo.getDiscountPercentage());
        final int discountPercentageInt = discountPercentage.unscaledValue().intValue();
        if ( discountPercentage.compareTo( new BigDecimal( 0 ) )
               != 0 ) {
          buf[ ii++ ] = (byte)( (DW_ID_TLOG_MG_SEND_DISCOUNT_P << 5)
                              | 1 );

          buf[ ii++ ] = (byte)( discountPercentageInt & 0x7F );
        }

        final BigDecimal exchangeRate = getNonNullBigDecimal(mgSendInfo.getExchangeRate());
        if ( exchangeRate.compareTo( new BigDecimal( 1 ) )
               != 0 ) {
          buf[ ii++ ] = (byte)( (DW_ID_TLOG_MG_SEND_XCHG_RATE << 5)
                              | 4 );

          buf[ ii++ ] = (byte)( (exchangeRate.scale() & 0x0F) << 4 );

          buf[ ii++ ] = (byte)( (discountPercentageInt >>> 16) & 0xFF );
          buf[ ii++ ] = (byte)( (discountPercentageInt >>>  8) & 0xFF );
          buf[ ii++ ] = (byte)( (discountPercentageInt       ) & 0xFF );
        }

        final String moneySaverNumber = StringUtility.getNonNullString(mgSendInfo.getMoneySaverNumber());
        if ( moneySaverNumber.length() > 0 ) {

          if ( _strIsNumeric( moneySaverNumber ) ) {
            tempInt = Math.min( (moneySaverNumber.length() + 1) / 2, 30 );

            buf[ ii++ ] = (byte)( (DW_ID_TLOG_MG_SEND_MS_NBR << 5)
                                | (( tempInt + 1) & 0x1F) );

            buf[ ii++ ] = (byte)( 0x80 );  // is packed.

            packNumberToNibbles( buf, ii, tempInt, moneySaverNumber );
            ii += tempInt;
          }
          else {
            tempInt = Math.min( moneySaverNumber.length(), 30 );

            buf[ ii++ ] = (byte)( (DW_ID_TLOG_MG_SEND_MS_NBR << 5)
                                | (( tempInt + 1) & 0x1F) );

            buf[ ii++ ] = (byte)( 0x00 );  // NOT is packed.

            for ( jj = 0; jj < tempInt; jj++ )
              buf[ ii++ ] = (byte)moneySaverNumber.charAt( jj );
          }
        }

        encodeHeader(buf,ii);

        // Return the encoded buffer.
        return ByteArray.subArray(buf,ii);
    }
}
