package dw.io.diag;

public class SecurityDiagMsg extends DiagMessage {
	private static final long serialVersionUID = 1L;

    public static final short HDV_LIMIT_OVERRIDE      =   1;
    public static final short MANAGER_OPTIONS         =   2;
    public static final short LOAD_FORMS_REQUEST      =   3;
    public static final short DUAL_CONTROL            =   4;
    public static final short ADD_EMPLOYEES           =   5;

    private short reasonCode;
    private short employeeID;
    
    private String securityText[] = { "",	
                                    "HDV Limit Override", 
                                    "Access to Change Mgr Options", 
                                    "Load Forms Request", 
                                    "Dual Control", 
                                    "Add Employees"}; 

    public SecurityDiagMsg(final short reasonCode,
                           final short employeeID) {
        super(DNET_TT_TLOG_DIAG_SECURITY);
        this.reasonCode = reasonCode;
        this.employeeID = employeeID;
    }

    /* Used by DiagReport to format Message to look like Plutto output. */
    @Override
	public String toString() {
        StringBuffer sb = new StringBuffer();
        
        // date/time stamp
        sb.append(super.toString());
        // label
        sb.append(" Security - "); 
        
        // reason code text
        if (reasonCode < securityText.length)
            sb.append(securityText[reasonCode]);
        else 
            sb.append("Unknown Reason Code #" + reasonCode); 
            
        // employee ID
        if (employeeID > 0)
            sb.append("; Employee " + employeeID); 
            
        return sb.toString();
    }

    /** Encodes a byte array (buffer) of a Security
        suitable for logging to the transaction log file.
        @param reasonCode        The reason code for the Security logging.
        @param employeeID        Employee ID of the 'security' user.

      *
      *  Transaction Log Diagnostic - Security
      * 
      *  NOTE:  This is an EXACT bit/byte layout linearly (left-to-right) and
      *         not a 'C' layout (e.g., like the Tandem mainframe sees it).
      *
      typedef struct tag_TL_DIAG_SECURITY
      {
         TL_HEADER  tl_h;

         struct
         {
           UBIT     _bit_filler_1  : 2;
           UBIT     emp_id         : 6;
         } bf;

         UBYTE reason;
      }
    */
    @Override
	public byte[] getTandemByteArray() {
        final byte[] buf = new byte[ DNET_LEN_TLOG_HDR + 2 ];
        encodeHeader(buf);
        int ii;

        // Encode the log data now.
        ii = DNET_LEN_TLOG_HDR;

        buf[ ii++ ] =  (byte)( employeeID & 0x3F );

        buf[ ii ] =  (byte)( reasonCode & 0xFF );

        // Return the encoded buffer.
        return( buf );
    }
}
