package dw.io.diag;

import dw.io.info.BillPaymentDiagInfo;
import dw.utility.StringUtility;

public class MGBillPaymentDiagMsg extends DiagMessage {
	private static final long serialVersionUID = 1L;

	private BillPaymentDiagInfo mgXpayInfo;

    public MGBillPaymentDiagMsg(final BillPaymentDiagInfo mgXpayInfo) {
        super(DW_TT_TLOG_FDOC_MG_XPAY);
        this.mgXpayInfo = mgXpayInfo;
    }

    /* Used by DiagReport to format Message to look like Plutto output. */
    @Override
	public String toString() {
        StringBuffer sb = new StringBuffer();
        
        // date/time stamp
        sb.append(super.toString());
        // label
        sb.append(" MG Receive "); 
        // MG info
        sb.append(mgXpayInfo);
        
        return sb.toString();
    }

    /** Encodes a byte array (buffer) of a MG BillPayment transaction
        suitable for logging to the transaction log file.
        @param mgXpayInfo        The MG BillPayment info to be included.
        @param bufLen            The returned lenght of the buffer to be logged.

      struct tag_TL_DIAG_MG_XPAY
      {
         TL_HEADER  tl_h;


         struct
         {
           UBIT     format_version     : 2;
           UBIT     is_success         : 1;
           UBIT     remote_flag        : 1;  * future...not in phase 1 DW *
           UBIT     nbr_attempts       : 4;

           UBIT     _bit_filler_2      : 1;
           UBIT     amount_len_from_1  : 2;
           UBIT     sender_name_len    : 5;

           UBIT     _bit_filler_3      : 2;
           UBIT     is_acct_nbr_packed : 1;
           UBIT     acct_nbr_len       : 5

           UBIT     _bit_filler_4      : 3;
           UBIT     emp_name_len       : 5;

           UBIT     emp_id_x           : 8;     * emp id (expanded to 256).
         } bf;


         char      ref_nbr[ 8 ];
         short     recv_code;
  
         UBYTE     amount[ variable ];    * length based on amount_len_from_1 (1 to 4)*

         char      sender_name[ variable ]; * length based on send_name_len (1 to 31) *

         char      acct_nbr[ variable ];  * length based on acct_nbr_len and packed flag *
                                          * if packed, var. length is (acct_nbr_len + 1) / 2 *

         char      emp_name[ variable ];  * length based on emp_name_len (0 to 31)*

         UBYTE     data[ 120 ];           * contains repeats of DIAG_DW_MG_XPAY_INFO *

      }


      typedef struct tag_DIAG_DW_MG_XPAY_INFO
      {
         struct
         {
           UBIT     field_id       : 3;
           UBIT     info_len       : 5; * does NOT include the size of 'h_bf' *
         } h_bf;

         union
         {
            struct
            {
               char      agency_name[ varialble ] * size is info_len *
            } a_n;

            struct
            {
               char      _3rd_party_name[ varialble ] * size is info_len *
            } _3pn;

            struct
            {
               struct
               {
                 UBIT    is_packed        : 1;
                 UBITE   _bit_filler_1    : 7;
               } b_f;

               char      money_saver_nbr[ variable ];  * size is info_len - 1 *
            } m_s;

            UBYTE        data[ 31 ]; * anticipated maximum data field...based on info_len *

         } info;
      }
    */
    @Override
	public byte[] getTandemByteArray() {
        final byte[] buf = new byte[ DNET_LEN_TLOG_HDR + 210 ];
        int ii, jj, tempInt, byte_1_ii, byte_2_ii;
        String tempStr;
        final String senderName = StringUtility.getNonNullString (mgXpayInfo.getSenderName());
        final String userName = StringUtility.getNonNullString (mgXpayInfo.getUserName());
        int senderNameLen =  Math.min (senderName.length(), 31); 
        int userNameLen =  Math.min (userName.length(), 31); 

        // Encode the log data now.
        ii = DNET_LEN_TLOG_HDR;

        buf[ ii++ ] =  (byte)( 0x00   //version 0
                             | (mgXpayInfo.isSuccessful() ? 0x20 : 0x00)
                             | ((mgXpayInfo.getNumberOfAttempts() < 15
                                   ? mgXpayInfo.getNumberOfAttempts() : 15) & 0x0F) );

        byte_1_ii   =  ii;
        buf[ ii++ ] =  (byte)( senderNameLen );

        byte_2_ii   =  ii;
        buf[ ii++ ] =  0;

        buf[ ii++ ] =  (byte)( userNameLen );

        buf[ ii++ ] =  (byte)( mgXpayInfo.getEmployeeNumber() & 0xFF );

        tempStr = StringUtility.getNonNullString (mgXpayInfo.getReferenceNumber());
        for ( jj = 0; jj < 8 && jj < tempStr.length(); jj++ )
          buf[ ii++ ] = (byte)tempStr.charAt( jj );

        for ( ; jj < 8; jj++ )
          buf[ ii++ ] = 0x20;

        tempInt = Integer.parseInt( mgXpayInfo.getReceiveCode() );
        buf[ ii++ ] = (byte)((tempInt >>> 8) & 0xFF);
        buf[ ii++ ] = (byte)((tempInt      ) & 0xFF);

        tempInt = getNonNullBigDecimal (mgXpayInfo.getAmount()).unscaledValue().intValue();
        jj      = 0;

        if ( tempInt > 0x00FFFFFF ) {
          buf[ ii++ ] = (byte)( (tempInt >>> 24) & 0xFF );
          jj++;
        }

        if ( tempInt > 0x0000FFFF ) {
          buf[ ii++ ] = (byte)( (tempInt >>> 16) & 0xFF );
          jj++;
        }

        if ( tempInt > 0x000000FF ) {
          buf[ ii++ ] = (byte)( (tempInt >>>  8) & 0xFF );
          jj++;
        }

        buf[ ii++ ] = (byte)( (tempInt      ) & 0xFF );

        buf[ byte_1_ii ] = (byte)( buf[ byte_1_ii ]
                                 | (jj << 5) );

        for ( jj = 0; jj < senderNameLen; jj++ )
          buf[ ii++ ] = (byte)senderName.charAt( jj );

        tempStr = StringUtility.getNonNullString (mgXpayInfo.getAccount());
        tempInt = Math.min( tempStr.length(), 31 );

        if ( tempInt > 0 && _strIsNumeric( tempStr ) ) {
          buf[ byte_2_ii ] = (byte)( 0x20  //turn Packed flag on
                                   | tempInt );

          tempInt = (tempInt + 1) / 2;

          packNumberToNibbles( buf, ii, tempInt, tempStr );
          ii += tempInt;
        }
        else {
          buf[ byte_2_ii ] = (byte)( 0x00  //NOT packed
                                   | tempInt );

          for ( jj = 0; jj < tempInt; jj++ )
            buf[ ii++ ] = (byte)tempStr.charAt( jj );
        }

        for ( jj = 0; jj < userNameLen; jj++ )
          buf[ ii++ ] = (byte)userName.charAt( jj );

        // Encode the optional log data fields now.
        tempStr = StringUtility.getNonNullString (mgXpayInfo.getAgencyName());
        if ( tempStr.length() > 0 ) {
          tempInt = Math.min( tempStr.length(), 31 );

          buf[ ii++ ] = (byte)( (DW_ID_TLOG_MG_XPAY_AGENCY_NAME << 5)
                              | tempInt );

          for ( jj = 0; jj < tempInt; jj ++ )
            buf[ ii++ ] = (byte)tempStr.charAt( jj );
        }

        tempStr = StringUtility.getNonNullString (mgXpayInfo.getThirdPartyName());
        if ( tempStr.length() > 0 ) {
          tempInt = Math.min( tempStr.length(), 31 );

          buf[ ii++ ] = (byte)( (DW_ID_TLOG_MG_XPAY_3RD_P_NAME << 5)
                              | tempInt );

          for ( jj = 0; jj < tempInt; jj ++ )
            buf[ ii++ ] = (byte)tempStr.charAt( jj );
        }

        tempStr = StringUtility.getNonNullString (mgXpayInfo.getMoneySaverNumber());
        if ( tempStr.length() > 0 ) {

          if ( _strIsNumeric( tempStr ) ) {
            tempInt = Math.min( (tempStr.length() + 1) / 2, 30 );

            buf[ ii++ ] = (byte)( (DW_ID_TLOG_MG_XPAY_MS_NBR << 5)
                                | (( tempInt + 1) & 0x1F) );

            buf[ ii++ ] = (byte)( 0x80 );  // is packed.

            packNumberToNibbles( buf, ii, tempInt, tempStr );
            ii += tempInt;
          }
          else {
            tempInt = Math.min( tempStr.length(), 30 );

            buf[ ii++ ] = (byte)( (DW_ID_TLOG_MG_XPAY_MS_NBR << 5)
                                | (( tempInt + 1) & 0x1F) );

            buf[ ii++ ] = (byte)( 0x00 );  // NOT is packed.

            for ( jj = 0; jj < tempInt; jj++ )
              buf[ ii++ ] = (byte)tempStr.charAt( jj );
          }
        }

        encodeHeader(buf,ii);

        // Return the encoded buffer.
        return ByteArray.subArray(buf,ii);
    }
}
