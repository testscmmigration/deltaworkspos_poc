package dw.io.diag;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import dw.utility.Scheduler;
import dw.utility.TimeUtility;

// Base class for DiagLog messages
// 11/8/2000 changed error handling of writeToLog so it is no longer fatal
public class DiagMessage implements Serializable {

    // Serialization compatibility was briefly broken in 2.5.3, fixed in 2.5.4.
    // This is to make sure it stays fixed.
    static final long serialVersionUID = 2222776630694149084L;

//    public static final Calendar LOCAL_CALENDAR = Scheduler.getLocalCalendar();

    public static final short DNET_ID_APP_DSPN                 = 1;
    public static final short DNET_ID_APP_CARD                 = 3;
    public static final short DNET_ID_APP_MGRAM                = 4;   /* new for DW */

    public static final short DNET_LEN_TLOG_HDR                = 4;

    public static final short DNET_TT_TLOG_FDOC_DISPENSER      = 101; /* Transaction Log Financial Doc. - Secured variety */

    public static final short DNET_TT_TLOG_DIAG_MUST_XMIT      = 204; /* Transaction Log Event - Must Transmit */
    public static final short DNET_TT_TLOG_DIAG_DSPN_STATUS    = 203; /* Transaction Log Diagnostic - Dispenser Status */
    public static final short DNET_TT_TLOG_DIAG_FDOC_LOAD      = 206; /* Transaction Log Diagnostic - Financial Doc. Load */
    public static final short DNET_TT_TLOG_DIAG_DATE_CH        = 207; /* Transaction Log Event - Date Change */
    public static final short DNET_TT_TLOG_DIAG_PERIOD_CH      = 209; /* Transaction Log Event - Period Change */
    public static final short DNET_TT_TLOG_DIAG_SECURITY       = 213; /* Transaction Log Event - Security */
    public static final short DNET_TT_TLOG_DIAG_VOIDED_FDOC    = 215; /* Transaction Log Diagnostic - Voided Financial Doc. */

    public static final short DW_TT_TLOG_FDOC_MG_SEND          = 501;
    public static final short DW_TT_TLOG_FDOC_MG_RECV          = 502;
    public static final short DW_TT_TLOG_FDOC_MG_XPAY          = 503;

    public static final short DW_TT_TLOG_DIAG_REPORT           = 602;

    public static final short DW_ID_TLOG_MG_SEND_DISCOUNT_P    =   0;
    public static final short DW_ID_TLOG_MG_SEND_XCHG_RATE     =   1;
    public static final short DW_ID_TLOG_MG_SEND_MS_NBR        =   2;

    public static final short DW_ID_TLOG_MG_XPAY_AGENCY_NAME   =   0;
    public static final short DW_ID_TLOG_MG_XPAY_3RD_P_NAME    =   1;
    public static final short DW_ID_TLOG_MG_XPAY_MS_NBR        =   2;

    public static final short APPLICATION_DISPENSER            =   1;

    // Taking this out to preserve object serialization compatibility
    //protected static String portID[] = { "MODEM", "PURPLE", "PINK", "BLUE" };

    // The record type (Group and Code) of the log record.
    private short recType;
    private Date time;
    private boolean flagHour = true;

    /** Compute and return the julian date in the TEMG-specific format:

          16-bits defined as:
           <0:5>   the year offset from 1970 (so 30 represents the year 2000)
           <6:15>  the julian day number (so 32 is February 1st)

        @param aCalendar  The Calendar date to be computed on.
    */
    public static short computeJulianDate( Calendar aCalendar ) {
      // Return the julian data (in TEMG-speicific format).
      return( (short)( ((aCalendar.get( Calendar.YEAR        ) - 1970) << 9)
                     |  (aCalendar.get( Calendar.DAY_OF_YEAR ) & 0x1FF) ) );
    }

    /** Pack a number represented in a string to a series of nibbles (4-bits)
        where an Ascii '1' in represented as binary 1 in the nibble, '2' as 2,
        and so on.
        @param buf       Buffer to store the packed number.
        @param offset    Byte offset in buffer to begin pack number storage.
        @param packBytes The packed size in bytes (1 byte stored 2 digits).
        @param nbrString The Ascii number string to be stored.
    */
    public static void packNumberToNibbles( byte[] buf,
                                            int offset,
                                            int packBytes,
                                            String nbrString ) {
      int ii, jj, kk, size;

      // Do the right thing if the input string is null
      if (nbrString == null) {
    nbrString = "";	
      }

      // Build the packed representation of the number string passed.
      ii   = offset;
      size = packBytes * 2;
      jj   = nbrString.length() - size;

      for ( kk = 0; kk < size; kk++, jj++ ) {
        if ( jj < 0 ) {
          buf[ ii ] = 0;

          if ( (kk & 1) == 1 )
            ii++;
        }

        else if ( (kk & 1) == 0 )
          buf[ ii ]  = (byte)((((byte)nbrString.charAt( jj ) - 0x30) & 0x0F) << 4);
        else {
          buf[ ii ] |= (byte)(((byte)nbrString.charAt( jj ) - 0x30) & 0x0F);
          ii++;
        }
      }
    }

    /** Test a String's contents to see if it contains only numeric chars.
        Return true if all numeric...else return false.
        @param str       Buffer to store the packed number.
    */
    protected static boolean _strIsNumeric( String str ) {
      if     ( str == null
            || str.length() == 0 )
        return( false );

      int  ii;
      for ( ii = 0; ii < str.length(); ii++ ) {
        if     ( str.charAt( ii ) < '0'
              || str.charAt( ii ) > '9' )
          return( false );
      }
      return( true );
    }

    public DiagMessage(final short recType) {
        this.recType = recType;

        //
        // Determinal the time precision needed (hour:min or min:sec).
        //
        switch ( recType / 100 ) {
            case 1:  //DNET-financial
            case 5:  //DW-financial
                     flagHour = false;
        }

        this.time = TimeUtility.currentTime();
    }

    protected Calendar getCalendar() {
        //Calendar calendar = (Calendar)(LOCAL_CALENDAR.clone());
        Calendar calendar = (Calendar)(Scheduler.getLocalCalendar().clone());
        calendar.setTime(time);
        return calendar;
    }

    /** Encodes the common transaction log header
        suitable for logging to the transaction log file.
        @param  buf     The buffer to be encoded with header info.
        @param  recType The record type (Group and Code).
        @param  recLen  The record length (excludes the size of the Header).
        @param  aCalendar The date and time to be encoded on the Header.

      *
      *  Transaction Log Header (begins ALL tlog records)
      *
      *  NOTE:  This is an EXACT bit/byte layout linearly (left-to-right) and
      *         not a 'C' layout (e.g., like the Tandem mainframe sees it).
      *
      struct tag_TL_HEADER
      {
         UBIT       rec_len_0_7            :  8;

         UBIT       rec_len_8_9            :  2;
         UBIT       time_hr_flag           :  1;
         UBIT       tcode                  :  5;

         UBIT       time_hr_or_secs        :  6;
         UBIT       tgroup_0_1             :  2;

         UBIT       tgroup_2_3             :  2;
         UBIT       time_mins              :  6;
      }

     */

    /**
     * Loads the byte array with the header information
     * @param buf     The buffer to be written.
     * @param bufLen  The length of the buffer to be written.
     */
    protected void encodeHeader(final byte[] buf, final int bufLen) {
        final short bodyLen = (short)(bufLen - DNET_LEN_TLOG_HDR);
        //
        // Sanity check the buffer length.
        //
        if ( buf.length < DNET_LEN_TLOG_HDR )
            return;

        Calendar calendar = getCalendar();

        //
        // Encode the header bit fields now (byte-by-byte).
        //
        int  ii = 0;

        buf[ ii++ ] =  (byte)((bodyLen >>> 2) & 0xFF);

        buf[ ii++ ] =  (byte)( ((bodyLen  & 0x03) << 6)
                             | ( flagHour ? 0x20 : 0 )
                             | ((recType  % 100) & 0x1F) );

        buf[ ii++ ] =  (byte)( ((( flagHour ? calendar.get( Calendar.HOUR_OF_DAY )
                                            : calendar.get( Calendar.SECOND ) )
                                                                     & 0x3F ) << 2)
                             | (((recType / 100) >>> 2) & 0x03) );

        buf[ ii++ ] =  (byte)( (((recType / 100) & 0x03 ) << 6)
                             | ( calendar.get( Calendar.MINUTE ) & 0x3F ) );
    }

    // Encoding where the buffer length is determined directly from the buffer
    protected void encodeHeader(final byte[] buf) {
        encodeHeader(buf,buf.length);
    }

    // Returns the data formated for the Tandem
    public byte[] getTandemByteArray() {
        final byte[] buf = new byte[ DNET_LEN_TLOG_HDR ];
        encodeHeader(buf);

        // Return the empty buffer (header only).
        return( buf );
    }

/* Removed DW4.0.

    // Write to the DiagLog
    public void writeToLog(DiagLog diagLog) {
        Calendar calendar = getCalendar();
        Calendar lastLoggedCalendar = diagLog.getLastLoggedCalendar();
        if      (needsDateChangeCheck
               && ((calendar.get(Calendar.YEAR       ) != lastLoggedCalendar.get(Calendar.YEAR       ))
                || (calendar.get(Calendar.DAY_OF_YEAR) != lastLoggedCalendar.get(Calendar.DAY_OF_YEAR))))
            diagLog.writeDateChange(time);
        else if (!flagHour
               && ((calendar.get(Calendar.YEAR       ) != lastLoggedCalendar.get(Calendar.YEAR       ))
                || (calendar.get(Calendar.DAY_OF_YEAR) != lastLoggedCalendar.get(Calendar.DAY_OF_YEAR))
                || (calendar.get(Calendar.HOUR_OF_DAY) != lastLoggedCalendar.get(Calendar.HOUR_OF_DAY))))
            diagLog.writeHourSync();
        try {
            diagLog.writeObject(this);
        }
        catch (IOException e) {
            ErrorManager.mustHandleLogs();
        }

        //
        // Reset the last logged calendar date.
        //
        lastLoggedCalendar.setTime(time);
    }
*/

    /** 
     * Prints to look like Plutto log inquiry. Subclasses call this method to 
     * get the initial portion consisting of record type and time.
     * @author Steve Steel - changed to use square brackets and HH:mm format
     * @author Geoff Atkin - factored time format out for efficiency
     */
    @Override
	public String toString() {
    	DateFormat df = new SimpleDateFormat(TimeUtility.TIME_12_HOUR_MINUTE_FORMAT);
        return "[" + recType + "] " + df.format(time);	 
    }

/* Taking this out to preserve object serialization compatibility
    public String getPortID(short serialPortID) {
        String value;
        int id = (new Short(serialPortID)).intValue();
        if (id <= portID.length)
            value = portID[id];
        else
            value = "Unknown Port ID " + serialPortID;
        return (value + " Port");

    }
*/

    private static BigDecimal ZERO = new BigDecimal(0d);
    public static BigDecimal getNonNullBigDecimal(final BigDecimal number) {
        if (number == null)
            return ZERO;
        else
            return number;
    }
}
