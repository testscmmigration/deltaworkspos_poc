package dw.io.diag;

public class FinancialDocLoadDiagMsg extends DiagMessage {
	private static final long serialVersionUID = 1L;

	private short dispenserID;
    private String docSerialNbrStart;
    private String docSerialNbrEnd;
    private boolean flagCardLoad;
    private short employeeID;

    public FinancialDocLoadDiagMsg(final short dispenserID,
                                   final String docSerialNbrStart,
                                   final String docSerialNbrEnd,
                                   final boolean flagCardLoad,
                                   final short employeeID) {
        super(DNET_TT_TLOG_DIAG_FDOC_LOAD);
        this.dispenserID = dispenserID;
        this.docSerialNbrStart = docSerialNbrStart;
        this.docSerialNbrEnd = docSerialNbrEnd;
        this.flagCardLoad = flagCardLoad;
        this.employeeID = employeeID;
    }

    /* Used by DiagReport to format Message to look like Plutto output. */
    @Override
	public String toString() {
        StringBuffer sb = new StringBuffer();
        
        // date/time stamp
        sb.append(super.toString());
        //label
        sb.append(" Load Pack - "); 
        
        // load method
        String s;
        if (flagCardLoad)
            s = "Card"; 
        else
            s = "Manual"; 
        sb.append(s);
        
        // dispenser ID
        sb.append("; Disp" + (dispenserID + 1)); 
        
        // employee ID
        sb.append("; Employee " + employeeID); 
        
        // start serial #
        sb.append("; Start Serial #" + docSerialNbrStart); 
        
        // end serial #
        sb.append("; End Serial #" + docSerialNbrEnd); 
        
        return sb.toString();
    }

    /** Encodes a byte array (buffer) of a Financial Document Load event.
        @param dispenserID       The dispenser ID (0 or 1).
        @param docSerialNbrStart The starting doc serial #.
        @param docSerialNbrEnd   The ending   doc serial #.
        @param flagCardLoad      Flag indicating Card-swipe load.
        @param employeeID        Employee ID who performed the load.

      *
      *  Transaction Log Diagnostic - Financial Doc. Load
      * 
      *  NOTE:  This is an EXACT bit/byte layout linearly (left-to-right) and
      *         not a 'C' layout (e.g., like the Tandem mainframe sees it).
      *
      typedef struct tag_TL_DIAG_FDOC_LOAD
      {
         TL_HEADER  tl_h;

         struct
         {
           UBIT     load_method    : 1;
           UBIT     dispenser_id   : 1;
           UBIT     emp_id         : 6;
         } bf;

         UBYTE   start_serial_number[ 9 ];
         UBYTE   end_serial_number[ 9 ];

      }
    */
    @Override
	public byte[] getTandemByteArray() {
        final byte[] buf = new byte[ DNET_LEN_TLOG_HDR + 19 ];
        encodeHeader(buf);
        int ii;

        // Encode the log data now.
        ii = DNET_LEN_TLOG_HDR;

        buf[ ii++ ] =  (byte)( ( flagCardLoad ? 0x10 : 0 )
                             | ((dispenserID  & 0x01) << 6)
                             | ( employeeID   & 0x3F) );


        packNumberToNibbles( buf, ii, 9, docSerialNbrStart );
        ii += 9;

        packNumberToNibbles( buf, ii, 9, docSerialNbrEnd );
        ii += 9;

        // Return the encoded buffer.
        return( buf );
    }
}
