package dw.io;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import dw.utility.Debug;

/**
 * Circular file with a message digest. Extends <code>CircularFile</code> 
 * with a cryptographic message digest to protect against tampering.
 * @author Geoff Atkin
 * modified by Francis Fung 10/5/2000 to extend CircularFileImpl as per new
 * CircularFile inheritance hierarchy
 */
public class SecureCircularFile extends CircularFileImpl {

    private MessageDigest initialDigest;
    private MessageDigest currentDigest;
    private MessageDigest savedDigest;
        
    /** 
     * Creates a secure circular file to read from and write to an existing
     * file. A message digest is created and initialized.
     * @param file the underlying file
     * @param start the offset into the file where existing data begins
     * @param end the offset into the file where new data can be written
     * @param mdseed data used to initialize the message digest
     * @throws IOException if the length of the file can not be determined
     */
    public SecureCircularFile(SimpleRandomAccessFile file, long start, 
	    long end, byte[] mdseed) 
	    throws IOException, NoSuchAlgorithmException {
    	super(file, start, end);
	initialDigest = MessageDigest.getInstance("SHA");	
	initialDigest.update(mdseed);
	this.setStart(start);
    }

    /** 
     * Creates a secure circular file to read from and write to a new file 
     * with the given name and length. An object of type 
     * SimpleRandomAccessFile is created and set to the requested length.
     * @param filename the name of the file to be created
     * @param length the size of the file to be created
     * @param mdseed data used to initialize the message digest
     * @throws IOException if the file can not be created
     */
    public static SecureCircularFile create(String filename, long length,
	    byte[] mdseed) throws IOException, NoSuchAlgorithmException {
    	SimpleRandomAccessFile file 
            = new SimpleRandomAccessFile(filename, "rw"); 
    	file.setLength(length);
    	return new SecureCircularFile(file, 0, 0, mdseed);
    }

    /** 
     * Creates a secure circular file to read from and write to a new 
     * encrypted file with the given name.
     * @param filename the name of the file to be created
     * @param length the size of the file to be created
     * @param key the data used to initialize the encryption algorithm
     * @param mdseed data used to initialize the message digest
     * @throws IOException if the file can not be created
     */
    public static SecureCircularFile create(String filename, long length, 
	    long key, byte[] mdseed) 
	    throws IOException, NoSuchAlgorithmException {
    	EncryptedRandomAccessFile file 
            = new EncryptedRandomAccessFile(filename, "rw", key); 
    	file.setLength(length);
    	return new SecureCircularFile(file, 0, 0, mdseed);
    }

    /**
     * Writes len bytes from the given byte array starting at offset off
     * to the file. This method simply calls super.write(byte[],int,int)
     * and updates the message digest.
     * @param b the array of bytes to be written
     * @param off the start offset in the array
     * @param len the number of bytes to write
     * @throws EOFException if len exceeds the available space
     * @throws IOException if the bytes can not be written for another reason
     */
    @Override
	public synchronized void write(byte[] b, int off, int len) 
	    throws IOException {
	/* 
	 * Do the write first, then the update. If an exception happens
	 * during the write, we want the update to be skipped.
	 */
	super.write(b, off, len);
	currentDigest.update(b, off, len);
    }

    /**
     * Sets the start pointer to the given position. This method calls
     * super.setStart(long) and recomputes the message digest.
     */
    @Override
	public synchronized void setStart(long newPosition)
	    throws IOException {
	byte[] buffer = new byte[1024];
	int len;
	
	super.setStart(newPosition);
	try {
    	    currentDigest = (MessageDigest) initialDigest.clone();
    	    CircularInputStream input = this.getInputStream();
    	    while ((len = input.read(buffer)) != -1) {
    		currentDigest.update(buffer, 0, len);
    	    }
	}
	catch (CloneNotSupportedException e) {
	    throw new IOException("clone not supported"); 
	}
    }

    /**
     * lock will save the currentDigest and call super
     * 
     * return success
     */
    @Override
	public boolean lock(){
        try {
            savedDigest = getMessageDigest();
        }
        catch (CloneNotSupportedException e) {
            return false; 
        }
        return super.lock();
    }
    
    /**
     * rewind will restore the savedDigest and call super.
     */
    @Override
	public boolean rewind(){
        if(super.lock){
            try {
                setMessageDigest((MessageDigest) savedDigest.clone());
            } catch (CloneNotSupportedException e) {
                Debug.printStackTrace(e);
            }
            
        }

        return super.rewind();
    }

    /**
     * Returns a final or intermediate message digest. This method returns
     * a clone of the current message digest, so that future writes can 
     * continue to update it.
     */
    public synchronized MessageDigest getMessageDigest() 
	    throws CloneNotSupportedException {
	return (MessageDigest) currentDigest.clone();
    }
    
    /**
     * set the currentDigest.
     * 
     * @param MessageDigest md
     */
    public synchronized void setMessageDigest(MessageDigest md){
        currentDigest = md;
    }

    /**
     * Flushes any buffers to disk. 
     */      
    @Override
	public void flush() throws IOException {
	file.flush();
	if (listener != null) {
	    byte[] md;
	    try {
	        md = getMessageDigest().digest();
	    }
	    catch (CloneNotSupportedException e) {
			md = new byte[1];
			Debug.println("MessageDigest.clone() not supported");	
	    }
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    DataOutputStream out = new DataOutputStream(baos);
	    out.writeLong(start);
	    out.writeLong(end);
	    out.writeByte(md.length);
	    out.write(md);
	    listener.stateChanged(baos.toByteArray(), 0, out.size());
	}
    }

    public static String hex(byte[] data) {
	String result = "";	
	int i;

	for (i = 0; i < data.length; i++) {
	    if (i != 0)
		result += " ";	
	    result += Integer.toHexString((data[i] >> 4) & 0x0F);
	    result += Integer.toHexString(data[i] & 0x0F);
	}
	return result;
    }
}
