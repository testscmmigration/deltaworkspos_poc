package dw.io.info;

import java.util.Date;

/** Represents the information in a send message for a MoneyGram.

    @author Erik S. Steinmetz
*/
public class MoneyGramSendDiagInfo extends MoneyGramDiagInfo {
	private static final long serialVersionUID = 1L;

    public MoneyGramSendDiagInfo( Date aTime, int anEmployeeNumber,
                    String aUserName, int aNumberOfAttempts, boolean successful) {
        super( aTime, anEmployeeNumber, aUserName, aNumberOfAttempts, successful);
    }
        
}
