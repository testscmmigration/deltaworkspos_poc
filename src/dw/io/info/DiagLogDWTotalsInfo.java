
//////////////////////////////////////////////////
//
//    DiagLogDWTotalsInfo.java
//
//    Copyright 2000,  MoneyGram
//    
//    17 April, 2000 - Created, B.Stumpf
//
//    20 April, 2000 - Added Application ID support.
//
//////////////////////////////////////////////////

package dw.io.info;

import java.io.Serializable;

/**
    Hold DW totals information for specific totals type (ID).
    (after creation, this information is intentially read-only)
*/
public class DiagLogDWTotalsInfo implements Serializable {
	private static final long serialVersionUID = 1L;

    private  short  _appID;
    private  short  _totalsID;
    private  int    _count;
    private  int    _amount;

    public static final short DW_ID_TOTALS_DSPN_MO                =  1;
    public static final short DW_ID_TOTALS_DSPN_VP                =  2;
    public static final short DW_ID_TOTALS_DSPN_GC                =  3;
    public static final short DW_ID_TOTALS_DSPN_DG                =  4;
    public static final short DW_ID_TOTALS_DSPN_VOID              =  5;

    public static final short DW_ID_TOTALS_MGRAM_RECV             =  1;
    public static final short DW_ID_TOTALS_MGRAM_SEND             =  2;
    public static final short DW_ID_TOTALS_MGRAM_XPAY             =  3;
    public static final short DW_ID_TOTALS_CARD_PURCHASE          =  4;
    public static final short DW_ID_TOTALS_CARD_LOAD              =  5;
    public static final short DW_TOTALS_PRODUCTS                  =  5;

    /** Constructor
        @param totalsID  The totals' ID.
        @param count     The # of items.
        @param totalsID  The amount of the items (2 decimal places assumed).
    */
    public DiagLogDWTotalsInfo( short appID, short totalsID, int count, int amount )
    {

       _appID    = appID;
       _totalsID = totalsID;
       _count    = count;
       _amount   = amount;

    }


    /** Get the application ID.
    */
    public short getAppID( )
    {

      return( _appID );

    }

    /** Get the totals' ID.
    */
    public short getTotalsID( )
    {

      return( _totalsID );

    }

    /** Get the count.
    */
    public int getCount( )
    {

      return( _count );

    }

    /** Get the totals' ID.
    */
    public int getAmount( )
    {

      return( _amount );

    }

}
