
package dw.io.info;

import java.math.BigDecimal;
import java.util.Date;

/** Base class of MoneyGram sends and receives.
    @author Erik S. Steinmetz
*/
public abstract class MoneyGramDiagInfo extends FinancialDiagInfo {
	private static final long serialVersionUID = 1L;

    private BigDecimal discountPercentage = new BigDecimal( 0);
    private String currency = "0"; // USD by default	
    private BigDecimal exchangeRate = new BigDecimal( 1);
    private String[] currencyCode = { "USD", "MXD"};	 
    
    public MoneyGramDiagInfo( Date aTime, int anEmployeeNumber,
                  String aUserName, int aNumberOfAttempts, boolean successful) {
        super( aTime, anEmployeeNumber, aUserName, aNumberOfAttempts, successful);
    }
        

    public BigDecimal getDiscountPercentage() { return( discountPercentage);}
    public void setDiscountPercentage( BigDecimal aDiscount) {
    	discountPercentage = aDiscount;
    }
    
    public String getCurrency() { return( currency);}
    public void setCurrency( String aCurrency) {
        currency = aCurrency;
    }
    
    public BigDecimal getExchangeRate() { return( exchangeRate);}
    public void setExchangeRate( BigDecimal anExchangeRate) {
        if( anExchangeRate != null) {
            exchangeRate = anExchangeRate;
        }
    }
    
    /* Used by DiagReport to format Message to look like Plutto output. */
    @Override
	public String toString() {
        StringBuffer sb = new StringBuffer();
        // amount 
        sb.append(" Amount=" + super.getAmount());	
        
        // currency 
        int i = 0;
        try {
        	i = (Integer.parseInt(currency));
        } catch (Exception e) {
        }
        if (i < currencyCode.length)
            sb.append(" (" + currencyCode[i] + ")"); 	 
        sb.append(super.toString());
        return sb.toString();
    }
}
