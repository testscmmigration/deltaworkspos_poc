package dw.io.info;

import java.util.Date;

public abstract class EventDiagInfo extends DiagInfo {
	private static final long serialVersionUID = 1L;

    private int type;
    
    protected abstract int getMaxType();
    
    /**
     *  Constructor with all attributes specified
     */
    public EventDiagInfo( Date aTime, int anEmployeeNumber, String aUserName,
                         int aType, int aNumberOfAttempts, boolean successful) {
        super( aTime, anEmployeeNumber, aUserName, aNumberOfAttempts, successful);
        setType(aType);
    }
    
    /**
     *  Constructor with employee number and user name unspecified
     */
    public EventDiagInfo( Date aTime,
                         int aType, int aNumberOfAttempts, boolean successful) {
        super( aTime, aNumberOfAttempts, successful);
        setType(aType);
    }
    
    public int getType() { return type;}
    public void setType( int aType) {
        type = aType;
    }
}
