package dw.io.info;

import java.util.Date;

/**
    Report event object used for wrapping up all the parameters for an diag
    event.
    @author Eric Inman/Chris Bartling
 */
public class ReportEventDiagInfo extends EventDiagInfo {
	private static final long serialVersionUID = 1L;

    public static final int DISPENSER_ITEMS_REPORT = 0;
    public static final int MONEYGRAM_AND_BILL_PAYMENT_REPORT = 1;
    public static final int DIRECTORY_OF_AGENTS = 4;
    public static final int PROCESSING_FEE_REPORT = 5;
    private static final int MAX_TYPE = DIRECTORY_OF_AGENTS;
    private String reportName[] = {"Dispenser Items Report", 
                                "Moneygram Report", 
                                "Flat File Creation", 
                                "Diagnostics Report", 
                                "Directory of Agents"}; 
    
    @Override
	protected int getMaxType() {
        return MAX_TYPE;
    }
    
    
    public ReportEventDiagInfo
                       ( Date aTime, int anEmployeeNumber, String aUserName,
                         int aType, int aNumberOfAttempts, boolean successful)
            //throws Exception                          //no it doesn't --gea
    {
        super( aTime, anEmployeeNumber, aUserName,
               aType, aNumberOfAttempts, successful);
    }
    
    /* Used by DiagReport to format Message to look like Plutto output. */
    @Override
	public String toString() {
        StringBuffer sb = new StringBuffer();
        
        // report name
        int i = super.getType();
        if (i < reportName.length)
            sb.append(reportName[i]);
        else
            sb.append("Unknown Report #" + i); 
        // report info
        sb.append(super.toString());
        
        return sb.toString();
    }
}
