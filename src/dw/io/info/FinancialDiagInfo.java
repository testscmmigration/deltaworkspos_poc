package dw.io.info;

import java.math.BigDecimal;
import java.util.Date;

/** Represents financial documents information.

    @author Erik S. Steinmetz
*/
public abstract class FinancialDiagInfo extends DiagInfo {
	private static final long serialVersionUID = 1L;

    private String referenceNumber = "";	
//    private String cardNumber = "";	
    private String moneySaverNumber = "";	
    private BigDecimal amount = new BigDecimal( 0);
        
    public FinancialDiagInfo( Date aTime, int anEmployeeNumber,
                  String aUserName, int aNumberOfAttempts, boolean successful) {
        super( aTime, anEmployeeNumber, aUserName, aNumberOfAttempts, successful);
    }
        
    public String getReferenceNumber() { return( referenceNumber);}
    public void setReferenceNumber( String aRefNum) {
        referenceNumber = aRefNum;
    }

    public String getMoneySaverNumber() { return( moneySaverNumber);}
    public void setMoneySaverNumber( String aMSNum) {
        moneySaverNumber = aMSNum;
    }
    
    public BigDecimal getAmount() { return( amount);}
    public void setAmount( BigDecimal anAmount) { amount = anAmount;}
    
    /* Used by DiagReport to format Message to look like Plutto output. */
    @Override
	public String toString() {
        StringBuffer sb = new StringBuffer();
        // Reference number
        sb.append("; Ref#=" + referenceNumber);	
        sb.append(super.toString());
        return sb.toString();
    }
}
