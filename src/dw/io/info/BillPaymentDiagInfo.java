
package dw.io.info;

import java.util.Date;

/** Represents the informatino in an bill payment financial transaction.
    @author Erik S. Steinmetz
*/
public class BillPaymentDiagInfo extends FinancialDiagInfo {
	private static final long serialVersionUID = 1L;

    private String senderName = "";	
    private String account = "";	
    private String thirdPartyName = "";	
    private String agencyName = "";	
    private String receiveCode = "0000";	
    
    
    public BillPaymentDiagInfo( Date aTime, int anEmployeeNumber,
                    String aUserName, int aNumberOfAttempts, boolean successful) {
        super( aTime, anEmployeeNumber, aUserName, aNumberOfAttempts, successful);
    }

    public String getSenderName() { return( senderName);}
    public void setSenderName( String aSenderName) throws Exception {
        if(aSenderName == null) {
            throw new Exception("Sender name non-existant."); 
        }
        else {
        	int iLen = aSenderName.length(); 
            if(  iLen > 20) {
            	iLen = 20;
            }
            senderName = aSenderName.substring(0,iLen);
        }
    }
    
    public String getAccount() { return( account);}
    public void setAccount( String anAccount) {
        if( anAccount != null) {
            account = anAccount;
        }
    }
    
    public String getThirdPartyName() { return( thirdPartyName);}
    public void setThirdPartyName( String aThirdPartyName) throws Exception {
        if( (aThirdPartyName == null) || (aThirdPartyName.length() > 20)) {
            throw new Exception("Third Party's name too long or non-existant."); 
        }
        else {
            thirdPartyName = aThirdPartyName;
        }
    }


    public String getAgencyName() { return( agencyName);}
    public void setAgencyName( String anAgencyName) throws Exception {
        if( anAgencyName == null) {
            throw new Exception("Agency's name non-existant."); 
        }
        else {
        	int iLen = anAgencyName.length(); 
            if(  iLen > 25) {
            	iLen = 25;
            }
            agencyName = anAgencyName.substring(0, iLen);
        }
    }

    public String getReceiveCode() { return( receiveCode);}
    public void setReceiveCode( String aReceiveCode) throws Exception {
//        if( (aReceiveCode == null) || (aReceiveCode.length() != 4)) {
//            throw new Exception("Receive Code is not 4 digits or non-existant."); 
//        }
//        else {
//            receiveCode = aReceiveCode;
//        }

        receiveCode = aReceiveCode;
    }
    
    /* Used by DiagReport to format Message to look like Plutto output. */
    @Override
	public String toString() {
        StringBuffer sb = new StringBuffer();
        
        // MG info
        sb.append(super.toString());
        // receive code    
        sb.append("; Receive Code=" + receiveCode);	
        // sender name
        //sb.append("; Sender=" + senderName);	
        // account number
        if (account != null) {
	        StringBuffer accountNumber= new StringBuffer(account);
	    	int l = accountNumber.length();
	        for (int i=0; i<l-4;i++){
	            accountNumber.setCharAt(i,'*');
	            account = accountNumber.toString();   
	    	}
	        sb.append("; Account#=" + account);	
        }
        
        return sb.toString();
    }
}
