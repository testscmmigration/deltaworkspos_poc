package dw.io.info;

import java.util.Date;

/** Represents information to be written to the diagnostic log.

    @author Erik S. Steinmetz
*/
public abstract class DiagInfo implements java.io.Serializable {
	private static final long serialVersionUID = 1L;


    private Date time;
    private int employeeNumber;
    private String userName;
    private int numberOfAttempts;
    private boolean successful;
        
    /** Creates a DiagInfo, but not without proper information.
    */
    public DiagInfo( Date aTime, int anEmployeeNumber, String aUserName, int aNumberOfAttempts, boolean successful) {
        setTime( aTime);
        setEmployeeNumber( anEmployeeNumber);
        setUserName( aUserName);
        setNumberOfAttempts(aNumberOfAttempts);
        setSuccessful(successful);
    }
        
    /** Creates a DiagInfo with employee number and user name unspecified
    */
    public DiagInfo( Date aTime, int aNumberOfAttempts, boolean successful) {
        this(aTime,0,"",aNumberOfAttempts,successful);	
    }
    
    public Date getTime() { return( time);}
    protected void setTime( Date aTime) { time = aTime;}
    
    public int getEmployeeNumber() { return( employeeNumber);}
    protected void setEmployeeNumber( int anEmployeeNumber) {
        employeeNumber = anEmployeeNumber;
    }
    
    public String getUserName() { return( userName);}
    protected void setUserName( String aUserName) {
        userName = aUserName;
    }
    
    public boolean isSuccessful() { return( successful);}
    public void setSuccessful( boolean successful) { 
        this.successful = successful;
    }
    
    public int getNumberOfAttempts() { return( numberOfAttempts);}
    public void setNumberOfAttempts( int aNumberOfAttempts) {
        numberOfAttempts = aNumberOfAttempts;
    }
    
    /* Used by DiagReport to format Message to look like Plutto output. */
    @Override
	public String toString() {
        StringBuffer sb = new StringBuffer();
        // successful flag
        sb.append("; Success=" + successful);	
        // employee ID
        sb.append("; Employee=" + employeeNumber);	
        // employee name
        sb.append("-" + userName);	
        return sb.toString();
    }
}
