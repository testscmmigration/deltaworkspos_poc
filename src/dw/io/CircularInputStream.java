package dw.io;
import java.io.IOException;
import java.io.InputStream;

public abstract class CircularInputStream extends InputStream {
    /* 
     * Removes data prior to the mark, making space available for
     * more data in the underlying CircularFile.
     */
    public abstract void clearToMark() throws IOException;
    
    public abstract void clearTo(long position) throws IOException;
    
    public abstract void clear() throws IOException;
    public abstract void reopen();
        
    // getters for debugging
    public abstract long getCurrent();
    public abstract long getMark();
}
