package dw.io;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Simplified, thread-safe version of RandomAccessFile. This class is a wrapper 
 * around RandomAccessFile. All read and write methods are synchronized, and
 * there is no seek method. Instead of saying<pre>
 * 	RandomAccessFile.seek(filePointer);
 * 	RandomAccessFile.write(bytes);
 * </pre> 
 * you say <pre>
 * 	SimpleRandomAccessFile.write(bytes, filePointer);
 * </pre>
 * @author Geoff Atkin
 */

public class SimpleRandomAccessFile {

    protected RandomAccessFile raf;

    /**
     * Creates a random access file stream to read from, and optionally to 
     * write to, a file with the specified name.
     */
    public SimpleRandomAccessFile(String filename, String mode) 
	throws IOException
    {
	this.raf = new RandomAccessFile(filename, mode);
    }

    /**
     * Reads a byte of data from this file, at the given position.
     * @param filePointer the offset from the beginning of the file.
     * @throws IOException if filePointer is less than 0 or if an I/O 
     * error occurs.
     * @return the next byte of data or -1 if end-of-file is reached.
     */
    public synchronized int read(long filePointer) throws IOException {
	raf.seek(filePointer);
    	return raf.read();
    }

    /**
     * Reads an array of bytes of data from this file, at the given position.
     * @param b the buffer into which the data is read
     * @param filePointer the offset from the beginning of the file
     * @throws IOException if filePointer is less than 0 or if an I/O 
     * error occurs.
     * @return the number of bytes read or -1 if at end-of-file.
     */
    public synchronized int read(byte[] b, long filePointer) 
	throws IOException
    {
    	raf.seek(filePointer);
	return raf.read(b);
    }

    /**
     * Reads up to len bytes into an array of bytes of data from this file, 
     * at the given position.
     * @param b the buffer into which the data is read.
     * @param off the start offset into the buffer.
     * @param len the maximum number of bytes to read.
     * @param filePointer the offset from the beginning of the file.
     * @throws IOException if filePointer is less than 0 or if an I/O 
     * error occurs.
     * @return the number of bytes read or -1 if at end-of-file.
     */
    public synchronized int read(byte[] b, int off, int len, long filePointer) 
	throws IOException 
    {
	raf.seek(filePointer);
    	return raf.read(b, off, len);
    }
    
    /**
     * Writes a single byte to this file at the given position.
     * @param b the byte to write.
     * @param filePointer the offset from the beginning of the file.
     * @throws IOException if filePointer is less than 0 or if an I/O 
     * error occurs.
     */
    public synchronized void write(int b, long filePointer) 
    	throws IOException
    {
	raf.seek(filePointer);
    	raf.write(b);
    }
    
    /**
     * Writes an array of bytes to this file at the given position.
     * @param b the data to write.
     * @param filePointer the offset from the beginning of the file.
     * @throws IOException if filePointer is less than 0 or if an I/O 
     * error occurs.
     */
    public synchronized void write(byte[] b, long filePointer) 
    	throws IOException
    {
    	raf.seek(filePointer);
    	raf.write(b);
    }

    /**
     * Writes len bytes from an array to this file at the given position.
     * @param b the data to write.
     * @param off the start offset into the array.
     * @param len the number of bytes to write.
     * @param filePointer the offset from the beginning of the file.
     * @throws IOException if filePointer is less than 0 or if an I/O 
     * error occurs.
     */
    public synchronized void write(byte[] b, int off, int len, long filePointer)
	throws IOException 
    {
    	raf.seek(filePointer);
    	raf.write(b, off, len);
    }

    /**
     * Returns the length of this file.
     * @throws IOException if an I/O error occurs.
     * @return the length of this file in bytes.
     */
    public synchronized long length() throws IOException {
	return raf.length();
    }
	
    /**
     * Sets the length of this file.
     * @param newLength the desired length in bytes.
     * @throws IOException if an I/O error occurs.
     */
    public synchronized void setLength(long newLength) throws IOException {
    	raf.setLength(newLength);
    }

    /**
     * Forces system buffers to synchronize with the underlying device.
     * @throws IOException if an I/O error occurs.
     */ 	    
    public void flush() throws IOException {
    	raf.getFD().sync();
    }

    /**
     * Closes this file.
     * @throws IOException if an I/O error occurs.
     */
    public void close() throws IOException {
    	raf.getFD().sync();
    	raf.close();
    }

    // Methods based on the DataInput and DataOutput interfaces. These 
    // have to be implemented in terms of this.read and this.write 
    // so the encrypted subclass will work.

    byte readByte(long filePointer) throws IOException {
	int b = this.read(filePointer);
	if (b < 0)
	    throw new EOFException();
	return (byte)(b);
    }

    int readInt(long filePointer) throws IOException {

	byte[] buf = new byte[4];
	if (this.read(buf, filePointer) != 4)
	    throw new EOFException();
/*
	return ((buf[0] << 24) + (buf[1] << 16) 
		+ (buf[2] << 8) + (buf[3] << 0));
*/
	DataInputStream in = new DataInputStream(
            new ByteArrayInputStream(buf));
        return in.readInt();
    }

    long readLong(long filePointer) throws IOException {
/*
	return ((long)(readInt(filePointer)) << 32) 
	    + (readInt(filePointer + 4) & 0xFFFFFFFFL);
*/
   	this.flush();
	byte[] buf = new byte[8];
	if (this.read(buf, filePointer) != 8)
	    throw new EOFException();
	
	DataInputStream in = new DataInputStream(
            new ByteArrayInputStream(buf));
        return in.readLong();
	
    }

    void writeByte(int b, long filePointer) throws IOException {
	this.write(b, filePointer);
    }

    void writeInt(int i, long filePointer) throws IOException {
/*	
	byte[] buf = new byte[4];
	buf[0] = (byte)((i >>> 24) & 0xFF);
	buf[1] = (byte)((i >>> 16) & 0xFF);
	buf[2] = (byte)((i >>> 8) & 0xFF);
	buf[3] = (byte)((i >>> 0) & 0xFF);
*/
	ByteArrayOutputStream baos = new ByteArrayOutputStream(4);
	DataOutputStream out = new DataOutputStream(baos);
	out.writeInt(i);
	this.write(baos.toByteArray(), filePointer);
	
    }

    void writeLong(long n, long filePointer) throws IOException {
/*
	byte[] buf = new byte[8];
	buf[0] = (byte)((n >>> 56) & 0xFF);
	buf[1] = (byte)((n >>> 48) & 0xFF);
	buf[2] = (byte)((n >>> 40) & 0xFF);
	buf[3] = (byte)((n >>> 32) & 0xFF);
	buf[4] = (byte)((n >>> 24) & 0xFF);
	buf[5] = (byte)((n >>> 16) & 0xFF);
	buf[6] = (byte)((n >>> 8) & 0xFF);
	buf[7] = (byte)((n >>> 0) & 0xFF);
*/
	ByteArrayOutputStream baos = new ByteArrayOutputStream(8);
	DataOutputStream out = new DataOutputStream(baos);
	out.writeLong(n);
	this.write(baos.toByteArray(), filePointer);
   	this.flush();
    }

}

