package dw.io;

/* Java imports */
import java.math.BigDecimal;
import java.util.Date;

import dw.io.diag.DateChangeDiagMsg;
import dw.io.diag.DiagMessage;
import dw.io.diag.DiagSummaryMessage;
import dw.io.diag.DispenserStatusDiagMsg;
import dw.io.diag.FinancialDocLoadDiagMsg;
import dw.io.diag.MGBillPaymentDiagMsg;
import dw.io.diag.MGReceiveDiagMsg;
import dw.io.diag.MGSendDiagMsg;
import dw.io.diag.MustTransmitDiagMsg;
import dw.io.diag.PeriodChangeDiagMsg;
import dw.io.info.BillPaymentDiagInfo;
import dw.io.info.DiagLogDWTotalsInfo;
import dw.io.info.MoneyGramReceiveDiagInfo;
import dw.io.info.MoneyGramSendDiagInfo;
import dw.io.info.ReportEventDiagInfo;
import dw.moneyorder.MoneyOrderInfomation;
import dw.profile.ClientInfo;
import dw.profile.UnitProfile;
import dw.utility.Debug;
import dw.utility.TimeUtility;


/**
 * Gone but not forgotten. The DiagLog used to be a circular file containing 
 * diagnostic records similar to the ones recorded by Delta/Dnet which show up
 * in Plutto. It is also responsible for creating and sending a DS (diagnostic 
 * summary) record containing various totals, although the totals themselves 
 * are stored elsewhere.
 * 
 * Prior to the release of 2.9 a change was made in the middleware to discard
 * diagnostic records at the servlet for performance reasons. In version 2.9 
 * changes were made to the POS to write the most interesting diag data to
 * the debug log instead, and to skip the transmission of the diag log.
 * 
 * However, a handful of less-interesting diag records were still being written 
 * to the DiagLog (and then cleared at transmit time) which means that two 
 * circular files (primary and backup) were still being used as of version 3.1.12.
 * 
 * During development of version 4.0 two things were noticed: first, that we were 
 * out of circular files, making it desirable to reclaim files we're not using; 
 * and second, that low-level logging and messaging code was writing stack dumps
 * to the diag log (and never seen again) but which would be helpful during
 * development of new messages and log files.
 * 
 * Because the "back-out" strategy if a problem is found during POS rollout is to 
 * revert to the previous version, it is best to remove references to the diag 
 * circular files at least one version before those files are reused for another 
 * purpose. Therefore, I have decided to do it now.
 *  	--gea 11/21/03.
 */
public class DiagLog {
    private static volatile DiagLog instance;

	private DiagLog() {
	}

    /** Returns the static instance of DiagLog. */
    public static DiagLog getInstance() {
        if (instance == null) {
        	instance = new DiagLog();
        }
        return instance;
    }

    /** Removes all data. */
    public void clear() {
        UnitProfile.getInstance().clearDiagBatchCountsAndTotals();
    }

    /** Convert a product document type into a summary document type. */
    private int toSummaryDocType(String docType) {
        int product = 1;
        int summary = 1;
        try {product = Integer.parseInt(docType); }
        catch (Exception e) {}

        if (product == 1)
            summary = DiagSummaryMessage.DIAG_SUMMARY_DOC_TYPE_MO;
        else if (product == 2)
            summary = DiagSummaryMessage.DIAG_SUMMARY_DOC_TYPE_VP;
        else if (product == 4)
            summary = DiagSummaryMessage.DIAG_SUMMARY_DOC_TYPE_DG;

        return summary;
    }

    /** Write a Money Order transaction log record. (Tcode 101)
        Also adjust summary totals. Summary totals are incremented in
        writeDWDispenserDocPrintStart, but here the limbo total is decremented
        because the item is known not to be limbo anymore. Also, if this is a
        void, decrement the product total and increment the void total.
        @param moneyOrderInfo The MoneyOrderInfo about this money order.
        @param clientInfo     The client's profile.
    */
    public synchronized void writeMoneyOrder( MoneyOrderInfomation moneyOrderInfo,
            ClientInfo clientInfo ) {

        // Move the writing of the 101 record to TempLog so that one
        // item is printed only once in the trace file.

        UnitProfile.getInstance().addDiagBatchAmount(
                moneyOrderInfo.getAmount().negate(),
                DiagSummaryMessage.DIAG_SUMMARY_DOC_TYPE_LIMBO);
        UnitProfile.getInstance().addDiagBatchCount(-1,
                DiagSummaryMessage.DIAG_SUMMARY_DOC_TYPE_LIMBO);

        if (moneyOrderInfo.getVoidFlag() == true) {
            int summaryDocType =
                toSummaryDocType(moneyOrderInfo.getDocumentType());

            // voids don't count towards overall summary total
            UnitProfile.getInstance().addDiagBatchAmount(
                    moneyOrderInfo.getAmount().negate(),
                    DiagSummaryMessage.DIAG_SUMMARY_DOC_TYPE_TOTAL);
            UnitProfile.getInstance().addDiagBatchCount(-1,
                    DiagSummaryMessage.DIAG_SUMMARY_DOC_TYPE_TOTAL);

            // no fee is collected for voided documents
            UnitProfile.getInstance().addDiagBatchFee(moneyOrderInfo.getFee().negate());

            UnitProfile.getInstance().addDiagBatchAmount(
                    moneyOrderInfo.getAmount().negate(),
                    summaryDocType);
            UnitProfile.getInstance().addDiagBatchCount(-1,
                    summaryDocType);

            UnitProfile.getInstance().addDiagBatchAmount(
                    moneyOrderInfo.getAmount(),
                    DiagSummaryMessage.DIAG_SUMMARY_DOC_TYPE_VOID);
            UnitProfile.getInstance().addDiagBatchCount(1,
                    DiagSummaryMessage.DIAG_SUMMARY_DOC_TYPE_VOID);
        }
    }

    /** Write a Power On transaction log record.
    */
    public synchronized void writePowerOn() {
    }

    /** Write a Dispenser Open transaction log record.
        @param dispenserID  The dispenser ID (0 or 1).
    */
    public synchronized void writeDispenserOpen(final short dispenserID ) {
    }

    /** Write a Dispenser Status transaction log record.
        @param dispenserID          The dispenser ID (0 or 1).
        @param dispenserStatusBits  The dispenser's 32-bit status.
        @param dispenserItemCount   The item counter from the dispenser.
    */
    public synchronized void writeDispenserStatus(
            final short dispenserID,
            final int   dispenserStatusBits,
            final short dispenserItemCount ) {

        Debug.println((new DispenserStatusDiagMsg(dispenserID,
                reverseBits(dispenserStatusBits),
                dispenserItemCount)).toString());
    }

    /** Write a Dispenser Open transaction log record.
        @param reasonCode   The reason code for the must xmit condition.
        @param xrefID       The crossref ID for the must xmit condition.
    */
    public synchronized void writeMustTransmit( short reasonCode, short xrefID ) {
        Debug.println((new MustTransmitDiagMsg(reasonCode,xrefID)).toString());
    }

    // Convenience method for non-document related events.
    public synchronized void writeMustTransmit( short reasonCode ) {
        writeMustTransmit(reasonCode,(short)0);
    }

    /** Write a Telco (Network) Connection transaction log record. */
    public synchronized void writeTelcoConnect() {
    }

    /** Write a Financial Document Load transaction log record.
        @param dispenserID       The dispenser ID (0 or 1).
        @param docSerialNbrStart The starting doc serial #.
        @param docSerialNbrEnd   The ending   doc serial #.
        @param flagCardLoad      Flag indicating Card-swipe load.
        @param employeeID        Employee ID who performed the load.
    */
    public synchronized void writeFinancialDocLoad( final short dispenserID,
                                                    final String docSerialNbrStart,
                                                    final String docSerialNbrEnd,
                                                    final boolean flagCardLoad,
                                                    final short employeeID ) {
      Debug.println((new FinancialDocLoadDiagMsg(dispenserID,docSerialNbrStart,docSerialNbrEnd,flagCardLoad,employeeID)).toString());
    }

    /** Write a Date Change transaction log record.
        @param aCalendar  The Calendar for this date change.
    */
    public synchronized void writeDateChange(Date time) {
		// removed DW4.0
	    //(new DateChangeDiagMsg(time)).writeToLog(this);

        Debug.println((new DateChangeDiagMsg(time)).toString());
    }

    // Write date change with current date and time
    public void writeDateChange() {
        writeDateChange(TimeUtility.currentTime());
    }

    /** Write a Hour Synchronization transaction log record.
    */
    public synchronized void writeHourSync( ) {
    }

    /** Write a accounting Period Change transaction log record.
        @param appID             The application ID for this Period ID.
        @param periodNbr         The new Period #.
        @param flagAutoChange    Auto. Accounting change? (not manual change)
        @param employeeID        Employee ID who performed the change (manual).
    */
    public synchronized void writePeriodChange(final short periodNbr,
                                               final boolean flagAutoChange,
                                               final short employeeID ) {
      Debug.println((new PeriodChangeDiagMsg(periodNbr,flagAutoChange,employeeID)).toString());
      
      // removed in DW4.0
      //(new PeriodChangeDiagMsg(periodNbr,flagAutoChange,employeeID)).writeToLog(this);
    }

    /** Write a Pointer Reset transaction log record.
    */
    public synchronized void writePointerReset() {
    }

    /** Write a Security transaction log record.
        @param reasonCode        The reason code for the Security logging.
        @param employeeID        Employee ID of the 'security' user.
    */
    public synchronized void writeSecurity(final short reasonCode, final short employeeID ) {
    }

    /** Write a Telco (network) Problem transaction log record.
        @param appError       The application error code.
        @param phoneEntryID   The phone Entry ID (which phone entry #).
        @param phoneNumber    The phone number.
        @param dialAttempts   The number of the dial attempt.
        Note: I took phone number and dial attempts back out of the
        TelcoProblemDiagMsg constructor for object serialization
        compatibility reasons. -- gea
    */
    public synchronized void writeTelcoProblem(final short appError,
                                               final short phoneEntryID,
                                               final String phoneNumber,
                                               final int dialAttempts) {
    }

//    /** Write a Voided Finacial (Dispenser) Document transaction log record.
//        (Tcode 215) Also increment the summary total void count.
//        @param dispenserID       The dispenser ID (0 or 1).
//        @param reasonCode        The reason code for the Voided Doc.
//        @param employeeID        Employee ID who 'caused' the Voided Doc.
//    */
//    public synchronized void writeFinancialDocVoid(final short dispenserID,
//                                                   final short reasonCode,
//                                                   final short employeeID) {
//        UnitProfile.getInstance().addDiagBatchCount(1, DiagSummaryMessage.DIAG_SUMMARY_DOC_TYPE_VOID);
//    }
//
/*
  	public synchronized void writeExDiagGeneral(final short diagCode,
                                                final byte[] diagData,
                                                final int diagDataLen ) {
    }

    // Convenience method
    public void writeExDiagGeneral(final short diagCode, final String message) {
        writeExDiagGeneral(getExtendedGeneralDiagMessage(diagCode,message));
    }

    // Convenience method
    public void writeExDiagGeneral(final String message) {
        writeExDiagGeneral(getExtendedGeneralDiagMessage(message));
    }

    // Convenience method
    public void writeExDiagGeneral(final ExDiagGeneralDiagMsg diagMessage) {
        diagMessage.writeToLog(this);
    }

    // Convenience method
    public ExDiagGeneralDiagMsg getExtendedGeneralDiagMessage(  final short diagCode,
                                                                final String message) {
        final byte[] diagData = message.getBytes();
        final int diagDataLen = diagData.length;
        return new ExDiagGeneralDiagMsg(diagCode,diagData,diagDataLen);
    }

    // Convenience method
    public ExDiagGeneralDiagMsg getExtendedGeneralDiagMessage(final String message) {
        return getExtendedGeneralDiagMessage((short)0,message);
    }
*/

    /** Write a MG Send transaction log record. (Tcode 501)
        Also increment the summary total.
        @param mgSendInfo        The MG Send info to be included.
    */
    public synchronized void writeMGSend(final MoneyGramSendDiagInfo mgSendInfo) {
        Debug.println((new MGSendDiagMsg(mgSendInfo)).toString());

        // increment the summary totals, but only if this was a sucessful transaction
        if (mgSendInfo.isSuccessful()) {
            UnitProfile.getInstance().addDiagBatchAmount(mgSendInfo.getAmount(),
                    DiagSummaryMessage.DIAG_SUMMARY_DOC_TYPE_TOTAL);
            UnitProfile.getInstance().addDiagBatchCount(1,
                    DiagSummaryMessage.DIAG_SUMMARY_DOC_TYPE_TOTAL);

            UnitProfile.getInstance().addDiagBatchAmount(mgSendInfo.getAmount(),
                    DiagSummaryMessage.DIAG_SUMMARY_DOC_TYPE_MGS);
            UnitProfile.getInstance().addDiagBatchCount(1,
                    DiagSummaryMessage.DIAG_SUMMARY_DOC_TYPE_MGS);
        }
    }

    /** Write a MG Receive transaction log record.
        Also increment the summary total.
        @param mgRecvInfo        The MG Receive info to be included.
    */
    public synchronized void writeMGReceive(final MoneyGramReceiveDiagInfo mgRecvInfo) {
        Debug.println((new MGReceiveDiagMsg(mgRecvInfo)).toString());

        // increment the summary totals, but only if this was a sucessful transaction
        if (mgRecvInfo.isSuccessful()) {

            // don't increment the overall summary total, because the DG items will

            UnitProfile.getInstance().addDiagBatchAmount(mgRecvInfo.getAmount(),
                    DiagSummaryMessage.DIAG_SUMMARY_DOC_TYPE_MGR);
            UnitProfile.getInstance().addDiagBatchCount(1,
                    DiagSummaryMessage.DIAG_SUMMARY_DOC_TYPE_MGR);
        }
    }

    /** Write a MG BillPayment transaction log record.
        Also increment the summary total.
        @param mgXpayInfo        The MG BillPayment info to be included.
    */
    public synchronized void writeMGBillPayment(
            final BillPaymentDiagInfo mgXpayInfo) {

        Debug.println((new MGBillPaymentDiagMsg(mgXpayInfo)).toString());

        // increment the summary totals, but only if this was a sucessful transaction
        if (mgXpayInfo.isSuccessful()) {
            UnitProfile.getInstance().addDiagBatchAmount(mgXpayInfo.getAmount(),
                    DiagSummaryMessage.DIAG_SUMMARY_DOC_TYPE_TOTAL);
            UnitProfile.getInstance().addDiagBatchCount(1,
                    DiagSummaryMessage.DIAG_SUMMARY_DOC_TYPE_TOTAL);

            UnitProfile.getInstance().addDiagBatchAmount(mgXpayInfo.getAmount(),
                    DiagSummaryMessage.DIAG_SUMMARY_DOC_TYPE_XP);
            UnitProfile.getInstance().addDiagBatchCount(1,
                    DiagSummaryMessage.DIAG_SUMMARY_DOC_TYPE_XP);
        }
    }

    /** Write a Money Order Printing Started transaction log record. (510)
        If there is no corresponding financial doc log record, then this
        is a limbo item. Increment the summary totals (overall and
        document) and the fee total as well as the limbo total, because
        limbo items are treated as live unless an adjustment is done.
        @param moneyOrderInfo The MoneyOrderInfo about this money order.
        @param clientInfo     The client's profile.
    */
    public synchronized void writeDWDispenserDocPrintStart(
            final MoneyOrderInfomation moneyOrderInfo,
            final ClientInfo clientInfo ) {

        int summaryDocType = toSummaryDocType(moneyOrderInfo.getDocumentType());

        UnitProfile.getInstance().addDiagBatchAmount(moneyOrderInfo.getAmount(),
                DiagSummaryMessage.DIAG_SUMMARY_DOC_TYPE_TOTAL);
        UnitProfile.getInstance().addDiagBatchCount(1,
                DiagSummaryMessage.DIAG_SUMMARY_DOC_TYPE_TOTAL);

        UnitProfile.getInstance().addDiagBatchAmount(moneyOrderInfo.getAmount(),
                summaryDocType);
        UnitProfile.getInstance().addDiagBatchCount(1,
                summaryDocType);

        UnitProfile.getInstance().addDiagBatchFee(moneyOrderInfo.getFee());

        UnitProfile.getInstance().addDiagBatchAmount(moneyOrderInfo.getAmount(),
                DiagSummaryMessage.DIAG_SUMMARY_DOC_TYPE_LIMBO);
        UnitProfile.getInstance().addDiagBatchCount(1,
                DiagSummaryMessage.DIAG_SUMMARY_DOC_TYPE_LIMBO);

    }

    /** Write a DW-specific Profile Change transaction log record.
        @param ponID        The Profile Option # ID (16-bit number).
        @param fieldDesc    The Field's description string (pretty).
        @param fieldData    The Field's data string (pretty).
        @param flagRemote   Was the change done my a remote interface to DW?
        @param employeeID   The employee ID that performed the changed.
        @param employeeName The employee's (user) name.
    */
    public synchronized void writeDWProfileChange(final short ponID,
                                                  final String fieldDesc,
                                                  final String fieldData,
                                                  final boolean flagRemote,
                                                  final short  employeeID,
                                                  final String employeeName ) {
    }

    /** Write a DW-specific Report transaction log record.
        @param reportEvDiagInfo        Report event data.
    */
    public synchronized void writeDWReport(final ReportEventDiagInfo reportEvDiagInfo ) {
    }

/*
    public synchronized void writeDWReport(
            final ReportEventDiagInfo reportEvDiagInfo, String message )
    {
        (new GenericDiagMsg(reportEvDiagInfo, message)).writeToLog(this);
    }

    public synchronized void writeGenericDiag(final String message) {
        (new GenericDiagMsg(message)).writeToLog(this);
    }
*/

    private static final int TOTAL_POSITION_MONEY_ORDER               = 0;
    private static final int TOTAL_POSITION_VENDOR_PAYMENT            = 1;
    private static final int TOTAL_POSITION_VOID                      = 2;
    private static final int TOTAL_POSITION_MONEYGRAM_RECEIVE         = 3;
    private static final int TOTAL_POSITION_MONEYGRAM_SEND            = 4;
    private static final int TOTAL_POSITION_MONEYGRAM_BILL_PAYMENT    = 5;
    private static final int NUMBER_OF_TOTALS = TOTAL_POSITION_MONEYGRAM_BILL_PAYMENT + 1;

    private void setTotal(final DiagLogDWTotalsInfo[] dwTotalsInfo,
            final int position,
            final short appID,
            final short docType) {
        final BigDecimal dollars = UnitProfile.getInstance().getGrandTotal(appID,docType);
        final BigDecimal cents = dollars.movePointRight(2);
        final int intCents = cents.intValue();
        final int count = UnitProfile.getInstance().getGrandCount(appID,docType);
        final DiagLogDWTotalsInfo nextInfo = new DiagLogDWTotalsInfo(appID,docType,count,intCents);
        dwTotalsInfo[position] = nextInfo;
    }


    public synchronized void writeDWTotals() {
        final DiagLogDWTotalsInfo[] dwTotalsInfo = new DiagLogDWTotalsInfo[NUMBER_OF_TOTALS];

        setTotal(dwTotalsInfo,TOTAL_POSITION_MONEY_ORDER,              DiagMessage.DNET_ID_APP_DSPN,
                                                                       DiagLogDWTotalsInfo.DW_ID_TOTALS_DSPN_MO);

        setTotal(dwTotalsInfo,TOTAL_POSITION_VENDOR_PAYMENT,           DiagMessage.DNET_ID_APP_DSPN,
                                                                       DiagLogDWTotalsInfo.DW_ID_TOTALS_DSPN_VP);

        setTotal(dwTotalsInfo,TOTAL_POSITION_VOID,                     DiagMessage.DNET_ID_APP_DSPN,
                                                                       DiagLogDWTotalsInfo.DW_ID_TOTALS_DSPN_VOID);

        setTotal(dwTotalsInfo,TOTAL_POSITION_MONEYGRAM_RECEIVE,        DiagMessage.DNET_ID_APP_MGRAM,
                                                                       DiagLogDWTotalsInfo.DW_ID_TOTALS_MGRAM_RECV);

        setTotal(dwTotalsInfo,TOTAL_POSITION_MONEYGRAM_SEND,           DiagMessage.DNET_ID_APP_MGRAM,
                                                                       DiagLogDWTotalsInfo.DW_ID_TOTALS_MGRAM_SEND);

        setTotal(dwTotalsInfo,TOTAL_POSITION_MONEYGRAM_BILL_PAYMENT,DiagMessage.DNET_ID_APP_MGRAM,
                                                                       DiagLogDWTotalsInfo.DW_ID_TOTALS_MGRAM_XPAY);
    }

    /**
     * Moves bit i to position 31 - i. This is needed for the dispenser
     * status diag, where Tandem/Plutto expect the status bits in the
     * opposite order from how the Dispenser class exposes them.
     * @author Geoff Atkin
     */
    public static int reverseBits(int bitset) {
        int result = 0;
        for (int j = 0; j < 32; j++) {
            if ((bitset & (1 << j)) != 0)
                result |= (1 << (31 - j));
        }
        return result;
    }

    public static void main( String[] args) {
        int i = 0x0100AAFF;
        Debug.println("i = "  + Integer.toHexString(i));	
        Debug.println("reverseBits(i) = " +	
                Integer.toHexString(reverseBits(i)));
        Debug.println("reverseBits(reverseBits(i)) = " +	
                Integer.toHexString(reverseBits(reverseBits(i))));
/*
        Debug.println("DiagLog Test");	
        State state = null;
        try {
            state = new State();
            state.createProfile();
        }
        catch (Exception e) {
            Debug.println("State or profile creation failure: " + e);	
        }
        Debug.println("State created");	
        DispenserFactory.getDispenser(1); // Avoid DiagLog constructor infinite recursion
        Debug.println("Dispenser created");	
        final DiagLog diagLog = DiagLog.getInstance();
        Debug.println("DiagLog created");	
        for (int i = 0; i < 40; i++)
            diagLog.writePointerReset();
        Debug.println("Pointer resets done");	
        diagLog.transmit(true, null);
        Debug.println("Transmit done");	
*/
    }

    private boolean transmitInProgress = false;

    /**
     * Manual transmit to middleware.
     */
    public synchronized boolean transmit() {

        //   same time
        if (transmitInProgress)
            return true;

        transmitInProgress = true;

        // Write the grand totals and counts
        writeDWTotals();

        // Transmit the summary, if not successful, do not continue.
        /* transmitDiagLog has been remove for DW5.0
        *if(!MessageFacade.getInstance().transmitDiagLog(parameters, "DS",0,	
        *        DiagSummaryMessage.getTransmission(), true, true)){
        *    transmitInProgress = false;
        *    return false;
        *}
        */

        clear();

        // If we were AWOL, we're not anymore.
        UnitProfile.getInstance().dailyTransmitSucceeded(TimeUtility.currentTime());
        transmitInProgress = false;
        return true;
    }
}
