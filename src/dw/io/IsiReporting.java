/*
 * Created on Jun 17, 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package dw.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import dw.main.MainPanelDelegate;
import dw.profile.UnitProfile;
import dw.utility.Debug;
import flags.CompileTimeFlags;

/**
 * @author A405
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class IsiReporting {

    public static final int ISI_LOG = 0;
    public static final int EXE_LOG = 2;
    public static final int NF_LOG = 3;
    public static final int ISI2_LOG = 4;
    public static final int EXE2_LOG = 5;
    //  Change to encryption 0 to make the text readable.
    private static final int encryptionAddon = 100; 
    private File file;
    private FileOutputStream fos;
    

    public void print (List<String> fields, int log_type) {
        StringBuffer cvsData = new StringBuffer();
        StringBuffer encryptedCvsData = new StringBuffer();
        
        char aChar;
        int charValue;
  
        // Spin through all fields and replace invalid characters with SPACE's
        for (int i = 0; i < fields.size(); i++) {
        	String temp = fields.get(i).replaceAll(",", " ").replaceAll("\"", " ").replaceAll("\r\n", " ").replaceAll("\n\r", " ");
            fields.remove(i);
            fields.add(i, temp);
        }

        // Create a CSV buffer
        for (int i=0; i < fields.size(); i++){
            cvsData.append("\"");
            cvsData.append(fields.get(i).trim());
            if (i < (fields.size() - 1))
                cvsData.append("\",");
            else
                cvsData.append("\"");  
        }
        
        // Encrypt CSV buffer
        for (int i=0; i < cvsData.length(); i++){
            aChar = cvsData.charAt(i);
            charValue = aChar;
            charValue = charValue + encryptionAddon;
            aChar = (char)charValue;
            encryptedCvsData.append(aChar);
        }

        switch (log_type) {
            case ISI_LOG:
                // Check profile item ISI_LOG defaulted to N.
                if (("Y".equals(UnitProfile.getInstance().get("ISI_LOG", "N"))) || (CompileTimeFlags.isIsiDebug())) {
                    // Code for ISI_LOG (MGRAM.TRN)
                    file = new File("MGRAM.TRN");
                    try {
                        fos = new FileOutputStream(file, true);
                        for (int i=0; i < encryptedCvsData.length(); i++ ){
                            fos.write(encryptedCvsData.charAt(i));
                        }
                        fos.write('\n');
                        fos.close();
                       	dumpIsiRecord(log_type, fields);
                       	
                    }
                    catch (FileNotFoundException e) {
                        Debug.println("FileNotFoundException: "+e.toString());
                    }
                    catch (IOException e1) {
                        Debug.println("IOException: "+e1.toString());
                    }
                }
                break;
            case EXE_LOG:
                // Code for EXE_LOG (DWISI.CSV)
//                if (MainPanelDelegate.getIsiExeCommand() &&
//                        MainPanelDelegate.getIsiAllowDataCollection())
                if (MainPanelDelegate.getIsiExeCommand()){
                    file = new File("DWISI.CSV");
                    try {
                        fos = new FileOutputStream(file, MainPanelDelegate.getIsiAppendMode());
                        for (int i=0; i < cvsData.length(); i++ ){
                            fos.write(cvsData.charAt(i));
                        }
                        fos.write('\n');
                        fos.close();
                       	dumpIsiRecord(log_type, fields);
                        MainPanelDelegate.setIsiAppendMode(true);
                    }
                    catch (FileNotFoundException e) {
                        Debug.println("FileNotFoundException: "+e.toString());
                    }
                    catch (IOException e1) {
                        Debug.println("IOException: "+e1.toString());
                    }
                }
                break;
            case NF_LOG:
                if (MainPanelDelegate.getIsiNfCommand()){
                    file = new File("DWISI.CSV");
                    try {
                        fos = new FileOutputStream(file, MainPanelDelegate.getIsiAppendMode());
                        for (int i=0; i < cvsData.length(); i++ ){
                            fos.write(cvsData.charAt(i));
                        }
                        fos.write('\n');
                        fos.close();
                       	dumpIsiRecord(log_type, fields);
                        MainPanelDelegate.setIsiAppendMode(true);
                    }
                    catch (FileNotFoundException e) {
                        Debug.println("FileNotFoundException: "+e.toString());
                    }
                    catch (IOException e1) {
                        Debug.println("IOException: "+e1.toString());
                    }
                }
                break;
            case ISI2_LOG:
                // Check profile item ISI_LOG defaulted to N.
                if ("Y".equals(UnitProfile.getInstance().get("ISI_LOG", "N"))) {
                    // Code for ISI_LOG (MGRAM.TRN)
                    file = new File("MGTEXT.TRN");
                    try {
                        fos = new FileOutputStream(file, true);
                        for (int i=0; i < encryptedCvsData.length(); i++ ){
                            fos.write(encryptedCvsData.charAt(i));
                        }
                        fos.write('\n');
                        fos.close();
                       	dumpIsiRecord(log_type, fields);
                    }
                    catch (FileNotFoundException e) {
                        Debug.println("FileNotFoundException: "+e.toString());
                    }
                    catch (IOException e1) {
                        Debug.println("IOException: "+e1.toString());
                    }
                }
                break;
            case EXE2_LOG:
                if (MainPanelDelegate.getIsiExe2Command()){
                    file = new File("MGTEXT.CSV");
                    try {
                        fos = new FileOutputStream(file, MainPanelDelegate.getIsiAppendMode());
                        for (int i=0; i < cvsData.length(); i++ ){
                            fos.write(cvsData.charAt(i));
                        }
                        fos.write('\n');
                        fos.close();
                       	dumpIsiRecord(log_type, fields);
                        MainPanelDelegate.setIsiAppendMode(true);
                    }
                    catch (FileNotFoundException e) {
                        Debug.println("FileNotFoundException: "+e.toString());
                    }
                    catch (IOException e1) {
                        Debug.println("IOException: "+e1.toString());
                    }
                }
                break;
        }
    }
    
    //==========================================================================
    
    private int fieldCount = 0;
    
    private void printIsiRecordField(String label, Iterator<String> iterator) {
    	String value;
    	try {
    		value = iterator.next();
    	} catch (Exception e) {
    		value = "* MISSING *";
    	}
    	formatIsiRecordField(label, value);
    }
    
    private void formatIsiRecordField(String label, String value) {
    	fieldCount++;
    	String line = String.format("%1$4d  %2$-30s %3$-30s", fieldCount, label, value);
    	Debug.println(line);
    }
    
    private void dumpIsiHeader(Iterator<String> iterator)  {
    	
    	// DeltaWorks Mode
    	
    	try {
        	String mode = iterator.next();
        	if (mode.equals("P")) {
        		formatIsiRecordField("DeltaWorks Mode:", "Production");
        	} else if (mode.equals("T")) {
        		formatIsiRecordField("DeltaWorks Mode:", "Training");
        	} else if (mode.equals("D")) {
        		formatIsiRecordField("DeltaWorks Mode:", "Demo");
        	} else {
        		formatIsiRecordField("DeltaWorks Mode:", "Unknown (" + mode + ")");
        	}
    	} catch (Exception e) {
    		formatIsiRecordField("DeltaWorks Mode:", "* MISSING *");
    	}
    	
    	// Date and time stamp
    	
    	try {
	    	String dt = iterator.next();
	    	String year = dt.substring(0, 4);
	    	String month = dt.substring(4, 6);
	    	String day = dt.substring(6, 8);
	    	String time = dt.substring(8, 16);
	    	formatIsiRecordField("Date Time Stamp:", month + "/" + day + "/" + year + " " + time);
    	} catch (Exception e) {
    		formatIsiRecordField("Date Time Stamp:", "* MISSING *");
    	}

    	// User id
    	
		printIsiRecordField("User Id:", iterator);

    	// Name
    	
		printIsiRecordField("Name:", iterator);

    	// MoneyGram Account
    	
		printIsiRecordField("MoneyGram Account:", iterator);

    	// Agent ID

		printIsiRecordField("Agent Id:", iterator);
    }
    
	private void dumpIsiRecord(int logType, List<String> fields) {
		
		Iterator<String> iterator = fields.iterator();
		
		String recordType;
		try {
			recordType = iterator.next();
		} catch (Exception e) {
			recordType = "* MISSING *";
		}
		
        if (! CompileTimeFlags.isIsiDebug()) {
        	
			switch (logType) {
				case ISI_LOG:
					Debug.println("Writing ISI record of type " + recordType + " to ISI log file (MGRAM.TRN)");
					break;
					
				case EXE_LOG:
					Debug.println("Writing ISI record of type " + recordType + " to command execution file (DWISI.CSV)");
					break;
					
				case NF_LOG:
					Debug.println("Writing NF ISI record of type " + recordType + " to command execution file (DWISI.CSV)");
					break;
					
				case ISI2_LOG:
					Debug.println("Writing ISI record of type " + recordType + " to alternate log file (MGTEXT.TRN)");
					break;
					
				case EXE2_LOG:
					Debug.println("Writing ISI record of type " + recordType + " to alternate command execution file (MGTEXT.CSV)");
					break;
					
				default:
					break;
			}
        	
        } else {
			fieldCount = 0;
			
			if ((fields == null) || (fields.isEmpty())) {
				Debug.println("Empty ISI Record");
			}
			
			switch (logType) {
				case ISI_LOG:
					Debug.println("Writing ISI record to ISI log file (MGRAM.TRN)");
					break;
					
				case EXE_LOG:
					Debug.println("Writing ISI record to command execution file (DWISI.CSV)");
					break;
					
				case NF_LOG:
					Debug.println("Writing NF ISI record to command execution file (DWISI.CSV)");
					break;
					
				case ISI2_LOG:
					Debug.println("Writing ISI record to alternate log file (MGTEXT.TRN)");
					break;
					
				case EXE2_LOG:
					Debug.println("Writing ISI record to alternate command execution file (MGTEXT.CSV)");
					break;
					
				default:
					break;
			}
			
			if (recordType.equals(MainPanelDelegate.clMoneyGramSend)) {
				formatIsiRecordField("ISI Record Type:", "MoneyGram Send (" + recordType + ")");
				dumpIsiHeader(iterator);
				printIsiRecordField("Destination Country:", iterator);
				printIsiRecordField("Sender First Name:", iterator);
				printIsiRecordField("Sender Middle Name:", iterator);
				printIsiRecordField("Sender Last Name:", iterator);
				printIsiRecordField("Sender Home Phone:", iterator);
				printIsiRecordField("Sender Address:", iterator);
				printIsiRecordField("Sender City:", iterator);
				printIsiRecordField("Sender State:", iterator);
				printIsiRecordField("Sender Zip Code:", iterator);
				printIsiRecordField("Sender Country:", iterator);
				printIsiRecordField("Sender Date of Birth:", iterator);
				printIsiRecordField("Sender Birth City:", iterator);
				printIsiRecordField("Sender Birth Country:", iterator);
				printIsiRecordField("Sender Passport Issue City:", iterator);
				printIsiRecordField("Sender Passport Issue Country:", iterator);
				printIsiRecordField("Sender Passport Issue Date:", iterator);
				printIsiRecordField("Sender Photo Id Type:", iterator);
				printIsiRecordField("Sender Photo Id Number:", iterator);
				printIsiRecordField("Sender Photo Id State:", iterator);
				printIsiRecordField("Sender Photo Id Country:", iterator);
				printIsiRecordField("Sender Legal Id Type:", iterator);
				printIsiRecordField("Sender Legal Id Number:", iterator);
				printIsiRecordField("Bank account number:", iterator);
				printIsiRecordField("Sender Occupation:", iterator);
				printIsiRecordField("Receiver First Name:", iterator);
				printIsiRecordField("Receiver Middle Name:", iterator);
				printIsiRecordField("Receiver Last Name 1:", iterator);
				printIsiRecordField("Receiver Last Name 2:", iterator);
				printIsiRecordField("Receiver Phone:", iterator);
				printIsiRecordField("Receiver Address:", iterator);
				printIsiRecordField("Receiver Home Delivery Address:", iterator);
				printIsiRecordField("Receiver Home Delivery Colonia:", iterator);
				printIsiRecordField("Receiver Home Delivery Municipio:", iterator);
				printIsiRecordField("Receiver Colonia:", iterator);
				printIsiRecordField("Receiver Municipio:", iterator);
				printIsiRecordField("Receiver City:", iterator);
				printIsiRecordField("Receiver State:", iterator);
				printIsiRecordField("Receiver Zip Code:", iterator);
				printIsiRecordField("Test Question:", iterator);
				printIsiRecordField("Test Answer:", iterator);
				printIsiRecordField("Message 1:", iterator);
				printIsiRecordField("Message 2:", iterator);
				printIsiRecordField("Third Pary First Name:", iterator);
				printIsiRecordField("Third Pary Middle Name:", iterator);
				printIsiRecordField("Third Pary Last Name:", iterator);
				printIsiRecordField("Third Pary Address:", iterator);
				printIsiRecordField("Third Pary City:", iterator);
				printIsiRecordField("Third Pary State:", iterator);
				printIsiRecordField("Third Pary Country:", iterator);
				printIsiRecordField("Third Pary Zip Code:", iterator);
				printIsiRecordField("Third Pary Legal ID Type:", iterator);
				printIsiRecordField("Third Pary Legal Id Number:", iterator);
				printIsiRecordField("Third Pary Date of Birth:", iterator);
				printIsiRecordField("Third Pary Occupation:", iterator);
				printIsiRecordField("Third Pary Organization:", iterator);
				printIsiRecordField("Delivery Option:", iterator);
				printIsiRecordField("Money Saver ID:", iterator);
				printIsiRecordField("Reference Number:", iterator);
				printIsiRecordField("Free Phone Call Pin:", iterator);
				printIsiRecordField("Toll Free Phone Number:", iterator);
				printIsiRecordField("Send Amount:", iterator);
				printIsiRecordField("Fee Total with Tax:", iterator);
				printIsiRecordField("Discount Amount:", iterator);
				printIsiRecordField("Discount Type:", iterator);
				printIsiRecordField("Total Send Amount:", iterator);
				printIsiRecordField("Send Currency:", iterator);
				printIsiRecordField("Receive Amount:", iterator);
				printIsiRecordField("Receive Currency:", iterator);
				printIsiRecordField("Exchange Rate:", iterator);
				printIsiRecordField("Partner Confirmation Number:", iterator);
				printIsiRecordField("Registration Number:", iterator);
				printIsiRecordField("Expected Date of Delivery:", iterator);
				printIsiRecordField("Approver Id:", iterator);
				printIsiRecordField("Original Transfer Fee Excluding Tax:", iterator);
				printIsiRecordField("Total Discount Amount:", iterator);
				printIsiRecordField("Total Fee Amount:", iterator);
				printIsiRecordField("Tax Amount:", iterator);
				printIsiRecordField("Promotion Code Count:", iterator);
				printIsiRecordField("Receiver's Country Address:", iterator);
			
			} else if (recordType.equals(MainPanelDelegate.clBillPayment)) {
				formatIsiRecordField("ISI Record Type:", "Bill Payment (" + recordType + ")");
				dumpIsiHeader(iterator);
				printIsiRecordField("Destination Country:", iterator);
				printIsiRecordField("Sender First Name:", iterator);
				printIsiRecordField("Sender Middle Name:", iterator);
				printIsiRecordField("Sender Last Name:", iterator);
				printIsiRecordField("Sender Home Phone:", iterator);
				printIsiRecordField("Sender Address:", iterator);
				printIsiRecordField("Sender City:", iterator);
				printIsiRecordField("Sender State:", iterator);
				printIsiRecordField("Sender Zip Code:", iterator);
				printIsiRecordField("Sender Country:", iterator);
				printIsiRecordField("Sender Date of Birth:", iterator);
				printIsiRecordField("Sender Photo Id Type:", iterator);
				printIsiRecordField("Sender Photo Id Number:", iterator);
				printIsiRecordField("Sender Photo Id State:", iterator);
				printIsiRecordField("Sender Photo Id Country:", iterator);
				printIsiRecordField("Sender Legal Id Type:", iterator);
				printIsiRecordField("Sender Legal Id Number:", iterator);
				printIsiRecordField("Bank account number:", iterator);
				printIsiRecordField("Sender Occupation:", iterator);
				printIsiRecordField("Agency id:", iterator);
				printIsiRecordField("Agency account number:", iterator);
				printIsiRecordField("Agency Name:", iterator);
				printIsiRecordField("Agency City:", iterator);
				printIsiRecordField("Message 1:", iterator);
				printIsiRecordField("Message 2:", iterator);
				printIsiRecordField("Third Pary First Name:", iterator);
				printIsiRecordField("Third Pary Middle Name:", iterator);
				printIsiRecordField("Third Pary Last Name:", iterator);
				printIsiRecordField("Third Pary Address:", iterator);
				printIsiRecordField("Third Pary City:", iterator);
				printIsiRecordField("Third Pary State:", iterator);
				printIsiRecordField("Third Pary Country:", iterator);
				printIsiRecordField("Third Pary Zip Code:", iterator);
				printIsiRecordField("Third Pary Legal ID Type:", iterator);
				printIsiRecordField("Third Pary Legal Id Number:", iterator);
				printIsiRecordField("Third Pary Date of Birth:", iterator);
				printIsiRecordField("Third Pary Occupation:", iterator);
				printIsiRecordField("Third Pary Organization:", iterator);
				printIsiRecordField("Money Saver ID:", iterator);
				printIsiRecordField("Reference Number:", iterator);
				printIsiRecordField("Send Amount:", iterator);
				printIsiRecordField("Fee Total with Tax:", iterator);
				printIsiRecordField("Total Send Amount:", iterator);
				printIsiRecordField("Send Currency:", iterator);
				printIsiRecordField("Exchange Rate:", iterator);
				printIsiRecordField("Bill Payment Type:", iterator);
				printIsiRecordField("Receive Agent Id:", iterator);
				printIsiRecordField("Beneficiary First Name:", iterator);
				printIsiRecordField("Beneficiary Middle Name:", iterator);
				printIsiRecordField("Beneficiary Last Name:", iterator);
				printIsiRecordField("Purpose of Funds:", iterator);
				
			} else if (recordType.equals(MainPanelDelegate.clMoneyGramReceive)) {
				formatIsiRecordField("ISI Record Type:", "MoneyGram Receive (" + recordType + ")");
				dumpIsiHeader(iterator);
				printIsiRecordField("Reference Number:", iterator);
				printIsiRecordField("Agent Check Number:", iterator);
				printIsiRecordField("Agent Amount:", iterator);
				printIsiRecordField("Authorization Code:", iterator);
				printIsiRecordField("Customer Check Number:", iterator);
				printIsiRecordField("Customer Amount:", iterator);
				printIsiRecordField("Currency", iterator);
				printIsiRecordField("Sender First Name:", iterator);
				printIsiRecordField("Sender Middle Name:", iterator);
				printIsiRecordField("Sender Last Name:", iterator);
				printIsiRecordField("Receiver First Name:", iterator);
				printIsiRecordField("Receiver Middle Name:", iterator);
				printIsiRecordField("Receiver Last Name:", iterator);
				printIsiRecordField("Receiver Second Last Name:", iterator);
				printIsiRecordField("Receiver Phone Number:", iterator);
				printIsiRecordField("Receiver Address:", iterator);
				printIsiRecordField("Receiver City:", iterator);
				printIsiRecordField("Receiver State:", iterator);
				printIsiRecordField("Receiver Postal Code:", iterator);
				printIsiRecordField("Receiver ID 1 Type:", iterator);
				printIsiRecordField("Receiver ID 1 Number:", iterator);
				printIsiRecordField("Receiver ID 1 State:", iterator);
				printIsiRecordField("Receiver ID 1 Country:", iterator);
				printIsiRecordField("Receiver ID 2 Type:", iterator);
				printIsiRecordField("Receiver ID 2 Number:", iterator);
				printIsiRecordField("Receiver Date of Birth:", iterator);
				printIsiRecordField("Receiver Occupation:", iterator);
				printIsiRecordField("Third Pary Receiver First Name:", iterator);
				printIsiRecordField("Third Pary Receiver Last Name:", iterator);
				printIsiRecordField("Third Pary Receiver Address:", iterator);
				printIsiRecordField("Third Pary Receiver City:", iterator);
				printIsiRecordField("Third Pary Receiver State:", iterator);
				printIsiRecordField("Third Pary Receiver Postal Code:", iterator);
				printIsiRecordField("Third Pary Receiver Country:", iterator);
				printIsiRecordField("Third Pary Receiver ID 2 Type:", iterator);
				printIsiRecordField("Third Pary Receiver ID 2 Number:", iterator);
				printIsiRecordField("Third Pary Receiver Date of Birth:", iterator);
				printIsiRecordField("Third Pary Receiver Occupation:", iterator);
				printIsiRecordField("Third Pary Receiver Organization:", iterator);
				printIsiRecordField("City of Birth:", iterator);
				printIsiRecordField("Country of Birth:", iterator);
				printIsiRecordField("Receiver ID Date of Issue:", iterator);
				printIsiRecordField("Receiver ID City of Issue:", iterator);
				printIsiRecordField("Receiver ID Country of Issue:", iterator);
				printIsiRecordField("Originating Country:", iterator);
				printIsiRecordField("Check 1 Type:", iterator);
				printIsiRecordField("Check 2 Type:", iterator);
				printIsiRecordField("Original Send Amount:", iterator);
				printIsiRecordField("Original Send Fee:", iterator);
				printIsiRecordField("Original Send Currency:", iterator);
				printIsiRecordField("Original Exchange Rate:", iterator);
				printIsiRecordField("Other Payout Type:", iterator);
				printIsiRecordField("Other Payout Amount:", iterator);
				printIsiRecordField("Other Payout Swiped:", iterator);
				printIsiRecordField("Other Payout ID (Last 4 Digits):", iterator);
				printIsiRecordField("Sender's Address:", iterator);
				printIsiRecordField("Sender's City:", iterator);
				printIsiRecordField("Sender's State:", iterator);
				printIsiRecordField("Sender's Zip Code:", iterator);
				printIsiRecordField("Sender's Country:", iterator);
				printIsiRecordField("Sender's Second Last Name:", iterator);
				printIsiRecordField("Sender's Home Phone:", iterator);
				printIsiRecordField("Receiver's Message:", iterator);
				printIsiRecordField("Receiver's Address Country:", iterator);
			
			} else if (recordType.equals(MainPanelDelegate.clPromoCodeData)) {
				formatIsiRecordField("ISI Record Type:", "Promo Code Data (" + recordType + ")");
				printIsiRecordField("Record Type:", iterator);
				printIsiRecordField("Promotional Code:", iterator);
				printIsiRecordField("Promotion Category ID:", iterator);
				printIsiRecordField("Promotion Discount ID:", iterator);
				printIsiRecordField("Promotion Discount Value:", iterator);
				printIsiRecordField("Promotion Discount Amount:", iterator);
			
			} else if (recordType.equals(MainPanelDelegate.clAmend)) {
				formatIsiRecordField("ISI Record Type:", "Amend (" + recordType + ")");
				dumpIsiHeader(iterator);
				printIsiRecordField("Reference Number:", iterator);
				printIsiRecordField("Previous Receivers's First Name:", iterator);
				printIsiRecordField("Previous Receivers's Last Name:", iterator);
				printIsiRecordField("Previous Receivers's Second Last Name:", iterator);
				printIsiRecordField("Receivers's First Name:", iterator);
				printIsiRecordField("Receivers's Last Name:", iterator);
				printIsiRecordField("Receivers's Second Last Name:", iterator);
			
			} else if (recordType.equals(MainPanelDelegate.clMoneyGramReceiveReversal)) {
				formatIsiRecordField("ISI Record Type:", "Money Gram Receive Reversal (" + recordType + ")");
				dumpIsiHeader(iterator);
				printIsiRecordField("Reference Number:", iterator);
				printIsiRecordField("Send Amount:", iterator);				
				printIsiRecordField("Fee Amount:", iterator);				
				printIsiRecordField("Fee Refunded:", iterator);
			
			} else if (recordType.equals(MainPanelDelegate.clMGSendDetailedAmounts)) {
				formatIsiRecordField("ISI Record Type:", "Send Detailed Amounts (" + recordType + ")");
				dumpIsiHeader(iterator);
				printIsiRecordField("Send Amount:", iterator);
				printIsiRecordField("Send Currency:", iterator);
				printIsiRecordField("Total Send Fees:", iterator);
				printIsiRecordField("Total Discount Amount:", iterator);
				printIsiRecordField("Total Send Taxes:", iterator);
				printIsiRecordField("Total Amount to Collect:", iterator);
				printIsiRecordField("Fee Send MGI Original No Tax:", iterator);
				printIsiRecordField("Fee Send Non MGI:", iterator);
				printIsiRecordField("Total Receive Taxes:", iterator);
				printIsiRecordField("Fee Send Total Orig No Tax:", iterator);
				printIsiRecordField("Fee Send Total Final No Tax:", iterator);
				printIsiRecordField("Tax Send MGI:", iterator);
				printIsiRecordField("Tax Send Non MGI:", iterator);
				printIsiRecordField("Fee Send MGI Collected and Tax:", iterator);
				printIsiRecordField("Total Amount MGI:", iterator);
				printIsiRecordField("Fee Send Total with Tax:", iterator);
				printIsiRecordField("Fee Send Non MGI with Tax:", iterator);
				printIsiRecordField("Receive Amount Original:", iterator);
				printIsiRecordField("Receive Currency:", iterator);
				printIsiRecordField("Valid:", iterator);
				printIsiRecordField("Payout Currency:", iterator);
				printIsiRecordField("Fee Receive Total:", iterator);
				printIsiRecordField("Tax Receive Total:", iterator);
				printIsiRecordField("Receive Amount Final:", iterator);
				printIsiRecordField("Fee Receive MGI:", iterator);
				printIsiRecordField("Fee Receive Other:", iterator);
				printIsiRecordField("Tax Receive MGI:", iterator);
				printIsiRecordField("Tax Recieve Other:", iterator);
				printIsiRecordField("Foreign Exchange:", iterator);
				printIsiRecordField("Has Fee Disclosure:", iterator);
				printIsiRecordField("Has Tax Disclosure:", iterator);
				printIsiRecordField("Has Estimated Fee:", iterator);
				printIsiRecordField("Has Estimated Tax:", iterator);
			
			} else if (recordType.equals(MainPanelDelegate.clSendReceiptText1)) {
				formatIsiRecordField("ISI Record Type:", "Send Receipt Text 1 (" + recordType + ")");
				dumpIsiHeader(iterator);
				printIsiRecordField("Receipt 1 Language:", iterator);
				printIsiRecordField("Receipt 1 Text:", iterator);
				printIsiRecordField("Receipt 2 Language:", iterator);
				printIsiRecordField("Receipt 2 Text:", iterator);
			
			} else if (recordType.equals(MainPanelDelegate.clSendReceiptText2)) {
				formatIsiRecordField("ISI Record Type:", "Send Receipt Text 2 (" + recordType + ")");
				dumpIsiHeader(iterator);
				printIsiRecordField("Receipt 1 Language:", iterator);
				printIsiRecordField("Receipt 1 Text:", iterator);
				printIsiRecordField("Receipt 2 Language:", iterator);
				printIsiRecordField("Receipt 2 Text:", iterator);
			
			} else if (recordType.equals("CNL")) {
				formatIsiRecordField("ISI Record Type:", "Cancel (" + recordType + ")");
				dumpIsiHeader(iterator);
				printIsiRecordField("Sub-Status Code:", iterator);
				
			} else if (recordType.equals("RFN")) {
				formatIsiRecordField("ISI Record Type:", "Send Refund (" + recordType + ")");
				dumpIsiHeader(iterator);
				printIsiRecordField("Reference Number:", iterator);
				printIsiRecordField("Send Amount:", iterator);
				printIsiRecordField("Fee Amount:", iterator);
				printIsiRecordField("Fee Refunded:", iterator);
				printIsiRecordField("Check 1 Type:", iterator);
				printIsiRecordField("Check 2 Type:", iterator);
				
			} else if (recordType.equals("CAN")) {
				formatIsiRecordField("ISI Record Type:", "Send Cancel (" + recordType + ")");
				dumpIsiHeader(iterator);
				printIsiRecordField("Reference Number:", iterator);
				printIsiRecordField("Send Amount:", iterator);
				printIsiRecordField("Fee Amount:", iterator);
				printIsiRecordField("Fee Refunded:", iterator);
				
			} else if (recordType.equals("DOC")) {
				formatIsiRecordField("ISI Record Type:", "Money Order Printed (" + recordType + ")");
				dumpIsiHeader(iterator);
				printIsiRecordField("Document Type:", iterator);
				printIsiRecordField("Document Number:", iterator);
				printIsiRecordField("Void:", iterator);
				printIsiRecordField("Limbo Flag:", iterator);
				printIsiRecordField("Period:", iterator);
				printIsiRecordField("Serial Number:", iterator);
				printIsiRecordField("Item Face Amount:", iterator);
				printIsiRecordField("Item Fee:", iterator);
				printIsiRecordField("Remote Issuance Flag:", iterator);
				printIsiRecordField("Sale Issue Tag:", iterator);
				printIsiRecordField("Face Discount Amount:", iterator);
				printIsiRecordField("Fee Discount Amount:", iterator);
				printIsiRecordField("Face Discount Percentage:", iterator);
				printIsiRecordField("Fee Discount Percentage:", iterator);
				printIsiRecordField("Payee Number:", iterator);
				printIsiRecordField("Payee Name:", iterator);
				printIsiRecordField("Purchaser:", iterator);
				printIsiRecordField("Reference Number:", iterator);
			
				
			} else if (recordType.equals("NON")) {
				formatIsiRecordField("ISI Record Type:", "Non-Financial (" + recordType + ")");
				dumpIsiHeader(iterator);
					
			} else if (recordType.equals("ERR")) {
				formatIsiRecordField("ISI Record Type:", "Non-Financial (" + recordType + ")");
				dumpIsiHeader(iterator);
				printIsiRecordField("Sub-Status Code:", iterator);
					
			} else {
				Debug.println("ISI Record Type: Unknown (" + recordType + ")");
			}
			
			while (iterator.hasNext()) {
				printIsiRecordField("* Extra *", iterator);
			}
        }
	}
}
