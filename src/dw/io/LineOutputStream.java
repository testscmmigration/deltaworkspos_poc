package dw.io;

import java.io.ByteArrayOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import dw.io.diag.ByteArray;
import dw.utility.Debug;

/**
 * Inserts a carriage return in front of any linefeeds that are not 
 * already preceeded by one. Intended for use with classes like
 * PrintStream and RotatingOutputStream to insure that the output
 * consistently uses DOS style line terminators (CRLF) instead of 
 * Unix style (LF only) or a mixture (which is what we were getting.)
 * This class will not correctly filter a stream containing 
 * multibyte characters (a subclass of FilterReader would be needed 
 * for that) but it is suitable for our debug log file.
 *
 * @author Boris Makhlin
 * @author Geoff Atkin
 */
public class LineOutputStream extends FilterOutputStream {

    private int previous = 0;

    /** 
     * Creates a LineOutputStream which filters the given 
     * output stream.
     */
    public LineOutputStream(OutputStream out) {
        super(out);
    }
    
    /**
     * Compares the character being written with the previous one.
     * If this character is a linefeed and the previous character
     * was not a carriage return, add a carriage return before this 
     * character.
     *
     * Implementation note: OutputStream defines three variations
     * of the write method, but our superclass FilterOutputStream
     * implements the other two by calling this one. So we only need
     * to implement one write method, as long as it is this one.
     */
    @Override
	public void write(int b) throws IOException {
        if ((b == '\n') && (previous != '\r'))
            out.write('\r');

        previous = b;
        out.write(b);
    }

    /** For testing. */
    public static void main(String args[]) {
        ByteArrayOutputStream baos1, baos2;
        PrintStream ps1, ps2;
        String testdata = "hi\n" + "there";  
        
        // first see what we get without using this class
        
        baos1 = new ByteArrayOutputStream();
        ps1 = new PrintStream(baos1);
        ps1.println(testdata);

        Debug.println("Unfiltered:");	
        Debug.println(ByteArray.hex(baos1.toByteArray()));

        // now see what this class does when we chain it in
        
        baos2 = new ByteArrayOutputStream();
        ps2 = new PrintStream(new LineOutputStream(baos2));
        ps2.println(testdata);
        ps2.close();

        Debug.println("Filtered:");	
        Debug.println(ByteArray.hex(baos2.toByteArray()));
    }
}
