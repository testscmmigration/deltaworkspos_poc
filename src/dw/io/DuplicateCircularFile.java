package dw.io;

import java.io.IOException;

import dw.utility.Debug;

/** 
 * Container for two CircularFile objects, a primary and a backup,
 * that are supposed to be in sync with each other. Implemented by
 * passing the arguments to the corresponding methods in super and
 * backupFile. Methods that return a value will return the value
 * from the invocation on super. 
 *
 * Invocations have not been performed on the backupFile when it has been
 * determined that no side effects will occur. 
 *
 * All methods that were synchronized in CircularFile are synchronized to
 * ensure that the same operations are performed on the primary file as on the 
 * backup file. This may be relaxed if it is determined
 * to be unnecessary. 
 *
 * We also implemented a DuplicateCircularStreamImpl class that generates an
 * input stream from this input stream but also advances pointers and so forth
 * as necessary in the backupFile.
 * @author Francis Fung 10/3/2000
 * 10/4/2000 Removed faulty overloaded static create methods
 * 10/5/2000 Changed signature of getFullThreshhold to expose to interface
 * 10/13/2000 fixed bugs where mark and read methods of the input stream 
 * were not updating
 * the backup input stream properly
 * 10/17/2000 Changed flush() method so that primary file gets flushed first
 */
class DuplicateCircularFile extends CircularFile {

    private CircularFile primaryFile;
    private CircularFile backupFile;

    public DuplicateCircularFile(CircularFile primaryFile, CircularFile backupFile) {
        this.primaryFile = primaryFile;
        this.backupFile = backupFile;
    }


    @Override
	public synchronized void clear() throws IOException  {
        primaryFile.clear();
        backupFile.clear();
    }
   
    @Override
	public synchronized void close()  throws IOException {
        primaryFile.close();
        backupFile.close();
    }
    
    @Override
	public synchronized void flush() throws IOException  {
        primaryFile.flush();
        backupFile.flush();
        
    }
    
    @Override
	public long freeSpace() {
        return primaryFile.freeSpace();
    } 
    
    @Override
	public long getEnd() {
        return primaryFile.getEnd();       
    }

    @Override
	protected long getFullThreshhold() {
        return primaryFile.getFullThreshhold();    
    }

    /* DuplicateCircularInputStreamImpl is a member class and so it has 
       access to members of DuplicateCircularFile including backupFile, 
       which it needs to modify in order to keep its markers in sync with 
       the markers of this object.
    */
    @Override
	public CircularInputStream getInputStream() {
        return new DuplicateCircularInputStreamImpl();
    }
    
    @Override
	public synchronized long getStart() {
        return primaryFile.getStart();
    }
    
    @Override
	public boolean isEmpty() {
        return primaryFile.isEmpty();
    }
    
    @Override
	public boolean isLocked() {
        return primaryFile.isLocked();
    }
    
    @Override
	public boolean isNearlyFull() {
    	return primaryFile.isNearlyFull();
    }

    @Override
	public synchronized boolean lock() {
        return primaryFile.lock();
    }
    
    @Override
	public synchronized void unLock() {
        primaryFile.unLock();
    }
    
    @Override
	public synchronized boolean rewind() {
        return primaryFile.rewind();
    }
    
    @Override
	public synchronized void setStart(long newPosition) throws IOException  {
    	primaryFile.setStart(newPosition);
    	backupFile.setStart(newPosition);
    }
    
    @Override
	public synchronized void write(byte[] b) throws IOException {
    	primaryFile.write(b);
    	backupFile.write(b);
    }
    @Override
	public synchronized void write(byte[] b, int off, int len) throws IOException {
    	primaryFile.write(b, off, len);
    	backupFile.write(b, off, len);
    }
    
    @Override
	public synchronized void write(int b) throws IOException {
    	primaryFile.write(b);
    	backupFile.write(b);
    }
    
    @Override
	public synchronized void write(java.lang.String s) throws IOException {
    	primaryFile.write(s);
    	backupFile.write(s);
    }

   /**
    * Implementation of CircularInputStream returned by getInputStream.
    * This is a member class (a non-static inner class) of DuplicateCircularFile
    * It needs to modify start and end markers of the primaryFile and backupFile
    * as well as using the length of the primaryFile.
    */
   private class DuplicateCircularInputStreamImpl extends CircularInputStream {                                       
       //Need to make sure that effects to the backupFile are synchronized
       
       private CircularInputStream primaryInputStream;
       private CircularInputStream backupInputStream;

       @Override
	public long getCurrent() { // getter for debugging
           return primaryInputStream.getCurrent();
       } 
       @Override
	public long getMark() { // getter for debugging
           return  primaryInputStream.getMark();
       } 
                                                                                  
       /**
        * Creates a DuplicateCircularInputStreamImpl.
        */
       public DuplicateCircularInputStreamImpl() {                     
           primaryInputStream = primaryFile.getInputStream();
	   backupInputStream = backupFile.getInputStream();	   
       }
       
       /**
        * Computes the number of bytes available for reading. This is the
        * amount of data between the current position and the end of the
        * data. However, there is no guarantee that a read will not block.
        * Does not alter any markers so no sync necessary.
        * note that long_available() is not reimplemented since it is only 
	* in available of CircularInputSrream      
        */
       @Override
	public int available() { 
            try {
                //backup called in case of side effects
                backupInputStream.available();
                return primaryInputStream.available(); 
            }
           catch (IOException e) {
               Debug.println("This should never happen since CircularInputStream's methods does not throw this exception!");	
               return 0;
           }
       }                                                                              
       /**
        * Closes this input stream. The current implementation does nothing.
        * Does not alter any markers, so no sync necessary.
        */
       @Override
	public void close() {
       }
       
       /**
        * Sets a bookmark at the current position.                             
        */  
       @Override
	public synchronized void mark(int readLimit) {                                
           primaryInputStream.mark(readLimit); 
	   backupInputStream.mark(readLimit);
       }
       
       /**
        * Indicates whether the mark method is implemented. Returns true.
        * Does not alter any markers, so no sync necessary.
        */
       @Override
	public boolean markSupported() {
           return true;
       }
       
       /**
        * Sets the current position to the previously bookmarked position.
        */
       @Override
	public synchronized void reset() {
	    try {
		primaryInputStream.reset();
		backupInputStream.reset();
	    }
	    catch (IOException e){
                Debug.println("This should never happen since CircularInputStream's methods does not throw this exception!"); 	
            }
	} 
       
       /**
        * Tells the underlying CircularFile to move the start pointer to
        * the bookmarked position. Should only be called when we are done with
        * the data read so far.
	* Requires setting start on primaryFile and on backupFile.
	* Synchronizing so that mark doesn't change in the middle.
	* Note to FF: Ensure that this synchronization is necessary and 
	* is effective.
        */
       @Override
	public synchronized void clearToMark() throws IOException {
	   primaryInputStream.clearToMark();
	   backupInputStream.clearToMark();
	   
	   Debug.println("Marks should be equal: " + primaryInputStream.getMark()	
                   + " and " + backupInputStream.getMark()); 
       } 
       
       /* The clear() method sets the start marker of the CircularFile, so
	* it also has to set the marker of the backupFile
	* Synchronized so that mark doesn't change in the middle
	* note that it must change the current and the mark markers, so it has
	* to change the state of the backupInputStream.
	*/
       @Override
	public synchronized void clear() throws IOException {   
           primaryInputStream.clear();
	   backupInputStream.clear();
       }   
       
       @Override
	public synchronized void clearTo(long position) throws IOException {   
           primaryInputStream.clearTo(position);
	   backupInputStream.clearTo(position);
       }

       @Override
	public synchronized void reopen() { 
           primaryInputStream.reopen();
	   backupInputStream.reopen();
       }           
       
       /** Reads a single byte.
        * @return the byte read, or -1 if at end of file.
	* uses other read method
	*/
       @Override
	public int read() throws IOException { 
	   backupInputStream.read();
	   return primaryInputStream.read();
       }

       /**
        * Reads an array of bytes.
        * @return the number of bytes read, or -1 if at end of file.        
        */
       @Override
	public int read(byte[] b) throws IOException {
	   return this.read(b, 0, b.length);     
       }

       /**                                            
        * Reads len bytes into an array of bytes.    
	*                      
        * Since this is an InputStream, attempts to read more data than
	* available should read as much as possible, then return the number
        * of bytes read. This is different from RandomAccessFile, which        
        * throws EOFException. This method handles the case where a read  
        * has to loop around from the end of the file to the beginning, 
	* so an EOFException will not be thrown unless there is an error  
	* in the implementation. 
        *
        * If len is 0, nothing is done and 0 is returned.              
        * If no data is available, -1 is returned.                     
        *
        * However, if some other condition prevents the expected amount
        * of data from being read, this method throws an IOException.
        * Since SimpleRandomAccessFile hides the pointer of RandomAccessFile,
	* there is no need to advance any read pointers in backupFile. 
	* We create a new array so that we can perform the same read in the
	* backup file, in order to sync the pointers correctly.
        */
        @Override
		public int read(byte[] b, int off, int len) throws IOException {
            byte[] backupb = new byte[b.length];
            backupInputStream.read(backupb, off, backupb.length);
            return primaryInputStream.read(b, off, len);
        }
        
        /**
         * Attempts to skip over n bytes of input. May skip fewer bytes
         * if fewer bytes are available. Never throws an exception.
         * If n is negative, no bytes are skipped.
         * @return the number of bytes skipped.
         */
        @Override
		public long skip(long n) {
            try {
            	long c = backupInputStream.skip(n);
            	if (n != c) {
            		Debug.println("Skipped " + c + " when expected to skip " + n);
            	}
            	return primaryInputStream.skip(n);
            } catch (Exception e) {
                Debug.println("This should never happen since CircularInputStream's methods does not throw this exception!");	
                return 0;
            }
        }
    }
}                                                                                                         
