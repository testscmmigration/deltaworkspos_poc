/*
 * State.java
 * 
 * $Revision$
 *
 * Copyright (c) 1999-2004 MoneyGram International
 */

package dw.io;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import dw.profile.UnitProfile;
import dw.profile.UnitProfileInterface;
import dw.utility.Debug;
import dw.utility.ExtraDebug;

/**
 * Stores application state. Includes miscellaneous values that change too
 * often to sensibly be part of the profile, and the starting and ending
 * pointers for all the circular files.
 *
 * @author Geoffrey Atkin
 * @author Francis Fung (modifications for duplicate backups)
 */
public class State {

    // the only reason this isn't private is so the state command can use it
    EncryptedRandomAccessFile stateFile;

    private static volatile State instance;
    private CircularFile profileFile;
//    private static CircularFile itemFile;
//    private static CircularFile diagFile;
//    private static CircularFile reportFile;
//    private static CircularFile dreportFile;
//    private static CircularFile tempFile;
//    private static CircularFile cciFile;

/*
   The state.dat file can be thought of as one big fixed-length record, with
   different persistent values stored at different offsets in the file, as
   follows:

     0 last check-in time        long              8
    24 current period            byte              1

   104 last daily xmit time      long              8
   112 last attempted d.x.t.     long              8
   120 last dispenser check      long              8
   128 last reauth time          long              8
   136 last daily total reset    long              8
   144 temp log created flag     byte              1
   145 trace file cleared flag   byte              1
   146 unused?
   147 cci log created flag      byte              1
   148 superagent profile id     int               4
   152 padding                                    72

   space for ten circular file states
     0  start pointer            long              8
     8  end pointer              long              8
    16  md length                byte              1
    17  message digest           byte[]           31 (max)
    48

   224 first (profile)          key 0
   272 second (item)            key 1
   320 third (diag)             key 2   no longer used
   368 fourth (report detail)   key 3
   416 fifth (report summary)   key 4   no longer used
   464 sixth (warehouse)
   512 seventh (item backup)    key 5
   560 eighth (diag backup)     key 6   no longer used
   608 ninth (temp log)         key 7
   656 tenth (CCI log)          key 8

   space for two dispenser states
   (since dispenser 2 hasn't been used yet, can still
   increase the length of single dispenser state)
   
     0  next serial number      long             8
     8  remaining count         long             8
    16  first used pair         long,long       16
    32  second                                  16
    48  third                                   16
    64  fourth                                  16
    80  fifth                                   16
    96 

   704 dispenser 1 state
   800 (reserved for dispenser 2 state)
   896 padding

   1024 assorted totals
   2048 warehouse profile


*/

    public static final int LAST_CHECK_IN             = 0;
    //public static final int STARTING_SERIAL           = 8;
    public static final int THERMAL_DISABLE_LINE_COMPRESSION    = 8;
    //public static final int ENDING_SERIAL             = 16;
    public static final int CURRENT_PERIOD            = 24;
    public static final int SAR_COUNTER               = 32;
    //public static final int DAILY_TOTAL               = 28;
    //public static final int BATCH_TOTAL_1             = 40;
    //public static final int BATCH_TOTAL_2             = 56;
    //public static final int BATCH_TOTAL_3             = 72;
    //public static final int BATCH_TOTAL_4             = 88;
    //public static final int BATCH_COUNT_1             = 52;
    //public static final int BATCH_COUNT_2             = 68;
    //public static final int BATCH_COUNT_3             = 84;
    //public static final int BATCH_COUNT_4             = 100;
    public static final int FORCE_TRANSMIT            = 40;
    public static final int LAST_DAILY_XMIT           = 104;
    public static final int LAST_ATTEMPTED_DAILY_XMIT = 112;
    public static final int LAST_DISPENSER_CHECK      = 120;
    public static final int LAST_REAUTH               = 128;
    public static final int LAST_DAILY_TOTAL_RESET    = 136;
    public static final int TEMP_LOG_CREATED          = 144;
    public static final int TRACE2_CLEARED            = 145;
	// public static final int CTR_LOG_CREATED        = 146;
    public static final int CCI_LOG_CREATED           = 147;
    public static final int SUPERAGENT_PROFILE_ID     = 148;

    public static final int PROFILE_ID				  = 156;
    public static final int DEVICE_ID				  = 164;
    
    public static final int CIRCULAR_PROFILE          = 224;
    public static final int CIRCULAR_ITEM             = 272;
	// public static final int CIRCULAR_DIAG          = 320;

    public static final int CIRCULAR_REPORT           = 368;
    // public static final int CIRCULAR_DIAG_REPORT   = 416;
    public static final int CIRCULAR_WAREHOUSE        = 464;

    //new locations for backup logs
    public static final int CIRCULAR_ITEM_BAK         = 512;
    // public static final int CIRCULAR_DIAG_BAK      = 560;

    // new temp log
    public static final int CIRCULAR_TEMP             = 608;

    // new CTR log
    public static final int CIRCULAR_CCI              = 656;

    public static final int DISPENSER_1               = 704;
    public static final int DISPENSER_2               = 800;

    public static final int BATCH_TOTAL_LENGTH        = 16;
    public static final int TOTAL_START               = 1024;   
    
   public static final int LAST_DAILY_BILL_PAY_TOTAL_RESET    = 2000;
     
    public static final int WAREHOUSE_START           = 2048;

    /**
      * Convert an array of bytes into a long.
      */
    static long decodeLong(byte[] a) throws IOException {
        DataInputStream in = new DataInputStream(
            new ByteArrayInputStream(a));
        return in.readLong();
    }

    /**
      * Construct a state object using the file of the given name.
      * State is supposed to be a singleton; classes other than
      * the main startup should use getInstance instead.
      */

    public State(String filename) throws IOException {
        if (instance != null) {
            throw new IOException("State file is already open"); 
        }
        stateFile = new EncryptedRandomAccessFile(filename,
                "rw", decodeLong(key1[0])); 
        instance = this;
    }

    /**
      * Construct a state object using the default file name state.dat.
      */
    public State() throws IOException {
        this("state.dat"); 
    }

    /** Returns a previously-created instance of State. */
    public static State getInstance() {
        try {
            if (instance == null) {
                instance = new State();
            }
        }
        catch (IOException e) {
            instance = null;
        }
        return instance;
    }




    /* Set by createCircularFileImpl, used by killFile. */
    EncryptedRandomAccessFile eraf;

    /**
      * Create an encrypted, secure circular file that records its
      * state changes with this state object.
      * @param name filename for the circular file
      * @param fp location in the state file where state is saved
      * @param key number identifying what key data to use
      * @throws AuthenticationException if the newly constructed circular
      * file does not match the message digest saved in the state file.
      * This could indicate tampering or a corrupted file.
      * Modified to create a DuplicateCircularFile
      * uses same start and end parameters for primary and backup
      *
      */
    public SecureCircularFile createCircularFileImpl(final String name,
            final int fp, int kb, int key)
            throws AuthenticationException, IOException {

        long start, end;
        MessageDigest md=null;
        SecureCircularFile scf=null;

        start = stateFile.readLong(fp);
        end = stateFile.readLong(fp + 8);
        
        ExtraDebug.println("Start and end from state.dat for " + name + " are " +start + " and " + end);
        
        eraf = new EncryptedRandomAccessFile(name, "rw", 
                decodeLong(key1[key]));
        
        ExtraDebug.println("Length of eraf for " +name + " is " + eraf.length());	
        
        if (eraf.length() == 0) {
            Debug.println("Detected length 0 ERAF and filling new ERAF for " + name);	
            fill(eraf, kb);
            //Debug.println("Finished filling new ERAF for " + name);	
        }
        
        //if the physical file has the wrong length, throw exception
        if (eraf.length() != kb* 1024) {
            throw new AuthenticationException("Incorrect physical file length"); 
        }
        
        //Debug.println("Starting to create SecureCircularFile for " + name);	
        try {
            scf = new SecureCircularFile(eraf, start, end, key2[key]);
            ExtraDebug.println("Created SecureCircularFile for " + name);	
            md = scf.getMessageDigest();
            //Debug.println("Got Message Digest for " + name);	
            
        }
        catch (NoSuchAlgorithmException e) {
            throw new AuthenticationException("No such algorithm: " + 
                    e.getMessage());
        }
        catch (CloneNotSupportedException e) {
            throw new AuthenticationException("Clone not supported: " + 
                    e.getMessage());
        }

        // compare md to the one saved in stateFile
        //Debug.println("about to read from stateFile at address: " + fp + " note stateFile has length: " + stateFile.length());	
        
        final  byte stateDigestLength = stateFile.readByte(fp + 16);
        // sanity check
        if (stateDigestLength > 32) {
            throw new AuthenticationException("Message digest length corrupted"); 
        }
        
        ExtraDebug.println("got a state digest length of: " + stateDigestLength);	
        final byte[] stateDigest = new byte[stateDigestLength];
        final int bytesRead = stateFile.read(stateDigest,0,stateDigestLength,fp + 17);
        //Debug.println("finished read from stateFile");	
        // See if the digest was read successfully from the state file
        
        if (bytesRead != stateDigestLength) {
            throw new AuthenticationException("Message digest corrupted"); 
        }
        
        final byte[] fileDigest = md.digest();
        
        if (stateDigestLength != fileDigest.length) {
            throw new AuthenticationException("Message digest length mismatch"); 
        }
        for (int i = 0; i < stateDigestLength; i++) {
            if (stateDigest[i] != fileDigest[i]) {
            	ExtraDebug.println("Message digest mismatch!!: ");	
            	ExtraDebug.println("stateDigest = " + hex(stateDigest));	
            	ExtraDebug.println("fileDigest = " + hex(fileDigest));	
                throw new AuthenticationException("Message digest mismatch"); 
            }
        }
        
        scf.addStateListener(new StateListener() {
            @Override
			public void stateChanged(byte[] newState, int off, int len)
                    throws IOException {
                stateFile.write(newState, off, len, fp);
                stateFile.flush();
                //Debug.println("StateListener event at : " + fp + " for file: " + name + ": " + hex(newState));
            }
        });
        return scf;
    }

    /*  Factored out the procedure to kill off the existing ERAF.
     *  Delete eraf and zero out the start and end pointers.
     */
    private void killFile(int fp, int key) throws IOException {
        eraf.setLength(0);
        eraf.close();
        writeLong(0L, fp);
        writeLong(0L, fp + 8);
        write(md[key],  fp + 16);
        //Debug.println("!!!!!!!!wrote 0s to addresses " + fp);	
    }

    /**
      * Create an encrypted, secure circular file that records its
      * state changes with this state object.
      * Wrapper for createCircularFileImpl. If that method throws
      * an AuthenticationException, this method resets the last
      * date/time check-in value, which forces a deauthorization,
      * uses the Error Manager to post an error message, deletes
      * the offending file, and tries again.
      *
      * @param name filename for the circular file
      * @param fp location in the state file where state is saved
      * @param key number identifying what key data to use
      */

    public CircularFile createCircularFile(final String name,
            final int fp, int kb, int key) throws IOException {

        CircularFile result;
        try {
            result = createCircularFileImpl(name, fp, kb, key);
            ExtraDebug.println("created CircularFile successfully for " + name + " at " + fp);	


        }
        catch (AuthenticationException e) {
            Debug.println("AuthenticationException: " + name);	
            try {

                //kill the underlying file
                killFile(fp, key);
                // force a deauth
                writeLong(1, LAST_CHECK_IN);
                
                // moved the below into killFile
                // reset so it matches next time
                //writeLong(0L, fp);
                //writeLong(0L, fp + 8);
                //write(md[key],  fp + 16);
                //Debug.println("!!!!!!!!wrote 0s to addresses " + fp);	
                
            }
            catch (Exception e2) {
            }
            
            //ErrorManager.handle(e, "authenticationException",	
            //        ErrorManager.ERROR, true);
            //ErrorManager.setPosAuthorization(false);

            result = createCircularFileImpl(name, fp, kb, key);
        }
        
        return result;
    }


    /** Returns the name of the backup file associated to the
     *  supplied filename. Currently it merely appends ".bak".
     */

//    private static String getBackupFilename(String filename) {
//        return filename + ".bak"; 
//    }


    /**
      * Create an encrypted, secure circular file that records its
      * state changes with this state object and automatically backs
      * a backup copy of itself.
      * <p>
      * This method makes two calls to createCircularFileImpl.
      * If that method throws an AuthenticationException for one of
      * the two underlying circular files, this method deletes the
      * offending file and recreates it with good data from the other
      * file. If createCircularFileImpl throws an AuthenticationException
      * for both of the underlying circular files, this method deletes
      * them both, resets the last date/time check-in value to force a
      * deauthorization, and tries again.
      *
      * @param name filename for the circular file
      * @param name_bak filename for the backup circular file
      * @param fp location in the state file where state is saved
      * @param fp_bak location in the state file where the backup file's
      * state is saved
      * @param kb desired size of the file in kilobytes
      * @param key index into array of key data to use
      * @param key_bak index into array of key data to use for the backup
      */
    public synchronized DuplicateCircularFile createDuplicateCircularFile(
            final String name, final String name_bak, final int fp,
            final  int fp_bak, int kb, int key, int key_bak)
            throws IOException
        {
            CircularFileImpl primaryFile = null;
            CircularFileImpl backupFile = null;
            boolean primaryFileFailed = false;
            boolean backupFileFailed = false;
            
            try {
                primaryFile = createCircularFileImpl(name, fp, kb, key);
                //Debug.println("created primaryFile");	
            }
            catch (AuthenticationException e) {
                primaryFileFailed = true;
                killFile(fp, key);
                Debug.println("failed to create primary file");	
            }
            
            try {
                backupFile = createCircularFileImpl(name_bak, fp_bak, kb, key_bak);
                //Debug.println("created backupFile");	
            }
            catch (AuthenticationException e) {
                backupFileFailed = true;
                killFile(fp_bak, key_bak);
                Debug.println("failed to create backupFile");	
            }
            
            //Debug.println("primary file failed? : " + primaryFileFailed + " backup file failed? :" + backupFileFailed);	
            
            //if both passed, check to make sure their contents coincide. If not,
            //arbitrarily assume that the primary one is correct and backup file
            //is corrupt
            
            if (!(primaryFileFailed || backupFileFailed)) {
                //pull out inputStreams and compare; if different, use primary
                boolean comparedOK =  compareStreams(primaryFile.getInputStream(), backupFile.getInputStream());
                //Debug.println("streams compared OK?:" + comparedOK);	
                if (!comparedOK) {
                    backupFileFailed = true;
                    //the eraf reference is still the backup file
                    killFile(fp_bak, key_bak);
                }
            }
            
            if (primaryFileFailed && !backupFileFailed) {
                //use the backup start value as the primary start and end values
                //so the new primary file is empty, starting at the backup start value
                writeLong(readLong(fp_bak), fp);
                writeLong(readLong(fp_bak), fp+8);
                primaryFile = createCircularFileImpl(name, fp, kb, key);
                copyStream(backupFile.getInputStream(), primaryFile);
                
            }
            else if (backupFileFailed && !primaryFileFailed) {
                //use the primary start value as the backup start and end values
                //so the new backup file is empty, starting at the primary start value
                writeLong(readLong(fp), fp_bak);
                writeLong(readLong(fp), fp_bak+8);
                backupFile = createCircularFileImpl(name_bak, fp_bak, kb, key_bak);
                copyStream(primaryFile.getInputStream(), backupFile);
            }
            else if (primaryFileFailed && backupFileFailed) {
                //the physical files have been killed already
                // now we perform a de-auth
                writeLong(1, LAST_CHECK_IN);
                primaryFile = createCircularFileImpl(name, fp, kb, key);
                backupFile = createCircularFileImpl(name_bak, fp_bak, kb, key_bak);
            }
            
            return new DuplicateCircularFile(primaryFile, backupFile);
        }
    
    
    /**
     * Creates a circular file for the profile and loads it.
     */
    public CircularFile createProfile() throws IOException {
        profileFile = createProfileFile();
        if ((profileFile.getStart() == 0) && (profileFile.getEnd() == 0)) {
            copyWarehouse(profileFile);
        }
        UnitProfile.getInstance().load(profileFile);
        UnitProfile.getInstance().setStateSaver(this);
        return profileFile;
    }

    /** Creates a circular file for the profile. (key = 0) */
    public CircularFile createProfileFile() throws IOException {
        profileFile = createCircularFile("profile.dat", 
                CIRCULAR_PROFILE, 1024, 0);
        return profileFile;
    }

//    /** Creates a circular file for the item log. (key = 1, 5) */
//    public CircularFile createItemFile() throws IOException {
//  return itemFile = createDuplicateCircularFile("item.dat", 
//                getBackupFilename("item.dat"), 
//                CIRCULAR_ITEM, CIRCULAR_ITEM_BAK, 1024, 1, 5);
//
//       }

    /** Creates a circular file for the diagnostics log. (key = 2, 6) */
/*
    public CircularFile createDiagFile() throws IOException {
        return diagFile = createDuplicateCircularFile("diag.dat",
                getBackupFilename("diag.dat"),
                CIRCULAR_DIAG, CIRCULAR_DIAG_BAK, 1024, 2, 6);
    }
*/
    /** Creates a circular file for the report detail log. (key = 3)
     *  This is for the Deltaworks 2.6 rewrite.
     */
    public CircularFile createDetailFile() throws IOException {
        return createCircularFile("detail.dat", 
                CIRCULAR_REPORT, 2048, 3);
    }

    /** Creates a circular file for the summary report log. (key = 4)
     *  This is for the Deltaworks 2.6 rewrite.
     */
    // this circular file is no longer needed.
//    public CircularFile createSummaryFile() throws IOException {
//        return dreportFile = createCircularFile("summary.dat",
//                CIRCULAR_DIAG_REPORT, 1024, 4);
//    }

    /**
     * Creates a circular file for the temporary log. (key = 7)
     * This file is new in Deltaworks 2.6 so we have to take
     * special steps to prevent chaos when the file needs to
     * be created the first time.
     */
    public CircularFile createTempFile() throws IOException {
        byte tempFileCreated = readByte(TEMP_LOG_CREATED);
        if (tempFileCreated != 1) {
            writeLong(0L, CIRCULAR_TEMP);
            writeLong(0L, CIRCULAR_TEMP + 8);
            write(md[7],  CIRCULAR_TEMP + 16);
            writeByte(1, TEMP_LOG_CREATED);
        }
        return createCircularFile("tlog.dat", 
                CIRCULAR_TEMP, 64, 7);
    }

    /** 
     * Creates a circular file for the Customer Compliance Info log. (key = 8)
     * This file is new in Deltaworks 4.0 so we have to initialize
     * its state data when the file is created for the first time.
     * Estimate ??? kilobyte per CCI report, times 180 days, = 2048 KB.
     */
    public CircularFile createCCIFile() throws IOException {
        File ctrFile = new File("ctr.dat"); 
        if (ctrFile.exists()) {
            boolean b = ctrFile.delete();
            if (! b) {
            	Debug.println("File " + ctrFile.getName() + " could not be deleted");
            }
        }
            
        byte cciFileCreated = readByte(CCI_LOG_CREATED);
        if (cciFileCreated != 1) {
            writeLong(0L, CIRCULAR_CCI);
            writeLong(0L, CIRCULAR_CCI + 8);
            write(md[8], CIRCULAR_CCI + 16);
            writeByte(1, CCI_LOG_CREATED);
        }
        return createCircularFile("cci.dat",  
                CIRCULAR_CCI, 2048, 8);
    }

//    /** Returns a previously created circular file. */
//    public static CircularFile getProfileFile() { return profileFile; }
//
//    /** Returns a previously created circular file. */
//    public static CircularFile getItemFile() { return itemFile; }
//
//    /** Returns a previously created circular file. */
//    public static CircularFile getDiagFile() { return diagFile; }
//
//    /** Returns a previously created circular file. */
//    public static CircularFile getReportFile() { return reportFile; }
//
//    /** Returns a previously created circular file. */
//    public static CircularFile getDReportFile() { return dreportFile; }
//
//    /** Returns a previously created circular file. */
//    public static CircularFile getTempFile() { return tempFile; }
//
//    /** Returns a previously created circular file. */
//    public static CircularFile getCCIFile() { return cciFile; }

    /** Sets the start and end pointers on all the circular files to
      * zero, thereby clearing all data. The profile file is excluded
      * and left untouched. Note that the higher-level objects built
      * on circular files, currently DiagLog and ItemLog, are treated
      * specially: if they have been loaded, their clear() method is
      * called. They would usually be loaded if the program is running,
      * but they wouldn't be if this method is being called from the
      * external state command.
      */
    public void clearLogs() throws IOException {
//        if (ItemLog.isLoaded()) {
//            Debug.println("Clearing out item log");	
//            ItemLog.getItemLog().clear();
//        }
//        else {
            stateFile.writeLong(0L, CIRCULAR_ITEM);
            stateFile.writeLong(0L, CIRCULAR_ITEM + 8);
            stateFile.write(md[1],  CIRCULAR_ITEM + 16);
            stateFile.writeLong(0L, CIRCULAR_ITEM_BAK);
            stateFile.writeLong(0L, CIRCULAR_ITEM_BAK + 8);
            stateFile.write(md[5],  CIRCULAR_ITEM_BAK + 16);
//        }

		// Removed references to DiagLog circular files in DW4.0
        DiagLog.getInstance().clear();

        stateFile.writeLong(0L, CIRCULAR_REPORT);
        stateFile.writeLong(0L, CIRCULAR_REPORT + 8);
        stateFile.write(md[3],  CIRCULAR_REPORT + 16);
//        stateFile.writeLong(0L, CIRCULAR_DIAG_REPORT);
//        stateFile.writeLong(0L, CIRCULAR_DIAG_REPORT + 8);
//        stateFile.write(md[4],  CIRCULAR_DIAG_REPORT + 16);
        stateFile.writeLong(0L, CIRCULAR_TEMP);
        stateFile.writeLong(0L, CIRCULAR_TEMP + 8);
        stateFile.write(md[7],  CIRCULAR_TEMP + 16);

        stateFile.writeLong(0L, CIRCULAR_CCI);
        stateFile.writeLong(0L, CIRCULAR_CCI + 8);
        stateFile.write(md[8],  CIRCULAR_CCI + 16);

        if (UnitProfile.getInstance().getProfileInstance() != null) {
            for (int i = 0; i < UnitProfileInterface.NUMBER_OF_TOTALS; i++) {
                UnitProfile.getInstance().getTotal(i).clear();
            }
        }

        // zero out the totals
        byte[] zeros = new byte[1024];
        java.util.Arrays.fill(zeros, (byte) 0);
        stateFile.write(zeros, 0, WAREHOUSE_START - TOTAL_START, TOTAL_START);

        stateFile.flush();
    }

    /** Resets everything to the original factory state.
      * Does what clearLogs() does, then resets the profile to the
      * original factory state, then resets all other persistant data
      * to the original state.
      */
    public void resetEverything() throws IOException {
        byte[] zeros = new byte[1024];
        java.util.Arrays.fill(zeros, (byte) 0);

        this.clearLogs();
        stateFile.writeLong(0L, CIRCULAR_PROFILE);
        stateFile.writeLong(0L, CIRCULAR_PROFILE + 8);
        stateFile.write(md[0],  CIRCULAR_PROFILE + 16);

        // zero out the state up to where the circular file pointers are stored
        stateFile.write(zeros, 0, CIRCULAR_PROFILE, 0);
        // zero out region between circular file pointers and totals
        stateFile.write(zeros, 0, TOTAL_START - DISPENSER_1, DISPENSER_1);
        // zero out the totals
        stateFile.write(zeros, 0, WAREHOUSE_START - TOTAL_START, TOTAL_START);
        stateFile.flush();
    }

    /** Zero out just the dispenser state. */
    public void zeroDispenser() throws IOException {
        byte[] zeros = new byte[1024];
        java.util.Arrays.fill(zeros, (byte) 0);
        stateFile.write(zeros, 0, DISPENSER_2 - DISPENSER_1, DISPENSER_1);
    }

    private static void fill(SimpleRandomAccessFile sraf, int length)
            throws IOException {
        SecureRandom random = new SecureRandom();
        byte[] buffer = new byte[1024];
        int i;

        for (i = 0; i < length; ++i) {
            random.nextBytes(buffer);
            sraf.write(buffer, i * 1024);
        }
    }

    // only reason this isn't private is so the state command can use it
    static void fill(String filename, int length)
                throws FileNotFoundException, IOException {
        SecureRandom random = new SecureRandom();
        FileOutputStream fos = new FileOutputStream(filename);
        byte[] buffer = new byte[1024];
        int i;

        for (i = 0; i < length; ++i) {
            random.nextBytes(buffer);
            fos.write(buffer);
        }
        fos.close();
    }

    static void copyStream(InputStream in, OutputStream out)
            throws IOException {
            int n;
        byte[] buffer = new byte[512];
        while (true) {
            n = in.read(buffer);
            if (n == -1) break;
            out.write(buffer, 0, n);
        }
    }

    /** Utility method to compare streams.
     *  may wish to buffer for efficiency
     */

    private static boolean compareStreams(InputStream in1, InputStream in2) throws IOException {
        int n;
        if (in1.available() != in2.available())
            return false;
        while (true) {
            n = in1.read();
            if (n != in2.read()){
                return false;
            }
            if (n == -1) {
                return true;
            }
        }
    }

    private void copyWarehouse(CircularFile out) throws IOException {
        long start = stateFile.readLong(CIRCULAR_WAREHOUSE);
        long end = stateFile.readLong(CIRCULAR_WAREHOUSE + 8);
        //Debug.println("Copying the Warehouse profile to current.");	
        CircularFile warehouse = new CircularFileImpl(stateFile, start, end);
        InputStream in = warehouse.getInputStream();
        copyStream(in, out);
//        warehouse.close();
    }

    public static String hex(byte[] data) {
        String result = "";	
        int i;

        for (i = 0; i < data.length; i++) {
            if (i != 0)
                result += " ";	
            result += Integer.toHexString((data[i] >> 4) & 0x0F);
            result += Integer.toHexString(data[i] & 0x0F);
        }
        return result;
    }

    /**
     * incrementSARCounter will increment the State var SAR_COUNTER
     * by one. this method used to track number of SAR's save to the 
     * ccilog.
     */
    public void incrementSARCounter(){
        try {
            writeInt(readInt(SAR_COUNTER)+1,SAR_COUNTER);
        } catch (IOException e) {
            Debug.println("IOException in incrementSARCounter");	
            Debug.printStackTrace(e);
        }
    }

    public void resetSARCounter(){
        try {
            writeInt(0,SAR_COUNTER);
        } catch (IOException e) {
            Debug.println("IOException in resetSARCounter");	
            Debug.printStackTrace(e);
        }
    }

    public int getSARCounter() {
        int count = 0;
        try {
            count = readInt(SAR_COUNTER); 
        }
        catch (IOException e) {
            Debug.println("IOException in getSARCounter");	
            Debug.printStackTrace(e);
        }
        return count;
    }

    public int read(byte buffer[], long filePointer) throws IOException {
        return stateFile.read(buffer, filePointer);
    }

    public void write(byte buffer[], long filePointer) throws IOException {
        stateFile.write(buffer, filePointer);
    }

    public void writeLong(long n, long filePointer) throws IOException {
        stateFile.writeLong(n, filePointer);
    }

    public long readLong(long filePointer) {
        try {
            return stateFile.readLong(filePointer);
        }
        catch (IOException e) {
            Debug.println("Ignoring IOException on state file!");
            return 0L;
        }
    }

    public void writeByte(int n, long filePointer) throws IOException {
        stateFile.writeByte(n, filePointer);
    }

    public byte readByte(long filePointer) throws IOException {
        return stateFile.readByte(filePointer);
    }

    public void writeInt(int n, long filePointer) throws IOException {
        stateFile.writeInt(n, filePointer);
    }

    public int readInt(long filePointer) throws IOException {
        return stateFile.readInt(filePointer);
    }

    public void writeBigDecimal(BigDecimal n, long filePointer)
            throws IOException {
        stateFile.writeLong(n.unscaledValue().longValue(), filePointer);
        stateFile.writeInt(n.scale(), filePointer + 8);
    }

    public BigDecimal readBigDecimal(long filePointer) throws IOException {
        long unscaled = stateFile.readLong(filePointer);
        int scale = stateFile.readInt(filePointer + 8);
        return BigDecimal.valueOf(unscaled, scale);
    }

    public void flush() throws IOException {
        stateFile.flush();
    }

    public void close() throws IOException {
        stateFile.close();
    }

    /**
      * Returns the serial number that the next item will print on.
      * Equivalent to Dispenser.getNextSerialNumber(). This method
      * is available for the diagnostic pointer reset, which may need
      * to happen before the dispenser is instantiated.
      * @param dispenser the dispenser number, either 1 or 2.
      */
    public static String getNextSerialNumber(int dispenser) {
        long n;
        try {
            if (dispenser == 1)
                n = State.getInstance().readLong(DISPENSER_1);
            else
                n = State.getInstance().readLong(DISPENSER_2);
        }
        catch (Exception e) {
            n = 0;
        }
        return Long.toString(n);
    }

    private static final byte[][] key1 = {{
        (byte) 0x60, (byte) 0x14, (byte) 0x10, (byte) 0x3f,
        (byte) 0x82, (byte) 0x80, (byte) 0x37, (byte) 0xa8
    },{
        (byte) 0xb9, (byte) 0x0a, (byte) 0x8b, (byte) 0x60,
        (byte) 0x2c, (byte) 0xd8, (byte) 0x5f, (byte) 0xb4
    },{
        (byte) 0xf9, (byte) 0xcf, (byte) 0x24, (byte) 0xb9,
        (byte) 0xbd, (byte) 0xa4, (byte) 0x2e, (byte) 0x20
    },{
        (byte) 0xe2, (byte) 0xe8, (byte) 0x67, (byte) 0x4a,
        (byte) 0x85, (byte) 0xe7, (byte) 0x0a, (byte) 0x03
    },{
        (byte) 0x8e, (byte) 0xe0, (byte) 0x8b, (byte) 0x6b,
        (byte) 0x5d, (byte) 0xd7, (byte) 0xfb, (byte) 0xbc
    },{
        (byte) 0x55, (byte) 0xec, (byte) 0xb8, (byte) 0x4c,
        (byte) 0x24, (byte) 0xa7, (byte) 0xee, (byte) 0x9e
    },{
        (byte) 0xb3, (byte) 0x73, (byte) 0xae, (byte) 0x75,
        (byte) 0x9d, (byte) 0xda, (byte) 0x42, (byte) 0xa5
    },{
        (byte) 0xee, (byte) 0x2b, (byte) 0x96, (byte) 0xba,
        (byte) 0x41, (byte) 0xa2, (byte) 0x59, (byte) 0x0a
    },{
        (byte) 0xf9, (byte) 0x8e, (byte) 0x97, (byte) 0x76,
        (byte) 0x3a, (byte) 0x90, (byte) 0x61, (byte) 0x0f
    }};

    private static final byte[][] key2 = {{
        (byte) 0xb6, (byte) 0x61, (byte) 0x40, (byte) 0x1d,
        (byte) 0xfe, (byte) 0xfe, (byte) 0x25, (byte) 0x5c,
        (byte) 0xad, (byte) 0x91, (byte) 0xfb, (byte) 0x23,
        (byte) 0xcf, (byte) 0x46, (byte) 0xff, (byte) 0x30,
        (byte) 0xf8, (byte) 0xcc, (byte) 0xe0, (byte) 0xed,
        (byte) 0x56, (byte) 0xd3, (byte) 0xd5, (byte) 0x47
    },{
        (byte) 0x82, (byte) 0x9f, (byte) 0x07, (byte) 0xfd,
        (byte) 0xa8, (byte) 0xbc, (byte) 0x19, (byte) 0xc3,
        (byte) 0x0f, (byte) 0x38, (byte) 0xfe, (byte) 0xe3,
        (byte) 0xfa, (byte) 0xc4, (byte) 0xe2, (byte) 0x03,
        (byte) 0x02, (byte) 0x06, (byte) 0xa8, (byte) 0xe2,
        (byte) 0xbe, (byte) 0x33, (byte) 0xdd, (byte) 0xb2
    },{
        (byte) 0x36, (byte) 0xe1, (byte) 0x6d, (byte) 0x52,
        (byte) 0xe1, (byte) 0x32, (byte) 0xc7, (byte) 0x22,
        (byte) 0xfa, (byte) 0xb3, (byte) 0x40, (byte) 0x3d,
        (byte) 0x89, (byte) 0x86, (byte) 0x2c, (byte) 0x54,
        (byte) 0xde, (byte) 0xd3, (byte) 0xfe, (byte) 0x6c,
        (byte) 0x92, (byte) 0xff, (byte) 0x43, (byte) 0x28
    },{
        (byte) 0xa3, (byte) 0x88, (byte) 0xd7, (byte) 0xb3,
        (byte) 0x04, (byte) 0x31, (byte) 0x60, (byte) 0x11,
        (byte) 0x61, (byte) 0xed, (byte) 0xe9, (byte) 0xa9,
        (byte) 0xbb, (byte) 0xa9, (byte) 0xcd, (byte) 0x54,
        (byte) 0x4c, (byte) 0xdf, (byte) 0xb6, (byte) 0x04,
        (byte) 0xdb, (byte) 0xf8, (byte) 0x1e, (byte) 0xad
    },{
        (byte) 0x35, (byte) 0x3b, (byte) 0x9d, (byte) 0x41,
        (byte) 0xde, (byte) 0x15, (byte) 0x0f, (byte) 0xe5,
        (byte) 0xd3, (byte) 0x2d, (byte) 0xfa, (byte) 0x5b,
        (byte) 0x1b, (byte) 0xce, (byte) 0x52, (byte) 0x47,
        (byte) 0xfd, (byte) 0x55, (byte) 0xfe, (byte) 0xc6,
        (byte) 0x62, (byte) 0xe5, (byte) 0x36, (byte) 0xc4
    },{
        (byte) 0xab, (byte) 0xba, (byte) 0x80, (byte) 0x31,
        (byte) 0xce, (byte) 0xc4, (byte) 0xf8, (byte) 0x84,
        (byte) 0xf3, (byte) 0xf0, (byte) 0xa6, (byte) 0x88,
        (byte) 0x25, (byte) 0x8d, (byte) 0xf2, (byte) 0xf7,
        (byte) 0x10, (byte) 0x58, (byte) 0xb5, (byte) 0x3b,
        (byte) 0x4b, (byte) 0x3e, (byte) 0xd2, (byte) 0x05
    },{
        (byte) 0xa4, (byte) 0x25, (byte) 0xf8, (byte) 0x8b,
        (byte) 0xf7, (byte) 0x7f, (byte) 0x3b, (byte) 0xdd,
        (byte) 0x9d, (byte) 0x5d, (byte) 0x2e, (byte) 0xb8,
        (byte) 0x62, (byte) 0x05, (byte) 0x90, (byte) 0xb3,
        (byte) 0x28, (byte) 0xb2, (byte) 0x7a, (byte) 0x9a,
        (byte) 0xba, (byte) 0xf7, (byte) 0x84, (byte) 0xe3
    },{
        (byte) 0x91, (byte) 0xf1, (byte) 0xd7, (byte) 0x40,
        (byte) 0x19, (byte) 0x53, (byte) 0x32, (byte) 0xd9,
        (byte) 0x24, (byte) 0x67, (byte) 0xc7, (byte) 0x67,
        (byte) 0xa5, (byte) 0xbf, (byte) 0xf6, (byte) 0x5d,
        (byte) 0x17, (byte) 0x92, (byte) 0xd0, (byte) 0x0b,
        (byte) 0x1d, (byte) 0xb3, (byte) 0x6a, (byte) 0x26
    },{
        (byte) 0x84, (byte) 0xbb, (byte) 0x41, (byte) 0xcf,
        (byte) 0xb5, (byte) 0x96, (byte) 0xbb, (byte) 0xf2,
        (byte) 0xc6, (byte) 0x7b, (byte) 0xdd, (byte) 0x0c,
        (byte) 0x75, (byte) 0x1a, (byte) 0x06, (byte) 0x67,
        (byte) 0x0b, (byte) 0xef, (byte) 0x57, (byte) 0x3c,
        (byte) 0x49, (byte) 0xf1, (byte) 0xa7, (byte) 0x14        
    }};

    private static final byte[][] md = {{ (byte) 20,
        (byte) 0xb2, (byte) 0x66, (byte) 0xd3, (byte) 0xcd,
        (byte) 0x52, (byte) 0x13, (byte) 0x92, (byte) 0x0a,
        (byte) 0x23, (byte) 0x49, (byte) 0x1f, (byte) 0xc3,
        (byte) 0xa1, (byte) 0x16, (byte) 0x09, (byte) 0x53,
        (byte) 0xab, (byte) 0x20, (byte) 0x73, (byte) 0xe1
    },{ (byte) 20,
        (byte) 0x27, (byte) 0x74, (byte) 0x55, (byte) 0xe9,
        (byte) 0xef, (byte) 0x10, (byte) 0x86, (byte) 0xba,
        (byte) 0x05, (byte) 0x95, (byte) 0x99, (byte) 0x3a,
        (byte) 0xa8, (byte) 0x60, (byte) 0xc1, (byte) 0x44,
        (byte) 0x94, (byte) 0x54, (byte) 0xe0, (byte) 0xec
    },{ (byte) 20,
        (byte) 0x6f, (byte) 0x2b, (byte) 0x9c, (byte) 0x5f,
        (byte) 0x0f, (byte) 0x5b, (byte) 0x19, (byte) 0xad,
        (byte) 0x30, (byte) 0x9d, (byte) 0x7e, (byte) 0xb7,
        (byte) 0xbc, (byte) 0x2b, (byte) 0x02, (byte) 0xbf,
        (byte) 0x0f, (byte) 0xc8, (byte) 0x5c, (byte) 0x93
    },{ (byte) 20,
        (byte) 0xf0, (byte) 0x19, (byte) 0x66, (byte) 0xab,
        (byte) 0x75, (byte) 0xa6, (byte) 0x53, (byte) 0xe3,
        (byte) 0x63, (byte) 0x72, (byte) 0x53, (byte) 0x39,
        (byte) 0x5a, (byte) 0x90, (byte) 0xa6, (byte) 0xe0,
        (byte) 0x59, (byte) 0xca, (byte) 0x48, (byte) 0xc9
    },{ (byte) 20,
        (byte) 0xfb, (byte) 0xc8, (byte) 0x78, (byte) 0x1f,
        (byte) 0x81, (byte) 0x48, (byte) 0x4b, (byte) 0x2a,
        (byte) 0x59, (byte) 0xa4, (byte) 0xc7, (byte) 0xe4,
        (byte) 0xc0, (byte) 0x18, (byte) 0x17, (byte) 0xa0,
        (byte) 0x14, (byte) 0x96, (byte) 0x3c, (byte) 0x82
    },{ (byte) 20,
        (byte) 0x33, (byte) 0xcb, (byte) 0xb6, (byte) 0xba,
        (byte) 0x91, (byte) 0xeb, (byte) 0x5a, (byte) 0x2c,
        (byte) 0x99, (byte) 0x01, (byte) 0x37, (byte) 0x05,
        (byte) 0x40, (byte) 0x2f, (byte) 0x60, (byte) 0xc1,
        (byte) 0x07, (byte) 0x21, (byte) 0x99, (byte) 0x3e
    },{ (byte) 20,
        (byte) 0x3f, (byte) 0x33, (byte) 0x17, (byte) 0x95,
        (byte) 0x91, (byte) 0x49, (byte) 0x30, (byte) 0x50,
        (byte) 0xc2, (byte) 0xa6, (byte) 0xff, (byte) 0x22,
        (byte) 0xb4, (byte) 0x86, (byte) 0xed, (byte) 0x5e,
        (byte) 0x56, (byte) 0x62, (byte) 0xdb, (byte) 0xa3
    },{ (byte) 20,
        (byte) 0xde, (byte) 0x0d, (byte) 0xb7, (byte) 0xac,
        (byte) 0x58, (byte) 0x52, (byte) 0x43, (byte) 0xb7,
        (byte) 0x62, (byte) 0xb1, (byte) 0xd4, (byte) 0x77,
        (byte) 0x5c, (byte) 0x0c, (byte) 0x31, (byte) 0xe4,
        (byte) 0x2e, (byte) 0x63, (byte) 0x15, (byte) 0x2a
    },{ (byte) 20,
       (byte) 0xe3, (byte) 0xd5, (byte) 0xf0, (byte) 0x6b,
       (byte) 0x6c, (byte) 0xae, (byte) 0xe6, (byte) 0x75,
       (byte) 0x42, (byte) 0x70, (byte) 0xba, (byte) 0x9d,
       (byte) 0xee, (byte) 0x41, (byte) 0x2b, (byte) 0x1c,
       (byte) 0xd6, (byte) 0x92, (byte) 0xf9, (byte) 0x0d,
    }};

    public static class StateAdapter implements StateListener {
        private long fp;

        public StateAdapter(long fp) {
            this.fp = fp;
        }

        @Override
		public void stateChanged(byte[] newState, int off, int len)
            throws java.io.IOException {
            instance.stateFile.write(newState, off, len, fp);
            instance.stateFile.flush();
//            Debug.println("state.dat [" + fp + "]: " + " at offset " + off + " with data: " +  hex(newState));
        }
    }
//    
    public static void closeProfilefile() throws Exception {
    	if (getInstance().profileFile != null) {
    		getInstance().profileFile.flush();
    		getInstance().profileFile.close();
    	}
    }
}
