package dw.io;
import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Persistent circular buffer used for local storage. This is a first-in,
 * first-out data structure implemented with a fixed-length file. Reads and
 * writes that extend past the end of the file transparently continue at the 
 * beginning. 
 *
 * Writes are performed using the methods inherited from OutputStream.
 * Reads are performed using the object returned by getInputStream.
 * The CircularFile slowly fills up as data is written. Calling the reset 
 * method invalidates old data, making space available.
 *
 * Within the circular file, valid data begins at a position named start,
 * and continues to the byte before the position named end. Read operations
 * begin at start, and write operations begin at end. When start == end, the
 * circular file is empty.
 * @author Geoff Atkin
 * modified by Francis Fung to change private field length to protected
 */
public abstract class CircularFile extends OutputStream  {
    
      /** Gets the start pointer for debugging. */
    public abstract long getStart(); 

    /** Gets the end pointed for debugging. */
    public abstract long getEnd();
    
    /** sets the lock flag to true. */
    public abstract boolean lock();

    /** sets the lock flag to false. */
    public abstract void unLock();

    /** set end pointer to lock pointer. */
    public abstract boolean rewind();


    /**
     * Writes a string to the file in UTF8 format.
     * @param s the string to write
     * @throws EOFException if the string length exceeds the available space
     * @throws IOException if the string can not be written for another reason
     */
    public abstract void write(String s) throws IOException;

    /**
     * Writes a single byte to the file.
     * @param b the byte to be written
     * @throws EOFException if there is no available space
     * @throws IOException if the byte can not be written for another reason
     */
     @Override
	public abstract void write(int b) throws IOException;
    
    /**
     * Writes b.length bytes to the file.
     * @param b the array of bytes to be written
     * @throws EOFException if b.length exceeds the available space
     * @throws IOException if the array can not be written for another reason
     */
     @Override
	public abstract void write(byte[] b) throws IOException;

    /**
     * Writes len bytes from the given byte array starting at offset off
     * to the file.
     * @param b the array of bytes to be written
     * @param off the start offset in the array
     * @param len the number of bytes to write
     * @throws EOFException if len exceeds the available space
     * @throws IOException if the bytes can not be written for another reason
     */
    @Override
	public  abstract void write(byte[] b, int off, int len) 
        throws IOException;

    /** 
     * Returns the amount of available space remaining in the circular file.
     * If the file is empty, the available space is one less than the length 
     * of the underlying random access file. If the file is not empty, the 
     * length is reduced by the amount of data between the start and end
     * pointers.
     */
    public abstract long freeSpace();

    /** Returns true if this circular file is empty. */
    public abstract boolean isEmpty();

    /** Returns true if this circular file is locked. */
    public abstract boolean isLocked();

    protected abstract long getFullThreshhold();

    // Indicates that the available space is below the threshhold
    public abstract boolean isNearlyFull();
    
    /**
     * Flushes any buffers to disk. 
     */      
    @Override
	public abstract  void flush() throws IOException;
    
    /**
     * Closes this circular file.
     */
    @Override
	public abstract void close() throws IOException;

    /**
     * Sets the start pointer to the given position. This method should
     * probably not be called directly. Space used by data before the
     * specified position becomes available for future calls to write.
     */
    public abstract void setStart(long newPosition) throws IOException;

    /**
      * Deletes all data in the circular file by setting the start pointer
      * to the position of the end pointer. 
      */
    public abstract void clear() throws IOException;
    
    /**
     * Returns a CircularInputStream which can be used to read the data
     * contained in this circular file. A call to the clearToMark method
     * of this stream changes the start pointer of this circular file.
     * The available method returns the number of bytes that can be read
     * before end of file, but makes no guarantee about blocking.
     * Otherwise, CircularInputStream is just like any other InputStream.
     */
    public abstract CircularInputStream getInputStream();

}
