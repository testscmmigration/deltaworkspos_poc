package dw.io;
import java.io.IOException;
import java.util.Random;

/**
 * Encrypted random access file. Each byte read or written to the file is
 * XOR'ed with the corresponding byte from a random number generator.
 * @author Geoff Atkin
 */

public class EncryptedRandomAccessFile extends SimpleRandomAccessFile {

    private Random random;
    private byte[] data;
    private long offset;
    private static final int size = 1024;
    private long seed;

    public EncryptedRandomAccessFile(String filename, String mode, long seed)
        throws IOException
    {
        super(filename, mode);
        random = new Random(seed);
        offset = 0;
        this.seed = seed;
        data = new byte[size];
        random.nextBytes(data);
    }

    /*
     * Determine how much random data is available for a read or write
     * starting at filePointer. If necessary, obtain appropriate data.
     * 
     * If the available random data corresponds to a point in the file
     * after the desired read or write, the random number generator is
     * reinitialized and nextBytes is called repeatedly until the 
     * correct point is reached.
     *
     * Note: could be really inefficient if interleaved calls are operating
     * on distant parts of the file.
     */
    public int availableRandomData(long filePointer) throws IOException {
        if (filePointer < 0) {
            throw new IOException("invalid file pointer"); 
        }

        if (filePointer < offset) {  
            offset = 0;
            random.setSeed(seed);
            random.nextBytes(data);
        }

        while (offset + size <= filePointer) {
            offset += size;
            random.nextBytes(data);
        }

        return (int) (offset + size - filePointer);
    }

    @Override
	public synchronized int read(long filePointer) throws IOException {
        byte[] temp = new byte[1];
        int n = this.read(temp, 0, 1, filePointer);
        if (n < 1)
            return -1;
        else 
            return temp[0] & 0xFF;
    }

    @Override
	public synchronized int read(byte[] b, long filePointer) 
        throws IOException
    {
        return this.read(b, 0, b.length, filePointer);
    }

    @Override
	public synchronized int read(byte[] b, int off, int len, long filePointer) 
    	throws IOException 
    {
        int n, n2;                          // number of bytes read
        int available;                      // available random data

        available = availableRandomData(filePointer);

        if (len > available) {              // have to split it up
            n = this.read(b, off, available, filePointer);
            n2 = this.read(b, off + available, len - available, 
                                 filePointer + available);
            if ((n == -1) || (n2 == -1)) {
                return n;
            }
            else {
                return n + n2;
            }
        }

	    raf.seek(filePointer);
    	n = raf.read(b, off, len);
        mask(b, off, len, filePointer);
        return n;
    }    

    @Override
	public synchronized void write(int b, long filePointer) 
        throws IOException
    {
        byte[] temp = new byte[1];
        temp[0] = (byte) b;
        this.write(temp, 0, 1, filePointer);
    }

    @Override
	public synchronized void write(byte[] b, long filePointer) 
    	throws IOException
    {
    	this.write(b, 0, b.length, filePointer);
    }

    @Override
	public synchronized void write(byte[] b, int off, int len, long filePointer)
	    throws IOException 
    {
        int available = availableRandomData(filePointer);

        if (len < 0)
            throw new IOException("illegal argument"); 

        if (len == 0)
            return;

        if (len > available) {              // have to split it up
            this.write(b, off, available, filePointer);
            this.write(b, off + available, len - available, 
                                 filePointer + available);
            return;
        }

       	raf.seek(filePointer);
        mask(b, off, len, filePointer);
    	raf.write(b, off, len);
	mask(b, off, len, filePointer);
    }    

    private void mask(byte[] b, int off, int len, long filePointer) {
        int i;
        for (i = 0; i < len; i++) {
            b[i + off] ^= data[i + (int) (filePointer % size)];
        }
    }
}
