package dw.io.report;

import java.io.Serializable;

/**
 * Encapsulates a MoneyGram Card type of detail record in the report log.
 */
public class MoneyGramCardDetail extends ReportDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	private String cardNumber;

	private byte attempts;

	private boolean successFlag = true;

	private boolean trainingFlag = false;

	private String customerName;

	private String cardType;

	private String empNum;

	/**default constructor used for conversion program */
	public MoneyGramCardDetail() {
		super(true);
	}

    @Override
	public String toString() {
		return super.toString()
				+ "\n  MoneyGramCardDetail {" + 
				"\n    cardNumber              : "
				+ String.valueOf(cardNumber)
				+ 
				"\n    customerName             : "
				+ String.valueOf(customerName)
				+ 
				"\n    successFlag            : "
				+ String.valueOf(successFlag)
				+ 
				"\n    trainingFlag            : "
				+ String.valueOf(trainingFlag)
				+ "\n    attempts                   : " + String.valueOf(attempts) + 
				"\n    cardType                   : "
				+ String.valueOf(cardType)
				+ "\n    empNum                   : " + String.valueOf(empNum)
				+ "\n  } MoneyGramCardDetail "; 
	}

	/* getters */
	public String getCardNumber() {
		return cardNumber;
	}

	public byte getAttempts() {
		return attempts;
	}

	public boolean getSuccessFlag() {
		return successFlag;
	}

	public boolean getTrainingFlag() {
		return trainingFlag;
	}

	public String getName() {
		return customerName;
	}

	public String getCardType() {
		return cardType;
	}

	public String getEmployeeNumber() {
		return empNum;
	}

	/* setters*/
	public void setCardNumber(final String cn) {
		cardNumber = cn;
	}

	public void setAttempts(final byte a) {
		attempts = a;
	}

	public void setSuccessFlag(final boolean sf) {
		successFlag = sf;
	}

	public void setTrainingFlag(final boolean tf) {
		trainingFlag = tf;
	}

	public void setName(final String n) {
		customerName = n;
	}

	public void setCardType(final String n) {
		cardType = n;
	}

	public void setEmployeeNumber(final String n) {
		empNum = n;
	}

}
