package dw.io.report;

import java.io.Serializable;

import dw.model.adapters.CountryInfo;
import dw.model.adapters.CountryInfo.CountrySubdivision;
import dw.model.adapters.MoneyGramAgentInfo;

/**
 * Encapsulates a MoneyGram Send type of detail record in the report log.
 * This includes:  MoneyGram sends and BillPayments.
 * @version 06-23-2000  1.0
 * @author John Allen
 */
public class DirectoryOfAgentsDetail extends ReportDetail implements Serializable {
	private static final long serialVersionUID = 1L;
    
    private String  name;
    private String  address;
    private String  state;
    private String  phone;

    /**default constructor used for conversion program */
    public DirectoryOfAgentsDetail() {
        super(true);
    }
    
    public DirectoryOfAgentsDetail(MoneyGramAgentInfo info) {          
        super();
        initializeLocalDetail(info);
    }

    private void initializeLocalDetail(final MoneyGramAgentInfo info) { // success flag
    	info.getStoreHrs();
    	String location; 

    	CountrySubdivision countrySubdivision = null;
    	String countrySubdivisionCode = info.getCountrySubdivisionCode();
    	if ((countrySubdivisionCode != null) && (! countrySubdivisionCode.isEmpty())) {
    		countrySubdivision = CountryInfo.getCountrySubdivision(countrySubdivisionCode);
    	}
		if (countrySubdivision != null) {
       		location = info.getAddress() + ", " + info.getCity() + ", " + countrySubdivision.getLegacyCode();
		} else {
       		location = info.getAddress() + ", " + info.getCity();
		}

    	this.name = info.getAgentName();
        this.address = location;
        this.state = countrySubdivision != null ? countrySubdivision.getLegacyCode() : "";
        this.phone = info.getAgentPhone();
    }

    /* toString */
    @Override
	public String toString() {
        return 
            super.toString() + 
            "\n  DirectoryOfAgentsDetail {" +	
            "\n    name          : " + name +	
            "\n    address       : " + address +
            "\n    state         : " + state +	
            "\n    phone         : " + phone +	
            "\n  } DirectoryOfAgentsDetail ";	
    }


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}

