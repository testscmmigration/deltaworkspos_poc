package dw.io.report;

import java.io.Serializable;

import dw.io.diag.DiagMessage;

/**
 * Encapsulates a dispenser load type of detail record in the report log.
 * @version 06-23-2000  1.0
 * @author John Allen
 */
public class DispenserLoadDetail extends ReportDetail implements Serializable {
	private static final long serialVersionUID = 1L;

    private long    serialNumberStart;
    private long    serialNumberEnd;
    private boolean cardLoadFlag = false;
    private byte    dispenser = 0;

    /* constructor */
    /*default constructor used for conversion program*/
    public DispenserLoadDetail() {
        super(true);        
    }
    
    public DispenserLoadDetail(short dispenserId,
            String docSerialNbrStart, String docSerialNbrEnd, 
            boolean flagCardLoad, String employeeId) {
        super();
        type = ReportLog.DISPENSER_LOAD_TYPE;
        dnetCode = DiagMessage.DNET_TT_TLOG_DIAG_FDOC_LOAD;
        serialNumberStart = stringToLong(docSerialNbrStart);
        serialNumberEnd = stringToLong(docSerialNbrEnd);
        userId = stringToByte(employeeId);
        cardLoadFlag = flagCardLoad;
        dispenser = (byte)dispenserId;
    }

    /* toString */
    @Override
	public String toString() {
        return 
            super.toString() + 
            "\n  DispenserLoadDetail {" +	
            "\n    serialNumberStart         : " + String.valueOf(serialNumberStart) +	
            "\n    serialNumberEnd           : " + String.valueOf(serialNumberEnd) +	
            "\n    dispenser                 : " + String.valueOf(dispenser) +	
            "\n    cardLoadFlag              : " + String.valueOf(cardLoadFlag) +	
            "\n  } DispenserLoadDetail ";	
    }

    /* getters */
    public long    getSerialNumberStart() { return serialNumberStart; }
    public long    getSerialNumberEnd()   { return serialNumberEnd; }
    public boolean getCardLoadFlag()      { return cardLoadFlag;}
    public byte    getDispenserNumber()   { return dispenser;}

    /* setters */
    public void setSerialNumberStart(final long sns) {serialNumberStart = sns; }
    public void setSerialNumberEnd(final long sne) {serialNumberEnd = sne; }
    public void setCardLoadFlag(final boolean clf) {cardLoadFlag = clf; }
    public void setDispenser(final byte dn) {dispenser = dn; }
}



