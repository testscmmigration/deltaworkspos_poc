package dw.io.report;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import dw.comm.AgentConnect;
import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.main.MainPanelDelegate;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.PaidMoneyGramInfo40;
import dw.profile.UnitProfile;
import dw.utility.Debug;
import dw.utility.TimeUtility;

public class AuditLog {

	public static final String AUDIT_FILE_NAME_BASE = "tudia.txt"; 

	public static final String AUDIT_LOG_QUERY = "temp.csv";

	public static final String AUDIT_LOG_TEMP_FILE = "temp.txt";

	private static boolean enableAudit = (UnitProfile.getInstance().get(
			"DAYS_IN_AUDIT_TRAIL", 7) > 0);

	private static final int ENCRYPTADDON = 20;

	private static final int DEBUG_ENCRYPTADDON = 0;

	private static int offset = ENCRYPTADDON;

	private static String mode = " ";

	private static String tempMode = " ";

	static {
		if (AgentConnect.AUDIT_ENCRYPTION)
			offset = DEBUG_ENCRYPTADDON;

		setTransactionMode();

	}

	private static void setTransactionMode() {

		boolean training = UnitProfile.getInstance().getProfileInstance().find(
				"TRAINING_MODE").booleanValue();
		if (UnitProfile.getInstance().isDemoMode()) {
			if (training)
				mode = "DMO/TRN";
			else
				mode = "DMO";
		} else if (training)
			mode = "TRN";
		else
			mode = " ";
	}

	public static void writeMgsRecord(int userID, String referenceNumber, BigDecimal sendAmount, String sendCurrency) {
		String detail = '"' + (mode + " Ref # = " + referenceNumber + "  Amount = " + sendAmount.toString() + " " + sendCurrency + '"');
		writeOutput(createRecord(String.valueOf(userID), "SND", "", detail));
	}

//	public static void writeMgsAmendRecord(int userID, String refNum,
//			DetailLookupResponse cancelInfo) {
//		Payload payload = cancelInfo.getPayload().getValue();
//		String detail = '"' + (mode + " Ref # = " + refNum + "  Amount = "
//				+ payload.getSendAmounts().getSendAmount() + " "
//				+ payload.getSendAmounts().getSendCurrency() + '"');
//		writeOutput(createRecord(String.valueOf(userID), 
//				MainPanelDelegate.clAmend, "", detail));
//
//	}

	public static void writeMgsTSRecord(int userID, String refNumber) {

		String detail = '"' + (mode + " Ref # = " + refNumber + '"');
		writeOutput(createRecord(String.valueOf(userID), 
				MainPanelDelegate.clTransactionStatus, "", detail));

	}

	public static void writeMgrRecord(int userID, PaidMoneyGramInfo40 paidInfo,
			String amount) {

		String detail = '"' + (mode + " Ref # = "
				+ paidInfo.getReferenceNumber() + "  Amount = " + amount + " "
				+ paidInfo.getCurrency() + '"');
		writeOutput(createRecord(String.valueOf(userID), "RCV", "", detail));

	}

//	public static void writeMgsCancelRecord(int userID, String refNum,
//			DetailLookupResponse cancelInfo) {
//		Payload payload = cancelInfo.getPayload().getValue();
//		String detail = '"' + (mode + " Ref # = " + refNum + "  Amount = "
//				+ payload.getSendAmounts().getSendAmount() + " "
//				+ payload.getSendAmounts().getSendCurrency() + '"');
//		writeOutput(createRecord(String.valueOf(userID), "CNL", "", detail));
//
//	}

	public static void writeMgrReverseRecord(int userID, String refNum,
			String amount) {

		String detail = '"' + (mode + " Ref # = " + refNum + "  Amount = "
				+ amount + '"');
		writeOutput(createRecord(String.valueOf(userID), 
				MainPanelDelegate.clMoneyGramReceiveReversal, "", detail));

	}

//	public static void writeMgsRefundRecord(int userID, String refNum,
//			DetailLookupResponse cancelInfo) {
//		Payload payload = cancelInfo.getPayload().getValue();
//		String detail = '"' + (mode + " Ref # = " + refNum + "  Amount = "
//				+ payload.getSendAmounts().getSendAmount() + " "
//				+ payload.getSendAmounts().getSendCurrency() + '"');
//		writeOutput(createRecord(String.valueOf(userID), "RVL", "", detail));
//
//	}


	public static void writeBpRecord(int userID, String referenceNumber, BigDecimal amount, String billerName, String currency) {

		String detail = '"' + (mode + " Biller name = " + billerName
				+ "  Ref # = " + referenceNumber 
				+ "  Amount = " + amount + " " + currency + '"');
		writeOutput(createRecord(String.valueOf(userID), "BP", "", detail));

	}

	public static void writeTranCancelRecord(int userID, String tranName) {

		String detail = '"' + (mode + " Cancel Type = " + tranName + '"');
		writeOutput(createRecord(String.valueOf(userID), "ESC", "", detail));
	}

	public static void writeBackoutRecord(int userID) {

		String detail = '"' + (mode + " User backout of process " + '"');
		writeOutput(createRecord(String.valueOf(userID), "ESC", "", detail));
	}

	public static void writeReportRecord(int userID, long dateSelected,
			String reportType) {

		DateFormat formatter;
        if (CountryInfo.isAgentKorean()) {
        	formatter = new SimpleDateFormat(TimeUtility.KOREAN_DATE_YEAR_MONTH_DAY_FORMAT_2);
        } else {
        	formatter = DateFormat.getDateInstance(DateFormat.LONG, FormatSymbols.getLocale());
        }
		String activityDate = formatter.format(new Date(dateSelected));
		String detail = '"' + (mode + " Date = " + activityDate) + '"';
		writeOutput(createRecord(String.valueOf(userID), reportType, "", detail));
	}

	public static void writeReportRecord(int userID, long start, long end,
			String reportType) {

		DateFormat formatter;
        if (CountryInfo.isAgentKorean()) {
        	formatter = new SimpleDateFormat(TimeUtility.KOREAN_DATE_YEAR_MONTH_DAY_FORMAT_2);
        } else {
        	formatter = DateFormat.getDateInstance(DateFormat.LONG, FormatSymbols.getLocale());
        }
		String startDate = formatter.format(new Date(start));
		String endDate = formatter.format(new Date(end));
		String detail = '"' + (mode + " Dates = " + startDate + " " + endDate) + '"';
		writeOutput(createRecord(String.valueOf(userID), reportType, "", detail));

	}

	private static boolean hideNewValue(String tag) {
		boolean retval = false;
		if (tag.indexOf("PASSWORD") > -1)
			retval = true;
		return retval;
	}

	public static void writeProfileChangeRecord(int userID, String tagName,
			String newValue, String oldValue) {

		tempMode = " ";
		setTransactionMode();
		if (tagName.equals("TRAINING_MODE")) {
			if (UnitProfile.getInstance().getProfileInstance().find(
					"TRAINING_MODE").booleanValue())
				if (UnitProfile.getInstance().isDemoMode())
					tempMode = "DMO";
				else
					tempMode = " ";
			else {
				if (UnitProfile.getInstance().isDemoMode())
					tempMode = "DMO/TRN";
				else
					tempMode = "TRN";
			}
		} else
			tempMode = mode;

		String detail = '"' + (tempMode + " Old Value = " + oldValue
				+ " New value = " + newValue) + '"';
		if (hideNewValue(tagName))
			detail = '"' + (tempMode + " New value = PASSWORD CHANGED"
					+ " User id = " + newValue) + '"';
		writeOutput(createRecord(String.valueOf(userID), "PLC", tagName, detail));

	}

	public static void writeProfileChangeRecord(int userID, String tagName,
			String newValue, String empName, String oldValue) {

		String detail = '"' + (mode + " Old value = " + oldValue
				+ " New value = " + newValue + " User id = " + empName) + '"';
		writeOutput(createRecord(String.valueOf(userID), "PLC", tagName, detail));

	}

	public static void writeProfileChangeRecord(int userID, int product,
			String tagName, String newValue, String oldValue) {

		String detail = '"' + (mode + " Product Number = " + product
				+ " Old value = " + oldValue + " New value = " + newValue) + '"';
		writeOutput(createRecord(String.valueOf(userID), "PLC", tagName, detail));

	}

	public static void writeTimeoutRecord(int userID, String tranName) {

		String detail = '"' + (mode + " Timeout location = " + tranName) + '"';
		writeOutput(createRecord(String.valueOf(userID), "TMO", "", detail));

	}

	public static void writeReferenceLookupRecord(int userID, String refNum) {

		String detail = '"' + (mode + " Ref # = " + refNum) + '"';
		writeOutput(createRecord(String.valueOf(userID), "RFL", "", detail));
	}

	public static void writePhoneLookupRecord(int userID, String phoneNum) {

		String detail = '"' + (mode + " Phone # = " + phoneNum) + '"';
		writeOutput(createRecord(String.valueOf(userID), "PHL", "", detail));

	}

	public static void writeNameLookupRecord(int userID, String recFirst,
			String recLast, String senderLast) {

		String detail = '"' + (mode + " Receive Name = " + recFirst + " "
				+ recLast + "  Sender Name = " + senderLast) + '"';
		writeOutput(createRecord(String.valueOf(userID), "NML", "", detail));

	}

	public static void writeErrorRecord(String userID, String code, String text) {

		if (userID == null)
			userID = " ";

		String detail = '"' + (mode + " Error code = " + code
				+ " Error text = " + text) + '"';
		writeOutput(createRecord(String.valueOf(userID), "MEC", "", detail));

	}

	public static void writeSuccessRecord(String userID, String text) {

		if (userID == null)
			userID = " ";

		String detail = '"' + (mode + " Success: " + text) + '"';
		writeOutput(createRecord(String.valueOf(userID), "MEC", "", detail));

	}

	private static long getTime() {
		return System.currentTimeMillis();
	}

	public static StringBuffer createRecord(String employeeNumber,
			String actionType, String profileItem, String detailInfo) {

		long auditDate = getTime();
		StringBuffer auditRecord = new StringBuffer();
		
		DateFormat df;
        if (CountryInfo.isAgentKorean()) {
        	df = new SimpleDateFormat(TimeUtility.KOREAN_DATE_YEAR_MONTH_DAY_FORMAT_2);
        } else {
        	df = new SimpleDateFormat(TimeUtility.SHORT_DATE_FORMAT);
        }
		String auditDateFormatted = df.format(new Date(auditDate));
		String auditTimeFormatted = new SimpleDateFormat(TimeUtility.TIME_24_HOUR_MINUTE_SECOND_FORMAT).format(new Date(auditDate));
		String deviceId = UnitProfile.getInstance().get("DEVICE_ID", "");
		if (deviceId.equals(""))
			deviceId = UnitProfile.getInstance().get("SUPER_AGENT_DEVICE_ID",
					"");
		String locationId = UnitProfile.getInstance().get("AGENT_ID", "");

		auditRecord.append(auditDate);
		auditRecord.append(',');
		auditRecord.append('"' + auditDateFormatted + '"');
		auditRecord.append(',');
		auditRecord.append(auditTimeFormatted);
		auditRecord.append(',');
		auditRecord.append(employeeNumber);
		auditRecord.append(',');
		auditRecord.append(actionType);
		auditRecord.append(',');
		auditRecord.append(profileItem);
		auditRecord.append(',');
		auditRecord.append(detailInfo);
		auditRecord.append(',');
		auditRecord.append(deviceId);
		auditRecord.append(',');
		auditRecord.append(locationId);
		return auditRecord;
	}

	public static void writeOutput(StringBuffer inData) {

		FileOutputStream out;

		if (enableAudit) {

			StringBuffer outData = encryptTextLine(inData);
			outData.append("\r\n");

			try {
				out = new FileOutputStream(AUDIT_FILE_NAME_BASE, true);
				out.write((outData.toString()).getBytes());
				out.close();
			} catch (IOException e) {
				Debug.println("Error in AuditLog writeOutput() ----> ");
				Debug.printException(e);
			}
		}
	}

	public static FileOutputStream getMatchingAuditRecords(long starttime,
			long endtime) {

		FileOutputStream auditInfoOut = null;
		String currentRecord = null;
		boolean recordsFound = false;
		boolean headerNeeded = true;
		FileReader txtIn = null;
		BufferedReader reader = null;

		try {
			txtIn = new FileReader(AUDIT_FILE_NAME_BASE);
			reader = new BufferedReader(txtIn);
			auditInfoOut = new FileOutputStream(AUDIT_LOG_QUERY, false);
			Writer out = null;
			currentRecord = getTextLine(reader);
			String sLang = Messages.getCurrentLocale().getLanguage();
			while (currentRecord != null) {
				long currentTimestamp = 0L;
				try {
					String ts = currentRecord.substring(0, currentRecord.indexOf(","));
					currentTimestamp = Long.parseLong(ts);
				}
                catch (Exception e) {
        			Debug.println("Error in AuditLog - Bad record["+currentRecord+"]");
    				currentRecord = getTextLine(reader);
    				continue;
                }
				if ((currentTimestamp >= starttime)
						&& (currentTimestamp <= endtime)) {
					if (headerNeeded) {
						String HEADER = Messages.getString("AuditHeader.1")
						+ "," + Messages.getString("AuditHeader.1a") + ","
						+ Messages.getString("AuditHeader.2") + ","
						+ Messages.getString("AuditHeader.3") + ","
						+ Messages.getString("AuditHeader.4") + ","
						+ Messages.getString("AuditHeader.5") + ","
						+ Messages.getString("AuditHeader.6") + ","
						+ Messages.getString("AuditHeader.7");
						if (sLang.compareToIgnoreCase("cn") == 0
								|| sLang.compareToIgnoreCase("ru") == 0) {
							byte[] bom = new byte[] { (byte)0xEF, (byte)0xBB, (byte)0xBF };
							auditInfoOut.write(bom);
							// open UTF8 writer
							out = new OutputStreamWriter(auditInfoOut, "UTF-8");
							out.write(HEADER + "\r\n");
						} else {
							auditInfoOut.write((HEADER + "\r\n").getBytes());
						}
						headerNeeded = false;
					}
					if (sLang.compareToIgnoreCase("cn") == 0
							|| sLang.compareToIgnoreCase("ru") == 0) {
						out.write((currentRecord.substring(currentRecord
								.indexOf(',') + 1) + "\r\n"));
					} else {
						auditInfoOut.write((currentRecord.substring(currentRecord
								.indexOf(',') + 1) + "\r\n").getBytes());
					}
					recordsFound = true;
				}
				currentRecord = getTextLine(reader);
			}
			
			if (sLang.compareToIgnoreCase("cn") == 0
					|| sLang.compareToIgnoreCase("ru") == 0) {

				out.close();
			} else {
				auditInfoOut.close();
			}
		} catch (FileNotFoundException e) {
			Debug.println("Error in AuditLog getMatchingAuditRecords() - file not found");
            Debug.printStackTrace(e);
		} catch (IOException e) {
			Debug.println("Error in AuditLog getMatchingAuditRecords() - IO exception");
            Debug.printStackTrace(e);
		}finally
		{
			try {
				if (txtIn != null) {
					txtIn.close();
				}
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				Debug.println("Error in closing handles - IO exception");
	            Debug.printStackTrace(e);
			}
			
		}
		if (recordsFound)
			return auditInfoOut;
		else
			return null;

	}

	public static String getTextLine(final BufferedReader reader) {

		String txtLine = null;
		try {
			txtLine = reader.readLine();
			if (txtLine != null)
				return decryptTextLine(txtLine).toString();
			else
				return null;
		} catch (IOException e) {
			Debug.println("Error in AuditLog getTextLine() ----> ");
			Debug.printException(e);
			return null;
		}
	}

	private static StringBuffer encryptTextLine(StringBuffer inData) {

		char aChar;
		int charValue;
		StringBuffer outData = new StringBuffer();

		for (int i = 0; i < inData.length(); i++) {
			aChar = inData.charAt(i);
			charValue = aChar;
			charValue = charValue - offset;
			aChar = (char) charValue;
			outData.append(aChar);
		}

		return outData;

	}

	private static StringBuffer decryptTextLine(String text) {

		char aChar;
		int charValue;
		StringBuffer outData = new StringBuffer();

		StringBuffer inData = new StringBuffer(text);
		for (int i = 0; i < inData.length(); i++) {
			aChar = inData.charAt(i);
			charValue = aChar;
			charValue = charValue + offset;
			aChar = (char) charValue;
			outData.append(aChar);
		}
		return outData;
	}

	public static int cleanup(String days) {

		long d = 0;
		try {
			d = Long.parseLong(days);
		} catch (Exception e) {
		}
		long daysInMillis = (long) TimeUtility.DAY * d;
		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		c.set(Calendar.MILLISECOND, 0);
		long currentTime = c.getTimeInMillis();
		long deleteTime = currentTime - daysInMillis;
		String currentRecord = null;
		int rowsRemoved = 0;
		FileOutputStream out = null;
		FileReader txtIn = null;
		BufferedReader reader = null;
		try {
			txtIn = new FileReader(AUDIT_FILE_NAME_BASE);
			reader = new BufferedReader(txtIn);
			out = new FileOutputStream(AUDIT_LOG_TEMP_FILE, true);
			currentRecord = getTextLine(reader);
			while (currentRecord != null) {
				String ts = currentRecord.substring(0, currentRecord
						.indexOf(","));
				long currentTimestamp = 0L;
				try {
					currentTimestamp = Long.parseLong(ts);
				} catch (Exception e) {
				}
				if (currentTimestamp > deleteTime) {
					StringBuffer outData = new StringBuffer(currentRecord);
					String s = encryptTextLine(outData).append("\r\n")
							.toString();
					out.write(s.getBytes());
				} else
					rowsRemoved++;
				currentRecord = getTextLine(reader);
			}

			File newFile = new File(AUDIT_LOG_TEMP_FILE);
			File oldFile = new File(AUDIT_FILE_NAME_BASE);
			boolean f = oldFile.delete();
	        if (! f) {
	        	Debug.println("File " + oldFile.getName() + " could not be deleted");
	        }
			boolean b = newFile.renameTo(new File(AUDIT_FILE_NAME_BASE));
            if (! b) {
            	Debug.println("File " + newFile.getName() + " could not be renamed to " + AUDIT_FILE_NAME_BASE);
            }

		} catch (FileNotFoundException e) {
			Debug.println("Error in AuditLog cleanup() - file not found");
			//e.printStackTrace();
		} catch (IndexOutOfBoundsException e) {
			Debug.println("Error in AuditLog cleanup() - IndexOutOfBoundsException");
			Debug.println("Trying to cleanup and empty file. -- OK");
			//e.printStackTrace();
		} catch (NumberFormatException e) {
			Debug.println("Error in AuditLog cleanup() - NumberFormatException exception");
			//e.printStackTrace();
		} catch (IOException e) {
			Debug.println("Error in AuditLog cleanup() - IO exception");
			//e.printStackTrace();
		} finally {
			try {
				if (txtIn != null) {
					txtIn.close();
				}
			} catch (IOException e) {
				Debug.println("Error in AuditLog cleanup() - IO exception");
				Debug.printException(e);
			}
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
				Debug.println("Error in AuditLog cleanup() - IO exception");
				Debug.printException(e);
			}
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException e) {
				Debug.println("Error in AuditLog cleanup() - IO exception");
				Debug.printException(e);
			}
		}

		return rowsRemoved;

	}

}
