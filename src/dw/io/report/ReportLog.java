package dw.io.report;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.TreeMap;
import java.util.TreeSet;

import com.moneygram.agentconnect.AmountInfo;
import com.moneygram.agentconnect.MoneyOrderInfo;
import com.moneygram.agentconnect.MoneyOrderPrintStatusType;
import com.moneygram.agentconnect.MoneyOrderVoidReasonCodeType;
import com.moneygram.agentconnect.SendAmountInfo;

import dw.dispenser.MoneyOrderDisplayFields;
import dw.framework.DataCollectionSet;
import dw.framework.FieldKey;
import dw.io.CircularFile;
import dw.io.State;
import dw.io.info.MoneyGramReceiveDiagInfo;
import dw.io.info.MoneyGramSendDiagInfo;
import dw.model.adapters.MoneyGramInfo40;
import dw.moneyorder.MoneyOrderInfomation;
import dw.profile.UnitProfile;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.ErrorManager;
import dw.utility.Period;
import dw.utility.TimeUtility;

/**
 * Records POS activity for later reporting.  A detail record
 * is created for every item or transaction, and a summary record is 
 * created for each day. Detail records are kept for 90 days or 15,000 
 * items.
 *
 * The design has always called for there to be two files; one for detail
 * records and one for summary records. But we're having problems where
 * data seems to go missing, which could result from inconsistencies
 * between summary and detail. Also, details were originally held for 14
 * days and summaries for 90 days; now details are held for 90 days and 
 * summaries are held for an unspecified length of time. So it makes less
 * sense to keep both details and summaries, and it complicates the 
 * conversion process, so I am now generating summary records from the 
 * detail records on demand.
 *
 * The original version stored data in an ad hoc manner and couldn't 
 * be extended as more reports were added. It also maintained its own
 * flat files containing serialized objects. The first rewrite stored 
 * much more data, but still used a serialized collection as the file
 * format. Since it would take too long to save the whole thing on 
 * every transaction, it added the "dreportRecent" file. But an unknown
 * bug was added somewhere along the way, and it lost data mysteriously.
 *
 * The design goal of this version is to represent the data in a brief
 * but extensible way. Brevity is required because of the amount of data
 * to be stored. Extensibility is required so that future versions can
 * store any additional data necessary for future reports. We also need
 * backward and forward compatibility, which suggest a self-describing 
 * format. It also uses the same kind of encrypted, secure circular files
 * used elsewhere in the application to avoid losing data.
 *
 * Serialized objects are self-describing, but have limitations with 
 * respect to backward and forward compatibility as the code changes.
 * XML is extensible and self-describing, but not consise. Compressed
 * data and serialized data have a trailer, so they can't be written 
 * one record at a time while maintaining file integrity. (We don't want
 * the file to get corrupt even if the user pulls the plug during I/O.)
 * 
 * For these reasons, this class uses a new format implemented by 
 * UnifiedReport detail.
 *
 * This class is a singleton, and I'm actually going to try to follow the
 * singleton pattern this time. Any caller who wants to use a non-static
 * method should call ReportLog.getInstance().
 *
 * @author John Allen (original version)
 * @author Eric Inman and Kelli Kohler (first rewrite)
 * @author Geoff Atkin (second rewrite)
 */
public class ReportLog {

    private static final SummaryRecordComparator SUMMARY_RECORD_COMPARATOR = 
        new SummaryRecordComparator();
    private static final DetailRecordComparator DETAIL_RECORD_COMPARATOR = 
        new DetailRecordComparator();

    // These are the old constants, and are preserved here for 
    // compatibility with existing code. A new set of constants is 
    // defined in ReportLogConstants. The new constants are used
    // by UnifiedReportDetail, and maybe should be used by new code.
    
    public static final int VOID_TYPE                	= 0;
    public static final int MONEY_ORDER_TYPE        	= 1;
    public static final int VENDOR_PAYMENT_TYPE      	= 2;
    public static final int MONEYGRAM_SEND_TYPE       	= 3;
    public static final int MONEYGRAM_RECEIVE_TYPE    	
    	= ReportLogConstants.MONEYGRAM_RECEIVE_TYPE;
    public static final int MONEYGRAM_RECEIVE_TYPE_PPC 	
    	= ReportLogConstants.MONEYGRAM_RECEIVE_TYPE_PPC;
    public static final int BILL_PAYMENT_TYPE   	    = 5;
    public static final int DELTAGRAM_TYPE            	= 6;
    public static final int GIFT_CERTIFICATE_TYPE     	= 7;
    public static final int MONEYGRAM_REVERSAL_TYPE   	= 8;
    public static final int MONEYGRAM_CARD_RELOAD_TYPE 	= 10;
    public static final int MONEYGRAM_CARD_PURCH_TYPE 	= 11;
    public static final int EXPRESS_PAYMENT_TYPE   	    = 12;
    public static final int UTILITY_PAYMENT_TYPE   	    = 13;
    public static final int PREPAID_SERVICES_TYPE       = 14;

    public static final int DISPENSER_LOAD_TYPE       = 101;
    public static final int INVALID_LOGIN_TYPE        = 102;

    /*
     * Number of days to save detail records. 
     */
    public static final int ARCHIVE_AGE = 90;

    private static volatile ReportLog instance;
    private CircularFile detail;
    //private CircularFile summary;

    private TreeMap<SummaryKey, ReportSummary> summaryRecords;
    
    /** 
     * Public do-nothing constructor for unit tests.
     * @deprecated Do not call this constructor under penalty of death.
     */
/*    
    protected ReportLog() {
        
    }
*/

    /** Private constructor. Callers should use getInstance() instead. */
    private ReportLog(CircularFile detail) {
        this.detail = detail;
        //this.summary = summary;
    }

    /**
     * Returns the singleton instance. If it hasn't been created yet,
     * create it now using the State class to get the circular files.
     */
    public static ReportLog getInstance() throws IOException {
        if (instance == null) {
            Debug.println("creating ReportLog instance");	
            State state = State.getInstance();
            instance = new ReportLog(state.createDetailFile()); 
        }
        return instance;
    }


    /**
     * Adds summary records for periods that have been closed since the last
     * time summary records were added.
     * 
     * This method does nothing because it is no longer relevant, but it 
     * might get reimplemeted in the future.
     * 
     * Called by main\DeltaworksMainPanel.java:240
     */
    public static void addNewSummaryRecords() {
        // does nothing   
    }
    
    
    /**
     * Deletes records from any period older than the archive age. Also 
     * deletes those that are in periods beyond the one in which the 
     * cumulative item count goes over the limit.
     *
     * Note: This implementation doesn't actually implement the item
     * count as a hard limit, but it does delete extra days if the 
     * log is close to being full, which accomplishes the real goal.
     *
     * Called by utility\Period.java:354
     */
    public static synchronized void archiveRecords() throws IOException {
        ReportLog log = getInstance();
        log.clearOldData(ARCHIVE_AGE + 1);
        while (log.isNearlyFull()) {
            log.clearOldestPeriod();
        }
    }
        
    /** Clears the report logs. */
    public void clear() throws IOException {
        detail.clear();
        //summary.clear();
    }

    /** 
     * Removes anything beyond the given age and anything within 24 hours
     * and belonging to the same period as the last record deleted.
     * Warning: don't call this while someone is reading records.
     * @param age age in days of oldest allowed record
     */
    public void clearOldData(long age) {    
        Iterator<UnifiedReportDetail> i = detailIterator();
        UnifiedReportDetail current = null;
        UnifiedReportDetail previous = null;
            //Debug.println("currentTimeMillis = " 	
            //    + TimeUtility.currentTimeMillis()
            //    + " (" + new Date(TimeUtility.currentTimeMillis()) + ")");	 
        long cutoffDateTime = TimeUtility.currentTimeMillis()
            - (TimeUtility.DAY * age);
            //Debug.println("cutoffDateTime = " + cutoffDateTime	
            //    + " (" + new Date(cutoffDateTime) + ")");	 
        while (i.hasNext()) {
            current = i.next();
            if (current.getDateTime() < cutoffDateTime) {
                i.remove();
                //Debug.println("removed: " + current);	
                previous = current;
            }
            else if ((previous != null) 
                    && (Math.abs(current.getDateTime() 
                        - previous.getDateTime()) < TimeUtility.DAY)
                    && (current.getPeriod() == previous.getPeriod())) {
                i.remove();
                previous = current;
            }
            else {
                return;
            }
        }
    }

    /** 
     * Removes the first record in the log and anything anything within 
     * 24 hours and belonging to the same period as it.
     */
    public void clearOldestPeriod() {
        Iterator<UnifiedReportDetail> i = detailIterator();
        UnifiedReportDetail current = null;
        UnifiedReportDetail first = null;
        while (i.hasNext()) {
            current = i.next();
            if (first == null) {
                i.remove();
                first = current;
            }
            else if ((Math.abs(current.getDateTime() 
                        - first.getDateTime()) < TimeUtility.DAY)
                    && (current.getPeriod() == first.getPeriod())) {
                i.remove();
            }
            else {
                return;
            }
        }
    }
    
    /**
     * Returns an iterator over all the UnifiedReportDetail records.
     */
    public Iterator<UnifiedReportDetail> detailIterator() {
        return new UnifiedIterator(detail.getInputStream());
    }
    
    /** 
     * Returns the summary record for the period containing the given date
     * and time. Implementation unchanged from previous version of ReportLog.
     */
    public static ReportSummary getCurrentSummaryRecord(final long dateTime) {
        //Debug.println("getCurrentSummaryRecord(" + dateTime + ")");	 
        final Iterator<ReportSummary> it = getSummaryRecordsClone().iterator();
        ReportSummary result = null;
        while (it.hasNext()) {
            final ReportSummary summaryRecord = it.next();
            if (summaryRecord.getPeriodOpened() <= dateTime) 
                result = summaryRecord;
            else
                break;
        }
        //Debug.println("result = " + result);	
        return result;
    }

    /** Return the detail circular file. The caller is expected to
      * get the input stream.
      */
    private CircularFile getDetailFile() {
        return detail;
    }
    
    /**
     * Gets all detail records for a specified selection criteria. 
     * If there are none an empty collection is returned.
     * The result should be treated as read-only; about the only thing
     * the caller should do to it is ask it for an iterator. In fact,
     * this method really ought to return an iterator instead of a
     * collection.
     * 
     * @param reportSelectionCriteria determines which records to include
     * @return Collection of ReportDetail records/objects or null.
     *
     * Called by printing\DispenserItemsReport.java:75     
     * printing\FTPMessage.java:44
     * printing\MoneyGramReport.java:93
     */
    public static Collection<ReportDetail> getDetailRecords(
            final ReportSelectionCriteria criteria)
    {
        Collection<ReportDetail> result;
        try {
            result = new ReportDetailCollection(
                    getInstance().getDetailFile(), criteria); 
        }
        catch (IOException e) {
            Debug.println("getDetailRecords caught IOException " + e);	
            result = new ArrayList<ReportDetail>();
        }
        // Debug.println("getDetailRecords() =>\n" + result);	
        return result;
    }
                                
    /**
     * Returns name of exported data file name just created.
     * @return exported file name (currently an empty string)
     * 
     * Called by printing\Report.java:98 (but that method isn't 
     * being called from anywhere.)
     *
     * @deprecated This doesn't belong here. The export method in
     * Report (or where ever it is) should do the file I/O, and
     * just ask ReportLog for the data.
     */
    public static String getExportedFileName() { 
        return ""; 	
    }
    
    /**
     * Returns the first unified report detail in the detail file as a
     * ReportDetail. Normally returns the first record in the file, but 
     * it is possible that unified report detail will be extended in the 
     * future to contain data that isn't supported by ReportDetail. 
     * UnifiedReportDetail.reportDetail() will return null in that case,
     * so this method iterates through the unified report detail records 
     * and returns the first one where that method doesn't return null.
     * Returns null if there is no such record or the detail file is empty.
     *
     * Called by dialogs\DateEntryDialog.java:68
     * dialogs\DateTimeEntryDialog.java:47
     */
    public static ReportDetail getFirstDetailRecord() {
        Iterator<UnifiedReportDetail> i; 
        UnifiedReportDetail detail;
        ReportDetail reportDetail;
        try {
            i = getInstance().detailIterator();
            while (i.hasNext()) {
                detail = i.next();
                if (detail == null) {
                    return null;        // this shouldn't happen
                }
                
                reportDetail = detail.reportDetail();
                if (reportDetail != null) {
                    return reportDetail;
                }
            }

            return null;
        }
        catch (NoSuchElementException e) {
            return null;
        }
        catch (IOException e) {
            return null;
        }
    }

    /**
     * Returns a new, empty TreeSet with a suitable comparator.
     * This is for methods that want to generate dummy data for test 
     * reports, and for methods that need to combine data.
     *  
     * Called by printing\DispenserItemsReport.java:48
     * printing\MoneyGramReport.java:49
     * printing\MoneyGramReport.java:50
     * printing\MoneyGramReport.java:94
     * printing\MoneyGramReport.java:95
     * printing\MoneyGramReport.java:96
     * printing\MoneyGramReport.java:238
     * printing\MoneyGramReport.java:239
     */
    public static TreeSet<ReportDetail> getNewDetailRecordCollection() {
        return new TreeSet<ReportDetail>(DETAIL_RECORD_COMPARATOR);
    }

    /**
     * Returns the collection of summary records after updating
     * them. 
     *
     * Note: It would be more efficient to initialize the summary
     * at startup and keep it up to date as each detail record is
     * logged. But this way is less error prone.
     *
     */
    private Collection<ReportSummary> getSummaryRecords(boolean includeToday) {
        if (isEmpty()) {
            Debug.println("getSummaryRecords: logging period change");	
            logPeriodChange();
        }

        summarizeDetail(includeToday);
        return summaryRecords.values();
    }

    
    /**
     * Returns a shallow copy of the summary records including a
     * summary record for the current period. Suitable for taking 
     * the iterator, each element will be a ReportSummary.
     * This method should really return an iterator.
     * 
     * Called by dialogs\DateEntryDialog.java:64 (doesn't use it)
     * dialogs\DatePeriodEntryDialog.java:42
     * dialogs\DateTimeEntryDialog.java:43 (doesn't use it)
     * dialogs\DetailSelectionEntryDialog.java:65
     */
    public static TreeSet<ReportSummary> getSummaryRecordsClone() {
    	TreeSet<ReportSummary> result = null;
        try {
            result = new TreeSet<ReportSummary>(SUMMARY_RECORD_COMPARATOR);
            result.addAll(getInstance().getSummaryRecords(true));
            //Debug.println("getSummaryRecordsClone() => \n" + result);	
            return result;
        }
        catch (IOException e) {
            ErrorManager.writeStackTrace("Failed in getSummaryRecordsClone", e); 
//            return getNewDetailRecordCollection();
            return result; 
        }

    }

    private boolean isEmpty() {
        return detail.isEmpty();
    }
    
    /**
     * Returns true if the detail log has less than N bytes of free space.
     * Currently, N is 20 kilobytes.
     */
    public boolean isNearlyFull() {
	return ( detail.freeSpace() < 20000);
    }
    
    /** Writes a detail record to the detail file. */
    private void logDetail(UnifiedReportDetail record) throws IOException {
        record.writeTo(detail);
        detail.flush();
    }

    /** Writes a detail record to the detail file. */
    private static void logDetail(UnifiedReportDetail record, 
            String method) {
        //Debug.println("logDetail called from " + method + ":");	 
        //Debug.println("  " + record);	
        //Debug.println("REPORTLOG " + method + ": " + record.toString());	 
        Debug.println("REPORTLOG " + record.toString());
        
        try {
            getInstance().logDetail(record);
        }
        catch (IOException e) {
            ErrorManager.writeStackTrace("Failed in " + method, e); 
        }
    }
    
    public static void markLogLocation() {
    	try {
			getInstance().detail.lock();
		} catch (IOException e) {
			Debug.printStackTrace(e);
		}
    }
    
    public static void unMarkLogLocation() {
    	try {
			getInstance().detail.unLock();
		} catch (IOException e) {
			Debug.printStackTrace(e);
		}
    }
    
    public static void rewindLogToMark() {
    	try {
    		ReportLog instance = getInstance();
    		instance.detail.rewind();
    		instance.detail.unLock();
    		instance.detail.flush();
		} catch (IOException e) {
			Debug.printStackTrace(e);
		}
    }
    
    /**
     * Writes a dispenser load record to the detail records file.
     * 
     * Called by dispenserload\DispenserLoadTransaction.java:815
     */
    public static void logDispenserLoad(short dispenserID,
            String docSerialNbrStart, String docSerialNbrEnd,
            boolean flagCardLoad, String employeeID) 
    {
        long dateTime = TimeUtility.currentTimeMillis();
        int period = Period.getPeriod();
        int user = Integer.parseInt(employeeID);

        UnifiedReportDetail record = new UnifiedReportDetail(
        		ReportLogConstants.DISPENSER_LOAD_RECORD,
                0, period, user, dateTime);

        record.set(ReportLogConstants.ABSOLUTE_PERIOD, 
                Period.getAbsolutePeriod(dateTime, period));
        record.set(ReportLogConstants.SERIAL_NUMBER_START, 
                docSerialNbrStart);
        record.set(ReportLogConstants.SERIAL_NUMBER_END, 
                docSerialNbrEnd);
        record.set(ReportLogConstants.DISPENSER_NUMBER, 
                (int) dispenserID);
        record.setFlag(ReportLogConstants.CARD_LOAD_FLAG,
                flagCardLoad);

        logDetail(record, "logDispenserLoad"); 
    }

    /**
     * Writes an invalid login record to the detail records file.
     *
     * Called by ErrorManager.java:390
     */
    public static void logInvalidLogin(int user) {
        long dateTime = TimeUtility.currentTimeMillis();
        int period = Period.getPeriod();

        UnifiedReportDetail record = new UnifiedReportDetail(
        		ReportLogConstants.INVALID_LOGIN_RECORD,
                0, period, user, dateTime);

        record.set(ReportLogConstants.ABSOLUTE_PERIOD, 
                Period.getAbsolutePeriod(dateTime, period));

        logDetail(record, "logInvalidLogin"); 
    }
    
    /**
     * Writes a MoneyGram Receive record to the detail records file.
     * 
     * Called by moneygramreceive\MoneyGramReceiveTransaction.java:484
     */
    public static void logMoneyGramReceive(MoneyGramReceiveDiagInfo mgrdi, MoneyOrderInfo moi, String payeeName, String currencyCode) {

        long dateTime = TimeUtility.currentTimeMillis();
        int period = Period.getPeriod();
        int user = mgrdi.getEmployeeNumber();
        UnifiedReportDetail record = new UnifiedReportDetail(ReportLogConstants.MONEYGRAM_RECORD, moi.getDocumentType().intValue(), period, user, dateTime);

        record.set(ReportLogConstants.ABSOLUTE_PERIOD, Period.getAbsolutePeriod(dateTime, period));
        record.set(ReportLogConstants.AMOUNT, mgrdi.getAmount());
        record.set(ReportLogConstants.FEE, 0); // assumes no fee on receives
        record.set(ReportLogConstants.REFERENCE_NUMBER, mgrdi.getReferenceNumber());
        record.set(ReportLogConstants.CURRENCY_CODE, currencyCode);
        record.set(ReportLogConstants.EXCHANGE_RATE, mgrdi.getExchangeRate());
        record.set(ReportLogConstants.ATTEMPTS, mgrdi.getNumberOfAttempts());
        record.setFlag(ReportLogConstants.SUCCESS_FLAG, mgrdi.isSuccessful());

        record.set(ReportLogConstants.SERIAL_NUMBER, moi.getSerialNumber());
        record.set(ReportLogConstants.PAYEE, payeeName);
        record.set(ReportLogConstants.EMPLOYEE_NUMBER, user);
        record.set(ReportLogConstants.ACCOUNT_NUMBER, moi.getMoAccountNumber());

        logDetail(record, "logMoneyGramReceive"); 
    }
    
    /**
     * Writes a MoneyGram Send record or BillPayment record to 
     * the detail records file. The first parameter distinguishes
     * normal sends from bill pay sends, using the constants
     * carried over from the old version of ReportLog.
     *
     * Called by moneygramsend\MoneyGramSendTransaction.java:875
     */
    public static void logMoneyGramSend(final int reportType,
    		DataCollectionSet dataCollectionSet,
            final MoneyGramSendDiagInfo sendDiagInfo,
            final MoneyGramInfo40 returnInfo,boolean isBillPayment) {

        long dateTime = TimeUtility.currentTimeMillis();
        int period = Period.getPeriod();
        int user = sendDiagInfo.getEmployeeNumber();
        int productType = ReportLogConstants.MONEYGRAM_SEND_TYPE;

        if (reportType == ReportLog.EXPRESS_PAYMENT_TYPE)
            productType = ReportLogConstants.EXPRESSPAY_TYPE;
        else if (reportType == ReportLog.UTILITY_PAYMENT_TYPE)
        	productType = ReportLogConstants.UTILITYPAY_TYPE;
        else if (reportType == ReportLog.PREPAID_SERVICES_TYPE)
        	productType = ReportLogConstants.PREPAID_TYPE;
        else 
        	productType = ReportLogConstants.MONEYGRAM_SEND_TYPE;
        	

        UnifiedReportDetail record = new UnifiedReportDetail(
        		ReportLogConstants.MONEYGRAM_RECORD,
                productType, period, user, dateTime);

        record.set(ReportLogConstants.ABSOLUTE_PERIOD, 
                Period.getAbsolutePeriod(dateTime, period));
        record.set(ReportLogConstants.REFERENCE_NUMBER, 
                returnInfo.getCommitTransactionInfoPayload().getReferenceNumber());
        record.set(ReportLogConstants.AMOUNT, dataCollectionSet.getFieldStringValue(FieldKey.SEND_AMOUNT_KEY));
        record.set(ReportLogConstants.FEE, returnInfo.getFee(isBillPayment,false));
		if (UnitProfile.getInstance().get("ALLOW_TAX_REPORT", "N").equals("Y")) {
			String tax = String.valueOf(returnInfo.getTaxSendNonMgi(false, true));
			record.set(ReportLogConstants.TAX_EXT, tax);
		}
		
		com.moneygram.agentconnect.BPValidationResponse.Payload bpValPayload = returnInfo.getBpValidationResponsePayload();
        if( bpValPayload != null )
        {
        	record.set(ReportLogConstants.EXCHANGE_RATE, bpValPayload.getExchangeRateApplied()); 
            record.set(ReportLogConstants.CURRENCY_CODE, bpValPayload.getSendAmounts().getSendCurrency());
            
            if( bpValPayload.getProcessingFee() != null) {
            	record.set(ReportLogConstants.PROCESSING_FEE, bpValPayload.getProcessingFee());
           	   // set info fee indication same as for the host report.  Force it to false if no
               // processing fee exists thereby it will not be part of the processing fee report.
               
               if (bpValPayload.getProcessingFee().signum() > 0)
               	record.set(ReportLogConstants.INFO_FEE_IND, "false"); 
               else
               	record.set(ReportLogConstants.INFO_FEE_IND, bpValPayload.isInfoFeeIndicator()+""); 
            }
        }
               
       
        try {
        
        if (productType == ReportLogConstants.MONEYGRAM_SEND_TYPE) {
        	record.set(ReportLogConstants.EXCHANGE_RATE, returnInfo.getForeignExchange()); 
            record.set(ReportLogConstants.CURRENCY_CODE, dataCollectionSet.getFieldStringValue(FieldKey.SEND_CURRENCY_KEY));
        }
        record.set(ReportLogConstants.ATTEMPTS, 
                sendDiagInfo.getNumberOfAttempts());
        record.setFlag(ReportLogConstants.SUCCESS_FLAG,
                sendDiagInfo.isSuccessful());
        record.set(ReportLogConstants.EMPLOYEE_NUMBER, 
                user);

        //Debug.println("logMoneyGramSend: sendInfo.getAmount()=" 	
        //        + sendInfo.getAmount());
        //Debug.println("logMoneyGramSend: returnInfo.getForeignExchange()=" 	
        //        + returnInfo.getForeignExchange());

        logDetail(record, "logMoneyGramSend"); 
        } catch (Exception e) {
        	Debug.printException(e);
        }
    }

  
    
   
    
    /**
     * Writes a MoneyGram Reversal record to the detail records file.
     *
     * Detail data written to record:
     *   Date and time of original transaction
     *   Original reference #
     *   Sender name (first and last name)
     *   User ID who processed the cancellation
     *   Transaction amount
     */
    public static void logMoneyGramReversal(Date sentTime, SendAmountInfo sendAmounts, String firstName, String lastName, String operatorName, String referenceNumber, int empNumber) {

        long dateTime = TimeUtility.currentTimeMillis();
        int period = Period.getPeriod();
        int recordType = ReportLogConstants.MONEYGRAM_REVERSAL_RECORD;
        int productType = ReportLogConstants.MONEYGRAM_REVERSAL_TYPE;
        UnifiedReportDetail record = new UnifiedReportDetail(recordType, productType, period, 0, dateTime);
        
        record.set(ReportLogConstants.ABSOLUTE_PERIOD, Period.getAbsolutePeriod(dateTime, period));
		record.set(ReportLogConstants.TIME, new SimpleDateFormat(TimeUtility.TIME_12_HOUR_MINUTE_FORMAT2).format(sentTime));
        record.set(ReportLogConstants.REFERENCE_NUMBER, referenceNumber);
        record.set(ReportLogConstants.SENDER, lastName + ", " + firstName);	

        BigDecimal sendAmount = sendAmounts.getSendAmount();
        BigDecimal feeAmount = null;
		if (sendAmounts != null) {
			List<AmountInfo> amounts = sendAmounts.getDetailSendAmounts().getDetailSendAmount();
			if (amounts != null) {
				for (AmountInfo amountInfo : amounts) {
					if (amountInfo.getAmountType().equalsIgnoreCase(DWValues.MF_SEND_AMT_TOTAL_FEE_TAX)) {
						feeAmount = amountInfo.getAmount();
					}
				}
			}
		}

        BigDecimal totalAmount = sendAmount.add(feeAmount);
        record.set(ReportLogConstants.AMOUNT, totalAmount);
        record.set(ReportLogConstants.CURRENCY_CODE, sendAmounts.getSendCurrency());
        record.set(ReportLogConstants.EMPLOYEE_NUMBER, String.valueOf(empNumber));

        record.set(ReportLogConstants.USER, operatorName); 
        record.setFlag(ReportLogConstants.SUCCESS_FLAG, true);
        logDetail(record, "logMoneyGramReversal"); 
    }
    
    /**
     * Writes money order record to the detail records file.
     * 
     * Called by dispenser\Dispenser.java:566
     * moneyorder\MoneyOrderTransaction.java:651
     * moneygramreceive\MoneyGramReceiveTransaction.java:493
     * moneygramreceive\MoneyGramReceiveTransaction.java:495
     */
    public static void logMoneyOrder(final MoneyOrderInfomation mo) {
        UnifiedReportDetail record = new UnifiedReportDetail(mo);
        logDetail(record, "logMoneyOrder"); 
    }

    public static void logMoneyOrder(final MoneyOrderInfo mo40, MoneyOrderDisplayFields modf) {
        MoneyOrderInfomation mo = new MoneyOrderInfomation(mo40.getDocumentType().intValue(),
                                                mo40.getEmployeeID().intValue(),
                                                mo40.getItemAmount(),
                                                mo40.getItemFee());
        mo.setDocumentType(mo40.getDocumentType().toString());
        mo.setCreationDateTime(mo40.getDateTimePrinted().toGregorianCalendar().getTime());
        mo.setDispenserNumber(mo40.getDispenserID());

        mo.setDocumentNumber(mo40.getDocumentSequenceNbr());
        mo.setSerialNumber(mo40.getSerialNumber());
        mo.setPayeeID(mo40.getVendorNumber());

        mo.setTaxID(mo40.getTaxID());
        mo.setDiscountAmount(mo40.getDiscountAmount());
        mo.setDiscountPercentage(mo40.getDiscountPercentage());

//        if (UnitProfile.getUPInstance().isTrainingMode()) {
//            mo.setTrainingFlag(true);
//        }
        mo.setTrainingFlag(modf.isTrainingFlag());

        if(MoneyOrderPrintStatusType.ATTEMPTED.equals(mo40.getPrintStatus())) {
            if(mo.getTrainingFlag()){
                mo.setVoidFlag(false);
            }
        } 
                
        if(MoneyOrderPrintStatusType.CONFIRMED_PRINTED.equals(mo40.getPrintStatus())) {
            if(mo.getTrainingFlag())
                mo.setVoidFlag(true,3);

            mo.setAttempted(true);
            mo.setCompleted(true);
            mo.setPrinted(true);
        }
        else{
            mo.setPrinted(false);
        }

        if (mo40.isVoidFlag()) {
            if (MoneyOrderVoidReasonCodeType.DISPENSER_ERROR.equals(mo40.getVoidReasonCode()))	
                mo.setVoidFlag(true,1);
        }  
        logMoneyOrder(mo);
    }


    /**
     * Writes a period change record to the detail records file.
     *
     * Implementation note: contains value-getting logic from the old
     * zero-arg ReportDetail constructor.
     * 
     * This method is called twice by Period.java when a period changes;
     * once to close the old period and a second time to open the new one.
     *
     * Note: Here's something odd: This is a zero-arg method. There is no
     * indication if we're opening or closing the period, whether it is
     * automatic or manual, or whether it is on schedule or happening at
     * startup time (because the PC was off at the period close time).
     * If manual, there's no way to know the logged in user. If we're 
     * catching up at startup time, there's no way to know what the 
     * nominal period start or end time was. (Which means subsequent code
     * has to figure things out from context, which is error-prone.)     
     *
     * Called by utility\Period.java:347
     * utility\Period.java:352
     * 
     * @deprecated Use logPeriodChange(period, automatic, user, closing).
     */
    public static void logPeriodChange() {
        long dateTime = TimeUtility.currentTimeMillis();
        int period = Period.getPeriod();

        UnifiedReportDetail record = new UnifiedReportDetail(
        		ReportLogConstants.PERIOD_CHANGE_RECORD,
                0, period, 0, dateTime);

        record.set(ReportLogConstants.ABSOLUTE_PERIOD, 
                Period.getAbsolutePeriod(dateTime,period));

        //Debug.println("logPeriodChange: dateTime = " + dateTime	
        //        + " period = " + period);	
        //Debug.println(record.toString());
                
        logDetail(record, "logPeriodChange"); 
    }

    /**
     * Writes a period change record to the detail records file.
     * This method is called twice by Period.java when a period changes;
     * once to close the old period and a second time to open the new one.
     *
     * This version takes arguments to get around the problems noted 
     * above, and skips the period close record if the report log is 
     * otherwise empty.
     *
     * This fixes a bug where a spurious period record occured at the 
     * start of the report log, due to the recomputing of the period
     * when the warehouse profile is replaced by the real profile.
     * 
     * The automatic and closing flags are not currently saved in the 
     * record, but they could be added if needed.
     *
     * @param period the new period number
     * @param automatic true means automatic, false means manual
     * @param user the user ID, if manual
     * @param closing true means period closing, false means opening
     */
    public static void logPeriodChange(int period, boolean automatic,
            int user, boolean closing) {

        long dateTime = TimeUtility.currentTimeMillis();

        try {
            if (getInstance().isEmpty() && closing) {
                Debug.println("logPeriodChange: isEmpty, skipping period close");	
                return;
            }
        }
        catch (IOException e) {
            ErrorManager.writeStackTrace("Failed in logPeriodChange", e); 
        }

        UnifiedReportDetail record = new UnifiedReportDetail(
        		ReportLogConstants.PERIOD_CHANGE_RECORD,
                0, period, user, dateTime);

        record.set(ReportLogConstants.ABSOLUTE_PERIOD, 
                Period.getAbsolutePeriod(dateTime,period));

        //Debug.println(record.toString());
                
        logDetail(record, "logPeriodChange"); 
    }
        
     
    /** For debugging. */
    public static void printDetailRecords() throws IOException {
        Iterator<UnifiedReportDetail> i = getInstance().detailIterator();
        while (i.hasNext()) {
            //Debug.println(i.next());
        }
    }

    /** 
     * Iterates through detail records to create summary records. 
     * Summary records are indexed by the tuple (absolutePeriod, 
     * period). Absolute period isn't sufficient by itself because
     * if the period start day changes, two different periods can
     * start on the same day. Sets summaryRecords to a new TreeMap
     * containing instances of ReportSummary.
     * 
     * Note: Here's something wacky: ReportSummary overrides equals() 
     * in such a way that it always returns false, so a simple Set
     * containing them can wind up with duplicates in the sense that
     * SummaryRecordComparator returns 0. That's why I'm using a
     * TreeMap instead.
     *
     * Implementation note: contains some of the logic from
     * ReportSummary.addDetail(Collection), which is no longer called.
     * The range of records contributing to a period is supposed to start 
     * with a period change record containing the open time, and end with 
     * another one containing the close time. That means the minimum 
     * timestamp in the set of relevant records is the period open time,
     * and the maximum timestamp is the period close time. The currently
     * open period can then be treated as a special case.
     *
     * Note that if the range of records is not bracked by period change
     * records, the result is a bit different from what the previous 
     * version did. (It used TimeUtility.getAccountingStartDateTime to 
     * set period open and close times to what they should have been,
     * assuming the period start day and start time haven't changed.)
     * I think this way makes more sense to me.
     *
     * Update: The min-max approch seems to be causing problems for the
     * existing report generating code, so I'm going to fill in the 
     * scheduled times after all. There is no special value the reports
     * understand to mean the period is still open, instead the reports
     * call a method in ReportSummary to ask it.
     *
     * @param includeToday if true, a partial summary is included
     * for the currently-open period.
     */
    public void summarizeDetail(boolean includeToday) {
        UnifiedReportDetail d;
        byte period;
        long absolutePeriod;
        ReportSummary s;
        SummaryKey k;
        // used to set start and end times from the scheduler
        long absolutePeriodBaseTime = Period.getAbsolutePeriodBaseTime();
        
        //Debug.println("summarizeDetail(" + includeToday + ")");	 
        summaryRecords = new TreeMap<SummaryKey, ReportSummary>();
        Iterator<UnifiedReportDetail> i = detailIterator();
        while (i.hasNext()) {
            d = i.next();
            period = d.getPeriod();
            absolutePeriod = d.getLong(ReportLogConstants.ABSOLUTE_PERIOD);
            
            k = SummaryKey.getInstance(absolutePeriod, period);
            if (summaryRecords.containsKey(k)) {
                s = summaryRecords.get(k);
            }
            else {
                s = new ReportSummary(absolutePeriod);
                s.setPeriod(period);
                //s.setPeriodOpened(d.getDateTime());
                s.setPeriodOpend(absolutePeriodBaseTime +
                        (absolutePeriod * TimeUtility.DAY));
                s.setPeriodClosed(s.getPeriodOpened() + TimeUtility.DAY);
                
                summaryRecords.put(k, s);
            }
            
            s.addDetail(d.reportDetail());
        }

        // Find the summary for the currently open period and set period 
        // close time to a value the reports will understand means the
        // period is not closed. If includeToday is false, remove it from
        // the collection.

        k = SummaryKey.getInstance(Period.getAbsolutePeriod(), 
                Period.getPeriod());
        //Debug.println("  k = " + k);	
        // Make sure no summaries exist for periods that aren't open yet.

        Iterator<SummaryKey> j = summaryRecords.keySet().iterator();
        SummaryKey item;
        while (j.hasNext()) {
            item = j.next();
            if (item.compareTo(k) > 0) {
                Debug.println("not open yet: " + item);	
                j.remove();
            }
        }
        
//    	if (CompileTimeFlags.reportLogging()) {
//    		ExtraDebug.println("summaryRecords = \n" + summaryRecords);	
//    	}
                    
    }

    /** 
     * Puts some test data in the log. Simulates calls made by
     * MoneyOrderTransaction and Dispenser.
     */
/*     
    private static void setupTests() {
        // $25 money order with a fee
        logMoneyOrder(MoneyOrderInfo.createTestMoneyOrderInfo(
                    "996421408", "25.00", "0.10", "",
                    992889043234L, 2, 992889103234L, false, false, 
                    false, false, "1", "1", 1, "", 50, 
                    "MONEY ORDER", "", "", ""));
        logMoneyOrder(MoneyOrderInfo.createTestMoneyOrderInfo(
                    "996421408", "25.00", "0.10", "",
                    992889043234L, 2, 992889103234L, true, true, 
                    false, false, "1", "1", 1, "", 50, 
                    "MONEY ORDER", "", "", ""));
        
        // $30 money order with no fee
        logMoneyOrder(MoneyOrderInfo.createTestMoneyOrderInfo(
                    "996421409", "30.00", "0", "",
                    992889043234L, 2, 992889103234L, false, false, 
                    false, false, "1", "1", 1, "", 50, 
                    "MONEY ORDER", "", "", ""));
        logMoneyOrder(MoneyOrderInfo.createTestMoneyOrderInfo(
                    "996421409", "30.00", "0", "",
                    992889043234L, 2, 992889103234L, true, true, 
                    false, false, "1", "1", 1, "", 50, 
                    "MONEY ORDER", "", "", ""));    

        // intentional test void (from dispenser menu)
        logMoneyOrder(MoneyOrderInfo.createTestMoneyOrderInfo(
                    "996421410", "0", "0", "",
                    992889103234L, 2, 0L, false, false, 
                    true, false, "", "", 0, "", 0, 
                    "", "", "", ""));

        // $35 money order with a fee
        logMoneyOrder(MoneyOrderInfo.createTestMoneyOrderInfo(
                    "996421411", "35.00", "0.10", "",
                    992889163234L, 2, 992889163234L, false, false, 
                    false, false, "1", "1", 1, "", 50, 
                    "MONEY ORDER", "", "", ""));
        logMoneyOrder(MoneyOrderInfo.createTestMoneyOrderInfo(
                    "996421411", "35.00", "0.10", "",
                    992889163234L, 2, 992889163234L, true, true, 
                    false, false, "1", "1", 1, "", 50, 
                    "MONEY ORDER", "", "", ""));
    }
*/
    /** 
     * Runs a report. Simulates calls made by DeltaworksMainPanel and
     * ReportDialog.
     */
/*    
    public static void testDailySalesReport() {
        TestPrinter printer = new TestPrinter();
        Report report = new DispenserItemsReport(
                "xml/reports/dispenseritemssalesreport.xml");	
        ReportSelectionCriteria rsc = new ReportSelectionCriteria();
        //rsc.setAbsolutePeriod(Period.getAbsolutePeriod(
        //            TimeUtility.currentTimeMillis(), Period.getPeriod())); 
        rsc.setAbsolutePeriod(533);
        rsc.setShowDetail(true);
        rsc.setSummaryDateTime(992897156296L);
        report.print(printer, rsc);
        Debug.println("");	
        Debug.println(printer.getText());
    }
*/

    /** For testing. */
/*
    public static void main(String[] args) {
        try {
            if ((args.length > 0) && (args[0].equals("archive")))	
                archiveRecords();
            else
                printDetailRecords();
        }
        catch (Exception e) {
            Debug.printStackTrace(e);
        }
        System.exit(0);
    }
*/

    /*public static void main(String[] args) {
        try {
            if (args.length == 0) {
                Debug.println("usage: ReportLog filename");	
                System.exit(1);
            }
            FileInputStream in = new FileInputStream(args[0]);
            Iterator i = new UnifiedIterator(in);
            while (i.hasNext()) {
                //Debug.println(i.next());
            }
        }
        catch (Exception e) {
            //Debug.println(e);
        }
    }*/

/** This class is unchanged from older versions, for compatibility. */
static class SummaryRecordComparator implements Comparator<ReportSummary>, Serializable {
	private static final long serialVersionUID = 1L;
    @Override
	public int compare(final ReportSummary sr1, final ReportSummary sr2) {
        final Long absolutePeriod1 = Long.valueOf(sr1.getAbsolutePeriod());
        final Long absolutePeriod2 = Long.valueOf(sr2.getAbsolutePeriod());
        return absolutePeriod1.compareTo(absolutePeriod2);
    }
}

/** This class is unchanged from older versions, for compatibility.
 * 	The class has been changed to compare on Agent ID if the person is 
 *  in MultiAgentMode or on AbsolutePeriod if the person is in 
 *  normal mode 
 * 
 *  */
static class DetailRecordComparator implements Comparator<ReportDetail>, Serializable {
	private static final long serialVersionUID = 1L;

	@Override
	public int compare(final ReportDetail dr1, final ReportDetail dr2) {
        
        /*MoneyGramReverseDetail mgrd1 = null;
        MoneyGramReverseDetail mgrd2  = null;
        MoneyGramDetail mgd1 = null;
        MoneyGramDetail mgd2 = null;
        String agentId1 = null ,agentId2 = null;
        if(o1 instanceof MoneyGramReverseDetail) {
             mgrd1 = (MoneyGramReverseDetail)o1;
             mgrd2 = (MoneyGramReverseDetail)o2;
        }
        else {
             mgd1 = (MoneyGramDetail)o1;
             mgd2 = (MoneyGramDetail)o2;
        } */
            
        if (UnitProfile.getInstance().isMultiAgentMode()){
            
            String agentId1 = "";
            String agentId2 = "";
            
            if(dr1 instanceof MoneyGramReverseDetail) {
                agentId1 = ((MoneyGramReverseDetail)dr1).getAgentID();
            }
            else {
                agentId1 = ((MoneyGramDetail)dr1).getAgentID();
            }
            
            if(dr2 instanceof MoneyGramReverseDetail) {
                agentId2 = ((MoneyGramReverseDetail)dr2).getAgentID();
            }
            else {
                agentId2 = ((MoneyGramDetail)dr2).getAgentID();
            }
            try {
                final int agentCompare =  agentId1.compareTo(agentId2);
                if (agentCompare !=0)
                	return agentCompare;
            } catch (Exception e) {
                //Debug.println(""+e.getMessage());
                // dont know what to do.
                // if either of the agentIds are null continue to compare dates
            }
            final Long date1 = Long.valueOf(dr1.getDateTime());
            final Long date2 = Long.valueOf(dr2.getDateTime());
            final int dateCompare = date1.compareTo(date2);
            if (dateCompare != 0)
                return dateCompare;
            return 1;
            
            }
            else {
            final Long absolutePeriod1 = Long.valueOf(dr1.getAbsolutePeriod());
            final Long absolutePeriod2 = Long.valueOf(dr2.getAbsolutePeriod());
            final int periodCompare = absolutePeriod1.compareTo(absolutePeriod2);
            if (periodCompare != 0)
                return periodCompare;
            final Long date1 = Long.valueOf(dr1.getDateTime());
            final Long date2 = Long.valueOf(dr2.getDateTime());
            final int dateCompare = date1.compareTo(date2);
            if (dateCompare != 0)
                return dateCompare;
            return 1;
            }
        }
}


/** 
 * Provides compatibility for existing report code. 
 * Wraps a collection around the report detail records.
 * The only method which is actually supported is iterator().
 */
static class ReportDetailCollection extends AbstractCollection<ReportDetail> {
    CircularFile input; 
    ReportSelectionCriteria criteria;

    ReportDetailCollection(CircularFile in, 
            ReportSelectionCriteria c) {
        input = in;
        criteria = c;
    }

    @Override
	public int size() {
        int n = 0;
        for (Iterator<ReportDetail> i = iterator(); i.hasNext(); i.next()) 
            ++n;
        return n;
    }

    @Override
	public Iterator<ReportDetail> iterator() {
        return new ReportDetailIterator(input.getInputStream(), criteria);
    }
}

/**
 * Used to index summary records. (Absolute period isn't sufficient
 * because if the period start day changes, you can have two periods
 * starting on the same day.)
 * Implementation note: immutable.
 */
static class SummaryKey implements Comparable<SummaryKey> {
    long absolutePeriod;
    int period;
    static SummaryKey previous = new SummaryKey(1, 1);

    SummaryKey(long absolutePeriod, int period) {
        this.absolutePeriod = absolutePeriod;
        this.period = (byte) period;
    }

    // avoids object creation overhead by reusing the previous one
    // if it has the requested values
    public static SummaryKey getInstance(long absolutePeriod, int period) {
        if ((absolutePeriod != previous.absolutePeriod)
                || (period != previous.period)) {
            previous = new SummaryKey(absolutePeriod, period);
        }
        return previous;
    }

    // see contract for Comparable
    @Override
	public int compareTo(SummaryKey x) {
        // Comparable allows this method to throw ClassCastException
        SummaryKey k = x;
        long delta = (absolutePeriod * 7 + period)
            - (k.absolutePeriod * 7 + k.period);
        
        // can't just return delta because it's a long, and we could lose
        // the sign if we cast it to int.
        if (delta < 0)
            return -1;
        else if (delta > 0)
            return 1;
        else
            return 0;
    }

    // see contract for SortedMap
    @Override
	public boolean equals(Object x) {
        if (x == null)
            return false;
        try {
            return (compareTo((SummaryKey) x) == 0);
        }
        catch (ClassCastException e) {
            return false;
        }
    }

    @Override
	public String toString() {
        return "(" + absolutePeriod + ", " + period + ")";   
    }
}

/*
    Methodology: My first step was to figure out which methods were 
    actually in use outside this class and therefore needed to be 
    reimplementing using the new format. To do this I deleted all
    the methods from this class, and recompiled everything. That
    generated 34 "method not found" errors. Working from that list
    and a backup copy of the class, I copy-and-pasted the doc 
    comments and method declarations for each method reported as
    missing. I deleted ReportLogTestDataLoad first, because methods
    called only from a test driver don't count.
    
    This left me with 18 public methods to reimplement. I marked two
    of them as deprecated right away, because they were specific to
    details of the earlier implementation. There will probably be a
    couple of new methods specific to this one.

    My second step was to go back to the old version and try to write
    unit tests. This was harder than I expected, so I asked for help.

    My third step was to write UnifiedReportDetail, which replaces
    the existing Detail classes and handle low-level IO for detail
    records.

    My fourth, surprisingly difficult step was to write all the log 
    methods (converting assorted input parameters into unified report
    detail records), and the reportDetail method (converting a unified
    report detail into the old report detail subclasses expected by 
    the existing report generating code). I added the toString method
    to unified report detail at this time for debugging. As I've 
    noted before, the design of Deltaworks requires far too much 
    piddly setFoo(convertFoo(getFoo())) code.
*/


}

