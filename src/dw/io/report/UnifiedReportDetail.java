package dw.io.report;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.xml.datatype.XMLGregorianCalendar;

import com.moneygram.agentconnect.ComplianceTransactionRequest;
import com.moneygram.agentconnect.ComplianceTransactionRequestType;
import com.moneygram.agentconnect.CustomerComplianceInfo;
import com.moneygram.agentconnect.CustomerComplianceTypeCodeType;
import com.moneygram.agentconnect.LegalIdType;
import com.moneygram.agentconnect.MoneyOrderInfo;
import com.moneygram.agentconnect.MoneyOrderPrintStatusType;
import com.moneygram.agentconnect.MoneyOrderVoidReasonCodeType;
import com.moneygram.agentconnect.PhotoIdType;

import dw.io.diag.DiagMessage;
import dw.moneyorder.MoneyOrderInfomation;
import dw.profile.UnitProfile;
import dw.utility.Debug;
import dw.utility.ExtraDebug;
import dw.utility.Period;
import dw.utility.TimeUtility;
import flags.CompileTimeFlags;

/** 
 * Supports reading and writing any kind of detail record. Records are
 * stored in a concise, extensible, self-describing manner. Older versions
 * of this class should be able to read records written by newer ones,
 * silently ignoring any fields or record types that have been added. 
 * The goal is a reasonable balance between brevity and flexibility.
 *
 * When written to an output stream, each record consists of a record 
 * type number, a record length, and a variable number of fields. Each
 * field consists of a field number, a field length, and the contents of
 * the field. The field contents are usually just ASCII encoded. There
 * are a bunch of common fields which are ganged together to form a
 * header field which should always be present. The contents of the 
 * header field are packed in binary.
 * 
 * All fields except the header field are considered optional. Optional
 * fields have a default value, such as a zero amount or an empty string.
 * Optional fields whose value is equal to the default value are omitted
 * when the record is written to the output stream. After the record is
 * read back in from an input stream, queries on an optional field will
 * return the default value if the field was omitted from the record.
 *
 * Compared to earlier implementations, there are a few fields which I 
 * have "demoted" to optional status. These include "amount" and
 * "absolute period." I demoted amount because I wanted to switch to 
 * ASCII encoding for it; I was afraid the existing binary format with
 * implied decimal point would be a problem when and if we have to add
 * support for different currencies. I demoted absolute period because
 * it is an artifact of the previous TreeSet implementation, which 
 * used it for sorting and searching. It is a strange concept and we 
 * might want to get rid of it someday. On the other hand, it is really
 * useful and in any case it will have to be kept around for a while 
 * for backward compatibility.
 *
 * A typical money order winds up at 50 bytes for the limbo record
 * plus 47 bytes for the non-limbo record, for a total of 97 bytes.
 * The best way to improve on that would be to eliminate the need
 * for the limbo record to be written.
 *
 * @author Geoff Atkin
 */
public class UnifiedReportDetail implements ReportLogConstants {
	
	private static final int MAX_READFROM_DEPTH = 10;

    // all records begin with a record type    
    private byte recordType;

    // all records have a header with the following fields
    private byte productType = 0;  //document or transaction type
    private byte period = 0;
    private byte user = 0;
    private long dateTime = 0;
    private int majorLength = 1;    // length of first dimension on field[][][]
    
    // Remaining fields are optional, depending on record type.
    // field[currentIndex][fieldNumber] is a byte array in DataOutputStream
    // format representing a field value.
    private byte field[][][];

    private int currentIndex = 0;   // goes from 0 to (majorLength - 1)

    // As of DW4.0, need one set of flags for each item in the mo array.
    // Bit 0 of flagArray[currentIndex] is flag 0 for the current item.
    private int flagArray[];
	
    /** Constructs a new Unified Report Detail object. */
    public UnifiedReportDetail() {
        currentIndex = 0;
        field = new byte[1][NUMBER_OF_FIELD_TYPES][];
        flagArray = new int[1];
    }
    
    /**
     * Constructs a Unified Report Detail with the given header info.
     * This kind of UnifiedReportDetail is used for period changes,
     * dispenser loads, and MoneyGram transactions.
     */
    public UnifiedReportDetail(int recordType, int productType, 
            int period, int user, long dateTime) {
        this();
        this.recordType = (byte) recordType;
        this.productType = (byte) productType;
        this.period = (byte) period;
        this.user = (byte) user;
        this.dateTime = dateTime;
    }
    
    /** 
     * Constructs a Unified Report Detail from a Money Order Info. 
     * This kind of UnifiedReportDetail replaces MoneyOrderDetail.
     */
    public UnifiedReportDetail(MoneyOrderInfomation mo) { 
        this();
        recordType =            DOCUMENT_RECORD;
        productType =           (byte) mo.getIntDocumentType();
        period =                mo.getPeriod();
        user =                  (byte) mo.getIntEmployeeID();
        dateTime =              mo.getCreationDateTime();
        set(ABSOLUTE_PERIOD,    Period.getAbsolutePeriod(dateTime,period));
        set(AMOUNT,             mo.getAmount());
        set(FEE,                mo.getFee());
        set(DOCUMENT_NUMBER,    mo.getDocumentNumber());
        set(SERIAL_NUMBER,      mo.getSerialNumber());
        set(PAYEE_ID,           mo.getPayeeID());
        setFlag(LIMBO_FLAG,     !(mo.isPrinted()));
        setFlag(VOID_FLAG,      mo.getVoidFlag());
        //setFlag(GROUP_FLAG,   mo.getSaleIssuanceTag().equals("1"));	
        setFlag(TRAINING_FLAG,  mo.getTrainingFlag());
        
        if (mo.getReportDocumentType() == ReportLog.VOID_TYPE) {
            productType = TEST_VOID_TYPE;
        }

        // Additional option fields for Wal Mart associate discount
        if (mo.getDiscountAmount().signum() != 0) {
            set(DISCOUNT_CARD_NUMBER,   mo.getTaxID());
            set(DISCOUNT_AMOUNT,        mo.getDiscountAmount());
            set(DISCOUNT_PERCENTAGE,    mo.getDiscountPercentage());
        }
    }
    
    /**
     * The UnifiedReportDetail constructor takes a ComplianceTransactionRequestInfo
     * object which is made up of a CCI, MoneyOrderInfo40 vector, and a MoneyGram
     * Reference Number.  The data is parsed into a multidimensional byte array.
     * Each row represents a CCI and/or a MoneyOrderInfo40.
     * @param ctri
     */
    public UnifiedReportDetail(ComplianceTransactionRequest ctri) {
        majorLength = Math.max(ctri.getMoneyOrder().size(),1);
        field = new byte[majorLength][NUMBER_OF_FIELD_TYPES][];
        flagArray = new int[majorLength];
        currentIndex = 0;
        recordType =            CUSTOMER_TRANSACTION_REQUEST_INFO_RECORD;
    
        //productType =    ?
        //period =         ?
        //user =           ?
        //dateTime =       ?
        // Pat and Geoff decided don't need values for the above. 
        // Doesn't matter that we're going
        // to trash them below if there are any money orders associated. 
        
        // Set Request Type from CTRI
        set(REQUEST_TYPE,       ctri.getRequestType());
        // Set MG reference number from CTRI
        set(MG_REFERENCE_NUMBER, ctri.getMgReferenceNumber());
    
        // Set CCI data from CTRI
        CustomerComplianceInfo cci = ctri.getCci();
    
    	// If the transaction did not need a CCI then none of its fields 
    	// need to be saved, they'd all be the default values. Note that
    	// we set aside typeCode 0 (none/empty) so we wouldn't have to 
    	// worry about middleware logic depending on the POS/RTS interface
    	// preserving (cci == null) correctly. 		--gea 
    	
//    	if ((cci != null) && (cci.getTypeCode() != null) && (! cci.getTypeCode().equals(CustomerComplianceTypeCode.EMPTY_OR_NONE))) {
    	if (cci != null) {
            set(TYPE_CODE,          cci.getTypeCode());
            set(LOCAL_DATE_TIME,    cci.getLocalDateTime());
            set(EMPLOYEE_NUMBER,    cci.getEmployeeNumber());
//            set(KEY,                cci.getKey());
            set(USER_NAME,          cci.getOperatorName());
            set(COMMENTS,           cci.getComments());
            set(CATEGORY,           cci.getCategory());
            set(OTHER_CATEGORY,     cci.getOtherCategory());
            set(CHARACTER,          cci.getCharacter());
            set(LAST_NAME,          cci.getLastName());
            set(FIRST_NAME,         cci.getFirstName());
            set(MIDDLE_INITIAL,     cci.getMiddleInitial());
            set(ADDRESS,            cci.getAddress());
            set(ADDRESS2,           cci.getAddress2());
            set(CITY,               cci.getCity());
            set(STATE,              cci.getState());
            set(ZIP,                cci.getZipCode());
            set(COUNTRY,            cci.getCountry());
            set(PHOTO_ID_TYPE,      cci.getPhotoIdType() != null ? cci.getPhotoIdType().value() : null);
            set(PHOTO_ID_NUMBER,    cci.getPhotoIdNumber());
            set(PHOTO_ID_STATE,     cci.getPhotoIdState());
            set(PHOTO_ID_COUNTRY,   cci.getPhotoIdCountry());
            set(DATE_OF_BIRTH,      cci.getDateOfBirth());
            set(PHONE_NUMBER,       cci.getPhoneNumber());
            set(OCCUPATION,         cci.getOccupation());
            set(LEGAL_ID_TYPE,      cci.getLegalIdType() != null ? cci.getLegalIdType().value() : null);
            set(LEGAL_ID_NUMBER,    cci.getLegalIdNumber());
            set(THIRD_PARTY_DBA,    cci.getThirdPartyDBA());
            set(THIRD_PARTY_LAST_NAME, cci.getThirdPartyLastName());
            set(THIRD_PARTY_FIRST_NAME, cci.getThirdPartyFirstName());
            set(THIRD_PARTY_MIDDLE_INITIAL, cci.getThirdPartyMiddleInitial());
            set(THIRD_PARTY_ADDRESS, cci.getThirdPartyAddress());
            set(THIRD_PARTY_ADDRESS2, cci.getThirdPartyAddress2());
            set(THIRD_PARTY_CITY,   cci.getThirdPartyCity());
            set(THIRD_PARTY_STATE,  cci.getThirdPartyState());
            set(THIRD_PARTY_ZIP,    cci.getThirdPartyZipCode());
            set(THIRD_PARTY_COUNTRY, cci.getThirdPartyCountry());
            set(THIRD_PARTY_DATE_OF_BIRTH, cci.getThirdPartyDOB());
            set(THIRD_PARTY_PHONE_NUMBER, cci.getThirdPartyPhoneNumber());
            set(THIRD_PARTY_OCCUPATION, cci.getThirdPartyOccupation());
            set(THIRD_PARTY_LEGAL_ID_TYPE, cci.getThirdPartyLegalIdType() != null ? cci.getThirdPartyLegalIdType().value() : null);
            set(THIRD_PARTY_LEGAL_ID_NUMBER, cci.getThirdPartyLegalIdNumber());
    	}
        
        // Set MoneyOrder vector data
        List<MoneyOrderInfo> moVector = ctri.getMoneyOrder();
        int vectorSize = moVector.size();
        set(NUMBER_OF_MONEY_ORDERS, vectorSize);
        
        for (currentIndex = 0; currentIndex < vectorSize; currentIndex++) {
            set(INDEX, currentIndex);
            MoneyOrderInfo mo =   moVector.get(currentIndex);
            productType =           Byte.parseByte(mo.getDocumentType().toString());
            period =                Byte.parseByte(mo.getPeriodNumber() != null ? mo.getPeriodNumber().toString() : "");
            user =                  Byte.parseByte(mo.getEmployeeID().toString());
            dateTime =              mo.getDateTimePrinted().toGregorianCalendar().getTimeInMillis();
            try {
                fillInHeader();
            } catch (IOException e) {
                Debug.printStackTrace(e);
            }
            
    		// set(ABSOLUTE_PERIOD,    Period.getAbsolutePeriod(dateTime,period)); 	// not needed --gea
    		// set(ABSOLUTE_PERIOD,    mo.getPeriodNumber()); 						// not needed --gea
            set(AMOUNT,             mo.getItemAmount());
            set(FEE,                mo.getItemFee());
            set(DOCUMENT_NUMBER,    mo.getDocumentSequenceNbr());
            set(SERIAL_NUMBER,      mo.getSerialNumber());
            set(DOCUMENT_TYPE,      mo.getDocumentType());
            set(PAYEE_ID,           mo.getVendorNumber());
            set(PRINT_STATUS,       mo.getPrintStatus() != null ? mo.getPrintStatus().value() : null);
            setFlag(VOID_FLAG,      mo.isVoidFlag());			
        //    setFlag(TRAINING_FLAG,  mo.getVoidReasonCode().equals("3"));	
            set(VOID_REASON_CODE,   mo.getVoidReasonCode() != null ? mo.getVoidReasonCode().value() : null);
            set(TAX_ID,             mo.getTaxID());        
            set(DISCOUNT_AMOUNT,    mo.getDiscountAmount());
            set(DISCOUNT_PERCENTAGE, mo.getDiscountPercentage());
            set(REMOTE_ISSUANCE_FLAG, mo.isRemoteIssuanceFlag()+"");
            set(SALE_ISSUANCE_TAG,  mo.getSaleIssuanceTag());
            set(DISPENSER_NUMBER,   mo.getDispenserID());
            set(ACCOUNT_NUMBER,     mo.getMoAccountNumber());
            set(UNIT_NUMBER,        "");
            set(ACCOUNTING_START_DAY,mo.getAccountingStartDay());
            fillInFlags(); 
        }
    }

    /** 
     * Sets flags from the FLAGS field. If more flags are added later,
     * flagbuffer.length might be more than 1. But right now we
     * only know about flags in the low-order byte, so we ignore
     * the high-order bytes, if there are any.
     */
    private void extractFlags() {
        byte flagbuffer[] = getBytes(FLAGS);
        if (flagbuffer == null) {
            setFlags(0);
        }
        else {
            setFlags(flagbuffer[flagbuffer.length - 1]);
        }
    }
    
    /** 
     * Set the FLAGS field from flags.
     */
    private void fillInFlags() {
        if (getFlags() == 0) {
            set(FLAGS, "");	
        }
        else {
            byte flagbuffer[] = new byte[1];
            flagbuffer[0] = (byte) (0xFF & getFlags());
            set(FLAGS, flagbuffer);
        }
    }

    /**
     * Set the header instance variables from the HEADER field.
     */
    private void extractHeader() throws IOException {
        byte buffer[] = getBytes(HEADER);
        DataInputStream in = new DataInputStream(new ByteArrayInputStream(buffer));
        dateTime = in.readLong();
        productType = in.readByte();
        period = in.readByte();
        user = in.readByte();
        
    	if (CompileTimeFlags.reportLogging()) {
    		ExtraDebug.println("Rec - DateTime[" + TimeUtility.formatTimeForLogging(dateTime)
			+ "], productType[" + productType + "], period[" + period
			+ "], user[" + user + "]");
    	}
    }
    
    /**
     * Search for a valid header record.
     */
	private boolean isThereAValidHeader(DataInputStream in2) throws IOException {
		boolean isValid = false;
		int fieldNum, length = 0;
		long dateTime = 0;
		byte productType = 0; // document or transaction type
		byte period = 0;
		byte user = 0;
		byte recordType;

		Debug.println("Attempting to find a valid header record");
		in2.reset();
		/*
		 * Try to find a valid header record
		 */
		do {
			in2.mark(-1);
			recordType = in2.readByte();
			lSkippedBytes++;
//			ExtraDebug.println("recordType?[" + recordType + "]");
			if (recordType < NUMBER_OF_RECORD_TYPES) {
				readLength(in2);
				fieldNum = in2.read();
//				ExtraDebug.println("fieldNum?[" + fieldNum + "]");
				if (fieldNum == HEADER) {
					length = readLength(in2);
					if (length == 11) {
						dateTime = in2.readLong();
						productType = in2.readByte();
						period = in2.readByte();
						user = in2.readByte();
//						ExtraDebug.println("fieldNum[" + fieldNum
//								+ "],DateTime["
//								+ TimeUtility.formatTime(dateTime)
//								+ "], productType[" + productType
//								+ "], period[" + period + "], user[" + user
//								+ "]");
						if ((period < 1) || (period > 7)) {
//							ExtraDebug.println("Invalid Period[" + period + "]");
							in2.reset();
							recordType = in2.readByte();
						} else if ((user < -1) || (user > 99)) {
//							ExtraDebug.println("Invalid user[" + user + "]");
							in2.reset();
							recordType = in2.readByte();
						} else if ((productType < 0) || (productType > 99)) {
//							ExtraDebug.println("Invalid productType["
//									+ productType + "]");
							in2.reset();
							recordType = in2.readByte();
						} else {
							Debug.println("Valid header record found, "
									+ (lSkippedBytes - 1)
									+ " bytes of corrupted data skipped");
							isValid = true;
							break;
						}
					} else {
						in2.reset();
						recordType = in2.readByte();
					}
				} else {
					in2.reset();
					recordType = in2.readByte();
				}
			} else {
				in2.reset();
				recordType = in2.readByte();
			}
		} while (recordType != -1);
		if (length != 11) {
			throw new EOFException("invalid field length"); 
		} else {
			in2.reset();
		}
		ExtraDebug.println("DateTime[" + TimeUtility.formatTimeForLogging(dateTime)
				+ "], productType[" + productType + "], period[" + period
				+ "], user[" + user + "]");
		return isValid;
	}

    /**
     * Set the HEADER field from header instance variables.
     */
    private void fillInHeader() throws IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(buffer);
        out.writeLong(dateTime);
        out.writeByte(productType);
        out.writeByte(period);
        out.writeByte(user);
        set(HEADER, buffer.toByteArray());
    }

    private boolean getBoolean(int fieldNumber) {
    	if (field[currentIndex][fieldNumber] == null) {
            return false;	
        }
    	  try {
              String str = new String(field[currentIndex][fieldNumber], "UTF-8"); 
              return Boolean.valueOf(str);
          }
          catch (UnsupportedEncodingException e) {    // should be impossible
        	  String str = new String(field[currentIndex][fieldNumber]);
              return Boolean.valueOf(str);
          }
    }
    /** 
     * Returns one of the fields as a string.
     * @param fieldNumber number between zero and NUMBER_OF_FIELD_TYPES - 1.
     */
    private String get(int fieldNumber) {
    	String value = null;
        if (field[currentIndex][fieldNumber] != null) {
	        try {
	            value = new String(field[currentIndex][fieldNumber], "UTF-8"); 
	        } catch (UnsupportedEncodingException e) {    // should be impossible
	            value = new String(field[currentIndex][fieldNumber]);
	        }
        }

        if ((value != null) && (! value.isEmpty())) {
        	return value;
        } else {
            return null;
        }
    }

    private XMLGregorianCalendar getGregorianCalendar(int fieldNumber) {
    	String value = get(fieldNumber);
    	return TimeUtility.toXmlCalendar(value);
    }    
    
    /** 
     * Returns one of the fields as a big decimal.
     * @param fieldNumber number between zero and NUMBER_OF_FIELD_TYPES - 1.
     */
    private BigDecimal getBigDecimal(int fieldNumber) {
        if (field[currentIndex][fieldNumber] == null) {
            return BigDecimal.ZERO;	
        }

        try {
            return new BigDecimal(get(fieldNumber));
        }
        catch (NumberFormatException e) {
            return BigDecimal.ZERO;
        }
    }
    
    private BigInteger getBigInteger(int fieldNumber) {
        if (field[currentIndex][fieldNumber] == null) {
            return BigInteger.ZERO;	
        }

        try {
            return new BigInteger(get(fieldNumber));
        }
        catch (NumberFormatException e) {
            return BigInteger.ZERO;	
        }
    }
    
    /**
     * Returns a field as a byte array.
     * @param fieldNumber number between zero and NUMBER_OF_FIELD_TYPES - 1.
     */
    private byte[] getBytes(int fieldNumber) {
        return field[currentIndex][fieldNumber];
    }

    /**
     * Returns one of the boolean flags.
     */
    private boolean getFlag(int mask) {
       return ((getFlags() & mask) != 0);
    }

    /** 
     * Returns a field as a long.
     * @param fieldNumber number between zero and NUMBER_OF_FIELD_TYPES - 1.
     */
    public long getLong(int fieldNumber) {
        if (field[currentIndex][fieldNumber] == null) {
            return 0L;
        }

        try {
            return Long.parseLong(get(fieldNumber));
        } catch (Exception e) {
            return 0L;
        }
    }
    
    /** Returns a field as an int. */
    public Integer getInt(int fieldNumber) {
        if (field[currentIndex][fieldNumber] == null) {
            return 0;
        }

        try {
            return Integer.parseInt(get(fieldNumber));
        } catch (Exception e) {
            return 0;
        }
    }
    
    /**
     * Returns a field as an account number. If the field doesn't exist,
     * returns the MO_ACCOUNT value from the unit profile.
     * 
     * Note that it really doesn't make sense to store this in a URD-based
     * log file because it takes up space and won't change from record to
     * record. In addition to taking space, there's an encrypt/decrypt burden.
     * So I've added code in 4.2 to get the value from the unit profile
     * if the field wasn't set in the URD. Once 4.0 is no longer supported, 
     * we can drop the corresponding set() call from URD(CTRI).
     */
    public String getAccountNumber(int fieldNumber) {
        String moAccount;
        if (field[currentIndex][fieldNumber] == null) {
            moAccount = UnitProfile.getInstance().get("MO_ACCOUNT", "");
        }
        else {
            moAccount = get(fieldNumber);
        }
        return moAccount;
    }
    
    public int getRecordType() { return recordType; }
    public byte getProductType() { return productType; }
    public byte getPeriod()      { return period; }
    public byte getUser()        { return user; }
    public long getDateTime()    { return dateTime; }
    public long getMajorLength() { return majorLength; }

    /**
     * Deserialize from a data input stream, storing result in this object.
     */
    public void readFrom(DataInputStream in) throws IOException {
    	iReadFromDepth = 0;
    	lSkippedBytes = 0;   
    	readFrom2(in);
    }
    
    private int iReadFromDepth = 0;
    private long lSkippedBytes = 0;
    
    /**
     * private version of the readFrom method that protects against being called
     * recursively more than MAX_READFROM_DEPTH times.
     */
    private void readFrom2(DataInputStream in) throws IOException {
    	iReadFromDepth++;
    	if (iReadFromDepth > MAX_READFROM_DEPTH) {
            Debug.println("file too corrupted"); 
            throw new EOFException("file too corrupted");
    	}
        currentIndex = 0;
        int recordLength, n;
        recordType = in.readByte();
        recordLength = readLength(in);
        if (recordLength < 0) return;
        byte buffer[] = new byte[recordLength];
        byte buf2[];
        n = in.read(buffer);
        if (n < recordLength)
            throw new EOFException("truncated record"); 


    	if (CompileTimeFlags.reportLogging()) {
            ExtraDebug.println("recordType,recordLength=[" + recordType+","+recordLength + "] ");
    	}
//        ExtraDebug.print("buffer[" );
//        for (int ii = 0; ii < recordLength; ii++) {
//            ExtraDebug.print(Integer.toString(buffer[ii]&0xFF)+ ",");
//        }
//        ExtraDebug.println("]" );
        
        if (recordType >= NUMBER_OF_RECORD_TYPES)
            return;

        DataInputStream in2 = new DataInputStream(
                new ByteArrayInputStream(buffer));

        int fieldNum, length;
        boolean firstTime = true;

        while(true) {
            fieldNum = in2.read();
            
            if (fieldNum == -1) break;

            if (fieldNum >= NUMBER_OF_FIELD_TYPES) continue;

            if (fieldNum == HEADER) {
                if (!firstTime)
                    ++currentIndex;

                firstTime = false;
            }

            length = readLength(in2);

            // sanity check
            if ((length < 0) ||(length > recordLength)) {
                Debug.println("\r\n**Bad Log Record length[" + length + "]"); 
                throw new EOFException("invalid field length"); 
            }
			/*
			 *  Protect against bogus headers 
			 */
            if ((fieldNum == HEADER) && (length < 11)) {
            	// sanity check
                Debug.println("\r\n**Bad Log Header Record length[" + length + "]"); 
                
                /*
                 * Try to find a valid header record
                 */
                if (isThereAValidHeader(in)) {
                	readFrom2(in);
                	return;
                }
            } else {
                buf2 = new byte[length];
                in2.read(buf2);
//                Debug.println("read [" + currentIndex + "," + fieldNum + "] " 
//                        + dw.io.diag.ByteArray.hex(buf2));
                set(fieldNum, buf2);
                
                if (fieldNum == NUMBER_OF_MONEY_ORDERS){
                    byte[][][] oldArray = field;
                    int[] oldFlagArray = flagArray;         // Geoff's fix
                     
                    majorLength = Integer.parseInt(get(fieldNum));       // was majlen
                    field = new byte[majorLength][NUMBER_OF_FIELD_TYPES][];
                    flagArray = new int[majorLength];
                    
                    System.arraycopy(oldArray, 0, field, 0, oldArray.length);
                    System.arraycopy(oldFlagArray, 0, flagArray, 0, oldFlagArray.length);   // Geoff's fix
                }

                if (fieldNum == FLAGS) {
                    extractFlags();
                }
            }
        }
        
        currentIndex = 0;
        extractHeader();
    }
    
    /**
     * Read a length from a data input stream. See writeLength for
     * a description of how fields lengths are stored.
     */
    private int readLength(DataInputStream in) throws IOException {
        int length;
        length = in.readByte();
        if (length == -1) {
            length = in.readShort();
            /*
             * assume that if length is negative we have a value that has 
             * overflowed a signed short (but would fit in an unsigned short), so
             * strip off the sign extension bits.
             */
            if (length < 0) {
            	length = length & 0xffff;
            }
        }
        return length;
    }   
    
    /**
     * Create a new ReportDetail equivalent to this object. May create
     * a subclass of ReportDetail depending on the record type. This
     * method is for compatibility with existing report generating code.
     * It might be desirable to refactor the reports to eliminate the
     * middleman; on the other hand, there's a lot of logic in the 
     * ReportDetail classes that doesn't really fit anyplace else.
     *
     * If this unified detail represents something that isn't supported
     * by ReportDetail or by existing code that acts on ReportDetails,
     * null is returned. 
     *
     * Note that ReportDetail.type (also called reportDocumentType) is 
     * a combination of the record type and the product type (document
     * or transaction type).
     *
     * Note that ReportDetail.type distinguishes intentional test voids 
     * from money orders that happen to be voided.
     */
    public ReportDetail reportDetail() {
        ReportDetail rd = null;

        if (getFlag(SUPPRESS_FLAG))     // this record would confuse old code
            return rd;
        
        if (recordType == DOCUMENT_RECORD) {
            // should be compatible with MoneyOrderDetail(MoneyOrderInfo)
            MoneyOrderDetail detail = new MoneyOrderDetail();
            rd = detail;

            detail.setType(ReportLog.MONEY_ORDER_TYPE);
            detail.setDnetCode(DiagMessage.DNET_TT_TLOG_FDOC_DISPENSER);

            detail.setSerialNumber(ReportDetail.stringToLong(get(SERIAL_NUMBER)));
            detail.setDocumentType(productType);
            detail.setDocumentNumber(ReportDetail.stringToByte(get(DOCUMENT_NUMBER)));
            detail.setPayeeId(ReportDetail.stringToByte(get(PAYEE_ID)));
            detail.setLimboFlag(getFlag(LIMBO_FLAG));
            detail.setVoidFlag(getFlag(VOID_FLAG));
            //mod.setGroupFlag(getFlag(GROUP_FLAG));
            detail.setTrainingFlag(getFlag(TRAINING_FLAG));
            detail.setAbsolutePeriod(getLong(ABSOLUTE_PERIOD));
            detail.setDateTime(dateTime);
            detail.setAmount(getBigDecimal(AMOUNT));
            detail.setFee(getBigDecimal(FEE));
            detail.setPeriod(period);
            detail.setUserId(user);
                        
            if (productType == TEST_VOID_TYPE) {
                detail.setType(ReportLog.VOID_TYPE);
                detail.setDnetCode((byte) 0);
            }
            else if (productType == VENDOR_PAYMENT_TYPE)
                detail.setType(ReportLog.VENDOR_PAYMENT_TYPE);
            else if (productType == GIFT_CERTIFICATE_TYPE)
                detail.setType(ReportLog.GIFT_CERTIFICATE_TYPE);
            else if (productType == DELTAGRAM_TYPE)
                detail.setType(ReportLog.DELTAGRAM_TYPE);
        }
        else if (recordType == MONEYGRAM_CARD_RELOAD_RECORD) {            
            MoneyGramCardDetail detail = new MoneyGramCardDetail();
            rd = detail;
            detail.setType(ReportLog.MONEYGRAM_CARD_RELOAD_TYPE);
            detail.setCardNumber(get(CARD_NUMBER));
            detail.setAmount(getBigDecimal(AMOUNT));
			detail.setFee(getBigDecimal(FEE));
			detail.setAbsolutePeriod(getLong(ABSOLUTE_PERIOD));
            detail.setAttempts((byte) getLong(ATTEMPTS));
            detail.setName(get(LAST_NAME));
            detail.setPeriod(period);
            detail.setSuccessFlag(getFlag(SUCCESS_FLAG));
			detail.setUserId(user);
			detail.setEmployeeNumber(get(EMPLOYEE_NUMBER));
			detail.setDateTime(dateTime);
			detail.setCardType(get(CARD_TYPE));
        }
        else if (recordType == MONEYGRAM_CARD_PURCH_RECORD) {            
            MoneyGramCardDetail detail = new MoneyGramCardDetail();
            rd = detail;
            detail.setType(ReportLog.MONEYGRAM_CARD_PURCH_TYPE);
            detail.setCardNumber(get(CARD_NUMBER));
            detail.setAmount(getBigDecimal(AMOUNT));
			detail.setFee(getBigDecimal(FEE));
			detail.setAbsolutePeriod(getLong(ABSOLUTE_PERIOD));
            detail.setAttempts((byte) getLong(ATTEMPTS));
            detail.setName(get(LAST_NAME));
            detail.setPeriod(period);
            detail.setSuccessFlag(getFlag(SUCCESS_FLAG));
            detail.setUserId(user);
            detail.setEmployeeNumber(get(EMPLOYEE_NUMBER));
			detail.setDateTime(dateTime);
			detail.setCardType(get(CARD_TYPE));           
        }
        else if (recordType == MONEYGRAM_RECORD) {
            // should be compatible with MoneyGramDetail(
            // MoneyGramReceiveDiagInfo, MoneyOrderInfo)
            // or MoneyGramDetail(int, MoneyGramSendInfo, 
            // MoneyGramSendDiagInfo, MoneyGramInfo)
            
            MoneyGramDetail detail = new MoneyGramDetail();
            rd = detail;

            detail.setAbsolutePeriod(getLong(ABSOLUTE_PERIOD));
            detail.setDateTime(dateTime);
            detail.setAmount(getBigDecimal(AMOUNT));
            detail.setFee(getBigDecimal(FEE));
            detail.setTax(getBigDecimal(TAX_EXT));
            detail.setPeriod(period);
            detail.setUserId(user);
            detail.setEmployeeNumber(get(EMPLOYEE_NUMBER));
            detail.setReferenceNumber(get(REFERENCE_NUMBER));
            detail.setExchange(getBigDecimal(EXCHANGE_RATE));
            detail.setAttempts((byte) getLong(ATTEMPTS));
            detail.setSuccessFlag(getFlag(SUCCESS_FLAG));


            if (productType == MONEYGRAM_RECEIVE_TYPE) {
                detail.setType(ReportLog.MONEYGRAM_RECEIVE_TYPE);
                detail.setDnetCode(DiagMessage.DW_TT_TLOG_FDOC_MG_RECV);
                detail.setName(get(PAYEE));
                detail.setSerialNumber(get(SERIAL_NUMBER));
                detail.setCurrencyCode(get(CURRENCY_CODE));
            }
            else if (productType == MONEYGRAM_RECEIVE_TYPE_PPC) {
                detail.setType(ReportLog.MONEYGRAM_RECEIVE_TYPE_PPC);
                detail.setDnetCode(DiagMessage.DW_TT_TLOG_FDOC_MG_RECV);
                detail.setName(get(PAYEE));
                detail.setSerialNumber(get(SERIAL_NUMBER));
                detail.setCurrencyCode(get(CURRENCY_CODE));
                detail.setAccountNumber(get(ACCOUNT_NUMBER));
            }
            else if (productType == MONEYGRAM_SEND_TYPE) {
                detail.setType(ReportLog.MONEYGRAM_SEND_TYPE);
                detail.setDnetCode(DiagMessage.DW_TT_TLOG_FDOC_MG_SEND);
                detail.setCurrencyCode(get(CURRENCY_CODE));
            }
            else if (productType == EXPRESSPAY_TYPE) {
                detail.setType(ReportLog.EXPRESS_PAYMENT_TYPE);
                detail.setDnetCode(DiagMessage.DW_TT_TLOG_FDOC_MG_XPAY);
                detail.setCurrencyCode(get(CURRENCY_CODE));
            }
            else if (productType == UTILITYPAY_TYPE) {
                detail.setType(ReportLog.UTILITY_PAYMENT_TYPE);
                detail.setProcessingFee(getBigDecimal(PROCESSING_FEE));
                detail.setInfoFeeIndicator(get(INFO_FEE_IND));
                //detail.setDnetCode(DiagMessage.DW_TT_TLOG_FDOC_MG_XPAY);
            }
            else if (productType == PREPAID_TYPE) {
                detail.setType(ReportLog.PREPAID_SERVICES_TYPE);
                detail.setProcessingFee(getBigDecimal(PROCESSING_FEE));
                detail.setInfoFeeIndicator(get(INFO_FEE_IND));
                //detail.setDnetCode(DiagMessage.DW_TT_TLOG_FDOC_MG_XPAY);
            }
            else {
                // Maybe some new MG product that ReportDetail can't 
                // represent, so return null. (And rewrite reports to
                // use UnifiedReportDetail objects instead.)
                rd = null;
            }
        }
        else if (recordType == MONEYGRAM_REVERSAL_RECORD) {
            MoneyGramReverseDetail detail = new MoneyGramReverseDetail();
            rd = detail;

            detail.setAbsolutePeriod(getLong(ABSOLUTE_PERIOD));
            detail.setTime(getDateTime());
            detail.setTotal(getBigDecimal(AMOUNT));
            detail.setFee(getBigDecimal(FEE));
            detail.setTax(getBigDecimal(TAX_EXT));
            detail.setPeriod(period);
            detail.setSender(get(SENDER));
            detail.setUser(get(USER));
            detail.setReferenceNumber(get(REFERENCE_NUMBER));
            detail.setEmployeeNumber(get(EMPLOYEE_NUMBER));
            detail.setSuccessFlag(getFlag(SUCCESS_FLAG));
            detail.setCurrencyCode(get(CURRENCY_CODE));
            detail.setType(ReportLog.MONEYGRAM_REVERSAL_TYPE);
        }
        else if (recordType == DISPENSER_LOAD_RECORD) {
            DispenserLoadDetail detail = new DispenserLoadDetail();
            rd = detail;
            
            detail.setType(ReportLog.DISPENSER_LOAD_TYPE);
            detail.setDnetCode(DiagMessage.DNET_TT_TLOG_DIAG_FDOC_LOAD);

            detail.setAbsolutePeriod(getLong(ABSOLUTE_PERIOD));
            detail.setDateTime(dateTime);
            detail.setPeriod(period);
            detail.setUserId(user);
            detail.setSerialNumberStart(getLong(SERIAL_NUMBER_START));
            detail.setSerialNumberEnd(getLong(SERIAL_NUMBER_END));
            detail.setCardLoadFlag(getFlag(CARD_LOAD_FLAG));
            detail.setDispenser((byte) getLong(DISPENSER_NUMBER));
        }
        else if (recordType == INVALID_LOGIN_RECORD) {
            InvalidLoginDetail detail = new InvalidLoginDetail();
            rd = detail;

            detail.setType(ReportLog.INVALID_LOGIN_TYPE);
            detail.setDnetCode(DiagMessage.DNET_TT_TLOG_DIAG_MUST_XMIT);
            detail.setAbsolutePeriod(getLong(ABSOLUTE_PERIOD));
            detail.setDateTime(dateTime);
            detail.setPeriod(period);
            detail.setUserId(user);
        }
        else if (recordType == PERIOD_CHANGE_RECORD) {
            // There's no subclass of ReportDetail for period changes,
            // so call the do-nothing version of ReportDetail constructor.
            rd = new ReportDetail(true);
            
            // There's no ReportLog record type for period changes either.
            // Might have been an oversight, but it can't be fixed now.
            rd.setType(ReportLog.VOID_TYPE);

            rd.setDnetCode(DiagMessage.DNET_TT_TLOG_DIAG_PERIOD_CH);
            rd.setAbsolutePeriod(getLong(ABSOLUTE_PERIOD));
            rd.setDateTime(dateTime);
            rd.setPeriod(period);
        }
        else {  // some new record type that ReportDetail doesn't support
            rd = null; 
        }
        return rd;
    }
    
    private PhotoIdType getPhotoIdType(String value) {
    	PhotoIdType type = null;
    	try {
    		type = PhotoIdType.fromValue(value);
    	} catch (Exception e) {
    	}
    	return type;
    }
    
    private LegalIdType getLegalIdType(String value) {
    	LegalIdType type = null;
    	try {
    		type = LegalIdType.fromValue(value);
    	} catch (Exception e) {
    	}
    	return type;
    }
    
    private MoneyOrderVoidReasonCodeType getMoneyOrderVoidReasonCode(String value) {
    	MoneyOrderVoidReasonCodeType type = null;
    	try {
    		type = MoneyOrderVoidReasonCodeType.fromValue(value);
    	} catch (Exception e) {
    	}
    	return type;
    }
    
    /**
     * Create a ComplianceTransactionRequestInfo equivalent to this object.
     * This is basically the converse of the constructor that takes a ctri.
     * The ctri has four parts: MG Reference Number, Request Type,
     * CCI, and a vector of MoneyOrders.
     * 
     * @return ComplianceTransactionRequestInfo
     */
    public ComplianceTransactionRequest ctriDetail() {
        Debug.println("ctriDetail() this = " + this.toString());
        currentIndex = 0;
        ComplianceTransactionRequest ctri = new ComplianceTransactionRequest();
        
        // MG Reference Number
        ctri.setMgReferenceNumber(get(MG_REFERENCE_NUMBER));
        // Request Type
        ctri.setRequestType(getComplianceTransactionRequestType(REQUEST_TYPE));
        // CCI
        
        CustomerComplianceTypeCodeType typeCode = getCustomerComplianceTypeCode(TYPE_CODE); 
        if (typeCode != null) {
            CustomerComplianceInfo cci = new CustomerComplianceInfo();
        
	        cci.setTypeCode(getCustomerComplianceTypeCode(TYPE_CODE));
	           
	        cci.setOperatorName(get(USER_NAME));
	        cci.setComments(get(COMMENTS));
	        cci.setCategory(getInt(CATEGORY));
	        cci.setOtherCategory(get(OTHER_CATEGORY));
	        cci.setCharacter(getInt(CHARACTER));
	        
	        cci.setEmployeeNumber(get(EMPLOYEE_NUMBER));
	        cci.setLocalDateTime(getGregorianCalendar(LOCAL_DATE_TIME));
	            
	        cci.setLastName(get(LAST_NAME));
	        cci.setFirstName(get(FIRST_NAME));
	        String value = get(MIDDLE_INITIAL);
	        cci.setMiddleInitial((value != null) && (! value.isEmpty()) ? value.substring(0, 1) : null);
	        cci.setAddress(get(ADDRESS));
	        cci.setAddress2(get(ADDRESS2));
	        cci.setCity(get(CITY));
	        value = get(STATE);
	        cci.setState(((value != null) && (value.length() == 5)) ? value.substring(3, 5) : null);
	        cci.setZipCode(get(ZIP));
	        cci.setCountry(get(COUNTRY));
	        cci.setPhotoIdType(getPhotoIdType(get(PHOTO_ID_TYPE)));
	        cci.setPhotoIdNumber(get(PHOTO_ID_NUMBER));
	        value = get(PHOTO_ID_STATE);
	        cci.setPhotoIdState(((value != null) && (value.length() == 5)) ? value.substring(3, 5) : null);
	        cci.setPhotoIdCountry(get(PHOTO_ID_COUNTRY));
	        cci.setDateOfBirth(getGregorianCalendar(DATE_OF_BIRTH));
	        cci.setPhoneNumber(get(PHONE_NUMBER));
	        cci.setOccupation(get(OCCUPATION));
	        cci.setLegalIdType(getLegalIdType(get(LEGAL_ID_TYPE)));
	        cci.setLegalIdNumber(get(LEGAL_ID_NUMBER));
	        cci.setThirdPartyDBA(get(THIRD_PARTY_DBA));
	        cci.setThirdPartyLastName(get(THIRD_PARTY_LAST_NAME));
	        cci.setThirdPartyFirstName(get(THIRD_PARTY_FIRST_NAME));
	        value = get(THIRD_PARTY_MIDDLE_INITIAL);  
	        cci.setThirdPartyMiddleInitial((value != null) && (! value.isEmpty()) ? value.substring(0, 1) : null);
	        cci.setThirdPartyAddress(get(THIRD_PARTY_ADDRESS));
	        cci.setThirdPartyAddress2(get(THIRD_PARTY_ADDRESS2));
	        cci.setThirdPartyCity(get(THIRD_PARTY_CITY));
	        value = get(THIRD_PARTY_STATE);
	        cci.setThirdPartyState(((value != null) && (value.length() == 5)) ? value.substring(3, 5) : null);
	        cci.setThirdPartyZipCode(get(THIRD_PARTY_ZIP));
	        cci.setThirdPartyCountry(get(THIRD_PARTY_COUNTRY));
	        cci.setThirdPartyDOB(getGregorianCalendar(THIRD_PARTY_DATE_OF_BIRTH));
	        cci.setThirdPartyPhoneNumber(get(THIRD_PARTY_PHONE_NUMBER));
	        cci.setThirdPartyOccupation(get(THIRD_PARTY_OCCUPATION));
	        cci.setThirdPartyLegalIdType(getLegalIdType(get(THIRD_PARTY_LEGAL_ID_TYPE)));
	        cci.setThirdPartyLegalIdNumber(get(THIRD_PARTY_LEGAL_ID_NUMBER));
	        
	        ctri.setCci(cci);
        }

        // MoneyOrder Vector
        
        Integer size = getInt(NUMBER_OF_MONEY_ORDERS);          //getInt can return null so must use Integer
        Vector<MoneyOrderInfo> moVector = new Vector<MoneyOrderInfo>(size);

        for (currentIndex = 0; currentIndex < size; ++currentIndex) {
            MoneyOrderInfo mo = new MoneyOrderInfo();
            String value = get(PRINT_STATUS);
            mo.setPrintStatus(((value != null) && (! value.isEmpty())) ? MoneyOrderPrintStatusType.fromValue(value) : null);
            mo.setMoAccountNumber(getAccountNumber(ACCOUNT_NUMBER));
            String sDeviceId = UnitProfile.getInstance().get("DEVICE_ID", "");
            
            /*
             * Profile can get screwed up and have agentID + Pos number as the
             * Device ID instead of the MO account number.  RTS does not like 
             * the longer number, so when debug is active for it to something
             * that RTS will accept
             */
            if ((sDeviceId.length() <= 8) || (!CompileTimeFlags.debug())){
                mo.setDeviceID(sDeviceId);
            } else {
                mo.setDeviceID(sDeviceId.substring(0, 8));
            }
            mo.setSerialNumber(getBigInteger(SERIAL_NUMBER));
            mo.setItemAmount(getBigDecimal(AMOUNT));
            mo.setDateTimePrinted(TimeUtility.toXmlCalendar(dateTime)); 
            mo.setItemFee(getBigDecimal(FEE));
            mo.setPeriodNumber(new BigInteger(String.valueOf(period)));
            mo.setDocumentSequenceNbr(getBigInteger(DOCUMENT_NUMBER));
           // mo.setDocumentType(String.valueOf(productType));
            Integer docType = getInt(DOCUMENT_TYPE);
            mo.setDocumentType(docType != null ? BigInteger.valueOf(getInt(DOCUMENT_TYPE)) : null);
            String documentType = get(DOCUMENT_TYPE);
            if ((documentType == null) || (documentType.isEmpty())) {    //doing this for backward compatibility
                mo.setDocumentType(BigInteger.valueOf(productType)); 
            }
            mo.setDispenserID(get(DISPENSER_NUMBER));           //currently blank because not set in MOT40
            mo.setVoidFlag(getFlag(VOID_FLAG));
            mo.setVoidReasonCode(getMoneyOrderVoidReasonCode(get(VOID_REASON_CODE)));
            mo.setTaxID(get(TAX_ID));
            mo.setEmployeeID(BigInteger.valueOf(user));
            mo.setVendorNumber(getBigInteger(PAYEE_ID));
            mo.setRemoteIssuanceFlag(getBoolean(REMOTE_ISSUANCE_FLAG));
            mo.setSaleIssuanceTag(getBigInteger(SALE_ISSUANCE_TAG));
            mo.setDiscountPercentage(getBigDecimal(DISCOUNT_PERCENTAGE));
            mo.setDiscountAmount(getBigDecimal(DISCOUNT_AMOUNT));
            mo.setAccountingStartDay(getBigInteger(ACCOUNTING_START_DAY));
                  
            // mo.setPeriodNumber(get(ABSOLUTE_PERIOD));   // not needed --gea
            //mo.setUnitNumber(getUnitNumber(UNIT_NUMBER));
                // Pat's fix includes commenting out these lines
            moVector.add(currentIndex, mo);
        }
        ctri.getMoneyOrder().clear();
        ctri.getMoneyOrder().addAll(moVector);
        
        //Debug.println("ctriDetail() result = " + ctri.toString()); 
        //Debug.println("ctriDetail() done " + this.toString());  
        return ctri;
    }
    
	/**
     * Sets one of the optional fields from a byte array. 
     * Stores a reference to the passed-in array.
     */
    public void set(int fieldNumber, byte value[]) {
//        Debug.println("fieldNumber[" + fieldNumber+"], currentIndex["+currentIndex+"]"); 
        field[currentIndex][fieldNumber] = value;
    }
    
    private ComplianceTransactionRequestType getComplianceTransactionRequestType(int fieldNumber) {
    	try {
    		byte[] bytes = field[currentIndex][fieldNumber];
    		String value = new String(bytes);
    		return ComplianceTransactionRequestType.fromValue(value);
    	} catch (Exception e) {
    		return null;
    	}
    }
    
    private CustomerComplianceTypeCodeType getCustomerComplianceTypeCode(int fieldNumber) {
    	try {
    		byte[] bytes = field[currentIndex][fieldNumber];
    		String value = new String(bytes);
    		return CustomerComplianceTypeCodeType.fromValue(value);
    	} catch (Exception e) {
    		return null;
    	}
	}

    private void set(int fieldNumber, CustomerComplianceTypeCodeType value) {
    	try {
    		field[currentIndex][fieldNumber] = value.toString().getBytes();
    	} catch (Exception e) {
    		field[currentIndex][fieldNumber] = null;
    	}
    }

    private void set(int fieldNumber, ComplianceTransactionRequestType value) {
    	try {
    		field[currentIndex][fieldNumber] = value.toString().getBytes();
    	} catch (Exception e) {
    		field[currentIndex][fieldNumber] = null;
    	}
    }

    /** 
     * Sets one of the optional fields from a string. Converts the
     * string into a byte array (ASCII encoded) for storage. If the
     * string is null or empty, a null is stored in the field, which
     * means it will be omitted on output. The corresponding get method
     * will imply an empty string when an omitted field is read.
     */
    public void set(int fieldNumber, String value) {
        if ((value == null) || (value.length() == 0)) {
            field[currentIndex][fieldNumber] = null;
        }
        else {
            try {
                set(fieldNumber, value.getBytes("UTF-8")); 
            }
            catch (UnsupportedEncodingException e) {       // should never happen
                set(fieldNumber, value.getBytes());
            }
        }
    }
    
    /**
     * Sets one of the optional fields from an int. Converts the int
     * into a string for storage. If the value is zero, a null is 
     * stored in the field, which means it will be omitted on output. 
     * The corresponding get method will imply a zero value when an 
     * omitted field is read.
     */
    public void set(int fieldNumber, Integer value) {
        if (value == null) {
            field[currentIndex][fieldNumber] = null;
        } else {
        	try {
        		set(fieldNumber, Integer.toString(value));
        	} catch (Exception e) {
                field[currentIndex][fieldNumber] = null;
        	}
        }
    }

    /**
     * Sets one of the optional fields from a long. Converts
     * the long into a string for storage. The string format
     * is a surprisingly compact representation since most values
     * are much smaller than the maximum range of a long. If the
     * value is zero, a null is stored in the field, which means 
     * it will be omitted on output. The corresponding get method
     * will imply a zero value when an omitted field is read.
     */
    public void set(int fieldNumber, Long value) {
        if (value == null) {
            field[currentIndex][fieldNumber] = null;
        } else {
        	try {
        		set(fieldNumber, Long.toString(value));
        	} catch (Exception e) {
                field[currentIndex][fieldNumber] = null;
        	}
        }
    }
    
    /**
     * Sets one of the optional fields from a BigDecimal. Converts
     * the BigDecimal into a string for storage. The string format
     * is a surprisingly compact representation since most amounts
     * are much smaller than the maximum range of a BigDecimal.
     * If the value is numerically zero, a null is stored in the
     * field, which means it will be omitted on output. The 
     * corresponding get method will imply a zero value when an 
     * omitted field is read.
     */
    public void set(int fieldNumber, BigDecimal value) {
    	if (value != null) {
	        if (value.signum() == 0) {
	            field[currentIndex][fieldNumber] = null;
	        } else {
	            set(fieldNumber, value.toString());
	        }
    	}
    }
    
    public void set(int fieldNumber, BigInteger value) {
    	if (value != null) {
	        if (value.signum() == 0) {
	            field[currentIndex][fieldNumber] = null;
	        } else {
	            set(fieldNumber, value.toString());
	        }
    	}
    }
    
    public void set(int fieldNumber, XMLGregorianCalendar value) {
    	try {
            set(fieldNumber, TimeUtility.toStringDate(value));
    	} catch (Exception e) {
            field[currentIndex][fieldNumber] = null;
    	}
    }
    
    /** Sets a boolean flag. */
    public void setFlag(int mask, Boolean value) {
        setFlags(getFlags() & (~mask));
        if (value != null && value) {
            setFlags(getFlags() | mask);
        }
    }

    private static final String docType[] = {"VOID", "MO", "VP", "GC", "DG", "DOC", "DOC"};       
    private static final String transactionType[] = {
        "", "", "", "", "MGR", "", "", "", "", "",          //$NON-NLS-10$
        "EXP", "UTP", "PPS", "", "", "", "", "", "", "",          //$NON-NLS-10$
        "", "", "", "", "", "", "", "", "", "",          //$NON-NLS-10$
        "", "", "", "", "", "", "", "", "", "",          //$NON-NLS-10$
        "", "CARD", "", "", "", "", "", "", "", "",          //$NON-NLS-10$
        "", "", "", "", "", "", "", "", "", "",          //$NON-NLS-10$
        "", "", "", "", "", "", "", "", "", "",          //$NON-NLS-10$
        "", "", "", "", "", "", "", "", "", "",          //$NON-NLS-10$
        "", "", "", "", "", "", "", "", "", "",          //$NON-NLS-10$
        "MGS", "BP", "MTR", "", "", "", "MCP", "MCR", "", ""};          //$NON-NLS-10$
    
    /** 
     * Returns a string representation. 
     * The generated string is similar to Tedcoll2 output.
     */
    @Override
	public String toString() {  
        StringBuffer sb = new StringBuffer();
        int saveIndex = currentIndex;
        try {
            currentIndex = 0;
            DateFormat df = new SimpleDateFormat(TimeUtility.REPORT_DATE_TIME_FORMAT);
            switch (recordType) {
                case CUSTOMER_TRANSACTION_REQUEST_INFO_RECORD:
                    sb.append("CTRI,");
                    String requestType = get(REQUEST_TYPE);
                    sb.append(requestType != null ? ComplianceTransactionRequestType.valueOf(requestType) : get(REQUEST_TYPE));
                    sb.append(","); 
                    sb.append(get(NUMBER_OF_MONEY_ORDERS));
                    //append money order data
                    Integer numberOfMoneyOrders = getInt(NUMBER_OF_MONEY_ORDERS); //getInt can return null so must use Integer
                    if (numberOfMoneyOrders == null) numberOfMoneyOrders = 0;  
                    for (currentIndex = 0; currentIndex < numberOfMoneyOrders; ++currentIndex) {
                        sb.append("\n\t  ");
                        sb.append(docType[productType]);
                        sb.append(get(DOCUMENT_NUMBER)).append(",");
                        sb.append(get(DOCUMENT_TYPE)).append(",");
                        sb.append(df.format(new Date(dateTime))).append(",");
                        sb.append(get(SERIAL_NUMBER)).append(",").append(get(AMOUNT)).append(",");
                        sb.append(get(FEE)).append(",").append(period).append(",");
                        sb.append(get(ABSOLUTE_PERIOD)).append(",");
                        sb.append(user).append(",").append(get(PAYEE_ID)).append(",");
                        sb.append((getFlag(LIMBO_FLAG)     ? "LIMBO," : ","));
                        sb.append((getFlag(VOID_FLAG)      ? "VOID," : ","));
                        sb.append((getFlag(TRAINING_FLAG)  ? "TRAINING," : ","));
                        sb.append(get(DISCOUNT_CARD_NUMBER)).append(",");
                        sb.append(get(DISCOUNT_AMOUNT)).append(",");
                        sb.append(get(DISCOUNT_PERCENTAGE)).append(",");
                        String printStatus = get(PRINT_STATUS);
                        MoneyOrderPrintStatusType.fromValue(printStatus);
                        sb.append(printStatus != null ? MoneyOrderPrintStatusType.fromValue(printStatus) : get(PRINT_STATUS));
                        sb.append(",");
                        sb.append(get(TAX_ID));
                    }
                    return sb.toString();
                case DOCUMENT_RECORD:
                    sb.append(docType[productType]);
                    sb.append(get(DOCUMENT_NUMBER)).append(",");
                    sb.append(df.format(new Date(dateTime))).append(",");
                    sb.append(get(SERIAL_NUMBER)).append(",").append(get(AMOUNT)).append(",");
                    sb.append(get(FEE)).append(",").append(period).append(",");
                    sb.append(get(ABSOLUTE_PERIOD)).append(",");
                    sb.append(user).append(",").append(get(PAYEE_ID)).append(",");
                    sb.append((getFlag(LIMBO_FLAG)     ? "LIMBO," : ","));
                    sb.append((getFlag(VOID_FLAG)      ? "VOID," : ","));
                    sb.append((getFlag(TRAINING_FLAG)  ? "TRAINING," : ","));
                    sb.append(get(DISCOUNT_CARD_NUMBER)).append(",");
                    sb.append(get(DISCOUNT_AMOUNT)).append(",");
                    sb.append(get(DISCOUNT_PERCENTAGE)).append(",");
                    sb.append(get(CURRENCY_CODE));  
                    return sb.toString();
                case MONEYGRAM_RECORD:
                    sb.append(transactionType[productType]).append(",");
                    sb.append(df.format(new Date(dateTime))).append(",");
                    sb.append(get(REFERENCE_NUMBER)).append(",").append(get(AMOUNT)).append(",");
                    sb.append(get(FEE)).append(",").append(get(EXCHANGE_RATE)).append(",");
                    sb.append(period).append(",").append(get(ABSOLUTE_PERIOD)).append(",");
                    sb.append(get(SENDER)).append(",").append(user).append(",").append(get(ATTEMPTS)).append(","); 
                    sb.append(get(SERIAL_NUMBER)).append(","); 
                    sb.append(get(PROCESSING_FEE)).append(",");
                    sb.append(get(INFO_FEE_IND)).append(",");
                    sb.append(get(CURRENCY_CODE)).append(",");   
                    sb.append((getFlag(SUCCESS_FLAG) ? "" : "FAILED"));                
                    return sb.toString();
                case MONEYGRAM_CARD_RELOAD_RECORD:
                    sb.append(transactionType[productType]).append(",");
                    sb.append(df.format(new Date(dateTime))).append(",");
                    sb.append(get(CARD_NUMBER)).append(",");
                    sb.append(get(CARD_TYPE)).append(",");
                    sb.append(get(EMPLOYEE_NUMBER)).append(",");
                    sb.append(get(AMOUNT)).append(",");
                    sb.append(get(FEE)).append(",");
                    sb.append(period).append(",").append(get(ABSOLUTE_PERIOD)).append(",");
                    sb.append(get(LAST_NAME)).append(",");
                    sb.append(get(ATTEMPTS)).append(",");
                    sb.append(get(USER)).append(",");
                    sb.append((getFlag(SUCCESS_FLAG) ? "" : "FAILED"));
                    return sb.toString();
                case MONEYGRAM_CARD_PURCH_RECORD:
                    sb.append(transactionType[productType]).append(",");
                    sb.append(df.format(new Date(dateTime))).append(",");
                    sb.append(get(CARD_NUMBER)).append(",");
                    sb.append(get(CARD_TYPE)).append(",");
                    sb.append(get(EMPLOYEE_NUMBER)).append(",");
                    sb.append(get(AMOUNT)).append(",");
                    sb.append(get(FEE)).append(",");
                    sb.append(period).append(",").append(get(ABSOLUTE_PERIOD)).append(",");
                    sb.append(get(LAST_NAME)).append(",");
                    sb.append(get(ATTEMPTS)).append(",");
                    sb.append(get(USER)).append(",");
                    sb.append((getFlag(SUCCESS_FLAG) ? "" : "FAILED"));
                    return sb.toString();
                case DISPENSER_LOAD_RECORD:
                    sb.append("DOCLOAD,").append(df.format(new Date(dateTime))).append(",");
                    sb.append(get(SERIAL_NUMBER_START)).append(",");
                    sb.append(get(SERIAL_NUMBER_END)).append(",");
                    sb.append(get(DISPENSER_NUMBER)).append(",");
                    sb.append(period).append(",").append(get(ABSOLUTE_PERIOD)).append(",");
                    sb.append(user).append(",");
                    sb.append((getFlag(CARD_LOAD_FLAG) ? "CARD" : ""));
                    return sb.toString();
                case INVALID_LOGIN_RECORD:
                    sb.append("BADLOGIN,").append(df.format(new Date(dateTime))).append(",");
                    sb.append(period).append(",").append(get(ABSOLUTE_PERIOD)).append(",").append(user);
                    return sb.toString();
                case PERIOD_CHANGE_RECORD:
                    sb.append("PERIOD,").append(df.format(new Date(dateTime))).append(","); 
                    sb.append(period).append(",").append(get(ABSOLUTE_PERIOD));
                    return sb.toString();
            }
        }
        finally {
            currentIndex = saveIndex;
        }
        return "";	
    }

    /**
     * Write a field length to a data output stream. If the field
     * is less than 128 bytes long (as most fields are) then one
     * byte is written to hold the length. Otherwise, a byte with
     * value of -1 is written to indicate that the field is longer 
     * than usual, and two bytes are written to hold the field length 
     * as a short.
     */
    private void writeLength(DataOutputStream out, int length) 
            throws IOException {
        if (length < 128)
            out.writeByte(length);
        else {
            out.writeByte(-1);
            out.writeShort(length);
        }
    }
    
    /** Writes this record to the specified output stream. */
    public void writeTo(OutputStream out) throws IOException {
        this.writeTo(new DataOutputStream(out));
    }

    /** Writes this record to the specified data output stream. */
    public void writeTo(DataOutputStream out) throws IOException {
        int recordLength = 0;
        int i;

        for (int j = 0; j < majorLength; ++j) {
            currentIndex = j;
            fillInHeader();
            fillInFlags();
            for (i = 0; i < NUMBER_OF_FIELD_TYPES; i++) {
                if (field[j][i] != null) {
                    recordLength += 2;
                    recordLength += field[j][i].length;
                    recordLength += (field[j][i].length < 128) ? 0 : 2;
                }
            }
        }
        // writing out the record type and the overall record length.
        out.writeByte(recordType);
        writeLength(out, recordLength);

        for (int j = 0; j < majorLength; ++j) {
            currentIndex = j;
            fillInHeader();
            fillInFlags();
            
            for (i = 0; i < NUMBER_OF_FIELD_TYPES; i++) {
                if (field[j][i] != null) {
                    //Debug.println("write [" + j + "," + i + "] " + ByteArray.hex(field[j][i]));
                    out.writeByte(i);
                    writeLength(out, field[j][i].length);
                    out.write(field[j][i]);
                }
            }
        }
    }
    
    public int getFlags() {
    	return flagArray[currentIndex];
    }
    
    public void setFlags(int i) {
    	flagArray[currentIndex] = i;
    }

}
