package dw.io.report;

public interface ReportLogConstants {

    // change these numbers as needed
    public static final int NUMBER_OF_RECORD_TYPES = 11;
    //Number of Field type should be 1 greater than the last fieldtype value.
    //public static final int NUMBER_OF_FIELD_TYPES = 76;
    //public static final int NUMBER_OF_FIELD_TYPES = 77;
    //public static final int NUMBER_OF_FIELD_TYPES = 79;
    //public static final int NUMBER_OF_FIELD_TYPES = 80;
    public static final int NUMBER_OF_FIELD_TYPES = 82;
    
    // In order to maintain backward compatibility, do not delete or
    // renumber any of the following constants once they've been used
    // by a production version of Deltaworks. You can add new ones
    // to support new record types, product types, fields, or flags.
    // You can comment out fields that are no longer used and don't
    // need to be saved in the report log anymore. But you can not
    // reuse values that used to mean something else.

    // record types 
    public static final int DOCUMENT_RECORD = 0;
    public static final int MONEYGRAM_RECORD = 1;
    public static final int DISPENSER_LOAD_RECORD = 2;
    public static final int INVALID_LOGIN_RECORD = 3;
    public static final int PERIOD_CHANGE_RECORD = 4;
    public static final int MONEYGRAM_REVERSAL_RECORD = 5;
    public static final int CUSTOMER_COMPLIANCE_INFO_RECORD = 6;
    public static final int MONEY_ORDER_INFO_40_RECORD = 7;
    public static final int CUSTOMER_TRANSACTION_REQUEST_INFO_RECORD = 8;
    public static final int MONEYGRAM_CARD_RELOAD_RECORD = 9;
    public static final int MONEYGRAM_CARD_PURCH_RECORD = 10;
    // Remember to increment NUMBER_OF_RECORD_TYPES when you add new ones.

    // document types (arbitrary, chosen to match existing codes)
    public static final int TEST_VOID_TYPE = 0;
    public static final int MONEY_ORDER_TYPE = 1;
    public static final int VENDOR_PAYMENT_TYPE = 2;
    public static final int GIFT_CERTIFICATE_TYPE = 3;
    public static final int DELTAGRAM_TYPE = 4;

    // MoneyGram transaction types (also arbitrary, chosen to match 
    // existing codes)
    public static final int MONEYGRAM_SEND_TYPE = 90;
    public static final int MONEYGRAM_RECEIVE_TYPE = 4;
    public static final int MONEYGRAM_RECEIVE_TYPE_PPC = 41;
    public static final int BILLPAY_TYPE = 91;
    public static final int MONEYGRAM_REVERSAL_TYPE = 92;
    public static final int MONEYGRAM_CARD_RELOAD_TYPE = 97;
    public static final int MONEYGRAM_CARD_PURCHASE_TYPE = 96;
    public static final int EXPRESSPAY_TYPE = 10;
    public static final int UTILITYPAY_TYPE = 11;
    public static final int PREPAID_TYPE = 12;

    // common fields
    public static final int HEADER = 0;
    public static final int ABSOLUTE_PERIOD = 1;
    public static final int AMOUNT = 2;
    public static final int FEE = 3;
    public static final int FLAGS = 4;
    // money order fields
    public static final int DOCUMENT_NUMBER = 5;
    public static final int SERIAL_NUMBER = 6;
    public static final int PAYEE_ID = 7;
    // moneygram fields
    public static final int PAYEE = 8;
    public static final int REFERENCE_NUMBER = 9;
    public static final int EXCHANGE_RATE = 10;
    public static final int ATTEMPTS = 11;
    // dispenser load fields
    public static final int SERIAL_NUMBER_START = 12;
    public static final int SERIAL_NUMBER_END = 13;
    public static final int DISPENSER_NUMBER = 14;
    // associate/employee discounted money orders
    public static final int DISCOUNT_CARD_NUMBER = 15;
    public static final int DISCOUNT_AMOUNT = 16;
    public static final int DISCOUNT_PERCENTAGE = 17;

    public static final int SENDER = 18;
    public static final int TIME = 19;
    public static final int USER = 20;
    public static final int PRINT_STATUS = 21;

    // customer compliance information
    public static final int TYPE_CODE = 22;
    public static final int KEY = 23;
    public static final int USER_NAME = 24;
    public static final int LOCAL_DATE_TIME = 25;
    public static final int EMPLOYEE_NUMBER = 26;
    public static final int COMMENTS = 27;
    public static final int CATEGORY = 28;
    public static final int OTHER_CATEGORY = 29;
    public static final int CHARACTER = 30;
    public static final int LAST_NAME = 31;
    public static final int FIRST_NAME = 32;
    public static final int MIDDLE_INITIAL = 33;
    public static final int ADDRESS = 34;
    public static final int ADDRESS2 = 35;
    public static final int CITY = 36;
    public static final int STATE = 37;
    public static final int ZIP = 38;
    public static final int COUNTRY = 39;
    public static final int PHOTO_ID_TYPE = 40;
    public static final int PHOTO_ID_NUMBER = 41;
    public static final int PHOTO_ID_STATE = 42;
    public static final int PHOTO_ID_COUNTRY = 43;
    public static final int DATE_OF_BIRTH = 44; // format mm/dd/yy ?
    public static final int PHONE_NUMBER = 45;
    public static final int OCCUPATION = 46;
    public static final int LEGAL_ID_TYPE = 47;
    public static final int LEGAL_ID_NUMBER = 48;

    public static final int THIRD_PARTY_DBA = 49;
    public static final int THIRD_PARTY_LAST_NAME = 50;
    public static final int THIRD_PARTY_FIRST_NAME = 51;
    public static final int THIRD_PARTY_MIDDLE_INITIAL = 52;
    public static final int THIRD_PARTY_ADDRESS = 53;
    public static final int THIRD_PARTY_ADDRESS2 = 54;
    public static final int THIRD_PARTY_CITY = 55;
    public static final int THIRD_PARTY_STATE = 56;
    public static final int THIRD_PARTY_ZIP = 57;
    public static final int THIRD_PARTY_COUNTRY = 58;
    public static final int THIRD_PARTY_DATE_OF_BIRTH = 59;
    public static final int THIRD_PARTY_PHONE_NUMBER = 60;
    public static final int THIRD_PARTY_OCCUPATION = 61;
    public static final int THIRD_PARTY_LEGAL_ID_TYPE = 62;
    public static final int THIRD_PARTY_LEGAL_ID_NUMBER = 63;

    public static final int REQUEST_TYPE = 64;
    public static final int MG_REFERENCE_NUMBER = 65;
    public static final int INDEX = 66;
    public static final int NUMBER_OF_MONEY_ORDERS = 67;
    public static final int VOID_REASON_CODE = 68;
    public static final int TAX_ID = 69;
    public static final int SALE_ISSUANCE_TAG = 70;
    public static final int ACCOUNT_NUMBER = 72;
    public static final int UNIT_NUMBER = 73;
    public static final int ACCOUNTING_START_DAY = 74;
    public static final int REMOTE_ISSUANCE_FLAG = 75;
    public static final int CARD_NUMBER = 76;
    public static final int CARD_TYPE = 77;
    public static final int CURRENCY_CODE = 78;
    public static final int PROCESSING_FEE = 79;
    public static final int INFO_FEE_IND = 80;
    public static final int TAX_EXT = 81;
    //
    // Remember to increment NUMBER_OF_FIELD_TYPES when you add new fields.

    // money order flag masks
    public static final int LIMBO_FLAG = 1;
    public static final int VOID_FLAG = 2;
    //public static final int GROUP_FLAG = 4;  // not used anywhere
    public static final int TRAINING_FLAG = 8;
    // moneygram flag mask
    public static final int SUCCESS_FLAG = 16;
    // dispenser load flag mask
    public static final int CARD_LOAD_FLAG = 32;
    // compatibility flag mask
    public static final int SUPPRESS_FLAG = 64;
    
    public static final int DOCUMENT_TYPE = 76;

}
