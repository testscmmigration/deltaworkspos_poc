/*
 * Created on Jul 6, 2006
 *
 */
package dw.io.report;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.moneygram.agentconnect.BillPaymentDetailInfo;

import dw.utility.TimeUtility;

/**
 * @author T025
 *
 */
public class MoneyGramBillPayReverseDetail extends ReportDetail implements Serializable {
	private static final long serialVersionUID = 1L;
    
    private String  referenceNumber;
    private long  time;
    private String  sender;
    private String  user;
    private String  empNumber;
    private BigDecimal  total;
    private boolean successFlag = true;
    private String agentID;

    /**default constructor used for conversion program */
    public MoneyGramBillPayReverseDetail() {
        super();
    }

    /** MoneyGram Reversal constructor, host */
    public MoneyGramBillPayReverseDetail(String activityDate, final BillPaymentDetailInfo bpdri) {
        super();
        referenceNumber = bpdri.getReferenceNumber();
        total = bpdri.getTotalAmount();
        sender = "";	
        final String dateTimeString = activityDate + " " + TimeUtility.getTime(bpdri.getDate());;	 
        Date dateTimeObject;
        try {
            dateTimeObject = new SimpleDateFormat(TimeUtility.DATE_TIME_N0_DELIMITER_FORMAT).parse(dateTimeString);
        }
        catch (ParseException pe) {
            dateTimeObject = TimeUtility.currentTime();
        }
        time = dateTimeObject.getTime();
        setDateTime(time);
        agentID = bpdri.getAgentNumber();
    }

    /* toString */
    @Override
	public String toString() {
        return 
            super.toString() + 
            "\n  MoneyGramBillPayReverseDetail {" +	
            "\n    referenceNumber           : " + referenceNumber +	
            "\n    time                      : " + String.valueOf(time) +	
            "\n    sender                    : " + String.valueOf(sender) +	
            "\n    user                      : " + String.valueOf(user) +	
            "\n    total                     : " + String.valueOf(total) +	
            "\n    successFlag               : " + String.valueOf(successFlag) +	
            "\n  } MoneyGramBillPayReverseDetail ";	
    }

    /* getters */
    public String  getReferenceNumber() { return referenceNumber; }
    public long  getTime()            { return time;}
    public String  getSender()          { return sender;}
    public String  getUser()            { return user;}
    public String  getEmployeeNumber()  { return empNumber;}
    public BigDecimal  getTotal()       { return total;}
    public boolean getSuccessFlag()     { return successFlag;}
    public String getAgentID()          { return agentID;}

    /* setters*/
    public void setReferenceNumber(final String rn) {referenceNumber = rn; }
    public void setTime(final long ht) {time = ht; }
    public void setSender(final String s) { sender = s; }
    public void setUser(final String u) { user = u; }
    public void setEmployeeNumber(final String u) { empNumber = u; }
    public void setTotal(final BigDecimal tot) { total = tot;}
    public void setSuccessFlag(final boolean sf) {successFlag = sf; 

}
}
