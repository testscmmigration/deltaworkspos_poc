package dw.io.report;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.moneygram.agentconnect.BillPaymentDetailInfo;
import com.moneygram.agentconnect.MoneyGramSendDetailInfo;
import com.moneygram.agentconnect.MoneyGramSendWithTaxDetailInfo;

import dw.utility.TimeUtility;

/**
 * Encapsulates a MoneyGram Reversal type of detail record in the report log.
 * @version 07-23-2003  1.0
 * @author Patrick Ottman
 */
public class MoneyGramReverseDetail extends ReportDetail implements Serializable {
	private static final long serialVersionUID = 1L;
    
    private String  referenceNumber;
    private long  time;
    private String  sender;
    private String  user;
    private String  empNumber;
    private BigDecimal  total;
    private boolean successFlag = true;
    private String agentID;
    private String  currencyCode;
    private String agentName;

    /**default constructor used for conversion program */
    public MoneyGramReverseDetail() {
        super();
    }

    /** MoneyGram Reversal constructor, host */
    public MoneyGramReverseDetail(String activityDate, final MoneyGramSendDetailInfo sdri) {
        super();
        referenceNumber = sdri.getReferenceNumber();
        total = sdri.getTotalAmount();
        sender = "";	
        final String dateTimeString = activityDate + " " + sdri.getTime();	
        Date dateTimeObject;
        try {
            dateTimeObject = new SimpleDateFormat(TimeUtility.DATE_TIME_N0_DELIMITER_FORMAT).parse(dateTimeString);
        }
        catch (ParseException pe) {
            dateTimeObject = TimeUtility.currentTime();
        }
        time = dateTimeObject.getTime();
        setDateTime(time);
        agentID = sdri.getAgentNumber();
        agentName = sdri.getAgentName();
        currencyCode = sdri.getSendCurrency();
        if(agentName.lastIndexOf("(") >0){
    		String agentNameMinusCurrency = agentName.substring(0,agentName.lastIndexOf("("));
    		agentName = agentNameMinusCurrency;
    	}
      
    }

    /** MoneyGram Reversal constructor given the GST detail report info, host */
	public MoneyGramReverseDetail(String activityDate, final MoneyGramSendWithTaxDetailInfo sdri) {
		super();
		referenceNumber = sdri.getReferenceNumber();
		total = sdri.getTotalAmount();
		sender = ""; 
		final String dateTimeString = activityDate; 
		Date dateTimeObject;
		try {
			dateTimeObject = new SimpleDateFormat(TimeUtility.DATE_TIME_N0_DELIMITER_FORMAT).parse(dateTimeString);
		} catch (ParseException pe) {
			dateTimeObject = TimeUtility.currentTime();
		}
		time = dateTimeObject.getTime();
		setDateTime(time);
		agentName = sdri.getAgentName();
		currencyCode = sdri.getSendCurrency();
		if (agentName.lastIndexOf("(") > 0) {
			String agentNameMinusCurrency = agentName.substring(0,
					agentName.lastIndexOf("("));
			agentName = agentNameMinusCurrency;
		}
	}

    public MoneyGramReverseDetail(String activityDate, final BillPaymentDetailInfo sdri) {
        super();
        referenceNumber = sdri.getReferenceNumber();
        total = sdri.getTotalAmount();
        sender = "";	
        final String dateTimeString = activityDate + " " + TimeUtility.getTime(sdri.getDate());	  
        Date dateTimeObject;
        try {
            dateTimeObject = new SimpleDateFormat(TimeUtility.DATE_TIME_N0_DELIMITER_FORMAT).parse(dateTimeString);
        }
        catch (ParseException pe) {
            dateTimeObject = TimeUtility.currentTime();
        }
        time = dateTimeObject.getTime();
        setDateTime(time);
        agentID = sdri.getAgentNumber();
       
        
    }

    /* toString */
    @Override
	public String toString() {
        return 
            super.toString() + 
            "\n  MoneyGramReverseDetail {" +	
            "\n    referenceNumber           : " + referenceNumber +	
            "\n    time                      : " + String.valueOf(time) +	
            "\n    sender                    : " + String.valueOf(sender) +	
            "\n    user                      : " + String.valueOf(user) +	
            "\n    total                     : " + String.valueOf(total) +	
            "\n    successFlag               : " + String.valueOf(successFlag) +	
            "\n  } MoneyGramReverseDetail ";	
    }

    /* getters */
    public String  getReferenceNumber() { return referenceNumber; }
    public long  getTime()            { return time;}
    public String  getSender()          { return sender;}
    public String  getUser()            { return user;}
    public String  getEmployeeNumber()  { return empNumber;}
    public BigDecimal  getTotal()       { return total;}
    public boolean getSuccessFlag()     { return successFlag;}
    public String getAgentID()          { return agentID;}
    public String  getCurrencyCode()    { return currencyCode; } 
    public String  getAgentName()         { return agentName; }

    /* setters*/
    public void setReferenceNumber(final String rn) {referenceNumber = rn; }
    public void setTime(final long ht) {time = ht; }
    public void setSender(final String s) { sender = s; }
    public void setUser(final String u) { user = u; }
    public void setEmployeeNumber(final String u) { empNumber = u; }
    public void setTotal(final BigDecimal tot) { total = tot;}
    public void setSuccessFlag(final boolean sf) {successFlag = sf; }
    public void setCurrencyCode(final String code) { currencyCode = code; }
}

