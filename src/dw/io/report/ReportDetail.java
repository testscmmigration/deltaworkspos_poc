package dw.io.report;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import dw.io.diag.DiagMessage;
import dw.utility.Period;
import dw.utility.PeriodWindow;
import dw.utility.TimeUtility;

/**
  * Represents a record in the detailed report log.  This is the base class.
  * Each record type has its own class.
  * @version 06-23-2000  1.0
  * @author John Allen
  */
public class ReportDetail implements Serializable {
	private static final long serialVersionUID = 1L;

    //-------------------------------------------------------------------------
    // Class variables
    //-------------------------------------------------------------------------

	public static final String DEFAULT_BASE_NAME = "dreport"; 
    public static final String DEFAULT_EXTENSION = ".dat"; 
    public static final String DEFAULT_FILE_NAME = DEFAULT_BASE_NAME + DEFAULT_EXTENSION;
    public static final String RECENT_DEFAULT_FILE_NAME = "dreportRecent.dat"; 
    public static final byte REF_NUM_LENGTH = 16;

    //-------------------------------------------------------------------------
    // instance variables
    //-------------------------------------------------------------------------

    private   long        absolutePeriod;
    protected long        dateTime;
    protected int         type;
    protected short       dnetCode;
    protected BigDecimal  amount;
    protected BigDecimal  fee;
    protected BigDecimal  tax;
    protected BigDecimal  processingFee;
    protected String      infoFeeIndicator;
    protected byte        period;
    protected byte        userId;

    //-------------------------------------------------------------------------
    // constructors
    //-------------------------------------------------------------------------
    
    /**
     * Created for the ImportDetailRecords
     */
    public ReportDetail(final boolean doNothing){
    }

    /**
     * Create an object with the current date, time, and period.
     */
    public ReportDetail() {
        dateTime = TimeUtility.currentTimeMillis();
        period = (byte)Period.getPeriod();
        setAbsolutePeriod(Period.getAbsolutePeriod(dateTime,period));
    }

    /**
     * Create an object with the specified absolute period and date.
     * Used for setting endpoints for subsets of detail records.
     */
    public ReportDetail(final long ap, final long dt) {
        setAbsolutePeriod(ap);
        setDateTime(dt);
    }

    /**
     * Create an object with the specified absolute period and a date of zero.
     * Used for setting endpoints for subsets of detail records.
     */
    public ReportDetail(final long ap) {
        this(ap,0);
    }

    //-------------------------------------------------------------------------
    // methods
    //-------------------------------------------------------------------------

    /* toString */
    @Override
	public String toString() {
        String typeString;
             if (type == ReportLog.VOID_TYPE) typeString = "  VOID_TYPE"; 
        else if (type == ReportLog.MONEY_ORDER_TYPE) typeString = "  MONEY_ORDER_TYPE"; 
        else if (type == ReportLog.VENDOR_PAYMENT_TYPE) typeString = "  VENDOR_PAYMENT_TYPE"; 
        else if (type == ReportLog.MONEYGRAM_SEND_TYPE) typeString = "  MONEYGRAM_SEND_TYPE"; 
        else if (type == ReportLog.MONEYGRAM_CARD_RELOAD_TYPE) typeString = "  MONEYGRAM_CARD_RELOAD_TYPE"; 
        else if (type == ReportLog.MONEYGRAM_CARD_PURCH_TYPE) typeString = "  MONEYGRAM_CARD_PURCHASE_TYPE"; 
        else if (type == ReportLog.MONEYGRAM_RECEIVE_TYPE) typeString = "  MONEYGRAM_RECEIVE_TYPE"; 
        else if (type == ReportLog.MONEYGRAM_RECEIVE_TYPE_PPC) typeString = "  MONEYGRAM_RECEIVE_TYPE_PPC"; 
        else if (type == ReportLog.MONEYGRAM_REVERSAL_TYPE) typeString = "  MONEYGRAM_REVERSAL_TYPE"; 
        else if (type == ReportLog.BILL_PAYMENT_TYPE) typeString = "  BILL_PAYMENT_TYPE"; 
        else if (type == ReportLog.DELTAGRAM_TYPE) typeString = "  DELTAGRAM_TYPE"; 
        else if (type == ReportLog.DISPENSER_LOAD_TYPE) typeString = "  DISPENSER_LOAD_TYPE"; 
        else if (type == ReportLog.INVALID_LOGIN_TYPE) typeString = "  INVALID_LOGIN_TYPE"; 
        else if (type == ReportLog.EXPRESS_PAYMENT_TYPE) typeString = "  EXPRESS_PAYMENT_TYPE"; 
        else if (type == ReportLog.UTILITY_PAYMENT_TYPE) typeString = "  UTILITY_PAYMENT_TYPE"; 
        else if (type == ReportLog.PREPAID_SERVICES_TYPE) typeString = "  PREPAID_SERVICES_TYPE"; 
        else typeString = "  NO SUCH TYPE"; 
        return 
            "\nReportDetail {" +	
            "\n  type                      : " + String.valueOf(type) + typeString +	
            "\n  absolutePeriod            : " + String.valueOf(absolutePeriod) +	
            "\n  dateTime                  : " + String.valueOf(dateTime) + " = " + new Date(dateTime) + 	 
            "\n  dnetCode                  : " + String.valueOf(dnetCode) +	
            "\n  amount                    : " + String.valueOf(amount) +	
            "\n  fee                       : " + String.valueOf(fee) +	
            "\n  tax                       : " + String.valueOf(tax) +	
            "\n  processing fee            : " + String.valueOf(processingFee) +
            "\n  info fee indicator        : " + String.valueOf(infoFeeIndicator) +
			"\n  period                    : " + String.valueOf(period) +	
            "\n  userId                    : " + String.valueOf(userId) +	
            "\n} ReportDetail ";	
    }

    /* getters */
    public long       getAbsolutePeriod()   { return absolutePeriod; }
    public int        getType()             { return type; }
    public long       getDateTime()         { return dateTime; }
    public short      getDnetCode()         { return dnetCode; }
    public BigDecimal getAmount()           { return amount;}
    public BigDecimal getFee()              { return fee;}
    public BigDecimal getTax()              { return tax;}
    public BigDecimal getProcessingFee()    { return processingFee;}
    public String     getInfoFeeIndicator() { return infoFeeIndicator;}
    public byte       getPeriod()           { return period;}
    public byte       getUserId()           { return userId;}

    /* setters */
    public void setAbsolutePeriod(final long ap) { absolutePeriod = ap; }
    public void setDateTime(final long dt) { dateTime = dt; }
    public void setAmount(final BigDecimal a) { amount = a; }
    public void setFee(final BigDecimal f) { fee = f; }
    public void setTax(final BigDecimal t) { tax = t; }
    public void setProcessingFee(final BigDecimal f) { processingFee = f; }
    public void setInfoFeeIndicator(final String f) {infoFeeIndicator = f; }
    public void setPeriod(final byte p) { period = p; }
    public void setUserId(final byte u) { userId = u; }
    public void setType(final int t) { type = t; }
    public void setDnetCode(final short dn) { dnetCode = dn; }


    /* helper methods */
    public static byte stringToByte(String s) {
        try { return Byte.parseByte(s); }
        catch (NumberFormatException e) { return 0; }
    }
    
    public static short stringToShort(String s) {
        try { return Short.parseShort(s); }
        catch (NumberFormatException e) { return 0; }
    }
    
    public static int stringToInt(String s) {
        try { return Integer.parseInt(s); }
        catch (NumberFormatException e) { return 0; }
    }
    
    public static long stringToLong(String s) {
        try { return Long.parseLong(s); }
        catch (NumberFormatException e) { return 0; }
    }


    /**
     * Indicates if this record is in the same period as the
     * indicated date and time
     */
    public boolean isSamePeriod(final long timeStamp) {
        return Period.isSamePeriod(getDateTime(),getPeriod(),timeStamp);
    }

    public boolean isSamePeriod(final PeriodWindow periodWindow) {
        return periodWindow.timeAndPeriodMatch(getDateTime(),getPeriod());
    }
    
    /**
     * Increment the date and time by a millisecond. Used for keeping different
     * detail records from having the same time.
     */
    public void incDateTime() {
        setDateTime(getDateTime() + 1);
    }
    
    /**
     * Indicates if the record is a period change record
     */
    public boolean isPeriodChange() {
        return (getDnetCode() == DiagMessage.DNET_TT_TLOG_DIAG_PERIOD_CH);
    }
}
