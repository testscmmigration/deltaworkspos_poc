package dw.io.report;

import java.math.BigDecimal;
import java.util.Date;

public class ReportSelectionCriteria {
    
    public static final int INT_NOT_SPECIFIED = -1;
    public static final BigDecimal INTBD_NOT_SPECIFIED = new BigDecimal(-1);
    
    private long startPeriod = INT_NOT_SPECIFIED;
    private long endPeriod = INT_NOT_SPECIFIED;
    private long startDateTime = INT_NOT_SPECIFIED;
    private long endDateTime = INT_NOT_SPECIFIED;
    private int employeeNumber = INT_NOT_SPECIFIED; 
    private BigDecimal startAmount = new BigDecimal(INT_NOT_SPECIFIED);
    private BigDecimal endAmount = new BigDecimal(INT_NOT_SPECIFIED);
    private long startSerial = INT_NOT_SPECIFIED;
    private long endSerial = INT_NOT_SPECIFIED;
    private long summaryDateTime = INT_NOT_SPECIFIED; 
    private boolean showDetail = false;
    private boolean accessHost = false;
    
    public static boolean intSpecified(final BigDecimal i) {
      //  return (i != INT_NOT_SPECIFIED);
        return (i.compareTo(new BigDecimal(INT_NOT_SPECIFIED)) != 0);
    }
    
    public static boolean intSpecified(final long i) {
        return (i != INT_NOT_SPECIFIED);
    }
        
    public long getStartPeriod (){
        return startPeriod;
    }
    
    public long getEndPeriod (){
        return endPeriod;
    }
        
    public long getStartDateTime (){
        return startDateTime;
    }
    
    public long getEndDateTime (){
        return endDateTime;
    }
    
    public long getSummaryDateTime (){
        return summaryDateTime;
    }
    
    public int getEmployeeNumber (){
        return employeeNumber;
    }
    
    public BigDecimal getStartAmount (){
        return startAmount;
    }
    
    public BigDecimal getEndAmount (){
        return endAmount;
    }

    public long getStartSerial (){
        return startSerial;
    }
    
    public long getEndSerial (){
        return endSerial;
    }

    public boolean getShowDetail (){
        return showDetail;
    }

    public boolean getAccessHost (){
        return accessHost;
    }
    
    public void setStartPeriod (final long period) {
        startPeriod = period;
    }
    
    public void setEndPeriod (final long period) {
        endPeriod = period;
    }
    
    public void setSummaryDateTime (final long dateTime) {
        summaryDateTime = dateTime;
    }
    
    public void setStartDateTime (final long dateTime) {
        startDateTime = dateTime;
    }
    
    public void setEndDateTime (final long dateTime) {
        endDateTime = dateTime;
    }
    
    public void setEmployeeNumber (final int employee) {
        employeeNumber = employee;
    }
   
    public void setStartAmount (final BigDecimal amount) {
        startAmount = amount;
    }
    
    public void setEndAmount (final BigDecimal amount) {
        endAmount = amount;
    }
    
    public void setStartSerial (final long serialNumber){
        startSerial = serialNumber;
    }
    
    public void setEndSerial (final long serialNumber){
        endSerial = serialNumber;
    }

    public void setShowDetail (final boolean show){
        showDetail =  show;
    }
    
    public void setAbsolutePeriod(final long ap){
       startPeriod = ap;
       endPeriod = ap;
    }
   
    public void setAccessHost (final boolean access){
        accessHost = access;
    }
 
    public boolean filtersTimeRangeOnly() {
        return (!intSpecified(getEmployeeNumber()) &&
                !intSpecified(getStartAmount()) &&
                !intSpecified(getEndAmount()) &&
                !intSpecified(getStartSerial()) &&
                !intSpecified(getEndSerial()));
    }

    private boolean employeeNumberSelected(final int number) {
        final int requiredEmployeeNumber = getEmployeeNumber();
        return (!intSpecified(requiredEmployeeNumber)
                 || (requiredEmployeeNumber == number));
    }
    
    private boolean amountSelected(final BigDecimal amount) {
        BigDecimal requiredStartAmount = getStartAmount();
        BigDecimal requiredEndAmount = getEndAmount();
        if (!intSpecified(requiredStartAmount))
            requiredStartAmount = BigDecimal.ZERO; 
        if (!intSpecified(requiredEndAmount))
            requiredEndAmount = new BigDecimal(Long.MAX_VALUE); 
        if (amount != null)
            return (requiredStartAmount.compareTo(amount) <= 0 )
                    && (requiredEndAmount.compareTo(amount) >= 0);
        else
            return false;
    }
    
    private static final long MAX_SERIAL = 999999999999l;
 
    private boolean serialSelected(final long serial) {
        long requiredStartSerial = getStartSerial();
        long requiredEndSerial = getEndSerial();
        if (!intSpecified(requiredStartSerial))
            requiredStartSerial = 0; 
        if (!intSpecified(requiredEndSerial))
            requiredEndSerial = MAX_SERIAL; 
        return (requiredStartSerial <= serial)
                && (requiredEndSerial >= serial);
    }   

    public boolean reportDetailSelected (final ReportDetail detail){
    	if (detail == null) {
    		return false;
    	}
        final boolean generallySelected = 
                                    employeeNumberSelected(detail.getUserId())
                                    && amountSelected(detail.getAmount());
        if (detail instanceof MoneyOrderDetail){
            final MoneyOrderDetail mod = (MoneyOrderDetail)detail;
            return generallySelected
                   && serialSelected(mod.getSerialNumber());           
        }
        else
            return generallySelected;
    }
    
    /* toString */
    @Override
	public String toString() {
        return 
            "\nReportSelectionCriteria {" +	
            "\n  startPeriod               : " + String.valueOf(startPeriod) +	
            "\n  endPeriod                 : " + String.valueOf(endPeriod) +	
            "\n  startDateTime             : " + String.valueOf(startDateTime) + " " + new Date(startDateTime) +	 
            "\n  endDateTime               : " + String.valueOf(endDateTime) + " " + new Date(endDateTime) +	 
            "\n  employeeNumber            : " + String.valueOf(employeeNumber) +	
            "\n  startAmount               : " + String.valueOf(startAmount) +	
            "\n  endAmount                 : " + String.valueOf(endAmount) +	
            "\n  startSerial               : " + String.valueOf(startSerial) +	
            "\n  endSerial                 : " + String.valueOf(endSerial) +	
            "\n  showDetail                : " + String.valueOf(showDetail) +	
            "\n  accessHost                : " + String.valueOf(accessHost) +	
            "\n} ReportSelectionCriteria ";	
    }
}
