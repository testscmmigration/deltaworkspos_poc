package dw.io.report;

import java.io.Serializable;

import dw.io.diag.DiagMessage;

/**
 * Encapsulates a invalid login type of detail record in the report log.
 * @version 06-23-2000  1.0
 * @author John Allen
 */
public class InvalidLoginDetail extends ReportDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	/* constructor */
    /*default constructor used for conversion program*/
    public InvalidLoginDetail() {
        super(true);
    }
    
    public InvalidLoginDetail(final int employeeId) {
        super();
        type = ReportLog.INVALID_LOGIN_TYPE;
        dnetCode = DiagMessage.DNET_TT_TLOG_DIAG_MUST_XMIT;
        userId = (byte)employeeId;
    }

    /* toString */
    @Override
	public String toString() {
        return 
            super.toString() + 
            "\n  InvalidLoginDetail {" +	
            "\n  } InvalidLoginDetail ";	
    }
}
