package dw.io.report;

import java.io.Serializable;

import dw.io.diag.DiagMessage;
import dw.moneyorder.MoneyOrderInfomation;

/**
 * Encapsulates a money order type of detail record in the report log.
 * This includes:  money orders, vendor payments, and MoneyGram receives.
 * @version 06-23-2000  1.0
 * @author John Allen
 */
public class MoneyOrderDetail extends ReportDetail implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private long    serialNumber;
    private byte    documentType;
    private byte    documentNumber;
    private byte    payeeId;
    private boolean limboFlag    = false;
    private boolean voidFlag     = false;
    private boolean groupFlag    = false;
    private boolean trainingFlag = false;

    /* constructor */
    /*default constructor used for conversion program*/
    public MoneyOrderDetail(){
        super(true);
    }
    
    public MoneyOrderDetail(MoneyOrderInfomation mo) {
        super();
        dateTime = mo.getCreationDateTime();            //overrides super
        type = mo.getReportDocumentType();
        period = mo.getPeriod();                        //overrides super
        //Debug.println("--------------------------New MoneyOrderDetail(override): dateTime ="+dateTime+" "+new Date(dateTime)+"    period ="+period );
        serialNumber = mo.getSerialNumber().longValue();
        userId = stringToByte(mo.getEmployeeID());
        amount = mo.getAmount();
        fee = mo.getFee();

        if (type != ReportLog.VOID_TYPE) {
            dnetCode = DiagMessage.DNET_TT_TLOG_FDOC_DISPENSER;
            documentType = stringToByte(mo.getDocumentType());
            documentNumber = stringToByte(mo.getDocumentNumber().toString());
            amount = mo.getAmount();
            fee = mo.getFee();
            payeeId = stringToByte(mo.getPayeeID().toString());

            limboFlag = !(mo.isPrinted());
            voidFlag = mo.getVoidFlag();
            groupFlag = mo.getSaleIssuanceTag().equals("1");	
            trainingFlag = mo.getTrainingFlag();
        }
    }

    /* toString */
    @Override
	public String toString() {
        return 
            super.toString() + 
            "\n  MoneyOrderDetail {" +	
            "\n    serialNumber              : " + String.valueOf(serialNumber) +	
            "\n    documentType              : " + String.valueOf(documentType) +	
            "\n    documentNumber            : " + String.valueOf(documentNumber) +	
            "\n    payeeId                   : " + String.valueOf(payeeId) +	
            "\n    limboFlag                 : " + String.valueOf(limboFlag) +	
            "\n    voidFlag                  : " + String.valueOf(voidFlag) +	
            "\n    groupFlag                 : " + String.valueOf(groupFlag) +	
            "\n    trainingFlag              : " + String.valueOf(trainingFlag) +	
            "\n  } MoneyOrderDetail ";	
    }

    /* getters */
    public long    getSerialNumber()   { return serialNumber; }
    public byte    getDocumentType()   { return documentType;}
    public byte    getDocumentNumber() { return documentNumber;}
    public byte    getPayeeId()        { return payeeId;}
    public boolean getLimboFlag()      { return limboFlag;}
    public boolean getVoidFlag()       { return voidFlag;}
    public boolean getGroupFlag()      { return groupFlag;}
    public boolean getTrainingFlag()   { return trainingFlag;}
    
    
    /* setters */
    public void setSerialNumber(final long sn) { serialNumber = sn; }
    public void setDocumentType(final byte dt) { documentType = dt; }
    public void setDocumentNumber(final byte dn) { documentNumber = dn; }
    public void setPayeeId(final byte pi) { payeeId = pi; }
    public void setLimboFlag(final boolean lf) {limboFlag = lf; }
    public void setVoidFlag(final boolean vf) {voidFlag = vf; }
    public void setGroupFlag(final boolean gf) {groupFlag = gf; }
    public void setTrainingFlag(final boolean tf) {trainingFlag = tf; }
}
