package dw.io.report;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;

import dw.io.CircularInputStream;
import dw.utility.ErrorManager;

/** Iterates over the UnifiedReportDetail objects in a given input stream. */
public class UnifiedIterator implements Iterator {
    CircularInputStream cis;
    DataInputStream in;
    UnifiedReportDetail next;
    
    public UnifiedIterator(CircularInputStream input) {
        cis = input;
        in = new DataInputStream(input);
        next = null;
    }
    
    /** For debugging. */
    public UnifiedIterator(FileInputStream input) {
        cis = null;
        in = new DataInputStream(input);
        next = null;
    }
    
    @Override
	public boolean hasNext() {
        if (next == null) 
            readNext();
        
        return (next != null);
    }

    @Override
	public Object next() throws NoSuchElementException {
        if (next == null)
            readNext();
        
        if (next == null)
            throw new NoSuchElementException();
        
        UnifiedReportDetail result = next;
        next = null;
        return result;
    }

    void readNext() {
        try {
            next = new UnifiedReportDetail();
            next.readFrom(in);
            in.mark(-1);
        }
        catch (IOException e) {
            next = null;
        }
    }

    @Override
	public void remove() {
        try {
            cis.clearToMark();
        }
        catch (IOException e) {
            ErrorManager.writeStackTrace("Failed in remove", e); 
        }
    }
}
