package dw.io.report;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import dw.i18n.FormatSymbols;
import dw.printing.Report;
import dw.profile.UnitProfile;
import dw.utility.CountAndAmount;
import dw.utility.ExtraDebug;
import dw.utility.Period;
import dw.utility.PeriodWindow;
import dw.utility.TimeUtility;
import flags.CompileTimeFlags;

/**
 * Encapsulates the serializeable summary data for reports.
 * Each record contains summary data for one period.
 *
 * This class is being kept around for compatibilty reasons.
 * Old report logs contain instances of this class, and
 * existing report generating code expects instances of this
 * class as input. But this class is not expandable without
 * breaking serialization compatibility. The next time we add 
 * a product (such as gift certificates) this class will have
 * to be obsoleted by something more flexible and the report
 * generating code will have to change. -- gea
 *
 * @author John Allen
 * @version 1.0  05-22-2000
 *               06-22-2000 jma Changed to Serializable
 *               jma 07-18-2000 Build 17
 *                   MO training types are counted like voids;
 *                   neither counted in totals.
 */
public class ReportSummary implements Serializable {

    // Do not change this line. Do not delete or change the signature of
    // any public methods. (Deprecating a method that isn't used is ok.)
    static final long serialVersionUID = 7298490107758260161L;
    
    public static final String DEFAULT_FILE_NAME = "report.dat"; 
    public static final String BACKUP_FILE_NAME = "report0.dat"; 

    private byte period;
    private long periodOpened;
    private long periodClosed;
    private long absolutePeriod;
    private long serialBeg;
    private long serialEnd;
    private byte dispenserLoads;
    private HashMap<String, CountAndAmount> receiveCurrencyMap = new HashMap<String, CountAndAmount>();
    private HashMap<String, CountAndAmount> receiveToCardCurrencyMap = new HashMap<String, CountAndAmount>();
    private HashMap<String, CountAndAmount> sendCurrencyMap = new HashMap<String, CountAndAmount>();
    private HashMap<String, CountAndAmount> sendReversalCurrencyMap = new HashMap<String, CountAndAmount>();

    private CountAndAmount moneyOrderCountAndAmount = new CountAndAmount();
    private CountAndAmount vendorPaymentCountAndAmount = new CountAndAmount();
    private CountAndAmount moneyGramReceiveCountAndAmount = new CountAndAmount();
    private CountAndAmount moneyGramSendCountAndAmount = new CountAndAmount();
    private CountAndAmount moneyGramReversalCountAndAmount = new CountAndAmount();
    private CountAndAmount expressPaymentCountAndAmount = new CountAndAmount();
    private CountAndAmount utilityPaymentCountAndAmount = new CountAndAmount();
    private CountAndAmount prepaidCountAndAmount = new CountAndAmount();
    private CountAndAmount cardReloadCountAndAmount = new CountAndAmount();
    private CountAndAmount cardPurchaseCountAndAmount = new CountAndAmount();
    private CountAndAmount voidCountAndAmount = new CountAndAmount();
    private CountAndAmount moFeeCountAndAmount = new CountAndAmount();
    private CountAndAmount mgFeeCountAndAmount = new CountAndAmount();
    private CountAndAmount mgTaxCountAndAmount = new CountAndAmount();
    private CountAndAmount utilityFeeCountAndAmount = new CountAndAmount();
    private CountAndAmount utilityProcFeeCountAndAmount = new CountAndAmount();
    private CountAndAmount prepaidFeeCountAndAmount = new CountAndAmount();
    private CountAndAmount prepaidProcFeeCountAndAmount = new CountAndAmount();
    private CountAndAmount cardReloadFeeCountAndAmount = new CountAndAmount();
    private CountAndAmount cardPurchaseFeeCountAndAmount = new CountAndAmount();
    private CountAndAmount ca = new CountAndAmount();

    private int  incompleteTransaction;
    private ArrayList<InvalidLogin> badlogs = null;
    private ArrayList<Long> deltagramList = new ArrayList<Long>();

    // with Externalizable      with BAOS  (not counting the ArrayList for either)
    //      base       =  26
    // +  3 bytes x  3 =   9           3
    // + 10 longs x 10 = 100          80
    // +  7 ints x   6 =  42          28
    // =                 177   vs.   111
    // x 90 days       15930        9990
    
    // 06-20-2000 2 Externalizable records = file 630 bytes x 90 days =  56,700 bytes
    //              Serializable                 1052 bytes x 90 days = 103,680 bytes
    //              Externalizable is about half the size of Serializable
    //              and only 2 bytes larger than a pure byte offset as in State,
    //              but without the danger of off-by-one byte errors.
    // Adding detail would probably make it worth going back to Externalizable
    // (just add the readObject and writeObject methods).

    //--------------------------------------------------------------------------
    // constructors
    //--------------------------------------------------------------------------

    /** 
     * Empty constructor.
     */
    public ReportSummary() {
    }

    /**
     * Constructs an object with a Collection of detail records.
     */
    public ReportSummary(Collection<ReportDetail> c) {
        init(c);
    }

	public void init(Collection<ReportDetail> c) {
		addDetail(c);
        clearList();
	}

	private void clearList() {
		deltagramList.clear();
	}

    /**
     * Constructor for objects used for indexing the summary record TreeSet
     */
    public ReportSummary(final long absolutePeriod) {
        this.absolutePeriod = absolutePeriod;
    }

    //-------------------------------------------------------------------------
    // methods
    //-------------------------------------------------------------------------

    /* toString */
    @Override
	public String toString() {
        return 
            "ReportSummary {" +	
            "\n  period                  : " + String.valueOf(period) +	
            "\n  periodOpened            : " + String.valueOf(periodOpened) + " = " + new Date(periodOpened) +	 
            "\n  periodClosed            : " + String.valueOf(periodClosed) + " = " + new Date(periodClosed) +	 
            "\n  absoulutePeriod         : " + String.valueOf(absolutePeriod) + 	
            "\n  serialBeg               : " + String.valueOf(serialBeg) +	
            "\n  serialEnd               : " + String.valueOf(serialEnd) +	
            "\n  dispenserLoads          : " + String.valueOf(dispenserLoads) +	
            "\n  moneyOrderAmount        : " + String.valueOf(getMoneyOrderAmount()) +	
            "\n  moneyOrderCount         : " + String.valueOf(getMoneyOrderCount()) +	
            "\n  vendorPaymentAmount     : " + String.valueOf(getVendorPaymentAmount()) +	
            "\n  vendorPaymentCount      : " + String.valueOf(getVendorPaymentCount()) +	
            "\n  moneyGramReceiveAmount  : " + String.valueOf(getMoneyGramReceiveAmount()) +	
            "\n  moneyGramReceiveCount   : " + String.valueOf(getMoneyGramReceiveCount()) +	
            "\n  moneyGramSendAmount     : " + String.valueOf(getMoneyGramSendAmount()) +	
            "\n  moneyGramSendCount      : " + String.valueOf(getMoneyGramSendCount()) +	
            "\n  expressPaymentAmount    : " + String.valueOf(getExpressPaymentAmount()) +	
            "\n  expressPaymentCount     : " + String.valueOf(getExpressPaymentCount()) +	
            "\n  utilityPaymentAmount    : " + String.valueOf(getUtilityPaymentAmount()) +	
            "\n  utilityPaymentCount     : " + String.valueOf(getUtilityPaymentCount()) +	
            "\n  prepaidAmount       	 : " + String.valueOf(getPrepaidAmount()) +	
            "\n  prepaidCount        	 : " + String.valueOf(getPrepaidCount()) +	
            "\n  cardReloadAmount        : " + String.valueOf(getCardReloadAmount()) +	
            "\n  cardReloadCount         : " + String.valueOf(getCardReloadCount()) +	
            "\n  cardPurchaseAmount      : " + String.valueOf(getCardPurchaseAmount()) +	
            "\n  cardPurchaseCount       : " + String.valueOf(getCardPurchaseCount()) +	
			"\n  voidAmount              : " + String.valueOf(getVoidAmount()) +	
            "\n  voidCount               : " + String.valueOf(getVoidCount()) +	
            "\n  moFeeAmount             : " + String.valueOf(getMOFeeAmount()) +	
            "\n  moFeeCount              : " + String.valueOf(getMOFeeCount()) +	
            "\n  mgFeeAmount             : " + String.valueOf(getMGFeeAmount()) +	
            "\n  mgFeeCount              : " + String.valueOf(getMGFeeCount()) +	
            "\n  cardReloadFeeAmount     : " + String.valueOf(getCardReloadFeeAmount()) +	
            "\n  cardReloadFeeCount      : " + String.valueOf(getCardReloadFeeCount()) +	
            "\n  cardPurchaseFeeAmount   : " + String.valueOf(getCardPurchaseFeeAmount()) +	
            "\n  cardPurchaseFeeCount    : " + String.valueOf(getCardPurchaseFeeCount()) +	
			"\n  incompleteTransaction   : " + String.valueOf(incompleteTransaction) +	
            "\n  ArrayList badlogs       : " + badlogs +	
            "\n  mg reversal amount      : " + String.valueOf(getMoneyGramReversalAmount()) +	
            "\n  mg reversal count       : " + String.valueOf(getMoneyGramReversalCount()) +	
            "\n} ReportSummary ";	
    }

    /* override equals */
    @Override
	public boolean equals(Object o) {
        if ((o != null) && (o instanceof ReportSummary)) {
//            return (hashNumber == ((ReportSummary)o).hashCode());
            return true;
        } else {
            return false;
        }
    }
    
    @Override
    public int hashCode() {
    	return 0;
    }

    /* Sets the incoming time stamp to the beginning of the period for that day. */
    public void setPeriodOpend(long timestamp) {
         periodOpened = TimeUtility.getAccountingStartDateTime(timestamp);
    }

    //--------------------------------------------------------------------------
    // getters
    //      Never ask an object for the information you need to do something;
    //      rather ask the object that has the informatio to do the work for you.
    //          -- Allen Holub, "Building User Interfaces...", JavaWorld, 7/99
    // Maybe this object should have parsing capabilities and print its own data. jma
    //--------------------------------------------------------------------------
    public byte           getPeriod()                       {return period;}
    public long           getPeriodOpened()                 {return periodOpened;}
    public long           getPeriodClosed()                 {return periodClosed;}
    public long           getAbsolutePeriod()               {return absolutePeriod;}
    public long           getSerialBeg()                    {return serialBeg;}
    public long           getSerialEnd()                    {return serialEnd;}
    public byte           getDispenserLoads()               {return dispenserLoads;}
    public BigDecimal     getMoneyOrderAmount()             {return moneyOrderCountAndAmount.getBigDecimalAmount();}
    public int            getMoneyOrderCount()              {return moneyOrderCountAndAmount.getCount();}
    public BigDecimal     getVendorPaymentAmount()          {return vendorPaymentCountAndAmount.getBigDecimalAmount();}
    public int            getVendorPaymentCount()           {return vendorPaymentCountAndAmount.getCount();}
    public BigDecimal     getMoneyGramReceiveAmount()       {return moneyGramReceiveCountAndAmount.getBigDecimalAmount();}
    public int            getMoneyGramReceiveCount()        {return moneyGramReceiveCountAndAmount.getCount();}
    public CountAndAmount getMoneyGramSendCountAndAmount()  {return moneyGramSendCountAndAmount;}
    public BigDecimal     getMoneyGramSendAmount()          {return moneyGramSendCountAndAmount.getBigDecimalAmount();}
    public int            getMoneyGramSendCount()           {return moneyGramSendCountAndAmount.getCount();}
    public CountAndAmount getMoneyGramReversalCountAndAmount()  {return moneyGramReversalCountAndAmount;}
    public BigDecimal     getMoneyGramReversalAmount()      {return moneyGramReversalCountAndAmount.getBigDecimalAmount();}
    public int            getMoneyGramReversalCount()       {return moneyGramReversalCountAndAmount.getCount();}
    public CountAndAmount getExpressPaymentCountAndAmount() {return expressPaymentCountAndAmount;}
    public BigDecimal     getExpressPaymentAmount()         {return expressPaymentCountAndAmount.getBigDecimalAmount();}
    public int            getExpressPaymentCount()          {return expressPaymentCountAndAmount.getCount();}  
    public CountAndAmount getUtilityPaymentCountAndAmount() {return utilityPaymentCountAndAmount;}
    public BigDecimal     getUtilityPaymentAmount()         {return utilityPaymentCountAndAmount.getBigDecimalAmount();}
    public int            getUtilityPaymentCount()          {return utilityPaymentCountAndAmount.getCount();} 
    public CountAndAmount getPrepaidCountAndAmount() 		{return prepaidCountAndAmount;}
    public BigDecimal     getPrepaidAmount()         		{return prepaidCountAndAmount.getBigDecimalAmount();}
    public int            getPrepaidCount()          		{return prepaidCountAndAmount.getCount();} 
    public BigDecimal     getCardReloadAmount()         	{return cardReloadCountAndAmount.getBigDecimalAmount();}
    public int            getCardReloadCount()          	{return cardReloadCountAndAmount.getCount();}
    public BigDecimal     getCardPurchaseAmount()         	{return cardPurchaseCountAndAmount.getBigDecimalAmount();}
    public int            getCardPurchaseCount()         	{return cardPurchaseCountAndAmount.getCount();}   
    public BigDecimal     getVoidAmount()                   {return voidCountAndAmount.getBigDecimalAmount();}
    public int            getVoidCount()                    {return voidCountAndAmount.getCount();}
    public BigDecimal     getMOFeeAmount()                  {return moFeeCountAndAmount.getBigDecimalAmount();}
    public int            getMOFeeCount()                   {return moFeeCountAndAmount.getCount();}
    public BigDecimal     getMGFeeAmount()                  {return mgFeeCountAndAmount.getBigDecimalAmount();}
    public int            getMGFeeCount()                   {return mgFeeCountAndAmount.getCount();} 
    public BigDecimal     getMGTaxAmount()                  {return mgTaxCountAndAmount.getBigDecimalAmount();}
    public int            getMGTaxCount()                   {return mgTaxCountAndAmount.getCount();}
    public BigDecimal     getUtilityFeeAmount()             {return utilityFeeCountAndAmount.getBigDecimalAmount();}
    public int            getUtilityFeeCount()              {return utilityFeeCountAndAmount.getCount();}   
    public BigDecimal     getUtilityProcFeeAmount()         {return utilityProcFeeCountAndAmount.getBigDecimalAmount();}
    public int            getUtilityProcFeeCount()          {return utilityProcFeeCountAndAmount.getCount();} 
    public BigDecimal     getPrepaidFeeAmount()             {return prepaidFeeCountAndAmount.getBigDecimalAmount();}
    public int            getPrepaidFeeCount()              {return prepaidFeeCountAndAmount.getCount();}   
    public BigDecimal     getPrepaidProcFeeAmount()         {return prepaidProcFeeCountAndAmount.getBigDecimalAmount();}
    public int            getPrepaidProcFeeCount()          {return prepaidProcFeeCountAndAmount.getCount();} 
    public BigDecimal     getCardReloadFeeAmount()          {return cardReloadFeeCountAndAmount.getBigDecimalAmount();}
    public int            getCardReloadFeeCount()           {return cardReloadFeeCountAndAmount.getCount();}
    public BigDecimal     getCardPurchaseFeeAmount()        {return cardPurchaseFeeCountAndAmount.getBigDecimalAmount();}
    public int            getCardPurchaseFeeCount()         {return cardPurchaseFeeCountAndAmount.getCount();}   
    public int            getIncompleteTransaction()        {return incompleteTransaction;}
    public ArrayList<InvalidLogin> getBadLogs()             {return badlogs;}

    //--------------------------------------------------------------------------
    // setters
    //--------------------------------------------------------------------------
    public void setPeriod                (final byte p) {period                 = p; }
    public void setPeriodOpened          (final long p) {periodOpened           = p; }
    public void setPeriodClosed          (final long p) {periodClosed           = p; }
    public void setAbsolutePeriod        (final long p) {absolutePeriod         = p; }
    public void setSerialBeg             (final long s) {serialBeg              = s; }
    public void setSerialEnd             (final long s) {serialEnd              = s; }
    public void setDispenserLoads        (final byte d) {dispenserLoads         = d; }
    public void setMoneyOrderAmount      (final long m) {moneyOrderCountAndAmount.setLongAmount(m);}
    public void setMoneyOrderCount       (final int  m) {moneyOrderCountAndAmount.setCount(m);}
    public void setVendorPaymentAmount   (final long v) {vendorPaymentCountAndAmount.setLongAmount(v);}
    public void setVendorPaymentCount    (final int  v) {vendorPaymentCountAndAmount.setCount(v);}
    public void setMoneyGramReceiveAmount(final BigDecimal m) {moneyGramReceiveCountAndAmount.setBigDecimalAmount(m);}
    public void setMoneyGramReceiveCount (final int  m) {moneyGramReceiveCountAndAmount.setCount(m);}
    public void setMoneyGramSendAmount   (final BigDecimal m) {moneyGramSendCountAndAmount.setBigDecimalAmount(m);}
    public void setMoneyGramSendCount    (final int  m) {moneyGramSendCountAndAmount.setCount(m); }
    public void setMoneyGramReversalAmount   (final BigDecimal m) {moneyGramReversalCountAndAmount.setBigDecimalAmount(m);}
    public void setMoneyGramReversalCount    (final int  m) {moneyGramReversalCountAndAmount.setCount(m); }
    public void setExpressPaymentAmount  (final BigDecimal e) {expressPaymentCountAndAmount.setBigDecimalAmount(e);}
    public void setExpressPaymentCount   (final int  e) {expressPaymentCountAndAmount.setCount(e);}   
    public void setUtilityPaymentAmount  (final BigDecimal e) {utilityPaymentCountAndAmount.setBigDecimalAmount(e);}
    public void setUtilityPaymentCount   (final int  e) {utilityPaymentCountAndAmount.setCount(e);}   
    public void setPrepaidAmount    	 (final BigDecimal e) {prepaidCountAndAmount.setBigDecimalAmount(e);}
    public void setPrepaidCount      	 (final int  e) {prepaidCountAndAmount.setCount(e);}   
    public void setCardReloadAmount      (final BigDecimal e) {cardReloadCountAndAmount.setBigDecimalAmount(e);}
    public void setCardReloadCount       (final int  e) {cardReloadCountAndAmount.setCount(e);}
    public void setCardPurchaseAmount    (final BigDecimal e) {cardPurchaseCountAndAmount.setBigDecimalAmount(e);}
    public void setCardPurchaseCount     (final int  e) {cardPurchaseCountAndAmount.setCount(e);}   
    public void setVoidAmount            (final BigDecimal v) {voidCountAndAmount.setBigDecimalAmount(v);}
    public void setVoidCount             (final int  v) {voidCountAndAmount.setCount(v);}
    public void setMOFeeAmount           (final long m) {moFeeCountAndAmount.setLongAmount(m);}
    public void setMOFeeCount            (final int  m) {moFeeCountAndAmount.setCount(m);}
    public void setMGFeeAmount           (final BigDecimal m) {mgFeeCountAndAmount.setBigDecimalAmount(m);}
    public void setMGFeeCount            (final int  m) {mgFeeCountAndAmount.setCount(m);}  
    public void setMGTaxAmount           (final BigDecimal m) {mgTaxCountAndAmount.setBigDecimalAmount(m);}
    public void setMGTaxCount            (final int  m) {mgTaxCountAndAmount.setCount(m);} 
    public void setUtilityFeeAmount      (final BigDecimal m) {utilityFeeCountAndAmount.setBigDecimalAmount(m);}
    public void setUtilityFeeCount       (final int  m) {utilityFeeCountAndAmount.setCount(m);}  
    public void setUtilityProcFeeAmount  (final BigDecimal m) {utilityProcFeeCountAndAmount.setBigDecimalAmount(m);}
    public void setUtilityProcFeeCount   (final int  m) {utilityProcFeeCountAndAmount.setCount(m);}  
    public void setPrepaidFeeAmount      (final BigDecimal m) {prepaidFeeCountAndAmount.setBigDecimalAmount(m);}
    public void setPrepaidFeeCount       (final int  m) {prepaidFeeCountAndAmount.setCount(m);}  
    public void setPrepaidProcFeeAmount  (final BigDecimal m) {prepaidProcFeeCountAndAmount.setBigDecimalAmount(m);}
    public void setPrepaidProcFeeCount   (final int  m) {prepaidProcFeeCountAndAmount.setCount(m);}  
    public void setCardReloadFeeAmount   (final BigDecimal m) {cardReloadFeeCountAndAmount.setBigDecimalAmount(m);}
    public void setCardReloadFeeCount    (final int  m) {cardReloadFeeCountAndAmount.setCount(m);}
    public void setCardPurchaseFeeAmount (final BigDecimal m) {cardPurchaseFeeCountAndAmount.setBigDecimalAmount(m);}
    public void setCardPurchaseFeeCount  (final int  m) {cardPurchaseFeeCountAndAmount.setCount(m);} 
    public void setIncompleteTransaction (final int  i) {incompleteTransaction  = i; }


    //--------------------------------------------------------------------------
    // regular methods
    //--------------------------------------------------------------------------

    /**
     * Adds to this object the ReportDetailRecords contained in this Collection.
     * @deprecated Refactored into ReportLog.summarizeDetail.
     */
    public void addDetail(Collection<ReportDetail> c) {
        if (c == null)
            return;
        boolean isFirst = true;   
        Iterator<ReportDetail> it = c.iterator();
        ReportDetail d = null;
        while (it.hasNext()) {
            d = it.next();
            addDetail(d);
            if (isFirst){
                isFirst = false;
                period = d.getPeriod();
                absolutePeriod = d.getAbsolutePeriod();
                if(d.isPeriodChange())
                    periodOpened = d.getDateTime();
            }
        }
        if((d != null) && d.isPeriodChange())
            periodClosed = d.getDateTime();
    }

    /*
     * Adds to this object the values contained in the ReportDetail.
     * Attributes serialBeg and serialEnd hold lowest and highest values
     * of the records added.
     * Setters hold the value of the last record added.
     * Accumulators hold the sum of the records added.
     */
    public void addDetail(ReportDetail detailRecord) {
        if (detailRecord instanceof MoneyOrderDetail)
            addMoreDetail((MoneyOrderDetail) detailRecord);
        else if (detailRecord instanceof MoneyGramDetail)
            addMoreDetail((MoneyGramDetail) detailRecord);
        else if (detailRecord instanceof MoneyGramReverseDetail)
            addMoreDetail((MoneyGramReverseDetail) detailRecord);
        else if (detailRecord instanceof MoneyGramBillPayReverseDetail)
            addMoreDetail((MoneyGramBillPayReverseDetail) detailRecord);
        else if (detailRecord instanceof MoneyGramCardDetail)
            addMoreDetail((MoneyGramCardDetail) detailRecord);
        else if (detailRecord instanceof DispenserLoadDetail)
            addMoreDetail();
        else if (detailRecord instanceof InvalidLoginDetail)
            addMoreDetail((InvalidLoginDetail) detailRecord);
    }
    
    private void addMoreDetail(MoneyGramCardDetail detailRecord) {
   	
    	final BigDecimal amount = detailRecord.getAmount();
        final BigDecimal fee = detailRecord.getFee();
        
        switch(detailRecord.getType()) {
        case ReportLog.MONEYGRAM_CARD_RELOAD_TYPE:
            cardReloadCountAndAmount.addCountAndBigDecimalAmount(1,amount);
        	if (fee.compareTo(BigDecimal.ZERO) == 1)    
        		cardReloadFeeCountAndAmount.addCountAndBigDecimalAmount(1, fee);
            break;
        case ReportLog.MONEYGRAM_CARD_PURCH_TYPE:
            cardPurchaseCountAndAmount.addCountAndBigDecimalAmount(1,amount);
        	if (fee.compareTo(BigDecimal.ZERO) == 1)    
        		cardPurchaseFeeCountAndAmount.addCountAndBigDecimalAmount(1, fee);
            break;
        }
            	
    }
    
    
    /* Adds a MoneyOrderDetail types:  mo, vp, dg, . */
    private void addMoreDetail(MoneyOrderDetail detailRecord) {
        // save serial number for limbo check (and use below)
    	if (CompileTimeFlags.reportLogging()) {
            ExtraDebug.println("MO Detail Record=[" + detailRecord + "] ");
    	}
        long serialSave = detailRecord.getSerialNumber();

        // get first/last serial number
        if (serialBeg == 0 || serialSave < serialBeg)
            serialBeg = serialSave;
        if (serialEnd == 0 || serialEnd < serialSave)
            serialEnd = serialSave;
            
        CountAndAmount summaryCountAndAmount = null;
        final BigDecimal amount = detailRecord.getAmount();
            
        switch(detailRecord.getType()) {
            case ReportLog.MONEY_ORDER_TYPE:
                summaryCountAndAmount = moneyOrderCountAndAmount;
                break;
            case ReportLog.VENDOR_PAYMENT_TYPE:
                summaryCountAndAmount = vendorPaymentCountAndAmount;
                break;
            case ReportLog.DELTAGRAM_TYPE:
            	deltagramList.add(Long.valueOf(serialSave));
                return;
            case ReportLog.VOID_TYPE:
                voidCountAndAmount.addCountAndBigDecimalAmount(1,amount);
                return;
        }

        final boolean voidFlag = detailRecord.getVoidFlag();
        final boolean limboFlag = detailRecord.getLimboFlag();
        final boolean trainingFlag = detailRecord.getTrainingFlag();
        final BigDecimal fee = detailRecord.getFee();
        
        /*
         *   Limbo - add amounts/count to Summary and MO counts/Amounts
         */
        if      (!trainingFlag && !voidFlag &&  limboFlag){
            summaryCountAndAmount.addCountAndBigDecimalAmount(1,amount);
            if (fee.compareTo(BigDecimal.ZERO) == 1)
                moFeeCountAndAmount.addCountAndBigDecimalAmount(1,fee);
        }
        /*
         *    Void - back amounts/count out of Summary and MO counts/Amounts,
         *    add to void amounts/count
         */
        else if (!trainingFlag &&  voidFlag && !limboFlag){
			if (!deltagramList.contains(Long.valueOf(serialSave))) {
				summaryCountAndAmount.addCountAndBigDecimalAmount(-1,
						amount.negate());
				if (fee.compareTo(BigDecimal.ZERO) == 1)
					moFeeCountAndAmount.addCountAndBigDecimalAmount(-1,
							fee.negate());
				voidCountAndAmount.addCountAndBigDecimalAmount(1, amount);
			}
        }
        /*
         *    Training - Add Void amount/count
         */
        else if ( trainingFlag &&  limboFlag) {
            voidCountAndAmount.addCountAndBigDecimalAmount(1,amount);
        }
    }

    /* Adds a MoneyGramDetail. */
    private void addMoreDetail(MoneyGramDetail detailRecord) {
        if (!(detailRecord.getSuccessFlag())) {
            incompleteTransaction++;
            return;
        }

        final BigDecimal amount = detailRecord.getAmount();
        if (detailRecord.getType() == ReportLog.MONEYGRAM_SEND_TYPE  || detailRecord.getType() == ReportLog.EXPRESS_PAYMENT_TYPE) {
        	String currency = detailRecord.getCurrencyCode();
        	if (detailRecord.getCurrencyCode() == null || 
        	    detailRecord.getCurrencyCode().equals("0")  ||
				detailRecord.getCurrencyCode().trim().length() == 0)
        		currency = UnitProfile.getInstance().get("AGENT_CURRENCY", "");
        	Object o = sendCurrencyMap.get(currency);
        	if (o != null) {
        		ca = (CountAndAmount) o; 
        		ca.addCountAndBigDecimalAmount(1, amount);
        		ca.addFeeAmount(detailRecord.getFee());
        		ca.addTaxAmount(detailRecord.getTax());
        	}
            else {
            	ca = new CountAndAmount();
    			ca.addCountAndBigDecimalAmount(1, amount);
    			ca.addFeeAmount(detailRecord.getFee());
    			ca.addTaxAmount(detailRecord.getTax());
    			sendCurrencyMap.put(currency, ca);
            }
            moneyGramSendCountAndAmount.addCountAndBigDecimalAmount(1, amount);
            mgFeeCountAndAmount.addCountAndBigDecimalAmount(1, detailRecord.getFee());
            if (detailRecord.getTax() != null) {
                mgTaxCountAndAmount.addCountAndBigDecimalAmount(1, detailRecord.getTax());
            }
        }
        else if (detailRecord.getType() == ReportLog.MONEYGRAM_RECEIVE_TYPE) { 
        	String currency = detailRecord.getCurrencyCode();
        	if (detailRecord.getCurrencyCode() == null || 
        	    detailRecord.getCurrencyCode().equals("0")  ||
				detailRecord.getCurrencyCode().trim().length() == 0)
        		currency = UnitProfile.getInstance().get("AGENT_CURRENCY", "");
        	Object o = receiveCurrencyMap.get(currency);
        	if (o != null) {
        		ca = (CountAndAmount) o; 
        		ca.addCountAndBigDecimalAmount(1, amount);
        	}
            else {
            	ca = new CountAndAmount();
    			ca.addCountAndBigDecimalAmount(1, amount);
    			receiveCurrencyMap.put(currency, ca);
            }
        	mgFeeCountAndAmount.addCountAndBigDecimalAmount(1, detailRecord.getFee());
        }
        else if (detailRecord.getType() == ReportLog.MONEYGRAM_RECEIVE_TYPE_PPC) { 
        	String currency = detailRecord.getCurrencyCode();
        	if (detailRecord.getCurrencyCode() == null || 
        	    detailRecord.getCurrencyCode().equals("0")  ||
				detailRecord.getCurrencyCode().trim().length() == 0)
        		currency = UnitProfile.getInstance().get("AGENT_CURRENCY", "");
        	Object o = receiveToCardCurrencyMap.get(currency);
        	if (o != null) {
        		ca = (CountAndAmount) o; 
        		ca.addCountAndBigDecimalAmount(1, amount);
        	}
            else {
            	ca = new CountAndAmount();
    			ca.addCountAndBigDecimalAmount(1, amount);
    			receiveToCardCurrencyMap.put(currency, ca);
            }
        	mgFeeCountAndAmount.addCountAndBigDecimalAmount(1, detailRecord.getFee());
        }
        else if (detailRecord.getType() == ReportLog.UTILITY_PAYMENT_TYPE) {
            utilityPaymentCountAndAmount.addCountAndBigDecimalAmount(1, amount);
            utilityFeeCountAndAmount.addCountAndBigDecimalAmount(1, detailRecord.getFee());
            if (detailRecord.getProcessingFee() != null)
            	utilityProcFeeCountAndAmount.addCountAndBigDecimalAmount(1, detailRecord.getProcessingFee());
            else
            	utilityProcFeeCountAndAmount.addCountAndBigDecimalAmount(1, BigDecimal.ZERO);
        }
        else if (detailRecord.getType() == ReportLog.PREPAID_SERVICES_TYPE)  {
            prepaidCountAndAmount.addCountAndBigDecimalAmount(1, amount);
            prepaidFeeCountAndAmount.addCountAndBigDecimalAmount(1, detailRecord.getFee());
            if (detailRecord.getProcessingFee() != null)
            	prepaidProcFeeCountAndAmount.addCountAndBigDecimalAmount(1, detailRecord.getProcessingFee());
            else
            	prepaidProcFeeCountAndAmount.addCountAndBigDecimalAmount(1, BigDecimal.ZERO);
        }
        else if (detailRecord.getType() == ReportLog.EXPRESS_PAYMENT_TYPE) {
            expressPaymentCountAndAmount.addCountAndBigDecimalAmount(1, amount);
            mgFeeCountAndAmount.addCountAndBigDecimalAmount(1, detailRecord.getFee());
        }
        else if (detailRecord.getType() == ReportLog.MONEYGRAM_REVERSAL_TYPE){
        	
            moneyGramReversalCountAndAmount.addCountAndBigDecimalAmount(1, amount);
            mgFeeCountAndAmount.addCountAndBigDecimalAmount(1, detailRecord.getFee());
        }

//        final BigDecimal fee = detailRecord.getFee();
//        if (fee.compareTo(BigDecimal.ZERO) == 1)    
//            mgFeeCountAndAmount.addCountAndBigDecimalAmount(1, fee);
    }

    /* Adds a MoneyGramDetail. */
    private void addMoreDetail(MoneyGramReverseDetail detailRecord) {
        if (!(detailRecord.getSuccessFlag())) {
            incompleteTransaction++;
            return;
        }

    	
    	 final BigDecimal amount = detailRecord.getTotal();
         moneyGramReversalCountAndAmount.addCountAndBigDecimalAmount(1, amount);
    	String currency = detailRecord.getCurrencyCode();
    	if (detailRecord.getCurrencyCode() == null || 
    	    detailRecord.getCurrencyCode().equals("0")  ||
			detailRecord.getCurrencyCode().trim().length() == 0)
    		currency = UnitProfile.getInstance().get("AGENT_CURRENCY", "");
    	Object o = sendReversalCurrencyMap.get(currency);
    	if (o != null) {
    		ca = (CountAndAmount) o; 
    		ca.addCountAndBigDecimalAmount(1, amount);
    		//ca.addFeeAmount(detailRecord.getFee());
    	}
        else {
        	ca = new CountAndAmount();
			ca.addCountAndBigDecimalAmount(1, amount);
		//	ca.addFeeAmount(detailRecord.getFee());
			sendReversalCurrencyMap.put(currency, ca);
        }
           
       
    }
    private void addMoreDetail(MoneyGramBillPayReverseDetail detailRecord) {
        if (!(detailRecord.getSuccessFlag())) {
            incompleteTransaction++;
            return;
        }

        final BigDecimal amount = detailRecord.getTotal();
        moneyGramReversalCountAndAmount.addCountAndBigDecimalAmount(1, amount);
    }

    /* Adds a DispenserLoadDetail. */
    private void addMoreDetail() {
        dispenserLoads++;
    }

    /* Adds a InvalidLoginDetail. */
    private void addMoreDetail(InvalidLoginDetail detailRecord) {
        if (badlogs == null) {
            badlogs = new ArrayList<InvalidLogin>();
        }
        badlogs.add(0, new InvalidLogin(detailRecord.getUserId(),
                    detailRecord.getDateTime()));
    }

    public boolean isForCurrentPeriod() {
/*
        Debug.println("isForCurrentPeriod: getPeriod() = " + getPeriod()	
                + " getPeriodClosed() = " + getPeriodClosed());	
        Debug.dumpStack();
        return (getPeriodClosed() == 0);
*/        
        final int currentPeriod = Period.getPeriod();
        final long currentTimeMillis = TimeUtility.currentTimeMillis();
/*
        Debug.println("isForCurrentPeriod: currentPeriod = " + currentPeriod	
                + "\n getPeriod() = " + getPeriod() 	
                + "\n currentTimeMillis = " + currentTimeMillis	
                + "\n getPeriodOpened() = " + getPeriodOpened()	
                + "\n difference        = " + (currentTimeMillis - getPeriodOpened())	
                + "\n MAX_PERIOD_OFFSET = " + PeriodWindow.MAX_PERIOD_OFFSET);	
*/
        if (currentPeriod != getPeriod())
            return false;
        return (currentTimeMillis - getPeriodOpened()) <=
                                   PeriodWindow.MAX_PERIOD_OFFSET;
    }

    //--------------------------------------------------------------------------
    // InvalidLogin class
    //--------------------------------------------------------------------------

    public class InvalidLogin implements Serializable {
		private static final long serialVersionUID = 1L;

		private byte user;
        private long timestamp;

        // constructors
        //public InvalidLogin() {
        //}

        public InvalidLogin(byte userId, long timestamp) {
            this.user = userId;
            this.timestamp = timestamp;
        }

        // getUserId
        public byte getUserId() { return user; }

        // getTimeStamp
        public long getTimeStamp() { return timestamp; }

        // toString
        @Override
		public String toString() {
            return 
                "  InvalidLogin {" +	
                "\n    user     : " + String.valueOf(user) +	
                "\n    timestamp: " + String.valueOf(timestamp) +	
                "\n  } InvalidLogin";	
        }
    }
    
//    protected static final int MO_PLACES = Report.MO_PLACES;
//    protected static final int MG_PLACES = Report.MG_PLACES;
    
    private void addSerialNumberFieldValue(final HashMap<String, Object> fieldMap, final String key, final long serialNumber) {
        fieldMap.put(key,Report.checkDigit(String.valueOf(serialNumber)));
    }
    
    private void addIntFieldValue(final HashMap<String, Object> fieldMap, final String key, final int value) {
        fieldMap.put(key,Integer.toString(value));
    }
    
    private void addAmountFieldValue(final HashMap<String, Object> fieldMap, final String key, final BigDecimal amount) {       
        fieldMap.put(key, FormatSymbols.convertDecimal(amount.toString()));	
    }
    
    private void addMOAmountFieldValue(final HashMap<String, Object> fieldMap, final String key, final BigDecimal amount) {
        addAmountFieldValue(fieldMap,key,amount);
    }
    
    private void addMGAmountFieldValue(final HashMap<String, Object> fieldMap, final String key, final BigDecimal amount) {
        addAmountFieldValue(fieldMap,key,amount);
    }
    
    private void addCardAmountFieldValue(final HashMap<String, Object> fieldMap, final String key, final BigDecimal amount) {
        addAmountFieldValue(fieldMap,key,amount);
    }
    
    public void addFieldValues(final HashMap<String, Object> fieldMap) {
        final BigDecimal moAmount              = getMoneyOrderAmount();
        final BigDecimal vpAmount              = getVendorPaymentAmount();
        final BigDecimal mgsAmount             = getMoneyGramSendAmount();
        final BigDecimal mgrevAmount           = getMoneyGramReversalAmount();
        final BigDecimal mgrAmount             = getMoneyGramReceiveAmount();
        final BigDecimal expAmount             = getExpressPaymentAmount();
        final BigDecimal utilityAmount         = getUtilityPaymentAmount();
        final BigDecimal prepaidAmount         = getPrepaidAmount();
        final BigDecimal voidAmount            = getVoidAmount();
        final BigDecimal moFeeAmount           = getMOFeeAmount();
        final BigDecimal mgFeeAmount   		   = getMGFeeAmount();
        final BigDecimal mgTaxAmount   		   = getMGTaxAmount();
        final BigDecimal utilityFeeAmount  	   = getUtilityFeeAmount();
        final BigDecimal prepaidFeeAmount  	   = getPrepaidFeeAmount();
        final BigDecimal utilityProcFeeAmount  = getUtilityProcFeeAmount();
        final BigDecimal prepaidProcFeeAmount  = getPrepaidProcFeeAmount();
        final BigDecimal cardReloadAmount      = getCardReloadAmount();
        final BigDecimal cardPurchaseAmount    = getCardPurchaseAmount();       
        final BigDecimal cardReloadFeeAmount   = getCardReloadFeeAmount();
        final BigDecimal cardPurchaseFeeAmount = getCardPurchaseFeeAmount();
        final BigDecimal grandCardFeeAmount    = cardReloadFeeAmount.add(cardPurchaseFeeAmount);
        final BigDecimal grandCardAmount       = cardReloadAmount.add(cardPurchaseAmount);
        final BigDecimal grandMOAmount         = moAmount.add(vpAmount).add(moFeeAmount);
        final BigDecimal grandMGAmount         = mgsAmount.add(expAmount).add(mgFeeAmount).add(mgTaxAmount);
        final BigDecimal grandUtilityAmount    = utilityAmount.add(utilityFeeAmount);
        final BigDecimal grandUtilityProcFeeAmount = getUtilityProcFeeAmount();
        final BigDecimal grandPrepaidProcFeeAmount = getPrepaidProcFeeAmount();
        final BigDecimal grandPrepaidAmount    = prepaidAmount.add(prepaidFeeAmount);
        final BigDecimal grandTotalCard        = grandCardAmount.add(grandCardFeeAmount);

        
        final int  moQty           = getMoneyOrderCount();
        final int  vpQty           = getVendorPaymentCount();
        final int  mgsQty          = getMoneyGramSendCount();
        final int  mgrevQty        = getMoneyGramReversalCount();
        final int  mgrQty          = getMoneyGramReceiveCount();
        final int  expQty          = getExpressPaymentCount();
        final int  utilityQty      = getUtilityPaymentCount();
        final int  prepaidQty      = getPrepaidCount();
        final int  utilityProcQty  = getUtilityProcFeeCount();
        final int  prepaidProcQty  = getPrepaidProcFeeCount();
        final int  voidQty         = getVoidCount();
        final int  moFeeQty        = getMOFeeCount();
        final int  mgFeeQty        = getMGFeeCount();
        final int  mgTaxQty        = getMGTaxCount();
        final int  cardReloadQty   = getCardReloadCount();
        final int  cardPurchaseQty = getCardPurchaseCount();
        final int  grandMOQty      = moQty + vpQty + mgrQty + voidQty;
        final int  grandMGQty      = mgsQty + expQty;
        final int  grandCardQty    = cardReloadQty + cardPurchaseQty;
        final int  grandUtilityQty = getUtilityPaymentCount();
        final int  grandPrepaidQty = getPrepaidCount();
        final int  grandUtilityProcQty = getUtilityProcFeeCount();
        final int  grandPrepaidProcQty = getPrepaidProcFeeCount();
        
        addSerialNumberFieldValue(fieldMap,"serialBeg"       ,getSerialBeg()); 
        addSerialNumberFieldValue(fieldMap,"serialEnd"       ,getSerialEnd()); 
        addIntFieldValue         (fieldMap,"dispenserLoads"  ,getDispenserLoads()); 
        addMOAmountFieldValue    (fieldMap,"mgReceiveTotal"  ,mgrAmount); 
        fieldMap.put             (         "badLogin"        ,"badLogin");  
        addMOAmountFieldValue    (fieldMap,"moAmount"        ,moAmount); 
        addMOAmountFieldValue    (fieldMap,"vpAmount"        ,vpAmount); 
        addMGAmountFieldValue    (fieldMap,"mgsAmount"       ,mgsAmount); 
        addMGAmountFieldValue    (fieldMap,"mgrAmount"       ,mgrAmount); 
        addMGAmountFieldValue    (fieldMap,"mgrevAmount"     ,mgrevAmount); 
        addMGAmountFieldValue    (fieldMap,"expAmount"       ,expAmount); 
        addMGAmountFieldValue    (fieldMap,"utilityAmount"   ,utilityAmount); 
        addMGAmountFieldValue    (fieldMap,"prepaidAmount"   ,prepaidAmount); 
        addMOAmountFieldValue    (fieldMap,"voidAmount"      ,voidAmount); 
        addMOAmountFieldValue    (fieldMap,"moFeeAmount"     ,moFeeAmount); 
        addMGAmountFieldValue    (fieldMap,"mgFeeAmount"     ,mgFeeAmount); 
        addMGAmountFieldValue    (fieldMap,"mgTaxAmount"     ,mgTaxAmount); 
        addMGAmountFieldValue    (fieldMap,"utilityFeeAmount"  ,utilityFeeAmount);
        addMGAmountFieldValue    (fieldMap,"prepaidFeeAmount"  ,prepaidFeeAmount);
        addMGAmountFieldValue    (fieldMap,"utilityProcFeeAmount"  ,utilityProcFeeAmount);
        addMGAmountFieldValue    (fieldMap,"prepaidProcFeeAmount"  ,prepaidProcFeeAmount);
        addMOAmountFieldValue    (fieldMap,"grandMOAmount"   ,grandMOAmount); 
        addMGAmountFieldValue    (fieldMap,"grandMGAmount"   ,grandMGAmount);         
        addMGAmountFieldValue    (fieldMap,"grandUtilityAmount"   ,grandUtilityAmount);
        addMGAmountFieldValue    (fieldMap,"grandUtilityProcFeeAmount"   ,grandUtilityProcFeeAmount);
        addMGAmountFieldValue    (fieldMap,"grandPrepaidProcFeeAmount"   ,grandPrepaidProcFeeAmount);
        addMGAmountFieldValue    (fieldMap,"grandPrepaidAmount"   ,grandPrepaidAmount);
        addCardAmountFieldValue  (fieldMap,"totReloadAmt"    ,cardReloadAmount);
        addCardAmountFieldValue  (fieldMap,"totPurchAmt"     ,cardPurchaseAmount);
        addCardAmountFieldValue  (fieldMap,"totReloadFees"   ,cardReloadFeeAmount);
        addCardAmountFieldValue  (fieldMap,"totPurchFees"    ,cardPurchaseFeeAmount);
        addCardAmountFieldValue  (fieldMap,"grandCardFees"   ,grandCardFeeAmount);
        addCardAmountFieldValue  (fieldMap,"grandCardAmt"    ,grandCardAmount);   
        addCardAmountFieldValue  (fieldMap,"grandTotalCard"  ,grandTotalCard);
        addIntFieldValue         (fieldMap,"moQty"           ,moQty); 
        addIntFieldValue         (fieldMap,"vpQty"           ,vpQty); 
        addIntFieldValue         (fieldMap,"mgsQty"          ,mgsQty); 
        addIntFieldValue         (fieldMap,"mgrQty"          ,mgrQty); 
        addIntFieldValue         (fieldMap,"mgrevQty"        ,mgrevQty); 
        addIntFieldValue         (fieldMap,"mgrQtyIncomplete",getIncompleteTransaction()); 
        addIntFieldValue         (fieldMap,"expQty"          ,expQty); 
        addIntFieldValue         (fieldMap,"utilityQty"      ,utilityQty); 
        addIntFieldValue         (fieldMap,"prepaidQty"      ,prepaidQty);
        addIntFieldValue         (fieldMap,"utilityProcQty"  ,utilityProcQty); 
        addIntFieldValue         (fieldMap,"prepaidProcQty"  ,prepaidProcQty); 
        addIntFieldValue         (fieldMap,"voidQty"         ,voidQty); 
        addIntFieldValue         (fieldMap,"moFeeQty"        ,moFeeQty); 
        addIntFieldValue         (fieldMap,"mgFeeQty"        ,mgFeeQty); 
        addIntFieldValue         (fieldMap,"mgTaxQty"        ,mgTaxQty); 
        addIntFieldValue         (fieldMap,"grandMOQty"      ,grandMOQty); 
        addIntFieldValue         (fieldMap,"grandMGQty"      ,grandMGQty);   
        addIntFieldValue         (fieldMap,"grandUtilityQty"      ,grandUtilityQty);
        addIntFieldValue         (fieldMap,"grandPrepaidQty"      ,grandPrepaidQty);
        addIntFieldValue         (fieldMap,"grandUtilityProcQty"  ,grandUtilityProcQty);
        addIntFieldValue         (fieldMap,"grandPrepaidProcQty"  ,grandPrepaidProcQty);
        addIntFieldValue         (fieldMap,"totReloadQty"    ,cardReloadQty); 
        addIntFieldValue         (fieldMap,"totPurchQty"     ,cardPurchaseQty); 
        addIntFieldValue         (fieldMap,"grandCardQty"    ,grandCardQty); 
    }

	/**
	 * @return Returns the currencyMap.
	 */
	public HashMap<String, CountAndAmount> getReceiveCurrencyMap() {
		return receiveCurrencyMap;
	}
	
	public HashMap<String, CountAndAmount> getReceiveToCardCurrencyMap() {
		return receiveToCardCurrencyMap;
	}
	
	public HashMap<String, CountAndAmount> getSendCurrencyMap() {
		return sendCurrencyMap;
	}
	
	public HashMap<String, CountAndAmount> getSendReversalCurrencyMap() {
		return sendReversalCurrencyMap;
	}
//	/**
//	 * @param currencyMap The currencyMap to set.
//	 */
//	public void setCurrencyMap(HashMap currencyMap) {
//		this.currencyMap = currencyMap;
//	}
}
