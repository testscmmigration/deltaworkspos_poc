package dw.io.report;

import java.util.NoSuchElementException;

import dw.io.CircularInputStream;

/** 
 * Provides compatibility for existing report code. 
 * Iterates over UnifiedReportDetail objects in the given input stream,
 * filters out any that do not match the given criteria, and converts
 * them into the ReportDetail objects expected by existing report code.
 *
 * Implementation note: this class contains the criteria selection logic
 * that used to be in ReportLog.getDetailRecords(ReportSelectionCriteria).
 */
class ReportDetailIterator extends UnifiedIterator {
    ReportSelectionCriteria criteria;
    boolean filtersPeriod = false;
    boolean filtersDateTime = false;
    boolean filtersTimeRangeOnly = false;
    boolean nothingWillMatch = false;
    
    long startPeriod;
    long endPeriod;
    long startDateTime;
    long endDateTime;
    
    ReportDetailIterator(CircularInputStream input,
            ReportSelectionCriteria c) {
        super(input);
        criteria = c;
        filtersTimeRangeOnly = criteria.filtersTimeRangeOnly();
        
        long specifiedStartDateTime = criteria.getStartDateTime();
        long specifiedEndDateTime = criteria.getEndDateTime();
        long specifiedStartPeriod = criteria.getStartPeriod();
        long specifiedEndPeriod = criteria.getEndPeriod();
        
        if (ReportSelectionCriteria.intSpecified(specifiedStartDateTime) &&
                ReportSelectionCriteria.intSpecified(specifiedEndDateTime)) {
            filtersDateTime = true;
            startDateTime = specifiedStartDateTime;
            endDateTime = specifiedEndDateTime;
        }
        else if (ReportSelectionCriteria.intSpecified(specifiedStartPeriod) &&
                ReportSelectionCriteria.intSpecified(specifiedEndPeriod)) {
            filtersPeriod = true;
            startPeriod = specifiedStartPeriod;
            endPeriod = specifiedEndPeriod;
        }
        else {
            // Don't know when this can happen, but this is the effect of
            // what the old ReportLog.getDetailRecords() method was doing
            // in this case. 
            nothingWillMatch = true;
        }
    }
    
    @Override
	public Object next() throws NoSuchElementException {
        return ((UnifiedReportDetail) super.next()).reportDetail();
    }
    
    @Override
	void readNext() {
        if (nothingWillMatch) {
            next = null;
        }
        else {
            super.readNext();
            while (! nextMatchesCriteria()) {
                super.readNext();
            }
        }
    }

    boolean nextMatchesCriteria() {
        if (next == null) 
            return true;  // lets us break out of readNext loop

        if (filtersTimeRangeOnly)
            return nextMatchesTimeRange();
        else
            return nextMatchesTimeRange() && nextSelected();
    }

    // This has to be compatible with DetailRecordComparator.
    // Next matches if is is between start time (inclusive) and 
    // end time (exclusive) or between start period (inclusive) 
    // and end period (inclusive). 
    boolean nextMatchesTimeRange() {
        long period, dateTime;
        
        //Debug.println("nextMatchesTimeRange(): filtersPeriod=" 	
        //        + filtersPeriod + " filtersDateTime=" + filtersDateTime);	
        if (filtersPeriod) {
            period = next.getLong(ReportLogConstants.ABSOLUTE_PERIOD);
            //Debug.println(" period=" + period + " startPeriod=" 	 
            //        + startPeriod + " endPeriod=" + endPeriod);	
            
            if (period < startPeriod)
                return false;
            if (period > endPeriod)     // subtle behavior of comparator
                return false;
        }

        if (filtersDateTime) {
            dateTime = next.getDateTime();
            //Debug.println(" dateTime=" + dateTime + " startDateTime=" 	 
            //        + startDateTime + " endDateTime=" + endDateTime);	
            
            if (dateTime < startDateTime)
                return false;
            if (dateTime >= endDateTime)
                return false;
        }

        return true;
        
    }

    boolean nextSelected() {
        return criteria.reportDetailSelected(next.reportDetail());
    }
}

