package dw.io.report;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.moneygram.agentconnect.BillPaymentDetailInfo;
import com.moneygram.agentconnect.MoneyGramReceiveDetailInfo;
import com.moneygram.agentconnect.MoneyGramSendDetailInfo;
import com.moneygram.agentconnect.MoneyGramSendWithTaxDetailInfo;

import dw.io.diag.DiagMessage;
import dw.io.info.MoneyGramReceiveDiagInfo;
import dw.moneyorder.MoneyOrderInfomation;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.Period;
import dw.utility.TimeUtility;

/**
 * Encapsulates a MoneyGram Send type of detail record in the report log.
 * This includes:  MoneyGram sends and BillPayments.
 * @version 06-23-2000  1.0
 * @author John Allen
 */
public class MoneyGramDetail extends ReportDetail implements Serializable {
	private static final long serialVersionUID = 1L;

    private String  referenceNumber;
    private String  hostTime;
    private BigDecimal    exchange;
    private byte    attempts;
    private boolean successFlag = true;
    private String  name;
    private String  empNumber;
    private String  serialNumber;
    private String  accountNumber;
    private String  currencyCode;
    private String  receiveCurrencyCount;
    private String  receiveToCardCurrencyCount;
    private String  sendCurrencyCount;
    private String  sendReversalCurrencyCount;
    private String  agentID;
    private String  agentName;
    private BigDecimal    taxPercentage;
    
    /**default constructor used for conversion program */
    public MoneyGramDetail() {
        super(true);
    }
    
    /** MoneyGram Receive constructor, local */
    public MoneyGramDetail(MoneyGramReceiveDiagInfo mgrdi, final MoneyOrderInfomation moi) {
        super();
        int dt = 0;
        try {
        	dt = Integer.parseInt(moi.getDocumentType());
        } catch (Exception e) {
        }
        
        initializeLocalDetail(dt,
                              mgrdi.getReferenceNumber(),
                              mgrdi.getAmount(),
                              BigDecimal.ZERO,
                              mgrdi.getEmployeeNumber(),
                              mgrdi.getExchangeRate(),
                              mgrdi.getNumberOfAttempts(),
                              mgrdi.isSuccessful());
        name = moi.getPayeeName();
        serialNumber = moi.getSerialNumber().toString();
    }
    
    /** MoneyGram Send and BillPayment constructor, host */
    public MoneyGramDetail(final String activityDate,
                           final MoneyGramSendDetailInfo sdri) {
        super();
       
        initializeHostDetail(activityDate,
                             TimeUtility.getTime(sdri.getTime()),
                             ReportLog.MONEYGRAM_SEND_TYPE,
                             sdri.getReferenceNumber(),
                             sdri.getFaceAmount(),
                             sdri.getFeeAmount(),
							 BigDecimal.ZERO,							 
                             sdri.getAgentNumber(),
                             sdri.getAgentName(),
							 sdri.getSendCurrency());
    }

    /** MoneyGram Send [with non-MGI tax] and BillPayment constructor, host */
    public MoneyGramDetail(final String activityDate,
                           final MoneyGramSendWithTaxDetailInfo sdri) {
        super();
       
        initializeHostDetail(activityDate,
        					 null, // GST agents without sdri.getTime() data
                             ReportLog.MONEYGRAM_SEND_TYPE,
                             sdri.getReferenceNumber(),
                             sdri.getFaceAmount(),
                             sdri.getFeeAmount(),
                             sdri.getTaxAmount(), // for GST agents
                             sdri.getTaxPercentage(), // GST agents
                             sdri.getAgentName(),
                             sdri.getTotalAmount(),
							 sdri.getSendCurrency());
    }
 
    /** MoneyGram Utility Bill Payment constructor, host */  
    public MoneyGramDetail(final String activityDate,
            final BillPaymentDetailInfo sdri, boolean isPreePay) {
        //int paymentType = 0;
               
          super();
             initializeHostDetail(activityDate,
              TimeUtility.getTime(sdri.getDate()),
              isPreePay == true ? ReportLog.PREPAID_SERVICES_TYPE : ReportLog.UTILITY_PAYMENT_TYPE,
              sdri.getReferenceNumber(),
              sdri.getFaceAmount(),
              sdri.getFeeAmount(),
			  sdri.getProcessingFee(),
              sdri.getAgentNumber(),
			  sdri.getAgentName(),
              null);
}
    
    /** MoneyGram Receive constructor, host */
    public MoneyGramDetail(final String activityDate,
                           final MoneyGramReceiveDetailInfo rdri) {
        super();
        
        int type;
        
        if (rdri.getPayoutType().equalsIgnoreCase(DWValues.RECV_TO_CARD)) {
         	type = ReportLogConstants.MONEYGRAM_RECEIVE_TYPE_PPC;
         	accountNumber = rdri.getAccountNumber();
        } else {
         	type = ReportLogConstants.MONEYGRAM_RECEIVE_TYPE;
        }
        initializeHostDetail(activityDate,
                             TimeUtility.getTime(rdri.getTime()),
                             type,
                             rdri.getReferenceNumber(),
                             rdri.getReceiveAmount(),
                             BigDecimal.ZERO,
                             BigDecimal.ZERO,
                             rdri.getAgentNumber(),
                             rdri.getAgentName(),
                             rdri.getReceiveCurrency());	
        name = rdri.getReceiverName();
        StringBuffer sb = new StringBuffer(rdri.getCheckNumber());
        
        // remove the prefix digit the mainframe adds to MO's per old code
        // commented out below
        
        // remove dashes, but in a safer way than the old code 
        int i = sb.indexOf("-");
        while (i != -1) {
            sb.deleteCharAt(i);
            i = sb.indexOf("-", i);
        }
        
        serialNumber = sb.toString();
        
        accountNumber = rdri.getAccountNumber();
    }
    
    private void initializeLocalDetail(final int t,        // type
                                       final String rn,    // reference number
                                       final BigDecimal a,       // amount
                                       final BigDecimal f,       // fee
                                       final int employeeNumber,
                                       final BigDecimal e,       // exchange
                                       final int at,       // attempts
                                       final boolean sf) { // success flag
        initializeLocal(t,rn,a,f);
        userId = (byte)employeeNumber;
        exchange = e;
        attempts = (byte)at;
        successFlag = sf;
    }

    private void initializeHostDetail(final String activityDate,
                                      final String timeString,
                                      final int type,
                                      final String referenceNumber,
                                      BigDecimal amount,
                                      BigDecimal fee,
                                      BigDecimal processingFee,
                                      final String agentID,
									   String agentName,
                                      final String currencyCode) {
    	
    	if(agentName.lastIndexOf("(") >0){
    		String agentNameMinusCurrency = agentName.substring(0,agentName.lastIndexOf("("));
    		agentName = agentNameMinusCurrency;
    	}

        initialize(type,
                   referenceNumber,
                   amount,
                   fee,
                   BigDecimal.ZERO, // processingFee N/A 
                   BigDecimal.ZERO, // tax N/A
                   BigDecimal.ZERO, // taxPercentage N/A
                   agentID,
				   agentName,
                   currencyCode);

        final String dateTimeString = activityDate + " " + timeString;	
        Date dateTimeObject;
        try {
            dateTimeObject = new SimpleDateFormat(TimeUtility.DATE_TIME_N0_DELIMITER_FORMAT).parse(dateTimeString);
        }
        catch (ParseException pe) {
            Debug.printStackTrace(pe);
            dateTimeObject = TimeUtility.currentTime();
        }
        dateTime = dateTimeObject.getTime();
        setAbsolutePeriod(Period.getAbsolutePeriod(dateTime));
        hostTime = new SimpleDateFormat(TimeUtility.TIME_24_HOUR_MINUTE_SECOND_FORMAT).format(dateTimeObject);
    }
    
    // initialize host detail given tax & taxCode
	private void initializeHostDetail(final String activityDate,
			final String timeString,
			final int type,
			final String referenceNumber,
			final BigDecimal amount,
			final BigDecimal fee,
			final BigDecimal tax,
			final BigDecimal taxPercentage,
			String agentName,
			final BigDecimal tot,
			final String currencyCode) {

    	// workaround if null timeString received 
 
		if (agentName.lastIndexOf("(") > 0) {
			String agentNameMinusCurrency = agentName.substring(0,
					agentName.lastIndexOf("("));
			agentName = agentNameMinusCurrency;
		}

		initialize(type, referenceNumber, amount, fee, BigDecimal.ZERO, tax,
				taxPercentage, "0", agentName, currencyCode);

		Date dateTimeObject = TimeUtility.currentTime();;
		dateTime = dateTimeObject.getTime();
		setAbsolutePeriod(Period.getAbsolutePeriod(dateTime));
		hostTime = new SimpleDateFormat(TimeUtility.TIME_24_HOUR_MINUTE_SECOND_FORMAT).format(dateTimeObject);
//		if (tot != null)
//			total = new BigDecimal(tot);
		// setDateTime(time);
	}

    private void initialize(final int t,     // type
                            final String rn, // reference number
                            final BigDecimal a,    // amount
                            final BigDecimal f,   // fee
                            final BigDecimal pf,   // processingFee
                            final BigDecimal tx,   // GST tax
                            final BigDecimal tp, // GST taxPercentage
                            final String agtid,  // agentId
							final String agentname,
                            final String currency){ // currencyCode
        type = t;
        switch (type) {
            case ReportLog.MONEYGRAM_SEND_TYPE:
                dnetCode = DiagMessage.DW_TT_TLOG_FDOC_MG_SEND;
                break;
            case ReportLog.MONEYGRAM_RECEIVE_TYPE:
            case ReportLog.MONEYGRAM_RECEIVE_TYPE_PPC:
                dnetCode = DiagMessage.DW_TT_TLOG_FDOC_MG_RECV;
                break;
            case ReportLog.BILL_PAYMENT_TYPE:
                dnetCode = DiagMessage.DW_TT_TLOG_FDOC_MG_XPAY;
                break;
        }
        referenceNumber = rn;
        amount = a;
        fee = f;
        processingFee = pf;
        tax = tx;
        taxPercentage = tp;
        agentID = agtid;
        agentName = agentname;
        currencyCode = currency;
    }

    private void initializeLocal(final int t, // type
            final String rn, // reference number
            final BigDecimal a, // amount
            final BigDecimal f) { // fee
        type = t;
        switch (type) {
        case ReportLog.MONEYGRAM_SEND_TYPE:
            dnetCode = DiagMessage.DW_TT_TLOG_FDOC_MG_SEND;
            break;
        case ReportLog.MONEYGRAM_RECEIVE_TYPE:
        case ReportLog.MONEYGRAM_RECEIVE_TYPE_PPC:
            dnetCode = DiagMessage.DW_TT_TLOG_FDOC_MG_RECV;
            break;
        case ReportLog.BILL_PAYMENT_TYPE:
            dnetCode = DiagMessage.DW_TT_TLOG_FDOC_MG_XPAY;
            break;
        }
        referenceNumber = rn;
        amount = a;
        fee = f;
    }


    /* toString */
    @Override
	public String toString() {
        return 
            super.toString() + 
            "\n  MoneyGramDetail {" +	
            "\n    referenceNumber           : " + referenceNumber +	
            "\n    serialNumber              : " + serialNumber +
            "\n    currencyCode              : " + currencyCode +
            "\n    hostTime                  : " + String.valueOf(hostTime) +	
            "\n    exchange                  : " + String.valueOf(exchange) +	
            "\n    attempts                  : " + String.valueOf(attempts) +	
            "\n    successFlag               : " + String.valueOf(successFlag) +	
            "\n    accountNumber             : " + accountNumber +
            "\n  } MoneyGramDetail ";	
    }
  
           
    /* getters */
    public String  getReferenceNumber() { return referenceNumber; }
    public String  getHostTime()        { return hostTime;}
    public BigDecimal getExchange()     { return exchange;}
    public byte    getAttempts()        { return attempts;}
    public boolean getSuccessFlag()     { return successFlag;}
    public String  getName()            { return name; } 
    public String  getEmployeeNumber()  { return empNumber; } 
    public String  getSerialNumber()    { return serialNumber; } 
    public String  getAccountNumber()   { return accountNumber; } 
    public String  getAgentID()         { return agentID; }
    public String  getAgentName()       { return agentName; }
    public String  getCurrencyCode()    { return currencyCode; } 
    public BigDecimal getTaxPercentage() { return taxPercentage;}
    
    /* setters*/
    public void setReferenceNumber(final String rn) {referenceNumber = rn; }
    public void setHostTime(final String ht) {hostTime = ht; }
    public void setExchange(final BigDecimal e) { exchange = e; }
    public void setAttempts(final byte a) { attempts = a; }
    public void setSuccessFlag(final boolean sf) {successFlag = sf; }
    public void setName(final String n) {name = n; }
    public void setEmployeeNumber(final String n) {empNumber = n; }
    public void setSerialNumber(final String sn) { serialNumber = sn; }
    public void setAccountNumber(final String an) { accountNumber = an; }
    public void setCurrencyCode(final String code) { currencyCode = code; }
    public void setTaxPercentage(final BigDecimal tp) {taxPercentage = tp; }

    /**
	 * @return Returns the receiveCurrencyCount.
	 */
	public String getReceiveCurrencyCount() {
		return receiveCurrencyCount;
	}
	/**
	 * @param receiveCurrencyCount The receiveCurrencyCount to set.
	 */
	public void setReceiveCurrencyCount(String receiveCurrencyCount) {
		this.receiveCurrencyCount = receiveCurrencyCount;
	}
	
	
    /**
	 * @return Returns the receiveCurrencyCount.
	 */
	public String getReceiveToCardCurrencyCount() {
		return receiveToCardCurrencyCount;
	}
	/**
	 * @param receiveCurrencyCount The receiveCurrencyCount to set.
	 */
	public void setReceiveToCardCurrencyCount(String receiveToCardCurrencyCount) {
		this.receiveToCardCurrencyCount = receiveToCardCurrencyCount;
	}
	
	
	/**
	 * @return Returns the sendCurrencyCount.
	 */
	public String getSendCurrencyCount() {
		return sendCurrencyCount;
	}
	/**
	 * @param sendCurrencyCount The sendCurrencyCount to set.
	 */
	public void setSendCurrencyCount(String sendCurrencyCount) {
		this.sendCurrencyCount = sendCurrencyCount;
	}
	
	/**
	 * @return Returns the sendReversalCurrencyCount.
	 */
	public String getSendReversalCurrencyCount() {
		return sendReversalCurrencyCount;
	}
	/**
	 * @param sendReversalCurrencyCount The sendReversalCurrencyCount to set.
	 */
	public void setSendReversalCurrencyCount(String sendReversalCurrencyCount) {
		this.sendReversalCurrencyCount = sendReversalCurrencyCount;
	}
	
}

