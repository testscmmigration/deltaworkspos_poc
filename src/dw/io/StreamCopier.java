/*
 * StreamCopier.java
 * 
 * $Revision$
 *
 * This code is from Java I/O by Elliotte Rusty Harold.
 * It is literally a textbook example.
 */
package dw.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class StreamCopier {
    
    public static void copy(InputStream in, OutputStream out) 
            throws IOException {
        
        synchronized (in) {
            synchronized (out) {
                byte [] buffer = new byte[256];
                while (true) {
                    int bytesRead = in.read(buffer);
                    if (bytesRead == -1) break;
                    out.write(buffer, 0, bytesRead);
                }
            }
        }       
    }
}
