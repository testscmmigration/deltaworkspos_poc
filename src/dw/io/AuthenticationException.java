package dw.io;

import java.io.IOException;

/**
  * Indicates that a newly created circular file did not match the previously
  * recorded message digest.
  * @author Geoff Atkin
  */
public class AuthenticationException extends IOException {
	private static final long serialVersionUID = 1L;

    public AuthenticationException() {
	super();
    }

    public AuthenticationException(String s) {
	super(s);
    }
}

