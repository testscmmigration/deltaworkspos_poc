package dw.io;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.List;
import java.util.Vector;

import com.moneygram.agentconnect.ComplianceTransactionRequest;
import com.moneygram.agentconnect.ComplianceTransactionResponse;
import com.moneygram.agentconnect.MoneyOrderInfo;
import com.moneygram.agentconnect.MoneyOrderPrintStatusType;
import com.moneygram.agentconnect.MoneyOrderTotalResponse;

import dw.comm.MessageFacade;
import dw.comm.MessageFacadeParam;
import dw.comm.MessageLogic;
import dw.framework.PauseSemaphore;
import dw.framework.WaitToken;
import dw.main.DeltaworksMainPanel;
import dw.profile.UnitProfile;
import dw.utility.Debug;
import dw.utility.Scheduler;
import dw.utility.TimeUtility;

/**
 * Thread for dialy transmits to the AC middleware.
 * 
 * @author Eric Inman (original)
 * @author Geoff Atkin (modifications)
 */
public class Transmitter implements Runnable {

    private CCILog cciLog;
    private boolean keepGoing = true;
    private WaitToken wait = new WaitToken();
    private Object sleepAlarm = new Object();
//    private long cycle = 0;
    private boolean waiting = false; // indicates there are no more items
    private boolean lastTransmissionFailed = false;
    private PauseSemaphore semaphore = null;

    private int backoffCounter = 1; // used to compute sleep time
    private SecureRandom random = null;
    private int sleepTime[];
	private BigInteger batchCount;
	private BigDecimal batchAmount;
	private String batchID;
	private String moAccountNumber;

    /**
     * Creates an transmitter to read from the given cci log and send to
     * AC middleware. Used by DeltaworksMainPanel.
     * @param pauseSemaphore
     * @throws IOException if an exception occurred in itemLogInput
     */
    public Transmitter(PauseSemaphore semaphore) throws IOException {
        this.semaphore = semaphore;
    }

//    /**
//     * Sets the sleep interval. Only useful for testing.
//     * @deprecated No longer relevant, and shouldn't have been 
//     * externally settable in the first place.
//     */
//    protected void setSleepInterval(int sleepInterval) {
//    }

    /**
     * Pauses.
     */
    private void sleep() {
        try {
            synchronized (sleepAlarm) {
                sleepAlarm.wait(computeSleepInterval());
            }
        }
        catch (InterruptedException e) {
        }
    }

    /** 
     * Implements a modified binary exponential backoff algorithm. The
     * number of milliseconds to sleep is randomly chosen over the interval
     * 0 &lt; m &lt; (60) (1000) (2 ^ i) + 1
     * The counter i should be incremented after a successful attempt and
     * decremented after an unsuccesful attempt, within the range 
     * 1 &lt;= i &lt;= 6
     * and it should be reset to 1 when there are no more items.
     *
     * This method does <strong> not </strong> do the incrementing and 
     * decrementing of the backoff counter, although it enforces the valid 
     * range.
     */
    private long computeSleepInterval() {
        long result;
        if (random == null) {
            try {
				random = SecureRandom.getInstance("SHA1PRNG");
			} catch (NoSuchAlgorithmException e) {
				Debug.printException(e);
				Debug.printStackTrace(e);
				return 1L + sleepTime[backoffCounter];
			}
            sleepTime = new int[7];
            for (int i = 1; i <= 6; i++) {
                sleepTime[i] = TimeUtility.MINUTE * (1 << i); // max 3,840,000
            }
        }
        validateBackoffCounter();

        // Add one to insure we don't return 0, because wait(0) has 
        // a special meaning.
        result = 1L + random.nextInt(sleepTime[backoffCounter]);
        if (backoffCounter > 1) {
            Debug.println(
                "computeSleepInterval() backoffCounter = "	
                    + backoffCounter
                    + " result = "	
                    + result);

        }
        return result;

    }

    private void validateBackoffCounter() {
        if (backoffCounter < 1) {
            backoffCounter = 1;
        }
        else if (backoffCounter > 6) {
            backoffCounter = 6;
        }
    }

    /** 
     * Perform one processing cycle. Read items from the CCI log. 
     * 
     * If the error is an instance of MessageError and the error code is 312,
     * then the nth item failed because it was invalid in some way, which 
     * means it will always fail, so we should not send it again, so we 
     * treat the first n items as sent and adjust the log so the next cycle
     * will start with item n + 1. Otherwise we adjust the log so the next
     * cycle s with item n.
     * 
     * If status=true and firstError=0, then the whole batch succeeded. If
     * status=false and firstError=0, then the whole batch failed (prior to
     * the point where firstError gets set), so pretend firstError was 1.
     *
     * Limbo items that are followed by a matching item are not sent; the 
     * matching (non-limbo) item is sent in its place. Limbo items that are 
     * followed by a non-matching item are sent as is. A limbo item at the
     * end of the log is sent if holdLimbo is false, and retained otherwise.
     */
    private synchronized void runACycle(final MessageFacadeParam parameters, final boolean holdLimbo) {      
        Object result = null;
        CircularInputStream cciInputStream = null;
        try { 
            cciLog = CCILog.getInstance();
            cciInputStream = cciLog.getDetailFile().getInputStream();
            while (!(cciLog.isEmpty())) {
                cciInputStream.mark(0);
                final ComplianceTransactionRequest cciItem = cciLog.readFrom(cciInputStream);
                //remove PROPOSED and NOT PRINTTED ITEMS from cci log
                List<MoneyOrderInfo> validMO = getTransmittableMO(cciItem);
                
                //added to protect against remote issuance flag not being set
                //in version prior to 5.0       
                    
                /*for (int i=0; i < validMO.size(); i++) {
                    MoneyOrderInfo40 moi40 = (MoneyOrderInfo40) validMO.get(i);
                    if (moi40.getRemoteIssuanceFlag().equals("")) {
                        moi40.setRemoteIssuanceFlag("false");
                    }
                }*/ 
                cciItem.getMoneyOrder().clear();
                cciItem.getMoneyOrder().addAll(validMO);
                               
                MessageLogic logic = new MessageLogic() {
                    @Override
					public Object run() {
                        return MessageFacade.getInstance().transmitItems(parameters, cciItem);
                    }
                };
                result = MessageFacade.run(parameters, logic, null);
                
                
               if (result instanceof ComplianceTransactionResponse) {
                	List<MoneyOrderInfo> moneyOrders = cciItem.getMoneyOrder();
                	int count = 0; 
                	BigDecimal amount = BigDecimal.ZERO;
                	String moAccount = null;
                	
                	for (MoneyOrderInfo moi40 : moneyOrders) {
                		if (moi40.getPrintStatus().equals(MoneyOrderPrintStatusType.CONFIRMED_PRINTED) || moi40.getPrintStatus().equals(MoneyOrderPrintStatusType.ATTEMPTED)) {
	                		count++;
	                		if (! moi40.isVoidFlag()) {
	                			amount = amount.add(moi40.getItemAmount());
	                		}
	                		moAccount = moi40.getMoAccountNumber();
                		}
                	}
                	batchID = ((ComplianceTransactionResponse) result).getPayload().getValue().getBatchID();
                	batchCount = BigInteger.valueOf(count);
                	batchAmount = amount;
                	moAccountNumber = moAccount;
                	
                	MessageLogic logic2 = new MessageLogic() {
                        @Override
						public Object run() {
                            return MessageFacade.getInstance().transmitMoneyOrderTotals(parameters, batchID, batchCount, batchAmount, moAccountNumber);
                        }
                    };
                    result = MessageFacade.run(parameters, logic2, null);                   

                    if (result instanceof MoneyOrderTotalResponse) {                  
                        cciInputStream.mark(0);
                        cciInputStream.clearToMark();
                        if (cciLog.isEmpty())
                            clearBatchTotalsAndCounts();
                        Scheduler.setNoMOTransmits(false);
                    }
                    else {
                    	failedCCI(cciInputStream);
                    	return;
                    }
                }
                else {
                    failedCCI(cciInputStream);
                    return;
                }
            }
        }
        catch (IOException ioe) { 
            failedCCI(cciInputStream);
            Debug.printStackTrace(ioe);
        }
        waiting = true;
    }

    /* Spin through the cciItem log and remove all PROPOSED and NOT PRINTED
     * money orders.
     */
    private List<MoneyOrderInfo> getTransmittableMO(ComplianceTransactionRequest cciItem) {
        List<MoneyOrderInfo> v = cciItem.getMoneyOrder(); 
        Vector<MoneyOrderInfo> validMO = new Vector<MoneyOrderInfo>();
        for (int i=0; i < v.size(); i++) {
        	MoneyOrderInfo moi40 = v.get(i);
            if ((moi40.getPrintStatus() != MoneyOrderPrintStatusType.PROPOSED) && 
                (moi40.getPrintStatus() != MoneyOrderPrintStatusType.CONFIRMED_NOT_PRINTED)){
                validMO.add(moi40);
            }
        }   
        return validMO;       
    }

    private void failedCCI(CircularInputStream cis) {
        lastTransmissionFailed = true;
        try {
            cis.reset();
        }
        catch (IOException e) {
            Debug.printStackTrace(e);
        }
        return;
    }

    /**
     * Determine if we need to transmit the items to the host this cycle.
     * We don't if we are in dial-up mode and have not reached transmission 
     * limits yet.
     */
    private boolean transmissionNeeded() {
        // don't transmit if CCI log is locked
        try {
            if (CCILog.getInstance().isLocked()) {
                return false;
            }
        }
        catch (IOException e) {
            Debug.printStackTrace(e);
        }
        
        // get the current number of items in the item log
        int[] currentCounts = new int[3];
        currentCounts[1] = UnitProfile.getInstance().getBatchCount(1);
        currentCounts[2] = UnitProfile.getInstance().getBatchCount(2);

        // always transmit in network mode if we have any items
        if (!UnitProfile.getInstance().isDialUpMode()){
            boolean result = false;
            try {
                result = !CCILog.getInstance().isEmpty() || 
                         (currentCounts[1] > 0 || 
                         currentCounts[2] > 0);
            }
            catch (IOException e) {
                Debug.printStackTrace(e);
            }
            return result;
        }

        // check MO and VP transmission batch values...doc seq 1 and 2
        for (int i = 1; i <= 2; i++) {
            BigDecimal currentAmount = UnitProfile.getInstance().getBatchTotal(i);
            int currentCount = currentCounts[i];
            BigDecimal transmissionAmount = UnitProfile.getInstance().getTransmissionAmount(i);
            int transmissionCount = UnitProfile.getInstance().getTransmissionCount(i);
            // check if we have surpassed a count or amount limit for items
            if (currentAmount.compareTo(transmissionAmount) > 0
                || (currentCount > transmissionCount)) {
                return true;
            }
        }
        return false;
    }

    private void clearBatchTotalsAndCounts() {
        UnitProfile.getInstance().clearBatchTotalsAndCounts();
    }

    /**
     * Perform processing cycles until there are no more items to transmit or 
     * there is a transmission failure. Called by DeltaworksMainPanel as part
     * of the manual and daily transmit. The caller must decide whether or not
     * to do a sleep after this call returns.
     * 
     * This method used to take a third parameter displayProgress, which
     * enabled an animation that has been removed.
     * 
     * @param param network connection parameters
     * @param holdLimbo whether or not we should hold up when we have a limbo
     * a limbo item or should we just send it. If false, the semaphore passed
     * in from DMP is ignored. If true, trailing limbo records won't be sent
     * as part of a BPMOM.
     * @return true if ok, or false if the loop stopped because a message failed
     */
    public boolean runCycle(MessageFacadeParam param, boolean holdLimbo) {
        
        try {
            // don't start the cycle until the semaphore frees up
            if (holdLimbo) {
                semaphore.waitUntilFree();
            }

            waiting = false;
            lastTransmissionFailed = false;

            do {
                // delay the next cycle until we are not paused anymore
                if (holdLimbo)
                    semaphore.waitUntilFree();
                runACycle(param, holdLimbo);

                // runACycle sets waiting=true when it runs out of items that
                // are eligible to be sent. It sets lastTransmissionFailed
                // when an item failed to be sent.
            }
            while (!(waiting || lastTransmissionFailed));
            
            return !lastTransmissionFailed;
        }
        finally {
            // DO NOTHING
        }
    }

    /**
     * Attempts to transmit everything, including limbo items that might
     * still have a chance of being matched. Called by DeltaworksMainPanel
     * as a last-ditch attempt to preserve data before clearing the logs,
     * so there should be no sleeping here.
     */
    public boolean attemptTransmit(MessageFacadeParam params) {
    	DeltaworksMainPanel.getMainPanel().moTransmitInProgress = true;
    	boolean b = runCycle(params, false);
    	DeltaworksMainPanel.getMainPanel().moTransmitInProgress = false;
        return b;
    }

    /**
     * Checks the CCI log periodically for messages that can be forwarded to
     * the AC middleware.
     */
    @Override
	public void run() {
        MessageFacadeParam parameters = new MessageFacadeParam(MessageFacadeParam.HIDE_WITHOUT_FOCUS_UNTIL_ERROR);
        //cycle = 0;
        synchronized (wait) {
        	wait.setWaitFlag(true);
            while (keepGoing) {
                boolean xmitNeeded = transmissionNeeded();

                // continue on with items even if check in fails
                if (transmissionNeeded()) {              	
                    // Run the cycle while holding in-limbo items time that 
                    // might be matched with non-in-limbo items.
                	DeltaworksMainPanel.getMainPanel().moTransmitInProgress = true;
                    runCycle(parameters, true);
                    DeltaworksMainPanel.getMainPanel().moTransmitInProgress = false;

                    if (lastTransmissionFailed) {
                        sleep();
                    }
                }

                if (!xmitNeeded) {
                    // we're caught up
                    backoffCounter = 1;
                    sleep();
                }
            }
        	wait.setWaitFlag(false);
        	wait.notifyAll();
        }
    }

    /**
     * Stops operation of the thread. Called by DeltaworksMainPanel
     * as part of the shut down process.
     */
    public void halt() {
        // Signal the forwarder thread to stop iterating
        keepGoing = false;

        // Wake up the forwarder thread if it's sleeping
        synchronized (sleepAlarm) {
            sleepAlarm.notify();
        }

        // Wait for the forwarder thread to terminate so that it is not
        // terminated prematurely by System.exit() in the calling method
        synchronized (wait) {
//    		try {
//    			while (wait.isWaitFlag()) {
//					wait.wait();
//				}
//			} catch (InterruptedException e) {
//				Debug.printException(e);
//	        }
    	}
    }
}

/*
    public static void main(String args[]) {
        try {
            Debug.println(computeSleepInterval()); ++backoffCounter;
            Debug.println(computeSleepInterval()); ++backoffCounter;
            Debug.println(computeSleepInterval()); ++backoffCounter;
            Debug.println(computeSleepInterval()); ++backoffCounter;
            Debug.println(computeSleepInterval()); ++backoffCounter;
            Debug.println(computeSleepInterval()); ++backoffCounter;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
*/
