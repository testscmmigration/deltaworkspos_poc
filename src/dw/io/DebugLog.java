package dw.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;

import dw.comm.MessageFacade;
import dw.comm.MessageFacadeParam;
import dw.i18n.Messages;
import dw.profile.UnitProfile;
import dw.utility.Debug;

/**
*
*   DebugLog contains the code to break up, compress, and transmit one or both
*   trace files, depending on a a flag set in the state file.
*
**/
public class DebugLog {
    private static final int MAXSIZE = 4048 * 16; // This is based on a 16:1 compression ratio
                                                  // to attempt to come up with abouta 4k
                                                  // compressed message size
    private boolean transmitFirstOfTwo = true;    // Used to determine the last message to be transmitted

    // data to be transmitted
    private String debugData;                               // The data to be sent
    private long endTimeStamp = System.currentTimeMillis(); // The time stamp to be sent
    private long sequenceNum = 0;                            // The file number to be sent
    private boolean endOfLog = false;                       // The last file indicator to be sent
    
    private State state = null;                             // An instance of the state file
    private static volatile DebugLog instance;
    private static boolean transmitting = false;
    
    private DebugLog() {
        
    }
    

    /**
    *
    *   Returns an instance of the DebugLog. 
    *   @return An instance of the DebugLog.
    *   @author sws
    **/
    public static DebugLog getInstance() {
        if (instance == null) {
            instance = new DebugLog();
        }
        return instance;
    }
        
        
    /**
    *
    *   Sends the Trace files in chunks using DebugMsg until the Trace files have been
    *   sent. 
    *   @param A MessageFacadeParam providing connection information.
    *   @return A boolean indicating the success of the send. 
    *   @author sws
    **/
    public synchronized boolean transmit(MessageFacadeParam parameters) {
        if (transmitting) {
            return false;
        }
        // do not transmit debug logs if in sub agent mode.
        if(UnitProfile.getInstance().isSubAgent()){
            Debug.println("in sub agent mode - Do not transmit debug logs");
            return true;
        }
            
        boolean returnVal = true;
        try {
            endTimeStamp = System.currentTimeMillis();
            transmitting = true;
            transmitFirstOfTwo = true;
            sequenceNum = 0;
            endOfLog = false;
            Debug.beginTransmission();
            if ((state = State.getInstance()) != null) {
                // Check if the second trace file must be transmitted and set flag appropriately
                try {
                    if (state.readByte(State.TRACE2_CLEARED) != 0) {
                        transmitFirstOfTwo = false;
                    }
                }
                catch (IOException ioe) {
                    Debug.println("Sending both trace files by default.");	
                }

                try {
                    if (transmitFirstOfTwo) {
                        // transmit Trace2.txt before rotating
                        //Debug.println("Sending the exsiting Trace2.txt...");	
                        sendTrace2(parameters);
                        //Debug.println("Sent the existing Trace2.txt");	
                    }
                    if (Debug.rotate()) {
                        //Debug.println("Sending the rotated Trace2.txt...");	
                        sendTrace2(parameters);
                        //Debug.println("Sent the rotated Trace2.txt");	
                    }
                }
                catch (IOException ioe) {
                    Debug.println("An IO Exception encountered while trying to transmit trace files.");	
                    returnVal = false;
                }
            }
            Debug.finishTransmission();
        }
        finally {
            transmitting = false;
        }
        return returnVal;
    }

    
    /**
    *
    *   Sends chunks of Trace2.txt until the entire message has been sent.
    *   @param parameters Connection information.
    *   @return A String. This should be close to 64k.
    *   @author sws
    *
    **/
    private void sendTrace2(MessageFacadeParam parameters) throws IOException {
        BufferedReader input = null;
        try {
            input = new BufferedReader(new FileReader(Debug.TRACE2));
        }
        catch (FileNotFoundException fnfe) {
            Debug.println("Could not read " + Debug.TRACE2);	
            Debug.printStackTrace(fnfe);
            return;
        }
        while (input.ready() && (debugData = getBody(input)) != null && debugData.length() != 0) {
            // Send message with debugData, length, endTimeStamp, sequenceNum, and endOfLog
            //Debug.println("sending: ");	
            //Debug.println("    debugData: <<" + debugData + "\n>>");	 
            //Debug.println("    debugData.length(): " + debugData.length());	
            //Debug.println("    endTimeStamp: " + endTimeStamp);	
            //Debug.println("    sequenceNum: " + sequenceNum);	
            //Debug.println("    endOfLog: " + endOfLog);	

            if (!MessageFacade.getInstance().transmitDebugLog(parameters, debugData, debugData.length(), 
                                                              endTimeStamp, sequenceNum, endOfLog)) {
                throw new IOException(Messages.getString("DebugLog.1926")); 
            }
            sequenceNum++;
        }
        transmitFirstOfTwo = false;
        input.close();
        
        traceSent();
    }


    /**
    *
    *   Reads in lines from an input file that will result in compressed bytes that
    *   are close to MAXSIZE.
    *   @return boolean indicating rotation success.
    *   @author sws
    *
    **/
    private boolean traceSent() {
        boolean success = false;
        try {
            state.writeByte(1,State.TRACE2_CLEARED);
            success = true;
            //Debug.println("Set TRACE2_CLEARED state flag");	
        }
        catch (IOException ioe) {
            Debug.println("Could not set TRACE2_CLEARED flag.");	
        }
        
        return success;
    }

    
    /**
    *
    *   Reads in lines from an input file that will result in compressed bytes that
    *   are close to MAXSIZE.
    *   @param input Information to send.
    *   @return A String. This will be no longer than 64k.
    *   @author sws
    *
    **/
    private String getBody(BufferedReader input) throws IOException {

        int bytesRead = 0;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        //HighCompressionGZIPOutputStream zipper = new HighCompressionGZIPOutputStream(baos);
        //OutputStreamWriter osw = new OutputStreamWriter(zipper);
        OutputStreamWriter osw = new OutputStreamWriter(baos);
        BufferedWriter bw = new BufferedWriter(osw);

        String line = null;
        String s = null;

        input.mark(MAXSIZE);
        s = input.readLine();
        if (s != null)
        	line = Debug.decryptTextLine(s).toString();

       // while ((line = Debug.decryptTextLine(input.readLine()).toString()) != null) {
        while (line != null) {
            if ((bytesRead + line.length()) > MAXSIZE) {
                input.reset();
                break;
            }
            else {
                input.mark(MAXSIZE);
            	//line = line + "\n";
                bytesRead += (line.length() + 1);
                bw.write(line, 0, line.length());
                bw.newLine();
            }
            s = input.readLine();
            if (s != null)
            	line = Debug.decryptTextLine(s).toString();
            else
            	line = null;            
        }

        bw.close();

        if (line == null && !transmitFirstOfTwo) {
            endOfLog = true;
        }

        return baos.toString();
    }
}
