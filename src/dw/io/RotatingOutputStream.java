//////////////////////////////////////////////////
//
//    RotatingOutputStream.java
//
//    Copyright 2001, MoneyGram
//
//    March 19, 2001 - Created, Rob Morgan
//
//////////////////////////////////////////////////

package dw.io;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

public class RotatingOutputStream extends OutputStream {

    private String filename1, filename2;

    private File file1, file2;

    private FileOutputStream output1;

    private PrintStream ps = null;

    // Half a megabyte. Hardcoded for now, we can always add a setter later.
    private int maxLength = 524288;
    private boolean transmissionInProgress = false;

    /**
     * Constructs a rotating output stream that writes to filename1 until it
     * is longer than maxLength. It then moves the data to filename2,
     * empties out filename1, and starts writing filename1 from scratch.
     */
    public RotatingOutputStream(String filename1, String filename2)
            throws IOException  {

        boolean appendFile1 = false;

        this.filename1 = filename1;
        this.filename2 = filename2;

        file1 = new File(filename1);

        //If the file exists and contains data append to it, otherwise
        // overwrite the file.
        if (file1.exists())
            if (file1.length() > 0)
                appendFile1 = true;

        output1 = new FileOutputStream(filename1,appendFile1);
    }

    /**
     *  Write a single byte to file1. If file1 is full and a transmission of
     *  the files is not taking place, rotate the files.
     */
    @Override
	public void write(int b) throws IOException {
        if (file1.length() > maxLength && !transmissionInProgress)
            rotate();

        output1.write(b);
    }

    /**
     * Delete file2 if it exists. Rename file1 to file2.
     * Recreate or append to file1 depending on the results
     * of the delete and rename steps.
     *
     * Changed from private to public and from returning void to
     * returning a boolean. These changes are necessary to inform
     * the class that transmits the trace files to the host to know
     * whether the second trace file contains new information and that
     * the first trace file was cleared. sws
     */
//    private void rotate() throws IOException
    public boolean rotate() throws IOException
    {
        output1.close();

        file2 = new File(filename2);

        // If file1 and file2 exist delete and rename. If only
        // file one exist just rename.
        if (file1.exists()) {
            if(file2.exists()) {
                if (!file2.delete()) {
                    // If you cannot delete the original file, open
                    // the original file and append to it.
                    output1 = new FileOutputStream(filename1, true);
                    return false;
                }
            }
            if (!file1.renameTo(file2)) {
                // If you cannot rename the original file, open
                // the original file and append to it.
                output1 = new FileOutputStream(filename1, true);
                return false;
            }
        }
        output1 = new FileOutputStream(file1);
        // The following was added to indicate that the second trace file needs
        // to be transmitted. This addition makes this class useful only for swapping
        // the trace files. sws
        State state = null;
        if ((state = State.getInstance()) != null) {
            state.writeByte(0,State.TRACE2_CLEARED);
        }
        return true;
    }


    /**
     * Return a new print stream based on this rotating output stream.
     * The caller, probably DeltaworksMainPanel, can use the result in
     * a call to System.setOut().
     */
    public PrintStream getPrintStream() {
        if (ps == null)
            ps = new PrintStream(new LineOutputStream(new BufferedOutputStream(this)),
                    true);

        return ps;
    }


    /**
     * Disables rotations of the debug output files.
     * @author sws
     */
    public void beginTransmission() {
        transmissionInProgress = true;
    }


    /**
     * Enables rotations of the debug output files.
     * @author sws
     */
    public void finishTransmission() {
        transmissionInProgress = false;
    }
}

