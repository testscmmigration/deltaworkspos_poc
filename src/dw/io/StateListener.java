package dw.io;
/**
  * This is what I'm thinking in terms of for saving the persistent state
  * of things like circular files, the dispenser, and such. The idea is 
  * that those classes will have a method addStateListener(StateListener). 
  * Whenever such a class makes a change to its persistent state, it will 
  * be expected to call stateChanged().
  * 
  * Implementations of StateListener, which are likely to be anonymous
  * inner classes in the startup routine, will write the given byte 
  * array to a file.
  */
public interface StateListener {
    public void stateChanged(byte[] newState, int off, int len) 
	throws java.io.IOException;
}

