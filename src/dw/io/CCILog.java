package dw.io;

import java.io.DataInputStream;
import java.io.IOException;

import com.moneygram.agentconnect.ComplianceTransactionRequest;
import com.moneygram.agentconnect.ComplianceTransactionRequestType;

import dw.io.report.UnifiedReportDetail;
import dw.utility.Debug;
import dw.utility.ErrorManager;

/**
 * Records Customer Compliance data and Money Order Item Data to be 
 * used in message with RTS.
 *
 * @author Patrick Ottman (original version)
 */
public class CCILog {

    private static CCILog instance;
    private CircularFile detail;

    /** Private constructor. Callers should use getInstance() instead. */
    private CCILog(CircularFile detail) {
        this.detail = detail;
    }

    /**
     * Returns the singleton instance. If it hasn't been created yet,
     * create it now using the State class to get the circular files.
     */
    public static CCILog getInstance() throws IOException {
        if (instance == null) {
            Debug.println("creating CCI Log instance");	
            State state = State.getInstance();
            instance = new CCILog(state.createCCIFile());
        }
        return instance;
    }

    /**
     * Clears the cci log
     */
    public void clear() throws IOException {
        detail.clear();
    }

    /** Return the detail circular file. The caller is expected to
      * get the input stream.
      */
    public CircularFile getDetailFile() {
        return detail;
    }
    
    public boolean isEmpty() {
        return detail.isEmpty();
    }

    public boolean isLocked() {
        return detail.isLocked();
    }

    /**
     * Returns true if the detail log has less than N bytes of free space.
     * Currently, N is 20 kilobytes.
     */
    public boolean isNearlyFull() {
	return ( detail.freeSpace() < 20000);
    }
    
    /** Writes a detail record to the detail file. */
    private void logDetail(UnifiedReportDetail record) throws IOException {
        record.writeTo(detail);
        detail.flush();
    }

    /**
     * Writes a Compliance Transaction Request Info record to the cci log file.
     */
    public void write(ComplianceTransactionRequest ctri){
        //Debug.println("CCILog.write(ctri) ctri = " + ctri.toString());
        UnifiedReportDetail record = new UnifiedReportDetail(ctri);
        
        try {
            Debug.println("CCILOG " + record.toString());            
        }
        catch (Throwable t) {
            Debug.printStackTrace(t);
        }

        try {
            logDetail(record);
            if(ctri.getRequestType().equals(ComplianceTransactionRequestType.STANDALONE)) {
                // increment persistent SAR counter
                State.getInstance().incrementSARCounter();
            }
        }
        catch (IOException e) {
            ErrorManager.writeStackTrace("Failed in CCILog.logComplianceTransactionRequestInfo, ", e); 
        }
    }

    /**
     * Read will read a record from the CCILog and construct a ComplianceTransactionRequestInfo.
     * 
     * @return ComplianceTransactionRequestInfo
     */
    public ComplianceTransactionRequest readFrom(CircularInputStream cis) throws IOException   {

    	ComplianceTransactionRequest ctri = null;
        UnifiedReportDetail urd = new UnifiedReportDetail();
        urd.readFrom(new DataInputStream(cis));
        ctri = urd.ctriDetail();
        return ctri;
    }

/*
    public static void main(String[] args) {
        try {
            CCILog cciLog = CCILog.getInstance();
            Debug.println("start");	
            Debug.println("start is "+cciLog.getDetailFile().getStart());	
            Debug.println("  end is "+cciLog.getDetailFile().getEnd());	

            ComplianceTransactionRequestInfo ctri = new ComplianceTransactionRequestInfo();
            ctri.setRequestType(8);
            ctri.setMgReferenceNumber("12345678");	
            CustomerComplianceInfo cci = new CustomerComplianceInfo();
            cci.setTypeCode(1);
            cci.setKey("1");	
            cci.setUserName("George Carlon");
            cci.setComments("I am the Prez");
            cci.setCategory(1);
            cci.setOtherCategory("Other Category");
            cci.setCharacter(1);
            cci.setLastName("Ottman");
            cci.setFirstName("Patrick");
            cci.setMiddleInitial("R");	
            cci.setAddress("1018 Shumway Street");
            cci.setCity("Shakopee");
            cci.setState("MN");	
            cci.setZip("55379");	
            cci.setCountry("USA");	
            cci.setPhotoIDType("Driver License");
            cci.setPhotoIDNumber("1234567890");	
            cci.setPhotoIDState("MN");	
            cci.setPhotoIDCountry("USA");	
            cci.setDateOfBirth("08/17/1954");	
            cci.setPhoneNumber("952-591-3283");	
            cci.setOccupation("Programmer");
            cci.setLegalIDType("Passport");
            cci.setLegalIDNumber("213123123");	
            cci.setThirdPartyDBA("dba");
            cci.setThirdPartyLastName("Smith");
            cci.setThirdPartyFirstName("Scotty");
            cci.setThirdPartyMiddleInitial("E");	
            cci.setThirdPartyAddress("here");
            cci.setThirdPartyCity("there");
            ctri.setCci(cci);
            
            BigDecimal amt = new BigDecimal("100.00");	
            
            Vector moVector = new Vector();
            for(int i = 50; i > 45; i--){
                MoneyOrderInfo40 mo = new MoneyOrderInfo40();
                mo.setDocumentType("1");	
                mo.setPeriodNumber("7");	
                mo.setDateTimePrinted(new Date());            
            

                mo.setEmployeeID(String.valueOf(i));
                mo.setPeriodNumber("4");	
                mo.setItemAmount(amt.toString());
                amt = amt.add(new BigDecimal("10.00"));	
                mo.setItemFee("11.33");	
                mo.setDocumentSequenceNbr("12345");	
                mo.setSerialNumber("123456789");	
                mo.setVendorNumber("12344444");	
                mo.setPrintStatus(0);
                mo.setVoidFlag(true);
                mo.setVoidReasonCode("3");	
                mo.setTaxID("456456456");	
                mo.setDiscountAmount(".10");	
                mo.setDiscountPercentage("10");	
                mo.setSaleIssuanceTag("999999999");	
                mo.setDispenserID("1");	
                mo.setVoidFlag(true);
                moVector.add(mo);
            }
            ctri.setMoneyOrders(moVector);

            instance.getDetailFile().lock();
            Debug.println("before write");	
            Debug.println("start is "+cciLog.getDetailFile().getStart());	
            Debug.println("  end is "+cciLog.getDetailFile().getEnd());	
            cciLog.write(ctri);
            Debug.println("after 1st write");	
            Debug.println("start is "+cciLog.getDetailFile().getStart());	
            Debug.println("  end is "+cciLog.getDetailFile().getEnd());	
            
            // make change to print status in movector
            moVector = ctri.getMoneyOrders();
            for(int i=0; i<moVector.size(); i++){
                MoneyOrderInfo40 mo = (MoneyOrderInfo40) moVector.get(i);
                mo.setPrintStatus(1);
                moVector.setElementAt(mo, i);
            }
            ctri.setMoneyOrders(moVector);
            
            instance.getDetailFile().rewind();
            cciLog.write(ctri);
            Debug.println("after 2nd write");	
            Debug.println("start is "+cciLog.getDetailFile().getStart());	
            Debug.println("  end is "+cciLog.getDetailFile().getEnd());	

            // make change to print status in movector
            moVector = ctri.getMoneyOrders();
            for(int i=0; i<moVector.size(); i++){
                MoneyOrderInfo40 mo = (MoneyOrderInfo40) moVector.get(i);
                mo.setPrintStatus(2);
                moVector.setElementAt(mo, i);
            }
            ctri.setMoneyOrders(moVector);
            
            instance.getDetailFile().rewind();
            cciLog.write(ctri);
            Debug.println("after 3rd write");	
            Debug.println("start is "+cciLog.getDetailFile().getStart());	
            Debug.println("  end is "+cciLog.getDetailFile().getEnd());	

            Debug.println("before read");	
            Debug.println("start is "+cciLog.getDetailFile().getStart());	
            Debug.println("  end is "+cciLog.getDetailFile().getEnd());	
            ctri = cciLog.read();
            Debug.println("after read");	
            Debug.println("start is "+cciLog.getDetailFile().getStart());	
            Debug.println("  end is "+cciLog.getDetailFile().getEnd());	
            instance.getDetailFile().unLock();
            Debug.println(ctri.toString());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
        System.exit(0);
	}
*/	
}


