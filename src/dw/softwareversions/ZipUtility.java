package dw.softwareversions;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.RandomAccessFile;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import dw.utility.Debug;

/**
 * ZipUtility contains some utility functions for splitting/joining files into
 *   pieces and zipping/unzipping files.
 * When a file is split, the first component file (<file>.0 file) will contain
 *   [<int - # of components> <long - CRC32 checksum>] at the beginning to
 *   ensure the correct reconstition of the file later.
 * @author Rob Campbell
 */
public class ZipUtility {

    /**
     * Split a file into n pieces, each of which being at most componentSize.The
     *   resulting files will be called fileName.0, fileName.1, etc. The first
     *   component file contain the total number of components and the CRC32
     *   checksum of the file.
     * @param fileName the name of the file to be split into pieces
     * @param componentSize the size of the components to be created
     * @param deleteOriginalFile whether or not to delete the file once it is
     *   successfully split up into its components.
     * @throws IOException if the file doesn't exist
     */
    public static void splitFile(String fileName, int componentSize,
                                 boolean deleteOriginalFile)
                                                    throws IOException {
        try {
			RandomAccessFile raf = new RandomAccessFile(fileName, "r"); 
			int totalRead = 0;
			int fileLength = (int) raf.length();
			int componentNum = 0;
			while (totalRead < fileLength) {
			    if ((fileLength - totalRead) < componentSize)
			        componentSize = fileLength - totalRead;
			    byte[] component = new byte[componentSize];
			    totalRead += raf.read(component);
			    // create a new file and write out this chunk to it.
			    File componentFile = new File(fileName + "." + componentNum);	
			    if (componentFile.exists()) {
			        boolean b = componentFile.delete();
			        if (! b) {
			        	Debug.println("File " + componentFile.getName() + " could not be deleted");
			        }
			    }
			    boolean f = componentFile.createNewFile();
			    Debug.println("File::: " + componentFile.getName() + " ");
			    if (! f) {
			    	Debug.println("File " + componentFile.getName() + " already existed");
			    }
			    FileOutputStream fos = new FileOutputStream(componentFile);
			    DataOutputStream dos = null;
			    // prepend the # of components and CRC32 if we are on the first file
			    if (componentNum == 0) {
			        BigDecimal div = new BigDecimal(
			                          (float) fileLength / (float) componentSize);
			        div = div.setScale(0, BigDecimal.ROUND_UP);
			        int numComponents = div.intValue();
			        long checksum = Long.parseLong(
			                               ChecksumUtility.getChecksum(fileName));
			        dos = new DataOutputStream(fos);
			        dos.writeInt(numComponents);
			        dos.writeLong(checksum);
			       
			    }

			    fos.write(component);
			    if(dos!=null){
			    	 dos.close();
			    }
			    fos.close();
			    componentNum++;
			}
			raf.close();
			// get rid of the original if necessary
			if (deleteOriginalFile) {
			    File file = new File(fileName);
			    boolean b = file.delete();
			    if (! b) {
			    	Debug.println("File " + file.getName() + " could not be deleted");
			    }
			}
		} catch (Exception e) {
			Debug.printException(e);
			Debug.printStackTrace(e);
		}
    }
    /**
     * Join together all the pieces of the given file. When reading the first
     *   piece, grab the number of components and CRC32 of the whole file from
     *   the beginning of the piece.
     * @param fileName the name of the file to be joined from its pieces.
     * @param logStream the print stream to log progress to.
     * @throws IOException if the file could not be successfully recreated.
     */
	public static void joinFile(final String fileName, PrintStream logStream) throws IOException {
		joinFile(fileName, logStream, false);
	}
    /**
     * Join together all the pieces of the given file. When reading the first
     *   piece, grab the number of components and CRC32 of the whole file from
     *   the beginning of the piece.
     * @param fileName the name of the file to be joined from its pieces.
     * @param logStream the print stream to log progress to.
     * @throws IOException if the file could not be successfully recreated.
     */
	public static void joinFile(final String fileName, PrintStream logStream, boolean deleteOriginalFile)
			throws IOException {
		String error = ""; 
		logStream.println("Joining file : " + fileName + " from components");  
		File mainFile = new File(fileName);

		// custom file filter to grab files that are components of the named
		// file
		FileFilter filter = new FileFilter() {
			@Override
			public boolean accept(File pathName) {
				String path = pathName.toString().replace('\\', '/');
				String fName = pathName.getName();
				int cnt = fileName.lastIndexOf("/");
				String flName = null;
				if (cnt > 0) {
					flName = fileName.substring(cnt+1, fileName.length());
				}
				if (fName.equals(flName)) {
					return false;
				}
				return path.indexOf(fileName) >= 0;
			}
		};

		// custom comparator to sort file extensions by number
		Comparator<File> comparator = new Comparator<File>() {
			@Override
			public int compare(File o1, File o2) {
				// get the extension number of the file
				String o1s = o1.toString();
				String o2s = o2.toString();
				Integer extension1 = Integer.valueOf(o1s.substring(o1s.lastIndexOf(".") + 1)); 
				Integer extension2 = Integer.valueOf(o2s.substring(o2s.lastIndexOf(".") + 1)); 
				return extension1.compareTo(extension2);
			}

			/*public boolean equals(Object o) {
				return false;
			}*/
		};

		long targetChecksum = 0;
		int targetNumComponents = 0;
		File dir = (new File(mainFile.getAbsolutePath())).getParentFile();
		File[] components = dir.listFiles(filter);
		// make sure the pieces are sorted correctly by number
		if (components == null || components.length == 0) {
			error = "Could not join file " + fileName + 
					". No components present."; 
			logStream.println(error);
			throw new IOException(error);
		}

		// sort all the pieces based on their numeric extensions
		Arrays.sort(components, comparator);

		boolean f = mainFile.createNewFile();
        if (! f) {
        	Debug.println("File " + mainFile.getName() + " already existed");
        }
		FileOutputStream fos = new FileOutputStream(mainFile);
		for (int i = 0; i < components.length; i++) {
			// read in the file chunk
			byte[] contents = new byte[(int) components[i].length()];
			FileInputStream fis = new FileInputStream(components[i]);

			// grab the # of components and checksum off the first file
			if (i == 0) {
				try {
					DataInputStream dis = new DataInputStream(fis);
					targetNumComponents = dis.readInt();
					targetChecksum = dis.readLong();
					logStream.println("File : " + mainFile + " should have " + targetNumComponents + " components.");   
					logStream.println("   and there are " + components.length + " components.");  
					/*
					 * Close dis to make vericode happy, but  it also closes fis which we still need.
					 * Open fis again, but strip out the 12 byte header that has the number of components 
					 * and the checksum.
					 */
					dis.close();
					fis = new FileInputStream(components[i]);
					fis.read(contents, 0, 12);
				} catch (IOException e) {
					error = "Could not join file " + fileName + 
							". First component corrupt."; 
					logStream.println(error);
					throw new IOException(error);
				}
				contents = new byte[contents.length - 12];
				// do we have all the pieces? If not, abort the process...
				if (components.length < targetNumComponents) {
					fis.close();
					fos.close();
					boolean b = mainFile.delete();
			        if (! b) {
			        	Debug.println("File " + mainFile.getName() + " could not be deleted");
			        }
					error = "Could not join file " + fileName + 
							". File components missing."; 
					logStream.println(error);
					throw new IOException(error);
				}
			}

			fis.read(contents);
			fos.write(contents);
			fos.flush();
			fis.close();
		}
		fos.close();
		// all done, make sure the new file has the correct checksum...
		long newChecksum = Long.parseLong(ChecksumUtility.getChecksum(fileName));
		if (newChecksum != targetChecksum) {
			boolean b = mainFile.delete();
	        if (! b) {
	        	Debug.println("File " + mainFile.getName() + " could not be deleted");
	        }
			error = "Could not join file " + fileName + 
					". Checksum incorrect."; 
			logStream.println(error);
			throw new IOException(error);
		} else if (deleteOriginalFile) {
			for (int i = 0; i < components.length; i++) {
				boolean b = components[i].delete();
		        if (! b) {
		        	Debug.println("File " + components[i].getName() + " could not be deleted");
		        }
			}
		}
	}

    public static void unzipTest(String fileName) {
      try {
        byte[] buffer = new byte[256];
        ZipFile zf = new ZipFile(fileName);
        Enumeration<? extends ZipEntry> e = zf.entries();
        while (e.hasMoreElements()) {
          ZipEntry ze = e.nextElement();
          Debug.println("Unzipping " + ze.getName());	
          FileOutputStream fout = new FileOutputStream(ze.getName());
          InputStream in = zf.getInputStream(ze);
          while (true) {
            int bytesRead = in.read(buffer);
            if (bytesRead == -1)
              break;
            fout.write(buffer, 0, bytesRead);
          }
          in.close();
          fout.close();
          zf.close();
        }
      }
      catch (IOException e) {
        Debug.println(e.toString());
        Debug.printStackTrace(e);
      }
    }

    /**
     * Unzip a compressed file.
     */
    public static void unzipFile(String zipFileName) throws IOException {
        ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFileName));
        ZipEntry entry = null;
        while ((entry = zis.getNextEntry()) != null) {
            Debug.println("Unzipping " + entry.getName());	
            Debug.println("Sizes : " + entry.getCompressedSize() + "," + entry.getSize());	 
            File file = new File(entry.getName());
            if (file.exists()) {
                boolean b = file.delete();
                if (! b) {
                	Debug.println("File " + file.getName() + " could not be deleted");
                }
            }
            if (entry.isDirectory()) {
                boolean b = file.mkdirs();
                if (! b) {
                	Debug.println("Directory " + file.getName() + " could not be created");
                }
                continue;
            } else {
                boolean f = file.createNewFile();
                if (! f) {
                	Debug.println("File " + file.getName() + " already existed");
                }
            }

            FileOutputStream fos = new FileOutputStream(file);
            byte[] contents = new byte[(int) entry.getSize()];
            zis.read(contents);
            fos.write(contents);
            fos.close();
        }
        zis.close();
    }

    /**
     * Zip the contents of this directory and its subdirectories.
     */
    public static void createZipFile(String zipFileName, ZipOutputStream zos,
                                               File dir) throws IOException {
        if (zos == null) {
            File zipFile = new File(zipFileName);
            if (zipFile.exists()) {
                boolean b = zipFile.delete();
                if (! b) {
                	Debug.println("File " + zipFile.getName() + " could not be deleted");
                }
            }
            zos = new ZipOutputStream(new FileOutputStream(zipFile));
        }

        File[] dirContents = dir.listFiles();
        Debug.println("Adding zip entry for : " + dir.getName());	
        if (dirContents != null) {
	        for (int i = 0; i < dirContents.length; i++) {
	            File file = dirContents[i];
	            if (file.isDirectory())
	                createZipFile(zipFileName, zos, file);
	            FileInputStream fis = new FileInputStream(file);
	            byte[] fileContents = new byte[(int) file.length()];
	            fis.read(fileContents);
	            fis.close();
	            ZipEntry entry = new ZipEntry(file.getPath());
	            zos.putNextEntry(entry);
	            zos.write(fileContents);
	        }
        }
        zos.close();
    }
}
