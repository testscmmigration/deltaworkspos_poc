package dw.softwareversions;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dw.framework.PauseSemaphore;
import dw.i18n.Messages;
import dw.utility.Debug;
import dw.utility.ErrorManager;
import dw.utility.ExtraDebug;
import dw.utility.TimeUtility;

/**
 * Contains information about a software version manifest. The manifest contains
 *   VersionManifestEntries, a version number, a manifest file size, and a URL
 *   for the manifest file. The manifest also knows how to save itself to a file
 *   and load itself from a file (or URL).
 * @author Rob Campbell
 */
public class VersionManifest extends VersionManifestEntry {
	private static Pattern VERSION_PATTERN = Pattern.compile("\\d\\d\\d\\d\\d\\d");
	private static Pattern JRE_PATTERN = Pattern.compile("jre((\\d{3})|(\\d{4}))z");
	private Map<String, VersionManifestEntry> manifestEntries = new TreeMap<String, VersionManifestEntry>(); // of
	private List<String> manifestSuccessEntries = new ArrayList<String>();
	private String versionNumber;
	private String type;
	private static int fileDownloadCounter = 0;

	public VersionManifest() {
	}


    /**
     * Create a manifest given a manifest URL. Split off the file name and
     *   server location from the given URL. Then load the contents of the
     *   manifest from this URL. 
     */
    public VersionManifest(String baseUrl, String location, long fileSize, 
            StringBuffer resultBuffer) throws IOException {
    	this(baseUrl,location,fileSize, resultBuffer,null,false);
    }
    
    public VersionManifest(String baseUrl, String location, long fileSize, 
            StringBuffer resultBuffer,String type,boolean skip) throws IOException {

        serverLocation = location.substring(0, location.lastIndexOf('/'));
        fileName = location.substring(location.lastIndexOf('/') + 1);
        this.fileSize = fileSize;
        this.type=type;
        isDependency = true;
        this.skip=skip;
        URL url = new URL(getRemoteFileUrl(baseUrl));
        byte[] contents = getFileContents(url, 100, resultBuffer);
        if (contents == null) {
        	Debug.println("Could not load file contents:"+ url.getFile());
            throw new IOException("Could not load file contents."); 
        }
        ByteArrayInputStream bais = null;
        InputStreamReader isr = null;
        BufferedReader reader = null;
        try {
	        bais = new ByteArrayInputStream(contents);
	        isr = new InputStreamReader(bais);
	        reader = new BufferedReader(isr);
	        load(baseUrl, reader, false, resultBuffer);
	        save();
        } finally {
        	if (reader != null) {
        		try {
        			reader.close();
        		} catch (Exception e) {
        			Debug.printException(e);
        		}
        	}
        	if (isr != null) {
        		try {
        			isr.close();
        		} catch (Exception e) {
        			Debug.printException(e);
        		}
        	}
        	if (bais != null) {
        		try {
        			bais.close();
        		} catch (Exception e) {
        			Debug.printException(e);
        		}
        	}
        }
    }

    /**
     * Create a manifest given a specific directory. Load the contents from the
     *   <directory>/manifest.dat file.
     */
    public VersionManifest(String directory) throws IOException {
        fileName = "manifest.dat"; 
        posLocation = directory;
        load();
    }
    public VersionManifest(String directory,String manifestName,String type,boolean doLoad) throws IOException {
        fileName = manifestName; 
        posLocation = directory;
        this.type=type;
        if(doLoad){
            load();
        }
    }
    

    @Override
	public void print() {
		Debug.println("VersionManifest for Version " + versionNumber + "{");  
		Debug.println("  super {" + toString() + "}");  
		Debug.println("Entries : "); 
		for (VersionManifestEntry entry : manifestEntries.values()) {
			entry.print();
		}
		Debug.println("} Manifest."); 
	}

    /**
     * Go through the manifest and check for any component files. If a file is
     *   a component, remove it from the manifest if its target file is already
     *   present and valid. This will remove the unnecessary downloading of
     *   components for files that already have been recreated.
     */
	private void removeUnnecessaryComponents() {
		List<String> deletions = new ArrayList<String>();
		for (VersionManifestEntry entry : manifestEntries.values()) {
			if (entry.isComponentFile()) {
				String targetFileName = entry.getLocalFilePath().substring(0,
						entry.getLocalFilePath().lastIndexOf(".")); 
				// is the target file already present and valid?
				if (manifestEntries.containsKey(targetFileName)) {
					VersionManifestEntry target =  manifestEntries.get(targetFileName);
					// mark this entry for deletion

					if (manifestSuccessEntries != null && manifestSuccessEntries.contains(targetFileName)) {
						deletions.add(entry.getLocalFilePath());
					} else if (target.getStatus(new StringBuffer()) == VersionManifestEntry.READY) {
						deletions.add(entry.getLocalFilePath());
						manifestSuccessEntries.add(targetFileName);
					}
				}
			}
		}
		// delete all the unnecessary component entries
		for (String comp : deletions) {
			manifestEntries.remove(comp);
		}
	}

	  

    /**
     * Download the contents described in this manifest. For any entry that is
     *   itself a dependency (manifest), download the contents of that manifest
     *   also. When all files are downloaded, try to join together the components
     *   of any file that had been split for download.
     * @param logStream the PrintStream for logging progress and status.
     * @param semaphore a PauseSemaphore to check during download.
     * @param result StringBuffer to store final results of download in.
     * @return whether or not the download was successful.
     * @throws InterruptedException 
     */
	
	// Note: This method must override the download method in the super class VersionManifest.
	// Although there are unused variables in this method's signatures, removal of them will
	// cause the software upgrade process to stop working.
	
	@Override
	public int download(PrintStream logStream, StringBuffer result, PauseSemaphore semaphore, String serverName, int fileCountlimit, String downloadType, boolean skipDownload, boolean deleteOriginalFile) throws IOException, InterruptedException {
		
		// first, get rid of any component entries that reference a file we
		// already have.
		removeUnnecessaryComponents();
		boolean manifestSuccess = true;
		boolean downloadPaused = false;
		logStream.println("Started version " + getVersionNumber() 
				+ " download at " + new Date(TimeUtility.currentTimeMillis())); 

		// Determine number of jre chunk files in the manifest.
		
		int numberEntries = 0;
		for (VersionManifestEntry entry : manifestEntries.values()) {
			if (entry.getFileName().contains(".zip.")) {
				numberEntries++;
			}
		}

		Matcher m1 = VERSION_PATTERN.matcher(getVersionNumber().trim()); 
		Matcher m2 = JRE_PATTERN.matcher(getVersionNumber().trim());
		if ((!m1.matches()) && (!m2.matches())) {
			Debug.println("SOFTWARE UPDATE FAILED: Regualar Expression Mismatch - Version Number = "
					+ getVersionNumber().trim());
			manifestSuccess = false;
		} else {

			// go through all the entries and download them
			int i = 0;
			int c = 0;

			ErrorManager.updateProgressBarProgress(i);
			for (VersionManifestEntry entry:  manifestEntries.values() ) {
				if (downloadType == null || downloadType.length() <= 0) {
					if (TYPE_PRE_REQUISITE.equals(type) || TYPE_POST_UPGRADE.equals(type)) {
						continue;
					}
				}

				c++;
				StringBuffer sb = new StringBuffer();
				sb.append(Messages.getString("VersionManifest.2043"));
				sb.append(versionNumber);
				sb.append(" (");
				sb.append(c);
				sb.append(" ");
				sb.append(Messages.getString("VersionManifest.2044"));
				sb.append(" ");
				sb.append(manifestEntries.size());
				sb.append(")");
				ErrorManager.updateProgressBarString(sb.toString());
				semaphore.waitUntilInterrupted();

				// if we get a bad entry, try the rest, but as a whole we have
				// failed
				
				int downloadSuccessFlag = entry.download(logStream, result, semaphore, serverName, 0, null, skipDownload, deleteOriginalFile);
				//Debug.println("FILE download Flag = " + downloadSuccessFlag);
				if (downloadSuccessFlag == VersionManifestEntry.DOWNLOAD_FAIL) {
					Debug.println("SOFTWARE UPDATE FAILED: Progress Bar String = " + sb.toString());
					Debug.println("SOFTWARE UPDATE FAILED: Download failed - chunkSize, serverName, result = "
							+  ", " + serverName + ", " + result.toString());
					manifestSuccess = false;
				} else if (downloadSuccessFlag == VersionManifestEntry.DOWNLOAD_SUCCESS || downloadSuccessFlag == VersionManifestEntry.DOWNLOAD_SUCCESS_SPLITFILE) {
					if ((TYPE_PRE_REQUISITE.equals(type) || TYPE_POST_UPGRADE.equals(type)) && !skipDownload
							&& downloadSuccessFlag == VersionManifestEntry.DOWNLOAD_SUCCESS) {
						fileDownloadCounter++;
					}
					Debug.println("FILE DOWNLOADED: Progress Bar String = " + sb.toString() + "::" + fileDownloadCounter);
					
					if (entry.getFileName().contains(".zip.")) {
						i++;
					} 
					
					downloadSuccessFlag = VersionManifestEntry.DOWNLOAD_SUCCESS;
				} else if (downloadSuccessFlag == VersionManifestEntry.DOWNLOAD_FILE_PRESENT) {
					if (entry.getFileName().contains(".zip.")) {
						i++;
					} 
				}
				SoftwareVersionUtility.setDownloadStatus(downloadType, i, numberEntries, getVersionNumber());
				if (downloadType != null && downloadType.length() > 0 && !skipDownload) {
					if ((downloadType.equals(type) && fileCountlimit > 0 && fileDownloadCounter >= fileCountlimit)) {
						downloadPaused = true;
						//break;
						skipDownload=true;
					}
				}
				
				if (Thread.interrupted()) {
		            ExtraDebug.println("Interrupted in downloadCustom()" + Thread.currentThread().getName());
		        	throw new InterruptedException();
		        }
			}
		}
		if(!m2.matches()){
			logStream.println("Version " + getVersionNumber() + " download " +  
					(manifestSuccess ? " successful." : "failed."));  	
		}
		

		// if we have successfully downloaded all the manifest contents, join
		// any split files into their original form and remove components
		// from the manifest.
		if (manifestSuccess && !downloadPaused && !skipDownload) {
			manifestSuccess = joinComponentFiles(logStream, result, deleteOriginalFile);
		}

		if (manifestSuccess && !downloadPaused) {
			return VersionManifestEntry.DOWNLOAD_SUCCESS;
		} else if (downloadPaused) {
			return VersionManifestEntry.DOWNLOAD_PAUSED;
		} else {
			return VersionManifestEntry.DOWNLOAD_FAIL;
		}
	}
    
	public int downloadCustom(PrintStream logStream, StringBuffer result, PauseSemaphore semaphore,
			String serverName, String downloadType, int fileLimitCount, boolean skipDownload)
			throws IOException, InterruptedException {
		
		// first, get rid of any component entries that reference a file we
		// already have.
		removeUnnecessaryComponents();
		boolean manifestSuccess = true;
		boolean downloadPaused = false;
		logStream.println("Started version " + getVersionNumber() 
				+ " download at " + new Date(TimeUtility.currentTimeMillis())); 

		// go through all the entries and download them
		int statusflag = -1;
		ErrorManager.updateProgressBarString(Messages.getString("DeltaworksMainPanel.902"));
		for (VersionManifestEntry entry : manifestEntries.values()) {
			// try to download the entry
			if (entry.entryType != null && entry.entryType.equals(downloadType)) {
				// if we get a bad entry, try the rest, but as a whole we have
				// failed
				int flag = entry.download(logStream, result, semaphore, serverName, fileLimitCount, downloadType, skipDownload, true);
				if (flag == VersionManifestEntry.DOWNLOAD_FAIL) {
					Debug.println("SOFTWARE UPDATE FAILED: Download failed - serverName, result = "
							+ serverName + ", " + result.toString());
					manifestSuccess = false;
					statusflag = DOWNLOAD_FAIL;
				} else if (flag == VersionManifestEntry.DOWNLOAD_PAUSED) {
					downloadPaused = true;
					statusflag = DOWNLOAD_PAUSED;
				}
			}

			if (Thread.interrupted()) {
				ExtraDebug.println("Interrupted in downloadCustom()" + Thread.currentThread().getName());
				throw new InterruptedException();
			}

		}

		// if we have successfully downloaded all the manifest contents, join
		// any split files into their original form and remove components
		// from the manifest.
		if (manifestSuccess && !downloadPaused && !skipDownload) {
			joinComponentFiles(logStream, result);
			statusflag = DOWNLOAD_SUCCESS;
		}

		return statusflag;
	}
	
	
    /**
     * For all split files listed in the manifest, try to join them into
     *   their original form and then delete the parts from the manifest and
     *   the directory if join was successful.
     * The hashmap of component files is in the form
     *      <"dir\target file"> -> <List of VersionManifestEntries (parts)>
     * @param logStream a PrintStream for logging events and status.
     * @param result a StringBuffer for creating a diag result string
     */
	public boolean joinComponentFiles(PrintStream logStream, StringBuffer result) {
		return joinComponentFiles(logStream, result, false);
	}
	
    /**
     * For all split files listed in the manifest, try to join them into
     *   their original form and then delete the parts from the manifest and
     *   the directory if join was successful.
     * The hashmap of component files is in the form
     *      <"dir\target file"> -> <List of VersionManifestEntries (parts)>
     * @param logStream a PrintStream for logging events and status.
     * @param result a StringBuffer for creating a diag result string
     */
	public boolean joinComponentFiles(PrintStream logStream, StringBuffer result, boolean deleteOriginalFile) {
		
		Map<String, List<VersionManifestEntry>> fileMap = new HashMap<String, List<VersionManifestEntry>>();

		// collect a map of all target files and the components that will join
		// to create them
		for (VersionManifestEntry entry : manifestEntries.values()) {
			String fileName = entry.getLocalFilePath();
			if (entry.isComponentFile()) {
				// get the name of the file to be joined into
				String targetFileName = fileName.substring(0, fileName.lastIndexOf(".")); 
				// is this target file also in the manifest? If not, then it is
				// not a valid component file
				if (!manifestEntries.containsKey(targetFileName))
					continue;
				// keep a list of the entries that correspond to each target
				// file
				String fileKey = targetFileName;
				List<VersionManifestEntry> entries = fileMap.get(fileKey);
				if (entries == null) {
					entries = new ArrayList<VersionManifestEntry>();
					fileMap.put(fileKey, entries);
				}
				entries.add(entry);
			}
		}

		// go through the map and attempt to join into the target files
		for (String fileKey : fileMap.keySet()) {
			logStream.println("Joining file : " + fileKey); 
			try {
				ZipUtility.joinFile(fileKey, logStream, deleteOriginalFile);
			} catch (IOException e) {
				Debug.println("SOFTWARE UPDATE FAILED: Download failed - Exception: " + e.getMessage());
				Debug.printStackTrace(e);
				logStream.println("Join File failed : " + e); 
				result.append("FAILED. [Could not join file " + fileKey + "]");  
				return false;
			}
		}

		// no exception; all files were joined successfully; remove components.
		for (List<VersionManifestEntry> entries : fileMap.values()) {
			for (VersionManifestEntry entry : entries) {
				manifestEntries.remove(entry.getLocalFilePath());
			}
		}

		// save the newly slimmed-down manifest file.
		try {
			this.save();
		} catch (IOException e) {
			Debug.printStackTrace(e);
		}

		return true;
	}

    public static void main(String[] args) {
        try {
            VersionManifest manifest = new VersionManifest(args[0]);
            manifest.joinComponentFiles(System.out, new StringBuffer(), false);
            manifest.synchronizeDirectory();
        }
        catch (Exception e) {
			Debug.printStackTrace(e);
        }
    }

    /**
     * Synchronize the directory contents with what is listed in the manifest.
     *   This basically deletes any files in the directory that are not listed
     *   in the manifest. This could happen if we are issuing a new version that
     *   does not need a file copied from a previous version. This will also be
     *   used to remove all component files after they have been successfully
     *   combined into the original file and the manifest has been re-written.
     */
	public void synchronizeDirectory() {
		File dir = new File(posLocation);
		File[] files = getDirectoryContents(dir).toArray(new File[0]);
		for (int i = 0; i < files.length; i++) {
			// manifest.dat is necessary, so don't delete it...
			if (files[i].getName().equalsIgnoreCase("manifest.dat")) 
				continue;
			// delete the file if it is not in the manifest
			else if (contains(files[i].getPath()))
				continue;
			boolean b = files[i].delete();
			if (!b) {
				Debug.println("File " + files[i].getName() + " could not be deleted");
			}
		}

	}

    /**
     * Get a recursive list of the contents of a directory.
     * @param the directory to collect the list of files for.
     * @return an List of File entries.
     */
    private List<File> getDirectoryContents(File dir) {
        File[] contents = dir.listFiles();
        List<File> allFiles = new ArrayList<File>();
        if (contents != null) {
	        for (int i = 0; i < contents.length; i++) {
	            if (contents[i].isDirectory())
	                allFiles.addAll(getDirectoryContents(contents[i]));
	            else
	                allFiles.add(contents[i]);
	        }
        }
        return allFiles;
    }

    /**
     * Go through the contents of the manifest and determine if the given file
     *   is an entry in it.
     * @param file the name of the file to look for in the manifest
     * @return whether or not the given file is a member of the manifest
     */
	private boolean contains(String fileName) {
		for (VersionManifestEntry entry : manifestEntries.values()) {
			if (entry.getLocalFilePath().equalsIgnoreCase(getTranslatedPath(fileName)))
				return true;
		}
		return false;
	}
	
	@Override
    public int getStatus(StringBuffer resultBuffer) {
    	return getStatus(resultBuffer,null,false);
    }
    /**
     * Get the status of this manifest.
     * @param removeUnnecessary 
     * @return the status of the manifest
     */
	@Override
	public int getStatus(StringBuffer resultBuffer, String type, boolean removeUnnecessary) {
		if (removeUnnecessary) {
			removeUnnecessaryComponents();
		}
		for (VersionManifestEntry entry : manifestEntries.values()) {
			if (type != null) {

				if (type.equals(entry.entryType)) {
					int status = entry.getStatus(resultBuffer, type, removeUnnecessary);
					if (entry.skip) {
						status = entry.checkFileExists(resultBuffer);
					} else {
						if (status != READY) {
							return status;
						}
					}
				} else {
					continue;
				}

			} else {
				int status = entry.getStatus(resultBuffer, type, removeUnnecessary);
				if (entry.skip) {
					status = entry.checkFileExists(resultBuffer);
				} else {
					if (status != READY) {
						return status;
					}
				}
			}

		}
		return READY;
	}

	 /**
     * Load the manifest from a BufferedReader source. (The buffered reader will
     *   be either a local file or a URL Connection)
     * @param reader the BufferedReader to get the manifest from.
     * @param loadLocal whether or not we are loading this manifest from the
     *   local manifest file. If not, we must go to the host to load the
     *   dependencies listed in the manifest.
     */
	private void load(String baseUrl, BufferedReader reader, boolean loadLocal,
			StringBuffer resultBuffer) throws IOException {
		manifestEntries = new TreeMap<String, VersionManifestEntry>();
		String line;
		boolean doLoad = true;
		if (type != null && type.length() > 0) {
			doLoad = false;
		}
		while ((line = reader.readLine()) != null) {
			if (line.toUpperCase(Locale.US).indexOf("VERSIONNUMBER=") >= 0) { 
				StringTokenizer strTok = new StringTokenizer(line, "="); 
				strTok.nextToken();
				versionNumber = strTok.nextToken();
				posLocation = versionNumber; // same as version for manifests
			}

			Map<String, String> map = getTokenized(line);

			if (map.get(TYPE_FILE) != null) { 
				addManifestEntry(new VersionManifestEntry(line, type, this.fileName));
			} else if (map.get(TYPE_DEPENDENCY) != null) { 
				VersionManifest depMan;
				if (loadLocal) {
					String depDir = map.get("PosLocation"); 
					depMan = new VersionManifest(depDir);
				} else {
					String url = map.get("ServerLocation") + "/" +  
							map.get(TYPE_DEPENDENCY); 
					int fileSize = Integer.parseInt(map.get("FileSize")); 
					depMan = new VersionManifest(baseUrl, url, fileSize, resultBuffer, TYPE_DEPENDENCY, false);
				}
				depMan.entryType = TYPE_DEPENDENCY;
				addManifestEntry(depMan);

			} else if (map.get(TYPE_PRE_REQUISITE) != null) {
				VersionManifest jreMan;
				if (loadLocal) {
					String depDir = map.get("PosLocation"); 
					String fileName = map.get(TYPE_PRE_REQUISITE); 
					jreMan = new VersionManifest(depDir, fileName, TYPE_PRE_REQUISITE, doLoad);
				} else {
					String url = map.get("ServerLocation") + "/" +  
							map.get(TYPE_PRE_REQUISITE); 
					int fileSize = Integer.parseInt(map.get("FileSize")); 
					jreMan = new VersionManifest(baseUrl, url, fileSize, resultBuffer, TYPE_PRE_REQUISITE, skip);
				}
				jreMan.entryType = TYPE_PRE_REQUISITE;
				addManifestEntry(jreMan);
			} else if (map.get(TYPE_POST_UPGRADE) != null) {
				VersionManifest jreMan;
				if (loadLocal) {
					String depDir = map.get("PosLocation"); 
					String fileName = map.get(TYPE_POST_UPGRADE); 
					jreMan = new VersionManifest(depDir, fileName, TYPE_POST_UPGRADE, doLoad);
				} else {
					String url = map.get("ServerLocation") + "/" +  
							map.get(TYPE_POST_UPGRADE); 
					int fileSize = Integer.parseInt(map.get("FileSize")); 
					jreMan = new VersionManifest(baseUrl, url, fileSize, resultBuffer, TYPE_POST_UPGRADE, false);
				}
				jreMan.entryType = TYPE_POST_UPGRADE;
				addManifestEntry(jreMan);
			}
		}
		reader.close();
	}

    /**
     * Load the manifest from the versionDirectory/manifest.dat file. The
     *   manifest.dat file is assumed to be in the directory corresponding to
     *   the version number.
     */
    private void load() throws IOException {
        File file = new File(getLocalFilePath());
        if (! file.exists()) {
            throw new IOException("Manifest file '" + getLocalFilePath() + "' does not exist."); 
        }
        FileReader fr = null;
        BufferedReader br = null;
        try {
            fr = new FileReader(file);
            br = new BufferedReader(fr);
	        load("", br, true, new StringBuffer());
        } finally {
        	if (br != null) {
        		try {
        			br.close();
        		} catch (Exception e) {
        			Debug.printException(e);
        		}
        	}
        	if (fr != null) {
        		try {
        			fr.close();
        		} catch (Exception e) {
        			Debug.printException(e);
        		}
        	}
        }
    }

    /**
     * Save the contents of this manifest to the file in the corresponding
     *   version directory / file name.
     */
    private void save() throws IOException {
        File file = createFileAndDir(getLocalFilePath());
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        bw.write("VERSIONNUMBER=" + versionNumber + "\n");	 
        for (VersionManifestEntry entry : manifestEntries.values()) {
            bw.write(entry.toString());
        }
        bw.close();
    }


    
    /**
     * Determine whether all the files listed in this manifest are present and
     *   uncorrupted in the version directory.
     * @return whether or not the manifest is valid. To be valid, all its
     *   entries must be valid.
     */
	public boolean isValid(StringBuffer resultBuffer) {
		for (VersionManifestEntry entry : manifestEntries.values()) {
			if (TYPE_PRE_REQUISITE.equals(entry.entryType) || TYPE_POST_UPGRADE.equals(entry.entryType)) {
				continue;
			}
			int status = entry.getStatus(resultBuffer);
			if (status != VersionManifestEntry.READY) {
				// Debug.debugTest(resultBuffer.toString());
				return false;
			}
		}
		// all files must have passed
		return true;
	}

	 @Override
	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	    @Override
	public long getFileSize() {
		return fileSize;
	}

    public void addManifestEntry(VersionManifestEntry newManifestEntry) {
        manifestEntries.put(newManifestEntry.getLocalFilePath(), newManifestEntry);
    }


    public String getVersionNumber() {
        return versionNumber;
    }
    
    public static void resetDownloadCounter() {
    	fileDownloadCounter=0;
    }
    
	public VersionManifest getDependency() throws IOException {

		File file = new File(getLocalFilePath());
		if (!file.exists())
			throw new IOException("Manifest file '" + getLocalFilePath() 
					+ "' does not exist."); 
		BufferedReader reader = new BufferedReader(new FileReader(file));

		try {
			String line;
			while ((line = reader.readLine()) != null) {
				if (line.toUpperCase(Locale.US).indexOf("VERSIONNUMBER=") >= 0) { 
					StringTokenizer strTok = new StringTokenizer(line, "="); 
					strTok.nextToken();
					versionNumber = strTok.nextToken();
					posLocation = versionNumber; // same as version for
													// manifests
				}

				HashMap<String, String> map = getTokenized(line);

				if (map.get(TYPE_DEPENDENCY) != null) { 
					VersionManifest depMan;
					String depDir = map.get("PosLocation"); 
					depMan = new VersionManifest(depDir);

					depMan.entryType = TYPE_DEPENDENCY;
					return depMan;
				}
			}
			return null;
		} finally {
			reader.close();
		}
	}
}


