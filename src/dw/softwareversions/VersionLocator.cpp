//	VersionLocator.cpp
  
//	A utility program for validation of DeltaWorks versions, which
//	returns the most current valid software version, based on command line option.
	
//	It replaces some of the functionality of SoftwareVersionUtility.java as well as
//	the functionality of validate.bat.
//                      

#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <process.h>

#define DEBUG_INFO_ON		0	//Print debug information to standard output, if not zero

#define DW_VERSIONS_MAX		6
#define JRE_VERSIONS_MAX	6
#define STRING_LENGTH_MAX	1024

//Add entries to these two string arrays for DeltaWorks future versions that support future JREs
static char *saDwVersionFirstUsingJre[]	= {  "5300", "999999"};	//First release that DW used a corresponding JRE
static char *saJresSupported[]			= {"jre142","jre1645","jre1651"};	//JREs supported by DW
//Note: If there is a jreNNN directory, there should be a jreNNNz directory too for unzipping the jre if it gets
//		corrupted. If a jreNNNu.zip exists in a DeltaWorks version, it will be unzip over the jreNNN directory.

char saDwVersionsAvailable[DW_VERSIONS_MAX][STRING_LENGTH_MAX];
char saJresAvailable[JRE_VERSIONS_MAX][STRING_LENGTH_MAX];

int intCommandLineVersion= 9999999;
int intDwVersions = 0;
int intGoodJreVersion = -1;
int intGoodDwVersion = -1;
int intJreVersions = 0;
int intMostRecentDwVersion = 0;
char strJreForMostRecentDwVersion[STRING_LENGTH_MAX] = "";

void updateJreIfUpdateAvailable(char* strDwVersion, char* strJre);


//	Check if a JRE directory contains two important files
boolean checkJre(char *strJre) {
	char strFile[STRING_LENGTH_MAX];
	WIN32_FIND_DATA xFindData;
	HANDLE hFile;

	sprintf(strFile, "%s\\manifest.dat", strJre);
	hFile = FindFirstFile(strFile, &xFindData);
	if ((hFile == (void *)INVALID_HANDLE_VALUE) || (hFile == (void *)ERROR_FILE_NOT_FOUND)) {
		printf("  Did not find JRE manifest.dat for %s\n", strJre);
		return FALSE;
	}

	//version of jre manifest.dat file exists
	FindClose(hFile);

	sprintf(strFile, "%s\\bin\\java.exe", strJre);
	hFile = FindFirstFile(strFile, &xFindData);
	if ((hFile == (void *)INVALID_HANDLE_VALUE) || (hFile == (void *)ERROR_FILE_NOT_FOUND)) {
		printf("  Found JRE manifest.dat, but not java.exe for %s\n", strJre);
		return FALSE;
	}

	//version of jre java.exe file exists
	FindClose(hFile);
	return TRUE;
}
boolean checkJreValid(char *strJre) {
	char strFile[STRING_LENGTH_MAX];
	WIN32_FIND_DATA xFindData;
	HANDLE hFile;
	
	sprintf(strFile, "%s\\bin\\java.exe", strJre);
	hFile = FindFirstFile(strFile, &xFindData);
	if ((hFile == (void *)INVALID_HANDLE_VALUE) || (hFile == (void *)ERROR_FILE_NOT_FOUND)) {
		printf("  Could not find java.exe for %s\n", strJre);
		return FALSE;
	}

	//version of jre java.exe file exists
	FindClose(hFile);
	return TRUE;
}

boolean checkJreManifestValid(char *strJre) {
	char strFile[STRING_LENGTH_MAX];
	WIN32_FIND_DATA xFindData;
	HANDLE hFile;
	
	sprintf(strFile, "%s\\manifest.dat", strJre);
	hFile = FindFirstFile(strFile, &xFindData);
	if ((hFile == (void *)INVALID_HANDLE_VALUE) || (hFile == (void *)ERROR_FILE_NOT_FOUND)) {
		printf("  Did not find JRE manifest.dat for %s\n", strJre);
		return FALSE;
	}	
	FindClose(hFile);
	return TRUE;
}


//	Unzip a jre.zip from a JRE zip directory, if two other important files exist too

//	It would be better if the JRE zip directory could be validated, but
//  at this point, there is not a validated dwGui.jar or java.exe; however
//  if a jre.zip is unzipped, its contents will be validated.
boolean unzipJreIfExists(char *strJre) {
	char strFile[STRING_LENGTH_MAX];
	char strUnzip[STRING_LENGTH_MAX];
	WIN32_FIND_DATA xFindData;
	HANDLE hFile;

	sprintf(strFile, "%sz\\manifest.dat", strJre);
	hFile = FindFirstFile(strFile, &xFindData);
	if ((hFile == (void *)INVALID_HANDLE_VALUE) || (hFile == (void *)ERROR_FILE_NOT_FOUND)) {
		printf("  Did not find JRE zip manifest.dat for %sz\n", strJre);
		return FALSE;
	}

	//version of jre zip directory manifest.dat file exists
	FindClose(hFile);

	sprintf(strFile, "%sz\\unzip.exe", strJre);
	hFile = FindFirstFile(strFile, &xFindData);
	if ((hFile == (void *)INVALID_HANDLE_VALUE) && (hFile == (void *)ERROR_FILE_NOT_FOUND)) {
		printf("  Found JRE zip manifest.dat, but not unzip.exe for %s\n", strJre);
		return FALSE;
	}

	//version of jre zip directory unzip.exe file exists
	FindClose(hFile);

	sprintf(strFile, "%sz\\jre.zip", strJre);
	hFile = FindFirstFile(strFile, &xFindData);
	if ((hFile == (void *)INVALID_HANDLE_VALUE) && (hFile == (void *)ERROR_FILE_NOT_FOUND)) {
		printf("  Found JRE zip manifest.dat and unzip.exe, but not jre.zip for %s\n", strJre);
		return FALSE;
	}

	//version of jre zip directory jre.zip file exists
	FindClose(hFile);

	sprintf(strUnzip, "%sz\\unzip.exe -qq -o %sz\\jre.zip", strJre, strJre);
	if (system(strUnzip)) {
		printf("  Failed to uncompress %s files from %sz directory\n", strJre, strJre);
		return FALSE;
	}

	if (checkJre(strJre) == FALSE) {
		printf("  Uncompressed %s files from %sz directory did contain the proper files\n", strJre, strJre);
		return FALSE;
	}

	return TRUE;
}

//	Check if a JRE directory contains two important files or they are contained in its zip directory
boolean checkJreAndMaybeItsZip(char *strJre) {
	
	if (checkJreValid(strJre) == FALSE && checkJreManifestValid(strJre) == TRUE) {
		//jre manifest.dat or java.exe does not exist, attempt to restore jre by unzipping the jre.zip, if present
		return unzipJreIfExists(strJre);
	}else if(checkJreValid(strJre) == TRUE && checkJreManifestValid(strJre) == FALSE) {
		return FALSE;
	}else if(checkJreValid(strJre) == TRUE && checkJreManifestValid(strJre) == TRUE) {
		return TRUE;
	}

	return FALSE;

}

//	Check if a DeltaWorks version directory contains two important files
boolean checkDwVersion(char *strDwVersion) {
	char strFile[STRING_LENGTH_MAX];
	WIN32_FIND_DATA xFindData;
	HANDLE hFile;

	sprintf(strFile, "%s\\manifest.dat", strDwVersion);
	hFile = FindFirstFile(strFile, &xFindData);
	if ((hFile == (void *)INVALID_HANDLE_VALUE) || (hFile == (void *)ERROR_FILE_NOT_FOUND)) {
		printf("  Did not find DeltaWorks manifest.dat for %s\n", strDwVersion);
		return FALSE;
	}

	//dw version manifest.dat file exists
	FindClose(hFile);

	sprintf(strFile, "%s\\dwGui.jar", strDwVersion);
	hFile = FindFirstFile(strFile, &xFindData);

	if ((hFile == (void *)INVALID_HANDLE_VALUE) || (hFile == (void *)ERROR_FILE_NOT_FOUND)) {
		printf("  Found DeltaWorks manifest.dat, but not dwGui.jar for %s\n", strDwVersion);
		return FALSE;
	}

	//dw version dwGui.jar file exists
	FindClose(hFile);
	return TRUE;
}

//	Find possilbe DeltaWorks version and JRE directories
boolean findAndCheckSubDirectories() {
	WIN32_FIND_DATA xFindData;
	HANDLE hFile;

	hFile = FindFirstFile("*", &xFindData);
	if ((hFile == (void *)INVALID_HANDLE_VALUE) || (hFile == (void *)ERROR_FILE_NOT_FOUND)) {
		printf("There are no files in this directory\n");
		return FALSE;
	}

	do {
		if (xFindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
			//File is a directory; check if a DeltaWorks version directory, which begins with a digit
			if ((xFindData.cFileName[0] >= '0') && (xFindData.cFileName[0] <= '9')) {
				if ((intDwVersions < DW_VERSIONS_MAX)) {
					if (checkDwVersion(xFindData.cFileName) == TRUE) {
						//Found possible DeltaWorks version directory
						if (DEBUG_INFO_ON) {printf("Found possible DeltaWorks directory for version %s\n", xFindData.cFileName);}
						strncpy(saDwVersionsAvailable[intDwVersions++], xFindData.cFileName, STRING_LENGTH_MAX);
					}
				}

			//Directory is not DeltaWorks, check if a JRE version directory, which begins with a 'J' and does not end with a 'Z'
			} else if ((xFindData.cFileName[0] == 'j') || (xFindData.cFileName[0] == 'J')) {
				if ((xFindData.cFileName[strlen(xFindData.cFileName) - 1] != 'z') && 
						(xFindData.cFileName[strlen(xFindData.cFileName) - 1] != 'Z')) {
					if (intJreVersions < JRE_VERSIONS_MAX) {
						if (checkJreAndMaybeItsZip(xFindData.cFileName) == TRUE) {
							//found possible JRE version directory
							if (DEBUG_INFO_ON) {printf("Found possible JRE directory for version %s\n", xFindData.cFileName);}
							strncpy(saJresAvailable[intJreVersions++], xFindData.cFileName, STRING_LENGTH_MAX);
						}
					}
				}
			}
		}
	} while (FindNextFile(hFile, &xFindData));

	FindClose(hFile);

	return (intDwVersions != 0) && (intJreVersions != 0);
}

//	Find at least one valid DeltaWorks version and at least one valid JRE

//	To validate, one needs a DeltaWorks dwGui.jar file and a JRE. Therefore, all DeltaWorks 
//	versions are tried with all JRE version to see if we can validate one of each. This
//	is done in reverse in order to attempt to validate the most reset one first.
boolean findAtLeastOneValidDwVersionAndJre() {
	char strValidate[STRING_LENGTH_MAX];

	if (DEBUG_INFO_ON) {printf("Dw Versions=%d, Jre Versions=%d\n", intDwVersions, intJreVersions);}
	for (int ii = intDwVersions - 1; ii >= 0; ii--) {
		for (int jj = intJreVersions - 1; jj >= 0; jj--) {
			if (intGoodJreVersion < 0 ) {
				//Attempt to validate a JRE version
				sprintf(strValidate, "%s\\bin\\java -classpath %s\\dwGui.jar; com.vu.VU %s %s", 
					saJresAvailable[jj], saDwVersionsAvailable[ii], saJresAvailable[jj],saDwVersionsAvailable[ii]);
				if (system(strValidate)) {
					printf("Failed to validate %s for DeltaWorks %s\n", 
						saJresAvailable[jj], saDwVersionsAvailable[ii]);
					
					//Jre could not be validated, see if the zip version exists
					if (checkJre(saJresAvailable[jj]) == TRUE) {
						if (unzipJreIfExists(saJresAvailable[jj]) == FALSE) {
							continue;	//Cannot unzip it, give up on this jre
						}
					}
					
					
					//Jre was unzipped, attempt to validate it
					if (system(strValidate)) {
						printf("Failed to validate %s from %sz for DeltaWorks %s\n", 
							saJresAvailable[jj], saJresAvailable[jj], saDwVersionsAvailable[ii]);
						// Update it just in case we neeed to use it.
						updateJreIfUpdateAvailable(saDwVersionsAvailable[jj], saJresAvailable[jj]);
						continue;	//Not valid, give up on this jre
					}else
					{
						//A JRE version has been validated, save it.
						intGoodJreVersion = jj;			
						intGoodDwVersion = ii;
						return TRUE;
					}
				}else
				{
					//A JRE version has been validated, save it.
					intGoodJreVersion = jj;		
					intGoodDwVersion = ii;
					return TRUE;
				}
				
			}

			//Attempt to validate a DeltaWorks version
			sprintf(strValidate, "%s\\bin\\java -classpath %s\\dwGui.jar; com.vu.VU %s", 
				saJresAvailable[jj], saDwVersionsAvailable[ii], saDwVersionsAvailable[ii]);
			if (system(strValidate)) {
				printf("Failed to validate DeltaWorks version %s with good %s\n", 
					saDwVersionsAvailable[ii], saJresAvailable[jj]);
				//continue;	//Not valid, give up on this DeltaWorks version
			}else{
				//A DeltaWorks version has been validated, save it.
				intGoodDwVersion = ii;
 				if (DEBUG_INFO_ON) {printf("Validated one of each: DeltaWorks version %s and jre version %s\n", 
					saDwVersionsAvailable[ii], saJresAvailable[jj]);}
				return TRUE;
			}
		}
	}

	return FALSE;
}

//	Validate a DeltaWorks version with its allowable and properly validated JRE

//	This is done in reverse in order to attempt to validate the most reset one first. It contains 
//	the functionality of the old validate.bat file. However the validate.bat file still needs to 
//	be there, so that you can upgrade from a version that doesn't use this C program.  
boolean validateDwVersionAndItsBestJreVersion(char* strDwVersion) {
	int iLength = sizeof(saJresSupported) / sizeof(saJresSupported[0]);
	char strJre[STRING_LENGTH_MAX];
	char strValidate[STRING_LENGTH_MAX];

	//Attempt to validate a DeltaWorks version
	if(strcmp(strDwVersion, saDwVersionsAvailable[intGoodDwVersion]) != 0) {
		sprintf(strValidate, "%s\\bin\\java -classpath %s\\dwGui.jar; com.vu.VU %s",
			saJresAvailable[intGoodJreVersion], saDwVersionsAvailable[intGoodDwVersion], strDwVersion);
		if (system(strValidate)) {
			printf("Failed to validate DeltaWorks version %s\n", strDwVersion);
			return FALSE;
		}
	}

	//Attempt to validate its best JRE version
	for (int ii = iLength - 1; ii >= 0 ; ii--) {
		strncpy(strJre, saJresSupported[ii], sizeof(strJre));

		if (checkJre(strJre) == FALSE) {
			continue;
		}

		if(saJresAvailable[intGoodJreVersion]==strJre && saDwVersionsAvailable[intGoodDwVersion]==strDwVersion)
		{
			
			sprintf(strValidate, "%s\\bin\\java -classpath %s\\dwGui.jar; com.vu.VU %s %s",
			saJresAvailable[intGoodJreVersion], saDwVersionsAvailable[intGoodDwVersion], saJresAvailable[intGoodJreVersion],saDwVersionsAvailable[intGoodDwVersion]);
			if (system(strValidate) == 0) {
				strcpy(strJreForMostRecentDwVersion, saJresAvailable[intGoodJreVersion]);
				if (DEBUG_INFO_ON) {printf("Validated DeltaWorks version %s with its best jre version %s\n", 
					saDwVersionsAvailable[intGoodDwVersion], strJreForMostRecentDwVersion);}
				return TRUE;
			}
		}
		sprintf(strValidate, "%s\\bin\\java -classpath %s\\dwGui.jar; com.vu.VU %s %s",
			strJre, strDwVersion, strJre,strDwVersion);
		if (DEBUG_INFO_ON) {printf("strValidate = [%s]\n", 	strValidate);}
		if (system(strValidate) == 0) {
			strcpy(strJreForMostRecentDwVersion, strJre);
			if (DEBUG_INFO_ON) {printf("Validated DeltaWorks version %s with its best jre version %s\n", 
				strDwVersion, strJreForMostRecentDwVersion);}
			return TRUE;
		}

		//Jre could not be validated, see if the zip version exists
		printf("Failed to validate %s for %s\n", strJre, strDwVersion);
		if (unzipJreIfExists(strJre) == TRUE) {
			//Jre was unzipped, attempt to validate it
			if (system(strValidate) == 0) {
				strcpy(strJreForMostRecentDwVersion, strJre);
				if (DEBUG_INFO_ON) {printf("Validated DeltaWorks version %s with its best jre version %s from %sz\n", 
					strDwVersion, strJreForMostRecentDwVersion, strJre);}
				return TRUE;
			}
		}
		printf("Failed to validate jre %s for Deltaworks version %s\n", strJre, strDwVersion);
		
	}
	

	printf("Failed to validate any jre for Deltaworks version %s\n", strDwVersion);
	return FALSE;
}
//Update the jre if an update available
void updateJreIfUpdateAvailable(char* strDwVersion, char* strJre)
{
	char strValidate[STRING_LENGTH_MAX];
	char strUnzipFile[STRING_LENGTH_MAX];
	char strZipFile[STRING_LENGTH_MAX];
	WIN32_FIND_DATA xFindData;
	HANDLE hFile;


	if (DEBUG_INFO_ON) {printf("updateJreIfUpdateAvailable DW:%s JRE:%s\n", strDwVersion, strJre);}

	//Check if jre update zip file (e.g.; jre142u.zip) exists
	sprintf(strZipFile, "%s\\%su.zip", strDwVersion, strJre);
	hFile = FindFirstFile(strZipFile, &xFindData);
	if ((hFile == (void *)INVALID_HANDLE_VALUE) || (hFile == (void *)ERROR_FILE_NOT_FOUND)) {
		//The jre update zip file does not exist
		return;
	}

	//In DeltaWorks version, jreNNNu.zip file exists
	FindClose(hFile);

	//Check if the unzip.exe program is in the jre zip directory
	sprintf(strUnzipFile, "%sz\\unzip.exe", strJre);
	hFile = FindFirstFile(strUnzipFile, &xFindData);
	if ((hFile == (void *)INVALID_HANDLE_VALUE) && (hFile == (void *)ERROR_FILE_NOT_FOUND)) {
		printf("  Found %su.zip for %s, but not unzip.exe for %sz\n", strJre, strDwVersion, strJre);
		return;
	}

	//Unzip.exe program exists
	FindClose(hFile);
	
	//Unzip the jre update zip over the jre updated directory
	sprintf(strUnzipFile, "%sz\\unzip.exe -qq -o %s", strJre, strZipFile);
	if (system(strUnzipFile)) {
		printf("  Failed to uncompress %s files from DeltaWorks %s directory\n", strZipFile, strDwVersion);
		return;
	}

	//Validate the jre updated directory
	sprintf(strValidate, "%s\\bin\\java -classpath %s\\dwGui.jar; com.vu.VU %s %s",
		strJre, strDwVersion,strJre,strDwVersion);
	if (system(strValidate)) {
		printf("  Failed to validate the updated jre directory %s from DeltaWorks %s directory\n", 
			strJre, strDwVersion);
		return;
	}

	if (DEBUG_INFO_ON) {printf("Validated DeltaWorks version %s with its best updated jre version %s\n", 
		strDwVersion, strJre);}

	return;
}

// Find and validate most recent DeltaWorks version by checking all subdirectories in the DeltaWorks
//	directory. This is done in reverse in order to attempt to validate the most reset one first. Also
//	update the JRE if an update is available, unless it has already been updated.
boolean findAndValidateMostRecentDWVersion() {
	int intSubdirectoryName;
	
	for (int ii = intDwVersions - 1; ii >= 0; ii--) {
		intSubdirectoryName = atoi(saDwVersionsAvailable[ii]);
		if ((intSubdirectoryName > intMostRecentDwVersion) && 
				(intSubdirectoryName <= intCommandLineVersion) && 
				(validateDwVersionAndItsBestJreVersion(saDwVersionsAvailable[ii]) == TRUE)) {
			//Found a new, more recent DeltaWorks version
			intMostRecentDwVersion = intSubdirectoryName;

			//Update the jre if an update available
			updateJreIfUpdateAvailable(saDwVersionsAvailable[ii], strJreForMostRecentDwVersion);
		}
	}
	
	return (intMostRecentDwVersion == 999999) ? FALSE : TRUE;
}

//	Create a batch file to set the current DeltaWorks version
//
//	This file creates the version.bat file that deltaworks.bat executes to set the 
//	CURRENT_VERSION environment variable. This was implemented as a workaround for 
//	Windows 98 where SET COMMAND_VARIABLE = %ERRORLEVEL% doesn't work.
void createSetCurrentVersionBatchFile() {
	char strJre[STRING_LENGTH_MAX];
	FILE *stream;

	stream = fopen ("version.bat", "w");
	fprintf(stream, "set CURRENT_VERSION=%d\n",intMostRecentDwVersion);

	if(strJreForMostRecentDwVersion[0] != 0) {
		strcpy(strJre, strJreForMostRecentDwVersion);
	} else if (intGoodJreVersion >= 0) {
		strcpy(strJre, saJresAvailable[intGoodJreVersion]);
	} else {
		strcpy(strJre, "jre1651");
	}

	fprintf(stream, "set CURRENT_JRE=%s\n",strJre);
	fprintf(stream, "echo DeltaWorks current version is %d and its JRE is %s.\n",
		intMostRecentDwVersion, strJreForMostRecentDwVersion);
	fprintf(stream, "echo DeltaWorks current version is %d and its JRE is %s. >>deltaworks.log\n",
		intMostRecentDwVersion, strJreForMostRecentDwVersion);
	fclose(stream);

}

//	Version locator
int main(int argc, char* argv[]) {
	char strCommandLine[1024] = "";

	//Print debug info for command line
	if (DEBUG_INFO_ON) {printf("VersionLocator: Started\n");}

	//Save the command line argument argument, if present, to limit the upper limit of the DeltaWorks versions
	if (argc > 1) {
		strcat(strCommandLine, argv[1]);
		intCommandLineVersion = atoi(strCommandLine);
	}

	// Don't limit the Deltaworks versions if the command line argument is less than two
	if (intCommandLineVersion < 2) {
		intCommandLineVersion = 9999999;
	}

	//Find and check sub-directories for possible DeltaWorks versions and JREs.
	if (findAndCheckSubDirectories() == FALSE ) {
		printf("VersionLocator: Failed to find at least one version each of DeltaWorks and JRE\n");
		return 1;	//Failed
	}

	//See if there is at least one valid DeltaWorks version and at least one valid JRE
	if (findAtLeastOneValidDwVersionAndJre() == FALSE) {
		printf("VersionLocator: Failed to validate at least one DeltaWorks version and at least one JRE\n");
		printf("Attempt to bring up an unvalidated version of DeltaWorks\n");

		//Attempt to bring up an unvalidated version of DeltaWorks
		intMostRecentDwVersion = atoi(saDwVersionsAvailable[0]);
	
	//Ok, threre ia least one valid DW version and one valid JRE; find the most recent Dw version
	} else if (findAndValidateMostRecentDWVersion() == FALSE) {
		printf("VersionLocator: Failed to validate at least one DeltaWorks version\n");
		printf("Attempt to bring up a validated version of DeltaWorks\n");

		//Attempt to bring up a validated version of DeltaWorks (there aren't any valid JREs)
		intMostRecentDwVersion = atoi(saDwVersionsAvailable[intGoodDwVersion]);

	}

	//Create batch file for the DeltaWorks version to be used
	createSetCurrentVersionBatchFile();

	if (DEBUG_INFO_ON) {printf("VersionLocator: Completed\n");}
	return 0;	//Success
}

