package dw.softwareversions;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import dw.utility.Debug;


/**
 * Utility to locate the most recent integer-based directory in the current
 * directory. An older version of this class (dating back to at least 8/22/2002)
 * is contained in locator.jar, which is part of the base distribution. If
 * we ever find that this class needs work, it should probably just be replaced
 * by a short C program.
 */
public class RecentVersionLocator {
    /**
     * Get the highest version number directory, starting at searchFrom if it
     *   is greater than 1. Otherwise, search backward from it. If there are no
     *   version directories, return 1.
     * Note : I am using a 1 as the return code when we have no valid versions
     *   because if for some reason this class is not locatable from the batch
     *   file, it will not go into an inifinite loop.
     */
    public static int getMostRecent(int searchFrom) {
        Integer[] versionArray = getAllVersions();
        // if no versions available, return 1
        if (versionArray.length == 0)
            return 1;
        // if we aren't searching from a particular one back, just return highest
        if (searchFrom == 1) {
            return versionArray[versionArray.length - 1].intValue();
        }
        // step through them backward if we need to start at a certain version
        for (int i = versionArray.length - 1; i >= 0; i--) {
            if (versionArray[i].intValue() < searchFrom)
                return versionArray[i].intValue();
        }
        return 1;
    }

    /**
     * Get a sorted list of all the version directories that exist in the
     *   current directory.
     * @return an array of Integer, sorted from lowest to highest version. If
     *    there are none available, return an empty array.
     */
    public static Integer[] getAllVersions() {
        // collect all the version directories available
        ArrayList<Integer> versionDirs = new ArrayList<Integer>();
        File dir = new File(".");	
        File[] files = dir.listFiles();
        if (files != null) {
	        for (int i = 0; i < files.length; i++) {
	            if (files[i].isDirectory()) {
	                try {
	                    String versionName = files[i].getCanonicalFile().getName();
	                    Integer version = Integer.parseInt(versionName);
	                    // dir is a number...must be a version directory
	                    versionDirs.add(version);
	                }
	                catch (NumberFormatException e) {}
	                catch (IOException e) {}
	            }
	        }
        }
        Integer[] versionArray = versionDirs.toArray(new Integer[0]);
        Arrays.sort(versionArray);
        return versionArray;
    }
    
	public static Integer[] getAllUnzippedJreVersions() {
		// collect all the version directories available
		ArrayList<Integer> versionDirs = new ArrayList<Integer>();
		File dir = new File("."); 
		File[] files = dir.listFiles();
        if (files != null) {
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					try {
						String versionName = files[i].getCanonicalFile().getName();
						if (versionName.toUpperCase(Locale.US).startsWith("JRE") && !versionName.toUpperCase(Locale.US).endsWith("Z")) {
							Integer version = Integer.parseInt(versionName.substring(3));
							// dir is a Jre folder...
							versionDirs.add(version);
						}
	
					} catch (NumberFormatException e) {
						Debug.println(e.getMessage());
					} catch (IOException e) {
					}
				}
			}
		}
		Integer[] versionArray =  versionDirs.toArray(new Integer[0]);
		Arrays.sort(versionArray);
		return versionArray;
	}

	public static String[] getAllJreVersions() {
		// collect all the version directories available
		ArrayList<String> versionDirs = new ArrayList<String>();
		File dir = new File("."); 
		File[] files = dir.listFiles();
        if (files != null) {
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					try {
						String versionName = files[i].getCanonicalFile().getName();
						if (versionName.toUpperCase(Locale.US).startsWith("JRE") && versionName.toUpperCase(Locale.US).endsWith("Z")) {
							versionDirs.add(versionName);
						}
					} catch (IOException e) {
					}
				}
			}
        }
		String[] versionArray = versionDirs.toArray(new String[0]);
		Arrays.sort(versionArray);
		return versionArray;
	}


    /**
     * Search for the most recent version directory, or display a message to
     *   the user.
     * Usage : RecentVersionLocator <locate> <searchFrom>
     *         RecentVersionLocator <error>
     */
    public static void main(String[] args) {
        if (args.length > 0 && args[0].equalsIgnoreCase("error")) {	
            // display a dialog to the user if Deltaworks can not start at all
            try {
               UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (IllegalAccessException e) {
            } catch (ClassNotFoundException e) {
            } catch (InstantiationException e) {
            } catch (UnsupportedLookAndFeelException e) {
            }
            JFrame f = new JFrame();
            JPanel errorPanel = new JPanel(new java.awt.GridLayout(3, 1));
            errorPanel.add(new JLabel(
                "No valid versions of DeltaWorks could be found."));
            errorPanel.add(new JLabel(
                "Please contact MoneyGram technical support."));
           // errorPanel.add(new JLabel(Resources.getString("RecentVersionLocator.technical_support_at_1-800-794-7005_4"))); 
            JOptionPane.showMessageDialog(f, errorPanel, 
                "DeltaWorks could not be started", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
        if (args.length > 1 && args[0].equalsIgnoreCase("locate")) {	
            int searchFrom = 0;
            try {
                searchFrom = Integer.parseInt(args[1]);
                int mostRecent = RecentVersionLocator.getMostRecent(searchFrom);
                System.exit(mostRecent);
            }
            catch (NumberFormatException e) {
                Debug.println("Usage : RecentVersionLocator locate " +	
                           "[<directory number to search back from>]"); 
                System.exit(1);
            }
        }
    }
}
