/*
 * VersionManifestEntry.java
 * 
 * $Revision$
 *
 * Copyright (c) 2000-2004 MoneyGram International
 */

package dw.softwareversions;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.StringTokenizer;






import dw.framework.PauseSemaphore;
import dw.io.StreamCopier;
import dw.profile.UnitProfile;
import dw.utility.Debug;
import dw.utility.ExtraDebug;

/**
 * VersionManifestEntry describes a single file needed for the application, with
 *   its pertinent information such as size, checksum, URL, etc.
 * The first tag in its description line may be either FileName or Dependency.
 *   If it is a file name, this represents a discrete file. If it is Dependency,
 *   it represents another manifest for a package that it is dependent on.
 */
public class VersionManifestEntry {
    // fully downloaded and checksum is valid
    public static final int READY = 0;
    // not fully downloaded
    public static final int INVALID_LENGTH = 1;
    // fully downloaded and checksum is not valid
    public static final int INVALID_CHECKSUM = 2;

    protected boolean isDependency = false; // is this a manifest itself?
    protected boolean skip = false; // if true , skips checksum and file size validation.
    protected String entryType;
    protected String fileName;
    protected long fileSize;
    protected String checksum;
    protected String serverLocation;
    protected String posLocation;
    public static final String TYPE_FILE="FileName";
    public static final String TYPE_DEPENDENCY="Dependency";
    public static final String TYPE_PRE_REQUISITE="Prerequisite";
    public static final String TYPE_POST_UPGRADE="Postupgrade";
    public static final int DOWNLOAD_FILE_PRESENT=3;
    public static final int DOWNLOAD_SUCCESS=2;
    public static final int DOWNLOAD_FAIL=1;
    public static final int DOWNLOAD_PAUSED=4;
    public static final int DOWNLOAD_SUCCESS_SPLITFILE=5;
    public VersionManifestEntry() {}
    
    public VersionManifestEntry(String fileName, long fileSize, String checksum,
            String serverLocation, String posLocation,
            boolean isDependency,String type,boolean skip) {
		this.fileName = fileName;
		this.fileSize = fileSize;
		this.checksum = checksum;
		this.serverLocation = serverLocation;
		this.posLocation = posLocation;
		this.isDependency = isDependency;
		this.entryType = type;
		this.skip = skip;
	}

    /**
     * Create an entry from a String representation
     */
    public VersionManifestEntry(String string, String entryType, String parent) {
        HashMap<String, String> lineMap = getTokenized(string);
        fileName = lineMap.get("FileName");	
        if (fileName == null) {   // must be a dependency instead
            fileName = lineMap.get("Dependency");	
            isDependency = true;
        }
        this.entryType=entryType;
        serverLocation = lineMap.get("ServerLocation");	
        posLocation = lineMap.get("PosLocation");	
        String size = lineMap.get("FileSize");	
        fileSize = size == null ? 0 : Long.parseLong(size);
        checksum = lineMap.get("Checksum");	
        String skipStr = (String) lineMap.get("Skip");	
        skip = "true".equalsIgnoreCase(skipStr);
        /*
         * We have to be careful here, the manifest file in the prerequisite manifest is for the
         * base JRE, but if the prerequisite JRE is already in use, then it may have already been
         * updated, so its manifest will not match the one in the prerequisite manifest, so we need
         * to skip the file size and CRC checks on the manifest.dat file.
         */
        if ((parent.compareToIgnoreCase("prerequisiteManifest.dat") == 0) && (fileName.compareToIgnoreCase("manifest.dat")==0)) {
        	skip = true;
        }
    }

    /**
     * Determine if this file is a component of a split-up target file. If it
     *   has an additional numeric extension and its target file is also in the
     *   manifest, it is.
     */
    public boolean isComponentFile() {
        // is there more than one extension?
        if (fileName.indexOf(".") != fileName.lastIndexOf(".")){	 
            // yep, is the last extension an integer?
            String lastExt = fileName.substring(fileName.lastIndexOf(".") + 1);	
            try {
                Integer.parseInt(lastExt);
                return true;
            }
            catch (NumberFormatException e) {}
        }
        return false;
    }

    /**
     * Check to see if the file is already present in local store and valid. If
     *   not, go to the URL and get the contents of it, then save it to disk.
     * If this entry is a dependency, start a download of the manifest it refers
     *   to, then continue.
     * Any statuses and errors should bbe written out to the logStream.
     * @return whether or not the file was successfully downloaded
     * @throws InterruptedException 
     */
    
	public int download(PrintStream logStream, StringBuffer result, PauseSemaphore semaphore, String serverName, int fileCountlimit, String downloadType, boolean skipDownload, boolean deleteOriginalFile) throws IOException, InterruptedException {
		
		String remoteFileUrl = getRemoteFileUrl(serverName);
		// check if we already have it
		StringBuffer buffer = new StringBuffer();
		if (skip) {
			logStream.println("Skiping validtion for : " + fileName); 
			if (checkFileExists(buffer) == READY) {
				logStream.println("Already have file : " + fileName); 
				return DOWNLOAD_FILE_PRESENT;
			}
		} else {
			if (getStatus(buffer) == READY) {
				logStream.println("Already have file : " + fileName); 
				return DOWNLOAD_FILE_PRESENT;
			}
		}

		// logStream.println("Checking for the file " + buffer); 
		if (!skipDownload) {
			// get the file contents
			long startTime = System.currentTimeMillis();

			// if this file does not have a remote file path, it must be a file
			// that will be created by joining some component files. Skip the
			// download attempt, but do not indicate an error.
			if (remoteFileUrl == null) {
				return DOWNLOAD_SUCCESS_SPLITFILE;
			}

			ExtraDebug.println("Downloading : " + remoteFileUrl); 

			URL fileURL = new URL(remoteFileUrl);

			// create the directory and file to save the contents into
			File target = createFileAndDir(getLocalFilePath());
			FileOutputStream fos = new FileOutputStream(target);

			boolean ok = getFileContents(fileURL, result, fos);
			if (!ok) {
				logStream.println("Failed download of : " + remoteFileUrl + 
						"...expected : " + fileSize + " bytes.");  
				result.insert(0, posLocation + " FAILED. "); 
				Debug.println("SOFTWARE UPDATE FAILED: Download failed - " + "Failed download of : " + remoteFileUrl
						+ "...expected : " + fileSize + " bytes.");
				return DOWNLOAD_FAIL;
			}

			try {
				fos.flush();
				fos.close();
			} catch (Exception ignore) {
				Debug.println("SOFTWARE UPDATE FAILED: Exception: " + ignore.getMessage());
				Debug.printStackTrace(ignore);
				// ignore
			}
			logStream.println("\t" + fileName + " downloaded in " +  
					(System.currentTimeMillis() - startTime) + " mSec"); 
			return DOWNLOAD_SUCCESS;
		}
		return DOWNLOAD_PAUSED;
	}

	protected File createFileAndDir(String filePath) throws IOException {
        File file = new File(filePath);
        File fileDir = file.getParentFile();
        if (fileDir != null && ! fileDir.exists()) {
            boolean b = fileDir.mkdirs();
            if (! b) {
            	Debug.println("Directory " + fileDir.getName() + " could not be created");
            }
        }

        // wipe it out if we already have it
        if (file.exists()) {
            boolean b = file.delete();
            if (! b) {
            	Debug.println("File " + file.getName() + " could not be deleted");
            }
        }
        try {
			file.createNewFile();
		} catch (IOException e) {
			Debug.println("Error creating file["+filePath+"]["+e.getLocalizedMessage()+"]");
			throw e;
		}
        return file;
    }

    public void print() {
        Debug.println(toString());
    }

    /**
     * Gets the contents of a file from a URL and returns as a byte array. 
     * Returns a null array if we could not get the file.
     */
    protected byte[] getFileContents(URL url, int chunkSize,
                                     StringBuffer resultBuffer) {
        try {
        	Debug.println("url = " + url);
            URLConnection conn = url.openConnection();
            conn.setUseCaches(false);
            int contentSize = conn.getContentLength();
            // make sure we have the file size we are expecting
            if (contentSize != fileSize) {
                resultBuffer.append("[File sizes for ").append(url).append( 
                    " don't match. expected ").append(fileSize).append( 
                    " and file is ").append(contentSize).append("]");	
                Debug.println(resultBuffer.toString());
                return null;
            }
            InputStream is = conn.getInputStream();
            int total = 0;
            byte[] contents = new byte[contentSize];

            while (total < contentSize) {
                if ((contentSize - total) < chunkSize)
                    chunkSize = contentSize - total;
                total += is.read(contents, total, chunkSize);
            }
            is.close();
            return contents;
        }
        catch (Exception e) {
			Debug.printStackTrace(e);
            resultBuffer.append("[Exception getting file contents : " + e.getMessage() + "]");	 
            // ignore...let caller handle null return array
        }
        return null;
    }

    /**
     * Gets the contents of a file from a URL and copies into an output stream.
     * Returns false if we could not get the file. This variation is much more 
     * efficient than getFileContents(URL, int, StringBuffer) if the file
     * being downloaded is large.
     */
    protected boolean getFileContents(URL url, StringBuffer resultBuffer, 
            OutputStream out) {
        
        Debug.println("url = " + url);
        InputStream is = null;
        try {
            URLConnection conn = url.openConnection();
            conn.setUseCaches(false);
            int contentSize = conn.getContentLength();
            // make sure we have the file size we are expecting
            if (contentSize != fileSize) {
                resultBuffer.append("[File sizes for ").append(url).append( 
                    " don't match. expected ").append(fileSize).append( 
                    " and file is ").append(contentSize).append("]"); 
                Debug.println("SOFTWARE UPDATE FAILED: " + "[File sizes for " + url + 
                        " don't match. expected " + fileSize + 
                                " and file is " + contentSize + "]");
                return false;
            }
            is = conn.getInputStream();
            StreamCopier.copy(is, out);
            return true;
        }
        catch (Exception e) {
        	Debug.println("SOFTWARE UPDATE FAILED: Exception: " + e.getMessage()); 
			Debug.printStackTrace(e);
            resultBuffer.append("[Exception getting file contents : " + e.getMessage() + "]");   
            // ignore...let caller handle null return array
        }
        finally {
            if (is != null) {
                try { is.close(); } catch (Exception ignore) {} 
            }
        }
        return false;
    }
    
    
   
    
    /**
     * Return a HashMap of all the <tag>=<value> pairs found in this String.
     */
    protected HashMap<String, String> getTokenized(String line) {
        HashMap<String, String> map = new HashMap<String, String>();
        StringTokenizer commaTokenizer = new StringTokenizer(line, ",");	
        while (commaTokenizer.hasMoreTokens()) {
            String token = commaTokenizer.nextToken();
            if (token.indexOf("=") > 0) {	
                StringTokenizer equalTokenizer = new StringTokenizer(token, "=");	
                String tag = equalTokenizer.nextToken().trim();
                String value = "";	
                if (equalTokenizer.hasMoreTokens())
                    value = equalTokenizer.nextToken().trim();
                map.put(tag, value);
            }
        }
        return map;
    }

    public boolean isDependency() {
        return isDependency;
    }
    /**
     * Determine the status of the entry. Check for the presence and validity
     *   of the file that this entry refers to.
     */
    public int getStatus(StringBuffer resultBuffer) {
    	return getStatus(resultBuffer,null,false); 
    }
    /**
     * Determine the status of the entry. Check for the presence and validity
     *   of the file that this entry refers to.
     */
    public int getStatus(StringBuffer resultBuffer,String type, boolean removeUnnecessary) {
        File file = new File(getLocalFilePath());
//        Disable this seems to be printing when called by version locator during startup        
//        ExtraDebug.println("getStatus: name, length: expected, actual = " + file.getAbsolutePath() + ", " + fileSize + ", " + file.length());
        if (! file.exists() || file.length() != fileSize) {
            file = null;
            resultBuffer.append("Entry : " + this + " Failed with INVALID_LENGTH.");  
            return INVALID_LENGTH;
        }
        else if (ChecksumUtility.compare(file.toString(), checksum) !=
                                              ChecksumUtility.SUCCESS) {
            file = null;
            resultBuffer.append("Entry : " + this + " Failed with INVALID_CHECKSUM.");  
            return INVALID_CHECKSUM;
        }
        else {
            file = null;
            return READY;
        }
    }
    
    
    /*
     * Ignoring checksum and file check 
     *   
     */
    public int checkFileExists(StringBuffer resultBuffer ) {
        File file = new File(getLocalFilePath());
//        ExtraDebug.println("checkFileExists: name, exists = " + file.getAbsolutePath() + ", " + file.exists());
        if (!file.exists()) {
            resultBuffer.append("Entry : " + this + " Failed with NOT FOUND.");  
            return INVALID_LENGTH;
        }
        else {
            return READY;
        }
    }


    /**
     * Strip off the server name from a given URL so we can be sure to prepend
     *   the server name that we are talking to. NOTE : This code will assume
     *   that the base of the software version directory structure will start
     *   with a '/dw_client'. This is now necessary because for dev and qa, the
     *   server path now includes a subpath of qaenv or devenv in the URL.
     */
    protected String stripServerName(String url) {
    	if(url!=null){
	        int pos = url.indexOf("://");	
	        if (pos < 0)
	            return url;
	        url = url.substring(pos + 3);
	        pos = url.indexOf("/dw_client"); 
	        return url.substring(pos + 1);
    	}
		return "";
    }

    /** 
     * Get the URL of the file by concatenating the base server URL with
     * the server location (the path of the file on the server) and the 
     * file name. Return null if the server location is null or empty,
     * for compatibility with old getRemoteFilePath method, but that only
     * happens when doing an autojoin, which was never fully implemented.
     * New in 3.0: check for profile item that specifies different URL for
     * downloads, use it instead of serverName if set.
     */
    public String getRemoteFileUrl(String serverName) {

        if (serverLocation == null || serverLocation.length() == 0) {
            return null;
        }

        String baseUrl = UnitProfile.getInstance().get("SOFTWARE_DOWNLOAD_URL", "").trim();	 
        //Debug.println("software download url = \"" + baseUrl + "\"");
        
        if (baseUrl.length() == 0)
           baseUrl = serverName;

        //Debug.println("base url = \"" + baseUrl + "\"");
       
        StringBuffer result = new StringBuffer(120);
        result.append(baseUrl);

        if (! baseUrl.endsWith("/"))	
            result.append("/");	

        result.append(serverLocation);

        if (! serverLocation.endsWith("/"))	
            result.append("/");	
        
        result.append(fileName);
        //Debug.println("VersionManifestEntry.getRemoteFileUrl(" + serverName + ") = " + result);
        return result.toString();
    }

        
    public String getLocalFilePath() {
        String dir = posLocation + "/";	
        if (posLocation == null || posLocation.length() == 0)
            dir = "";	
        return dir + fileName;
    }

    /**
     * Convenience method for creating filename path strings. Changes all \ to
     *   / and removes leading .\
     */
    protected static String getTranslatedPath(String path) {
        if (path.equals("."))	
            path = "";	
        else if (path.indexOf(".\\") >= 0)	
            path = path.substring(2);
        path = path.replace('\\', '/');
        return path;
    }

	@Override
    public String toString() {
    	String manifestStr = (isDependency ? "Dependency=" : "FileName=") + fileName + "," +   
                "ServerLocation=" + stripServerName(serverLocation) + "," +	 
                "PosLocation=" + posLocation + "," +	 
                "FileSize=" + fileSize + "," +	 
                (skip ? "Skip=true," : "")  + 
                "Checksum=" + checksum + "\n";	 ;
		if (entryType != null) {
			if (entryType.equals(TYPE_PRE_REQUISITE)) {
				manifestStr = "Prerequisite=" + fileName + "," +   
						"ServerLocation=" + stripServerName(serverLocation) + "," +  
						"PosLocation=" + posLocation + "," +  
						"FileSize=" + fileSize + "," +  
						(skip ? "Skip=true," : "")  + 
						"Checksum=" + checksum + "\n";  ;
			} else if (entryType.equals(TYPE_POST_UPGRADE)) {
				manifestStr = "Postupgrade=" + fileName + "," +   
						"ServerLocation=" + stripServerName(serverLocation) + "," +  
						"PosLocation=" + posLocation + "," +  
						"FileSize=" + fileSize + "," +  
						(skip ? "Skip=true," : "")  + 
						"Checksum=" + checksum + "\n";  ;
			}
		}
        return manifestStr;
               
    }

    public void setFileName(String newFileName) {
        fileName = newFileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setServerLocation(String newServerLocation) {
        serverLocation = newServerLocation;
    }

    public String getServerLocation() {
        return serverLocation;
    }

    public void setPosLocation(String newPosLocation) {
        posLocation = newPosLocation;
    }

    public String getPosLocation() {
        return posLocation;
    }
    public void setChecksum(String newChecksum) {
        checksum = newChecksum;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setFileSize(long newSize) {
        fileSize = newSize;
    }

    public long getFileSize() {
        return fileSize;
    }
}
