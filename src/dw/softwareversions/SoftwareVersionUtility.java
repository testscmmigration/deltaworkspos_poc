/*
 * SoftwareVersionUtility.java
 * 
 * $Revision$
 *
 * Copyright (c) 2000-2004 MoneyGram International
 */

package dw.softwareversions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JProgressBar;

import dw.comm.MessageFacade;
import dw.comm.MessageFacadeError;
import dw.comm.MessageFacadeParam;
import dw.comm.MessageLogic;
import dw.dialogs.UpdateStatusDialog;
import dw.framework.PauseSemaphore;
import dw.io.StreamCopier;
import dw.model.adapters.CountryInfo;
import dw.profile.UnitProfile;
import dw.utility.Debug;
import dw.utility.ErrorManager;
//import dw.utility.ExtraDebug;
import dw.utility.TimeUtility;

/**
 * SoftwareversionUtility is a utility class for handling software versions. It
 *    does the creation of manifests, validation of versions, and downloading of
 *    new versions.
 * @author Rob Campbell
 */
public class SoftwareVersionUtility {
    private static int maxVersions = 5;  // how many versions do we allow?
    private static int jreMaxVersions = 3;  // how many versions do we allow?
    private static Pattern VERSION_PATTERN = Pattern.compile("\\d\\d\\d\\d\\d\\d");
    private static Pattern JRE_VERSION_PATTERN = Pattern.compile("jre((\\d{3})|(\\d{4}))z");
    // used to prohibit multiple concurrent downloads
    private static boolean softwareDownloadInProgress = false;
    private static Map<String, SoftwareStatus> SOFTWARE_STATUS_MAP = new HashMap<String, SoftwareStatus>();

    static public class SoftwareStatus {
    	private int count;
    	private int number;
    	private String jre;

    	SoftwareStatus(int count, int number, String jre) {
    		this.count = count;
    		this.number = number;
    		this.jre = jre;
    	}

		public int getCount() {
			return count;
		}

		public int getNumber() {
			return number;
		}

		public String getJre() {
			return jre;
		}
    }
    
    /**
     * Set the count for the maximum number of versions we want to store on the
     *   POS before we start deleting old ones to make room the new.
     * @param max the new max number of versions to store
     */
    public static void setMaxVersions(int max) {
        maxVersions = max;
    }

    /**
     * Copy the contents of an existing version directory to a new version
     *   directory. This will be used when we want to create a new version by
     *   patching an existing version instead of downloading a whole new version
     * @param newDirectory the directory name of the new version to create
     * @param oldDirectory the directory name of the version to copy from
     */
    public static void copyDirectory(String oldDirectory, String newDirectory) {
        try {
            Debug.println("copyDirectory(" + oldDirectory + ", " + newDirectory + ")");
            File oldDir = new File(oldDirectory);
            if (! oldDir.exists())
                return;
            
            File newDir = new File(newDirectory);
            if (newDir.exists() || newDir.mkdir()) {
                // grab the contents of the old and put em in the new
                File[] files = oldDir.listFiles();
                String fromFile = "";
                if (files != null) {
	                for (int i = 0; i < files.length; i++) {
	                    if ((! files[i].isDirectory()) && (! files[i].getName().startsWith("manifest"))) {
							fromFile = oldDirectory + "\\" + files[i].getName(); 
							if (fromFile.length() > 0) {
								copyFile(fromFile,
										newDirectory + "\\" + files[i].getName()); 
							}
	                    }
	                }
                }
            }
        }
        catch (IOException e) {}
    }

    /**
     * Copy the contents of one file to another file. If the new file already
     *   exists, leave it alone.
     * @param of the name of the file to copy from
     * @param nf the name of the file to copy to
     */
    public static void copyFile(String of, String nf) throws IOException {
        File oldFile = new File(of);
        if (! oldFile.exists())
            return;
        File newFile = new File(nf);
        if (newFile.exists())
            return;
        
        boolean f = newFile.createNewFile();
        if (! f) {
        	Debug.println("File " + newFile.getName() + " already existed");
        }
        FileInputStream in = new FileInputStream(oldFile);
        FileOutputStream out = new FileOutputStream(newFile);
        StreamCopier.copy(in, out);
        out.flush();
        out.close();
        in.close();
        Debug.println("copyFile(" + of + ", " + nf + ") done");
    }

    /**
     * Validate the version in the current directory and place the return code
     *  in %ERRORLEVEL% through System.exit(x).
     * Return codes : 0 = version is valid, 1 = version not valid
     * @param versionNumber the number of the version (directory) to validate
     */
	public static void validateVersion(String versionNumber, String jreVersion) {
		//dw.comm.AgentConnect.enableExtraDebug(); // TEMPORARY: DO NOT RELEASE
													// WITHOUT COMMENTING THIS
													// OUT [EEO]
		//dw.comm.AgentConnect.enableDebugEncryption(); // TEMPORARY: DO NOT
														// RELEASE WITHOUT
														// COMMENTING THIS OUT
														// [EEO]
		// get the manifest from local store
		StringBuffer buffer = new StringBuffer();
		boolean valid = true;
		try {
			VersionManifest manifest = new VersionManifest(versionNumber);
			// manifest is loaded...ask it if version is all here and valid
			valid = manifest.isValid(buffer);
//			ExtraDebug.println("Version Manifest :"+valid);
			if (valid && jreVersion != null && jreVersion.length() > 0) {
				VersionManifest jreManifest = new VersionManifest(versionNumber);
				valid = jreManifest.isValid(new StringBuffer());
//				ExtraDebug.println("JRE Manifest :"+valid);
				VersionManifest dependencyManifest = manifest.getDependency();
				if (dependencyManifest != null) {
					String appJre = dependencyManifest.getVersionNumber();
					int indexOfJre = 3;// JRe name is expected to start with Jre
										// and
					// followed by the number which indicates
					// the version
					String jreNumber = jreVersion.substring(indexOfJre, jreVersion.length());
					appJre = appJre.substring(indexOfJre, appJre.length()-1);
					try {
						int jreVal = Integer.parseInt(jreNumber);
						int appJreVal = Integer.parseInt(appJre);
						if (jreVal != appJreVal) {
							valid = false;
//							ExtraDebug.println("JRE not valid jreVal:"+jreVal+"--appJreVal--"+appJreVal);
						}
					} catch (NumberFormatException e) {
						valid = false;
					}
				}

			}
		} catch (IOException e) {
			// most likely the file was not found...
			valid = false;
			Debug.printException(e);
		}
		if (!valid){
//			ExtraDebug.println("validateVersion :"+buffer);
			Debug.println("Version " + versionNumber + " failed validation...");  			
		}
		System.exit(valid ? 0 : 1);
	}

	
	public static String getReqJreVersion(String versionNumber) {
		try {
			VersionManifest manifest = new VersionManifest(versionNumber);
			boolean valid = manifest.isValid(new StringBuffer());
			VersionManifest dependencyManifest = manifest.getDependency();
			if (valid && dependencyManifest != null) {
				return dependencyManifest.getVersionNumber();
			}
		} catch (IOException e) {

		}
		return null;
	}
    /**
     * Delete a given version directory. Recurse into any subdirectories that
     *   may be present.
     * @param directory the directory to be deleted
     */
    public static void deleteDirectory(String directory) {
        File dir = new File("" + directory);	

        // delete the contents of the directory
        File[] files = dir.listFiles();
        if (files != null) {
	        for (int i = 0; i < files.length; i++) {
	            if (files[i].isDirectory())
	                deleteDirectory(files[i].getPath());
	            else {
	                boolean b = files[i].delete();
	                if (! b) {
	                	Debug.println("File " + files[i].getName() + " could not be deleted");
	                }
	            }
	        }
        }

        // delete the now-empty directory
        boolean b = dir.delete();
        if (! b) {
        	Debug.println("Directory " + dir.getName() + " could not be deleted");
        }
    }

    /**
     * Delete all versions that are not needed anymore. If we have more versions
     *   than the max allowed, we will delete the lowest numbered versions until
     *   we are left with the max.
     * @param maxVersions the max number of versions to be stored on this POS.
     * @param version : Current DW version 
     */
	public static void deleteOutdatedVersions(int maxVersions, String version) {
		// get all the versions available to this POS
		Integer[] versions = RecentVersionLocator.getAllVersions();
		if (versions.length > maxVersions) {
			for (int i = 0; i < versions.length - maxVersions; i++) {
				if (version != null && !version.isEmpty()) {
					if (versions[i].toString().equals(version)) {
						continue;
					}
				}
				deleteDirectory(versions[i].toString());
			}
		}
	}
    
    public static void deleteOutdatedJreVersions(int maxVersions) {
        // get all the versions available to this POS
        String[] versions = RecentVersionLocator.getAllJreVersions();
    	Matcher m = null;
        if (versions.length > maxVersions)
            for (int i = 0; i < versions.length - maxVersions; i++) {
        		m = JRE_VERSION_PATTERN.matcher(versions[i].trim());
        		if (m.matches())
        			deleteDirectory(versions[i]);
            }
    }


    /**
     * De-activate the given version. This is accomplished by deleting the
     *   manifest file for that version. This will be used in the case of
     *   rolling back to a previous version if the new version does not function
     *   properly.
     * @param versionNumber the version to be deactivated.
     */
    public static void deactivateVersion(int versionNumber) {
        File manifest = new File(versionNumber + "/manifest.dat"); 
        if (manifest.exists()) {
            boolean b = manifest.delete();
            if (! b) {
            	Debug.println("File " + manifest.getName() + " could not be deleted");
            }
        }
    }

	public static void deactivateJre(int versionNumber) {
		File manifest = new File("jre" + versionNumber+"/manifest.dat"); 

		Debug.println("Deactivating JRE::" + versionNumber);
		if (manifest.exists()) {
			//delete(manifest);
			boolean b = manifest.delete();
	        if (! b) {
	        	Debug.println("File " + manifest.getName() + " could not be deleted");
	        }
		}
	}
    /**
     * Deactivate all versions that are newer than the given version.
     * @param version the version we want to keep.
     * @param out a PrintStream to use for displaying statuses.
     */
    public static void deactivateNewerVersions(String version, PrintStream out){
        out.println("Rolling back to version " + version + "..." +	 
                    "deactivating newer versions..."); 
        int mostRecent = RecentVersionLocator.getMostRecent(1);
        int current = Integer.parseInt(version);
        while (mostRecent > current) {
            out.println("  - deactivating version : " + mostRecent);	
            deactivateVersion(mostRecent);
            mostRecent = RecentVersionLocator.getMostRecent(mostRecent);
        }
    }
    /**
     * Deactivate all JRE newer than the JRE used by the given version.
     */
	public static void deactivateNewerJres(String version, PrintStream out) {
		String reqJre = getReqJreVersion(version);
		if (reqJre != null && reqJre.toUpperCase(Locale.US).startsWith("JRE")) {
			int reqJreVersion = Integer.parseInt(reqJre.substring(3,reqJre.length()-1));

			Integer[] jres = RecentVersionLocator.getAllUnzippedJreVersions();
			for (int i = 0; i < jres.length; i++) {
				if (jres[i].intValue() > reqJreVersion) {
					out.println("Deactivating JREs..."+jres[i].intValue()); 
					deactivateJre(jres[i].intValue());
				}
			}

		}
	}
    /**
     * Download the new version of the software in a background thread. Write
     *   out the status of the download to the diag log also.
     * 1. Send a message to the host asking for the new manifest information.
     * 2. Get the manifest file.
     * 3. Copy the contents of the current version directory to the new
     *       version directory.
     * 4. Download all the files listed in the manifest that we don't already
     *       have from copying the current version.
     * 5. Join together any component files into their original form, and then
     *       delete the components.
     * 6. Delete any files in the new version directory that are not listed in
     *       the manifest.
     * 7. If we have a completed new valid version, check to see if it is an
     *     older version (rolling back), and then deactivate newer versions.
     * 8. If we now have more than the max # of versions on the POS, delete the
     *     oldest version.
     * @param pauseSemaphore the semaphore object that allows the process to
     *    be paused and resumed.
     * @param promptForRestart whether or not to prompt the user before restart
     *    of the app when a successful download is complete. If false, will
     *    restart automatically.
     */
    public static void handlePendingVersion(final PauseSemaphore semaphore, final int reterivalType, final JProgressBar progressBar, final VersionManifest manifest) {
        final MessageFacadeParam parameters = new MessageFacadeParam(MessageFacadeParam.NO_DIALOG);

        // ignore possible multiple invocations
        if (softwareDownloadInProgress)
            return;
        // start the copy/download of the manifest contents in a thread
        
	        ErrorManager.softwareDownloadStarted();
	
	        // wait a couple seconds for any previous hangup to finish
	        try {
	            Thread.sleep(5000);
	        }
	        catch (InterruptedException e) {}
	
	        softwareDownloadInProgress = true;
	
	        // get the current version that we are running
	        final String currentVersion = UnitProfile.getInstance().getVersionNumber(false);
			Matcher m1 = VERSION_PATTERN.matcher(currentVersion);
	
	        // get ready to log the progress of the download
	        final PrintStream logStream = getFileLogStream();
			if (!m1.matches()) {
				logStream
						.println("Invalid version number : " + currentVersion); 
				logStream.println("Software download aborted."); 
				return;
			}
	        logStream.println("Starting pending download at : "	
	                + new Date(TimeUtility.currentTimeMillis()));
	
	        MessageFacade.run(parameters, new MessageLogic() {
	            @Override
				public Object run() {
	                boolean success = true;
	                
	                // keep track of what happened to the download if it failed
	                StringBuffer reasonBuffer = new StringBuffer();
	
	                // get the manifest for the version we should be running
	                try {
	                    if (manifest == null) {
	                    	Debug.println("SOFTWARE UPDATE FAILED: manifest object is null");
	                        reasonBuffer.insert(0, "FAILED."); 
	                        logStream.println("Software download failed."); 
	                        success = false;                        
	                    }
	                    else { // if (manifest != null) {
							String newVersion = manifest.getVersionNumber().trim(); 
							Matcher m2 = VERSION_PATTERN.matcher(newVersion);
	
							if (!m2.matches()) {
		                    	Debug.println("SOFTWARE UPDATE FAILED: Invalid version number - " + newVersion);
								logStream
										.println("Invalid version number : " + newVersion); 
								reasonBuffer.insert(0, "FAILED."); 
								logStream.println("Software download failed."); 
								success = false;
	
							} else {
								String serverName = "";
								try {
									// first copy all the files from the current
									// version
									// to the new directory...for incremental
									// downloads
									copyDirectory(currentVersion, newVersion);
	
									serverName = getServerName();
	
									// now download the files we need for the new
									// version
	
									int flag = manifest.download(logStream, reasonBuffer, semaphore, serverName, 0, null, false, false);
									// vmi.getCurrentServerNameWithProtocol());
									if (flag == VersionManifestEntry.DOWNLOAD_SUCCESS) {
										success = true;
									}
									// we got all the files..is the manifest valid?
									if (success) {
										if (manifest.isValid(reasonBuffer)) {
											// get rid of any files not in the
											// manifest
											manifest.synchronizeDirectory();
										} else {
											Debug.println("SOFTWARE UPDATE FAILED: Manifest is invalid - " + reasonBuffer.toString());
											success = false;
											reasonBuffer.insert(0, newVersion
													+ " FAILED."); 
											logStream
													.println("Failed...Version is invalid."); 
										}
									}
	
									if (success) {
										// check for older version and deactivate
										// newers
										String version = manifest
												.getVersionNumber();
										if (UnitProfile.getInstance()
												.compareVersions(version) < 0
												&& manifest.isValid(reasonBuffer)) {
											deactivateNewerVersions(version,
													logStream);
											
											deactivateNewerJres(version,
													logStream);
										}
										// delete any outdated versions
										deleteOutdatedVersions(maxVersions,version);
										deleteOutdatedJreVersions(jreMaxVersions);
										// notify the ErrorManager about the new
										// version
										reasonBuffer.append(newVersion
												+ " SUCCESSFUL."); 
										logStream
												.println("Software download succeeded."); 
									} else {
										logStream
												.println("Software download failed."); 
									}
									// logStream.close();
								} catch (Exception e) {
									if (logStream != null) {
										reasonBuffer
												.append("FAILED : [Reason Unknown : " + e.getMessage() + "]");  
										logStream
												.println("Exception while downloading manifest :"); 
										Debug.dumpStack();
										String exception = Debug.getStackTrace(e);
										logStream.println(exception);
										logStream
												.println("Software download failed."); 
										success = false;
									}
									Debug.println("SOFTWARE UPDATE FAILED: Exception thrown - " + e.getMessage());
									Debug.printStackTrace(e);
								}
							}
						}
					}
	                finally {
	                    ErrorManager.softwareDownloadFinished(success,
	                            reasonBuffer.toString());
	                    softwareDownloadInProgress = false;
	                }
	                return Boolean.TRUE;
	            }


	        }, null);
    }
//    public synchronized int getCurrentDownloadStatus(String type){
//    	return getCurrentDownloadStatus(type);
//    }
    
    public synchronized int getCurrentDownloadStatus(String type, VersionManifest manifest, int reterivalType) {
		StringBuffer reasonBuffer = new StringBuffer();
		try {
			if (manifest == null) {
				manifest = MessageFacade.getInstance().getCurrentManifest(reasonBuffer, reterivalType);
			}
			
			if (manifest != null) {
				return manifest.getStatus(reasonBuffer,type,true);
			}
		} catch (MessageFacadeError e) {
			manifest = null;
		}
		Debug.println("JRE MANIFEST NOT PRESENT");
		return -1; 
    }
    
    
    public synchronized VersionManifest getCurrentManifest() {
		StringBuffer reasonBuffer = new StringBuffer();
		VersionManifest manifest = null;
		try {
			manifest = MessageFacade.getInstance().getCurrentManifest(reasonBuffer, CountryInfo.MANUAL_TRANSMIT_RETERIVAL);
		} catch (MessageFacadeError e) {
			Debug.println("JRE MANIFEST NOT PRESENT");
			manifest = null;
		}
		return manifest;
	}
    

	public int handlePrePostUpgrade(PauseSemaphore semaphore, String type, int fileLimitCount, boolean skipDownload, VersionManifest manifest) {
		StringBuffer reasonBuffer = new StringBuffer();
		final PrintStream logStream = getFileLogStream();
		String serverName = getServerName();
		int manifestSuccess = 0;

		try {
			if (manifest != null) {
				manifestSuccess = manifest.downloadCustom(logStream, reasonBuffer, semaphore, serverName, type, fileLimitCount, skipDownload);
			}
		} catch (MessageFacadeError e) {
			Debug.println("JRE MANIFEST NOT PRESENT");
		} catch (IOException e) {
			if (logStream != null) {
				logStream.println("Could not download " + type);
				logStream.println("Exception is : " + e); 
			}
		} catch (InterruptedException e) {
			Debug.println("JRE Download interruped..");
		}
		// manifestSuccess=0;
		return manifestSuccess;
	}

	public static void setDownloadStatus(String type, int current, int maximum, String jreName) {
		SoftwareStatus ss = new SoftwareStatus(current, maximum, jreName);
		SOFTWARE_STATUS_MAP.put(type, ss);
		UpdateStatusDialog.getInstance().refreshDownloadStatus(type, ss);
	}
	
	public static SoftwareStatus getDownloadStatus(String type) {
		SoftwareStatus downloadStatus = SOFTWARE_STATUS_MAP.get(type);
		if (downloadStatus != null) {
			Debug.println(type + " Jre status :: Downloaded " + downloadStatus.count + " of " + downloadStatus.number);
		} else {
			Debug.println(" No " + type + " Jre available");
		}
		return downloadStatus;
	}

	public static void clearDownladStatus() {
		SOFTWARE_STATUS_MAP.clear();
	}
	
	private static String getServerName() {
		String location;
		String serverName = null;
		if (UnitProfile.getInstance().isDialUpMode())
			location = UnitProfile.getInstance().get(
					"SSL_DIALUP_URL", "");
		else
			location = UnitProfile.getInstance().get(
					"SSL_NETWORK_URL", "");

		int endIndex = location.indexOf("servlet");

		if (endIndex > 0)
			serverName = location
					.substring(0, endIndex);
		return serverName;
	}
	/** 
     * Get a print stream to use for logging software updates/validations.
     * Since we now have rotating trace files and debug transmits, this
     * just returns System.out, which will usually have been redirected
     * to the debug log. But it is probably an error to close this stream!
     */
    private static PrintStream getFileLogStream() {
        return System.out;
    }

    public static String pad5(String version) {
        int needed = (5 - version.length());
        for (int i = 0; i < needed; i++)
            version = "0" + version;	
        return version;
    }

    public static void synchronizeWithManifest(String version) {
        try {
            VersionManifest manifest = new VersionManifest(version);
            manifest.synchronizeDirectory();
        }
        catch (IOException e) {
        	Debug.printException(e);
        }
    }

    public static void validateVersion(String[] args) {
		if (args != null) {
			if (args.length == 2) {
				SoftwareVersionUtility.validateVersion(args[1], null);
			} else if (args.length == 3) {
				SoftwareVersionUtility.validateVersion(args[1], args[2]);
			}
		} else {
			Debug.println("Invalid Version details");
		}

	}
}
