package dw.softwareversions;

import java.io.File;
import java.io.FileInputStream;
import java.util.zip.CRC32;

/**
 * Compare the calculated CRC32 checksum of a file to a supplied CRC32 checksum
 *   value. Return the system exit code corresponding to the results of the
 *   compare. Return values:
 *   SUCCESS          : file matched given checksum
 *   NO_MATCH         : file did not match given checksum
 *   NO_FILE          : file named did not exist
 *   EXCEPTION        : unspecified exception occurred
 */
public class ChecksumUtility {
    // return codes for comparing checksums
    public static final int SUCCESS = 0;
    public static final int NO_MATCH = 1;
    public static final int NO_FILE = 2;
    public static final int EXCEPTION = 3;

    /**
     * Compare the given file to the given checksum.
     * @param fileName the name of the file to calculate the checksum for
     * @param checksum the correct checksum value to compare against
     */
    public static int compare(String fileName, String checksum) {
        try {
            long actualValue = 0;
            long suppliedValue = 0;
            String checkStr = getChecksum(fileName);
            if (checkStr.equals("no file"))	{
                return NO_FILE;
            }
            suppliedValue = Long.parseLong(checksum);
            actualValue = Long.parseLong(checkStr);
            if (actualValue == suppliedValue){
                return SUCCESS;
            }
            else{
            	//Debug.println(fileName+":checksum mismatch Expected::"+suppliedValue+"::actualValue::"+actualValue);
                return NO_MATCH;
            }
        }
        catch (Exception e) {
            //Debug.println(e);
            return EXCEPTION;
        }
    }

    /**
     * Get the CRC32 checksum of a given file.
     * @param fileName the name of the file to calculate the checksum for.
     */
    public static String getChecksum(String fileName) {
        try {
            File file = new File(fileName);
            if (! file.exists()) {
                return "no file"; 
            }
            CRC32 crc = new CRC32();
            FileInputStream fis = new FileInputStream(file);
            
            // run crc block at a time
            int result;
			byte[] fileBytes = new byte[1000];
			while((result = fis.read(fileBytes)) != -1){
				crc.update(fileBytes,0,result);
			}
			// close file input stream0
			fis.close();
            

			return "" + crc.getValue();	
        }
        catch (Exception e) {
            return "";	
        }
    }

    /**
     * If the filename only is supplied, print out the checksum to std. out.
     *   If both the filename and a value are supplied, compare the 2 and
     *   set the system exit code accordingly.
     */
//    public static void main(String args[]) {
//        if (args.length < 1) {
//            Debug.println("Usage : ChecksumUtility <fileName> " +	
//                                  "[<CRC32 compare value>]"); 
//            System.exit(1);
//        }
//        if (args.length == 1) {
//            Debug.println("Checksum for " + args[0] + " = " +	 
//                             getChecksum(args[0]));
//        }
//        else {
//            System.exit(compare(args[0], args[1]));
//        }
//    }
}
