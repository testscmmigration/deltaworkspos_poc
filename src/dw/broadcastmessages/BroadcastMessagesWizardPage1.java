package dw.broadcastmessages;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import dw.broadcastmessages.BroadcastMessagesTransaction.BroadcastMessage;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.utility.Base64;
import dw.utility.Debug;
import dw.utility.TimeUtility;

/**
 * Page Code: BMW01
 * 
 * Page 1 of the Broadcast Messages wizard. This page displays the broadcast 
 * messages.
 */
public class BroadcastMessagesWizardPage1 extends BroadcastMessagesFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;
	
	static private final int MINUMUM_SCREEN_WIDTH = 480;
	private final int SUBJECT_HEADER_WIDTH;
	
	private List<BroadcastMessage> messages;
	private JScrollPane scrollPane1;
	private JScrollPane scrollPane2;
	private JPanel panel2;
	private List<DWTextPane> subjectTextPaneList;
	private List<DWTextPane> contentTextPaneList;
	private boolean firstShowFlag;
	private JLabel noMessages;
	private JPanel title;
	private JPanel firstMessagePanel = null;
	private JPanel panel1;
	
	public BroadcastMessagesWizardPage1(
			BroadcastMessagesTransaction tran, String name,
			String pageCode, PageFlowButtons buttons) {
		super(tran, "broadcastmessages/BroadcastMessagesWizard1.xml", name,
				pageCode, buttons);
		SUBJECT_HEADER_WIDTH = (new JLabel(Messages.getString("BroadcastMessagesWizardPage1.2"))).getPreferredSize().width + 5;
		setupComponents();
	}
	
	private void setupComponents() {
		scrollPane1 = (JScrollPane) getComponent("scrollPane1");
		scrollPane2 = (JScrollPane) getComponent("scrollPane2");
		panel1 = (JPanel) getComponent("panel1");
		panel2 = (JPanel) getComponent("panel2");
		title = (JPanel) getComponent("title");
		noMessages = (JLabel) getComponent("noMessages"); 
		
		this.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				dwTextPaneResized();
			}
		});
		
		// Add a key listener.
		
		final JButton cancelButton = flowButtons.getButton("cancel"); 
		KeyListener nexter = new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					e.consume();
					cancelButton.doClick();
				}
			}
		};
		cancelButton.addKeyListener(nexter);
		cancelButton.requestFocus();
	}
	
	private String removeUnicode(byte[] bytes) {
		
		StringBuffer sb = new StringBuffer();
		try {
			String text = new String(bytes, "UTF-8");
			sb.append(text);
		} catch (UnsupportedEncodingException e) {
			sb.append(Messages.getString("BroadcastMessagesWizardPage1.5"));
			Debug.println("Invalid text in broadcast message: " + new String(bytes));
		}

		int j = 0;
		while (true) {
			int i = sb.indexOf("\\u", j);
			if (i == -1) {
				break;
			}
			
			String n = sb.substring(i + 2, i + 6);
			try {
				char c = (char) Integer.parseInt(n, 16);
				sb.replace(i, i + 6, String.valueOf(c));
				j = i + 1;
			} catch (Exception e) {
				j = i + 6;
			}
		}
		return sb.toString();
	}

	@Override
	public void start(int direction) {
		PageNotification.notifyExitListeners(this, PageExitListener.DONOTHING);
		flowButtons.reset();
		flowButtons.setVisible("expert", false);
		flowButtons.setVisible("back", false);
		flowButtons.setVisible("next", false);
		
		// Remove previous messages from the screen
		
		panel2.removeAll();
		subjectTextPaneList = new ArrayList<DWTextPane>();
		contentTextPaneList = new ArrayList<DWTextPane>();
		
		messages = transaction.getBroadcastMessages(null);
		
		if ((messages != null) && (messages.size() > 0)) {
			scrollPane2.setVisible(true);
			noMessages.setVisible(false);
			
			Font boldFont = (new JLabel()).getFont().deriveFont(Font.BOLD);
			
			// Determine size of labels and text on screen.
			
			JLabel subjectLabel = new JLabel(Messages.getString("BroadcastMessagesWizardPage1.2"));
			JLabel dateLabel = new JLabel(Messages.getString("BroadcastMessagesWizardPage1.3"));
			
			int w1 = (Math.max(subjectLabel.getPreferredSize().width, dateLabel.getPreferredSize().width)) + 10;
			int w2 = 0;
			for (int i = 0; i < messages.size(); i++) {
				BroadcastMessage message = messages.get(i);
				JLabel l = new JLabel(""+message.getId());
				w2 = Math.max(w2, l.getPreferredSize().width);
			}
			Dimension d1 = new Dimension(w1, 20);
			Dimension d2 = new Dimension(w2, 20);
			
			boolean newMessageViewedFlag = false;
			
			for (int i = 0; i < messages.size(); i++) {
				BroadcastMessage message = messages.get(i);
				
				// Message panel
				
				JPanel messagePanel = new JPanel(new GridBagLayout());
				if (firstMessagePanel == null) {
					firstMessagePanel = messagePanel;
				}
				GridBagConstraints gbc = new GridBagConstraints();
				gbc.gridx = 0;
				gbc.gridy = i;
				gbc.weightx = 1;
				gbc.fill = GridBagConstraints.HORIZONTAL;
				gbc.insets = new Insets(0, 0, 5, 0);
				panel2.add(messagePanel, gbc);
				messagePanel.setBorder(BorderFactory.createTitledBorder(""));
				
				// Subject label
				
				JLabel label = new JLabel(Messages.getString("BroadcastMessagesWizardPage1.2"));
				label.setPreferredSize(d1);
				label.setFont(boldFont);
				gbc = new GridBagConstraints();
				gbc.gridx = 0;
				gbc.gridy = 0;
				gbc.anchor = GridBagConstraints.NORTHWEST;
				messagePanel.add(label, gbc);
				
				// Subject value
				
				byte[] bytes = Base64.decodedBase64Item(message.getSubject());
				String text = removeUnicode(bytes);
				DWTextPane tp = new DWTextPane();
				subjectTextPaneList.add(tp);
				tp.setInvalidContentDisplayFlag(true);
				tp.setText(text);
				
				gbc = new GridBagConstraints();
				gbc.gridx = 1;
				gbc.gridwidth = 3;
				gbc.gridy = 0;
				gbc.weightx = 1;
				gbc.fill = GridBagConstraints.HORIZONTAL;
				gbc.anchor = GridBagConstraints.WEST;
				messagePanel.add(tp, gbc);
				
				// Date label
				
				label = new JLabel(Messages.getString("BroadcastMessagesWizardPage1.3"));
				label.setPreferredSize(d1);
				label.setFont(boldFont);
				gbc = new GridBagConstraints();
				gbc.gridx = 0;
				gbc.gridy = 1;
				messagePanel.add(label, gbc);
				
				// Date value
				
				label = new JLabel(new SimpleDateFormat(TimeUtility.DATE_MONTH_DAY_LONG_YEAR_FORMAT).format(message.getStartDate()));
				gbc = new GridBagConstraints();
				gbc.gridx = 1;
				gbc.gridy = 1;
				gbc.weightx = 1;
				gbc.fill = GridBagConstraints.HORIZONTAL;
				messagePanel.add(label, gbc);
				
				//
				
				gbc = new GridBagConstraints();
				gbc.gridx = 2;
				gbc.gridy = 1;
				gbc.weightx = 1;
				gbc.fill = GridBagConstraints.HORIZONTAL;
				messagePanel.add(new JLabel(), gbc);
				
				// Message Id label
				
				label = new JLabel(Messages.getString("BroadcastMessagesWizardPage1.6"));
				label.setFont(boldFont);
				gbc = new GridBagConstraints();
				gbc.gridx = 3;
				gbc.gridy = 1;
				gbc.insets = new Insets(0, 5, 0, 5);
				messagePanel.add(label, gbc);
				
				// Message Id value
				
				label = new JLabel(""+message.getId());
				label.setPreferredSize(d2);
				gbc = new GridBagConstraints();
				gbc.gridx = 4;
				gbc.gridy = 1;
				messagePanel.add(label, gbc);
				
				// Message content

				tp = new DWTextPane();
				contentTextPaneList.add(tp);
				tp.setInvalidContentDisplayFlag(true);
				
				bytes = Base64.decodedBase64Item(message.getContent());
				text = removeUnicode(bytes);
				tp.setText(text);
				
				gbc = new GridBagConstraints();
				gbc.gridx = 0;
				gbc.gridwidth = 4;
				gbc.gridy = 2;
				gbc.weightx = 1;
				gbc.fill = GridBagConstraints.HORIZONTAL;
				gbc.insets = new Insets(0, 0, 5, 0);
				messagePanel.add(tp, gbc);

				// Check if the message has never been viewed before.
				
				if (! message.isViewed()) {
					newMessageViewedFlag = true;
					message.setViewed(true);
				}
			}
			
			// If a message has been viewed for the first time, update the
			// message cache file and the button text.
			
			if (newMessageViewedFlag) {
				transaction.writeBroadcastMessages();
				transaction.updateMessageMenuButton();
			}
			
		} else {
			scrollPane2.setVisible(false);
			noMessages.setVisible(true);
		}
		
		firstShowFlag = true;
	}

	@Override
	public void finish(int direction) {
		PageNotification.notifyExitListeners(this, direction);
	}

	@Override
	public void dwTextPaneResized() {
		if (scrollPane2.isVisible()) {
			int width1 = scrollPane1.getVisibleRect().width;
			int width2 = width1 * 4 / 5;
			int width3 = Math.min(Math.max(width1, MINUMUM_SCREEN_WIDTH), width2);
			
			if (subjectTextPaneList.size() > 0) {
				for (int i = 0; i < subjectTextPaneList.size(); i++) {
					DWTextPane tp = subjectTextPaneList.get(i);
					tp.setWidth(width3 - SUBJECT_HEADER_WIDTH);
				}
			}
			
			if (contentTextPaneList.size() > 0) {
				for (int i = 0; i < contentTextPaneList.size(); i++) {
					DWTextPane tp = contentTextPaneList.get(i);
					tp.setWidth(width3);
				}
			}
			
		} else {
			title.setPreferredSize(new Dimension(MINUMUM_SCREEN_WIDTH, 23));
		}
		
		int y1 = scrollPane2.getVerticalScrollBar().getValue();
		int y2 = scrollPane2.getVerticalScrollBar().getMaximum() - scrollPane2.getVerticalScrollBar().getSize().height;
		
		scrollpaneResized(scrollPane1, scrollPane2, panel2);
		
		if (scrollPane2.getVerticalScrollBar().isVisible()) {
			if (firstShowFlag) {
				Thread t = new Thread() {
					@Override
					public void run() {
						int y = scrollPane2.getVerticalScrollBar().getValue();
						do {
							y /= 4;
						} while (y > 0);
						
						scrollPane2.revalidate();
						panel1.revalidate();
					}
				};
				SwingUtilities.invokeLater(t);
				firstShowFlag = false;
			} else {
				int y3 = scrollPane2.getVerticalScrollBar().getMaximum() - scrollPane2.getVerticalScrollBar().getSize().height;
				int y4 = y1 * y3 / y2;
				scrollPane2.getVerticalScrollBar().setValue(y4);
			}
		}
	}
}
