package dw.broadcastmessages;

import dw.framework.ClientTransaction;
import dw.framework.PageNameInterface;
import dw.framework.TransactionInterface;

public abstract class BroadcastMessagesTransactionInterface extends
		TransactionInterface {
	private static final long serialVersionUID = 1L;

	protected BroadcastMessagesTransaction transaction;

	public BroadcastMessagesTransactionInterface(String fileName,
			BroadcastMessagesTransaction transaction,
			PageNameInterface naming, boolean sideBar) {
		super(fileName, naming, sideBar);
		this.transaction = transaction;
	}

	@Override
	public ClientTransaction getTransaction() {
		return transaction;
	}
}
