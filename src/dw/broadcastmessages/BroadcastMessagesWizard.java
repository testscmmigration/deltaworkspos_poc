package dw.broadcastmessages;

import java.awt.Component;
import java.awt.event.KeyEvent;

import dw.framework.FlowPage;
import dw.framework.KeyMapManager;
import dw.framework.PageExitListener;
import dw.framework.PageNameInterface;

public class BroadcastMessagesWizard extends BroadcastMessagesTransactionInterface {
	private static final long serialVersionUID = 1L;

	public BroadcastMessagesWizard(
			BroadcastMessagesTransaction transaction,
			PageNameInterface naming) {
		super("broadcastmessages/BroadcastMessagesWizardPanel.xml", transaction, naming,
				false);
		createPages();
	}

	@Override
	protected void createPages() {
		addPage(BroadcastMessagesWizardPage1.class, transaction,
				"BroadcastMessagesTable", "BMW01", buttons);
	}

	/**
	 * Add the single keystroke mappings to the buttons. Use the function keys
	 * for back, next, cancel, etc.
	 */
	private void addKeyMappings() {
		KeyMapManager.mapMethod(this, KeyEvent.VK_F2, 0, "f2SAR");
		buttons.addKeyMapping("cancel", KeyEvent.VK_ESCAPE, 0);
	}

	@Override
	public void start() {
		transaction.clear();
		super.start();
		addKeyMappings();
	}

	/**
	 * Page traversal logic here.
	 */
	@Override
	public void exit(int direction) {
		if (direction == PageExitListener.DONOTHING) {
			return;
		}
		super.exit(direction);
	}

	public void f2SAR() {
		Component current = cardLayout.getVisiblePage();
		if (current instanceof FlowPage) {
			((FlowPage) current).updateTransaction();
		}
		transaction.f2SAR();
	}
}
