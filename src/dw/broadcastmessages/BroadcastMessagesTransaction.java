package dw.broadcastmessages;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.CRC32;

import javax.swing.JButton;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.moneygram.agentconnect.GetBroadcastMessagesRequest;
import com.moneygram.agentconnect.GetBroadcastMessagesResponse;
import com.moneygram.agentconnect.GetBroadcastMessagesResponse.Payload.MessageInfos;
import com.moneygram.agentconnect.MessageInfo;
import com.moneygram.agentconnect.Response;

import dw.comm.MessageFacade;
import dw.comm.MessageLogic;
import dw.framework.CommCompleteInterface;
import dw.framework.DataCollectionPanel;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.FlowPage;
import dw.framework.WaitToken;
import dw.i18n.Messages;
import dw.main.DeltaworksMainMenu;
import dw.mgsend.MoneyGramClientTransaction;
import dw.model.adapters.ConsumerProfile;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.CustomerInfo;
import dw.profile.UnitProfile;
import dw.utility.Debug;
import dw.utility.DwEncryptionAES;
import dw.utility.TimeUtility;
import dw.utility.DwEncryptionAES.DwEncryptionFileException;

public final class BroadcastMessagesTransaction extends MoneyGramClientTransaction implements CommCompleteInterface {
	private static final long serialVersionUID = 1L;

	private static final String BROADCAST_MESSAGES_DATA_FILE_NAME = "bm.obj";
	private static final Object OBJECT = new Object();
	private static final WaitToken WAIT = new WaitToken();
	private static final WaitToken PAUSE = new WaitToken();
	
	private static BroadcastMessagesTransaction instance;
	private static volatile Thread intervalThread;
	private static volatile Map<String, BroadcastMessages> cachedBroadcastMessagesMap = null;
	private static int reterivalType = CountryInfo.AUTOMATIC_TRANSMIT_RETERIVAL;
	
	public static class BroadcastMessages implements Serializable {
		private static final long serialVersionUID = 1L;

		private long date;
		private List<BroadcastMessage> broadcastMessages;

		public long getDate() {
			return date;
		}

		public void setDate(long date) {
			this.date = date;
		}

		public List<BroadcastMessage> getBroadcastMessages() {
			return broadcastMessages;
		}

		public void setBroadcastMessages(List<BroadcastMessage> broadcastMessages) {
			this.broadcastMessages = broadcastMessages;
		}
	}

	public static class BroadcastMessage implements Comparable<BroadcastMessage>, Serializable  {
		private static final long serialVersionUID = 1L;

		private int id;
		private int version;
		private Date startDate;
		private String subject;
		private String content;
		private String language;
		private boolean viewed;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public Date getStartDate() {
			return startDate;
		}

		public void setStartDate(Date date) {
			this.startDate = date;
		}

		public String getSubject() {
			return subject;
		}

		public void setSubject(String subject) {
			this.subject = subject;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}

		@Override
		public int compareTo(BroadcastMessage bm) {
			return bm.startDate.compareTo(startDate);
		}
		
		@Override
		public boolean equals(Object o) {
			if (o instanceof BroadcastMessage) {
				return ((BroadcastMessage) o).startDate.equals(startDate);
			} else {
				return false;
			}
		}
		
		@Override
		public int hashCode() {
			return this.getStartDate().hashCode();
		}

		public int getVersion() {
			return version;
		}

		public void setVersion(int version) {
			this.version = version;
		}

		public String getLanguage() {
			return language;
		}

		public void setLanguage(String language) {
			this.language = language;
		}

		public boolean isViewed() {
			return viewed;
		}

		public void setViewed(boolean viewed) {
			this.viewed = viewed;
		}
	}
	
	private static void setInstance(BroadcastMessagesTransaction i) {
		instance = i;
	}
	
	public BroadcastMessagesTransaction(String tranName) {
		super(tranName, "");
		setInstance(this);
		timeoutEnabled = true;
		dispenserNeeded = false;
		receiptPrinterNeeded = false;
		if (cachedBroadcastMessagesMap == null) {

			// Get the cached messages from the file system.
			
			File file = new File(BROADCAST_MESSAGES_DATA_FILE_NAME);
			if (file.exists() && file.isFile()) {
				DwEncryptionAES oe = new DwEncryptionAES();
				oe.vInitialize(DwEncryptionAES.OFFSET_BM_DATA, BROADCAST_MESSAGES_DATA_FILE_NAME);
				try {
					cachedBroadcastMessagesMap = (Map<String, BroadcastMessages>) oe.readObject();
				} catch (DwEncryptionFileException e) {
					Debug.printException(e);
				}
			}
			
			// If there is no file in the file system, return an empty Map object.
				
			if (cachedBroadcastMessagesMap == null) {
				cachedBroadcastMessagesMap = new HashMap<String, BroadcastMessages>();
			}
			
			updateMessageMenuButton();
		}
		clear();

		// If a thread has not been started already, start a thread to refresh 
		// the broadcast messages.
		
		if (intervalThread == null) {
			if (getBroadcastMessageUpdateInterval() > 0) {
				Thread t = new Thread() {
					
					@Override
					public void run() {
	
						// Save a reference to the current thread.
						
		    			intervalThread = Thread.currentThread();
						
						// Check if there are currently messages in cache for the current 
						// selected language.  
						
	    				BroadcastMessages broadcastMessages = getCurrentBroadcastMessages();
						
						// Calculate an initial wait time based upon the messages
						// currently in cache.
						
						long initialSleepTime;
						if (broadcastMessages == null) {
							initialSleepTime = 0;
						} else {
							initialSleepTime = getWaitTime(broadcastMessages.getDate());
							updateMessageMenuButton();
						}
						
						// Start a method that will update the broadcast messages.
						// Note: This method will not exit will DeltaWorks is
						// running.
					
		    			refreshBroadcastMessages(initialSleepTime);
					}
				};
				t.start();
			} else {
				intervalThread = Thread.currentThread();
			}
		}
	}

	private long getBroadcastMessageUpdateInterval() {
		return UnitProfile.getInstance().getBroadcastMessageUpdateInterval() * TimeUtility.MINUTE;
	}
	
	static public BroadcastMessagesTransaction getInstance() {
		return instance;
	}
	
	@Override
	public boolean isFinancialTransaction() {
		return false;
	}

	@Override
	public void setFinancialTransaction(boolean b) {
	}

	@Override
	public boolean isTransmitRequired(boolean flag) {
		return false;
	}

	@Override
	public boolean isCheckinRequired() {
		return false;
	}

	@Override
	public boolean isNamePasswordRequired() {
	    // login type required for non financial transactions
		String loginRequired = super.getGlobalLoginTypeRequired();
	    /**
	     *   N -- No login required
	     *   P -- Pin Required
	     *   Y -- Name and Pin required.
	     */
		return "Y".equals(loginRequired);
	}

	@Override
	public boolean isPasswordRequired() {
	    // login type required for non financial transactions
		String loginRequired = super.getGlobalLoginTypeRequired();
	    /**
	     *   N -- No login required
	     *   P -- Pin Required
	     *   Y -- Name and Pin required.
	     */
        if ("N".equals(loginRequired)) {
            return false;
        } else {
            return true;
        }
	}

	@Override
	public boolean isUsingWizard() {
		return true;
	}

    @Override
	public boolean isReceiptPrinterNeeded() {
		return false;
	}
    
    public static String getMsgLanguageCode(String shortLanguageCode) {
    	if (shortLanguageCode.equalsIgnoreCase("en")) {
    		return "EN-US";
    	} else if (shortLanguageCode.equalsIgnoreCase("es")) {
    		return "ES-ES";
    	} else if (shortLanguageCode.equalsIgnoreCase("fr")) {
    		return "FR-FR";
    	} else if (shortLanguageCode.equalsIgnoreCase("pl")) {
    		return "PL-PL";
    	} else if (shortLanguageCode.equalsIgnoreCase("de")) {
    		return "DE-DE";
    	} else if (shortLanguageCode.equalsIgnoreCase("cn")) {
    		return "ZH-CN";
    	} else if (shortLanguageCode.equalsIgnoreCase("ru")) {
    		return "ru-RU";
    	} else if (shortLanguageCode.equalsIgnoreCase("pt")) {
    		return "pt-PT";
    	} else {
    		return "EN-US";
    	}
    }
    
    private String getCurrentMsgLanguageCode() {
    	return getMsgLanguageCode(Messages.getCurrentLanguage());
    }
    
    private BroadcastMessages getCurrentBroadcastMessages() {
		synchronized(OBJECT) {
			return cachedBroadcastMessagesMap.get(getMsgLanguageCode(Messages.getCurrentLanguage()));
		}
    }
    
    List<BroadcastMessage> getBroadcastMessages(String language) {
    	
    	// Get the current language.

    	String language3 = language;
    	if (language3 == null) {
			language3 = getMsgLanguageCode(Messages.getCurrentLanguage());
    	}
    	
    	// Get any cached messages.
    	
    	synchronized(OBJECT) {
			BroadcastMessages broadcastMessages = cachedBroadcastMessagesMap.get(language3);
			if (broadcastMessages != null) {
				return broadcastMessages.getBroadcastMessages();
			} else {
				return null;
			}
    	}
    }
    
    private long getWaitTime(long lastTime) {
    	long broadcastMessageUpdateInterval = getBroadcastMessageUpdateInterval();
		long waitTime = Math.max(0, broadcastMessageUpdateInterval - System.currentTimeMillis() + lastTime);
		waitTime = Math.min(waitTime, broadcastMessageUpdateInterval);
		return waitTime;
    }
    
    private void refreshBroadcastMessages(long initialWaitTime) {

		// Set the sleep time to the initial wait time.
		
		long waitTime = initialWaitTime;
		
		// Continue to loop while DeltaWorks is running.
		
    	while (true) {
    		try {
    			
    			if (waitTime > 0) {
    				
    				// Update the menu button.
    				
    				updateMessageMenuButton();
    				
        			// Wait for the specified amount of time or until the thread is awoken.
    				
	    			int m = ((int) waitTime) / 60000;
	    			int s = (((int) waitTime) % 60000) / 1000;
	    			Debug.println("Sleeping for " + m + " minute" + (m != 1 ? "s" : "") + ", " + s + " second" + (s != 1 ? "s" : "") + " before next retrieval of broadcast messages for the " + getMsgLanguageCode(Messages.getCurrentLanguage()) + " language");
    				
	    			synchronized(WAIT) {
	    				try {
	    					reterivalType = CountryInfo.AUTOMATIC_TRANSMIT_RETERIVAL;
	    					WAIT.setWaitFlag(true);
		    				WAIT.wait(waitTime);
	    				} catch (Exception e) {
	    					Debug.printException(e);
	    				}
	    			}
        			
    		    	// Get the messages currently cached for the current language.

    				BroadcastMessages broadcastMessages = getCurrentBroadcastMessages();
    				
    				// Calculate a wait time before retrieving new messages. 
    				//   1) If there are old messages in cache, reset the sleep 
    				//      time based upon when the messages for the current 
    				//		select language were last retrieved.
    				//   2) If the communication dialog is showing, unconditionally wait a
    				//      specific amount of time before checking again.
    				//   3) If there are no message in cache, get any new messages
    				//      immediately.

    				if (broadcastMessages != null) {
    					waitTime = getWaitTime(broadcastMessages.getDate());
    				} else {
    					waitTime = 0;
    				}
    			}
    			
    			if (waitTime <= 0) {
	   				retrieveBroadcastMessages(reterivalType);
	   				waitTime = getBroadcastMessageUpdateInterval();
				} else {
    				synchronized(PAUSE) {
    					PAUSE.setWaitFlag(false);
    					PAUSE.notifyAll();
    				}
				}
    		} catch (Exception e) {
    			Debug.printException(e);
    		}
    	}
    }

    public static void languageChange() {
		PAUSE.setWaitFlag(true);
    	
		synchronized(WAIT) {
			reterivalType = CountryInfo.MANUAL_TRANSMIT_RETERIVAL;
			WAIT.setWaitFlag(false);
			WAIT.notifyAll();
		}
		
		synchronized(PAUSE) {
			try {
				while (PAUSE.isWaitFlag()) {
					PAUSE.wait();
				}
			} catch (Exception e) {
				Debug.printException(e);
			}
		}
    }
	
	private void retrieveBroadcastMessages(final int reterivalType) {
		
		// Make a call to retrieve the current messages for the current 
		// selected language.
		
		MessageLogic logic = new MessageLogic() {
			@Override
			public synchronized Object run() {
				synchronized(OBJECT) {
					String language = getCurrentMsgLanguageCode();
	
					GetBroadcastMessagesRequest request = new GetBroadcastMessagesRequest();
					GetBroadcastMessagesResponse response = null;
					
					request.setMsgLanguageCode(language);
					
					try {
						Map<String, Long> viewedMessageMap = new HashMap<String, Long>();
						List<BroadcastMessage> messages = getBroadcastMessages(language);
						if (messages != null) {
							for (int i = 0; i < messages.size(); i++) {
								BroadcastMessage bm = messages.get(i);
								MessageInfo mi = new MessageInfo();
								mi.setMessageId(bm.getId());
								mi.setMessageVersion(bm.getVersion());
								if (request.getMessageInfos() == null) {
									request.setMessageInfos(new GetBroadcastMessagesRequest.MessageInfos());
								}
								request.getMessageInfos().getMessageInfo().add(mi);

								if (bm.isViewed()) {
									CRC32 crc32 = new CRC32();
									crc32.update(bm.getContent().getBytes());
									viewedMessageMap.put(String.valueOf(bm.getId()), Long.valueOf(crc32.getValue()));
								}
							}
						}
						
						response = MessageFacade.getInstance().getBroadcastMessages(request, reterivalType);
						
						processBroadcastMessages(response, viewedMessageMap);
						updateMessageMenuButton();
	
					} catch (Exception e) {
						Debug.println("Exception: " + e.getMessage());
						
						// An error occurred retrieving the broadcast messages.
						// Set the time that messages were last retrieved to the
						// current time so the next retrieval attempt will be in
						// one hour.
						
						BroadcastMessages bm = getCurrentBroadcastMessages();
						if (bm == null) {
							bm = new BroadcastMessages();
							bm.setBroadcastMessages(new ArrayList<BroadcastMessage>());
							cachedBroadcastMessagesMap.put(getMsgLanguageCode(Messages.getCurrentLanguage()), bm);
						}
						bm.setDate(System.currentTimeMillis());
						writeBroadcastMessages();
						
						return Boolean.FALSE;
					} finally {
	    				synchronized(PAUSE) {
	    					PAUSE.setWaitFlag(false);
	    					PAUSE.notifyAll();
	    				}
					}
					return Boolean.TRUE;
				}
			}
		};
		MessageFacade.run(logic, this, 0, Boolean.FALSE);
	}
	
	@Override
	public void commComplete(int commTag, Object returnValue) {
	}
	
	private void processBroadcastMessages(GetBroadcastMessagesResponse element, Map<String, Long> viewedMessageMap) {
		
		// Get the messages currently in cache.
		
		String language = getCurrentMsgLanguageCode();
		BroadcastMessages broadcastMessages = cachedBroadcastMessagesMap.get(language);
		if (broadcastMessages == null) {
			broadcastMessages = new BroadcastMessages();
			broadcastMessages.setBroadcastMessages(new ArrayList<BroadcastMessage>());
			cachedBroadcastMessagesMap.put(language, broadcastMessages);
		}
		broadcastMessages.setDate(System.currentTimeMillis());

		// Get the status from the response.
		
		String status = element.getPayload().getValue().getStatus().trim();
		
		// If the status is 1, then there was a change to the messages.
		
		if (status.equals("1")) {

			// Remove all of the old messages for the current language from cache.
			
			broadcastMessages.getBroadcastMessages().clear();
	
			// Get the messages from the results of the agent connect call and put them in cache.
			
			MessageInfos messages = element.getPayload().getValue().getMessageInfos();
			for (int i = 0; i < messages.getMessageInfo().size(); i++) {
				MessageInfo message = messages.getMessageInfo().get(i);
				Date date = null;
				XMLGregorianCalendar d;
				try {
					d = DatatypeFactory.newInstance().newXMLGregorianCalendar(message.getStartDate().trim());
					date = TimeUtility.toDate(d);
				} catch (Exception e) {
					Debug.println("Invalid date format, message not used: " + message.getStartDate());
					continue;
				}
				Integer id = message.getMessageId();
				Integer version = message.getMessageVersion();
				String subject = message.getSubjectMimeData().trim();
				String content = message.getContentMimeData().trim();
				BroadcastMessage bm = new BroadcastMessage();
				bm.setId(id);
				bm.setVersion(version);
				bm.setStartDate(date);
				bm.setSubject(subject);
				bm.setContent(content);
				
				Long oldChecksum = viewedMessageMap.get(String.valueOf(id));
				if (oldChecksum != null) {
					CRC32 crc32 = new CRC32();
					crc32.update(bm.getContent().getBytes());
					if (oldChecksum.longValue() == crc32.getValue()) {
						bm.setViewed(true);
					} else {
						bm.setViewed(false);
					}
				} else {
					bm.setViewed(false);
				}
				
				broadcastMessages.getBroadcastMessages().add(bm);
			}
	
			// Update the button text on the main DeltaWorks screen.
	
			updateMessageMenuButton();
		}
		
		// Save the broadcast messages to a file in the current working directory.

		writeBroadcastMessages();
	}
	
	void updateMessageMenuButton() {
		BroadcastMessages broadcastMessages = getCurrentBroadcastMessages();

		DeltaworksMainMenu dwmm = DeltaworksMainMenu.getInstance();
		JButton b = dwmm.getBroadcastMessageButton();

		if ((broadcastMessages != null) && broadcastMessages.getBroadcastMessages()!=null && (broadcastMessages.getBroadcastMessages().size() > 0)) {
			
			// Check if all messages have been viewed.
			// Determine the publish date of the newest message.

			Date date = new Date(0);
			boolean flag = false;
			for (int i = 0; i < broadcastMessages.getBroadcastMessages().size(); i++) {
				BroadcastMessage bm = broadcastMessages.getBroadcastMessages().get(i);
				if (date.compareTo(bm.getStartDate()) < 0) {
					date = bm.getStartDate();
				}
				if (! bm.isViewed()) {
					flag = true;
				}
			}
			
			// Generate the text of the button.

			String text1 = Messages.getString("DeltaworksMainMenu.722");
			int i = text1.indexOf("</CENTER>");
			StringBuffer sb = new StringBuffer();
			sb.append(text1.substring(0, i));
			sb.append(" (");
			sb.append(new SimpleDateFormat(TimeUtility.DATE_MONTH_DAY_LONG_YEAR_FORMAT).format(date));
			sb.append(") ");
			sb.append(text1.substring(i));
			
			// If one of the messages has never been viewed, make the text of the button read.
			
			if (flag) {
				i = sb.indexOf("<CENTER>");
				sb.insert(i, "<FONT COLOR=\"RED\"");
				i = sb.indexOf("</HTML>");
				sb.insert(i, "</FONT>");
			}
			
			b.setText(sb.toString());
		} else {
			b.setText(Messages.getString("DeltaworksMainMenu.722"));
		}
		b.revalidate();
		b.repaint();
	}
	
	void writeBroadcastMessages() {
		try {
			DwEncryptionAES oe = new DwEncryptionAES();
			oe.vInitialize(DwEncryptionAES.OFFSET_BM_DATA, BROADCAST_MESSAGES_DATA_FILE_NAME);
			oe.vWriteSaveFile(cachedBroadcastMessagesMap);
		} catch (Exception e) {
			Debug.printException(e);
		}
	}
	
	// Data Collection support method (not used by this type of transaction)

	@Override
	public List<DataCollectionPanel> getSupplementalDataCollectionPanels() {
		return null;
	}

	@Override
	public void setValidationStatus(DataCollectionStatus status) {
	}

	@Override
	public void validation(FlowPage callingPage) {
	}

	@Override
	public String nextDataCollectionScreen(String currentScreen) {
		return null;
	}

	@Override
	public String getDestinationCountry() {
		return null;
	}

	@Override
	public void setScreensToShow() {
	}

	@Override
	public void applyHistoryData(CustomerInfo customerInfo) {
	}

	@Override
	public CustomerInfo getCustomerInfo() {
		return null;
	}

	@Override
	public void applyTransactionLookupData() {
	}

	@Override
	public void applyProfileData(ConsumerProfile profile) {
	}

	@Override
	public boolean createOrUpdateConsumerProfile(ConsumerProfile profile, ProfileStatus status) {
		return false;
	}

	@Override
	public void createOrUpdateConsumerProfile(ConsumerProfile profile, CommCompleteInterface cci) {
	}

	@Override
	public void getConsumerProfile(ConsumerProfile profile, String mgiSessionID, int rule, Map<String, String> currentValues, CommCompleteInterface cci) {
	}

	@Override
	public Response getConsumerProfileResponse() {
		return null;
	}

	@Override
	public void applyCurrentData() {
	}
}
