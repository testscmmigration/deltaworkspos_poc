package dw.dispenserload;

import java.awt.event.KeyEvent;

import javax.swing.JTextField;

import dw.dialogs.Dialogs;
import dw.dispenser.DispenserInterface;
import dw.dwgui.CardSwipeTextField;
import dw.framework.FlowPage;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.main.MainPanelDelegate;
import dw.profile.UnitProfile;
import dw.utility.Debug;
import dw.utility.ErrorManager;
import dw.utility.IdleBackoutTimer;

/**
 * Abstract flow page for the dispenser loading GUI.
 * @author Christopher Bartling
 */
public abstract class DispenserLoadFlowPage extends FlowPage {
	private static final long serialVersionUID = 1L;

	protected DispenserLoadTransaction transaction;

    public DispenserLoadFlowPage(
        DispenserLoadTransaction tran,
        String fileName,
        String name,
        String pageCode,
        PageFlowButtons buttons) {
        super(fileName, name, pageCode, buttons);
        transaction = tran;
    }

   /*
    * When expert mode was added, a lot of code got copy-and-pasted
    * from wizard page n to expert page n. For version 4.2 we have a
    * third version, and I don't want to copy-and-paste it again, so
    * I'm pulling all the common code up here.  Most of this code
    * actually belongs in DispenserLoadTransaction, but I don't have
    * time to move it there. --gea
    */

    protected void startStep1() {
        if (flowButtons != null) {
            flowButtons.reset();
            flowButtons.setEnabled("expert",false); 
            flowButtons.setVisible("expert",false); 
            flowButtons.getButton("next").setEnabled(false);    
            flowButtons.getButton("cancel").requestFocus(); 
        }
        if (UnitProfile.getInstance().isDemoMode() && transaction.getDispenserStatus() ==DispenserInterface.NO_CTS){
            flowButtons.getButton("next").setEnabled(true);    
            flowButtons.getButton("next").requestFocus(true);
        } else {
            transaction.setTransactionCanceled(false);
            transaction.setForceLoad(false);

            // Is the dispenser on?
            if (!transaction.isDispenserPoweredOn()) {
                // Dispenser is not powered on or is otherwise noncommunicative.
                Dialogs.showError("dialogs/DialogDispenser_NO_CTS_Error.xml"); 
                finish(PageExitListener.FAILED);
                return;
            }

            // Set the dispenser into load required state.  Need this for clearing
            // any error status in the dispenser.
            int status = transaction.getDispenserStatus();
            switch (status) {
                case DispenserInterface.NOT_INITIALIZED :
                case DispenserInterface.LOAD_REQUIRED :
                case DispenserInterface.EOF :
                case DispenserInterface.ERROR :
                case DispenserInterface.VOIDED :
                case DispenserInterface.NOT_PRINTED :
                case DispenserInterface.WAITING :
                    transaction.setLoadRequired();
                    transaction.setDispenserChangeUser(false);
                    break;
            }
            if (!transaction.getDispenserChangeUser()) {
                transaction.setDispenserChangeUser(true);
                Thread.currentThread().setName("MainThread"); 
                Thread txnThread = new Thread(new Runnable() {
                    @Override
					public void run() {
                        try {
                            Thread.sleep(1000);
                        }
                        catch (InterruptedException e) {
                        }

                        if (!transaction.isDispenserLocked())
                            backoutForm();
                        else if (transaction.getRemainingNumberOfForms() <= 0) {
                            // There are no forms to back out or void.  Start
                            // polling the dispenser for the unlock event.
                            transaction.pollForUnlockEvent();
                            if (!transaction.isDispenserLocked())
                                backoutForm();
                        }
                        else if (transaction.canVoidRemainingForms()) {
                            if (showVoidFormsDialog())
                                preVoidRemainingForms();
                            else
                                preBackoutForm();
                        }
                        else
                            preBackoutForm();

                    }
                });
                txnThread.setName("TransactionThread"); 
                txnThread.start();
            }
        }
    }

    protected void finishStep1(int direction) {
        if (flowButtons != null) {
            flowButtons.disable();
        }

        if (direction == PageExitListener.NEXT
            && transaction.isDispenserLocked()) {
            if (UnitProfile.getInstance().isDemoMode() && transaction.getDispenserStatus() ==DispenserInterface.NO_CTS)
                PageNotification.notifyExitListeners(this, direction);
            else {
                Dialogs.showError("dialogs/DialogDispenserUnlockError.xml"); 
    	        flowButtons.reset();
            }
        } 
        else {
        	if (direction == PageExitListener.CANCELED){
    	  	  	if ("Y".equals(UnitProfile.getInstance().get("ISI_ENABLE", "N"))){
    	  	  		String programName = "";
    	  	  		if (MainPanelDelegate.getIsiExeProgramArguments() != null)
    	  	  			programName = MainPanelDelegate.getIsiExeProgramArguments().trim();
    	  	  		if (programName.length() > 0 && !(programName.startsWith("-EXE"))) {
    	  	  			MainPanelDelegate.executeIsiProgram(programName);
    	  	  		}
    	  	  	}
    	  	  	MainPanelDelegate.initializeIsiFields();
    	  	  	MainPanelDelegate.setIsiSkipRecord(false);    	  		
        	}

            PageNotification.notifyExitListeners(this, direction);
        }
    }

    /**
     * Checks the status of dispenser to determine whether we can void the
     * remaining forms.
     */
    protected void preVoidRemainingForms() {
        voidRemainingForms();
    }

    /**
     * Checks the status of dispenser to determine whether we can back out
     * the current form.
     */
    protected void preBackoutForm() {
        if (!transaction.isDispenserLocked())
            backoutForm();
        else
            startPollingDispenserForUnlockEvent();
    }

    /**
     *  Determine whether the user really wants to void all the remaining
     *  forms. Logic finally fixed in version 4.2.
     *  @return true if the user wants to void the remaining forms
     */
    protected boolean showVoidFormsDialog() {

        if (transaction.getRemainingNumberOfForms() > transaction.getMaxVoidableForms()) {
            // This should never happen, but there is reason to believe the logic
            // leading here was wrong at one time, so double-check to prevent
            // voiding an entire pack. The rule is we only void up to the profile
            // option MAX_VOIDABLE_FORMS, not including the unprintable one at the
            // end of the pack.

            return false;
        }

        int rc = Dialogs.showConfirm("dialogs/DialogVoidRemainingForms.xml");  

        return (rc == Dialogs.YES_OPTION);
    }

    /**
     *  Backs out the current form.  The dispenser is unlocked at this point
     *  and its status is such that it can backout the remaining form.
     */
    protected void backoutForm() {
        final DispenserLoadBackOutDialog backOutDialog =
            new DispenserLoadBackOutDialog();
        // dialog will block this thread, so create a new one to do the backing
        // out of the remaining forms.
        Thread backOutThread = new Thread(new Runnable() {
            @Override
			public void run() {
                    // This send message should be
                    // synchronized in the Transaction object.
                transaction.backoutForm(backOutDialog);
            }
        });
        backOutThread.start();
        Dialogs.showPanel(backOutDialog);

        if (backOutDialog.getSuccess())
            finish(PageExitListener.NEXT);
        else {
            transaction.setDispenserChangeUser(false);
            finish(PageExitListener.CANCELED);
        }
    }

    /**
     *   Voids the remaining forms through the dispenser.
     *   Fire off the print process by calling into the transaction and showing
     *   the printing dialog. Since this thread will block when modal dialog is
     *   shown, create a new thread to call the transaction print process before
     *   showing the dialog
     */
    protected void voidRemainingForms() {
        if (transaction.getRemainingNumberOfForms() == 0)
            return;

        // Print dialog doesn't automatically pause idle timer anymore,
        // so has to be done here.
        boolean timerPending = IdleBackoutTimer.isRunning();
        IdleBackoutTimer.pause();

        final DispenserLoadPrintingDialog printDialog =
            new DispenserLoadPrintingDialog();
        // dialog will block this thread, so create a new one to do the printing
        Thread printThread = new Thread(new Runnable() {
            @Override
			public void run() {
                    // The send message should be synchronized in the
                    // Transaction object.
                transaction.voidRemainingForms(printDialog);
            }
        });
        printThread.start();
        Dialogs.showPanel(printDialog);
        if (timerPending)
            IdleBackoutTimer.start();

        if (printDialog.getSuccess()) {
            // Eject the last voided form from the dispenser so it can be easily
            // removed.
            transaction.ejectForm();
            // Now poll the dispenser for the unlock event.
            startPollingDispenserForUnlockEvent();
        }
        else
            finish(PageExitListener.FAILED);
    }

    /**
     *  Polls the controller until the dispenser is unlocked.
     */
    protected void startPollingDispenserForUnlockEvent() {
        // dialog will block this thread, so create a new one to do the prompting
        Thread t = new Thread(new Runnable() {
            @Override
			public void run() {
                    // The send message should be synchronized in the
                    // Transaction object.
                transaction.pollForUnlockEvent();
            }
        });
        t.start();
        try {
            // Wait for the thread to complete its execution before proceeding.
            t.join();
        }
        catch (InterruptedException e) {
            Debug.printStackTrace(e);
        }

        if (!transaction.isDispenserLocked()
            && !transaction.isTransactionCanceled()) {
            if (transaction.getRemainingNumberOfForms() > 0) {
                backoutForm();
            }
            else if (transaction.getRemainingNumberOfForms() <= 0) {
                finish(PageExitListener.NEXT);
            }
        }
    }

    protected static final int STARTING_SERIAL_NUMBER_INDEX = 1;

    protected static final int NUMBER_OF_FORMS_INDEX = 2;

    protected int numberOfTries = 1;

    protected String endingSerialNumber;

    protected String startingSerialNumber;

    protected CardSwipeTextField startingSerialNumberField = null;

    protected JTextField endingSerialNumberField = null;

    protected void startStep2() {
        if (flowButtons != null)
            flowButtons.reset();
        startingSerialNumberField = (CardSwipeTextField) getComponent
            ("startingSerialNumberField"); 
        endingSerialNumberField = (JTextField) getComponent
            ("endingSerialNumberField");
        addListeners();
        if (flowButtons != null) {
            flowButtons.setEnabled("expert",false);	
            flowButtons.setVisible("expert",false);	
            // Disable the Next button until the serial numbers are entered.
            flowButtons.setEnabled("next", false);	
        }
        startingSerialNumberField.requestFocus();
        startingSerialNumberField.setText("");	
        endingSerialNumberField.setText("");	
        startingSerialNumberField.requestFocus();
        numberOfTries = 1;
    }

    protected void finishStep2(int direction) {
        // no need to validate serial numbers if canceling
        if (direction != PageExitListener.NEXT) {
        	if (direction == PageExitListener.CANCELED){
    	  	  	if ("Y".equals(UnitProfile.getInstance().get("ISI_ENABLE", "N"))){
    	  	  		String programName = "";
    	  	  		if (MainPanelDelegate.getIsiExeProgramArguments() != null)
    	  	  			programName = MainPanelDelegate.getIsiExeProgramArguments().trim();
    	  	  		if (programName.length() > 0 && !(programName.startsWith("-EXE"))) {
    	  	  			MainPanelDelegate.executeIsiProgram(programName);
    	  	  		}
    	  	  	}
    	  	  	MainPanelDelegate.initializeIsiFields();
    	  	  	MainPanelDelegate.setIsiSkipRecord(false);    	  		
        	}
            transaction.setDispenserChangeUser(false);
            PageNotification.notifyExitListeners(this, direction);
            return;
        }

        // This should only be possible in expert mode, but doesn't hurt wizard:
        // Check to make sure we actually have a value...
        if (startingSerialNumberField.getText().equals("") || endingSerialNumberField.getText().equals("")) {    
            isValidSerialNumbers();
            startingSerialNumberField.setText("");  
            endingSerialNumberField.setText("");    
            startingSerialNumberField.requestFocus();
            return;
        }

      	// If values have been entered into the starting and ending serial number
    	// fields, enable the next button.
        long startingSerial = Long.parseLong(startingSerialNumberField.getText());

        // set the starting serial string again, cuz now no leading zeros
        startingSerialNumber = "" + startingSerial;	
        endingSerialNumber = endingSerialNumberField.getText().trim();
        checkNumberSwap();

        if (isValidSerialNumbers()
                && isValidBundleSize()
                && !isSerialNumberRangeInUse()) {
            // The serial numbers are valid and the bundle size is within
            // the bundle size limit defined in the profile.
            transaction.setStartingSerialNumber(startingSerialNumber);
            transaction.setEndingSerialNumber(endingSerialNumber);
            transaction.setDispenserChangeUser(false);
            PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
        }
        else {
            if (numberOfTries >= 3) {
                // Three strikes and you're out!!
                PageNotification.notifyExitListeners(this,
                                                    PageExitListener.FAILED);
            }
            else {
                // Try again.
                numberOfTries++;
                startingSerialNumberField.setText("");	
                endingSerialNumberField.setText("");	
                startingSerialNumberField.requestFocus();
            }
        }
    }

    /**
        Adds various event listeners to the form components.
     */
    protected void addListeners() {
        addActionListener("startingSerialNumberField", this, 	
            "startingSerialNumberFieldAction"); 
        addActionListener("endingSerialNumberField", this, 	
            "endingSerialNumberFieldAction"); 
        if (flowButtons != null) {
            addTextListener("endingSerialNumberField", this, 	
                "endingSerialNumberFieldText"); 
        }
    }

    /**
     * Action event handler for the startingSerialNumberField component
     * action events.
     */
    public void startingSerialNumberFieldAction() {
        long startingSerial = 0;
        long endingSerial = 0;
        boolean isCardSwipedInput = false;

        // Check to make sure we actually have a value...
        if (startingSerialNumberField.getText().equals(""))	
            return;

        if (startingSerialNumberField.getField(STARTING_SERIAL_NUMBER_INDEX) != null)
            startingSerial = Long.parseLong(startingSerialNumberField
                             .getField(STARTING_SERIAL_NUMBER_INDEX));
        else
           startingSerial = Long.parseLong(startingSerialNumberField.getText());

        // set the starting serial string again, cuz now no leading zeros
        startingSerialNumber = "" + startingSerial;	
        if (startingSerialNumberField.getField(NUMBER_OF_FORMS_INDEX) != null) {
            isCardSwipedInput = true;
            // truncate the check digit from the starting, add number of forms,
            //   then calculate the check digit for the ending and add it
            String startTemp = startingSerialNumber.substring(
                         0, startingSerialNumber.length() - 1);
            long startNoCheck = Long.parseLong(startTemp);
            endingSerial = startNoCheck - 1 + Long.parseLong
                (startingSerialNumberField.getField(NUMBER_OF_FORMS_INDEX));
            String endTemp = endingSerial + "" + endingSerial % 11 % 10;	
            endingSerial = Long.parseLong(endTemp);
        }
        else {
            endingSerial = startingSerial;
        }
        startingSerialNumberField.setText(Long.toString(startingSerial));
        endingSerialNumberField.setText(Long.toString(endingSerial));
        endingSerialNumber = "" + endingSerial;	

        // Enable the Next button.
        if (flowButtons != null)
            flowButtons.setEnabled("next", true);	

        // Was the input coming from the card swipe mechanism?
        if (isCardSwipedInput) {
            transaction.setCardSwiped(isCardSwipedInput);
            Thread t = new Thread(new Runnable() {
                @Override
				public void run() {
        	        try {
        	            Thread.sleep(500);
        	        }
        	        catch (InterruptedException exc) { Debug.printStackTrace(exc); }
                    finish(PageExitListener.NEXT);
                }
            });
            t.start();
        }
        else {
            // Put the focus in the endingSerialNumberField so the user can
            // easily edit that value.
            endingSerialNumberField.requestFocus();
        }
    }

    /**
        Action event handler for the endingSerialNumberField component
        action events.
     */
    public void endingSerialNumberFieldAction() {
        if (flowButtons != null){
            flowButtons.getButton("next").doClick();    
        }
        else {
            finish(PageExitListener.NEXT);
        }
    }

    /**
        Text event handler for the endingSerialNumberField component
        text change events.
     */
    public void endingSerialNumberFieldText() {
        if (flowButtons != null) {
            // If values have been entered into the starting and ending serial number
            // fields, enable the next button.
            if (startingSerialNumberField.getText().trim().length() != 0
                && endingSerialNumberField.getText().trim().length() != 0)
                flowButtons.getButton("next").setEnabled(true); 
            else
                flowButtons.getButton("next").setEnabled(false); 
        }
    }

    /**
        Check to make sure the serial numbers are not swapped.
     */
    protected void checkNumberSwap() {
        long start, end;

        start = Long.parseLong(startingSerialNumber);
        end = Long.parseLong(endingSerialNumber);

        if (start > end) {
            Debug.println("swapping numbers.");	
            // Numbers are out of order.
            String temp = startingSerialNumber;
            startingSerialNumber = endingSerialNumber;
            endingSerialNumber = temp;
        }
    }

    /**
     * Determines whether the serial numbers are valid.
     * @return A boolean flag.
     */
    protected boolean isValidSerialNumbers() {
        boolean result = true;

        if (UnitProfile.getInstance().isDemoMode() && transaction.getDispenserStatus() ==DispenserInterface.NO_CTS)
            return result;

        if (startingSerialNumberField.getText().equals("") 
                || endingSerialNumberField.getText().equals("")) 
            result = false;
        else {
            result = transaction.isValidSerialNumber(startingSerialNumber)
                && transaction.isValidSerialNumber(endingSerialNumber);
        }
        if (!result) {
            // Notify the user that something is amiss and cancel the dispenser
            // loading wizard.
            Dialogs.showWarning("dialogs/DialogSerialNumberValidationError.xml"); 
            if (flowButtons != null) {
                flowButtons.getButton("cancel").setEnabled(true); 
            }
        }
        return result;
    }

    /**
     * Determines whether the bundle size of money order forms is valid.
     * @return A boolean flag.
     */
    protected boolean isValidBundleSize() {
        boolean result = true;

		if (UnitProfile.getInstance().isDemoMode() && transaction.getDispenserStatus() ==DispenserInterface.NO_CTS)
		   return result;

        result = transaction.isBundleSizeValid
                                       (startingSerialNumber,
                                        endingSerialNumber);
        if (! result) {
            // Notify the user that something is amiss and cancel the dispenser
            // loading wizard.
            ErrorManager.handle
                (new Exception(Messages.getString("DLWizardPage2.1893")), 
                		"bundleSizeInputValidationError", Messages.getString("ErrorMessages.bundleSizeInputValidationError"), 
                 ErrorManager.ERROR,
                 false);
            /*
            XmlDefinedDialog dialog = new BundleSizeValidationErrorDialog
                (this);
            dialog.show();
            */
        }
        return result;
    }

    /**
     * Determines whether the serial number range has been used or is
     * currently in use.  Necessary for maintaining security and avoiding
     * the possibility of writing a duplicate serial number on a form.
     * @return A boolean stating whether the serial number range has been used
     * or is currently in use.
     */
    protected boolean isSerialNumberRangeInUse() {
        boolean result = true;

        if (UnitProfile.getInstance().isDemoMode() && transaction.getDispenserStatus() ==DispenserInterface.NO_CTS)
            return false;

        result = transaction.isSerialNumberRangeInUse(
                Long.parseLong(startingSerialNumber),
                Long.parseLong(endingSerialNumber));
        if (result) {
            // Notify the user that something is amiss and cancel the dispenser
            // loading wizard.
            Dialogs.showWarning("dialogs/DialogDispenserSerialNumberRangeError.xml"); 
            if (flowButtons != null) {
                flowButtons.getButton("cancel").setEnabled(true); 
            }
        }
        return result;
    }

    protected void startStep3() {
        if (flowButtons != null) {
            flowButtons.reset();
            flowButtons.setEnabled("expert",false);	
            flowButtons.setVisible("expert",false);	
            flowButtons.getButton("next").setEnabled(false);	
        }
        if (UnitProfile.getInstance().isDemoMode() && transaction.getDispenserStatus() ==DispenserInterface.NO_CTS) {
            flowButtons.getButton("next").setEnabled(true);    
            flowButtons.getButton("next").requestFocus(true);    

        } else {
            transaction.setForceLoad(true);
            Thread t = new Thread(new Runnable() {
                @Override
				public void run() {
                    try {
                        Thread.sleep(1000);
                    }
                    catch (InterruptedException e) {}
                    if (! transaction.isTransactionCanceled())
                        startPollingDispenserForLockEvent();
                }
            });
            t.setName("DLWPage3_TransactionThread"); 
            t.start();
        }
    }

    protected void finishStep3(int direction) {
        if (direction == PageExitListener.CANCELED) {
           transaction.setTransactionCanceled(true);
           // Canceling.  Doesn't care whether dispenser is locked or not.
	  	  	if ("Y".equals(UnitProfile.getInstance().get("ISI_ENABLE", "N"))){
	  	  		String programName = "";
	  	  		if (MainPanelDelegate.getIsiExeProgramArguments() != null)
	  	  			programName = MainPanelDelegate.getIsiExeProgramArguments().trim();
	  	  		if (programName.length() > 0 && !(programName.startsWith("-EXE"))) {
	  	  			MainPanelDelegate.executeIsiProgram(programName);
	  	  		}
	  	  	}
	  	  	MainPanelDelegate.initializeIsiFields();
	  	  	MainPanelDelegate.setIsiSkipRecord(false);    	  		

           PageNotification.notifyExitListeners(this, direction);
        }
        else if (UnitProfile.getInstance().isDemoMode() && transaction.getDispenserStatus() ==DispenserInterface.NO_CTS) {
            PageNotification.notifyExitListeners(this, direction);
        }
        else if (transaction.isDispenserLocked()) {
            // Set the serial number range in the dispenser
            if (transaction.setSerialNumberRange()) {
                PageNotification.notifyExitListeners(this,
                                                    PageExitListener.NEXT);
            }
            else {
                Dialogs.showWarning("dialogs/DialogDispenserLoadCompleteError.xml");	

                while (transaction.isDispenserLocked()) {
                    Dialogs.showWarning("dialogs/DialogDispenserUnlockPrompt.xml");	
                }
                PageNotification.notifyExitListeners(this,
                        PageExitListener.BACK);
            }
        }
        else {
            Dialogs.showError("dialogs/DialogDispenserNotLockedError.xml");	
        }
    }

    /**
        Poll the dispenser until its locked.
     */
    protected void startPollingDispenserForLockEvent() {
        Thread t = new Thread(
            new Runnable() {
                @Override
				public void run() {
                    // The send message should be synchronized in the
                    // Transaction object.
                    transaction.pollForLockEvent();
                }
        });
        t.start();
        try {
            // Wait for the thread to complete its execution before proceeding.
        	t.join();
        }
        catch (InterruptedException e) {
    		Debug.printStackTrace(e);
        }
        if (! transaction.isTransactionCanceled())
            finish(PageExitListener.NEXT);
    }

    protected JTextField serialNumberField = null;

    protected void startStep4() {
        if (flowButtons != null) {
            flowButtons.reset();
            flowButtons.setEnabled("expert",false);	
            flowButtons.setVisible("expert",false);	
            flowButtons.setAlternateText("next",Messages.getString("OKEnter"));    
            flowButtons.setAlternateKeyMapping("next", KeyEvent.VK_ENTER, 0);	
            flowButtons.setMnemonic("next", 'O'); 
            flowButtons.setEnabled("next", false);	
        }

        serialNumberField = (JTextField) getComponent("serialNumberField");	
        serialNumberField.setText("");	
        addActionListener("serialNumberField", this, "serialNumberFieldAction");	 
        if (flowButtons != null) {
            addTextListener("serialNumberField", this, "serialNumberFieldText");	 
        }
        serialNumberField.requestFocus();
        // Extend the current form...
        transaction.extendForm();
        numberOfTries = 1;
    }

    protected void finishStep4(int direction) {
        if (direction == PageExitListener.NEXT) {
            // Verify the last 4 digits of the starting serial number.
            boolean verified = (!serialNumberField.getText().equals("") &&     
                transaction.verifySerialNumberDigits(Long.parseLong(serialNumberField.getText())));

            if (verified) {
				if (!UnitProfile.getInstance().isDemoMode())
                    transaction.writeReportLogRecord();
                //transaction.writeFinancialDocLoadMessage();
                transaction.retractForm();
                PageNotification.notifyExitListeners(this, PageExitListener.COMPLETED);
                return;
             }
            else {
                numberOfTries++;
                // Verification failed.  Display dialog and then send the FAILED
                // PageExitListener constant.
                Dialogs.showWarning("dialogs/DialogSerialNumberDigitsVerificationError.xml");	
                if (flowButtons != null) {
                    flowButtons.getButton("cancel").setEnabled(true);	
                    flowButtons.getButton("next").setEnabled(true);	
                }

                if (numberOfTries > 3) {
                    // The dispenser is not properly
                    // loaded and should not be used for any normal
                    // operations until it is properly loaded.
                    transaction.setLoadRequired();
                    PageNotification.notifyExitListeners(this, PageExitListener.FAILED);
                    return;
                }
            }
        }
        else {
            // The dispenser is not properly
            // loaded and should not be used for any normal operations until
            // it is properly loaded.
        	if (direction == PageExitListener.CANCELED){
    	  	  	if ("Y".equals(UnitProfile.getInstance().get("ISI_ENABLE", "N"))){
    	  	  		String programName = "";
    	  	  		if (MainPanelDelegate.getIsiExeProgramArguments() != null)
    	  	  			programName = MainPanelDelegate.getIsiExeProgramArguments().trim();
    	  	  		if (programName.length() > 0 && !(programName.startsWith("-EXE"))) {
    	  	  			MainPanelDelegate.executeIsiProgram(programName);
    	  	  		}
    	  	  	}
    	  	  	MainPanelDelegate.initializeIsiFields();
    	  	  	MainPanelDelegate.setIsiSkipRecord(false);    	  		
        	}
            transaction.retractForm();
            transaction.setLoadRequired();
            PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
        }
    }

    /**
     * Text listener callback for the serialNumberField.
     */
    public void serialNumberFieldText() {
        if (serialNumberField.getText().trim().length() != 0)
            flowButtons.setEnabled("next", true);	
        else flowButtons.setEnabled("next", false);	
    }

    /**
     * Action listener callback for the serialNumberField.
     */
    public void serialNumberFieldAction() {
        if (flowButtons != null) {
            flowButtons.getButton("next").doClick();	
        }
        else {
            finish(PageExitListener.NEXT);
        }
    }
}
