package dw.dispenserload;

/** The callback interface for a progress bar which entertains the user while the
    dispenser is backing out remaining forms.
    @author Christopher Bartling

    Note: This class is overkill for the task at hand. Chris copy-and-pasted it
    from the money order printing progress dialog, apparently not realizing
    that backing out the current form (not "remaining forms") only takes 1 or 2
    seconds. Thus the dialog box which implements this interface is gone before
    the user can read it. But I don't have the time or the mandate to rip it out
    right now. --gea
*/
public interface BackOutProgressInterface {

    /** Begins a backing out progress bar.
     *  This should not actually display the progress bar until the first
     *  begin() call is made.
     *  @param totalNumberToPrint The total number of items to print.
     */
    public void begin(int totalNumberToPrint);
 
    
    /** Eliminates the backing out progress bar.
    */
    public void done();
}
