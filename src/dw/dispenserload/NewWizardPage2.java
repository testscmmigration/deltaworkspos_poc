package dw.dispenserload;

import javax.swing.JPanel;

import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.PageFlowButtons;

/**
 * Page 2 of the Dispenser Loading Wizard.
 * @author Christopher Bartling
 */
public class NewWizardPage2 extends DispenserLoadFlowPage  implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	private JPanel parentBox = null;
	private DWTextPane label1;
    public NewWizardPage2(DispenserLoadTransaction tran,
                       String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "dispenserload/NewWizardPage2.xml", name, pageCode, buttons);
    }
    
    @Override
	public void start(int direction) {
        startStep2();
        parentBox = (JPanel) getComponent("parentBox");
        label1 = (DWTextPane) getComponent("label1");
        label1.addListener(this, this);
        dwTextPaneResized();
    }
    
    @Override
	public void finish(int direction) {
        finishStep2(direction);
        label1.removeListener(this);
    }

    @Override
	public void dwTextPaneResized() {
		int width2 = parentBox.getSize().width;
		width2= width2-10;
		label1.setWidth(width2);
	}
}
