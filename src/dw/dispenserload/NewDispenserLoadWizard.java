package dw.dispenserload;

import java.awt.event.KeyEvent;

import javax.swing.JButton;

import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNameInterface;

/**
 * New dispenser load wizard interface. It's the same as
 * the old dispenser load wizard interface except that each
 * page defines its own illustrations instead of relying on 
 * this page to do an iconlabel.setIcon() call.
 */
public class NewDispenserLoadWizard extends DispenserLoadTransactionInterface {
	private static final long serialVersionUID = 1L;

    public NewDispenserLoadWizard(DispenserLoadTransaction transaction,
                               PageNameInterface naming) {
        super("dispenserload/NewWizardPanel.xml", transaction, naming, false);	
        createPages();
    }


    /**
     * Add custom page traversal logic here. If no custom logic needed, just
     * call super.exit(direction) to do default traversal.
     */
    @Override
	public void exit(int direction) {
        if (direction == PageExitListener.CANCELED || 
                                        direction == PageExitListener.FAILED) {
            // We're canceling out of the wizard or the wizard operation has 
            // failed along the way.
            transaction.setTransactionCanceled(true);
        }
        super.exit(direction);
    }

    @Override
	protected void createPages() {
        addPage(NewWizardPage1.class, transaction, "dlwpage1", "DLW05", buttons);	 
        addPage(NewWizardPage2.class, transaction, "dlwpage2", "DLW10", buttons);	 
        addPage(NewWizardPage3.class, transaction, "dlwpage3", "DLW15", buttons);	 
        addPage(NewWizardPage4.class, transaction, "dlwpage4", "DLW20", buttons);	 
    }

    /**
     * Add the single keystroke mappings to the buttons. Use the function keys
     *   for back, next, cancel, etc.
     */
    private void addKeyMappings() {
        buttons.addKeyMapping("next", KeyEvent.VK_F12, 0);	
        buttons.addKeyMapping("cancel", KeyEvent.VK_ESCAPE, 0);	
    }

    @Override
	public void start() {
        super.start();
        addKeyMappings();
    }

    @Override
	public PageFlowButtons createButtons() {
        javax.swing.JButton[] buttons = { 
            (JButton) getComponent("nextButton"), 	
            (JButton) getComponent("cancelButton") };	
        String[] names = { "next", "cancel" };  
        return new PageFlowButtons(buttons, names, this);
    }
    
}
