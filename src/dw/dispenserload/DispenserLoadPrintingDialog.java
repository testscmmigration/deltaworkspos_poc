package dw.dispenserload;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;

import dw.dialogs.Dialogs;
import dw.framework.PrintingProgressInterface;
import dw.framework.XmlDialogPanel;
import dw.i18n.Messages;

/**
 *   DispenserLoadPrintingDialog shows the current status of the voided forms 
 *   being printed.
 *   It calls the DispenserLoadTransaction to intiate printing of the voided 
 *   forms and displays the status of the prints with a progress bar.
 */
public class DispenserLoadPrintingDialog extends XmlDialogPanel
        implements PrintingProgressInterface {

    private JLabel printingLabel = null;
    private JProgressBar printProgressBar = null;
    private JTextField serialField = null;
    private JButton cancelButton = null;
    private boolean cancelled = false;
    private int totalItems = 0;
    private int lastItem = 0;

    public DispenserLoadPrintingDialog() {
        super("dialogs/DialogDispenserLoadPrinting.xml");	
        this.title = Messages.getString("DispenserLoadPrintingDialog.1895"); 

        printingLabel = (JLabel) mainPanel.getComponent("printingLabel");	
        printProgressBar = (JProgressBar)
                                mainPanel.getComponent("printProgressBar");	
        serialField = (JTextField) mainPanel.getComponent("serialTextField");	
        cancelButton = (JButton) mainPanel.getComponent("cancelButton");	
        cancelButton.addActionListener(new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
                cancelled = true;
            }
        });
    }

    public boolean getSuccess() {
        return !cancelled;
    }

    /**
     * Eliminates the printing progress bar.
     */
    @Override
	public void done() {
        if (lastItem < totalItems) cancelled = true;
        closeDialog(Dialogs.CLOSED_OPTION);
    }

    /** Begins a printing progress bar.
     *  This should not actually display the progress bar until the first
     *  begin() call is made.
     *  @param totalNumberToPrint The total number of items to print.
     */
    @Override
	public void begin( int totalNumberToPrint) {
        cancelled = false;
        totalItems = totalNumberToPrint;
        lastItem = 0;
        printProgressBar.setMinimum(0);
        printProgressBar.setMaximum(totalNumberToPrint);
        printProgressBar.setValue(0);
        printingLabel.setText(Messages.getString("DispenserLoadPrintingDialog.1896")); 
    }

    /** Updates the printing progress bar. Sets visible true on the progress 
     *  dialog if not already visible.
     *  @param itemOrdinal The number of the current item (out of the total 
     *  number to print).
     *  @param serialNumber The serial number of the currently printing item.
     *  @param totalSoFar The amount printed through the current item, 
     *  including fees.
     *  @return Whether or not it is OK to proceed with the item.  False if 
     *  the user has cancelled the transaction.
     */
    @Override
	public boolean item( int itemOrdinal, String serialNumber,
            BigDecimal totalSoFar) {
        lastItem = itemOrdinal;
        serialField.setText(serialNumber);
        printingLabel.setText(Messages.getString("DispenserLoadPrintingDialog.1897") + itemOrdinal + Messages.getString("DispenserLoadPrintingDialog.1898") + totalItems +  
        		Messages.getString("DispenserLoadPrintingDialog.1899")); 
        printProgressBar.setValue(itemOrdinal - 1); //items will come in 1-based
        return !cancelled;
    }
    
    public void disableCancel() {
        cancelButton.setVisible(false);
    }
}
