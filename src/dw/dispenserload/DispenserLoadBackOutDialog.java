package dw.dispenserload;


import dw.framework.XmlDialogPanel;
import dw.i18n.Messages;

/**
 *  Shows feedback for backout operation in progress. 
 */
public class DispenserLoadBackOutDialog extends XmlDialogPanel
        implements BackOutProgressInterface {

    private boolean cancelled = false;

    public DispenserLoadBackOutDialog() {
        super("dialogs/DialogDispenserLoadBackOut.xml");	
        this.title = Messages.getString("DispenserLoadBackOutDialog.1894"); 
        mainPanel.addActionListener("cancelButton", this, "cancelButtonAction");	 
    }

    /** Called when cancel button is clicked. */
    public void cancelButtonAction() { 
        cancelled = true;
        closeDialog();
    }

    /** Returms the value the posting code cares about. */
    public boolean getSuccess() {
        return !cancelled;
    }

    /** Called when backout operation is complete. */
    @Override
	public void done() {
        closeDialog();
    }

    /** Called when backout operation is starting. */
    @Override
	public void begin( int totalNumberToPrint) {
    }
}
