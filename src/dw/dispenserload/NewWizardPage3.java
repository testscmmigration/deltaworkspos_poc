package dw.dispenserload;

import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.PageFlowButtons;

/**
 * Page 3 of the Dispenser Loading Wizard.
 * @author Christopher Bartling
 */
public class NewWizardPage3 extends DispenserLoadFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	private DWTextPane text1;
	private DWTextPane text2;
	
    public NewWizardPage3(
        DispenserLoadTransaction tran,
        String name,
        String pageCode,
        PageFlowButtons buttons) {
        super(tran, "dispenserload/NewWizardPage3.xml", name, pageCode, buttons); 
    }

	@Override
	public void dwTextPaneResized() {
		int width = this.getSize().width-40;
		text1.setWidth(width);
		text2.setWidth(width);
	}

    /** 
     * Called before making the panel visible.    
     */
    @Override
	public void start(int direction) {
    	// set up the Text Pane listener
		text1 = (DWTextPane) getComponent("text1");
		text2 = (DWTextPane) getComponent("text2");
		text2.addListener(this, this);

        startStep3();
        
        dwTextPaneResized();
    }

    /** 
     * Called before making the panel not visible.    
     */
    @Override
	public void finish(int direction) {
        finishStep3(direction);
    }

}
