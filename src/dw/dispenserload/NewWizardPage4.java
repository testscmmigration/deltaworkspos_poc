package dw.dispenserload;

import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.PageFlowButtons;

/**
 * Page 4 of the Dispenser Loading Wizard.
 * @author Christopher Bartling
 */
public class NewWizardPage4 extends DispenserLoadFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	private DWTextPane text1;

    public NewWizardPage4(
        DispenserLoadTransaction tran,
        String name,
        String pageCode,
        PageFlowButtons buttons) {
        super(tran, "dispenserload/NewWizardPage4.xml", name, pageCode, buttons);
    }

	@Override
	public void dwTextPaneResized() {
		int width = this.getSize().width-40;
		text1.setWidth(width);
	}

    @Override
	public void start(int direction) {
    	// set up the Text Pane listener
		text1 = (DWTextPane) getComponent("text1");
		text1.addListener(this, this);

        startStep4();

        dwTextPaneResized();
    }

    @Override
	public void finish(int direction) {
        finishStep4(direction);
    }

}
