package dw.dispenserload;

import java.util.NoSuchElementException;

import com.moneygram.agentconnect.MoneyOrderVoidReasonCodeType;

import dw.dispenser.DispenserFactory;
import dw.dispenser.DispenserInterface;
import dw.framework.ClientTransaction;
import dw.framework.PrintingProgressInterface;
import dw.i18n.Messages;
import dw.io.DiagLog;
import dw.io.diag.SecurityDiagMsg;
import dw.io.report.ReportLog;
import dw.profile.Profile;
import dw.profile.ProfileItem;
import dw.profile.UnitProfile;
import dw.utility.Debug;
import dw.utility.ErrorManager;
import flags.CompileTimeFlags;

/**
 * Encapsulates the dispenser loading functionality.
 * @author Christopher Bartling
 */
public class DispenserLoadTransaction extends ClientTransaction {
	private static final long serialVersionUID = 1L;

    // Constants
//    public static final boolean DEBUG = false;
    public static final int SELECTED_DISPENSER = 1;
    public static final String DISPENSER_PORT_ID        = "DISPENSER_NUMBER";	
    public static final String MAX_VOIDABLE_FORMS_TAG   = "MAX_VOIDABLE_FORMS";	
    public static final String DISPENSER_TAG            = "DISPENSER";	
    public static final String MO_AGENT_ID              = "MO_AGENT_ID";	
    public static final String BUNDLE_SIZE_TAG          = "BUNDLE_SIZE";	

    public static final String NO_CTS_ERROR_MSG = Messages.getString("DispenserLoadTransaction.1900"); 
    public static final String GENERAL_DISPENSER_ERROR_MSG = Messages.getString("DispenserLoadTransaction.1901"); 
    public static final String INSTANTIATION_ERROR_MSG = Messages.getString("DispenserLoadTransaction.1902"); 

    // Instance vars
    private String startingSerialNumber = "";	
    private String endingSerialNumber = "";	
    private boolean cardSwiped = false;
    private boolean transactionCanceled = false;
    private boolean forceLoad = false;
    private boolean dispenserReload = false;
    private boolean dispenserChangeUser = false;

    /**
     * Constructor.  Create a new DispenserLoadTransaction.
     */
    public DispenserLoadTransaction(String tranName) {
        //super(tranName);
        super(tranName , "");
        clear();

        // this seems kinda backward, but we don't want to block a load because
        //   we need a load.
        dispenserNeeded = false;

        // This transaction isn't time limited.
        timeoutEnabled = false;

        // This transaction has no need to print a dann thing
        receiptPrinterNeeded = false;

        // This transaction does not need manager approval
        managerApprovalRequired = false;

        // Gain access to the instances of the log files.
        // Write a security log message for the user entering in their pin.
        writeSecurityEventMessage();
    }


    public void setDispenserReload(boolean reload) {
        dispenserReload = reload;
    }

    public boolean isDispenserReload() {
        return dispenserReload;
    }

    /**
     * Override superclass method.  User id/password combinations are needed
     * for the dispenser loading transaction.
     * @return TRUE
     */
    @Override
	public boolean isNamePasswordRequired() {
        String requiredValue = UnitProfile.getInstance().get("GLOBAL_NAME_PIN_REQ", "Y");                                 
        if (requiredValue.equals("Y"))  
            return true;
        else
            return false;
    }

    /**
        Override superclass method.  User id/password combinations are needed
        for the dispenser loading transaction.
        @return TRUE
     */
    @Override
	public boolean isPasswordRequired() {
        String requiredValue = UnitProfile.getInstance().get("GLOBAL_NAME_PIN_REQ", "Y");

        if (requiredValue.equals("N"))  
            return false;
        else
            return true;
    }

    /**
     * See if this user has the ability to load the dispenser.
     */
    @Override
	public int isNamePasswordValid(String name, String password) {
        int userID = super.isNamePasswordValid(name, password);
        if (userID <= 0)
            return userID;
        // this is a valid user in general..check dispenser special case
        Profile userProfile =
            (Profile) UnitProfile.getInstance().getProfileInstance().find("USER", userID);	
        if (userProfile.find("LOAD_FORMS").booleanValue())	
            return userID;
        else
            return 0;
    }

    /**
     * Writes a FinancialDocLoad message to the diagnostic log. (206)
     */

    void writeFinancialDocLoadMessage() {
        long start, end, temp;
        start = Long.parseLong(startingSerialNumber) / 10;
        end = Long.parseLong(endingSerialNumber) / 10;
        if (start > end) {
            temp = start;
            start = end;
            end = temp;
        }

        DiagLog.getInstance().writeFinancialDocLoad(
                (short) (SELECTED_DISPENSER - 1),
                Long.toString(start),
        	Long.toString(end),
                cardSwiped,
                (short) userID);
    }


    /**
     * Writes a Security event message to the diagnostic log.
     */
    void writeSecurityEventMessage() {
        DiagLog.getInstance().writeSecurity(
                SecurityDiagMsg.LOAD_FORMS_REQUEST,
                Integer.valueOf(userID).shortValue());
    }

    /**
     * Override of ClientTransaction method. There is only one type of this
     *   interface, so just default to wizard always.
     */
    @Override
	public boolean isUsingWizard() {
        return true;
    }

    /**
     * Backs out the remaining moeny order forms from the dispenser.
     * @param progressBar An implementation of the BackOutProgressInterface
     * interface.
     */
    synchronized void backoutForm(final BackOutProgressInterface progressBar) {
        // Get the dispenser and check to make sure it's on-line.
        DispenserInterface dispenser = null;
        dispenser = DispenserFactory.getDispenser(SELECTED_DISPENSER);

        int status = dispenser.queryStatus();
        switch(status) {
            case DispenserInterface.OK:
                break;// do nothing special
            case DispenserInterface.NOT_INITIALIZED:
            case DispenserInterface.EOF:
                // ought to try to recreate the Dispenser object.
                this.setStatus(FAILED);
                return;
            case DispenserInterface.NO_CTS:
                // "The dispenser is not on or not connected"
                this.setStatus(FAILED);
                ErrorManager.handle
                    (new Exception(NO_CTS_ERROR_MSG), "dispenserNoCTSError",
                    		Messages.getString("ErrorMessages.dispenserNoCTSError"), ErrorManager.ERROR, true); 
                return;
            case DispenserInterface.WAITING:
            case DispenserInterface.ACK:
            case DispenserInterface.BAD_RESPONSE:
            case DispenserInterface.ERROR:
            case DispenserInterface.VOIDED:
            case DispenserInterface.NOT_PRINTED:
            default:
                // call the generic error dialog
                // "The dispenser encountered an error"
                this.setStatus(FAILED);
                ErrorManager.handle
                    (new Exception(GENERAL_DISPENSER_ERROR_MSG), "dispenserStatusError",
                    		Messages.getString("ErrorMessages.dispenserStatusError"), ErrorManager.ERROR, true); 
                return;
            case DispenserInterface.VERIFY_REQUIRED:
                // this should actually be the same as above
                // but for now fake it out
                dispenser.verified();
                break;
            case DispenserInterface.LOAD_REQUIRED:
                dispenser.verified();
        }

        int remainingForms = dispenser.getRemainingCount();
        progressBar.begin(remainingForms);
        int dispenserStatus = 0;
        dispenserStatus = dispenser.backOutForm();
        checkDispenserId();

        switch(dispenserStatus) {
            //The generic error means to return to the main menu, perhaps
            //after setting a flag about the dispenser's status, while the
            //load/cancel error means to allow the user to go to the
            //dispenser load screens.
            case DispenserInterface.OK:
                break;// do nothing special
            case DispenserInterface.NOT_INITIALIZED:
                // hmmm, retry to make a Dispenser object, or call the error?
                this.setStatus(FAILED);
                progressBar.done();
                ErrorManager.handle
                    (new Exception(INSTANTIATION_ERROR_MSG), "dispenserInstantiationError",
                    		Messages.getString("ErrorMessages.dispenserInstantiationError"), ErrorManager.ERROR, true); 
                return;
            case DispenserInterface.NO_CTS:
                // call the generic error dialog "The dispenser needs to
                // be connected"
                this.setStatus(FAILED);
                progressBar.done();
                ErrorManager.handle
                    (new Exception(NO_CTS_ERROR_MSG), "dispenserNoCTSError",
                    		Messages.getString("ErrorMessages.dispenserNoCTSError"), ErrorManager.ERROR, true); 
                return;
                //break;
            case DispenserInterface.WAITING:
            case DispenserInterface.ACK:
            case DispenserInterface.EOF:
            case DispenserInterface.BAD_RESPONSE:
            case DispenserInterface.ERROR:
            case DispenserInterface.LOAD_REQUIRED:
                // This is the condition we expect the dispenser to be in if we
                // successfully backed out the remaining forms.
                this.setStatus(COMPLETED);
                // tell the progress bar we are done
                progressBar.done();
                return;
            case DispenserInterface.VOIDED:
                this.setStatus(FAILED);
                progressBar.done();
                // Should something be sent to the ErrorManager?
                return;
            default:
                // This shouldn't happen.
                this.setStatus(FAILED);
                progressBar.done();
                ErrorManager.handle
                    (new Exception(GENERAL_DISPENSER_ERROR_MSG), "dispenserStatusError",
                    		Messages.getString("ErrorMessages.dispenserStatusError"), ErrorManager.ERROR, true); 
                return;

        }
        // Everything is working and completed successfully.
        this.setStatus(COMPLETED);
        progressBar.done();
    }


    /**
     * Loops through the remaining money order forms and voids each one
     * of them.
     * @param progressBar An implementation of the PrintingProgressInterface
     * interface.
     */
    public synchronized void voidRemainingForms(final PrintingProgressInterface progressBar) {
        this.voidRemainingForms(progressBar, true);
    }

    /**
     * Loops through the remaining forms and voids them. Rewritten in 4.2
     * to move most error handling responsibility to the dispenser class,
     * which is better equiped to avoid air voids and other weird errors.
     *
     * @param progressBar hook to a user feedback mechanism
     * @param commit flags whether to commit temp log after each item.
     * @author Geoff Atkin
     */
    public synchronized void voidRemainingForms(final PrintingProgressInterface progressBar,
            boolean commit) {

        boolean ok;
        DispenserInterface dispenser = DispenserFactory.getDispenser(SELECTED_DISPENSER);

        // We'll void all the remaining forms, including the last
        // form which is normally unprintable and therefore not
        // normally counted.
        int remainingForms = dispenser.getRemainingCount() + 1;

        progressBar.begin(remainingForms);
        try {
            for (int i = 0; i < remainingForms; i++) {
                String serialNumber = dispenser.getNextSerialNumber();
                if (! progressBar.item(i + 1, serialNumber, ZERO)) {
                    // progress bar returns false if user wants to cancel
                    this.setStatus(CANCELED);
                    break;
                }

                if (i == 0)
                    ok = dispenser.voidFirstRemainingForm();
                else
                    ok = dispenser.voidNextRemainingForm();

                if (ok){
                    dispenser.writeVoidRecordToLogs(serialNumber, userID, MoneyOrderVoidReasonCodeType.VOID_REMAINING);
                }
                else {
                    this.setStatus(FAILED);
                    break;
                }
            }
        }
        finally {
            progressBar.done();
        }
    }

    /**
     * Ejects a form from the dispenser.
     */
    public void ejectForm() {
        // Eject the last form in the dispenser after voiding it.
        DispenserInterface dispenser = null;
        dispenser = DispenserFactory.getDispenser(SELECTED_DISPENSER);
        dispenser.ejectForm();
    }

    /**
     * Extends the current form in the dispenser so the number can be read.
     */
    void extendForm() {
        DispenserInterface dispenser = null;
        dispenser = DispenserFactory.getDispenser(SELECTED_DISPENSER);
        dispenser.queryStatus();
        // Everything is cool to this point, so go ahead and extend the current
        // form.
        dispenser.extend();
    }



    /**
     * Retracts the current form in the dispenser so the form can be printed.
     */
    void retractForm() {
        DispenserInterface dispenser = null;
        dispenser = DispenserFactory.getDispenser(SELECTED_DISPENSER);
        dispenser.queryStatus();
        dispenser.retract();
    }


    /**
     * Get a user friendly description of this transaction's contents.
     */
    @Override
	public String toString() {
        String out = "DispenserLoadTransaction :"; 
        out += "\n\tCreated at : " + dateTime;	
        out += "\n\tStatus     : " + status;	
        return out;
    }

    /**
     * Sets the serial number range in the dispenser.
     * @return A boolean verifying that the action proceeded correctly.
     */
    boolean setSerialNumberRange() {
        boolean result = false;
        DispenserInterface dispenser = DispenserFactory.getDispenser(SELECTED_DISPENSER);

        int status = dispenser.setSerialNumberRange
            (Long.parseLong(startingSerialNumber),
             Long.parseLong(endingSerialNumber));
        switch(status) {
            case DispenserInterface.OK:
            case DispenserInterface.NOT_INITIALIZED:
            case DispenserInterface.EOF:
            case DispenserInterface.NO_CTS:
            case DispenserInterface.WAITING:
            case DispenserInterface.ACK:
            case DispenserInterface.BAD_RESPONSE:
            case DispenserInterface.ERROR:
            case DispenserInterface.VOIDED:
            case DispenserInterface.LOAD_REQUIRED:
            case DispenserInterface.NOT_PRINTED:
            default:
                // Something didn't work, so return false to the user.
                break;
            case DispenserInterface.VERIFY_REQUIRED:
                writeFinancialDocLoadMessage();
                result = true;
                break;
        }
        return result;
    }


    /**
     * Sets the starting serial number for the pack of forms.
     */
    void setStartingSerialNumber(String startingSerialNumber) {
        this.startingSerialNumber = startingSerialNumber;
    }

    /**
     * Obtains the starting serial number for the pack of forms.
     */
    String getStartingSerialNumber() {
        return this.startingSerialNumber;
    }

    /**
     * Sets the ending serial number for the pack of forms.
     */
    void setEndingSerialNumber(String endingSerialNumber) {
        this.endingSerialNumber = endingSerialNumber;
    }

    /**
     * Obtains the ending serial number for the pack of forms.
     */
    String getEndingSerialNumber() {
        return this.endingSerialNumber;
    }


    /**
     * Mutator for card swiped attribute. TRUE = the serial numbers were input
     * through a card swipe action, FALSE = the serial numbers were input
     * through the keyboard.
     * @param newCardSwiped Boolean value. TRUE = the serial numbers were input
     * through a card swipe action, FALSE = the serial numbers were input
     * through the keyboard.
     */
    void setCardSwiped(boolean newCardSwiped) {
        cardSwiped = newCardSwiped;
    }

    /**
     * Accessor for card swiped attribute. TRUE = the serial numbers were input
     * through a card swipe action, FALSE = the serial numbers were input
     * through the keyboard.
     * @return A boolean. TRUE = the serial numbers were input
     * through a card swipe action, FALSE = the serial numbers were input
     * through the keyboard.
     */
    boolean isCardSwiped() {
        return cardSwiped;
    }

    /**
     * Obtains the number of forms remaining in the dispenser.
     * @return An integer value representing the number of forms remaining
     * in the dispenser.
     */
    public int getRemainingNumberOfForms() {
        DispenserInterface dispenser = null;
        dispenser = DispenserFactory.getDispenser(SELECTED_DISPENSER);
        return dispenser.getRemainingCount();
    }

    /**
     * Poll dispenser until it's unlocked.
     */
    synchronized void pollForUnlockEvent() {
        DispenserInterface dispenser = null;
        dispenser = DispenserFactory.getDispenser(SELECTED_DISPENSER);
        // Break out of the thread if we don't have references to a
        // callback or a dispenser.

        dispenser.getStatus();

        boolean locked = dispenser.isLocked();
        int timesThrough = 0;

        // Poll until the dispenser is unlocked.
        while (locked) {
            if (transactionCanceled)
                break;
        	locked = dispenser.isLocked();
            timesThrough++;
            try {
//            	Thread.sleep(500);
                this.wait(500);
            }
            catch (InterruptedException e) {
            	Debug.printStackTrace(e);
            }
            // Shortcircuit the loop when running in debug mode.
            if (CompileTimeFlags.dspnLoadShortCircuit() && timesThrough > 50) {
            	locked = false;
            }
        }
    }


    /**
     * Poll dispenser until it's locked.
     */
    synchronized void pollForLockEvent() {
        DispenserInterface dispenser = null;
        dispenser = DispenserFactory.getDispenser(SELECTED_DISPENSER);

        // Break out of the thread if we don't have references to a
        // callback or a dispenser.
        dispenser.getStatus();

        boolean locked = dispenser.isLocked();
        int timesThrough = 0;

        // Poll until the dispenser is unlocked.
        while (!locked) {
            if (transactionCanceled) {
                break;
            }
            locked = dispenser.isLocked();
            timesThrough++;
            try {
//                Thread.sleep(500);
                this.wait(500);
            }
            catch (InterruptedException e) {
                Debug.printStackTrace(e);
            }
            // Shortcircuit the loop when running in debug mode.
            if (CompileTimeFlags.dspnLoadShortCircuit() && timesThrough > 50) {
                locked = true;
            }
        }
    }

    /**
     * Poll dispenser until it's unlocked.
     */
/*
     synchronized void pollForLockEvent(LockDispenserPromptDialog dialog) {
        pollForLockEvent();
        // Close the dialog, thus setting the dialogResult to OK_OPTION.
        dialog.closeDialog();
    }
*/

    /**
     * Determines whether the dispenser is currently in a locked state.
     * @return A boolean result of this dispenser check.  Dispenser locked =
     * TRUE.  Dispenser unlocked = FALSE.
     */
    boolean isDispenserLocked() {
        DispenserInterface dispenser = null;
        dispenser = DispenserFactory.getDispenser(SELECTED_DISPENSER);
        dispenser.getStatus();
        return dispenser.isLocked();
    }

    /**
     * Validates the serial number with the dispenser.
     * Returns false if s doesn't parse to a number or is
     * shorter than 9 characters or parses to 0.
     *
     * Otherwise returns true if the check digit is correct.
     */
    boolean isValidSerialNumber(String s) {
        if (s.length() < 9)
            return false;

        long serialNumber = 0;
        try {
            serialNumber = Long.parseLong(s);
        }
        catch (NumberFormatException e) {
            return false;
        }

        if (serialNumber == 0)
            return false;

        DispenserInterface dispenser = null;
        dispenser = DispenserFactory.getDispenser(SELECTED_DISPENSER);
        return dispenser.isValid(serialNumber);
    }

    /**
     * Determines whether the serial number range has been used or is
     * currently in use.  Necessary for maintaining security and avoiding
     * the possibility of writing a duplicate serial number on a form.
     * @return A boolean stating whether the serial number range has been used
     * or is currently in use.
     */
    boolean isSerialNumberRangeInUse(final long startingSerialNumber,
                                                final long endingSerialNumber) {
        DispenserInterface dispenser = null;
        dispenser = DispenserFactory.getDispenser(SELECTED_DISPENSER);
        return dispenser.inUse(startingSerialNumber, endingSerialNumber);
    }

    /**
     * Sends the dispenser status to standard output.
     */
    void printDispenserStatus(final String debugMessage) {
        DispenserInterface dispenser = null;
        dispenser = DispenserFactory.getDispenser(SELECTED_DISPENSER);
        dispenser.getStatus();
        Debug.println(debugMessage);
    }

    /**
     * Determines whether the remaining money order forms can be voided
     * through the dispenser.  There is an upper limit to how many money
     * order forms can be voided.
     * @return A boolean stating whether the remaining money order forms
     * can be voided through the dispenser.
     */
    boolean canVoidRemainingForms() {
        DispenserInterface dispenser = null;
        dispenser = DispenserFactory.getDispenser(SELECTED_DISPENSER);

        dispenser.getStatus();

        int numberOfRemainingForms = dispenser.getRemainingCount();
        if (numberOfRemainingForms == 0) return false;


        // Is the number of remaining money order forms less than or equal to
        // the maximum number of forms allowed for voiding?

        return (numberOfRemainingForms <= getMaxVoidableForms());
    }

    /** Returns the maximum number of forms we can void at end of pack. */
    int getMaxVoidableForms() {
        int maxVoidableForms = 0;

        try {
            // Obtain the maximum voidable forms value by navigating through
            // the unit profile and the dispenser profile.
            Profile dispenserProfile =
                (Profile) UnitProfile.getInstance().getProfileInstance().find(
                    DISPENSER_TAG, SELECTED_DISPENSER);
            maxVoidableForms = dispenserProfile.find(
                    MAX_VOIDABLE_FORMS_TAG).intValue();
        }
        catch (NoSuchElementException exc) {
        }
        return maxVoidableForms;
    }

    /**
     * Verifies that the final 4 digits of the current serial number,
     * including the check digit.
     * @return A boolean stating whether the final 4 digits of the currently
     * extended money order form serial number, including the check digit,
     * match the digits of starting serial number set in the dispenser.
     */
    boolean verifySerialNumberDigits(long serialNumberDigits) {
        DispenserInterface dispenser = null;
        if (UnitProfile.getInstance().isDemoMode() && getDispenserStatus() ==DispenserInterface.NO_CTS)
           return true;

        dispenser = DispenserFactory.getDispenser(SELECTED_DISPENSER);
        dispenser.getStatus();
        boolean result = dispenser.verify(serialNumberDigits);
        return result;
    }



    /**
     * Verifies that the range value between the starting and ending
     * serial number is not larger than the bundle size constant found
     * in the profile. Returns true if the profile item is not found.
     * @return A boolean stating whether the range value between the
     * starting and ending serial number is not larger than the bundle
     * size constant found in the profile.
     */
    boolean isBundleSizeValid(String startingSerialNumber,
            String endingSerialNumber) {
        // Strip off the the check digits from the incoming serial numbers.
        String start = startingSerialNumber.substring(0,
                startingSerialNumber.length() - 1);
        String end = endingSerialNumber.substring(0,
                endingSerialNumber.length() - 1);
        boolean result = false;
        long range = 0;
        try {
        	range = (Long.parseLong(end) - Long.parseLong(start));
        } catch (Exception e) {
        }
        try {
            Profile dispenserProfile =
                (Profile) UnitProfile.getInstance().getProfileInstance().find(DISPENSER_TAG,
                                                         SELECTED_DISPENSER);
            ProfileItem bundleSizeItem =
                dispenserProfile.find(BUNDLE_SIZE_TAG);
            long bundleSize = 0;
            try {
            	bundleSize = Long.parseLong(bundleSizeItem.stringValue());
            } catch (Exception e) {
            }
            if (range <= bundleSize) result = true;
        }
        catch (NoSuchElementException e) {
            result = true;
        }
        catch (NumberFormatException e) {
            result = true;
        }
        return result;
    }

    /**
     * Sets the transaction to canceled status.  Used for polling thread
     * cleanup.
     * @param canceled A boolean.
     */
    void setTransactionCanceled(boolean canceled) {
        transactionCanceled = canceled;
        if (transactionCanceled && forceLoad) {
            setLoadRequired();
        }
    }

    /**
     * Is the transaction in canceled status.  Used for polling thread
     * cleanup.
     * @return A boolean.
     */
    boolean isTransactionCanceled() {
        return transactionCanceled;
    }

    /**
     * Sets the dispenser into load state.  Used when the user has unloaded
     * or voided the remaining forms, but did not complete the dispenser
     * load process.
     */
    void setLoadRequired() {
        DispenserInterface dispenser = null;
        dispenser = DispenserFactory.getDispenser(SELECTED_DISPENSER);
        dispenser.setLoadRequired();
    }

    /**
     * Retrieve the status of the dispenser.
     * @return A integer value representing the current status of the
     * dispenser.
     */
    int getDispenserStatus() {
        DispenserInterface dispenser = null;
        dispenser = DispenserFactory.getDispenser(SELECTED_DISPENSER);
        return dispenser.getStatus();
    }


    /**
      Forces the dispenser to be loaded if the wizard is canceled.
      @param forceLoad A boolean flag which states that the dispenser must
      be loaded.
     */
    void setForceLoad(boolean forceLoad) {
        this.forceLoad = forceLoad;
    }


    /**
     * States whether the dispenser is powered on.
     * @return A boolean flag stating whether the dispenser is powered on
     * or not.
     */
    boolean isDispenserPoweredOn() {
        boolean result = true;
        DispenserInterface dispenser = null;
        dispenser = DispenserFactory.getDispenser(SELECTED_DISPENSER);
        if (dispenser.queryStatus() == DispenserInterface.NO_CTS) {
            // Dispenser is powered off or otherwise noncommunicative.
            result = false;
        }
        return result;
    }

    /**
     * Writes a dispenser loaded log record to the ReportLog object.
     */
    void writeReportLogRecord() {
        // write to ReportLog
        ReportLog.logDispenserLoad((short) SELECTED_DISPENSER,
                startingSerialNumber, endingSerialNumber, cardSwiped, String.valueOf(userID));
    }
        // check to see if the dispenser has changed
        private void checkDispenserId() {
             UnitProfile.getInstance().setDispenserId(DispenserFactory.getDispenser(1).
                                   getDispenserSerialNumber());
        }

    public void setDispenserChangeUser(boolean status)
    {
        dispenserChangeUser = status;
    }

    public boolean getDispenserChangeUser()
    {
        return dispenserChangeUser;
    }

	@Override
	public boolean isFinancialTransaction() {
		return false;
	}

	@Override
	public boolean isCheckinRequired() {
		return false;
	}
    
    @Override
	public void setFinancialTransaction(boolean b)
    {
        // Do nothing.
    }

}



