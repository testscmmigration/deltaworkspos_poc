package dw.multiagent;

import java.awt.Color;
import java.awt.Component;
import java.awt.SystemColor;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.moneygram.agentconnect.SubagentInfo;

import dw.dwgui.DWTable;
import dw.dwgui.DWTable.DWTableListener;
import dw.dwgui.DWTextField;
import dw.dwgui.RowSelectCellRenderer;
import dw.framework.ClientTransaction;
import dw.framework.FlowPage;
import dw.framework.KeyMapManager;
import dw.framework.PageExitListener;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.main.DeltaworksStartup;
import dw.mgsend.MoneyGramClientTransaction;
import dw.model.adapters.CountryInfo;
import dw.profile.UnitProfile;
import dw.utility.ReceiptTemplates;
import dw.utility.StringUtility;

/**
 * The main page for the Multi Agent Mode functionality.
 * @author Rob Morgan,
 */
public class MultiAgentFlowPage extends FlowPage implements DWTableListener {
	private static final long serialVersionUID = 1L;

	// component references
    private JTextField searchInputField = null;
    
    private JButton okButton = null;
    private JButton searchButton = null;
    private JButton nextInvisibleButton;

    // table information
    private DWTable table;
    private JLabel enterLabel;
    private DefaultTableModel tableModel;
 
    protected MultiAgentTransaction transaction;
    private SubagentInfo[] agents;
 
    private static int AGENT_ID_COLUMN = 0;
    private static int LEGACY_ID_COLUMN = 1;
    private static int CITY_COLUMN = 2;
    private static int BRANCH_COLUMN = 3;
    private static int STORE_COLUMN = 4;
    private static int STORE_NAME_COLUMN = 5;
       
    private boolean refreshButtonSelected = false;
             
    public MultiAgentFlowPage(MultiAgentTransaction tran, 
            String name, String pageCode) {
        super("multiagent/MultiAgents.xml", name, pageCode, null);    
        transaction = tran;
        setupComponents();
    }
    
    private void setupComponents() {

        searchInputField = (JTextField) getComponent("searchInputField");
        searchButton = (JButton) getComponent("searchButton");  
        okButton = (JButton) getComponent("okButton");  
        enterLabel = (JLabel) getComponent("pressEnterLabel");  
        setupTable();
    }

    /**
     * Create and set upp the money order table. Add the table to the scrollpane
     *   defined in the XML.
     */
    private void setupTable() {
        
        String[] columnNames = { 
        		Messages.getString("MultiAgentFlowPage.1800"),  
        		Messages.getString("MultiAgentFlowPage.1801"),  
        		Messages.getString("MultiAgentFlowPage.1802"),  
        		Messages.getString("MultiAgentFlowPage.1803"),  
        		Messages.getString("MultiAgentFlowPage.1804"),  
        		Messages.getString("MultiAgentFlowPage.1805") 
        	};  
        tableModel = new MultiAgentTableModel(columnNames, 0, STORE_NAME_COLUMN, transaction);
        
        table = (DWTable) this.getComponent("agentsTable");
        table.getTable().setModel(tableModel);
//        MultiagentCellRenderer renderer = new MultiagentCellRenderer();
        RowSelectCellRenderer.setAsDefaultRenderer(table.getTable());
        
//        TableColumnModel colModel = table.getTable().getColumnModel();
        table.getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

//        colModel.getColumn(STORE_NAME_COLUMN).setCellRenderer(renderer);
        
        
		TableColumnModel model = table.getTable().getColumnModel();
		table.getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    	for (int i = 0; i < model.getColumnCount(); i++) {
    		TableColumn column = model.getColumn(i);
        	column.setCellRenderer(table.new DWTableCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));
    	}
        model.getColumn(AGENT_ID_COLUMN).setCellEditor(new DefaultCellEditor(new DWTextField()));
        
		table.getTable().getTableHeader().setResizingAllowed(true);

        KeyMapManager.mapComponent(this, KeyEvent.VK_F6, 0, searchButton);
        KeyMapManager.mapMethod(this, KeyEvent.VK_ESCAPE, 0, "escape"); 
        setButtonsEnabled(true);
        if (table.getTable().getRowCount() > 0) {
        	table.getTable().requestFocus();
        }
		table.addListener(this, this);
    }

    private void updateTable(SubagentInfo[] updateAgents) {
        int rows = tableModel.getRowCount();
        
        for (int i = 0; i < rows; i++)
            tableModel.removeRow(0);

        if (updateAgents == null) {
            table.getTable().setEnabled(false);
            return;
        }
        
        for (int i = 0; i < updateAgents.length; i++) {
           // String store = updateAgents[i].getStoreNumber();
            String[] row = {String.valueOf(updateAgents[i].getAgentID()),
                updateAgents[i].getLegacyAgentID(),
                updateAgents[i].getCity(),
                updateAgents[i].getBranchName(),
                updateAgents[i].getStoreNumber(),
				updateAgents[i].getAgentStoreName()};
            tableModel.addRow(row);
        }
        table.getTable().setEnabled(table.getTable().getRowCount() > 0);
        enterLabel.setVisible(updateAgents.length > 0);
        
		JScrollPane scrollPane = (JScrollPane) this.getComponent("scrollpane");
		JPanel panel = (JPanel) this.getComponent("panel");
		table.initializeTable(scrollPane, panel, 500, 10);
		dwTableResized();
    }
    
    private void populateTable() {
        int rows = tableModel.getRowCount();
        for (int i = 0; i < rows; i++) {
            tableModel.removeRow(0);
        }
        agents = transaction.getAgents(); 
		if (agents == null || agents.length == 0 || agents[0] == null) {
            table.getTable().setEnabled(false);
            return;
        }
        for (int i = 0; i < agents.length; i++) {
            String[] row = {String.valueOf(agents[i].getAgentID()),
                            agents[i].getLegacyAgentID(),
                            agents[i].getCity(),
                            agents[i].getBranchName(),
                            agents[i].getStoreNumber(),
							agents[i].getAgentStoreName()}; 
            tableModel.addRow(row);
            
        }
        table.getTable().setEnabled(table.getTable().getRowCount() > 0);
        enterLabel.setVisible(agents.length > 0);
        
		JScrollPane scrollPane = (JScrollPane) this.getComponent("scrollpane");
		JPanel panel = (JPanel) this.getComponent("panel");
		table.initializeTable(scrollPane, panel, 500, 10);
		table.setColumnStaticWidth(0);
		table.setColumnStaticWidth(1);
		table.setColumnStaticWidth(4);

		dwTableResized();
    }
    
    private boolean isCriteriaEntered() {
        return   (searchInputField.getText().length() > 0);
    }
    
    public void setBoxStates() {
        // set the state of the searchbutton...only allowed if criteria entered
        searchButton.setEnabled(isCriteriaEntered());
    }

    public void doSearch() {
        searchButton.doClick();
    }

    public void refreshButtonAction() {
         // Transaction.getAgentList() writes the new values to memory.
        // so we just call populateTable() to display what is in memory.
        refreshButtonSelected = true;
        clearFields();
        transaction.getAgentList(this);
    }

    /**
     * Notify listeners that the user wishes to close the transaction.
     */
    public void okButtonAction() {
        nextInvisibleButton.setEnabled(false);
        int row = table.getTable().getSelectedRow();
        if (row > -1){
            // Create and populate the info with the selected data.
        	String device = UnitProfile.getInstance().get("DEVICE_ID", "");
        	if (! device.equals("")) {  		
        	    UnitProfile.getInstance().set("SUPER_AGENT_DEVICE_ID", device, "P",  true);
        	}
        	SubagentInfo selected = new SubagentInfo();
			String agentID = tableModel.getValueAt(row, AGENT_ID_COLUMN) != null
					? tableModel.getValueAt(row, AGENT_ID_COLUMN).toString() : "0";
            
			int agentId = 0;
			try {
				agentId = Integer.parseInt(agentID);
			} catch (Exception e) {
			}
			selected.setAgentID(agentId);
			String legacyID = tableModel.getValueAt(row, LEGACY_ID_COLUMN) != null
					? tableModel.getValueAt(row, LEGACY_ID_COLUMN).toString() : "";
            selected.setLegacyAgentID(legacyID);
			String city = tableModel.getValueAt(row, CITY_COLUMN) != null
					? tableModel.getValueAt(row, CITY_COLUMN).toString() : "";
		    selected.setCity(city);
			String branchName = tableModel.getValueAt(row, BRANCH_COLUMN) != null
					? tableModel.getValueAt(row, BRANCH_COLUMN).toString() : "";
			selected.setBranchName(branchName);
			if (tableModel.getValueAt(row, STORE_COLUMN) != null) {
				selected.setStoreNumber(
						StringUtility.getNonNullString(tableModel.getValueAt(row, STORE_COLUMN).toString()));
			}
			int seqNo = transaction.getSequenceNumber(agentID);
            selected.setSequenceNumber(Integer.valueOf(seqNo));
			String storeName = tableModel.getValueAt(row, STORE_NAME_COLUMN) != null
					? tableModel.getValueAt(row, STORE_NAME_COLUMN).toString() : "";
            selected.setAgentStoreName(storeName);
            
            //Save the true identity(unit Profile ID) in state file.
            transaction.storeTrueIdentity();
            
            // Save current profile changes and switch to the new agent if successful. 
           // transaction.saveProfileChanges(this);
            transaction.changeToNewAgent(selected,this);
            UnitProfile.getInstance().setSubAgentTakeOverUserID(transaction.getUserID());
            } else {
            transaction.setStatus(ClientTransaction.CANCELED);
            finish(PageExitListener.CANCELED);
        }
    }
    
    /**
     * Perform a search for receives which match the given criteria. If there
     *   are no matches, show an appropriate error dialog and let the user
     *   search again.
     */
    public void searchButtonAction() {
        findAgents();
    }
    
    public void saveButtonAction() {
    	transaction.updateSubagentValue(this);
    }
    
    private void clearFields() {
        this.searchInputField.setText("");
    }
    
    @Override
	public void start(int direction) {

        enterLabel.setVisible(false);
        searchButton.setVisible(true);
        
        nextInvisibleButton = (JButton) getComponent("nextInvisibleButton");    
        nextInvisibleButton.setBorder(null);
        addActionListener("nextInvisibleButton", this, "entrySelected");     
        addReturnTabListeners(new Component[0], okButton);
        
        
        if (direction == PageExitListener.NEXT)
            clearFields();
        else if (table.getTable().getRowCount() > 0)
            enterLabel.setVisible(true);
        final Object parent = this;
        addKeyListener("agentsTable", new KeyAdapter() {   
            @Override
			public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    e.consume();
                    okButton.requestFocus(true);
                    okButton.doClick();
                }
                if (e.getKeyCode() == KeyEvent.VK_TAB) {
                    e.consume();
                    nextInvisibleButton.setEnabled(false);
                    focusNextComponent("agentsTable"); 
                    nextInvisibleButton.setEnabled(true);
                    table.getTable().clearSelection();
                }
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    e.consume();
                    PageNotification.notifyExitListeners(parent,
                                                     PageExitListener.CANCELED);
                }
            }});
        KeyMapManager.clear();
        KeyMapManager.mapComponent(this, KeyEvent.VK_F6, 0, searchButton);
        KeyMapManager.mapMethod(this, KeyEvent.VK_ESCAPE, 0, "escape"); 
        addTextListener(this.searchInputField, this, "setBoxStates");
        addActionListener("searchInputField", this, "doSearch");
        addActionListener("searchButton", this);    
        addActionListener("okButton", this);    
        addActionListener("refreshButton", this);    
        addActionListener("saveButton", this);    
        this.searchInputField.addFocusListener(new FocusAdapter() {
                @Override
				public void focusGained(FocusEvent e) { 
                    table.getTable().clearSelection();
                    populateTable();                    
                }
        });
     // this line saves the cell being edited when save is clicked 
        table.getTable().putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
          
        if((DeltaworksStartup.getInstance().isStartingFirstTime())
            ||(MultiAgentTransaction.agentList.isEmpty())){
           transaction.getAgentList(this);
           populateTable();
        }
        addFocusGainedListener("agentsTable", this);   
        startFocusOrder();
        
    }
       
    public void agentsTableFocusGained() {
        table.getTable().setRowSelectionInterval(0, 0);
    }
    
    private void startFocusOrder() {
        setBoxStates();
        searchInputField.requestFocus();
    }
   
    
    public void escape(){
        PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
    }

    @Override
	public void finish(int direction) {
    	KeyMapManager.clear();
        PageNotification.notifyExitListeners(this, direction);
    }

    public void entrySelected() {
        nextInvisibleButton.setEnabled(false);
        
    }            
  
    private void findAgents() {
        int rows = table.getTable().getRowCount();
        SubagentInfo[] agentList = new SubagentInfo[rows];
        int cnt =0;

        if (rows > -1){
        	String searchString = StringUtility.getNonNullString(searchInputField.getText()).trim();
			for (int i = 0; i < rows; i++) {

				String tvLegacy = tableModel.getValueAt(i, LEGACY_ID_COLUMN) != null
						? tableModel.getValueAt(i, LEGACY_ID_COLUMN).toString() : "";
				String tvCity = tableModel.getValueAt(i, CITY_COLUMN) != null
						? tableModel.getValueAt(i, CITY_COLUMN).toString() : "";
				String tvStore = tableModel.getValueAt(i, STORE_COLUMN) != null
						? tableModel.getValueAt(i, STORE_COLUMN).toString() : "";
				String tvStoreName = tableModel.getValueAt(i, STORE_NAME_COLUMN) != null
						? tableModel.getValueAt(i, STORE_NAME_COLUMN).toString() : "";

				if (tvStore.trim().equals(searchString)
						|| tableModel.getValueAt(i, AGENT_ID_COLUMN).toString().trim().equals(searchString)
						|| tvLegacy.trim().equals(searchString)
						|| tvCity.trim().toUpperCase().startsWith(searchString.toUpperCase().trim())
						|| tableModel.getValueAt(i, BRANCH_COLUMN).toString().trim().toUpperCase()
								.startsWith(searchInputField.getText().toUpperCase().trim())
						|| tvStoreName.trim().toUpperCase().startsWith(searchString.toUpperCase().trim())) {

					SubagentInfo agent = new SubagentInfo();
					agent.setStoreNumber(tvStore.trim());
					int agentId = 0;
					try {
						agentId = Integer.parseInt(tableModel.getValueAt(i, AGENT_ID_COLUMN).toString().trim());
					} catch (Exception e) {
					}
					agent.setAgentID(agentId);
					String legacyID = tableModel.getValueAt(i, LEGACY_ID_COLUMN) != null
							? tableModel.getValueAt(i, LEGACY_ID_COLUMN).toString().trim() : "";
					agent.setLegacyAgentID(legacyID);
					agent.setCity(tvCity.trim());
					agent.setBranchName(tableModel.getValueAt(i, BRANCH_COLUMN).toString().trim());
					agent.setAgentStoreName(tvStoreName.trim());

					agentList[cnt] = agent;
					cnt++;
				}
			}
        }
        
        // don't pass updateTable empty entries in the array.
        SubagentInfo[] finalAgentList = new SubagentInfo[cnt];
        for (int i=0; i < cnt; i++)
            finalAgentList[i] = agentList[i];
        updateTable(finalAgentList);
        okButton.requestFocus(true);
           
    }
    
    @Override
	public void commComplete(int commTag, Object returnValue) {
        boolean b = ((Boolean) returnValue).booleanValue();
        switch (commTag) {
            case MoneyGramClientTransaction.SUBAGENTS:
                if(b){
                    if(refreshButtonSelected){
                        populateTable();  
                        refreshButtonSelected = false; 
                    }
                    return;
                } 
                else{
                    PageNotification.notifyExitListeners(this, PageExitListener.DONOTHING);
                    searchInputField.requestFocus();
                }
                    
            break;  
            case MoneyGramClientTransaction.SAVE_SUBAGENTS:  
            case MoneyGramClientTransaction.DW_PROFILE:  
                if (b) {
                	ReceiptTemplates.update(this, CountryInfo.MANUAL_TRANSMIT_RETERIVAL); 
                  return;
                }
                else if (transaction.getStatus() == ClientTransaction.CANCELED) {
                    PageNotification.notifyExitListeners(this, PageExitListener.DONOTHING);
                }
                else{
                    PageNotification.notifyExitListeners(this, PageExitListener.FAILED);
                }
            case MoneyGramClientTransaction.RECEIPTS_FORMAT_DETAILS:  
                if (b) {
                  transaction.setStatus(ClientTransaction.COMPLETED); 
                  finish(PageExitListener.COMPLETED);
                }
                else if (transaction.getStatus() == ClientTransaction.CANCELED) {
                    PageNotification.notifyExitListeners(this, PageExitListener.DONOTHING);
                }
                else{
                    PageNotification.notifyExitListeners(this, PageExitListener.FAILED);
                }
        }
    }  
    
    public void refreshTable() {
    	populateTable();
    	table.getTable().repaint();
    }
    
    
    class SemiEditableTableModel extends DefaultTableModel {
    	private static final long serialVersionUID = 1L;

    	int[] editableCols;

        public SemiEditableTableModel(Object[] colNames, int numRows,
                                      int[] editableCols) {
            super(colNames, numRows);
            this.editableCols = editableCols;
        }

        public SemiEditableTableModel(Object[] colNames, int numRows,
                                      int editableCol) {
            this(colNames, numRows, null);
            int[] editCols = {editableCol};
            editableCols = editCols;
        }

        @Override
		public boolean isCellEditable(int row, int col) {
            if(row ==0)
            	return false;
        	for (int i = 0; i < editableCols.length; i++)
                if (editableCols[i] == col)
                    return true;
            return false;
        }
    }

    
    class MultiAgentTableModel extends SemiEditableTableModel  {
    	private static final long serialVersionUID = 1L;

    	MultiAgentTransaction transaction;

        public MultiAgentTableModel(Object[] colNames, int numRows,
                                    int storeCol, MultiAgentTransaction tran) {
            super(colNames, numRows, storeCol);
            this.transaction = tran;
        }

        /**
         * When a store-name is set in the model, update the corresponding subagent 
         *   in the transaction.
         */
        
        @Override
		public void setValueAt(Object value, int row, int col) {
        	String agentID = tableModel.getValueAt(row, AGENT_ID_COLUMN).toString();
        	String storeName = (String)value;
        	transaction.updateStoreNameForSubAgent(agentID, storeName);
        	super.setValueAt(value, row, col);
        	refreshTable();
        }
    }
    
    class MultiagentCellRenderer extends DefaultTableCellRenderer {
    	private static final long serialVersionUID = 1L;

    	@Override
		public Component getTableCellRendererComponent(JTable table,
                                                       Object value,
                                                       boolean isSelected,
                                                       boolean hasFocus,
                                                       int row,
                                                       int column) {
            Component rend = super.getTableCellRendererComponent(table, value,
                                         isSelected, hasFocus, row, column);
            if (isSelected && hasFocus) {
                rend.setBackground(Color.white);
                rend.setForeground(Color.black);
                ((JComponent)rend).setBorder(BorderFactory.createLineBorder(
                                                    SystemColor.textHighlight));
            }
            else if (isSelected) {
                    rend.setBackground(SystemColor.textHighlight);
                    rend.setForeground(Color.white);
            }
            else {
                rend.setBackground(Color.white);
                rend.setForeground(Color.black);
            }
            return rend;
        }
    }

	@Override
	public void dwTableResized() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.resizeTable();
			}
		});
	}
}
