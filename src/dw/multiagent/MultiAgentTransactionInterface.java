package dw.multiagent;


import javax.swing.JButton;

import dw.framework.ClientTransaction;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNameInterface;
import dw.framework.TransactionInterface;
import dw.main.DeltaworksStartup;

/**
 * TransactionInterface implementation for Multi Agent Mode functionality.
 * @author Chris Bartling
 */
public class MultiAgentTransactionInterface
                                                extends TransactionInterface {
	private static final long serialVersionUID = 1L;

	protected MultiAgentTransaction transaction;
    
    // Used for testing purposes   -- Renuka - commenting these out - not used any where.
//    private String version = null;
//    private String profileFileName = "profile.xml"; 
    // exit codes to send back to indicate app exit status
//    public static final int NORMAL_EXIT = 0;
//    public static final int ERROR_EXIT = 1;
//    public static final int RESTART_EXIT = 99;
    // status severity codes
//    public static final int INFORMATION = 0;
//    public static final int WARNING = 1;
//    public static final int ERROR = 2;

    /** 
     * Constructor.
     */
    public MultiAgentTransactionInterface(
                                 MultiAgentTransaction transaction,
                                 PageNameInterface naming) {
        super("EmptyPanel.xml", naming, false);    
        this.transaction = transaction;
        createPages();
    }

    @Override
	public PageFlowButtons createButtons() {
        JButton[] buttons = {};
        String[] names = {};
        return new PageFlowButtons(buttons, names, this);
    }

    @Override
	public ClientTransaction getTransaction() {
        return transaction;
    }

    @Override
	public void createPages() {
        addPage(MultiAgentFlowPage.class, transaction, "agentdirfp", "DW005");   
    }

    /**
     * Multi Agent mode UI is only one screen, so just call super.exit(COMPLETE).
     */
    @Override
	public void exit(int direction) {
        if (direction == PageExitListener.DONOTHING){
          return;
        }
        else{        	
            DeltaworksStartup.getInstance().setStartingFirstTime(false);
            if (direction == PageExitListener.TIMEOUT)
            	super.exit(direction);
            else
            	super.exit(PageExitListener.COMPLETED);
        }
        
    }
    
    
    
}
