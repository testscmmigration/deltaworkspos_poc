package dw.multiagent;

import java.util.ArrayList;
import java.util.Iterator;

import com.moneygram.agentconnect.SubagentInfo;
import com.moneygram.agentconnect.SubagentProfileUpdateType;

import dw.comm.MessageFacade;
import dw.comm.MessageFacadeError;
import dw.comm.MessageFacadeParam;
import dw.comm.MessageLogic;
import dw.framework.ClientTransaction;
import dw.framework.CommCompleteInterface;
import dw.main.DeltaworksStartup;
import dw.mgsend.MoneyGramClientTransaction;
import dw.profile.UnitProfile;
import dw.simplepm.SimplePMTransaction;
import dw.utility.StringUtility;

/**
 * MultiAgentTransaction encapsulates the directory of agents.
 */
public class MultiAgentTransaction extends ClientTransaction {
	private static final long serialVersionUID = 1L;

	// Instance vars
	public static final ArrayList<SubagentInfo> agentList = new ArrayList<SubagentInfo>();
   	private SubagentInfo[] agents = null;

    private SubagentProfileUpdateType[] subAgentProfileItemsToUpdate ;
    ArrayList<SubagentProfileUpdateType> subagentItemList = new ArrayList<SubagentProfileUpdateType>();
    private int count=0;
        
    public SubagentInfo[] getAgents() {
	    if (!DeltaworksStartup.getInstance().isStartingFirstTime()) {
		//	this.createAgents();
            agents = agentList.toArray(new SubagentInfo[0]);
			return agents;
		} else
			return agents;
	}

	/**
	 * Create a new MultiAgentTransaction.
	 * Before calling this method, invoke UnitProfile.getUPInstance().load().
	 */
	public MultiAgentTransaction(String tranName) {
        super(tranName, "");
		timeoutEnabled = true;
		dispenserNeeded = false;
		receiptPrinterNeeded = false;
     //   agentList = new ArrayList();
		clear();
	}

	/**
	 * Override of method in Client Transaction. User id/password combinations are needed
	 * for the multi agent mode transaction.
	 * @return boolean
	 */
	@Override
	public boolean isNamePasswordRequired() {
		String requiredValue = UnitProfile.getInstance().get("GLOBAL_NAME_PIN_REQ", "Y"); 
		if (requiredValue.equals("Y")) 
			return true;
		else
			return false;

	}

	/**
	 * Override of method in Client Transaction.
	 *  User id/password combinations are needed
	 *   for the multi agent mode transaction.
	 */
	@Override
	public boolean isPasswordRequired() {
		String requiredValue = UnitProfile.getInstance().get("GLOBAL_NAME_PIN_REQ", "Y");
		if (requiredValue.equals("N")) 
			return false;
		else
			return true;
	}

	/**
	 * Override of ClientTransaction method. There is only one type of this
	 *   interface, so just default to wizard always.
	 */
	@Override
	public boolean isUsingWizard() {
		return true;
	}

	@Override
	public boolean isFinancialTransaction() {
		return false;
	}
    
    @Override
	public void setFinancialTransaction(boolean b)
    {
        // Do nothing.
    }

	@Override
	public boolean isCheckinRequired() {
		return false;
	}

	public void storeTrueIdentity() {
		UnitProfile.getInstance().saveSuperagentUnitProfileID();
	}

	public int getTrueIdentity() {
		return UnitProfile.getInstance().getUnitProfileID();
	}

	public void changeToNewAgent(
		final SubagentInfo newAgent,
		final CommCompleteInterface callingPage) {

        final MessageFacadeParam parameters =
			new MessageFacadeParam(MessageFacadeParam.REAL_TIME,
                                   MessageFacadeParam.USER_CANCEL);
        
		final MessageLogic logic = new MessageLogic() {
			String newProfile = ""; 
			int agentID = newAgent.getAgentID();
            int sequenceNumber = newAgent.getSequenceNumber(); 
            int unitProfileID = getTrueIdentity();
            String legacyAgentID=newAgent.getLegacyAgentID();
           	
            		
			
            @Override
			public Object run() {
                SimplePMTransaction spm =
                                    SimplePMTransaction.getCurrentInstance();
                boolean successfulSend = spm.send(parameters);
                if (!successfulSend)
                    return Boolean.FALSE;
                     
				try {
                     newProfile =
						MessageFacade.getInstance().getSubagentProfile(
							parameters,
							unitProfileID,
							agentID+"",
                            sequenceNumber+"");
					//install the new profile.
					//copy profile items such as the connectivity URL to the subagent profile
					// have to distinguish here if we are requesting for subagent/superagent profile.
                    //non-transactional superagent profile does not have legacy agentID.
                    //set the flag only if profile request was successful.
                    if(null != newProfile){
                        if(!legacyAgentID.equals(""))
                            UnitProfile.getInstance().setTakeOverSubagentprofile(true);
                        else
                            UnitProfile.getInstance().setTakeOverSubagentprofile(false);     
					    UnitProfile.getInstance().setNewProfile(newProfile);
                        return Boolean.TRUE;
                    }
                    else
                        return Boolean.FALSE;
                 } catch (MessageFacadeError mfe) {
                    if (mfe.hasErrorCode(MessageFacadeParam.USER_CANCEL)) {
                        setStatus(ClientTransaction.CANCELED);
                        return Boolean.FALSE;
                    } 
                    else                  
					return Boolean.FALSE;
				} catch (Exception e) {
					return Boolean.FALSE;
				}
			}
		};
		MessageFacade.run(logic, callingPage, MoneyGramClientTransaction.DW_PROFILE, Boolean.TRUE);
	}

	/**
	 * Query RTS for list of agents and store it in memory to reuse throughout the day.
	 * @param callingPage
	 */
	public void getAgentList(final CommCompleteInterface callingPage) {
        final MessageFacadeParam parameters =
			new MessageFacadeParam(MessageFacadeParam.REAL_TIME);
		final MessageLogic logic = new MessageLogic() {

			@Override
			public Object run() {
				agents = new SubagentInfo[0];
				try {
					agents =
						MessageFacade.getInstance().getAgentList(
							callingPage,
							parameters);
                    if ((agents == null) || (agents.length == 0)) {
                        return Boolean.FALSE;
                    } else {
                        storeAgentListInMemory(agents);
                        return Boolean.TRUE;               
                    }
					
				} catch (MessageFacadeError mfe) {
					return Boolean.FALSE;
				} catch (Exception e) {
					return Boolean.FALSE;
				}
			}
		};
		MessageFacade.run(logic, callingPage, MoneyGramClientTransaction.SUBAGENTS, Boolean.TRUE);
	}

	public void storeAgentListInMemory(SubagentInfo[] agents) {
		agentList.clear();
		
		// add all the items in the array to the list.
		for (int i = 0; i < agents.length; i++) {
			 if(StringUtility.getNonNullString(agents[i].getLegacyAgentID()).equals(""))
			 	UnitProfile.getInstance().setAgentInfo(agents[i]);
			agentList.add(i, agents[i]);
        }

	}
	
	public void updateSubagentValue(final CommCompleteInterface callingPage) {
		final MessageFacadeParam parameters =
			new MessageFacadeParam(MessageFacadeParam.REAL_TIME,
                                   MessageFacadeParam.USER_CANCEL);
		subAgentProfileItemsToUpdate = subagentItemList.toArray(new SubagentProfileUpdateType[0]);
		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {
				Boolean isSuccess = MessageFacade.getInstance().updateSubagentProfile(parameters,
						  getTrueIdentity(),subAgentProfileItemsToUpdate);
							
                if (isSuccess) {
                    return Boolean.FALSE;
                }
                
                for(int i=0;i < subAgentProfileItemsToUpdate.length; i++) {
                	if (subAgentProfileItemsToUpdate[i].getProfileItemName().trim().equals("AGENT_STORE_NAME") && 
                		subAgentProfileItemsToUpdate[i].getAgentID().trim().equals(
                		UnitProfile.getInstance().get("AGENT_ID","")))  {
                			UnitProfile.getInstance().set("AGENT_STORE_NAME", 
                			subAgentProfileItemsToUpdate[i].getProfileItemValue().trim());
                	}
                }
                
                subagentItemList.clear();
                count = 0;
                return Boolean.TRUE;
			}
		};
		MessageFacade.run(logic, callingPage, MoneyGramClientTransaction.SAVE_SUBAGENTS, Boolean.TRUE);
	}

	/**
	 * return the agent sequence number for the requested agentID. 
	 */
	public int getSequenceNumber(String agentID) {
        int sequenceNumber = 0;
      for (Iterator<SubagentInfo> i = agentList.iterator(); i.hasNext(); ){
    	  SubagentInfo agent = i.next();
          if(agentID.equals(""+agent.getAgentID())){
              sequenceNumber = agent.getSequenceNumber() != null ? agent.getSequenceNumber() : 0;
              break;
          }
       }
        return sequenceNumber;
	}
	
	/**
	 * Update the subAgent list with the newly edited store Name for the sub agent from the table.
	 * on the screen.
	 * @param agentID
	 * @param storeName
	 */
	public void updateStoreNameForSubAgent(String agentID, String storeName) {
		SubagentInfo agent = new SubagentInfo();
		SubagentProfileUpdateType newItem = new SubagentProfileUpdateType();
	    int sequenceNumber = 0;
		for (Iterator<SubagentInfo> i = agentList.iterator(); i.hasNext(); ){
	          agent = i.next();
	          if(agentID.equals(agent.getAgentID()+"")){
	        	  agent.setAgentStoreName(storeName);
	              sequenceNumber = agent.getSequenceNumber();
	              break;
	          }
	    }
		if(!storeName.equals("")) {
			newItem.setAgentID(agentID);
			newItem.setSequenceNumber(sequenceNumber);
			newItem.setProfileItemName("AGENT_STORE_NAME");
			newItem.setProfileItemValue(agent.getAgentStoreName());
			subagentItemList.add(count, newItem);
			count++;
		}
	}
}
