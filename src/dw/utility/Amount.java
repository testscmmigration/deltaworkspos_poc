package dw.utility;

import java.io.Serializable;
import java.math.BigDecimal;

/**
  * Represents an amount (such as a dollar amount), and supports conversions 
  * between  different representations of amounts. The amount is stored 
  * internally as a BigDecimal. The intention is to allow easy manipulation
  * of string values representing numbers that may or may not have an implied
  * decimal point. When no decimal point is present, an implied decimal point
  * exists two places from the end of the string. Thus, the two strings 
  * "25.00" and "2500" represent the same value. The static methods
  * Amount.toString and Amount.toDecimalString convert an input string in 
  * either format to the desired format.
  *
  * @author Geoff Atkin
  */
public class Amount implements Serializable {
	private static final long serialVersionUID = 1L;
	
    private BigDecimal value;
    
    // Constructor for using the default currency and amount
    public Amount() {
        this.value = BigDecimal.ZERO;
    }
    
    // Adds two MoneyAmount's after verifying that the currencies match
    public void add(final Amount increment) {
            value = value.add(increment.value);
    }
    
    public void addBigDecimalValue(final BigDecimal v) {
        setBigDecimalValue(getBigDecimalValue().add(v));
    }
    
    public BigDecimal getValue() {
        return value;
    }
    
    public void setLongValue(final long v) {
        value = BigDecimal.valueOf(v);
    }
    
    public BigDecimal getBigDecimalValue() {
        return value;
    }
    
    public void setBigDecimalValue(final BigDecimal v) {
        value = v;
    }
}
