/*
 * ErrorManager.java
 * 
 * $Revision$
 *
 * Copyright (c) 2000-2004 MoneyGram, Inc. All rights reserved
 */

package dw.utility;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.StringTokenizer;

import javax.swing.SwingUtilities;

import dw.i18n.Messages;
import dw.io.DiagLog;
import dw.io.diag.MustTransmitDiagMsg;
import dw.io.report.ReportLog;
import dw.profile.UnitProfile;

/**
 * Handles error calls from different points in the applicaton and forwards
 * them to the registered handler.
 *
 * @author Rob Campbell
 * @author Chris Bartling
 */
public class ErrorManager {
    
    public static final int INFORMATION_STATUS = 0;
    public static final int WARNING_STATUS = 1;
    public static final int ERROR_STATUS = 2;

    // for debug log
    private static final String statusString[] = {"[i] ", "[!] ", "[#] "};   
    
    public static final int WARNING = 1;
    public static final int ERROR = 2;
    public static final int FATAL = 3;
    public static final int DEBUG = 4;
    
    // Error codes coming from MessageError
    private static ErrorListener listener = null;
    private static boolean active = false;

    private static int badLogins = 0;
    private static int badLoginLimit = 3;
    
    /** 
     * Adds an ErrorListener to this class for error handling notification.
     * @param el An ErrorListener implementation.
     */
    public static void addErrorListener(ErrorListener el) {
        listener = el;
    } // END_METHOD

    /**
     * Dispatching method for fatal errors.
     * @param message A String representing the error message.
     * @param e An Exception object.
     */
    public static void fatal(String message, Exception e) {
        if (!checkAndSetActiveFlag()){
            if (listener != null)
                listener.fatalErrorOccurred(message, e);
        }
    } // END_METHOD


    /**
        Resets the active flag.
     */
    public static void resetActiveFlag() {
        active = false;
    } // END_METHOD

    /**
     *  Check the active flag.
     *  returns true if active flag is already set.
     *  if active flag is false, set active to true and return false.
     */
    private static synchronized boolean checkAndSetActiveFlag() {
        boolean activeFlag = true;
        if (!active){
            active = true;
            activeFlag = false;
        }
        return activeFlag;
    } // END_METHOD
    
    /**
     * Dispatching method for critical errors.  This method uses the 
     * SwingUtilities.invokeLater(Runnable) method to queue up the event
     * notification to the ErrorListener on the event-dispatching thread.
     * @param message A String representing the error message.
     * @param e An Exception object.
     * @author Chris Bartling
     */
    public static void criticalError(final String message, final Exception e) {
        if (!checkAndSetActiveFlag()){
            if (listener != null) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
					public void run() {
                        // Provide change notification.
                        listener.criticalErrorOccurred(message, e);
                    }
                });
            }
        }
    } // END_METHOD

    /**
     * Dispatching method for warnings.  This method uses the 
     * SwingUtilities.invokeLater(Runnable) method to queue up the event 
     * notification to the ErrorListener on the event-dispatching thread.
     * @param message A String representing the error message.
     * @param e An Exception object.
     * @author Chris Bartling
     */
    public static void warning(final String message, final Exception e) {
        if (!checkAndSetActiveFlag()){
            if (listener != null) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
					public void run() {
                        // Provide change notification.
                        listener.warningOccurred(message, e);
                    }
                });
            }
        }
    } // END_METHOD

    /**
     * Dispatching method for debug events.  This method uses the 
     * SwingUtilities.invokeLater(Runnable) method to queue up the event 
     * notification to the ErrorListener on the event-dispatching thread.
     * @param message A String representing the error message.
     * @param e An Exception object.
     * @author Chris Bartling
     */
    public static void debug(final String message, final Exception e) {
        if (!checkAndSetActiveFlag()){
            if (listener != null) {
                // Instantiate and run the Waiter.
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
					public void run() {
                        // Provide change notification.
                        listener.debugEventOccurred(message, e);
                    }
                });
            }
        }
    } // END_METHOD

    /**
     * Sets the status for the listener.  This method uses a Thread as 
     * a waiter to handle the change notification on the ErrorListener, so 
     * the synchronization lock is not held on this Subject.
     * @param message A String representing the error message.
     * @param severity An integer value representing the severity of the error.
     */
    public static void setStatus(final String message, final int severity) {
        if (listener != null) {
            // Instantiate and run the Waiter.
            SwingUtilities.invokeLater(new Runnable() {
                @Override
				public void run() {
                    // Provide change notification.
                    listener.setStatus(message, severity);
                }
            });
        }
    }

    /**
     * Notify the listener that the software has changed.
     */
/*    
    public static synchronized void softwareChanged() {
        if (listener != null) {
            listener.softwareChanged();
        }
    }
*/
    /**
     * Notify the listener that new software has been successfully downloaded.
     * @param success whether or not the software download was successful
     */
    public static void softwareDownloadFinished(final boolean success, final String resultString) {
        SwingUtilities.invokeLater(new Runnable() { 
        @Override
		public void run() {
        if (listener != null) {
            listener.softwareDownloadFinished(success, resultString);
        }                                                              }});
    } // END_METHOD

    /**
     * Notify the listener that a new software download has started.
     * @param success whether or not the software download was successful
     */
    public static void softwareDownloadStarted() {
        SwingUtilities.invokeLater(new Runnable() {
        @Override
		public void run() {
            if (listener != null) {
                listener.softwareDownloadStarted();
            }
        }});
    } // END_METHOD
    
    /** 
     * Sets a dynamic message to indicate software download progress.
     * The message will appear on or near the progress bar.
     */
    public static void updateProgressBarString(String message){
        listener.setProgressBarString(message);
    }

    /** 
     * Sets the value of the software download progress bar, between min
     * value and max value. Not being used, would work only for 
     * determinate mode progress bar. 
     */
    public static void updateProgressBarProgress(int progress){
        listener.setProgressBarProgress(progress);
    }

    /** 
     * Sets the value corresponding to the upper limit of the software
     * download progress bar. Not being used, would work only for 
     * determinate mode progress bar. 
     */
    public static void updateProgressBarMaxValue(int maxValue){
        listener.setProgressBarMaxValue(maxValue);
    }

    /** 
     * Set the value corresponding to the lower limit of the software
     * download progress bar. Not being used, would work only for 
     * determinate mode progress bar. 
     */
    public static void updateProgressBarMinValue(int minValue){
        listener.setProgressBarMinValue(minValue);
    }


    /**
     * Notify the listener that a diag transmit is needed
     */
    public static synchronized boolean transmitAll(final boolean manual) {
        if (listener != null) {
            return listener.transmitAll(manual);
        }
        return false;
    } // END_METHOD

    /**
     * Notify the listener to de-auth the POS on the client side
     */
    public static void setPosAuthorization(final boolean authorized) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
				public void run() {
                        if (listener != null) {
                                listener.setPosAuthorization(authorized);
                        }
                    }
            });
    } // END_METHOD
    
    /**
     * Handle the case where a lot has issued an IOException (might be full,
     corrupt, etc.).
     */
    
    public static void mustHandleLogs() {
        SwingUtilities.invokeLater(new Runnable() {
                    @Override
					public void run() {
                    if (listener != null) {
                            listener.mustHandleLogs();
                        }
                    }
            });
    }

    /**
     * Notify the listener that the diag transmit is finished.
     */
    public static void diagTransmitFinished(final boolean autoRestart) {
        SwingUtilities.invokeLater(new Runnable() { @Override
		public void run() {
            if (listener != null) {
                listener.diagTransmitFinished(autoRestart);
            }
        }});
    } // END_METHOD

    /**
     * Notify the listener that a diag transmit has started.
     */
    public static void diagTransmitStarted() {
        SwingUtilities.invokeLater(new Runnable() { @Override
		public void run() {
            if (listener != null) {
                listener.diagTransmitStarted();
            }
        }});
    } // END_METHOD

   /** ENH - 1133
     * Notify the listener that the ftp transfer to TEMG is finished.
     */
    public static void ftpFileFinished() {
        SwingUtilities.invokeLater(new Runnable() { @Override
		public void run() {
            if (listener != null) {
                listener.ftpFileFinished();
            }
        }});
    } // END_METHOD

    /** ENH - 1133
     * Notify the listener that the ftp transfer to TEMG has started.
     */
    public static void ftpFileStarted() {
        SwingUtilities.invokeLater(new Runnable() { @Override
		public void run() {
            if (listener != null) {
                listener.ftpFileStarted();
            }
        }});
    } // END_METHOD

    /**
     * Notify the listener that the POS has gone AWOL or returned.
     */
    public static void setAWOLStatus(final boolean awol) {
        SwingUtilities.invokeLater(new Runnable() { @Override
		public void run() {
            listener.setAWOLStatus(awol);
        }});
    } // END_METHOD

    /**
     * Notify the listener of a new ReprintReceipt state.
     */
    public static void updateReprintReceiptState() {
        SwingUtilities.invokeLater(new Runnable() { @Override
		public void run() {
            listener.updateReprintReceiptState();
        }});
    } // END_METHOD

    /**
     * Notify the listener that training mode has been entered or exited.
     */
    public static void setTrainingMode(final boolean training) {
        SwingUtilities.invokeLater(new Runnable() { @Override
		public void run() {
            listener.setTrainingMode(training);
        }});
    }

    /**
     * If the login was successful, reset the bad login counter, otherwise
     * increment it. If the number of consecutive logins hits the limit, then
     * write a 'must transmit' diagnostic. Also send the attempt to the
     * ReportLog.
     */
    public static void recordLoginAttempt(final boolean successful, final int userID) {
        if (successful)
            badLogins = 0;
        else {
            badLogins++;
        }
        if (badLogins == badLoginLimit) {
            // send the bad attempt to the report log w/user & pwd
            Debug.println("There were : " + badLogins + " bad logins.");	 
            ReportLog.logInvalidLogin(userID);
            DiagLog.getInstance().writeMustTransmit(MustTransmitDiagMsg.SECURITY_BREACH);
            badLogins = 0;
        } // END_IF
    } // END_METHOD

    public static void recordLoginAttempt(boolean successful, String userName) {
        final int userID = UnitProfile.getInstance().getUserID(userName);
        recordLoginAttempt(successful,userID);
    }

    /**
     * Handles exceptions.  This may entail notifying the user, writing a
     * diagnostic log message, or writing a stack trace to the console.
     * @param e An Exception object.
     * @param msgId A String representing the name of the parameter for the
     * user-friendly error message coming from ErrorMessages.xml.
     * @param level An integer representing the error level for this
     * exception.  This error level will differentiate the handling of the
     * Exception object by this object.
     * @author Chris Bartling
     */
    public static void handle(Exception e, String msgId, String errorMessage, int level) {
        handle(e, msgId, errorMessage, level, false);
    }

    /**
     * Handles exceptions.  This may entail notifying the user, writing a
     * diagnostic log message, or writing a stack trace to the console.
     * @param e An Exception object.
     * @param msgId A String representing the name of the parameter for the
     * user-friendly error message coming from ErrorMessages.xml.
     * @param level An integer representing the error level for this
     * exception.  This error level will differentiate the handling of the
     * Exception object by this object.
     * @param addAssistanceMessage A boolean flag stating whether to add the
     * TECI assistance message to the error dialog message.
     * @author Chris Bartling
     */
    public static void handle(Exception e, String msgId, String errorMessage, int level, boolean addAssistanceMessage) {

        Debug.println("handle(" + e.getClass().getName() + "," + msgId + "," + level + "," + addAssistanceMessage + ")");	 
        Debug.println("  " + e.getMessage());	

        Exception forwardedExc = e;
        StringBuffer msg = new StringBuffer();
        msg.append(errorMessage);

        // write out a general diag to help diagnose problems in production
        if (level != DEBUG && level != WARNING)
            writeGeneralDiagnostic(msgId + " " + e.toString());	

        if (addAssistanceMessage) {
            msg.append("\n");	
            msg.append(Messages.getString("ErrorMessages.assistanceMessage"));	
        }
        // Now determine the notification level
        switch (level) {
            case WARNING:
                warning(msg.toString(), forwardedExc);
                break;
            case ERROR:
                criticalError(msg.toString(), forwardedExc);
                break;
            case FATAL:
                fatal(msg.toString(), forwardedExc);
                break;
            case DEBUG:
                debug(msg.toString(), forwardedExc);
                break;
            default:
                break;
        }
    }

    /**
     * Print a stack trace to the diag log, using as many writeGeneralDiag
     *   calls as we need, splitting it into 255 char sections. Put a (n) on the
     *   beginning of each section so we can piece it together in the diag log
     *   if its not in order
     * Note : Code '[STACKTRACE]' can be searched for in Plutto, as it will be
     *    prepended to all stack trace diags.
     * @param message a String used to place additional info about the problem
     *   along with the stack trace
     * @param e the Exception to print the stack trace for.
     */
    public static void writeStackTrace(String message, Throwable e) {
        // capture the whole stack trace to a string
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        e.printStackTrace(new PrintStream(baos));
        String stripped = "";
        StringTokenizer tok = new StringTokenizer("[STACKTRACE] " + message + " : " + baos.toString());	 
        while (tok.hasMoreTokens()) {
            stripped += (tok.nextToken() + " ");	
        }
        // split it up into n components 0 255 chars, and write a general diag for each
        int start = 0;
        int i = 0;
        while (start < stripped.length()) {
            int end = start + 250;
            if (end >= stripped.length())
                end = stripped.length() - 1;
            writeGeneralDiagnostic("(" + i + ")" +	 
                        stripped.substring(start, end));
            start += 250;
            i++;
        }
    }

    /**
     * Write out a diagnostic containing some plain text.
     * Now allows up to 255 characters.
     */
    public static void writeGeneralDiagnostic(String message) {
    	// changed from DiagLog to Debug log in DW4.0
    	Debug.println(message);
    }

    /**
     * Handle unexpected exceptions thrown in the application.
     * @param e An Exception.  This is an exception of some sort, but it's not one
     * the application was expecting.
     * @param useAssistanceMessage A boolean stating whether to use the
     * TECI assistance message in the error dialog message.
     */
    static void handleUnexpectedException (Exception e, boolean useAssistanceMessage) {

        StringBuffer buf = new StringBuffer();
        buf.append(Messages.getString("ErrorMessages.unexpectedExceptionMsg"));	
        buf.append("\n");	
        buf.append("Exception type: " + e.getClass().toString() + "\n");	 
        String msg = e.getMessage();
        if (msg != null) {
            buf.append("Message: " + msg + "\n");	 
        }
        // Do we need to add the assistance message?
        if (useAssistanceMessage)
            buf.append(Messages.getString("ErrorMessages.assistanceMessage"));	
        criticalError(buf.toString(), e);
    }

    public static String getStatusString(int severity) {
        if (severity >=0 && severity < statusString.length) {
            return statusString[severity];
        }
        else {
            return "[?] ";	
        }
    }
}
