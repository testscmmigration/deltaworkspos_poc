/*
 * $Workfile:   Trace.java  $
 *
 * Copied from AgentConnect.
 */

package dw.utility;

import java.util.Date;

/** 
 * Records millisecond durations between specfied events.
 */
public class Trace {
    private static boolean enabled = false;
    private static long previous = 0;
    private static String message = "";	

    public static void trace(String s) {
        if (! enabled) return;

        //Have to use system time because the discontinuity caused by resyncing
        //with the middleware will mess up measured durations.
        long now = System.currentTimeMillis();
        long duration = now - previous;
        if (previous == 0) {
            Debug.println("Tracing started: " + new Date(now) + " (system time)");	 
        }
        else {
            Debug.println(message + ":\t" + duration + " ms");  
        }
       
        message = s;
        previous = System.currentTimeMillis();
    }

    public static void pause() {
        if (! enabled) return;

        if (previous != 0)
            trace("");	
        
        previous = 0;
    }
    
    public static void enable() {
        enabled = true;
    }
}
