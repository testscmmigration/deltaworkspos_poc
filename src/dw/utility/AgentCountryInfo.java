package dw.utility;

import dw.dialogs.Dialogs;
import dw.i18n.Messages;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.CountryInfo.Country;

public class AgentCountryInfo {
	private static final String[] stateReqCountries = 
	{
			"USA",
			"JAM"
	};
	private static boolean stateReq = false;
	
	
	private static boolean usAgent = false;
	
	private static String agentCountry;
	

	private static boolean checkTable( String[] countryList )
	{
		
		for (int ii = 0; ii < countryList.length; ii++ ) {
			if (agentCountry.compareToIgnoreCase(countryList[ii]) == 0) {
				return true;
			}
		}
		
		return false;
	}

	public static boolean isStateRequired() {
		return stateReq;
	}
	
	public static boolean isUsAgent() {
		return usAgent;
	}
	
	public static void setAgentCountryFields() {
		/*
		 * Get the country info for the agents country. 
		 */
		Country country = CountryInfo.getAgentCountry();
		/*
		 * If we received country information, get the country code for the 
		 * agent's country.
		 */
		if (country != null) {
			agentCountry = country.getCountryCode();
		} else {
			agentCountry = "";
			Dialogs.showWarning("dialogs/BlankDialog.xml",
					Messages.getString("AgentCountryInfo.DialogLine1"),
					Messages.getString("AgentCountryInfo.DialogLine2"));
		}
		
		stateReq = checkTable(stateReqCountries);
		usAgent = DWValues.isUsOrUsTerr(agentCountry);
//		Debug.println("stateReq["+stateReq+"]");
//		Debug.println("usAgent["+usAgent+"]");
	}
	
    public static String getCurrentLocale() {
    	/*
    	 * For now the business does NOT want the actual locale, they have instructed AC to return
    	 * English for any locales that they do not support.  Therefore for now we will send the default
    	 * locale up for all languages
    	 */
        String shortLanguageCode = Messages.getCurrentLanguage();
        	if (shortLanguageCode.equalsIgnoreCase("en")) {
        		return DWValues.AC_LANG_ENGLISH;
        	} else if (shortLanguageCode.equalsIgnoreCase("es")) {
        		return DWValues.AC_LANG_SPANISH;
        	} else if (shortLanguageCode.equalsIgnoreCase("fr")) {
        		return DWValues.AC_LANG_FRENCH;
        	} else if (shortLanguageCode.equalsIgnoreCase("pl")) {
        		return DWValues.AC_LANG_POLISH;
        	} else if (shortLanguageCode.equalsIgnoreCase("de")) {
        		return DWValues.AC_LANG_GERMAN;
        	} else if (shortLanguageCode.equalsIgnoreCase("cn")) {
        		return DWValues.AC_LANG_CHINESE;
        	} else if (shortLanguageCode.equalsIgnoreCase("ru")) {
        		return DWValues.AC_LANG_RUSSIAN;
        	} else if (shortLanguageCode.equalsIgnoreCase("pt")) {
        		return DWValues.AC_LANG_PORTUGUESE;
        	} else {
        		return DWValues.AC_LANG_ENGLISH;
        	}
    }
}
