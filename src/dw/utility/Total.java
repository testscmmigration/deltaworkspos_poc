package dw.utility;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.math.BigDecimal;

import dw.io.State;
import dw.io.StateListener;

/**
 * Represents a total which persists across invocations. A total consists of a
 * count and an amount (represented internally as an int and a BigDecimal).
 * There is no notion of currency in these values, to avoid having to save
 * currency infomation in the persistent state. Totals are not immutable; the
 * increment and clear methods change the internal values in-place, then save
 * them using a state listener.
 * 
 * There is some unfortunate overlap in functionality with the Amount and
 * CountAndAmount classes. Some refactoring is probably called for. (But
 * don't mess with this class without consulting me; I have a plan!)
 * @author Geoff Atkin
 */
public class Total {
    private BigDecimal amount;
    private int count;
    private StateListener listener;
    private ByteArrayOutputStream baos;
    private DataOutputStream dos;
    
    /**
      * Constructs a new total using saved state.
      * @throws IllegalArgumentException if len is not 16.
      */
    public Total(byte[] initialState, int off, int len) {

        try {
            load(initialState, off, len);
            baos = new ByteArrayOutputStream(16);
            dos = new DataOutputStream(baos);
            listener = null;
        }
        catch (IOException e) {
            throw new IllegalArgumentException(e.toString());
        }
    }

    /**
      * Constructs a new total using saved state.
      */
    public Total(State state, long fp) {
        try {
            byte[] initialState = new byte[16];
            state.read(initialState, fp);
            load(initialState, 0, 16);
            baos = new ByteArrayOutputStream(16);
            dos = new DataOutputStream(baos);
            listener = new State.StateAdapter(fp);
        }
        catch (IOException e) {
            throw new IllegalArgumentException(e.toString());
        }
    }

    /** Called by constructors. */
    private void load(byte[] initialState, int off, int len) 
                throws IOException {
        if (len != 16)
            throw new IllegalArgumentException("len must be 16"); 

        ByteArrayInputStream bais = 
            new ByteArrayInputStream(initialState, off, len);
        DataInputStream dis = new DataInputStream(bais);
        
        long unscaled = dis.readLong();
        int scale = dis.readInt();
        count = dis.readInt();
        amount = BigDecimal.valueOf(unscaled, scale);
    }
    
    /** 
      * Registers the specified state listener. This implementation only 
      * supports one state listener at a time, so this replaces any state
      * listener previously registered.
      */
    public void addStateListener(StateListener sl) {
        listener = sl;
    }

    /** Save the count and amount using the state listener. */
    public void saveState() {
        try {
            if (listener != null) {
                baos.reset();
                dos.writeLong(amount.unscaledValue().longValue());
                dos.writeInt(amount.scale());
                dos.writeInt(count);
                listener.stateChanged(baos.toByteArray(), 0, baos.size());
            }
        }
        catch (IOException e) {
            ErrorManager.fatal("Could not save totals.", e); 
        }
    }

    /** Returns the count as an int. */
    public int getCountIntegerValue() {
        return count;
    }

    /** Returns the count as an int. */
    public int getCount() {
        return count;
    }

    /** Returns the amount as a BigDecimal. */
    public BigDecimal getAmountBigDecimalValue() {
        return amount;
    }

    /** Returns the amount as a BigDecimal. */
    public BigDecimal asBigDecimal() {
        return amount;
    }
    
//    /** Returns the amount as an Amount. */
//    public Amount getAmount() {
//        return new Amount(amount);
//    }
//
//    /** Returns the count and amount as a CountAndAmount. */
//    public CountAndAmount getCountAndAmount() {
//        return new CountAndAmount(count, this.getAmount());
//    }
    
    /** 
      * Increments the count by the given integer value. 
      * The new value is saved to the persistent state.
      */
    public Total incrementCount(int i) {
        count += i;
        saveState();
        return this;
    }

    /** 
      * Increments the amount by the given BigDecimal value. 
      * The new value is saved to the persistent state.
      */
    public Total incrementAmount(BigDecimal bd) {
        amount = amount.add(bd);
        saveState();
        return this;
    }
    
    /** 
      * Increments the amount by the BigDecimal value with the
      * given unscaled value and scale. 
      * The new value is saved to the persistent state.
      */
    public Total incrementAmount(long a, int scale) {
        return incrementAmount(BigDecimal.valueOf(a, scale));
    }

    /** 
      * Increments the amount by the BigDecimal value with the
      * the given unscaled value and a scale of 2. Note this means
      * the parameter must have an implied decimal point two places
      * from the right.
      * The new value is saved to the persistent state.
      */
    public Total incrementAmount(long a) {
        return incrementAmount(a, 2); 
    }
    
    /**
      * Increments the amount by the given Amount. Note that 
      * currency is ignored.
      */
    public Total incrementAmount(Amount a) {
        return incrementAmount(a.getValue());
    }

    /** Clears the count. */
    public void clearCount() {
        count = 0;
        saveState();
    }

    /** Clears the amount. */
    public void clearAmount() {
        amount = BigDecimal.ZERO;
        saveState();
    }

    /** Clears the count and the amount. */
    public void clear() {
        count = 0;
        amount = BigDecimal.ZERO;
//        saveState();
    }

    @Override
	public String toString() {
        return count + " " + amount;	
    }
}
