package dw.utility;

import java.io.IOException;

import dw.dialogs.Dialogs;
import dw.dialogs.ExecutingDialog;

public class ExecuteCommand {

	/**
	 * Thread class to wait for the Execute Process to exit
	 */
	private static class ExecuteCheckerThread extends Thread {
		private final Process pExecuteProcess;
		private Integer iExitValue = null;

		ExecuteCheckerThread(final Process pm_pExecuteProcess) {
			this.setName("ExecuteChecker");
			this.pExecuteProcess = pm_pExecuteProcess;
		}

		/**
		 * Wait for Execute process to complete or for an interrupt
		 * 
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Thread#run()
		 */
		@Override
		public void run() {
			try {
				iExitValue = Integer.valueOf(pExecuteProcess.waitFor());
				vRemoveDialog();
			} catch (final InterruptedException ignore) {
				iExitValue = Integer.valueOf(CANCEL_CODE);
			}
			return;
		}

		/**
		 * Returns the exitCode of the specified ExecuteProcess
		 * 
		 * @return null if Executing did not complete, otherwise the exitCode of
		 *         the ExecuteProcess
		 */
		private Integer iGetExitValue() {
			return iExitValue;
		}
	}

	private static ExecuteCheckerThread tExecuteProcessCheckerThread;
	private static StreamGobbler sgOutputGobbler;
	private static StreamGobbler agErrorGobbler;

	private static Process pCmdProcess;

	private static int CANCEL_CODE = -999999;

	/**
	 * Executes a command.
	 * 
	 * @param pm_sCommand
	 *            - String with the command to send to the OS
	 * @param pm_bLogStdOutput
	 *            - boolean controlling logging of stdOutput
	 * @param pm_bLogErrorOutput
	 *            - boolean controlling logging of stdError
	 * @param pm_lTimeOut
	 *            - long containing the timeout value (milliseconds)
	 * @param pm_sDiaglogText
	 *            - String containing text for the executing dialog
	 * 
	 * @return Integer result code (exitCode+ )
	 * 
	 * @throws java.io.IOException
	 * @throws java.lang.InterruptedException
	 */
	public static Integer executePdfPrint(String pm_sCommand, String printerName, String fileName, final String pm_sDiaglogText) throws Exception {
		// validate the system and command line and get a system-appropriate
		// command line
		
		try {
			String cmd = pm_sCommand + " -print-to \"" + DWValues.getValidPrinterName(printerName) + "\" " + fileName + " -exit-on-print -silent";
			String massagedCommand = sValidateSystemAndPackageCommand(cmd);

			// create the process which will run the command
			final Runtime runtime = Runtime.getRuntime();

			pCmdProcess = runtime.exec(massagedCommand);

			// consume and display the error and output streams
			sgOutputGobbler = new StreamGobbler(pCmdProcess.getInputStream(),
					"OUTPUT", true);
			agErrorGobbler = new StreamGobbler(pCmdProcess.getErrorStream(),
					"ERROR", true);
			sgOutputGobbler.start();
			agErrorGobbler.start();

			// create and start a Worker thread which this thread will join for
			// the timeout period
			tExecuteProcessCheckerThread = new ExecuteCheckerThread(pCmdProcess);
			tExecuteProcessCheckerThread.start();
			try {
				/*
				 * Setup dialog to allow cancel of printing.
				 */
				ExecutingDialog.setMessageLabels(pm_sDiaglogText);
				vShowDialog();
				tExecuteProcessCheckerThread.join(0);
				final Integer exitValue = tExecuteProcessCheckerThread
						.iGetExitValue();
				if (exitValue != null) {
					// the worker thread completed within the timeout period
					if (exitValue.intValue() == CANCEL_CODE) {
						final String errorMessage = "The command ["
								+ pm_sCommand + "] was canceled.";
						Debug.println(errorMessage);
						throw new RuntimeException(errorMessage);
					} else {
						return exitValue;
					}
				}

				// if we get this far then we never got an exit value from the
				// worker thread as a result of a timeout
				final String errorMessage = "The command [" + pm_sCommand
						+ "] timed out.";
				Debug.println(errorMessage);
				throw new RuntimeException(errorMessage);
			} catch (final InterruptedException ex) {
				tExecuteProcessCheckerThread.interrupt();
				Thread.currentThread().interrupt();
				throw ex;
			}
		} catch (final InterruptedException ex) {
			final String errorMessage = "The command [" + pm_sCommand
					+ "] did not complete due to an unexpected interruption.";
			Debug.println(errorMessage + ex.getMessage());
			throw new RuntimeException(errorMessage, ex);
		} catch (final IOException ex) {
			final String errorMessage = "The command [" + pm_sCommand
					+ "] did not complete due to an IO error.";
			Debug.println(errorMessage + ex.getMessage());
			throw new RuntimeException(errorMessage, ex);
		} catch (Exception ex) {
			throw ex;
		}
	}

	/**
	 * Interrupt threads waiting on the execution.  Intended use is to cancel
	 * waiting for the execute process to complete.
	 */
	public static void interrupt() {
		tExecuteProcessCheckerThread.interrupt();
		pCmdProcess.destroy();
		sgOutputGobbler.interrupt();
		agErrorGobbler.interrupt();
	}

	/**
	 * Validates that the command exists and returns a system-appropriate
	 * command line.
	 * 
	 * @param pm_sOriginalCommand
	 *            - String containing the original command line request
	 * @return String containing a validated and packaged command appropriate
	 *         for the OS
	 */
	private static String sValidateSystemAndPackageCommand(
			final String pm_sOriginalCommand) {
		// make sure that we have a command
		if ((pm_sOriginalCommand == null) || (pm_sOriginalCommand.length() < 1)) {
			final String sErrMsg = "Missing or empty command line parameter.";
			Debug.println(sErrMsg);
			throw new RuntimeException(sErrMsg);
		}

		// Package the command line appropriately for the OS
		String sPackagedCmd;
		sPackagedCmd = "cmd.exe /C " + pm_sOriginalCommand;
		return sPackagedCmd;
	}

	/**
	 * Removes the Executing dialog
	 */
	private static void vRemoveDialog() {
		final ExecutingDialog eDialog = ExecutingDialog.getInstance();
		if (eDialog.isShowing()) {
			eDialog.closeDialog(Dialogs.CLOSED_OPTION);
			return;
		}
	}

	/**
	 * Shows the Executing dialog with the specified text.
	 */
	private static void vShowDialog() {
		if (ExecutingDialog.getInstance().isShowing()) {
			return;
		}
		final Runnable r = new Runnable() {
			@Override
			public void run() {
				Dialogs.showPanel(ExecutingDialog.getInstance());
			}
		};
		r.run();
	}
}
