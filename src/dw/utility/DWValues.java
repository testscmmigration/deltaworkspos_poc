package dw.utility;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Set;

import javax.print.DocFlavor;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;

import com.moneygram.agentconnect.ProductVariantType;
import com.moneygram.agentconnect.TransactionStatusType;

import dw.i18n.Messages;
import dw.profile.UnitProfile;

/**
 * DWValues is a utility class that contains all the valid values needed on the
 *   POS. Countries, ID types, states, status codes, etc. The countries will
 *   come from a flat file, and the other values will be hard-coded for now.
 * Country Required Field Indicators:
 * U - Standard required fields
 * u - Receiver's 2nd last name required
 * Q - Test question restricted
 * q - Receiver's 2nd last name required and test question restricted
 * d - Depends on Mexico option:
 *     Will Call - same as 'u'
 *     Home Delivery - same as 'u', &amp; receiver's address required
 * m - Depends on Mexico option:
 *     Cambio Plus - same a 'u'
 *     Next Day Pesos - same as 'u', &amp; rec. home phone, Banamex acct#/branch
 *                                                              optional
 *     Next Day Pesos w/ Notification - same as 'd' &amp; Next Day Pesos
 *     Remote Location Pesos - same as Next Day Pesos w/ Notification
 */
public class DWValues {

    // These fields are now obtained from QDO of CodeTables from Middleware
    private static CustomMap legalIdTypes = new CustomMap();
    private static CustomMap photoIdTypes = new CustomMap();
    private static CustomMap directionList = new CustomMap();
    private static CustomMap agentPrinterList = new CustomMap();
    private static CustomMap agentReceiptTypeList = new CustomMap();
    private static CustomMap agentReceiptPaperList = new CustomMap();
    private static CustomMap productVariantList = new CustomMap();
    private static CustomMap receiptLanguageList = new CustomMap();
    private static CustomMap languageRegionList = new CustomMap();
	private static Map<String, Bcp47Info> receiptLanguageInfo = new HashMap<String, Bcp47Info>();

    private static Map<String, String> localeMap;
    
    public static final String CR_LF = "\r\n";
    
    public static final String DW_AC_URL = "https://ws2.moneygram.com/wsprod/services/";
    
	public static final String THIRD_PARTY_TYPE_NONE = "NONE";
	
	public static final String DO_OVERNIGHT = "OVERNIGHT2ANY";
	public static final String OK_DELAY_24  = "24_HOUR";
	public static final String OK_DELAY_48  = "48_HOUR";

	public static final int BIN_SIZE = 6;

    public static final String PHOTO_ID_DEFAULT = "DRV";	
    public static final String THIRD_PARTY_TYPE_DEFAULT = THIRD_PARTY_TYPE_NONE;	
    
    public static final int LOOKUP_PHONE = 0;
    public static final int LOOKUP_CUST_ID = 1;
    public static final int LOOKUP_ACCT = 2;
    public static final int LOOKUP_AGENT_CUST_ID = 4;
    public static final int LOOKUP_MAX = LOOKUP_AGENT_CUST_ID;
    
    private static final String[] sCodeTableCountries = {"CAN", "USA", "MEX", "PRI"};
    
    public static String[] getCodeTableStateListCountries() {
        return sCodeTableCountries;
    }
    
    public static final String PPC_CLASS_OF_TRADE = "51";

    public static final String EMP_RECV_REVERSAL = "RECEIVE_REVERSAL";
    public static final String EMP_AMEND = "AMEND";
    
    public static final String PROMO_NOT_FOUND_TEST ="Promotion Code not found.  Please review code and reenter if necessary";
    
    public static final String    PROMO_CAT_LOYALTY_DISC = "1";
    public static final String    PROMO_CAT_WM_EMP_DISC  = "2";
    
    public static final int    PROMO_MAX_CODES = 5;
    
    public static final String MF_RECV_AMT_ORIG = "receiveAmount";
    
    public static final String MF_RECV_FEE_MGI  = "mgiReceiveFee";
    public static final String MF_RECV_FEE_NON_MGI  = "nonMgiReceiveFee";
    public static final String MF_RECV_TAX_MGI  = "mgiReceiveTax";
    public static final String MF_RECV_TAX_NON_MGI  = "nonMgiReceiveTax";
    
    public static final String MF_SEND_AMT_TOTAL_FEE_TAX  = "totalMgiCollectedFeesAndTaxes";
    public static final String MF_SEND_AMT_TOTAL_COLLECT_MGI  = "totalAmountToMgi";
    
    public static final String MF_SEND_DETAIL_FINAL_MGI_SEND_FEE = "discountedMgiSendFee";
    public static final String MF_SEND_DETAIL_ORIG_MGI_SEND_FEE = "mgiNonDiscountedSendFee";
    public static final String MF_SEND_DETAIL_OTHER_SEND_FEE = "nonMgiSendFee";
    public static final String MF_SEND_DETAIL_OTHER_SEND_TAX = "nonMgiSendTax";
    public static final String MF_SEND_DETAIL_TOTAL_ORIG_SEND_FEE = "totalNonDiscountedFees";
    public static final String MF_SEND_DETAIL_TOTAL_SEND_FEE_TAX = "totalSendFeesAndTaxes";
    public static final String MF_SEND_DETAIL_TOTAL_SEND_OTHER_FEE_TAX = "totalNonMGISendFeesAndTaxes";
    
    public static final String MF_SEND_TAX_MGI = "mgiSendTax";
    
   	public static final String DATE_TIME_SENT = "dateTimeSent";
   	
    public static final String TRX_LOOKUP_RECV  = "RECEIVE";
    public static final String TRX_LOOKUP_RECV_COMP = "RECEIVEANDCOMPLETION";
    public static final String TRX_LOOKUP_STATUS  = "STATUS";
    public static final String TRX_LOOKUP_SENDREV = "SENDREV";
    public static final String TRX_LOOKUP_RECEIVE_REV = "RCVREV"; 
    public static final String TRX_LOOKUP_AMEND = "AMEND";
    public static final String TRX_LOOKUP_SEND_COMP = "SENDCOMPLETION";
    public static final String TRX_LOOKUP_BP_COMP = "BPCOMPLETION";
    
    /*
     * Receipt Template Types
     */
    public static final String RT_TYPE_BP_AGENT = "BillPayAgent";
    public static final String RT_TYPE_BP_CUST = "BillPayCust";
    public static final String RT_TYPE_BP_VALIDATION = "BillPayPrepay";
    public static final String RT_TYPE_PPS_AGENT = "PPSAgent";
    public static final String RT_TYPE_PPS_CUST = "PPSCust";
    public static final String RT_TYPE_RECV_AGENT_LANG_1 = "ReceiveAgent";
    public static final String RT_TYPE_RECV_CUSTOMER_LANG_1 = "ReceiveCustomer";
    public static final String RT_TYPE_RECV_AGENT_LANG_1_INTL = "ReceiveAgentIntl";
    public static final String RT_TYPE_RECV_CUSTOMER_LANG_1_INTL = "ReceiveCustomerIntl";
    public static final String RT_TYPE_RECV_AGENT_LANG_2 = "ReceiveAgentHybrid";
    public static final String RT_TYPE_RECV_CUSTOMER_LANG_2 = "ReceiveCustomerHybrid";
    public static final String RT_TYPE_SEND_VALIDATION = "SendPrepayment";
    public static final String RT_TYPE_SEND_VALIDATION_LANG_2 = "SendPrepaymentDFHybrid";
    public static final String RT_TYPE_SEND_AGENT = "SendAgent";
    public static final String RT_TYPE_SEND_AGENT_LANG_2 = "SendAgentDFHybrid";
    public static final String RT_TYPE_SEND_CUSTOMER = "SendCustomer";
    public static final String RT_TYPE_SEND_CUSTOMER_LANG_2 = "SendCustomerDFHybrid";
    public static final String RT_TYPE_UBP_AGENT = "UBPAgent";
    public static final String RT_TYPE_UBP_CUST = "UBPCust";
    public static final String RT_TYPE_XPAY_AGENT = "XPAYAgent";
    public static final String RT_TYPE_XPAY_CUST = "XPAYCust";
    public static final String RT_TYPE_XPAY_VALIDATION = "XPAYPrepay";
    // added types for international sends
    public static final String RT_TYPE_INTL_SEND_US_PREPAY_LANG_2 = "SendPrepaymentHybridUS";
    public static final String RT_TYPE_INTL_SEND_US_AGENT_LANG_2 = "SendAgentHybridUS";
    public static final String RT_TYPE_INTL_SEND_US_CUSTOMER_LANG_2 = "SendCustomerHybridUS";
    public static final String RT_TYPE_INTL_SEND_CA_PREPAY_LANG_2 = "SendPrepaymentHybridCA";
    public static final String RT_TYPE_INTL_SEND_CA_AGENT_LANG_2 = "SendAgentHybridCA";
    public static final String RT_TYPE_INTL_SEND_CA_CUSTOMER_LANG_2 = "SendCustomerHybridCA";
    public static final String RT_TYPE_INTL_SEND_PREPAY_NODF_LANG_1 = "SendPrepaymentNoDF";
    public static final String RT_TYPE_INTL_SEND_AGENT_NODF_LANG_1 = "SendAgentNoDF";
    public static final String RT_TYPE_INTL_SEND_CUSTOMER_NODF_LANG_1 = "SendCustomerNoDF";
    public static final String RT_TYPE_INTL_SEND_PREPAY_INTL_LANG_1 = "SendPrepaymentNoDFIntl";
    public static final String RT_TYPE_INTL_SEND_AGENT_INTL_LANG_1 = "SendAgentNoDFIntl";
    public static final String RT_TYPE_INTL_SEND_CUSTOMER_INTL_LANG_1 = "SendCustomerNoDFIntl";
    // added miscellaneous types    
    public static final String RT_TYPE_WARNING_LANG_1 = "WarningReceipt"; 
    public static final String RT_TYPE_WARNING_NS_LANG_1 = "WarningNSReceipt";
    public static final String RT_TYPE_WARNING_INTL_LANG_1 = "WarningIntlReceipt"; 
    public static final String RT_TYPE_WARNING_NS_INTL_LANG_1 = "WarningNSIntlReceipt";
    public static final String RT_TYPE_WARNING_LANG_2 = "WarningReceiptHybrid"; 
    public static final String RT_TYPE_WARNING_NS_LANG_2 = "WarningNSReceiptHybrid";
    public static final String RT_TYPE_REFUND_AGENT_LANG_1 = "RefundAgentReceipt"; 
    public static final String RT_TYPE_REFUND_CUSTOMER_LANG_1 = "RefundCustomerReceipt";
    public static final String RT_TYPE_REFUND_AGENT_LANG_2 = "RefundAgentReceiptHybrid"; 
    public static final String RT_TYPE_REFUND_CUSTOMER_LANG_2 = "RefundCustomerReceiptHybrid";
    
    public static final String PROFILE_ISI_DF_AMTS = "ISI_OTHER_AMOUNTS"; 
    public static final String PROFILE_ISI_DF_TEXT = "ISI_ADDITIONAL_TEXT"; 
   
    public static final String PROFILE_REWARDS_ACTIVATION = "ACTIVATE_REWARDS_CARD_OPTION";
    public static final String PROFILE_PROMO_MAX = "PROMOTIONAL_CODE_LIMIT";
    
    public static final String PROFILE_RCPT_LANG = "ALLOW_LANG_OPT"; 
    public static final String PROFILE_RCPT_LANG_NONE = "NONE"; 
    public static final String PROFILE_RCPT_LANG_ALLOW_ONE = "ALLOW_NO_SECONDARY_LANG";
    public static final String PROFILE_RCPT_LANG_NUMBER = "NUM_OF_LANG_ON_RECEIPT";
    public static final String PROFILE_RCPT_LANG_PRIMARY = "PRIMARY_RECEIPT_LANGUAGE";    
    
    public static final String PROFILE_COMBO_BOX_RCPT_TYPE = "receiptTypeList"; 
    public static final String PROFILE_COMBO_BOX_RCPT_PAPER = "receiptPaperList"; 
    
    public static final String PROFILE_PRINTER_RCPT_TYPE = "AGENT_PRINTER_RECEIPT_TYPE"; 
    public static final String PROFILE_PRINTER_PAPER_SIZE = "AGENT_PRINTER_PAPER_SIZE"; 
    
    public static final String PROFILE_POE_TYPE = "POE_TYPE";
    public static final String PROFILE_DEFAULT_CHANNEL_TYPE = "LOCATION";
    public static final String PROFILE_DEFAULT_POE_TYPE = "DELTAWORKS";
    
    public static final String PROFILE_CHANNEL_TYPE = "CHANNEL_TYPE";
    public static final String PROFILE_TARGET_AUDIENCE = "TARGET_AUDIENCE";
    public static final String PROFILE_DEFAULT_TARGET_AUDIENCE = "AGENT_FACING";
    
    public static final String AGENT_PRINTER_PAPER_SIZE_1 = "LETTER";
    public static final String AGENT_PRINTER_PAPER_SIZE_DEFAULT = AGENT_PRINTER_PAPER_SIZE_1;
    
    public static final String AGENT_PRINTER_TYPE_1 = "TEXT";
    public static final String AGENT_PRINTER_TYPE_2 = "PDF";
    public static final String AGENT_PRINTER_TYPE_DEFAULT = AGENT_PRINTER_TYPE_1;
    
    public static final String GFFP_ORIGINATING_COUNTRY = "originatingCountry";    
    
    public static final String E_MAX_SEND_AMOUNT = "E_MAX_SEND_AMOUNT";
    public static final String RECV_OTHER_PAYOUT_TYPE = "OTHER_PAYOUT_TYPE";
    public static final String RECV_TO_CARD = "CARD";
    public static final String DSR_INLINE = "INTRA_TRANSACTION_REGISTRATION";
    
    public static final String MFA_REGISTARTION_STATUS = "registrationSuccessful";
    
    public static final String ORGINATING_COUNTRY = "originatingCountry";
    public static final String DESTINATION_COUNTRY = "destinationCountry";
    
    // doc seq #s for transactions...fixed for phase I
    public static final int MONEY_ORDER_DOC_SEQ = 1;
    public static final int VENDOR_PAYMENT_DOC_SEQ = 2;
    public static final int MONEY_GRAM_SEND_DOC_SEQ = 3;
    public static final int MONEY_GRAM_RECEIVE_DOC_SEQ = 4;
    public static final int BILL_PAYMENT_DOC_SEQ = 5;
    public static final int MONEY_GRAM_CARD_DOC_SEQ = 99;
    public static final int MONEY_GRAM_CARD_PURCH_DOC_SEQ = 96;
    public static final int MONEY_GRAM_CARD_RELOAD_DOC_SEQ = 97;
    public static final int MONEY_GRAM_RECEIVER_REGISTRATION = 9;

    public static final String HELP_TEXT = "Help Text";   
    
    public static final int DW_MINIMUM_WIDTH = 650;
    public static final int DW_MINIMUM_HEIGHT = 530;
    
    // ThirdParty Information
    
    public static final String INDICATIVE_EXCHANGE_RATE = "indicativeExchangeRate";
    public static final String INDICATIVE_RECEIVE_AMOUNT = "indicativeReceiveAmount";
    public static final String INDICATIVE_RECEIVE_CURRENCY = "indicativeReceiveCurrency";
    
    public static final String AGENT_CHECK_AUTHORIZATION_NUMBER = "agentCheckAuthorizationNumber";
    
    public static final String PINPAD_REF_FOUND = "Pinpad: reference found";
    public static final String PINPAD_REF_FOUND_AND_CORRECT = "Pinpad: reference found and pin correct";
    public static final String PINPAD_PIN_FOUND_AND_CORRECT = "Pinpad: reference found and pin correct";
    public static final String PINPAD_ERROR_REF_NOTENOUGH_DIGITS = "Pinpad error: reference number did not enough digits or its sarting digit was a zero";
    public static final String PINPAD_ERROR_REFNUM_TOOMANY_RETRIES  = "Pinpad error: too many retries for reference number";
    public static final String PINPAD_ERROR_PIN_NOTENOUGH_DIGITS = "Pinpad error: PIN did not have enough digits";
    public static final String PINPAD_ERROR_PIN_TOOMANY_RETRIES  = "Pinpad error: too many retries for PIN";
    
    
    //AC Language Support
    public static final String AC_LANG_SPANISH    = "es-MX";    
    public static final String AC_LANG_ENGLISH    = "en-US";    
    public static final String AC_LANG_FRENCH     = "fr-FR";    
    public static final String AC_LANG_GERMAN     = "de-DE";    
    public static final String AC_LANG_PORTUGUESE = "pt-BR";    
    public static final String AC_LANG_POLISH     = "pl-PL";    
    public static final String AC_LANG_CHINESE    = "zh-CN";    
    public static final String AC_LANG_RUSSIAN    = "ru-RU";    
    
    // MISC 
    
    public static final String DISCOUNT_OPTION = "DISCOUNT_OPTION";

//    public static final BigDecimal ZERO = new BigDecimal(0);
    
    static {
    	String[] languages = Locale.getISOLanguages(); 
    	localeMap = new HashMap<String, String>(languages.length); 
    	for (int ii = 0; ii < languages.length; ii++) { 
    	    Locale locale = new Locale(languages[ii]); 
    	    localeMap.put(locale.getISO3Language().toUpperCase(Locale.US), languages[ii].toUpperCase(Locale.US)); 
    	} 
    }

    public static boolean isUsOrUsTerr(String pm_sLongLangCode) {
    	boolean bRetValue;
    	bRetValue = ((pm_sLongLangCode.compareToIgnoreCase("USA") == 0)
				|| (pm_sLongLangCode.compareToIgnoreCase("ASM") == 0)
				|| (pm_sLongLangCode.compareToIgnoreCase("GUM") == 0)
				|| (pm_sLongLangCode.compareToIgnoreCase("MNP") == 0)
				|| (pm_sLongLangCode.compareToIgnoreCase("PRI") == 0)
				|| (pm_sLongLangCode.compareToIgnoreCase("UMI") == 0)
				|| (pm_sLongLangCode.compareToIgnoreCase("VIR") == 0)
				);
    	return bRetValue;
    }
    
    /**
     * Return true if the version number indicates that this is a MGI owned PC
     */
    public static boolean isMGIOwnedPC() {
    	boolean bRetVal = false;
    	String sVerNum = UnitProfile.getInstance().getVersionNumber(true);
    	int iDPIdx = sVerNum.indexOf('.')+1;
    	
    	if (iDPIdx >= 0) {
    		int iVerNum = 0;
    		try {
    			iVerNum = Integer.parseInt(sVerNum.substring(iDPIdx, iDPIdx+2));
    		} catch (Exception e) {
    		}
    		bRetVal = ((iVerNum % 2) == 0);
    	}
    	return  bRetVal;
    }

    /**
     * determines if the passed in String is not empty
     */
    public static boolean nonEmpty(String sValue) {
    	boolean bRetValue = false;

    	bRetValue = ((sValue != null) && (sValue.length() > 0));
    	
    	return bRetValue;
    }
    
    /**
     * determines if the passed in String contains a non-zero value
     */
    public static boolean nonZero(String sValue) {
    	boolean bRetValue = false;
    	try {
			bRetValue = ((sValue != null) 
					&& (! sValue.equals("")) 
					&& (new BigDecimal(sValue).compareTo(BigDecimal.ZERO) != 0));
		} catch (Exception e) {
			Debug.dumpStack("Attempting zero check on bad value["+sValue+"]");
			bRetValue = false;
		} 
    	return bRetValue;
    }
    
    public static String getLegalIdName(String number) {
        return legalIdTypes.get(number);
    }

    public static Map<String, String> getPhotoIdTypes() {
    	return photoIdTypes;
    }

    public static String getPhotoIdName(String number) {
        return photoIdTypes.get(number);
    }

    public static String getProductVariantName(ProductVariantType key) {
    	if (key != null) {
    		return productVariantList.get(key.value());
    	} else {
    		return "";
    	}
    }

    public static String[] getAgentPrinterList() {
        return agentPrinterList.getSortedValues();
    }

    public static String[] getAgentReceiptPaperKeyList() {
        return agentReceiptPaperList.getSortedKeys();
    }
    
    public static String[] getAgentReceiptPaperValueList() {
        return agentReceiptPaperList.getSortedValues();
    }
    
    public static String[] getAgentReceiptTypeKeyList() {
        return agentReceiptTypeList.getSortedKeys();
    }

    public static String[] getAgentReceiptTypeValueList() {
        return agentReceiptTypeList.getSortedValues();
    }

    public static String[] getPhotoIdNames() {
        return photoIdTypes.getSortedValues();
    }

    public static String[] getLegalIdNames() {
        return legalIdTypes.getSortedValues();
    }

    public static int getPromoCodeLimit() {
        int retValue;
        try{
            String profileValue = UnitProfile.getInstance().get(PROFILE_PROMO_MAX, Integer.toString(PROMO_MAX_CODES));	 
            retValue = Integer.parseInt(profileValue);
        } 
        catch (Exception e) {
            retValue = PROMO_MAX_CODES;
        }
        return retValue;
    }
    
    public static PrintService getDefaultPrinter(){
        return PrintServiceLookup.lookupDefaultPrintService();
    }

    public static String[] getPrinterList(){
        //loadPrinterSelections();
        
        DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
        PrintService[] services = PrintServiceLookup.lookupPrintServices(flavor, null);
        String[] printerSelectionList = new String[services.length];
        
        for (int i=0; i<services.length; i++){
            printerSelectionList[i] = services[i].getName();
        }
        return printerSelectionList;
    }

    public static int getSelectedPrinter(){
        int printerIndex = -1;
        String printerSelection = UnitProfile.getInstance().get("AGENT_PRINTER_NAME", "");  

        String[] printerList = getPrinterList();
        
        for (int i=0; i<printerList.length; i++){
            if(printerList[i].equalsIgnoreCase(printerSelection)){
                printerIndex = i;
                break;
            }
        }
        if(printerIndex < 0){
            printerIndex = 0;
            String defaultPrinter = getDefaultPrinter().getName();
            for (int i=0; i<printerList.length; i++){
                if(printerList[i].equalsIgnoreCase(defaultPrinter)){
                    printerIndex = i;
                    break;
                }
            }
        }
            
        return printerIndex;
    }
    
    public static String getValidPrinterName(String profilePrinter) throws Exception {
    	if (profilePrinter != null) {
	    	for (String systemPrinter : getPrinterList()) {
	    		if (profilePrinter.equals(systemPrinter)) {
	    			return systemPrinter;
	    		}
	    	}
    	}
		throw new Exception("Printer Name " + profilePrinter + " from agent profile is not a system printer name");
    }


    public static int getSelectedAgentPrinter(){
        int printerIndex = -1;
        String agentPrinterSelection = UnitProfile.getInstance().get("USE_AGENT_PRINTER", "");  

        String[] agentPrinterList = getAgentPrinterList();
        
        for (int i=0; i<agentPrinterList.length; i++){
            if(getAgentPrinterListKey(agentPrinterList[i]).equalsIgnoreCase(agentPrinterSelection)){
                printerIndex = i;
                break;
            }
        }
        if(printerIndex < 0){
            printerIndex = 0;
            for (int i=0; i<agentPrinterList.length; i++){
                if(getAgentPrinterListKey(agentPrinterList[i]).equalsIgnoreCase("N")){
                    printerIndex = i;
                    break;
                }
            }
        }
            
        return printerIndex;
    }

    public static int getSelectedAgentReceiptPaper(){
        int paperIndex = -1;
        String paperSelection = UnitProfile.getInstance().get(DWValues.PROFILE_PRINTER_PAPER_SIZE, "");  

        String[] paperList = getAgentReceiptPaperKeyList();
        
        for (int i=0; i<paperList.length; i++){
            if(paperList[i].equalsIgnoreCase(paperSelection)){
                paperIndex = i;
                break;
            }
        }
        if(paperIndex < 0){
            paperIndex = 0;
            for (int i=0; i<paperList.length; i++){
                if(paperList[i].equalsIgnoreCase(AGENT_PRINTER_PAPER_SIZE_DEFAULT)){
                    paperIndex = i;
                    break;
                }
            }
        }
            
        return paperIndex;
    }

    public static int getSelectedAgentReceiptType(){
        int receiptTypeIndex = -1;
        String receiptTypeSelection = UnitProfile.getInstance().get(DWValues.PROFILE_PRINTER_RCPT_TYPE, "");  

        String[] receiptTypeList = getAgentReceiptTypeKeyList();
        
        for (int i=0; i<receiptTypeList.length; i++){
            if(receiptTypeList[i].equalsIgnoreCase(receiptTypeSelection)){
                receiptTypeIndex = i;
                break;
            }
        }
        if(receiptTypeIndex < 0){
            receiptTypeIndex = 0;
            for (int i=0; i<receiptTypeList.length; i++){
                if(receiptTypeList[i].equalsIgnoreCase(AGENT_PRINTER_TYPE_DEFAULT)){
                    receiptTypeIndex = i;
                    break;
                }
            }
        }
            
        return receiptTypeIndex;
    }

    /**
     * Get the name of the country that should be used to find states. For
     *   countries that do not have their own state lists, return US.
     */
    public static String getAgentPrinterListKey(String agentPrinter) {
    	return agentPrinterList.getKey(agentPrinter);
    }   
    
    /**
     * Get the name of the ReceiptType associated with the key.
     */
    public static String getAgentReceiptTypeListKey(String agentReceiptType) {
    	return agentReceiptTypeList.getKey(agentReceiptType);
    }   
    
    /**
     * Get the name of the PaperList associated with the key.
     */
    public static String getAgentReceiptPaperListKey(String agentPaper) {
    	return agentReceiptPaperList.getKey(agentPaper);
    }   
    

    // initialize the lists
    static {
		String language = Messages.getCurrentLocale().getLanguage();
		if ((language == null) || (language.equals(""))) {
			language = UnitProfile.getInstance().get("LANGUAGE", "en");
		}

        String[] legalIds = {"SSN", Messages.getString("DWValues.2054"),    
                             "TAX", Messages.getString("DWValues.2055"), 
                             "INT", Messages.getString("DWValues.2056"),
                             "ALN", Messages.getString("DWValues.2055")};    
        loadMap(legalIds, legalIdTypes);

        String[] agentReceiptTypes = {AGENT_PRINTER_TYPE_1,     Messages.getString("DWValues.RCPT_TYPE_1"),
        		AGENT_PRINTER_TYPE_2, Messages.getString("DWValues.RCPT_TYPE_2")};
        loadMap(agentReceiptTypes, agentReceiptTypeList);

        String[] agentReceiptPapers = {AGENT_PRINTER_PAPER_SIZE_1, Messages.getString("DWValues.RCPT_PAPER_1")};
        loadMap(agentReceiptPapers, agentReceiptPaperList);

        String[] photoIds = {"DRV", Messages.getString("DWValues.2057"),	 
                             "PAS", Messages.getString("DWValues.2058"), 
                             "STA", Messages.getString("DWValues.2059"),    
                             "GOV", Messages.getString("DWValues.2060"), 
                             "ALN", Messages.getString("DWValues.2061")};    
        loadMap(photoIds, photoIdTypes);
        
        String[] directions = {"D", "Downtown", "N", "North", "S", "South",      
                               "E", "East", "W", "West"};    
        loadMap(directions, directionList);

		String[] agentPrintersData = new String[] { "N", Messages.getString("DWValues.2066"),  
				"S", Messages.getString("DWValues.2067"),  
				"NP", Messages.getString("DWValues.2074"),  
				"SP", Messages.getString("DWValues.2075"),  
				"Y", Messages.getString("DWValues.2068") };  
		loadMap(agentPrintersData, agentPrinterList);

		if ("RU".equalsIgnoreCase(language)
				|| "CN".equalsIgnoreCase(language)) {  // use english for now, this receipt goes away in the future\
			  String[] productVariants = 
	        		{
	        		ProductVariantType.EP.value(), "ExpressPayment",	 
	        		ProductVariantType.PREPAY.value(), "Prepaid Services",	 
	        		ProductVariantType.UBP.value(), "Utility Bill Payment",	 
	                 };
	        loadMap(productVariants, productVariantList);
	        
	        
		} else { // languages with printable character set
			String[] productVariants = 
	        		{
	        		ProductVariantType.EP.value(), Messages.getString("DWValues.PV_EP"),	 
	        		ProductVariantType.PREPAY.value(), Messages.getString("DWValues.PV_PREPAY"),	 
	        		ProductVariantType.UBP.value(), Messages.getString("DWValues.PV_UTILITY_PAY"),	 
	                 };
	        loadMap(productVariants, productVariantList);
		}

        String[] languageRegions = {"am", "ET", "hy", "AM",    
                "ar", "SA", "bn", "BD",    
                "bg", "BG", "zh", "CN",    
                "cpf", "HT", "en", "US",    
                "fr", "FR", "de", "DE",    
                "id", "ID", "km", "KH",    
                "ko", "KR", "lo", "LA",    
                "ms", "MY", "mk", "MK", "pl", "PN",    
                "pt", "BR", "pa", "IN",    
                "ro", "RO", "ru", "RU",    
                "es", "MX", "sr", "RS", "tl", "PH",    
                "th", "TH", "uk", "UA",    
                "ur", "PK", "uz", "UZ",    
                "vi", "VN", "hmn", "VN"};    
		loadMap(languageRegions, languageRegionList);

        String[] receiptLanguages = {"AMH", "Amharic", "HYE", "Armenian",    
                "ARA", "Arabic", "BEN", "Bengali",    
                "BUL", "Bulgarian", "ZHO", "Chinese",    
                "HAT", "Creole", "ENG", "English",    
                "FRA", "French", "DEU", "German",    
                "IND", "Indonesian", "KHM", "Khmer",    
                "KOR", "Korean", "LAO", "Laotian",    
                "MSA", "Malaysian", "MKD", "Macedonian","POL", "Polish",    
                "POR", "Portuguese", "PAN", "Punjabi",    
                "RON", "Romanian", "RUS", "Russian",    
                "SPA", "Spanish", "SRP","Serbian","TGL", "Tagalog",    
                "THA", "Thai", "UKR", "Ukranian",    
                "URD", "Urdu", "UZB", "Uzbeki",    
                "VIE", "Vietnamese", "HMN", "Hmong"};    
        loadReceiptLanguageMap(receiptLanguages, receiptLanguageList);
    }

    /**
     * Load a map with mappings of the form <indicator> -&gt; <description>
     */
    private static void loadMap(String[] values, HashMap<String, String> map) {
        for (int i = 0; i < values.length; i = i + 2)
            map.put(values[i], values[i + 1]);
    }

	/**
	 * Load up a map with receiptLanguage mappings in the form of <3-char-abbrev> -&gt; <2-char-abbrev>
	 * and create a mapping of this receiptLanguage key to the BCP-47 format representation
	 */
	private static void loadReceiptLanguageMap(String[] values, HashMap<String, String> map) {
		receiptLanguageInfo.clear();
		Character.UnicodeBlock eScript = Character.UnicodeBlock.LATIN_1_SUPPLEMENT;
		for (int i = 0; i < values.length - 2; i = i + 2) {
			String lang2 = ((String) localeMap.get(values[i])).toLowerCase(Locale.US);
			String region = (String) languageRegionList.get(lang2);
			eScript = Character.UnicodeBlock.LATIN_1_SUPPLEMENT;
			if (lang2.equals("ru") || lang2.equals("uk") || lang2.equals("bg") || lang2.equals("uz")) {
				eScript = Character.UnicodeBlock.CYRILLIC;
			} else if (lang2.equals("zh")) {
				eScript = Character.UnicodeBlock.HANUNOO;
			} else if (lang2.equals("ar") || lang2.equals("ur")) {
				eScript = Character.UnicodeBlock.ARABIC;
			} else if (lang2.equals("am")) {
				eScript = Character.UnicodeBlock.ETHIOPIC;
			} else if (lang2.equals("hy")) {
				eScript = Character.UnicodeBlock.ARMENIAN;
			} else if (lang2.equals("bn")) {
				eScript = Character.UnicodeBlock.BENGALI;
			} else if (lang2.equals("km")) {
				eScript = Character.UnicodeBlock.KHMER;
			} else if (lang2.equals("ko")) {
				eScript = Character.UnicodeBlock.HANGUL_JAMO;
			} else if (lang2.equals("lo")) {
				eScript = Character.UnicodeBlock.LAO;
			} else if (lang2.equals("pa")) {
				eScript = Character.UnicodeBlock.GURMUKHI;
			} else if (lang2.equals("th")) {
				eScript = Character.UnicodeBlock.THAI;
			}
			receiptLanguageInfo.put(values[i], new Bcp47Info(values[i], values[i + 1], lang2, eScript, region));
		}

		// add the lang-script-region info for special case
		receiptLanguageInfo.put("HMN", new Bcp47Info("HMN", "Hmong", "hmn", Character.UnicodeBlock.LATIN_1_SUPPLEMENT, "CN"));
	}

    public static String stripSpaces(String pm_sInString) {
        String sOutString = pm_sInString.trim();
        int iIdx = sOutString.indexOf(' ');
        while (iIdx != -1) {
           	sOutString = sOutString.substring(0, iIdx) + sOutString.substring(iIdx+1);
            iIdx = sOutString.indexOf(' ');
        }
        return sOutString;
    }
    
    public static String intToLeadingZeroString(int pm_iNum, int pm_iDigits) {
    	int iDigits = pm_iDigits;
        if (iDigits < 1) {
        	iDigits = 1;
        }

        // create variable length array of zeros
        char[] zeros = new char[iDigits];
        Arrays.fill(zeros, '0');
        // format number as String
        DecimalFormat df = new DecimalFormat(String.valueOf(zeros));

        return df.format(pm_iNum);
    }
    
    public final static String MNEM_AMH = "AMH";
	public final static String MNEM_ARA = "ARA";
	public final static String MNEM_HYE = "HYE";
	public final static String MNEM_BEN = "BEN";
	public final static String MNEM_BUL = "BUL";
	public final static String MNEM_ZHO = "ZHO";
	public final static String MNEM_JAM = "JAM";
	public final static String MNEM_ENG = "ENG";
	public final static String MNEM_FRA = "FRA";
	public final static String MNEM_DEU = "DEU";
	public final static String MNEM_HNJ = "HNJ";
	public final static String MNEM_IND = "IND";
	public final static String MNEM_KHM = "KHM";
	public final static String MNEM_KOR = "KOR";
	public final static String MNEM_LAO = "LAO";
	public final static String MNEM_MKD = "MKD";
	public final static String MNEM_ZSM = "ZSM";
	public final static String MNEM_POL = "POL";
	public final static String MNEM_POR = "POR";
	public final static String MNEM_PAN = "PAN";
	public final static String MNEM_RON = "RON";
	public final static String MNEM_RUS = "RUS";
	public final static String MNEM_SPA = "SPA";
	public final static String MNEM_SRP = "SRP";
	public final static String MNEM_TGL = "TGL";
	public final static String MNEM_THA = "THA";
	public final static String MNEM_UKR = "UKR";
	public final static String MNEM_URD = "URD";
	public final static String MNEM_UZB = "UZB";
	public final static String MNEM_VIE = "VIE";
    
    
    public final static String MNEM_NONE = "NONE";
    
    public static String sConvertLang2ToLang3(String pm_sISO2) {
    	String sRetValue;
        Locale locale = new Locale(pm_sISO2); 
        try {
			sRetValue = locale.getISO3Language();
		} catch (MissingResourceException e) {
			Debug.println("Error - Bad ISO Language Code["+pm_sISO2+"]");
			// special case for PE misnomer of "cn" instead of "zh"
			if (pm_sISO2.equalsIgnoreCase(languageRegionList.get("zh"))) {
				sRetValue = MNEM_ZHO;
			} else {
				sRetValue = "ENG";
			}
		}
        return sRetValue.toUpperCase(Locale.US);
    }
    
    public static String sConvertLang3ToLang2(String pm_sISO3) {
        return localeMap.get(pm_sISO3.toUpperCase(Locale.US));
    }

	public static String sConvertLang3ToLang5(String pm_sISO3) {
		if (receiptLanguageInfo != null && pm_sISO3.length() > 0) {
			String sISO3 = pm_sISO3.toUpperCase(Locale.US);
			dw.utility.Bcp47Info langCodes = receiptLanguageInfo.get(sISO3);
			if (langCodes != null) {
				return langCodes.getLangWithRegion();
			} else {
				return "en-US";
			}
		} else {
			// no language specification will be used
			return "";
		}
	}

    /**
     * Refresh the ID lists when language Toggle is selected.
     * Called from Messages.setCurrentLocale();
     * @author Renuka Easwar
     */
    public static void refreshIdLists(){
		String language = Messages.getCurrentLocale().getLanguage();
		if ((language == null) || (language.equals(""))) {
			language = UnitProfile.getInstance().get("LANGUAGE", "en");
		}

        String[] legalIds = {"SSN", Messages.getString("DWValues.2054"),    
                            "TAX", Messages.getString("DWValues.2055"),             
                            "INT", Messages.getString("DWValues.2056"),
                            "ALN", Messages.getString("DWValues.2055")};        
        loadMap(legalIds, legalIdTypes);
        
        String[] photoIds = {"DRV", Messages.getString("DWValues.2057"),    
                             "PAS", Messages.getString("DWValues.2058"),                   
                             "STA", Messages.getString("DWValues.2059"),                   
                             "GOV", Messages.getString("DWValues.2060"),             
                             "ALN", Messages.getString("DWValues.2061")};                  
        loadMap(photoIds, photoIdTypes);
        
        if (!"RU".equalsIgnoreCase(language)
				&& !"CN".equalsIgnoreCase(language))  { // exclude russian & chinese - this receipt goes away in the near future		
			String[] productVariants = 
	    		{
	    		ProductVariantType.EP.value(), Messages.getString("DWValues.PV_EP"),	 
	    		ProductVariantType.PREPAY.value(), Messages.getString("DWValues.PV_PREPAY"),	 
	    		ProductVariantType.UBP.value(), Messages.getString("DWValues.PV_UTILITY_PAY"),	 
	             };
	    loadMap(productVariants, productVariantList);
		}

        String[] agentReceiptTypes = {AGENT_PRINTER_TYPE_1,     Messages.getString("DWValues.RCPT_TYPE_1"),
        		AGENT_PRINTER_TYPE_2, Messages.getString("DWValues.RCPT_TYPE_2")};
        loadMap(agentReceiptTypes, agentReceiptTypeList);
        
        String[] agentReceiptPapers = {AGENT_PRINTER_PAPER_SIZE_1, Messages.getString("DWValues.RCPT_PAPER_1")};
        loadMap(agentReceiptPapers, agentReceiptPaperList);
    }
    
	public static String handleSpecialChars(String pm_sInput) {
		String sTemp = pm_sInput;
		String sRetValue = "";
		int iStart = sTemp.indexOf("&#");
		if (iStart == -1) {
			return sTemp;
		}
		while (iStart != -1) {
			int iEnd = sTemp.indexOf(";");
			int iSize = iEnd-iStart;
			/*
			 * Try for decimal value &#ddd;
			 */
			if ((iSize > 2) && (iSize <= 5)) {
				/*
				 * Try as decimal value (skipping beginning '&#'
				 */
				String sDecValue = sTemp.substring(iStart+2, iEnd);
				try {
					int iCharValue = 0;
					try {
						iCharValue = Integer.parseInt(sDecValue);
					} catch (Exception e) {
					}
					sRetValue = sRetValue + 
						sTemp.substring(0, iStart) +
						Character.toString((char)iCharValue) ;
				} catch (NumberFormatException e) {
					try {
						/*
						 * Try as hex value (skipping beginning 'x')
						 */
						int iCharValue = Integer.parseInt(sDecValue.substring(1), 16);  
						sRetValue = sRetValue + 
							sTemp.substring(0, iStart) +
							Character.toString((char)iCharValue) ;
					} catch (NumberFormatException e1) {
						sRetValue = sRetValue + 
						sTemp.substring(0, iStart) +
						"?" ;
					}
				}
				/*
				 * Try for hex value &#xdddd;
				 */
			} else if (iSize == 7){
				try {
					/*
					 * Try as hex value (skipping beginning '&#x'
					 */
					String sDecValue = sTemp.substring(iStart+3, iEnd);
					int iCharValue = Integer.parseInt(sDecValue, 16);  
					sRetValue = sRetValue + 
						sTemp.substring(0, iStart) +
						Character.toString((char)iCharValue) ;
				} catch (NumberFormatException e1) {
					sRetValue = sRetValue + 
					sTemp.substring(0, iStart) +
					"?" ;
				}
			} else {
				if (iEnd != -1) {
					sRetValue = sRetValue + sTemp.substring(0, iEnd);
				} else {
					sRetValue = sRetValue +	sTemp;
				}
			}
			if (sTemp.length() <= iEnd || (iEnd == -1)) {
				return sRetValue;
			}
			sTemp = sTemp.substring(iEnd+1);
			iStart = sTemp.indexOf("&#");
		}
		return sRetValue + sTemp;
	}

    public static String formatName(String firstName, String middleName, String lastName1, String lastName2) {
    	StringBuffer sb = new StringBuffer();
    	if (firstName != null) {
    		sb.append(firstName);
    	}
		if ((middleName != null) && (! middleName.equals(""))) {
			if (sb.length() > 0) {
				sb.append(" ");
			}
			sb.append(middleName);
		}
		if (lastName1 != null) {
			if (sb.length() > 0) {
				sb.append(" ");
			}
			sb.append(lastName1);
		}
		if ((lastName2 != null) && (! lastName2.equals(""))) {
			if (sb.length() > 0) {
				sb.append(" ");
			}
			sb.append(lastName2);
		}
		return sb.toString();
    }

    public static String getAddress(String address1, String address2, String address3, String address4) {
		StringBuffer sb = new StringBuffer();
		if ((address1 != null) && (! address1.isEmpty())) {
			sb.append(address1);
		}
		if ((address2 != null) && (! address2.isEmpty())) {
			sb.append(", ");
			sb.append(address2);
		}
		if ((address3 != null) && (! address3.isEmpty())) {
			sb.append(", ");
			sb.append(address3);
		}
		if ((address4 != null) && (! address4.isEmpty())) {
			sb.append(", ");
			sb.append(address4);
		}
		return sb.toString();
	}

    /**
	 * returns the language display name for the given long (3 character) 
	 * language code
	 * 
	 * @param code
	 *            Three-letter currency code
	 * @return
	 */
	public static String getLanguageDisplayName(String language) {
    	String displayName = "";
		if (language != null) {
			
	    	if (language.compareToIgnoreCase(DWValues.MNEM_AMH) == 0) {
	    		displayName = Messages.getString("Transalation.Language.AMH");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_ARA) == 0) {
	    		displayName = Messages.getString("Transalation.Language.ARA");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_HYE) == 0) {
	    		displayName = Messages.getString("Transalation.Language.HYE");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_BEN) == 0) {
	    		displayName = Messages.getString("Transalation.Language.BEN");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_BUL) == 0) {
	    		displayName = Messages.getString("Transalation.Language.BUL");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_ZHO) == 0) {
	    		displayName = Messages.getString("Transalation.Language.ZHO");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_JAM) == 0) {
	    		displayName = Messages.getString("Transalation.Language.JAM");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_ENG) == 0) {
	    		displayName = Messages.getString("Transalation.Language.ENG");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_FRA) == 0) {
	    		displayName = Messages.getString("Transalation.Language.FRA");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_DEU) == 0) {
	    		displayName = Messages.getString("Transalation.Language.DEU");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_HNJ) == 0) {
	    		displayName = Messages.getString("Transalation.Language.HNJ");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_IND) == 0) {
	    		displayName = Messages.getString("Transalation.Language.IND");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_KHM) == 0) {
	    		displayName = Messages.getString("Transalation.Language.KHM");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_KOR) == 0) {
	    		displayName = Messages.getString("Transalation.Language.KOR");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_LAO) == 0) {
	    		displayName = Messages.getString("Transalation.Language.LAO");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_MKD) == 0) {
	    		displayName = Messages.getString("Transalation.Language.MKD");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_ZSM) == 0) {
	    		displayName = Messages.getString("Transalation.Language.ZSM");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_POL) == 0) {
	    		displayName = Messages.getString("Transalation.Language.POL");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_POR) == 0) {
	    		displayName = Messages.getString("Transalation.Language.POR");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_PAN) == 0) {
	    		displayName = Messages.getString("Transalation.Language.PAN");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_RON) == 0) {
	    		displayName = Messages.getString("Transalation.Language.RON");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_RUS) == 0) {
	    		displayName = Messages.getString("Transalation.Language.RUS");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_SPA) == 0) {
	    		displayName = Messages.getString("Transalation.Language.SPA");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_SRP) == 0) {
	    		displayName = Messages.getString("Transalation.Language.SRP");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_TGL) == 0) {
	    		displayName = Messages.getString("Transalation.Language.TGL");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_THA) == 0) {
	    		displayName = Messages.getString("Transalation.Language.THA");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_UKR) == 0) {
	    		displayName = Messages.getString("Transalation.Language.UKR");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_URD) == 0) {
	    		displayName = Messages.getString("Transalation.Language.URD");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_UZB) == 0) {
	    		displayName = Messages.getString("Transalation.Language.UZB");
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_VIE) == 0) {
	    		displayName = Messages.getString("Transalation.Language.VIE");
	    	
	    	} else if (language.compareToIgnoreCase(DWValues.MNEM_NONE) == 0) {
	    		displayName = Messages.getString("DWValues.2069");
	    	} else {
	    		// Default to English
	    		displayName = Messages.getString("DeltaworksMainPanel.761");
	        	Debug.println("Language code not found: " + language);
	    	}
		} else { 
			displayName = Messages.getString("DeltaworksMainPanel.761");
		}
		return displayName;
	}

	/*
	 * Convert ISO (cc-ss) style subdivision codes to a 2 character code for use
	 * in ISI records.
	 */
	public static String convertIsoSubdivToState(String isoCode) {
		if (isoCode != null) {
			int codeLength = isoCode.length();
			if (codeLength > 2) {
				int afterDashPos = isoCode.indexOf('-') + 1;
				if (codeLength >= afterDashPos + 2) {
					isoCode = isoCode.substring(afterDashPos, afterDashPos + 2);
				} else {
					isoCode = isoCode.substring(afterDashPos);
				}
			}
		}
		return isoCode;
	}
}

/**
 * A simple map that stores key/value pairs and may be sorted on the key
 *   or value.
 */
class CustomMap extends HashMap<String, String> {
	private static final long serialVersionUID = 1L;

	/**
     * Get the value for a specific key. Should not be case sensitive, so search
     *   by the key and then also the by toUpperCase(key).
     */
    public String get(String key) {
//        if (! (key instanceof String))
//            return super.get(key);
        String value = super.get(key);
        if (value == null)
            // try upper case
            value = super.get((key).toUpperCase());
        return value;
    }

    /**
     * Return a sorted array of String keys. Assumes the keys added are Strings.
     */
    public String[] getSortedKeys() {
        Set<String> keys = super.keySet();
        String[] keyArray = keys.toArray(new String[0]);
        Arrays.sort(keyArray);
        return keyArray;
    }

    /**
     * Return a array of values. Assumes the values added are Strings.
     */
    public String[] getValues() {
        Collection<String> values = super.values();
        String[] valueArray = values.toArray(new String[0]);
        return valueArray;
    }

    /**
     * Return a sorted array of values. Assumes the values added are Strings.
     */
    public String[] getSortedValues() {
        Collection<String> values = super.values();
        String[] valueArray = values.toArray(new String[0]);
        Arrays.sort(valueArray);
        return valueArray;
    }

    /**
     * Reverse lookup of key from value. Assumes key is a String. Search is
     *   not case sensitive.
     */
    public String getKey(String val) {
        for (Iterator<String> i = keySet().iterator(); i.hasNext();) {
        	String key =  i.next();
            String value = get(key);
            if (value.equalsIgnoreCase(val))
                return key;
        }
        return null;
    }
    
}
