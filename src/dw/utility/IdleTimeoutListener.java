package dw.utility;

/**
 * Callback interface for the IdleBackoutTimer. Classes inplementing this may
 *   add themselves to the IdleBackoutTimer to be registered for the timeout
 *   event.
 */
public interface IdleTimeoutListener {
    public void idleTimeoutLong();
    public void idleTimeoutShort();
    public String getNamePinRequiredValue();
}
