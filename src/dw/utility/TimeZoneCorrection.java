/*
 * TimeZoneCorrection.java
 * 
 * $Revision$
 * 
 */

package dw.utility;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Singleton class used to hold TimeZone correction information. 
 * 
 */

public class TimeZoneCorrection {
	private String timeZoneId; 
	private int offset;			
	private int dstSavings;
	private int startMode;	
	private int startDay;
	private int startMonth;
	private int startDayOfWeek;
	private int startTime;
	private int startTimeMode;
	private int endMode;
	private int endDay;
	private int endMonth;
	private int endDayOfWeek;
	private int endTime;
	private int endTimeMode;
	
	private static TimeZoneCorrection instance = null;

    public static TimeZoneCorrection getInstance() {
    	
    	
    	if (instance == null) { 
    		instance = new TimeZoneCorrection();
    	}
    	return instance;
    	
    }
	
	private TimeZoneCorrection() {
		/* default to local timezone */
		TimeZone tz = Calendar.getInstance().getTimeZone();
		timeZoneId = tz.getID();
		offset = tz.getRawOffset();
		dstSavings = tz.getDSTSavings();
	}
	
	public String getId() {
		return timeZoneId;
	}
	public void setId(String tzId) {
		this.timeZoneId = tzId;
	}
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public int getDstSavings() {
		return dstSavings;
	}
	public void setDstSavings(int savings) {
		this.dstSavings = savings;
	}
	public int getStartMode() {
		return startMode;
	}
	public void setStartMode(int mode) {
		this.startMode = mode;
	}
	public int getEndMode() {
		return endMode;
	}
	public void setEndMode(int mode) {
		this.endMode = mode;
	}
	public int getStartDay() {
		return startDay;
	}
	public void setStartDay(int day) {
		this.startDay = day;
	}
	public int getStartMonth() {
		return startMonth;
	}
	public void setStartMonth(int month) {
		this.startMonth = month;
	}
	public int getStartDayOfWeek() {
		return startDayOfWeek;
	}
	public void setStartDayOfWeek(int dayOfWeek) {
		this.startDayOfWeek = dayOfWeek;
	}
	public int getStartTime() {
		return startTime;
	}
	public void setStartTime(int hoursAsMillis) {
		this.startTime = hoursAsMillis;
	}
	public int getStartTimeMode() {
		return startTimeMode;
	}
	public void setStartTimeMode(int mode) {
		this.startTimeMode = mode;
	}
	public int getEndDay() {
		return endDay;
	}
	public void setEndDay(int day) {
		this.endDay = day;
	}
	public int getEndMonth() {
		return endMonth;
	}
	public void setEndMonth(int month) {
		this.endMonth = month;
	}
	public int getEndDayOfWeek() {
		return endDayOfWeek;
	}
	public void setEndDayOfWeek(int dayOfWeek) {
		this.endDayOfWeek = dayOfWeek;
	}
	public int getEndTime() {
		return endTime;
	}
	public void setEndTime(int hoursAsMillis) {
		this.endTime = hoursAsMillis;
	}
	public int getEndTimeMode() {
		return endTimeMode;
	}
	public void setEndTimeMode(int mode) {
		this.endTimeMode = mode;
	}
	public boolean usesDst() {
		return dstSavings != 0;
	}
		
	@Override
	public String toString() {
		return "timeZoneId:"+timeZoneId+
				"\noffset:"+offset+
				"\ndstSavings:"+dstSavings+
				"\nstartMode:"+startMode+
				"\nstartDay:"+startDay+
				"\nstartMonth:"+startMonth+
				"\nstartDayOfWeek:"+startDayOfWeek+
				"\nstartTime:"+startTime+
				"\nstartTimeMode:"+startTimeMode+
				"\nendMode:"+endMode+
				"\nendDay:"+endDay+
				"\nendMonth:"+endMonth+
				"\nendDayOfWeek:"+endDayOfWeek+
				"\nendTime:"+endTime+
				"\nendTimeMode:"+endTimeMode;
	}
}