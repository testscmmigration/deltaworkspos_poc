package dw.utility;

import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.moneygram.agentconnect.CurrencyInfo;
import com.moneygram.agentconnect.GetCurrencyInfoResponse;

import dw.comm.MessageFacade;
import dw.comm.MessageLogic;
import dw.framework.CommCompleteInterface;
import dw.framework.WaitToken;
import dw.utility.DwEncryptionAES.DwEncryptionFileException;

public class Currency {
	
	private static final String CURRENCY_INFO_DATA_FILE_NAME = "cur.obj";

	private static CurrencyData currencyData = new CurrencyData(); 
	private static GetCurrencyInfoResponse currencyInfoResponse;
    private static WaitToken wait = new WaitToken();
    
    private static class CurrencyData implements Serializable {
		private static final long serialVersionUID = 1L;

		private String version = null;
        private Map<String, CurrencyInfo> currencyInfoMap = new HashMap<String, CurrencyInfo>();
    }
    
	public static void getCurrencyInfo(final int retrieveType) {
		currencyInfoResponse = null;
		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {
				try {
					checkCurrencyData();
					currencyInfoResponse = MessageFacade.getInstance().getCurrencyInfo(retrieveType, currencyData.version);
					return Boolean.TRUE;
				} catch (Exception e) {
					return Boolean.FALSE;
				}
			}
		};
		CommCompleteInterface cci = new CommCompleteInterface() {
			@Override
			public void commComplete(int commTag, Object returnValue) {
				try {
					boolean b = returnValue instanceof Boolean ? (Boolean) returnValue : false;
					if (b) {
						processCurrencyInfo();
					}
				} finally {
					wait.setWaitFlag(false);
					synchronized (wait) {
						wait.notifyAll();
					}
				}
			}
		};

		MessageFacade.run(logic, cci, 0, Boolean.TRUE);

		wait.setWaitFlag(true);
		synchronized (wait) {
			try {                       
				while (wait.isWaitFlag()) {
					wait.wait();
             	}
			} catch (InterruptedException ie) {
                 Debug.println(" caught InterruptedException"); 
			}
		}    
	}
	
	private static void processCurrencyInfo() {
		if ((currencyInfoResponse != null) && 
				(currencyInfoResponse.getPayload() != null) && 
				(currencyInfoResponse.getPayload().getValue().getCurrencyInfos() != null) && 
				(! currencyInfoResponse.getPayload().getValue().getCurrencyInfos().getCurrencyInfo().isEmpty())) {
			currencyData.currencyInfoMap.clear();
			
			currencyData.version = currencyInfoResponse.getPayload().getValue().getVersion();
			for (CurrencyInfo currencyInfo : currencyInfoResponse.getPayload().getValue().getCurrencyInfos().getCurrencyInfo()) {
				String code = currencyInfo.getCurrencyCode();
				currencyData.currencyInfoMap.put(code, currencyInfo);
			}

			try {
				DwEncryptionAES oe = new DwEncryptionAES();
				oe.vInitialize(DwEncryptionAES.OFFSET_CUR_DATA, CURRENCY_INFO_DATA_FILE_NAME);
				oe.vWriteSaveFile(currencyData);
			} catch (Exception e) {
				Debug.printException(e);
			}
		}
	}
	
	private static synchronized void checkCurrencyData() {
		if (currencyData.version == null) {
			File file = new File(CURRENCY_INFO_DATA_FILE_NAME);
			if (file.exists() && file.isFile()) {
				DwEncryptionAES oe = new DwEncryptionAES();
				oe.vInitialize(DwEncryptionAES.OFFSET_CUR_DATA, CURRENCY_INFO_DATA_FILE_NAME);
				try {
					currencyData = (CurrencyData) oe.readObject();
				} catch (DwEncryptionFileException e) {
					Debug.printException(e);
				}
			}
		}
	}
	
	public static int getCurrencyPrecision(String currency) {
		checkCurrencyData();
		CurrencyInfo currencyInfo = currencyData.currencyInfoMap.get(currency);
		return  currencyInfo != null ? currencyInfo.getCurrencyPrecision().intValue() : 2;
	}
}
