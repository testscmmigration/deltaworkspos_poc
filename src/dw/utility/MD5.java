package dw.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import dw.io.diag.ByteArray;
import flags.CompileTimeFlags;

public class MD5 {

	public static boolean fileExists(String pm_sFileName) {
		final boolean bExists = (new File(pm_sFileName)).exists();
		return bExists;
	}

	public static String fileMD5(String pm_sFileName) {
		InputStream is;
		String sMD5 = "";

		try {
			final File f = new File(pm_sFileName);
			if (!f.exists()) {
				Debug.println("fileMD5 - File[" + pm_sFileName + "] Not Found");
				return "";
			}
			is = new FileInputStream(f);
		} catch (final FileNotFoundException e) {
			Debug.printException(e);
			return "";
		}
		sMD5 = streamMD5(is);
		ExtraDebug.println("MD5:" + pm_sFileName + "[" + sMD5 + "]");
		return sMD5;
	}

	public static String streamMD5(InputStream is) {
		MessageDigest mdDigest;
		String sMD5 = "";

		if (is == null) {
			Debug.println("*** streamMD5 Null input stream!!");
			return sMD5;
		}

		try {
			mdDigest = MessageDigest.getInstance("MD5");
		} catch (final NoSuchAlgorithmException e1) {
			Debug.printException(e1);
			return sMD5;
		}

		final byte[] buffer = new byte[8192];
		int read = 0;
		try {
			while ((read = is.read(buffer)) > 0) {
				mdDigest.update(buffer, 0, read);
			}
			sMD5 = ByteArray.hex(mdDigest.digest());

		} catch (final IOException e) {
			Debug.printException(e);
			return "";
		} finally {
			try {
				is.close();
			} catch (final IOException e) {
				Debug.printException(e);
				return "";
			}
		}
		return sMD5;
	}

	public static boolean validateFile(InputStream pm_is, String pm_sName,
			String pm_sExpectedMD5) {
		boolean bRetValue = false;

		if (CompileTimeFlags.rcptNoFileValidation()) {
			return true;
		}
		
		final String sExpectedMD5 = DWValues.stripSpaces(pm_sExpectedMD5);
		final String sCalculatedMD5 = DWValues.stripSpaces(streamMD5(pm_is));

		if (sExpectedMD5.compareToIgnoreCase(sCalculatedMD5) == 0) {
			bRetValue = true;
		} else {
			Debug.println("MD5 Failure " + pm_sName + " - Expected = "
					+ sExpectedMD5 + ", Actual = " + sCalculatedMD5);
		}
		return bRetValue;
	}

	public static boolean validateFile(String pm_sFileName,
			String pm_sExpectedMD5) {
		boolean bRetValue = false;

		if (CompileTimeFlags.rcptNoFileValidation()) {
			return true;
		}
		
		final String sExpectedMD5 = DWValues.stripSpaces(pm_sExpectedMD5);
		final String sCalculatedMD5 = DWValues
				.stripSpaces(fileMD5(pm_sFileName));
		
		if (sExpectedMD5.compareToIgnoreCase(sCalculatedMD5) == 0) {
			bRetValue = true;
		} else {
			Debug.println("MD5 Failure " + pm_sFileName + "Expected = "
					+ sExpectedMD5 + ", Actual = " + sCalculatedMD5);
		}
		return bRetValue;
	}

}
