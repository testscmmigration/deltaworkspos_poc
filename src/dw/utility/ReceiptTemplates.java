package dw.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;

import org.xml.sax.InputSource;

import com.moneygram.agentconnect.ReceiptFormatDetailsType;
import com.moneygram.agentconnect.ReceiptsFormatDetailsResponse;

import dw.comm.MessageFacade;
import dw.comm.MessageFacadeParam;
import dw.comm.MessageLogic;
import dw.framework.CommCompleteInterface;
import dw.framework.WaitToken;
import dw.io.DiagLog;
import dw.io.diag.MustTransmitDiagMsg;
import dw.main.DeltaworksMainPanel;
import dw.mgreceive.MoneyGramReceiveTransaction;
import dw.mgsend.MoneyGramClientTransaction;
import dw.mgsend.MoneyGramSendTransaction;
import dw.model.adapters.ReceiptTemplateInfo;
import dw.profile.Profile;
import dw.profile.ProfileItem;
import dw.profile.UnitProfile;
import flags.CompileTimeFlags;

/**
 * Provides storage and access of Receipt Templates.
 * 
 * @author John Lesko (W156)
 */
public class ReceiptTemplates extends DwEncryptionAES {
	public static class Rmi implements MD5fileData.ReadMethodInterface,
			Serializable {
		private static final long serialVersionUID = 1L;


		@Override
		public InputStream getInputStream(String pm_sFileName) {
			CipherInputStream cis = null;
			InputStream is;
			try {
				final File fFile = new File(pm_sFileName);
				is = new FileInputStream(fFile);
				if (CompileTimeFlags.rcptNoTemplateEncryption()) {
					return is;
				} else {
					//
					// Set the cipher to decryption mode.
					//
					try {
						ref.cipher.init(Cipher.DECRYPT_MODE, ref.skeySpec, ref.ivspec);
						//
						// Setup the encrypted input stream
						//
						final FileInputStream fileIn = new FileInputStream(
								pm_sFileName);
						cis = new CipherInputStream(fileIn, ref.cipher);
					} catch (final InvalidKeyException e) {
						Debug.printException(e);
						return null;
					} catch (InvalidAlgorithmParameterException e) {
						Debug.printException(e);
						return null;
					}
				}
			} catch (final FileNotFoundException e) {
				Debug.printException(e);
				return null;
			}
			return cis;
		}
	}

	private static class TemplateRecord extends MD5fileData {
		private static final long serialVersionUID = 1L;

		@Override
		public String toString() {
			return "TemplateRecord [sLanguage=" + sLanguage + ", sVersion=" + sVersion + "]";
		}

		private final String sLanguage;
		private final String sVersion;

		private TemplateRecord(String pm_sFileName, String pm_sExpectedMD5,
				String pm_sLanguage, String pm_sVersion) {
			super(pm_sFileName, pm_sExpectedMD5, new Rmi());
			sLanguage = pm_sLanguage;
			sVersion = pm_sVersion;
		}

		/**
		 * @return String containing the Language of the Template
		 */
		public String getsLanguage() {
			return sLanguage;
		}

		/**
		 * @return String containing the Version of the Template
		 */
		public String getsVersion() {
			return sVersion;
		}
	}

	private static volatile ReceiptTemplates ref = null;

	public final static String PRIMARY_LANGUAGE = DWValues.MNEM_ENG;

	public final static int RECEIPT_SEND_CONF = 0;
	public final static int RECEIPT_SEND_AGENT = 1;
	public final static int RECEIPT_SEND_CUST = 2;
	public final static int RECEIPT_RECV_AGENT = 3;
	public final static int RECEIPT_RECV_CUST = 4;
	public final static int RECEIPT_BP_CONF = 5;
	public final static int RECEIPT_BP_AGENT = 6;
	public final static int RECEIPT_BP_CUST = 7;
	public final static int RECEIPT_PPS_AGENT = 8;
	public final static int RECEIPT_PPS_CUST = 9;
	public final static int RECEIPT_UBP_AGENT = 10;
	public final static int RECEIPT_UBP_CUST = 11;
	public final static int RECEIPT_XPAY_CONF = 12;
	public final static int RECEIPT_XPAY_AGENT = 13;
	public final static int RECEIPT_XPAY_CUST = 14;
	public final static int RECEIPT_REFUND_AGENT = 15;
	public final static int RECEIPT_REFUND_CUST = 16;
	public final static int RECEIPT_WARNING_NS = 17;
	public final static int RECEIPT_WARNING = 18;
	
	public final static int RECEIPT_INDEX_FIRST = RECEIPT_SEND_CONF;
	public final static int RECEIPT_INDEX_LAST = RECEIPT_WARNING;

//	public final static int RECEIPT_TYPE_SEND_START = RECEIPT_SEND_CONF;
//	public final static int RECEIPT_TYPE_SEND_END = RECEIPT_SEND_CUST;
//	public final static int RECEIPT_TYPE_RECV_START = RECEIPT_RECV_AGENT;
//	public final static int RECEIPT_TYPE_RECV_END = RECEIPT_RECV_CUST;
//	public final static int RECEIPT_TYPE_BP_START = RECEIPT_BP_CONF;
//	public final static int RECEIPT_TYPE_BP_END = RECEIPT_XPAY_CUST;
//	public final static int RECEIPT_TYPE_REFUND_START = RECEIPT_REFUND_AGENT;
//	public final static int RECEIPT_TYPE_REFUND_END = RECEIPT_REFUND_CUST ;
	public final static int RECEIPT_TYPE_WARNING_START = RECEIPT_WARNING_NS;
	public final static int RECEIPT_TYPE_WARNING_END = RECEIPT_WARNING;

	public final static int NUMBER_RECEIPT_TYPES_WARNING = RECEIPT_TYPE_WARNING_END
			- RECEIPT_TYPE_WARNING_START + 1;
	public final static int NUMBER_RECEIPT_TYPES_ALL = RECEIPT_INDEX_LAST + 1;

	private final static String FILE_EXTENSION = ".rt";
	private final static String SAVE_FILE_NAME = "0000" + FILE_EXTENSION;
	private final static String[] saFileNameTypeList = { "VALID", "SENDAG",
			"SENDCU", "RECVAG", "RECVCU", "BPVALID", "BPAG", "BPCU", "PPSAG",
			"PPSCU", "UBPAG", "UBPCU", "XPAYVALID", "XPAYAG", "XPAYCU",
			"REFAG", "REFCU", "WRNGNS", "WRNING"  };

	public final static HashMap<String, String> externalToInternalMap = new HashMap<String, String>();

	private static HashMap<String, TemplateRecord>[] hmaTemplateMapArray = new HashMap[NUMBER_RECEIPT_TYPES_ALL];

	private static Boolean gotNewTemplateInfo = Boolean.FALSE;

	static {
		getSingletonObject();
		if (saFileNameTypeList.length != NUMBER_RECEIPT_TYPES_ALL) {
			Debug.println("ERROR: saFileNameTypeList.length["
					+ saFileNameTypeList.length + "], MUST be ["
					+ NUMBER_RECEIPT_TYPES_ALL + "]");
		} else {
			externalToInternalMap.put(DWValues.RT_TYPE_SEND_VALIDATION,
					DWValues.intToLeadingZeroString(RECEIPT_SEND_CONF, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_SEND_VALIDATION_LANG_2,
					DWValues.intToLeadingZeroString(RECEIPT_SEND_CONF, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_SEND_AGENT,
					DWValues.intToLeadingZeroString(RECEIPT_SEND_AGENT, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_SEND_AGENT_LANG_2,
					DWValues.intToLeadingZeroString(RECEIPT_SEND_AGENT, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_SEND_CUSTOMER,
					DWValues.intToLeadingZeroString(RECEIPT_SEND_CUST, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_SEND_CUSTOMER_LANG_2,
					DWValues.intToLeadingZeroString(RECEIPT_SEND_CUST, 2));

			externalToInternalMap.put(DWValues.RT_TYPE_INTL_SEND_US_PREPAY_LANG_2,
					DWValues.intToLeadingZeroString(RECEIPT_SEND_CONF, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_INTL_SEND_US_AGENT_LANG_2,
					DWValues.intToLeadingZeroString(RECEIPT_SEND_AGENT, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_INTL_SEND_US_CUSTOMER_LANG_2,
					DWValues.intToLeadingZeroString(RECEIPT_SEND_CUST, 2));

			externalToInternalMap.put(DWValues.RT_TYPE_INTL_SEND_CA_PREPAY_LANG_2,
					DWValues.intToLeadingZeroString(RECEIPT_SEND_CONF, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_INTL_SEND_CA_AGENT_LANG_2,
					DWValues.intToLeadingZeroString(RECEIPT_SEND_AGENT, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_INTL_SEND_CA_CUSTOMER_LANG_2,
					DWValues.intToLeadingZeroString(RECEIPT_SEND_CUST, 2));

			externalToInternalMap.put(DWValues.RT_TYPE_INTL_SEND_PREPAY_NODF_LANG_1,
					DWValues.intToLeadingZeroString(RECEIPT_SEND_CONF, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_INTL_SEND_AGENT_NODF_LANG_1,
					DWValues.intToLeadingZeroString(RECEIPT_SEND_AGENT, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_INTL_SEND_CUSTOMER_NODF_LANG_1,
					DWValues.intToLeadingZeroString(RECEIPT_SEND_CUST, 2));

			externalToInternalMap.put(DWValues.RT_TYPE_INTL_SEND_PREPAY_INTL_LANG_1,
					DWValues.intToLeadingZeroString(RECEIPT_SEND_CONF, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_INTL_SEND_AGENT_INTL_LANG_1,
					DWValues.intToLeadingZeroString(RECEIPT_SEND_AGENT, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_INTL_SEND_CUSTOMER_INTL_LANG_1,
					DWValues.intToLeadingZeroString(RECEIPT_SEND_CUST, 2));

			externalToInternalMap.put(DWValues.RT_TYPE_RECV_AGENT_LANG_1,
					DWValues.intToLeadingZeroString(RECEIPT_RECV_AGENT, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_RECV_CUSTOMER_LANG_1,
					DWValues.intToLeadingZeroString(RECEIPT_RECV_CUST, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_RECV_AGENT_LANG_1_INTL,
					DWValues.intToLeadingZeroString(RECEIPT_RECV_AGENT, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_RECV_CUSTOMER_LANG_1_INTL,
					DWValues.intToLeadingZeroString(RECEIPT_RECV_CUST, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_RECV_AGENT_LANG_2,
					DWValues.intToLeadingZeroString(RECEIPT_RECV_AGENT, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_RECV_CUSTOMER_LANG_2,
					DWValues.intToLeadingZeroString(RECEIPT_RECV_CUST, 2));
			
			externalToInternalMap.put(DWValues.RT_TYPE_BP_VALIDATION,
					DWValues.intToLeadingZeroString(RECEIPT_BP_CONF, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_BP_AGENT,
					DWValues.intToLeadingZeroString(RECEIPT_BP_AGENT, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_BP_CUST,
					DWValues.intToLeadingZeroString(RECEIPT_BP_CUST, 2));

			externalToInternalMap.put(DWValues.RT_TYPE_PPS_AGENT,
					DWValues.intToLeadingZeroString(RECEIPT_PPS_AGENT, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_PPS_CUST,
					DWValues.intToLeadingZeroString(RECEIPT_PPS_CUST, 2));

			externalToInternalMap.put(DWValues.RT_TYPE_UBP_AGENT,
					DWValues.intToLeadingZeroString(RECEIPT_UBP_AGENT, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_UBP_CUST,
					DWValues.intToLeadingZeroString(RECEIPT_UBP_CUST, 2));

			externalToInternalMap.put(DWValues.RT_TYPE_XPAY_VALIDATION,
					DWValues.intToLeadingZeroString(RECEIPT_XPAY_CONF, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_XPAY_AGENT,
					DWValues.intToLeadingZeroString(RECEIPT_XPAY_AGENT, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_XPAY_CUST,
					DWValues.intToLeadingZeroString(RECEIPT_XPAY_CUST, 2));

			externalToInternalMap.put(DWValues.RT_TYPE_WARNING_LANG_1,
					DWValues.intToLeadingZeroString(RECEIPT_WARNING, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_WARNING_NS_LANG_1,
					DWValues.intToLeadingZeroString(RECEIPT_WARNING_NS, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_WARNING_INTL_LANG_1,
					DWValues.intToLeadingZeroString(RECEIPT_WARNING, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_WARNING_NS_INTL_LANG_1,
					DWValues.intToLeadingZeroString(RECEIPT_WARNING_NS, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_WARNING_LANG_2,
					DWValues.intToLeadingZeroString(RECEIPT_WARNING, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_WARNING_NS_LANG_2,
					DWValues.intToLeadingZeroString(RECEIPT_WARNING_NS, 2));

			externalToInternalMap.put(DWValues.RT_TYPE_REFUND_AGENT_LANG_1,
					DWValues.intToLeadingZeroString(RECEIPT_REFUND_AGENT, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_REFUND_CUSTOMER_LANG_1,
					DWValues.intToLeadingZeroString(RECEIPT_REFUND_CUST, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_REFUND_AGENT_LANG_2,
					DWValues.intToLeadingZeroString(RECEIPT_REFUND_AGENT, 2));
			externalToInternalMap.put(DWValues.RT_TYPE_REFUND_CUSTOMER_LANG_2,
					DWValues.intToLeadingZeroString(RECEIPT_REFUND_CUST, 2));
		}
			
	}

	/**
	 * Adds a receipt template to DW
	 * 
	 * @param pm_iTemplateType
	 *            - int identifying the type of receipt template being added
	 * @param pm_sLanguage
	 *            - String specifying the language of the receipt template
	 * @param pm_sFileName
	 *            - String specifying the file name of the template
	 * 
	 * @return true if the template was valid and added
	 */
	public static boolean addTemplate(int pm_iTemplateType,
			String pm_sLanguage, String pm_sVersion, String pm_sFileName) {
		/*
		 * Verify template exists
		 */
		if (MD5.fileExists(pm_sFileName)) {
			return addTemplate(pm_iTemplateType, pm_sLanguage, pm_sVersion,
					pm_sFileName, MD5.fileMD5(pm_sFileName));
		} else {
			Debug.println("addTemplate - File[" + pm_sFileName + "] Not Found");
			return false;
		}
	}

	/**
	 * Adds a receipt template to DW
	 * 
	 * @param pm_iTemplateType
	 *            - int identifying the type of receipt template being added
	 * @param pm_sLanguage
	 *            - String specifying the language of the receipt template
	 * @param pm_sFileName
	 *            - String specifying the file name of the template
	 * @param pm_sMD5
	 *            - String containing the MD5 hash for the template
	 * 
	 * @return true if the template was valid and added
	 */
	public static boolean addTemplate(int pm_iTemplateType,
			String pm_sLanguage, String pm_sVersion, String pm_sFileName,
			String pm_sMD5) {
		boolean bRetValue = false;
		String sNewFileName;

		/*
		 * Validate Template Type
		 */
		if (!bIsTemplateTypeValid(pm_iTemplateType)) {
			Debug.println("addTemplate Failure");
			return bRetValue;
		}

		/*
		 * Verify template exists
		 */
		if (!MD5.fileExists(pm_sFileName)) {
			Debug.println("addTemplate - File[" + pm_sFileName + "] Not Found");
			return bRetValue;
		}
		/*
		 * Create encrypted version of the file, if the encryption fails, then
		 * return false
		 */
		sNewFileName = sEncryptTemplate(pm_sFileName, pm_iTemplateType,
				pm_sLanguage);
		if (sNewFileName == null) {
			return false;
		}

		/*
		 * Create TemplateRecord for the template
		 */
		final TemplateRecord tr = new TemplateRecord(sNewFileName, pm_sMD5,
				pm_sLanguage, pm_sVersion);
		/*
		 * If the record is valid, then add it to the appropriate hashMap.
		 * Otherwise add a dummy record to the list so the file shows up as
		 * invalid
		 */
		if (tr.isValid() || UnitProfile.getInstance().isDemoMode()) {
			vAddRecord(hmaTemplateMapArray[pm_iTemplateType], tr);
			bRetValue = true;
		} else {
			Debug.println("File:" + sNewFileName + " Not valid");
			final TemplateRecord trDummy = new TemplateRecord(pm_sFileName,
					pm_sMD5, pm_sLanguage, "");
			vAddRecord(hmaTemplateMapArray[pm_iTemplateType], trDummy);
		}
		return bRetValue;
	}

	/**
	 * Gets the receive receipt language.
	 * 
	 * @return String containing the receive receipt language
	 */
	public static String getReceiptLanguageRecv() {
		String sRetValue = "";
		String receiptFormat = null;
		final Profile productProfile = (Profile) UnitProfile.getInstance()
				.getProfileInstance()
				.find("PRODUCT", DWValues.MONEY_GRAM_RECEIVE_DOC_SEQ);

		try {
			receiptFormat = productProfile.find("RECEIPT_FORMAT").stringValue();
		} catch (final NoSuchElementException e) {
			Debug.println("No RECEIPT_FORMAT value for Receive : ");
			return sRetValue;
		}

		if ((receiptFormat != null) && (receiptFormat.startsWith("_"))) {
			if (receiptFormat.endsWith("_intl")) {
				receiptFormat = receiptFormat.substring(0, receiptFormat.length()-5);
			} 
			if (receiptFormat.length() > 3) {
				sRetValue = receiptFormat.substring(1, 4);
			} else {
				sRetValue = DWValues.sConvertLang2ToLang3(receiptFormat
						.substring(1));
			}

		} else {
			// hybrid receipt, ensure that 2nd language is defined
			List<String> alLanguages = new ArrayList<String>(
					MoneyGramReceiveTransaction.getRecvReceiptAllLanguageList());
			if (alLanguages.size() == 1) {
				sRetValue = (String) alLanguages.get(0);
			} else {
				Iterator<String> i = alLanguages.iterator();
				while (i.hasNext()) {
					String sLan = ((String) i.next()).toUpperCase(Locale.US);
					if ((sLan != null) && (sLan.equals(DWValues.MNEM_ENG))) {
						continue;
					}
					sRetValue = DWValues.MNEM_ENG + "/" + sLan;
				}
			}
		}
		return sRetValue;
	}
	
	/**
	 * Gets the send receipt language.
	 * 
	 * @return String containing the send receipt language
	 */
	public static String getReceiptLanguageSend() {
		String sRetValue = "";
		String receiptFormat = null;
		final Profile productProfile = (Profile) UnitProfile.getInstance()
				.getProfileInstance()
				.find("PRODUCT", DWValues.MONEY_GRAM_SEND_DOC_SEQ);

		try {
			receiptFormat = productProfile.find("RECEIPT_FORMAT").stringValue();
		} catch (final NoSuchElementException e) {
			Debug.println("No RECEIPT_FORMAT value for Send : ");
			return sRetValue;
		}

		if ((receiptFormat != null) && (receiptFormat.startsWith("_"))) {
			if (receiptFormat.endsWith("_intl")) {
				receiptFormat = receiptFormat.substring(0, receiptFormat.length()-5);
			} 
			if (receiptFormat.length() > 3) {
				sRetValue = receiptFormat.substring(1, 4);
			} else {
				sRetValue = DWValues.sConvertLang2ToLang3(receiptFormat
						.substring(1));
			}

		} else {
			// hybrid receipt, ensure that 2nd language is defined
			List<String> alLanguages = new ArrayList<String>(
					MoneyGramSendTransaction.getSendReceiptAllLanguageList());
			if (alLanguages.size() == 1) {
				sRetValue = alLanguages.get(0);
			} else {
				Iterator<String> i = alLanguages.iterator();
				while (i.hasNext()) {
					String sLan = i.next().toUpperCase(Locale.US);
					if ((sLan != null)
							&& (sLan.equals(DWValues.MNEM_ENG))) {
						continue;
					}
					sRetValue = DWValues.MNEM_ENG + "/" + sLan;
				}
			}
		}
		return sRetValue;
	}

	/**
	 * Gets the warning receipt language.
	 * 
	 * @return String containing the warning receipt language
	 */
	public static String getReceiptLanguageWarning() {
		String sRetValue = "";
		String receiptFormat = null;
		final Profile productProfile = (Profile) UnitProfile.getInstance()
				.getProfileInstance()
				.find("PRODUCT", DWValues.MONEY_GRAM_RECEIVE_DOC_SEQ);

		try {
			receiptFormat = productProfile.find("RECEIPT_FORMAT").stringValue();
		} catch (final NoSuchElementException e) {
			Debug.println("No RECEIPT_FORMAT value for Warning : ");
			return sRetValue;
		}

		if ((receiptFormat != null) && (receiptFormat.startsWith("_"))) {
			if (receiptFormat.endsWith("_intl")) {
				receiptFormat = receiptFormat.substring(0, receiptFormat.length()-5);
			} 
			if (receiptFormat.length() > 3) {
				sRetValue = receiptFormat.substring(1, 4);
			} else {
				sRetValue = DWValues.sConvertLang2ToLang3(receiptFormat
						.substring(1));
			}

		} else {
			/*
			 * hybrid receipt, for now always use Spanish as the second language
			 */
			sRetValue = "ENG/SPA";
		}
		return sRetValue;
	}

	/**
	 * Returns a SAX input source for a template.
	 * 
	 * @param pm_iTemplateType
	 *            - Integer identifying a template type used to specify a
	 *            template
	 * @param pm_sLanguage
	 *            - String containing a language used to specify a template
	 * @return - String containing the file name of the receipt template for
	 *         pm_iTemplateType and pm_sLanguage
	 */
	public static InputSource getTemplateInputStream(int pm_iTemplateType,
			String pm_sLanguage) {
		/*
		 * Validate Template Type
		 */
		if (!bIsTemplateTypeValid(pm_iTemplateType)) {
			return null;
		}
		/*
		 * RTS only returns a single language per receipt, so all templates
		 * are stored with only a single language, so when getting a template
		 * just use the first language.
		 */
		final String sRecvLang = pm_sLanguage.substring(0,3);
		final HashMap<String, TemplateRecord> hmTemplateMap = hmaTemplateMapArray[pm_iTemplateType];
		final TemplateRecord tr = hmTemplateMap.get(sRecvLang);

		final InputSource isInSrc = new InputSource();

		// May be a null parameter
		if (tr == null) {
			Debug.println("Error- Receipt Template not found for type["
					+ pm_iTemplateType + "], language[" + pm_sLanguage + "]");
		} else {
			isInSrc.setByteStream(tr.getInputStream());
		}

		return isInSrc;
	}

	/**
	 * Gets a list of ReceiptTemplateInfo records for all receipt templates
	 * 
	 * @return List of ReceiptTemplateInfo records for all required receipt
	 *         templates
	 */
	public static List<ReceiptTemplateInfo> getTemplateInventory() {
		final List<ReceiptTemplateInfo> lRetList = new ArrayList<ReceiptTemplateInfo>();

		final String sWarnLang = getReceiptLanguageWarning();

		boolean mgrAuth = DeltaworksMainPanel.getMainPanel().getTranAuth(
				DWValues.MONEY_GRAM_RECEIVE_DOC_SEQ);
		boolean receiptForReceive = false;

		for (int iTType = 0; iTType < NUMBER_RECEIPT_TYPES_ALL; iTType++) {

			receiptForReceive = ((iTType >= RECEIPT_TYPE_WARNING_START) && 
					(iTType <= RECEIPT_TYPE_WARNING_END));

			if (receiptForReceive && mgrAuth) {
				if (iTType == RECEIPT_WARNING || iTType == RECEIPT_WARNING_NS) {
					lRetList.add(rtiBuildTemplateRecord(iTType, sWarnLang));
				}
			}
		}
		return lRetList;
	}

	/**
	 * Initialize ReceiptTemplate class/data
	 */
	public static void initialize() {
		ref.vInitialize(OFFSET_RECIPT_TEMPLATE_DATA, SAVE_FILE_NAME);
		try {
			hmaTemplateMapArray = (HashMap<String, TemplateRecord>[]) ref.readArray();
		} catch (DwEncryptionFileException e) {
			Debug.printException(e);
		}
		for (int ii = 0; ii < NUMBER_RECEIPT_TYPES_ALL; ii++) {
			if (hmaTemplateMapArray[ii] == null) {
				hmaTemplateMapArray[ii] = new HashMap<String, TemplateRecord>();
			}
		}
		validateTemplateLists();
	}

	/**
	 * Attempts to update ReceiptTemplate data from RTS
	 */
	public static void update(CommCompleteInterface cci, int reterivalType) {
		boolean mgrAuth = DeltaworksMainPanel.getMainPanel().getTranAuth(
				DWValues.MONEY_GRAM_RECEIVE_DOC_SEQ);
		
		if (mgrAuth) {
			MessageFacadeParam paramTemplateInfo = MessageFacade.getInstance().getTransmitParams(reterivalType);
			if (bGetTemplates(paramTemplateInfo, cci)) {
				UnitProfile.getInstance().setNeedReceiptTemplates(false);
			}
		} else {
			UnitProfile.getInstance().setNeedReceiptTemplates(false);
			/*
			 * If the caller is expecting a commComplete, then supply one
			 */
			if (cci != null) {
				cci.commComplete(MoneyGramClientTransaction.RECEIPTS_FORMAT_DETAILS, 
						Boolean.FALSE);
			}
		}
	}

	public static void update(int reterivalType) {
		update(null, reterivalType);
	}

	/**
	 * Verifies all templates and returns a list of bad templates
	 * 
	 * @return List of bad templates
	 */
	public static List<TemplateRecord> validateTemplateLists() {
		final List<TemplateRecord> lBadTemplates = new ArrayList<TemplateRecord>();

		/*
		 * For now only use templates for US agents
		 */
		if (!AgentCountryInfo.isUsAgent()) {
			return lBadTemplates;
		}

		/*
		 * Check all the template type lists
		 */
		for (int ii = 0; ii < hmaTemplateMapArray.length; ii++) {
			lBadTemplates.addAll(lValidateTemplateList(ii));
		}
		/*
		 * If we have any bad templates, we need to transmit to get new
		 * templates
		 */
		if (lBadTemplates.size() > 0) {
			DiagLog.getInstance().writeMustTransmit(
					MustTransmitDiagMsg.SECURITY_BREACH);
			UnitProfile.getInstance().setNeedReceiptTemplates(true);
		}
		return lBadTemplates;
	}

	private static boolean bGetTemplates(final MessageFacadeParam pm_mfpParameters, CommCompleteInterface pm_cci) {
		final WaitToken wait = new WaitToken();
		final CommCompleteInterface cci;
		
		if (pm_cci == null) {
			cci = new CommCompleteInterface() {
				@Override
				public void commComplete(int commTag, Object returnValue) {
					synchronized (wait) {
						wait.setWaitFlag(false);
						gotNewTemplateInfo = (Boolean) returnValue;
						wait.notify();
					}
				}
			};
		} else {
			cci = pm_cci;
		}

		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {
				final ReceiptsFormatDetailsResponse receiptFormatsResponse = MessageFacade
						.getInstance().getReceiptFormats(pm_mfpParameters);
				Boolean isSuccess = Boolean.FALSE;
				try {
					if (receiptFormatsResponse != null) {
						final List<ReceiptFormatDetailsType> lRcptList = receiptFormatsResponse.getPayload().getValue().getReceiptFormatDetails();
						final Iterator<ReceiptFormatDetailsType> itr = lRcptList.iterator();
						while (itr.hasNext()) {
							final ReceiptFormatDetailsType eRcptDetails = itr.next();
							final String sTemplate = eRcptDetails.getResponseText();
							final String sRcptType = sConvertExternalToInternalType(eRcptDetails.getReceiptText());
							final String sVersion = eRcptDetails.getVersion();
							final String sMD5 = eRcptDetails.getMd5CheckSum();
							final String sErrorCode = eRcptDetails.getErrorCode();
							final String pLangLong = eRcptDetails.getLongLanguageCode();
							
							if ((sErrorCode == null) || (sErrorCode.length() == 0)) {
								if (eRcptDetails.getAdditionalLanguages() != null
										&& eRcptDetails.getAdditionalLanguages().getLongLanguageCode() != null
										&& !eRcptDetails.getAdditionalLanguages().getLongLanguageCode().isEmpty()) {
									for (String sLangLong : eRcptDetails.getAdditionalLanguages().getLongLanguageCode()) {
										isSuccess = createTemplate(sTemplate, sRcptType, sVersion, sMD5, sLangLong);
									}
								} else if(!pLangLong.isEmpty()) {									
									isSuccess = createTemplate(sTemplate, sRcptType, sVersion, sMD5, pLangLong);
								}
							}
						}
					}
				} catch (final Exception e) {
					Debug.printException(e);
					return Boolean.FALSE;
				}

				return isSuccess;
			}

			private Boolean createTemplate(final String sTemplate, final String sRcptType, final String sVersion,
					final String sMD5, String sLangLong) {
				final String sLangShort = DWValues.sConvertLang3ToLang2(sPrimeLanguage(sLangLong));
				FileOutputStream fosFileOut = null;
				final String sTempFileName = sRcptType + sLangShort + ".xml";
				try {
					fosFileOut = new FileOutputStream(sTempFileName);
					fosFileOut.write(sTemplate.getBytes());
					fosFileOut.close();
				} catch (final Exception e) {
					Debug.printException(e);
					return Boolean.FALSE;
				}
				final int iTemplateType = Integer.parseInt(sRcptType);
				return addTemplate(iTemplateType, sLangLong, sVersion, sTempFileName, sMD5);
			}
		};

		wait.setWaitFlag(true);
		MessageFacade.run(logic, cci, MoneyGramClientTransaction.RECEIPTS_FORMAT_DETAILS, Boolean.FALSE);
		if (pm_cci == null) {
			synchronized (wait) {
				try {
					while (wait.isWaitFlag()) {
						wait.wait();
					}
				} catch (final InterruptedException ie) {
					Debug.println("ReceiptTemplates.bGetTemplates() caught InterruptedException"); 
				}
			}
			return gotNewTemplateInfo.booleanValue();
		} else {
			return true;
		}
	}

	/**
	 * @param pm_iTemplateType
	 *            - Integer identifying a template type to verify
	 * @return - true if pm_iTemplateType is a valid value for a Template Type
	 */
	private static boolean bIsTemplateTypeValid(int pm_iTemplateType) {
		boolean bRetValue = true;
		if ((pm_iTemplateType < 0)
				|| (pm_iTemplateType >= NUMBER_RECEIPT_TYPES_ALL)) {
			Debug.println("Invalid Template Type:" + pm_iTemplateType);
			bRetValue = false;
		}
		return bRetValue;
	}

	private static ReceiptTemplates getSingletonObject() {
		if (ref == null) {
			// it's ok, we can call this constructor
			ref = new ReceiptTemplates();
		}
		return ref;
	}

	private static List<TemplateRecord> lValidateTemplateList(int pm_iTemplateType) {
		final List<TemplateRecord> lBadTemplates = new ArrayList<TemplateRecord>();
		/*
		 * Get list of secondary languages, and add the primary language so all
		 * required languages are checked.
		 */
		final List<String> lLanguageList = MoneyGramSendTransaction
				.getSendReceiptAllLanguageList();
		final List<String> l2ndLanguageList = MoneyGramSendTransaction
				.getSendReceipt2ndLanguageList();

		/*
		 * Validate Template Type
		 */
		if (!bIsTemplateTypeValid(pm_iTemplateType)) {
			return lBadTemplates;
		}
		final HashMap<String, TemplateRecord> lTemplateList = hmaTemplateMapArray[pm_iTemplateType];
		final Iterator<String> i = lLanguageList.iterator();
		/*
		 * Check all languages for a Template
		 */
		while (i.hasNext()) {
			final String sLanguage = i.next();
			if (l2ndLanguageList.size() > 0
					&& lLanguageList.size() > l2ndLanguageList.size()) {
				if (!l2ndLanguageList.contains(sLanguage)) {
					// skip the hybrid ENG		
					continue;
				}
			}
			final TemplateRecord sTR = lTemplateList.get(sLanguage);

			/*
			 * If the template does not exist, add a dummy record for that
			 * language to the bad template list
			 */
			if (sTR == null) {
				Debug.println("Receipt Template Validation Failure - Template Not Found for Type["
						+ pm_iTemplateType + "], Language[" + sLanguage + "]");
				final TemplateRecord trDummy = new TemplateRecord("", "",
						sLanguage, "");
//				lBadTemplates.add(trDummy);
				/*
				 * If the template is not valid, add it to the bad template list
				 */
			} else if (!sTR.isValid()
					&& !UnitProfile.getInstance().isDemoMode()) {
				Debug.println("Receipt Template Validation Failure - Corrupted Template Found for Type["
						+ pm_iTemplateType + "], Language[" + sLanguage + "]");
//				lBadTemplates.add(sTR);
			}
		}
		return lBadTemplates;
	}

	private static ReceiptTemplateInfo rtiBuildTemplateRecord(int pm_iTType,
			String pm_sLang) {
		final ReceiptTemplateInfo rtiRec = new ReceiptTemplateInfo();
		final TemplateRecord trRec = trGetTemplate(pm_iTType, pm_sLang);
		rtiRec.setType(sConvertInternalToExternalType(pm_iTType));
		if (trRec != null) {
			String sMD5 = trRec.getMD5();
			if ((sMD5 == null) || (sMD5.length() == 0)) {
				sMD5 = "0";
			}
			rtiRec.setmD5(sMD5);

			String sVer = trRec.getsVersion();
			if ((sVer == null) || (sVer.length() == 0)) {
				sVer = "0.00";
			}
			rtiRec.setVersion(sVer);
			rtiRec.setLanguage(pm_sLang); // primary/secondary languages
		} else {
			rtiRec.setmD5("0");
			rtiRec.setVersion("0.00");
			rtiRec.setLanguage(pm_sLang); // primary/secondary languages
		}
		return rtiRec;
	}

	private static String sConvertExternalToInternalType(String pm_sExternalType) {
		String sInternalType = externalToInternalMap
				.get(pm_sExternalType);
		if (!DWValues.nonEmpty(sInternalType)) {
			Debug.dumpStack("Unsupported External Type[" + pm_sExternalType
					+ "]");
			sInternalType = "XX";
		}
		return sInternalType;
	}

	private static String sConvertInternalToExternalType(int pm_iInternalType) {
		String sRetValue = "";

		//discriminate for domestic/international sends
		boolean usAgent = AgentCountryInfo.isUsAgent();

		switch (pm_iInternalType) {
		case RECEIPT_SEND_CONF:
			if (usAgent) {
				// US send may be hybrid for a particular internal type
				sRetValue = sGetSendRcptFormat(pm_iInternalType, true);
			} else {
				// international sends
				sRetValue = sGetSendRcptFormat(pm_iInternalType);
			}
			break;
		case RECEIPT_SEND_AGENT:
			//discriminate for domestic/international sends
			if (usAgent) {
				// US send may be hybrid for a particular internal type
				sRetValue = sGetSendRcptFormat(pm_iInternalType, true);		
			} else {
				// international sends
				sRetValue = sGetSendRcptFormat(pm_iInternalType);
			}
			break;
		case RECEIPT_SEND_CUST:
			//discriminate for domestic/international sends
			if (usAgent) {
				// US send may be hybrid for a particular internal type
				sRetValue = sGetSendRcptFormat(pm_iInternalType, true);	
			} else {
				// international sends
				sRetValue = sGetSendRcptFormat(pm_iInternalType);
			}		
			break;
		case RECEIPT_REFUND_AGENT:
		case RECEIPT_REFUND_CUST:
			// discriminate for domestic/international types
			sRetValue = sGetSendRcptFormat(pm_iInternalType);
			break;

		case RECEIPT_WARNING:
			// applicable language formats & a dispenser discriminator
			sRetValue = sGetWarnRcptFormat(pm_iInternalType);
			break;
		case RECEIPT_WARNING_NS:
			// non-dispenser (also applies to dispenser with paper jam case)
			sRetValue = sGetWarnRcptFormat(pm_iInternalType);
			break;

		case RECEIPT_RECV_AGENT:
			sRetValue = sGetRecvRcptFormat(pm_iInternalType);
			break;
		case RECEIPT_RECV_CUST:
			sRetValue = sGetRecvRcptFormat(pm_iInternalType);
			break;
		case RECEIPT_BP_CONF:
			sRetValue = DWValues.RT_TYPE_BP_VALIDATION;
			break;
		case RECEIPT_BP_AGENT:
			sRetValue = DWValues.RT_TYPE_BP_AGENT;
			break;
		case RECEIPT_BP_CUST:
			sRetValue = DWValues.RT_TYPE_BP_CUST;
			break;
		case RECEIPT_PPS_AGENT:
			sRetValue = DWValues.RT_TYPE_PPS_AGENT;
			break;
		case RECEIPT_PPS_CUST:
			sRetValue = DWValues.RT_TYPE_PPS_CUST;
			break;
		case RECEIPT_UBP_AGENT:
			sRetValue = DWValues.RT_TYPE_UBP_AGENT;
			break;
		case RECEIPT_UBP_CUST:
			sRetValue = DWValues.RT_TYPE_UBP_CUST;
			break;
		case RECEIPT_XPAY_CONF:
			sRetValue = DWValues.RT_TYPE_XPAY_VALIDATION;
			break;
		case RECEIPT_XPAY_AGENT:
			sRetValue = DWValues.RT_TYPE_XPAY_AGENT;
			break;
		case RECEIPT_XPAY_CUST:
			sRetValue = DWValues.RT_TYPE_XPAY_CUST;
			break;
		default:
			Debug.dumpStack("Unsupported Internal Receipt Type["
					+ pm_iInternalType + "]");
			break;
		}
		if (sRetValue == null || sRetValue.length() == 0) {
			final Profile productProfile = (Profile) UnitProfile.getInstance()
					.getProfileInstance()
					.find("PRODUCT", DWValues.MONEY_GRAM_SEND_DOC_SEQ);
			ProfileItem profileFormat = productProfile.find("RECEIPT_FORMAT");
			Debug.dumpStack("Unsupported Receipt Format["
					+ profileFormat.stringValue() + "]");
			sRetValue = "XX";
		}
		return sRetValue;
	}

	private static String sEncryptTemplate(String pm_sFileName,
			int pm_iTemplateType, String pm_sLanguage) {
		final String sLangShort = DWValues
				.sConvertLang3ToLang2(sPrimeLanguage(pm_sLanguage));
		InputStream fisFileIn = null;
		FileOutputStream fosFileOut = null;
		final String sEncryptedFileName = DWValues.intToLeadingZeroString(
				pm_iTemplateType, 2) + sLangShort + FILE_EXTENSION;

		try {
			final File fOldFile = new File(pm_sFileName);
			if (!fOldFile.exists()) {
				Debug.println("vEncryptTemplate - File[" + pm_sFileName
						+ "] Not Found");
				return "";
			}
			fisFileIn = new FileInputStream(fOldFile);
			fosFileOut = new FileOutputStream(sEncryptedFileName);
			/*
			 * Set the cipher to encryption mode.
			 */
			ref.cipher.init(Cipher.ENCRYPT_MODE, ref.skeySpec, ref.ivspec);

			final CipherOutputStream cos = new CipherOutputStream(fosFileOut,
					ref.cipher);

			final byte[] baBuffer = new byte[8192];
			int iReadCnt = 0;
			try {
				while ((iReadCnt = fisFileIn.read(baBuffer)) > 0) {
					if (!CompileTimeFlags.rcptNoTemplateEncryption()) {
						cos.write(baBuffer, 0, iReadCnt);
					} else {
						fosFileOut.write(baBuffer, 0, iReadCnt);
					}
				}
			} catch (final IOException e) {
				Debug.printException(e);
				return null;
			} catch (final Exception e) {
				Debug.printException(e);
				return null;
			} finally {
				try {
					fisFileIn.close();
					if (!CompileTimeFlags.rcptNoTemplateEncryption()) {
						cos.flush();
						cos.close();
					} else {
						fosFileOut.flush();
						fosFileOut.close();
					}
					if (!CompileTimeFlags.rcptNoDeleteOldFile()) {
						boolean b = fOldFile.delete();
				        if (! b) {
				        	Debug.println("File " + fOldFile.getName() + " could not be deleted");
				        }
					}
				} catch (final IOException e) {
					Debug.printException(e);
					return null;
				}
			}
		} catch (final InvalidKeyException e) {
			Debug.printException(e);
			return null;
		} catch (final FileNotFoundException e) {
			Debug.printException(e);
			return null;
		} catch (InvalidAlgorithmParameterException e1) {
			Debug.printException(e1);
			return null;
		} finally {
			try {
				if (fisFileIn != null) {
					fisFileIn.close();
				}
			} catch (Exception e2) {
			}
			try {
				if (fosFileOut != null) {
					fosFileOut.close();
				}
			} catch (Exception e2) {
			}
		}
		return sEncryptedFileName;
	}

	private static String sGetSendRcptFormat(int pm_iInternalType) {
		return sGetSendRcptFormat(pm_iInternalType, false);
	}

	private static String sGetSendRcptFormat(int pm_iInternalType, boolean pm_bUsAgent) {
		String rcptFmt = "";
		ProfileItem profileFormat = null;
		final Profile productProfile = (Profile) UnitProfile.getInstance()
				.getProfileInstance()
				.find("PRODUCT", DWValues.MONEY_GRAM_SEND_DOC_SEQ);
		
		try {
			profileFormat = productProfile.find("RECEIPT_FORMAT");
		} catch (final NoSuchElementException e) {
			Debug.println("No RECEIPT_FORMAT value for Send : ");
			return null;
		}

		if ((profileFormat != null)
				&& (profileFormat.stringValue().startsWith("_"))) {
			if (profileFormat.stringValue().endsWith("intl")) {
				
				switch (pm_iInternalType) {
				case RECEIPT_SEND_CONF:
					if (!pm_bUsAgent) {
						rcptFmt = DWValues.RT_TYPE_INTL_SEND_PREPAY_INTL_LANG_1;
					} else {
						// DF single language does not have an Intl fmt template
						rcptFmt = DWValues.RT_TYPE_SEND_VALIDATION;
					}
					break;

				case RECEIPT_SEND_AGENT:
					if (!pm_bUsAgent) {
						rcptFmt = DWValues.RT_TYPE_INTL_SEND_AGENT_INTL_LANG_1;
					} else {
						// DF single language does not have an Intl fmt template
						rcptFmt = DWValues.RT_TYPE_SEND_AGENT;
					}
					break;

				case RECEIPT_SEND_CUST:
					if (!pm_bUsAgent) {
						rcptFmt = DWValues.RT_TYPE_INTL_SEND_CUSTOMER_INTL_LANG_1;
					} else {
						// DF single language does not have an Intl fmt template
						rcptFmt = DWValues.RT_TYPE_SEND_CUSTOMER;
					}
					break;

				case RECEIPT_REFUND_AGENT:
					// no int'l format for reversal/refund receipt; use std fmt
					rcptFmt = DWValues.RT_TYPE_REFUND_AGENT_LANG_1;
					break;

				case RECEIPT_REFUND_CUST:
					// no int'l format for reversal/refund receipt; use std fmt
					rcptFmt = DWValues.RT_TYPE_REFUND_CUSTOMER_LANG_1;
					break;

				default:
					Debug.dumpStack("Unsupported Internal Receipt Type["
							+ pm_iInternalType + "] for international");
					break;
				}
			} else {
				switch (pm_iInternalType) {
				case RECEIPT_SEND_CONF:
					rcptFmt = (pm_bUsAgent ? DWValues.RT_TYPE_SEND_VALIDATION
							: DWValues.RT_TYPE_INTL_SEND_PREPAY_NODF_LANG_1);
					break;

				case RECEIPT_SEND_AGENT:
					rcptFmt = (pm_bUsAgent ? DWValues.RT_TYPE_SEND_AGENT
							: DWValues.RT_TYPE_INTL_SEND_AGENT_NODF_LANG_1);
					break;

				case RECEIPT_SEND_CUST:
					rcptFmt = (pm_bUsAgent ? DWValues.RT_TYPE_SEND_CUSTOMER
							: DWValues.RT_TYPE_INTL_SEND_CUSTOMER_NODF_LANG_1);
					break;

				case RECEIPT_REFUND_AGENT:
					rcptFmt = DWValues.RT_TYPE_REFUND_AGENT_LANG_1;
					break;

				case RECEIPT_REFUND_CUST:
					rcptFmt = DWValues.RT_TYPE_REFUND_CUSTOMER_LANG_1;
					break;

				default:
					Debug.dumpStack("Unsupported Internal Receipt Type["
							+ pm_iInternalType + "] for No Dodd_Frank");
					break;
				}
		
			}
		} else if ((profileFormat != null)
				&& (profileFormat.stringValue().startsWith("mgs"))) {
			/*
			 * special check in case hybrid receipt format but no secondary
			 * language listed in the Receipt Language list
			 */
			List<String> additionalLanguages = MoneyGramSendTransaction
					.getSendReceipt2ndLanguageList();
			int listSize = additionalLanguages.size();
	
			if (profileFormat.stringValue().endsWith("us49")) {
				
				switch (pm_iInternalType) {
				case RECEIPT_SEND_CONF:
					if (!pm_bUsAgent) {
						// no-DF hybrid template discriminator only
						rcptFmt = DWValues.RT_TYPE_INTL_SEND_US_PREPAY_LANG_2;
					} else {
						// DF hybrid template applicable to all states
						rcptFmt = listSize > 0 ? DWValues.RT_TYPE_SEND_VALIDATION_LANG_2
								: DWValues.RT_TYPE_SEND_VALIDATION;
					}
					break;

				case RECEIPT_SEND_AGENT:
					if (!pm_bUsAgent) {
						// no-DF hybrid template discriminator only
						rcptFmt = DWValues.RT_TYPE_INTL_SEND_US_AGENT_LANG_2;
					} else {
						// DF hybrid template applicable to all states
						rcptFmt = listSize > 0 ? DWValues.RT_TYPE_SEND_AGENT_LANG_2
								: DWValues.RT_TYPE_SEND_AGENT;
					}
					break;

				case RECEIPT_SEND_CUST:
					if (!pm_bUsAgent) {
						// no-DF hybrid template discriminator only
						rcptFmt = DWValues.RT_TYPE_INTL_SEND_US_CUSTOMER_LANG_2;
					} else {
						// DF hybrid template applicable to all states
						rcptFmt = listSize > 0 ? DWValues.RT_TYPE_SEND_CUSTOMER_LANG_2
								: DWValues.RT_TYPE_SEND_CUSTOMER;
					}
					break;

				case RECEIPT_REFUND_AGENT:
					rcptFmt = DWValues.RT_TYPE_REFUND_AGENT_LANG_2;
					break;

				case RECEIPT_REFUND_CUST:
					rcptFmt = DWValues.RT_TYPE_REFUND_CUSTOMER_LANG_2;
					break;
				default:
					Debug.dumpStack("Unsupported Internal Receipt Type["
							+ pm_iInternalType + "] for US Hybrid");
					break;
				}
			} else 	if (profileFormat.stringValue().endsWith("usca")) {

				switch (pm_iInternalType) {
				case RECEIPT_SEND_CONF:
					if (!pm_bUsAgent) {
						// no-DF hybrid template discriminator only
						rcptFmt = DWValues.RT_TYPE_INTL_SEND_CA_PREPAY_LANG_2;
					} else {
						// DF hybrid template applicable to all states
						rcptFmt = listSize > 0 ? DWValues.RT_TYPE_SEND_VALIDATION_LANG_2
								: DWValues.RT_TYPE_SEND_VALIDATION;
					}
					break;

				case RECEIPT_SEND_AGENT:
					if (!pm_bUsAgent) {
						// no-DF hybrid template discriminator only
						rcptFmt = DWValues.RT_TYPE_INTL_SEND_CA_AGENT_LANG_2;
					} else {
						// DF hybrid template applicable to all states
						rcptFmt = listSize > 0 ? DWValues.RT_TYPE_SEND_AGENT_LANG_2
								: DWValues.RT_TYPE_SEND_AGENT;
					}
					break;

				case RECEIPT_SEND_CUST:
					if (!pm_bUsAgent) {
						// no-DF hybrid template discriminator only
						rcptFmt = DWValues.RT_TYPE_INTL_SEND_CA_CUSTOMER_LANG_2;
					} else {
						// DF hybrid language does not have an state template
						rcptFmt = listSize > 0 ? DWValues.RT_TYPE_SEND_CUSTOMER_LANG_2
								: DWValues.RT_TYPE_SEND_CUSTOMER;
					}
					break;

				case RECEIPT_REFUND_AGENT:
					// no CA format for reversal/refund receipt; use hybrid fmt
					rcptFmt = DWValues.RT_TYPE_REFUND_AGENT_LANG_2;
					break;

				case RECEIPT_REFUND_CUST:
					// no CA format for reversal/refund receipt; use hybrid fmt
					rcptFmt = DWValues.RT_TYPE_REFUND_CUSTOMER_LANG_2;
					break;

				default:
					Debug.dumpStack("Unsupported Internal Receipt Type["
							+ pm_iInternalType + "] for CA Hybrid");
					break;
				}
			}
		}
		return rcptFmt;
	}
	
	private static String sGetRecvRcptFormat(int pm_iInternalType) {
		String finalReceiptFormat = "";
		boolean isAgentRcpt = (pm_iInternalType == RECEIPT_RECV_AGENT);
		boolean isCustRcpt = (pm_iInternalType == RECEIPT_RECV_CUST);
		boolean isWarningRcpt = (pm_iInternalType == RECEIPT_WARNING)
				|| (pm_iInternalType == RECEIPT_WARNING_NS);
		ProfileItem profileFormat = null;
		final Profile productProfile = (Profile) UnitProfile.getInstance()
				.getProfileInstance()
				.find("PRODUCT", DWValues.MONEY_GRAM_RECEIVE_DOC_SEQ);

		try {
			profileFormat = productProfile.find("RECEIPT_FORMAT");
		} catch (final NoSuchElementException e) {
			Debug.println("No RECEIPT_FORMAT value for Receive : ");
			return null;
		}

		if ((profileFormat != null)
				&& (profileFormat.stringValue().startsWith("_"))) {
			if (profileFormat.stringValue().endsWith("intl")) {
				if (isAgentRcpt) {
					finalReceiptFormat = DWValues.RT_TYPE_RECV_AGENT_LANG_1_INTL;
				} else if (isCustRcpt) {
					finalReceiptFormat = DWValues.RT_TYPE_RECV_CUSTOMER_LANG_1_INTL;
				} else if (isWarningRcpt) {
					// add dispenser discriminator
					if ("DG".equals(UnitProfile.getInstance().get("MG_CHECKS",
							"DG"))) {
						finalReceiptFormat = DWValues.RT_TYPE_WARNING_INTL_LANG_1;
					} else {
						finalReceiptFormat = DWValues.RT_TYPE_WARNING_NS_INTL_LANG_1;
					}
				}
			} else {
				if (isAgentRcpt) {
					finalReceiptFormat = DWValues.RT_TYPE_RECV_AGENT_LANG_1;
				} else if (isCustRcpt) {
					finalReceiptFormat = DWValues.RT_TYPE_RECV_CUSTOMER_LANG_1;
				} else if (isWarningRcpt) {
					// discriminate on warning type
					if ("DG".equals(UnitProfile.getInstance().get("MG_CHECKS",
							"DG"))) {
						finalReceiptFormat = DWValues.RT_TYPE_WARNING_LANG_1;
					} else {
						finalReceiptFormat = DWValues.RT_TYPE_WARNING_NS_LANG_1;
					}
				}
			}
		} else {
			if (isAgentRcpt) {
				finalReceiptFormat = DWValues.RT_TYPE_RECV_AGENT_LANG_2;
			} else if (isCustRcpt) {
				finalReceiptFormat = DWValues.RT_TYPE_RECV_CUSTOMER_LANG_2;
			} else {
				// discriminate on warning type
				if (pm_iInternalType == RECEIPT_WARNING) {
					// add dispenser discriminator
					if ("DG".equals(UnitProfile.getInstance().get("MG_CHECKS", "DG"))) {
						finalReceiptFormat = DWValues.RT_TYPE_WARNING_LANG_2;
					} else {
						finalReceiptFormat = DWValues.RT_TYPE_WARNING_NS_LANG_2;
					}
				}
			}
		}
		return finalReceiptFormat;
	}

	private static String sGetWarnRcptFormat(int pm_iInternalType) {
		String finalReceiptFormat = "";
		ProfileItem profileFormat = null;
		final Profile productProfile = (Profile) UnitProfile.getInstance()
				.getProfileInstance()
				.find("PRODUCT", DWValues.MONEY_GRAM_RECEIVE_DOC_SEQ);

		try {
			profileFormat = productProfile.find("RECEIPT_FORMAT");
		} catch (final NoSuchElementException e) {
			Debug.println("No RECEIPT_FORMAT value for Receive : ");
			return null;
		}

		if ((profileFormat != null)
				&& (profileFormat.stringValue().startsWith("_"))) {
			if (profileFormat.stringValue().endsWith("intl")) {
				if (pm_iInternalType == RECEIPT_WARNING_NS) {
					// for either no-dispenser or dispenser with paper jam
					finalReceiptFormat = DWValues.RT_TYPE_WARNING_NS_INTL_LANG_1;
				} else {
					finalReceiptFormat = DWValues.RT_TYPE_WARNING_INTL_LANG_1;					
				}
			} else {
				if (pm_iInternalType == RECEIPT_WARNING_NS) {
					// for either no-dispenser or dispenser with paper jam
					finalReceiptFormat = DWValues.RT_TYPE_WARNING_NS_LANG_1;
				} else {
					finalReceiptFormat = DWValues.RT_TYPE_WARNING_LANG_1;					
				}
			}
		} else {
			// hybrid receipt - no dispenser discriminator
			if (pm_iInternalType == RECEIPT_WARNING_NS) {
				finalReceiptFormat = DWValues.RT_TYPE_WARNING_NS_LANG_2;
			} else {
				finalReceiptFormat = DWValues.RT_TYPE_WARNING_LANG_2;				
			}
		}
		return finalReceiptFormat;
	}

	private static String sPrimeLanguage(String pm_sLang) {
		String sRetValue = pm_sLang;
		final int iDivider = sRetValue.indexOf('/');
		if (iDivider > 0) {
			sRetValue = sRetValue.substring(0, iDivider);
		}
		return sRetValue;
	}

	/**
	 * Returns the Receipt Template file name for the specified template type
	 * and language.
	 * 
	 * @param pm_iTemplateType
	 *            - Integer identifying a template type used to specify a
	 *            template
	 * @param pm_sLanguage
	 *            - String containing a language used to specify a template
	 * @return - String containing the version of the specified template
	 */
	private static TemplateRecord trGetTemplate(int pm_iTemplateType,
			String pm_sLanguage) {
		TemplateRecord trRetValue = null;
		/*
		 * RTS only returns a single language per receipt, so all templates
		 * are stored with only a single language, so when getting a template
		 * if more than 1 language is returned, just use the second language
		 * (assume the first language is English for now).
		 */
		String sRcptLang = pm_sLanguage;
		if (sRcptLang.length() > 4) {
			sRcptLang = sRcptLang.substring(4);
		}
		/*
		 * Validate Template Type
		 */
		if (!bIsTemplateTypeValid(pm_iTemplateType)) {
			return trRetValue;
		}
		/*
		 * get the template map for the specified template type
		 */
		final HashMap<String, TemplateRecord> hmTemplateMap = hmaTemplateMapArray[pm_iTemplateType];
		/*
		 * Find the template for specified language
		 */
		trRetValue = hmTemplateMap.get(sRcptLang);

		return trRetValue;
	}

	/**
	 * Add a TemplateRecord to a list of TemplateRecords
	 * 
	 * @param pm_lTemplateList
	 *            - List of TemplateRecords
	 * @param pm_tr
	 *            - TemplateRecord to add to list
	 */
	private static void vAddRecord(HashMap<String, TemplateRecord> pm_lTemplateList,
			TemplateRecord pm_tr) {
		/*
		 * Add the record to the specified hash map
		 */
		try {
			pm_lTemplateList.put(pm_tr.getsLanguage(), pm_tr);
			ref.vWriteSaveFile(hmaTemplateMapArray);
		} catch (Exception e) {
			Debug.printException(e);
		}
	}

	private ReceiptTemplates() {
		// no code req'd
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
		// that'll teach 'em
	}

}
