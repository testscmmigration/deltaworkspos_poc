package dw.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import dw.io.DiagLog;
import dw.io.State;
import dw.io.diag.MustTransmitDiagMsg;
import dw.main.DeltaworksMainPanel;
import dw.profile.ClientInfo;
import dw.profile.UnitProfile;
import flags.CompileTimeFlags;

/**
 * Base Class for reading/writing AES encrypted DW files
 *
 * @author w156
 */
public class DwEncryptionAES {

	/*
	 * Common location for offset values. Allows for seeing what has already
	 * been used and where.
	 */
    // Advanced Encryption Standard, Cipher Block Chaining mode, and padding
    
	private static final String CIPHER_KEY_SPECIFICATION = "AES/CBC/PKCS5Padding";
	private static final String SECRET_KEY_SPECIFICATION = "AES";
	
    protected static final int 	OFFSET_STATE_REG_DATA 			= 0x37;
	protected static final int 	OFFSET_RECEIPT_DATA 			= 0x5B;
	protected static final int 	OFFSET_PDF_DATA 				= 0x6D;
	public static final int 	OFFSET_ADP_DATA 				= 0x9E;
	public static final int 	OFFSET_BM_DATA 					= 0xA3;
	public static final int 	OFFSET_CI_DATA 					= 0xC5;
	public static final int 	OFFSET_PRT_DATA 				= 0x76;
	public static final int 	OFFSET_CUR_DATA 				= 0xB9;
	protected static final int 	OFFSET_RECIPT_TEMPLATE_DATA 	= 0xDA;
	public static final int 	OFFSET_CACHED_AC_RESPONSES		= 0xEC;
	
	protected SecretKeySpec skeySpec = null;
	protected IvParameterSpec ivspec = null;
	protected Cipher cipher = null;
	protected String sEncryptedFileName;

	private static class DwEncryptionData implements Serializable {
		private static final long serialVersionUID = 1L;
		private String version;
		private Object[] objects;
	}
	
	public class DwEncryptionFileException extends Exception {
		private static final long serialVersionUID = 1L;
		
		private DwEncryptionFileException(String message) {
			super(message);
		}
		
		private DwEncryptionFileException(Throwable t) {
			super(t.getMessage(), t);
		}
	}

	/**
	 * Reads an object from an encrypted save file
	 *
	 * @param pm_oData
	 *            object to read data into
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws InvalidAlgorithmParameterException
	 * @throws InvalidKeyException
	 *
	 */
	
	public Object readObject() throws DwEncryptionFileException {
		Object[] objectArray = readArray();
		return objectArray[0];
	}
	
	/**
	 * Reads an array of objects from an encrypted save file
	 *
	 * @param pm_oData
	 *            array of objects to read data into
	 * @throws InvalidAlgorithmParameterException
	 * @throws InvalidKeyException
	 * @throws IOException
	 */
	protected Object[] readArray() throws DwEncryptionFileException {
		FileInputStream fisFileIn = null;
		ObjectInputStream ois = null;

		File file = new File(sEncryptedFileName);
		try {
			fisFileIn = new FileInputStream(file);
			/*
			 * Set the cipher to decryption mode.
			 */
			if (! CompileTimeFlags.rcptNoTemplateEncryption()) {
				try {
					cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivspec);
					CipherInputStream cis = new CipherInputStream(fisFileIn, cipher);
					ois = new ObjectInputStream(cis);
				} catch (Exception e1) {
					throw e1;
				}
			} else {
				ois = new ObjectInputStream(fisFileIn);
			}
			
			DwEncryptionData data = null;
			
			ExtraDebug.println("Read file " + sEncryptedFileName + "...");
			data = (DwEncryptionData) ois.readObject();
			String version = UnitProfile.getInstance().getVersionNumber(false);
			if (! version.equals(data.version)) {
				deleteFile(file, fisFileIn, ois);
				throw new DwEncryptionFileException("Version Mismatch: Expected: " + version + ", Actual: " + data.version);
			}
			return data.objects;

		} catch (DwEncryptionFileException e) {
			throw e;
		} catch (FileNotFoundException e) {
			Debug.printException(e);
			throw new DwEncryptionFileException(e);
		} catch (Exception e) {
			Debug.printException(e);
			deleteFile(file, fisFileIn, ois);
			throw new DwEncryptionFileException(e);
		} finally {
			try {
				if (ois != null) {
					ois.close();
				}
			} catch (IOException e) {
				Debug.println("Could not close file " + sEncryptedFileName);
			}
			try {
				if (fisFileIn != null) {
					fisFileIn.close();
				}
			} catch (IOException e) {
				Debug.println("Could not close file " + sEncryptedFileName);
			}
		}
	}
	
	public void deleteFile(File file, FileInputStream fis, ObjectInputStream ois) {
		if (! file.exists()) {
			return;
		}
		if (fis != null) {
			try {
				fis.close();
			} catch (Exception e) {
				Debug.println("File Input Stream for file " + file.getName() + " could not be closed");
				Debug.printException(e);
			}
		}

		if (ois != null) {
			try {
				ois.close();
			} catch (Exception e) {
				Debug.println("Object Input Stream for file " + file.getName() + " could not be closed");
				Debug.printException(e);
			}
		}
        
		DiagLog.getInstance().writeMustTransmit(MustTransmitDiagMsg.AWOL_LIMIT);
		UnitProfile.getInstance().setForceTransmit(true);

		try {
			boolean b = file.delete();
	        if (b) {
	        	Debug.println("File " + file.getName() + " deleted");
	        } else {
	        	Debug.println("File " + file.getName() + " will be deleted on exit");
	        	file.deleteOnExit();
	        	Thread t = new Thread() {
					@Override
					public void run() {
			        	DeltaworksMainPanel.getMainPanel().showRestartWarning("dialogs/DialogConfigurationRestart.xml"); 
			        	DeltaworksMainPanel.getMainPanel().tryExit(DeltaworksMainPanel.RESTART_EXIT);
					}
	        	};
	        	t.start();
	        }
		} catch (Exception e) {
			Debug.println("File " + file.getName() + " could not be deleted");
			Debug.printException(e);
		}
	}

	/**
	 * Initialize cipher and secret key spec for the data file
	 *
	 * @param pm_bOffset
	 *            - offset value used to vary key
	 * @param pm_sFileName
	 *            - file name for data file
	 */
	public void vInitialize(int pm_bOffset, String pm_sFileName) {
		
		sEncryptedFileName = pm_sFileName;

		// create the secret key specification using our key

		byte[] key = this.getKey(pm_bOffset);
		skeySpec = new SecretKeySpec(key, SECRET_KEY_SPECIFICATION);
		ivspec = new IvParameterSpec(key);

		// Instantiate the cipher

		try {
			cipher = Cipher.getInstance(CIPHER_KEY_SPECIFICATION);
		} catch (final NoSuchAlgorithmException e) {
			Debug.printException(e);
		} catch (final NoSuchPaddingException e) {
			Debug.printException(e);
		}
	}
	
	private byte[] getKey(long offset) {
		
		byte[] key = null;
		
		ClientInfo clientInfo = new ClientInfo();
		long deviceId = 0L;
		long profileId = 0L;
		
		try {
			deviceId = Long.parseLong(clientInfo.getUnitNumber());
		} catch (Exception e) {
			deviceId = State.getInstance().readLong(State.DEVICE_ID);
		}
		
		try {
			profileId = Long.parseLong(clientInfo.getProfileID());
		} catch (Exception e) {
			profileId = State.getInstance().readLong(State.PROFILE_ID);
		}
		
		long seed = 0L;
		try {
			long mask = offset | (offset << 8) | (offset << 16) | (offset << 24) | (offset << 32) | (offset << 40) | (offset << 48) | (offset << 56);
			seed = (deviceId | (profileId << 40)) ^ mask;
		} catch (Exception e) {
		}

		Random random = new Random(seed);
		key = new byte[16];
		random.nextBytes(key);

		return key;
	}

	/**
	 * Writes an object to an encrypted save file
	 *
	 * @param pm_oData
	 *            object to write to file
	 */
	public void vWriteSaveFile(Object pm_oData) throws DwEncryptionFileException {
		ExtraDebug.println("Writing file " + sEncryptedFileName + "...");
		final Object[] objectArray = new Object[1];
		objectArray[0] = pm_oData;
		vWriteSaveFile(objectArray);
	}

	/**
	 * Writes an array of objects to an encrypted save file
	 *
	 * @param pm_oData
	 *            array of objects to write to file
	 */
	protected void vWriteSaveFile(Object[] pm_oData) throws DwEncryptionFileException {
		FileOutputStream fosFileOut = null;
		ObjectOutputStream oos = null;

		try {
			fosFileOut = new FileOutputStream(sEncryptedFileName);
			/*
			 * Set the cipher to encryption mode.
			 */
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivspec);
			final CipherOutputStream cos = new CipherOutputStream(fosFileOut, cipher);
			if (! CompileTimeFlags.rcptNoTemplateEncryption()) {
				oos = new ObjectOutputStream(cos);
			} else {
				oos = new ObjectOutputStream(fosFileOut);
			}
			
			DwEncryptionData data = new DwEncryptionData();
			data.version = UnitProfile.getInstance().getVersionNumber(false);
			data.objects = pm_oData;
			
			oos.writeObject(data);

		} catch (Exception e) {
			Debug.printException(e);
			throw new DwEncryptionFileException(e);
		} finally {
			try {
				if (oos != null) {
					oos.flush();
					oos.close();
				}
			} catch (IOException e7) {
				Debug.printException(e7);
				Debug.println("Could not close file " + sEncryptedFileName);
			}
			try {
				if (fosFileOut != null) {
					fosFileOut.flush();
					fosFileOut.close();
				}
			} catch (IOException e7) {
				Debug.printException(e7);
				Debug.println("Could not close file " + sEncryptedFileName);
			}
		}
	}
}
