/*
 * StringUtility.java
 * 
 * $Revision$
 *
 * Copyright (c) 2000-2005 MoneyGram International
 */

package dw.utility;

import java.math.BigDecimal;

/**
 * StringUtility
 * 
 * Some handy and not-so-handy string handling methods.
 */
public class StringUtility {

    public static String getNonNullString(final String s,
            final String nullVersion) {
        if (s == null)
            return nullVersion;
        else
            return s;
    }

    public static String getNonNullString(final String s) {
        return getNonNullString(s, "");
    }

    public static BigDecimal stringToBigDecimal(String s) {
        if ((s == null) || (s.length() == 0)) {
            return BigDecimal.ZERO;
        }
        else {
            return new BigDecimal(s);
        }
    }
    
    public static String stringToMaskedString(String value) {
    	if (value == null) {
    		return "********";
    	}
    	if (value.length() < 4) {
    		while (value.length() < 4) {
    			value = "*" + value;
    		}
    	}
        return "****" + value.substring(value.length()-4);
    }
}
