package dw.utility;

import java.io.FileOutputStream;
import java.io.IOException;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

// Java Base64 Encoder / Java Base64 Decoder Example

public class Base64 {
	
    private static BASE64Decoder decoder = new BASE64Decoder();
    private static BASE64Encoder encoder = new BASE64Encoder();
    
	public static byte[] decodedBase64Item(String sEncoded) {
		byte[] baDecodedBytes = null;
		try {
			baDecodedBytes = decoder.decodeBuffer(sEncoded);
		} catch (IOException e) {
			Debug.printException(e);
			baDecodedBytes = null;
		}
		return baDecodedBytes;
	}

	public static String encodeBase64Item(byte[] baDecodedBytes) {
		String sEncodedString =  encoder.encodeBuffer(baDecodedBytes);
		return sEncodedString;
	}
	
	public static boolean writeBase64ItemAsFile(String sEncoded, String sFileName) {
		boolean bRetValue = true;
		if (sEncoded == null) {
			Debug.println("writeBase64ItemAsFile: No item to decode");
			bRetValue = false;
		}
		if (sFileName == null) {
			Debug.println("writeBase64ItemAsFile: No filename supplied");
			bRetValue = false;
		}
		if (bRetValue) {
			try { 
				FileOutputStream fosFileOut = new FileOutputStream(sFileName);
				fosFileOut.write(decodedBase64Item(sEncoded));
				fosFileOut.close();
			} catch (final Exception e) {
				Debug.printException(e);
				bRetValue = false;
			}
		}
		return bRetValue;
	}
}