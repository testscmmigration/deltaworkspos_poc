package dw.utility;

final class Counter {
    int count = 1;
    
    void increment() {
        ++count;
    }

    @Override
	public String toString() {
        return Integer.toString(count);
    }
}

