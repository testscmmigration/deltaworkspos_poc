/*
 * Debug.java
 * 
 * $Revision$
 *
 * Copyright (c) 2000-2004 MoneyGram International
 */

package dw.utility;

/**
 * Centralized debugging facility.
 * @author Eric Inman
 */
public final class ExtraDebug {
    /** Toggle for turning debugging on/off */
    private static boolean debug = true;
    
    /**
     * Prints a string, without a newline character appended to the end, to
     * standard output.
     * @param s A debugging string.
     * @author Eric Inman
     */
    public static void print(final String s) {
        if (debug)
            System.out.print(s);
    }

    /**
     * Prints a string, with a newline character appended to the end, to
     * standard output. Information of special interest to customer support
     * start with [, =, or -. Other lines are indented with a tab.
     * @param s A debugging string.
     * @author Eric Inman (original)
     * @author Steve Steele (tab logic)
     * @author Geoff Atkin (new tab logic)
     */

    public static void println(final String s) {
        if (debug) {
        	Debug.println(s);
        }
    }

    /**
     * Prints the stack trace of the Throwable object.
     * @param t A Throwable object.
     * @author Eric Inman
     */
    public static void printStackTrace(final Throwable t) {
        System.out.flush();
        if (debug)
        	Debug.printStackTrace(t);
    }

    /**
     * Dumps the stack trace for the current thread.
     * @author Eric Inman
     */
    public static void dumpStack() {
        if (debug)
        	Debug.dumpStack();
    }

    /** Dumps the stack following a debug line. */
    public static void dumpStack(String s) {
        if (debug)
        	Debug.dumpStack(s);
    }
    
    /**
     * Mutator for the debug class variable.
     * @param newDebugFlag A boolean stating whether debug mode
     * is toggled on/off.
     * @author Chris Bartling
     */
    public static void setDebug(boolean newDebugFlag) {
        debug = newDebugFlag;
    }

    /**
     * Accessor for the debug class variable.
     * @return A boolean stating whether debug mode
     * is toggled on/off.
     * @author Chris Bartling
     */
    public static boolean isDebug() {
        return debug;
    }
}
