package dw.utility;

import java.io.InputStream;
import java.io.Serializable;

import flags.CompileTimeFlags;

/**
 * Provides a class for pairing file names and expected MD5 values.
 * 
 * @author John Lesko (W156)
 */
public class MD5fileData implements Serializable {
	private static final long serialVersionUID = 1L;

	public interface ReadMethodInterface {
		InputStream getInputStream(String pm_sFileName);
	}

	private final String sFileName;
	private String sExpectedMD5;

	ReadMethodInterface rmi;

	MD5fileData(String pm_sFileName, String pm_sExpectedMD5,
			ReadMethodInterface pm_rmi) {
		sFileName = pm_sFileName;
		if (pm_sExpectedMD5.length() > 0) {
			sExpectedMD5 = pm_sExpectedMD5;
		} else {
			sExpectedMD5 = "-1";
		}
		rmi = pm_rmi;
	}

	/**
	 * Gets the File Name of the MD5 File Data record
	 * 
	 * @return String containing the File Name
	 */
	public String getFileName() {
		return sFileName;
	}

	/**
	 * Gets the InputStream of the MD5 File Data record
	 * 
	 * @return InputStream for reading the file associated with the MD5 File
	 *         Data record.
	 */
	public InputStream getInputStream() {
		return rmi.getInputStream(sFileName);
	}
	
	public String getMD5() {
		if (CompileTimeFlags.rcptNoFileValidation()) {
			return sExpectedMD5;
		} else {
			return DWValues.stripSpaces(MD5.streamMD5(rmi.getInputStream(sFileName)));
		}
	}

	/**
	 * Returns true is the record's associated file is valid
	 * 
	 * @return true if the file's calculated MD5 matches its expected MD5
	 */
	public boolean isValid() {
		if (rmi == null) {
			return MD5.validateFile(sFileName, sExpectedMD5);
		} else {
			if (MD5.fileExists(sFileName)) {
				return MD5.validateFile(rmi.getInputStream(sFileName),
						sFileName, sExpectedMD5);
			} else {
				Debug.println("MD5fileData.isValid - File does not exist '"
						+ sFileName + "'");
				return false;
			}
		}
	}

}
