package dw.utility;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingUtilities;
import javax.swing.Timer;

import dw.accountdepositpartners.AccountDepositPartnersTransactionInterface;
import dw.agentdirectory.AgentDirectoryTransactionInterface;
import dw.broadcastmessages.BroadcastMessagesTransactionInterface;
import dw.multiagent.MultiAgentTransactionInterface;
import dw.profile.UnitProfile;
import dw.reprintreceipt.ReprintReceiptTransactionInterface;
import dw.simplepm.SimplePMTransactionInterface;

/**
 * Utility class that allows the GUI to check in at arbitrary action points to
 *   reset the timer. If the timer has been started and there is not a check-in
 *   for the backout time, the backout callback will be fired to cancel the
 *   transaction.
 */
public class IdleBackoutTimer {
    private static int tickTime = 3000; // default...check every 3 seconds.
    private static long longTimeout = 540; // default to 9 minutes  
    private static long shortTimeout = 180; //default to 3 minutes SAK
    private static IdleListener currentLongListener;
    private static IdleListener currentShortListener;
    private static boolean running = false;
    private static Timer timer = null;
    
    /**
     * Create the timer.
     */
    static {
        timer = new Timer(tickTime, new ActionListener() {
             @Override
			public synchronized void actionPerformed(ActionEvent e) {
                 
                 if (! running)
                     return;
                  
                 if (! currentShortListener.isShortExpired()) {                   
                     SwingUtilities.invokeLater(new Runnable() {@Override
					public void run() {
                         currentShortListener.updateShort();
                     }});               
                 }
                
                 if (currentLongListener.isLongExpired()) {
                     stopLong();
                 }
                 else { 
                    currentLongListener.updateLong();
                 }
             }});
    }

    public static boolean isRunning() {
        return running;
    }

    /**
     * Start the timer, supplying it with the given listener.
     */
    public static void start(IdleTimeoutListener listener) {
        setBackoutTimeSeconds(UnitProfile.getInstance().get("LONG_TIMEOUT", 540),
                              UnitProfile.getInstance().get("SHORT_TIMEOUT", 120));
        currentLongListener = new IdleListener(listener, longTimeout);
        currentShortListener = new IdleListener(listener, shortTimeout);
        timer.start();
        running = true;
    }

    /**
     * Start the timer.
     */
    public static void start() {
        checkIn();
        running = true;
    }

    /**
     * Pause the timer.
     */
    public static void pause() {
        running = false;
    }

    /**
     * Stop the timer.
     */
    public static void stopLong() {
        currentLongListener = null;
        currentShortListener = null;
        timer.stop();
        running = false;
    }
    

    /**
     * Set the rate at which the timer will poll for idle time.
     */
    public static void setTickTime(int seconds) {
        tickTime = seconds * 1000;
    }

    /**
     * Set the number of minutes that a transaction is allowed to remain idle.
     */
    public static void setBackoutTimeSeconds(int longSeconds, int shortSeconds) {
        

        if (longSeconds <= shortSeconds)
           shortSeconds = 0;
           
        longTimeout = longSeconds * 1000;
        shortTimeout = shortSeconds * 1000;
    }

    public static synchronized void checkIn() {
        if (currentLongListener == null)
            return;
        currentLongListener.reset();
        currentShortListener.reset();
    }

    public static void resetExpiredShort() {
        currentShortListener.setShortExpired(false);
    }


    private static class IdleListener {
        private IdleTimeoutListener listener;
        private long interval;
        private long expirationTime;
        private boolean longExpired = false;
        private boolean shortExpired = false;
        private boolean loginRequired = true;

        public IdleListener(IdleTimeoutListener listener, long interval) {
            this.interval = interval;
            this.listener = listener;
           
            if (listener instanceof SimplePMTransactionInterface) {
               loginRequired = true;
            } else if (listener instanceof AgentDirectoryTransactionInterface) {
            	loginRequired = false;                   
            } else if (listener instanceof ReprintReceiptTransactionInterface) {
                	loginRequired = false;                   
            } else if (listener instanceof MultiAgentTransactionInterface) {
                    loginRequired = true;        
            } else if (listener instanceof AccountDepositPartnersTransactionInterface) {
                loginRequired = false;                   
            } else if (listener instanceof BroadcastMessagesTransactionInterface) {
                loginRequired = false;                   
            } else if (listener.getNamePinRequiredValue().equals("N")) {
                   loginRequired = false;
            } else {
                   loginRequired = true;
            }
            reset();
        }

        public boolean isLongExpired() {
            return longExpired;
        }
        
        public boolean isShortExpired() {
            return shortExpired;
        }

        public void setShortExpired(boolean b) {
            shortExpired = b;
        }

        public void reset() {
            
            if (interval == 0)
               expirationTime = 0;
            else   
               expirationTime = System.currentTimeMillis() + interval;
        }

        public void updateLong() {
            if (expirationTime == 0){
                longExpired = true;
                shortExpired = true;           
            }
            else {
                if (System.currentTimeMillis() > expirationTime) {
                    longExpired = true;
                    shortExpired = true;
                    listener.idleTimeoutLong();               
                }
            }
        }
        
        public void updateShort() {
         
            if ((expirationTime == 0) || !loginRequired){
                shortExpired = true;
            }
            else {
                if (System.currentTimeMillis() > expirationTime) {
                    shortExpired = true;
                    listener.idleTimeoutShort();             
                } 
            }
        }
        
    }
}
