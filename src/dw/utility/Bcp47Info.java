package dw.utility;

import java.io.Serializable;

public class Bcp47Info implements Serializable {
	/*
	 * Used because of java.util.Locale handling of ISO 639 codes - specifically
	 * for 639-1 for Indonesian ("id" rather than "in" and the 639-2 code for 3
	 * alphanumerics specifically Hmong ("hmn")
	 * 
	 * could be expanded for variant that the Locale handles as well, but have
	 * not found a use for it.
	 * 
	 * reference RFC BCP-47 at https://tools.ietf.org/html/bcp47 dated 2009
	 */
	private static final long serialVersionUID = -8597749954847041590L;
	private String name;
    private String abbrev;
    private String langCode2;
    private String script;
    private String langWithRegion;
    private String bcp47Value;

	/**
	 * constructor to create needed BCP-47 form of the language/regional info
	 * 
	 * @param abbrev
	 *            3*ALPHA ISO 639-3 language code
	 * @param name
	 *            descriptive name for the language
	 * @param langCode2
	 *            2*3ALPHA ISO 639 language code followed by optional extension
	 *            of 3*ALPHA ISO 639 code
	 * @param eScript
	 *            enumerated UnicodeScript value
	 * @param region
	 *            2*ALPHA ISO 3166-1 code or 3*DIGIT fro the UN M.49 code
	 */
	public Bcp47Info(String abbrev, String name, String langCode2, Character.UnicodeBlock eScript, String region) {
		this.name = name.trim();
		this.abbrev = abbrev.trim();
		this.langCode2 = langCode2.trim();
		if (eScript.equals(Character.UnicodeBlock.LATIN_1_SUPPLEMENT)) {
			this.script = "Latn";
		} else if (eScript.equals(Character.UnicodeBlock.CYRILLIC)) {
			this.script = "Cyrl";
		} else if (eScript.equals(Character.UnicodeBlock.ARABIC)) {
			this.script = "Arab";
		} else if (eScript.equals(Character.UnicodeBlock.HANUNOO)) {
			this.script = "Hans"; // simplified Chinese
		} else if (eScript.equals(Character.UnicodeBlock.HANGUL_JAMO)) {
			this.script = "Hang"; // Korean specific
		} else if (eScript.equals(Character.UnicodeBlock.THAI)) {
			this.script = "Thai";
		} else if (eScript.equals(Character.UnicodeBlock.KHMER)) {
			this.script = "Khmr";
		} else if (eScript.equals(Character.UnicodeBlock.LAO)) {
			this.script = "Laoo";
		} else if (eScript.equals(Character.UnicodeBlock.BENGALI)) {
			this.script = "Beng";
		} else if (eScript.equals(Character.UnicodeBlock.GURMUKHI)) {
			this.script = "Guru";
		} else if (eScript.equals(Character.UnicodeBlock.ARMENIAN)) {
			this.script = "Armn";
		} else if (eScript.equals(Character.UnicodeBlock.ETHIOPIC)) {
			this.script = "Ethi";
		} else {
			this.script = "";
		}

		this.langWithRegion = langCode2 + "-" + region;
		this.bcp47Value = (script.length() > 0) ? langCode2 + "-" + script + "-" + region : langWithRegion;
    }

	@Override
    public String toString() {
        if (abbrev.trim().length() == 0)
            return "";	
        else
            return abbrev + " - " + name;	
    }

	/*
	 * return BCP-47 formatted for 2-alpha language code (ISO 639-1)
	 * concatenated with ISO 15924 4-alpha script code and ISO 3166 country code
	 */
    public String getBcp47Value() {
    	return bcp47Value;
    }

	/*
	 * return BCP-47 formatted for 2-alpha language code (ISO 639-1)
	 * concatenated with ISO 3166 country code
	 */
    public String getlangWithRegion() {
    	return langWithRegion;
    }

    public String getIso639_1() {
    	return langCode2;
    }

    public String getIso639_2() {
    	return abbrev.toLowerCase();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
     }

    public String getAbbrev() {
        return abbrev;
    }

    public void setAbbrev(String abbrev) {
        this.abbrev = abbrev;
    }

	public String getLabel() {
		return toString();
	}

	public String getValue() {
		return abbrev;
	}
	
	public String getLangWithRegion() {
		return langWithRegion;
	}
}
