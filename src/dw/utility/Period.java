/*
 * Period.java (formerly AccountingPeriodUtility.java)
 * 
 * $Revision$
 *
 * Copyright (c) 2000-2005 MoneyGram International
 */

package dw.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import dw.i18n.Messages;
import dw.io.DiagLog;
import dw.io.State;
import dw.io.report.ReportLog;
import dw.main.MainPanelDelegate;
import dw.profile.Profile;
import dw.profile.UnitProfile;

/** A set of methods to help with converting from the current
    day to the day of the accounting week.
    Agents are given a window of 1-240 minutes on both sides
    of the account start day in the profile during which they
    can manually close the period.  I.e., the auto close is 0-240
    minutes after the account start day.  During that window, therefore
    the period could correspond to the previous or coming period number.
    The "real" close of the period = startTime + window.
    The manual close allowed period = startTime - window.
    
    @author Erik S. Steinmetz
    @author John Allen
    @author Geoff Atkin
*/
public class Period {

    /** 
     * The maximum number of minutes before or after the start of the 
     * accounting start time that the user can close the period.
     */
    public static final int MAX_ACCT_WINDOW_MINUTES = 240;
    public static final int MAX_ACCT_WINDOW_MILLIS = TimeUtility.MINUTE * MAX_ACCT_WINDOW_MINUTES;
    
    /**
      * Figure out what period it is based on the current time of day.
      * Equivalent to computePeriod(TimeUtility.currentTime(), hint).
      * @param hint the period the caller thinks it might be, or zero
      * if the caller has no idea.
      * @return a period number in the range 1 through 7.
      * @author Geoff Atkin
      */
    public static byte computePeriod(int hint) {
        Date now = TimeUtility.currentTime();
        return computePeriod(now, hint);
    }

    /**
      * Figure out what period it is based on the given time of day.
      * If the answer is ambiguous (because we're in the window), the 
      * supplied hint is examined; if the hint matches one of the two
      * possible answers, it is returned as the period. Otherwise, the 
      * period is chosen based on the accounting start time.
      * @param now the date and time for which to compute the period
      * @param hint the period the caller thinks it might be, or zero
      * if the caller has no idea.
      * @return a period number in the range 1 through 7.
      * @author Geoff Atkin
      */
    public static byte computePeriod(Date now, int hint) {
        //Debug.println("computePeriod("+now+","+hint+") ");
        int period = 0;
        
        // get the time when periods are supposed to start
        
        Date startTime = getAccountingStartTimeAsDate();
        Calendar start = Calendar.getInstance();
        start.setTime(startTime);

        //DateFormat df = DateFormat.getInstance();
        //SimpleDateFormat df = new DwSimpleDateFormat();
        //Debug.println("start = " + df.format(start.getTime()) );	
        
        // compute raw period, where 0 means the interval between 0:00 Sunday
        // and the period start time on Sunday; 7 means the interval between 
        // the period start time on Saturday and 24:00 Saturday.

        Calendar today = new GregorianCalendar();
        today.setTime(now);
        
        // set c to the date/time that a period began last Sunday.
        // (or that it began today, if today is Sunday; or that it 
        // will begin today, if today is Sunday and it hasn't yet)
        
        Calendar c = new GregorianCalendar(
                today.get(Calendar.YEAR),
                today.get(Calendar.MONTH),
                today.get(Calendar.DATE),
                start.get(Calendar.HOUR_OF_DAY), 
                start.get(Calendar.MINUTE));

        c.add(Calendar.DATE, 0 - today.get(Calendar.DAY_OF_WEEK) 
                + Calendar.SUNDAY);

        // get the earliest period change date/time after now, 
        // counting periods as we go
        
        Date d = c.getTime();
        while (now.after(d)) {
            ++period;
            c.add(Calendar.DATE, 1);
            d = c.getTime();
        }
        
        //Debug.println("c = " + df.format(c.getTime()));	
        //Debug.println("period = " + period);	
       
        // adjust the raw period for the day of week designated as
        // the start of period 1

        period = period - getAccountingStartDay() + 1;
        
        // if we don't have a hint, we're done

        if (hint == 0) {
            return (byte) normalizePeriod(period);
        }

        // where are we with respect to the window?
        
        int windowBias = inWindow(today, start);
        //Debug.println("windowBias = " + windowBias);	
        
        // If we aren't in the window, we're done. We can ignore the hint 
        // we were given. Otherwise we use the hint to determine the answer.
        
        if (windowBias == 0) {
            return (byte) normalizePeriod(period);
        } 
        else if (normalizePeriod(period - windowBias) == hint) {
            return (byte) hint;
        }
        else {
            return (byte) normalizePeriod(period);
        }
    }

    /**
      * Determine if the specified time is in the accounting window.
      * Returns 0 if not, 1 if in window after the accounting start time,
      * and -1 if in window before the accounting start time.
      * @param t the time to test
      * @param start the accounting period start time
      * @author Geoff Atkin
      */
    private static int inWindow(Calendar t, Calendar start) {

        long t1, t2, delta;
        t1 = t.getTime().getTime();
        
        // Get the size of the window in milliseconds 
        int window = getAccountingWindowMillis();

        if (window == 0) {
            return 0;
        }

        // set c to today's period start time
        final int startHour = start.get(Calendar.HOUR_OF_DAY);
        final int startMinute = start.get(Calendar.MINUTE);
        
        Calendar c = new GregorianCalendar(
                t.get(Calendar.YEAR),
                t.get(Calendar.MONTH),
                t.get(Calendar.DATE),
                startHour, 
                startMinute);
                
        final long startTimeOfDayMillis = (startHour * TimeUtility.HOUR) + (startMinute * TimeUtility.MINUTE);
        // Set the window start time without crossing noon
        final long windowStartMin = (startTimeOfDayMillis >= TimeUtility.NOON) ? TimeUtility.NOON : -TimeUtility.NOON;
        final long windowStart = Math.max(startTimeOfDayMillis - window,windowStartMin);
        final long windowBefore = startTimeOfDayMillis - windowStart;
        // Set the window stop time without crossing noon
        final long windowStopMax = (startTimeOfDayMillis <= TimeUtility.NOON) ?
            TimeUtility.NOON : TimeUtility.DAY + TimeUtility.NOON;
        final long windowStop = Math.min(startTimeOfDayMillis + window,windowStopMax);
        final long windowAfter = windowStop - startTimeOfDayMillis;

        // the nearest period start time could be yesterday, today, 
        // or tomorrow
        
        c.add(Calendar.DATE, -1);

        for (int i = 0; i < 3; ++i) {
            t2 = c.getTime().getTime();
            delta = t1 - t2;
            if ((delta >= 0) && (delta <= windowAfter)) 
                return 1;
            else if ((delta < 0) && (0 - delta <= windowBefore)) 
                return -1;

            c.add(Calendar.DATE, 1);
        }

        return 0;
    }

    /**
      * Determines if the current time is in the accounting window.
      * Returns 0 if not, 1 if in window after the accounting start time,
      * and -1 if in window before the accounting start time.
      * @author Geoff Atkin
      */
    private static int inWindow() {
        Date startTime = getAccountingStartTimeAsDate();
        Calendar start = Calendar.getInstance();
        start.setTime(startTime);

        Date now = TimeUtility.currentTime();
        Calendar today = new GregorianCalendar();
        today.setTime(now);
        
        return inWindow(today, start);
    }
    
    /** 
      * Returns the profile option ACCOUNTING_START_TIME as a Date. This
      * is the time that accounting periods nominally start every day.
      * If the option is missing or unparsable, returns midnight.
      * @author Geoff Atkin
      */
    public static Date getAccountingStartTimeAsDate() {
        Date d;
        try {
            d = new SimpleDateFormat(TimeUtility.TIME_12_HOUR_MINUTE_FORMAT).parse(UnitProfile.getInstance().find("ACCOUNTING_START_TIME").stringValue());	
        }
        catch (Exception e) {
            GregorianCalendar g = new GregorianCalendar(1970, 1, 1, 0, 0, 0);
            d = g.getTime();
        }
        return d;
    }

    /** 
      * Returns the profile option ACCOUNTING_START_DAY as an int. This is the 
      * day of the week containing the start of period 1. If the option is 
      * missing or invalid, returns 1.
      * @return 1 for Sunday, 2 for Monday, etc.
      * @author Geoff Atkin
      */
    public static int getAccountingStartDay() {
        int result;
        try {
            result = UnitProfile.getInstance().find("ACCOUNTING_START_DAY").intValue();	
        }
        catch (Exception e) {
            result = 1;
        }
        if ((result < 1) || (result > 7)) {
            result = 1;
        }
        //Debug.println("getAccountingStartDay() => " + result);	
        return result;
    }

    /** 
      * Takes a period that may not be in the range 1 to 7, and 
      * adjusts it so it is.
      * @author Geoff Atkin
      */
    public static int normalizePeriod(int period) {
        while (period < 0)
            period += 7;
        period = period % 7;
        if (period == 0) 
            period = 7;
        return period;
    }
    
    public static int nextPeriod(final int period) {
        return normalizePeriod(period + 1);
    }
    
    /**
      * Returns the accounting window in milliseconds. If the profile 
      * option is missing or invalid, return 0.
      * @author Geoff Atkin
      */
    public static int getAccountingWindowMillis() {
        int result;
        try {
            result = UnitProfile.getInstance().find("ACCOUNTING_WINDOW").intValue() 	
                * TimeUtility.MINUTE;
        }
        catch (Exception e) {
            result = 0;
        }
        return result;
    }
    
    // Returns the starting time of day in milliseconds
    private static long getAccountingStartTime() throws Exception {
        final Profile profile = UnitProfile.getInstance().getProfileInstance();
        return profile.find("ACCOUNTING_START_TIME").timeOfDayValue();	
    }
    
    /** 
     * Opens a new period, writes diag log message, and archives report log.  
     * Sets the dispenser into VERIFY_REQUIRED status, thus prompting the 
     * user for a 4-digit verification the next time a transaction is created
     * in this new period.
     * @param newPeriod Integer value representing the new period index.
     * @param automatic Boolean value stating whether automatic period close
     * occurrs.
     * @param employeeID Integer value representing employee id who opened
     * this period.
     * @author Christopher Bartling (added dispenser verification code) 
     */
    public static void openPeriod(int newPeriod, boolean automatic, 
            int employeeID) {
        // Marks the end of the previous period
        //ReportLog.logPeriodChange();
        ReportLog.logPeriodChange(getPeriod(), automatic, employeeID, true);
        setPeriod(newPeriod);
        DiagLog.getInstance().writePeriodChange((short) newPeriod, 
                automatic, (short) employeeID);
        // Marks the beginning of the new period
        //ReportLog.logPeriodChange();        
        ReportLog.logPeriodChange(newPeriod, automatic, employeeID, false);
        try {
            ReportLog.archiveRecords();
        }
        catch (Exception e) {
        }
        // Get the dispenser and check to make sure it's on-line.
        MainPanelDelegate.periodChanged();
    }
    
    /**
     * Closes the accounting period if it is valid to do so. Before calling
     * this method, make sure the employee has privilege to do this.
     * Also writes diag log message and archives report log.
     * @param employeeID employee performing the (manual) change
     * @return false if outside the window and not changed
     * @author Geoff Atkin (rewrite)
     */
    public static boolean closeAccountingPeriod(int employeeID) {
        int previousPeriod = getPeriod();
        int nextPeriod = computePeriod(nextPeriod(previousPeriod));

        if (nextPeriod != previousPeriod) {
            openPeriod(nextPeriod, false, employeeID);
            return true;
        }
        return false;
    }
        
    /** 
     * Does an automatic accounting period close if it is valid to
     * do so. Also writes diag log message and archives report log.
     * @author Geoff Atkin
     */
    public static void autoCloseAccountingPeriod() {

        boolean autoAccountingOn = true;
        int currentPeriod;
        int nextPeriod;
      
        try {
            autoAccountingOn = 
                UnitProfile.getInstance().find("AUTO_ACCOUNTING").booleanValue();	
        }
        catch (Exception e) {
            autoAccountingOn = true;
        }
        
        if (! autoAccountingOn) 
            return;

        currentPeriod = getPeriod();
        //nextPeriod = normalizePeriod(currentPeriod + 1);
        nextPeriod = computePeriod(0);

        // If it really is time to be in the next period, and we are not
        // in the window, then open the new period.
        
        if ((nextPeriod != currentPeriod) && (inWindow() == 0)) {
            openPeriod(nextPeriod, true, 0);
        }
        else {
            //Debug.println("(ignored autoCloseAccountingPeriod() - not in window)");	
        }
    }

    /** Returns the current period number. */
    public static int getPeriod() {
        return UnitProfile.getInstance().getCurrentPeriod();
    }

    private static void setPeriod(int period) {
        UnitProfile.getInstance().setCurrentPeriod((byte) period);
    }

    /**
     * Checks if a particular period for a particular day and time is the 
     * same period as the period and date/time according to the profile 
     * starting account time.
     * It takes the close period window into account.  That is, within the 
     * window, the period could be the same or one off of the one expected
     * by the profile.  This is mostly for comparing saved ReportLog records
     * with a past date.
     * @param testDateTime = date/time to check
     * @param testperiod = period number to check
     * @param checkDateTime = date/time to check against.  The period to check
     * against is the one determined by this date/time without consideration of 
     * the period close window.
     * @return true or false
     */
    
    public static boolean isSamePeriod(long testDateTime, int testPeriod,
            long checkDateTime) {
        /*-
         * Example: 
         * isSamePeriod(05-30-00 23:59,        // testDateTime
         *              2,                     // testPeriod
         *              05-30-00 1100)         // checkDateTime
         * If the the starting account time is:
         *              0600, return true
         *              2200, return false (It's part of 05-31-00.)
         */
        boolean inRange = false;
        try {
            final PeriodWindow pw = new PeriodWindow(checkDateTime);
            inRange = pw.timeAndPeriodMatch(testDateTime,testPeriod);
        } catch (Exception e) {
          //  ErrorManager.handle(e, "accountingPeriodError", ErrorManager.ERROR, false); 
             ErrorManager.handle(e, "applicationError", Messages.getString("ErrorMessages.applicationError"), ErrorManager.ERROR, false);           

            // changed from DiagLog to Debug in DW4.0
            Debug.println("Period.isSamePeriod: "	
				+ e.getMessage()
				+ ", failed to get accounting period"); 
            inRange = false;
        }
        //Debug.println("inRange="+inRange);	
        return inRange;
    }


    /** Clear the logs. Should there should be a diag write here? */
    public static void clearLogs() {
        State state = State.getInstance();
        try {
            state.clearLogs();
            ReportLog.getInstance().clear();
//            ReportLog.purgeAllDetailRecords();
//            ReportLog.purgeAllSummaryRecords();
        }
        catch (java.io.IOException e) {
            //ErrorManager.handle(e, "stateClearLogsError", ErrorManager.ERROR, true);	
            ErrorManager.handle(e, "applicationError", Messages.getString("ErrorMessages.applicationError"), ErrorManager.ERROR, true); 
        }
    }

    /** Reset the whole darn application. Should there should be a diag write? */
    public static void reverseSetup() {
        State state = State.getInstance();
        try {
                state.resetEverything();
        }
        catch (java.io.IOException e) {
           // ErrorManager.handle(e, "reverseSetupError", ErrorManager.ERROR, true);	
           ErrorManager.handle(e, "applicationError", Messages.getString("ErrorMessages.applicationError"), ErrorManager.ERROR, true);   
        }
    }

    /** Returns the ordinal (1 through 7) day of the accounting week.
     *  @param currentTime The current or desired time for which to determine
     *  the accounting period.
     */
    public static int getDayOfAccountingWeek(final Date currentTime) throws Exception {
        int accountStartDay = 1;
        try{
            accountStartDay = getAccountingStartDay();
        }
        catch ( Exception e) {
            //ErrorManager.handle(e, "getAccountingStartDayError", ErrorManager.ERROR, false);	
            ErrorManager.handle(e, "applicationError", Messages.getString("ErrorMessages.applicationError"), ErrorManager.ERROR, false);  
            Debug.println("Bad number for accountStartDay from the client profile information");	
        }
        long startTimeMillis = 0;
        try {
            startTimeMillis = getAccountingStartTime();
        }
        catch (ParseException e) {
           // ErrorManager.handle(e, "getAccountingStartTimeError", ErrorManager.ERROR, false);	
            ErrorManager.handle(e, "applicationError", Messages.getString("ErrorMessages.applicationError"), ErrorManager.ERROR, false);  
            Debug.println("Bad number for accountStartTime from the client profile information");	
        }
        return( getDayOfAccountingWeek( currentTime, accountStartDay, startTimeMillis));
    }
        
    /** Returns the ordinal (1 through 7) day of the accounting week.
     * @param currentTime The current or desired time for which to determine
     * the accounting period.
     * @param startDay The start day of the accounting week.
     * @param startTime The starting time of day of the accounting week.
     */
    public static int getDayOfAccountingWeek(final Date currentTime,
            final int startDay, final long startTime) {

        // get a calendar
        final Calendar localCalendar = Scheduler.getClonedLocalCalendar(currentTime);
        int currentDay = TimeUtility.getDwDayOfWeek(localCalendar);
        final long currentTimeOfDay = Scheduler.timeOfDay(localCalendar);
        // If we're before the start time during the day, we need to subtract
        // one from the current day for day in the week comparison purposes.
        if (currentTimeOfDay < startTime)
            currentDay = TimeUtility.getPreviousDwDayOfWeek(currentDay);
        final int dayDifference = currentDay - startDay;
        final int normalPeriodNumber = TimeUtility.normalizedDayOfWeek(dayDifference);
        final int periodNumber = TimeUtility.normalToDwDayOfWeek(normalPeriodNumber);
        return( periodNumber);
    }
    
    /**
     * Returns the time from which absolute periods are counted
     */
    public static long getAbsolutePeriodBaseTime() {
        // Use the local calendar to obtain the base date and time so that
        // the correctime time zone is used
        final Calendar localCalendar = Scheduler.getClonedLocalCalendar();
        localCalendar.setTimeZone(TimeZone.getDefault());
        localCalendar.clear(Calendar.MILLISECOND);
        localCalendar.set(2000,Calendar.JANUARY,2,0,0,0);
        final Date baseTime = localCalendar.getTime();
        final long baseTimeMillis = baseTime.getTime();
        long accountingStartTime;
        try {
            accountingStartTime = getAccountingStartTime();
        }
        catch (Exception e) {
            Debug.printStackTrace(e);
            accountingStartTime = 0;
        }
        return baseTimeMillis + accountingStartTime;
    }
    
    /**
     * Returns the relative period for absolute period 0
     */
    public static int getAbsolutePeriodBaseRelativePeriod() {
        int accountingStartDay;
        try {
            accountingStartDay = getAccountingStartDay();
        }
        catch (Exception e) {
            Debug.printStackTrace(e);
            accountingStartDay = 1;
        }
         
        // Compute the period beginning on Sunday. Period 1 will come out as
        // period 8 until normalization below.
        int basePeriod = 9 - accountingStartDay;
              
        // Make sure the base period is from 1 to 7
        if (basePeriod > 7)
            basePeriod -= 7;
        return basePeriod;
    }
    
    public static int getRelativePeriod(final long absolutePeriod) {
        int period = (getAbsolutePeriodBaseRelativePeriod() + (int)absolutePeriod) % 7;
        if (period == 0)
            period = 7;
        return period;
    }
    
    /**
     * Returns the absolute period number for the specified date, time and relative
     * period.
     * Whereas normal period numbers cycle through the range 1 to 7 each week,
     * the absolute period number does not cycle back to 1 but continues incrementing.
     * It is counted from Sunday, 1-2-00.
     */
    public static long getAbsolutePeriod(final long dateTime,
                                         final int relativePeriod) {
        final long baseTimeMillis = getAbsolutePeriodBaseTime();
        final int basePeriod = getAbsolutePeriodBaseRelativePeriod();
        return getPeriodDifference(baseTimeMillis,basePeriod,
                                   dateTime,relativePeriod);
    }
    
    /**
     * Returns the absolute period number for the specified date and time,
     * assuming the period starts and ends at the accounting start time.
     */
    public static long getAbsolutePeriod(final long timeStamp) {
        try {
            final PeriodWindow periodWindow = new PeriodWindow(timeStamp);
            return Period.getAbsolutePeriod(timeStamp,periodWindow.getPeriod());
        }
        catch (Exception e) {
            return 0;
        }
    }
    
    /**
     * Returns the absolute period number for the current date, time and period.
     */
    public static long getAbsolutePeriod() {
        return getAbsolutePeriod(TimeUtility.currentTimeMillis(),Period.getPeriod());
    }
    
    /**
     * Computes the difference, in days, of the periods indicated by the given dates,
     * times and periods
     */
    public static int getPeriodDifference(final long begDateTime,
                                          final int begPeriod,
                                          final long endDateTime,
                                          final int endPeriod) {
        final long timeDiff = endDateTime - begDateTime;
        final int periodDiff = endPeriod - begPeriod;
        final long periodDiffMillis = (long) periodDiff * (long) TimeUtility.DAY;
        final float weekDiffFloat =
            (float)(timeDiff - periodDiffMillis) / (float)TimeUtility.WEEK;
        final int weekDiffInt = Math.round(weekDiffFloat);
        final int totalPeriodDiff = (weekDiffInt * TimeUtility.DAYS_IN_WEEK) + periodDiff;
        return totalPeriodDiff;
    }
    
    /** A test main */
    public static void main( String[] args) {
    }
}
