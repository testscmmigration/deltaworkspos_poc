/*
 * CSVParser.java
 * 
 * $Revision$
 *
 * Copyright (c) 2004 MoneyGram International
 */

package dw.utility;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * CSVParser is a utility class that is used to parse a CSV file and return a 
 * specified field within the CSV file. Intended for country guidelines and
 * country restrictions data. The guidelines and restrictions files currently
 * use <br> to denote a line break, and <euro> to denote a Euro symbol; in 
 * effect the file format is a variant of CSV. 
 */
public class CSVParser {


    private URL url;
    private List<List<String>> rows;
    private List<String> cols;
    
    public CSVParser(String filename){
        url = ClassLoader.getSystemResource(filename);
    }

    public void process(BufferedReader reader) throws IOException {

        int previousCharacter = 0; 
        boolean insidequotes = false;
        rows = new ArrayList<List<String>>();
        cols = new ArrayList<String>();
        rows.add(cols);
        StringBuffer sb = new StringBuffer();
   
        for (int ch = reader.read(); ch != -1; ch = reader.read()) {
            if (ch == '"') { 
                insidequotes = !insidequotes;
                if (previousCharacter == ch) {
                    sb.append((char) ch);
                }
            }
            else if ((ch == '\r' || ch == '\n') && !insidequotes) {
                if ((previousCharacter != '\r' && previousCharacter != '\n')) {
                    // marks the end of the field
                    cols.add(preprocess(sb.toString()));
                    sb = new StringBuffer();
                    // also marks the end of the record
                    cols = new ArrayList<String>();
                    rows.add(cols);
                }
            }
            else if (ch == ',' && !insidequotes) {
                // marks the end of the field, but not the record
                cols.add(preprocess(sb.toString()));
                sb = new StringBuffer();
            }
            else {
                sb.append((char) ch);
            }
            previousCharacter = ch;
        }
    }

    public void process() throws FileNotFoundException, IOException {  
        if (url == null) {
            throw new FileNotFoundException("null");
        }

        InputStream input = url.openStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(input, "Cp1252"));         
        try {
           process(reader);
        }
        finally {
            reader.close();
        }
    }
    
    /** 
     * Returns the field at the given row and column, where the first field 
     * in the first row is at row=0, col=0.
     */
    public String getField(int row, int col) {
        return rows.get(row).get(col);
    }

    /** Returns the number of records (rows) read. */
    public int size() {
        return rows.size();
    }
    
    /** Returns the number of cols in a row  */
    public int rowSize(int rowNum) {
        return rows.get(rowNum).size();
    }     

    /** Convert &lt;br&gt; and &lt;euro&gt; notation to literal symbols. */
    private static String preprocess(String s) {
        String result;
        result = s.replaceAll("\\<[Bb][Rr]\\>", "\r\n");
        result = result.replaceAll("\\<[Ee][Uu][Rr][Oo]\\>", "\u20AC");
        return result;
    }
    
    public static void main(String[] args) {
        CSVParser p = new CSVParser("xml/countryguidelines.csv");
        try {
            p.process();
            Debug.println(p.getField(1, 1));
        }
        catch (Exception e) {
			Debug.printStackTrace(e);
        }
    }
}
