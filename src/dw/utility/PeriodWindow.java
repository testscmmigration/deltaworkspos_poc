/**
 * Represents the expected period for a given date and time and a time range in which
 * all transactions for that period must occur.
 * @author Eric Inman
 */
package dw.utility;

import java.util.Date;

public class PeriodWindow {
     
    /* 
     * The maximum time difference possible between any given time and a transaction
     * that is within the period for that time. This time includes one day for the
     * length of a normal period, one day for the maximum difference between the
     * current accounting start time and the accounting start time on the day of the
     * transaction, and the maximum size of the accounting start window.
     */
    public static final long MAX_PERIOD_OFFSET = (2 * TimeUtility.DAY) +
                                                  Period.MAX_ACCT_WINDOW_MILLIS;

    private long dateTime;
    private int period;
    private long begDateTime;
    private long endDateTime;
    
    public PeriodWindow(final long t) throws Exception {
        dateTime = t;
        period = Period.getDayOfAccountingWeek(new Date(dateTime));
        begDateTime = dateTime - MAX_PERIOD_OFFSET;
        endDateTime = dateTime + MAX_PERIOD_OFFSET;
    }
    
    public long getDateTime() {return dateTime;}
    public int  getPeriod() {return period;}
    public long getBegDateTime() {return begDateTime;}
    public long getEndDateTime() {return endDateTime;}
    
    public boolean timeAndPeriodMatch(final long testDateTime, final int testPeriod) {
        return (testPeriod == getPeriod())
                    && (getBegDateTime() <= testDateTime)
                    && (testDateTime < getEndDateTime());
        
    }
}
