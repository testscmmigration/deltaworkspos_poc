/*
 * AccessNumbers.java
 *
 * Copyright (c) 2004 MoneyGram, Inc. All rights reserved
 */

package dw.utility;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import dw.dwgui.CityProviderNumberListComboBox;
import dw.dwgui.StateListComboBox;
import dw.model.XmlUtil;
import dw.model.adapters.CountryDialupInfo;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.CountryInfo.Country;
import dw.model.adapters.CountryInfo.CountrySubdivision;

/**
 * @author T014 Access Accessnumbers.xml file and then access codetables.xml
 *         file and then generates the Country List that has to be populated in
 *         the CountryComboBox of the DialUp Settings Profiler Used ArrayList
 *         instead of a normal List as there should be unique values in the
 *         countryBox. But the xml file produces the same countryname ... many
 *         times .. checking with TreeSet to get the values
 * 
 * The states are retrived from DWvalues not from the XML file
 * 
 * The values of the CityProviderPhoneNumber are to be retrived from here as a
 * Array
 * 
 */

public class AccessNumbers {

	public static AccessNumbers instance;

	private Document accessNumbers = null;
	
	private AccessNumbers() {

		accessNumbers = loadAccessNumbersFile("accessnumbers.xml",
				"xml/accessnumbers.xml",
				"Failed to parse local access numbers table, using internal one.");
	}
	
	public Document getAccessNumbers() {
		return accessNumbers;
	}

	private static Document loadAccessNumbersFile(String filename, String resource, String error) {
		File f;
		Document result = null;

		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
			dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
			dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
			
			f = new File(filename);
			if (f.exists()) {
				// Debug.println()
				result = dbf.newDocumentBuilder().parse(f);
				if (result == null) {
					Debug.println(error);
				}
			}
			if (result == null) {
				InputStream in = AccessNumbers.class.getResourceAsStream(resource);
				result = dbf.newDocumentBuilder().parse(in);
			}
		} catch (SAXException e) {
			Debug.printException(e);
		} catch (IOException e) {
			Debug.printException(e);
		} catch (ParserConfigurationException e) {
			Debug.printException(e);
		}

		return result;
	}


	public static List<CityProviderNumberInfo> getcityProviderPhoneNumber(Country country, CountrySubdivision state) {

		List<CountryDialupInfo> tagList = AccessNumbers.getInstance().getDialInfos();
		List<CityProviderNumberInfo> cityProviderPhoneScript = new ArrayList<CityProviderNumberInfo>();

		for (CountryDialupInfo e : tagList) {
			String countryCode = e.getCountryCode();
			String stateCode = e.getState();
			
			if ((country.getCountryCode().equalsIgnoreCase(countryCode)) && (state.getCountrySubdivisionCode().equalsIgnoreCase(stateCode))) {
				String city = e.getCity();
				String prefix = stateCode + " - ";
				if (city.startsWith(prefix)) {
					city = city.substring(prefix.length());
				}
				String provider = e.getProvider();
				String phoneNumber = e.getPhoneNumber();
				String scriptName = e.getScriptName();
				if (scriptName.length() == 0) {
					scriptName = "NONE";
				}
				cityProviderPhoneScript.add(new CityProviderNumberInfo(city, provider, phoneNumber, scriptName));

			}
		}
		cityProviderPhoneScript.add(new CityProviderNumberInfo());
		return cityProviderPhoneScript;
	}


	public List<CountryDialupInfo> getDialInfos() {
		String expStr = "//countryDialupInfo";
		XmlUtil<CountryDialupInfo> xmlUtil = new XmlUtil<CountryDialupInfo>(CountryDialupInfo.class);
		List<CountryDialupInfo> cInfos = xmlUtil.getInfo(getInstance().getAccessNumbers(), expStr);
		return cInfos;
	}

	public static AccessNumbers getInstance() {
		if (instance == null) {
			instance = new AccessNumbers();
		}
		return instance;
	}

	/**
	 * @param selectedItem
	 * @param selectedItem2
	 * @param cityProviderBox
	 */
	public static void updatecityProviderPhoneNumber(Country country, CountrySubdivision state, CityProviderNumberListComboBox cityProviderBox) {
		String listName = country.getCountryName() + state.getCountrySubdivisionName();

		if (country.getCountryCode().toUpperCase(Locale.US).equals("USA")) {
			List<CityProviderNumberInfo> cityProviderPhoneNumber = AccessNumbers.getcityProviderPhoneNumber(country, state);
			if (cityProviderPhoneNumber.size() != 0) {
				cityProviderBox.addList(cityProviderPhoneNumber, listName);
				cityProviderBox.setCurrentList(listName);
			}
		} else {
			List<CityProviderNumberInfo> cityProviderPhoneNumber = AccessNumbers.getcityProviderPhoneNumber(country, state);
			cityProviderBox.addList(cityProviderPhoneNumber, listName);
			cityProviderBox.setCurrentList(listName);
		}
	}

	/**
	 * This method is used by the Dial Up Setting Screen as
	 * xml/SimpleProfileMaintanence.xml has no reference to countryBox . This is
	 * developed for populating the stateBox by getting the name of the country
	 * from countryBox. Used the DWValues.java for adding the list, Called for
	 * both populating and updating of the stateBox.
	 * 
	 * @param selectedItem
	 * @param stateBox
	 */
	public static void updateStateList(Country country, StateListComboBox<CountrySubdivision> stateBox) {
		if (country.getCountryCode().toUpperCase(Locale.US).equals("USA")) {
			stateBox.setEnabled(true);
			stateBox.setCurrentList("USA");
		} else {
			stateBox.addList(CountryInfo.getBlankCountrySubdivisionList(), "Empty");
			stateBox.setEnabled(false);
		}
	}

	public static void populateStateList(StateListComboBox<CountrySubdivision> stateBox) {
		Country c = CountryInfo.getCountry("US");
		List<CountrySubdivision> states = CountryInfo.getAccessCountrySubdivisionList(c);
		stateBox.addList(states, c.getCountryCode());
		if (c.getCountryCode().toUpperCase(Locale.US).equals("USA")) {
			stateBox.setEnabled(true);
			stateBox.setCurrentList(c.getCountryCode());
		} else {
			stateBox.setEnabled(false);
			stateBox.setCurrentList("Empty");
		}
	}
}
