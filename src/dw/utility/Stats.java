package dw.utility;

import java.util.Map;
import java.util.TreeMap;


/** Facility for tracking things and reporting them at program exit. */
public class Stats implements Runnable {
    private static Stats instance;
    private TreeMap<String, Counter> map;
    
    private Stats() {
        map = new TreeMap<String, Counter>();
    }

    private synchronized static Stats getInstance() {
        if (instance == null) {
            Debug.println("creating instance of Stats");	
            instance = new Stats();
            
            Runtime.getRuntime().addShutdownHook(new Thread(instance));
        }
        return instance;
    }

    @Override
	public void run() {
        Debug.println("Stats:");	
        for (Map.Entry<String, Counter> e : map.entrySet()) {
            Debug.println(e.getValue() + "\t" + e.getKey());	
        }
    }
    
    private void record(String s) {
        if (map.containsKey(s))
            (map.get(s)).increment();
        else 
            map.put(s, new Counter());
    }
    
    public static void track(String s) {
        getInstance().record(s);
    }
}

