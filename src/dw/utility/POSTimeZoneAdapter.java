/*
 * Created on Aug 5, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

package dw.utility;

import java.lang.reflect.Field;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

/**
 * @author T006, August 1, 2005
 *
 */
public class POSTimeZoneAdapter {

	private static POSTimeZoneAdapter singleton;
	
	// variables used for object minipulation
	private Field offsetField;
	private Field dstSavingsField;
	private Field startModeField;
	private Field startYearField;
	private Field startMonthField;
	private Field startDayField;
	private Field startDayOfWeekField;
	private Field startTimeField;
	private Field startTimeModeField;
	private Field endModeField;
	private Field endMonthField;
	private Field endDayField;
	private Field endDayOfWeekField;
	private Field endTimeField;
	private Field endTimeModeField;
	private Field useDaylight;
	
	/**
	 * Private constructor.  Force users to follow singleton.
	 */
	private POSTimeZoneAdapter() {
		initializeClassFields();
	}

	/**
	 * Get static instance of this object.
	 * @return
	 */
	public static POSTimeZoneAdapter getInstance() {
		if (singleton == null) {
			singleton = new POSTimeZoneAdapter();
		}
		return singleton;
	}
	
	
	/**
	 * Perform modifications to the TimeZone object.
	 * 
	 * @param timeZoneData  - A TimeZoneCorrection object that holds
	 * TimeZone parameters
	 * @return TimeZone
	 */
	public TimeZone buildTimeZone(TimeZoneCorrection timeZoneData) {
		SimpleTimeZone correctedZone = null;
		correctedZone = new SimpleTimeZone(timeZoneData.getOffset(),timeZoneData.getId());
		
		Debug.println("applying timezone override for "+timeZoneData.getId());
		try {
			if(timeZoneData.getDstSavings()> 0)
				useDaylight.setBoolean( correctedZone, true);
			offsetField.setInt(correctedZone, timeZoneData.getOffset());
			dstSavingsField.setInt(correctedZone, timeZoneData.getDstSavings());
			startModeField.setInt(correctedZone, timeZoneData.getStartMode());
			startMonthField.setInt(correctedZone, timeZoneData.getStartMonth());
			startDayField.setInt(correctedZone, timeZoneData.getStartDay());
			startDayOfWeekField.setInt(correctedZone, timeZoneData.getStartDayOfWeek());
			startTimeField.setInt(correctedZone, timeZoneData.getStartTime());
			startTimeModeField.setInt(correctedZone, timeZoneData.getStartTimeMode());
			endModeField.setInt(correctedZone, timeZoneData.getEndMode());
			endMonthField.setInt(correctedZone, timeZoneData.getEndMonth());
			endDayField.setInt(correctedZone, timeZoneData.getEndDay());
			endDayOfWeekField.setInt(correctedZone, timeZoneData.getEndDayOfWeek());
			endTimeField.setInt(correctedZone, timeZoneData.getEndTime());
			endTimeModeField.setInt(correctedZone, timeZoneData.getEndTimeMode());
		}
		catch (Exception e) {
			Debug.println("Caught exception applying timezone field overrides:" +
				" returning standard TimeZone instead of expected correction, " +  e);
			return TimeZone.getTimeZone(timeZoneData.getId());
		}
		return correctedZone;
	}

	
	/** one-time init of these objects - too expensive to do each time
	 * adapter is called
	 */
	private void initializeClassFields() {
		try {
			useDaylight = SimpleTimeZone.class.getDeclaredField("useDaylight");
			offsetField = SimpleTimeZone.class.getDeclaredField("rawOffset");
			dstSavingsField = SimpleTimeZone.class.getDeclaredField("dstSavings");
			startModeField = SimpleTimeZone.class.getDeclaredField("startMode");
			startYearField = SimpleTimeZone.class.getDeclaredField("startYear");
			startMonthField = SimpleTimeZone.class.getDeclaredField("startMonth");
			startDayField = SimpleTimeZone.class.getDeclaredField("startDay");
			startDayOfWeekField = SimpleTimeZone.class.getDeclaredField("startDayOfWeek");
			startTimeField = SimpleTimeZone.class.getDeclaredField("startTime");
			startTimeModeField = SimpleTimeZone.class.getDeclaredField("startTimeMode");
			endModeField = SimpleTimeZone.class.getDeclaredField("endMode");
			endMonthField = SimpleTimeZone.class.getDeclaredField("endMonth");
			endDayField = SimpleTimeZone.class.getDeclaredField("endDay");
			endDayOfWeekField = SimpleTimeZone.class.getDeclaredField("endDayOfWeek");
			endTimeField = SimpleTimeZone.class.getDeclaredField("endTime");
			endTimeModeField = SimpleTimeZone.class.getDeclaredField("endTimeMode");

			// make methods accessible - they usually are not (sshhh!!)
			useDaylight.setAccessible(true);
			offsetField.setAccessible(true);
			dstSavingsField.setAccessible(true);
			startModeField.setAccessible(true);
			startYearField.setAccessible(true);
			startMonthField.setAccessible(true);
			startDayField.setAccessible(true);
			startDayOfWeekField.setAccessible(true);
			startTimeField.setAccessible(true);
			startTimeModeField.setAccessible(true);
			endModeField.setAccessible(true);
			endMonthField.setAccessible(true);
			endDayField.setAccessible(true);
			endDayOfWeekField.setAccessible(true);
			endTimeField.setAccessible(true);
			endTimeModeField.setAccessible(true);
		}
		catch (Exception e) {
			Debug.println("Caught error setting access rights to SimpleTimeZone class attributes, " + e);
		}
	}

	public static void main(String[] args) {
		TimeZoneCorrection correction = TimeZoneCorrection.getInstance();
		
		correction.setId("America/Santiago");
		correction.setOffset(-14400000);
		correction.setDstSavings(3600000);
		
		correction.setStartMode(4);
		correction.setStartMonth(9);
		correction.setStartDay(16);
		correction.setStartDayOfWeek(1);
		correction.setStartTime(3600000);
		correction.setStartTimeMode(1);
		
		correction.setEndMode(4);
		correction.setEndMonth(2);
		correction.setEndDay(16);
		correction.setEndDayOfWeek(7);
		correction.setEndTime(82800000);
		correction.setEndTimeMode(1);
		
		TimeZone posTimeZone = 
			POSTimeZoneAdapter.getInstance().buildTimeZone(correction);
		
		Debug.println("-- POS timezone:" +
				printRawTimeZone(posTimeZone));		
	}

	// just for test/debug purposes
	static String printRawTimeZone(TimeZone zone) {
		return "Raw " + zone.getDisplayName() + ":" + zone;
	}

}
