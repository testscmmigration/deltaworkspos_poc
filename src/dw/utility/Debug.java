/*
 * Debug.java
 * 
 * $Revision$
 *
 * Copyright (c) 2000-2004 MoneyGram International
 */

package dw.utility;

import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.FileHandler;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import dw.comm.AgentConnect;
import dw.io.RotatingOutputStream;
import flags.CompileTimeFlags;

/**
 * Centralized debugging facility.
 * @author Eric Inman
 */
public final class Debug {
    /** Toggle for turning debugging on/off */
    private static boolean debug = true;
    private static boolean track = false;

//    public static final String TRACE1 = "Trace1.txt"; 
//    public static final String TRACE2 = "Trace2.txt"; 
    public static final String TRACE1 = "Trace3.txt"; 
    public static final String TRACE2 = "Trace4.txt"; 
    private static RotatingOutputStream trace;
	private static final int ENCRYPTADDON = 10;
	private static final int DEBUG_ENCRYPTADDON = 0;
	private static int offset = ENCRYPTADDON;
	private static Logger logger;
	private static FileHandler fh;  


    // Moved the instance of the RotatingOutputStream here to centralize 
    // debug functionality and to make the rotating file more easily accessible 
    // for transmission purposes.
    static {
   	
        try {
            trace = new RotatingOutputStream(TRACE1,TRACE2);
            logger = Logger.getLogger("junitlog"); 
            fh = new FileHandler("junitlog.log");  
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter()
            {
            	@Override
				public String format(LogRecord record) {
            		return new java.util.Date() + " " + record.getLevel() + " " + record.getMessage() + "\r\n";
            	}
            };
            fh.setFormatter(formatter);  

        }
        catch (IOException e) {
            setDebug(false);
           println ("Trace functionality has been disabled");
        }       
    }


    /**
     * Prints a string, without a newline character appended to the end, to
     * standard output.
     * @param s A debugging string.
     * @author Eric Inman
     */
    public static void print(final String s) {
        if (debug) {
            System.out.print(encryptTextLine(new StringBuffer(s)).toString());
        }
    }

    /**
     * Prints a string, with a newline character appended to the end, to
     * standard output. Information of special interest to customer support
     * start with [, =, or -. Other lines are indented with a tab.
     * @param s A debugging string.
     * @author Eric Inman (original)
     * @author Steve Steele (tab logic)
     * @author Geoff Atkin (new tab logic)
     */
    
    public static void println(final String s) {
        if (debug) {
            if ((s == null) || (s.length() == 0)) {
                System.out.println();
            }
            else if ((s.charAt(0) != '[') && (s.charAt(0) != '=') && (s.charAt(0) != '-')) {
                System.out.print("\t");	
                System.out.println(encryptTextLine(new StringBuffer(s)).toString());
            }
            else {
                System.out.println(encryptTextLine(new StringBuffer(s)).toString());
            }
        }
    }

    /**
     * Prints a passed in method name, as well as the exception name and message 
     * 
     * @param s - String containing the method that caught the exception
     * @param e - Exception that was caught.
     */
    public static void printException(Exception e) {
    	StackTraceElement[] eTrace = new Throwable().fillInStackTrace().getStackTrace();
		Debug.println("*** " +eTrace[1].getClassName()
				+ "-" +  eTrace[1].getMethodName() 
				+ " caught Exception - " 
				+ e.getClass().getName() + ": " + e.getMessage());
    }
    

    /**
     * Prints the stack trace of the Throwable object.
     * @param t A Throwable object.
     * @author Eric Inman
     */
    public static void printStackTrace(final Throwable t) {
        System.out.flush();
        if (debug) {
        	println("Debug.printStackTrace() " + t.getMessage());
            StackTraceElement[] ste = t.getStackTrace();
            for (int i=0; i < ste.length; i++) {
            	println("\tat " + ste[i].toString());
            }
            if (t.getCause() != null) {
            	println("Caused by...");
            	printStackTrace(t.getCause());
            }
        }
    }
    
	public static String getStackTrace(final Throwable t) {
		StringBuffer buffer = new StringBuffer();
		StackTraceElement[] ste = t.getStackTrace();
		for (int i = 0; i < ste.length; i++) {
			buffer.append(ste[i].toString());
			buffer.append("/n");
		}
		return buffer.toString();
	}


    /**
     * Dumps the stack trace for the current thread.
     * @author Eric Inman
     */
    public static void dumpStack() {
        if (debug)
            Thread.dumpStack();
    }

    /** Dumps the stack following a debug line. */
    public static void dumpStack(String s) {
        System.out.flush();
       // System.err.println(s);
        println(s);
        Thread.dumpStack();
    }
    
    /**
     * Mutator for the debug class variable.
     * @param newDebugFlag A boolean stating whether debug mode
     * is toggled on/off.
     * @author Chris Bartling
     */
    public static void setDebug(boolean newDebugFlag) {
        debug = newDebugFlag;
    }

    /**
     * Accessor for the debug class variable.
     * @return A boolean stating whether debug mode
     * is toggled on/off.
     * @author Chris Bartling
     */
    public static boolean isDebug() {
        return debug;
    }

    public static void track(java.lang.reflect.Method m) {
        if (track) {
            Stats.track(m.toString());
        }
    }
    
    public static void track(String string) {
        if (track) {
            Stats.track(string);
        }
    }
    
    /**
     * Return a new print stream based on the rotating output stream.
     * The caller, probably DeltaworksMainPanel, can use the result in
     * a call to System.setOut().
     * @return A PrintStream pointing to a rotating file.
     */
    public static PrintStream getPrintStream() {
        return trace.getPrintStream();
    }
    
    
    /**
    * Attempts to swap the debug output files.
    * @return A boolean representing success or failure of the swap.
    * @author sws
    */
    public static boolean rotate() throws IOException {
        return trace.rotate();
    }


    /**
     * Disables rotations of the debug output files.
     * @author sws
     */
    public static void beginTransmission() {
        trace.beginTransmission();
    }


    /**
     * Enables rotations of the debug output files.
     * @author sws
     */
    public static void finishTransmission() {
        trace.finishTransmission();
    }

    private static StringBuffer encryptTextLine(StringBuffer inData) {
    	        
        char aChar;
        int charValue;
        StringBuffer outData = new StringBuffer();   	       
        
        if (AgentConnect.debugEncryption || CompileTimeFlags.debug()) {
        	outData.append(inData);
        } else {
			for (int i=0; i < inData.length(); i++){
	            aChar = inData.charAt(i);
	            charValue = aChar;
	            charValue = charValue - offset;
	            aChar = (char)charValue;
	            outData.append(aChar);
	        }
        }		
		return outData;
    }
    
    public static StringBuffer decryptTextLine(String text) {
    	
        
    	char aChar;
        int charValue;
        StringBuffer outData = new StringBuffer();
    	StringBuffer inData = new StringBuffer(text);
    	
        if (AgentConnect.debugEncryption)
        	offset = DEBUG_ENCRYPTADDON;
    	
        for (int i=0; i < inData.length(); i++){
            aChar = inData.charAt(i);
            charValue = aChar;
            if (charValue != 9)
            	charValue = charValue + offset;
            aChar = (char)charValue;
            outData.append(aChar);   	
        }
        return outData;
    }

	

/*	
   public static void debugTest(String msg) {
		debugTest(msg, "");
	}
   public static void debugTestException(Throwable t) {
		if (CompileTimeFlags.test()) {
			logger.log(Level.SEVERE, "Exception while running the test :: [ "
					+ t.getMessage() + " ]", t);
			StackTraceElement[] ste = t.getStackTrace();
			StringBuffer buffer = new StringBuffer();
			for (int i = 0; i < ste.length; i++) {
				buffer.append("\n\t");
				buffer.append((ste[i].toString()));
			}
			logger.log(Level.SEVERE, buffer.toString());
		}
	}

	public static void debugTest(String msg, String testName) {
		if (CompileTimeFlags.test()) {
			if (testName != null && testName.length() > 0) {
				logger.log(Level.INFO, testName + "::" + msg);
			} else {
				logger.log(Level.INFO, msg);
			}
		}
	}*/
    
}
