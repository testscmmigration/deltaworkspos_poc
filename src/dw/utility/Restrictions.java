
package dw.utility;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.TreeMap;

import dw.i18n.Messages;

/**
 * Restrictions is a utility class that is used to extract the receive restrictions
 * for a specific country.
 */

public class Restrictions {
    
    private TreeMap<String, String> map;
    private static Restrictions instance;
    private String restrictfile = "xml/countryrestrictions";
    private int COUNTRYCODE = 4;    
    private int RESTRICTION = 10;
         
    
    private Restrictions() {
        
        map = new TreeMap<String, String>();
        String newrestriction = "";
        String language = Messages.getCurrentLocale().toString();
        boolean loaded = false;
        
        if (!language.equals("en"))
            restrictfile = restrictfile + "_" + language + ".csv";
        else
            restrictfile = restrictfile + ".csv";  
        
        CSVParser parser = new CSVParser(restrictfile); 
            
        try {
            parser.process();
            loaded = true;
        } catch (FileNotFoundException e) {
            Debug.printStackTrace(e);
        } catch (IOException e) {
            Debug.printStackTrace(e);
        }
        
        if (loaded)  {        
            for (int index = 1; index < parser.size(); index++) {
                String countrycode = parser.getField(index - 1, COUNTRYCODE - 1);
                String description = parser.getField(index - 1, RESTRICTION - 1);                   
                   if (!map.containsKey(countrycode)) {
                       map.put(countrycode, description);
                   }
                   else
                      newrestriction = map.get(countrycode);
                      newrestriction = newrestriction + "\r\n" + description;
                      map.put(countrycode, newrestriction);
                      newrestriction = "";                        
            }
        }       
  
    }
    
    public static Restrictions getInstance(){
        if (instance == null)
           instance = new Restrictions();
        return instance;
    }
    
    public static void nullInstance() {
        instance = null;
    }
    
    public String getRestrictions(String legacycode){       
        return map.get(legacycode);
    }

}