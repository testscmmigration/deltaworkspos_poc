package dw.utility;

import java.io.Serializable;
import java.math.BigDecimal;


/* Represents the combination of a count of items and the total amount of those items. Contains length information
   for string representations of these fields */
public class CountAndAmount implements Serializable {
	private static final long serialVersionUID = 1L;

	public  static final int COUNT_LENGTH          =  4;
    
    private int count;
    private Amount amount;
    private Amount feeAmount;
    private Amount taxAmount;
    
    // Full constructor
    public CountAndAmount(final int count, final Amount amount, final Amount feeAmount, final Amount taxAmount) {
        this.count = count;
        this.amount = (amount == null) ? new Amount() : amount;
        this.feeAmount = (feeAmount == null) ? new Amount() : feeAmount;
        this.taxAmount = (taxAmount == null) ? new Amount() : taxAmount;
    }
    
//    // Constructor with amount length defaulted
//    public CountAndAmount(final int count, final Amount amount, final Amount feeAmount) {
//        this(count,amount,feeAmount, DEFAULT_AMOUNT_LENGTH);
//    }
    
    // Constructor with default values
    public CountAndAmount() {
        this(0,null, null, null);
    }
    
    public BigDecimal getBigDecimalAmount() {
        return amount.getValue();
    }
    
    public void setLongAmount(final long a) {
        amount.setLongValue(a);
    }
    
    public void setBigDecimalAmount(final BigDecimal a) {
        amount.setBigDecimalValue(a);
    }
    
    public BigDecimal getBigDecimalFeeAmount() {
        return feeAmount.getValue();
    }

    public BigDecimal getBigDecimalTaxAmount() {
        return taxAmount.getValue();
    }

    public void setLongFeeAmount(final long a) {
        feeAmount.setLongValue(a);
    }

    public void setBigDecimalFeeAmount(final BigDecimal a) {
        feeAmount.setBigDecimalValue(a);
    }

    public void setBigDecimalTaxAmount(final BigDecimal a) {
        taxAmount.setBigDecimalValue(a);
    }   

    public int getCount() {
        return count;
    }
    
    public void setCount(final int c) {
        count = c;
    }
    
    public void setToZero() {
        setCount(0);
        setLongAmount(0);
    }
    
    public Amount getAmount() {
        return amount;
    }
    
    public Amount getFeeAmount() {
        return feeAmount;
    }

    public Amount getTaxAmount() {
        return taxAmount;
    }

    public void add(final CountAndAmount increment) {
        setCount(getCount() + increment.getCount());
        amount.add(increment.getAmount());
        feeAmount.add(increment.getFeeAmount());
        taxAmount.add(increment.getTaxAmount());
    }    
    
    public void addCountAndBigDecimalAmount(final int c, final BigDecimal a) {
        setCount(getCount() + c);
        amount.addBigDecimalValue(a);
    }   
    public void addFeeAmount(final BigDecimal a) {
        feeAmount.addBigDecimalValue(a);
    }

	public void addTaxAmount(final BigDecimal a) {
		if (a == null) {
			taxAmount.addBigDecimalValue(BigDecimal.ZERO);
		} else {
			taxAmount.addBigDecimalValue(a);
		}
	}
}
