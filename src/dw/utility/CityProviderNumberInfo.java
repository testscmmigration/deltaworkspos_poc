package dw.utility;

import java.io.Serializable;

import dw.framework.DataCollectionScreenToolkit.DWComboBoxItem;

public class CityProviderNumberInfo extends DWComboBoxItem implements Comparable<CityProviderNumberInfo>, Serializable {
	private static final long serialVersionUID = 1L;

	private String city;
    private String provider;
    private String number;
    private String script;

    public CityProviderNumberInfo(String city, String provider, String number, String script) {
        this.city = city;
        this.provider = provider;
        this.number = number;
        this.script = script;
    }

    public CityProviderNumberInfo(){
		this.city = "";
		this.provider = "";
		this.number = "";
		this.script = "NONE";
	}
    
    @Override
	public String toString() {
    	if(city.trim().length() == 0 && provider.trim().length() == 0 && number.trim().length() == 0)
    		return "None";
        else if (city.trim().length() == 0)
            return "";	
        else
            return city.trim() + " - " + provider.trim() + " - " + number.trim();	
    }

	/**
	 * @return Returns the city.
	 */
	public String getCity() {
		return city;
	}
	
	/**
	 * @param city The city to set.
	 */
	public void setCity(String city) {
		this.city = city;
	}
	
	/**
	 * @return Returns the number.
	 */
	public String getNumber() {
		return number;
	}
	
	/**
	 * @param number The number to set.
	 */
	public void setNumber(String number) {
		this.number = number;
	}
	
	/**
	 * @return Returns the provider.
	 */
	public String getProvider() {
		return provider;
	}
	
	/**
	 * @param provider The provider to set.
	 */
	public void setProvider(String provider) {
		this.provider = provider;
	}
	
	/**
	 * @return Returns the script.
	 */
	public String getScript() {
		return script;
	}
	
	/**
	 * @param script The script to set.
	 */
	public void setScript(String script) {
		this.script = script;
	}

	@Override
	public int compareTo(CityProviderNumberInfo cp) {
		int c = this.city.compareTo(cp.city);
		if (c == 0) {
			c = this.provider.compareTo(cp.provider);
			if (c == 0) {
				c = this.number.compareTo(cp.number);
			}
		}
		return c;
	}

	@Override
	public String getLabel() {
		return this.city;
	}

	@Override
	public String getValue() {
		return this.number;
	}

	@Override
	public String getLabelShort() {
		return this.city;
	}

	@Override
	public String getIdentifier() {
		return this.number;
	}
}
