/*
 * TimeUtility.java
 * 
 * $Revision$
 *
 * Copyright (c) 2000-2004 MoneyGram, Inc. All rights reserved
 */

package dw.utility;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import dw.dwgui.DwSimpleDateFormat;
import dw.profile.Profile;
import dw.profile.UnitProfile;

/** Facilities for time synchronized with server. */
public class TimeUtility {

    public static final int MINUTE = 60 * 1000; // 1 minute in milliseconds
    public static final int TWO_MINUTES = 2 * MINUTE;
    public static final int HOUR = 60 * MINUTE;
    public static final int NOON = 12 * HOUR;
    public static final int DAY = 24 * HOUR;
    public static final int DAYS_IN_WEEK = 7;
    public static final int WEEK = DAYS_IN_WEEK * DAY;
    public static final long MONTH = ((long) DAY) * 30;

    public static final String SERVER_DATE_TIME_FORMAT_1 = "yyyy-MM-dd'T'HH:mm:ss.SSSz";
    public static final String SERVER_DATE_TIME_FORMAT_2 = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public static final String KOREAN_DATE_TIME_FORMAT_1 = "y.M.d h:m:s";
    public static final String KOREAN_DATE_TIME_FORMAT_2 = "y.M.d h:m";
    public static final String ISI_DATE_TIME_FORMAT = "yyyyMMddHH:mm:ss";
    public static final String DATE_TIME_N0_DELIMITER_FORMAT = "MMddyy HHmmss";
    public static final String REPORT_DATE_TIME_FORMAT = "MM/dd/yyyy,HH:mm";
    public static final String RECEIPT_DATE_TIME_FORMAT = "MM/dd/yyyy h:mm a";
    public static final String LOG_DATE_TIME_FORMAT = "yyyy/MM/dd, HH:mm:ss, zzzz";

    public static final String KOREAN_DATE_YEAR_MONTH_DAY_FORMAT = "y.M.d";
    public static final String KOREAN_DATE_MONTH_DAY_FORMAT = "M.d";
    public static final String KOREAN_DATE_YEAR_MONTH_DAY_FORMAT_1 = "yyyy.MM.d";
    public static final String KOREAN_DATE_YEAR_MONTH_DAY_FORMAT_2 = "yyyy.MM.dd";
    public static final String DATE_YEAR_MONTH_SHORT_YEAR_FORMAT = "yyyy-MM-dd";
    public static final String DATE_LONG_YEAR_MONTH_DAY_FORMAT = "yyyyMMdd";
    public static final String DATE_MONTH_DAY_SHORT_YEAR_FORMAT = "MM/dd/yy";
    public static final String DATE_MONTH_DAY_LONG_YEAR_FORMAT = "MM/dd/yyyy";
    public static final String LONG_DATE_FORMAT = "MMMMM d, yyyy";
    public static final String SHORT_DATE_FORMAT = "MMM dd, yyyy";
    public static final String DATE_MONTH_DAY_LONG_YEAR_NO_DELIMITER_FORMAT = "MMddyyyy";
    public static final String DATE_MONTH_DAY_SHORT_YEAR_NO_DELIMITER_FORMAT = "MMddyy";
    public static final String DAY_OF_YEAT_FORMAT = "DDD";
    
    public static final String TIME_12_HOUR_MINUTE_FORMAT = "hh:mm";
    public static final String TIME_24_HOUR_MINUTE_FORMAT = "H:mm";
    public static final String TIME_12_HOUR_MINUTE_FORMAT2 = "hh:mm a";
    public static final String TIME_24_ZERO_BASED_HOUR_MINUTE_FORMAT = "kk:mm";
    public static final String TIME_12_HOUR_MINUTE_SECOND_FORMAT = "hh:mm:ss a";
    public static final String TIME_24_HOUR_MINUTE_SECOND_FORMAT = "HH:mm:ss";

    private static DateFormat[] dateFormats = { 
    		new SimpleDateFormat(DATE_YEAR_MONTH_SHORT_YEAR_FORMAT), 
    		new SimpleDateFormat(KOREAN_DATE_YEAR_MONTH_DAY_FORMAT_1), 
    		new SimpleDateFormat(DATE_MONTH_DAY_SHORT_YEAR_NO_DELIMITER_FORMAT), 
    		new SimpleDateFormat(LONG_DATE_FORMAT),
    		new SimpleDateFormat(DATE_LONG_YEAR_MONTH_DAY_FORMAT),
    		new SimpleDateFormat(ISI_DATE_TIME_FORMAT),
    		new SimpleDateFormat(DATE_MONTH_DAY_SHORT_YEAR_FORMAT), 
    		new SimpleDateFormat(DATE_MONTH_DAY_LONG_YEAR_FORMAT),
    		new SimpleDateFormat(KOREAN_DATE_YEAR_MONTH_DAY_FORMAT), 
			new SimpleDateFormat("hh.mm"), 
			new SimpleDateFormat(KOREAN_DATE_TIME_FORMAT_2),
			new SimpleDateFormat(DATE_TIME_N0_DELIMITER_FORMAT), 
			new DwSimpleDateFormat("hh:mm a"),
			new SimpleDateFormat(REPORT_DATE_TIME_FORMAT),
			new DwSimpleDateFormat("yyyy/MM/dd, HH:mm:ss"), 
			new SimpleDateFormat(SERVER_DATE_TIME_FORMAT_1), 
			new DwSimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss") 
		};
    
    /**
     * The difference between the time on the RTS and the time on the
     * client. 
     *
     * This is not needed if working with the client.utility.Clock class.
     * -Steinmetz 22 June, 2000
     */
    private static long offset = 0;

    /**
      * Return the current time (GMT), adjusting for discrepancies between
      * the local PC clock and the middleware server.
      */
    public static long currentTimeMillis() {
        //return( Clock.currentTimeMillis());
        return System.currentTimeMillis() + offset;
    }

    // Date version of currentTimeMillis()
    public static Date currentTime() {
        return new Date(currentTimeMillis());
    }

    /**
     * Checks to see if the given datetime is more than 2 minutes from our
     * current datetime. If so, adjusts the offset so our current datetime 
     * agrees with the given datetime.
     * @param serverDate Timestamp provided by the RTS.
     */
    public static void synchronize(Calendar serverDate) {
        long currentTime = currentTimeMillis();
        long serverTime = serverDate.getTimeInMillis();
        long systemTime = System.currentTimeMillis();
        
        logTime("serverTime=", serverTime);
        logTime("currentTime=", currentTime);
        logTime("systemTime=", systemTime);

        if ((currentTime - serverTime > TWO_MINUTES)
                || (serverTime - currentTime > TWO_MINUTES)) {
            Debug.println("Synchronizing local time with server.");
            offset = serverTime - systemTime;
            Debug.println("Current time is now " + currentTime());
            // recompute period and stuff
            try {
                UnitProfile.getInstance().changesApplied();
            }
            catch (Exception e) {
                Debug.printStackTrace(e);
            }
        }
        ExtraDebug.println("offset["+offset+"ms,"+(offset/1000)+"sec,"+(offset/1000/60)
        		+"min,"+(offset/1000/3600)+"hr]" );
    }

    /**
      * Returns the value of this profile item as a long milliseconds.
      */
    public static long timeOfDayMillis(final String timeString) {
        int colonPosition = timeString.indexOf(":");
        long hours = 0;
        try {
        	hours = Long.parseLong(timeString.substring(0,colonPosition));
        } catch (Exception e) {
        }
        long minutes = 0;
        try {
        	minutes = Long.parseLong(timeString.substring(colonPosition + 1));
        } catch (Exception e) {
        }
        final long end = ((HOUR * hours) + (MINUTE * minutes));
        return end;
    }

    /* Convert day of week from the value returned by Calendar to the value
       used in DeltaWorks. The switch statement is used because there is no
       documentation indicating what the range of the Calendar value is. */
    public static int javaToDwDayOfWeek(final int javaDayOfWeek) {
        int dwDayOfWeek = 1;
        switch (javaDayOfWeek) {
            case Calendar.   SUNDAY: dwDayOfWeek = 1; break;
            case Calendar.   MONDAY: dwDayOfWeek = 2; break;
            case Calendar.  TUESDAY: dwDayOfWeek = 3; break;
            case Calendar.WEDNESDAY: dwDayOfWeek = 4; break;
            case Calendar. THURSDAY: dwDayOfWeek = 5; break;
            case Calendar.   FRIDAY: dwDayOfWeek = 6; break;
            case Calendar. SATURDAY: dwDayOfWeek = 7; break;
        }
        return dwDayOfWeek;
    }

    // Get the indicator of whether or not a tranmission is to occur today
    public static int getDwDayOfWeek(final Calendar calendar) {
        int javaDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        return javaToDwDayOfWeek(javaDayOfWeek);
    }

    public static int dwToNormalDayOfWeek(final int dwDayOfWeek) {
        return dwDayOfWeek - 1;
    }

    public static int normalToDwDayOfWeek(final int normalDayOfWeek) {
        return normalDayOfWeek + 1;
    }

    public static int normalizedDayOfWeek(final int dayOfWeek) {
        final int rawDayOfWeek = dayOfWeek % DAYS_IN_WEEK;
        return (rawDayOfWeek < 0) ? (rawDayOfWeek + DAYS_IN_WEEK) : rawDayOfWeek;
    }

    // Returns the day of the week preceding the one specified
    public static int getPreviousDwDayOfWeek(final int dwDayOfWeek) {
        final int normalDayOfWeek = dwToNormalDayOfWeek(dwDayOfWeek);
        final int rawPreviousNormalDayOfWeek = (normalDayOfWeek - 1) + DAYS_IN_WEEK;
        final int previousNormalDayOfWeek = normalizedDayOfWeek(rawPreviousNormalDayOfWeek);
        return normalToDwDayOfWeek(previousNormalDayOfWeek);
    }

    /**
     * Returns the accounting start date/time for the given date/time.
     * In other words, for whatever date/time given, this returns the
     * starting date/time of the period for that time.
     */
    public static long getAccountingStartDateTime(long dateTime) {
        // find account start time from profile
        String accountTime;     // ex.  23:59
        final Profile profile = UnitProfile.getInstance().getProfileInstance();
        try {
            accountTime = profile.find("ACCOUNTING_START_TIME").stringValue();	
        }
        catch (Exception e) {
            accountTime = "00:00";	
            //throw new Exception("Accounting start day not found in unit profile");
        }
        final Calendar calendar = Scheduler.getClonedLocalCalendar(new Date(dateTime));
        Date accountingDate = setTimeOfDay(calendar,accountTime);
        // move back a day if start time AFTER given time
        if (accountingDate.after(new Date(dateTime))) {
            calendar.add(Calendar.DATE, -1);
            accountingDate = calendar.getTime();
        }
        return accountingDate.getTime();
    }

    private static Date setTimeOfDay(final Calendar calendar,
                                     final String timeOfDayString) {
        int hour, minute;
        try {
            StringTokenizer st = new StringTokenizer(timeOfDayString, " :");	
            hour = Integer.parseInt(st.nextToken());
            minute = Integer.parseInt(st.nextToken());
            if (hour == 24)
                hour = 0;
        }
        catch (Exception e) {
            hour = 0;
            minute = 0;
        }
        return setTimeOfDay(calendar,hour,minute);
    }

    private static Date setTimeOfDay(final Calendar calendar,
                                     final int hour,
                                     final int minute) {
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        return calendar.getTime();
    }

    public static void main(String[] args) {

        try {
            // set accounting start time
            String accountTime = "06:00";	
            StringTokenizer st = new StringTokenizer(accountTime, ":");	
            int accountHour = Integer.parseInt(st.nextToken());
            int accountMinute = Integer.parseInt(st.nextToken());

            // set Calendar to today's accounting start time
            long dateTime = System.currentTimeMillis();
            Date dt = new Date(dateTime);

            Calendar localCalendar = Calendar.getInstance();
            localCalendar.setTime(dt);
            localCalendar.set(Calendar.HOUR_OF_DAY, accountHour);
            localCalendar.set(Calendar.MINUTE, accountMinute);
            Date accountingDate = localCalendar.getTime();

            // if start time AFTER given time, move it back a day
            if (accountingDate.after(new Date(dateTime))) {
                localCalendar.add(Calendar.DATE, -1);
                accountingDate = localCalendar.getTime();
          //      Debug.println("Inside 1st if:  accountingDate="+accountingDate);	
            }

            Debug.println("");	
            Debug.println("accountHour ="+accountHour );	
            Debug.println("accountMinute ="+accountMinute );	
            Debug.println("dt            ="+dt );	
            Debug.println("accountingDate="+accountingDate);	

            // after
            dateTime -= (1000 * 60 * 60 * 8);
            dt = new Date(dateTime);
            // move back a day if start time AFTER given time
            if (accountingDate.after(new Date(dateTime))) {
                localCalendar.add(Calendar.DATE, -1);
                accountingDate = localCalendar.getTime();
            }
            Debug.println("");	
            Debug.println("dt            ="+dt );	
            Debug.println("accountingDate="+accountingDate);	

        }
        catch (Exception e) {
            Debug.println(e.toString());
        }

    }
    
    /**
     *  Create a formatted date/time string using the specified time value.
     *  "yyyy/MM/dd, HH:mm:ss, zzzz"
     *  @param pm_timeValue - long value containing specified number of 
     *  milliseconds since the standard base time known as "the epoch"
     */  
	public static String formatTimeForLogging(long pm_timeValue) {
        Calendar gmtCalendar = Calendar.getInstance();
        gmtCalendar.setTime(new Date(pm_timeValue));
        return new SimpleDateFormat(LOG_DATE_TIME_FORMAT).format(gmtCalendar.getTime());
	}
	
    /**
     *  Create a formatted debug log entry using the specified label and time 
     *  value.
     *  @param pm_sLablel - String containing the label to use.
     *  @param pm_timeValue - long value containing specified number of 
     *  milliseconds since the standard base time known as "the epoch"
     */  
	public static void logTime(String pm_sLablel, long pm_timeValue) {
        Calendar gmtCalendar = Calendar.getInstance();
        gmtCalendar.setTimeInMillis(pm_timeValue);
        Debug.println(pm_sLablel + new SimpleDateFormat(LOG_DATE_TIME_FORMAT).format(gmtCalendar.getTime()));
	}

    /**
     *  Creates a formatted GMT offset string.
     *  @param pm_tzTimeZone - TimeZone to create the GMT offset string for
     *  @return a String with the formatted GMT offset "GMT +/- xx:xx"
     */  
	public static String gmtOffset(TimeZone pm_tzTimeZone) {
		int pm_iOffset = pm_tzTimeZone.getRawOffset(); 
        int hour = pm_iOffset / (TimeUtility.HOUR);             
        int minute = Math.abs(pm_iOffset / (TimeUtility.MINUTE)) % 60;
        String sResult = "GMT"
    		+ (hour >= 0 ? "+" : "")+ hour + ":" 
    		+ (minute < 10 ? "0" : "") + minute;
        return sResult;
	}
	
	public static XMLGregorianCalendar toXmlCalendar(long time) {
		Date date = new Date(time);
		return toXmlCalendar(date);
	}
	
	public static XMLGregorianCalendar toXmlCalendar(Date tDate) {
		XMLGregorianCalendar date2=null;
		if (tDate != null) {
			GregorianCalendar c = new GregorianCalendar();
			c.setTime(tDate);
			date2 = null;
			try {
				date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
			} catch (DatatypeConfigurationException e) {
				Debug.println("Error parsing Date[" + tDate + "]");
			} 
		}
		return date2;
	}

	public static XMLGregorianCalendar toXmlCalendarDateOnly(Date tDate) {
		XMLGregorianCalendar date2=null;
		if (tDate != null) {
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTime(tDate);
			try {
				date2 = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DAY_OF_MONTH), DatatypeConstants.FIELD_UNDEFINED);
			} catch (DatatypeConfigurationException e) {
				Debug.println("Error parsing Date[" + tDate + "]");
			} 
		}
		return date2;
	}

	public static Date toDate(XMLGregorianCalendar calendar) {
		if (calendar == null) {
			return null;
		}
		return calendar.toGregorianCalendar().getTime();
	}

	public static Calendar toCalendar(XMLGregorianCalendar calendar) {
		
		if (calendar == null) {
			return null;
		}
		Debug.println("toCalendar:" + calendar.getTimezone());
		return calendar.toGregorianCalendar();
	}

	private static String toUtcDate(String dateStr) {
		for (DateFormat dateFormat : dateFormats) {
			try {
				return new SimpleDateFormat(SERVER_DATE_TIME_FORMAT_1).format(dateFormat.parse(dateStr));
			} catch (ParseException ignore) {
			}
		}
		return dateStr;
	}

	public static String toStringDate(XMLGregorianCalendar calendar, DateFormat dateFormat) {
		if (calendar == null) {
			return null;
		}
		return dateFormat.format(toDate(calendar));
	}

	public static String toStringDate(XMLGregorianCalendar calendar) {
		return toStringDate(calendar, new SimpleDateFormat(TimeUtility.SERVER_DATE_TIME_FORMAT_1));
	}
	
	public static Date toDate(String dateStr) {
		if (dateStr == null) {
			return null;
		}
		try {
			String tDate = toUtcDate(dateStr);

			return new SimpleDateFormat(TimeUtility.SERVER_DATE_TIME_FORMAT_1).parse(tDate);
		} catch (ParseException e) {
		}
		return null;
	}

	public static XMLGregorianCalendar toXmlCalendar(String tDate) {
		return toXmlCalendar(toDate(tDate));
	}

	public static String getTime(XMLGregorianCalendar gCal) {
		if(gCal!=null){
			Date time = gCal.toGregorianCalendar().getTime();
			SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
			String formatted = sdf.format(time);
			return formatted;
		}
		return null;		
	}
	
	public static int daysBetween(Date d1, Date d2) {
        return (int) ((d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
    }	
}
