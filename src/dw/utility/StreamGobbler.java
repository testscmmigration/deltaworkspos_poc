package dw.utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Utility thread class which consumes and displays stream input.
 * 
 * Original code taken from
 * http://www.javaworld.com/javaworld/jw-12-2000/jw-1229-traps.html?page=4
 */
class StreamGobbler extends Thread {
	private final InputStream inputStream;
	private final String streamType;
	private final boolean displayStreamOutput;

	/**
	 * Constructor.
	 * 
	 * @param inputStream
	 *            the InputStream to be consumed
	 * @param streamType
	 *            the stream type (should be OUTPUT or ERROR)
	 * @param displayStreamOutput
	 *            whether or not to display the output of the stream being
	 *            consumed
	 */
	StreamGobbler(final InputStream inputStream, final String streamType,
			final boolean displayStreamOutput) {
		this.inputStream = inputStream;
		this.streamType = streamType;
		this.displayStreamOutput = displayStreamOutput;
		this.setName("Gobbler-" + streamType);
	}

	/**
	 * Consumes the output from the input stream and displays the lines consumed
	 * if configured to do so.
	 */
	@Override
	public void run() {
		try {
			final InputStreamReader inputStreamReader = new InputStreamReader(
					inputStream);
			final BufferedReader bufferedReader = new BufferedReader(
					inputStreamReader);
			String line = null;
			while ((line = bufferedReader.readLine()) != null) {
				if (displayStreamOutput) {
					Debug.println(streamType + ">" + line);
				}
			}
		} catch (final IOException ex) {
			Debug.println("Failed to successfully consume and display the input stream of type "
					+ streamType + "." + ex.getLocalizedMessage());
			Debug.printStackTrace(ex);
		}
	}
}