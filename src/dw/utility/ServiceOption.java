package dw.utility;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.moneygram.agentconnect.DestinationCountryInfo;
import com.moneygram.agentconnect.FeeInfo;
import com.moneygram.agentconnect.GetServiceOptionsResponse;
import com.moneygram.agentconnect.ServiceOptionInfo;

import dw.comm.MessageFacade;
import dw.comm.MessageLogic;
import dw.framework.CommCompleteInterface;
import dw.model.adapters.CountryInfo;
import dw.profile.UnitProfile;
import dw.utility.DwEncryptionAES.DwEncryptionFileException;

public class ServiceOption extends ServiceOptionInfo implements Comparable<ServiceOption> {
	
    private final static long serialVersionUID = 1L;
    
	private static final String SERVICE_OPTIONS_DATA_FILE_NAME = "adp.obj";
	private static final long UPDATE_PERIOD = TimeUtility.HOUR * 23;

	private static Map<String, LanguageServiceOptions> serviceOptionsMaps = new HashMap<String, LanguageServiceOptions>();
    private static LanguageServiceOptions currentServiceOptions = null;
	private static GetServiceOptionsResponse serviceOptionsResponse;
    private static boolean needTransmit = true;
    
	private String destinationCountry;
	private String receiveAgentAbbreviation;
	private String payoutCurrency;
	private String countryAndCurrency;
	
	private static class LanguageServiceOptions implements Serializable {
		private static final long serialVersionUID = 1L;
		
		private String language;
		private long lastUpdateTime;
		private Map<ServiceOptionKey, ServiceOption> map;
		
		private LanguageServiceOptions(Map<ServiceOptionKey, ServiceOption> serviceOptionsMap) {
			this.map = serviceOptionsMap;
		}
	}

	private static class ServiceOptionKey implements Serializable {
		private static final long serialVersionUID = 1L;
		
		String serviceOption;
		String destinationCountry;
		String receiveCurrency;
		String receiveAgentID;
		
		private ServiceOptionKey(String serviceOption, String destinationCountry, String receiveCurrency, String receiveAgentID) {
			this.serviceOption = serviceOption;
			this.destinationCountry = destinationCountry;
			this.receiveCurrency = receiveCurrency;
			this.receiveAgentID = (receiveAgentID == null) || (receiveAgentID.isEmpty()) ? null : receiveAgentID;
		}
		
		@Override
		public boolean equals(Object o) {
			if (o instanceof ServiceOptionKey) {
				if ((serviceOption != null) && (destinationCountry != null) && (receiveCurrency != null)) {
					ServiceOptionKey sok = (ServiceOptionKey) o;
					if ((sok.serviceOption != null) && (serviceOption.equals(sok.serviceOption)) && 
							(sok.destinationCountry != null) && (destinationCountry.equals(sok.destinationCountry)) && 
							(sok.receiveCurrency != null) && (receiveCurrency.equals(sok.receiveCurrency)) && 
							(((receiveAgentID == null) && (sok.receiveAgentID == null)) || ((receiveAgentID != null) && (sok.receiveAgentID != null) && (receiveAgentID.equals(sok.receiveAgentID))))) {
						return true;
					}
				}
			}
			return false;
		}
		
		@Override
		public int hashCode() {
			return (serviceOption != null ? serviceOption.hashCode() : 0) | (destinationCountry != null ? destinationCountry.hashCode() : 0) | (receiveCurrency != null ? receiveCurrency.hashCode() : 0) | (receiveAgentID != null ? receiveAgentID.hashCode() : 0);
		}
	}
    
	public ServiceOption() {
		this.countryAndCurrency = "";
	}

    public ServiceOption(ServiceOptionInfo serviceOption) {
    	this();
		this.setReceiveAgentID(serviceOption.getReceiveAgentID());
		this.setReceiveCurrency(serviceOption.getReceiveCurrency());
		this.setServiceOption(serviceOption.getServiceOption());
		this.setServiceOptionCategoryDisplayName(serviceOption.getServiceOptionCategoryDisplayName());
		this.setServiceOptionCategoryId(serviceOption.getServiceOptionCategoryId());
		this.setServiceOptionDisplayDescription(serviceOption.getServiceOptionDisplayDescription());
		this.setServiceOptionDisplayName(serviceOption.getServiceOptionDisplayName());
    }
	
	public ServiceOption(FeeInfo feeInfo) {
    	this();
		this.setDestinationCountry(feeInfo.getDestinationCountry());
		this.setReceiveAgentID(feeInfo.getReceiveAgentID());
		this.setReceiveCurrency(feeInfo.getValidReceiveCurrency());
		this.setServiceOption(feeInfo.getServiceOption());
		this.setServiceOptionCategoryDisplayName(feeInfo.getServiceOptionCategoryDisplayName());
		this.setServiceOptionCategoryId(feeInfo.getServiceOptionCategoryId());
		this.setServiceOptionDisplayDescription(feeInfo.getServiceOptionDisplayDescription());
		this.setServiceOptionDisplayName(feeInfo.getServiceOptionDisplayName());
	}

	@Override
	public int compareTo(ServiceOption di) {
		int c = this.destinationCountry.compareTo(di.destinationCountry);
		if (c == 0) {
			c = this.receiveCurrency.compareTo(di.receiveCurrency);
			if (c == 0) {
//				if ((this.serviceOptionDisplayName != null) && (di.serviceOptionDisplayName != null)) {
//					c = this.serviceOptionDisplayName.compareTo(di.serviceOptionDisplayName);
//				} else {
					c = this.serviceOption.compareTo(di.serviceOption);
//				}
				if ((c == 0) && (this.receiveAgentID != null) && (di.receiveAgentID != null)) {
					c = this.receiveAgentID.compareTo(di.receiveAgentID);
				}
			}
		}
		return c;
	}

	public boolean compareDeliveryInfo(ServiceOption obj2) {

		if ((obj2 != null) && (this.getServiceOption().equals(obj2.getServiceOption())) && (this.getDestinationCountry().equals(obj2.getDestinationCountry())) && (this.getReceiveCurrency().equals(obj2.getReceiveCurrency()))) {
			if ((this.getReceiveAgentID() != null && obj2.getReceiveAgentID() != null) && (this.getReceiveAgentID().equals(obj2.getReceiveAgentID()))) {
				return true;
			} else if ((this.getReceiveAgentID() == null && obj2.getReceiveAgentID() == null)) {
				return true;
			}
		}
		return false;
	}
	
	public String getReceiveAgentAbbreviation() {
		return receiveAgentAbbreviation;
	}

	public void setReceiveAgentAbbreviation(String receiveAgentAbbreviation) {
		this.receiveAgentAbbreviation = receiveAgentAbbreviation;
	}

	public static void getServiceOptions(final int retrievalType, final boolean closeCommunicationWindow) {
		checkServiceOptionList();
		if (currentServiceOptions != null) {
			if (currentServiceOptions.lastUpdateTime > System.currentTimeMillis() - UPDATE_PERIOD) {
				return;
			}
		}
		serviceOptionsResponse = null;
		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {
				try {
					serviceOptionsResponse = MessageFacade.getInstance().getServiceOptions(retrievalType);
					return Boolean.TRUE;
				} catch (Exception e) {
					return Boolean.FALSE;
				}
			}
		};
		CommCompleteInterface cci = new CommCompleteInterface() {
			@Override
			public void commComplete(int commTag, Object returnValue) {
				boolean b = returnValue instanceof Boolean ? (Boolean) returnValue : false;
				if (b) {
					processServiceOptions();
				}
				
				if (closeCommunicationWindow) {
					MessageFacade.hideDialog(1000);
				}
			}
		};

		MessageFacade.run(logic, cci, 0, Boolean.TRUE);
	}
	
	private static void processServiceOptions() {
		String language = UnitProfile.getInstance().get("LANGUAGE", "en");
		LanguageServiceOptions serviceOptions = serviceOptionsMaps.get(language);
		if (serviceOptions != null) {
			serviceOptions.map.clear();
		} else {
			serviceOptions = new LanguageServiceOptions(new HashMap<ServiceOptionKey, ServiceOption>());
			serviceOptionsMaps.put(language, serviceOptions);
			serviceOptions.language = language;
		}
		serviceOptions.lastUpdateTime = System.currentTimeMillis();
		needTransmit = false;
		
		if ((serviceOptionsResponse != null) && (serviceOptionsResponse.getPayload() != null) && (serviceOptionsResponse.getPayload().getValue().getDestinationCountryInfos() != null)) {
			for (DestinationCountryInfo destinationCountyInfo : serviceOptionsResponse.getPayload().getValue().getDestinationCountryInfos().getDestinationCountryInfo()) {
				for (ServiceOptionInfo serviceOptionInfo : destinationCountyInfo.getServiceOptionInfos().getServiceOptionInfo()) {
					ServiceOptionKey key = new ServiceOptionKey(serviceOptionInfo.getServiceOption(), destinationCountyInfo.getDestinationCountry(), serviceOptionInfo.getReceiveCurrency(), serviceOptionInfo.getReceiveAgentID());
					ServiceOption serviceOption = new ServiceOption(serviceOptionInfo);
					serviceOption.setDestinationCountry(destinationCountyInfo.getDestinationCountry());
					serviceOptions.map.put(key, serviceOption);
				}
			}
		}
		
		try {
			DwEncryptionAES oe = new DwEncryptionAES();
			oe.vInitialize(DwEncryptionAES.OFFSET_ADP_DATA, SERVICE_OPTIONS_DATA_FILE_NAME);
//			ServiceOptionMap data = new ServiceOptionMap(serviceOptionsMap);
			oe.vWriteSaveFile(serviceOptionsMaps);
		} catch (Exception e) {
			Debug.printException(e);
		}
	}

	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}
	
	public String getDestinationCountry() {
		return this.destinationCountry;
	}

	private static void checkServiceOptionList() {
		String language = UnitProfile.getInstance().get("LANGUAGE", "en");
		if ((currentServiceOptions == null) || (! language.equals(currentServiceOptions.language))) {
			if (serviceOptionsMaps.isEmpty()) {
				File file = new File(SERVICE_OPTIONS_DATA_FILE_NAME);
				if (file.exists() && file.isFile()) {
					DwEncryptionAES oe = new DwEncryptionAES();
					oe.vInitialize(DwEncryptionAES.OFFSET_ADP_DATA, SERVICE_OPTIONS_DATA_FILE_NAME);
					try {
						serviceOptionsMaps = (Map<String, LanguageServiceOptions>) oe.readObject();
					} catch (DwEncryptionFileException e) {
						Debug.printException(e);
					}
				}
			}
			if (! serviceOptionsMaps.isEmpty()) {
				currentServiceOptions = serviceOptionsMaps.get(language);
			} else {
				currentServiceOptions = null;
			}
		}
	}
	
	//==========================================================================

	public static List<ServiceOption> getServiceOptions(String countryCode) {
		checkServiceOptionList();
		List<ServiceOption> list = new ArrayList<ServiceOption>();
		if (currentServiceOptions != null) {
			for (ServiceOption serviceOption : currentServiceOptions.map.values()) {
				if (serviceOption.getDestinationCountry().equalsIgnoreCase(countryCode)) {
					list.add(new ServiceOption(serviceOption));
				}
			}
		}
		return list;
	}

	public static List<ServiceOption> getDirectDepositServiceOptions() {
		checkServiceOptionList();
		List<ServiceOption> list = new ArrayList<ServiceOption>();
		if (currentServiceOptions != null) {
			for (ServiceOption serviceOption : currentServiceOptions.map.values()) {
				if (serviceOption.getServiceOptionCategoryId().equals("2")) {
					String countryName = CountryInfo.getCountryName(serviceOption.getDestinationCountry());
					serviceOption.countryAndCurrency = countryName + " - " + serviceOption.getReceiveCurrency();
					list.add(serviceOption);
				}
			}
			Collections.sort(list);
		}
		return list;
	}

	//==========================================================================
	
	public String getServiceOptionDisplayName(ServiceOptionInfo serviceOption) {
		if (serviceOption == null) {
			return null;
		}
		
		return serviceOption.getServiceOptionDisplayName();
	}
	
	public String getServiceOptionDisplayDescription(ServiceOptionInfo serviceOption) {
		return serviceOption.getServiceOptionDisplayDescription();
	}
	
	public boolean isDSS() {
		return getServiceOptionCategoryId().equals("2");
	}

	public static String getServiceOptionDisplayDescription(String serviceOption, String countryCode, String receiveCurrency, String receiveAgentID) {
		String desc = "";
		if (currentServiceOptions != null) {
			ServiceOptionKey key = new ServiceOptionKey(serviceOption, countryCode, receiveCurrency, receiveAgentID);
			ServiceOptionInfo so = currentServiceOptions.map.get(key);
			if (so != null) {
				desc = so.getServiceOptionDisplayDescription();
			}
		}
		return desc;
	}

	public static String getReceiveAgentID(String serviceOption, String countryCode, String receiveCurrency, String receiveAgentID) {
		String desc = "";
		if (currentServiceOptions != null) {
			ServiceOptionKey key = new ServiceOptionKey(serviceOption, countryCode, receiveCurrency, receiveAgentID);
			ServiceOptionInfo so = currentServiceOptions.map.get(key);
			if (so != null) {
				desc = so.getReceiveAgentID();
			}
		}
		return desc;
	}
	
	public static ServiceOption getServiceOption(String serviceOption, String countryCode, String receiveCurrency, String receiveAgentID) {
		checkServiceOptionList();
		ServiceOption so = null;
		if (currentServiceOptions != null) {
			ServiceOptionKey key = new ServiceOptionKey(serviceOption, countryCode, receiveCurrency, receiveAgentID);
			so = currentServiceOptions.map.get(key);
		}
		return so;
	}
	
	public static boolean needTransmit() {
		if (needTransmit) {
			File file = new File(SERVICE_OPTIONS_DATA_FILE_NAME);
			if (file.exists() && file.isFile()) {
				needTransmit = false;
			}
		}
		return needTransmit;
	}

	public void setPayoutCurrency(String payoutCurrency) {
		this.payoutCurrency = payoutCurrency;
	}

	public String getPayoutCurrency() {
		return this.payoutCurrency;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(serviceOptionDisplayName);

		if ((receiveAgentAbbreviation != null) && (! receiveAgentAbbreviation.isEmpty())) {
			sb.append(" - ").append(receiveAgentAbbreviation);
		} else if ((receiveAgentID != null) && (! receiveAgentID.isEmpty())) {
			sb.append(" - ").append(receiveAgentID);
		}

		if ((payoutCurrency != null) && (! payoutCurrency.isEmpty())) {
			sb.append(" - ").append(payoutCurrency);
		}
		return sb.toString();
	}

	public String getCountryAndCurrency() {
		return this.countryAndCurrency;
	}
}
