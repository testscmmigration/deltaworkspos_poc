
package dw.utility;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.TreeMap;

import dw.i18n.Messages;

/**
 * Guidelines is a utility class that is used to extract the receive guidelines
 * for a specific country.
 */

public class Guidelines {
    
    private TreeMap<String, String> map;
    private static Guidelines instance;   
    private String guidelines = "xml/countryguidelines";
    private int COUNTRYCODE = 3;    
    private int LEGACYCODE = 4;
    private int RECV = 9;
    private int DESCRIPTION = 13;
    
    private Guidelines() {
        map = new TreeMap<String, String>();
        String newguideline = "";
        String language = Messages.getCurrentLocale().toString();
        boolean loaded = false;

        if (!language.equals("en"))
            guidelines = guidelines + "_" + language + ".csv";
        else
            guidelines = guidelines + ".csv"; 
             
        CSVParser parser = new CSVParser(guidelines); 
        
        try {
            parser.process();
            loaded = true;
		} catch (FileNotFoundException e) {
			Debug.printStackTrace(e);
		} catch (IOException e) {
			Debug.printStackTrace(e);
		}
          
        if (loaded) {      
            for (int index = 1; index < parser.size(); index++) {
                String countrycode = parser.getField(index - 1, COUNTRYCODE - 1);
                String legacycode = parser.getField(index - 1, LEGACYCODE - 1);
                String receivecountry = parser.getField(index - 1, RECV - 1);
                String description = parser.getField(index - 1, DESCRIPTION - 1);
                if (!receivecountry.equals("")) {
                   if (!map.containsKey(countrycode)) {
                       map.put(countrycode, description);
                       map.put(legacycode, description);
                   }
                   else
                      newguideline = map.get(countrycode);
                   
                   newguideline = newguideline + "\r\n" + description;
                   map.put(countrycode, newguideline);
                   map.put(legacycode, newguideline);
                   newguideline = "";
                }              
            }
        }       
    }
    
    public static Guidelines getInstance(){
        if (instance == null)
           instance = new Guidelines();
        return instance;
    }
    
    public static void nullInstance() {
        instance = null;
    }
    
    public String getGuideLines(String code){
        return map.get(code);
    }

    public static void main(String[] args) {
        Messages.setCurrentLocale("en");
        Guidelines g = getInstance();
        Debug.println(g.getGuideLines("IRL"));
        Debug.println(g.getGuideLines("QAT"));
    }
}
