/*
 * Scheduler.java
 * 
 * $Revision$
 *
 * Copyright (c) 2000-2005 MoneyGram International
 */

package dw.utility;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

import dw.comm.MessageFacade;
import dw.comm.MessageFacadeParam;
import dw.comm.MessageLogic;
import dw.framework.WaitToken;
import dw.profile.Profile;
import dw.profile.ProfileItem;
import dw.profile.UnitProfile;

public class Scheduler implements Runnable {

    private static final int FIVE_MINUTES = 5 * TimeUtility.MINUTE;
    private static final int TWO_AM = 2 * TimeUtility.HOUR;

    private static final int DEFAULT_SLEEP_INTERVAL = FIVE_MINUTES;
    
    private static final long ZERO_MIDNIGHT = getZeroMidnight();

    private static final NumberFormat nf1 = getNumberFormat(1);
    private static final NumberFormat nf2 = getNumberFormat(2);

    private static boolean noMOTransmits = true;   
    
    private static long getZeroMidnight() {
        return (new GregorianCalendar(1990, 0, 1, 0, 0, 0)).getTimeInMillis();
    }

    private static NumberFormat getNumberFormat (final int minimumIntegerDigits) {
        final NumberFormat nf = NumberFormat.getInstance();
		nf.setMinimumIntegerDigits(minimumIntegerDigits);
		nf.setGroupingUsed(false);
		return nf;
    }

    private static TimeZone createTimeZone(final String name,
                                           final int gmtOffsetMillis,
                                           final boolean usesDaylightSavingsTime) {
        // Create the time zone based on the offset
        final SimpleTimeZone timeZone = new SimpleTimeZone(
                                                        gmtOffsetMillis,name);
        if(timeZone.inDaylightTime(TimeUtility.currentTime()))
			Debug.println("Daylight savings in effect for time zone");
		
        // Add any daylight savings information to the time zone
       
        if (usesDaylightSavingsTime) {
            timeZone.setStartRule(
                    Calendar.MARCH,            // Daylight savings start month
                    2,                         // Daylight savings start week
                    Calendar.SUNDAY,           // Daylight savings start day
                    TWO_AM);                   // Daylight savings start time
            timeZone.setEndRule(
                    Calendar.NOVEMBER,          // Daylight savings end month
                    1,                        // Daylight savings end week (last)
                    Calendar.SUNDAY,           // Daylight savings end day
                    TWO_AM);                   // Daylight savings end time
        }
        return timeZone;
    }

    // Define a nested time parsing method
    static class TimeParser {
        private String timeString = null;

        public TimeParser(String ts) {
            timeString = ts;
        }

        public int parse(final int hourLength, final int minutePos) {
            String hourString = timeString.substring(0,hourLength);
            if (hourLength < 2)
                hourString = "0" + hourString;	
            String minuteString;
            if (minutePos < 0)
                minuteString = "00";	
            else
                minuteString = timeString.substring(minutePos);
            return (int)(TimeUtility.timeOfDayMillis(hourString + ":" +	
                                                               minuteString));
        }
    }

    /**
     * Create the local time zone based on the time zone specified and
     * daylight savings flag in the profile. The time zone format must be
     * GMT[+|-][h]h[[:]mm].
     */
    public synchronized static TimeZone createLocalTimeZone()
    		throws IllegalArgumentException {
    	String timeZoneString = null;   	
    	if (currentLocalTimeZone == null) {
    		Debug.println("****creating local timezone from profile ");
    		// Get the time zone specifier from the profile
    		//            final Profile profile = UnitProfile.getUPInstance().getInstance();
    		//            final String timeZoneString =
    		//                               profile.find("AGENT_TIME_ZONE").stringValue();	
    		timeZoneString = UnitProfile.getInstance().get("AGENT_TIME_ZONE_NAME", "");
    		Debug.println("AGENT_TIME_ZONE_NAME = \"" + timeZoneString + "\"");
    		if(timeZoneString.length() != 0){
    			currentLocalTimeZone  = TimeZone.getTimeZone(timeZoneString);
    			if(currentLocalTimeZone.inDaylightTime(TimeUtility.currentTime()))
    				Debug.println("Daylight savings in effect for "+ timeZoneString +"time zone");
    		}	
    		else{
    			Debug.println(" TimeZone Name is blank. Using AGENT_TIME_ZONE and DST");
    			timeZoneString = UnitProfile.getInstance().get("AGENT_TIME_ZONE", "GMT-06:00");	 

    			// Strip off the 'GMT' prefix, leaving the offset
    			if (!(timeZoneString.startsWith("GMT")))	
    			{
    				Debug.println("timeZoneString does not start with GMT");
    				throw new IllegalArgumentException
    				("Time zone does not begin with 'GMT'."); 
    			}
    			final String signedTimeString = timeZoneString.substring(3);

    			// Determine the sign of the offset
    			int sign = 1;
    			boolean signed = false;
    			if (signedTimeString.startsWith("+"))	
    				signed = true;
    			else if (signedTimeString.startsWith("-")) {	
    				sign = -1;
    				signed = true;
    			}
    			// Strip off the sign from the offset
    			final String timeString = signed ? signedTimeString.substring(1) :
    				signedTimeString;
    			// Parse the offset and convert it to time of day in milliseconds
    			final int colonPos = timeString.indexOf(":");	
    			final int timeStringLength = timeString.length();
    			boolean timeFormatValid = true;

    			TimeParser parser = new TimeParser(timeString);

    			// Do the parsing and converting
    			int timeOfDayMillis = 0;
    			switch (colonPos) {
	    			case -1: switch (timeStringLength) { 
		    			case 1: timeOfDayMillis = parser.parse(1,-1); break;
		    			case 2: timeOfDayMillis = parser.parse(2,-1); break;
		    			case 3: timeOfDayMillis = parser.parse(1, 1); break;
		    			case 4: timeOfDayMillis = parser.parse(2, 2); break;
		    			default: timeFormatValid = false;
	    			} break;
	    			case  1: timeOfDayMillis = parser.parse(1,2); break;
	    			case  2: timeOfDayMillis = parser.parse(2,3); break;
	    			default: timeFormatValid = false;
    			}
    			if (!timeFormatValid)
    				throw new IllegalArgumentException("Invalid time format"); 

    			//            final boolean clientUsesDaylightSavingsTime =
    			//                                     profile.find("AGENT_DST").booleanValue();	

    			boolean clientUsesDaylightSavingsTime = "Y".equals(UnitProfile.getInstance().get("AGENT_DST", "Y"));   

    			currentLocalTimeZone = createTimeZone("Local",sign * timeOfDayMillis, 
    					clientUsesDaylightSavingsTime);

    		}    
    	}
    	//        else {       
    	//        	currentLocalTimeZone = POSTimeZoneAdapter.getInstance().buildTimeZone(TimeZoneCorrection.getInstance());
    	//        	Debug.println("****creating local timezone from a checkin correction ********* ");      	
    	//        }
    	TimeZone.setDefault(currentLocalTimeZone);
    	Debug.println("Scheduler.createLocalTimeZone() currentLocalTimeZone=" + currentLocalTimeZone.getDisplayName()); 
    	Debug.println(currentLocalTimeZone.toString());
    	return currentLocalTimeZone;

    }
    
    /**
     * Returns the time zone of the host, which is CST with a period of
     * daylight savings. CST is 6 hours behind GMT.
     */
    public static TimeZone getHostTimeZone() {
        if (hostTimeZone == null)
            hostTimeZone = createTimeZone("Host", -6 * TimeUtility.HOUR, true); 
        return hostTimeZone;
    }

    private static TimeZone currentLocalTimeZone;
    private static volatile TimeZone hostTimeZone;

    // Get the calendar based on the local time zone
    public static synchronized Calendar getLocalCalendar() {
        if (currentLocalCalendar == null) {
            final TimeZone localTimeZone = createLocalTimeZone();
            currentLocalCalendar = Calendar.getInstance(localTimeZone);
        }
       currentLocalCalendar.setTime(TimeUtility.currentTime());
        return currentLocalCalendar;
    }

    // Get the calendar based on the host time zone
    public static Calendar getHostCalendar() {
        if (hostCalendar == null) {
            final TimeZone hostTimeZone = getHostTimeZone();
            hostCalendar = Calendar.getInstance(hostTimeZone);
        }
        return hostCalendar;
    }

    private static volatile Calendar currentLocalCalendar;
    private static volatile Calendar hostCalendar;

    /**
     * Get a copy of the local calendar with the time set to the specified
     * date
     */
    public static Calendar getClonedLocalCalendar(Date date) {
        final Calendar calendar = (Calendar)(getLocalCalendar().clone());
        calendar.setTime(date);
        return calendar;
    }

    // Get a copy of the local calendar for the specified time in milliseconds
    private static Calendar getClonedLocalCalendar(final long millis) {
        return getClonedLocalCalendar(new Date(millis));
    }

    // Get a copy of the local calendar for the current date and time
    public static Calendar getClonedLocalCalendar() {
        return getClonedLocalCalendar(TimeUtility.currentTimeMillis());
    }

    /**
     * Returns time of day in milliseconds for the specified calendar
     */
    public static long timeOfDay(final Calendar calendar) {
        final int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        final int minuteOfHour = calendar.get(Calendar.MINUTE);
        return (hourOfDay * TimeUtility.HOUR) + (minuteOfHour * TimeUtility.MINUTE);
    }

    /**
     * Returns time of day in milliseconds for the current local calendar
     */
    private static synchronized long timeOfDay() {
        return timeOfDay(currentLocalCalendar);
    }

    // Public version, where the current local calendar must be obtained first
//    public static long getTimeOfDay() {
//        return timeOfDay(getLocalCalendar());
//    }

    /**
     * Returns time of day in milliseconds for the specified date
     */
    public static long timeOfDay(final Date date) {
        return timeOfDay(getClonedLocalCalendar(date));
    }

    // Returns day of year for the specified Calendar
    private static int dayOfYear(final Calendar calendar) {
        return calendar.get(Calendar.DAY_OF_YEAR);
    }

    // Returns day of year for the current local calendar
    private static synchronized int dayOfYear() {
        return dayOfYear(currentLocalCalendar);
    }

    // Returns day of year for the specified Date
    private static int dayOfYear(final Date date) {
        return dayOfYear(getClonedLocalCalendar(date));
    }

    // Returns the number of days from January 1, 1990 to the specified date
    private static long universalDay(final Date date) {
        return (date.getTime() - ZERO_MIDNIGHT) / TimeUtility.DAY;
    }
    
    public static synchronized void clearCurrentLocalCalendar() {
    	currentLocalCalendar = null;
    }

    /** get date only. Returns a Date which equals the specified date
    * minus the time of day, based on the specified calendar.
    */
    public static Date getDateOnly (Calendar calendar, Date dateTime) {
    	DateFormat format = new SimpleDateFormat(TimeUtility.DATE_MONTH_DAY_LONG_YEAR_FORMAT);
    	format.setCalendar(calendar);
        String dateTimeString = format.format(dateTime);
        return parseDateTime(dateTimeString, format);
    }

    /** get date only. Returns a Date which equals the specified date
    * minus the time of day, based on the specified calendar.
    */
    public static long getLongDateOnly (final Calendar calendar,
                                        final Date dateTime) {
        return getDateOnly(calendar,dateTime).getTime();
    }

    public static Calendar getClonedHostCalendar(Date dateTime) {
    	final Calendar calendar = (Calendar)getHostCalendar().clone();
    	calendar.setTime(dateTime);
        return (Calendar)getHostCalendar().clone();
    }

    /** get date only. Returns a Date which equals the specified date
    * minus the time of day, based on the local calendar.
    */
    public static long getLongDateOnly (final Date dateTime) {
        return getLongDateOnly(getClonedLocalCalendar(),dateTime);
    }

    /**
     * Parse the date and time string according to the specified format
     * and return a Date object
     */
    private static Date parseDateTime(String dateTimeString, DateFormat format) {
        try {
            return format.parse(dateTimeString);
        } catch (ParseException pe) {
            return null;
        }
    }

    /**
      * Format the given time as HHM, where HH is the hour in 24-hour
      * format, and M is the number of 6-minute increments. The date is GMT
      * synched with the middleware.
      */
    public static String formatHHM(final long current) {
        final Calendar calendar = getClonedLocalCalendar(current);
        final StringBuffer buffer = new StringBuffer();
        buffer.append(nf2.format(calendar.get(Calendar.HOUR_OF_DAY)));
        buffer.append(nf1.format(calendar.get(Calendar.MINUTE) / 6));
        return buffer.toString();
    }

    private int sleepInterval = DEFAULT_SLEEP_INTERVAL;
    private String iterationMessage;

    // Constructors
    public Scheduler() {
        this(null);
    }

    public Scheduler(String iterationMessage) {
        this(DEFAULT_SLEEP_INTERVAL, iterationMessage);
    }

    public Scheduler(int sleepInterval, String iterationMessage) {
        this.sleepInterval = sleepInterval;
        this.iterationMessage = iterationMessage;
    }

    // Check on the dispenser
    private void checkDispenserStatus() {
        
        // This is going to try to instanciate Dispenser every two hours,
        // for no particularly good reason. I'm ruling this an obsolete
        // requirement which only made sense when every DeltaWorks install
        // had a dispenser.
        
//        final long lastTimeMillis =
//                            UnitProfile.getUPInstance().getLastDispenserCheckTime().getTime();
//        if ((TimeUtility.currentTimeMillis() - lastTimeMillis) >= TWO_HOURS) {
//            DispenserInterface dispenser = DispenserFactory.getDispenser(1);
//            String message = null;
//            // Since this message won't be updated for another two hours,
//            // statements like "is unlocked" and "is powered off" are not
//            // really appropriate.
//            switch (dispenser.queryStatus()) {
//                case DispenserInterface.NOT_INITIALIZED:
//                case DispenserInterface.NO_CTS:
//                case DispenserInterface.WAITING:
//                case DispenserInterface.EOF:
//                case DispenserInterface.ERROR:
//                case DispenserInterface.LOAD_REQUIRED:
//                    message = Resources.getString("Scheduler.Dispenser_load_is_required._14"); break; 
//            }
//            if (message != null)
//                ErrorManager.setStatus(message, ErrorManager.WARNING_STATUS);
//            UnitProfile.getUPInstance().setLastDispenserCheckTime(TimeUtility.currentTime());
//        }
    }

    // Get the indicator of whether or not a tranmission is to occur today
    public static synchronized ProfileItem getHostTransmitToday() {
        final int dwDayOfWeek = TimeUtility.getDwDayOfWeek(currentLocalCalendar);
        final Profile profile = UnitProfile.getInstance().getProfileInstance();
        return profile.find("HOST_XMIT_DAY",dwDayOfWeek);	
    }

    // See about doing the daily transmit of the diag log
    private String checkDailyDiagLogTransmit(final MessageFacadeParam parameters) {
    	//        try {
        // See if transmission has already been done today
        final int dayOfYear = dayOfYear();
        final Date lastTransmissionTime = UnitProfile.getInstance().getLastDailyTransmitTime();
        //Debug.println("the last time we succeeded!!! should it change?: " + lastTransmissionTime);	
        final int lastTransmissionDayOfYear = dayOfYear(lastTransmissionTime);
        if (dayOfYear == lastTransmissionDayOfYear)
            return "No more transmission attempts to occur today 1"; 

        // See if transmissions are to be done on this day of the week
        if (!(getHostTransmitToday().booleanValue()))
            return "Transmission not to occur today 2"; 

        // Determine which transmit time to use
        final Date lastAttemptedTime = UnitProfile.getInstance().getLastAttemptedDailyTransmitTime();
        final int lastAttemptedDayOfYear = dayOfYear(lastAttemptedTime);
        final long lastAttemptedTimeOfDay = timeOfDay(lastAttemptedTime);
        final long timeOfDay = timeOfDay();
        long transmitTime;
        String ordinal;
        final Profile profile = UnitProfile.getInstance().getProfileInstance();

        long firstTransmitTime = profile.find("HOST_XMIT_TIME_1").
        timeOfDayValue();
        
        long secondTransmitTime = profile.find("HOST_XMIT_TIME_2").
        timeOfDayValue();

        // make sure we have at least one good transmit time
        if (firstTransmitTime == -1 && secondTransmitTime == -1)
            return "No Valid Transmit Times Found."; 
        else if (firstTransmitTime == -1)
            firstTransmitTime = secondTransmitTime;
        else if (secondTransmitTime == -1)
            secondTransmitTime = firstTransmitTime;

        // If no transmissions have been attempted yet today
        if (dayOfYear != lastAttemptedDayOfYear) {
            transmitTime = firstTransmitTime;
            ordinal = "First"; 
        }
        // else if the second attempt has not been made yet
        else if (lastAttemptedTimeOfDay < secondTransmitTime) {
            transmitTime = secondTransmitTime;
            ordinal = "Second"; 
        }
        else
            return "No more transmits to be attempted today 3"; 

        // See if the time to transmit has been reached yet
        if (timeOfDay < transmitTime)
            return "Not time to transmit yet 4"; 

        // Do the transmission and report the results
        //    Debug.println("about to transmit diags in Scheduler!!!!!!!!");	

        UnitProfile.getInstance().setLastAttemptedDailyTransmitTime(TimeUtility.currentTime());

        /* transmit item from item log  and Diags to the host */
        if (!(ErrorManager.transmitAll(false))) {
            Debug.println("returning in Scheduler!!"); 
            return ordinal + " transmission of the day failed"; 
        }
       
        if (noMOTransmits) {
        	MessageLogic logic2 = new MessageLogic() {
                @Override
				public Object run() {              	
                    return MessageFacade.getInstance().transmitMoneyOrderTotals(parameters, "", BigInteger.ZERO, BigDecimal.ZERO, null);
                }
            };           
            MessageFacade.run(parameters, logic2, null);       	
        }
        else {
        	noMOTransmits = true;
        }
        
        return "Transmit succeeded"; 
    }

    // Computes the number of calendar days since the given date
    public static long daysElapsed(Date date) {
        final long pastDay = universalDay(date);
        final long today = universalDay(TimeUtility.currentTime());
//        Debug.println("daysElapsed: Today["+TimeUtility.formatTimeForLogging(TimeUtility.currentTime().getTime())
//        		+"] - PastDay["+TimeUtility.formatTimeForLogging(date.getTime())+"] = "+(today - pastDay));
        return today - pastDay;
    }

    /**
     * Checks to see if a new day has started, in which case the daily financial total needs to be reset
     */
    private void checkDailyTotalReset() {
        final Date lastResetTime = UnitProfile.getInstance().getLastDailyTotalResetTime();
        if (daysElapsed(lastResetTime) > 0) {
            UnitProfile.getInstance().clearDailyTotal();
            UnitProfile.getInstance().clearDailyCount();
            UnitProfile.getInstance().setLastDailyTotalResetTime(TimeUtility.currentTime());
        }
    }
    private void checkDailyBillPayTotalReset() {
        final Date lastResetTime = UnitProfile.getInstance().getLastDailyBillPayTotalResetTime();
        if (daysElapsed(lastResetTime) > 0) {
            
            UnitProfile.getInstance().clearDailyBillPayTotal();
            UnitProfile.getInstance().setLastBillPayDailyTotalResetTime(TimeUtility.currentTime());
        }
    }

    private boolean keepGoing = true;
    private WaitToken wait = new WaitToken();
    private Object sleepAlarm = new Object();

    /**
     * Checks the dispenser status every 2 minutes and performs the daily
     * transmit for the diag log
     */
    @Override
	public void run() {
        String transmissionResult;

        // Start with a sleep, so we don't interfere with startup
        // processing. For example, checking the dispenser status
        // when the dispenser hasn't yet been created by the main
        // panel will cause DispenserFactory to create the Dispenser,
        // which will do a diag write, which may create the diag log
        // before the main panel has gotten around to it. Which is
        // all well and good, but it can interfere with the file
        // integrity checking that main panel and State are doing.
        // -- gea
        try {
            Thread.sleep(sleepInterval);
        }
        catch (InterruptedException e) {}

        synchronized (wait) {
        	wait.setWaitFlag(true);
            while (keepGoing) {
                if (iterationMessage != null)
                    Debug.println(iterationMessage);

                // Update currentLocalTimeZone and currentLocalCalendar
                getLocalCalendar();

                if (!keepGoing)
                    break;

                checkDispenserStatus();

                if (!keepGoing)
                    break;

                // these checks should not be performed in DEMO mode
                if (! UnitProfile.getInstance().isDemoMode()) {
                	MessageFacadeParam parameters = new MessageFacadeParam(MessageFacadeParam.HIDE_WITHOUT_FOCUS_UNTIL_ERROR);
                	transmissionResult = checkDailyDiagLogTransmit(parameters);                   
                    if (iterationMessage != null)
                        Debug.println(" " + transmissionResult);	
                }

                if (!keepGoing)
                    break;


		/* we could put a condition here to force a manual transmit
		 * of the diag log, or we could add a different handler in
		 * ErrorListener to put up a warning or something else.
		if (DiagLog.getInstance().isNearlyFull()) {
		    ErrorManager.transmitDiags(true);
		}

		if (!keepGoing)
		    break;

		*/

                checkDailyTotalReset();
                checkDailyBillPayTotalReset();

                if (!keepGoing)
                    break;

                Period.autoCloseAccountingPeriod();

                if (!keepGoing)
                    break;

                // check for POS Awol status and disable all transactions if it is
                if (UnitProfile.getInstance().isAWOLTooLong()) {
                    ErrorManager.setAWOLStatus(true);
                }

                if (!keepGoing)
                    break;

                try {
                    synchronized (sleepAlarm) {
                        sleepAlarm.wait(sleepInterval);
                    }
                }
                catch (InterruptedException e) {}
            }
        	wait.setWaitFlag(false);
        }
    }

    /**
     * Stops operation of the thread.
     */
    public void halt() {
        // Signal the scheduler thread to stop iterating
        keepGoing = false;

        // Wake up the forwarder thread if it's sleeping
        synchronized (sleepAlarm) {
            sleepAlarm.notify();
        }

        // Wait for the scheduler thread to terminate so that it is not
        // terminated prematurely by System.exit() in the calling method
        synchronized (wait) {
//    		try {
//    			while (wait.isWaitFlag()) {
//					wait.wait();
//				}
//			} catch (InterruptedException e) {
//				Debug.printException(e);
//	        }
    	}
    }
	/**
	 * @param noMOTransmits The noMOTransmits to set.
	 */
	public static void setNoMOTransmits(boolean noMOTransmits) {
		Scheduler.noMOTransmits = noMOTransmits;
	}
	
	/**
     * For Vista and Windows 7 we cannot trust the default TimeZone 
     * to be set correctly, so we need to do it ourselves.
	 */
	public static void verifyTimeZone() {
        float fOsVersion = 0;
		try {
			String sOsVersion = System.getProperty("os.version");
			fOsVersion = Float.valueOf(sOsVersion).floatValue();

		} catch (Exception e) {
			Debug.println("Exception getting OS Version[" + e.getMessage()+"]");
		}
        if (fOsVersion >= 6.0) {
    		try {
				TimeZone localTimeZone = getLocalCalendar().getTimeZone();
				Debug.println("**Local TimeZone["
						+ localTimeZone.getDisplayName() + ", "
						+ TimeUtility.gmtOffset(localTimeZone) + "]"); 
				TimeZone.setDefault(localTimeZone);
			} catch (Exception e) {
				Debug.println("Exception while fixing the TimeZone["
						+ e.getLocalizedMessage() + "]");
			}
        }
		
	}
}

