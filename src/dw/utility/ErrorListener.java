package dw.utility;

/**
 * Callback interface that notifies about various errors, warnings, and 
 * status updates.
 * 10/31/2000 added mustHandleLogs
 */
public interface ErrorListener {
    
    public void warningOccurred(String message, Exception e);
    
    public void criticalErrorOccurred(String message, Exception e);
    
    public void fatalErrorOccurred(String message, Exception e);
    
    public void debugEventOccurred(String message, Exception e);

    // The following events are not errors, so they arguably should be in
    //   another interface, but since they are all meant as callbacks to main
    //   panel (the only implementer), this is a convenient place for them
    //   short term...RLC

    // give a message of a given severity to the user
    public void setStatus(String message, int severity);

    // invoked when the host has signaled a software version change
    //public void softwareChanged();

    // invoked when a software download has started
    public void softwareDownloadStarted();

    // invoked when a software download has completed (and success of it)
    public void softwareDownloadFinished(boolean successful, String resultString);

    // invoked when the a diag transmit is needed
    public boolean transmitAll(boolean manual);

    //invoked when the logs need to be transmitted/cleared (full, corrupt, etc.)
    public void mustHandleLogs();

    // invoked when a diag transmit has started
    public void diagTransmitStarted();

    // invoked when a diag transmit has completed
    public void diagTransmitFinished(boolean promptForRestart);

    // invoked when an item transmission has started
    //public void itemTransmissionStarted();

    // invoked when an item transmission has finished
    //public void itemTransmissionFinished();
    
    // ENH - 1133
    // invoked when a ftt transmission has started
    public void ftpFileStarted();

    // ENH - 1133
    // invoked when a ftp transmission has finished
    public void ftpFileFinished();

    // set whether or not the POS is in an AWOL state
    public void setAWOLStatus(boolean awol);

    // update whether or not Reprint Receipt is available
    public void updateReprintReceiptState();

    /**
     * set authorization of the POS on the client side
     */
    public void setPosAuthorization(boolean authorized); 


    // set whether or not the POS is in training mode
    public void setTrainingMode(boolean training);
    
    public void setProgressBarString(String message);
    
    public void setProgressBarProgress(int progress);
    
    public void setProgressBarMaxValue(int maxValue);
    
    public void setProgressBarMinValue(int minValue);
    
}
