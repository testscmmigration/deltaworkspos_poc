/*
 * MainPanelDelegate.java
 * 
 * $Revision$
 *
 * Copyright (c) 2005 MoneyGram International
 */
package dw.main;

import com.moneygram.agentconnect.SubagentInfo;

import dw.profile.UnitProfile;


/**
 * Allows methods to notify main panel of various things without having
 * a compile-time dependency on DeltaworksMainPanel.
 * 
 * This class could probably replace ErrorListener and PageNameInterface
 * someday. It does replace calls of the form 
 * <pre>
 *         DeltaworksMainPanel.getMainPanel().getIsiExeCommand()
 * with
 *         MainPanelDelegate.getIsiExeCommand()
 * </pre>
 * 
 * Methods that return a value will typically throw NullPointerException
 * if called before the main panel has been constructed. Methods that 
 * return void will simply do nothing in that case.
 *
 * Further refactoring may be in order, but this class, together with 
 * MainPanelHooks, at least gets some unexpected dependencies out into the open.
 * 
 * @author Geoff Atkin
 */
public class MainPanelDelegate {

    private static MainPanelHooks delegate;
    
    /*
     * Start including audit log abbreviations also, so in the future we can
     * be consistent between the two.
     */
	public static final String clMoneyGramSend = "MGS";
	public static final String clBillPayment = "EXP";
	public static final String clMoneyOrder = "MOD";
	public static final String clMoneyGramReceive = "MGR";
	public static final String clCardReload = "RLD";
	public static final String clCardPurchase = "PUR";
	public static final String clVendorPayment = "VPD";
	public static final String clTranQuote = "MGQ";
	public static final String clDirectoryBillers = "DOB";
	public static final String clMoneyGramSendReversal = "REV";
	public static final String clPromoCodeData = "PCD";
	public static final String clAmend = "AMD";
	public static final String clMoneyGramReceiveReversal = "RRT";
	public static final String clTransactionStatus = "TST";
	public static final String clMGSendDetailedAmounts = "SDA";
	public static final String clSendReceiptText1 = "ST1";
	public static final String clSendReceiptText2 = "ST2";
	public static final String clFormFreeSend = "MGP";
	public static final String clAccountDeposit = "MGA";

    public static void executeIsiBackCommand() {
        if (delegate != null) {
            delegate.executeIsiBackCommand();
        }
    }

    public static void executeIsiFrontCommand() {
        if (delegate != null) {
            delegate.executeIsiFrontCommand();
        }
    }

    public static boolean getIsiAllowDataCollection() {
        return delegate.getIsiAllowDataCollection();
    }

    public static boolean getIsiRequireRcptTextData() {
        return delegate.getIsiRequireRcptTextData();
    }

    public static boolean getIsiAppendMode() {
        return delegate.getIsiAppendMode();
    }

    public static boolean getIsiExeCommand() {
        return delegate.getIsiExeCommand();
    }
    
    public static boolean getIsiExe2Command() {
        return delegate.getIsiExe2Command();
    }
    
    public static String getIsiExeProgramArguments() {
        return delegate.getIsiExeProgramArguments();
    }
    
    public static String getIsiExe2ProgramArguments() {
        return delegate.getIsiExe2ProgramArguments();
    }
    
    public static boolean getIsiRpCommandValid() {
        return delegate.getIsiRpCommandValid();
    }
    
    public static boolean getIsiSkipRecord() {
        return delegate.getIsiSkipRecord();
    }

    public static boolean getIsiTransactionInProgress() {
        return delegate.getIsiTransactionInProgress();
    }
    
    public static boolean getIsiFrontCommandOccurred() {
        return delegate.getIsiFrontCommandOccurred();
    }

    public static boolean getIsiControlledLaunchCardReload() {
        return delegate.getIsiControlledLaunchCardReload();
    }

    public static boolean getIsiControlledLaunchCardPurchase() {
        return delegate.getIsiControlledLaunchCardPurchase();
    }

    
    // NF ISI information
    public static boolean getIsiNfCommand() {
        return delegate.getIsiNfCommand();
    }
    
    public static String getIsiNfProgramArguments() {
        return delegate.getIsiNfProgramArguments();
    }

    public static void initializeIsiFields() {
        if (delegate != null) {
            delegate.initializeIsiFields();
        }
    }

    public static void languageChanged() {
        if (delegate != null) {
            delegate.languageChanged();
        }        
    }

    public static void refreshMainPanel() {
        if (delegate != null)
            delegate.refreshMainPanel();
    }
    
    public static void refreshToggleButton() {
        if (delegate != null)
            delegate.getIsiExeProgramArguments();
    }

    public static void setConnectivityChange(boolean b) {
        if (delegate != null) {
            delegate.setConnectivityChange(b);
        }
    }
    
    public static void setDeauthMessage(boolean deauthMessage) {
        if (delegate != null) {
            delegate.setDeauthMessage(deauthMessage);
        }
    }
    
    public static void setDeActivateMessage(boolean deActivateMessage) {
        if (delegate != null) {
            delegate.setDeActivateMessage(deActivateMessage);
        }
    }
    
    // for use by DeltaWorksMainPanel or DeltaworksStartup
    protected static void setDelegate(MainPanelHooks mainPanel) {
        delegate = mainPanel;
    }

    public static void setIsiAllowDataCollection(boolean b) {
        if (delegate != null) {
            delegate.setIsiAllowDataCollection(b);
        }
    }

    public static void setIsiRequireRcptTextData(boolean b) {
        if (delegate != null) {
            delegate.setIsiRequireRcptTextData(b);
        }
    }

    public static void setIsiAppendMode(boolean b) {
        if (delegate != null) {
            delegate.setIsiAppendMode(b);
        }
    }

    public static void setIsiControlledLaunchCardPurchase(boolean b) {
        if (delegate != null) {
            delegate.setIsiControlledLaunchCardPurchase(b);
        }
    }

    public static void setIsiControlledLaunchCardReload(boolean b) {
        if (delegate != null) {
            delegate.setIsiControlledLaunchCardReload(b);
        }
    }

    public static void setIsiExeCommand(boolean b) {
        if (delegate != null) {
            delegate.setIsiExeCommand(b);
        }
    }

    public static void setIsiExe2Command(boolean b) {
        if (delegate != null) {
            delegate.setIsiExe2Command(b);
        }
    }

    public static void setIsiExeProgramArguments(String string) {
        if (delegate != null) {
            delegate.setIsiExeProgramArguments(string);
        }
    }
    
    public static void setIsiExe2ProgramArguments(String string) {
        if (delegate != null) {
            delegate.setIsiExe2ProgramArguments(string);
        }
    }
    
    public static void setIsiSkipRecord(boolean b) {
        if (delegate != null) {
            delegate.setIsiSkipRecord(b);
        }
    }

    public static void setIsiRpCommandValid(boolean b) {
        if (delegate != null) {
            delegate.setIsiRpCommandValid(b);
        }
    }
    
    public static void setIsiTransactionInProgress(boolean b) {
        if (delegate != null) {
            delegate.setIsiTransactionInProgress(b);
        }
    }
 
    public static void setIsiFrontCommandOccurred(boolean b) {
        if (delegate != null) {
            delegate.setIsiFrontCommandOccurred(b);
        }
    }

    public static void setPosAuthorization(boolean authorized) {
        if (delegate != null) {
            delegate.setPosAuthorization(authorized);
        }
    }

    public static void setTransactionEnabled() {
        if (delegate != null) {
            delegate.setTransactionEnabled();
        }
    }

    public static void setupCancelISI(String string) {
        if (delegate != null) {
            delegate.setupCancelISI(string);
        }
    }

    public static void setupNonFinancialISI() {
        if (delegate != null) {
            delegate.setupNonFinancialISI();
        }
    }
    
    public static void setupErrorISI(String string) {
        if (delegate != null) {
            delegate.setupErrorISI(string);
        }
    }
    
    public static void setupControlledLaunchISI(String string) {
        if (delegate != null) {
            delegate.setupControlledLaunchISI(string);
        }
    }
    
    public static boolean isValidateNfLaunchPage() {
        if (delegate != null) {
            return delegate.isValidateNfLaunchPage();
         
        }
       return false;
    }

    public static void executeIsiProgram(String programName){
        if (delegate != null) {
            delegate.executeIsiProgram(programName);
        }    	
    }
    
    // NF ISI information
    public static void setIsiNfCommand(boolean b) {
        if (delegate != null) {
            delegate.setIsiNfCommand(b);
        }
    }

    public static void setIsiNfProgramArguments(String string) {
        if (delegate != null) {
            delegate.setIsiNfProgramArguments(string);
        }
    }
    
    
    public static void tryExit(int code) {
        if (delegate != null) {
            delegate.tryExit(code);
        }
    }
    
    public static void changeToSuperAgent(final boolean idleTimeOutLong) {
        if (delegate != null) {   
        	 final SubagentInfo newAgent = UnitProfile.getInstance().getAgentInfo();
        	 if(newAgent==null)
        	 	delegate.getAgentList(idleTimeOutLong );
        	 else
                delegate.changeToSuperAgent(idleTimeOutLong);
        }
    }

    /**
     * 
     */
    public static void periodChanged() {
        if (delegate != null) {
            delegate.periodChanged();
        }
    }
    
}
