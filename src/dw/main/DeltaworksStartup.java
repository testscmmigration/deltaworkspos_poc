/*
 * DeltaworksStartup.java
 * 
 * $Revision$
 *
 * Copyright (c) 2013 MoneyGram, Inc. All rights reserved
 */

package dw.main; 
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.Security;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

import dw.comm.FakeResponse;
import dw.comm.MessageFacade;
import dw.comm.MessageFacadeParam;
import dw.comm.MessageLogic;
import dw.comm.SOAPMessageHandler;
import dw.dialogs.Dialogs;
import dw.dispenser.ExtraRanges;
import dw.framework.CommCompleteInterface;
import dw.framework.PauseSemaphore;
import dw.framework.WaitToken;
import dw.framework.XmlDefinedPanel;
import dw.i18n.Messages;
import dw.io.AuthenticationException;
import dw.io.CCILog;
import dw.io.State;
import dw.io.Transmitter;
import dw.io.report.AuditLog;
import dw.io.report.ReportLog;
import dw.isi.IsiServer;
import dw.mgreceive.MoneyGramReceiveTransaction;
import dw.mgsend.MoneyGramSendTransaction;
import dw.model.adapters.CountryInfo;
import dw.pdfreceipt.PdfReceipt;
import dw.pinpad.Pinpad;
import dw.printing.EpsonSpooledPrinter;
import dw.printing.EpsonTMT88II;
import dw.printing.GenericPrinter;
import dw.printing.TEPrinter;
import dw.profile.UnitProfile;
import dw.prtreceipt.PrtReceipt;
import dw.softwareversions.SoftwareVersionUtility;
import dw.utility.Debug;
import dw.utility.ErrorManager;
import dw.utility.ExtraDebug;
import dw.utility.ReceiptTemplates;
import dw.utility.Scheduler;
import dw.utility.TimeUtility;
import dw.utility.VerifyJar;
import flags.CompileTimeFlags;

/**
 *  This class is basically a cleanup of the DeltaworksMainPanel class.
 *  DeltaworksMainPanel constructor had a lot of methods that need to be done 
 *  even before the main panel is constructed. It is  important to do this 
 *  because we don't want to repeat all the method calls while switching 
 *  languages.
 *  this class contains the main method now.
 */
public class DeltaworksStartup {

	private static final String SYSTEM_PROPERTY_HTTPS_PROTOCOLS = "https.protocols";
	private static final String HTTPS_PROTOCOLS = "TLSv1.2";

    private static volatile DeltaworksStartup instance; // singleton instance

//    private DeltaworksMainPanel mainPanel;
	
    //  controls when chronologically triggered events occur
    private Scheduler scheduler = null;
    private Thread schedulerThread = null;
    // exit codes to send back to indicate app exit status
    public static final int ERROR_EXIT = 1;

    private static final String DNS_CACHE_TTL= "30";
	private static Pattern DWISI_PATTERN = Pattern
			.compile("\\d\\d\\d\\d\\d\\d\\\\dwisi.exe");

    //  persistent storage for pertinent app information
    private State state = null;

//    private ItemForwarder itemForwarder = null;
//    private Thread itemForwarderThread = null;
    private Transmitter transmitter = null;
    private Thread transmitterThread = null;

    //  responds to remote requests for reports
//    private Thread ftpThread = null;

    // responds to remote ISI requests
   private IsiServer isiServer = null;
   private Thread isiThread = null;
   private String toFrontCommand = "-FRONT";

   //  semaphores for pausing / resuming software updates and item transmission
    private PauseSemaphore softwareDownloadSemaphore = new PauseSemaphore();
    private PauseSemaphore itemTransmissionSemaphore = new PauseSemaphore();

    //  public static boolean modemSpkr = false;

    /* keep a reference to a dialog that is over the main panel, so if one is
      created before panel is shown, we can bring it to the front.*/
    private static Runnable pendingDialog = null;
    //whether or not we have connected to host successfully
    private boolean connected = false;

    private static boolean killScheduledTransmit = false;
    
    private boolean startingFirstTime = false;

    //Set true for displaying logging not encrypted
    private static boolean enableExtraDebug = CompileTimeFlags.debug();		
    //Set true for displaying logging not encrypted
    private static boolean disableDebugEncryption = CompileTimeFlags.debug();	
    
    private Container contentPane;
    
    //************************************************************************
    //PCI (Payment Card Industry) Definitions
    //************************************************************************
    
    //    PCI Script (12.3 and older) exit codes:
    //
	//    100 - Not a MoneyGram Owned asset, do not run script.
	//    101 - PCI Script has already been run, this is a NT4 machine.
	//    102 - PCI Script has already been run, this is a W2K machine.
	//    103 - PCI Script has already been run, this is a XP machine.
	//    **104 - Not SuperValu machine, NT4 workstation, not logged in as Administrator; unable to run script.
	//    105 - Not SuperValu machine, W2K workstation, not logged in as Administrator; script has been run.
	//    106 - Not SuperValu machine, XP workstation, not logged in as Administrator; script has been run.
	//    107 - Not SuperValu machine, NT4 workstation, logged in as Administrator; script has been run.
	//    108 - Not SuperValu machine, W2K workstation, logged in as Administrator; script has been run.
	//    109 - Not SuperValu machine, XP workstation, logged in as Administrator; script has been run.
	//    **110 - Is a SuperValu machine, NT4 workstation, not logged in as svadmin user; unable to run script.
	//    111 - Is a SuperValu machine, W2K workstation, not logged in as svadmin user; script has been run.
	//    112 - Is a SuperValu machine, XP workstation, not logged in as svadmin user; script has been run.
	//    113 - Is a SuperValu machine, NT4 workstation, logged in as svadmin script; has been run.
	//    114 - Is a SuperValu machine, W2K workstation, logged in as svadmin script; has been run.
	//    115 - Is a SuperValu machine, XP workstation, logged in as svadmin script; has been run.
    
    
    //    PCI Script (12.4 and newer) exit codes:
    //
	//    200 - Not a MoneyGram Owned asset, do not run script.
	//    201 - PCI Script v2.0 has already been run, this is a NT4 machine.
	//    202 - PCI Script v2.0 has already been run, this is a W2K or XP machine.
	//    203 - PCI Script v1.0 has already been run, NT4 workstation.
	//    204 - Not SuperValu machine, PCI Script v1.0 has already been run, W2K or XP machine, not logged in as Administrator, PCI Script v2.0 has been run.
	//    205 - Not SuperValu machine, PCI Script v1.0 has already been run, W2K or XP machine, logged in as Administrator, PCI Script v2.0 has been run.
	//    206 - Is a SuperValu machine, PCI Script v1.0 has already been run, W2K or XP machine.
	//    **207 - Not SuperValu machine, NT4 workstation, not logged in as Administrator; unable to run scripts.
	//    208 - Not SuperValu machine, W2K or XP workstation, not logged in as Administrator; both scripts have been run.
	//    209 - Not SuperValu machine, NT4 workstation, logged in as Administrator; PCI Script v1.0 has been run.
	//    210 - Not SuperValu machine, W2K or XP workstation, logged in as Administrator; both scripts have been run.
	//    **211 - Is a SuperValu machine, NT4 workstation, not logged in as svadmin user; unable to run scripts.
	//    212 - Is a SuperValu machine, W2K or XP workstation, not logged in as svadmin user; PCI Script v1.0 has been run
	//    213 - Is a SuperValu machine, NT4 workstation, logged in as svadmin; PCI Script v1.0 has been run.
	//    214 - Is a SuperValu machine, W2K or XP workstation, logged in as svadmin script; PCI Script v1.0 has been run.
    
    //    PCI Script (14.1 and newer) exit codes:
    //
	//    300 - No mguser, this is not a MoneyGram owned machine
	//    301 - OS is not Windows 2000 or Windows XP
	//    302 - Most recent version of the script has already completed
	//    303 - 12.4 version of the script has already been run, apply current changes only
	//    304 - 12.4 version of the script has not been run, apply 12.4 changes and current changes
	//    305 - No version of the script has ever completed, needed to apply all changes (pre-12.4, 12.4, and current)
    
    //    PCI Script (14.11 and newer) exit codes:
    //    400 - Not a MoneyGram owned PC (mguser account does not exist)
    //    401 - OS is not Windows XP or Windows 2000
    //    402 - Script has already run
    //    403 - Script completed successfully



    /* Original PCI exit code values*/
//  private static final int PCI_MINIMUN_EXIT_CODE = 100;
//  private static final int PCI_MAXIMUN_EXIT_CODE = 115;
//  private static final int[] PCI_EXIT_CODES_TO_CAUSE_RERUN = {104, 110}; //Not logged in as ADMIN on NT4 Machines
  
    /* 2013 PCI exit code values*/
//    private static final int PCI_MINIMUN_EXIT_CODE = 200;
//    private static final int PCI_MAXIMUN_EXIT_CODE = 214;
//    private static final int[] PCI_EXIT_CODES_TO_CAUSE_RERUN = {207, 211}; //Not logged in as ADMIN on NT4 Machines

    /* 2014 PCI exit code values*/
//    private static final int PCI_MINIMUN_EXIT_CODE = 300;
//    private static final int PCI_MAXIMUN_EXIT_CODE = 305;
    
    /* 2015 PCI exit code values*/
    private static final int PCI_MINIMUN_EXIT_CODE = 400;
    private static final int PCI_MAXIMUN_EXIT_CODE = 403;
    private static final int[] PCI_EXIT_CODES_TO_CAUSE_RERUN = {}; //Assume no more NT PCs
    private static final String PCI_EXIT_CODE_FILE = "ExitCode.txt";

    private int intPciExitCode;
    private String strError;

    /**
     * Default constructor.
     */
    private DeltaworksStartup() {
    }
    
    /**
     * Perform all the one-time startup initializations and then
     * construct the main panel. 
     * @param versionNumber
     */
    private void startup(String versionNumber) {
        setStartingFirstTime(true);
        Properties props = System.getProperties();
        props.setProperty(SYSTEM_PROPERTY_HTTPS_PROTOCOLS, HTTPS_PROTOCOLS);
        Security.setProperty("networkaddress.cache.ttl", DNS_CACHE_TTL);
        Messages.setCurrentLocale("en"); // don't have unit profile loaded yet,
                                     // so this will set locale to default value
                                     // only needed if we have to display an error
                                     // before we load the profile 
        
        //Use the State.getInstance() method instead to avoid occasional inappropriate
        // fatal errors
        if ((state = State.getInstance()) == null) {
            ErrorManager.handle(new IOException(Messages.getString("DeltaworksMainPanel.1942")), 
            		"stateInstantiationError", Messages.getString("ErrorMessages.stateInstantiationError"), 
            ErrorManager.FATAL, true);
            System.exit(ERROR_EXIT);
        }
        //load the profile from persistence storage
        loadProfile();

//        Debug.println("**** URL_DEBUGENCRYPTION  = " 
//        	+ UnitProfile.getInstance().get("URL_DEBUGENCRYPTION", "N"));
//        Debug.println("**** URL_EXTRADEBUG  = " 
//        	+ UnitProfile.getInstance().get("URL_EXTRADEBUG", "N"));
        
        if (UnitProfile.getInstance().get("URL_DEBUGENCRYPTION", "N").equals("Y") 
        		|| disableDebugEncryption) {
        	dw.comm.AgentConnect.enableDebugEncryption();
        	//Debug.println("----- Debug Encryption OFF -----");
        }
        else {
       	 	dw.comm.AgentConnect.disableDebugEncryption();
        	//Debug.println("----- Debug Encryption ON -----");
        }
        
        if (UnitProfile.getInstance().get("URL_EXTRADEBUG", "N").equals("Y") 
        		|| enableExtraDebug) {
        	SOAPMessageHandler.enableExtraDebug();
        	ExtraDebug.setDebug(true);
       	 	//Debug.println("----- Extra debug info ON -----");
        }
        else  {
        	SOAPMessageHandler.disableExtraDebug();
        	ExtraDebug.setDebug(false);
        	//Debug.println("----- Extra debug info OFF -----");
        }

	    String workingDir = System.getProperty("user.dir");
        String sDwDir = workingDir + "\\" + versionNumber+ "\\";
        
        if (!VerifyJar.isJarVerified(sDwDir+"dwGui.jar") 
        		|| !VerifyJar.isJarVerified(sDwDir+"dwXml.jar")
        		|| !VerifyJar.isJarVerified(sDwDir+"comm.jar")
        		|| !VerifyJar.isJarVerified(sDwDir+"jdun.jar")
        		) {
            Dialogs.showError("dialogs/DialogFatalError.xml", 
            		Messages.getString("DialogFatalError.CorruptJar")); 
            System.exit(ERROR_EXIT);
        }

        /*
         * Run PCI (Payment Card Industry) script if not previously run 
         * to prevent admin account running on MoneyGram PCs
         */
        checkIfPciRunSuccessfully();

        // Attempt to initialize Pinpad only if it's enabled in Profile
        Pinpad.INSTANCE.initialize();
               
        Debug.println("setCurrentPeriod(" + UnitProfile.getInstance().getCurrentPeriod() 
        		+ ") " + TimeUtility.currentTime());	 
        
        Debug.println(""); 
        Debug.println("Starting Deltaworks " + versionNumber); 
        	
        Debug.println("Java version " + System.getProperty("java.runtime.version"));
        
		try {
			String sOsName = System.getProperty("os.name");
			String sOsVersion = System.getProperty("os.version");
			Debug.println("OS[" + sOsName +"]");
			Debug.println ("OS Version[" + sOsVersion +"]");

		} catch (Exception e) {
			Debug.println("Exception getting OS Data[" + e.getMessage()+"]");
		}
        	  
        String serviceTag = UnitProfile.getInstance().get("DEVICE_SERVICE_TAG","");
        if ((serviceTag != null) && (serviceTag.length() > 0)) {
            Debug.println("serviceTag[" + serviceTag + "]"); 
        }
       	String hostName;
		try {
			hostName = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			hostName = "";
		}
       	if ((hostName != null ) && hostName.length() > 0) {
            Debug.println("HostName[" + hostName + "]"); 
       	}
       	
        String aID = UnitProfile.getInstance().get("AGENT_ID", "");
        String pID = UnitProfile.getInstance().get("AGENT_POS_SEQ", "");
        Debug.println("ID[" + aID + "," + pID + "]"); 

       	Locale l = Locale.getDefault();
        String s = l.toString();
        UnitProfile.getInstance().set("LOCALE", s);
        Debug.println("Locale[" + s + "]"); 
        
        if (UnitProfile.getInstance().isDemoMode()) {
            // For setup/install transaction, change screen language to match 
            // system locale.
            Debug.println("Default locale = " + s);
            if (s.startsWith("es")) {
                setLanguages("es", "en");
            }
            else if (s.startsWith("de")) {
                setLanguages("de", "en");
            }
            else if (s.startsWith("fr")) {
                setLanguages("fr", "en");
            }
            else if (s.startsWith("cn")) {
                setLanguages("cn", "en");
            }
            else if (s.startsWith("pl")) {
                setLanguages("pl", "en");
            }
            else if (s.startsWith("ru")) {
                setLanguages("ru", "en");
            }
            else if (s.startsWith("pt")) {
                setLanguages("pt", "en");
            }
            else {
                setLanguages("en", "es");
            }
        }
        
        TimeZone hwTimeZone = TimeZone.getDefault();
        Debug.println("HW TimeZone[" + hwTimeZone.getDisplayName()+", "
        		+ TimeUtility.gmtOffset(hwTimeZone) + "]"); 
        File fileAudit = new File (AuditLog.AUDIT_FILE_NAME_BASE);
        String auditfilename = AuditLog.AUDIT_FILE_NAME_BASE; 
        
        if (!fileAudit.exists()){
	        Debug.println("Audit Trail doesn't exist ");
	        try {
				fileAudit.createNewFile();
			} catch (IOException e) {
				Debug.println("Unable to create audit file" + auditfilename);
			}
        }
             
        File fileISI = new File ("dwisi.exe");
        String filename = versionNumber+ "\\dwisi.exe"; 
		Matcher m = DWISI_PATTERN.matcher(filename);
        
        if (!fileISI.exists() && m.matches()) {
	        Debug.println("Executable doesn't exist copying file "+ filename + 
	        		" to dwisi.exe");
	        try {
				SoftwareVersionUtility.copyFile(filename, "dwisi.exe");
			} catch (IOException e) {
				Debug.println("Unable to copy file " + filename);
			}
        }
        
		if ("Y".equals(UnitProfile.getInstance().get("UPDATE_DWISI", "N"))
				&& m.matches()) {
			if (fileISI.exists()) {
				boolean b = fileISI.delete();
		        if (! b) {
		        	Debug.println("File " + fileISI.getName() + " could not be deleted");
		        }
			}

			Debug.println("Attemping to copy file " + filename
					+ " to dwisi.exe");

			try {
				UnitProfile.getInstance().set("UPDATE_DWISI", "N", "P", true);
				SoftwareVersionUtility.copyFile(filename, "dwisi.exe");
			} catch (IOException e) {
				Debug.println("Unable to copy file " + filename);
				UnitProfile.getInstance().set("UPDATE_DWISI", "Y", "P", true);
			}
		}

        // true locale now that we have profile loaded
        Messages.setCurrentLocale(UnitProfile.getInstance().get("LANGUAGE", "en"));    
              
        /*
         * Setup the line compression value (just in case).
         */
        UnitProfile.getInstance().initEpsonLineCompression();
        
        //get the printer, dispenser, and logs ready to go
        Debug.println("Initializing resources");
        initializeResources();
        
        // Postpone setting up proxy properties until now so the above
        // socket to local host doesn't try to use a proxy server.
        Debug.println("Updating proxy properties");
        UnitProfile.getInstance().updateProxyProperties();
        // check for the necessity of sending the service tag and dispenser id
        setInitialProfileItems();
        // for Wal-mart: write the store number to a file
        saveStoreNumber();
        //set the version of the app that we get from the command line
        UnitProfile.getInstance().setVersionNumber(versionNumber);

        /*
         * For Vista and Windows 7 we cannot trust the default TimeZone 
         * to be set correctly, so we need to verify the TimeZone setup.  
         */
        Scheduler.verifyTimeZone();
        
        //check in with the host
        checkInWithHost(true, CountryInfo.MANUAL_TRANSMIT_RETERIVAL);
        
        if (UnitProfile.getInstance().getUnitProfileID() != 0) {
        	CountryInfo.checkCountryInformation(CountryInfo.MANUAL_TRANSMIT_RETERIVAL);
        }

        final JFrame f = new JFrame();
        f.getContentPane().setLayout(new BorderLayout());
        DeltaworksMainPanel mainPanel = DeltaworksMainPanel.create(f);
        
        //set up the icon image for the main window to the cool TEMG logo.
        //Moving these here from the main panel constructor.
        String iconFileName = "xml/images/temg_window.gif"; 
        java.net.URL url = ClassLoader.getSystemResource(iconFileName);
        ImageIcon icon = null;
        if (url != null)
            icon = new ImageIcon(url);
        else
            icon = new ImageIcon(iconFileName);
        f.setIconImage(icon.getImage());
        f.setLocation(XmlDefinedPanel.getLocationForCenter(f));
               
        MyWindowAdapter wndAdaptor = new MyWindowAdapter();
        f.addWindowListener(wndAdaptor);
        
        // this allows us to intercept the close before the window hides
        f.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        f.setResizable(true);
        
        f.getContentPane().add(BorderLayout.CENTER, mainPanel);
        mainPanel.setupFrameComponents();
        f.setLocation(XmlDefinedPanel.getLocationForCenter(f));
        
        // Set the minimum size of the application program window.

        ///f.setMinimumSize(new Dimension(DWValues.DW_MINIMUM_WIDTH, DWValues.DW_MINIMUM_HEIGHT));
        
        f.setVisible(true);

        Dialogs.setParent(f);
        //see if we have any pending dialog from the constructor of mainPanel
        if (pendingDialog != null) {
            pendingDialog.run();
            pendingDialog = null;
        }

        //Record memory state
        Runtime runtime = Runtime.getRuntime();
        long max = runtime.maxMemory();
        long total = runtime.totalMemory();
        long free = runtime.freeMemory();
        long used = (total - free) / 1024;
        Debug.println("MaxMemory[" + max/1024 +"]");
        Debug.println("TotalMemory[" + total/1024 +"]");
        Debug.println("usedMemory[" + used +"]");
        MoneyGramSendTransaction.populateSendReceiptLanguageList();
        MoneyGramReceiveTransaction.populateRecvReceiptLanguageList();
        ReceiptTemplates.initialize();
        
        /*
         * Access all potential PDF files, and setup their Time to Live (TTL).
         */
        PrtReceipt.setReprintReceiptFileDeletion();
        PdfReceipt.setReprintReceiptFileDeletion();
        
        contentPane = f.getContentPane();
    }
    
    public Container getContentPane() {
    	return this.contentPane;
    }

    /**
     * Sets language and alt language in the unit profile, but does not
     * call Messages.setCurrentLocale().
     */
    private void setLanguages(String current, String alt) {
        UnitProfile.getInstance().set("ALT_LANGUAGE", alt);
        UnitProfile.getInstance().set("LANGUAGE", current);
    }

    /**
     * Returns the singleton instance.
     */
    public static DeltaworksStartup getInstance() {
    	if(instance==null)
    	{
    		instance = new DeltaworksStartup();
    	}
        return instance;
    }

    /**
     * Load the profile into memory from its local storage home. Gather any
     *   tags that are needed in this class.
     */
    private void loadProfile() {
        try {
            try {
                state.createProfile();
            } catch (AuthenticationException e) {
                ErrorManager.handle(e, "authenticationException", Messages.getString("ErrorMessages.authenticationException"), 
                ErrorManager.ERROR, true);
                MainPanelDelegate.setPosAuthorization(false);
            }
        } catch (Exception e) {
            // Unable to load the profile from the persistent state, so exit.
            ErrorManager.handle(e, "profileLoadError", Messages.getString("ErrorMessages.profileLoadError"), ErrorManager.FATAL, true); 
            System.exit(ERROR_EXIT);
        }
    }


    /**
     * Get some resources ready for use. Printer, dispenser,
     * item forwarder, scheduler.
     */
    private void initializeResources() {
        // get SSL ready to go
        //initializeSSL();
        
        Debug.println("Checking ISI port...");
        int port = getIsiPort();

        if (port > 0) {

            //try to connect to port
            try {
                Socket s = new Socket("localhost", port);
                if (s.isConnected()) {
                    OutputStream o = s.getOutputStream();
                    byte[] b = toFrontCommand.getBytes();
                    o.write(b);
                    o.flush();
                    o.close();
                    s.close();
                    Debug.println("DeltaWorks already running; used ISI to bring it to front; exiting.");
                    System.exit(1);
                }

            }
            catch (UnknownHostException e1) {
                Debug.printStackTrace(e1);
            }
            catch (ConnectException e1) {
                Debug.println(" not connected yet; Carry on");
            }
            catch (IOException e1) {
                Debug.printStackTrace(e1);
            }
        }
       
        // start up the scheduler process
        Debug.println("Starting up scheduler process...");
        try {
            scheduler = new Scheduler();
            schedulerThread = new Thread(scheduler);
        } catch (Exception e) {
            // Could not start up the Scheduler, so exit.
            /*   ErrorManager.handle(e, "schedulerInstantiationError", 
                                                         ErrorManager.FATAL, true);*/
            ErrorManager.handle(e, "applicationError", Messages.getString("ErrorMessages.applicationError"), ErrorManager.FATAL, true); 
            System.exit(ERROR_EXIT);
        }

        // removed creation the Diagnostic log file in DW4.0

        // write a diagnostic indicating the application was started
        //DiagLog.getInstance().writePowerOn();


        // create the Currency Compliance Info log file
        Debug.println("Creating CCI log...");
        try {
            CCILog.getInstance();
        } catch (AuthenticationException e) {
            ErrorManager.handle(e, "authenticationException", Messages.getString("ErrorMessages.authenticationException"), 
            ErrorManager.ERROR, true);
            MainPanelDelegate.setPosAuthorization(false);
        } catch (Exception e) {
            // Could not start up the CCD log, so exit.
            /*  ErrorManager.handle(e, "ccdLogInstantiationError",    
                      ErrorManager.ERROR, true); */
            ErrorManager.handle(e, "applicationError", Messages.getString("ErrorMessages.applicationError"), ErrorManager.ERROR, true); 
            System.exit(ERROR_EXIT);
        }

        // create the Report log file
        Debug.println("Creating report log...");
        try {
            ReportLog.getInstance();
        } catch (AuthenticationException e) {
            ErrorManager.handle(e, "authenticationException", Messages.getString("ErrorMessages.authenticationException"), 
            ErrorManager.ERROR, true);
            MainPanelDelegate.setPosAuthorization(false);
        } catch (Exception e) {

            ErrorManager.handle(e, "applicationError", Messages.getString("ErrorMessages.applicationError"), ErrorManager.ERROR, true); 
            System.exit(ERROR_EXIT);
        }

        // set up the extra ranges of used serial numbers (uses report log)
        Debug.println("Loading range data...");
        ExtraRanges.getInstance();

        // Get dispenser ready for action. Moved it down here (it had
        // been first) because it writes a diag for port open.
//        Debug.println("Setting up dispenser...");

        // Check with print instance needs to be loaded.
        if (UnitProfile.getInstance().get("USE_AGENT_PRINTER","N").equals("N") || UnitProfile.getInstance().get("USE_AGENT_PRINTER","NP").equals("NP")){
        	boolean done;
        	int     cnt = 0;
        	GenericPrinter.clearInstance();
        	EpsonSpooledPrinter.clearInstance();
        	do {
        		Debug.println("Clearing generic printer and setting up Epson printer...");
        		EpsonTMT88II.getInstance();
        		done = (TEPrinter.getPrinter() != null);
        		if (!done) {
        			int result = Dialogs.showOkCancel("dialogs/DialogPrinterNotReady.xml");
        			cnt++;
        			EpsonTMT88II.clearInstance();
        			if (result != 0){
        				done = true;
        			}
        		}
        	} while (!done && (cnt < 3));
        }
        else if (UnitProfile.getInstance().get("USE_AGENT_PRINTER","S").equals("S") || UnitProfile.getInstance().get("USE_AGENT_PRINTER","SP").equals("SP")){
        	boolean done;
        	int     cnt = 0;
        	GenericPrinter.clearInstance();
        	EpsonTMT88II.clearInstance();
        	do {
        		Debug.println("Clearing generic printer and setting up Epson printer with spooler...");
        		EpsonSpooledPrinter.getInstance();
        		done = (TEPrinter.getPrinter() != null);
        		if (!done) {
        			int result = Dialogs.showOkCancel("dialogs/DialogPrinterNotReady.xml");
        			cnt++;
        			EpsonSpooledPrinter.clearInstance();
        			if (result != 0){
        				done = true;
        			}
        		}
        	} while (!done && (cnt < 3));
        }
        else{
        	boolean done;
        	int     cnt = 0;
        	EpsonTMT88II.clearInstance();
        	EpsonSpooledPrinter.clearInstance();
        	do {
        		Debug.println("Clearing Epson printer and setting up generic printer...");
        		GenericPrinter.getInstance();
        		done = (TEPrinter.getPrinter() != null);
        		if (!done) {
        			int result = Dialogs.showOkCancel("dialogs/DialogPrinterNotReady.xml");
        			cnt++;
        			GenericPrinter.clearInstance();
        			if (result != 0){
        				done = true;
        			}
        		}
        	} while (!done && (cnt < 3));
        }

        // set up the transmit cycle thread in Transmitter.
        try {
            transmitter = new Transmitter(itemTransmissionSemaphore);
            transmitterThread = new Thread(transmitter);
        }
        catch (IOException e1) {
            Debug.println("IOException caught setting up transmitter.");
            Debug.printStackTrace(e1);
        } 

        // create the ISI server process
        Debug.println("Starting ISI service...");
        try {
            isiServer = new IsiServer();
            isiThread = new Thread(isiServer);
        } catch (Exception e) {
            Debug.println("unable to start the ISI Server: " + e); 
        }
    }
    
    /**
     * This functions reads the file port.txt and retrieves the port number
     * that the ISI communication is using.
     * @return port number
     */
    private int getIsiPort(){
        int portValue = 0;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(
            		new FileInputStream("port.txt"))); 
            String line = null;
            line = br.readLine();
            if (line != null) {
            	portValue = Integer.parseInt(line);
            }
            br.close();
            return portValue;
        } catch (Exception e) {
            portValue = -1;
        }
        return portValue;
   }

    public static void junitSetup() {

    	if (instance == null) {
    		instance = new DeltaworksStartup();
    	}

        // set up the transmit cycle thread in Transmitter.
    	
        try {
            instance.transmitter = new Transmitter(instance.itemTransmissionSemaphore);
            instance.transmitterThread = new Thread(instance.transmitter);
        }
        catch (IOException e1) {
            Debug.println("IOException caught setting up transmitter.");
            Debug.printStackTrace(e1);
        } 
    }
    

    /**
     * These items need to be sent to the host each time we start the application
     * in case we are on a different PC or have a different dispenser.
     */
    private void setInitialProfileItems() {
        // check the status of these 2 profile items
        Thread t = new Thread(new Runnable() {
            @Override
			public void run() {
                UnitProfile.getInstance().setServiceTag(collectServiceTag());
            }
        });
        t.start();
    }

    /**
     * Returns the BIOS serial number or service tag number. This implementation
     * assumes that the startup script has already executed Dell's MOTOINFO.EXE
     * or IBM's WINBIOS.EXE or the equivalent, and stored the output in a file
     * motoinfo.out.  This implementation does not change the profile (the calling
     * method does that.) Both the colon and the equal sign are recognized as
     * delimiters. If the number can not be found, it is set to the empty string.
     *
     * Note: This really ought to specify the DOS character encoding.
     */
    private String collectServiceTag() {
        String serviceTag = ""; 
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(
            		new FileInputStream("motoinfo.out"))); 
            String line = null;
            // look for the Serial Number: <service tag> line
            while ((line = br.readLine()) != null) {
                if (line.indexOf("Serial") >= 0) { 
                    StringTokenizer tok = new StringTokenizer(line, "=:"); 
                    tok.nextToken(); // ignore the 'Serial Number:'
                    serviceTag = tok.nextToken().trim(); // grab the serial #
                }
            }
            Debug.println("Service tag = " + serviceTag); 
        } catch (Exception e1) {
            Debug.println("No service tag: " + e1); 
            serviceTag = ""; 
        } finally {
        	if (br != null) {
        		try {
        			br.close();
        		} catch (Exception e2) {
        		}
        	}
        }
        return serviceTag;
    }

    /** Look up the store number in the profile and write it to a file for WalMart. */
    private void saveStoreNumber() {
        try {
            FileOutputStream out = new FileOutputStream("serial.txt"); 
            try {
                String msds = UnitProfile.getInstance().get(
                		"MO_AGENT_ID", "000000000000");  
                    String store = "Serial Number:\tTEMG001S0" 
                      +msds.substring(8) + "US\r\n"; 
                out.write(store.getBytes());
            } finally {
                out.close();
            }
        } catch (Exception e) {
        }
    }

    /**
     * Reset all fonts to Dialog 12. As of 1.4, Java defaults to the
     * font specified in control panel-display properties, which is
     * often smaller than the default font used in previous versions
     * of Java. On Windows 2000 with the Windows Classic theme it
     * triggers Sun bug 4140220, clipping the corner of the letter W
     * in comm dialog messages starting with the word "Waiting."
     * Note: Java measures font size differently than Windows does,
     * so Dialog 12 maps to Arial 9.
     */
    public static void fixFonts() {
        UIDefaults defaults = UIManager.getDefaults();
        Enumeration<Object> e = defaults.keys();
        Font f = new javax.swing.plaf.FontUIResource("Dialog", Font.PLAIN, 12); 

        while (e.hasMoreElements()) {
            Object obj = e.nextElement();
            String key = obj.toString();
            //Debug.println(UIManager.get(key));
            if (key.endsWith(".font") || key.endsWith("Font")) {  
                UIManager.put(key, f);
            }
        }
    }

    private boolean checkInWithHostResult;

    private void setCheckInWithHostResult(boolean b) {
        checkInWithHostResult = b;
    }

    /**
     * Try to connect with the host to check in. Takes a boolean to determine
     * message facade parameters. Also used by ItemForwarder, which is why it's
     * public. 
     */
    public boolean checkInWithHost(boolean displayWaitDialog, int reterivalType) {
        MessageFacadeParam param;
        if (displayWaitDialog) {
            param = new MessageFacadeParam(MessageFacadeParam.REAL_TIME);
        } else {
            param =
                new MessageFacadeParam(MessageFacadeParam.REAL_TIME, 
                		MessageFacadeParam.NO_DIALOG);
        }
        return checkInWithHost(param, reterivalType);
    }

    /**
     * Try to connect with the host to check in. If we cannot successfully
     * check in, display the disconnected message on the status bar and
     * disable all financial transactions.
     *
     * Converted to use the message facade, uses wait/notify to simulate
     * the synchronous behavior of the old code.
     */
    boolean checkInWithHost(final MessageFacadeParam parameters, int retrieveType) {
		final WaitToken wait = new WaitToken();
		
        final CommCompleteInterface cci = new CommCompleteInterface() {
            @Override
			public void commComplete(int commTag, Object returnValue) {
            	wait.setWaitFlag(false);
                if (returnValue instanceof Boolean) {
                    setCheckInWithHostResult(((Boolean) returnValue).booleanValue());
                }
                synchronized (wait) {
                	wait.notify();
                }
            }
        };

        final MessageLogic logic = new MessageLogic() {
            @Override
			public Object run() {
                return MessageFacade.getInstance().clientCheckIn(cci, parameters);
            }
        };

        setCheckInWithHostResult(false);
        MessageFacade.run(logic, cci, 0, Boolean.FALSE);
        synchronized (wait) {
            try {
            	while (wait.isWaitFlag()) {
            		wait.wait();
            	}
            } catch (InterruptedException ie) {
                //Debug.println("DeltaworksMainPanel.checkInWithHost() caught InterruptedException");   
            }
        }

        //DeltaworksMainPanel.getMainPanel().setDeauthMessage(false); //Renuka says - is this required?
        boolean checkInSuccessful = checkInWithHostResult;
        setConnected(haveConnected() | checkInSuccessful);
        //setTransactionEnabled();
        if (!checkInSuccessful) {
            ErrorManager.setStatus(Messages.getString("DeltaworksMainPanel.disconnectedMessage"), 
            ErrorManager.ERROR_STATUS);
        }

        return checkInSuccessful;
    }

//    /**
//      * Implementation of ErrorListener.
//      * Display a dialog to the user and exit the application.
//      */
//    public void fatalErrorOccurred(String message) {
//        Dialogs.showError("dialogs/DialogFatalError.xml", message); 
//        System.exit(1);
//    }

    /**
      * Start up the background processes.
      */
    void startBackgroundProcesses() {
        schedulerThread.start();
        transmitterThread.start();
//        if (ftpThread != null)
//            ftpThread.start();
        if (isiThread != null)
            isiThread.start();
    }

    /**
      * Start the Deltaworks application. The first command line arg is the
      * version number for the app. This is coded into the batch file that
      * launches the app.
      */
    public static void main(String[] args) {
        initApp(args);
    }

	public static void initApp(String[] args) {
		

        if (args.length < 1) {
            String usage = "Usage : DeltaworksMainPanel <version Number> "; 
            Debug.println(usage);
            System.exit(ERROR_EXIT);
        }

        // Use the Windows Classic Look and Feel
        
        try {
        	UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsClassicLookAndFeel");
        } catch (Exception e) {
        }

        // For Java 1.4, set font properties to match the old defaults
        fixFonts();

        // Set a flag for Gemstone to use convert the date to a String instead
        // of the original long repesentation.
        System.setProperty("xmlDateFormatVersion", "2");  
        FakeResponse.initDataMap();
        // turn on debug
        //Debug.println("Trace functionality is enabled.");    
        Debug.setDebug(true);

        boolean useConsole = false;
        
        // undocumented options come after the version number, which is args[0]
        for (int i = 1; i < args.length; i++) {
             if (args[i].equalsIgnoreCase("-console")) {
                 useConsole = true;
             } else if (args[i].equalsIgnoreCase("-noproxy")) {
                 UnitProfile.getInstance().disableProxy();
             } else if (args[i].equalsIgnoreCase("-connected")) {
                 DeltaworksStartup.getInstance().setConnected(true);
             } else if (args[i].equalsIgnoreCase("-nozip")) {
                 dw.comm.AgentConnect.setUseCompression(false);
             }
        }
      
        if (! useConsole) {
            // Redirect the standard output
            System.setOut(Debug.getPrintStream());
            System.setErr(Debug.getPrintStream());
        }
        
//        Debug.println(""); 
//        Debug.println("Starting Deltaworks " + args[0]); 
//        	
//        Debug.println("Java version " + System.getProperty("java.runtime.version"));
        
//        if (args.length > 1) {
//            // turn off SSL if it is not desired (dev mode)
//            // assume the -nossl flag would have to be in the 2nd parm
//            if (args[1].equalsIgnoreCase("-nossl")) 
//                UnitProfile.getUPInstance().setProtocolPrefix("http://"); 
//
////            // turn off compression if not desired (QA load test support)
////            if (args[1].equalsIgnoreCase("-nozip")) 
////                Transceiver.enableCompression(false);
//        }
        
        try {
            getInstance().startup(args[0]);
        } catch (SecurityException e) {
            Dialogs.showError("dialogs/DialogFatalError.xml", 
            		Messages.getString("DialogFatalError.CorruptJar")); 
            Debug.printStackTrace(e);
            System.exit(ERROR_EXIT);
        } catch (Throwable t) {
            Debug.println("Uncaught top-level Exception"); 
            Debug.printStackTrace(t);
            // catch any uncaught exception and print it in the diag log.
            ErrorManager.writeStackTrace("Uncaught top-level Exception", t); 
            System.exit(ERROR_EXIT);
        }
	}

    public void setConnected(boolean connected) {
//        Debug.dumpStack("setConnected(" + connected + ") called from");
        this.connected = connected;
    }

    public boolean haveConnected() {
//        Debug.println("DeltaworksStartup.haveConnected() = " + connected);
        return connected;
    }
    
    void cleanupResources() {
        setKillScheduledTransmit(true);
        //  resumeSemaphores(); // make sure we are not waiting on anything...
        resumeStartupSemaphores();
        scheduler.halt();
//        itemForwarder.halt();
        transmitter.halt();
    }

    synchronized void setPendingDialog(Runnable pendingDialog) {
        DeltaworksStartup.pendingDialog = pendingDialog;
    }

    void pauseStartupSemaphores() {
        softwareDownloadSemaphore.pause();
        itemTransmissionSemaphore.pause();
    }

    void resumeStartupSemaphores() {
        softwareDownloadSemaphore.resume();
        itemTransmissionSemaphore.resume();
    }

    void setSoftwareDownloadSemaphore(PauseSemaphore softwareDownloadSemaphore) {
        this.softwareDownloadSemaphore = softwareDownloadSemaphore;
    }

    public PauseSemaphore getSoftwareDownloadSemaphore() {
        return softwareDownloadSemaphore;
    }

//    public ItemForwarder getItemForwarder() {
//        return itemForwarder;
//    }

    public Transmitter getTransmitter() {
        return transmitter;
    }

    public static boolean isKillScheduledTransmit() {
        return killScheduledTransmit;
    }

    public static void setKillScheduledTransmit(boolean b) {
        killScheduledTransmit = b;
    }

	public void setStartingFirstTime(boolean startingFirstTime) {
		this.startingFirstTime = startingFirstTime;
	}

	public boolean isStartingFirstTime() {
		return startingFirstTime;
	}

    class MyWindowAdapter extends WindowAdapter {
        @Override
		public void windowClosing(WindowEvent e) {   
        	    if(UnitProfile.getInstance().isMultiAgentMode()){
        	    	 if (Dialogs.showQuestion("dialogs/DialogExit.xml") == Dialogs.YES_OPTION)        	    	   
            	      if(!UnitProfile.getInstance().isSuperAgent()){
            	     	MainPanelDelegate.changeToSuperAgent(false);
            	      }
            	      else
            	        MainPanelDelegate.tryExit(DeltaworksMainPanel.NORMAL_EXIT);           	 
        	    }
            	else{
            	   if (Dialogs.showQuestion("dialogs/DialogExit.xml") == Dialogs.YES_OPTION) 
                    MainPanelDelegate.tryExit(DeltaworksMainPanel.NORMAL_EXIT);
            	  
            	}
        }
    }
    
    /*
     * Execute PCI (Payment Card Industry) script
     */
     public void executeProgram() {
        Debug.println("PCI: Warning = " + strError + 
			"; Exit Code=" + Integer.toString(intPciExitCode) + "; Running Script now...");
        try {
        	Runtime.getRuntime().exec("cmd /c start mgi.exe");
        }
        catch (IOException ioe) {
            Debug.println("PCI: Error = Script IO Exception; Script Name=mgi.exe");
        }
    }

     /*
      * Run PCI (Payment Card Industry) script if not previously run 
      * to prevent admin account running on MoneyGram PCs
      */
    private void checkIfPciRunSuccessfully()
    {
		int intCharacter;
		FileReader txtIn = null;
		BufferedReader reader = null;

	    try {
			//Attempt to read the PCI file
 			intPciExitCode = 0;
 			strError = null;
 			txtIn = new FileReader(PCI_EXIT_CODE_FILE);
 			reader = new BufferedReader(txtIn);
 			
 			//Attempt to get the PCI exit code status
 			for(int ii = 0; ii < 3; ii++) {
 	 			intCharacter = reader.read();
 	 			
 	 			if(intCharacter == -1) {
 	 				strError = "end of file";
 	 				break;
 	 			}
 	 			
 	 			else if(intCharacter < '0' || intCharacter > '9') {
 	 				strError = "character not a digit";
 	 				break;
 	 			
 	 			} else {
 	 				intPciExitCode = intPciExitCode * 10 + intCharacter - '0';
 	 			}
 			}
	    } catch (FileNotFoundException e) {
	    	strError = "file not found exception";
		} catch (IOException e) {
			strError = "IO exception";
		} finally {
			try {
				if (txtIn != null) {
					txtIn.close();
				}
			} catch (Exception e) {
				Debug.printException(e);
			}
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (Exception e) {
				Debug.printException(e);
			}
		}

 		//Check if got at least three digit number	
		if(strError == null) {
 			//Yes: check for a valid PCI exit code status
		    if(intPciExitCode < PCI_MINIMUN_EXIT_CODE || intPciExitCode > PCI_MAXIMUN_EXIT_CODE) {
		    	strError = "invalid PCI exit code";

		    } else {
	 			//Valid PCI exit code status
			    for(int ii = 0; ii < PCI_EXIT_CODES_TO_CAUSE_RERUN.length; ii++) {
			    	if(intPciExitCode == PCI_EXIT_CODES_TO_CAUSE_RERUN[ii]) {
					    //Valid PCI exit code status; but not a successful one - so rerun the PCI script
			    		strError = "unable to run script";
	 	 				break;
			    	}
			    }
		    }
		}

	    if(strError == null) {
		    //The PCI exit code status is OK; do not need to run the PCI script.
			Debug.println("PCI: Success = script prevously executed; Exit code=" + 
				Integer.toString(intPciExitCode));
	    } else {
	    	//Run the PCI script
	    	executeProgram();
	    }
	}
    
    public void doClickMenu(String comName)
    {

    	DeltaworksMainPanel.getMainPanel().doClickMenu(comName);
    }
}
