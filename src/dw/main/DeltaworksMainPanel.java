/*
 * DeltaworksMainPanel.java
 *
 * Copyright (c) 2000-2005 MoneyGram International
 */

package dw.main;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.TimeZone;

import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

import com.moneygram.agentconnect.CustomerComplianceTypeCodeType;
import com.moneygram.agentconnect.MoneyOrderVoidReasonCodeType;
import com.moneygram.agentconnect.SubagentInfo;

import dw.accountdepositpartners.AccountDepositPartnersTransaction;
import dw.agentdirectory.AgentDirectoryTransaction;
import dw.agentdirectory.AgentDirectoryWizard;
import dw.amend.AmendTransaction;
import dw.amend.AmendTransactionWizard;
import dw.billpayment.BillPaymentWizard;
import dw.broadcastmessages.BroadcastMessagesTransaction;
import dw.broadcastmessages.BroadcastMessagesWizard;
import dw.comm.MessageFacade;
import dw.comm.MessageFacadeError;
import dw.comm.MessageFacadeParam;
import dw.comm.MessageLogic;
import dw.dialogs.AuditDialog;
import dw.dialogs.ChangePasswordDialog;
import dw.dialogs.CommunicationDialog;
import dw.dialogs.ComplianceDialog;
import dw.dialogs.DatePeriodEntryDialog;
import dw.dialogs.DateTimeEntryDialog;
import dw.dialogs.DetailSelectionEntryDialog;
import dw.dialogs.Dialogs;
import dw.dialogs.ManualDialDialog;
import dw.dialogs.NamePasswordDialog;
import dw.dialogs.UpdateStatusDialog;
import dw.dispenser.DispenserFactory;
import dw.dispenser.DispenserInterface;
import dw.dispenserload.DispenserLoadTransaction;
import dw.dispenserload.NewDispenserLoadWizard;
import dw.framework.ClientTransaction;
import dw.framework.CommCompleteInterface;
import dw.framework.KeyMapManager;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageFlowContainer;
import dw.framework.PageNameInterface;
import dw.framework.PageNotification;
import dw.framework.PauseSemaphore;
import dw.framework.TransactionInterface;
import dw.framework.XmlDefinedPanel;
import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.install.InstallTransaction;
import dw.install.InstallWizard;
import dw.io.CCILog;
import dw.io.DebugLog;
import dw.io.DiagLog;
import dw.io.IsiReporting;
import dw.io.Transmitter;
import dw.io.report.AuditLog;
import dw.mfaRegistration.RegisterMFAWizard;
import dw.mfaRegistration.RegisterMfaToken;
import dw.mgreceive.MoneyGramReceiveTransaction;
import dw.mgreceive.MoneyGramReceiveWizard;
import dw.mgreversal.ReceiveReversalTransaction;
import dw.mgreversal.ReceiveReversalTransactionWizard;
import dw.mgreversal.SendReversalTransaction;
import dw.mgreversal.SendReversalTransactionWizard;
import dw.mgsend.MoneyGramClientTransaction;
import dw.mgsend.MoneyGramSendTransaction;
import dw.mgsend.MoneyGramSendWizard;
import dw.mgsend.TSWizard;
import dw.model.adapters.CachedACResponses;
import dw.model.adapters.CountryInfo;
import dw.moneyorder.MoneyOrderExpert;
import dw.moneyorder.MoneyOrderTransaction40;
import dw.moneyorder.MoneyOrderWizard;
import dw.multiagent.MultiAgentTransaction;
import dw.multiagent.MultiAgentTransactionInterface;
import dw.pdfreceipt.PdfReceipt;
import dw.pinpad.Pinpad;
import dw.printing.DispenserItemsReport;
import dw.printing.MoneyGramReport;
import dw.printing.ProcessingFeeReport;
import dw.printing.Report;
import dw.printing.TEPrinter;
import dw.profile.Profile;
import dw.profile.ProfileItem;
import dw.profile.UnitProfile;
import dw.prtreceipt.PrtReceipt;
import dw.ras.RasProperties;
import dw.reprintreceipt.ReprintReceiptTransaction;
import dw.reprintreceipt.ReprintReceiptWizard;
import dw.simplepm.SimplePMTransaction;
import dw.simplepm.SimplePMTransactionInterface;
import dw.softwareversions.SoftwareVersionUtility;
import dw.softwareversions.SoftwareVersionUtility.SoftwareStatus;
import dw.softwareversions.VersionManifest;
import dw.softwareversions.VersionManifestEntry;
import dw.utility.AgentCountryInfo;
import dw.utility.Currency;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.ErrorListener;
import dw.utility.ErrorManager;
import dw.utility.ExtraDebug;
import dw.utility.Guidelines;
import dw.utility.IdleBackoutTimer;
import dw.utility.Period;
import dw.utility.ReceiptTemplates;
import dw.utility.Restrictions;
import dw.utility.Scheduler;
import dw.utility.ServiceOption;
import dw.utility.StringUtility;
import dw.utility.TimeUtility;
import dw.vendorpayment.VendorPaymentExpert;
import dw.vendorpayment.VendorPaymentWizard;

/**
 * DeltaworksMainPanel is the main entry point for the app. It will create
 * the wizard and expert interfaces for all the transaction types, and will
 * allow access to them through a main menu.
 * DW42 - cleanup of the MainPanel constructor. Moved all the initializations to
 * DeltaworksStartup.
 *
 * @author Rob Campbell
 * @author Renuka Easwar
 */
public class DeltaworksMainPanel extends PageFlowContainer implements ErrorListener, PageNameInterface, MainPanelHooks {
	private static final long serialVersionUID = 1L;

	private static DeltaworksMainPanel instance; // singleton instance

    // components we need to access throughout this class
    private JFrame parentFrame = null;
    private JPanel statusPanel = null;
    private JLabel statusLabel = null;
    private JLabel formsLabel = null;
    private JLabel dispenserFormsLabel = null;
    private JLabel periodLabel = null;
    private JMenuBar menuBar = null;
    private JCheckBoxMenuItem trainingModeMenuItem = null;
    private JLabel softwareLabel = null;

    // transaction objects...only created once and re-used throughout run of app
    private MoneyOrderTransaction40 moneyOrderTransaction;
    private MoneyGramSendTransaction moneyGramSendTransaction;
    private AccountDepositPartnersTransaction moneyGramSendToAccountTransaction;
    private MoneyGramSendTransaction billPaymentTransaction;
    private MoneyGramSendTransaction formFreeSendTransaction;
    private MoneyGramSendTransaction addMinutesTransaction;
    private MoneyGramSendTransaction prepaidCardTransaction;
    private MoneyGramSendTransaction transactionStatusTransaction;
//    private MoneyGramCardTransaction moneyGramCardTransaction;
//    private MoneyGramCardPurchaseTransaction moneyGramCardPurchaseTransaction;
//    private MoneyGramCardReloadTransaction moneyGramCardReloadTransaction;
    private MoneyGramReceiveTransaction moneyGramReceiveTransaction;
    private SendReversalTransaction sendReversalTransaction;
    private ReceiveReversalTransaction receiveReversalTransaction;
    private AmendTransaction amendTransaction;
    private MoneyOrderTransaction40 vendorPaymentTransaction;
    private AgentDirectoryTransaction agentDirectoryTransaction;
    private ReprintReceiptTransaction reprintReceiptTransaction;
    private RegisterMfaToken registerMfaToken;
    private DispenserLoadTransaction dispenserLoadTransaction;
    private MultiAgentTransaction multiAgentTransaction;
    private InstallTransaction installTransaction;
    private SimplePMTransaction simplePMTransaction;
	//private AccountDepositPartnersTransaction accountDepositPartnersTransaction;
	private BroadcastMessagesTransaction broadcastMessagesTransaction;

    // current transaction state
    private TransactionInterface currentTransactionInterface = null;
    private ClientTransaction currentTransaction = null;
    private MoneyGramClientTransaction moneyGramClientTransaction;

    private DeltaworksMainMenu mainMenuPage = null;

    public static boolean modemSpkr = false;

    // the timing object will be used to calculate the time
    // it take to complete a transaction, start to finish.
    private static long timing = System.currentTimeMillis();

    // needed for TELCO diag
    private static boolean duringManualTransmit = false;

    // used to lock user out of most of the app while it is taking place
    public boolean moTransmitInProgress = false;

    private static int transmitProcessAttempts = 0;

    // whether the message is a deauth message for the status bar
    private boolean deAuthMessage = false;


    // whether the message is a deactivated message for the status bar
    private boolean deActivateMessage = false;

    // store the transaction to resume after the dispenser load
    private ClientTransaction pendingTransaction = null;

    // exit codes to send back to indicate app exit status
    public static final int NORMAL_EXIT = 0;
    public static final int RESTART_EXIT = 99;

//    private static final String[] DISABLED_FOR_DEMO = { "transmit", "reverseSetup",  
//    "closeDispenserPeriod" }; 
    private static final String[] DISABLED_FOR_DEMO = {"reverseSetup",  
    "closeDispenserPeriod" }; 

    // during the install tran, don't want <<DEMO MODE>> displayed
    private boolean displayModeTags = true;

    // used to lock user out of most of the app while it is taking place
    private boolean profileUpdateInProgress = false;

    // start the diag transmit after the current transaction if this is true
    private boolean diagTransmitPending = false;

    private boolean newSoftwareReady = false;

    //set to true if logs overflow/get corrupt; must transmit or clear logs
    //to set to false
    private boolean mustHandleLogsFlag = false;
    private boolean mustHandleLogsDialogPending = false;

    private PauseSemaphore tranCreationSemaphore = new PauseSemaphore();

    private boolean frameSetUp = false;

    private static boolean ftpSuccess = true;

    // used by transmitDiag
    private boolean newDialup;

    // used by startTransactionInterface to ensure no other transaction is started
    private boolean transactionStarting = false;

    private Boolean gotNewTranslationTable = Boolean.FALSE;

    private Transmitter transmitter = DeltaworksStartup.getInstance().getTransmitter();
    private boolean isiRpCommand;               // -RP ISI command status
    private boolean isiExeCommand;              // -EXE ISI command status
    private boolean isiExe2Command;              // -EXE ISI command status
    private String isiExeCommandArguments;      // -EXE ISI command program arguments
    private String isiExe2CommandArguments;      // -EXE ISI command program arguments
    private boolean isiAllowDataCollection;
    private boolean isiAllowRcptTextData;
    private boolean isiFrontCommandOccurred;
    private boolean isiTransactionInProgress;   // ISI transaction being processed status
    private boolean isiAppendMode = true;       // ISI flag for the EXE command to know
                                                // if you want to append or intiate the file.
    private boolean isiSkipRecord = false;      // ISI flag to not used to not do the Execute command.
    public static boolean continueCheckinProcess=false;
    private boolean isiCardReload=false;
    private boolean isiCardPurchase= false;


    private boolean isiNfCommand;              // -NF ISI command status
    private String isiNfCommandArguments;      // -NF ISI command program arguments

    // used for network connectivity change.
    private boolean connectivityChange = false;
    private SubagentInfo[] agents = null;
    ChangePasswordDialog cDialog =null;
    private Process process;
	private boolean checkCachedResponses;

   /**
     * Create the main panel and add the main menu screen as the first card.
     * Create the interfaces for all transactions, and add action listeners
     * to the buttons on the main menu.
     * @param parentFrame JFrame in which the app is being displayed.
     * @param qaBuild to display on the title bar to identify builds easily.
     */
    private DeltaworksMainPanel(JFrame parentFrame) {
        super("main/DeltaworksMainPanel.xml", false);
        instance = this;
        JComponent.setDefaultLocale(Messages.getCurrentLocale());
        ErrorManager.addErrorListener(this);
        //set this panel to be the main component for mapping key actions
        trainingModeMenuItem = (JCheckBoxMenuItem) getComponent("trainingMode"); 
        statusPanel = (JPanel) getComponent("statusPanel"); 
        formsLabel = (JLabel) getComponent("remainingDocsLabel"); 
        dispenserFormsLabel = (JLabel) getComponent("dispenserFormsLabel"); 
        statusLabel = (JLabel) getComponent("transactionStatusLabel"); 
        periodLabel = (JLabel) getComponent("periodLabel"); 
        menuBar = (JMenuBar) getComponent("menubar"); 

        //softwareLabel and progressBar make up the messaging for
        // the software download.  they are set up in the DeltaworksMainPanel.XML.
        // they are a part of the "transmittingPanel" component.
        softwareLabel = (JLabel) getComponent("softwareLabel"); 
        getComponent("transmittingPanel").setVisible(false);

        //Set the modem speaker on/off from the profile.
        modemSpkr = UnitProfile.getInstance().isModemSpeakerMuted();
        ((JCheckBoxMenuItem) getComponent("modemSpeaker")).setSelected(modemSpkr); 
        RasProperties.setMuted(modemSpkr);

        createMainMenu();
        simplePMTransaction = new SimplePMTransaction(Messages.getString("DeltaworksMainPanel.1943")); 
        this.parentFrame = parentFrame;
        // set this panel to be the main component for mapping key actions
        KeyMapManager.setMainMapComponent(this);
        setupMenuBar();
        /*
         * Update the form count now before we call setStatus, since setStatus
         * cannot update the form count since updateFormsLabel can call setStatus
         * for low forms.  Otherwise we show a default form count until we
         * exit a transaction screen.
         */
        updateFormsLabel();
        //show the welcome message on starting the application or whenever the language is switched.
        ErrorManager.setStatus(Messages.getString("DeltaworksMainPanel.welcomeMessage"), 
        ErrorManager.INFORMATION_STATUS);
        start();
    }

    public void f2SAR() {
        // For this build of Deltaworks we don't want the code to use profile
        // "SAR_ENABLE" so it was changed to "SAR_ENABLE_ni" 	
        String result = UnitProfile.getInstance().get("SAR_ENABLE_ni", "N");  
        if (result.equalsIgnoreCase("Y")) { 
            ComplianceDialog dialog = new ComplianceDialog(CustomerComplianceTypeCodeType.SAR);
            Dialogs.showPanel(dialog);
        }
    }


    public static final String[] MENU_ITEMS = {
            // FILE MENU
            "transmit", "clearLogs", "reverseSetup",   
            // "restart",
            "exit", 
            // DISPENSER MENU
            "loadForms", "printTestVoid",  
            // REPORTS MENU
            // "exportData",
            "dispenserItemsSalesReport", 
            "employeeTotals", "detailReporting", "moneyGramReport",
            "processingFeeReport","auditTrailReport",
			"ctrReport", 
            //  "diagnosticsReport",
            // OPTIONS MENU
            "generalOptions","proxySettingOptions",
            "dialupSettingOptions", "dialupSettingOptionsSecondary", "dispenserOptions",
            "printerOptions", "payeeTable",
            "moneyOrderOptions", "vendorPaymentOptions",  
            "moneyGramSendOptions", "moneyGramReceiveOptions",  
            "billPaymentOptions", "managerOptions",
            "employeeOptions", "trainingMode",
            "cardPurchaseOptions", "cardReloadOptions",
            // ACCOUNTING MENU
            "closeDispenserPeriod", 
            "sendReversalTransaction", 
            "receiveReversalTransaction", 
            "amendTransaction", 
            // "screwUpDispenserPeriod",
            // TRANSACTION SEARCH
            "moneyGramFeeQuery", "directoryOfAgents", "directoryOfBillers",   
            "receiverRegistration", "transactionStatus","reprintReceipt",
            // MFA Explicit Registration
            "registerMfaToken",
            // DIALUP MENU
            "manualDial", 
            "modemSpeaker", 
            //LANGUAGE
            "spanish", 
            "english", 
            "german", 
            "french", 
            "chinese", 
            "polish",
            "russian",
            "portuguese",
            // ACCOUNTS
            "changeMultiAgent", 
            // HELP MENU
            "about",  
            "updateStatus",  
    };


    /**
     * Add action listeners to all the menu items.
     */
    private void setupMenuBar() {
        addSameNameActions(MENU_ITEMS, this);
        getComponent("moneyGramFeeQuery").setEnabled(false); 
        getComponent("directoryOfAgents").setEnabled(false); 
        getComponent("directoryOfBillers").setEnabled(false); 
        getComponent("loadForms").setEnabled(false); 

        if (!UnitProfile.getInstance().isDialUpMode()) {
            getComponent("dialupMenu").setVisible(false); 
            getComponent("dialupMenu").setEnabled(false);
        }
        if (!UnitProfile.getInstance().isMultiAgentMode()) {
            getComponent("accountsMenu").setVisible(false); 
            getComponent("accountsMenu").setEnabled(false);
        }
        getComponent("ctrReport").setVisible("Y".equals(UnitProfile.getInstance().get("MANUAL_CTR_ENABLE", "N")));    
        getComponent("sendReversalTransaction").setEnabled(getTranAuth(DWValues.MONEY_GRAM_SEND_DOC_SEQ) && 
            "Y".equals(UnitProfile.getInstance().get("ALLOW_SEND_CANCELS", "N")));   
        getComponent("receiveReversalTransaction").setEnabled(getTranAuth(DWValues.MONEY_GRAM_RECEIVE_DOC_SEQ) && 
            "Y".equals(UnitProfile.getInstance().get("ALLOW_RECEIVE_REVERSALS", "N")));   
        getComponent("amendTransaction").setEnabled(getTranAuth(DWValues.MONEY_GRAM_SEND_DOC_SEQ) && 
            "Y".equals(UnitProfile.getInstance().get("ALLOW_AMENDS", "N")));   
    }

    /**
     * Set the enabled status of certain components based on whether or not
     *    we are in DEMO mode.
     */
    private void setDemoOptionAccess() {
        // disable /enable all the menu items that don't work in DEMO mode
        for (int i = 0; i < DISABLED_FOR_DEMO.length; i++)
            getComponent(DISABLED_FOR_DEMO[i]).setEnabled(!UnitProfile.getInstance().isDemoMode());
        // set the availability of the training mode menu item
        trainingModeMenuItem = (JCheckBoxMenuItem) getComponent("trainingMode"); 
        ProfileItem trainingMode = UnitProfile.getInstance().getProfileInstance().find("TRAINING_MODE"); 
        String trainingLock = trainingMode.getMode();
        getComponent("trainingMode").setEnabled(trainingLock.equals("S") ||  
        trainingLock.equals("P")); 
        trainingModeMenuItem.setSelected(trainingMode.booleanValue());
    }

    /**
     * Get the status of whether or not a certain product is authorized to be
     *   be sold by this agent based on the profile settings.
     * @return if this agent may sell this product.
     */
    public boolean getTranAuth(int docSeq) {
        Profile profile = UnitProfile.getInstance().getProfileInstance();
        ProfileItem item = profile.find("PRODUCT", docSeq); 
        return item.getStatus().equalsIgnoreCase("A"); 
    }

    public boolean getFormFreeAuth(int docSeq) {
        Profile productProfile = (Profile) UnitProfile.getInstance().getProfileInstance().find("PRODUCT", docSeq);	
        String value = productProfile.findString("FORM_FREE_ENABLED");
        return value.equalsIgnoreCase("Y"); 
    }

    /**
     * Get the status of whether inline Directed Sends Registrations are
     * enabled for this agent.
     * @return if this agent uses inline Directed Sends Registrations.
     */
    public boolean isInlineDSR() {
//        Profile profile = UnitProfile.getInstance().getProfileInstance();
//        ProfileItem item = profile.find("PRODUCT", DWValues.MONEY_GRAM_SEND_DOC_SEQ); 
//		ProfileItem piTemp;
//		boolean isInlineDSR;
//
//		piTemp = item.get(DWValues.DSR_INLINE);
//		if (piTemp != null) {
//			isInlineDSR = piTemp.booleanValue(); 
//		}
//		else {
//			isInlineDSR = false;
//		}
		return true;//Turning on Intra-Trn flag alway true as part of project#54351b
        //return isInlineDSR; 
    }

    /**
      * Set the transaction buttons and their corresponding menu options to be
      *   disabled or not, depending on the profile values.
      *
      * Set the status of all the financial transactions. If we have not been able
      *   to connect or we are in an AWOL state, do allow them to be enabled, and
      *   catch the CONNECTED and AWOL states at time of startTransactionInterface.
      *   Only enable the ones that are authorized on this POS.
      * @param enabled whether or not the transactions are being enabled
      */
    @Override
	public void setTransactionEnabled() {
        setTransactionEnabled(true);
    }

    private void setTransactionEnabled(final boolean enabled) {
        //Debug.dumpStack("setTransactionEnabled(" + enabled + ") called from");
        Runnable task = new Runnable() {
            @Override
			public void run() {

                boolean isSuperAgent = UnitProfile.getInstance().get("SUPERAGENT","N").equals("Y");
                // set authorization flags for each transaction
                boolean moAuth = getTranAuth(DWValues.MONEY_ORDER_DOC_SEQ);
                boolean vpAuth = getTranAuth(DWValues.VENDOR_PAYMENT_DOC_SEQ);
                boolean mgsAuth = getTranAuth(DWValues.MONEY_GRAM_SEND_DOC_SEQ);
                boolean mgsffAuth = getFormFreeAuth(DWValues.MONEY_GRAM_SEND_DOC_SEQ);
                //boolean mgsToAccAuth = getTranAuth(DWValues.MONEY_GRAM_SEND_DOC_SEQ);
                boolean mgrAuth = getTranAuth(DWValues.MONEY_GRAM_RECEIVE_DOC_SEQ);
                boolean bpAuth = getTranAuth(DWValues.BILL_PAYMENT_DOC_SEQ);
                boolean bpffAuth = getFormFreeAuth(DWValues.BILL_PAYMENT_DOC_SEQ);
                boolean rrAuth = getTranAuth(DWValues.MONEY_GRAM_RECEIVER_REGISTRATION);
//                boolean cardAuth = getTranAuth(MONEY_GRAM_CARD_DOC_SEQ);
                boolean purAuth = getTranAuth(DWValues.MONEY_GRAM_CARD_PURCH_DOC_SEQ);
                boolean loadAuth = getTranAuth(DWValues.MONEY_GRAM_CARD_RELOAD_DOC_SEQ);
                if (isSuperAgent && UnitProfile.getInstance().isDemoMode()){
                    moAuth = false;
                    vpAuth = false;
                    mgsAuth = false;
                    mgrAuth = false;
                    bpAuth = false;
                    rrAuth = false;
                    purAuth = false;
                    loadAuth = false;
                }
                // disable all languages if in suba agent mode.
                boolean isSubAgent = UnitProfile.getInstance().isSubAgent();
                getComponent("english").setEnabled(!isSubAgent);
                getComponent("spanish").setEnabled(!isSubAgent);
                getComponent("german").setEnabled(!isSubAgent);
                getComponent("french").setEnabled(!isSubAgent);
                getComponent("chinese").setEnabled(!isSubAgent);
                getComponent("polish").setEnabled(!isSubAgent);
                getComponent("russian").setEnabled(!isSubAgent);
                getComponent("portuguese").setEnabled(!isSubAgent);
                getComponent("dialupSettingOptions").setEnabled(!isSubAgent);
                getComponent("dialupSettingOptionsSecondary").setEnabled(!isSubAgent);

                //if (!UnitProfile.getUPInstance().isPosAuthorized() || !connected || mustHandleLogsFlag){
                //    return;
                //}
                //when product is disbled in unit profile make it invisible.
                if (mainMenuPage == null) {
                    Debug.println("new Runnable().run() mainMenuPage=null");
                }
                else {
                    mainMenuPage.setButtonVisible(DeltaworksMainMenu.MONEY_ORDER,moAuth);
                    mainMenuPage.setSelectionEnabled(DeltaworksMainMenu.MONEY_ORDER, enabled && moAuth);

                    mainMenuPage.setButtonVisible(DeltaworksMainMenu.MONEY_GRAM_RECEIVE,mgrAuth);
                    mainMenuPage.setSelectionEnabled(DeltaworksMainMenu.MONEY_GRAM_RECEIVE, enabled && mgrAuth);

                    mainMenuPage.setButtonVisible(DeltaworksMainMenu.MONEY_GRAM_SEND,mgsAuth);
                    mainMenuPage.setSelectionEnabled(DeltaworksMainMenu.MONEY_GRAM_SEND, enabled && mgsAuth);

                    mainMenuPage.setButtonVisible(DeltaworksMainMenu.MONEY_GRAM_SEND_TO_ACCOUNT,(mgsAuth));
                    mainMenuPage.setSelectionEnabled(DeltaworksMainMenu.MONEY_GRAM_SEND_TO_ACCOUNT, enabled && mgsAuth);

                    mainMenuPage.setButtonVisible(DeltaworksMainMenu.TRANSACTION_QUOTE,mgsAuth);
                    mainMenuPage.setSelectionEnabled(DeltaworksMainMenu.TRANSACTION_QUOTE, enabled && mgsAuth);

					mainMenuPage.setButtonVisible(DeltaworksMainMenu.ACCOUNT_DEPOSIT_PARTNERS, mgsAuth);
					mainMenuPage.setSelectionEnabled(DeltaworksMainMenu.ACCOUNT_DEPOSIT_PARTNERS, enabled && mgsAuth);

					boolean flag = (UnitProfile.getInstance().getBroadcastMessageUpdateInterval() > 0);
					mainMenuPage.setButtonVisible(DeltaworksMainMenu.BROADCAST_MESSAGES, flag);
					mainMenuPage.setSelectionEnabled(DeltaworksMainMenu.BROADCAST_MESSAGES, (enabled && flag));

                    mainMenuPage.setButtonVisible(DeltaworksMainMenu.VENDOR_PAYMENT,vpAuth);
                    mainMenuPage.setSelectionEnabled(DeltaworksMainMenu.VENDOR_PAYMENT, enabled && vpAuth);

                    mainMenuPage.setButtonVisible(DeltaworksMainMenu.CARD,bpAuth);
                    mainMenuPage.setSelectionEnabled(DeltaworksMainMenu.CARD, enabled && bpAuth);

                    mainMenuPage.setButtonVisible(DeltaworksMainMenu.BILL_PAYMENT,bpAuth);
                    mainMenuPage.setSelectionEnabled(DeltaworksMainMenu.BILL_PAYMENT, enabled && bpAuth);

                    mainMenuPage.setButtonVisible(DeltaworksMainMenu.DIRECTORY_BILLERS,bpAuth);
                    mainMenuPage.setSelectionEnabled(DeltaworksMainMenu.DIRECTORY_BILLERS, enabled && bpAuth);
                    // Language toggle button should not be visible in multi-agent mode.
                    mainMenuPage.setButtonVisible(DeltaworksMainMenu.LANGUAGE_TOGGLE,!UnitProfile.getInstance().isMultiAgentMode());

                    mainMenuPage.setButtonVisible(DeltaworksMainMenu.DIRECTORY_AGENTS,!isSuperAgent);
                    mainMenuPage.setSelectionEnabled(DeltaworksMainMenu.DIRECTORY_AGENTS, enabled);

                    mainMenuPage.setButtonVisible(DeltaworksMainMenu.ADD_MINUTES,bpAuth);
                    mainMenuPage.setSelectionEnabled(DeltaworksMainMenu.ADD_MINUTES, enabled && bpAuth);

                    boolean ff = (mgsAuth && mgsffAuth) || (bpAuth && bpffAuth);
                    mainMenuPage.setButtonVisible(DeltaworksMainMenu.FORM_FREE_ENTRY, ff);
                    mainMenuPage.setSelectionEnabled(DeltaworksMainMenu.FORM_FREE_ENTRY, enabled && ff);
                }

                // menu options for services
            	Component cRR = getComponent("receiverRegistration");
                if (isInlineDSR()) {
                	cRR.setEnabled(false);
                	cRR.setVisible(false);
                } else {
                	cRR.setVisible(true);
                	cRR.setEnabled(enabled && rrAuth);
                }
                getComponent("transactionStatus").setEnabled(enabled && mgsAuth );//&& isSuperAgent);
				getComponent("reprintReceipt")
						.setEnabled(enabled && (PdfReceipt.reprintAvailable() || PrtReceipt.reprintAvailable()));              
                getComponent("sendReversalTransaction").setEnabled(enabled && mgsAuth);

                Component cMFA = getComponent("registerMfaToken");
                String mfaTokenId = UnitProfile.getInstance().getMfaTokenId();
                if(UnitProfile.getInstance().isMfaEnabled() && ((mfaTokenId == null) || (mfaTokenId.length() == 0))){
                	cMFA.setVisible(true);
                	cMFA.setEnabled(enabled);
                }else{
                	cMFA.setVisible(false);
                	cMFA.setEnabled(false);
                }

               // menu options for profile maintenance
                getComponent("moneyOrderOptions").setEnabled(enabled && moAuth); 
                getComponent("vendorPaymentOptions").setEnabled(enabled && vpAuth); 
                getComponent("payeeTable").setEnabled(enabled && vpAuth); 
                getComponent("moneyGramSendOptions").setEnabled(enabled && mgsAuth); 
                getComponent("moneyGramReceiveOptions").setEnabled(enabled && mgrAuth); 
                getComponent("billPaymentOptions").setEnabled(enabled && bpAuth); 
                getComponent("cardPurchaseOptions").setEnabled(enabled && purAuth); 
                getComponent("cardReloadOptions").setEnabled(enabled && loadAuth); 

                // in multi agent mode make training mode invisible if subagent.
                getComponent("trainingMode").setVisible(!UnitProfile.getInstance().isSubAgent());

                // transaction quote and directory of billers are alternate entry
                // points to MGS and EP (respectively) so need the corresponding auth
                getComponent("moneyGramFeeQuery").setEnabled(enabled && mgsAuth); 
                getComponent("directoryOfBillers").setEnabled(enabled && bpAuth); 
                getComponent("sendReversalTransaction").setEnabled(enabled && mgsAuth && 
                  "Y".equals(UnitProfile.getInstance().get("ALLOW_SEND_CANCELS", "N")));   
                getComponent("receiveReversalTransaction").setEnabled(enabled && mgrAuth && 
                    "Y".equals(UnitProfile.getInstance().get("ALLOW_RECEIVE_REVERSALS", "N")));   
                getComponent("amendTransaction").setEnabled(enabled && mgsAuth && 
                    "Y".equals(UnitProfile.getInstance().get("ALLOW_AMENDS", "N")));   

                getComponent("generalOptions").setEnabled(enabled); 
                if (dispenserTransactionsAllowed())
                    getComponent("dispenserOptions").setEnabled(enabled); 
                else
                    getComponent("dispenserOptions").setVisible(false); 
                
                // Printer Options
                
                String useAgentPrinter = UnitProfile.getInstance().get("USE_AGENT_PRINTER", "");
                boolean enablePrinterOption = useAgentPrinter.equals("S") || useAgentPrinter.equals("SP") || useAgentPrinter.equals("Y");
                getComponent("printerOptions").setEnabled(enablePrinterOption);
                
                //getComponent("managerOptions").setEnabled(enabled); 
                getComponent("managerOptions").setEnabled(!getGlobalLoginTypeRequired().equals("N"));
                //getComponent("employeeOptions").setEnabled(enabled); 
                getComponent("employeeOptions").setEnabled(!getGlobalLoginTypeRequired().equals("N"));

               if(UnitProfile.getInstance().isSubAgent()|| UnitProfile.getInstance().isTakeOverSubagentprofile()){
               	getComponent("managerOptions").setEnabled(false);
                //getComponent("employeeOptions").setEnabled(enabled); 
                getComponent("employeeOptions").setEnabled(false);
               }
                /** Sets access to non-transaction related menu items.
                */
                //from Dispenser menu
                getComponent("dispenserMenu").setVisible(dispenserTransactionsAllowed());
                getComponent("loadForms").setEnabled(enabled); 
                getComponent("printTestVoid").setEnabled(enabled); 

                //from Reports menu
                getComponent("dispenserItemsReport").setVisible(dispenserTransactionsAllowed());
                getComponent("dispenserItemsSalesReport").setEnabled(enabled); 
                getComponent("employeeTotals").setEnabled(enabled); 
                getComponent("detailReporting").setEnabled(enabled); 
                getComponent("moneyGramReport").setEnabled(enabled); 
                getComponent("processingFeeReport").setEnabled(getTranAuth(DWValues.BILL_PAYMENT_DOC_SEQ));

                //getComponent("prepaidCardReport").setEnabled(enabled); 
                //  getComponent("prepaidCardReport").setEnabled(cardTransactionsAllowed()); 
                // Removed per design item #2 for release 2.9 A416
                if (DWValues.isMGIOwnedPC()) {
                	removeComponent("auditTrailReport");
                	
                } else {
                    getComponent("auditTrailReport").setEnabled(UnitProfile.getInstance().get("DAYS_IN_AUDIT_TRAIL", 7) > 0);
                }
                  //  getComponent("diagnosticsReport").setEnabled(enabled);
                  
                getComponent("directoryOfAgents").setEnabled(enabled); 

                //from Accounting menu
                if (dispenserTransactionsAllowed())
                    getComponent("closeDispenserPeriod").setEnabled(enabled); 
                else
                    getComponent("closeDispenserPeriod").setVisible(false); 

                // set the access to the items that are disabled for demo mode
                setDemoOptionAccess();

            }
        };

        if (SwingUtilities.isEventDispatchThread()) {
            task.run();
        }
        else {
            SwingUtilities.invokeLater(task);
        }

    }

    // override for default wizard buttons...create an empty set
    @Override
	public PageFlowButtons createButtons() {
        JButton[] b = {
        };
        String[] n = {
        };
        return new PageFlowButtons(b, n, this);
    }

    private void updatePeriodLabel() {
    	String sPeriod = Byte.toString(UnitProfile.getInstance().getCurrentPeriod());
        periodLabel.setText(sPeriod); 
        ExtraDebug.print("Update Period Display["+sPeriod+"]");
        periodLabel.repaint();
    }

    /**
     * Display the remaining number of forms in the dispenser on the forms status
     *   label. Also set the status line to a warning if the forms are past the
     *   low form warning threshold.
     */
    public void updateFormsLabel() {
        if (dispenserTransactionsAllowed()){
            int remainingForms = DispenserFactory.getDispenser(1).getRemainingCount();
            if (remainingForms < 0)
                remainingForms = 0;
            formsLabel.setText("" + remainingForms); 
            formsLabel.repaint();
            if (remainingForms == 0) {
                ErrorManager.setStatus(Messages.getString("DeltaworksMainPanel.noFormsText"), 
                ErrorManager.WARNING_STATUS);
            }
            else if (remainingForms < getLowFormsWarningLimit()) {
                ErrorManager.setStatus(Messages.getString("DeltaworksMainPanel.lowFormsText"), 
                ErrorManager.WARNING_STATUS);
            }
        }
         else {formsLabel.setVisible(false);
             dispenserFormsLabel.setVisible(false);}

    }

    /**
     * Set the title bar of the application, including the mode tags
     * @param the base title being set.
     */
    @Override
	public void setTitle(String title) {
        String branchName = "";
        String legacyNumber = "";
        if (UnitProfile.getInstance().isDemoMode() && displayModeTags)
            title += Messages.getString("DeltaworksMainPanel.1944"); 
        if (UnitProfile.getInstance().isTrainingMode() && displayModeTags)
            title += Messages.getString("DeltaworksMainPanel.1945"); 
        if (UnitProfile.getInstance().isQaMode() && displayModeTags)
            title += Messages.getString("DeltaworksMainPanel.1946"); 
        if(UnitProfile.getInstance().isMultiAgentMode() && displayModeTags){
          //  branchName +=UnitProfile.getInstance().get("GENERIC_DOCUMENT_STORE","");
            branchName += UnitProfile.getInstance().get("AGENT_STORE_NAME","");
        	if(branchName.equals(""))
        		branchName +=UnitProfile.getInstance().get("GENERIC_DOCUMENT_STORE","");
            legacyNumber +=UnitProfile.getInstance().get("MG_ACCOUNT","");
            if (legacyNumber.equals(""))
                title = title+"   " +branchName;
            else
                title = title+"   "+ legacyNumber + " - " +branchName;
        }
        super.setTitle(title);
    }

    @Override
	public void setupCancelISI(String subStatusCode) {
        List<String> fields = new ArrayList<String>();
        IsiReporting isi = new IsiReporting();
        fields.add("CNL");
        String[] headerItems = {(UnitProfile.getInstance().isTrainingMode() ? "T" : (UnitProfile.getInstance().isDemoMode() ? "D" : "P")),
        		new SimpleDateFormat(TimeUtility.ISI_DATE_TIME_FORMAT).format(TimeUtility.currentTime()),
            UnitProfile.getInstance().get("USER_ID", ""),
            UnitProfile.getInstance().get("NAME", ""),
            UnitProfile.getInstance().get("MG_ACCOUNT","NO_MG_ACCOUNT"),
            UnitProfile.getInstance().get("AGENT_ID","NO_AGENT_ID")};

        for(int i=0; i<headerItems.length; i++){
            fields.add(headerItems[i]);
        }
        fields.add(subStatusCode);

        isi.print(fields, isi.ISI_LOG);
        if (MainPanelDelegate.getIsiExeCommand()){
            isi.print(fields, isi.EXE_LOG);
        } else if (MainPanelDelegate.getIsiNfCommand()){
            isi.print(fields, isi.NF_LOG);
        }
    }

    @Override
	public void setupErrorISI(String subStatusCode) {

        List<String> fields = new ArrayList<String>();
        IsiReporting isi = new IsiReporting();
        fields.add("ERR");
        String[] headerItems = {(UnitProfile.getInstance().isTrainingMode() ? "T" : (UnitProfile.getInstance().isDemoMode() ? "D" : "P")),
        		new SimpleDateFormat(TimeUtility.ISI_DATE_TIME_FORMAT).format(TimeUtility.currentTime()),
            UnitProfile.getInstance().get("USER_ID", ""),
            UnitProfile.getInstance().get("NAME", ""),
            UnitProfile.getInstance().get("MG_ACCOUNT","NO_MG_ACCOUNT"),
            UnitProfile.getInstance().get("AGENT_ID","NO_AGENT_ID")};

        for(int i=0; i<headerItems.length; i++){
            fields.add(headerItems[i]);
        }
        fields.add(subStatusCode);

        isi.print(fields, isi.ISI_LOG);
        if (MainPanelDelegate.getIsiExeCommand())
            isi.print(fields, isi.EXE_LOG);
        else if (MainPanelDelegate.getIsiNfCommand())
            isi.print(fields, isi.NF_LOG);

        if (getIsiExeProgramArguments() != null ){
            String programName = getIsiExeProgramArguments().trim();
            if (programName.length() > 0 && !(programName.startsWith("-EXE"))) {
  	  			executeIsiProgram(programName);
            }
        }
        if (getIsiNfProgramArguments() != null ){
            String programName = getIsiNfProgramArguments().trim();
            if (programName.length() > 0 && !(programName.startsWith("-NF"))) {
  	  			executeIsiProgram(programName);
            }
        }
    	setIsiTransactionInProgress(false);
		setIsiExeCommand(false);
		setIsiExe2Command(false);
		setIsiExeProgramArguments("");
		setIsiRpCommandValid(false);
		setIsiNfCommand(false);
		setIsiNfProgramArguments("");
    }

    @Override
	public void executeIsiProgram(String programName) {
        if (programName.length() > 0 ) {
            try {
    			Debug.println("  calling External Program:: " + programName);
                process = Runtime.getRuntime().exec(programName);
            } catch (Exception e) {
                Debug.println("ISI program " + programName + " failed to execuate");
				Debug.printException(e);
			}
        }
    }

    @Override
	public void setupNonFinancialISI() {

        List<String> fields = new ArrayList<String>();
        IsiReporting isi = new IsiReporting();
        fields.add("NON");
        String[] headerItems = {(UnitProfile.getInstance().isTrainingMode() ? "T" : (UnitProfile.getInstance().isDemoMode() ? "D" : "P")),
        		new SimpleDateFormat(TimeUtility.ISI_DATE_TIME_FORMAT).format(TimeUtility.currentTime()),
            UnitProfile.getInstance().get("USER_ID", ""),
            UnitProfile.getInstance().get("NAME", ""),
            UnitProfile.getInstance().get("MG_ACCOUNT","NO_MG_ACCOUNT"),
            UnitProfile.getInstance().get("AGENT_ID","NO_AGENT_ID")};

        for(int i=0; i<headerItems.length; i++){
            fields.add(headerItems[i]);
        }

        isi.print(fields, isi.ISI_LOG);
        if (MainPanelDelegate.getIsiNfCommand())
            isi.print(fields, isi.NF_LOG);

        if (getIsiNfProgramArguments() != null ){
            String programName = getIsiNfProgramArguments().trim();
            if (programName.length() > 0 && !(programName.startsWith("-NF"))) {
  	  			executeIsiProgram(programName);
            }
        }
    	setIsiTransactionInProgress(false);
		setIsiNfCommand(false);
		setIsiNfProgramArguments("");
		//setIsiRpCommandValid(false);
    }


    public boolean didConnectivityChange() {
        return connectivityChange;
    }

    @Override
	public void setConnectivityChange(boolean s){
        connectivityChange = s;
    }

    /**
      * Handle the exit of the current page. If the page is a transaction
      *  interface, get the status of the transaction's exit and act accordingly.
      *  If the page is the main menu, get the current selection and fire off
      *  the corresponding transaction interface.
      * @param direction the direction in which the page is exiting as defined in
      *    PageExitListener.
      */
    @Override
	public void exit(int direction) {
        if (! SwingUtilities.isEventDispatchThread()) {
            Debug.println("DeltaworksMainPanel.exit(" + direction + ") called, not in event dispatch thread.");
        }
        // calculate duration of transaction and write it to the trace file.
        Debug.println("Transaction duration: "
            + new BigDecimal(System.currentTimeMillis()-timing).divide(new BigDecimal(1000),2,0)+" seconds");
        // Ensure no other transactions are started while this is running.
        if (!transactionStarting) {
            try {
                transactionStarting = true;
                displayModeTags = true;

                KeyMapManager.mapMethod(this, KeyEvent.VK_F2, 0, "f2SAR"); 

                // unpause the tran create, software download, and item xmit threads
                resumeSemaphores();
                if (UnitProfile.getInstance().isDialUpMode()) {
                    getComponent("dialupMenu").setVisible(true); 
                    getComponent("dialupMenu").setEnabled(true);
                } else {
                    getComponent("dialupMenu").setVisible(false); 
                    getComponent("dialupMenu").setEnabled(false);

                }

                try {
                    if (CCILog.getInstance() != null){
                        CCILog.getInstance().getDetailFile().unLock();
                    }
                }
                catch (IOException e) {
                    Debug.println("Exception caught while attempting to unlock CCILog.");
                    Debug.printStackTrace(e);
                }

                // Reset the ISI command to default values for the next transaction.
                if ((direction == PageExitListener.NEXT) ||
                    (direction == PageExitListener.EXPERT) ||
                    (direction == PageExitListener.WIZARD) ||
                    (direction == PageExitListener.DONOTHING)) {
                        // Do Nothing
                }
                else {
                    if (direction == PageExitListener.CANCELED){
                    	if (currentTransaction == agentDirectoryTransaction && ((AgentDirectoryTransaction)currentTransaction).getDataReturned())
                    		setupNonFinancialISI();
                    	else
                    		setupCancelISI("01");

                    	if (currentTransaction != moneyOrderTransaction &&
                    		currentTransaction != vendorPaymentTransaction &&
                    		currentTransaction != reprintReceiptTransaction &&
                    		currentTransaction != registerMfaToken &&
                    		currentTransaction != agentDirectoryTransaction)
                    		AuditLog.writeTranCancelRecord(currentTransaction.getUserID(), getAuditTransactionName(currentTransaction));
                    }
                    if (direction == PageExitListener.TIMEOUT){
                    	setupCancelISI("01");
                    	if (currentTransaction != moneyOrderTransaction &&
                        	currentTransaction != vendorPaymentTransaction &&
                    		currentTransaction != reprintReceiptTransaction &&
                    		currentTransaction != registerMfaToken &&
                        	currentTransaction != agentDirectoryTransaction)
                    		AuditLog.writeTimeoutRecord(currentTransaction.getUserID(), getAuditTransactionName(currentTransaction));
                    }
                    if (direction == PageExitListener.COMPLETED) {
                    	if (currentTransaction == dispenserLoadTransaction ||
                    		currentTransaction == simplePMTransaction ||
							currentTransaction == transactionStatusTransaction)
                    		setupNonFinancialISI();
                    }

                    if (currentTransaction != dispenserLoadTransaction){
	                    if (getIsiAllowDataCollection() && (!getIsiSkipRecord())){
	                        if (getIsiExeProgramArguments() != null ){
	                            String programName = getIsiExeProgramArguments().trim();
	                            if (programName.length() > 0 && !(programName.startsWith("-EXE"))) {
	            	  	  			executeIsiProgram(programName);
	                            }
	                        }
                        }
	                    setIsiSkipRecord(false);
	                    initializeIsiFields();
                    }

                    if (getIsiNfProgramArguments() != null ){
                        String programName = getIsiNfProgramArguments().trim();
                        if (programName.length() > 0 && !(programName.startsWith("-NF"))) {
        	  	  			executeIsiProgram(programName);
                        }
                        setIsiNfCommand(false);
                        setIsiNfProgramArguments("");
                    }

                }

                // moving from wizard to expert or vice versa, so re-start the
                //  tran interface instead of re-displaying the menu
                if (direction == PageExitListener.EXPERT || direction == PageExitListener.WIZARD) {
                    currentTransaction.setUsingWizard(direction == PageExitListener.WIZARD);
                    startTransactionInterface(true);
                    return;
                }
//                if (cardLayout.getVisiblePageName().equals(Messages.getString("DeltaworksMainPanel.1973") + "Wizard") &&
//                    direction == PageExitListener.COMPLETED) {
//                	if (moneyGramCardTransaction.getCardTransactionType().equals(
//                			ClientTransaction.CARD_LOAD_TYPE))   {
//                		moneyGramCardReloadTransaction.setUser(moneyGramCardTransaction.getCardUserId());
//                		currentTransaction = moneyGramCardReloadTransaction;
//                    	moneyGramCardReloadTransaction.setDocumentSequence(MoneyGramCardTransaction.CARD_LOAD_SEQ);
//                    	moneyGramCardReloadTransaction.setDocumentType(MoneyGramCardTransaction.CARD_LOAD_SEQ);
//                    	moneyGramCardReloadTransaction.setMaxBatchAmount(moneyGramCardTransaction.getMaxBatchAmount());
//                	}
//                    else {
//                    	moneyGramCardPurchaseTransaction.setUser(moneyGramCardTransaction.getCardUserId());
//                    	currentTransaction = moneyGramCardPurchaseTransaction;
//                    	moneyGramCardPurchaseTransaction.setDocumentSequence(MoneyGramCardTransaction.CARD_PURCHASE_SEQ);
//                		moneyGramCardPurchaseTransaction.setDocumentType(MoneyGramCardTransaction.CARD_PURCHASE_SEQ);
//                		moneyGramCardPurchaseTransaction.setMaxBatchAmount(moneyGramCardTransaction.getMaxBatchAmount());
//                    }
//                	currentTransaction.setStatus(ClientTransaction.COMPLETED);
//                    startTransactionInterface(false, false);
//                    return;
//                }

                // coming from main menu...start correct transaction interface
                checkCachedResponses = false;
                if (cardLayout.getVisiblePageName().equals("mainMenu")) { 
                        switch (mainMenuPage.getSelection()) {
                        case DeltaworksMainMenu.MONEY_ORDER :
                            currentTransaction = moneyOrderTransaction;
                            break;
                        case DeltaworksMainMenu.MONEY_GRAM_SEND :
                        	checkCachedResponses = true;
                            currentTransaction = moneyGramSendTransaction;
                            break;
                        case DeltaworksMainMenu.MONEY_GRAM_SEND_TO_ACCOUNT :
                        	checkCachedResponses = true;
                            currentTransaction = moneyGramSendToAccountTransaction;
                            break;
                        case DeltaworksMainMenu.MONEY_GRAM_RECEIVE :
                        	checkCachedResponses = true;
                            currentTransaction = moneyGramReceiveTransaction;
                            break;
                        case DeltaworksMainMenu.VENDOR_PAYMENT :
                            currentTransaction = vendorPaymentTransaction;
                            // make sure we have some vendors to pay
                            if (vendorPaymentTransaction.getVendorList() == null) {
                                Dialogs.showError("dialogs/DialogNoVendors.xml"); 
                                return;
                            }
                            break;
                        case DeltaworksMainMenu.BILL_PAYMENT :
                            currentTransaction = billPaymentTransaction;
                            break;
                        case DeltaworksMainMenu.FORM_FREE_ENTRY :
                            currentTransaction = formFreeSendTransaction;
                            break;
                        case DeltaworksMainMenu.TRANSACTION_QUOTE :
                        	checkCachedResponses = true;
                            currentTransaction = moneyGramSendTransaction;
                            moneyGramSendTransaction.setFeeQuery(true);
                            break;
                        case DeltaworksMainMenu.DIRECTORY_AGENTS :
                            currentTransaction = agentDirectoryTransaction;
                            break;
                        case DeltaworksMainMenu.DIRECTORY_BILLERS :
                            currentTransaction = billPaymentTransaction;
                            billPaymentTransaction.setDirectoryOfBillers(true);
                            break;
						case DeltaworksMainMenu.ACCOUNT_DEPOSIT_PARTNERS:
							currentTransaction = moneyGramSendToAccountTransaction;
							break;
						case DeltaworksMainMenu.SEND_REVERSAL:
                            currentTransaction = sendReversalTransaction;
                            break;						
						case DeltaworksMainMenu.BROADCAST_MESSAGES:
							currentTransaction = broadcastMessagesTransaction;
							break;
                        case DeltaworksMainMenu.TRANSACTION_STATUS :
                            currentTransaction = transactionStatusTransaction;
                            break;
                        case DeltaworksMainMenu.REPRINT_RECEIPT :
                            currentTransaction = reprintReceiptTransaction;
                            break;
                        case DeltaworksMainMenu.REGISTER_MFA_TOKEN:
                            currentTransaction = registerMfaToken;
                            break;
                        case DeltaworksMainMenu.ADD_MINUTES :
                            currentTransaction = addMinutesTransaction;
                            break;
                        case DeltaworksMainMenu.CARD :
                            currentTransaction = prepaidCardTransaction;
//                            moneyGramCardTransaction.setDocumentSequence(MoneyGramCardTransaction.CARD_DOC_SEQ);
//                            moneyGramCardTransaction.setDocumentType(MoneyGramCardTransaction.CARD_DOC_SEQ);
                            break;
                    }
                    currentTransaction.setStatus(ClientTransaction.COMPLETED);
                    startTransactionInterface();

                    return;
                }

                // coming from the dispenser load transaction...start pending tran
                if (currentTransaction == dispenserLoadTransaction) {
                    if (pendingTransaction != null) {
                        if (currentTransaction.getStatus() == ClientTransaction.COMPLETED) {
                            currentTransaction = pendingTransaction;
                            currentTransaction.setStatus(ClientTransaction.COMPLETED);
                            startTransactionInterface();
                            return;
                        }
                        else
                            pendingTransaction = null;
                    }
                }

                // coming from the Multi Agent Mode transaction...start pending tran
                if (currentTransaction == multiAgentTransaction) {
                   if (pendingTransaction != null) {
                        if (currentTransaction.getStatus() == ClientTransaction.COMPLETED) {
                            currentTransaction = pendingTransaction;
                            currentTransaction.setStatus(ClientTransaction.COMPLETED);
                            startTransactionInterface();
                            return;
                        }
                        else
                            pendingTransaction = null;
                    }
					/*
					 * special case where no transmission was performed for the
					 * superagent before selecting the subagent
					 */
                    AgentCountryInfo.setAgentCountryFields();
                }


                // kill the idle timer
                IdleBackoutTimer.stopLong();

                Dialogs.closeAllDialogs();
                Messages.setCurrentLocale(UnitProfile.getInstance().get("LANGUAGE","en")); 


                // restore the main menu and display status of last transaction
                setMenuComponentsVisible(true);
                setTitle(Messages.getString("DeltaworksMainPanel.title")); 

                showPage("mainMenu", PageExitListener.NEXT); 
                if (currentTransaction == moneyGramSendTransaction) {
                    moneyGramSendTransaction.setFeeQuery(false);
                }
                if (currentTransaction == billPaymentTransaction) {
                    billPaymentTransaction.setDirectoryOfBillers(false);
                }

                if (currentTransaction == addMinutesTransaction) {
                	addMinutesTransaction.setDirectoryOfBillers(false);
                }

                //if there is a log error, display it now and clear the flag
                if (mustHandleLogsDialogPending) {
                    Debug.println("Setting status and displaying dialog!!"); 
                    showMustHandleLogsDialog();
                }

                // display a status about what just happened with the last transaction
                if (currentTransaction == installTransaction) {
                    ErrorManager.setStatus(Messages.getString("DeltaworksMainPanel.welcomeMessage"), 
                    ErrorManager.INFORMATION_STATUS);
                    deAuthMessage = false;
                    deActivateMessage=false;
                }
                else if (direction == PageExitListener.COMPLETED)
                    ErrorManager.setStatus(currentTransaction.getTransactionName() + Messages.getString("DeltaworksMainPanel.completedText"), ErrorManager.INFORMATION_STATUS); 
                else if (direction == PageExitListener.CANCELED)
                    ErrorManager.setStatus(currentTransaction.getTransactionName() + Messages.getString("DeltaworksMainPanel.canceledText"), ErrorManager.WARNING_STATUS); 
                else if (direction == PageExitListener.TIMEOUT)
                    ErrorManager.setStatus(currentTransaction.getTransactionName() + Messages.getString("DeltaworksMainPanel.timedOutText"), ErrorManager.WARNING_STATUS); 
                else if (direction == PageExitListener.FAILED)
                    ErrorManager.setStatus(currentTransaction.getTransactionName() + Messages.getString("DeltaworksMainPanel.failedText"), ErrorManager.ERROR_STATUS); 

                // special case of the install wizard...exit app if fail or cancel
                if (currentTransaction == installTransaction) {
                    if (direction != PageExitListener.COMPLETED) {
                        System.exit(NORMAL_EXIT);
                    }

                    DeltaworksStartup.getInstance().startBackgroundProcesses();

                    // set the last reauth time to NOW
                    UnitProfile.getInstance().setLastReauthTime(TimeUtility.currentTime());

                    setDemoOptionAccess();

                    // start the page creation now
                    createPages();

                    // enable / disable the dialup menu
                    getComponent("dialupMenu").setVisible(UnitProfile.getInstance().isDialUpMode()); 
                    getComponent("dialupMenu").setEnabled(UnitProfile.getInstance().isDialUpMode());

                    // enable / disable the accounts menu
                    getComponent("accountsMenu").setVisible(UnitProfile.getInstance().isMultiAgentMode()); 
                    getComponent("accountsMenu").setEnabled(UnitProfile.getInstance().isMultiAgentMode());

                    // Set modem speaker off after initial install and write profile item.
                    UnitProfile.getInstance().setModemSpeakerMuted(true);
                    RasProperties.setMuted(true);
                    ((JCheckBoxMenuItem) getComponent("modemSpeaker")).setSelected(true); 
                }

                // check if MANUAL_CTR_ENABLE_ENABLED is true and set visible.
                getComponent("ctrReport").setVisible("Y".equals(UnitProfile.getInstance().get("MANUAL_CTR_ENABLE", "N")));    

                getComponent("sendReversalTransaction").setEnabled(getTranAuth(DWValues.MONEY_GRAM_SEND_DOC_SEQ) && 
               		"Y".equals(UnitProfile.getInstance().get("ALLOW_SEND_CANCELS", "N")));   

                getComponent("receiveReversalTransaction").setEnabled(getTranAuth(DWValues.MONEY_GRAM_RECEIVE_DOC_SEQ) && 
                        "Y".equals(UnitProfile.getInstance().get("ALLOW_RECEIVE_REVERSALS", "N")));   

                getComponent("amendTransaction").setEnabled(getTranAuth(DWValues.MONEY_GRAM_SEND_DOC_SEQ) && 
                        "Y".equals(UnitProfile.getInstance().get("ALLOW_AMENDS", "N")));   

                // update the form label after every transaction
                updateFormsLabel();/*EEO: Comment out to allow pinpad on COM 1*/

                // clear the just-completed transaction and reclaim memory
                currentTransaction.clear();
                currentTransactionInterface.unloadPages();
                System.gc();

                // do we have a pending daily transmit to start up?
                Thread t = new Thread(new Runnable() {
                    @Override
					public void run() {
                        if (diagTransmitPending) {
                            transmitAll(false);
                        }
                    }
                });
                t.start();

                if(didConnectivityChange())
                    transmit();
            }
            finally {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
					public void run() {
                        transactionStarting = false;
                    }
                });
            }
        }
    }

    private String getAuditTransactionName(ClientTransaction ct){
    	String retval = "";
    	if (ct == moneyGramSendTransaction)
    		retval = "MG Send";
    	else if (ct == moneyGramReceiveTransaction)
    		retval = "MG Rec";
    	else if ((ct == billPaymentTransaction)
    			|| (ct == addMinutesTransaction))
    		retval = "MG BP";
    	else if (ct == formFreeSendTransaction)
    		retval = "MG FF";
//    	else if (ct == moneyGramCardReloadTransaction)
//    		retval = "Card Reload";
//    	else if (ct == moneyGramCardPurchaseTransaction)
//    		retval = "Card Purchase";
    	else if (ct instanceof SendReversalTransaction)
    		retval = "MG Rev";
    	else if (ct instanceof SimplePMTransaction)
    		retval = "Options";
    	else if (ct instanceof MoneyGramSendTransaction )
    		retval = "Status";
    	return retval;
    }

    /**
     * Implementation of ErrorListener.
     * Display a dialog to the user and exit the application.
     */
    @Override
	public void fatalErrorOccurred(String message, Exception exception) {
        Dialogs.showError("dialogs/DialogFatalError.xml", message); 
        System.exit(1);
    }

    /**
     * Implementation of ErrorListener.
     * Display a dialog to the user about an application critical error.
     */
    @Override
	public void criticalErrorOccurred(final String message, Exception exception) {
        if ((message.toUpperCase(Locale.US)).indexOf("DEAUTH") >= 0) 
            deAuthMessage = true;
        else
        	if ((message.toUpperCase(Locale.US)).indexOf("DEACTIVATED") >= 0) 
                deActivateMessage = true;
        Runnable runme = new Runnable() {
            @Override
			public void run() {
                Dialogs.showError("dialogs/BlankDialog.xml", message); 
            }
        };
        if (!frameSetUp) {
            //  pendingDialog = runme;
            DeltaworksStartup.getInstance().setPendingDialog(runme);
        }
        else {
            runme.run();
            ErrorManager.resetActiveFlag();
        }
    }

    /**
     * Implementation of ErrorListener.
     * Display a dialog to the user about an application warning.
     */
    @Override
	public void warningOccurred(final String message, Exception exception) {
        Runnable runme = new Runnable() {
            @Override
			public void run() {
                Dialogs.showWarning("dialogs/BlankDialog.xml", message); 
            }
        };

        if (!frameSetUp) {
            //pendingDialog = runme;
            DeltaworksStartup.getInstance().setPendingDialog(runme);
        }
        else {
            runme.run();
            ErrorManager.resetActiveFlag();
        }
    }

    /**
     * Implementation of ErrorListener.
     * Handles any debug events.
     */
    @Override
	public void debugEventOccurred(String message, Exception exception) {
        Debug.println("EXCEPTION:\n\n"); 
        Debug.println(exception.getMessage());
        Debug.println("\n\n"); 
        Debug.printStackTrace(exception);
        ErrorManager.resetActiveFlag();
    }

    /**
     * Implementation of ErrorListener method.
     * Re-set the menu item and transaction button access accordingly in
     * case any products statuses have changed.
     * @return Returns true only if communication occurred successfully and
     * a new profile needs to be retrieved. Otherwise returns false. sws
     */
    public boolean newProfileNeeded(MessageFacadeParam param) {
        // nothing to do in demo mode...
        if (UnitProfile.getInstance().isDemoMode())
            return true;

        // do we have to send up any profile changes?
        boolean successfulSend = simplePMTransaction.send(param);
        if (!successfulSend)
            return false;

        /* If the profile editor has the POS authorized and the local
         * profile has the POS deauthorized, then there is a good
         * possibility that the host and POS are out of sync in regards
         * to the POS_STATUS.  This will force a profile exchange.
         */
        if(!UnitProfile.getInstance().isProfileChanged() && UnitProfile.getInstance().checkTransmitConditions())
            UnitProfile.getInstance().setProfileChanged();

        // Is the POS currently authorized prior to getting a new profile?
        final boolean posAuthorizedInitial = UnitProfile.getInstance().isPosAuthorized();

        // go out and grab the new profile from the host
        boolean newProf = simplePMTransaction.getUpdatedProfile(param);


        // re-set the menu option/button access in case any
        // products changed status
        Debug.println("changing status in newProfileNeeded!"); 

        if (newProf) {
            // get new connect method, dialup or network.
            newDialup = UnitProfile.getInstance().isDialUpMode();

            ErrorManager.setStatus(Messages.getString("DeltaworksMainPanel.updatedProfileMessage"), 
            ErrorManager.INFORMATION_STATUS);

            // set the POS authorization only if it has changed
            if (posAuthorizedInitial != UnitProfile.getInstance().isPosAuthorized()) {
                setPosAuthorization(UnitProfile.getInstance().isPosAuthorized());
            }
        }
        return true;
    }

    /**
     * Implementation of ErrorListener.
     * A diag transmit is needed. Start it if we are at the main menu, otherwise
     *   wait until the end of the current transaction to start it.
     * @param manual whether or not this transmit was triggered by the menu item
     * @return whether or not the transmit succeeded
     */
    @Override
	public boolean transmitAll(final boolean manual) {
        MessageFacadeParam parameters;
        boolean xmitCheck = false;
        
        final int KB = 1024;
        final int DEFAULT_THRESHOLD_IN_KB = 128000; //Was 48000 KB
    	final Runtime runtime = Runtime.getRuntime();

        // ignore daily attempt if during manual and vice versa
        if (duringManualTransmit) {
            return false;
        }
        VersionManifest.resetDownloadCounter();
        // if we are in a transaction, start daily transmit after it is complete
        if(!didConnectivityChange()){
            if (!cardLayout.getVisiblePageName().equals("mainMenu")
            		|| Dialogs.getPasswordDialogActive() ) { 
                diagTransmitPending = true;
                return false;
            }
        }
        // reset the connectivity changed flag so that you don't continue to tranmit
        setConnectivityChange(false);
        setDuringManualTransmit(true);

        //   preserve the mode until after process completes
        boolean initialDialup = UnitProfile.getInstance().isDialUpMode();
        newDialup = UnitProfile.getInstance().isDialUpMode();
		final int reterivalType = manual ? CountryInfo.MANUAL_TRANSMIT_RETERIVAL : CountryInfo.AUTOMATIC_TRANSMIT_RETERIVAL;
        try {
            // pause the semaphores during the transmit...but let the tran
            //  creation semaphore continue...
            pauseSemaphores();
            tranCreationSemaphore.resume();
            String dialogTitle;

            if (manual) {
                dialogTitle = getManualDialogTitle();
                parameters = new MessageFacadeParam(MessageFacadeParam.REAL_TIME, MessageFacadeParam.DO_NOT_HIDE);
                xmitCheck = transmitProcess(parameters, initialDialup, dialogTitle, reterivalType);
            }
            else {
                DeltaworksStartup.setKillScheduledTransmit(false);
                dialogTitle = getScheduledDialogTitle();
                parameters =
                    new MessageFacadeParam(
                        MessageFacadeParam.NON_REAL_TIME,
                        MessageFacadeParam.AUTO_RETRY);
                while (xmitCheck == false
                    && parameters.getDiagAttempt() < MessageFacade.getMaxDialAttempts()) {
                    try {
                        long delay = parameters.getDiagDelay();
                        if (delay > 0) {
                        	setDuringManualTransmit(false);
                            //setTransactionEnabled(true);
                        	String text = Messages.getString("DeltaworksMainPanel.1947");
                    		ExtraDebug.println("-x-setActivity[" + text + "]"); 
                            CommunicationDialog.getInstance().setMessage(text + ".", 0);
                            int click = 1000;
                            while (delay > 0) {
                                Thread.sleep(click);
                                delay = delay - click;
                                if (DeltaworksStartup.isKillScheduledTransmit()) {
                                    return false;
                                }
                            }
                        }
                    }
                    catch (InterruptedException e) {
                    }

                    setDuringManualTransmit(true);
                    if (cardLayout.getVisiblePageName().equals("mainMenu")) { 
                        xmitCheck = transmitProcess(parameters, initialDialup, dialogTitle, reterivalType);
                    }
                    else
                        xmitCheck = false;
                }
            }

            if (xmitCheck == false) {
                return false;
            }

            // host was contacted successfully
            // connected = true;
            DeltaworksStartup.getInstance().setConnected(true);

            Debug.println("mustHandleLogsFlag = " + mustHandleLogsFlag + 
            " in transmitDiags right before resetting access"); 

            //cleanup mustHandleLogsFlag if it was set
            if (mustHandleLogsFlag) {
                mustHandleLogsFlag = false;
                clearMustHandleLogsFlag();
            }
            MessageFacade.note(Messages.getString("MessageFacade.13"), Messages.getString("MessageFacade.13"));
            int i = cleanupAuditLog();
            Debug.println("Removed " + i + " rows from the audit log !!!");

            ErrorManager.setStatus(Messages.getString("DeltaworksMainPanel.connectedMessage"), 
            ErrorManager.INFORMATION_STATUS);

            UnitProfile.getInstance().setForceTransmit(false);
            ReceiptTemplates.getTemplateInventory();

            return true;
        } finally {

        	setDuringManualTransmit(false);
            resumeSemaphores();

            Pinpad.INSTANCE.updateProfileOptions();

   		 	// If a pinpad is required, log the pinpad statistics on a transmit.
            
            if (Pinpad.INSTANCE.getIsPinpadRequired() == true) {
            	Pinpad.INSTANCE.logStatistics();
    		}

        	// Log Java heap memory usage
        	
            Debug.println("###Java Heap Memory (in KB)" +
            	", Used=" + (runtime.totalMemory() - runtime.freeMemory()) / KB + 
            	", Free=" + runtime.freeMemory() / KB +
            	", Total=" + runtime.totalMemory() / KB +
            	", Max=" + runtime.maxMemory() / KB);

            Debug.println("transmitDiags() done, isSoftwareVersionPending() = " 
            	+UnitProfile.getInstance().isSoftwareVersionPending());
            
            // check to see if the software has changed and notify ErrorManager
            
            int prereqStatus =-1;
            VersionManifest manifest = null;
			if (! UnitProfile.getInstance().isDemoMode()) {

				PauseSemaphore softwareDownloadSemaphore = UpdateStatusDialog.getInstance().getPrereqDownloadSemaphore();
				softwareDownloadSemaphore.resume();
				SoftwareVersionUtility softwareVersionUtility = new SoftwareVersionUtility();
				manifest = softwareVersionUtility.getCurrentManifest();
				softwareVersionUtility.handlePrePostUpgrade(softwareDownloadSemaphore, VersionManifestEntry.TYPE_PRE_REQUISITE, 0, true, manifest);
				SoftwareStatus downloadStatus = SoftwareVersionUtility.getDownloadStatus(VersionManifestEntry.TYPE_PRE_REQUISITE);
				long startTime = Calendar.getInstance().getTimeInMillis();
				softwareDownloadSemaphore.resume();
				
				prereqStatus = -1;
				if (manifest != null) {
					prereqStatus = manifest.getStatus(new StringBuffer(), VersionManifestEntry.TYPE_PRE_REQUISITE, true);
				}
				
				Debug.println("Prerequisite Status time taken....("+(Calendar.getInstance().getTimeInMillis()-startTime)+")");
				
				if ((downloadStatus != null) && (prereqStatus != VersionManifestEntry.READY)) {
					processPrereqDownload(downloadStatus.getCount(), downloadStatus.getNumber(),manifest, reterivalType);
				}
			}
        	
        	// Hide the communication dialog
        	
            MessageFacade.hideDialog(1000); 	
			
			Debug.println("Prerequisite JRE ready " + ((prereqStatus == 0) ? "Yes": "No"));
            if (prereqStatus == VersionManifestEntry.READY  && UnitProfile.getInstance().isSoftwareVersionPending()) {
                getComponent("pendingVersionLabel").setVisible(false); 
                getComponent("downloadFailedLabel").setVisible(false); 
                softwareChanged(manual, reterivalType, manifest);
            
            } else {    	
				if (! UnitProfile.getInstance().isDemoMode()) {
					processPostUpgradeDownload(manifest, reterivalType);
				}
            	// not manual and not about to download software.
            	long total = runtime.totalMemory() / KB;
                if (!manual && (total > UnitProfile.getInstance().
                		get("MEMORY_RESTART_THRESH", DEFAULT_THRESHOLD_IN_KB))) { 

                	//Log Java heap memory usage
                	Debug.println("###Java Heap Memory (in KB)" +
                    	", Used=" + (runtime.totalMemory() - runtime.freeMemory()) / KB + 
                    	", Free=" + runtime.freeMemory() / KB +
                    	", Total=" + runtime.totalMemory() / KB +
                    	", Max=" + runtime.maxMemory() / KB);

                    Debug.println("Memory restart threshold exceeded"); 
                    showRestartWarning("dialogs/DialogMemoryRestart.xml"); 
                    tryExit(RESTART_EXIT);
                }
            }
        }
    }

    /**
     * Is DW currently doing a Manual Transmit
     * @return true if DW is currently performing a manual transmit
     */
    public static boolean isDuringManualTransmit() {
		return duringManualTransmit;
	}

	/**
	 * Sets whether or not DW is currently doing a manual transmit.
	 * @param duringManualTransmit (boolean value)
	 */
	public static void setDuringManualTransmit(boolean duringManualTransmit) {
		DeltaworksMainPanel.duringManualTransmit = duringManualTransmit;
	}

	private boolean transmitProcess(final MessageFacadeParam param, final boolean initialDialup, String title, int reterivalType) {

        CommunicationDialog.getInstance().setTitle(title);

        //==================================================================
        // Check In
        //==================================================================

        if (! DeltaworksStartup.getInstance().checkInWithHost(param, reterivalType)) {
           // if(!continueCheckinProcess)
        	return false;
        }

        //==================================================================
        // Currency Information
        //==================================================================

    	Currency.getCurrencyInfo(reterivalType);

        //==================================================================
        // Country and country subdivision information.
        //==================================================================

        CountryInfo.checkCountryInformation(reterivalType);

        diagTransmitPending = false;

        //==================================================================
        // Customer Compliance Information
        //==================================================================
        
        Boolean success = Boolean.FALSE;
        if (transmitter.runCycle(param, false)) {
            // the balance message send up the SAR count and since SAR are not
            // a part of this first release, it will be commented out.
            // FUTURE uncomment the following line for SAR balancing.
            //            itemForwarder.sendBalanceMessage(param);
            MessageLogic logic = new MessageLogic() {
                @Override
				public Object run() {
                    return finishDiagXmit(param, initialDialup);
                }
            };
            success = (Boolean) MessageFacade.run(param, logic, Boolean.FALSE);
            UnitProfile.getInstance().setDialUpMode(newDialup, true);
            CommunicationDialog.getInstance().refreshIcon();
            getComponent("dialupMenu").setVisible(UnitProfile.getInstance().isDialUpMode()); 
            getComponent("dialupMenu").setEnabled(UnitProfile.getInstance().isDialUpMode()); 
            getComponent("accountsMenu").setVisible(UnitProfile.getInstance().isMultiAgentMode()); 
            getComponent("accountsMenu").setEnabled(UnitProfile.getInstance().isMultiAgentMode());
            getComponent("ctrReport").setVisible("Y".equals(UnitProfile.getInstance().get("MANUAL_CTR_ENABLE", "N")));    
           	getComponent("sendReversalTransaction").setEnabled(getTranAuth(DWValues.MONEY_GRAM_SEND_DOC_SEQ) && 
                    "Y".equals(UnitProfile.getInstance().get("ALLOW_SEND_CANCELS", "N")));   
           	getComponent("receiveReversalTransaction").setEnabled(getTranAuth(DWValues.MONEY_GRAM_RECEIVE_DOC_SEQ) && 
                    "Y".equals(UnitProfile.getInstance().get("ALLOW_RECEIVE_REVERSALS", "N")));   
            getComponent("amendTransaction").setEnabled(getTranAuth(DWValues.MONEY_GRAM_SEND_DOC_SEQ) && 
                    "Y".equals(UnitProfile.getInstance().get("ALLOW_AMENDS", "N")));   

        }

        //==================================================================
        // Service Options
        //==================================================================

    	ServiceOption.getServiceOptions(reterivalType, false);

        //==================================================================
        // Receipt Templates
        //==================================================================

        ReceiptTemplates.update(reterivalType);
        
        //==================================================================
        // Cached AC Responses
        //==================================================================

        CachedACResponses.cacheACResponses(false);

        //==================================================================
        // Account Deposit Partners List
        //==================================================================

        // Populate the account deposit partners list if a new code table was
		// not loaded. If a new code table was loaded, the account deposit
        // partners list will be populated after the code table is loaded.

        if (isCciLogEmpty() && success.booleanValue())
            return true;
        else {
            ErrorManager.setStatus(Messages.getString("DeltaworksMainPanel.disconnectedMessage"), ErrorManager.ERROR_STATUS); 
            //connected = false;
            return false;
        }
    }

    private int cleanupAuditLog() {
    	return AuditLog.cleanup(UnitProfile.getInstance().get("DAYS_IN_AUDIT_TRAIL", "7"));
    }

    /**
     * Returns true if the CCI log is empty or unreadable.
     * The CCI log should never become unreadable, but if it does
     * don't let that stop a transmit because then we'll never get a
     * debug log to diagnose the problem.
     */
    private boolean isCciLogEmpty() {
        try {
            return CCILog.getInstance().isEmpty();
        }
        catch (IOException e) {
            Debug.printStackTrace(e);
            return true;
        }
    }

    private Boolean finishDiagXmit(MessageFacadeParam param, boolean initialDialup) {
        if (newProfileNeeded(param)) {
            //            newDialup = UnitProfile.getUPInstance().isDialUpMode();
            UnitProfile.getInstance().setDialUpMode(initialDialup, false);
            // Transmit the DiagLog and the DebugLog (if called for by the profile)
            // If the DiagLog fails, do not continue with the DebugLog - sent it next time
            if (DiagLog.getInstance().transmit() && (UnitProfile.getInstance().get("TRANSMIT_DEBUG", "Y").equals("N") ||   
            DebugLog.getInstance().transmit(param)))
                return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    private void clearMustHandleLogsFlag() {
        mustHandleLogsFlag = false;
        if (mustHandleLogsDialogPending) {
            mustHandleLogsDialogPending = false;
        }
    }

    @Override
	public void diagTransmitStarted() {

    }

    /** @deprecated No longer needed, looks like this was a kludge anyway. */
    @Override
	public void diagTransmitFinished(boolean manual) {
    }

    /** Display a dialog box stating that the app will restart. The
     *  user has no choice, but wait five seconds so it
     *  is clear the app isn't crashing.
     */
    public void showRestartWarning(String xmlFilename) {
        Thread timer = new Thread(new Runnable() {
            @Override
			public void run() {
                try {
                    Thread.sleep(5000);
                    // cheat by using idle timeout feature to close the dialog
                    Dialogs.idleTimeout();
                }
                catch (InterruptedException e) {
                }
            }
        });
        timer.start();
        Dialogs.showWarning(xmlFilename);
        // dialog has exited on its own, so kill the thread
        if (timer.isAlive())
            timer.interrupt();
    }

    /**
     * Implementation of ErrorListener. Called when a log throws an IOException
     * when writing and causes
     * application to deactivate its options except transmit and clear logs
     * (basically the same options as if de-authed)
     */

    @Override
	public void mustHandleLogs() {
        Debug.println("Must handle logs!!!!!!!!!!!!!!"); 
        //only print one message per problem, even though this may be called
        //several times

        if (!mustHandleLogsFlag) {
            mustHandleLogsFlag = true;
            mustHandleLogsDialogPending = true;
            ErrorManager.setStatus(Messages.getString("DeltaworksMainPanel.mustHandleLogsMessage"), ErrorManager.ERROR_STATUS); 
        }
    }

    private void showMustHandleLogsDialog() {
        mustHandleLogsDialogPending = false;
        SwingUtilities.invokeLater(new Runnable() {
            @Override
			public void run() {
                Dialogs.showWarning("dialogs/DialogMustHandleLogs.xml"); 
            }
        });
    }

    /**
     * Called when the host informs us that our software version is now out of
     * date. Go to the host and retrieve the new version in a background
     * process. When this is complete, notify the user that a new version is
     * present. The flag from the host will only be set on ClientCheckInMsg.
     */
	private void softwareChanged(boolean manual, int reterivalType, VersionManifest manifest) {
		Debug.println("softwareChanged(" + manual + ") newSoftwareReady = " + newSoftwareReady);  
		// do we already have new software downloaded and haven't restarted yet?
		boolean readyForUpgrade = true;
		if (newSoftwareReady) {
			softwareDownloadFinished(true, null);
		} else if (manual) {
			Calendar forcedLocalTime = null;
			
			String formattedDate = null;
			boolean upgrade = false;
			boolean forcedUpgradeFlag = false;

			// If a communication dialog window is showing, wait until it
			// closes.

			while (CommunicationDialog.getInstance().isShowing()) {
				try {
					Thread.sleep(100);
				} catch (Exception e) {
				}
			}
			
			forcedLocalTime = getLocalForcedTime();
			if (forcedLocalTime != null) {
				formattedDate = FormatSymbols.formatDateForLocale(forcedLocalTime.getTime());
				upgrade = userConfirm("dialogs/DialogForcedSoftwareUpdate.xml", formattedDate + ".");
				forcedUpgradeFlag = true;
			} else {
				upgrade = userConfirm("dialogs/DialogSoftwareUpdate.xml");
				forcedUpgradeFlag = false;
			}

			if (!upgrade) {
				if (forcedUpgradeFlag)
					readyForUpgrade = checkForForcedUpgrade(forcedLocalTime);
				else {
					UnitProfile.getInstance().setSoftwareVersionPending(false);
					return;
				}
			}

		}
		if (readyForUpgrade) {
			UnitProfile.getInstance().set("URL_EXTRADEBUG", "N", "P", false);
			UnitProfile.getInstance().set("URL_DEBUGENCRYPTION", "N", "P", false);

			PauseSemaphore softwareDownloadSemaphore = DeltaworksStartup.getInstance().getSoftwareDownloadSemaphore();
			softwareDownloadSemaphore.resume();
			UnitProfile.getInstance().setForceTransmit(true);
			SoftwareVersionUtility.handlePendingVersion(softwareDownloadSemaphore, reterivalType, null, manifest);
		}
	}

	private void processPrereqDownload(int actual,int total,VersionManifest manifest, int reterivalType) {
		long startTime = Calendar.getInstance().getTimeInMillis();
		Debug.println("Process Prerequisite Download....");
		PauseSemaphore softwareDownloadSemaphore = UpdateStatusDialog.getInstance().getPrereqDownloadSemaphore();
		softwareDownloadSemaphore.resume();
		SoftwareVersionUtility softwareVersionUtility = new SoftwareVersionUtility();

		softwareVersionUtility.handlePrePostUpgrade(softwareDownloadSemaphore, VersionManifestEntry.TYPE_PRE_REQUISITE, getFileDownloadLimit(actual, total), false, manifest);
		Debug.println("Prerequisite Download time taken....("+(Calendar.getInstance().getTimeInMillis()-startTime)+")");
	}

	private void processPostUpgradeDownload(VersionManifest manifest, int reterivalType) {
		long startTime = Calendar.getInstance().getTimeInMillis();
		Debug.println("Process PostUpgrade Download....");
		PauseSemaphore softwareDownloadSemaphore = UpdateStatusDialog.getInstance().getPostUpgrdDownloadSemaphore();
		softwareDownloadSemaphore.resume();
		SoftwareVersionUtility softwareVersionUtility = new SoftwareVersionUtility();
		softwareVersionUtility.handlePrePostUpgrade(softwareDownloadSemaphore, VersionManifestEntry.TYPE_POST_UPGRADE, getFileDownloadLimit(0, 0), false, manifest);

		Debug.println("PostUpgrade Download time taken....("+(Calendar.getInstance().getTimeInMillis()-startTime)+")");
	}
	
	private int getFileDownloadLimit(int actual, int total) {
		int limit = 1;
		String limitStr = UnitProfile.getInstance().get("JRE_FILE_DOWNLOAD_LIMIT", "1");
		try {
			limit = Integer.parseInt(limitStr);
		} catch (NumberFormatException e) {
			Debug.println(e.getMessage());
		}

		Calendar forcedLocalTime = getLocalForcedTime();
		if (forcedLocalTime != null) {
			int days = TimeUtility.daysBetween( Calendar.getInstance().getTime(),forcedLocalTime.getTime()) - 1;

			if (days > 0) {
				if (total != actual) {
					limit = (int) Math.ceil((double) (total - actual) / (double) days);
				}
			}
		}
		if(limit<=0){
			limit=1;
		}
		return limit;
	}
    private Calendar getLocalForcedTime() {
    	Calendar americaTime = null;
    	Calendar localTime = null;
    	String forcedDate = UnitProfile.getInstance().get("VERSION_UPGRADE_DATE", "");
		Date d;
    	try {
    	     d = new SimpleDateFormat(TimeUtility.DATE_MONTH_DAY_LONG_YEAR_FORMAT).parse(forcedDate);
    	}  catch (Exception e) {
    		return null;
    	}

    	americaTime = new GregorianCalendar(TimeZone.getTimeZone("America/Chicago"));
    	americaTime.setTime(d);
//    	americaTime.set(Calendar.MONTH, d.getMonth());
//    	americaTime.set(Calendar.DAY_OF_MONTH, d.getDate());
//    	americaTime.set(Calendar.YEAR, d.getYear() + 1900);
    	americaTime.set(Calendar.HOUR, 0);
    	americaTime.set(Calendar.MINUTE, 0);
    	americaTime.set(Calendar.SECOND, 0);
    	americaTime.set(Calendar.MILLISECOND, 0);

    	String tz = UnitProfile.getInstance().get("AGENT_TIME_ZONE_NAME", "America/Chicago");

    	localTime = new GregorianCalendar(TimeZone.getTimeZone(tz));
    	localTime.setTimeInMillis(americaTime.getTimeInMillis());
    	localTime.set(Calendar.HOUR, 0);
    	localTime.set(Calendar.MINUTE, 0);
    	localTime.set(Calendar.SECOND, 0);
    	localTime.set(Calendar.MILLISECOND, 0);

    	return localTime;
    }

    private boolean checkForForcedUpgrade(Calendar americaTime) {

    	Calendar currentDate = Calendar.getInstance(FormatSymbols.getLocale());
    	Date d = TimeUtility.currentTime();
    	currentDate.setTime(d);
    	currentDate.set(Calendar.HOUR, 0);
    	currentDate.set(Calendar.MINUTE, 0);
    	currentDate.set(Calendar.SECOND, 0);
    	currentDate.set(Calendar.MILLISECOND, 0);

    	Calendar forcedDate = americaTime;

    	boolean confirm = false;
    	if (currentDate.getTimeInMillis() >= forcedDate.getTimeInMillis()) {
    		confirm = userConfirmAbort("dialogs/DialogForceSoftwareUpdate.xml");
    		if (!confirm) {
    			UnitProfile.getInstance().setForceUpgrade(true);
    			return false;
    		}
    		else {
    			UnitProfile.getInstance().setForceUpgrade(false);
    			return true;
    		}
    	}
    	else
    		return false;

    }

    /**
     * Implementation of ErrorListener method.
     * invoked when a software download has started. Start the indicator and
     *   write out a general report diag.
     */
    @Override
	public void softwareDownloadStarted() {
        getComponent("transmittingPanel").setVisible(true); 
        softwareLabel.setText(Messages.getString("DeltaworksMainPanel.1948")); 

        // write out the report diag saying we have started the download
        ErrorManager.writeGeneralDiagnostic("SOFTWARE DOWNLOAD STARTED"); 
    }

    @Override
	public void setProgressBarString(String message) {
        softwareLabel.setText(message);
    }

    @Override
	public void setProgressBarProgress(int progress) {
		((JProgressBar) getComponent("progressBar")).setValue(progress);
    }

    @Override
	public void setProgressBarMaxValue(int maxValue) {
		((JProgressBar) getComponent("progressBar")).setMaximum(maxValue);
    }

    @Override
	public void setProgressBarMinValue(int minValue) {
		((JProgressBar) getComponent("progressBar")).setMinimum(minValue);
    }

    /**
     * Implementation of ErrorListener method.
     * Called when a new software version has been downloaded to the POS.
     *   If it succeeded, inform the user that the new version has been
     *   downloaded and give them the option to restart the application.
     */
    @Override
	public void softwareDownloadFinished(boolean successful, String resultString) {

        Debug.println("softwareDownloadFinished() successful = " + successful); 

        if (resultString != null) {
            Debug.println("Download finished, result = " + resultString); 
        }

        getComponent("transmittingPanel").setVisible(false); 

        if (successful) {
            getComponent("pendingVersionLabel").setVisible(true); 
            getComponent("downloadFailedLabel").setVisible(false); 
            newSoftwareReady = true;
            UnitProfile.getInstance().set("UPDATE_DWISI", "Y", "P",  true);

            // If we are outside store hours and at main menu, restart.
            if (cardLayout.getVisiblePageName().equals("mainMenu")) { 
                Debug.println("Restarting to put new software in effect"); 
                showRestartWarning("dialogs/DialogRestarting.xml"); 
                  tryExit(RESTART_EXIT);
            }
        }
        else {
            getComponent("pendingVersionLabel").setVisible(false); 
            getComponent("downloadFailedLabel").setVisible(true); 
        }

        setProgressBarString(""); 

        // write out the report diag saying we have finished the download
        ErrorManager.writeGeneralDiagnostic("SOFTWARE DOWNLOAD " + resultString); 
    }

    /**  ENH - 1133
     * Implementation of ErrorListener method.
     * invoked when an item transmission has started. Show an indicator letting
     *   the user know it is taking place. Do not overwrite the download label.
     */
    @Override
	public void ftpFileStarted() {

        getComponent("transmittingPanel").setVisible(true); 
        softwareLabel.setText(Messages.getString("DeltaworksMainPanel.1950")); 

        // write out the report diag saying we have started the download
        ErrorManager.writeGeneralDiagnostic("FTP FILE TO HOST STARTED"); 
    }

    /** ENH - 1133
     * Implementation of ErrorListener method.
     * invoked when an item transmission has finished.
     */
    @Override
	public void ftpFileFinished() {
        getComponent("transmittingPanel").setVisible(false); 
        ErrorManager.writeGeneralDiagnostic("FTP FILE TO HOST FINISHED"); 
    }

    /**
      * The FRONT ISI COMMAND.  Bring DW back to the front.
      */
     @Override
	public void executeIsiFrontCommand() {
        parentFrame.setState(Frame.ICONIFIED);
        parentFrame.setState(Frame.NORMAL);
        parentFrame.repaint();

        JDialog currentDialog = Dialogs.getCurrentDialog();
        if (currentDialog != null) {
            currentDialog.getMostRecentFocusOwner().requestFocus();
        }
     }

     /**
       * The FRONT ISI COMMAND.  Minimize DW and put to the background.
       */
      @Override
	public void executeIsiBackCommand() {
         parentFrame.setState(Frame.ICONIFIED);
         parentFrame.repaint();
      }

     /**
       * Set the RP ISI COMMAND for the transaction.
       */
      @Override
	public void setIsiRpCommandValid(boolean allow) {
          isiRpCommand = allow;
      }

    /**
     * Get the RP ISI COMMAND for the transaction.
     */
    @Override
	public boolean getIsiRpCommandValid() {
        return isiRpCommand;
    }

    /**
     * Set the EXE ISI COMMAND for the transaction.
     */
    @Override
	public void setIsiExeCommand(boolean allow) {
        isiExeCommand = allow;
    }

    /**
     * Set the EXE ISI COMMAND for the transaction.
     */
    @Override
	public void setIsiExe2Command(boolean allow) {
        isiExe2Command = allow;
    }

    /**
     * Get the START ISI COMMAND for the transaction.
     */
    @Override
	public boolean getIsiAppendMode() {
        return isiAppendMode;
    }

    /**
     * Set the EXE ISI COMMAND for the transaction.
     */
    @Override
	public void setIsiAppendMode(boolean append) {
        isiAppendMode = append;
    }


    /**
     * Get the EXE ISI COMMAND for the transaction.
     */
    @Override
	public boolean getIsiExeCommand() {
        return isiExeCommand;
    }

    /**
     * Get the EXE2 ISI COMMAND for the transaction.
     */
    @Override
	public boolean getIsiExe2Command() {
        return isiExe2Command;
    }

    /**
     * Set the ISI EXE Program Arguments for the transaction.
     */
    @Override
	public void setIsiExeProgramArguments(String arguments) {
        isiExeCommandArguments = arguments;
    }

    /**
     * Set the ISI EXE2 Program Arguments for the transaction.
     */
    @Override
	public void setIsiExe2ProgramArguments(String arguments) {
    	isiExe2CommandArguments = arguments;
    }

    /**
     * Get the ISI EXE Program Arguments for the transaction.
     */
    @Override
	public String getIsiExeProgramArguments() {
        return isiExeCommandArguments;
    }

    /**
     * Get the ISI EXE2 Program Arguments for the transaction.
     */
    @Override
	public String getIsiExe2ProgramArguments() {
        return isiExe2CommandArguments;
    }

    /**
     * Set the ISI skip record.
     */
    @Override
	public void setIsiSkipRecord(boolean skip) {
        isiSkipRecord = skip;
    }

    /**
     * Get the ISI skip record.
     */
    @Override
	public boolean getIsiSkipRecord() {
        return isiSkipRecord;
    }

    /**
     * Set the ISI EXE Program Arguments for the transaction.
     */
    @Override
	public void setIsiTransactionInProgress(boolean state) {
        isiTransactionInProgress = state;
    }

    /**
     * Get the ISI EXE Program Arguments for the transaction.
     */
    @Override
	public boolean getIsiTransactionInProgress() {
        return isiTransactionInProgress;
    }


    @Override
	public void setIsiFrontCommandOccurred(boolean state) {
    	isiFrontCommandOccurred = state;
    }

    @Override
	public boolean getIsiFrontCommandOccurred() {
        return isiFrontCommandOccurred;
    }



    @Override
	public void setIsiAllowDataCollection(boolean state) {
        isiAllowDataCollection = state;
    }

    @Override
	public boolean getIsiAllowDataCollection() {
        return isiAllowDataCollection;
    }

    @Override
	public void setIsiRequireRcptTextData(boolean state) {
        isiAllowRcptTextData = state;
    }

    @Override
	public boolean getIsiRequireRcptTextData() {
        return isiAllowRcptTextData;
    }


	@Override
	public boolean getIsiControlledLaunchCardReload() {
		return isiCardReload;
	}

	@Override
	public void setIsiControlledLaunchCardReload(boolean b) {
		isiCardReload = b;

	}

	@Override
	public boolean getIsiControlledLaunchCardPurchase() {
		return isiCardPurchase;
	}


	@Override
	public void setIsiControlledLaunchCardPurchase(boolean b) {
		isiCardPurchase = b;
	}

    /**
     * Get the NF ISI COMMAND for the transaction.
     */
    @Override
	public boolean getIsiNfCommand() {
        return isiNfCommand;
    }

    /**
     * Set the EXE ISI COMMAND for the transaction.
     */
    @Override
	public void setIsiNfCommand(boolean allow) {
        isiNfCommand = allow;
    }

    /**
     * Set the ISI NF Program Arguments for the transaction.
     */
    @Override
	public void setIsiNfProgramArguments(String arguments) {
        isiNfCommandArguments = arguments;
    }

    /**
     * Get the ISI EXE Program Arguments for the transaction.
     */
    @Override
	public String getIsiNfProgramArguments() {
        return isiNfCommandArguments;
    }





    /**
     * Reset the ISI parameters to there default stating position
     *
     */
    @Override
	public void initializeIsiFields(){
	    if (currentTransaction.isFinancialTransaction() || currentTransaction instanceof DispenserLoadTransaction){
	    	setIsiTransactionInProgress(false);
			setIsiFrontCommandOccurred(false);
			setIsiExeCommand(false);
			setIsiExe2Command(false);
			setIsiExeProgramArguments("");
			setIsiRpCommandValid(false);
	    }
    }

    @Override
	public void setupControlledLaunchISI(String string) {
		isiCardReload =false;
		isiCardPurchase =false;

    	if (string.length() > 0)
    		setIsiAppendMode(false);
    	if (parentFrame.getTitle().startsWith(Messages.getString("DeltaworksMainPanel.title"))){
			if (string.equals(MainPanelDelegate.clMoneyGramSend)){
				if (getTranAuth(DWValues.MONEY_GRAM_SEND_DOC_SEQ))
					mainMenuPage.sendMoneyButtonAction();
				else
					setupErrorISI("02");
			}
			else if (string.equals(MainPanelDelegate.clBillPayment)){
				if (getTranAuth(DWValues.BILL_PAYMENT_DOC_SEQ))
					mainMenuPage.billPaymentButtonAction();
				else
					setupErrorISI("02");
			}
			else if (string.equals(MainPanelDelegate.clMoneyOrder)) {
				if (getTranAuth(DWValues.MONEY_ORDER_DOC_SEQ))
					mainMenuPage.moneyOrderButtonAction();
				else
					setupErrorISI("02");
			}
			else if (string.equals(MainPanelDelegate.clMoneyGramReceive)){
				if (getTranAuth(DWValues.MONEY_GRAM_RECEIVE_DOC_SEQ))
					mainMenuPage.receiveMoneyButtonAction();
				else
					setupErrorISI("02");
			}
			else if (string.equals(MainPanelDelegate.clCardReload)) {
				if (getTranAuth(DWValues.MONEY_GRAM_CARD_DOC_SEQ)
						&& getTranAuth(DWValues.MONEY_GRAM_CARD_RELOAD_DOC_SEQ)){
					mainMenuPage.cardButtonAction();
					isiCardReload =true;
				} else{
					setupErrorISI("02");
				}
			}
			else if (string.equals(MainPanelDelegate.clCardPurchase)) {
				if (getTranAuth(DWValues.MONEY_GRAM_CARD_DOC_SEQ)
						&& getTranAuth(DWValues.MONEY_GRAM_CARD_PURCH_DOC_SEQ)){
					mainMenuPage.cardButtonAction();
					isiCardPurchase =true;
				} else {
					setupErrorISI("02");
				}
			}
			else if (string.equals(MainPanelDelegate.clVendorPayment)) {
				mainMenuPage.vendorPaymentButtonAction();
			}
			else if (string.equals(MainPanelDelegate.clTranQuote)) {
				if (getTranAuth(DWValues.MONEY_GRAM_SEND_DOC_SEQ))
					mainMenuPage.hotQuoteAction();
				else
					setupErrorISI("02");
			}
			else if (string.equals(MainPanelDelegate.clDirectoryBillers)) {
				if (getTranAuth(DWValues.BILL_PAYMENT_DOC_SEQ))
					mainMenuPage.hotBillersAction();
				else
					setupErrorISI("02");
			}
			else if (string.equals(MainPanelDelegate.clMoneyGramSendReversal)) {
				// Do something here.
				if (getTranAuth(DWValues.MONEY_GRAM_SEND_DOC_SEQ)
						&& "Y".equals(UnitProfile.getInstance().get("ALLOW_SEND_CANCELS", "N"))){
					JMenuItem item1 = (JMenuItem)getComponent("sendReversalTransaction");
					item1.doClick();
				}
				else
					setupErrorISI("02");
			}
			else if (string.equals(MainPanelDelegate.clMoneyGramReceiveReversal)) {
				if (getTranAuth(DWValues.MONEY_GRAM_RECEIVE_DOC_SEQ)
						&& "Y".equals(UnitProfile.getInstance().get("ALLOW_RECEIVE_REVERSALS", "N"))){
					JMenuItem item1 = (JMenuItem)getComponent("receiveReversalTransaction");
					item1.doClick();
				}
				else
					setupErrorISI("02");
			}
			else if (string.equals(MainPanelDelegate.clAmend)) {
				if (getTranAuth(DWValues.MONEY_GRAM_SEND_DOC_SEQ)
						&& "Y".equals(UnitProfile.getInstance().get("ALLOW_AMENDS", "N"))){
					JMenuItem item1 = (JMenuItem)getComponent("amendTransaction");
					item1.doClick();
				}
				else
					setupErrorISI("02");
			}
			else if (string.equals(MainPanelDelegate.clFormFreeSend)){
				if (getTranAuth(DWValues.MONEY_GRAM_SEND_DOC_SEQ))
					mainMenuPage.formFreeEntryButtonAction();
				else
					setupErrorISI("02");
			}
			else if (string.equals(MainPanelDelegate.clAccountDeposit)){
				if (getTranAuth(DWValues.MONEY_GRAM_SEND_DOC_SEQ))
					mainMenuPage.hotAccountDepositPartnersAction();
				else
					setupErrorISI("02");
			}
    	} else
			setupErrorISI("02");
   }

    @Override
	public boolean isValidateNfLaunchPage() {
    	return parentFrame.getTitle().startsWith(Messages.getString("DeltaworksMainPanel.title"));



   }

    /**
     * Implementation of ErrorListener.
     * Set the status in the status bar with an icon that indicates its
     *   severity. If this unit is currently de-authed, always display the
     *   de-auth message and make sure the transactions become disabled.
     */
    @Override
	public void setStatus(String status, int severity) {

        if (!DeltaworksStartup.getInstance().haveConnected() && !deAuthMessage && !deActivateMessage) {
            statusLabel.setText(Messages.getString("DeltaworksMainPanel.disconnectedMessage")); 
            severity = ErrorManager.ERROR_STATUS;
        }
        else if ((!UnitProfile.getInstance().isPosAuthorized()&& !UnitProfile.getInstance().isPosDeactivated()) || deAuthMessage) {
            // overwrite the message with the de-auth message
            severity = ErrorManager.ERROR_STATUS;
            statusLabel.setText(Messages.getString("DeltaworksMainPanel.deauthMessage")); 
            //setTransactionEnabled(false);

        }
        else if (UnitProfile.getInstance().isPosDeactivated() || deActivateMessage) {
            // overwrite the message with the de-auth message
            severity = ErrorManager.ERROR_STATUS;
            statusLabel.setText(Messages.getString("DeltaworksMainPanel.deActivateMessage")); 
            //setTransactionEnabled(false);

        }
        else if (UnitProfile.getInstance().isAWOLTooLong()) {
            // overwrite the message with the awol message
            severity = ErrorManager.ERROR_STATUS;
            statusLabel.setText(UnitProfile.getInstance().isDemoMode() ? Messages.getString("DeltaworksMainPanel.demoTimeoutMessage") : 
            	Messages.getString("DeltaworksMainPanel.awolMessage")); 
            //setTransactionEnabled(false);
        }
        else if (mustHandleLogsFlag) {
            Debug.println("Setting transactions with mustHandleLogFlag"); 
            // overwrite the message with the awol message
            severity = ErrorManager.ERROR_STATUS;
            statusLabel.setText(Messages.getString("DeltaworksMainPanel.mustHandleLogsMessage")); 
            //setTransactionEnabled(false);
        }
        else
            statusLabel.setText(status);

        switch (severity) {
            case ErrorManager.INFORMATION_STATUS :
                //  statusLabel.setIcon(getIcon(getParam("informationIcon")));	
                statusLabel.setIcon(getIcon("xml/images/information_circle.gif")); 
                break;
            case ErrorManager.WARNING_STATUS :
                //    statusLabel.setIcon(getIcon(getParam("warningIcon")));	
                statusLabel.setIcon(getIcon("xml/images/warning_triangle.gif")); 
                break;
            case ErrorManager.ERROR_STATUS :
                //  statusLabel.setIcon(getIcon(getParam("errorIcon")));	
                statusLabel.setIcon(getIcon("xml/images/error_circle.gif")); 
                break;
        }

        // update the period label here also
        updatePeriodLabel();

        // show status info in the trace log, including memory usage
        Runtime runtime = Runtime.getRuntime();
        long total = runtime.totalMemory();
        Debug.println(ErrorManager.getStatusString(severity) + statusLabel.getText() + "\t[period " + periodLabel.getText() + " docs " + formsLabel.getText()  
        +" mem " + ((total - runtime.freeMemory()) / 1024) 
        +" over " + (total / 1024) + "]");  
    }

    /**
     * Implementation of ErrorListener.
     * If training mode has just been entered or exited, replace the moneygram
     *   forwarders with the corresponding ones (live or demo forwarders).
     */
    @Override
	public void setTrainingMode(final boolean training) {
        // get the profile item that corresponds to training mode
        ProfileItem trainingItem = UnitProfile.getInstance().getProfileInstance().find("TRAINING_MODE"); 
        boolean current = trainingItem.booleanValue();

        // they will be the same if this is triggered from profile download
        boolean fromProfileDownload = (training == current);

        // don't set the profile option if it is already set (when this is
        //   triggered as the result of a profile download)
        if (!fromProfileDownload) {
            trainingItem.setValue(training);
        }
        setTitle(Messages.getString("DeltaworksMainPanel.title")); 
        ErrorManager.setStatus(Messages.getString("DeltaworksMainPanel.1951") + 
         (training ? Messages.getString("DeltaworksMainPanel.1952") : Messages.getString("DeltaworksMainPanel.1953")),  
        ErrorManager.INFORMATION_STATUS);

        // set the menu item to the new value
        trainingModeMenuItem.setSelected(training);

    }

    /**
     * Implementation of ErrorListener. If the POS has been authed/de-authed,
     *   set the corresponding access to the financial transactions.
     */
    @Override
	public void setPosAuthorization(boolean authorized) {
        if (authorized) {
            setStatus(Messages.getString("DeltaworksMainPanel.authMessage"), ErrorManager.INFORMATION_STATUS); 
        }
        else {
            setStatus(Messages.getString("DeltaworksMainPanel.deauthMessage"), ErrorManager.ERROR_STATUS); 
        }
    }

    /**
     * Implementation of ErrorListener. Display a message to the user that the
     *   POS has gone awol (or in the case of DEMO mode, that the timeout limit
     *   has been reached). Also disable all financial transactions.
     */
    @Override
	public void setAWOLStatus(boolean awol) {

        // RLC : regarding Tracker log #857. The options are not all getting
        //  re-enabled correctly following a profile update or a awol=false call.
        //  update code to make sure this is happening correctly in these cases

        Debug.println("setting AWOL behavior to: " + awol); 

        //setTransactionEnabled(! awol);
        // try to set to auth message...if still AWOL, will be overridden in
        //    the setStatus() method to display the AWOL message
        setStatus(Messages.getString("DeltaworksMainPanel.authMessage"), ErrorManager.INFORMATION_STATUS); 
    }

   /**
    *   Update the ReprintReceipt state on the services menu
    */
    @Override
	public void updateReprintReceiptState() {
		getComponent("reprintReceipt")
				.setEnabled(dw.pdfreceipt.PdfReceipt.reprintAvailable() || dw.prtreceipt.PrtReceipt.reprintAvailable());
	}

    /**
     * Create the main menu page and the install wizard also. Install wizard
     *   may have to be shown right away, so we don't want to wait for it to
     *   be creating with the financial transaction interfaces.
     */
    private void createMainMenu() {
        try {
			if (! SwingUtilities.isEventDispatchThread()) {
			    Debug.println("createMainMenu() called, not in event dispatch thread.");
			}
			// add the main starting page
			mainMenuPage = new DeltaworksMainMenu("main/DeltaworksMainMenu.xml", 
			            "mainMenu", "MN005", buttons);  
			addPage(mainMenuPage);

			// We only need the install transaction and profile transaction
			//   if we are in DEMO mode
			if (UnitProfile.getInstance().isDemoMode()) {
			    installTransaction = new InstallTransaction(Messages.getString("DeltaworksMainPanel.1954")); 
			    InstallWizard installWizard = new InstallWizard(installTransaction, this);
			    addPage(installWizard, installTransaction.getTransactionName() + "Wizard"); 
			}
			Debug.println("createMainMenu() completed");
		} catch (Exception e) {
			Debug.println("Exception in createMainMenu["
					+e.getLocalizedMessage()+"]");
		}
    }

    /** To avoid problem where IOException is thrown after this thread
        *  starts, but this thread then re-enables all the buttons, we place
        *  this check, taking in the old allowed flag,
        *  after each semaphore wait and then at the end
        */

    private boolean checkAllowed() {
        if (mustHandleLogsFlag) {
            //if there is a log error, display it now
            showMustHandleLogsDialog();
        }
        return !mustHandleLogsFlag;
    }

    /**
     * This class allows us to pause the intensive initial creation of
     *   interfaces when the agent goes to use one.
     */
    private class TranThread implements Runnable {
        private PageNameInterface pni = null;

        public TranThread(PageNameInterface pni) {
            this.pni = pni;
        }

        /**
         * Create all the transaction interfaces and set them enabled as we go.
         *   A transaction should be enabled if we have connectivity to the
         *   host, if we are not de-authed, and if that transaction is
         *   available to this agent.
         */
        @Override
		public void run() {
            try {
				setTransactionEnabled(false);

				Debug.println("TranThread: mustHandleLogsFlag=" + mustHandleLogsFlag 
				+" connected=" + DeltaworksStartup.getInstance().haveConnected() 
				+" auth=" + UnitProfile.getInstance().isPosAuthorized()); 
				//+" noAwol=" + noAwol); 

				// we might have started with a new version, so use the profile
				//   transaction to send the change to the host
				if (!UnitProfile.getInstance().isDemoMode() && UnitProfile.getInstance().isSoftwareVersionIdChanged()) {
				    Thread sendThread = new Thread(new Runnable() {
				        @Override
						public void run() {
				            try {
				                    // send the change to host
				            	simplePMTransaction.send(new MessageFacadeParam(MessageFacadeParam.REAL_TIME));
				            }
				            finally {
				                // DO NOTHING
				            }
				        }
				    });
				    sendThread.start();
				}

				tranCreationSemaphore.waitUntilFree();

				mainMenuPage.requestFocus();

				// SIMPLIFIED PROFILE MAINTENANCE
				tranCreationSemaphore.waitUntilFree();

				SimplePMTransactionInterface simplePM =
				    new SimplePMTransactionInterface(simplePMTransaction, pni);
				addPage(simplePM, simplePMTransaction.getTransactionName() + "Wizard"); 

				// DISPENSER LOAD...can be called by others...so create it first
				tranCreationSemaphore.waitUntilFree();

				dispenserLoadTransaction = new DispenserLoadTransaction(Messages.getString("DeltaworksMainPanel.1955")); 
				NewDispenserLoadWizard dlWizard =
				    new NewDispenserLoadWizard(dispenserLoadTransaction, pni);
				addPage(dlWizard, dispenserLoadTransaction.getTransactionName() + "Wizard"); 
				getComponent("loadForms").setEnabled(true); 

				// MONEY ORDERS
				moneyOrderTransaction = new MoneyOrderTransaction40(Messages.getString("DeltaworksMainPanel.1956"), 
				                            ClientTransaction.RECEIPT_PREFIX_MONEY_ORDER, DWValues.MONEY_ORDER_DOC_SEQ);
				tranCreationSemaphore.waitUntilFree();

				MoneyOrderWizard moWizard = new MoneyOrderWizard(moneyOrderTransaction, pni);
				addPage(moWizard, moneyOrderTransaction.getTransactionName() + "Wizard"); 
				tranCreationSemaphore.waitUntilFree();

				MoneyOrderExpert moExpert = new MoneyOrderExpert(moneyOrderTransaction, pni);
				addPage(moExpert, moneyOrderTransaction.getTransactionName() + "Expert"); 

				// VENDOR PAYMENTS
				vendorPaymentTransaction = new MoneyOrderTransaction40(Messages.getString("DeltaworksMainPanel.1957"),
				      ClientTransaction.RECEIPT_PREFIX_MONEY_ORDER, DWValues.VENDOR_PAYMENT_DOC_SEQ, true); 
				tranCreationSemaphore.waitUntilFree();

				VendorPaymentWizard vpWizard = new VendorPaymentWizard(vendorPaymentTransaction, pni);
				addPage(vpWizard, vendorPaymentTransaction.getTransactionName() + "Wizard"); 
				tranCreationSemaphore.waitUntilFree();

				VendorPaymentExpert vpExpert = new VendorPaymentExpert(vendorPaymentTransaction, pni);
				addPage(vpExpert, vendorPaymentTransaction.getTransactionName() + "Expert"); 

				// MONEY GRAM SEND
				moneyGramSendTransaction = new MoneyGramSendTransaction(Messages.getString("DeltaworksMainPanel.1958"),
				    ClientTransaction.RECEIPT_PREFIX_SEND, DWValues.MONEY_GRAM_SEND_DOC_SEQ); 
				tranCreationSemaphore.waitUntilFree();
				MoneyGramSendWizard mgsWizard = new MoneyGramSendWizard(moneyGramSendTransaction, pni);
				addPage(mgsWizard, moneyGramSendTransaction.getTransactionName() + "Wizard"); 

				// MONEY GRAM SEND TO ACCOUNT
				moneyGramSendToAccountTransaction = new AccountDepositPartnersTransaction(Messages.getString("DeltaworksMainPanel.1958a"),
				    ClientTransaction.RECEIPT_PREFIX_SEND, DWValues.MONEY_GRAM_SEND_DOC_SEQ); 

				tranCreationSemaphore.waitUntilFree();
				MoneyGramSendWizard mgsWizardToAcc = new MoneyGramSendWizard(moneyGramSendToAccountTransaction, pni);
				addPage(mgsWizardToAcc, moneyGramSendToAccountTransaction.getTransactionName() + "Wizard"); 
				/*
				if(CompileTimeFlags.test())
				{
					MoneyGramSendWizardTest mgsWizard = new MoneyGramSendWizardTest(moneyGramSendTransaction, pni);
					addPage(mgsWizard, moneyGramSendTransaction.getTransactionName() + "Wizard"); 
				}else
				{
					MoneyGramSendWizard mgsWizard = new MoneyGramSendWizard(moneyGramSendTransaction, pni);
					addPage(mgsWizard, moneyGramSendTransaction.getTransactionName() + "Wizard"); 
				}
				 */
				tranCreationSemaphore.waitUntilFree();

				// SEND TRANSACTION REVERSAL
				moneyGramReceiveTransaction = new MoneyGramReceiveTransaction(Messages.getString("DeltaworksMainPanel.1960"),
				    ClientTransaction.RECEIPT_PREFIX_RECEIVE, DWValues.MONEY_GRAM_RECEIVE_DOC_SEQ); 
				tranCreationSemaphore.waitUntilFree();

				sendReversalTransaction = new SendReversalTransaction(Messages.getString("DeltaworksMainPanel.1959a"),
				    ClientTransaction.RECEIPT_PREFIX_REVERSAL, DWValues.MONEY_GRAM_RECEIVE_DOC_SEQ); 
				tranCreationSemaphore.waitUntilFree();

				SendReversalTransactionWizard trWizard = new SendReversalTransactionWizard(sendReversalTransaction, moneyGramReceiveTransaction, pni);
				addPage(trWizard, sendReversalTransaction.getTransactionName() + "Wizard"); 
				tranCreationSemaphore.waitUntilFree();

				// RECEIVE TRANSACTION REVERSAL
				moneyGramReceiveTransaction = new MoneyGramReceiveTransaction(Messages.getString("DeltaworksMainPanel.1960"),
				        ClientTransaction.RECEIPT_PREFIX_RECEIVE, DWValues.MONEY_GRAM_RECEIVE_DOC_SEQ); 
				    tranCreationSemaphore.waitUntilFree();
				receiveReversalTransaction = new ReceiveReversalTransaction(Messages.getString("DeltaworksMainPanel.1959b"),
				    ClientTransaction.RECEIPT_PREFIX_REVERSAL, DWValues.MONEY_GRAM_RECEIVE_DOC_SEQ); 
				tranCreationSemaphore.waitUntilFree();

				ReceiveReversalTransactionWizard rrtWizard = new ReceiveReversalTransactionWizard(receiveReversalTransaction, pni);
				addPage(rrtWizard, receiveReversalTransaction.getTransactionName() + "Wizard"); 
				tranCreationSemaphore.waitUntilFree();

				// AMEND TRANSACTION
				amendTransaction = new AmendTransaction(Messages.getString("DeltaworksMainPanel.1959c"), null, DWValues.MONEY_GRAM_SEND_DOC_SEQ);
				tranCreationSemaphore.waitUntilFree();

				AmendTransactionWizard atWizard = new AmendTransactionWizard(amendTransaction, pni);
				addPage(atWizard, amendTransaction.getTransactionName() + "Wizard"); 
				tranCreationSemaphore.waitUntilFree();

				// MONEY GRAM RECEIVE
				moneyGramReceiveTransaction = new MoneyGramReceiveTransaction(Messages.getString("DeltaworksMainPanel.1960"),
				    ClientTransaction.RECEIPT_PREFIX_RECEIVE, DWValues.MONEY_GRAM_RECEIVE_DOC_SEQ); 
				//moneyGramReceiveTransaction = new MoneyGramReceiveTransaction("MoneyGram Receive", MONEY_GRAM_RECEIVE_DOC_SEQ); 
				tranCreationSemaphore.waitUntilFree();

				MoneyGramReceiveWizard mgrWizard =
				    new MoneyGramReceiveWizard(moneyGramReceiveTransaction, pni);
				addPage(mgrWizard, moneyGramReceiveTransaction.getTransactionName() + "Wizard"); 
				tranCreationSemaphore.waitUntilFree();

				// BILL PAYMENT
				billPaymentTransaction = new MoneyGramSendTransaction(Messages.getString("DeltaworksMainPanel.PayaBill"),
				    ClientTransaction.RECEIPT_PREFIX_BILL_PAYMENT, DWValues.BILL_PAYMENT_DOC_SEQ, true); 
				//billPaymentTransaction = new MoneyGramSendTransaction("BillPayment", BILL_PAYMENT_DOC_SEQ, true); 
				tranCreationSemaphore.waitUntilFree();

				BillPaymentWizard epWizard =
				    new BillPaymentWizard(billPaymentTransaction, pni);
				addPage(epWizard, billPaymentTransaction.getTransactionName() + "Wizard"); 
				tranCreationSemaphore.waitUntilFree();

				// FORM FREE SEND
				formFreeSendTransaction = new MoneyGramSendTransaction(Messages.getString("DeltaworksMainPanel.FormFreeSend"),
				    ClientTransaction.RECEIPT_PREFIX_BILL_PAYMENT, DWValues.MONEY_GRAM_SEND_DOC_SEQ, true); 
				formFreeSendTransaction.setFormFreeSendEnabled(true);
				tranCreationSemaphore.waitUntilFree();

				MoneyGramSendWizard ffsWizard =
				    new MoneyGramSendWizard(formFreeSendTransaction, pni);
				addPage(ffsWizard, formFreeSendTransaction.getTransactionName() + "Wizard"); 
				tranCreationSemaphore.waitUntilFree();

				// Add Minutes
				addMinutesTransaction = new MoneyGramSendTransaction(Messages.getString("DeltaworksMainPanel.AddMinutes"),
				    ClientTransaction.RECEIPT_PREFIX_BILL_PAYMENT, DWValues.BILL_PAYMENT_DOC_SEQ, true); 
				tranCreationSemaphore.waitUntilFree();

				BillPaymentWizard amWizard =
				    new BillPaymentWizard(addMinutesTransaction, pni);
				addPage(amWizard, addMinutesTransaction.getTransactionName() + "Wizard"); 
				tranCreationSemaphore.waitUntilFree();

				// DIRECTORY OF AGENTS
				agentDirectoryTransaction = new AgentDirectoryTransaction(Messages.getString("DeltaworksMainPanel.1962")); 
				tranCreationSemaphore.waitUntilFree();

				AgentDirectoryWizard adWizard = new AgentDirectoryWizard(agentDirectoryTransaction, pni);
				// only need one interface for the agent directory.wizard is default
				addPage(adWizard, agentDirectoryTransaction.getTransactionName() + "Wizard"); 


				// ACCOUNT DEPOSIT PARTNERS
				/*accountDepositPartnersTransaction = new AccountDepositPartnersTransaction(
						Resources
								.getString("DeltaworksMainPanel.Account_Deposit_Partners_125"));
				tranCreationSemaphore.waitUntilFree();
				AccountDepositPartnersWizard adpWizard = new AccountDepositPartnersWizard(
						accountDepositPartnersTransaction, pni);
				addPage(adpWizard,
						accountDepositPartnersTransaction.getTransactionName()
								+ "Wizard"); 
				*/
				// BROADCAST MESSAGES

				broadcastMessagesTransaction = new BroadcastMessagesTransaction(Messages.getString("DeltaworksMainPanel.Broadcast_Messages"));
				tranCreationSemaphore.waitUntilFree();
				BroadcastMessagesWizard bmWizard = new BroadcastMessagesWizard(broadcastMessagesTransaction, pni);
				addPage(bmWizard, broadcastMessagesTransaction.getTransactionName() + "Wizard");

				// MULTI AGENT MODE
				multiAgentTransaction = new MultiAgentTransaction(Messages.getString("DeltaworksMainPanel.1970")); 
				tranCreationSemaphore.waitUntilFree();

				MultiAgentTransactionInterface maInterface = new MultiAgentTransactionInterface(multiAgentTransaction, pni);
				// only need one interface for the agent directory.wizard is default
				addPage(maInterface, multiAgentTransaction.getTransactionName() + "Wizard"); 

				// TRANSACTION STATUS
				transactionStatusTransaction = new MoneyGramSendTransaction(
				    	Messages.getString("DeltaworksMainPanel.1972"),
				    	ClientTransaction.RECEIPT_PREFIX_SEND,
				    	DWValues.MONEY_GRAM_SEND_DOC_SEQ); 
				tranCreationSemaphore.waitUntilFree();

				TSWizard tsWizard = new TSWizard( 
				    transactionStatusTransaction, pni);
				addPage(tsWizard, transactionStatusTransaction.getTransactionName() + "TSWizard"); 
				Debug.println(" when page added :" + transactionStatusTransaction.getTransactionName() + "TSWizard");
				setTransactionEnabled(true);

				// Reprint Receipt
				reprintReceiptTransaction = new ReprintReceiptTransaction(Messages.getString("DeltaworksMainPanel.Reprint_Receipt"));
				tranCreationSemaphore.waitUntilFree();

				ReprintReceiptWizard rrWizard = new ReprintReceiptWizard(reprintReceiptTransaction, pni);
				addPage(rrWizard, reprintReceiptTransaction.getTransactionName() + "Wizard"); 

				// Explicit MFA Registration - Register MFA Token
				registerMfaToken = new RegisterMfaToken(Messages.getString("DeltaworksMainPanel.796"));
				tranCreationSemaphore.waitUntilFree();

				RegisterMFAWizard rmfaWizard = new RegisterMFAWizard(registerMfaToken, moneyGramClientTransaction, pni);
				addPage(rmfaWizard, registerMfaToken.getTransactionName() + "Wizard"); 
				Debug.println("When MFA page added :" + registerMfaToken.getTransactionName() + "RegisterMFAWizard");

				// CARD TRANSACTIONS
				prepaidCardTransaction = new MoneyGramSendTransaction(Messages.getString("DeltaworksMainPanel.PrepaidCard"),
				      ClientTransaction.RECEIPT_PREFIX_BILL_PAYMENT, DWValues.BILL_PAYMENT_DOC_SEQ, true); 
				tranCreationSemaphore.waitUntilFree();

				BillPaymentWizard cardWizard =
					new BillPaymentWizard(prepaidCardTransaction, pni,
							MoneyGramSendTransaction.TRAN_ENTRY_PPC);
				addPage(cardWizard, prepaidCardTransaction.getTransactionName() + "Wizard"); 
				tranCreationSemaphore.waitUntilFree();

				AgentCountryInfo.setAgentCountryFields();
			} catch (Exception e) {
				Debug.println("Exception in TranThread: run["
						+e.getLocalizedMessage()+"]");
	            Debug.printStackTrace(e);
			}
        }
    }

    /**
     * Create all the transaction interfaces here. [Create them in a background
     *   thread so we don't stall startup waiting for everything to parse.]
     */
    @Override
	protected void createPages() {
        //Debug.dumpStack("createPages() called from");
        Runnable task = new TranThread(this);
        if (SwingUtilities.isEventDispatchThread()) {
            task.run();
        }
        else {
            SwingUtilities.invokeLater(task);
        }
    }

    /**
     * Clean out the main frame to make room for a transaction panel. Pull off
     *   the menu bar and status bars if not visible, show them if visible.
     */
    private void setMenuComponentsVisible(boolean visible) {
        JFrame f = (JFrame) JOptionPane.getFrameForComponent(this);
        if (visible) {
            f.setJMenuBar(menuBar);
        }
        else {
            f.setJMenuBar(null);
            // there used to be code here to save the old menu bar to be
            // restored by a subsequent call; moved to constructor --gea
        }
        statusPanel.setVisible(visible);
        f.validate();
    }

    /**
     * Check the status of the dispenser and act accordingly if there is a
     *   problem.
     * Scenarios :
     *    LOAD_REQUIRED : ask user if they want to start the load transaction.
     *    VERIFY_REQUIRED : prompt user for last four digits (3 tries)
     *    VOIDED : do a four digit verification in this case as well
     *    OK : continue
     *    All other dispenser errors : show error dialog & prompt for load also
     *    Not enough forms to start transaction : prompt for load
     */
    private boolean isDispenserReady() {
        // collect some pertinent criteria
        DispenserInterface dispenser = DispenserFactory.getDispenser(1);

        if (!dispenser.isOn() && UnitProfile.getInstance().isDemoMode())
            return true;

        int formsRemaining = dispenser.getRemainingCount();
        int formsRequired =
            currentTransaction == null ? 0 : currentTransaction.getRequiredFormCount();
        int status = dispenser.queryStatusWithIntent(); // I really mean it!

        // ask the user to start the load transaction is one is required
        if (status == DispenserInterface.LOAD_REQUIRED
            || (formsRequired > 0 && formsRequired > formsRemaining)) {
            //Debug.println("Showing the DispenserLoadRequiredDialog!!!!!");	
            //Thread.dumpStack();
            if (Dialogs.showConfirm("dialogs/DialogDispenserLoadRequired.xml") == Dialogs.YES_OPTION) { 
                // remember the current transaction and start the load
                pendingTransaction = currentTransaction;
                currentTransaction = dispenserLoadTransaction;
                // currentUser = pendingTransaction.getUserID();
                currentTransaction.setStatus(ClientTransaction.COMPLETED);
                startTransactionInterface();
            }
            else {
                currentTransaction.setStatus(ClientTransaction.FAILED);
                setupCancelISI("01");
                if (getIsiAllowDataCollection()){
                    if (getIsiExeProgramArguments() != null ){
                        String programName = getIsiExeProgramArguments().trim();
                        if (programName.length() > 0 && !(programName.startsWith("-EXE"))) {
        	  	  			executeIsiProgram(programName);
                        }
                    }
                }
                initializeIsiFields();
                startTransactionInterface();
            }
            return false;
        }
        else if (
            status == DispenserInterface.VERIFY_REQUIRED || status == DispenserInterface.VOIDED) {
            // show the verify dialog and then continue if its OK
            dispenser.extend();
            int attempt = 0; // give them 3 tries...then forget it
            while (attempt < 3) {
                Object entry = null;
                if (attempt++ == 0) {
                    entry = Dialogs.showInput("dialogs/DialogEnterFourDigits1.xml", null, "");  
                }
                else {
                    entry = Dialogs.showInput("dialogs/DialogEnterFourDigits2.xml", null, "");  
                }

                if ((entry == null) || !(entry instanceof String)) { // user clicked cancel
                    dispenser.retract();
                    attempt = 3; // set kick out condition
                    setupCancelISI("01");
                    if (getIsiAllowDataCollection()){
                        if (getIsiExeProgramArguments() != null ){
                            String programName = getIsiExeProgramArguments().trim();
                            if (programName.length() > 0 && !(programName.startsWith("-EXE"))) {
            	  	  			executeIsiProgram(programName);
                            }
                        }
                    }
                    initializeIsiFields();
                    return false;
                }
                else {
                    try {
                        if (dispenser.verify(Long.parseLong((String) entry))) {
                            dispenser.retract();
                            return true;
                        }
                    }
                    catch (NumberFormatException e) {
                    } // treat as incorrect
                }
            }
            dispenser.retract();
            setupCancelISI("01");
            if (getIsiAllowDataCollection()){
                if (getIsiExeProgramArguments() != null ){
                    String programName = getIsiExeProgramArguments().trim();
                    if (programName.length() > 0 && !(programName.startsWith("-EXE"))) {
    	  	  			executeIsiProgram(programName);
                    }
                }
            }
            initializeIsiFields();
            return false;
        }
        else if (status != DispenserInterface.OK) {
            // there is a general dispenser error...notify user
            if (Dialogs.showConfirm("dialogs/DialogDispenserError.xml") == Dialogs.YES_OPTION) { 
                currentTransaction = dispenserLoadTransaction;
                currentTransaction.setStatus(ClientTransaction.COMPLETED);
                startTransactionInterface();
            }
            setupCancelISI("01");
            if (getIsiAllowDataCollection()){
                if (getIsiExeProgramArguments() != null ){
                    String programName = getIsiExeProgramArguments().trim();
                    if (programName.length() > 0 && !(programName.startsWith("-EXE"))) {
    	  	  			executeIsiProgram(programName);
                    }
                }
            }
            initializeIsiFields();
            return false;
        }

        // nothing wrong with the dispenser at this point...
        return true;
    }

    /**
     * Determine whether or not a profile update or diag transmit is currently
     *   in progress. If it is, let the user know via a dialog box and return
     *   true. Otherwise, return false.
     */
    private boolean isHostCommInProgress() {
        if (profileUpdateInProgress) {
            Dialogs.showWarning("dialogs/DialogUpdatingProfile.xml"); 
            return true;
        }
        if (duringManualTransmit) {
            Dialogs.showWarning("dialogs/DialogTransmittingDiags.xml"); 
            return true;
        }

        if (moTransmitInProgress) {
            Dialogs.showWarning("dialogs/DialogTransmittingDiags.xml"); 
            return true;
        }

        return false;
    }

    private void startTransactionInterface(boolean ignoreDial) {
        startTransactionInterface(currentTransaction.isPasswordRequired(), ignoreDial);
    }

    /**
     * Start an interface for the current transaction. The transaction's
     *   isUsingWizard() method determines if we show the expert or wizard
     *   interface. The transaction's isPasswordRequired() method determines
     *   whether a password is required.
     */
    private void startTransactionInterface() {
        startTransactionInterface(currentTransaction.isPasswordRequired(), false);
    }

    /**
     * Start an interface for the current transaction. The transaction's
     * isUsingWizard() method determines if we show the expert or wizard
     * interface.
     * @param collectPassword a boolean value to use in place of the
     *       transaction's isPasswordRequired() method.
     */
    private void startTransactionInterface(boolean collectPassword, boolean ignoreDial) {
        // Save start time of transaction.
        timing = System.currentTimeMillis();

        // make sure we are not updating the profile
        if (isHostCommInProgress())
            return;

        // check if logs need to be handled
        if (!checkAllowed())
            return;

        // set userId to 0 if no login is required for this transaction.
        // or if the transaction is a non-financial transaction.

        //Debug.println(" financial transaction : " + currentTransaction.isFinancialTransaction());
        //Debug.println(" name pin required : " + currentTransaction.getNamePinRequired());
        //Debug.println(" transaction name : " + currentTransaction.getTransactionName());

	    if (!currentTransaction.isFinancialTransaction() || currentTransaction.getNamePinRequired().equals("N"))
	        currentTransaction.setUser(0);

		if (currentTransaction.isFinancialTransaction() || 
				currentTransaction.getTransactionReceiptPrefix().equals(ClientTransaction.RECEIPT_PREFIX_CARD) || 
				currentTransaction instanceof AccountDepositPartnersTransaction) {
           if (currentTransaction.isTransmitRequired(this.checkCachedResponses)) {
               if (Dialogs.YES_OPTION == Dialogs.showConfirm("dialogs/DialogTransmitNow.xml")) { 
                   transmit();
               }
               moneyGramSendTransaction.setFeeQuery(false);   //Pat says this is required.
               billPaymentTransaction.setDirectoryOfBillers(false);
               return;
           }
        //  make sure we are within business hours to be able to perform a tran
           if (! currentTransaction.withinStoreHours()) {
               moneyGramSendTransaction.setFeeQuery(false);   //Pat says this is required.
               billPaymentTransaction.setDirectoryOfBillers(false);
               return;
           }
        }
        // ping the printer now, because it takes a few seconds to respond..
        if (currentTransaction.isReceiptPrinterNeeded()) {
      		TEPrinter printer = TEPrinter.getPrinter();
      		if (printer == null) {
        		Dialogs.showOkCancel("dialogs/DialogPrinterNotReady.xml");
                if (Dialogs.YES_OPTION == Dialogs.showConfirm("dialogs/DialogTransmitNow.xml")) { 
                    transmit();
                }
                moneyGramSendTransaction.setFeeQuery(false);   //Pat says this is required.
                billPaymentTransaction.setDirectoryOfBillers(false);
                return;
      		}
            if (!printer.isReady(this)) {
                moneyGramSendTransaction.setFeeQuery(false);   //Pat says this is required.
                billPaymentTransaction.setDirectoryOfBillers(false);
                initializeIsiFields();
                return;
            }
        }

        // pause tran creation and software download so this tran can go smooth
        pauseSemaphores();

        String pageName = null;

        // special case : if we are starting a money order transaction and
        //  the send currency is not USD, do not allow
        if (currentTransaction == moneyOrderTransaction &&
            (! CountryInfo.getAgentCountry().getBaseReceiveCurrency().equals("USD") ||
             ! (FormatSymbols.getDecimalPrecision() == 2))) {
            Dialogs.showError("dialogs/DialogMOTransactionNotAllowed.xml");
            initializeIsiFields();
            return;
        }

        // special case : if we are starting a vendor payment transaction and
        //  the send currency is not USD, do not allow
        if (currentTransaction == vendorPaymentTransaction &&
            (! CountryInfo.getAgentCountry().getBaseReceiveCurrency().equals("USD") ||
             ! (FormatSymbols.getDecimalPrecision() == 2))) {
            Dialogs.showError("dialogs/DialogVPTransactionNotAllowed.xml");
            initializeIsiFields();
            return;
        }

        // special case : if we are starting the dispenser load and we have a
        //  pending tran, see if a manager logged in to that tran
        if (currentTransaction == dispenserLoadTransaction && pendingTransaction != null) {
            if (ClientTransaction.canLoadDispenser(pendingTransaction.getUserID())) {
                collectPassword = false; // this user can load forms
                currentTransaction.setUser(pendingTransaction.getUserID());
            }
        }

        /*
         * Check if the current transaction status is failed, if so, clean up
         * current transaction and return to the main menu.
         */
        if (currentTransaction.getStatus() == ClientTransaction.FAILED) {
            // restore the main menu and display status of last transaction
            currentTransaction.clear();
            currentTransactionInterface.unloadPages();
            setMenuComponentsVisible(true);
            setTitle(Messages.getString("DeltaworksMainPanel.title")); 
            showPage("mainMenu", PageExitListener.NEXT); 
            return;
        }

        // no password required, so get the transaction's preference and set the
        //   user of the transaction to 'NO USER' (-1)
        if (currentTransaction.getStatus() == ClientTransaction.IN_PROCESS || !collectPassword) {
            // You are starting a transaction set append mode to false
            setIsiAppendMode(false);
            // Validate ISI business logic and stop the transaction if needed.
            if (!getIsiTransactionInProgress()){
                if ((! currentTransaction.isIsiCommandValid()) && (currentTransaction.isIsiEnabled())) {
                    // Stop the transaction.
                    initializeIsiFields();
                    return;
                }
            }
            //if the transaction is fee query or directory of billers
            // set to wizard mode.
//            if(((currentTransaction == moneyGramSendTransaction)
//                &&(moneyGramSendTransaction.isFeeQuery())
//                ||(currentTransaction == billPaymentTransaction))){
//                   currentTransaction.setUsingWizard(true);
//            }

            if(currentTransaction == moneyGramSendTransaction
            		|| currentTransaction == moneyGramSendToAccountTransaction
                    ||currentTransaction == billPaymentTransaction
                    ||currentTransaction == formFreeSendTransaction
                    ||(currentTransaction == prepaidCardTransaction)
                    ||currentTransaction == addMinutesTransaction
					||currentTransaction == moneyGramReceiveTransaction){
                       currentTransaction.setUsingWizard(true);
                }

//            if(currentTransaction == moneyGramCardTransaction ||
//               currentTransaction == moneyGramCardReloadTransaction ||
//			   currentTransaction == moneyGramCardPurchaseTransaction){
//               currentTransaction.setUsingWizard(true);
//            }

            if (currentTransaction.isUsingWizard())
            	/*
            	 * Add Minutes is really a billPaymentTransaction
            	 */
            	if (currentTransaction == addMinutesTransaction) {
                    pageName = billPaymentTransaction.getTransactionName() + "Wizard"; 
            	}
            	else {
                    pageName = currentTransaction.getTransactionName() + "Wizard"; 
            	}
            else
                pageName = currentTransaction.getTransactionName() + "Expert"; 
        }
        // if this is a pending transaction, we have already logged in before
        //   the load was kicked off

        else if (currentTransaction == pendingTransaction || currentTransaction.isUserAuthorized(currentTransaction instanceof DispenserLoadTransaction,false)) {
            if (pendingTransaction == currentTransaction)
                pendingTransaction = null;
//            if(((currentTransaction == moneyGramSendTransaction)
//                &&(moneyGramSendTransaction.isFeeQuery())
//                ||((currentTransaction == billPaymentTransaction)))
//                ||((currentTransaction == directedSendTransaction)
//                        &&(billPaymentTransaction.isReceiverRegistration()))){
//                currentTransaction.setUsingWizard(true);
//            }

            if ((currentTransaction == moneyGramSendTransaction) ||
            		(currentTransaction == moneyGramSendToAccountTransaction) ||
            		(currentTransaction == addMinutesTransaction) ||
            		(currentTransaction == prepaidCardTransaction) ||
            		(currentTransaction == billPaymentTransaction) ||
            		(currentTransaction == formFreeSendTransaction)) {
                    	currentTransaction.setUsingWizard(true);
                }

            if(currentTransaction == sendReversalTransaction){
                currentTransaction.setUsingWizard(true);
                if (!UnitProfile.getInstance().isMultiAgentMode()
                		&& isPinRequired(DWValues.MONEY_GRAM_SEND_DOC_SEQ)){
                    try {
                        Profile userProf = (Profile) UnitProfile.getInstance().getProfileInstance().find("USER", currentTransaction.getUserID());
                        ProfileItem item = userProf.find("REVERSAL");
                        if (!item.booleanValue()){
                            showNotAuthorizedDialog();
                            setupCancelISI("01");
                            return;
                        }
                    }
                    catch (NoSuchElementException e) {
                        showNotAuthorizedNoLoginDialog();
                        setupCancelISI("01");
                        return;
                    }
                }
            }
            if(currentTransaction == receiveReversalTransaction){
                currentTransaction.setUsingWizard(true);
                if (!UnitProfile.getInstance().isMultiAgentMode()
                		&& isPinRequired(DWValues.MONEY_GRAM_RECEIVE_DOC_SEQ)){
                    try {
                        Profile userProf = (Profile) UnitProfile.getInstance().getProfileInstance().find("USER", currentTransaction.getUserID());
                        ProfileItem item = userProf.find(DWValues.EMP_RECV_REVERSAL);
                        if (!item.booleanValue()){
                            showNotAuthorizedDialog();
                            setupCancelISI("01");
                            return;
                        }
                    }
                    catch (NoSuchElementException e) {
                        showNotAuthorizedNoLoginDialog();
                        setupCancelISI("01");
                        return;
                    }
                }
            }
            if(currentTransaction == amendTransaction){
                currentTransaction.setUsingWizard(true);
                if (!UnitProfile.getInstance().isMultiAgentMode()
                		&& isPinRequired(DWValues.MONEY_GRAM_SEND_DOC_SEQ)){
                    try {
                        Profile userProf = (Profile) UnitProfile.getInstance().getProfileInstance().find("USER", currentTransaction.getUserID());
                        ProfileItem item = userProf.find(DWValues.EMP_AMEND);
                        if (!item.booleanValue()){
                            showNotAuthorizedDialog();
                            setupCancelISI("01");
                            return;
                        }
                    }
                    catch (NoSuchElementException e) {
                        showNotAuthorizedNoLoginDialog();
                        setupCancelISI("01");
                        return;
                    }
                }
            }
            if(currentTransaction == moneyGramReceiveTransaction)
          	  currentTransaction.setUsingWizard(true);
            if (currentTransaction.isUserWizard() || currentTransaction.isUsingWizard())
                pageName = currentTransaction.getTransactionName() + "Wizard"; 
            else
                pageName = currentTransaction.getTransactionName() + "Expert"; 
        }
        else {
            // resume tran creation and software download
            resumeSemaphores();
            moneyGramSendTransaction.setFeeQuery(false);
            billPaymentTransaction.setDirectoryOfBillers(false);
            return;
        }

        // set for TS Wizard
        if ((currentTransaction == transactionStatusTransaction)
                &&(transactionStatusTransaction.isTransactionStatus())) {
            	pageName = currentTransaction.getTransactionName() + "TSWizard";
            }

        if(currentTransaction == registerMfaToken){
            currentTransaction.setUsingWizard(true);
            if (!UnitProfile.getInstance().isMfaEnabled() && isPinRequired(DWValues.MONEY_GRAM_SEND_DOC_SEQ)){
                try {
                    Profile userProf = (Profile) UnitProfile.getInstance().getProfileInstance().find("USER", currentTransaction.getUserID());
                    ProfileItem item = userProf.find(DWValues.MFA_REGISTARTION_STATUS);
                    if (!item.booleanValue()){
                        showNotAuthorizedDialog();
                        setupCancelISI("01");
                        return;
                    }
                }
                catch (NoSuchElementException e) {
                    showNotAuthorizedNoLoginDialog();
                    setupCancelISI("01");
                    return;
                }
            }
        }

        // find the corresponding transaction interface
        //        TransactionInterface tranInterface = (TransactionInterface) cardLayout.getComponent(pageName);
        TransactionInterface tranInterface = (TransactionInterface) this.getCardComponent(pageName);

        currentTransactionInterface = tranInterface;


        // if the dispenser is needed for this tran, check its status first
        if ((currentTransaction == sendReversalTransaction || currentTransaction == moneyGramReceiveTransaction) &&
            UnitProfile.getInstance().get("MG_CHECKS", "DG").equals("MTC")){

            // Do nothing
        } else if (currentTransaction.isDispenserNeeded() && !isDispenserReady()) {
            resumeSemaphores();
            return;
        }

        // if we need to talk to the host during this tran, start connecting
        if (UnitProfile.getInstance().isDialUpMode()
            && currentTransaction.isHostConnectionRequired()
            && (!ignoreDial)) {
            MessageFacade.willNeed();
        }

        // clear out the transaction and its corresponding interface
        currentTransaction.clear();
        if(tranInterface!=null)
        tranInterface.clear();

        // hide the main menu components to make room for the interface
        SwingUtilities.invokeLater(new Runnable() {
            @Override
			public void run() {
            	setMenuComponentsVisible(false);
            }
        });

        //setMenuComponentsVisible(false);
        // pop the interface name into the title bar
        showPage(pageName, PageExitListener.NEXT);
        if(tranInterface!=null)
        tranInterface.start();

        // start the backout timer if it applies to this transaction
        if (currentTransaction.isTimeoutEnabled())
            IdleBackoutTimer.start(tranInterface);


    }

	private void pauseSemaphores() {
        tranCreationSemaphore.pause();
        DeltaworksStartup.getInstance().pauseStartupSemaphores();
    }

    private void resumeSemaphores() {
        tranCreationSemaphore.resume();
        DeltaworksStartup.getInstance().resumeStartupSemaphores();
    }

    /**
     * Display an option pane with a yes/no question, return true
     * if user clicks yes.
     */
    private boolean userConfirm(String filename) {
        return Dialogs.YES_OPTION == Dialogs.showQuestion(filename);
    }

    /**
     * Display an option pane with a yes/no question, return true
     * if user clicks yes.
     */
    private boolean userConfirmAbort(String filename) {
        return Dialogs.YES_OPTION == Dialogs.showContinueAbort(filename);
    }

    /**
     * Display an option pane with a yes/no question, return true
     * if user clicks yes.
     */
    private boolean userConfirm(String filename, String message) {
        return Dialogs.YES_OPTION == Dialogs.showQuestion(filename, message);
    }

    /**
     * Exit the app. Caller should prompt user to confirm OK, if appropriate.
     * If we in DEMO mode, clear out the logs.
     * @param code the exit code to send to the system on exit(normal, restart, error)
     */
    @Override
	public void tryExit(int code) {
        // save off any changes we have made to the in-memory profile
        try {
            UnitProfile.getInstance().saveChanges();
        }
        catch (IOException e) {
            ErrorManager.handle(e, "unitProfileSaveChangesError", Messages.getString("ErrorMessages.unitProfileSaveChangesError"), 
            ErrorManager.ERROR, true);
        }

        // for DEMO mode, always clear out the logs and reset last reauth=0
        if (UnitProfile.getInstance().isDemoMode()) {
            Period.clearLogs();
            UnitProfile.getInstance().setLastReauthTime(new Date(0));
        }

        DeltaworksStartup.getInstance().cleanupResources();

        if (UnitProfile.getInstance().isDialUpMode())
            MessageFacade.getInstance().exitDeltaWorks();

        setupCancelISI("02");
        // if (getIsiAllowDataCollection() && (!getIsiSkipRecord())){
        if (!getIsiSkipRecord()){
            if (getIsiExeProgramArguments() != null ){
                String programName = getIsiExeProgramArguments().trim();
                if (programName.length() > 0 && !(programName.startsWith("-EXE"))) {
	  	  			executeIsiProgram(programName);
                }
            }
        }
        setIsiSkipRecord(false);
  	  	MainPanelDelegate.setIsiTransactionInProgress(false);
  	  	MainPanelDelegate.setIsiExeCommand(false);
  	  	MainPanelDelegate.setIsiExe2Command(false);
  	  	MainPanelDelegate.setIsiExeProgramArguments("");
  	  	MainPanelDelegate.setIsiRpCommandValid(false);
        if (getIsiNfProgramArguments() != null ){
            String programName = getIsiNfProgramArguments().trim();
            if (programName.length() > 0 && !(programName.startsWith("-NF"))) {
  	  			executeIsiProgram(programName);
            }
        }
  	  	MainPanelDelegate.setIsiNfCommand(false);
  	  	MainPanelDelegate.setIsiNfProgramArguments("");
        System.exit(code);
    }

    /**
     * Get the number of forms where the low forms warning will be displayed
     *   from the Unit Profile.
     * @return the number of forms where the limit is displayed.
     */
    private int getLowFormsWarningLimit() {
        // get the low forms warning threshold
        Profile dispenserProfile = (Profile) UnitProfile.getInstance().getProfileInstance().find("DISPENSER", 1); 
        return dispenserProfile.find("LOW_FORM_WARNING").intValue(); 
    }


    /**
     * Override of setupFrameComponents in PageFlowContainer. If we are in demo
     *   mode, we want to fire up the install transaction right away. We do this
     *   here, because the default stuff for starting a transaction interface
     *   won't work until the frame is constructed and shown.
     */
    @Override
	public void setupFrameComponents() {
        super.setupFrameComponents();

        // if we are in demo mode, we have to start with the setup transaction
        if (UnitProfile.getInstance().isDemoMode()) {
            startInstall();
        }
        else {
            createPages();
            DeltaworksStartup.getInstance().startBackgroundProcesses();
        }

        setTitle(Messages.getString("DeltaworksMainPanel.title")); 

        // set the height and width of the app according to our screen res.
        setDefaultScreenResolution();

        // pop any dialog up to the front that was already shown
        frameSetUp = true;
    }

    /**
     * Refresh just the frameComponents when languages are switched.
     */
    public void refreshFrameComponents() {
        super.refreshMenuBar();
        createPages();
        setTitle(Messages.getString("DeltaworksMainPanel.title")); 
        frameSetUp = true;
    }

    /**
     * Find the system's screen size and set the size of the application
     *   accordingly.
     */
    private void setDefaultScreenResolution() {
        setAppSize(Toolkit.getDefaultToolkit().getScreenSize().width);
    }

    /**
     * Set the size of the window to the desired resolution.
     * @param targetScreenWidth the target screen width (640 or 800)
     */
    private void setAppSize(int targetScreenWidth) {
        Frame f = JOptionPane.getFrameForComponent(this);

        //DW60 set screen size to a constant irrespective of screen resolution.
        // this is for the top banner issue -15 inch vs 17 inch monitors.
        Dimension minDim = new Dimension(DWValues.DW_MINIMUM_WIDTH, DWValues.DW_MINIMUM_HEIGHT);
        f.setSize(minDim);
        f.setMinimumSize(minDim);
        //f.setResizable(true);
//EEO:  The line below is what you would modify to get a 800 x 600 large screen;
//		but at a price: Icons would have to be modified as well as some screens.
//		And with a standard 15" monitor, the bottom part of the DW screen
//		will be overlapped by the task bar. It might not be worth it at this time.
//      f.setSize(800, 600);

        f.setLocation(XmlDefinedPanel.getLocationForCenter(f));
        f.validate();
        f.repaint();


//        Frame f = JOptionPane.getFrameForComponent(this);
//
//		final Dimension ss = Toolkit.getDefaultToolkit().getScreenSize();
//
//		ExtraDebug.println("Screen Widht: " + ss.getWidth());
//		ExtraDebug.println("Screen Height: " + ss.getHeight());
//		int ratio1 = 650;
//		int ratio2 = 800;
//		int width = (ss.width * ratio1)/ratio2;
//		int height = (ss.height * ratio1)/ratio2;
//        f.setSize(width, height);
//        f.setLocation(XmlDefinedPanel.getLocationForCenter(f));
//        f.validate();
//        f.repaint();
//
//        //setFont(new Font("sanserif", Font.PLAIN, 20));	
    }

    /**
     * Start the agent setup transaction.
     */
    public void startInstall() {
        displayModeTags = false;
        currentTransaction = installTransaction;
        currentTransaction.setStatus(ClientTransaction.COMPLETED);
        startTransactionInterface();
    }

    /**
     * Display the about box.
     */
    public void about() {
        Dialogs.showAboutDialog(UnitProfile.getInstance().getVersionNumber(true));

    	MainPanelDelegate.setupNonFinancialISI();
     	String programName = "";
		if (MainPanelDelegate.getIsiNfProgramArguments() != null)
			programName = MainPanelDelegate.getIsiNfProgramArguments().trim();
		if (programName.length() > 0 && !(programName.startsWith("-NF"))) {
	  			executeIsiProgram(programName);
		}
    	MainPanelDelegate.setIsiNfCommand(false);
    	MainPanelDelegate.setIsiNfProgramArguments("");
    }


	public void updateStatus() {
		UpdateStatusDialog.getInstance().show();
		UpdateStatusDialog.getInstance().getDialog().pack();
	}

    /**
     * Sets language and alt language in unit profile, sets locale
     * used by on-screen text.
     */
	public void changeLanguages(final String current, final String alt) {
		try {
			Thread t = new Thread() {
				@Override
				public void run() {
					UnitProfile.getInstance().set("LANGUAGE", current);
					Messages.setCurrentLocale(current);

					CountryInfo.checkCountryInformation(CountryInfo.MANUAL_TRANSMIT_RETERIVAL);

					UnitProfile.getInstance().setAltLanguage(alt);
					UnitProfile.getInstance().setLanguage(current);

					ServiceOption.getServiceOptions(CountryInfo.MANUAL_TRANSMIT_RETERIVAL, true);
					
			        CachedACResponses.cacheACResponses(true);
			        
					BroadcastMessagesTransaction.languageChange();
					
			        MessageFacade.hideDialogNow();
				}
			};
			t.start();
		} catch (Exception e) {
			Debug.printException(e);
		}
	}

    /** Toggle to Spanish. */
    public void spanish() {
    	changeLanguages("es", "en");
    }

    /** Toggle to English. */
    public void english() {
		changeLanguages("en", Messages.getCurrentLocale().getLanguage());
    }

    /** Toggle to German. */
    public void german(){
    	 changeLanguages("de", "en");
    }

    /** Toggle to French. */
    public void french(){
    	 changeLanguages("fr", "en");
     }

    /** Toggle to Chinese. */
    public void chinese(){
    	changeLanguages("cn", "en");
     }

    /** Toggle to Polish. */
    public void polish(){
    	changeLanguages("pl", "en");
     }

    /** Toggle to Russian. */
    public void russian(){
    	changeLanguages("ru", "en");
     }

    /** Toggle to Portuguese. */
    public void portuguese(){
    	changeLanguages("pt", "en");
     }

	/**
	 * 12-17-04: More completely destroy the main panel.
	 */
    @Override
	public void refreshMainPanel() {
        if (! SwingUtilities.isEventDispatchThread()) {
            Debug.println("refreshMainPanel() called, not in event dispatch thread.");
        }

        parentFrame.getContentPane().removeAll();

		instance.clear();
		instance.cleanup();
		instance.removeAll();
		PageNotification.removeAllPageExitListeners();
		KeyMapManager.removeAllMappings();

		//START : NULL all non-prim references
		// components we need to access throughout this class

		//Make progress bar non-indeterminate to let it be free!
		JProgressBar jp = ((JProgressBar) getComponent("progressBar"));
		if (jp != null)
			jp.setIndeterminate(false);

		statusPanel = null;
		statusLabel = null;
		formsLabel = null;
		dispenserFormsLabel = null;
		periodLabel = null;
		menuBar = null;
		trainingModeMenuItem = null;
		softwareLabel = null;

		// transaction objects...only created once and re-used throughout run of app
		moneyOrderTransaction = null;
		moneyGramSendTransaction = null;
		billPaymentTransaction = null;
		formFreeSendTransaction = null;
		addMinutesTransaction = null;
		moneyGramReceiveTransaction = null;
		sendReversalTransaction = null;
		receiveReversalTransaction = null;
		amendTransaction = null;
		vendorPaymentTransaction = null;
		agentDirectoryTransaction = null;
		dispenserLoadTransaction = null;
		multiAgentTransaction = null;
		installTransaction = null;
		simplePMTransaction = null;

		// current transaction state
		currentTransactionInterface = null;
		currentTransaction = null;

		mainMenuPage = null;

		// store the transaction to resume after the dispenser load
		pendingTransaction = null;

		if (tranCreationSemaphore != null) {
			tranCreationSemaphore.resume();
			tranCreationSemaphore = null;
		}

		transmitter = null;
		//END: NULL all non-prim references

		DeltaworksMainPanel i = create(parentFrame);
        setInstance(i);
//		DeltaworksStartup.mainPanelInstanceChange(instance);

        parentFrame.getContentPane().add(BorderLayout.CENTER, instance);
        instance.refreshFrameComponents();


        parentFrame = null;
    }

    private static void setInstance(DeltaworksMainPanel i) {
    	instance = i;
    }

    @Override
	public void refreshToggleButton(){
        instance.mainMenuPage.resetMapping(DeltaworksMainMenu.LANGUAGE_TOGGLE);
    }

    /**
     * Start the directory of agents transaction.
     */
    public void moneyGramFeeQuery() {
        if (isHostCommInProgress())
            return;

        currentTransaction = moneyGramSendTransaction;
        moneyGramSendTransaction.setFeeQuery(true);
        currentTransaction.setUsingWizard(true);
        currentTransaction.setStatus(ClientTransaction.COMPLETED);
        startTransactionInterface();
    }

    /**
     * Start the directory of agents transaction.
     */
    public void directoryOfAgents() {
        if (isHostCommInProgress())
            return;
        currentTransaction = agentDirectoryTransaction;
        currentTransaction.setStatus(ClientTransaction.COMPLETED);
        startTransactionInterface();
    }


    /**
     * Start the Transaction Status Service.
     */
    public void transactionStatus() {
        if (isHostCommInProgress())
            return;
        transactionStatusTransaction.setTransactionStatus(true);
        currentTransaction = transactionStatusTransaction;
        currentTransaction.setStatus(ClientTransaction.COMPLETED);
        currentTransaction.setFinancialTransaction(false);
        startTransactionInterface();
    }

    /**
     * Reprint Receipt.
     */
    public void reprintReceipt() {
        if (isHostCommInProgress())
            return;

        /*
         * If the save environment cannot be restored, delete the file, error,
         * and exit.
         */
//        if (!ReprintReceiptFlowPage.readTran()) {
//        	ReprintFile.deleteFile(true);
//            Dialogs.showError("dialogs/DialogReprintUnavailable.xml");
//            return;
//        }
//        currentTransaction = reprintReceiptTransaction;
//        currentTransaction.setStatus(ClientTransaction.COMPLETED);
//        startTransactionInterface();

		try {
			Thread t = new Thread() {
				private final int[] PDF_RCPTS_TO_PRINT = {
						PdfReceipt.PDF_AGENT_1,
						PdfReceipt.PDF_AGENT_2,
						PdfReceipt.PDF_CUST_1,
						PdfReceipt.PDF_CUST_2,
					};
					
					private final int[] PRT_RCPTS_TO_PRINT = {
						PrtReceipt.PRT_AGENT_1,
						PrtReceipt.PRT_AGENT_2,
						PrtReceipt.PRT_CUST_1,
						PrtReceipt.PRT_CUST_2,
					};
				@Override
				public void run() {
					
			    	/*
			    	 * Check for and use any PDF receipts first
			    	 */
			    	boolean bPdfAvailable = false;
			    	for(int ii = 0; ii< PDF_RCPTS_TO_PRINT.length; ii++) {
			    		int iii = PDF_RCPTS_TO_PRINT[ii];
			    		if (PdfReceipt.isReprintReceiptFileAvailable(iii)) {
			    			bPdfAvailable = true;
				    		PdfReceipt pdfReceipt = new PdfReceipt(iii);
							while (! pdfReceipt.print()) {
								if (Dialogs.showRetryCancel("dialogs/DialogReceiptReprintFailure.xml")== Dialogs.CANCEL_OPTION) {
									break;
								}
							}
			    		}
			    	}
			    	/*
			    	 * If there were PDF receipts found, exit now
			    	 */
			    	if (bPdfAvailable) {
			    		return;
			    	}

			    	/*
			    	 * Check for and use any PRT receipts next
			    	 */
			    	boolean bPrtAvailable = false;
			    	for(int ii = 0; ii < PRT_RCPTS_TO_PRINT.length; ii++) {
			    		int iii = PRT_RCPTS_TO_PRINT[ii];
			    		if (PrtReceipt.isReprintReceiptFileAvailable(iii)) {
			    			bPrtAvailable = true;
			    			PrtReceipt prtReceipt = new PrtReceipt(iii);
							while (! prtReceipt.print()) {
								if (Dialogs.showRetryCancel("dialogs/DialogReceiptReprintFailure.xml")== Dialogs.CANCEL_OPTION) {
									break;
								}
							}
			    		}
			    	}
			    	/*
			    	 * If there were RT receipts found, exit now
			    	 */
			    	if (bPrtAvailable) {
			    		return;
			    	}
				}
			};
			t.start();
		} catch (Exception e) {
			Debug.printException(e);
		}
    }

    public void registerMfaToken() {
        if (isHostCommInProgress())
            return;

        currentTransaction = registerMfaToken;
        currentTransaction.setStatus(ClientTransaction.COMPLETED);
        currentTransaction.setFinancialTransaction(false);
        startTransactionInterface();
    }


    /**
    /**
     * Start the directory of agents transaction.
     */
    public void directoryOfBillers() {
        if (isHostCommInProgress())
            return;
        currentTransaction = billPaymentTransaction;
        billPaymentTransaction.setDirectoryOfBillers(true);
        currentTransaction.setUsingWizard(true);
        currentTransaction.setStatus(ClientTransaction.COMPLETED);
        startTransactionInterface();
    }

    /**
     * Prompt for a manager password before changing to / from training mode.
     *   First make sure the profile lock flag allows the mode to be changed.
     *   If it does, prompt for manager then switch the value.
     * When we enter / exit training mode, notify the money gram transactions
     *   of this so they can use the appropriate message forwarder.
     */
    public void trainingMode() {
        ProfileItem trainingItem = UnitProfile.getInstance().getProfileInstance().find("TRAINING_MODE"); 
        boolean current = trainingItem.booleanValue();
        if (isHostCommInProgress()) {
            trainingModeMenuItem.setSelected(current); // set it back
            return;
        }
        setCurrentTransaction(SimplePMTransaction.TRAINING_MODE_OPTIONS);
        String mode = trainingItem.getMode();
        if (!mode.equals("S") && !mode.equals("P")) {  
            // should never get here, because there will be no menu item for it
            trainingModeMenuItem.setSelected(current); // set it back
            return;
        }

        // display the login dialog and look for this tag in their user profile
        String loginType = getGlobalLoginTypeRequired();
        if (! loginType.equals("N")) {
            boolean nameRequired = loginType.equals("Y");
            int userID = getUserAuthority("TOGGLE_TRAINING_MODE", nameRequired); 

            if (userID > 0) {
                // login was ok, so set training mode to the toggled value
                setTrainingMode(!current);
                String c = String.valueOf(current);
                if (c.trim().equals("false"))
                	c = "N";
                else
                	c = "Y";
        		AuditLog.writeProfileChangeRecord(userID, "TRAINING_MODE", UnitProfile.getInstance().getProfileInstance().find("TRAINING_MODE").stringValue(), c);
                return;
            }
            else if (userID == 0) // they do not have authority for this action
                showNotAuthorizedDialog();
        }
        else {
            setTrainingMode(!current);
            return;
        }

        trainingModeMenuItem.setSelected(current); // set it back

    }

    /**
     * Start the dispenser loading transaction.
     */
    public void loadForms() {
        if (isHostCommInProgress())
            return;
        currentTransaction = dispenserLoadTransaction;
        currentTransaction.setStatus(ClientTransaction.COMPLETED);
        startTransactionInterface();
    }

    public void restart() {
        if (Dialogs.showQuestion("dialogs/DialogRestartExit.xml") == Dialogs.YES_OPTION) 
            tryExit(RESTART_EXIT);
    }

    public void exit() {
    	 if(UnitProfile.getInstance().isMultiAgentMode()){
	    	 if (Dialogs.showQuestion("dialogs/DialogExit.xml") == Dialogs.YES_OPTION) 
   	      if(!UnitProfile.getInstance().isSuperAgent()){
   	     	MainPanelDelegate.changeToSuperAgent(false);
   	      }
   	      else
   	        MainPanelDelegate.tryExit(DeltaworksMainPanel.NORMAL_EXIT);
	      }
   	   else{
   	        if (Dialogs.showQuestion("dialogs/DialogExit.xml") == Dialogs.YES_OPTION) 
             MainPanelDelegate.tryExit(DeltaworksMainPanel.NORMAL_EXIT);
   	      }
    }

    public void generalOptions() {
        startSimplePM(SimplePMTransaction.GENERAL_OPTIONS);
    }

    public void proxySettingOptions() {
        startSimplePM(SimplePMTransaction.PROXY_SETTING_OPTIONS);
    }

    public void dialupSettingOptions() {
        startSimplePM(SimplePMTransaction.DIALUP_SETTING_OPTIONS);
    }
    public void dialupSettingOptionsSecondary() {
        startSimplePM(SimplePMTransaction.DIALUP_SETTING_OPTIONS_SECONDARY);
    }

    public void dispenserOptions() {
        startSimplePM(SimplePMTransaction.DISPENSER_OPTIONS);
    }

    public void printerOptions() {
        startSimplePM(SimplePMTransaction.PRINTER_OPTIONS);
    }

    public void payeeTable() {
        startSimplePM(SimplePMTransaction.PAYEE_TABLE);
    }

    public void moneyOrderOptions() {
        startSimplePM(SimplePMTransaction.MO_OPTIONS);
    }

    public void vendorPaymentOptions() {
        startSimplePM(SimplePMTransaction.VP_OPTIONS);
    }

    public void moneyGramSendOptions() {
        startSimplePM(SimplePMTransaction.MGS_OPTIONS);
    }

    public void moneyGramReceiveOptions() {
        startSimplePM(SimplePMTransaction.MGMO_OPTIONS);
    }

    public void billPaymentOptions() {
        startSimplePM(SimplePMTransaction.BILL_PAYMENT_OPTIONS);
    }

    public void cardPurchaseOptions() {
    	startSimplePM(SimplePMTransaction.CARD_PURCHASE_OPTIONS);
    }

    public void cardReloadOptions() {
    	startSimplePM(SimplePMTransaction.CARD_RELOAD_OPTIONS);
    }

    public void managerOptions() {
        startSimplePM(SimplePMTransaction.MANAGER_OPTIONS);
    }

    public void employeeOptions() {
        startSimplePM(SimplePMTransaction.USER_OPTIONS);
    }

    private void startSimplePM(int optionType) {
        if (isHostCommInProgress())
            return;

        simplePMTransaction.setCurrentOption(optionType);
        currentTransaction = simplePMTransaction;

        currentTransaction.setStatus(ClientTransaction.COMPLETED);

        startTransactionInterface();
    }
    private void setCurrentTransaction(int optionType) {
        simplePMTransaction.setCurrentOption(optionType);
        currentTransaction = simplePMTransaction;
        currentTransaction.setStatus(ClientTransaction.COMPLETED);
    }


    /**
     * Spit out a voided form and update the forms label accordingly.
     */
    public void printTestVoid() {
        currentTransaction = null;    //clear out the current transaction.
        if (isHostCommInProgress())
            return;
        if (isDispenserReady()) {
            DispenserInterface d = DispenserFactory.getDispenser(1);
            d.fullVoidForm(0, false, MoneyOrderVoidReasonCodeType.TEST_VOID);
        }
        updateFormsLabel();
    }

    /**
     * Send a manual transmit to the host. Before attempting to send the data,
     *   do a client check in to make sure there is connectivity.
     * This is called from the menu item.
     */

    public void transmit() {
        // don't allow transmit during profile update
        if (isHostCommInProgress())
            return;

        // send the diags, profile changes, get new profile in background thread
        Thread t = new Thread(new Runnable() {
            @Override
			public void run() {
            	VersionManifest.resetDownloadCounter();
                if (transmitAll(true) == false) {
                    // Transmit failed, show a dialog.
                    // It would be nice for RasActivatorImpl to expose the
                    // most recent error code at about line 1130, but lets
                    // just see if this part works.
                    if(deAuthMessage)
                        ErrorManager.setStatus(Messages.getString("DeltaworksMainPanel.deauthMessage"), ErrorManager.ERROR_STATUS); 
                    else
                    	 if(deActivateMessage)
                            ErrorManager.setStatus(Messages.getString("DeltaworksMainPanel.deActivateMessage"), ErrorManager.ERROR_STATUS); 
                    else
                        ErrorManager.setStatus(Messages.getString("DeltaworksMainPanel.unableToTransmit"), ErrorManager.WARNING_STATUS); 
                    setDuringManualTransmit(false);
                    //deAuthMessage = false;

                }

                checkDailyTotalReset();
                checkDailyBillPayTotalReset();
                AgentCountryInfo.setAgentCountryFields();
//                ReceiptTemplates.updateTemplateLists();   /*Temporary until DCA works */
            }

        });

        t.start();

    }
    private void checkDailyTotalReset() {
        final Date lastResetTime = UnitProfile.getInstance().getLastDailyTotalResetTime();
        if (Scheduler.daysElapsed(lastResetTime) > 0) {
            UnitProfile.getInstance().clearDailyTotal();
            UnitProfile.getInstance().clearDailyCount();
            UnitProfile.getInstance().setLastDailyTotalResetTime(TimeUtility.currentTime());
        }
    }
    private void checkDailyBillPayTotalReset() {
        final Date lastResetTime = UnitProfile.getInstance().getLastDailyBillPayTotalResetTime();
        if (Scheduler.daysElapsed(lastResetTime) > 0) {
            UnitProfile.getInstance().clearDailyBillPayTotal();
            UnitProfile.getInstance().setLastBillPayDailyTotalResetTime(TimeUtility.currentTime());
        }
    }

    /**
     * Clear out the log files. This requires the setup password stored in the
     *   profile.
     */
    public void clearLogs() {
        final MessageFacadeParam parameters = new MessageFacadeParam(MessageFacadeParam.REAL_TIME);
        try {
            setDuringManualTransmit(true);
            int result = getResetLogin();
            if (result == -1) { // bad login
                showNotAuthorizedDialog();
                return;
            }
            else if (result == 0) { // user canceled login attempt
                return;
            }
        }
        finally {
            setDuringManualTransmit(false);
        }

        setDuringManualTransmit(true);

        Thread t = new Thread(new Runnable() {
            @Override
			public void run() {
                    // if we are not in DEMO, transmit before clearing logs.
                try {
                    if (!UnitProfile.getInstance().isDemoMode()) {
                        // start the cycling label saying we are sending diags
                        diagTransmitStarted();

                        // transmit the diags
                        DiagLog.getInstance().transmit();
                        Debug.println("Before itemForwarder.attemptTransmit, mustHandleLogsFlag is: " + mustHandleLogsFlag); 

                        // try to send off all items
                        transmitter.attemptTransmit(parameters);
                        Debug.println("After itemForwarder.attemptTransmit, mustHandleLogsFlag is: " + mustHandleLogsFlag); 
                    }

                    // good login...clear logs and notify user
                    Period.clearLogs();

                    Dialogs.showInformation("dialogs/DialogLogsCleared.xml"); 

                    Debug.println("Reaching the MustHandleLogsFlag handler! Flag is: " + mustHandleLogsFlag); 

                    //if mustHandleLogsFlag was set, then they're handled now; re-allow access and perform any other necessary actions.

                    if (mustHandleLogsFlag) {
                        clearMustHandleLogsFlag();
                    }

                    ErrorManager.setStatus(Messages.getString("DeltaworksMainPanel.1963"), ErrorManager.INFORMATION_STATUS); 

                    //write a diag message to the new log saying that old logs were cleared
                    ErrorManager.writeGeneralDiagnostic("CLEARED LOGS"); 

                    // if we were locked up due to DEMO timeout, unlock now
                    if (UnitProfile.getInstance().isDemoMode()) {
                        UnitProfile.getInstance().setLastReauthTime(new Date());
                    }
                }
                finally {
                    setDuringManualTransmit(false);
                }
            }
        });
        t.start();
    }

    public static int getTransmitProcessAttempts() {
        return transmitProcessAttempts;
    }

    /**
     * Determine whether or not the user has authority to perform a task based
     *   on a particular user profile setting.
     * @return userID if they have authority, 0 if they do not, -1 if they cancel
     */
    private int getUserAuthority(String requiredTag, boolean nameRequired) {
        // prompt the user for a name/password
        int attempt = 0;
        int userID = -1;
        while (attempt < 3) {
            boolean invalid = false;
            if (attempt++ > 0)
                invalid = true;
            NamePasswordDialog dialog =
                Dialogs.showNamePasswordDialog(
                    NamePasswordDialog.USER_LOGIN,
                    nameRequired,
                    invalid);

            if (dialog.isCanceled()) {
           	   MainPanelDelegate.setupCancelISI("01");
               if (MainPanelDelegate.getIsiNfProgramArguments() != null  &&
              		"Y".equals(UnitProfile.getInstance().get("ISI_ENABLE", "N"))){
                  String programName = MainPanelDelegate.getIsiNfProgramArguments().trim();
                  if (programName.length() > 0 && !(programName.startsWith("-NF"))) {
                     MainPanelDelegate.executeIsiProgram(programName);
                  }
                  MainPanelDelegate.setIsiNfCommand(false);
                  MainPanelDelegate.setIsiNfProgramArguments("");
                }
                return -1;
            }
            String name = dialog.getName();
            String pwd = dialog.getPassword();
            userID = ClientTransaction.getUserID(name, pwd, false);
            if(currentTransaction.isUserLocked(name,pwd)){
                Dialogs.showWarning("dialogs/DialogAccountLocked.xml");
                return -1;
            }

            if (userID > 0) {
            	if(UnitProfile.getInstance().isSubAgent()){
            		if(UnitProfile.getInstance().isSubAgent()){
                        if(!isSubAgentTakeOverUserID(userID)){
                        	  if(attempt <=2){
                        	  int option = Dialogs.showConfirm("dialogs/DialogUserAuthirizationInSubAgentMode.xml");
                        	  if(option == Dialogs.OK_OPTION){
                        		Dialogs.closeAllDialogs();
                  		 	    userID = 0;
                  		 	    continue;
                        	  }
                        	  else{
                        	  	Dialogs.closeAllDialogs();
                        	  	userID = -1;
                    		    MainPanelDelegate.changeToSuperAgent(true);
                    		    return userID;

                        	  }
                        	  }
                        	else
                        		return -1;


                        }
                      }
                  }

                // make sure their profile tag is Y
                Profile userProf = (Profile) UnitProfile.getInstance().getProfileInstance().find("USER", userID); 
                try {
                    ProfileItem item = userProf.find(requiredTag);
                    if (item.booleanValue()){
                      return isPasswordChangeRequired(name,pwd,userID);
                    }
                    else
                        return 0;
                }
                catch (NoSuchElementException e) {
                    Debug.println("No element : " + requiredTag + " in user profile.");  
                    return 0;
                }

            } else if (userID < 0) {
                currentTransaction.handleExpired(name,pwd);
                // closePeriod doesn't use a transaction, so must cancel this way
                return -1;

            } else {
                ErrorManager.recordLoginAttempt(false, name);
                if(attempt==3){
                    if(currentTransaction.isSixCharPinType()){
                    	currentTransaction.lockUser(name,pwd);
                    	return -1;
                    }
                }
                continue;
            }
//            ErrorManager.recordLoginAttempt(true, name);
//
//
//            break;
        }
        return -1;
    }


private int isPasswordChangeRequired(String name, String pwd, int userID){

	 if(currentTransaction.hasPasswordExpired(name,pwd)){
	 	currentTransaction.handleExpired(name,pwd);
       cDialog = Dialogs.showChangePasswordDialog(userID, pwd,currentTransaction);
       if (cDialog.isCanceled()){
		      return -1;
		      }
       else
       	 return userID;
		  }
	 else
	 {
	 	if(currentTransaction.isPasswordChangeRequired(userID)){
	 	 cDialog = Dialogs.showChangePasswordDialog(userID,pwd,currentTransaction);
        if (cDialog.isCanceled()){
		      return -1;
		      }
        else
        	 return userID;
	 }
	else
		return userID;

	 }

}
    /**
     * Give the user 3 tries to login. Return their userID if they login in
     *   successfully. Otherwise, return -1. Return -2 if they canceled.
     */
    private int getUserLogin(

        boolean managerRequired,
        boolean nameRequired) {
        // prompt the user for a name/password
        int attempt = 0;
        int userID = -1;

        int loginType =
            managerRequired ? NamePasswordDialog.MANAGER_LOGIN : NamePasswordDialog.USER_LOGIN;
        while (attempt < 3) {
            boolean invalid = false;
            if (attempt++ > 0)
                invalid = true;
            NamePasswordDialog dialog = Dialogs.showNamePasswordDialog(loginType, nameRequired, invalid);

            if (dialog.isCanceled()) {
           	   MainPanelDelegate.setupCancelISI("01");
               if (MainPanelDelegate.getIsiNfProgramArguments() != null  &&
              		"Y".equals(UnitProfile.getInstance().get("ISI_ENABLE", "N"))){
                  String programName = MainPanelDelegate.getIsiNfProgramArguments().trim();
                  if (programName.length() > 0 && !(programName.startsWith("-NF"))) {
                     MainPanelDelegate.executeIsiProgram(programName);
                  }
                  MainPanelDelegate.setIsiNfCommand(false);
                  MainPanelDelegate.setIsiNfProgramArguments("");
                }
                return -1;
            }
            String name = dialog.getName();
            String pwd = dialog.getPassword();
            userID = ClientTransaction.getUserID(name, pwd, managerRequired);
            if(currentTransaction.isUserLocked(name,pwd)){
                Dialogs.showWarning("dialogs/DialogAccountLocked.xml");
                return -1;
            }

            if (userID < 0) {
                currentTransaction.handleExpired(name, pwd);
                // closePeriod doesn't use a transaction, so must cancel this way
                return -1;

            } else if (userID > 0){
            	if(UnitProfile.getInstance().isSubAgent()){
                    if(!isSubAgentTakeOverUserID(userID)){
                    	  if(attempt <=2){
                    	  int option = Dialogs.showConfirm("dialogs/DialogUserAuthirizationInSubAgentMode.xml");
                    	  if(option == Dialogs.OK_OPTION){
                    		Dialogs.closeAllDialogs();
              		 	    userID = 0;
              		 	    continue;
                    	  }
                    	  else{
                    	  	Dialogs.closeAllDialogs();
                    	  	userID = -1;
                		    MainPanelDelegate.changeToSuperAgent(true);
                		    return userID;

                    	  }
                    	  }
                    	else
                    		return -1;


                    }
                  }
            	return isPasswordChangeRequired(name,pwd,userID);

            } else {
                ErrorManager.recordLoginAttempt(false, name);
                if(attempt==3){
                    if(currentTransaction.isSixCharPinType()){
                    	currentTransaction.lockUser(name,pwd);
                    	return -1;
                    }
                }
                continue;
            }
//            ErrorManager.recordLoginAttempt(true, name);
//            break;
        }
        return userID;
    }

    /**
     * Prompt the user for the reset password. If it is entered correctly,
     *   return a 1. If canceled, return a 0. If bad login, return a -1.
     */
    private int getResetLogin() {
        int attempt = 0;
        while (attempt < 3) {
            boolean invalid = false;
            if (attempt++ > 0)
                invalid = true;
            NamePasswordDialog dialog =
                Dialogs.showNamePasswordDialog(NamePasswordDialog.USER_LOGIN, false, invalid);

            if (dialog.isCanceled()) {
          	   MainPanelDelegate.setupCancelISI("01");
               if (MainPanelDelegate.getIsiNfProgramArguments() != null  &&
              		"Y".equals(UnitProfile.getInstance().get("ISI_ENABLE", "N"))){
                  String programName = MainPanelDelegate.getIsiNfProgramArguments().trim();
                  if (programName.length() > 0 && !(programName.startsWith("-NF"))) {
                     MainPanelDelegate.executeIsiProgram(programName);
                  }
                  MainPanelDelegate.setIsiNfCommand(false);
                  MainPanelDelegate.setIsiNfProgramArguments("");
                }
                return 0;
            }
            String pwd = dialog.getPassword();
            // is this the profile reset password?
            String resetPwd = UnitProfile.getInstance().getProfileInstance().find("RESET_PASSWORD").stringValue(); 
            if (pwd.equalsIgnoreCase(resetPwd)) {
                return 1;
            }
        }
        return -1;
    }

    private void showNotAuthorizedDialog() {
        Dialogs.showError("dialogs/DialogNotAuthorized.xml"); 
    }

    private void showNotAuthorizedNoLoginDialog() {
        Dialogs.showError("dialogs/DialogNotAuthorizedNoLogin.xml"); 
    }

    /**
     * Modem Speaker menu option. Turn on/off the modem speaker. This is off by
     *   default, but may want to turn it on for troubleshooting purposes. Not
     *   persistent, so turns off again when restarted.
     */
    public void modemSpeaker() {
        JCheckBoxMenuItem item = (JCheckBoxMenuItem) getComponent("modemSpeaker"); 
        RasProperties.setMuted(item.isSelected());
        UnitProfile.getInstance().setModemSpeakerMuted(item.isSelected());
    }

    /**
     * Manual Dial menu option. Give the user a dialog with the phone #, name
     *   and password to dial. When the dial completes, release it shortly with
     *   dontNeed(). If they cancel the dial, release the connection immediately
     */
    public void manualDial() {
        final CommCompleteInterface cci = null;

        String currentPhoneNumber = null;
        String currentUsername = null;
        String currentPassword = null;

        ManualDialDialog dialog = new ManualDialDialog();
        dialog.setDialInfo(UnitProfile.getInstance().getProfileInstance().find("HOST_SPECIAL_DIAL").stringValue() + 
        UnitProfile.getInstance().getProfileInstance().find("HOST_PRIMARY_PHONE").stringValue(), 
        UnitProfile.getInstance().getProfileInstance().find("PRIMARY_DUN_USER_ID").stringValue(), 
        UnitProfile.getInstance().getProfileInstance().find("PRIMARY_DUN_PASSWORD").stringValue()); 
        Dialogs.showPanel(dialog);

        boolean canceled = dialog.isCanceled();

        if (canceled) {
            return;
        }
        else {
            currentPhoneNumber = dialog.getPhoneNumber();
            currentUsername = dialog.getUsername();
            currentPassword = dialog.getPassword();
        }

            final MessageFacadeParam parameters = new MessageFacadeParam(MessageFacadeParam.REAL_TIME, "", 
            		currentPhoneNumber, currentUsername, currentPassword);

        final MessageLogic logic = new MessageLogic() {
            @Override
			public Object run() {
                MessageFacade.getInstance().manualDial(parameters);
                return Boolean.TRUE;
            }
        };
        MessageFacade.run(logic, cci, 0, Boolean.TRUE);
    }

    /**
     * Close out the current period. This may only be done by a user who has the
     *   period close option enabled in their profile. If the login is correct,
     *   try to close the period. Give the user an error it they are outside the
     *   period close window.
     */
    public void closeDispenserPeriod() {
        if (isHostCommInProgress())
            return;
        setCurrentTransaction(SimplePMTransaction.CLOSE_DISPENSER_PERIOD_OPTIONS);
        int userID = 0;
        String loginType = getGlobalLoginTypeRequired();
        if (! loginType.equals("N")) {
            boolean nameRequired = loginType.equals("Y");
            userID = getUserAuthority("PERIOD_CLOSE", nameRequired); 
            if (userID == 0) {
                showNotAuthorizedDialog();
                return;
            }
            if (userID == -1)
                return; // means they canceled...
        }
        try{
            currentTransaction.setUserID(userID);
            }catch(Exception e){

            }
        // all is well...go ahead and try to close the period
        if (!Period.closeAccountingPeriod(userID)) {
            Dialogs.showError("dialogs/DialogOutsideWindow.xml"); 
        }
        else {
            Dialogs.showInformation("dialogs/DialogPeriodClosed.xml"); 
            // update the period label to the new period & set the status
            updatePeriodLabel();
            ErrorManager.setStatus(Messages.getString("DeltaworksMainPanel.1964"), 
            ErrorManager.INFORMATION_STATUS);
        }
    }

    /**
     * Amend Transaction
     */
    public void amendTransaction() {

        if (isHostCommInProgress())
            return;

        currentTransaction = amendTransaction;
        currentTransaction.setUsingWizard(true);
        currentTransaction.setStatus(ClientTransaction.COMPLETED);
        startTransactionInterface(true, false);
    }

    /**
     * Receive Transaction Reversal
     */
    public void receiveReversalTransaction() {

        if (isHostCommInProgress())
            return;

        currentTransaction = receiveReversalTransaction;
        currentTransaction.setUsingWizard(true);
        currentTransaction.setStatus(ClientTransaction.COMPLETED);
        startTransactionInterface(true, false);
    }

    /**
     * Send Transaction Reversal
     */
    public void sendReversalTransaction() {
    	
        if (isHostCommInProgress())
           return;

        currentTransaction = sendReversalTransaction;
        currentTransaction.setUsingWizard(true);
        currentTransaction.setStatus(ClientTransaction.COMPLETED);
        startTransactionInterface(true, false);
    }

    /**
     * Multi Agent Mode
     */
    public void changeMultiAgent() {
        if (isHostCommInProgress())
            return;

        currentTransaction = multiAgentTransaction;
        int userID =-1;
        /* We only want to allow this transaction to be allowed if
           the user is within business hours.  If they are, commence with
           the login process.  If they are not within business hours,
           startTransactionInterface will catch this and not allow the transaction
           to occur.
        */
        String loginType = getGlobalLoginTypeRequired();
        if (! loginType.equals("N")) {
            boolean nameRequired = loginType.equals("Y");
             userID = getUserLogin(false, nameRequired);
            if (userID == 0) {
                showNotAuthorizedDialog();
                return;
            }
            if (userID == -1)
                return; // means they canceled...
        }


        currentTransaction.setStatus(ClientTransaction.IN_PROCESS);
        startTransactionInterface(false, false);
        try{
            currentTransaction.setUserID(userID);
            }catch(Exception e){

            }
    }


    /** For testing. */
    public void screwUpDispenserPeriod() {
        int period = UnitProfile.getInstance().getCurrentPeriod();
        period = period + 4;
        if (period > 7) {
            period = period - 7;
        }
        Period.openPeriod(period, false, 0);
        updatePeriodLabel();
    }

    /**
     * Clear the logs and revert back to the warehouse profile and DEMO mode.
     *   This requires the profile reset password. Must also connect to the
     *   host to do a transmit before this is allowed.
     */
    public void reverseSetup() {
        if (isHostCommInProgress())
            return;
        int result = getResetLogin();
        if (result == 0)
            return;
        if (result == -1) { // bad login
            showNotAuthorizedDialog();
            return;
        }
        // do the reverse setup stuff here
        Period.reverseSetup();
        // restart the app now with the restart status code
        System.exit(RESTART_EXIT);
    }

    public synchronized void displayFtpErrorDialog() {
        if (ftpSuccess) {
            ftpSuccess = false;

            Dialogs.showError("dialogs/DialogFtpError.xml"); 
        }
    }

   public void fakeDownload() {
        Runnable run = new Runnable() {
            @Override
			public void run() {
                getComponent("pendingVersionLabel").setVisible(false); 
                getComponent("downloadFailedLabel").setVisible(false); 
                ErrorManager.softwareDownloadStarted();
                try {
                    for (int i = 1; i <= 13; i++) {
                        Thread.sleep(2000);
                        ErrorManager.updateProgressBarString(i + " of 13"); 
                    }
                }
                catch (InterruptedException e) {
                }

                ErrorManager.softwareDownloadFinished(true, "fakeDownload complete"); 
            }
        };
        new Thread(run).start();
    }

//    public void prepaidCardReport() {
//    	int userID = -1;
//        if (isHostCommInProgress())
//            return;
//        setCurrentTransaction(SimplePMTransaction.PREPAID_CARD_REPORT_OPTIONS);
//        String loginType = getGlobalLoginTypeRequired();
//        if (! loginType.equals("N")) {
//            boolean nameRequired = loginType.equals("Y");
//            userID = getUserAuthority("TOTALS_REPORT", nameRequired); 
//            if (userID == 0) {
//                showNotAuthorizedDialog();
//                return;
//            }
//            if (userID == -1)
//                return; // means they canceled...
//        }
//        try{
//            currentTransaction.setUserID(userID);
//            }catch(Exception e){
//
//            }
//        final Report report = new PrepaidCardReport(userID);
//        try {
//        	(new DatePeriodEntryDialog(null, report, true, true, false,
//        			Resources.getString("DeltaworksMainPanel.Prepaid_Card_Report__-_RP10_154"), false, false, false)).show(); 
//        }
//        catch (Throwable t) {
//            Debug.printStackTrace(t);
//        }
//   }
//
//
    public void auditTrailReport() {

    	int userID = -1;
        if (isHostCommInProgress())
            return;
        setCurrentTransaction(SimplePMTransaction.AUDIT_TRAIL_REPORT_OPTIONS);
        String loginType = getGlobalLoginTypeRequired();
        if (! loginType.equals("N")) {
            boolean nameRequired = loginType.equals("Y");
            userID = getUserLogin(true, nameRequired); 

            if (userID == 0) {
                showNotAuthorizedDialog();
                return;
            }
            if (userID == -1)
                return; // means they canceled...
        }
        try{
            currentTransaction.setUserID(userID);
            }catch(Exception e){

            }
        int dateRange = UnitProfile.getInstance().get("DAYS_IN_AUDIT_TRAIL", 7);
        AuditDialog auditDialog = new AuditDialog(dateRange, userID);
        auditDialog.show();
    }

    public void dispenserItemsSalesReport() {
        if (isHostCommInProgress())
            return;
        setCurrentTransaction(SimplePMTransaction.DISPENSER_ITEM_SALES_REPORT_OPTIONS);
        int userID = -1;
        String loginType = getGlobalLoginTypeRequired();
        if (! loginType.equals("N")) {
            boolean nameRequired = loginType.equals("Y");
            userID = getUserAuthority("TOTALS_REPORT", nameRequired); 
            if (userID == 0) {
                showNotAuthorizedDialog();
                return;
            }
            if (userID == -1)
                return; // means they canceled...
        }
        try{
            currentTransaction.setUserID(userID);
            }catch(Exception e){

            }
        final Report report = new DispenserItemsReport("xml/reports/dispenseritemssalesreport.xml"); 
         (new DatePeriodEntryDialog(report, true, true, false,
         		Messages.getString("DeltaworksMainPanel.1965"), true, false, false)).show(); 
    }

    public void employeeTotals() {
        if (isHostCommInProgress())
            return;
        setCurrentTransaction(SimplePMTransaction.EMPLOYEES_TOTAL_OPTIONS);
        int userID= -1;
        String loginType = getGlobalLoginTypeRequired();
        if (! loginType.equals("N")) {
            boolean nameRequired = loginType.equals("Y");
            userID = getUserAuthority("TOTALS_REPORT", nameRequired); 
            if (userID == 0) {
                showNotAuthorizedDialog();
                return;
            }
            if (userID == -1)
                return; // means they canceled...
        }
        try{
            currentTransaction.setUserID(userID);
            }catch(Exception e){

            }
        final Report report = new DispenserItemsReport("xml/reports/employeereport.xml"); 
         (new DetailSelectionEntryDialog(report, true, true, true, true, false, false, false, false,
         		Messages.getString("DeltaworksMainPanel.1966"), false, false)).show(); 
    }

    public void detailReporting() {
        if (isHostCommInProgress())
            return;
        setCurrentTransaction(SimplePMTransaction.DETAIL_REPORTING_OPTIONS);
        int userID= -1;
        String loginType = getGlobalLoginTypeRequired();
        if (! loginType.equals("N")) {
            boolean nameRequired = loginType.equals("Y");
           userID = getUserAuthority("TOTALS_REPORT", nameRequired); 
            if (userID == 0) {
                showNotAuthorizedDialog();
                return;
            }
            if (userID == -1)
                return; // means they canceled...
        }
        try{
            currentTransaction.setUserID(userID);
            }catch(Exception e){

            }
        final Report report = new DispenserItemsReport("xml/reports/detailreport.xml"); 
         (new DetailSelectionEntryDialog(report, true, true, true, false, true, true, true, true, Messages.getString("DeltaworksMainPanel.1967"), false, false)).show(); 
    }

    public void moneyGramReport() {
    	int userID = -1;
        if (isHostCommInProgress())
           return;
        setCurrentTransaction(SimplePMTransaction.MONEYGRAM_REPORT_OPTIONS);
        String loginType = getGlobalLoginTypeRequired();
        if (! loginType.equals("N")) {
            boolean nameRequired = loginType.equals("Y");
            try{
            userID = getUserAuthority("TOTALS_REPORT", nameRequired); 
            }catch(Exception e){
                Debug.printStackTrace(e);
            }
            if (userID == 0) {
                showNotAuthorizedDialog();
                return;
            }
            if (userID == -1)
                return; // means they canceled...
        }

        final Report report = new MoneyGramReport(userID);
        try{
        currentTransaction.setUserID(userID);
        }catch(Exception e){

        }
        try {
            (new DateTimeEntryDialog(report, true, true, true, false, Messages.getString("DeltaworksMainPanel.1968"), true, true, false)).show(); 
        }
        catch (Throwable t) {
            Debug.printStackTrace(t);
        }
    }



    public void processingFeeReport() {
    	int userID = -1;
        if (isHostCommInProgress())
           return;
        setCurrentTransaction(SimplePMTransaction.PROCESSING_FEE_REPORT_OPTIONS);
        String loginType = getGlobalLoginTypeRequired();
        if (! loginType.equals("N")) {
            boolean nameRequired = loginType.equals("Y");
            try{
            userID = getUserAuthority("TOTALS_REPORT", nameRequired); 
            }catch(Exception e){
                Debug.printStackTrace(e);
            }
            if (userID == 0) {
                showNotAuthorizedDialog();
                return;
            }
            if (userID == -1)
                return; // means they canceled...
        }

        final Report report = new ProcessingFeeReport(userID);
        try{
        currentTransaction.setUserID(userID);
        }catch(Exception e){

        }
        try {
            (new DateTimeEntryDialog(report, true, true, true, false, Messages.getString("DeltaworksMainPanel.1977"), false, false, false)).show(); 
        }
        catch (Throwable t) {
            Debug.printStackTrace(t);
        }
    }

    public void ctrReport() {
        try {
            ComplianceDialog dialog = new ComplianceDialog(CustomerComplianceTypeCodeType.CTR);
            Dialogs.showPanel(dialog);
        }
        catch (Exception e) {
            Debug.printStackTrace(e);
        }
    }

    /**
     * Get the login type required for non financial transactions
     *   N -- No login required
     *   P -- Pin Required
     *   Y -- Name and Pin required.
     */
    public String getGlobalLoginTypeRequired() {
        return UnitProfile.getInstance().get("GLOBAL_NAME_PIN_REQ", "P");
    }

    /**
     * Returns true if dispenser transactions should be allowed.
     * The implementation is based on whether a valid-looking account
     * number appears in the profile item MO_ACCOUNT. In demo mode,
     * dispenser transactions are allowed regardless.
     */
    private boolean dispenserTransactionsAllowed() {
        String moAccount = UnitProfile.getInstance().get("MO_ACCOUNT", "");

    	if (UnitProfile.getInstance().isDemoMode())
    		// Compare with the demo account number to see if you have a dispenser
        	if (moAccount.equals("00000000000000"))
        		return true;
        	else
        		return false;

        boolean retval = false;
        // A valid-looking account number is one with at least one
        // digit in the range 1-9; in other words, something other
        // than empty string, all-spaces, or all-zeros.
        for (int i = 0; i < moAccount.length(); i++){
            char c = moAccount.charAt(i);
            if (c >= '1' && c <='9'){
                retval = true;
                break;
            }
        }
        return retval;
    }

//    /**
//     * Returns true if card transactions should be allowed.
//     */
//    private boolean cardTransactionsAllowed() {
//        if (UnitProfile.getInstance().isDemoMode())
//            return true;
//
//        boolean cardLoad = false;
//        Profile profileCL = UnitProfile.getInstance().getProfileInstance();
//        ProfileItem itemCL = profileCL.find("PRODUCT", MoneyGramCardTransaction.CARD_LOAD_SEQ); 
//        cardLoad = itemCL.getStatus().equalsIgnoreCase("A"); 
//
//        boolean cardPurchase = false;
//        Profile profileCP = UnitProfile.getInstance().getProfileInstance();
//        ProfileItem itemCP = profileCP.find("PRODUCT", MoneyGramCardTransaction.CARD_PURCHASE_SEQ); 
//        cardPurchase = itemCP.getStatus().equalsIgnoreCase("A"); 
//        return (cardLoad || cardPurchase);
//    }
//
//
    /**
     * Implementation of PageNameInterface. Put the screen name in the title
     *   bar.
     */
    @Override
	public void setCurrentPageName(String pageName) {
        StringBuffer buffer = new StringBuffer(40);
        buffer.append("DeltaWorks "); 
        buffer.append(currentTransaction.getTransactionName());
        buffer.append(" ("); 
        buffer.append(pageName);
        buffer.append(")"); 
        setTitle(buffer.toString());
    }


    /**
     * Return the singleton instance. This is a temporary hack so ItemForwarder
     * can do a check-in. Before using this method, see if there's a better way.
     */
    public static DeltaworksMainPanel getMainPanel() {
        return instance;
    }
    @Override
	public void setDeauthMessage(boolean deauthMessage) {
        this.deAuthMessage = deauthMessage;
    }


    @Override
	public void setDeActivateMessage(boolean deActivateMessage) {
        this.deActivateMessage = deActivateMessage;
    }

    public static void main(String args[]) {
        DeltaworksStartup.main(args);
    }

    private static String getScheduledDialogTitle() {
        return Messages.getString("DeltaworksMainPanel.1940");
    }

    private static String getManualDialogTitle() {
        return Messages.getString("DeltaworksMainPanel.1941");
    }

    /**
     * Factory method. Returns a new instance of DeltaworkMainPanel.
     * Sets this class's instance variable, and registers with
     * MainPanelDelegate.
     */
    public static DeltaworksMainPanel create(JFrame f) {
        // The constructor also sets instance, not sure why.
        instance = new DeltaworksMainPanel(f);
        MainPanelDelegate.setDelegate(instance);
        return instance;
    }

    /**
     * Called by Messages.setCurrentLocale() to indicate that the display
     * language has changed.
     */
    @Override
	public void languageChanged() {
        // This code used to be in Messages.setCurrentLocale(). It was
        // moved here to remove unnecessary dependancies.
        if (!CommunicationDialog.getInstance().isShowing())
            CommunicationDialog.refreshDialog();
        DWValues.refreshIdLists();
        Dialogs.refreshAboutDialog();
        Guidelines.nullInstance();
        Restrictions.nullInstance();
    }

    /**
     * Called (indirectly) by Period to indicate period changed.
     * Sets the verifyRequired status on the dispenser (if there is one).
     */
    @Override
	public void periodChanged() {
        // Original code from Period.java:
        //        DispenserInterface dispenser = DispenserFactory.getDispenser(DISPENSER_1);
        //        if (dispenser != null)
        //            dispenser.verifyRequired();

        DispenserFactory.verifyRequired();
        updatePeriodLabel();
    }

	/**
	 * @return Returns the currentTransaction.
	 */
	public ClientTransaction getCurrentTransaction() {
		return currentTransaction;
	}
   @Override
public void changeToSuperAgent(final boolean idleTimeOutLong){
     final SubagentInfo newAgent = UnitProfile.getInstance().getAgentInfo();
    if(newAgent!=null){
       final MessageFacadeParam parameters =
		 new MessageFacadeParam(MessageFacadeParam.REAL_TIME,
                               MessageFacadeParam.USER_CANCEL);

    final CommCompleteInterface cci = new CommCompleteInterface() {
        @Override
		public void commComplete(int commTag, Object returnValue) {


             if (returnValue instanceof String)

            {
                 UnitProfile.getInstance().setNewProfile((String)returnValue);
                   if(idleTimeOutLong)
                   	MainPanelDelegate.refreshMainPanel();
                   else{
         		      MainPanelDelegate.tryExit(DeltaworksMainPanel.NORMAL_EXIT);
                   }
            }

        }
    };

	final MessageLogic logic = new MessageLogic() {
		String newProfile = ""; 
		String agentID = newAgent.getAgentID()+"";
        String sequenceNumber = newAgent.getSequenceNumber()+"";
        String unitProfileID = UnitProfile.getInstance().getUnitProfileID()+"";
        String legacyAgentID=newAgent.getLegacyAgentID();

        @Override
		public Object run() {
			try {
                 newProfile =
					MessageFacade.getInstance().getSubagentProfile(
						parameters,
						Integer.parseInt(unitProfileID),
						agentID,
                        sequenceNumber);

                if(null != newProfile){
                    if(!StringUtility.getNonNullString(legacyAgentID).equals(""))
                        UnitProfile.getInstance().setTakeOverSubagentprofile(true);
                    else
                        UnitProfile.getInstance().setTakeOverSubagentprofile(false);
                    return newProfile;

                }
                else
                    return Boolean.FALSE;
             } catch (MessageFacadeError mfe) {
                if (mfe.hasErrorCode(MessageFacadeParam.USER_CANCEL)) {
                    return Boolean.FALSE;
                }
                else
				return Boolean.FALSE;
			} catch (Exception e) {
				return Boolean.FALSE;
			}
		}
	};
	MessageFacade.run(logic, cci, 3, Boolean.TRUE);

  }
 }
	/**
	 * Query RTS for list of agents and to get the SA info to go to the SA blank Screen .
	 * in case we dont have SA info.
	 */
	@Override
	public void getAgentList(final boolean idleTimeOutLong) {

       final MessageFacadeParam parameters =
			new MessageFacadeParam(MessageFacadeParam.REAL_TIME);
       final CommCompleteInterface cci = new CommCompleteInterface() {
        @Override
		public void commComplete(int commTag, Object returnValue) {
             if (returnValue instanceof SubagentInfo[])
               {
             	storeSuperAgentInUnitProfile(agents);
             	changeToSuperAgent(idleTimeOutLong);
            }
        }
    };
		final MessageLogic logic = new MessageLogic() {

			@Override
			public Object run() {
				try {
					agents =
						MessageFacade.getInstance().getAgentList(
							cci,
							parameters);
                   if((agents == null))
                       return Boolean.FALSE;
                   else{

                       return agents;
                   }

				} catch (MessageFacadeError mfe) {
					return Boolean.FALSE;
				} catch (Exception e) {
					return Boolean.FALSE;
				}
			}
		};
		MessageFacade.run(logic, cci, 0, Boolean.TRUE);


	}

	public void storeSuperAgentInUnitProfile(SubagentInfo[] agent) {
		if(agent!=null && agent.length>0)
		{
		 if(StringUtility.getNonNullString(agent[0].getLegacyAgentID()).equals(""))
		 	UnitProfile.getInstance().setAgentInfo(agent[0]);
		}
	}
	 public boolean isSubAgentTakeOverUserID(int userID){
   	 if(UnitProfile.getInstance().getSubAgentTakeOverUserID() == userID)
   	 	return true;
		else
			return false;
   }

	public boolean isTransactionStarting() {
		return transactionStarting;
	}

//	public void setTransactionStarting(boolean transactionStarting) {
//		this.transactionStarting = transactionStarting;
//	}
//
	private boolean isPinRequired(int docSeq) {
		Profile profile = UnitProfile.getInstance().getProfileInstance();
		ProfileItem item = profile.find("PRODUCT", docSeq); 
		String requiredValue = item.get("NAME_PIN_REQUIRED").stringValue(); 

		if (requiredValue.equals("N")) { 
			return false;
		} else
			return true;
	}

	/**
	 * @return the parentFrame
	 */
	public JFrame getParentFrame() {
		return parentFrame;
	}

    public void junitTransmitProcess() {
        MessageFacadeParam parameters = new MessageFacadeParam(MessageFacadeParam.NON_REAL_TIME, MessageFacadeParam.AUTO_RETRY);
        transmitProcess(parameters, false, "JUnit Test", CountryInfo.MANUAL_TRANSMIT_RETERIVAL);
    }

	public void doClickMenu(String comName)
	{
		if("sendMoneyButton".equals(comName))
		{
			mainMenuPage.sendMoneyButtonAction();
		}
	}
	
	public static Component getFocusOwner() {
		Component focusOwner = null;
		if ((getMainPanel() != null) && (DeltaworksMainPanel.getMainPanel().getParentFrame() != null)) {
			focusOwner = getMainPanel().getParentFrame().getFocusOwner();
		}
		return focusOwner;
	}
}


/**
 * FileMaker creates file on a disk. Used by ftpFile() to allow it
 * to create either diag or report file before it ftp it.
 */
interface FileMaker {
    public void makeFile();
}
