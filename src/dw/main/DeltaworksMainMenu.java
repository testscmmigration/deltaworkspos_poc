package dw.main;


import java.awt.Color;
import java.awt.Component;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.HashMap;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.MenuElement;
import javax.swing.MenuSelectionManager;

import dw.framework.FlowPage;
import dw.framework.KeyMapManager;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.profile.UnitProfile;
import dw.utility.Debug;

public class DeltaworksMainMenu extends FlowPage {
	private static final long serialVersionUID = 1L;
	
	private HashMap<String, Boolean> buttonMap = new HashMap<String, Boolean>();
    public static final int MONEY_ORDER = 0;
    public static final int VENDOR_PAYMENT = 1;
    public static final int MONEY_GRAM_SEND = 2;
    public static final int MONEY_GRAM_RECEIVE = 3;
    public static final int BILL_PAYMENT = 4;
    public static final int TRANSACTION_QUOTE = 5;
    public static final int DIRECTORY_AGENTS = 6;
    public static final int DIRECTORY_BILLERS = 7;
    public static final int LANGUAGE_TOGGLE = 8;
    public static final int CARD = 9;
    public static final int ADD_MINUTES = 10;
    public static final int ACCOUNT_DEPOSIT_PARTNERS = 11;
    public static final int BROADCAST_MESSAGES = 12;
    public static final int MONEY_GRAM_SEND_TO_ACCOUNT = 13;
    public static final int FORM_FREE_ENTRY = 14;
    
    public static final int CARD_RELOAD = 15;
    public static final int CARD_PURCHASE = 16;
    public static final int REPRINT_RECEIPT = 17;
    public static final int TRANSACTION_STATUS = 18;
    public static final int REGISTER_MFA_TOKEN = 20;
    public static final int SEND_REVERSAL = 21;
        
    public static final int NUMBER_BUTTONS = 15;

	private static final int ENGLISH = 0;
	private static final int SPANISH = 1;
	private static final int FRENCH = 2;
	private static final int GERMAN = 3;
	private static final int CHINESE = 4;
	private static final int POLISH = 5;
	private static final int RUSSIAN = 6;
	private static final int PORTUGUESE = 7;

    static public final int KEY_MAPPINGS[][] = new int[][] {
//		 English        Spanish                French                 German                 Chinese                Polish                   Russian               Portuguese
   	{KeyEvent.VK_M, 	KeyEvent.VK_G,         KeyEvent.VK_M,         KeyEvent.VK_M,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_Y,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_O}, // Money Orders
   	{KeyEvent.VK_V,		KeyEvent.VK_V,         KeyEvent.VK_F,         KeyEvent.VK_V,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_V,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_F}, // Vendor Payments
   	{KeyEvent.VK_S, 	KeyEvent.VK_E,         KeyEvent.VK_E,         KeyEvent.VK_G,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_W,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_E}, // Send
   	{KeyEvent.VK_R, 	KeyEvent.VK_R,         KeyEvent.VK_R,         KeyEvent.VK_E,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_O,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_R}, // Receive
   	{KeyEvent.VK_P, 	KeyEvent.VK_P,         KeyEvent.VK_P,         KeyEvent.VK_R,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_Z,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_P}, // Bill Payments
   	{KeyEvent.VK_Q, 	KeyEvent.VK_C,         KeyEvent.VK_D,         KeyEvent.VK_T,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_T,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_N}, // Transaction Quotes
   	{KeyEvent.VK_A, 	KeyEvent.VK_A,         KeyEvent.VK_A,         KeyEvent.VK_A,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_A,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_G}, // Directory of Agents
   	{KeyEvent.VK_B, 	KeyEvent.VK_F,         KeyEvent.VK_N,         KeyEvent.VK_B,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_P,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_B}, // Directory of Billers
   	{KeyEvent.VK_S, 	KeyEvent.VK_C,         KeyEvent.VK_C,         KeyEvent.VK_U,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_S}, // Language Toggle
   	{KeyEvent.VK_D, 	KeyEvent.VK_T,         KeyEvent.VK_T,         KeyEvent.VK_K,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_K,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_C}, // Add to Card
   	{KeyEvent.VK_N, 	KeyEvent.VK_M,         KeyEvent.VK_J,         KeyEvent.VK_N,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_M,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_A}, // Add Minutes
   	{KeyEvent.VK_K, 	KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_D}, // Account Deposit Banks
   	{KeyEvent.VK_E, 	KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_M}, // Broadcast Messages
   	{KeyEvent.VK_O, 	KeyEvent.VK_N, 		   KeyEvent.VK_V		, KeyEvent.VK_S, 		 KeyEvent.VK_UNDEFINED, KeyEvent.VK_J,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_I},  // Send Money To Account
	{KeyEvent.VK_T, 	KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_T}	// Free Form Entry
   };

    private static boolean bStarted = false;
    
    private int selection = 0;
    private boolean firstTime = true;
    private Component bigButton[];
    private int language ;
    private int altLanguage ;

    private static volatile DeltaworksMainMenu instance;
    private boolean starting = false;
    
    public DeltaworksMainMenu(String xmlFileName, String name,
                        String pageCode, PageFlowButtons buttons) {
       
        super(xmlFileName, name, pageCode, buttons);
        
        bigButton = new Component[NUMBER_BUTTONS];

        bigButton[MONEY_ORDER] = getComponent("moneyOrderButton");	
        bigButton[VENDOR_PAYMENT] = getComponent("vendorPaymentButton");	
        bigButton[MONEY_GRAM_SEND] = getComponent("sendMoneyButton");	
        bigButton[MONEY_GRAM_RECEIVE] = getComponent("receiveMoneyButton");	
        bigButton[BILL_PAYMENT] = getComponent("billPaymentButton");	
        bigButton[TRANSACTION_QUOTE] = getComponent("hotQuote");	
        bigButton[DIRECTORY_AGENTS] = getComponent("hotAgents");	
        bigButton[DIRECTORY_BILLERS] = getComponent("hotBillers");	
        bigButton[LANGUAGE_TOGGLE] = getComponent("languageToggle");      
        bigButton[CARD] = getComponent("cardButton");      
        bigButton[ADD_MINUTES] = getComponent("addMinutesButton");	
        bigButton[ACCOUNT_DEPOSIT_PARTNERS] = getComponent("hotAccountDepositPartners");      
        bigButton[BROADCAST_MESSAGES] = getComponent("hotBroadcastMessages");	
        bigButton[MONEY_GRAM_SEND_TO_ACCOUNT] = getComponent("sendMoneyToAccountButton");	
        bigButton[FORM_FREE_ENTRY] = getComponent("formFreeEntryButton");	
        setButtonColor();
        
        setInstance(this);
    }
    
    private static void setInstance(DeltaworksMainMenu i) {
    	instance = i;
    }
    
    private int getLanguage(String language) {
    	if (language.equals("en")) {
    		return ENGLISH;
    	} else if (language.equals("es")) {
    		return SPANISH;
    	} else if (language.equals("fr")) {
    		return FRENCH;
    	} else if (language.equals("de")) {
    		return GERMAN;
    	} else if (language.equals("cn")) {
    		return CHINESE;
    	} else if (language.equals("pl")) {
			return POLISH;
		} else if (language.equals("ru")) {
			return RUSSIAN;
		} else if (language.equals("pt")) {
			return PORTUGUESE;
		} else {
			return ENGLISH;
		}
    }
    
    private String getLanguage(int language) {
    	switch (language) {
    		case ENGLISH: return "en"; 
    		case SPANISH: return "es"; 
    		case FRENCH:  return "fr";  
    		case GERMAN:  return "de";  
    		case CHINESE: return "cn"; 
    		case POLISH:  return "pl";
    		case RUSSIAN: return "ru";
    		case PORTUGUESE: return "pt";  
    		default:	  return "en"; 
    	}
    }
    
    public void setButtonColor() {
    	language = getLanguage(UnitProfile.getInstance().get("LANGUAGE", "en"));
    	
        bigButton[MONEY_ORDER].setBackground(Color.WHITE);
        bigButton[VENDOR_PAYMENT].setBackground(Color.WHITE);
        bigButton[MONEY_GRAM_SEND].setBackground(Color.WHITE);
        bigButton[MONEY_GRAM_RECEIVE].setBackground(Color.WHITE);
        bigButton[BILL_PAYMENT].setBackground(Color.WHITE);
        bigButton[CARD].setBackground(Color.WHITE);
        bigButton[ADD_MINUTES].setBackground(Color.WHITE);
        bigButton[MONEY_GRAM_SEND_TO_ACCOUNT].setBackground(Color.WHITE);
        bigButton[FORM_FREE_ENTRY].setBackground(Color.WHITE);
    }

    public int getSelection() {
        return selection;
    }
   
    @Override
	public void start(int direction) {
    //Renuka - DWNG - Set the text for the language toggle button on the basis of  
    // LANGUAGE and ALT_LANGUAGE profile options.
    //Note: At present, there are no hot keys for Chinese.
    	
        language = getLanguage(UnitProfile.getInstance().get("LANGUAGE", "en"));
        altLanguage = getLanguage(UnitProfile.getInstance().getAlternateLanguage());
    	
        
        addActionListener("moneyOrderButton", this);	
        addActionListener("vendorPaymentButton", this);	
        addActionListener("sendMoneyButton", this);	
        addActionListener("receiveMoneyButton", this);	
        addActionListener("billPaymentButton", this);	
        addActionListener("hotQuote", this);    
        addActionListener("hotAgents", this);	
        addActionListener("hotBillers", this);	
        addActionListener("languageToggle", this);  
        addActionListener("cardButton", this);  
        addActionListener("addMinutesButton", this);	
		addActionListener("hotAccountDepositPartners", this); 
		addActionListener("hotBroadcastMessages", this); 
		addActionListener("sendMoneyToAccountButton", this); 
		addActionListener("formFreeEntryButton", this); 
		
		for (int button = 0; button < NUMBER_BUTTONS; button++) {
			resetMapping(button);
		}
		
        if (!firstTime) {
            // set focus on the previously-selected button
            bigButton[getSelection()].requestFocus();
        }
        removeMappingForInvisibleButtons();
        bStarted = true;
    }
    
    private void removeMappingForInvisibleButtons(){
    	for (int i = 0; i < NUMBER_BUTTONS; i++) {
    	  	if (i == LANGUAGE_TOGGLE) {
    	  		continue;
    	  	}
    	  	if (!bigButton[i].isVisible()) {           
    	  		KeyMapManager.unregisterComponent( bigButton[i]);    	           
    	        buttonMap.put(bigButton[i].getName(),Boolean.FALSE);
    	  	} else {
    	  		if (buttonMap.containsKey(bigButton[i].getName())) {   
    	  			resetMapping(i);
    	  			buttonMap.remove(bigButton[i].getName());    	      
    	  		}
    	  	}
    	}
    }
    
    public void setButtonVisible(int transaction,boolean visible){
        bigButton[transaction].setVisible(visible);
        if(!visible)
        {           
            KeyMapManager.unregisterComponent( bigButton[transaction]);
            buttonMap.put(bigButton[transaction].getName(),Boolean.FALSE);
        } else {
           if( buttonMap.containsKey(bigButton[transaction].getName()) )  {   
            resetMapping(transaction);
            buttonMap.remove(bigButton[transaction].getName());
           }
        }
    }

    public void setSelectionEnabled(int transaction, boolean enabled) {
        if(bigButton[transaction].isVisible()){
            bigButton[transaction].setEnabled(enabled);
            if(enabled)
                bigButton[transaction].setForeground(Color.black);
            else 
                bigButton[transaction].setForeground(Color.gray);
         }
    }     
    
    @Override
	public void finish(int direction) {
        PageNotification.notifyExitListeners(this, direction);
    }

    private synchronized void tryFinish(int selection) {
        // Bug 982 Action events shouldn't come here if the menu bar is
        // active (has keyboard focus). But if they are routed here by
        // accident, find the menu selection and activate it instead of
        // letting the button activate.
        MenuSelectionManager man = MenuSelectionManager.defaultManager();
        // find the path, if any, of a dropped down menu
        MenuElement[] elements = man.getSelectedPath();
        if (elements.length > 0) {
            // there is a menu down, so activate the option selected
            MenuElement item = elements[elements.length - 1];
            // force a click on the menu item
            if (item instanceof AbstractButton) {
                // get rid of the menu
                man.clearSelectedPath();
                ((AbstractButton) item).doClick();
            }
            return;
        }

        starting = true;
        this.selection = selection;
        firstTime = false;
        try {
            finish(PageExitListener.NEXT);
        }
        catch (Exception e) {
            Debug.println("Uncaught exception during transaction : ");	
            Debug.printStackTrace(e);
            // just to make sure we don't cripple the app from something
            //   unexpected happening
            finish(PageExitListener.CANCELED);
        }
        starting = false;
    }

    public void moneyOrderButtonAction() {
        if (starting)
            return;
        tryFinish(MONEY_ORDER);
    }

    public void sendMoneyButtonAction() {
        if (starting)
            return;
        tryFinish(MONEY_GRAM_SEND);
    }
    
    public void sendMoneyToAccountButtonAction() {
        if (starting)
            return;
        tryFinish(MONEY_GRAM_SEND_TO_ACCOUNT);
    }
    
    public void receiveMoneyButtonAction() {
        if (starting)
            return;
        tryFinish(MONEY_GRAM_RECEIVE);
    }

    public void vendorPaymentButtonAction() {
        if (starting)
            return;
        tryFinish(VENDOR_PAYMENT);
    }

    public void billPaymentButtonAction() {
        if (starting)
            return;
        tryFinish(BILL_PAYMENT);
    }
    
    public void hotQuoteAction() {
        if (starting)
            return;
        tryFinish(TRANSACTION_QUOTE);
    }

    public void hotAgentsAction() {
        if (starting)
            return;
        tryFinish(DIRECTORY_AGENTS);
    }

    public void hotBillersAction() {
        if (starting)
            return;
        tryFinish(DIRECTORY_BILLERS);
    }
    
    public void hotAccountDepositPartnersAction() {
        if (starting) {
            return;
        }
        tryFinish(MONEY_GRAM_SEND_TO_ACCOUNT);
    }
    
    public void hotBroadcastMessagesAction() {
        if (starting) {
            return;
        }
        tryFinish(BROADCAST_MESSAGES);
    }
    
    public void cardButtonAction() {
        if (starting)
            return;
        tryFinish(CARD);
    }
 
    
    public void purchaseButtonAction() {
        if (starting)
            return;
        tryFinish(CARD_PURCHASE);
    }

    public void reloadButtonAction() {
        if (starting)
            return;
        tryFinish(CARD_RELOAD);
    }

    public void addMinutesButtonAction() {
        if (starting)
            return;
        tryFinish(ADD_MINUTES);
    }
    
    public void formFreeEntryButtonAction() {
        if (starting) {
            return;
        }
        tryFinish(FORM_FREE_ENTRY);
    }
    
    /**
     * Language toggle action: query unit proile for current value for Language
     * and ALT_LANGUAGE profile item. It then should toggle between English and the langauge 
     * specified by ALT_LANGUAGE.
     * then refresh the comm dialog and mainPanel.
     */
    public void languageToggleAction() {
		if (language == ENGLISH) {
			DeltaworksMainPanel.getMainPanel().changeLanguages(getLanguage(altLanguage), "en");
		} else {
			DeltaworksMainPanel.getMainPanel().changeLanguages("en", getLanguage(language));
        }
    }

    /**
     * This method resets the button when it is reauthed 
     * @param transaction
     */
    
    public void resetMapping(int buttonIndex) {
    	Component buttonComponent = bigButton[buttonIndex];
   		int key = (buttonIndex != LANGUAGE_TOGGLE) ? KEY_MAPPINGS[buttonIndex][language] : KEY_MAPPINGS[buttonIndex][altLanguage];
		if (key != KeyEvent.VK_UNDEFINED) {
			KeyMapManager.mapComponent(this, key, InputEvent.ALT_MASK, buttonComponent);
		}
       	if (buttonIndex == LANGUAGE_TOGGLE) {
        	switch (altLanguage) {
	    		case ENGLISH: ((AbstractButton)buttonComponent).setText(Messages.getString("DeltaworksMainMenu.english")); break;
	    		case SPANISH: ((AbstractButton)buttonComponent).setText(Messages.getString("DeltaworksMainMenu.spanish")); break;
	    		case FRENCH:  ((AbstractButton)buttonComponent).setText(Messages.getString("DeltaworksMainMenu.french"));  break;
	    		case GERMAN:  ((AbstractButton)buttonComponent).setText(Messages.getString("DeltaworksMainMenu.german"));  break;
	    		case CHINESE: ((AbstractButton)buttonComponent).setText(Messages.getString("DeltaworksMainMenu.chinese")); break;
	    		case POLISH:  ((AbstractButton)buttonComponent).setText(Messages.getString("DeltaworksMainMenu.polish"));  break;
	    		case RUSSIAN: ((AbstractButton)buttonComponent).setText(Messages.getString("DeltaworksMainMenu.russian")); break;
	    		case PORTUGUESE: ((AbstractButton)buttonComponent).setText(Messages.getString("DeltaworksMainMenu.portuguese")); break;
        	}
    	}
    }
 
	/**
	 * @return boolean, has main menu started
	 */
	public static boolean isStarted() {
		return bStarted;
	}
	
	public JButton getBroadcastMessageButton() {
		return (JButton) bigButton[BROADCAST_MESSAGES];
	}
	
	static public DeltaworksMainMenu getInstance() {
		return instance;
	}
}
