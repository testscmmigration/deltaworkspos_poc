/*
 * MainPanelHooks.java
 * 
 * $Revision$
 *
 * Copyright (c) 2005 MoneyGram International
 */
package dw.main;

/**
 * Methods implemented by DeltaworkMainPanel for use by other classes.
 * Further refactoring may be in order, but this at least gets some 
 * unexpected dependencies out into the open.
 *
 * @author Geoff Atkin
 */
public interface MainPanelHooks {
    public void executeIsiBackCommand();
    public void executeIsiFrontCommand();
    public boolean getIsiAllowDataCollection();
    public boolean getIsiRequireRcptTextData();
    public boolean getIsiAppendMode();
    public boolean getIsiExeCommand();
    public boolean getIsiExe2Command();
    public String getIsiExeProgramArguments();
	public String getIsiExe2ProgramArguments();
    public boolean getIsiRpCommandValid();
    public boolean getIsiTransactionInProgress();
    public boolean getIsiFrontCommandOccurred();
    public boolean getIsiSkipRecord();
    public boolean getIsiControlledLaunchCardReload();
    public boolean getIsiControlledLaunchCardPurchase();
    public void initializeIsiFields();
    public void languageChanged();
    public void periodChanged();
    public void refreshMainPanel();
    public void refreshToggleButton();
    public void setConnectivityChange(boolean b);
    public void setDeauthMessage(boolean deauthMessage);
    public void setDeActivateMessage(boolean deActivatedMessage);
    public void setIsiAllowDataCollection(boolean b);
    public void setIsiRequireRcptTextData(boolean b);
    public void setIsiAppendMode(boolean b);
    public void setIsiExeCommand(boolean b);
    public void setIsiExe2Command(boolean b);
    public void setIsiExeProgramArguments(String string);
    public void setIsiExe2ProgramArguments(String string);
    public void setIsiRpCommandValid(boolean b);
    public void setIsiTransactionInProgress(boolean b);
    public void setIsiFrontCommandOccurred(boolean b);
    public void setIsiSkipRecord(boolean b);
    public void setPosAuthorization(boolean authorized);
    public void setTransactionEnabled();
    public void setupCancelISI(String string);
    public void setupNonFinancialISI();
    public void setupErrorISI(String string);
    public void setIsiControlledLaunchCardReload(boolean b);
    public void setIsiControlledLaunchCardPurchase(boolean b);
    public void tryExit(int code);
    public void changeToSuperAgent(boolean idleTimeOutLong);
//    public void getAgentList();
    public void getAgentList(boolean idleTimeOutLong);
    public void setupControlledLaunchISI(String string);
    public boolean isValidateNfLaunchPage();
    public void executeIsiProgram(String programName);

    // Non Financial ISI functions.
    public void setIsiNfCommand(boolean b);
    public boolean getIsiNfCommand();
    public void setIsiNfProgramArguments(String string);
    public String getIsiNfProgramArguments();

}
