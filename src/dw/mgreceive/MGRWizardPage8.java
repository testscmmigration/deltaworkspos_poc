package dw.mgreceive;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.moneygram.agentconnect.SessionType;
import com.moneygram.agentconnect.TransactionStatusType;

import dw.comm.MessageFacadeParam;
import dw.dialogs.Dialogs;
import dw.dispenser.DispenserFactory;
import dw.dispenser.DispenserInterface;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.ClientTransaction;
import dw.framework.DataCollectionSet;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.FieldKey;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.mgreversal.SendReversalTransaction;
import dw.mgsend.MoneyGramClientTransaction;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.SendReversalType;
import dw.model.adapters.CountryInfo.Country;
import dw.pinpad.Pinpad;
import dw.profile.UnitProfile;
import dw.utility.DWValues;
import dw.utility.Debug;

/**
 * Page 8 of the MoneyGram Receive Wizard. This page displays the details of the
 *   transaction for verification before printing money orders.
 */
public class MGRWizardPage8 extends MoneyGramReceiveFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

    private JLabel amt1Label;
    private JLabel amt2Label;
    private JPanel statusPanel;
    private JLabel incorrectInformationLabel;
    private JPanel purposePanel;
    private JLabel purposeLabel;
    private JScrollPane sp1;
    private JScrollPane sp2;
    private JPanel panel2;
    private boolean reversalTran = false;
    private boolean cancel = false;
	private List<DWTextPane> textPanes;
	private int tagWidth;

    public MGRWizardPage8(MoneyGramReceiveTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "moneygramreceive/MGRWizardPage8.xml", name, pageCode, buttons);	
        init();
        reversalTran = false;
		cancel = false;
    }

    public MGRWizardPage8(SendReversalTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "moneygramreceive/MGRWizardPage8.xml", name, pageCode, buttons); 
        init();
        this.transaction = tran;
        reversalTran = true;
		cancel = tran.isCancelAllowed();
    }
    
    private void init() {
		textPanes = new ArrayList<DWTextPane>();
        amt1Label = (JLabel) getComponent("amount1TagLabel");	
        amt2Label = (JLabel) getComponent("amount2TagLabel");	
        statusPanel = (JPanel) getComponent("statusPanel");	

        incorrectInformationLabel = (JLabel) getComponent("incorrectInformationLabel");

        purposePanel = (JPanel) getComponent("purposePanel");
        purposeLabel = (JLabel) getComponent("purposeLabel");
        
        ((DWTextPane) getComponent("senderNamePane")).addListener(this, this);

		sp1 = (JScrollPane) getComponent("scrollPane1");
		sp2 = (JScrollPane) getComponent("scrollPane2");
		panel2 = (JPanel) this.getComponent("scrollee");
        
        
        this.addComponentListener(new ComponentAdapter() {
        	@Override
			public void componentResized(ComponentEvent e) {
        		scrollpaneResized(sp1, sp2, panel2);
        	}
        });
    }

	@Override
	public void dwTextPaneResized() {
		int w = FIELD_TOTAL_WIDTH - tagWidth;
		for (DWTextPane pane : textPanes) {
			pane.setWidth(w);
		}
		((DWTextPane) getComponent("senderNamePane")).setListenerEnabled(false);
	}

	@Override
    public void start(int direction) {
        flowButtons.reset();
        flowButtons.setVisible("expert", false);	
        String buttonText = reversalTran ? Messages.getString("SRTWizardPage2.1277") : Messages.getString("MGRWizardPage8.printText");
        flowButtons.setAlternateText("next", buttonText);
        flowButtons.setAlternateKeyMapping("next", KeyEvent.VK_F5, 0);	
        flowButtons.setSelected("next");	

    	if (transaction.isFormFreeTransaction()) {
    		incorrectInformationLabel.setText(Messages.getString("MGRWizardPage8.956b"));
    	} else {
    		incorrectInformationLabel.setText(Messages.getString("MGRWizardPage8.956"));
    	}
    	
    	DataCollectionSet data = transaction instanceof SendReversalTransaction ? transaction.getDataCollectionData() : transaction.getSearchProfilesData();
    	
    	List<JLabel> tags = new ArrayList<JLabel>();
    	
    	// Sender Name
    	
    	String firstName = data.getValue(FieldKey.SENDER_FIRSTNAME_KEY);
    	String middleName = data.getValue(FieldKey.SENDER_MIDDLENAME_KEY);
    	String lastName1 = data.getValue(FieldKey.SENDER_LASTNAME_KEY);
    	String lastName2 = data.getValue(FieldKey.SENDER_LASTNAME2_KEY);
    	String senderName = DWValues.formatName(firstName, middleName, lastName1, lastName2);
		this.displayPaneItem("senderName", senderName, tags, textPanes);
		
		// Sender Address

    	String address1 = data.getValue(FieldKey.SENDER_ADDRESS_KEY);
    	String address2 = data.getValue(FieldKey.SENDER_ADDRESS2_KEY);
    	String address3 = data.getValue(FieldKey.SENDER_ADDRESS3_KEY);
    	String address4 = data.getValue(FieldKey.SENDER_ADDRESS4_KEY);
    	String address = DWValues.getAddress(address1, address2, address3, address4);
		this.displayPaneItem("senderAddress", address, tags, textPanes);
        
        // Sender City

		String tag = transaction.getTransactionLookupInfoLabel(FieldKey.SENDER_CITY_KEY.getInfoKey());
        String senderCity = data.getValue(FieldKey.SENDER_CITY_KEY);
		this.displayLabelItem("senderCity", tag, senderCity, tags);
		
		// Sender State

		tag = transaction.getTransactionLookupInfoLabel(FieldKey.SENDER_COUNTRY_SUBDIVISIONCODE_KEY.getInfoKey());
        String senderState = data.getValue(FieldKey.SENDER_COUNTRY_SUBDIVISIONCODE_KEY);
		this.displayLabelItem("senderState", tag, senderState, tags);

		// Sender Postal Code
		
		tag = transaction.getTransactionLookupInfoLabel(FieldKey.SENDER_POSTALCODE_KEY.getInfoKey());
        String senderZipCode = data.getValue(FieldKey.SENDER_POSTALCODE_KEY);
		this.displayLabelItem("senderZipCode", tag, senderZipCode, tags);
		
		// Sender Country
        
		tag = transaction.getTransactionLookupInfoLabel(FieldKey.SENDER_COUNTRY_KEY.getInfoKey());
		String countryCode = data.getValue(FieldKey.SENDER_COUNTRY_KEY);
		Country country = CountryInfo.getCountry(countryCode);
		this.displayLabelItem("senderCountry", tag, country != null ? country.getPrettyName() : null, tags);
		
		// Sender Phone Number
		
		tag = transaction.getTransactionLookupInfoLabel(FieldKey.SENDER_PRIMARYPHONE_KEY.getInfoKey());
		String phoneNumber = data.getValue(FieldKey.SENDER_PRIMARYPHONE_KEY);
		this.displayLabelItem("senderPhoneNumber", tag, phoneNumber, tags);

		// Send Country
		
		tag = transaction.getTransactionLookupInfoLabel(FieldKey.ORIGINATINGCOUNTRY_KEY.getInfoKey());
		countryCode = data.getValue(FieldKey.ORIGINATINGCOUNTRY_KEY);
		country = CountryInfo.getCountry(countryCode);
		this.displayLabelItem("sendCountry", tag, country != null ? country.getPrettyName() : null, tags);
		
		// Sent Date and Time
		
		tag = transaction.getTransactionLookupInfoLabel("dateTimeSent");
		String s = reversalTran ? transaction.getTransactionLookupResponse().getPayload().getValue().getTimeStamp().toString() : data.getCurrentValue("dateTimeSent");
        this.displayLabelItem("sent", tag, FormatSymbols.formatDateTimeForLocaleWithTimeZone(s), tags);

        // Receiver Name
        
    	firstName = data.getValue(FieldKey.RECEIVER_FIRSTNAME_KEY);
    	middleName = data.getValue(FieldKey.RECEIVER_MIDDLENAME_KEY);
    	lastName1 = data.getValue(FieldKey.RECEIVER_LASTNAME_KEY);
    	lastName2 = data.getValue(FieldKey.RECEIVER_LASTNAME2_KEY);
    	String receiverName = DWValues.formatName(firstName, middleName, lastName1, lastName2);
		this.displayPaneItem("receiverName", receiverName, tags, textPanes);
		
		BigDecimal amount1 = null;
		BigDecimal amount2 = null;
		BigDecimal amountFace = null;
		BigDecimal amountFee = null;
		BigDecimal amountTotal = null;
		String currency1 = null;
		String currency2 = null;
		String currencyFace = null;
		String currencyFee = null;
		String currencyTotal = null;

        if (! reversalTran) {
            String receiveCurrency = transaction.getCurrency();
    		if (transaction.getPayoutMethod() == MoneyGramReceiveTransaction.PAYOUT_CASH) {
    			amount1 = transaction.getAmount();
    			amount2 = BigDecimal.ZERO;
    		} else if (transaction.getCustPayoutAmount().compareTo(BigDecimal.ZERO) > 0) {
    			amt1Label.setText(Messages.getString("MGRWizardPage8.964a"));
    			amt2Label.setText(Messages.getString("MGRWizardPage8.965a"));
    			currency2 = Messages.getString("MGRWizardPage8.965a");
    			amount1 = transaction.getCustPayoutAmount();
    			amount2 = null;
    		} else {
    			amount1 = transaction.getAgentAmount();
    			amount2 = transaction.getCustomerAmount();
    		}
            currency1 = receiveCurrency;
            currency2 = receiveCurrency;
    		
            amountTotal = transaction.getAmount();
            currencyTotal = receiveCurrency;
        } else {
        	SendReversalTransaction rTransaction = (SendReversalTransaction)transaction;
        	SendReversalType reversalType = transaction.getSendReversalType();
        	
        	boolean payoutRequired = reversalType != null ? reversalType.equals(SendReversalType.R) : false;
        	
            if ((rTransaction.getRefundFee()) && (rTransaction.getPayoutMethod() == MoneyGramReceiveTransaction.PAYOUT_CASH)) {
                transaction.setAgentAmount(rTransaction.getAgentAmount().add(rTransaction.getFeeAmount()));
            }
        	
        	if (payoutRequired) {
        		amount1 = transaction.getAgentAmount();
        		currency1 = transaction.getSendAmountInfo().getSendCurrency();
    			
        		amount2 = transaction.getCustomerAmount();
        		currency2 = transaction.getSendAmountInfo().getSendCurrency();

        	} else {
        		amountFace = transaction.getSendAmountInfo() != null ? transaction.getSendAmountInfo().getSendAmount() : null;
        		currencyFace = transaction.getSendAmountInfo().getSendCurrency();

       			amountFee = transaction.getSendAmountInfo() != null ? transaction.getSendAmountInfo().getTotalSendFees() : null;
       			currencyFee = transaction.getSendAmountInfo().getSendCurrency();
        	}
    		amountTotal = transaction.getAmount();
    		currencyTotal = transaction.getSendAmountInfo().getSendCurrency();
        }
        
        transaction.setReceiveAmount1(amount1);
        transaction.setReceiveAmount2(amount2);
        transaction.setReceiveTotalAmount(amountTotal);
        transaction.setReceiveCurrency(currency1);
        
        this.displayMoney("amount1", amount1, currency1, tags);
        this.displayMoney("amount2", amount2, currency2, tags);
        this.displayMoney("face", amountFace, currencyFace, tags);
        this.displayMoney("fee", amountFee, currencyFee, tags);
        this.displayMoney("total", amountTotal, currencyTotal, tags);
        
        // Message
        
        StringBuffer sb = new StringBuffer();
    	String message1 = data.getValue(FieldKey.MESSAGEFIELD1_KEY);
        if ((message1 != null) && (! message1.isEmpty())) {
        	sb.append(message1);
        }
    	String message2 = data.getValue(FieldKey.MESSAGEFIELD2_KEY);
        if ((message2 != null) && (! message2.isEmpty())) {
        	sb.append(" ").append(message2);
        }
		this.displayPaneItem("message", sb.toString(), tags, textPanes);
		
        // Reference Number
        
		String referenceNumber = Pinpad.INSTANCE.getIsPinpadRequired() ? "********" : transaction.getReferenceNumber();
		this.displayLabelItem("reference", referenceNumber, tags);
		
        if (transaction.isFormFreeTransaction()) {
        	statusPanel.setVisible(false);
        } else {
        	TransactionStatusType status = transaction.getTransactionLookupResponse().getPayload().getValue().getTransactionStatus();
        	Map<String, String> currentValues = transaction.getSearchProfilesData() != null ? transaction.getSearchProfilesData().getCurrentValueMap() : null;
        	String statusText = getTranactionStatus(status, currentValues, true, transaction.isFormFreeTransaction());
        	this.displayPaneItem("status", statusText, false, tags, textPanes);
        }

		// Send Purpose of Transaction
		
		String purpose = transaction.getSenderPurposeOfTransaction();
		if (purpose != null) {
			purposeLabel.setText(purpose);
			purposePanel.setVisible(true);
		} else {
			purposePanel.setVisible(false);
		}
		
		tagWidth = this.setTagLabelWidths(tags, MINIMUM_TAG_WIDTH);

		dwTextPaneResized();
        scrollpaneResized(sp1, sp2, panel2);
    }

	@Override
	public void finish(int direction) {
		// call setButtonsEnabled to avoid duplicated transactions.
		flowButtons.setButtonsEnabled(false);
		if (direction == PageExitListener.NEXT) {
			
			// if dispenser is not connected show error
			if ((transaction.getAgentAmount().compareTo(BigDecimal.ZERO) > 0) && transaction.getProfileMoneyGramChecks().equals(("DG"))) {
				DispenserInterface dispenser = DispenserFactory.getDispenser(1);
				if (! dispenser.isOn()) {
					if (! UnitProfile.getInstance().isDemoMode()) {
						Dialogs.showError("dialogs/DialogDispenser_NO_CTS_Error.xml"); 
						flowButtons.setButtonsEnabled(true); 
						flowButtons.getButton("next").requestFocus(); 
						return;
					}
				}
			}

			// Lock the CCI Log file. This will allow the pointer to the current location in
			// this file to be moved back to a saved location, thus only the last record for the
			// DeltaGram checks printed for a transaction will be present in the CCI Log file.

			transaction.lockCciLog();
			
			// Set up to print the money orders.
			
			transaction.setupMoneyOrders();
			int iStatus = transaction.getStatus();
			
			Debug.println("MGRWizardPage8.finish: Transaction status = " + iStatus);
			
			// finalize the transaction
			if ((iStatus == ClientTransaction.IN_PROCESS) || (iStatus == MessageFacadeParam.INVALID_CHECK_NUMBER)) {
				SessionType productType = reversalTran == false ? SessionType.RCV : SessionType.SREV;
				transaction.completeSession(this, productType, transaction.getValidationRequest(), transaction.getReferenceNumber());
				flowButtons.setEnabled("cancel", true); 
				flowButtons.setSelected("cancel"); 
			} else {
				//if in demo mode and dispenser not connected enable OK button and exit.
				if ("MTC".equals(transaction.getProfileMoneyGramChecks()) && UnitProfile.getInstance().isDemoMode()) {
					flowButtons.setEnabled("cancel", true); 
					flowButtons.setSelected("cancel"); 
					return;
				}
				if ("DG".equals(transaction.getProfileMoneyGramChecks()) && !cancel) {
					DispenserInterface dispenser = DispenserFactory.getDispenser(1);
					if (! dispenser.isOn() && UnitProfile.getInstance().isDemoMode()) {
						flowButtons.setEnabled("cancel", true); 
						flowButtons.setSelected("cancel"); 
						return;
					}
					this.resetDispenserReady();
				}
				transaction.setStatus(ClientTransaction.IN_PROCESS);
				transaction.print(this, cancel);
				if ((! UnitProfile.getInstance().isDemoMode()) && (! UnitProfile.getInstance().isTrainingMode())) {
					transaction.logTransaction();
				}
				reSend();
			}
		} else {
			flowButtons.setButtonsEnabled(true);
			PageNotification.notifyExitListeners(this, direction);
		}
	}

	@Override
	public void commComplete(int commTag, Object returnValue) {
		boolean b = false;
		if (returnValue != null) {
			if (returnValue instanceof Boolean) {
				b = ((Boolean) returnValue).booleanValue();
			} else {
				b = true;
			}
		}
		if (commTag == MoneyGramClientTransaction.COMPLETE_SESSION) {
			if (b) {
				MoneyGramReceiveTransaction srTransaction =  (MoneyGramReceiveTransaction) transaction;
				srTransaction.print(this, false);
				if ((! UnitProfile.getInstance().isDemoMode()) && (! UnitProfile.getInstance().isTrainingMode())) {
					transaction.logTransaction();
				}
                return;
			} else {
				transaction.getDataCollectionData().setValidationStatus(DataCollectionStatus.ERROR);
			}
		} else if (commTag == MoneyGramClientTransaction.TRANSACTION_LOOKUP) {
			if (! b) {
				transaction.getDataCollectionData().setValidationStatus(DataCollectionStatus.ERROR);
			}
			PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
		} else if ((commTag == MoneyGramClientTransaction.COMMIT_DONE) || (commTag == MoneyGramClientTransaction.COMMIT_ERROR) || (commTag == MoneyGramClientTransaction.COMMIT_COMMERROR)) {
			if (commTag != MoneyGramClientTransaction.COMMIT_DONE) {
				if (commTag == MoneyGramClientTransaction.COMMIT_COMMERROR) {
					if (transaction.getStatus() == ClientTransaction.CANNOT_COMPLETE_AS_DEFINED) {
						PageNotification.notifyExitListeners(this, PageExitListener.BACK);
						transaction.unlockCciLog();
						return;
					}
				}
			    PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
			} else {
                transaction.transactionLookup(transaction.getReferenceNumber(), null, this, DWValues.TRX_LOOKUP_STATUS, false);
                return;
			}

			if (commTag != MoneyGramClientTransaction.COMMIT_COMMERROR && !reversalTran) {
				//Show transaction complete, then quiescent message on pinpad
				MoneyGramReceiveWizard.showPinpadMessages(Messages.getString("MGRWizardPage1.PinpadIngenico_ValidTransactionCollectFunds"), 15000); 

				transaction.transactionLookup(transaction.getReferenceNumber(), null, this, DWValues.TRX_LOOKUP_STATUS, false);

			}
			
			Debug.println("MGRWizardPage8.commComplete() authorizationMessage=" + transaction.getAuthorizationMessage());
		}
	}
	
	private void resetDispenserReady() {
		// Initialize the dispenser status bits
		DispenserInterface dispenser = DispenserFactory.getDispenser(1);
		dispenser.verified();
	}
}
