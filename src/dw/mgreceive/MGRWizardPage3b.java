package dw.mgreceive;

import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JTextField;

import dw.dialogs.Dialogs;
import dw.framework.KeyMapManager;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;

/**
 * Page 5 of the MoneyGram Receive Wizard. This page displays the correct
 *   answer to the question asked on the last page. The user may press the
 *   'Incorrect' or 'Correct' button according to what the customer answered.
 *   If the choice is 'Incorrect', a dialog will be displayed that will notify
 *   the user that a Photo ID is now required.
 */
public class MGRWizardPage3b extends MoneyGramReceiveFlowPage {
	private static final long serialVersionUID = 1L;

	public MGRWizardPage3b(MoneyGramReceiveTransaction tran,
                       String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "moneygramreceive/MGRWizardPage3b.xml", name, pageCode, buttons);	
    }

    @Override
	public void start(int direction) {
        flowButtons.setSelected("expert");	
        flowButtons.reset();
        flowButtons.setVisible("expert", false);	
        flowButtons.setEnabled("next", false);	
        JTextField answerField = (JTextField) getComponent("answerField");	
        answerField.setText(transaction.getAnswer());
        JButton correctButton = (JButton) getComponent("correctButton");	
        JButton incorrectButton = (JButton) getComponent("incorrectButton");	
        
        KeyMapManager.mapComponent(this, KeyEvent.VK_F8, 0, correctButton);
        KeyMapManager.mapComponent(this, KeyEvent.VK_F9, 0, incorrectButton);
       
        addActionListener("incorrectButton", this, "incorrect");	 
        addActionListener("correctButton", this, "correct");	 
        correctButton.requestFocus();
        JComponent[] focusOrder = {correctButton, incorrectButton,
                                   flowButtons.getButton("back"),	
                                    flowButtons.getButton("cancel")};	
        setFocusOrder(focusOrder);
      
    }

    /**
     * Customer answered the question correctly, so all is well. Carry on.
     */
    public void correct() {
        transaction.setAnswerCorrect(true);
        finish(PageExitListener.NEXT);
    }

    /**
     * Customer answered incorrectly, so let the agent know that a Photo ID will
     *   now be required.
     */
    public void incorrect() {
        transaction.setAnswerCorrect(false);
        // give the user a dialog indicating that they must now use a photo ID
        Dialogs.showWarning("dialogs/DialogMGRIncorrectResponse.xml");	
        finish(PageExitListener.NEXT);
    }

    @Override
	public void finish(int direction) {
    	transaction.setQuestionAsked(true);
		String nextPage = transaction.nextDataCollectionScreen(MoneyGramReceiveWizard.MRW30_ANSWER);
    	if (nextPage.isEmpty()) {
			transaction.receiveValidation(this, ValidationType.SECONDARY);
   	    } else {
   	        PageNotification.notifyExitListeners(this, direction);
   	    }
    }
    
	@Override
	public void commComplete(int commTag, Object returnValue) {
		PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
	}
}
