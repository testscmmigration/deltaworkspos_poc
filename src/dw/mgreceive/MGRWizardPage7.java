/*
 * MGRWizardPage7.java
 *
 * Copyright (c) 2001-2012 MoneyGram International
 */
package dw.mgreceive;

import java.awt.Toolkit;
import java.math.BigDecimal;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;

import dw.comm.MessageFacadeParam;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.dwgui.MoneyField;
import dw.framework.ClientTransaction;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.mgreversal.SendReversalTransaction;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.ReceiveDetailInfo;
import dw.model.adapters.ReceiverInfo;

/**
 * Page 7 of the MoneyGram Receive Wizard. This page allows the user to enter
 *   the information to determine how the receive will be payed out.
 */

/** NOTE.....
 *
 *  If DeltaWorks is to be internationalized, totalField, amount1Field, and
 *  amount2Field will need to be modified to show better currency precision than
 *  the current implementation of MoneyField allows.  It currently changes precision
 *  to two deicimal places.
 */

public class MGRWizardPage7 extends MoneyGramReceiveFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	private MoneyField totalField;
    private MoneyField amount1Field;
    private MoneyField amount2Field;
    private JRadioButton cashRadio;
    private BigDecimal total;
    private BigDecimal defaultPayout;
    private boolean reversalType;
    private MoneyGramReceiveTransaction transaction;
    private SendReversalTransaction rTransaction;
    private JLabel inst3;
    private JLabel tranCurrency1;
    private JLabel tranCurrency2;
    private JLabel tranCurrency3;

    public MGRWizardPage7(MoneyGramReceiveTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "moneygramreceive/MGRWizardPage7.xml", name, pageCode, buttons);    
        transaction = tran;
        reversalType = false;
        init();
    }

    public MGRWizardPage7(SendReversalTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "moneygramreceive/MGRWizardPage7.xml", name, pageCode, buttons);    
        rTransaction = tran;
        transaction = tran;
        reversalType = true;
        init();
    }
    
    private void init() {
        totalField = (MoneyField) getComponent("totalField");   
        amount1Field = (MoneyField) getComponent("amount1Field");   
        amount2Field = (MoneyField) getComponent("amount2Field");   
        cashRadio = (JRadioButton) getComponent("cashRadio");   
        inst3 = (JLabel) getComponent("inst3");
        tranCurrency1 = (JLabel) getComponent("tranCurrency1");
        tranCurrency2 = (JLabel) getComponent("tranCurrency2");
        tranCurrency3 = (JLabel) getComponent("tranCurrency3");
        DWTextPane tp = (DWTextPane) getComponent("text1");
        tp.addListener(this, this);
    }
    
	@Override
	public void dwTextPaneResized() {
		DWTextPane tp = (DWTextPane) getComponent("text1");
		JPanel panel = (JPanel) getComponent("panel1");
		int width = panel.getPreferredSize().width;
		tp.setWidth(width);
		tp.setListenerEnabled(false);
	}

    @Override
	public void start(int direction) {
        flowButtons.reset();
        flowButtons.setVisible("expert", false);	
		cashRadio.setSelected(transaction.getCustPayoutAmount().compareTo(BigDecimal.ZERO) == 0);
        amount1Field.setEnabled(true);
        addActionListener("amount1Field", this);	
        addFocusLostListener("amount1Field", this, "updateAmounts");	 

        DWTextPane tp = (DWTextPane) getComponent("text1");

   		tp.setText(Messages.getString("MGRWizardPage7.945")); 
       	inst3.setText(Messages.getString("MGRWizardPage7.947"));

        if (transaction.getReceiveDetail() == null) {
            transaction.createNewReceiveDetailInfo();
        }

        if (reversalType) {
        	com.moneygram.agentconnect.TransactionLookupResponse.Payload payload = rTransaction.getTransactionLookupResponse().getPayload().getValue();
            BigDecimal sendAmount = payload.getSendAmounts().getSendAmount();
            BigDecimal feeAmount = payload.getSendAmounts().getTotalSendFees();
            if (rTransaction.getRefundFee()) {
            	rTransaction.getReceiveDetail().setReceiveAmount(sendAmount.add(feeAmount));
                rTransaction.setAgentAmount(sendAmount.add(feeAmount));
            } else {
            	rTransaction.getReceiveDetail().setReceiveAmount(sendAmount);
                rTransaction.setAgentAmount(sendAmount);
            }
            ReceiveDetailInfo detail = rTransaction.getReceiveDetail();
            ReceiverInfo ri = new ReceiverInfo();
            ri.setFirstName(detail.getSenderFirstName());
            ri.setLastName(detail.getSenderLastName());
            ri.setSecondLastName(detail.getSenderLastName2());
            ri.setHomePhone(detail.getSenderHomePhone());
            transaction.setReceiver(ri);
            transaction.setReferenceNumber(rTransaction.getReversalReferenceNumber());
            transaction.getReceiveDetail().setReceiveCurrency(payload.getReceiveAmounts().getReceiveCurrency());
        }

        String receiveCurrency = transaction.getCurrency();
        /*
         * Set all the MoneyFields to use the receive currency, just in case it
         * is different from the base currency.
         */
        if (CountryInfo.getAgentCountry().getBaseReceiveCurrency().compareToIgnoreCase(receiveCurrency) != 0) {
            totalField.setCurrency(receiveCurrency);
            amount1Field.setCurrency(receiveCurrency);
            amount2Field.setCurrency(receiveCurrency);
        }
        tranCurrency1.setText(receiveCurrency);
        tranCurrency2.setText(receiveCurrency);
        tranCurrency3.setText(receiveCurrency);

        if (reversalType) {
        	defaultPayout = rTransaction.getReversalDefaultPayout(receiveCurrency);
            total = rTransaction.getAmount();
        } else {
        	defaultPayout = transaction.getDefaultPayout();
            total = transaction.getAmount();
        }
        totalField.setText(total.toString());

        // set the default payout in the amount 1 field if total is not smaller
        if (total.compareTo(defaultPayout) < 0) {
            amount1Field.setText(total.toString());
        } else {
            amount1Field.setText(defaultPayout.toString());
        }

		updateAmounts();
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				amount1Field.requestFocus();
			}
		});
    }

    /**
     * Update the amount fields to add up to the total value.
     */
    public void updateAmounts() {
        BigDecimal amount1 = new BigDecimal(amount1Field.getText());
        // make sure amount 1 <= total and <= MAX_MO
        if (amount1.compareTo(total) > 0)
            amount1 = new BigDecimal(total.toString());
        if (amount1.compareTo(MoneyGramReceiveTransaction.MAX_MO) > 0)
            amount1 = MoneyGramReceiveTransaction.MAX_MO;

        // set balance in amount2 field...make sure this is not > MAX_MO either
        BigDecimal amount2 = total.subtract(amount1);
        if (amount2.compareTo(MoneyGramReceiveTransaction.MAX_MO) > 0) {
            amount1 = total.subtract(MoneyGramReceiveTransaction.MAX_MO);
            amount2 = MoneyGramReceiveTransaction.MAX_MO;
        }
        amount1Field.setText(amount1.toString());
        amount2Field.setText(amount2.toString());
    }

    public void amount1FieldAction() {
        flowButtons.setSelected("next");	
    }

    @Override
	public void finish(int direction) {
        flowButtons.disable();
        updateAmounts();
		/*
		* The following code will fix AHD 174745.
		*/

        if (direction != PageExitListener.NEXT) {
            PageNotification.notifyExitListeners(this, direction);
            return;
        }

        // do not allow the user to enter an amount of 0 for the agent's MO.
        
        if ((new BigDecimal(amount1Field.getText())).compareTo(BigDecimal.ZERO) == 0) {
            Toolkit.getDefaultToolkit().beep();
            this.start(direction);
            return;
        }
        
        transaction.setAgentAmount(new BigDecimal(amount1Field.getText()));
        transaction.setCustomerAmount(new BigDecimal(amount2Field.getText()));
        transaction.setOtherPayoutAmount(BigDecimal.ZERO);
        transaction.setAmount1Cash(cashRadio.isSelected());
            
    	if (! reversalType) {
            boolean statusFlag = (transaction.getStatus() == ClientTransaction.IN_PROCESS || transaction.getStatus() == MessageFacadeParam.INVALID_CHECK_NUMBER);
            if (statusFlag && transaction.getDataCollectionData().isAdditionalInformationScreenValid()) {
				transaction.receiveValidation(this, ValidationType.SECONDARY);
				return;
            }
    	} else {
    		rTransaction.sendReversalValidation(this, ValidationType.SECONDARY);
			return;
    	}
        
       	PageNotification.notifyExitListeners(this, direction);
    }

	@Override
	public void commComplete(int commTag, Object returnValue) {
		if (returnValue != null) {
			PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
		}
	}
}
