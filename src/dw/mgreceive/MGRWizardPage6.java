package dw.mgreceive;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import dw.framework.DataCollectionField;
import dw.framework.DataCollectionScreenToolkit;
import dw.framework.DataCollectionSet;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;
import dw.utility.ExtraDebug;

/**
 * Page 6c of the MoneyGram Receive Wizard. This page prompts for information about
 *   the third party sending money, if there is one.
 */
public class MGRWizardPage6 extends MoneyGramReceiveFlowPage {
	private static final long serialVersionUID = 1L;

	private JScrollPane scrollPane1;
	private JScrollPane scrollPane2;
	private JPanel panel2;
	private DataCollectionScreenToolkit toolkit;
	private Object dataCollectionSetChangeFlag;
	private JButton nextButton;
	
    public MGRWizardPage6(MoneyGramReceiveTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "moneygramreceive/MGRWizardPage6.xml", name, pageCode, buttons); 
		scrollPane1 = (JScrollPane) getComponent("scrollPane1");
		scrollPane2 = (JScrollPane) getComponent("scrollPane2");
		panel2 = (JPanel) getComponent("scrollee"); 
		nextButton = this.flowButtons.getButton("next");
    }

    private void setupComponents() {
		
		DataCollectionSet data = this.transaction.getDataCollectionData();

		if (dataCollectionSetChangeFlag != transaction.getDataCollectionData().getChangeFlag()) {
			
			// Save a reference to the GFFP information.
			
	    	dataCollectionSetChangeFlag = transaction.getDataCollectionData().getChangeFlag();
	    	
	    	// Rebuild the data collection items for the screen.
	    	
	    	ExtraDebug.println("Rebuilding Screen " + getPageCode() + " from Fields to Collect");
			int flags = DataCollectionScreenToolkit.DISPLAY_REQUIRED_FLAG;
			JButton backButton = this.flowButtons.getButton("back");
			toolkit = new DataCollectionScreenToolkit(this, scrollPane1, this, scrollPane2, panel2, flags, transaction.getThirdPartyScreenStatus().getDataCollectionPanels(data), transaction, backButton, nextButton, null);
		}
		
		// Check if the screen needs to be laid out
		
		if (transaction.getThirdPartyScreenStatus().needLayout(data)) {
			toolkit.populateDataCollectionScreen(panel2, transaction.getThirdPartyScreenStatus().getDataCollectionPanels(data), false);
		}
    }

    @Override
	public void start(int direction) {
		
		// Make sure the screen has been built.
		
		setupComponents();
		
		// Setup the flow buttons at the bottom of the screen.
		
		flowButtons.reset();
		flowButtons.setVisible("expert", false); 
		flowButtons.setEnabled("expert", false); 
		
		// Populate the screen with any previously entered data.
		
		if (direction == PageExitListener.NEXT || direction == PageExitListener.BACK) {
			DataCollectionScreenToolkit.restoreDataCollectionFields(toolkit.getScreenFieldList(), transaction.getDataCollectionData());
		}
		
		// Move the cursor to 1) the first required item with no value or item 
		// with an invalid value or 2) the first optional item with no value.
		
		toolkit.moveCursor(toolkit.getScreenFieldList(), nextButton);
		
		// Enable and call the resize listener.
		
		toolkit.enableResize();
		toolkit.getComponentResizeListener().componentResized(null);
    }
    
    @Override
    public void finish(int direction) {
    	
    	// Disable the resize listener for this screen.
    	
		toolkit.disableResize();
        
    	// If the flow direction is to the next screen, check the validity of the
    	// data. If a value is invalid or a required item does not have a value,
    	// return to the screen.
    	
    	if (direction == PageExitListener.NEXT) {
        	String tag = toolkit.validateData(toolkit.getScreenFieldList(), DataCollectionField.DISPLAY_ERRORS_VALIDATION);
        	if (tag != null) {
				flowButtons.setButtonsEnabled(true);
				toolkit.enableResize();
        		return;
        	}
        }
    	
    	// If the flow direction is to the next screen or back to the previous
    	// screen, save the value of the current items in a data structure.

        if ((direction == PageExitListener.BACK) || (direction == PageExitListener.NEXT)) {
        	DataCollectionScreenToolkit.saveDataCollectionFields(toolkit.getScreenFieldList(), transaction.getDataCollectionData());
        }
    	
    	// Check if receiveValidation could be performed now or if another page should be displayed.
    	
    	if (direction == PageExitListener.NEXT) {
        	transaction.setValidationStatus(DataCollectionStatus.VALIDATION_NOT_PERFORMED);
    		boolean performValidation = transaction.performValidation(MoneyGramReceiveWizard.MRW45_THIRD_PARTY, this.transaction);
    		if (performValidation && transaction.getDataCollectionData().isAdditionalInformationScreenValid()) {
    			transaction.receiveValidation(this, ValidationType.SECONDARY);
    			return;
    		}
    	}
    	PageNotification.notifyExitListeners(this, direction);
    }

	@Override
	public void commComplete(int commTag, Object returnValue) {
		if (returnValue != null) {
			PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
		}
	}
}
