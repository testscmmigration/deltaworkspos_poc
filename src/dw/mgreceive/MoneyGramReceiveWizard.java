package dw.mgreceive;

import java.awt.Component;
import java.awt.event.KeyEvent;

import dw.comm.MessageFacadeParam;
import dw.dispenserload.DispenserLoadTransaction;
import dw.dispenserload.NewWizardPage1;
import dw.dispenserload.NewWizardPage2;
import dw.dispenserload.NewWizardPage3;
import dw.dispenserload.NewWizardPage4;
import dw.framework.AdditionalInformationWizardPage;
import dw.framework.ClientTransaction;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.DataCollectionSet.DataCollectionType;
import dw.framework.FlowPage;
import dw.framework.KeyMapManager;
import dw.framework.FormFreeIdVerificationWizardPage;
import dw.framework.PageExitListener;
import dw.framework.PageNameInterface;
import dw.framework.ProfileEditWizardPage1;
import dw.framework.ProfileEditWizardPage2;
import dw.framework.ProfileReviewWizardPage;
import dw.framework.ProfileSearchWizardPage;
import dw.framework.ProfileSelectionWizardPage;
import dw.framework.SupplementalInformationWizardPage;
import dw.i18n.Messages;
import dw.mgsend.MoneyGramClientTransaction.ProfileStatus;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;
import dw.model.adapters.ConsumerProfile;
import dw.model.adapters.ConsumerProfile.NotificationOptionDisplayStatus;
import dw.pinpad.Pinpad;
import dw.utility.Debug;

public class MoneyGramReceiveWizard extends MoneyGramReceiveTransactionInterface {
	private static final long serialVersionUID = 1L;

	private static final String MRW10_RECEIVE_SEARCH = "receiveSearch";
	private static final String MRW12_PROFILE_SEARCH_RESULTS = "profileSearchResults";
	private static final String MRW16_PROFILE_CREATE_UPDATE_1 = "profileCreateUpdate1";
	private static final String MRW17_PROFILE_CREATE_UPDATE_2 = "profileCreateUpdate2";
	private static final String MRW18_PROFILE_REVIEW = "profileReview";
    public static final String MRW20_RECEIVE_DETAIL = "receiveDetail";
    static final String MRW25_QUESTION = "question";
    static final String MRW30_ANSWER = "answer";
    static final String MRW35_COMPLIANCE = "compliance";
    public static final String MRW40_FORM_FREE_ID_VALIDATION = "formFreeIdValidation";
    static final String MRW45_THIRD_PARTY = "thirdParty";
    static final String MRW50_SUPPLEMENTAL = "supplemental";
    static final String MRW55_PAYOUT = "payout";
    static final String MRW57_PAYOUT_CHECK = "payoutCheck";
    public static final String MRW59_ADDITIONAL_RECEIVER_INFORMATION = "additionalReceiverInformation";
    static final String MRW60_REVIEW = "review";
    private static final String MRW65_PRINTING = "printing";
    private static final String MRW75_DISPENSER_ERROR = "dispenserError";
    private static final String MRW85_NO_FORM_ERROR = "dispenserErrorNoForms";
    private static final String MRW90_COMMUNICATION_ERROR = "communicationError";
	
    private static final String DLW05_DISPENSER_LOAD_1 = "dlwpage1";
    private static final String DLW10_DISPENSER_LOAD_2 = "dlwpage2";
    private static final String DLW15_DDISPENSER_LOAD_3 = "dlwpage3";
    private static final String DLW20_DISPENSER_LOAD_4 = "dlwpage4";
	
    private DispenserLoadTransaction dispenserLoadTransaction;
    static Thread showPinpadMessagesThread = null;
    
    public MoneyGramReceiveWizard(MoneyGramReceiveTransaction transaction, PageNameInterface naming) {
        super("moneygramreceive/MGRWizardPanel.xml", transaction, naming, false);	
        createPages();
    }

    @Override
	public void createPages() {
    
    	dispenserLoadTransaction = new DispenserLoadTransaction(Messages.getString("MoneyGramReceiveWizard.1985")); 
       
		addPage(ProfileSearchWizardPage.class,   transaction, MRW10_RECEIVE_SEARCH, "MRW10", buttons);  
		addPage(ProfileSelectionWizardPage.class,  transaction, MRW12_PROFILE_SEARCH_RESULTS, "MRW12", buttons);
		addPage(ProfileEditWizardPage1.class,   transaction, MRW16_PROFILE_CREATE_UPDATE_1, "MRW16", buttons);  
		addPage(ProfileEditWizardPage2.class,   transaction, MRW17_PROFILE_CREATE_UPDATE_2, "MRW17", buttons);
		addPage(ProfileReviewWizardPage.class,   transaction, MRW18_PROFILE_REVIEW, "MRW18", buttons);
        addPage(NewWizardPage1.class, dispenserLoadTransaction, DLW05_DISPENSER_LOAD_1, "DLW05", buttons);	 
        addPage(NewWizardPage2.class, dispenserLoadTransaction, DLW10_DISPENSER_LOAD_2, "DLW10", buttons);	 
        addPage(NewWizardPage3.class, dispenserLoadTransaction, DLW15_DDISPENSER_LOAD_3, "DLW15", buttons);	 
        addPage(NewWizardPage4.class, dispenserLoadTransaction, DLW20_DISPENSER_LOAD_4, "DLW20", buttons);	 
        addPage(MGRWizardPage3.class, transaction, MRW20_RECEIVE_DETAIL, "MRW20", buttons);	 
        addPage(MGRWizardPage3a.class, transaction, MRW25_QUESTION, "MRW25", buttons);	 
        addPage(MGRWizardPage3b.class, transaction, MRW30_ANSWER, "MRW30", buttons);	 
        addPage(MGRWizardPage4.class, transaction, MRW35_COMPLIANCE, "MRW35", buttons);	 
        addPage(FormFreeIdVerificationWizardPage.class, transaction, MRW40_FORM_FREE_ID_VALIDATION, "MRW40", buttons);	 
        addPage(MGRWizardPage6.class, transaction, MRW45_THIRD_PARTY, "MRW45", buttons);	 
        addPage(SupplementalInformationWizardPage.class, transaction, MRW50_SUPPLEMENTAL, "MRW50", buttons);
        addPage(MGRWizardPage7.class, transaction, MRW55_PAYOUT, "MRW55", buttons);     
        addPage(MGRWizardPage7b.class, transaction, MRW57_PAYOUT_CHECK, "MRW57", buttons);     
        addPage(MGRWizardPage8.class, transaction, MRW60_REVIEW, "MRW60", buttons);	 
        addPage(AdditionalInformationWizardPage.class, transaction, MRW59_ADDITIONAL_RECEIVER_INFORMATION, "MRW59", buttons);
        addPage(MGRWizardPage9.class, transaction, MRW65_PRINTING, "MRW65", buttons);	 
        addPage(MGRWizardPage11.class, transaction, MRW75_DISPENSER_ERROR, "MRW75", buttons);	 
        addPage(MGRWizardPage13.class, transaction, MRW85_NO_FORM_ERROR, "MRW85", buttons);	 
        addPage(MGRWizardPage14.class, transaction, MRW90_COMMUNICATION_ERROR, "MRW90", buttons);	 
    }

    /**
     * Add the single keystroke mappings to the buttons. Use the function keys
     *   for back, next, cancel, etc.
     */
    private void addKeyMappings() {
        KeyMapManager.mapMethod(this, KeyEvent.VK_F2, 0, "f2SAR");	
        buttons.addKeyMapping("back", KeyEvent.VK_F11, 0);	
        buttons.addKeyMapping("next", KeyEvent.VK_F12, 0);	
        buttons.addKeyMapping("cancel", KeyEvent.VK_ESCAPE, 0);	
    }

    @Override
	public void start() {
        super.start();
        addKeyMappings();
    }

    public void f2SAR() {
        Component current = cardLayout.getVisiblePage();
        if (current instanceof FlowPage){
            ((FlowPage) current).updateTransaction();
        }
        transaction.f2SAR();
    }
    /**
     * Page traversal logic here.
     */
    @Override
	public void exit(int direction) {
        String currentPage = cardLayout.getVisiblePageName();
        String nextPage = null;
        
        switch (direction) {
        
        	//=================================================================
        	// Forward
        	//=================================================================
        
	        case PageExitListener.NEXT:
	            if (currentPage.equals(MRW10_RECEIVE_SEARCH)) {	
            		nextPage = MRW20_RECEIVE_DETAIL;
	            	
				} else if (currentPage.equals(MRW20_RECEIVE_DETAIL)) {
					if (! transaction.isFormFreeTransaction()) {
		        		int profileCount = transaction.getSearchProfiles() != null ? transaction.getSearchProfiles().size() : 0;
		        		if (profileCount == 0) {
		        			nextPage = MRW16_PROFILE_CREATE_UPDATE_1;
		        		} else {
		        			nextPage = MRW12_PROFILE_SEARCH_RESULTS;
		        		}
					} else {
						if (transaction.isTestQuestionAvailable()) {
							nextPage = MRW25_QUESTION;
						} else {
							nextPage = nextDataCollectionScreen(currentPage);
			            	if (nextPage.isEmpty()) {
								nextPage = getPayoutPage();
		               	    }      
		            	}
					}
					
	            } else if (currentPage.equals(MRW12_PROFILE_SEARCH_RESULTS)) {
	            	if (transaction.getProfileStatus().equals(ProfileStatus.CREATE_PROFILE)) {
						nextPage = MRW16_PROFILE_CREATE_UPDATE_1;
					} else {
						nextPage = MRW18_PROFILE_REVIEW;
					}
	            	
				} else if (currentPage.equals(MRW16_PROFILE_CREATE_UPDATE_1)) {
					if (transaction.getProfileDataScreen2().hasDataToCollect()) {
						nextPage = MRW17_PROFILE_CREATE_UPDATE_2;
					} else {
						nextPage = profileForwardPage();
					}

				} else if (currentPage.equals(MRW17_PROFILE_CREATE_UPDATE_2)) {
					ConsumerProfile profile = transaction.getProfileObject();
					nextPage = profileForwardPage();
					profile.setNotificationsDisplayed(NotificationOptionDisplayStatus.ALlREADY_DISPLAYED);

	            } else if (currentPage.equals(MRW18_PROFILE_REVIEW)) {
					nextPage = MRW17_PROFILE_CREATE_UPDATE_2;
					ProfileStatus profileStatus = transaction.getProfileStatus();
					if ((profileStatus == null) || (profileStatus.equals(ProfileStatus.USE_CURRENT_PROFILE))) {
						ConsumerProfile profile = transaction.getProfileObject();
						if (profile.getNotificationsDisplayed().equals(NotificationOptionDisplayStatus.NOT_DISPLAYED) && transaction.getProfileDataScreen2().hasDataToCollect()) {
							nextPage = MRW17_PROFILE_CREATE_UPDATE_2;
							profile.setNotificationsDisplayed(NotificationOptionDisplayStatus.DISPLAYING_IN_REVIEW);
						} else {
			            	nextPage = nextDataCollectionScreen(currentPage);
						}
					} else {
						nextPage = MRW16_PROFILE_CREATE_UPDATE_1;
					}
					
	            } else if (currentPage.equals(MRW25_QUESTION)) {
	            	if (transaction.isAnswerCorrect()) {
	            		nextPage = nextDataCollectionScreen(currentPage);
		            	if (nextPage.isEmpty()) {
							nextPage = getPayoutPage();
	               	    }      
	            	} else {
	            		nextPage = MRW30_ANSWER;
	            	}
	            	
	            } else if (currentPage.equals(MRW30_ANSWER)) {
	        		nextPage = transaction.nextDataCollectionScreen(currentPage);
	            	if (nextPage.isEmpty()) {
						nextPage = getPayoutPage();
	           	    }      
	            	
	            } else if ((currentPage.equals(MRW35_COMPLIANCE)) ||  
	            		(currentPage.equals(MRW40_FORM_FREE_ID_VALIDATION)) || 
	            		(currentPage.equals(MRW45_THIRD_PARTY)) || 
	            		(currentPage.equals(MRW50_SUPPLEMENTAL))) {
	            	nextPage = nextDataCollectionScreen(currentPage);
	            
	            } else if ((currentPage.equals(MRW55_PAYOUT)) || (currentPage.equals(MRW57_PAYOUT_CHECK))) {	
	               	nextPage = nextDataCollectionScreen(currentPage);
	            
	            } else if (currentPage.equals(MRW59_ADDITIONAL_RECEIVER_INFORMATION)) {
	            	DataCollectionType type = transaction.getDataCollectionData().getDataCollectionType();
	            	if (type.equals(DataCollectionType.VALIDATION)) {
	    				String pseudoPage;
	    				if (transaction.getValidationType().equals(ValidationType.SECONDARY)) {
	    					pseudoPage = MRW59_ADDITIONAL_RECEIVER_INFORMATION;
	    				} else {
	    					pseudoPage = MRW20_RECEIVE_DETAIL;
	    				}
	            		nextPage = nextDataCollectionScreen(pseudoPage);
	            	} else {
	    				DataCollectionStatus status = transaction.getDataCollectionData().getValidationStatus();
	    				if (status.equals(DataCollectionStatus.VALIDATED)) {
	    					nextPage = profileForwardPage();
	    				} else if (status.equals(DataCollectionStatus.NEED_MORE_DATA)) {
	    					nextPage = MRW59_ADDITIONAL_RECEIVER_INFORMATION;
	    				} else {
	    					nextPage = MRW90_COMMUNICATION_ERROR;
	    				}
	            	}
	            
	            } else if (currentPage.equals(MRW60_REVIEW)) {
	           		nextPage = MRW65_PRINTING;
	                
	            } else if (currentPage.equals(MRW65_PRINTING)) {	
	                if (transaction.getStatus() == ClientTransaction.COMPLETED) {
	                    nextPage = MRW65_PRINTING;	
	                } else if (transaction.getStatus() == ClientTransaction.FAILED) {
	                    nextPage = MRW65_PRINTING;	
	                } else if (transaction.getStatus() == ClientTransaction.NO_FORMS) {
	                    nextPage = MRW85_NO_FORM_ERROR;	
	                } else if (transaction.getStatus() == ClientTransaction.COMM_ERROR) {
	                    nextPage = MRW90_COMMUNICATION_ERROR;    
	                } else if (transaction.getStatus() == MessageFacadeParam.INVALID_CHECK_NUMBER) {
	                    if (transaction.isDeltaGramChecks()) {
	                        nextPage = MRW55_PAYOUT; 
	                    } else {
	                        nextPage = MRW57_PAYOUT_CHECK;
	                    }
	                } else {
	                    nextPage = MRW75_DISPENSER_ERROR;
	                }
	            
	            } else if (currentPage.equals(MRW75_DISPENSER_ERROR)) {	
	                nextPage = DLW05_DISPENSER_LOAD_1;	
	            
	            } else if (currentPage.equals(MRW85_NO_FORM_ERROR)) {	
	                nextPage = DLW05_DISPENSER_LOAD_1;	
	            }
	            break;
	        	
        	//=================================================================
        	// Backward
        	//=================================================================
	        
	        case PageExitListener.BACK:	
	        	if (currentPage.equals(DLW05_DISPENSER_LOAD_1)) {	
		            if (dispenserLoadTransaction.getRemainingNumberOfForms() <= 2) {
		                nextPage = MRW85_NO_FORM_ERROR;
		            }

	            } else if (currentPage.equals(MRW20_RECEIVE_DETAIL)) {
	            	nextPage = MRW10_RECEIVE_SEARCH;

	            } else if (currentPage.equals(MRW12_PROFILE_SEARCH_RESULTS)) {
	            	nextPage = MRW20_RECEIVE_DETAIL;
	            	
	            } else if (currentPage.equals(MRW16_PROFILE_CREATE_UPDATE_1)) {
	        		int profileCount = transaction.getSearchProfiles() != null ? transaction.getSearchProfiles().size() : 0;
	            	ProfileStatus profileStatus = transaction.getProfileStatus();
	            	if (profileStatus.equals(ProfileStatus.CREATE_PROFILE)) {
	            		if (profileCount == 0) {
	            			nextPage = MRW10_RECEIVE_SEARCH;
	            		} else {
	            			nextPage = MRW12_PROFILE_SEARCH_RESULTS;
	            		}
	            	} else {
	            		nextPage = MRW18_PROFILE_REVIEW;
	            	}
	            
	            } else if (currentPage.equals(MRW17_PROFILE_CREATE_UPDATE_2)) {
	            	nextPage = MRW16_PROFILE_CREATE_UPDATE_1;

	            } else if (currentPage.equals(MRW18_PROFILE_REVIEW)) {
	        		int profileCount = transaction.getSearchProfiles() != null ? transaction.getSearchProfiles().size() : 0;
	            	if (transaction.isFormFreeTransaction()) {
	            		nextPage = MRW10_RECEIVE_SEARCH;
	            	} else if (profileCount == 0) {
		            	nextPage = MRW20_RECEIVE_DETAIL;
					} else if (profileCount == 1) {
						nextPage = MRW12_PROFILE_SEARCH_RESULTS;
					} else {
						nextPage = MRW12_PROFILE_SEARCH_RESULTS;
					}

	            } else if (currentPage.equals(MRW25_QUESTION)) {
					transaction.setDataCollecionReview(true);
	            	nextPage = MRW20_RECEIVE_DETAIL;      
	            
	            } else if ((currentPage.equals(MRW35_COMPLIANCE)) || 
	            		(currentPage.equals(MRW45_THIRD_PARTY)) ||
	            		(currentPage.equals(MRW50_SUPPLEMENTAL)) ||
	            		(currentPage.equals(MRW59_ADDITIONAL_RECEIVER_INFORMATION))) {
	            	DataCollectionType type = transaction.getDataCollectionData().getDataCollectionType();
	            	if (type.equals(DataCollectionType.VALIDATION)) {
						transaction.setDataCollecionReview(true);
		            	nextPage = transaction.previousDataCollectionScreen(currentPage);
		                if (nextPage.isEmpty()) {
		                	if (! transaction.isFormFreeTransaction()) {
								if (transaction.isTestQuestionAvailable()) {
									nextPage = MRW25_QUESTION;
								} else {
									nextPage = MRW18_PROFILE_REVIEW;
								}
			                } else {
			                	nextPage = MRW20_RECEIVE_DETAIL;
			                }
	                	}
	            	} else {
	                	if (! transaction.isFormFreeTransaction()) {
		            		if (transaction.getProfileDataScreen2().hasDataToCollect()) {
		            			nextPage = MRW17_PROFILE_CREATE_UPDATE_2;
		            		} else {
		            			nextPage = MRW16_PROFILE_CREATE_UPDATE_1;
		            		}
		                } else {
		                	nextPage = MRW20_RECEIVE_DETAIL;
		                }
	            	}
	              
	            } else if (currentPage.equals(MRW40_FORM_FREE_ID_VALIDATION)) {
					transaction.setDataCollecionReview(true);
					popHistoryUntil(MRW10_RECEIVE_SEARCH);
		            super.exit(PageExitListener.CANCELED);
		            return;
	            
	            } else if ((currentPage.equals(MRW55_PAYOUT)) || 
	            		(currentPage.equals(MRW57_PAYOUT_CHECK))) {
					transaction.setDataCollecionReview(true);
	        		nextPage = transaction.previousDataCollectionScreen(currentPage);
	        		if (nextPage.isEmpty()) {
	        			if (transaction.isTestQuestionAvailable()) {
	        				nextPage = MRW25_QUESTION;   	
	        			} else {
	        				nextPage = MRW20_RECEIVE_DETAIL;
	        			}
	                }
	        		
	            } else if (currentPage.equals(MRW60_REVIEW)) {
	        		int payoutType = transaction.getPayoutMethod();
	        		if (payoutType == MoneyGramReceiveTransaction.PAYOUT_DELTA_GRAM) {
	        			nextPage = MRW55_PAYOUT; 
	        		} else if (payoutType == MoneyGramReceiveTransaction.PAYOUT_MONEY_TRANSFER_CHECK) {
	        			nextPage = MRW57_PAYOUT_CHECK;
	        		} else { 
						transaction.setDataCollecionReview(true);
		        		nextPage = transaction.previousDataCollectionScreen(currentPage);
		        		if (nextPage.isEmpty()) {
		        			if (transaction.isTestQuestionAvailable()) {
		        				nextPage = MRW25_QUESTION;   	
		        			} else {
		        				nextPage = MRW20_RECEIVE_DETAIL;
		        			}
		                }
	            	}
	            } else if (currentPage.equals(MRW75_DISPENSER_ERROR)) {	
	                nextPage = MRW75_DISPENSER_ERROR;	
	             
	            } else if (currentPage.equals(MRW85_NO_FORM_ERROR))	{ 
	            	nextPage = MRW85_NO_FORM_ERROR;	
				}
	        	break;
	        	
        	//=================================================================
        	// Completed
        	//=================================================================
	        
	        case PageExitListener.COMPLETED:
	            if (currentPage.equals(DLW20_DISPENSER_LOAD_4)) {	
	                nextPage = MRW65_PRINTING;	
	                transaction.setStatus(ClientTransaction.LOAD_REQUIRED);
	            } else {
	                transaction.f2NAG();
	            }
	            break;
	        	
        	//=================================================================
        	// Canceled
        	//=================================================================
	        
	        case PageExitListener.CANCELED:
	            transaction.escSAR();
	            if (currentPage.equals(MRW75_DISPENSER_ERROR)) {	
	                nextPage = MRW65_PRINTING; 
	            } else if (currentPage.equals(DLW05_DISPENSER_LOAD_1) || currentPage.equals(DLW10_DISPENSER_LOAD_2) ||	
	                   currentPage.equals(DLW15_DDISPENSER_LOAD_3) || currentPage.equals(DLW20_DISPENSER_LOAD_4)) {	
	                if (dispenserLoadTransaction.getRemainingNumberOfForms() > 2) {
	                    nextPage = MRW75_DISPENSER_ERROR;	
	                } else {
	                    nextPage = MRW85_NO_FORM_ERROR;	
	            	}
	            } else if (Pinpad.INSTANCE.getIsPinpadRequired()) {
	            	showPinpadMessages(Messages.getString("MGRWizardPage1.PinpadIngenico_TransactionCancelledByAgent"), 15000); 
	            }
	            break;
	        	
        	//=================================================================
        	// Timeout
        	//=================================================================
	        
	        case PageExitListener.TIMEOUT:
	        	showPinpadMessages(Messages.getString("MGRWizardPage1.PinpadIngenico_TransactionTimedOut"), 3000); 
//	        	if ((currentPage.equals(MRW10_RECEIVE_SEARCH)) && (Pinpad.INSTANCE.getPinpadStatus() != 0)) {
//	            	nextPage = MRW10_RECEIVE_SEARCH; 	
//	            }
	        	break;
	        	
        	//=================================================================
        	// Failed
        	//=================================================================
	        
	        case PageExitListener.FAILED:
	            if (currentPage.equals(MRW65_PRINTING)) {	
	                nextPage = MRW65_PRINTING;
	            }
	            break;
        	
        	default:
                super.exit(direction);
                return;
        }
                
        if (nextPage != null) {
            showPage(nextPage, direction);
        } else {
            super.exit(direction);
        }
    }
    
    private String getPayoutPage() {
		int payoutType = transaction.getPayoutMethod();
		if (payoutType == MoneyGramReceiveTransaction.PAYOUT_DELTA_GRAM) {
			return MRW55_PAYOUT; 
		} else if (payoutType == MoneyGramReceiveTransaction.PAYOUT_MONEY_TRANSFER_CHECK) {
			return MRW57_PAYOUT_CHECK;
		} else {
			return MRW60_REVIEW;
		}
    }

    protected static void showPinpadMessages(String pm_string, final int pm_sleepSecs) {
		if (Pinpad.INSTANCE.getIsPinpadRequired()) {
			Pinpad.INSTANCE.sendDisplayMessageRequest(pm_string);
		    //Start a separate thread to allow user to read pinpad display, then display quiescent message.
		    showPinpadMessagesThread = new Thread(
		    	new Runnable() {
			        @Override
					public void run() {
			            try {
			                Thread.sleep(pm_sleepSecs);
			            } catch (InterruptedException ie) {
			                Debug.println("Operator interrupted the sleep wait");	
			    			return;
			            }
			            Pinpad.INSTANCE.sendDisplayMessageRequest(Messages.getString(
			        		"MGRWizardPage1.PinpadIngenico_MoneygramInternational")); 
		        }
		    });
		    showPinpadMessagesThread.start();
		}
	}

    public static void interuptshowPinpadMessagesThread() {
    	if (showPinpadMessagesThread != null) {
    		showPinpadMessagesThread.interrupt();
    		showPinpadMessagesThread = null;
    	}
    }

    private String nextDataCollectionScreen(String currentPage) {
		
		String nextPage = transaction.nextDataCollectionScreen(currentPage);
		if ((nextPage.isEmpty()) && 
				transaction.getDataCollectionData().isAfterValidationAttempt() && 
				((transaction.getValidationStatus().equals(DataCollectionStatus.NEED_MORE_DATA)) || (transaction.getValidationStatus().equals(DataCollectionStatus.NOT_VALIDATED)))) {
			nextPage = MRW59_ADDITIONAL_RECEIVER_INFORMATION;
		}
		
		// If no more data needs to be collected, then a send validation should have been performed. 
		// Display the next screen based upon the status of the validation.
		
		if (nextPage.isEmpty()) {
			if (transaction.getValidationStatus().equals(DataCollectionStatus.ERROR)) {
				nextPage = MRW90_COMMUNICATION_ERROR;
			} else if (! transaction.getDataCollectionData().isAfterValidationAttempt()) {
				if (transaction.getPayoutMethod() == MoneyGramReceiveTransaction.PAYOUT_DELTA_GRAM) {
					nextPage = MoneyGramReceiveWizard.MRW55_PAYOUT; 
				} else if (transaction.getPayoutMethod() == MoneyGramReceiveTransaction.PAYOUT_MONEY_TRANSFER_CHECK) {
					nextPage = MoneyGramReceiveWizard.MRW57_PAYOUT_CHECK;
				} else if (transaction.getPayoutMethod() == MoneyGramReceiveTransaction.PAYOUT_CASH) {
					nextPage = MRW60_REVIEW;
				} else if (transaction.isFormFreeTransaction() && (transaction.isFormFreeIdFields()) && (! currentPage.endsWith(MRW40_FORM_FREE_ID_VALIDATION))) {
					nextPage = MRW40_FORM_FREE_ID_VALIDATION;
				}
			
			} else if (transaction.isFormFreeTransaction() && (transaction.isFormFreeIdFields()) && (! currentPage.endsWith(MRW40_FORM_FREE_ID_VALIDATION))) {
				nextPage = MRW40_FORM_FREE_ID_VALIDATION;
			
			} else if (transaction.getValidationStatus().equals(DataCollectionStatus.VALIDATED)) {
				nextPage = MRW60_REVIEW;
			
			} else if (transaction.getValidationStatus().equals(DataCollectionStatus.RETRY)) {
				nextPage = currentPage;
	    	
			} else {
        		if (transaction.getDataCollectionData().isAdditionalInformationScreenValid()) {
        			nextPage = MRW90_COMMUNICATION_ERROR;
        		} else {
        			nextPage = MRW59_ADDITIONAL_RECEIVER_INFORMATION;
        		}
	    	}
		}
		return nextPage;
	}

	private String profileForwardPage() {
		String page;
		DataCollectionStatus status = transaction.getDataCollectionData().getValidationStatus();
		DataCollectionType dataType = transaction.getDataCollectionData().getDataCollectionType();
		if (status.equals(DataCollectionStatus.VALIDATED) || dataType.equals(DataCollectionType.VALIDATION)) {
			page = nextDataCollectionScreen(MRW20_RECEIVE_DETAIL);
		} else if (status.equals(DataCollectionStatus.NEED_MORE_DATA)) {
			page = MRW59_ADDITIONAL_RECEIVER_INFORMATION;
		} else {
			page = MRW90_COMMUNICATION_ERROR;
		}
		return page;
	}
}
