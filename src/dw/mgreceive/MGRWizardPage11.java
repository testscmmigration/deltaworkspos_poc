package dw.mgreceive;

import java.awt.event.KeyEvent;
import java.util.NoSuchElementException;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;

import dw.dialogs.Dialogs;
import dw.framework.ClientTransaction;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.profile.UnitProfile;
import dw.utility.ErrorManager;
import dw.utility.IdleBackoutTimer;

/**
 * Page 11 of the MoneyGram Receive Wizard.
 */
public class MGRWizardPage11 extends MoneyGramReceiveFlowPage {
	private static final long serialVersionUID = 1L;

	private JLabel serialNumberField;

	private JTextField userPinField;

	private int userID = 0;

	public MGRWizardPage11(MoneyGramReceiveTransaction tran, String name,
			String pageCode, PageFlowButtons buttons) {
		super(tran,
				"moneygramreceive/MGRWizardPage11.xml", name, pageCode, buttons); 
		serialNumberField = (JLabel) getComponent("serialNumberField"); 
		userPinField = (JTextField) getComponent("userPinField"); 
	}

	// Checks the profile to validate that the user has permission to load the
	// dispenser. 
	private boolean isValidUser() {
		try {
			if (userPinField == null) {
				return true;
			}
			userID = (UnitProfile.getInstance().getProfileInstance()
					.find(null, userPinField.getText())).find(
					"USER_ID").intValue(); 
			if (isCriteriaEntered()
					&& ClientTransaction.canLoadDispenser(userID)) {
				// The User Pin entered has valid privilages
				return true;
			} else {
				// The User Pin entered has invalid privilages, prompt message
				Dialogs.showWarning("dialogs/DialogDispenserInsufficient.xml"); 
				userPinField.setText(""); 
				return false;
			}
		} catch (NoSuchElementException e) {
			// The User Pin entered has invalid privilages, prompt message
			Dialogs.showWarning("dialogs/DialogDispenserInsufficient.xml"); 
			userPinField.setText(""); 
			return false;
		}
	}

	private boolean isCriteriaEntered() {
		return userPinField.getText().length() > 0;
	}

	public void setBoxStates() {
		flowButtons.setEnabled("next", isCriteriaEntered()); 
		flowButtons.setEnabled("cancel", true); 
	}

	@Override
	public void start(int direction) {
		// disable the screen timeout.
		IdleBackoutTimer.pause();

		userPinField.setText(""); 
		serialNumberField.setText(transaction.getDispenserJammedSerialNumber());

		flowButtons.reset();
		flowButtons.setVisible("expert", false); 
		flowButtons.setEnabled("expert", false); 
		flowButtons.setAlternateText("next", Messages.getString("MGRWizardPage11.reloadText"));  
		flowButtons.setAlternateKeyMapping("next", KeyEvent.VK_F5, 0); 

		flowButtons.setEnabled("back", false); 
		flowButtons.setEnabled("next", true); 
		flowButtons.setEnabled("cancel", true); 

		// set up the focus order
		JComponent[] focus = { userPinField, flowButtons.getButton("next"), 
				flowButtons.getButton("cancel") }; 
		setFocusOrder(focus);

		// start the focus order
		startFocusOrder();
	}

	private void startFocusOrder() {
		userPinField.requestFocus();
	}

	@Override
	public void finish(int direction) {
		if (direction == PageExitListener.CANCELED) {
			int result = Dialogs.showConfirmDgCancel(transaction
					.getReferenceNumber(), transaction
					.getSerialNumberFirstMoneyOrder(), transaction
					.getSerialNumberSecondMoneyOrder());
			if (result == Dialogs.YES_OPTION) {
				IdleBackoutTimer.start();
				// Print the Warning receipt if receipt enabled.
				if (transaction.isReceiptReceived()) {
					transaction.printReceipt(this);
				}

				userID = transaction.getUserID();

				// write out the report diag saying we have Canceled the Dispenser Load
				ErrorManager
						.writeGeneralDiagnostic("Load Dispenser - Canceled by user " + userID); 
			} else {
				// Set the ExitListener to BACK to cause the the same page to load again.
				direction = PageExitListener.BACK;
			}
		} else if (direction == PageExitListener.NEXT) {
			if (isValidUser()) {
				flowButtons.setButtonsEnabled(false);
				PageNotification.notifyExitListeners(this, direction);
			} else {
				this.start(PageExitListener.NEXT);
			}
		}

		if (direction != PageExitListener.NEXT) {
			flowButtons.setButtonsEnabled(false);
			PageNotification.notifyExitListeners(this, direction);
		}
	}
}
