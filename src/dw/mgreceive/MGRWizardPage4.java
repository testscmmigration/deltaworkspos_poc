package dw.mgreceive;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.DataCollectionField;
import dw.framework.DataCollectionScreenToolkit;
import dw.framework.DataCollectionSet;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;
import dw.utility.ExtraDebug;

/**
 * Page 6 of the MoneyGram Receive Wizard. This page prompts for the customer's
 *   photo ID information and additional information about the receiver (address, home phone).
 */
public class MGRWizardPage4 extends MoneyGramReceiveFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	private DataCollectionScreenToolkit toolkit;
	private Object dataCollectionSetChangeFlag;
	private DWTextPane line1Tp = null;
	private JScrollPane scrollPane1;
	private JScrollPane scrollPane2;
	private JPanel panel2;
	private JButton nextButton;

    public MGRWizardPage4(MoneyGramReceiveTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "moneygramreceive/MGRWizardPage4.xml", name, pageCode, buttons); // use same as MGS	
    	
    	this.scrollPane1 = (JScrollPane) getComponent("scrollPane1");
    	this.scrollPane2 = (JScrollPane) getComponent("scrollPane2");
    	this.panel2 = (JPanel) getComponent("scrollee"); 
        this.line1Tp = (DWTextPane) getComponent("line1");
    	this.scrollPane2 = (JScrollPane) getComponent("scrollPane2");
        this.line1Tp.addListener(this, this);
		nextButton = this.flowButtons.getButton("next");
    }

	@Override
	public void dwTextPaneResized() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				int width = Math.max(MGRWizardPage4.this.scrollPane2.getViewport().getPreferredSize().width, 500);
				MGRWizardPage4.this.line1Tp.setWidth(width);
			}
		});
	}

    private void setupComponents() {

		DWTextPane tp = (DWTextPane) getComponent("line1");
		tp.setText(Messages.getString("MGRWizardPage4.932"));

		// Check if the GFFP information has changed.
		
		DataCollectionSet data = this.transaction.getDataCollectionData();

		if (this.dataCollectionSetChangeFlag != this.transaction.getDataCollectionData().getChangeFlag()) {
	    	this.dataCollectionSetChangeFlag = this.transaction.getDataCollectionData().getChangeFlag();
	    	
	    	// Rebuild the data collection items for the screen.
	    	
	    	ExtraDebug.println("Rebuilding Screen " + getPageCode() + " from Fields to Collect");
	    	
			int flags = DataCollectionScreenToolkit.SHORT_LABEL_VALUES_FLAG + DataCollectionScreenToolkit.DISPLAY_REQUIRED_FLAG;
			JButton backButton = this.flowButtons.getButton("back");
			this.toolkit = new DataCollectionScreenToolkit(this, this.scrollPane1, this, this.scrollPane2, this.panel2, flags, transaction.getReceiverScreenStatus().getDataCollectionPanels(data), this.transaction, backButton, nextButton, null);
		}
		
		// Check if the screen needs to be laid out.
		
		if (transaction.getReceiverScreenStatus().needLayout(data)) {
			toolkit.populateDataCollectionScreen(panel2, transaction.getReceiverScreenStatus().getDataCollectionPanels(data), false);
		}
    }

	@Override
    public void start(int direction) {
		
		// Make sure the screen has been built.
		
		setupComponents();
		
		// Setup the flow buttons at the bottom of the screen.
		
		this.flowButtons.reset();
		this.flowButtons.setVisible("expert", false); 
		this.flowButtons.setEnabled("expert", false); 
		
		// Populate the screen with any previously entered data.
		
		if (direction == PageExitListener.NEXT || direction == PageExitListener.BACK) {
			DataCollectionScreenToolkit.restoreDataCollectionFields(this.toolkit.getScreenFieldList(), this.transaction.getDataCollectionData());
		}
		
		// Move the cursor to 1) the first required item with no value or item 
		// with an invalid value or 2) the first optional item with no value.
		
		this.toolkit.moveCursor(this.toolkit.getScreenFieldList(), nextButton);
    	
		// Enable and call the resize listener.
		
		toolkit.enableResize();
		this.toolkit.getComponentResizeListener().componentResized(null);
    }
    
	@Override
    public void finish(int direction) {
    	
    	// Disable the resize listener for this screen.
    	
		toolkit.disableResize();
	        
    	// If the flow direction is to the next screen, check the validity of the
    	// data. If a value is invalid or a required item does not have a value,
    	// return to the screen.
    	
    	if (direction == PageExitListener.NEXT) {
        	String tag = this.toolkit.validateData(this.toolkit.getScreenFieldList(), DataCollectionField.DISPLAY_ERRORS_VALIDATION);
        	if (tag != null) {
				this.flowButtons.setButtonsEnabled(true);
				toolkit.enableResize();
        		return;
        	}
        }
    	
    	// If the flow direction is to the next screen or back to the previous
    	// screen, save the value of the current items in a data structure.

        if ((direction == PageExitListener.BACK) || (direction == PageExitListener.NEXT)) {
        	DataCollectionScreenToolkit.saveDataCollectionFields(this.toolkit.getScreenFieldList(), this.transaction.getDataCollectionData());
        }
    	
    	// Check if receiveValidation could be performed now or if another page should be displayed.
    	
    	if (direction == PageExitListener.NEXT) {
        	transaction.setValidationStatus(DataCollectionStatus.VALIDATION_NOT_PERFORMED);
    		boolean performValidation = transaction.performValidation(MoneyGramReceiveWizard.MRW35_COMPLIANCE, this.transaction);
    		if (performValidation && transaction.getDataCollectionData().isAdditionalInformationScreenValid()) {
    			transaction.receiveValidation(this, ValidationType.SECONDARY);
    			return;
    		}
    	}
    	PageNotification.notifyExitListeners(this, direction);
    }

	@Override
	public void commComplete(int commTag, Object returnValue) {
		if (returnValue != null) {
			PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
		}
	}
}
