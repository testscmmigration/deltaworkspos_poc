/*
 * Copyright (c) 2001-2012 MoneyGram International
 */
package dw.mgreceive;

import java.awt.Color;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import com.moneygram.agentconnect.TransactionLookupResponse;

import dw.comm.MessageFacadeParam;
import dw.dialogs.Dialogs;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.dwgui.MoneyField;
import dw.framework.ClientTransaction;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.mgreversal.SendReversalTransaction;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.ReceiveDetailInfo;
import dw.model.adapters.ReceiverInfo;
import dw.utility.Debug;

/**
 * Page 7b of the MoneyGram Receive Wizard. This page allows the user to enter
 *   the information to determine how the receive will be payed out.
 */

/** NOTE.....
 *
 *  If DeltaWorks is to be internationalized, totalField, amount1Field, and
 *  amount2Field will need to be modified to show better currency precision than
 *  the current implementation of MoneyField allows.  It currently changes precision
 *  to two decimal places.
 */

public class MGRWizardPage7b extends MoneyGramReceiveFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	private MoneyField amount1Field;
    private JLabel currency1Label;
    private MoneyField totalField;
    private JTextField checkSerial1Field;
    private MoneyField checkAmount1Field;
    private JLabel checkCurrency1Label;
    private JPanel check1Panel;
    private MoneyField cashAmount1Field;
    private JLabel cashCurrency1Label;
    private JPanel cash1Panel;
    private JPanel check2Panel;
    private JTextField serial2Field;
    private MoneyField amount2Field;
    private JLabel currency2Label;
    private BigDecimal total;
    private boolean reversalType;
    private MoneyGramReceiveTransaction transaction;
    private SendReversalTransaction rTransaction;
    private JButton nextButton;
	List<String> list = null;
    private boolean serialNbrReq;

    private String receiveCurrency;

    public MGRWizardPage7b(MoneyGramReceiveTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "moneygramreceive/MGRWizardPage7b.xml", name, pageCode, buttons);   
        transaction = tran;
        reversalType = false;
        init();
    }

    public MGRWizardPage7b(SendReversalTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "moneygramreceive/MGRWizardPage7b.xml", name, pageCode, buttons);   
        transaction = tran;
        rTransaction = tran;
        reversalType = true;
        init();
    }
    
    private void init() {
        totalField = (MoneyField) getComponent("totalField");   
        check1Panel = (JPanel) getComponent("check1Panel");	
        checkSerial1Field = (JTextField) getComponent("checkSerial1Field");
        checkAmount1Field = (MoneyField) getComponent("checkAmount1Field");   
        checkCurrency1Label = (JLabel) getComponent("checkCurrency1Label");   
        cash1Panel = (JPanel) getComponent("cash1Panel");	
        cashAmount1Field = (MoneyField) getComponent("cashAmount1Field");   
        cashCurrency1Label = (JLabel) getComponent("cashCurrency1Label");   
        check2Panel = (JPanel) getComponent("check2Panel");	
        serial2Field = (JTextField) getComponent("serial2Field");
        amount2Field = (MoneyField) getComponent("amount2Field");   
        currency2Label = (JLabel) getComponent("currency2Label");   
        nextButton = flowButtons.getButton("next");   
        DWTextPane tp = (DWTextPane) getComponent("text1");
        tp.addListener(this, this);
    }

	@Override
	public void dwTextPaneResized() {
		DWTextPane tp = (DWTextPane) getComponent("text1");
		JPanel panel = (JPanel) getComponent("check1Panel");
		int width = panel.getPreferredSize().width + 100;
		tp.setWidth(width);
		tp.setListenerEnabled(false);
	}

    @Override
	public void start(int direction) {
        flowButtons.reset();
        flowButtons.setVisible("expert", false);	
        list = transaction.currencyForTransferCheck();
        amount2Field.setBackground(SystemColor.control);
        serialNbrReq = transaction.isProfileUseOurPaper() && isValidCurrencyAndCount();
        DWTextPane tp = (DWTextPane) getComponent("text1");

        if (serialNbrReq) {
        	amount1Field = checkAmount1Field;
        	currency1Label = checkCurrency1Label;
            amount1Field.setInputVerifier(new amountVerifier());
            check1Panel.setEnabled(true);
            check1Panel.setVisible(true);
            cash1Panel.setEnabled(false);
            cash1Panel.setVisible(false);
            check2Panel.setEnabled(true);
    		String part1 =Messages.getString("MGRWizardPage7b.100");
    		StringBuffer sb = new StringBuffer(part1);
    		sb.append(" ");
        	sb.append(Messages.getString("MGRWizardPage7b.110"));
            addActionListener("serial2Field", this, "focusNext");
            if (serial2Field.getText().length() == 0) {
                serial2Field.setEnabled(false);
                serial2Field.setBackground(SystemColor.control);
                amount2Field.setEnabled(false);
            }
            tp.setText(sb.toString().trim()); 

        } else {
            checkSerial1Field.setText("");
        	amount1Field = cashAmount1Field;
        	currency1Label = cashCurrency1Label;
            check1Panel.setEnabled(false);
            check1Panel.setVisible(false);
            cash1Panel.setEnabled(true);
            cash1Panel.setVisible(true);
    		String part1 = Messages.getString("MGRWizardPage7b.100");
    		StringBuffer sb = new StringBuffer(part1);
    		sb.append(" ");
            checkSerial1Field.setEnabled(false);
            checkSerial1Field.setBackground(SystemColor.control);
            amount1Field.setEnabled(false);
            amount1Field.setBackground(SystemColor.control);
            serial2Field.setEnabled(false);
            serial2Field.setBackground(SystemColor.control);
            amount2Field.setEnabled(false);
            tp.setText(sb.toString().trim()); 
            nextButton.requestFocus(true);
        }

        if (transaction.getReceiveDetail() == null) {
            transaction.createNewReceiveDetailInfo();
        } 

        if (reversalType) {
        	TransactionLookupResponse.Payload payload = rTransaction.getTransactionLookupResponse().getPayload().getValue();
            BigDecimal sendAmount = payload.getSendAmounts().getSendAmount();
            BigDecimal totalAmount = payload.getSendAmounts().getTotalAmountToCollect();
            
            if (rTransaction.getRefundFee()) {
                transaction.getReceiveDetail().setReceiveAmount(totalAmount);
                rTransaction.setAgentAmount(totalAmount);
            } else {
                transaction.getReceiveDetail().setReceiveAmount(sendAmount);
                rTransaction.setAgentAmount(sendAmount);
            }
            ReceiveDetailInfo detail = rTransaction.getReceiveDetail();
            ReceiverInfo ri = new ReceiverInfo();
            ri.setFirstName(detail.getSenderFirstName());
            ri.setMiddleName(detail.getSenderMiddleName());
            ri.setLastName(detail.getSenderLastName());
            ri.setSecondLastName(detail.getSenderLastName2());
            ri.setHomePhone(detail.getSenderHomePhone());
            transaction.setReceiver(ri);
            transaction.setReferenceNumber(rTransaction.getReversalReferenceNumber());
            transaction.getReceiveDetail().setReceiveCurrency(payload.getSendAmounts().getSendCurrency());
        }

        receiveCurrency = transaction.getCurrency();
        /*
         * Set all the MoneyFields to use the receive currency, just in case it
         * is different from the base currency.
         */
        if (CountryInfo.getAgentCountry().getBaseReceiveCurrency().compareToIgnoreCase(receiveCurrency) != 0) {
            totalField.setCurrency(receiveCurrency);
            checkAmount1Field.setCurrency(receiveCurrency);
            cashAmount1Field.setCurrency(receiveCurrency);
            amount2Field.setCurrency(receiveCurrency);
        }

        if (reversalType) {
        	total = transaction.getAgentAmount();
        } else {
        	total = transaction.getAmount();
        }
        
        totalField.setText(total.toString());
        amount1Field.setText(total.toString());

        // override the defaults with the new values if they have been entered
        if (transaction.getAgentAmount() != null && serial2Field.getText().length() > 0) {
            amount1Field.setText(transaction.getAgentAmount().toString());
        }
        updateAmounts(amount1Field);

        if (serialNbrReq) {
            SwingUtilities.invokeLater(new Runnable() {@Override
			public void run() {
                BigDecimal amount1 = new BigDecimal(amount1Field.getText());
                if (amount1.compareTo(BigDecimal.ZERO) > 0) {
                    checkSerial1Field.requestFocus(true);
                } else {
                	amount1Field.requestFocus(true);
                }
            }});
        } else {
            finish(direction);
        }

        addKeyListener(amount1Field, nextAmt1);
    }

	KeyListener nextAmt1 = new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				e.consume();
				Debug.println("  Enter Pressed-1"); 
				if (updateAmounts(amount1Field)) {
					// amount 1 is zero
					BigDecimal amount = new BigDecimal(amount1Field.getText());
					if (amount.compareTo(BigDecimal.ZERO) != 0) {
						if (serialNbrReq && checkSerial1Field.getText().length() == 0) {
							checkSerial1Field.requestFocus();
						} else {
							flowButtons.getButton("next").requestFocus();
						}
					}
					// amount 1 exists and is less than the total amount
					else {
						if (serialNbrReq && checkSerial1Field.getText().length() == 0) {
							checkSerial1Field.requestFocus();
						} else {
							if (serialNbrReq && serial2Field.getText().length() == 0) {
								serial2Field.requestFocus();
							} else {
								flowButtons.getButton("next").requestFocus();
							}
						}
					}
				}
			}
		}
	};

    private void displayInvalidDialog() {

        if(list.size() >0){
        if(list.size()==1){
        	String currency =list.get(0);

        	if(currency.equals("USD")){
              Dialogs.showWarning("dialogs/DialogInvalidSerialNumber.xml",    
              Messages.getString("DialogInvalidSerialNumberUSA.1"));
        	} else
        		if(currency.equals("CAD")){
                  Dialogs.showWarning("dialogs/DialogInvalidSerialNumber.xml",    
                                   Messages.getString("DialogInvalidSerialNumberCAN.1"));
        		}
        		 else
        	          Dialogs.showWarning("dialogs/DialogInvalidSerialNumber.xml",    
        	          Messages.getString("DialogInvalidSerialNumberOTHER.1"));

        }
        else if(list.size()>1)
        	     Dialogs.showWarning("dialogs/DialogInvalidSerialNumber.xml",    
      	          Messages.getString("DialogInvalidSerialNumberOTHER.1"));
    }
    }
    private boolean isValidCurrencyAndCount(){
    	 boolean retVal = false;
         //java.util.List list= transaction.CurrencyForTransferCheck();
         if(list.size() >0){
         if(list.size()==1){
         	String currency =list.get(0);
         	if((currency.equals("USD")||
         			(currency.equals("CAD"))
         			)){
         		retVal = true;
         	}
         }
         }

         return retVal;

    }
    private boolean validSerialNumber(String serial){
    	// validate that an agent with single payout currency USD has  checks starting  with 24 or 99,
        // validate that an agent with single payout currency CAD has  checks starting  with 58 or 99, ,
        // validate that all other currency and multiple receive  checks start with 99.
        boolean retVal = false;
        //java.util.List list= transaction.CurrencyForTransferCheck();
        if(list.size() >0){
        if(list.size()==1){
        	String currency =list.get(0);
        	if((currency.equals("USD") && serial.startsWith("24"))||
        			(currency.equals("CAD") && (serial.startsWith("58"))
        			)){
        		retVal = true;
        	}
        	else if(!currency.equals("USD")&& !currency.equals("CAD") && serial.startsWith("99")){
        		retVal = true;
        	}
        	else
        		 displayInvalidDialog();
        }
        else if(list.size() > 1){
        	if(serial.startsWith("99")){

        		retVal = true;
        	}
        	else
        		 displayInvalidDialog();
        }
        }
        return retVal;
    }

    public boolean serialOneValid(){
        boolean retValue = true;
        if (transaction.isProfileUseOurPaper()&& isValidCurrencyAndCount()){
            String serial = checkSerial1Field.getText();

            if (serial.length() != 12) {
                displayInvalidDialog();
                Toolkit.getDefaultToolkit().beep();
                checkSerial1Field.setText("");
                checkSerial1Field.requestFocus(true);
                retValue = false;
            } else {
                if (!validSerialNumber(serial)){
                    checkSerial1Field.setText("");
                    checkSerial1Field.requestFocus(true);
                    retValue = false;
                }
            }
        }
        return retValue;
    }

    public boolean serialTwoValid(){
        boolean retValue = true;
        if (transaction.isProfileUseOurPaper()&& isValidCurrencyAndCount()){
            String serial = serial2Field.getText();
            if (serial.length() == 0){
                BigDecimal amount = new BigDecimal(amount2Field.getText());
                if (amount.floatValue() > 0.000){
                    displayInvalidDialog();
                    Toolkit.getDefaultToolkit().beep();
                    retValue = false;
                }
            }else {
                if (serial.length() != 12) {
                    displayInvalidDialog();
                    Toolkit.getDefaultToolkit().beep();
                    serial2Field.setText("");
                    serial2Field.requestFocus(true);
                    retValue = false;
                }else {
                    if (!validSerialNumber(serial)){
                        serial2Field.setText("");
                        serial2Field.requestFocus(true);
                        retValue = false;
                    }
                }
            }
        }
        return retValue;
    }

	/**
	 * Update the amount fields to add up to the total value.
	 */
	public boolean updateAmounts(JComponent input) {
		boolean validInput = true;
		final int lastField = (input.equals(amount1Field) ? 1 : 3);
		BigDecimal amount1 = new BigDecimal(amount1Field.getText());
		BigDecimal amount2 = BigDecimal.ZERO;
		currency1Label.setText(receiveCurrency);

		Debug.println("updateAmounts-" + lastField); 

		amount1Field.setEnabled(true);
		amount1Field.setEditable(true);
		// make sure amount 1 <= total
		if (amount1.compareTo(total) > 0) {
			amount1 = new BigDecimal(total.toString());
			validInput = false;
			if (!checkSerial1Field.isEnabled() && serialNbrReq) {
				checkSerial1Field.setEnabled(true);
				checkSerial1Field.setEditable(true);
				checkSerial1Field.setBackground(Color.white);
			}
			serial2Field.setText("");
			serial2Field.setEnabled(false);
			serial2Field.setBackground(SystemColor.control);
		}
		// amount 1 equals the total amount
		else if (amount1.compareTo(total) == 0) {
			if (!checkSerial1Field.isEnabled() && serialNbrReq) {
				checkSerial1Field.setEnabled(true);
				checkSerial1Field.setEditable(true);
				checkSerial1Field.setBackground(Color.white);
			}
			serial2Field.setText("");
			serial2Field.setEnabled(false);
			serial2Field.setBackground(SystemColor.control);
		}
		// amount 1 equals 0.0
		else if (amount1.compareTo(BigDecimal.ZERO) == 0) {
			checkSerial1Field.setText("");
			checkSerial1Field.setEnabled(false);
			checkSerial1Field.setBackground(SystemColor.control);
			serial2Field.setText("");
			serial2Field.setEnabled(false);
			serial2Field.setBackground(SystemColor.control);
			amount2Field.setEnabled(false);
			amount2Field.setEditable(false);
		}
		// amount 1 exists and is less than the total amount
		else {
			if (serialNbrReq) {
				if (!checkSerial1Field.isEnabled() && serialNbrReq) {
					checkSerial1Field.setEnabled(true);
					checkSerial1Field.setEditable(true);
					checkSerial1Field.setBackground(Color.white);
				}
				serial2Field.setEnabled(true);
				serial2Field.setBackground(Color.white);
				// amount2Field.setBackground(Color.white);

				// set balance in amount2 field
				amount2 = total.subtract(amount1);
				currency2Label.setText(receiveCurrency);
				serial2Field.setEnabled(true);
			}
			/*
			 * No serial number required (net settle)
			 */
			else {
				amount1 = new BigDecimal(total.toString());
				validInput = false;
			}
		}
		amount1Field.setText(amount1.toString());
		amount2Field.setText(amount2.toString());

		Debug.println("  ValidInput[" + validInput + "]"); 
		return validInput;

	}

    class amountVerifier extends InputVerifier {
    	  @Override
		public boolean verify(JComponent input) {
    	    return updateAmounts(input);
    	  }
    }

    public void focusNext(){
		Debug.println("  Request Next"); 
        flowButtons.getButton("next").requestFocus(true);
    }

    @Override
	public void finish(int direction) {
        flowButtons.disable();

        BigDecimal amount1 = new BigDecimal(amount1Field.getText());

        // do not allow the user to enter an amount of 0 for the agent's MO.
        if (amount1.compareTo(BigDecimal.ZERO) == 0) {
            if (direction != PageExitListener.NEXT) {
                PageNotification.notifyExitListeners(this, direction);
                return;
            }
            Toolkit.getDefaultToolkit().beep();
            this.start(direction);
            return;
        }
        
    	if (! serialNbrReq) {
            checkSerial1Field.setText("99888888888");
    	}
        transaction.setAgentAmountOnly(amount1);
        
        BigInteger checkSerial1 = null;
        try {
        	checkSerial1 = new BigInteger(checkSerial1Field.getText());
        } catch (Exception e) {
        }
        transaction.setAgentCheckNumber(checkSerial1);
        
        BigDecimal amount2 = null;
        try {
        	amount2 = new BigDecimal(amount2Field.getText());
        } catch (Exception e) {
        }
        transaction.setCustomerAmountOnly(amount2);

        BigInteger serial2 = null;
        try {
        	serial2 = new BigInteger(serial2Field.getText());
        } catch (Exception e) {
        }
        transaction.setCustomerCheckNumber(serial2);
        
        if (direction == PageExitListener.NEXT) {
            updateAmounts(amount1Field);
        	/*
        	 * If a check is being written on our paper, verify the
        	 * serial number entered.
        	 */
        	
        	if (serialNbrReq && (amount1.compareTo(BigDecimal.ZERO) != 0)) {
                if (! serialOneValid() || ! serialTwoValid()) {
                    this.start(direction);
                    return;
                } 
            }
        	
        	if (! reversalType) {
                boolean statusFlag = (transaction.getStatus() == ClientTransaction.IN_PROCESS || transaction.getStatus() == MessageFacadeParam.INVALID_CHECK_NUMBER);
                if (statusFlag && transaction.getDataCollectionData().isAdditionalInformationScreenValid()) {
       				transaction.receiveValidation(this, ValidationType.SECONDARY);
    				return;
                }
        	} else {
        		rTransaction.sendReversalValidation(this, ValidationType.SECONDARY);
				return;
        	}
        }
       	PageNotification.notifyExitListeners(this, direction);
    }

	@Override
	public void commComplete(int commTag, Object returnValue) {
		if (returnValue != null) {
			PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
		}
	}
}
