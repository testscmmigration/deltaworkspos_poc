package dw.mgreceive;

import java.awt.event.KeyEvent;
import java.util.NoSuchElementException;

import javax.swing.JComponent;
import javax.swing.JTextField;

import dw.dialogs.Dialogs;
import dw.dispenser.DispenserFactory;
import dw.dispenser.DispenserInterface;
import dw.framework.ClientTransaction;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.profile.UnitProfile;
import dw.utility.ErrorManager;
import dw.utility.IdleBackoutTimer;


/**
 * Page 13 of the MoneyGram Receive Wizard.
 */
public class MGRWizardPage13 extends MoneyGramReceiveFlowPage {
	private static final long serialVersionUID = 1L;

	private JTextField userPinField;
    private int userID = 0;
    
    public MGRWizardPage13(MoneyGramReceiveTransaction tran,
                       String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "moneygramreceive/MGRWizardPage13.xml", name,pageCode,  buttons);	
        
        userPinField = (JTextField) getComponent("userPinField");	
    }

    // Checks the profile to validate that the user has permission to load the
    // dispenser. 
	private boolean isValidUser() {
         try {
             userID = (UnitProfile.getInstance().getProfileInstance().find(null,
                     userPinField.getText())).find("USER_ID").intValue();	

             if (ClientTransaction.canLoadDispenser(userID)) {
                 // The User Pin entered has valid privilages
                 return true;
             }
             else {
                 // The User Pin entered has invalid privilages, prompt message
                 Dialogs.showWarning("dialogs/DialogDispenserInsufficient.xml");	
                 userPinField.setText("");	
                 return false;
             }
         }
         catch (NoSuchElementException e) {
             // The User Pin entered has invalid privilages, prompt message
             Dialogs.showWarning("dialogs/DialogDispenserInsufficient.xml");	
             userPinField.setText("");	
             return false;
         }
    }

    @Override
	public void start(int direction) {        

       // disable the screen timeout.
       IdleBackoutTimer.pause();

       userPinField.setText("");	

//       dispenserLoadTransaction = new DispenserLoadTransaction(
//                    Resources.getString("MGRWizardPage13.Dispenser_Load_1")); 
//
        flowButtons.reset();
        flowButtons.setVisible("expert", false);	
        flowButtons.setEnabled("expert", false);	
        flowButtons.setAlternateText("next", Messages.getString("MGRWizardPage13.loadText"));	 
        flowButtons.setAlternateKeyMapping("next", KeyEvent.VK_F5, 0);	
        
        flowButtons.setEnabled("back", false);	
        flowButtons.setEnabled("next", true);	
        flowButtons.setEnabled("cancel", true);	

        // set up the focus order
        JComponent[] focus = {userPinField,
                              flowButtons.getButton("next"),	
                              flowButtons.getButton("cancel")};	
        setFocusOrder(focus);

        // start the focus order
        startFocusOrder();
    }

    private void startFocusOrder() {
        flowButtons.getButton("cancel").requestFocus();	
        userPinField.requestFocus();
    }

    private void resetDispenserReady() {
        // Initialize the dispenser status bits
        DispenserInterface dispenser = DispenserFactory.getDispenser(1);
        dispenser.verified();
    }
 
    @Override
	public void finish(int direction) {   
        if (direction == PageExitListener.CANCELED) {
                int result = Dialogs.showConfirmDgCancel(
                        transaction.getReferenceNumber(), 
                        transaction.getSerialNumberFirstMoneyOrder(),
                        transaction.getSerialNumberSecondMoneyOrder());
                if (result == Dialogs.YES_OPTION) {

                    // Initialize the dispenser status bits.
                    resetDispenserReady();
                    
                    // Remove extra records 
//                    transaction.commitPsuedoSetIgnore();
                    
                    // Void all remaining forms
                    //voidRemainingForms();
                    
                    // Cancel the transacitons
//                    transaction.commitPsuedoCancelSend();
                    
                    // Process the data.
//                    TempLog.commitTempLog();
                    
                    IdleBackoutTimer.start();
                    
                    userID = transaction.getUserID();
                    // write out the report diag saying we have Canceled the Dispenser Load
                    ErrorManager.writeGeneralDiagnostic("Load Dispenser - Canceled by user " + userID); 
                    // Print the Warning Receipt.
                    transaction.printWarningReceipt(this);
                }
                else {
                   // Set the ExitListener to BACK to cause the the same page to load again.
                   direction = PageExitListener.BACK;
                }
        }
        else if (direction == PageExitListener.NEXT){
            // Initialize the dispenser status bits.
            resetDispenserReady();
            
            // Check if the user can load the dispenser.

			if (isValidUser()) {
				flowButtons.setButtonsEnabled(false);
				PageNotification.notifyExitListeners(this, direction);
			} else {
				this.start(PageExitListener.NEXT);
			}
        }
        
        if (direction != PageExitListener.NEXT) {   
            flowButtons.setButtonsEnabled(false);
            PageNotification.notifyExitListeners(this, direction);
        }
    }
}
