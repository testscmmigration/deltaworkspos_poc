package dw.mgreceive;

import javax.swing.JLabel;

import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.mgreversal.SendReversalTransaction;
import dw.utility.IdleBackoutTimer;

/**
 * Page 14 of the MoneyGram Receive Wizard.
 */
public class MGRWizardPage14 extends MoneyGramReceiveFlowPage {
	private static final long serialVersionUID = 1L;

	private JLabel referenceNumberField;    
    
    public MGRWizardPage14(MoneyGramReceiveTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "moneygramreceive/MGRWizardPage14.xml", name,pageCode,  buttons);   
        referenceNumberField = (JLabel) getComponent("referenceNumberField");   
    }

    public MGRWizardPage14(SendReversalTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "moneygramreceive/MGRWizardPage14.xml", name,pageCode,  buttons);   
        referenceNumberField = (JLabel) getComponent("referenceNumberField");   
    }

    @Override
	public void start(int direction) {
		if (transaction != null && transaction.getReferenceNumber() != null) {
			referenceNumberField.setText(transaction.getReferenceNumber());

			// disable the screen timeout.
			IdleBackoutTimer.pause();

			flowButtons.reset();
			flowButtons.setVisible("expert", false); 
			flowButtons.setEnabled("expert", false); 

			flowButtons.setEnabled("back", false); 
			flowButtons.setEnabled("next", false); 
			flowButtons.setEnabled("cancel", true); 
		} else {
			// allow cancellation on communication warning display
			flowButtons.setAlternateText("cancel", Messages.getString("OK"));  
			flowButtons.setSelected("cancel"); 
			flowButtons.setEnabled("cancel", true); 
		}
    }

    @Override
	public void finish(int direction) {
        
        if ((transaction != null) && (transaction.isReceiptReceived()) && (transaction.getValidationStatus().equals(DataCollectionStatus.VALIDATED)) && (transaction.getCompleteSessionResponse() == null)) {
            transaction.printReceipt(this);
        }
            
        PageNotification.notifyExitListeners(this, direction);
    }
}
