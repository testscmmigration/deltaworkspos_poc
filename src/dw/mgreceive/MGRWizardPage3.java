package dw.mgreceive;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import com.moneygram.agentconnect.Response;
import com.moneygram.agentconnect.TransactionStatusType;

import dw.comm.AgentConnect;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.dwgui.IndicativeBox;
import dw.framework.CommCompleteInterface;
import dw.framework.DataCollectionField;
import dw.framework.FieldKey;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.DataCollectionSet.DataCollectionType;
import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.mgsend.MoneyGramClientTransaction.ProfileStatus;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;
import dw.model.adapters.ConsumerProfile;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.CountryInfo.Country;
import dw.pinpad.Pinpad;
import dw.utility.DWValues;
import dw.utility.ReceiptTemplates;

/**
 * Page 3 of the MoneyGram Receive Wizard. This page displays the details of the
 *   selected transaction. If the transaction is not 'OK to receive', the user
 *   will not be allowed to continue.
 */
public class MGRWizardPage3 extends MoneyGramReceiveFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	private static final float FLOAT_ZERO = 0.0F;
	
    private DWTextPane statusPane;
    private JLabel indicativeLabel;
    private JPanel canceledPanel;
    private JPanel reasonPanel;
    private JPanel infoPanel;
    private JLabel infoLabel1;
    private JLabel infoLabel2;
    private IndicativeBox indicativeBox;
    private boolean readyStatus = false;
	private DWTextPane line1Tp = null;
    private boolean okForPickup;
	private List<DWTextPane> textPanes;
	private int tagWidth;

    public MGRWizardPage3(MoneyGramReceiveTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "moneygramreceive/MGRWizardPage3.xml", name, pageCode, buttons);	
		textPanes = new ArrayList<DWTextPane>();

		statusPane = (DWTextPane) getComponent("statusPane");	
        indicativeLabel = (JLabel) getComponent("indicativeLabel"); 
        canceledPanel = (JPanel) getComponent("canceledPanel");	
        reasonPanel = (JPanel) getComponent("reasonPanel");	
        infoPanel = (JPanel) getComponent("infoPanel");	
        infoLabel1 = (JLabel) getComponent("infoLabel1");	
        infoLabel2 = (JLabel) getComponent("infoLabel2");	
        indicativeBox = (IndicativeBox) getComponent("indicativeBox");
        line1Tp = (DWTextPane) getComponent("line1");

        line1Tp.addListener(this, this);
		
        this.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						scrollpaneResized();
					}
				});
			}
        });
    }

	private void scrollpaneResized() {
		dwTextPaneResized();
		
		JScrollPane sp1 = (JScrollPane) this.getComponent("scrollPane1");
		JScrollPane sp2 = (JScrollPane) this.getComponent("scrollPane2");
		JPanel panel2 = (JPanel) this.getComponent("panel1");
		this.scrollpaneResized(sp1, sp2, panel2);
	}

    @Override
	public void start(int direction) {
		flowButtons.reset();
		flowButtons.setVisible("expert", false); 
		readyStatus = transaction.getTransactionLookupResponse().getPayload().getValue().getTransactionStatus().equals(TransactionStatusType.AVAIL);
		okForPickup = readyStatus;
		
		if (direction == PageExitListener.NEXT) {
			transaction.setDataCollecionReview(false);
		}
		
		Map<String, String> dataMap = transaction.getSearchProfilesData().getCurrentValueMap();
		
       	standardSetup(dataMap);
        checkForPending();
    	dwTextPaneResized();
    	scrollpaneResized();
    }
    
	@Override
	public void dwTextPaneResized() {
		line1Tp.setWidth(FIELD_TOTAL_WIDTH);

		int w = FIELD_TOTAL_WIDTH - tagWidth;
		for (DWTextPane pane : textPanes) {
			pane.setWidth(w);
		}
	}

    private void standardSetup(Map<String, String> dataMap) {
		String labelType = "";

		String countryName = transaction.getDefaultCountry();
		transaction.getReceiveDetail().setReceiverCountry(countryName);
		
		// Determine the length of the label field.
		
		List<String> labels = new ArrayList<String>();
		labels.add(Messages.getString("MGRWizardPage3.920"));
		labels.add(Messages.getString("MGRWizardPage3.918"));
		labels.add(transaction.getTransactionLookupInfoLabel("dateTimeSent"));
    	List<JLabel> tags = new ArrayList<JLabel>();
		
        //---------------------------------------------------------------------
        // Transaction Information
        //---------------------------------------------------------------------

		// Status
		
		String status = getTranactionStatus(transaction.getTransactionLookupResponse().getPayload().getValue().getTransactionStatus(), transaction.getDataCollectionData().getCurrentValueMap(), true, transaction.isFormFreeTransaction());
		this.displayPaneItem("status", status, false, tags, textPanes);
		
		// Reference Number

		String referenceNumber = Pinpad.INSTANCE.getIsPinpadRequired() ? "********" : transaction.getReferenceNumber();
		this.displayLabelItem("reference", referenceNumber, tags);
		
		// Total Amount
		
		BigDecimal amount = transaction.getAmount();
		this.displayMoney("amount", amount, transaction.getCurrency(), tags);
		
		// Transaction Sent Time
		
		String tag = transaction.getTransactionLookupInfoLabel("dateTimeSent");
		String s = transaction.getSearchProfilesData().getCurrentValue("dateTimeSent");
        this.displayLabelItem("sent", tag, FormatSymbols.formatDateTimeForLocaleWithTimeZone(s), tags);
        
		// Send Country
		
		String countryCode = dataMap.get(FieldKey.ORIGINATINGCOUNTRY_KEY.getInfoKey());
		tag = transaction.getTransactionLookupInfoLabel(FieldKey.ORIGINATINGCOUNTRY_KEY.getInfoKey());
		Country country = CountryInfo.getCountry(countryCode);
		this.displayLabelItem("sendCountry", tag, country != null ? country.getPrettyName() : null, tags);
    	
        //---------------------------------------------------------------------
        // Receiver Information
        //---------------------------------------------------------------------

		// Receiver Name
        
        String firstName = dataMap.get(FieldKey.RECEIVER_FIRSTNAME_KEY.getInfoKey());
        String middleName = dataMap.get(FieldKey.RECEIVER_MIDDLENAME_KEY.getInfoKey());
        String lastName1 = dataMap.get(FieldKey.RECEIVER_LASTNAME_KEY.getInfoKey());
        String lastName2 = dataMap.get(FieldKey.RECEIVER_LASTNAME2_KEY.getInfoKey());
		String name = DWValues.formatName(firstName, middleName, lastName1, lastName2);
		this.displayPaneItem("receiverName", Messages.getString("MGRWizardPage3.916"), name, tags, textPanes);
		
		// Message

		String message1 = dataMap.get(FieldKey.MESSAGEFIELD1_KEY.getInfoKey());
		String message2 = dataMap.get(FieldKey.MESSAGEFIELD2_KEY.getInfoKey());
        StringBuffer sb = new StringBuffer();
        if ((message1 != null) && (! message1.isEmpty())) {
        	sb.append(message1);
        }
        if ((message2 != null) && (! message2.isEmpty())) {
        	sb.append(" ");
        	sb.append(message2);
        }
		this.displayPaneItem("message", sb.toString(), tags, textPanes);
		
		// Edit Button
		
        //---------------------------------------------------------------------
        // Sender Information
        //---------------------------------------------------------------------
		
		// Sender Name

    	firstName = dataMap.get(FieldKey.SENDER_FIRSTNAME_KEY.getInfoKey());
    	middleName = dataMap.get(FieldKey.SENDER_MIDDLENAME_KEY.getInfoKey());
    	lastName1 = dataMap.get(FieldKey.SENDER_LASTNAME_KEY.getInfoKey());
    	lastName2 = dataMap.get(FieldKey.SENDER_LASTNAME2_KEY.getInfoKey());
    	String senderName = DWValues.formatName(firstName, middleName, lastName1, lastName2);
		this.displayPaneItem("senderName", Messages.getString("MGRWizardPage3.913"), senderName, tags, textPanes);
    	
		// Sender Home Phone 
		
		String homePhone = dataMap.get(FieldKey.SENDER_PRIMARYPHONE_KEY.getInfoKey());
		tag = transaction.getTransactionLookupInfoLabel(FieldKey.SENDER_PRIMARYPHONE_KEY.getInfoKey());
		this.displayLabelItem("homePhone", tag, homePhone, tags);
		
		// Sender Address
		
		String address1 = dataMap.get(FieldKey.SENDER_ADDRESS_KEY.getInfoKey());
		String address2 = dataMap.get(FieldKey.SENDER_ADDRESS2_KEY.getInfoKey());
		String address3 = dataMap.get(FieldKey.SENDER_ADDRESS3_KEY.getInfoKey());
		String address4 = dataMap.get(FieldKey.SENDER_ADDRESS4_KEY.getInfoKey());
    	String address = DWValues.getAddress(address1, address2, address3, address4);
		this.displayPaneItem("senderAddress", Messages.getString("MGRWizardPage3.958b"), address, tags, textPanes);
		
		// Sender City

		tag = transaction.getTransactionLookupInfoLabel(FieldKey.SENDER_CITY_KEY.getInfoKey());
		String city = dataMap.get(FieldKey.SENDER_CITY_KEY.getInfoKey());
		this.displayLabelItem("senderCity", tag, city, tags);
		
		// Sender State

		tag = transaction.getTransactionLookupInfoLabel(FieldKey.SENDER_COUNTRY_SUBDIVISIONCODE_KEY.getInfoKey());
		String state = dataMap.get(FieldKey.SENDER_COUNTRY_SUBDIVISIONCODE_KEY.getInfoKey());
		this.displayLabelItem("senderState", tag, state, tags);

		// Sender Postal Code
		
		tag = transaction.getTransactionLookupInfoLabel(FieldKey.SENDER_POSTALCODE_KEY.getInfoKey());
		String zipCode = dataMap.get(FieldKey.SENDER_POSTALCODE_KEY.getInfoKey());
		this.displayLabelItem("senderZipCode", tag, zipCode, tags);
		
		// Sender Country

		tag = transaction.getTransactionLookupInfoLabel(FieldKey.SENDER_COUNTRY_KEY.getInfoKey());
		countryCode = dataMap.get(FieldKey.SENDER_COUNTRY_KEY.getInfoKey());
		country = CountryInfo.getCountry(countryCode);
		this.displayLabelItem("senderCountry", tag, country != null ? country.getPrettyName() : null, tags);
		
		//----------------------------------------------------------------------
		/*
		 * If all the required indicative information was returned, then display the indicative box.
		 */
		String indicativeExchangeRateLabel = transaction.getDataCollectionData().getCurrentValue(DWValues.INDICATIVE_EXCHANGE_RATE);
		String indicativeReceiveAmount = transaction.getDataCollectionData().getCurrentValue(DWValues.INDICATIVE_RECEIVE_AMOUNT);
		String indicativeReceiveCurrency = transaction.getDataCollectionData().getCurrentValue(DWValues.INDICATIVE_RECEIVE_CURRENCY); 
		float indicativeReceiveAmt;
		try {
			indicativeReceiveAmt = Float.parseFloat(indicativeReceiveAmount);
		} catch (Exception e) {
			indicativeReceiveAmt = FLOAT_ZERO;
		}
		if ( (indicativeExchangeRateLabel != null && indicativeExchangeRateLabel.length() > 0) 
				&& (indicativeReceiveAmount != null && indicativeReceiveAmount.length() > 0 && indicativeReceiveAmt >  FLOAT_ZERO)

				&& (indicativeReceiveCurrency != null && indicativeReceiveCurrency.length() > 0)){
			indicativeBox.setVisible(true);
			indicativeLabel.setText(Messages.getString("IndicativeBox.3")
					+ " "
					+ indicativeReceiveAmount
					+ " "
					+ indicativeReceiveCurrency
					+ " = "
					+ FormatSymbols.convertDecimal(transaction.getAmount().toString()) + " " + transaction.getCurrency() + " "
					+ Messages.getString("IndicativeBox.4") + " "
					+ indicativeExchangeRateLabel);

		} else {
			indicativeBox.setVisible(false);
		}

		// set status-specific values and instruction labels

		String part1 = readyStatus ? Messages.getString("MGRWizardPage3.okLine1") : Messages.getString("MGRWizardPage3.badLine1"); 
		String part2 = readyStatus ? Messages.getString("MGRWizardPage3.okLine2") : Messages.getString("MGRWizardPage3.badLine2"); 
		
		sb = new StringBuffer(part1);
		sb.append(" ");
		sb.append(part2);
		DWTextPane tp = (DWTextPane) getComponent("line1");
		tp.setText(sb.toString().trim()); 

		labelType = readyStatus ? "ok" : "bad";  
		reasonPanel.setVisible(!labelType.equals("ok")); 
		canceledPanel.setVisible(!labelType.equals("ok")); 
		
		// Send Purpose of Transaction
		
		String purpose = transaction.getSenderPurposeOfTransaction();
		this.displayLabelItem("purpose", purpose, tags);
		
		tagWidth = this.setTagLabelWidths(tags, MINIMUM_TAG_WIDTH);
    }

    private void checkForPending() {
		infoLabel1.setText("");
		infoLabel2.setText("");
		if (readyStatus) { 
			flowButtons.setSelected("next"); 
			infoPanel.setEnabled(false);
			if (! okForPickup) {
				
				String deliveryOption = transaction.getDataCollectionData().getCurrentValue(FieldKey.SERVICEOPTION_KEY.getInfoKey());
				
				if (deliveryOption.equals(DWValues.DO_OVERNIGHT)) {
					infoLabel1.setText(Messages.getString("MGRWizardPage3.infoBadDelay1a"));
					infoLabel2.setText(Messages.getString("MGRWizardPage3.infoBadDelay1b"));
				} else if (deliveryOption.equals(DWValues.OK_DELAY_24)) {
					infoLabel1.setText(Messages.getString("MGRWizardPage3.infoBadDelay2a"));
					infoLabel2.setText(Messages.getString("MGRWizardPage3.infoBadDelay2b"));
				} else if (deliveryOption.equals(DWValues.OK_DELAY_48)) {
					infoLabel1.setText(Messages.getString("MGRWizardPage3.infoBadDelay2a"));
					infoLabel2.setText(Messages.getString("MGRWizardPage3.infoBadDelay2b"));
				} else {
					infoLabel1.setText(Messages.getString("MGRWizardPage3.infoBadDelay2a"));
				}
				statusPane.setText(Messages.getString("DWValues.2047a"));
				flowButtons.setEnabled("next", false); 
				flowButtons.setSelected("back"); 
			} else {
				String status = getTranactionStatus(transaction.getTransactionLookupResponse().getPayload().getValue().getTransactionStatus(), transaction.getDataCollectionData().getCurrentValueMap(), true, transaction.isFormFreeTransaction());
				statusPane.setText(status);
			}
		} else {
			String status = getTranactionStatus(transaction.getTransactionLookupResponse().getPayload().getValue().getTransactionStatus(), transaction.getDataCollectionData().getCurrentValueMap(), true, transaction.isFormFreeTransaction());
			statusPane.setText(status);
			flowButtons.setEnabled("next", false); 
			flowButtons.setSelected("back"); 
		}
		dwTextPaneResized();
    }

	CommCompleteInterface cci1 = new CommCompleteInterface() {
		@Override
		public void commComplete(int commTag, Object returnValue) {
			boolean b = ((Boolean) returnValue).booleanValue();
			if (b) {
				
				// This is the first receiveValidation call from this screen. If the there are no data collection screens to show, and the status is not NEED_MORE_DATE or VALIDATED,
				// then make a second receiveValidation call.
				
				String nextPage = transaction.nextDataCollectionScreen(MoneyGramReceiveWizard.MRW20_RECEIVE_DETAIL);
				if ((nextPage.isEmpty()) && 
						(transaction.getPayoutMethod() == MoneyGramReceiveTransaction.PAYOUT_CASH) && 
						(! transaction.getDataCollectionData().getValidationStatus().equals(DataCollectionStatus.VALIDATED)) && 
						(! transaction.getDataCollectionData().getValidationStatus().equals(DataCollectionStatus.ERROR))) {
					transaction.receiveValidation(cci2, ValidationType.SECONDARY);
					return;
				}
				
		    	transaction.getDataCollectionData().clearFieldValueMap();
				PageNotification.notifyExitListeners(MGRWizardPage3.this, PageExitListener.NEXT);
			} else {
				flowButtons.setButtonsEnabled(true);
				flowButtons.setEnabled("next", false);
				flowButtons.setSelected("back");
			}
		}
	};
    
	CommCompleteInterface cci2 = new CommCompleteInterface() {
		@Override
		public void commComplete(int commTag, Object returnValue) {
			boolean b = ((Boolean) returnValue).booleanValue();
			if (b) {
				
				// This is the second receiveValidation call from this screen. If the response indicates that more data needs to be collected,
				// but these is no data collection screen to show, then set the data collection status to ERROR.
				
				String nextPage = transaction.nextDataCollectionScreen(MoneyGramReceiveWizard.MRW20_RECEIVE_DETAIL);
				if (nextPage.isEmpty() && (transaction.getDataCollectionData().getValidationStatus().equals(DataCollectionStatus.NEED_MORE_DATA))) {
					transaction.getDataCollectionData().setValidationStatus(DataCollectionStatus.ERROR);
				}
				
		    	transaction.getDataCollectionData().clearFieldValueMap();
				PageNotification.notifyExitListeners(MGRWizardPage3.this, PageExitListener.NEXT);
			} else {
				flowButtons.setButtonsEnabled(true);
				flowButtons.setEnabled("next", false);
				flowButtons.setSelected("back");
			}
		}
	};
    
	@Override
	public void finish(int direction) {
		if (direction == PageExitListener.NEXT) {
		
			transaction.setProfileStatus(ProfileStatus.USE_CURRENT_PROFILE);
			if ((transaction.isOverDailyLimit() || transaction.isOverReceiveLimit())) {
				flowButtons.setButtonsEnabled(true);
			} else  {
				flowButtons.setButtonsEnabled(false);
				if ((ReceiptTemplates.getReceiptLanguageRecv().indexOf("/") > 0)
						&& ((MoneyGramReceiveTransaction.getRecvReceipt2ndLanguageList().size() == 1)
								&& MoneyGramReceiveTransaction.allow2ndLangNone())) {
					MoneyGramReceiveTransaction.populateRecvReceiptLanguageList(ReceiptTemplates.getReceiptLanguageRecv(), false);
				}
				if (! transaction.isFormFreeTransaction()) {
					if (! transaction.getDataCollectionData().getDataCollectionType().equals(DataCollectionType.PROFILE_SEARCH)) {
						transaction.setDataCollectionData(transaction.getSearchProfilesData());
					}
					this.searchConsumerProfiles(AgentConnect.AUTOMATIC);
				} else {
					transaction.receiveValidation(cci1, ValidationType.INITIAL_FORM_FREE);
				}
			}
		} else {
			PageNotification.notifyExitListeners(this, direction);
		}
	}
	
	private void searchConsumerProfiles(int rule) {
		
		Map<String, String> transactionLookupValues = transaction instanceof MoneyGramReceiveTransaction ? transaction.getDataCollectionData().getCurrentValueMap() : null;
		Map<String, String> values = new HashMap<String, String>();
		if (transactionLookupValues != null) {
			String value = transactionLookupValues.get(FieldKey.RECEIVER_FIRSTNAME_KEY.getInfoKey());
			values.put(FieldKey.CONSUMER_FIRSTNAME_KEY.getInfoKey(), value);
			value = transactionLookupValues.get(FieldKey.RECEIVER_MIDDLENAME_KEY.getInfoKey());
			values.put(FieldKey.CONSUMER_MIDDLENAME_KEY.getInfoKey(), value);
			value = transactionLookupValues.get(FieldKey.RECEIVER_LASTNAME_KEY.getInfoKey());
			values.put(FieldKey.CONSUMER_LASTNAME_KEY.getInfoKey(), value);
			value = transactionLookupValues.get(FieldKey.RECEIVER_LASTNAME2_KEY.getInfoKey());
			values.put(FieldKey.CONSUMER_LASTNAME2_KEY.getInfoKey(), value);
			value = transactionLookupValues.get(FieldKey.RECEIVER_NAMESUFFIX_KEY.getInfoKey());
			values.put(FieldKey.CONSUMER_NAMESUFFIX_KEY.getInfoKey(), value);
			value = transactionLookupValues.get(FieldKey.RECEIVER_NAMESUFFIXOTHER_KEY.getInfoKey());
			values.put(FieldKey.CONSUMER_NAMESUFFIXOTHER_KEY.getInfoKey(), value);
		}
		
		if (transaction.getDataCollectionData() != null) {
			String value = transaction.getDataCollectionData().getValue(FieldKey.CONSUMER_DOB_KEY);
			values.put(FieldKey.CONSUMER_DOB_KEY.getInfoKey(), value);
		}
		
		transaction.searchConsumerProfiles(transaction.getDataCollectionData(), values, rule, new CommCompleteInterface() {
			@Override
			public void commComplete(int commTag, Object returnValue) {
				boolean b = ((returnValue != null) && (returnValue instanceof Boolean)) ? ((Boolean) returnValue) : false;
				if (b) {
					checkSearchConsumerProfilesResults();
//					if (toolkit != null) {
//						toolkit.disableResize();
//					}
				} else {
					resetButtons();
				}
			}
		});
	}
	
	private void checkSearchConsumerProfilesResults() {
		int profileCount = transaction.getSearchProfiles() != null ? transaction.getSearchProfiles().size() : 0;
		if (profileCount == 0) {
			Map<String, String> dataMap = transaction.getDataCollectionData().getCurrentValueMap();
			boolean f = transaction.createOrUpdateConsumerProfile(transaction.getProfileObject(), ProfileStatus.CREATE_PROFILE);
			if (! f) {
				PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
				return;
			}
			for (DataCollectionField field : transaction.getDataCollectionData().getFieldMap().values()) {
				String value = dataMap.get(field.getInfoKey());
				if ((value != null) && (! value.isEmpty())) {
					field.setDefaultValue(value);
				}
			}
		} else if (profileCount == 1) {
			final ConsumerProfile profile = transaction.getSearchProfiles().get(0);
			int rule = AgentConnect.AUTOMATIC;
			String mgiSessionID = transaction instanceof MoneyGramReceiveTransaction && transaction.getTransactionLookupResponsePayload() != null ? transaction.getTransactionLookupResponsePayload().getMgiSessionID() : null;
			transaction.getConsumerProfile(profile, mgiSessionID, rule, null, new CommCompleteInterface() {
				@Override
				public void commComplete(int commTag, Object returnValue) {
					boolean b = ((returnValue != null) && (returnValue instanceof Boolean)) ? ((Boolean) returnValue) : false;
					if (b) {
						Response response = transaction.getConsumerProfileResponse();
						if (response != null) {
							transaction.setProfileObject(profile);
							transaction.setProfileStatus(ProfileStatus.USE_CURRENT_PROFILE);
							PageNotification.notifyExitListeners(MGRWizardPage3.this, PageExitListener.NEXT);
						}
					} else {
						resetButtons();
					}
				}
			});
			return;
		}
		PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
	}

	private void resetButtons() {
		flowButtons.reset();
		flowButtons.setVisible("expert", false);
		flowButtons.setEnabled("back", true); 
	}
}
