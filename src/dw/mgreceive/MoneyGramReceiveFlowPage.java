package dw.mgreceive;

import dw.framework.FlowPage;
import dw.framework.PageFlowButtons;

public abstract class MoneyGramReceiveFlowPage extends FlowPage {
	private static final long serialVersionUID = 1L;

	protected MoneyGramReceiveTransaction transaction;

    public MoneyGramReceiveFlowPage(MoneyGramReceiveTransaction transaction, String fileName, String name, String pageCode, PageFlowButtons buttons) {
        super(fileName, name, pageCode, buttons);
        this.transaction = transaction;
    }  

    // subclass pages will override this to provide their own return value
    // handling. this page will handle the commitSend() and commitPrint calls
    // in send() and print()
    @Override
	public void commComplete(int commTag, Object returnValue) {}

    /**
     *  Sets up the TempLog for the business rules on a Reload.
     */
    protected synchronized void reSend() {
    	transaction.commitReSend();
    }
}
