package dw.mgreceive;

import dw.framework.ClientTransaction;
import dw.framework.PageNameInterface;
import dw.framework.TransactionInterface;

public abstract class MoneyGramReceiveTransactionInterface extends TransactionInterface {
	private static final long serialVersionUID = 1L;
	
	protected MoneyGramReceiveTransaction transaction;

    public MoneyGramReceiveTransactionInterface(String fileName,
                                 MoneyGramReceiveTransaction transaction,
                                 PageNameInterface naming,
                                 boolean sideBar) {
        super(fileName, naming, sideBar);
        this.transaction = transaction;
    }

    @Override
	public ClientTransaction getTransaction() {
        return transaction;
    }
}
