package dw.mgreceive;

import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JTextField;

import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.KeyMapManager;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;

/**
 * Page 4 of the MoneyGram Receive Wizard. This page displays the test question
 *   if there is one for this transaction. The user should ask the question to
 *   the receiver before continuing.
 */
public class MGRWizardPage3a extends MoneyGramReceiveFlowPage implements
		DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	private DWTextPane text1;

    public MGRWizardPage3a(MoneyGramReceiveTransaction tran,
                       String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "moneygramreceive/MGRWizardPage3a.xml", name, pageCode, buttons);  	
        
		text1 = (DWTextPane) getComponent("text1");
		text1.addListener(this, this);
		text1.setListenerEnabled(true);
    }

    @Override
	public void start(int direction) {
    	 transaction.setAnswerCorrect(false);
        flowButtons.reset();
        flowButtons.setVisible("expert", false);	
        JTextField questionField = (JTextField) getComponent("questionField");	
        JButton idVerifiedButton = (JButton) getComponent("idVerifiedButton");	

		text1.setText(Messages.getString("MGRWizardPage3a.1"));

        KeyMapManager.mapComponent(this, KeyEvent.VK_F10, 0, idVerifiedButton);
        questionField.setText(transaction.getQuestion());
        addActionListener("idVerifiedButton", this, "verified");	 
        idVerifiedButton.requestFocus();
        JComponent[] focusOrder = {idVerifiedButton,flowButtons.getButton("back"),	
                                    flowButtons.getButton("next"),	
                                    flowButtons.getButton("cancel")};	
        setFocusOrder(focusOrder);
    }

	@Override
	public void dwTextPaneResized() {
		DWTextPane text1 = (DWTextPane) getComponent("text1");
		text1.setWidth(this.getPreferredSize().width);
		text1.setListenerEnabled(false);
	}

	/**
     * Customer ID verified, so all is well. Carry on.
     */
    public void verified() {
    	 transaction.setAnswerCorrect(true);
         finish(PageExitListener.NEXT);
    }

    @Override
	public void finish(int direction) {
		String nextPage = transaction.nextDataCollectionScreen(MoneyGramReceiveWizard.MRW25_QUESTION);
    	if (nextPage.isEmpty()) {
			transaction.receiveValidation(this, ValidationType.SECONDARY);
   	    } else {
   	        PageNotification.notifyExitListeners(this, direction);
   	    }
    }
    
	@Override
	public void commComplete(int commTag, Object returnValue) {
		PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
	}
}
