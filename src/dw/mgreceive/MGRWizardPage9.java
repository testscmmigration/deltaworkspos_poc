/*
 * MGRWizardPage9.java
 *
 * $Revision$
 *
 * Copyright (c) 2004 MoneyGram International
 */

package dw.mgreceive;

import java.awt.Color;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.moneygram.agentconnect.TransactionLookupResponse;
import com.moneygram.agentconnect.TransactionStatusType;

import dw.dispenser.DispenserFactory;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.ClientTransaction;
import dw.framework.DataCollectionSet;
import dw.framework.FieldKey;
import dw.framework.KeyMapManager;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.mgreversal.SendReversalTransaction;
import dw.mgsend.MoneyGramClientTransaction;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.CountryInfo.Country;
import dw.model.adapters.CountryInfo.CountrySubdivision;
import dw.model.adapters.ReceiveDetailInfo;
import dw.profile.UnitProfile;
import dw.utility.DWValues;
import dw.utility.Debug;

/**
 * Page 9 of the MoneyGram Receive Wizard. This page displays instructions on
 *   what to do with the printing money orders based on what the payout scenario
 *   is.
 */
public class MGRWizardPage9 extends MoneyGramReceiveFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	private DWTextPane statusPane;
	private JPanel senderInfoPanel;
	private JPanel receiverInfoPanel;
	private JPanel rateInfoPanel;
	private JPanel infoPanel;
	private JLabel line1Label;
	private JLabel instruction1Label;
	private JLabel instruction2Label;
	private JLabel instruction3Label;
	private JLabel instruction4Label;
	private JLabel instruction5Label;
	private JLabel instruction6Label;
	private JLabel authorizationLabel;
	private JLabel reprintTextLabel;
	private JLabel attentionLabel;
	private JPanel sendAmountPanel;
	private JLabel sendAmt;
	private JLabel sendAmtCurrency;
	private JPanel sendFeePanel;
	private JLabel sendFee;
	private JLabel sendFeeCurrency;
	private JPanel fxRatePanel;
	private JLabel sendFxRate;
	private JPanel feeNotePanel;
	private boolean cancel = false;
	private boolean refund = false;
	private boolean alternateText = false;
	private boolean abort = false;
	private ReceiveDetailInfo lookupDetail=null;
	private JButton reprintButton;
    private JPanel purposePanel;
    private JLabel purposeLabel;
	private List<DWTextPane> textPanes;
	private int tagWidth;

	public MGRWizardPage9(MoneyGramReceiveTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super(tran, "moneygramreceive/MGRWizardPage9.xml", name, pageCode, buttons); 
		
		init();

		cancel = false;
		refund = false;
	}

	public MGRWizardPage9(SendReversalTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super(tran, "moneygramreceive/MGRWizardPage9.xml", name, pageCode, buttons); 
		
		init();
		cancel = tran.isCancelAllowed();
		refund = true;
	}
	
	private void init() {
		textPanes = new ArrayList<DWTextPane>();
		line1Label = (JLabel) getComponent("line1Label"); 
		instruction1Label = (JLabel) getComponent("instruction1Label"); 
		instruction2Label = (JLabel) getComponent("instruction2Label"); 
		instruction3Label = (JLabel) getComponent("instruction3Label"); 
		instruction4Label = (JLabel) getComponent("instruction4Label"); 
		instruction5Label = (JLabel) getComponent("instruction5Label"); 
		instruction6Label = (JLabel) getComponent("instruction6Label"); 
		authorizationLabel = (JLabel) getComponent("authorizationLabel");
		reprintTextLabel = (JLabel) getComponent("reprintTextLabel");
		attentionLabel = (JLabel) getComponent("attentionLabel");
		rateInfoPanel = (JPanel) getComponent("rateInfoPanel");
		senderInfoPanel = (JPanel) getComponent("senderInfoPanel");
		receiverInfoPanel = (JPanel) getComponent("receiverInfoPanel");
		infoPanel = (JPanel) getComponent("statusInfoPanel");
		statusPane = (DWTextPane) getComponent("statusPane"); 
		sendAmountPanel = (JPanel) getComponent("sendAmountPanel");
		sendAmtCurrency = (JLabel) getComponent("sendAmtCurrency");
		sendAmt = (JLabel) getComponent("sendAmt");
		sendFeePanel = (JPanel) getComponent("sendFeePanel");
		sendFeeCurrency = (JLabel) getComponent("sendFeeCurrency");
		sendFee = (JLabel) getComponent("sendFee");
		fxRatePanel = (JPanel) getComponent("fxRatePanel");
		sendFxRate = (JLabel) getComponent("sendFxRate");
		feeNotePanel = (JPanel) getComponent("feeNotePanel");
		reprintButton = (JButton) getComponent("reprintButton"); 
        purposePanel = (JPanel) getComponent("purposePanel");
        purposeLabel = (JLabel) getComponent("purposeLabel");
        
		this.addComponentListener(new ComponentAdapter() {
        	@Override
			public void componentResized(ComponentEvent e) {
        		dwTextPaneResized();
        	}
        });
	}

	@Override
	public void dwTextPaneResized() {
		
		JScrollPane sp1 = (JScrollPane) getComponent("scrollPane1");
		JScrollPane sp2 = (JScrollPane) getComponent("scrollPane2");
		JPanel panel2 = (JPanel) this.getComponent("scrollee");

		int w = FIELD_TOTAL_WIDTH - tagWidth;
		for (DWTextPane pane : textPanes) {
			pane.setWidth(w);
		}
		
		this.scrollpaneResized(sp1, sp2, panel2);
	}

	@Override
	public void start(int direction) {
		alternateText = false;
		flowButtons.reset();
		flowButtons.setVisible("expert", false); 
		flowButtons.setEnabled("next", false); 
		flowButtons.setEnabled("back", false); 
		flowButtons.setAlternateText("cancel", Messages.getString("OK"));  
		reprintButton.setEnabled(false);
		addActionListener("reprintButton", this, "reprint");  
		KeyMapManager.mapComponent(this, KeyEvent.VK_F5, 0, reprintButton);
        try {
        	if (refund) {
        		showReversalInfoPanel();
        	} else {
        		showReceiveInfoPanel();
        	}
        } catch(Exception e){
			Debug.printStackTrace(e);
        }

        if (refund) {
			reprintButton.setEnabled(transaction.isReceiptReceived());
		}

		if ((transaction.getAgentAmount().compareTo(BigDecimal.ZERO) > 0) && "DG".equals(transaction.getProfileMoneyGramChecks())) {
			String scenario = transaction.getPayoutScenario();
			if (scenario.equals("AG")) {
				line1Label.setText(Messages.getString("MGRWizardPage9.AGLine1")); 
				instruction3Label.setText(Messages.getString("MGRWizardPage9.AGInstruction1")); 
				instruction4Label.setText(Messages.getString("MGRWizardPage9.AGInstruction2")); 
				instruction5Label.setText(Messages.getString("MGRWizardPage9.AGInstruction3")); 
				instruction6Label.setText(Messages.getString("MGRWizardPage9.AGInstruction4")); 
			} else if (scenario.equals("CU")) {
				line1Label.setText(Messages.getString("MGRWizardPage9.CULine1")); 
				instruction3Label.setText(Messages.getString("MGRWizardPage9.CUInstruction1")); 
				instruction4Label.setText(Messages.getString("MGRWizardPage9.CUInstruction2")); 
				instruction5Label.setText(Messages.getString("MGRWizardPage9.CUInstruction3")); 
				instruction6Label.setText(Messages.getString("MGRWizardPage9.CUInstruction4")); 
			} else if (scenario.equals("AG_CU")) {
				line1Label.setText(Messages.getString("MGRWizardPage9.AG_CULine1")); 
				instruction3Label.setText(Messages.getString("MGRWizardPage9.AG_CUInstruction1")); 
				instruction4Label.setText(Messages.getString("MGRWizardPage9.AG_CUInstruction2")); 
				instruction5Label.setText(Messages.getString("MGRWizardPage9.AG_CUInstruction3")); 
				instruction6Label.setText(Messages.getString("MGRWizardPage9.AG_CUInstruction4")); 
			} else if (scenario.equals("CU_CU")) {
				line1Label.setText(Messages.getString("MGRWizardPage9.CU_CULine1")); 
				instruction3Label.setText(Messages.getString("MGRWizardPage9.CU_CUInstruction1")); 
				instruction4Label.setText(Messages.getString("MGRWizardPage9.CU_CUInstruction2")); 
				instruction5Label.setText(Messages.getString("MGRWizardPage9.CU_CUInstruction3")); 
				instruction6Label.setText(Messages.getString("MGRWizardPage9.CU_CUInstruction4")); 
			}
			authorizationLabel.setText("");
		} else {
			if (!transaction.isReceiptReceived()) {
				attentionLabel.setText(Messages.getString("MGRWizardPage9.AlternateAttentionTextStart"));
				instruction1Label.setText(""); 
				reprintTextLabel.setVisible(false);
				reprintButton.setVisible(false);
				alternateText = true;
			}
			instruction2Label.setText(""); 
			instruction3Label.setText(""); 
			instruction4Label.setText(""); 
			instruction5Label.setText(""); 
			instruction6Label.setText(""); 
			if (direction == PageExitListener.NEXT)
				authorizationLabel.setText("");
			else {
				authorizationLabel.setText(transaction.getAuthorizationMessage());
				authorizationLabel.setForeground(Color.red);
			}
		}
		abort = false;

		// call setButtonsEnabled to avoid duplicated transactions.
		flowButtons.setButtonsEnabled(false);

		// PageExitListener.CANCELED is the normal exit of this page.
		if (direction == PageExitListener.CANCELED) {
			if ("MTC".equals(transaction.getProfileMoneyGramChecks())) {
				abort = true;
				this.finish(ClientTransaction.CANCELED);
			} else if (DispenserFactory.getDispenser(1).getRemainingCount() > 2) {
				abort = true;
				this.finish(ClientTransaction.CANCELED);
			}
		} else {
			
			if (refund && "MTC".equals(transaction.getProfileMoneyGramChecks())) {
				authorizationLabel.setText(transaction.getAuthorizationMessage(transaction.getAuthorizationCode()));
//				statusLabel.setText(DWValues.getReceivedStatusDescription(transaction.getTransactionLookupResponse().getPayload().getValue().getTransactionStatus()));
			} else {
				authorizationLabel.setText(transaction.getAuthorizationMessage());
			}
			TransactionStatusType status = (transaction.getTransactionLookupResponse() != null) && (transaction.getTransactionLookupResponse().getPayload() != null) && (transaction.getTransactionLookupResponse().getPayload().getValue() != null) ? transaction.getTransactionLookupResponse().getPayload().getValue().getTransactionStatus() : null;
			Map<String, String> currentValues = transaction.getSearchProfilesData() != null ? transaction.getSearchProfilesData().getCurrentValueMap() : null;
			String text = getTranactionStatus(status, currentValues, false, false);
			statusPane.setText(text);
			dwTextPaneResized();
			if (alternateText) {
				if (transaction.getAuthorizationMessage().length() > 0) {
					attentionLabel.setText(Messages.getString("MGRWizardPage9.AlternateAttentionTextFinish"));
				}
			}
			flowButtons.setEnabled("cancel", true); 
			flowButtons.setSelected("cancel"); 
		}
	}
	
	private void showReceiveInfoPanel() {
		if ("MTC".equals(transaction.getProfileMoneyGramChecks())) {
	    	List<JLabel> tags = new ArrayList<JLabel>();
			
	    	String firstName = transaction.getSearchProfilesData().getValue(FieldKey.SENDER_FIRSTNAME_KEY);
	    	String middleName = transaction.getSearchProfilesData().getValue(FieldKey.SENDER_MIDDLENAME_KEY);
	    	String lastName1 = transaction.getSearchProfilesData().getValue(FieldKey.SENDER_LASTNAME_KEY);
	    	String lastName2 = transaction.getSearchProfilesData().getValue(FieldKey.SENDER_LASTNAME2_KEY);
	    	String senderName = DWValues.formatName(firstName, middleName, lastName1, lastName2);
	    	this.displayPaneItem("senderName", senderName, tags, textPanes);
			
			// Sender Address

	    	String address1 = transaction.getSearchProfilesData().getValue(FieldKey.SENDER_ADDRESS_KEY);
	    	String address2 = transaction.getSearchProfilesData().getValue(FieldKey.RECEIVER_ADDRESS2_KEY);
	    	String address3 = transaction.getSearchProfilesData().getValue(FieldKey.RECEIVER_ADDRESS3_KEY);
	    	String address4 = transaction.getSearchProfilesData().getValue(FieldKey.RECEIVER_ADDRESS4_KEY);
	    	String address = DWValues.getAddress(address1, address2, address3, address4);
			this.displayPaneItem("senderAddress", address, tags, textPanes);
	        
			// Sender City
	        
			String tag = transaction.getTransactionLookupInfoLabel(FieldKey.SENDER_CITY_KEY.getInfoKey());
	        String senderCity = transaction.getSearchProfilesData().getValue(FieldKey.SENDER_CITY_KEY);
	        this.displayLabelItem("senderCity", tag, senderCity, tags);

	        String senderCountry = transaction.getSearchProfilesData().getValue(FieldKey.SENDER_COUNTRY_KEY);
	        Country country = (senderCountry != null) && (! senderCountry.isEmpty()) ? CountryInfo.getCountry(senderCountry) : null;
	        String senderState = transaction.getSearchProfilesData().getValue(FieldKey.SENDER_COUNTRY_SUBDIVISIONCODE_KEY);
	        CountrySubdivision state = country != null ? country.getCountrySubdivision(senderState) : null;
			tag = transaction.getTransactionLookupInfoLabel(FieldKey.SENDER_COUNTRY_SUBDIVISIONCODE_KEY.getInfoKey());
	        this.displayLabelItem("senderState", tag, state != null ? state.getPrettyName() : "", tags);

			tag = transaction.getTransactionLookupInfoLabel(FieldKey.SENDER_POSTALCODE_KEY.getInfoKey());
	        String senderZipCode = transaction.getSearchProfilesData().getValue(FieldKey.SENDER_POSTALCODE_KEY);
	        this.displayLabelItem("senderZipCode", tag, senderZipCode, tags);
	        
			tag = transaction.getTransactionLookupInfoLabel(FieldKey.SENDER_COUNTRY_KEY.getInfoKey());
	        this.displayLabelItem("senderCountry", tag, country != null ? country.getPrettyName() : "", tags);
	        
			tag = transaction.getTransactionLookupInfoLabel(FieldKey.SENDER_PRIMARYPHONE_KEY.getInfoKey());
	        String homePhone = transaction.getSearchProfilesData().getValue(FieldKey.SENDER_PRIMARYPHONE_KEY);
	        this.displayLabelItem("senderHomePhone", tag, homePhone, tags);
			
			tag = transaction.getTransactionLookupInfoLabel(FieldKey.ORIGINATINGCOUNTRY_KEY.getInfoKey());
	        String sendCountry = transaction.getSearchProfilesData().getCurrentValue(FieldKey.ORIGINATINGCOUNTRY_KEY.getInfoKey());
	        country = (sendCountry != null) && (! sendCountry.isEmpty()) ? CountryInfo.getCountry(sendCountry) : null;
	        this.displayLabelItem("sendCountry", tag, country != null ? country.getPrettyName() : "", tags);

			tag = transaction.getTransactionLookupInfoLabel("dateTimeSent");
			String s = transaction.getSearchProfilesData().getCurrentValue("dateTimeSent");
	        this.displayLabelItem("sent", tag, FormatSymbols.formatDateTimeForLocaleWithTimeZone(s), tags);
			
			// Send Purpose of Transaction
			
			String purpose = transaction.getSenderPurposeOfTransaction();
			if (purpose != null) {
				purposeLabel.setText(purpose);
				purposePanel.setVisible(true);
			} else {
				purposePanel.setVisible(false);
			}
	        this.displayLabelItem("purpose", purpose, tags);
			
			// Receiver Name
			
	    	firstName = transaction.getSearchProfilesData().getValue(FieldKey.RECEIVER_FIRSTNAME_KEY);
	    	middleName = transaction.getSearchProfilesData().getValue(FieldKey.RECEIVER_MIDDLENAME_KEY);
	    	lastName1 = transaction.getSearchProfilesData().getValue(FieldKey.RECEIVER_LASTNAME_KEY);
	    	lastName2 = transaction.getSearchProfilesData().getValue(FieldKey.RECEIVER_LASTNAME2_KEY);
	    	String receiverName = DWValues.formatName(firstName, middleName, lastName1, lastName2);
			this.displayPaneItem("receiverName", receiverName, tags, textPanes);
			
	    	String message1 = transaction.getDataCollectionData().getValue(FieldKey.MESSAGEFIELD1_KEY);
	    	String message2 = transaction.getDataCollectionData().getValue(FieldKey.MESSAGEFIELD2_KEY);
			String value = ((message1 != null ? message1 + " " : "") + (message2 != null ? message2 : "")).trim();
	        this.displayPaneItem("message", value, tags, textPanes);

	        this.displayLabelItem("reference", transaction.getReferenceNumber(), tags);

	        this.displayPaneItem("status", " ", false, tags, textPanes);

			this.displayMoney("amount1", transaction.getReceiveAmount1(), transaction.getReceiveCurrency(), tags);
			this.displayMoney("amount2", transaction.getReceiveAmount2(), transaction.getReceiveCurrency(), tags);
			this.displayMoney("amount3", transaction.getReceiveTotalAmount(), transaction.getReceiveCurrency(), tags);

			rateInfoPanel.setVisible(false);
			sendAmountPanel.setVisible(false);
			sendFeePanel.setVisible(false);
			feeNotePanel.setVisible(false);
			fxRatePanel.setVisible(false);
			
			boolean txnSendAmtNeeded = transaction.isSendAmtNeeded();
			boolean txnSendFeeNeeded = transaction.isSendFeeNeeded();
			boolean fxNeeded = transaction.isFxNeeded();
			DataCollectionSet SearchProfileData = null;
			
			if (txnSendAmtNeeded || txnSendFeeNeeded || fxNeeded) {
				SearchProfileData = transaction.getSearchProfilesData();
			}
			if (txnSendAmtNeeded && SearchProfileData != null && !refund) {
				rateInfoPanel.setVisible(true);
				sendAmountPanel.setVisible(true);
				sendAmt.setText(SearchProfileData.getCurrentValue("originalSendAmount") 
						+ "  " + SearchProfileData.getCurrentValue("originalSendCurrency"));
			}
			if (txnSendFeeNeeded && SearchProfileData != null && !refund) {
				rateInfoPanel.setVisible(true);
				sendFeePanel.setVisible(true);
				feeNotePanel.setVisible(true);
				sendFee.setText(SearchProfileData.getCurrentValue("originalSendFee")
					+ "  " + SearchProfileData.getCurrentValue("originalSendCurrency"));
			}
			if (fxNeeded && SearchProfileData != null && !refund) {
				rateInfoPanel.setVisible(true);
				fxRatePanel.setVisible(true);
				sendFxRate.setText(SearchProfileData.getCurrentValue("originalExchangeRate"));
			}
			
			tagWidth = this.setTagLabelWidths(tags, MINIMUM_TAG_WIDTH);
		} else {
			rateInfoPanel.setVisible(false);
			senderInfoPanel.setVisible(false);
			receiverInfoPanel.setVisible(false);
			infoPanel.setVisible(false);
		}
		if (transaction.isReceiptReceived()) {
			reprintButton.setEnabled(true);
		} else {
			reprintButton.setEnabled(false);
		}
	}

	private void showReversalInfoPanel() {

    	TransactionLookupResponse results = transaction.getTransactionLookupResponse();
		if (transaction.getProfileMoneyGramChecks().equals("MTC")) {

			Map<String, String> map = transaction.getDataCollectionData().getCurrentValueMap();
			
	    	List<JLabel> tags = new ArrayList<JLabel>();
			
			String firstName = map.get(FieldKey.SENDER_FIRSTNAME_KEY.getInfoKey()); 
			String middleName = map.get(FieldKey.SENDER_MIDDLENAME_KEY.getInfoKey());
			String lastName1 = map.get(FieldKey.SENDER_LASTNAME_KEY.getInfoKey());
			String lastName2 = map.get(FieldKey.SENDER_LASTNAME2_KEY.getInfoKey());
			String senderName = DWValues.formatName(firstName, middleName, lastName1, lastName2);
	    	this.displayPaneItem("senderName", senderName, tags, textPanes);

			String address1 = map.get(FieldKey.SENDER_ADDRESS_KEY.getInfoKey()); 
			String address2 = map.get(FieldKey.SENDER_ADDRESS2_KEY.getInfoKey());  
			String address3 = map.get(FieldKey.SENDER_ADDRESS3_KEY.getInfoKey());
			String address4 = map.get(FieldKey.SENDER_ADDRESS4_KEY.getInfoKey());
	    	String address = DWValues.getAddress(address1, address2, address3, address4);
			this.displayPaneItem("senderAddress", address, tags, textPanes);

			String tag = transaction.getTransactionLookupInfoLabel(FieldKey.SENDER_CITY_KEY.getInfoKey());
			String value = map.get(FieldKey.SENDER_CITY_KEY.getInfoKey());
	        this.displayLabelItem("senderCity", tag, value, tags);

			tag = transaction.getTransactionLookupInfoLabel(FieldKey.SENDER_COUNTRY_SUBDIVISIONCODE_KEY.getInfoKey());
			value = map.get(FieldKey.SENDER_COUNTRY_SUBDIVISIONCODE_KEY.getInfoKey());
	        this.displayLabelItem("senderState", tag, value, tags);

			tag = transaction.getTransactionLookupInfoLabel(FieldKey.SENDER_POSTALCODE_KEY.getInfoKey());
			value = map.get(FieldKey.SENDER_POSTALCODE_KEY.getInfoKey());
	        this.displayLabelItem("senderZipCode", tag, value, tags);
	        
			tag = transaction.getTransactionLookupInfoLabel(FieldKey.SENDER_COUNTRY_KEY.getInfoKey());
			value = map.get(FieldKey.SENDER_COUNTRY_KEY.getInfoKey());
	        this.displayLabelItem("senderCountry", tag, value, tags);
			
			tag = transaction.getTransactionLookupInfoLabel(FieldKey.SENDER_PRIMARYPHONE_KEY.getInfoKey());
			value = map.get(FieldKey.SENDER_PRIMARYPHONE_KEY.getInfoKey());
	        this.displayLabelItem("senderHomePhone", tag, value, tags);
			
			tag = transaction.getTransactionLookupInfoLabel(FieldKey.ORIGINATINGCOUNTRY_KEY.getInfoKey());
			value = map.get(DWValues.GFFP_ORIGINATING_COUNTRY);
	        this.displayLabelItem("sendCountry", value, tags);

			tag = transaction.getTransactionLookupInfoLabel("dateTimeSent");
			value = map.get(DWValues.DATE_TIME_SENT);
	        this.displayLabelItem("sent", FormatSymbols.formatDateTimeForLocaleWithTimeZone(value), tags);

			String receiveCurrency = transaction.getSendAmountInfo().getSendCurrency();

			// Receiver Name
			
			firstName = map.get(FieldKey.RECEIVER_FIRSTNAME_KEY.getInfoKey());
			middleName = map.get(FieldKey.RECEIVER_MIDDLENAME_KEY.getInfoKey());
			lastName1 = map.get(FieldKey.RECEIVER_LASTNAME_KEY.getInfoKey());
			lastName2 = map.get(FieldKey.RECEIVER_LASTNAME2_KEY.getInfoKey());
			String receiverName = DWValues.formatName(firstName, middleName, lastName1, lastName2);
			this.displayPaneItem("receiverName", receiverName, tags, textPanes);

			String message1 = map.get(FieldKey.MESSAGEFIELD1_KEY.getInfoKey());
			String message2 = map.get(FieldKey.MESSAGEFIELD2_KEY.getInfoKey());
			value = ((message1 != null ? message1 + " " : "") + (message2 != null ? message2 : "")).trim();
	        this.displayPaneItem("message", value, tags, textPanes);

	        this.displayLabelItem("reference", transaction.getReferenceNumber(), tags);
	        
	        this.displayPaneItem("status", " ", false, tags, textPanes);
			
			this.displayMoney("amount1", transaction.getAgentAmount(), receiveCurrency, tags);
			this.displayMoney("amount2", transaction.getCustomerAmount(), receiveCurrency, tags);
			this.displayMoney("amount3", transaction.getAmount(), receiveCurrency, tags);
			rateInfoPanel.setVisible(false);
			
			tagWidth = this.setTagLabelWidths(tags, MINIMUM_TAG_WIDTH);

		} else {
			rateInfoPanel.setVisible(false);
			senderInfoPanel.setVisible(false);
			receiverInfoPanel.setVisible(false);
			infoPanel.setVisible(false);
		}
		// In demo mode set status to completed.
    	Map<String, String> currentValues = transaction.getSearchProfilesData() != null ? transaction.getSearchProfilesData().getCurrentValueMap() : null;
		if (UnitProfile.getInstance().isDemoMode()) {
			statusPane.setText(getTranactionStatus(TransactionStatusType.RECVD, currentValues, false, false));
		} else {
			statusPane.setText(getTranactionStatus(results.getPayload().getValue().getTransactionStatus(), currentValues, false, false));
		}
		dwTextPaneResized();
	}

	@Override
	public void commComplete(int commTag, Object returnValue) {
		if (commTag == MoneyGramClientTransaction.COMPLETE_SESSION) {
			
			boolean b = false;
			if (returnValue != null) {
				if (returnValue instanceof Boolean) {
					b = ((Boolean) returnValue).booleanValue();
				} else {
					b = true;
				}
			}
			if (b) {
				transaction.print(this, cancel);
				if ((! UnitProfile.getInstance().isDemoMode()) && (! UnitProfile.getInstance().isTrainingMode())) {
					transaction.logTransaction();
				}
			}
			dwTextPaneResized();
		
		} else if (commTag == MoneyGramClientTransaction.TRANSACTION_LOOKUP) {
			if (! refund) {
				lookupDetail = transaction.getReceiveDetail();
				// In demo mode set status to completed.
				if (UnitProfile.getInstance().isDemoMode()) {
					lookupDetail.setTransactionStatus(TransactionStatusType.RECVD);
				}
		    	Map<String, String> currentValues = transaction.getDataCollectionData() != null ? transaction.getDataCollectionData().getCurrentValueMap() : null;
				statusPane.setText(getTranactionStatus(lookupDetail.getTransactionStatus(), currentValues, false, false));
				dwTextPaneResized();
			}

		} else if ((commTag == MoneyGramClientTransaction.COMMIT_DONE) || (commTag == MoneyGramClientTransaction.COMMIT_ERROR) || (commTag == MoneyGramClientTransaction.COMMIT_COMMERROR)) {
			if (alternateText) {
				if (transaction.getAuthorizationMessage().length() > 0) {
					attentionLabel.setText(Messages.getString("MGRWizardPage9.AlternateAttentionTextFinish"));
				}
			}
			flowButtons.setEnabled("cancel", true); 
			flowButtons.setSelected("cancel"); 
			if (commTag != MoneyGramClientTransaction.COMMIT_DONE) {
				if (commTag == MoneyGramClientTransaction.COMMIT_COMMERROR) {
					if (transaction.getStatus() == ClientTransaction.CANNOT_COMPLETE_AS_DEFINED) {
						PageNotification.notifyExitListeners(this, PageExitListener.BACK);
						transaction.unlockCciLog();
						return;
					}
				}
			    PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
			}

			transaction.unlockCciLog();
			if (refund && "MTC".equals(transaction.getProfileMoneyGramChecks())) {
				authorizationLabel.setText(transaction.getAuthorizationMessage(transaction.getAuthorizationCode()));
		    	Map<String, String> currentValues = transaction.getDataCollectionData() != null ? transaction.getDataCollectionData().getCurrentValueMap() : null;
				statusPane.setText(getTranactionStatus(transaction.getTransactionLookupResponse().getPayload().getValue().getTransactionStatus(), currentValues, false, false));
				dwTextPaneResized();
			} else {
				authorizationLabel.setText(transaction.getAuthorizationMessage());
			}

			if (commTag != MoneyGramClientTransaction.COMMIT_COMMERROR && !refund) {
				//Show transaction complete, then quiescent message on pinpad
				MoneyGramReceiveWizard.showPinpadMessages(Messages.getString("MGRWizardPage1.PinpadIngenico_ValidTransactionCollectFunds"), 15000); 

				if (transaction.isReceiptReceived()) {
					reprintButton.setEnabled(true);
				} else {
					reprintButton.setEnabled(false);
				}
				transaction.transactionLookup(transaction.getReferenceNumber(), null, this, DWValues.TRX_LOOKUP_STATUS, false);
			}
			
			Debug.println("MGRWizardPage9.commComplete() authorizationMessage=" + transaction.getAuthorizationMessage());
		}
	}

	public void reprint() {
		reprint(transaction);
	}

	/*
	 *  Reprint of Receive (or Reversal) receipt(s).
	 */
	public static void reprint(MoneyGramReceiveTransaction tran) {
		if (tran.isReceiveAgentAndCustomerReceipts() || tran.isReceiveAgentReceipt()) {
			tran.printReceipt(false, true);
		}
		
		if (tran.isReceiveAgentAndCustomerReceipts() || tran.isReceiveCustomerReceipt()) {
			tran.printReceipt(true, true);
		}
	}

	/**
	 * Transaction must be complete at this point.
	 */
	@Override
	public void finish(int direction) {

		if (direction == PageExitListener.CANCELED) {
			if (abort) {
				PageNotification.notifyExitListeners(this, direction);
			} else {
				PageNotification.notifyExitListeners(this, PageExitListener.COMPLETED);
			}
		} else if (direction == PageExitListener.NEXT) {
			if (refund) {
				
				
			}
			
		} else {
			PageNotification.notifyExitListeners(this, direction);
		}
	}
}
