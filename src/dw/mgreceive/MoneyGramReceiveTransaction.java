package dw.mgreceive;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.StringTokenizer;

import javax.swing.JComponent;

import com.moneygram.agentconnect.BusinessError;
import com.moneygram.agentconnect.CompleteSessionRequest;
import com.moneygram.agentconnect.CompleteSessionResponse;
import com.moneygram.agentconnect.ConsumerProfileIDInfo;
import com.moneygram.agentconnect.CreateOrUpdateProfileReceiverResponse;
import com.moneygram.agentconnect.EnumeratedIdentifierInfo;
import com.moneygram.agentconnect.FieldInfo;
import com.moneygram.agentconnect.GetProfileReceiverResponse;
import com.moneygram.agentconnect.InfoBase;
import com.moneygram.agentconnect.KeyValuePairType;
import com.moneygram.agentconnect.MoneyOrderInfo;
import com.moneygram.agentconnect.MoneyOrderPrintStatusType;
import com.moneygram.agentconnect.MoneyOrderVoidReasonCodeType;
import com.moneygram.agentconnect.ReceiveValidationResponse;
import com.moneygram.agentconnect.Request;
import com.moneygram.agentconnect.Response;
import com.moneygram.agentconnect.SendAmountInfo;
import com.moneygram.agentconnect.SessionType;
import com.moneygram.agentconnect.TransactionLookupRequest;
import com.moneygram.agentconnect.TransactionLookupResponse;
import com.moneygram.agentconnect.TransactionStatusType;

import dw.comm.MessageFacade;
import dw.comm.MessageFacadeError;
import dw.comm.MessageFacadeParam;
import dw.comm.MessageLogic;
import dw.dialogs.Dialogs;
import dw.dialogs.Receipt1stLanguageDialog;
import dw.dialogs.Receipt2ndLanguageDialog;
import dw.dispenser.MoneyOrderDisplayFields;
import dw.framework.ClientTransaction;
import dw.framework.CommCompleteInterface;
import dw.framework.DataCollectionField;
import dw.framework.FieldKey;
import dw.framework.DataCollectionPanel;
import dw.framework.DataCollectionSet;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.DataCollectionSet.DataCollectionType;
import dw.framework.FlowPage;
import dw.framework.FormFreeIdVerificationWizardPage;
import dw.framework.PrintingProgressInterface;
import dw.framework.WaitToken;
import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.io.DiagLog;
import dw.io.IsiReporting;
import dw.io.diag.MustTransmitDiagMsg;
import dw.io.info.MoneyGramReceiveDiagInfo;
import dw.io.report.ReportLog;
import dw.main.DeltaworksStartup;
import dw.main.MainPanelDelegate;
import dw.mgreversal.SendReversalTransaction;
import dw.mgsend.MoneyGramClientTransaction;
import dw.model.adapters.CachedACResponses;
import dw.model.adapters.ConsumerProfile;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.CustomerInfo;
import dw.model.adapters.CountryInfo.Country;
import dw.model.adapters.PaidMoneyGramInfo40;
import dw.model.adapters.ReceiveDetailInfo;
import dw.model.adapters.ReceiverInfo;
import dw.model.adapters.SendReversalType;
import dw.moneyorder.MOPrintingDialog;
import dw.moneyorder.MoneyOrderTransaction40;
import dw.pdfreceipt.PdfReceipt;
import dw.printing.ReceiptReport;
import dw.printing.TEPrinter;
import dw.profile.ClientInfo;
import dw.profile.Profile;
import dw.profile.ProfileItem;
import dw.profile.UnitProfile;
import dw.prtreceipt.PrtReceipt;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.ErrorManager;
import dw.utility.ExtraDebug;
import dw.utility.IdleBackoutTimer;
import dw.utility.StringUtility;
import dw.utility.TimeUtility;

/**
 * MoneyGramReceiveTransaction encapsulates the information needed to receive a
 * MoneyGram from the POS.
 * 
 * @author Geoff Atkin
 */
public class MoneyGramReceiveTransaction extends MoneyGramClientTransaction {
	private static final long serialVersionUID = 1L;

	// possible return codes for setting amounts

	public static final int OK = 0;
	public static final int MAX_ITEM_AMOUNT_EXCEEDED = 1;
	public static final int DAILY_LIMIT_EXCEEDED = 4;
	public static final int TRANSACTION_AMOUNT_EXCEEDED = 5; // (a.k.a. CTR)

	public static final BigDecimal MAX_MO = BigDecimal.valueOf(999999, 2);
	private static final BigDecimal ZERO_DECIMAL = new BigDecimal(0.0);

    private static String receipt1stLanguageMnemonic = DWValues.MNEM_ENG;
    private static String receipt2ndLanguageMnemonic = "";
    private static boolean receiptAllow2ndLanguageNone = false;
	private static boolean bHasMultipleLanguages = true;
	private static boolean bHasSingleReceiptLanguage = false;
	private static List<String> receipt1stLanguageMnemonicList;
	private static List<String> receipt2ndLanguageMnemonicList;
	
	// receipts from MimeData in response msg
    private PdfReceipt pdfReceiptDisc1;
    private PdfReceipt pdfReceiptDisc2;
    protected PdfReceipt pdfReceiptAgent1;
    protected PdfReceipt pdfReceiptCust1;
    protected PdfReceipt pdfReceiptCust2;
    private PrtReceipt prtReceiptDisc1;
    private PrtReceipt prtReceiptDisc2;
    protected PrtReceipt prtReceiptAgent1;
    protected PrtReceipt prtReceiptCust1;
    protected PrtReceipt prtReceiptCust2;	

    private boolean bPrintFailure = false;
    private int receipt1stLanguageIdx = 0;
    private int receipt2ndLanguageIdx = 0;
	private BigDecimal expectedPayout;
	private BigDecimal maxTransactionAmount;
	protected String receiptFormat;
	private BigDecimal thirdPartyLimit;
	private boolean missingCurrency = false;
	private boolean missingTransaction = false; // added as part of AGG3 API changes for referenceNumberLookup to transactionLookup
	private ReceiveDetailInfo detail;
	protected PaidMoneyGramInfo40 paidInfo;
    private CompleteSessionResponse receiveInfo;
    private boolean answerCorrect = false;
    private boolean questionAsked = false;
    private boolean amount1Cash = true;
    BigDecimal dailyTotal;
    private String thirdPartyType;
    private boolean formFreeTransaction = false;
    protected MoneyOrderTransaction40 mot;
    protected static MessageFacadeError pinpadMfe;
	private boolean unreceivableTransaction;
	private TransactionStatusType transactionStatus;
	protected static MoneyOrderInfo mo1;
	protected static MoneyOrderInfo mo2;
	protected static MoneyOrderDisplayFields modf1;
	protected static MoneyOrderDisplayFields modf2;
	// Added as part of new API Changes - referenceNumberLookup to transactionLookup - aep4 - 07/07/2015
	//Removing it as it is added in MoneyGramClientTransaction - bhl5 - 09/28/2016
	//private TransactionLookupResponse transactionLookupResponse;

    protected WaitToken wait = new WaitToken();
	private SendAmountInfo sendAmountInfo;
	
    private DataCollectionScreenStatus receiverScreenStatus = new DataCollectionScreenStatus(DataCollectionSet.MRW30_PANEL_NAMES, null);
    private DataCollectionScreenStatus thirdPartyScreenStatus = new DataCollectionScreenStatus(DataCollectionSet.MRW35_PANEL_NAMES, null);;
	private DataCollectionScreenStatus supplementalInformationScreenStatus;
	private String mgiTransactionSessionID;
	private boolean refundAllowed;
	private boolean cancelAllowed;
	private GetProfileReceiverResponse getProfileReceiverResponse;
	private BigDecimal receiveAmount1;
	private BigDecimal receiveAmount2;
	private BigDecimal receiveTotalAmount;
	private String receiveCurrency;
    
	public MoneyGramReceiveTransaction(String tranName, String tranReceiptPrefix, int docSeq) {
		super(tranName, tranReceiptPrefix, docSeq);
		mot = new MoneyOrderTransaction40(tranName, tranReceiptPrefix, docSeq);
		// profile item MG_CHECKS. Valid values are "DG", "REQ", and "OPT".
		// "DG" is the only option that you need a dispenser.
		if ("DG".equals(UnitProfile.getInstance().get("MG_CHECKS", "DG")))
			dispenserNeeded = true;
		else
			dispenserNeeded = false;

		requiredFormCount = 2;
		clear();
		dataCollectionData = new DataCollectionSet(DataCollectionType.VALIDATION);
	}

	@Override
	public void clear() {
		// clear out all the transaction information
		super.clear();
		mot.clear();
		paidInfo = new PaidMoneyGramInfo40();
		detail = null;
		answerCorrect = false;
		questionAsked = false;
		amount1Cash = true;
		wait.setWaitFlag(true);
		dataCollectionData = new DataCollectionSet(DataCollectionType.VALIDATION);
		receiveInfo = null;
	}

	/**
	 * Retrieve some values that limit the transaction.
	 */
	protected boolean getProfileValues() {
		String sCurrency = this.getCurrency();
		// make sure the currency is set to something useful
		if (sCurrency == null || sCurrency.length() == 0) {
			sCurrency = this.agentBaseCurrency();
		}

		// if DEFAULT_CASH_PAYOUT_AMOUNT exists in the profile, the meaning of
		// MAX_ITEM_AMOUNT is changed to logically be the max transaction
		// amount, the same as it does in the send. If it does not exist,
		// MAX_ITEM_AMOUNT continues to mean cash payout and the max
		// max transaction amount should be 2800.00 as per WalMart's request.
		// detail.setCurrency("EUR");

		maxTransactionAmount = new BigDecimal("2800.00"); 

		// This will return false if your profile doesn't have the correct
		// currency.
		if (getProductProfileOption("MAX_AMOUNT_PER_ITEM", sCurrency) == null) {
			return false;
		}
		expectedPayout = getProductProfileDecimalOption("MAX_AMOUNT_PER_ITEM", sCurrency); 
		ProfileItem payoutItem = getProductProfileOption("DEFAULT_CASH_PAYOUT_AMOUNT", sCurrency); 

		if (payoutItem != null) {
			maxTransactionAmount = expectedPayout;
			expectedPayout = payoutItem.bigDecimalValue();
		}
		receiptFormat = getReceiptFormat();
		thirdPartyLimit = getProductProfileDecimalOption("FRAUD_LIMIT_THIRD_PARTY_ID", sCurrency); 
		return true;
	}

	/**
	 * Retrieve some values that limit the transaction only if the transaction
	 * is not ready to be received.
	 */
	protected void getNotAvailableProfileValues() {
		String sCurrency = this.getCurrency();
		// make sure the currency is set to something useful
		if (sCurrency == null || sCurrency.length() == 0) {
			sCurrency = this.agentBaseCurrency();
		}

		// if DEFAULT_CASH_PAYOUT_AMOUNT exists in the profile, the meaning of
		// MAX_ITEM_AMOUNT is changed to logically be the max transaction
		// amount, the same as it does in the send. If it does not exist,
		// MAX_ITEM_AMOUNT continues to mean cash payout and the max
		// max transaction amount should be 2800.00 as per WalMart's request.
		maxTransactionAmount = new BigDecimal("2800.00"); 
		expectedPayout = getProductProfileDecimalOption("MAX_AMOUNT_PER_ITEM", sCurrency); 
		ProfileItem payoutItem = getProductProfileOption("DEFAULT_CASH_PAYOUT_AMOUNT"); 
		if (payoutItem != null) {
			maxTransactionAmount = expectedPayout;
			expectedPayout = payoutItem.bigDecimalValue();
		}
		receiptFormat = getReceiptFormat();
		thirdPartyLimit = getProductProfileDecimalOption("FRAUD_LIMIT_THIRD_PARTY_ID", sCurrency); 
	}

	public ReceiveDetailInfo getReceiveDetail() {
		return detail;
	}

	public void createNewReceiveDetailInfo() {
		detail = new ReceiveDetailInfo();
	}

	public void restoreReceiveDetailInfo(ReceiveDetailInfo oldDetail) {
		detail = oldDetail;
	}

	public void restoreReceivePaidInfo(PaidMoneyGramInfo40 oldPaidInfo) {
		paidInfo = oldPaidInfo;
	}


	/**
	 * There are 4 possible scenarios based on the selected payout in the transaction: 
	 * 		AG_CU : Cash to customer (MO to agent),	MO to customer 
	 * 		CU_CU : 2 money orders to customer. 
	 * 		AG : Cash only to customer (MO to agent) 
	 * 		CU : 1 money order to customer. 
	 */
	public String getPayoutScenario() {
		BigDecimal amount1 = getAgentAmount();
		BigDecimal amount2 = getCustomerAmount();

		if (amount1.compareTo(ZERO) == 0)
			return "CU"; 

		if (amount2.compareTo(ZERO) == 0)
			if (amount1Cash)
				return "AG"; 
			else
				return "CU"; 

		if (amount1Cash)
			return "AG_CU"; 
		else
			return "CU_CU"; 
	}

	/**
	 * Makes sure the payee name is short enough to fit in the payee line on the
	 * printed item. First remove extra spaces and periods. Then while the
	 * result is longer than 20 characters, replace each space separated word
	 * with its initial. But do not replace the last word with its initial. If
	 * the result can not be made shorter than 20 characters without replacing
	 * every word, the result will be longer.
	 */
	public static String adjustPayee(String payee) {
		String result = payee;
		StringTokenizer st = new StringTokenizer(payee, " ."); 
		int count = st.countTokens();
		String word[] = new String[count];

		for (int i = 0; i < count; i++) {
			word[i] = st.nextToken();
		}
		for (int i = 0; i < count; i++) {
			result = word[0];
			for (int j = 1; j < count; j++) {
				result = result + " " + word[j]; 
			}
			if (result.length() <= 20)
				return result;
			else
				word[i] = word[i].substring(0, 1);
		}
		return result;
	}
	
	protected void setPayout(int payoutType) {
		BigDecimal customerAmount = paidInfo.getCustomerAmount() != null ? new BigDecimal(paidInfo.getCustomerAmount()) : ZERO_DECIMAL;
		BigDecimal agentAmount = paidInfo.getAgentAmount() != null ? new BigDecimal(paidInfo.getAgentAmount()) : ZERO_DECIMAL;
		BigDecimal otherAmount = paidInfo.getOtherPayoutAmount() != null ? new BigDecimal(Double.parseDouble(paidInfo.getOtherPayoutAmount())) : ZERO_DECIMAL;
		
//		String checkType = "";
		String agentCheckNumber = "";
		long serialNumber = 0;
		String customerCheckNumber = "";
		paidInfo.setCurrency(detail.getReceiveCurrency());
		if (payoutType == MoneyGramReceiveTransaction.PAYOUT_DELTA_GRAM) {
//			checkType = "DG";
			try {
				serialNumber = Long.parseLong(mot.getNextSerialNumber().trim());
			} catch (Exception e) {
				Debug.printException(e);
			}
			if ((agentAmount != null) && (agentAmount.compareTo(ZERO_DECIMAL) > 0)) {
				agentCheckNumber = getSerialNumberWithCheckDigit(BigInteger.valueOf(serialNumber)).toString();
				serialNumber++;
			}
			if ((customerAmount != null) && (customerAmount.compareTo(ZERO_DECIMAL) > 0)) {
				customerCheckNumber = getSerialNumberWithCheckDigit(BigInteger.valueOf(serialNumber)).toString();
			}
		} else if (payoutType == MoneyGramReceiveTransaction.PAYOUT_MONEY_TRANSFER_CHECK) {
//			checkType = "MTC";
			agentCheckNumber = getAgentCheckNumber() != null ? getAgentCheckNumber().toString() : "";
			customerCheckNumber = getCustomerCheckNumber() != null ? getCustomerCheckNumber().toString() : "";
		} else {
			paidInfo.setAgentCheckNumber(new BigInteger("99888888888"));
			if (this instanceof SendReversalTransaction) {
				paidInfo.setAgentAmount(transactionLookupResponsePayload.getSendAmounts().getSendAmount().toString());
			} else {
				paidInfo.setAgentAmount(transactionLookupResponsePayload.getReceiveAmounts().getReceiveAmount().toString());
			}
	        paidInfo.setCustomerAmount("0.00");
	        paidInfo.setOtherPayoutAmount("0.00");
		}
		
		String payoutScenario = getPayoutScenario();
		if (payoutScenario.equals("AG")) {
			dataCollectionData.getFieldValueMap().put(FieldKey.PAYOUT1_TYPE_KEY, "A");
			dataCollectionData.getFieldValueMap().put(FieldKey.PAYOUT2_TYPE_KEY, "N");
		} else if (payoutScenario.equals("CU")) {
			dataCollectionData.getFieldValueMap().put(FieldKey.PAYOUT1_TYPE_KEY, "C");
			dataCollectionData.getFieldValueMap().put(FieldKey.PAYOUT2_TYPE_KEY, "N");
		} else if (payoutScenario.equals("AG_CU")) {
			dataCollectionData.getFieldValueMap().put(FieldKey.PAYOUT1_TYPE_KEY, "A");
			dataCollectionData.getFieldValueMap().put(FieldKey.PAYOUT2_TYPE_KEY, "C");
		} else if (payoutScenario.equals("CU_CU")) {
			/* Note: AC does not like getting two customer checks, so call one agent */
			dataCollectionData.getFieldValueMap().put(FieldKey.PAYOUT1_TYPE_KEY, "A");
			dataCollectionData.getFieldValueMap().put(FieldKey.PAYOUT2_TYPE_KEY, "C");
		}
		
		if ((agentAmount != null) && (agentAmount.compareTo(ZERO_DECIMAL) > 0)) {
			dataCollectionData.getFieldValueMap().put(FieldKey.PAYOUT1_CHECKAMOUNT_KEY, agentAmount);
			dataCollectionData.getFieldValueMap().put(FieldKey.PAYOUT1_CHECKNUMBER_KEY, agentCheckNumber);
		} else {
			dataCollectionData.getFieldValueMap().remove(FieldKey.PAYOUT1_CHECKAMOUNT_KEY);
			dataCollectionData.getFieldValueMap().remove(FieldKey.PAYOUT1_TYPE_KEY);
			dataCollectionData.getFieldValueMap().remove(FieldKey.PAYOUT1_CHECKNUMBER_KEY);
		}
		
		if ((customerAmount != null) && (customerAmount.compareTo(ZERO_DECIMAL) > 0)) {
			dataCollectionData.getFieldValueMap().put(FieldKey.PAYOUT2_CHECKAMOUNT_KEY, customerAmount);
			dataCollectionData.getFieldValueMap().put(FieldKey.PAYOUT2_CHECKNUMBER_KEY, customerCheckNumber);
		} else {
			dataCollectionData.getFieldValueMap().remove(FieldKey.PAYOUT2_CHECKAMOUNT_KEY);
			dataCollectionData.getFieldValueMap().remove(FieldKey.PAYOUT2_TYPE_KEY);
			dataCollectionData.getFieldValueMap().remove(FieldKey.PAYOUT2_CHECKNUMBER_KEY);
		}
		
		if ((otherAmount != null) && (otherAmount.compareTo(ZERO_DECIMAL) > 0)) {
			dataCollectionData.getFieldValueMap().put(FieldKey.OTHERPAYOUTAMOUNT_KEY, otherAmount);
			dataCollectionData.getFieldValueMap().put(FieldKey.OTHERPAYOUTTYPE_KEY, DWValues.RECV_TO_CARD);
		} else {
			dataCollectionData.getFieldValueMap().remove(FieldKey.OTHERPAYOUTAMOUNT_KEY);
			dataCollectionData.getFieldValueMap().remove(FieldKey.OTHERPAYOUTTYPE_KEY);
		}
	}

	/**
	 * Prints money orders in the indicated amounts, sends transactions to the
	 * middleware, and writes logs.
	 * 
	 * @param progress the callback to anyone who cares about the progress of the print
	 * @param parent a component to display dialogs over if things go wrong 
	 * @return the Thread that is created to perform the processing in the BG
	 */
	
	public void receiveValidation(final CommCompleteInterface callingPage, final ValidationType validationType) {

		final MessageFacadeParam parameters = new MessageFacadeParam(
				MessageFacadeParam.REAL_TIME,
				MessageFacadeParam.INVALID_CHECK_NUMBER);
		final MessageLogic logic = new MessageLogic() {
			
			@Override
			public Object run() {
				Boolean returnStatus = Boolean.TRUE;
				try {
					
					// agentCheckNumber
					// agentCheckType
					// customerCheckAmount
					
					int payoutType = MoneyGramReceiveTransaction.this.getPayoutMethod();
					
					if (! validationType.equals(ValidationType.SECONDARY)) {
						dataCollectionData = new DataCollectionSet(DataCollectionType.VALIDATION);
						dataCollectionData.updated();
						if (getSupplementalDataCollectionPanels() != null) {
							getSupplementalDataCollectionPanels().clear();
						}
						supplementalInformationScreenStatus = new DataCollectionScreenStatus(); 
					} else {
						setPayout(payoutType);
					}

					dataCollectionData.setFieldValue(FieldKey.MF_VALID_LANG_RCPT_PRIME_KEY, DWValues.sConvertLang3ToLang5(getPrimaryReceiptSelectedLanguage()));
					dataCollectionData.setFieldValue(FieldKey.MF_VALID_LANG_RCPT_SECOND_KEY, DWValues.sConvertLang3ToLang5(getSecondaryReceiptSelectedLanguage()));
					
		            // Send the receiveValidation request to Agent Connect.
					
		        	boolean isInitial = ! validationType.equals(ValidationType.SECONDARY);
					ReceiveValidationResponse result = MessageFacade.getInstance().receiveValidation(callingPage, parameters, dataCollectionData, payoutType, getTransactionLookupResponse(), getProfileObject(), isInitial);
					
					// Check if a valid response was returned from the receiveValidation call.
					
					if (result != null) {
						ReceiveValidationResponse.Payload payload = result.getPayload() != null ? result.getPayload().getValue() : null;
						List<InfoBase> fieldsToCollectList =  payload != null && payload.getFieldsToCollect() != null ? payload.getFieldsToCollect().getFieldToCollect() : null;
						List<BusinessError> errorList = result.getErrors() != null ? result.getErrors().getError() : null;
						boolean isReadyForCommit = payload != null ? payload.isReadyForCommit() : false;
						
						processValidationResponse(validationType, fieldsToCollectList, errorList, isReadyForCommit, DataCollectionSet.RECEIVE_BASE_EXCLUDE);
						
						getDataCollectionData().setAfterValidationAttempt(validationType.equals(ValidationType.SECONDARY));

						if ((isFormFreeEnabled() && isFormFreeTransaction())) {
							List<KeyValuePairType> currentValues = MoneyGramReceiveTransaction.this.transactionLookupResponsePayload.getCurrentValues().getCurrentValue();
							boolean b = FormFreeIdVerificationWizardPage.haveReceiverIdFields(dataCollectionData, currentValues);
							setFormFreeIdFields(b);
						} else {
							setFormFreeIdFields(false);
						}
						
						returnStatus = Boolean.TRUE;
					} 

				} catch (MessageFacadeError mfe) {
					returnStatus = Boolean.FALSE;
				}

				finishComm(RECEIVE_VALIDATION, callingPage, returnStatus);
				return returnStatus;
			}
		};
		MessageFacade.run(logic, null, 0, Boolean.FALSE);
	}

	public static boolean allow2ndLangNone() {
		return receiptAllow2ndLanguageNone;
	}

	public static List<String> getRecvReceiptAllLanguageList() {
		List<String> lRetValue = new ArrayList<String>();
		if ((receipt1stLanguageMnemonic != null) && (receipt1stLanguageMnemonic.length() > 0)) {
			lRetValue.add(receipt1stLanguageMnemonic);
		} else {
			if (receipt1stLanguageMnemonicList != null) {
				Iterator<String> itr = receipt1stLanguageMnemonicList.iterator();
				while (itr.hasNext()) {
					lRetValue.add(itr.next());
				}
			}
		}
		if (receipt2ndLanguageMnemonicList != null) {
			Iterator<String> itr = receipt2ndLanguageMnemonicList.iterator();
			while (itr.hasNext()) {
				lRetValue.add(itr.next());
			}
		}
		return lRetValue;
	}

    public static List<String> getRecvReceipt1stLanguageList(){
    	List<String> lRetList = new ArrayList<String>(receipt1stLanguageMnemonicList);
    	return lRetList;
    }

    public static List<String> getRecvReceipt2ndLanguageList(){
    	List<String> lRetList;
    	if (receipt2ndLanguageMnemonicList != null) {
    		lRetList = receipt2ndLanguageMnemonicList;
    	} else {
    		lRetList = new ArrayList<String>();
    	}
    	return lRetList;
    }

    public boolean isOKToPrint() {
		boolean bReturnValue = true;

		if (this.isReceiptReceived()) {
			// are there multiple selections available for primary language?
			if (bHasSingleReceiptLanguage) {
				List<String> pLanguageList = getRecvReceipt1stLanguageList();
				if (pLanguageList.size() > 1) {
					// dialog box for selection
					Receipt1stLanguageDialog dialog = new Receipt1stLanguageDialog(pLanguageList, receipt1stLanguageIdx);
					Dialogs.showPanel(dialog);
					if (dialog.isAccepted()) {
						receipt1stLanguageIdx = dialog.getLanguageIdx();
						receipt1stLanguageMnemonic = dialog.getLanguageMnemonic();
					} else {
						bReturnValue = false;
					}
				} else {
					// non-hybrid only 1 language available for primary
					return true;
				}
			}
	
			// multiple selections available for secondary language
			if (bHasMultipleLanguages && bReturnValue) {
				// no Dodd-Frank constraint
				List<String> lLanguageList = getRecvReceipt2ndLanguageList();
	
				if (allow2ndLangNone()) {
					lLanguageList.add(DWValues.MNEM_NONE);
				}
	
				Receipt2ndLanguageDialog dialog = new Receipt2ndLanguageDialog(lLanguageList, receipt2ndLanguageIdx);
				Dialogs.showPanel(dialog);
				if (dialog.isAccepted()) {
					receipt2ndLanguageIdx = dialog.getLanguageIdx();
					receipt2ndLanguageMnemonic = dialog.getLanguageMnemonic();
				} else {
					bReturnValue = false;
				}
			}
		}
		return bReturnValue;
    }

	public static void populateRecvReceiptLanguageList() {
		Profile profile = UnitProfile.getInstance().getProfileInstance();
		
		try {
			bHasSingleReceiptLanguage = (profile.find(DWValues.PROFILE_RCPT_LANG_NUMBER).stringValue()
					.compareToIgnoreCase("singleLanguage") == 0);
		} catch (NoSuchElementException e) {
			bHasSingleReceiptLanguage = true;
		} catch (Exception e) {
			Debug.printException(e);
			Debug.dumpStack(e.getMessage());
		}		
		
 		if (bHasSingleReceiptLanguage) {
 			// non-hybrid - so only one language specified to use for the receipt
 			bHasMultipleLanguages = false;
 			// primary language only - use list for selection
			receipt1stLanguageMnemonic = "";

 			// build list
			int iNbr1stLanguages = 0;
			try {
				List<String> receipt1stLanguageList = profile.findList(DWValues.PROFILE_RCPT_LANG);

				receipt1stLanguageMnemonicList = new ArrayList<String>();
				receipt2ndLanguageMnemonic = "";
				Iterator<String> i = receipt1stLanguageList.iterator();
				boolean skipFirst = true;  // formerly "eng" was reserved as first listed item
				while (i.hasNext()) {
					String sLan = (i.next()).toUpperCase(Locale.US);
					if (skipFirst) {
						skipFirst = false;
						continue;
					}
					if ((sLan != null) && (sLan.compareToIgnoreCase(DWValues.PROFILE_RCPT_LANG_NONE) != 0)) {
						receipt1stLanguageMnemonicList.add(sLan);
						iNbr1stLanguages++;
					}
				}
			} catch (Exception e) {
				iNbr1stLanguages = 0;
				Debug.printException(e);
				Debug.dumpStack(e.getMessage());
			}

			if (iNbr1stLanguages == 0) {
				receipt1stLanguageMnemonicList.add(DWValues.MNEM_ENG);
				iNbr1stLanguages++;
			}

			if (iNbr1stLanguages == 1) {
				receipt1stLanguageMnemonic = receipt1stLanguageMnemonicList.get(0);
			}

		} else {
			// multiple language for hybrid receipts - primary is specified
			int iNbr2ndLanguages = 0;

			try {
				receipt1stLanguageMnemonic = profile.find(DWValues.PROFILE_RCPT_LANG_PRIMARY, 0).stringValue()
						.toUpperCase();
			} catch (NoSuchElementException e) {
				receipt1stLanguageMnemonic = DWValues.MNEM_ENG;
			} catch (Exception e) {
				Debug.printException(e);
				Debug.dumpStack(e.getMessage());
			}

			// "NONE" may be a valid selection for the additional language list
			try {
				receiptAllow2ndLanguageNone = (profile.find(DWValues.PROFILE_RCPT_LANG_ALLOW_ONE, 0).stringValue()
						.compareToIgnoreCase("Y") == 0);
			} catch (NoSuchElementException e) {
				receiptAllow2ndLanguageNone = false;
			} catch (Exception e) {
				Debug.printException(e);
				Debug.dumpStack(e.getMessage());
			}

			try {
				List<String> receipt2ndLanguageList = profile.findList(DWValues.PROFILE_RCPT_LANG);

				receipt2ndLanguageMnemonicList = new ArrayList<String>();
				receipt2ndLanguageMnemonic = "";
				Iterator<String> i = receipt2ndLanguageList.iterator();
				boolean skipFirst = true;  // formerly "eng" was reserved as first listed item
				while (i.hasNext()) {
					String sLan = (i.next()).toUpperCase();
					if (skipFirst) {
						skipFirst = false;
						continue;
					}					
					if ((sLan != null) && (sLan.compareToIgnoreCase(DWValues.PROFILE_RCPT_LANG_NONE) != 0)
							&& (sLan.compareToIgnoreCase(receipt1stLanguageMnemonic) != 0)) {
						receipt2ndLanguageMnemonicList.add(sLan);
						iNbr2ndLanguages++;
					}
				}
			} catch (Exception e) {
				Debug.printException(e);
				Debug.dumpStack(e.getMessage());
			}

			bHasMultipleLanguages = ((iNbr2ndLanguages > 1) || (iNbr2ndLanguages == 1 && receiptAllow2ndLanguageNone));
			if (iNbr2ndLanguages == 1) {
				receipt2ndLanguageMnemonic = receipt2ndLanguageMnemonicList.get(0);
			}
		}
	}

	public static void populateRecvReceiptLanguageList(String receiptLang, boolean allowNone) {
		// empirical determination of language list
		receipt1stLanguageMnemonic = receiptLang.substring(0, 3);
		receiptAllow2ndLanguageNone = allowNone;

		Profile profile = UnitProfile.getInstance().getProfileInstance();
		int iNbr2ndLanguages = 0;
		try {
			List<String> receipt2ndLanguageList = profile.findList(DWValues.PROFILE_RCPT_LANG);

			receipt2ndLanguageMnemonicList = new ArrayList<String>();
			receipt2ndLanguageMnemonic = "";
			Iterator<String> i = receipt2ndLanguageList.iterator();
			boolean skipFirst = true;  // formerly "eng" was reserved as first listed item
			while (i.hasNext()) {
				String sLan = ((String) i.next()).toUpperCase(Locale.US);
				if (skipFirst) {
					skipFirst = false;
					continue;
				}				
				if ((sLan != null) && (sLan.compareToIgnoreCase(DWValues.PROFILE_RCPT_LANG_NONE) != 0)
						&& (sLan.compareToIgnoreCase(receipt1stLanguageMnemonic) != 0)) {
					receipt2ndLanguageMnemonicList.add(sLan);
					iNbr2ndLanguages++;
				}
			}
		} catch (Exception e) {
			Debug.printException(e);
			Debug.dumpStack(e.getMessage());
		}

		bHasMultipleLanguages = ((iNbr2ndLanguages > 1) || (iNbr2ndLanguages == 1 && receiptAllow2ndLanguageNone));
		if (iNbr2ndLanguages == 1) {
			receipt2ndLanguageMnemonic = (String) receipt2ndLanguageMnemonicList.get(0);
		}
	}

	protected String getPrimaryReceiptSelectedLanguage() {
		return receipt1stLanguageMnemonic;
	}

	protected String getSecondaryReceiptSelectedLanguage() {
		return receipt2ndLanguageMnemonic;
	}

	protected void setFieldCollectionStatus() {
		List<String> panels = new ArrayList<String>();
		panels.addAll(this.receiverScreenStatus.getPanelNames());
		panels.addAll(this.thirdPartyScreenStatus.getPanelNames());
		
		for (String panel : panels) {
			DataCollectionPanel dcp = getDataCollectionData().getDataCollectionPanelMap().get(panel);
			if (dcp != null) {
				dcp.markFieldsCollected(DataCollectionField.BASE_SCREENS_COLLECTION);
			}
		}
	}
	
	@Override
	public void setScreensToShow() {
		
		DataCollectionType dataCollectionType = this.getDataCollectionData().getDataCollectionType();
		if (dataCollectionType.equals(DataCollectionType.VALIDATION)) {

			// Determine which of the data collection screens need to be shown. 
		
			Set<String> panelSet = new HashSet<String>(DataCollectionSet.RECEIVE_BASE_EXCLUDE);
			panelSet.addAll(receiverScreenStatus.getPanelNames(this.getDataCollectionData()));
			panelSet.addAll(thirdPartyScreenStatus.getPanelNames(this.getDataCollectionData()));
			
			this.receiverScreenStatus.calculateDataCollectionScreenStatus(getDataCollectionData());
			this.thirdPartyScreenStatus.calculateDataCollectionScreenStatus(getDataCollectionData());
	
			List<DataCollectionPanel> supplementalDataCollectionPanels = getDataCollectionData().getSupplementalDataCollectionPanels(panelSet);
			this.setSupplementalDataCollectionPanels(supplementalDataCollectionPanels);
			this.supplementalInformationScreenStatus = getDataCollectionScreenStatus(supplementalDataCollectionPanels);
			this.setExcludePanelNames(panelSet);
			ExtraDebug.println("Receiver Information Screen Status = " + receiverScreenStatus.getStatus().toString());
			ExtraDebug.println("Third Party Information Screen Status = " + thirdPartyScreenStatus.getStatus().toString());
			ExtraDebug.println("Receiver Supplemental Screen Status = " + supplementalInformationScreenStatus.getStatus().toString());
		} else if (dataCollectionType.equals(DataCollectionType.PROFILE_EDIT)) {
			setProfileDataCollectionScreensToShow(DataCollectionSet.RECEIVER_PROFILE_SCREEN_2_PANEL_NAMES);
		}
	}

	public void completeSession(final FlowPage callingPage, final SessionType productType, final Request validationRequest, final String referenceNumber) {

		final MessageFacadeParam parameters = new MessageFacadeParam(MessageFacadeParam.REAL_TIME, MessageFacadeParam.USER_CANCEL);

		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {
				dataCollectionData.setFieldValue(FieldKey.MGISESSIONID_KEY, getTransactionLookupResponse().getPayload().getValue().getMgiSessionID()); 
				dataCollectionData.setFieldValue(FieldKey.MF_PROD_TYPE_KEY, productType); 

				PdfReceipt.deletePdfFiles();
				PrtReceipt.deletePrtFiles();

				CompleteSessionRequest completeSessionRequest = new CompleteSessionRequest();
				completeSessionRequest.setMgiSessionID(getTransactionLookupResponse().getPayload().getValue().getMgiSessionID());
				completeSessionRequest.setMgiSessionType(productType);
				CompleteSessionResponse commitResponse = MessageFacade.getInstance().completeSession(parameters, completeSessionRequest, validationRequest, referenceNumber);
				receiveInfo = commitResponse;

				if (commitResponse == null) {
					tranSuccessful = false;
				} else {
					tranSuccessful = true;
					
					// Process and receipt data returned in the response.

					try {
						CompleteSessionResponse.Payload payload = commitResponse.getPayload().getValue();
						if ((payload.getReceipts() != null) && (payload.getReceipts().getReceiptMimeType() != null)) {
							if (payload.getReceipts().getReceiptMimeType().indexOf("/pdf") > 0) {
								if (payload.getReceipts().getAgentReceiptMimeData() != null) {
									pdfReceiptAgent1 = new PdfReceipt(payload.getReceipts().getAgentReceiptMimeData().getReceiptMimeDataSegment(), PdfReceipt.PDF_AGENT_1);
								}
								if (payload.getReceipts().getConsumerReceipt1MimeData() != null) {
									pdfReceiptCust1 = new PdfReceipt(payload.getReceipts().getConsumerReceipt1MimeData().getReceiptMimeDataSegment(), PdfReceipt.PDF_CUST_1);
								}
								if (payload.getReceipts().getConsumerReceipt2MimeData() != null) {
									pdfReceiptCust2 = new PdfReceipt(payload.getReceipts().getConsumerReceipt2MimeData().getReceiptMimeDataSegment(), PdfReceipt.PDF_CUST_2);
								}
								prtReceiptAgent1 = null;
								prtReceiptCust1 = null;
								prtReceiptCust2 = null;
							} else {
								if (payload.getReceipts().getAgentReceiptMimeData() != null) {
									prtReceiptAgent1 = new PrtReceipt(payload.getReceipts().getAgentReceiptMimeData().getReceiptMimeDataSegment(), PrtReceipt.PRT_AGENT_1);
								}
								if (payload.getReceipts().getConsumerReceipt1MimeData() != null) {
									prtReceiptCust1 = new PrtReceipt(payload.getReceipts().getConsumerReceipt1MimeData().getReceiptMimeDataSegment(), PrtReceipt.PRT_CUST_1);
								}
								if (payload.getReceipts().getConsumerReceipt2MimeData() != null) {
									prtReceiptCust2 = new PrtReceipt(payload.getReceipts().getConsumerReceipt2MimeData().getReceiptMimeDataSegment(), PrtReceipt.PRT_CUST_2);
								}
								pdfReceiptAgent1 = null;
								pdfReceiptCust1 = null;
								pdfReceiptCust2 = null;
							}
						}
						
					} catch (IOException e) {
						Debug.printException(e);
					}
					
					// Add an entry for the transaction in the ISI log file.
					
					logISIEntry();
					
					dataCollectionData.setValidationStatus(DataCollectionStatus.COMPLETED);
				}

				endOfSend();
				return commitResponse != null ? Boolean.TRUE : Boolean.FALSE;
			}
		};
		MessageFacade.run(logic, callingPage, COMPLETE_SESSION, Boolean.FALSE);
	}

	protected MoneyOrderInfo getMoneyOrderInfo(int i) {
		MoneyOrderInfo moi = mot.getMoneyOrderInfo(i);
		Debug.println("getMoneyOrderInfo(" + i + ") document type="	+ moi.getDocumentType());
		return moi;
	}

	/**
	 * Return true if this money order was voided by the dispenser error
	 * handler.
	 */
	private boolean wasVoided(MoneyOrderInfo mo) {
		return mo.isVoidFlag() && mo.getVoidReasonCode().equals(MoneyOrderVoidReasonCodeType.DISPENSER_ERROR);
	}

	public Thread commitReSend() {
		Thread tSend = new Thread(new Runnable() {
			@Override
			public void run() {
				mo1 = getMoneyOrderInfo(0);
				modf1 = mot.getMoneyOrderDisplayFields(0);
				if (mot.getMoneyOrderList().size() > 1) {
					mo2 = getMoneyOrderInfo(1);
					modf2 = mot.getMoneyOrderDisplayFields(1);
				} else {
					mo2 = null;
					modf2 = null;
				}
				// mo1 was voided
				if (wasVoided(mo1)) {
					Debug.println("commitReSend() mo1 was voided");
					mo1.setVoidFlag(false);
					mo1.setSerialNumber(new BigInteger(mot.getNextSerialNumber()));
					// fill in the serial numbers on the paid info object now
					if ((mo1.getItemAmount()).compareTo(ZERO) > 0) {
						setAgentCheckNumber(getSerialNumberWithCheckDigit(mo1.getSerialNumber()));
					} else
						setAgentCheckNumber(BigInteger.ZERO); 

					// fill in the reference numbers only if successful
					mo1.setTaxID(getReferenceNumber());

					if (mo2 != null) {
						Long snLong = Long.valueOf(mot.getNextSerialNumber());
						long snlong = (snLong.longValue()) + 1;
						mo2.setSerialNumber(BigInteger.valueOf(snlong));

						if ((mo2.getItemAmount()).compareTo(ZERO) > 0)
							setCustomerCheckNumber(getSerialNumberWithCheckDigit(mo2.getSerialNumber()));
						else
							setCustomerCheckNumber(BigInteger.ZERO); 

						mo2.setTaxID(getReferenceNumber());
					}
				}

				// mo1 printed and mo2 was voided
				else if (MoneyOrderPrintStatusType.CONFIRMED_PRINTED.equals(mo1.getPrintStatus()) && !wasVoided(mo1) && wasVoided(mo2)) {
					Debug.println("commitReSend() mo1 print status=" + mo1.getPrintStatus() + " && !wasVoided(mo1) && wasVoided(mo2)");
					// Remove the printed money order for the list.
					if (mot.getMoneyOrderList().size() > 1)
						mot.deleteMoneyOrder(0); // GEOFF THINK THIS IS RIGHT IF
													// WE'RE GOING TO COMMMIT
													// THE CCI
					mo2.setVoidFlag(false);
					mo2.setSerialNumber(new BigInteger(mot.getNextSerialNumber()));
					if ((mo2.getItemAmount()).compareTo(ZERO) > 0)
						setCustomerCheckNumber(getSerialNumberWithCheckDigit(mo2.getSerialNumber()));
					else
						setCustomerCheckNumber(BigInteger.ZERO); 

					// fill in the reference numbers only if successful
					mo2.setTaxID(getReferenceNumber());
				}

				// Dispenser Reload on Printing mo1
				else if (MoneyOrderPrintStatusType.ATTEMPTED.equals(mo1.getPrintStatus()) && !wasVoided(mo1)) {
					Debug.println("commitReSend() mo1 print status = " + mo1.getPrintStatus() + " && !wasVoided(mo1)");
					mo1.setDocumentType(BigInteger.valueOf(4));
					mo1.setSerialNumber(new BigInteger(mot.getNextSerialNumber())); // GEOFF SAYS THIS CAN'T BE RIGHT
					// fill in the serial numbers on the paid info object now
					if ((mo1.getItemAmount()).compareTo(ZERO) > 0)
						setAgentCheckNumber(getSerialNumberWithCheckDigit(mo1
								.getSerialNumber()));
					else
						setAgentCheckNumber(BigInteger.ZERO); 

					// fill in the reference numbers only if successful
					mo1.setTaxID(getReferenceNumber());

					if (mo2 != null) {
						Long snLong = Long.valueOf(mot.getNextSerialNumber());
						long snlong = (snLong.longValue()) + 1;
						mo2.setSerialNumber(BigInteger.valueOf(snlong));
						if (mo2.getItemAmount().compareTo(ZERO) > 0)
							setCustomerCheckNumber(getSerialNumberWithCheckDigit(mo2.getSerialNumber()));
						else
							setCustomerCheckNumber(BigInteger.ZERO); 
						mo2.setTaxID(getReferenceNumber());
					}
				}

				// Dispenser Reload on Printing mo2
				else if (MoneyOrderPrintStatusType.CONFIRMED_PRINTED.equals(mo1.getPrintStatus()) && !wasVoided(mo1)
						&& MoneyOrderPrintStatusType.ATTEMPTED.equals(mo1.getPrintStatus()) && !wasVoided(mo2)) {
					Debug.println("commitReSend() mo1 print status=" + mo1.getPrintStatus()
							+ " && !wasVoided(mo1) && mo2 print status=" + mo2.getPrintStatus() + " && !wasVoided(mo2)");
					// Remove printed Money Order from list
					mot.deleteMoneyOrder(0);

					if (mo2 != null) {
						mo2.setDocumentType(BigInteger.valueOf(4));
						// fill in the serial numbers on the paid info object
						// now
						mo2.setSerialNumber(new BigInteger(mot.getNextSerialNumber()));
						if ((mo2.getItemAmount()).compareTo(ZERO) > 0)
							setCustomerCheckNumber(getSerialNumberWithCheckDigit(mo2.getSerialNumber()));
						else
							setCustomerCheckNumber(BigInteger.ZERO); 

						// fill in the reference numbers only if successful
						mo2.setTaxID(getReferenceNumber());
					}
				} else
					Debug.println("INVALID Case in CommitPsuedoSend"); 

				tranSuccessful = true;
				endOfSend();
			}
		});
		tSend.start();
		return tSend;
	}

	public Thread commitPrint(final MOPrintingDialog dialog, final FlowPage callingPage, final boolean cancel) {
		
    	final MoneyGramReceiveTransaction tran = this;
		final Thread tPrint = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					startOfPrint();
				} catch (InterruptedException ie) {
					return;
				}
				if (tranSuccessful) { // comm was successful, so continue with print
					if ("DG".equals(getProfileMoneyGramChecks()) && !cancel) {
						PrintingProgressInterface progress = dialog;
						// pause screen timeout
						IdleBackoutTimer.pause();
						mot.commit(progress, false);
						if (mot.getStatus() != COMPLETED) {
							// CANCELED means one or both were never attempted 
							// (for example, dispenser was open to start with or there weren't enough forms, or the dispenser was interrupted).
							// FAILED and LOAD_REQUIRED means something is probably limbo.
							// Either way, examine mo1 and mo2, figure out how far we got.

							// Nothing has printed, but the dispenser is in an unstable state, Dispenser must be loaded and the serial numbers will be recalculated.
							for (int i = 0; i < mot.getMoneyOrderList().size(); i++) {
								MoneyOrderInfo mo = mot.getMoneyOrderList().get(i);
								
								if (MoneyOrderPrintStatusType.CONFIRMED_PRINTED.equals(mo.getPrintStatus()) && !wasVoided(mo)) {
									continue;
								}
								// if ((! mo1.wasAttempted()) && (!
								// mo2.wasAttempted())) {
								if (MoneyOrderPrintStatusType.NOT_ATTEMPTED.equals(mo.getPrintStatus())) {
									// nothing printed at all
									tranSuccessful = false;
									setStatus(ClientTransaction.NO_FORMS);
									Debug.println("  setting status = 90"); 
									finishComm(COMMIT_ERROR, callingPage, null);
									break;
								} else if (wasVoided(mo)) {
									setStatus(FAILED);
									tranSuccessful = false;
									Debug.println("  setting status = failed"); 
									finishComm(COMMIT_ERROR, callingPage, null);
									break;
								}
								// one of the mo's started to print but was unable to complete leaving the dispenser in an unstable state.
								// Serial numbers must be recalculated and the dispenser must be reloaded before printing can continue.
								else {
									setStatus(LOAD_REQUIRED);
									tranSuccessful = false;
									Debug.println("  setting status = loadreq"); 
									finishComm(COMMIT_ERROR, callingPage, null);
									break;
								}
							}
						}

						dialog.closeDialog(Dialogs.CLOSED_OPTION);
					}

					// restart screen timeout.
					IdleBackoutTimer.start();

					if (tranSuccessful) {
						Debug.println("All is well...printing the receipts."); 
						/*
						 * Save the print environment so a reprint can be done later.
						 */
//						saveRecvEnvironment(null);

						/*
						 * Print applicable agent/consumer receipts received from server
						 */
						if (isReceiveAgentAndCustomerReceipts() || isReceiveAgentReceipt()) {
							tran.printReceipt(false, true);
						}
						
						if (isReceiveAgentAndCustomerReceipts() || isReceiveCustomerReceipt()) {
				    		tran.printReceipt(true, true);
						}
						
						setStatus(COMPLETED);

						// Void the last document in the dispenser, if need be. Can't do this any earlier because the DiagLog has to be in order.
						if ("DG".equals(getProfileMoneyGramChecks()))
							voidIfLastForm(mot.getDispenser());

						finishComm(COMMIT_DONE, callingPage, null);
					}
				} else {
					int status = getStatus();
					if (status != MessageFacadeParam.INVALID_CHECK_NUMBER) {
						if (status != ClientTransaction.CANNOT_COMPLETE_AS_DEFINED) {
							setStatus(ClientTransaction.COMM_ERROR);
							Debug.println("  setting status = 91"); 
						}
						finishComm(COMMIT_COMMERROR, callingPage, Boolean.FALSE);
					}
				}
			}
		});
		tPrint.start();
		return tPrint;
	}

	public synchronized Thread moDialogShow(final MOPrintingDialog dialog) {
		final Thread tDS = new Thread(new Runnable() {
			@Override
			public void run() {
				startOfMODialogShow();
				if (tranSuccessful)
					Dialogs.showPanel(dialog);
			}
		});
		tDS.start();

		return tDS;
	}

	public void startOfMODialogShow() {
		if (wait.isWaitFlag()) {
			try {
				synchronized (wait) {
					while (wait.isWaitFlag()) {
						wait.wait();
					}
				}
			} catch (InterruptedException ie) {
				Debug.println("InterruptedException caught. " + ie);
			}
		}
	}

	public void endOfSend() {
        synchronized (wait) {
			wait.setWaitFlag(false);
			wait.notifyAll();
        }
	}

	public void startOfPrint() throws InterruptedException {
		if (wait.isWaitFlag()) {
			try {
		        synchronized (wait) {
					while (wait.isWaitFlag()) {
						wait.wait();
					}
		        }
			} catch (InterruptedException ie) {
				Debug.println("InterruptedException caught. " + ie);
			}
		}
	}

	/**
	 * Check the amount of the receive against limits.
	 */
	private int checkLimits(BigDecimal amount) {
		dailyTotal = UnitProfile.getInstance().getDailyTotal();
		// Debug.println("**** moneygramreceivetransaciton.checkLimits() dailyTotal = " + dailyTotal.toString());
		if (amount.compareTo(maxTransactionAmount) > 0)
			return TRANSACTION_AMOUNT_EXCEEDED;
		return OK;
	}

	/**
	 * Data coming from the message will be with an implied decimal point, so
	 * set the BigDecimal scale to the correct value.
	 */
	public BigDecimal getAmount() {
		if (this instanceof SendReversalTransaction) {
			SendReversalTransaction rt = (SendReversalTransaction) this;
			BigDecimal sendAmount = transactionLookupResponse.getPayload().getValue().getSendAmounts().getSendAmount();
			if (rt.getRefundFee()) {
				BigDecimal fee = transactionLookupResponse.getPayload().getValue().getSendAmounts().getTotalSendFees();
				return sendAmount.add(fee);
			} else {
				return sendAmount;
			}
		} else {
			BigDecimal receive = null;
			if ((transactionLookupResponse != null) && (transactionLookupResponse.getPayload() != null) && (transactionLookupResponse.getPayload().getValue() != null)) {
				receive = transactionLookupResponse.getPayload().getValue().getReceiveAmounts().getReceiveAmount();
			}
			return receive;
		}
	}

	/**
	 * Get the currency that the transaction will be received in. If it is not
	 * specified by the host, use the country currency for this agent.
	 */
	public String getCurrency() {
		String sCurrency;
		if (detail != null) {
			sCurrency = detail.getReceiveCurrency();
		} else {
			sCurrency = transactionLookupResponse.getPayload().getValue().getReceiveAmounts().getReceiveCurrency();
		}
		if ((sCurrency == null || sCurrency.isEmpty())) {
			Debug.println("Receive Currency Not Found - Using Agent Base Currency");
			sCurrency = agentBaseCurrency();
		}
		return sCurrency;
	}

	public boolean isAnswerCorrect() {
		return answerCorrect;
	}

	public void setQuestionAsked(boolean asked) {
		questionAsked = asked;
	}

	public boolean isQuestionAsked() {
		return questionAsked;
	}

	public void setAnswerCorrect(boolean correct) {
		answerCorrect = correct;
	}

	public boolean isAmount1Cash() {
		return amount1Cash;
	}

	public void setAmount1Cash(boolean cash) {
		amount1Cash = cash;
	}

	public String getReferenceNumber() {
		return paidInfo.getReferenceNumber();
	}

	public String getSerialNumberFirstMoneyOrder() {
		return mo1.getSerialNumber().toString();
	}

	// this method returns the Serial Number of the Customers MoneyOrder. If their transaction is canceled when printing the Agents MoneyOrder or the
	// Customers amount is zero dollars the serial number is set to zero.
	public String getSerialNumberSecondMoneyOrder() {
		if (mo2 == null || ((mo2.getItemAmount().compareTo(ZERO) <= 0) || !(MoneyOrderPrintStatusType.CONFIRMED_PRINTED.equals(mo2.getPrintStatus()))))
			return "0"; 
		return mo2.getSerialNumber()+"";
	}

	/** Return the serial number of mo1 or mo2 if either is limbo, otherwise 0. */
	public String getDispenserJammedSerialNumber() {
		if ((mo2 != null) && (MoneyOrderPrintStatusType.CONFIRMED_NOT_PRINTED.equals(mo2.getPrintStatus()))) {
			return mo1.getSerialNumber()+"";
		} else if ((mo2 != null) && (MoneyOrderPrintStatusType.CONFIRMED_NOT_PRINTED.equals(mo2.getPrintStatus()))) {
			return mo2.getSerialNumber()+"";
		} else {
			return "0"; 
		}
	}

	public void setReferenceNumber(String referenceNumber) {
		paidInfo.setReferenceNumber(referenceNumber);
	}

	public ReceiverInfo getReceiver() {
		return paidInfo.getReceiver();
	}

	public void setReceiver(ReceiverInfo rec) {
		paidInfo.setReceiver(rec);
	}

	public void setAgentCheckNumber(BigInteger number) {
		paidInfo.setAgentCheckNumber(number);
	}

	public BigInteger getAgentCheckNumber() {
		return paidInfo.getAgentCheckNumber();
	}

	public void setCustomerCheckNumber(BigInteger number) {
		paidInfo.setCustomerCheckNumber(number);
	}

	public BigInteger getCustomerCheckNumber() {
		return paidInfo.getCustomerCheckNumber();
	}

	public void setOtherPayoutId(String number) {
		paidInfo.setOtherPayoutId(number);
	}

	public String getOtherPayoutIdLast4() {
		return StringUtility.stringToMaskedString(paidInfo.getOtherPayoutId());
	}

	public void setOtherPayoutSwiped(boolean s) {
		paidInfo.setOtherPayoutSwiped(s);
	}

	public void setOtherPayoutType(String Type) {
		paidInfo.setOtherPayoutType(Type);
	}

	/**
	 * Print a receipt for this transaction.
	 *
	 * @param customerReceipt
	 *            whether or not this is for the customer.
	 * @param reprint
	 *            whether or not this is a reprinted version.
	 */
	public void printReceipt(boolean customerReceipt, boolean reprint) {

		boolean bPrimaryAvailable;
		boolean bSecondaryAvailable = false;

		if (!customerReceipt) {
			bPrimaryAvailable = isAgentPdfPrimaryAvailable() || isAgentPrtPrimaryAvailable();
		} else {
			bPrimaryAvailable = isCustPdfPrimaryAvailable() || isCustPrtPrimaryAvailable();
			bSecondaryAvailable = isCustPdfSecondaryAvailable() || isCustPrtSecondaryAvailable();
		}

		if (bPrimaryAvailable || bSecondaryAvailable) {
			if (bPrimaryAvailable) {
				if (!customerReceipt) {
					// agent receipt
					while (pdfReceiptAgent1 != null && !pdfReceiptAgent1.print()) {
						if (cancelPrint(reprint)) {
							bPrintFailure = true;
							break;
						}
					}

					// text receipt type
					while (prtReceiptAgent1 != null && !prtReceiptAgent1.print()) {
						if (cancelPrint(reprint)) {
							bPrintFailure = true;
							break;
						}
					}
				} else {
					// customer receipt
					while (pdfReceiptCust1 != null && !pdfReceiptCust1.print()) {
						if (cancelPrint(reprint)) {
							bPrintFailure = true;
							break;
						}
					}

					while (prtReceiptCust1 != null && !prtReceiptCust1.print()) {
						if (cancelPrint(reprint)) {
							bPrintFailure = true;
							break;
						}
					}
				}
				if (bSecondaryAvailable && !bPrintFailure) {
					if (customerReceipt) {
						while (pdfReceiptCust2 != null && !pdfReceiptCust2.print()) {
							if (cancelPrint(reprint)) {
								bPrintFailure = true;
								break;
							}
						}
						// text receipt type
						while (prtReceiptCust2 != null && !prtReceiptCust2.print()) {
							if (cancelPrint(reprint)) {
								bPrintFailure = true;
								break;
							}
						}
					}
				}
			}
		}
	}

	/**
	* Print a Warning receipt for this transaction.
	* 
	*/
 	public void printReceipt(JComponent parent) {
 		if (TEPrinter.getPrinter().isReady(parent))  {
 			try {
 				// Set the customer Check Number to zero so that the customer check number
 				// for a document that is not printed is not displayed on the receipt.
 				if (getSerialNumberSecondMoneyOrder().equalsIgnoreCase("0"))	
 					paidInfo.setCustomerCheckNumber(BigInteger.ZERO);	

 				if (getDispenserJammedSerialNumber().equals("0")) 	
 					ReceiptReport.printWarningMGReceipt(receiptFormat+"ns", paidInfo);                
 				else
 					ReceiptReport.printWarningMGReceipt(receiptFormat, paidInfo);
 			}
 			catch (Exception e) {
 				ErrorManager.handle(e, "printReceiptError", Messages.getString("ErrorMessages.printReceiptError"), ErrorManager.ERROR, 	
                                                       true);
 			}
 		}
 	}

	/**
	* Print a Warning receipt for this transaction.
	* 
	*/
	public void printWarningReceipt(javax.swing.JComponent parent) {
		if (TEPrinter.getPrinter().isReady(parent))  {
			try {
				// Set the customer Check Number to zero so that the customer check number
				// for a document that is not printed is not displayed on the receipt.
				if (this.getSerialNumberSecondMoneyOrder().equalsIgnoreCase("0"))	
					paidInfo.setCustomerCheckNumber(BigInteger.ZERO);	                
	
				ReceiptReport.printWarningMGReceipt(receiptFormat, paidInfo);
			}
			catch (Exception e) {
				ErrorManager.handle(e, "printReceiptError", Messages.getString("ErrorMessages.printReceiptError"), ErrorManager.ERROR, 	
		                                                       true);
			}
		}
	}

	/**
	 * If a test question is on the detail, it must be available.
	 */
	public boolean isTestQuestionAvailable() {
		Object testQuestion = this.getSearchProfilesData().getCurrentValue(FieldKey.TESTQUESTION_KEY.getInfoKey());
		return (! testQuestion.toString().isEmpty());
	}

	public String getQuestion() {
		return this.getSearchProfilesData().getCurrentValue(FieldKey.TESTQUESTION_KEY.getInfoKey());
	}

	public String getAnswer() {
		return this.getSearchProfilesData().getCurrentValue(FieldKey.TESTANSWER_KEY.getInfoKey());
	}

	public boolean isOverDailyLimit() {
		// check against DAILY_LIMIT_FINANCIAL...can not go over
		int limitCheck = checkLimits(getAmount());
		if (limitCheck == DAILY_LIMIT_EXCEEDED) {
			String formattedDailyLimit = FormatSymbols.convertDecimal(dailyLimit.toString());
			String formattedDailyTotal = FormatSymbols.convertDecimal(dailyTotal.toString());
			Dialogs.showWarning("dialogs/DialogDailyLimitExceeded.xml", formattedDailyLimit, formattedDailyTotal); 					
			return true;
		}
		return false;
	}

	public boolean isOverReceiveLimit() {
		int limitCheck = checkLimits(getAmount());
		if (limitCheck == TRANSACTION_AMOUNT_EXCEEDED) {
			Dialogs.showError("dialogs/DialogMaxReceiveAmountExceeded.xml", maxTransactionAmount.toString().concat(" " + transactionLookupResponse.getPayload().getValue().getReceiveAmounts().getReceiveCurrency())); 					
			return true;
		}
		return false;
	}

	/**
	 * Attempt to set the reference number of this transaction. Query the host
	 * for a matching detail record for this ref. #. If there is one and it is
	 * within our store limits, return true. Otherwise, return false. If we
	 * exceed any limits, give the agent a dialog box indicating the problem.
	 * The TransactionResponse object retrieved will be saved to be accessed via
	 * getTransactionResponse().
	 */

	/**
	 * Attempt to set the reference number of this transaction. Query the host
	 * for a matching detail record for this ref. #. If there is one and it is
	 * within our store limits, return true. Otherwise, return false. If we
	 * exceed any limits, give the agent a dialog box indicating the problem.
	 * The TransactionResponse object retrieved will be saved to be accessed via
	 * getTransactionResponse().
	 */

	public void transactionLookup(final String refNumber, final String pin, final CommCompleteInterface callingPage, final String purposeOfLookup, final boolean isInitial) {
		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {
				boolean status;
				try {
			    	TransactionLookupRequest transactionLookupRequest = new TransactionLookupRequest();
					transactionLookupCommon(transactionLookupRequest, refNumber, pin, callingPage, purposeOfLookup, isInitial);
					status = isClientStatus();
					setMissingTransaction(false);
					TransactionLookupResponse.Payload transactionLookupResponsePayload = getTransactionLookupResponse() != null
							&& getTransactionLookupResponse().getPayload() != null
									? getTransactionLookupResponse().getPayload().getValue() : null;
					if (transactionLookupResponsePayload != null) {
						if (! purposeOfLookup.equals(DWValues.TRX_LOOKUP_STATUS)) {
							MoneyGramReceiveTransaction.this.setUnreceivableTransaction(false);
							
							String mgiTransactionSessionID = transactionLookupResponsePayload.getMgiSessionID();
							MoneyGramReceiveTransaction.this.setMgiTransactionSessionID(mgiTransactionSessionID);
						}
						
						// Save all of the values returned by the transactionLookup call in the data collection set.
						
						createNewReceiveDetailInfo();							    				
						
						if (detail != null) {
							for (KeyValuePairType objCurrentValues : transactionLookupResponsePayload.getCurrentValues().getCurrentValue()) {
						    
						    	if (objCurrentValues != null) {
						       		String strXmlTag = objCurrentValues.getInfoKey();
						       		String strValue = objCurrentValues.getValue().getValue();
						       		
									if ((strXmlTag != null) && (strValue != null)) {
										if (FieldKey.RECEIVER_FIRSTNAME_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
											detail.setReceiverFirstName(strValue.trim());
										else if(FieldKey.RECEIVER_LASTNAME_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
											detail.setReceiverLastName(strValue.trim());
										else if(FieldKey.RECEIVER_MIDDLENAME_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
											detail.setReceiverMiddleName(strValue.trim());		
										else if(FieldKey.RECEIVER_LASTNAME2_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
											detail.setReceiverLastName2(strValue.trim());
										else if(FieldKey.SENDER_FIRSTNAME_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
											detail.setSenderFirstName(strValue.trim());
										else if(FieldKey.SENDER_LASTNAME_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
											detail.setSenderLastName(strValue.trim());
										else if(FieldKey.SENDER_MIDDLENAME_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
											detail.setSenderMiddleName(strValue.trim());
										else if(FieldKey.SENDER_LASTNAME2_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
											detail.setSenderLastName2(strValue.trim());
										else if(FieldKey.SENDER_ADDRESS_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
											detail.setSenderAddress(strValue.trim());
										else if(FieldKey.SENDER_CITY_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
											detail.setSenderCity(strValue.trim());
										else if(FieldKey.SENDER_POSTALCODE_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
											detail.setSenderZipCode(strValue.trim());
										else if(FieldKey.SENDER_COUNTRY_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
											detail.setSenderCountry(strValue.trim());	
										else if(DWValues.MF_RECV_AMT_ORIG.equalsIgnoreCase(strXmlTag))
											detail.setReceiveAmount(new BigDecimal(strValue.trim()));	
										else if(DWValues.AGENT_CHECK_AUTHORIZATION_NUMBER.equalsIgnoreCase(strXmlTag))
											detail.setAgentCheckAuthorizationNumber(strValue.trim());
										else if(DWValues.ORGINATING_COUNTRY.equalsIgnoreCase(strXmlTag))
											detail.setOriginatingCountry(strValue.trim());
										else if(DWValues.DESTINATION_COUNTRY.equalsIgnoreCase(strXmlTag))
											detail.setDestinationCountry(strValue.trim());
										else if (DWValues.INDICATIVE_EXCHANGE_RATE.equalsIgnoreCase(strXmlTag))
											detail.setIndicativeExchangeRate(strValue.trim());
										else if(DWValues.INDICATIVE_RECEIVE_AMOUNT.equalsIgnoreCase(strXmlTag))
											detail.setIndicativeReceiveAmount(strValue.trim());
										else if(FieldKey.MESSAGEFIELD1_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
											detail.setMessageField1(strValue.trim());
										else if(FieldKey.MESSAGEFIELD2_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
											detail.setMessageField2(strValue.trim());
										else if(FieldKey.SENDER_PRIMARYPHONE_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
											detail.setSenderHomePhone(strValue.trim());
										else if(FieldKey.RECEIVER_COUNTRY_KEY.getInfoKey().equalsIgnoreCase(strXmlTag))
											detail.setReceiverCountry(strValue.trim());
										else if("dateTimeSent".equalsIgnoreCase(strXmlTag))
											detail.setDateTimeSent(strValue.trim());
									}
								}
							}
					        detail.setTransactionStatus(transactionLookupResponsePayload.getTransactionStatus());
							detail.setReceiveCurrency(transactionLookupResponsePayload.getReceiveAmounts() != null
									? transactionLookupResponsePayload.getReceiveAmounts().getReceiveCurrency() : null);
							if (transactionLookupResponsePayload.getRedirectInfo() != null) {
								detail.setRedirectIndicator(true);
								detail.setRedirectInfo(transactionLookupResponsePayload.getRedirectInfo());
							}
							
					        // make sure the currency is set to something useful
			                 
					        if (detail.getReceiveCurrency() == null || detail.getReceiveCurrency().length() == 0)  {
			                	detail.setReceiveCurrency(new ClientInfo().getAgentCountry().getBaseReceiveCurrency());
					        }
			                 
					        // format the amount field
			                 
					        detail.setReceiveAmount(detail.getReceiveAmount());  /*Not sure why this was here, but brought it along with the above IF*/
					        
						}
				        // Determine the form free status.
			                 
				        TransactionStatusType ts = transactionLookupResponsePayload.getTransactionStatus();
						
				        // For send reversal lookups, check if a cancel or a refund is allowed.
				        
				        if (purposeOfLookup.equals(DWValues.TRX_LOOKUP_SENDREV)) {
				        	String flag = dataCollectionData.getCurrentValue("isRefundAllowedFlag");
				        	refundAllowed = flag.trim().equalsIgnoreCase("TRUE");
				        	flag = dataCollectionData.getCurrentValue("isCancelAllowedFlag");
				        	cancelAllowed = flag.trim().equalsIgnoreCase("TRUE");
				        	Debug.println("cancelAllowed = " + cancelAllowed);
				        	Debug.println("refundAllowed = " + refundAllowed);
				        }
					
//				        List<InfoBase> alInfos = transactionLookupResponse.getPayload().getValue().getInfos();
//				        if (alInfos != null) {
//				        	for (InfoBase objInfoBase : alInfos) {
//				        		if (objInfoBase != null && objInfoBase instanceof CategoryInfo) {
//				        			String strXmlTag = objInfoBase.getXmlTag();
//				        			String strLabel = objInfoBase.getLabel();
//				        			String strStandAloneLbl = objInfoBase.getStandAloneLabel();
//				        			Integer strDispOrder = objInfoBase.getDisplayOrder();
//				        			String strExtdedHelpTxt = objInfoBase.getExtendedHelpText();
//				        			Debug.println("CategoryInfo XML TAG: "+ strXmlTag+   "CategoryInfo Label: "+ strLabel+   "CategoryInfo StandAloneLabel: "+ strStandAloneLbl+   "CategoryInfo DisplayOrder: "+ strDispOrder+   "CategoryInfo ExtendedHelpText: " + strExtdedHelpTxt);
//				        		}						        	 
//				        	}
//				        }
				        
				        // pull all the receiver fields 
					
				        SendAmountInfo sendAmountInfo = new SendAmountInfo();
				        
				        if (transactionLookupResponsePayload != null && transactionLookupResponsePayload.getSendAmounts() != null) {
				        	com.moneygram.agentconnect.SendAmountInfo sendAmounts = transactionLookupResponsePayload.getSendAmounts();
					        sendAmountInfo.setSendAmount(sendAmounts.getSendAmount());
					        sendAmountInfo.setSendCurrency(sendAmounts.getSendCurrency());
					        sendAmountInfo.setTotalSendFees(sendAmounts.getTotalSendFees());
					        sendAmountInfo.setTotalSendTaxes(sendAmounts.getTotalSendTaxes());
					        sendAmountInfo.setTotalAmountToCollect(sendAmounts.getTotalAmountToCollect());
				        }
				        
				        setSendAmountInfo(sendAmountInfo);
				        
				        ReceiverInfo receiver = new ReceiverInfo();
				        
				        if (receiver != null && detail != null) {
							receiver.setFirstName(detail.getReceiverFirstName());
							receiver.setMiddleName(detail.getReceiverMiddleName());
							receiver.setLastName(detail.getReceiverLastName());
							receiver.setSecondLastName(detail.getReceiverLastName2());
							receiver.setAddress(detail.getReceiverAddress());
							receiver.setAddress2(detail.getReceiverAddress2());
							receiver.setAddress3(detail.getReceiverAddress3());
							receiver.setAddress4(detail.getReceiverAddress4());
							receiver.setState(detail.getReceiverState());
							receiver.setZip(detail.getReceiverZipCode());
							receiver.setCity(detail.getReceiverCity());
							receiver.setCountry(detail.getReceiverCountry());
							receiver.setHomePhone(detail.getReceiverPhone());	
							receiver.setMessage1(detail.getMessageField1());
							receiver.setMessage2(detail.getMessageField2());							
						}
						setReceiver(receiver);
						setReferenceNumber(refNumber);
						
						// make sure the currency is set to something useful
						
						if (transactionLookupResponsePayload != null && (transactionLookupResponsePayload.getReceiveAmounts() == null || transactionLookupResponsePayload.getReceiveAmounts().getReceiveCurrency() == null 
								|| transactionLookupResponsePayload.getReceiveAmounts().getReceiveCurrency().isEmpty())) {
							transactionLookupResponsePayload.getReceiveAmounts().setReceiveCurrency(new ClientInfo().getAgentCountry().getBaseReceiveCurrency());
							transactionLookupResponsePayload.getReceiveAmounts().setReceiveAmount(transactionLookupResponsePayload.getReceiveAmounts().getReceiveAmount());
						}
						
						// Determine if the receive transaction has been staged.
						
						String transactionStagingFlag = MoneyGramReceiveTransaction.this.getDataCollectionData().getCurrentValue(FieldKey.ISTRANSACTIONSTAGED_KEY.getInfoKey());
						boolean flag = ((transactionStagingFlag != null) && (transactionStagingFlag.equalsIgnoreCase("TRUE")));
						MoneyGramReceiveTransaction.this.setFormFreeTransaction(flag);
						
						if (((ts != null) && (ts.equals(TransactionStatusType.AVAIL))) || (! detail.isOkForAgent())) {
							getNotAvailableProfileValues();
							status = Boolean.TRUE;
						} else {
							if (getProfileValues()) {
								status = Boolean.TRUE;
							} else {
								setMissingTransaction(true);
								status = Boolean.FALSE;
							}
						}
					} else {
						status =  Boolean.FALSE;
					}
				} catch (MessageFacadeError mfe) {
					if ((mfe.hasErrorCode(MessageFacadeParam.INVALID_TRANSACTION)) || (mfe.hasErrorCode(MessageFacadeParam.INVALID_PIN))) {
						pinpadMfe = mfe;
					} else {
						pinpadMfe = null;
					}
					return Boolean.FALSE;
				}
				return status;
			}
		};
		MessageFacade.run(logic, callingPage, TRANSACTION_LOOKUP, Boolean.FALSE);
	} 

	private void setMgiTransactionSessionID(String mgiTransactionSessionID) {
		this.mgiTransactionSessionID = mgiTransactionSessionID;
	}
	
	public String getMgiTransactionSessionID() {
		return this.mgiTransactionSessionID;
	}

	public void setSendAmountInfo(SendAmountInfo sendAmountInfo) {
		this.sendAmountInfo = sendAmountInfo;
	}
	
	public SendAmountInfo getSendAmountInfo() {
		return this.sendAmountInfo;
	}

	public BigDecimal getThirdPartyLimit() {
		return this.thirdPartyLimit;
	}

	public BigDecimal getDefaultPayout() {
		return expectedPayout;
	}

	public BigDecimal getReversalDefaultPayout(String currency) {

		BigDecimal ep = getProductProfileDecimalOption("MAX_AMOUNT_PER_ITEM",
				currency); 
		ProfileItem item = getProductProfileOption(
				"DEFAULT_CASH_PAYOUT_AMOUNT", currency); 

		if (item != null) {
			ep = item.bigDecimalValue();
		}
		return ep;
	}

	public void setAgentAmount(BigDecimal newAmount) {
		if (detail != null && getAmount().compareTo(newAmount) <= 0) {
			paidInfo.setAgentAmount(getAmount().toString());
			paidInfo.setCustomerAmount(ZERO.toString());
		} else {
			if (detail != null) {
				paidInfo.setAgentAmount(getAmount().toString());
			} else {
				paidInfo.setAgentAmount(ZERO.toString());
			}
			String oldAmount = paidInfo.getAgentAmount();
			paidInfo.setAgentAmount(newAmount.toString());
			paidInfo.setCustomerAmount(((new BigDecimal(oldAmount))
					.subtract(newAmount)).toString());
		}
	}

	public void setAgentAmountOnly(BigDecimal newAmount) {
		paidInfo.setAgentAmount(newAmount.toString());
	}

	public void setCustomerAmount(BigDecimal newAmount) {
		if (detail != null && getAmount().compareTo(newAmount) <= 0) {
			paidInfo.setCustomerAmount(getAmount().toString());
			paidInfo.setAgentAmount(ZERO.toString());
		} else {
			paidInfo.setCustomerAmount(newAmount.toString());
			paidInfo.setAgentAmount(getAmount().subtract(newAmount).toString());
		}
	}

	public void setCustomerAmountOnly(BigDecimal newAmount) {
		if (newAmount != null) {
			paidInfo.setCustomerAmount(newAmount.toString());
		}
	}

	public void setOtherPayoutAmount(BigDecimal newAmount) {
		if (newAmount != null) {
			paidInfo.setOtherPayoutAmount(newAmount.toString());
		}
	}

	public BigDecimal getAgentAmount() {
		String sAgentAmt = paidInfo.getAgentAmount();
		if ((sAgentAmt == null) || (sAgentAmt.equals("")))
			return ZERO;
		else
			return new BigDecimal(sAgentAmt);
	}

	public BigDecimal getCustomerAmount() {
		String sCustomerAmt = paidInfo.getCustomerAmount();
		if ((sCustomerAmt == null) || (sCustomerAmt.equals("")))
			return ZERO;
		return new BigDecimal(sCustomerAmt);
	}

	public BigDecimal getCustPayoutAmount() {
		String sOtherPayoutAmt = paidInfo.getOtherPayoutAmount();
		if ((sOtherPayoutAmt == null) || (sOtherPayoutAmt.equals("")))
			return ZERO;
		return new BigDecimal(sOtherPayoutAmt);
	}

	/**
	 * Set the user of this transaction. The user has to have access to both
	 * this transaction and the money order transaction contained within.
	 */
	@Override
	public boolean setUser(int userID) {
		return super.setUser(userID) && mot.setUser(userID);
	}

	/** Calculates check digit and returns serial number including check digit. */
	public BigInteger getSerialNumberWithCheckDigit(BigInteger serialNumber) {
		long sn;
		try {
			sn = serialNumber.longValue();
		} catch (NumberFormatException e) {
			sn = 0;
		}
		String retVal = ""+serialNumber + checkDigit(sn);
		return new BigInteger(retVal);
	}

	/**
	 * Computes check digit.
	 */
	private static long checkDigit(long serial) {
		return (serial % 11) % 10;
	}

	private String convertCCYYMMDDToDateYYYYMMDD(String date) {
		if ((date != null) && (date.length() == 10)) {
			return date.substring(0, 4) + date.substring(5, 7) + date.substring(8, 10);
		} else {
			return "";
		}
	}
	
	protected void logISIEntry() {
    	String checkOneType="";
    	String checkTwoType="";
		    	
    	if (modf1 != null && getProfileMoneyGramChecks().equalsIgnoreCase("DG")){
    		checkOneType = modf1.getDeltagramFlag();
    	}
    	
    	if (modf2 != null && getProfileMoneyGramChecks().equalsIgnoreCase("DG")) {
    		checkTwoType = modf2.getDeltagramFlag();
    	}
   
    	
    	
        Object[] receiveItems = {
            paidInfo.getReferenceNumber(),
            paidInfo.getAgentCheckNumber(),
            paidInfo.getAgentAmount(), 
            getAuthorizationCode(),
            paidInfo.getCustomerCheckNumber(),
            paidInfo.getCustomerAmount(),
            paidInfo.getCurrency(),
            getBestDataValue(FieldKey.SENDER_FIRSTNAME_KEY), 
            getBestDataValue(FieldKey.SENDER_MIDDLENAME_KEY),
            getBestDataValue(FieldKey.SENDER_LASTNAME_KEY),
            getBestDataValue(FieldKey.RECEIVER_FIRSTNAME_KEY),
            getBestDataValue(FieldKey.RECEIVER_MIDDLENAME_KEY),
            getBestDataValue(FieldKey.RECEIVER_LASTNAME_KEY),
            getBestDataValue(FieldKey.RECEIVER_LASTNAME2_KEY),
            getBestDataValue(FieldKey.RECEIVER_PRIMARYPHONE_KEY),
            getBestDataValue(FieldKey.RECEIVER_ADDRESS_KEY),
            getBestDataValue(FieldKey.RECEIVER_CITY_KEY),
            DWValues.convertIsoSubdivToState(getBestDataValue(FieldKey.RECEIVER_COUNTRY_SUBDIVISIONCODE_KEY)),
            getBestDataValue(FieldKey.RECEIVER_POSTALCODE_KEY),
            getBestIdTypeValue(idTypes.ID1),
            getBestIdNumber(idTypes.ID1),
            DWValues.convertIsoSubdivToState(getBestDataValue(FieldKey.RECEIVER_PERSONALID1_ISSUE_COUNTRY_SUBDIVISIONCODE_KEY)),
            getBestDataValue(FieldKey.RECEIVER_PERSONALID1_ISSUE_COUNTRY_KEY),
            getBestIdTypeValue(idTypes.ID2),
            getBestIdNumber(idTypes.ID2),
            convertCCYYMMDDToDateYYYYMMDD(getBestDataValue(FieldKey.RECEIVER_DOB_KEY)), 
            getBestOccupationValue(FieldKey.RECEIVER_OCCUPATION_KEY, FieldKey.RECEIVER_OCCUPATIONOTHER_KEY),
            getBestDataValue(FieldKey.THIRDPARTY_RECEIVER_FIRSTNAME_KEY),
            getBestDataValue(FieldKey.THIRDPARTY_RECEIVER_LASTNAME_KEY),
            getBestDataValue(FieldKey.THIRDPARTY_RECEIVER_ADDRESS_KEY),		
            getBestDataValue(FieldKey.THIRDPARTY_RECEIVER_CITY_KEY),
            DWValues.convertIsoSubdivToState(getBestDataValue(FieldKey.THIRDPARTY_RECEIVER_COUNTRY_SUBDIVISIONCODE_KEY)),
            getBestDataValue(FieldKey.THIRDPARTY_RECEIVER_POSTALCODE_KEY),
            getBestDataValue(FieldKey.THIRDPARTY_RECEIVER_COUNTRY_KEY),
            getBestDataValue(FieldKey.THIRDPARTY_RECEIVER_PERSONALID2_TYPE_KEY),
            getBestDataValue(FieldKey.THIRDPARTY_RECEIVER_PERSONALID2_NUMBER_KEY),
            convertCCYYMMDDToDateYYYYMMDD(getBestDataValue(FieldKey.THIRDPARTY_RECEIVER_DOB_KEY)),
            getBestOccupationValue(FieldKey.THIRDPARTY_RECEIVER_OCCUPATION_KEY, FieldKey.THIRDPARTY_RECEIVER_OCCUPATIONOTHER_KEY),
            getBestDataValue(FieldKey.THIRDPARTY_RECEIVER_ORGANIZATION_KEY),
            getBestDataValue(FieldKey.RECEIVER_BIRTH_CITY_KEY),   
            getBestDataValue(FieldKey.RECEIVER_BIRTH_COUNTRY_KEY),
            paidInfo.getIdDateOfIssue() != null ? formatIsiDateField(paidInfo.getIdDateOfIssue()) : "",
            paidInfo.getIdCityOfIssue(),  
            paidInfo.getIdCountryOfIssue(),
            this.getSearchProfilesData().getCurrentValue(FieldKey.key("originatingCountry").getInfoKey()),
            checkOneType,
            checkTwoType,            
            this.isSendAmtNeeded() && detail.getRedirectInfo() != null ? detail.getRedirectInfo().getOriginalSendAmount() : "",
            this.isSendFeeNeeded() && detail.getRedirectInfo() != null ? detail.getRedirectInfo().getOriginalSendFee() : "",
            (this.isSendAmtNeeded() || this.isSendFeeNeeded()) && detail.getRedirectInfo() != null ? detail.getRedirectInfo().getOriginalSendCurrency() : "",
            this.isFxNeeded() && detail.getRedirectInfo() != null ? detail.getRedirectInfo().getOriginalExchangeRate() : "",
       		paidInfo.getOtherPayoutType(),
       		paidInfo.getOtherPayoutAmount(),
       		paidInfo.isOtherPayoutSwiped() ? "Y" : "N",
			getOtherPayoutIdLast4().substring(4),
			getBestDataValue(FieldKey.SENDER_ADDRESS_KEY),
			getBestDataValue(FieldKey.SENDER_CITY_KEY),
			DWValues.convertIsoSubdivToState(getBestDataValue(FieldKey.SENDER_COUNTRY_SUBDIVISIONCODE_KEY)),
			getBestDataValue(FieldKey.SENDER_POSTALCODE_KEY),
			getBestDataValue(FieldKey.SENDER_COUNTRY_KEY),
			getBestDataValue(FieldKey.SENDER_LASTNAME2_KEY),
			getBestDataValue(FieldKey.SENDER_PRIMARYPHONE_KEY),
			getBestDataValue(FieldKey.MESSAGEFIELD1_KEY),
			getBestDataValue(FieldKey.DESTINATION_COUNTRY_KEY)
        };

		List<String> fields = new ArrayList<String>();
		IsiReporting isi = new IsiReporting();
		fields.add(MainPanelDelegate.clMoneyGramReceive);
		getIsiHeader(fields);
		for (int i = 0; i < receiveItems.length; i++) {
			fields.add(nullToSpace(receiveItems[i]));
		}

		isi.print(fields, isi.ISI_LOG);
		if (MainPanelDelegate.getIsiExeCommand()) {
			isi.print(fields, isi.EXE_LOG);
		}
	}

	@Override
	public String toString() {
		String mo1Str = null;
		String mo2Str = null;
		if (mo1 != null) {
			mo1Str = mo1.toString();
		}
		if (mo2 != null) {
			mo2Str = mo2.toString();
		}
		return "MoneyGramReceiveTransactions  mo1 {" + mo1Str + " { \n mo2 { " + mo2Str + "\n";   
	}

	@Override
	public boolean isFinancialTransaction() {
		return true;
	}

	@Override
	public void setFinancialTransaction(boolean b) {
		// Do nothing.
	}

	@Override
	public boolean isCheckinRequired() {
		return !(DeltaworksStartup.getInstance().haveConnected());
	}

	// returns the profile item MG_CHECKS. Valid values are "DG" and "MTC"
	public String getProfileMoneyGramChecks() {
		return UnitProfile.getInstance().get("MG_CHECKS", "DG");
	}
	
	public boolean isDeltaGramChecks() {
		return this.getProfileMoneyGramChecks().equals("DG");
	}

	// returns the profile item MG_CHECKS. Valid values are "DG" and "MTC"
	public boolean isProfileUseOurPaper() {
		boolean retVal = false;
		if ("Y".equals(UnitProfile.getInstance().get("USE_OUR_PAPER", "Y")))
			retVal = true;
		return retVal;
	}
	
	public static final int PAYOUT_DELTA_GRAM = 1;
	public static final int PAYOUT_MONEY_TRANSFER_CHECK = 2;
	public static final int PAYOUT_CASH = 3;
	
	public int getPayoutMethod() {
		if (isProfileUseOurPaper()) {
			if (getProfileMoneyGramChecks().equals("DG")) {
				return PAYOUT_DELTA_GRAM;
			} else {
				return PAYOUT_MONEY_TRANSFER_CHECK;
			}
		} else {
			return PAYOUT_CASH;
		}
	}

	/**
	 * Returns the 5-digit agent check authorization code, used internationally.
	 */
	public String getAuthorizationCode() {
		String agentCheckAuthorizationNumber = receiveInfo.getPayload().getValue().getAgentCheckAuthorizationNumber();
		return agentCheckAuthorizationNumber != null ? agentCheckAuthorizationNumber : "";
	}

	/** Returns "Authorization: " followed by the 5-digit authorization code. */
	public String getAuthorizationMessage() {
		if (receiveInfo == null)
			return "";
		
		if ("DG".equals(this.getProfileMoneyGramChecks()))
			return "";
		
		return Messages.getString("MGRWizardPage8.auth2") + " "	+ getAuthorizationCode();
	}

	public String getAuthorizationMessage(String code) {
		return Messages.getString("MGRWizardPage8.auth2") + " " + code;
	}

	public boolean isMissingCurrency() {
		return missingCurrency;
	}

	public String getThirdPartyType() {
		if (thirdPartyType == null) {
			thirdPartyType = DWValues.THIRD_PARTY_TYPE_DEFAULT;
		}
		return thirdPartyType;
	}

    public boolean isSendAmtNeeded() {
   		return this.getSendAmtFlag().equalsIgnoreCase("Y");
    }
    
    public boolean isSendFeeNeeded() {
   		return this.getSendFeeFlag().equalsIgnoreCase("Y");
    }
    
    public boolean isFxNeeded() {
   		return this.getFxRateFlag().equalsIgnoreCase("Y");
    }
    
	/**
	 * Returns the 3-letter ISO code for the agent country. This can be used as
	 * the default sender country.
	 */
	public String getDefaultCountry() {
		Country ci = CountryInfo.getAgentCountry();
		if ((ci == null) || (ci.getCountryCode().trim().equals(""))) {
			return "USA";
		} else {
			return ci.getCountryCode();
		}
	}
   
	/**
	 * return the FORM_FREE_ENABLED setting for the current transaction
	 */
	public boolean isStandardReceiveEnabled() {

		String enabled = "Y";

		ProfileItem p = getProductProfileOption("STANDARD_RECEIVE_ENABLED");
		if (p != null)
			enabled = getProductProfileOption("STANDARD_RECEIVE_ENABLED").stringValue();

		return enabled.equalsIgnoreCase("Y");
	}

	public boolean isFormFreeTransaction() {
		return formFreeTransaction;
	}

	public void setFormFreeTransaction(boolean flag) {
		formFreeTransaction = flag;
	}

	public boolean allowReceiveToCard() {
		return UnitProfile.getInstance().get(DWValues.RECV_OTHER_PAYOUT_TYPE, "NONE")
				.equalsIgnoreCase(DWValues.RECV_TO_CARD); 
	}

	public void setUnreceivableTransaction(boolean flag) {
		unreceivableTransaction = flag;
	}

	public boolean isUnreceivableTransaction() {
		return unreceivableTransaction;
	}

	public DataCollectionStatus getValidationStatus() {
		return dataCollectionData.getValidationStatus();
	}

	@Override
	public void setValidationStatus(DataCollectionStatus validationStatus) {
		dataCollectionData.setValidationStatus(validationStatus);
	}

	public void setTransactionStatus(TransactionStatusType transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public TransactionStatusType getTransactionStatus() {
		return this.transactionStatus;
	}

	/**
	 * @return the missingTransaction
	 */
	public boolean isMissingTransaction() {
		return missingTransaction;
	}

	/**
	 * @param missingTransaction
	 *            the missingTransaction to set
	 */
	public void setMissingTransaction(boolean missingTransaction) {
		this.missingTransaction = missingTransaction;
	}
	
	public boolean performValidation(String currentPage, MoneyGramReceiveTransaction receivetransaction) {
		
		// Check if more data needs to be collected from another page.
		
        if (currentPage.equals(MoneyGramReceiveWizard.MRW35_COMPLIANCE)) {
        	if (this.thirdPartyScreenStatus.hasDataToCollect() || this.supplementalInformationScreenStatus.hasDataToCollect()) {
        		return false;
        	}
        } else if (currentPage.equals(MoneyGramReceiveWizard.MRW45_THIRD_PARTY)) {
        	if (this.supplementalInformationScreenStatus.hasDataToCollect()) {
        		return false;
        	}
        }
        
        // Check if payment information needs to be collected.
        
		if ((this.getPayoutMethod() == PAYOUT_CASH) || (this.getDataCollectionData().isAfterValidationAttempt())) {
			return true;
		} else {
			return false;
		}
	}

	public void setupMoneyOrders() {
		
		// if ("DG".equals(getProfileMoneyGramChecks())){
		// add the money orders to the MO transaction if they have an
		// amount
		if (getAgentAmount().compareTo(ZERO) > 0) {
			mo1 = getMoneyOrderInfo(0);
			if ("DG".equals(getProfileMoneyGramChecks())) {
				long serialNumber = 0;
				try {
					serialNumber = Long.parseLong(mot.getNextSerialNumber());
				} catch (Exception e) {
				}
				mo1.setSerialNumber(BigInteger.valueOf(serialNumber));
				setAgentCheckNumber(getSerialNumberWithCheckDigit(mo1.getSerialNumber()));
				mot.setReferenceNumber(getReferenceNumber());
				mo1.setTaxID(getReferenceNumber());
			} else {
				BigInteger temp = getAgentCheckNumber();
				temp = temp.divide(BigInteger.valueOf(10));
				mo1.setSerialNumber(temp);
			}
			mo1.setDocumentType(BigInteger.valueOf(ReportLog.MONEYGRAM_RECEIVE_TYPE));
			mo1.setEmployeeID(BigInteger.valueOf(userID));
			mo1.setItemAmount(getAgentAmount());
			mo1.setItemFee(ZERO);
			mo1.setTaxID(getReferenceNumber());
			modf1 = mot.getMoneyOrderDisplayFields(0);
			String temp = getProductProfileOption("AGENT_RECEIVER_NAME").stringValue(); 
			if (temp.trim().equalsIgnoreCase("A")) {
				modf1.setPayeeName(UnitProfile.getInstance().get("GENERIC_DOCUMENT_STORE", adjustPayee(getReceiver().getFullName())));
			} else {
				modf1.setPayeeName(adjustPayee(getReceiver().getFullName()));
			}
						
			if (amount1Cash) {
				modf1.setDeltagramFlag("AG"); 
			} else {
				modf1.setDeltagramFlag("CU"); 
			}
			if ("DG".equals(getProfileMoneyGramChecks())) {
				mot.addNewDeltaGramToMO(0, mo1);
				mot.addNewDeltaGramToMODF(0, modf1);
				// status1 = mot.setMoneyOrder(0, (new
				// BigDecimal(mo1.getItemAmount())),
				// mo1.getItemFee().trim().length()!=0,
				// adjustPayee(getReceiver().getFullName()));
			}
		}

		if (getCustomerAmount().compareTo(ZERO) > 0) {
			mo2 = getMoneyOrderInfo(1);
			// Long snLong = Long.valueOf( mot.getNextSerialNumber());
			// long snlong = (snLong.longValue())+1;
			if ("DG".equals(getProfileMoneyGramChecks())) {
				Long snLong = Long.valueOf(mot.getNextSerialNumber());
				long snlong = (snLong.longValue()) + 1;
				mo2.setSerialNumber(BigInteger.valueOf(snlong));
				setCustomerCheckNumber(getSerialNumberWithCheckDigit(mo2.getSerialNumber()));
				mot.setReferenceNumber(getReferenceNumber());
				mo2.setTaxID(getReferenceNumber());
			} else {
				BigInteger temp = getCustomerCheckNumber();
				temp = temp.divide(BigInteger.valueOf(10L));
				mo2.setSerialNumber(temp);
			}
			mo2.setDocumentType(BigInteger.valueOf(ReportLog.MONEYGRAM_RECEIVE_TYPE));
			mo2.setEmployeeID(BigInteger.valueOf(userID));
			mo2.setTaxID(getReferenceNumber());
			mo2.setItemAmount(getCustomerAmount());
			mo2.setItemFee(ZERO);
			modf2 = mot.getMoneyOrderDisplayFields(1);
			modf2.setPayeeName(adjustPayee(getReceiver().getFullName()));
			modf2.setDeltagramFlag("CU"); 
			if ("DG".equals(getProfileMoneyGramChecks())) {
				mot.addNewDeltaGramToMO(1, mo2);
				mot.addNewDeltaGramToMODF(1, modf2);
				// status2 = mot.setMoneyOrder(1,(new
				// BigDecimal(mo2.getItemAmount())),
				// mo2.getItemFee().trim().length()!=0,
				// adjustPayee(getReceiver().getFullName()));
			}
		} else {
			setCustomerCheckNumber(null); 
		}
	}

	public String previousDataCollectionScreen(String currentPage) {
		
		String nextPage = "";

		// Determine which screens can be shown based upon the current screen.
		
		boolean showReceiverInformationScreen = false;
		boolean showReceiverThirdPartyScreen = false;
		boolean showSupplementalInformationScreen = false;
		
		if ((currentPage.equals(MoneyGramReceiveWizard.MRW57_PAYOUT_CHECK)) || 
				(currentPage.equals(MoneyGramReceiveWizard.MRW55_PAYOUT)) || 
				(currentPage.equals(MoneyGramReceiveWizard.MRW59_ADDITIONAL_RECEIVER_INFORMATION)) ||
				(currentPage.equals(MoneyGramReceiveWizard.MRW60_REVIEW))) {
			showReceiverInformationScreen = this.receiverScreenStatus.hasDataToCollect();
			showReceiverThirdPartyScreen = this.thirdPartyScreenStatus.hasDataToCollect();
			showSupplementalInformationScreen = this.supplementalInformationScreenStatus.hasDataToCollect();

		} else if (currentPage.equals(MoneyGramReceiveWizard.MRW50_SUPPLEMENTAL)) {
			showReceiverInformationScreen = this.receiverScreenStatus.hasDataToCollect();
			showReceiverThirdPartyScreen = this.thirdPartyScreenStatus.hasDataToCollect();

		} else if (currentPage.equals(MoneyGramReceiveWizard.MRW45_THIRD_PARTY)) {
			showReceiverInformationScreen = this.receiverScreenStatus.hasDataToCollect();
		} 
		
		// Check if more data needs to be collection and which screen needs to be displayed.
		
		if (showSupplementalInformationScreen) {
			nextPage = MoneyGramReceiveWizard.MRW50_SUPPLEMENTAL;
		
		} else if (showReceiverThirdPartyScreen) {
			nextPage = MoneyGramReceiveWizard.MRW45_THIRD_PARTY;
		
		} else if (showReceiverInformationScreen) {
			nextPage = MoneyGramReceiveWizard.MRW35_COMPLIANCE; 
		}

		return nextPage;
	}

	@Override
	public String nextDataCollectionScreen(String currentPage) {
		
		String nextPage = "";

		// Determine which screens can be shown based upon the current screen.
		
		boolean showReceiverInformationScreen = false;
		boolean showReceiverThirdPartyScreen = false;
		boolean showSupplementalInformationScreen = false;
		
		if (currentPage.equals(MoneyGramReceiveWizard.MRW20_RECEIVE_DETAIL) || currentPage.equals(MoneyGramReceiveWizard.MRW25_QUESTION) || currentPage.equals(MoneyGramReceiveWizard.MRW30_ANSWER)) {
			showReceiverInformationScreen = this.receiverScreenStatus.hasDataToCollect();
			showReceiverThirdPartyScreen = this.thirdPartyScreenStatus.hasDataToCollect();
			showSupplementalInformationScreen = this.supplementalInformationScreenStatus.hasDataToCollect();

		} else if (currentPage.equals(MoneyGramReceiveWizard.MRW35_COMPLIANCE)) {
			showReceiverThirdPartyScreen = this.thirdPartyScreenStatus.hasDataToCollect();
			showSupplementalInformationScreen = this.supplementalInformationScreenStatus.hasDataToCollect();

		} else if (currentPage.equals(MoneyGramReceiveWizard.MRW45_THIRD_PARTY)) {
			showSupplementalInformationScreen = this.supplementalInformationScreenStatus.hasDataToCollect();
		}
		
		// If history data is been used to populate the data collection items and 
		// the user has not clicked a back button during the data collection process,
		// eliminate screens that shown not be shown if all the required data for 
		// the screen can be found in the history data.
		
		if ((this.isFormFreeTransaction()) || (! this.isDataCollecionReview())) {
			showReceiverInformationScreen = showReceiverInformationScreen && receiverScreenStatus.requiredDataNotFullfilled();
			showReceiverThirdPartyScreen = showReceiverThirdPartyScreen && thirdPartyScreenStatus.requiredDataNotFullfilled();
			showSupplementalInformationScreen = showSupplementalInformationScreen && supplementalInformationScreenStatus.requiredDataNotFullfilled();
		}

		// Check if more data needs to be collection and which screen needs to be displayed.
		
		if (showReceiverInformationScreen) {
			nextPage = MoneyGramReceiveWizard.MRW35_COMPLIANCE; 
		
		} else if (showReceiverThirdPartyScreen) {
			nextPage = MoneyGramReceiveWizard.MRW45_THIRD_PARTY;
		
		} else if (showSupplementalInformationScreen) {
			nextPage = MoneyGramReceiveWizard.MRW50_SUPPLEMENTAL;		
		}

		return nextPage;
	}

	public boolean isRefundAllowed() {
		return refundAllowed;
	}

	public boolean isCancelAllowed() {
		return cancelAllowed;
	}

	/*
	 * Future Use
	 */
	public boolean isDiscPrtPrimaryAvailable() {
		return (prtReceiptDisc1 != null);
	}

	/*
	 * Future Use
	 */
	public boolean isDiscPrtSecondaryAvailable() {
		return (prtReceiptDisc2 != null);
	}

	public boolean isAgentPrtPrimaryAvailable() {
		return (prtReceiptAgent1 != null);
	}

	public boolean isCustPrtPrimaryAvailable() {
		return (prtReceiptCust1 != null);
	}

	public boolean isCustPrtSecondaryAvailable() {
		return (prtReceiptCust2 != null);
	}

	/*
	 * Future Use
	 */
	public boolean isDiscPdfPrimaryAvailable() {
		return (pdfReceiptDisc1 != null);
	}

	/*
	 * Future Use
	 */
	public boolean isDiscPdfSecondaryAvailable() {
		return (pdfReceiptDisc2 != null);
	}

	public boolean isAgentPdfPrimaryAvailable() {
		return (pdfReceiptAgent1 != null);
	}

	public boolean isAgentPdfSecondaryAvailable() {
		return false;
	}

	public boolean isCustPdfPrimaryAvailable() {
		return (pdfReceiptCust1 != null);
	}

	public boolean isCustPdfSecondaryAvailable() {
		return (pdfReceiptCust2 != null);
	}

	/**
	 * @return true if there was a failure printing receipts
	 */
	public void resetPrintFailure() {
		bPrintFailure = false;
	}

	/**
	 * @return true if there was a failure printing receipts
	 */
	public boolean wasPrintFailure() {
		return bPrintFailure;
	}

	private boolean cancelPrint(boolean pm_bReprint) {
		String sMessage, sMessage2, sMessage3, sMessage4;

		if (pm_bReprint) {
			sMessage = Messages.getString("DialogReceiptReprintFailurePost.Text1");
			sMessage2 = Messages.getString("DialogReceiptReprintFailurePost.Text2");
			sMessage3 = Messages.getString("DialogReceiptReprintFailurePost.Text3");
			sMessage4 = Messages.getString("DialogReceiptReprintFailurePost.Text4");
		} else {
			sMessage = Messages.getString("DialogReceiptPrintFailurePost.Text1");
			sMessage2 = Messages.getString("DialogReceiptPrintFailurePost.Text2");
			sMessage3 = Messages.getString("DialogReceiptPrintFailurePost.Text3");
			sMessage4 = Messages.getString("DialogReceiptPrintFailurePost.Text4");
		}

		boolean bRetValue = Dialogs.showPrintCancel("dialogs/BlankDialog3.xml",
				sMessage, sMessage2, sMessage3, sMessage4) != Dialogs.OK_OPTION;
		return bRetValue;
	}
	
	public SendReversalType getSendReversalType() {
		if (this.isCancelAllowed()) {
			return SendReversalType.C;
		} else if (this.isRefundAllowed()) {
			return SendReversalType.R;
		} else {
			return null;
		}
	}


	@Override
	public void validation(FlowPage callingPage) {
		this.receiveValidation(callingPage, ValidationType.SECONDARY);
	}

	@Override
	public String getDestinationCountry() {
		return null;
	}

	@Override
	public void applyHistoryData(CustomerInfo customerInfo) {
	}

	@Override
	public CustomerInfo getCustomerInfo() {
		return null;
	}

	@Override
	public void applyTransactionLookupData() {
		for (DataCollectionField field : this.getDataCollectionData().getFieldMap().values()) {
			String infoKey = field.getInfoKey();
			String value = this.getSearchProfilesData().getCurrentValue(infoKey);
			if (value != null) {
				field.setDefaultValue(value);
			}
		}
	}

	public DataCollectionScreenStatus getReceiverScreenStatus() {
		return receiverScreenStatus;
	}
	
    public DataCollectionScreenStatus getThirdPartyScreenStatus() {
    	return thirdPartyScreenStatus;
    }
    
    public String getSenderPurposeOfTransaction() {
    	String key = FieldKey.SEND_PURPOSEOFTRANSACTION_KEY.getInfoKey();
    	String purpose = this.getDataCollectionData().getCurrentValue(key);
    	if (purpose != null) {
    		if (purpose.equals("OTHER")) {
    			return this.getDataCollectionData().getCurrentValue(FieldKey.SEND_PURPOSEOFTRANSACTIONOTHER_KEY.getInfoKey());
    		} else {
	    		FieldInfo field = this.getTransactionLookupInfos().get(key);
	    		if ((field != null) && (field.getEnumeration() != null) && (field.getEnumeration().getEnumeratedItems() != null)) {
	    			List<EnumeratedIdentifierInfo> items = field.getEnumeration().getEnumeratedItems().getEnumeratedItem();
	    			if (items != null) {
	    				for (EnumeratedIdentifierInfo item : items) {
	    					if (item.getIdentifier().equals(purpose)) {
	    						return item.getLabel();
	    					}
	    				}
	    			}
	    		}
    		}
    	}
    	return null;
    }

    /**
     * Print the money orders.
     */
    static Thread commitThread = null;
    static Thread dialogThread = null;

    public synchronized void print(FlowPage page, boolean cancel) {
    	
        final MOPrintingDialog dialog = new MOPrintingDialog(false);

        if ((commitThread != null) && (commitThread.isAlive())) {
            commitThread.interrupt();
        }
        if ((dialogThread != null) && (dialogThread.isAlive())) {
            dialogThread.interrupt();
        }

        commitThread = commitPrint(dialog, page, cancel);

        if ("DG".equals(getProfileMoneyGramChecks()) && !cancel) {
        	dialogThread = moDialogShow(dialog);
        }
    }

	@Override
	public void applyProfileData(ConsumerProfile profile) {
		for (DataCollectionField field : this.getDataCollectionData().getFieldMap().values()) {
			String value = profile.getProfileValue(field.getInfoKey());
			if ((value != null) && (! value.isEmpty())) {
				field.setDefaultValue(value);
			}
		}
	}

	@Override
	public boolean createOrUpdateConsumerProfile(ConsumerProfile profile, ProfileStatus status) {
		setProfileStatus(status);
		if (profile == null) {
			setProfileObject(new ConsumerProfile("UCPID"));
		}
		CreateOrUpdateProfileReceiverResponse response = CachedACResponses.getCreateOrUpdateProfileReceiverResponse();
		if (response != null) {
			List<BusinessError> errors = response.getErrors() != null ? response.getErrors().getError() : null;
			List<InfoBase> fieldsToCollectTagList = null;
			CreateOrUpdateProfileReceiverResponse.Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
			if (payload != null) {
				fieldsToCollectTagList = payload.getFieldsToCollect() != null ? payload.getFieldsToCollect().getFieldToCollect() : null;
			}
				
			this.setDataCollectionData(new DataCollectionSet(DataCollectionType.PROFILE_EDIT));
			processValidationResponse(ValidationType.INITIAL_NON_FORM_FREE, fieldsToCollectTagList, errors, false, null);
			return true;
		} else {
			Debug.println("Cached createOrUpdateProfileReceiver response invalid. Must transmit to refresh cache.");
			CachedACResponses.deleteObjectFile();
			DiagLog.getInstance().writeMustTransmit(MustTransmitDiagMsg.AWOL_LIMIT);
			UnitProfile.getInstance().setForceTransmit(true);
			Dialogs.showRequest("dialogs/DialogCannotCreateUpdateCustomerInformation.xml");
			return false;
		}
	}

	@Override
	public void createOrUpdateConsumerProfile(final ConsumerProfile profile, CommCompleteInterface cci) {
		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {
				boolean flag = getProfileStatus().equals(ProfileStatus.EDIT_PROFILE);
				CreateOrUpdateProfileReceiverResponse response = MessageFacade.getInstance().createOrUpdateProfileReceiver(profile, getDataCollectionData(), flag);
				
				if (response != null) {
					List<BusinessError> errors = response.getErrors() != null ? response.getErrors().getError() : null;
					List<InfoBase> fieldsToCollectTagList = null;
					List<ConsumerProfileIDInfo> profileList = null;
					CreateOrUpdateProfileReceiverResponse.Payload payload = response.getPayload() != null ? response.getPayload().getValue() : null;
					if (payload != null) {
						fieldsToCollectTagList = payload.getFieldsToCollect() != null ? payload.getFieldsToCollect().getFieldToCollect() : null;
						
						if (payload.getConsumerProfileIDs() != null) {
							profileList = payload.getConsumerProfileIDs().getConsumerProfileIDInfo();
							
							for (ConsumerProfileIDInfo info : profileList) {
								String id = info.getConsumerProfileID();
								String type = info.getConsumerProfileIDType();
								if (type.equals("TRANSIENT")) {
									profile.setProfileId(id);
									break;
								}
							}
						}  else {
							profileList = null;
						}
					}
						
					boolean isComplete = ((profileList != null) && (! profileList.isEmpty()));
					processValidationResponse(ValidationType.SECONDARY, fieldsToCollectTagList, errors, isComplete, null);

					if ((profileList != null) && (! profileList.isEmpty())) {
						setProfileStatus(ProfileStatus.PROFILE_COMPLETED);
					}
					
					for (DataCollectionField field : getDataCollectionData().getFieldMap().values()) {
						String value = field.getValue();
						if ((value == null) || (value.isEmpty())) {
							value = field.getDefaultValue();
						}
						if ((value != null) && (! value.isEmpty())) {
							profile.setValue(field.getInfoKey(), value);
						}
					}
				}
				
				return (response != null);
			}
		};
		MessageFacade.run(logic, cci, CREATE_OR_UPDATE_PROFILES_RECEIVER, Boolean.FALSE);
	}

	@Override
	public void getConsumerProfile(final ConsumerProfile profile, final String mgiSessionID, final int rule, final Map<String, String> currentValues, CommCompleteInterface cci) {
		final MessageLogic logic = new MessageLogic() {
			
			@Override
			public Object run() {
				Map<String, String> values = new HashMap<String, String>();
				if (getProfileObject() != null) {
					String value = getProfileObject().getSearchValues().get(FieldKey.CONSUMER_FIRSTNAME_KEY.getInfoKey());
					values.put(FieldKey.RECEIVER_FIRSTNAME_KEY.getInfoKey(), value);
					value = getProfileObject().getSearchValues().get(FieldKey.CONSUMER_MIDDLENAME_KEY.getInfoKey());
					values.put(FieldKey.RECEIVER_MIDDLENAME_KEY.getInfoKey(), value);
					value = getProfileObject().getSearchValues().get(FieldKey.CONSUMER_LASTNAME_KEY.getInfoKey());
					values.put(FieldKey.RECEIVER_LASTNAME_KEY.getInfoKey(), value);
					value = getProfileObject().getSearchValues().get(FieldKey.CONSUMER_LASTNAME2_KEY.getInfoKey());
					values.put(FieldKey.RECEIVER_LASTNAME2_KEY.getInfoKey(), value);
					value = getProfileObject().getSearchValues().get(FieldKey.CONSUMER_NAMESUFFIX_KEY.getInfoKey());
					values.put(FieldKey.RECEIVER_NAMESUFFIX_KEY.getInfoKey(), value);
					value = getProfileObject().getSearchValues().get(FieldKey.CONSUMER_NAMESUFFIXOTHER_KEY.getInfoKey());
					values.put(FieldKey.RECEIVER_NAMESUFFIXOTHER_KEY.getInfoKey(), value);
					value = getProfileObject().getSearchValues().get(FieldKey.CONSUMER_DOB_KEY.getInfoKey());
					values.put(FieldKey.RECEIVER_DOB_KEY.getInfoKey(), value);
				}
				
				getProfileReceiverResponse = MessageFacade.getInstance().getProfileReceiver(profile, mgiSessionID, values);
				if (getProfileReceiverResponse != null) {
					GetProfileReceiverResponse.Payload payload = getProfileReceiverResponse.getPayload() != null ? getProfileReceiverResponse.getPayload().getValue() : null; 
					if (payload != null) {
						if (! UnitProfile.getInstance().isTrainingMode() || (currentValues == null)) {
							profile.getInfoMap().clear();
							profile.addInfoValues(payload.getInfos() != null ? payload.getInfos().getInfo() : null);
							String mgiSessionId = payload.getMgiSessionID();
							profile.setMgiSessionId(mgiSessionId);
							profile.addValues(payload.getCurrentValues().getCurrentValue());
						} else {
							profile.setCurrentValuesMap(currentValues);
						}
						if (getProfileObject() != null) {
							getProfileObject().setModified(false);
						}
					}
				}
				return (getProfileReceiverResponse != null);
			}
		};
		MessageFacade.run(logic, cci, 0, Boolean.FALSE);
	}

	@Override
	public Response getConsumerProfileResponse() {
		return this.getProfileReceiverResponse;
	}

	@Override
	public void applyCurrentData() {
		for (DataCollectionField field : this.getDataCollectionData().getFieldMap().values()) {
			String key = field.getInfoKey();
			String value = this.getDataCollectionData().getCurrentValue(key);
			if ((value != null) && (! value.isEmpty())) {
				field.setDefaultValue(value);
			}
		}
	}
	
	public void logTransaction() {
		// write out the diag saying we had a successful send
		MoneyGramReceiveDiagInfo mgrdi = new MoneyGramReceiveDiagInfo(new Date(TimeUtility.currentTimeMillis()), userID, userName, 1, tranSuccessful);
		mgrdi.setAmount(getAmount());
		mgrdi.setReferenceNumber(getReferenceNumber());

		// write the diag (which goes to trace file) whether
		// successful or not
		DiagLog.getInstance().writeMGReceive(mgrdi);

		if (tranSuccessful) {
			// Adjust payee name for report log because we want
			// last name only on reports (SCR 43).
			modf1.setPayeeName(getReceiver().getLastName());
			// ReportLog.logMoneyGramReceive(mgrdi,mo1,modf1);
			ReportLog.logMoneyGramReceive(mgrdi, mo1, modf1.getPayeeName(), getCurrency());
			String temp = getProductProfileOption("AGENT_RECEIVER_NAME").stringValue();
			if (temp.trim().equalsIgnoreCase("A")) {
				modf1.setPayeeName(UnitProfile.getInstance().get("GENERIC_DOCUMENT_STORE", adjustPayee(getReceiver().getFullName())));
			} else {
				modf1.setPayeeName(adjustPayee(getReceiver().getFullName()));
			}

		} else {
			if (! UnitProfile.getInstance().isDemoMode() && ! UnitProfile.getInstance().isTrainingMode()) {
				mot.clear();
			}
		}
	}

	public CompleteSessionResponse getCompleteSessionResponse() {
		return this.receiveInfo;
	}
	
	@Override
	public boolean isTransmitRequired(boolean checkCachedResponses) {
		boolean flag = super.isTransmitRequired(checkCachedResponses);
		if ((! flag) && (checkCachedResponses)) {
			flag = CachedACResponses.isReceiveResponsesInvalid();
		}
		return flag;
	}
	
	
	/*
	 * Return the best ID Type (taking into ID Verification) from first the data collection set, and if nothing there
	 * then from the profile
	 */
	private String getBestIdTypeValue(idTypes idNbr) {
		FieldKey sIDChoice;
		FieldKey fkIDType;
		
		if (idNbr == idTypes.ID1 ) {
			sIDChoice = FieldKey.RECEIVER_PERSONALID1_CHOICE_TYPE_KEY;
			fkIDType = FieldKey.RECEIVER_PERSONALID1_TYPE_KEY;
		} else {
			sIDChoice = FieldKey.RECEIVER_PERSONALID2_CHOICE_TYPE_KEY;
			fkIDType = FieldKey.RECEIVER_PERSONALID2_TYPE_KEY;
		}
		
		return getBestIdTypeValue(sIDChoice, fkIDType);
	}
	
	/*
	 * Return the best ID Number (taking into ID Verification) from first the data collection set, and if nothing there
	 * then from the profile
	 */
	private String getBestIdNumber(idTypes idNbr) {
		FieldKey fkIDNumber;
		FieldKey fkIDChoice;
		FieldKey fkVerify;
		
		if (idNbr == idTypes.ID1 ) {
			fkIDChoice = FieldKey.RECEIVER_PERSONALID1_CHOICE_TYPE_KEY;
			fkIDNumber = FieldKey.RECEIVER_PERSONALID1_NUMBER_KEY;
			fkVerify = FieldKey.RECEIVER_PERSONALID1_CHOICE_VERIFY_KEY;
		} else {
			fkIDChoice = FieldKey.RECEIVER_PERSONALID2_CHOICE_TYPE_KEY;
			fkIDNumber = FieldKey.RECEIVER_PERSONALID2_NUMBER_KEY;
			fkVerify = FieldKey.RECEIVER_PERSONALID2_CHOICE_VERIFY_KEY;
		}
		return getBestIdNumber(fkIDChoice, fkIDNumber, fkVerify);
	}

	public BigDecimal getReceiveAmount1() {
		return receiveAmount1;
	}

	public void setReceiveAmount1(BigDecimal receiveAmount1) {
		this.receiveAmount1 = receiveAmount1;
	}

	public BigDecimal getReceiveAmount2() {
		return receiveAmount2;
	}

	public void setReceiveAmount2(BigDecimal receiveAmount2) {
		this.receiveAmount2 = receiveAmount2;
	}

	public BigDecimal getReceiveTotalAmount() {
		return receiveTotalAmount;
	}

	public void setReceiveTotalAmount(BigDecimal receiveTotalAmount) {
		this.receiveTotalAmount = receiveTotalAmount;
	}

	public String getReceiveCurrency() {
		return receiveCurrency;
	}

	public void setReceiveCurrency(String receiveCurrency) {
		this.receiveCurrency = receiveCurrency;
	}
}
