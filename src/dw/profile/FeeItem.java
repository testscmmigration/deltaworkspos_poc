package dw.profile;

import java.math.BigDecimal;

/**
 * Note: Call setID before calling setBreakpoint or setFee.
 */

public class FeeItem extends ProfileItem {
	private static final long serialVersionUID = 1L;
   
    BigDecimal breakpoint;
    BigDecimal fee;
    
    public FeeItem(String tag) {
	super(tag);
    }

    @Override
	void setBreakpoint(String bp) {
	if (bp == null) 
	    return;
	FeeTable table = (FeeTable) parent;
	breakpoint = new BigDecimal(bp);
	table.setBreakpoint(id, breakpoint);
    }

    @Override
	void setFee(String f) {
	if (f == null)
	    return;
	FeeTable table = (FeeTable) parent;
	fee = new BigDecimal(f);
	table.setFee(id, fee);
    }
    
    /**
        Obtain the breakpoint amount for this FeeItem object.
        @return java.math.BigDecimal object representing the breakpoint amount.
     */
    public BigDecimal getBreakpoint() {
        return breakpoint.setScale(2, BigDecimal.ROUND_HALF_EVEN);
    }
    
    /**
        Sets the breakpoint amount for this FeeItem object.  
        Also propagates breakpoint amount change to the 
        FeeTable parent object.
        @param newBreakpoint A java.math.BigDecimal object representing 
        the breakpoint amount.
     */
    public void setBreakpoint(BigDecimal newBreakpoint) {
        breakpoint = newBreakpoint;
    	FeeTable table = (FeeTable) parent;
    	table.setBreakpoint(id, breakpoint);
    	changed = true;
    	parent.setChanged();
    }
    
    /**
        Obtain the fee amount for this FeeItem object.
        @return java.math.BigDecimal object representing the fee amount.
     */
    public BigDecimal getFee() {
        return fee.setScale(2, BigDecimal.ROUND_HALF_EVEN);
    }

    /**
        Sets the fee amount for this FeeItem object.  Also propagates fee
        amount change to the FeeTable parent object.
        @param newFee A java.math.BigDecimal object representing 
        the fee amount.
     */
    public void setFee(BigDecimal newFee) {
        fee = newFee;
    	FeeTable table = (FeeTable) parent;
    	table.setFee(id, fee);
    	changed = true;
    	parent.setChanged();
    }

    /** Returns fee or breakpoint as a string using path syntax. */
    @Override
	public ProfileItem get(String path) {
        if ((path != null) && path.equalsIgnoreCase("fee"))	
            return new FeeAttribute();

        if ((path != null) && path.equalsIgnoreCase("breakpoint"))	
            return new BreakpointAttribute();

        return null;        
    }

    /** Limited-use class for making the breakpoint attribute look
     *  like a normal profile item. Note that the API for ProfileItem
     *  is only partially implemented, so it doesn't look entirely like
     *  a normal item. Avoid methods that aren't overridden here.
     */
    public class BreakpointAttribute extends ProfileItem {
    	private static final long serialVersionUID = 1L;

    	public BreakpointAttribute() {
            super("BREAKPOINT_ATTRIBUTE");	
        }

        @Override
		public void setValue(String s) {
            this.setValue(new BigDecimal(s));
        }

        @Override
		public void setValue(BigDecimal bd) {
            FeeItem.this.setBreakpoint(bd);
        }

        @Override
		public String stringValue() {
            return breakpoint.toString();
        }

        @Override
		public BigDecimal bigDecimalValue() {
            return breakpoint;
        }

        @Override
		public String getMode() {
            if (FeeItem.this.getMode() != null) {
                return FeeItem.this.getMode();
            }
            else {
                return FeeItem.this.getParent().getMode();
            }
        }
    }
    
    /** Limited-use class for making the breakpoint attribute look
     *  like a normal profile item. See the comments for 
     *  BreakpointAttribute.
     */
    public class FeeAttribute extends ProfileItem {
    	private static final long serialVersionUID = 1L;
        
    	public FeeAttribute() {
            super("FEE_ATTRIBUTE");	
        }
        @Override
		public void setValue(String s) {
            this.setValue(new BigDecimal(s));
        }

        @Override
		public void setValue(BigDecimal bd) {
            FeeItem.this.setFee(bd);
        }

        @Override
		public String stringValue() {
            return fee.toString();
        }

        @Override
		public BigDecimal bigDecimalValue() {
            return fee;
        }

        @Override
		public String getMode() {
            if (FeeItem.this.getMode() != null) {
                return FeeItem.this.getMode();
            }
            else {
                return FeeItem.this.getParent().getMode();
            }
        }
    }
    
    /** Returns a string containing an XML representation. */
    @Override
	public String toString() {
	StringBuffer result = new StringBuffer(60);
	result.append("<").append(element);	
	if (id != 0) result.append(" id=\"").append(id).append("\"");  
	if (status != null) result.append(" status=\"").append(status).append("\"");  
	if ((breakpoint != null) && (fee != null)) {
	    result.append(" breakpoint=\"").append(this.getBreakpoint()).append("\"");  
	    result.append(" fee=\"").append(this.getFee()).append("\"");  
	}
	result.append("/>\n"); 
	return result.toString();
    }
}    
