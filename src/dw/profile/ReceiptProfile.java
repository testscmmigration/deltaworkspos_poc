package dw.profile;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.NoSuchElementException;

/**
 * A profile is a collection of profile items. Profiles can be
 * nested, so a profile is itself a profile item.
 * @author Steve Steele
 */
public class ReceiptProfile extends Profile {
	private static final long serialVersionUID = 1L;

    public static final String RECEIPTRULEXML = "RECEIPT_RULE_XML";	

    /**
     * Construct an empty receipt profile.
     */
    public ReceiptProfile(String element) {
	super(element);
	children = new ArrayList<ProfileItem>(10);
    }


    /**
     * Reconstructs a ReceiptProfile and its sub-profile items.
     * @param pi Contains a RECEIPTRULEXML tag with XML content.
     * @return A newly created ReceiptProfile and its sub-profile items.
     */
    public static Profile parseEncodedReceiptRuleTags(ProfileItem pi) {
		try {
			if (!pi.getTag().equals(RECEIPTRULEXML)) {
				return null;
			}
			StringBuffer sb = new StringBuffer();
			sb.append("<?xml version=\"1.0\"?>\n"); 
			sb.append("<PROFILE changed=\"true\">\n"); 
			sb.append(pi.stringValue());
			sb.append("</PROFILE>"); 
            Profile returnVal = (Profile) new ProfileLoader().load(new ByteArrayInputStream(sb.toString().getBytes()), false).find("RECEIPT_RULE_MSG");	

			return returnVal;
		}
		catch (NoSuchElementException nsee) {
			return null;
		}
		catch (Exception e) {
			return null;
		}
    }

    /** 
     * Creates an XML item containing XML tags converted into XML-safe charactesr.
     * @return A single tag containing the 'nerfed' inner tags.
     */
    @Override
	public String toString() {
	StringBuffer result = new StringBuffer();
	
	result.append("<").append(RECEIPTRULEXML).append(" mode=\"P\"");  
	if (tag != null) result.append(" tag=\"").append(tag).append("\"");  
	if (id != 0) result.append(" id=\"").append(id).append("\">");  
	result.append(sanitize(super.toString()));
	result.append("</").append(RECEIPTRULEXML).append(">\n");	 

	return result.toString();
    }
}

