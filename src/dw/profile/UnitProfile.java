package dw.profile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.TimeZone;

import com.moneygram.agentconnect.SubagentInfo;

import dw.comm.AgentConnectAuthenticator;
import dw.framework.ClientTransaction;
import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.io.CircularFile;
import dw.io.State;
import dw.main.DeltaworksMainPanel;
import dw.main.MainPanelDelegate;
import dw.mgsend.MoneyGramSendTransaction;
import dw.utility.AgentCountryInfo;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.ErrorManager;
import dw.utility.ExtraDebug;
import dw.utility.Period;
import dw.utility.Scheduler;
import dw.utility.TimeUtility;
import dw.utility.Total;


/**
 * Provides single point of access to the Profile object representing
 * the unit profile. The unit profile is a singleton, so all fields and
 * methods in this class are static.
 */
public final class UnitProfile implements UnitProfileInterface {
    
	/**
     * Profile value change listeners map.  The profile item tag is used as
     * a key to retrieve the List collection of listeners to fire 
     * ProfileChangedEvents.
     */
    private Map<String, List<ProfileChangedListener>> profileListenersMap = new HashMap<String, List<ProfileChangedListener>>();
    
    private Profile profileInstance = null;
    private boolean profileChanged = false;
    private State stateSaver = null;
    private CircularFile file = null;
    private boolean forceUpgrade = false;

    // transient...gets set every start of the app
    private String oldVersionNumber; // what was the version at startup?

    // transient...default is SSL
    //private static String protocolPrefix = "https://"; 

    // transient
    private boolean softwareVersionPending = false;
    
    // transient
    private SubagentInfo info=null;   
    private boolean needReceiptTemplates = false;
    private boolean needStateRegInfo = false;
    private boolean needReceiptData = false;
    
    // moved from Period
    private byte period = 0;
    
    // for development use
    private boolean noproxy = false;
    
    // for new demo mode
    private boolean newDemoMode = false;

    /** 
     * Flag used in multi agent mode profile takeover logic.
     * 
     * Moved here from MultiAgentTransaction to avoid unwanted dependancy.
     */ 
    private boolean takeOverSubagentprofile=false;
    
    private int subAgentTakeOverUserID = 0; // user who took over the subAgent
    
    private static UnitProfile UP_INSTANCE = new UnitProfile();
    private static UnitProfileInterface MOCK_INSTANCE = null;
    public static void setMOCK(UnitProfileInterface mock) {
    	MOCK_INSTANCE = mock;
    }
    public static UnitProfileInterface getInstance() {
    	if (MOCK_INSTANCE != null)
    		return MOCK_INSTANCE;
    	
    	return UP_INSTANCE;
    }
    private UnitProfile() {}
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#disableProxy()
	 */
    @Override
	public void disableProxy() {
        noproxy = true;
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#load(dw.io.CircularFile)
	 */
    @Override
	public Profile load(CircularFile cf) throws IOException {
        file = cf;
        try {
            ProfileLoader loader = new ProfileLoader();
            profileInstance = loader.load(file.getInputStream(), false);

            // Retrieve encoded receipt rule tags
            try {
                while (true) {
                    ProfileItem pi =
                        profileInstance.find(ReceiptProfile.RECEIPTRULEXML);
                    Profile p = ReceiptProfile.parseEncodedReceiptRuleTags(pi);
                    profileInstance.add(p);
                    profileInstance.remove(pi);
                }
            } catch (NoSuchElementException nsee) {
                // no more encoded receipt rule tags to decode
            }
            
            profileInstance.sortChildren();
        } 
        catch (org.xml.sax.SAXException e) {
            throw new IOException(e.toString());
        }

        return profileInstance;
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#find(java.lang.String)
	 */
    @Override
	public ProfileItem find(String tag) throws NoSuchElementException {
        return profileInstance.find(tag);
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getCurrentPeriod()
	 */
    @Override
	public byte getCurrentPeriod() {
        try {
            if (period == 0) {
                period = stateSaver.readByte(State.CURRENT_PERIOD);
                period = Period.computePeriod(period);
                setCurrentPeriod(period); // in case it changed
            }
        }
        catch (Exception e) {
            period = 0;
        }
        return period;
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setNeedReceiptTemplates(boolean)
	 */
    @Override
	public void setNeedReceiptTemplates(boolean bValue) {
    	needReceiptTemplates = bValue;
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#needReceiptTemplates()
	 */
    @Override
	public boolean needReceiptTemplates() {
    	ClientTransaction cTran = DeltaworksMainPanel.getMainPanel().getCurrentTransaction();
    	if (AgentCountryInfo.isUsAgent() 
    			&& ((cTran == null) 
    					|| ((cTran instanceof MoneyGramSendTransaction)
    							&& !((MoneyGramSendTransaction) cTran).isBillPayment()))) {
            return needReceiptTemplates;
    	} else {
    		return false;
    	}
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setNeedStateRegInfo(boolean)
	 */
    @Override
	public void setNeedStateRegInfo(boolean bValue) {
    	needStateRegInfo = bValue;
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#needStateRegInfo()
	 */
    @Override
	public boolean needStateRegInfo() {
        return needStateRegInfo;
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setSoftwareVersionPending(boolean)
	 */
    @Override
	public void setSoftwareVersionPending(boolean pending) {
        softwareVersionPending = pending;
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#isSoftwareVersionPending()
	 */
    @Override
	public boolean isSoftwareVersionPending() {
        return softwareVersionPending;
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#isSoftwareVersionIdChanged()
	 */
    @Override
	public boolean isSoftwareVersionIdChanged() {
        ProfileItem versionItem = profileInstance.find("SOFTWARE_VERSION_ID");	
        return versionItem.isChanged();
    }
    

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getDispenserId()
	 */
    @Override
	public String getDispenserId() {
        Profile dispenserProfile = (Profile) profileInstance.find("DISPENSER", 1);	
        ProfileItem idItem = dispenserProfile.find("UNIT_NUMBER");	
        return idItem.stringValue();
    }
   

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setDispenserId(java.lang.String)
	 */
    @Override
	public void setDispenserId(String id) {
        Profile dispenserProfile = (Profile) profileInstance.find("DISPENSER", 1);	
        ProfileItem idItem = dispenserProfile.find("UNIT_NUMBER");	
        String dispenserId = idItem.stringValue();  //rcm.v209
        if (dispenserId.compareTo(id) != 0) //rcm.v209
            idItem.setValue(id);
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setServiceTag(java.lang.String)
	 */
    @Override
	public void setServiceTag(String tag) {
        set("DEVICE_SERVICE_TAG", tag);
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#isDemoMode()
	 */
     @Override
	public boolean isDemoMode() {
         return getAppMode() == DEMO_MODE;
     }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#isQaMode()
	 */
     @Override
	public boolean isQaMode() {
         return false;
     }

     /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setVersionNumber(java.lang.String)
	 */
     @Override
	public void setVersionNumber(String version) {
         Debug.println("UnitProfile.setVersionNumber(" + version + ")");
         oldVersionNumber = version;
         ProfileItem versionItem = profileInstance.find("SOFTWARE_VERSION_ID");	
         String value = versionItem.stringValue();
         if (value.compareTo(version) != 0)
            versionItem.setValue(version);
     }

     /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#compareVersions()
	 */
    @Override
	public int compareVersions() {
        return compareVersions(getVersionNumber(false));
     }

     /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#compareVersions(java.lang.String)
	 */
     @Override
	public int compareVersions(String currentVersion) {
         try {
             int oldVersion = Integer.parseInt(oldVersionNumber);
             int newVersion = Integer.parseInt(currentVersion);
             if (newVersion == oldVersion)
                 return 0;
             else if (newVersion > oldVersion)
                 return 1;
             else
                 return -1;
         }
         catch (NumberFormatException e) {
             Debug.println("Caught " + e);	
             return 0;
         }
     }

     /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getVersionNumber(boolean)
	 */
     @Override
	public  String getVersionNumber(boolean withDecimal) {
         String versionString = get("SOFTWARE_VERSION_ID", "0");
         String stripped = "" + Integer.parseInt(versionString);
         String result;
         if (! withDecimal) 
             result = stripped;
         else
             result = formatVersionNumber(stripped);
         
         //Debug.println("UnitProfile.getVersionNumber(" + withDecimal + ") result = " + result);
         return result;
     }

     
     /** Turns version number into user-readable string.
       * Examples:  
       *        "251" becomes "2.51"	 
       *        "100" becomes "1.0"	 
       *        "250" becomes "2.50"	 
       *        "2501" becomes "2.5 (build 1)"	
       *        "2545" becomes "2.5 (build 45)"	
       */
     private  String formatVersionNumber(String stripped){
         
         if (stripped.length() <= 3) {
             return stripped.substring(0, stripped.length() - 2) +
                 "." + stripped.substring(stripped.length() -2);	
         }
         else {
             return getNewFormatVersionNumber(stripped);
         }
     }            
     
     
    /**
     * @return version string in new format MMNBB where 
     * the 1st 2 digits (MM) is for the major release.  
     * The 3rd digit (N) is for each minor release.  
     * The 4th & 5th digits (BB) is for the build number within a release.  
     * Example, in new format version 02501 referred to as version 2.5 (Build 1)
     * @param version - version string to convert into new format.
     */
     private  String getNewFormatVersionNumber(String version){        
         //version has minimum 4 digits in new format.
         if (version.length() <=3)
            return version; //this should never happen;
         
         String versionString = version;        
         String majorRelease;
         String minorRelease;
         String buildNumber;
         
         if (versionString.length() == 4)
             versionString = "0"+versionString;	
           
         buildNumber = versionString.substring(4);
         minorRelease = versionString.substring(2,4);
         majorRelease = versionString.substring(0,2);
         
         if (majorRelease.charAt(0) == '0')
            majorRelease = majorRelease.substring(1);
         
         if (buildNumber.charAt(0) == '0')
            buildNumber=buildNumber.substring(1);
         
         versionString = majorRelease + "." + minorRelease + Messages.getString("UnitProfile.2032") + buildNumber + ")";   
         return versionString;
     }

     /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#isDialUpMode()
	 */
     @Override
	public  boolean isDialUpMode() {
         return get("POS_HOST_CONNECTION", "N").equalsIgnoreCase("D");
     }
    
     @Override
	public boolean isSuperAgent() {
         if (profileInstance == null) 
             return false;
         else
             return get("SUPERAGENT", "N").equals("Y");
     }

    /* (non-Javadoc)
     * @see dw.profile.UnitProfileInterface#isSubAgent()
     */
    @Override
	public  boolean isSubAgent() {
         if (profileInstance == null) 
             return true;
         else
             return get("SUBAGENT_MODE", "N").equals("Y");
    }

     /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#isMultiAgentMode()
	 */
     @Override
	public  boolean isMultiAgentMode() {
         if (get("SUPERAGENT", "N").equals("Y") ||
             get("SUBAGENT_MODE","N").equals("Y"))
             return true;
         else
             return false;    
     }

     /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#isTrainingMode()
	 */
     @Override
	public  boolean isTrainingMode() {
         return get("TRAINING_MODE", "N").equals("Y");
     }

     /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setTrainingMode(boolean)
	 */
     @Override
	public  void setTrainingMode(boolean mode) {
         set("TRAINING_MODE", mode ? "Y" : "N");
     }

     /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setDialUpMode(boolean, boolean)
	 */
    @Override
	public  void setDialUpMode(boolean isDialUp, boolean recordAsChanged) {
         ProfileItem connMode;
         String dialMode;
         
         try {
             connMode = profileInstance.find("POS_HOST_CONNECTION");	
             dialMode = profileInstance.find("POS_HOST_CONNECTION").stringValue();	
         }
         catch (NoSuchElementException e) {
             Debug.println("No POS_HOST_CONNECTION in profile..." +	
                     "Creating profile item."); 
             connMode = ProfileItem.create("ITEM");	
             connMode.setTag("POS_HOST_CONNECTION");	
             connMode.setMode("S");	
             dialMode = "N";	
             profileInstance.add(connMode);
         }
         if ((isDialUp && (dialMode.compareTo("N") == 0)) ||	
             (!isDialUp && (dialMode.compareTo("D") == 0))) 	
            connMode.setValue(isDialUp ? "D" : "N");	 
            
         if (! recordAsChanged)
            connMode.setChanged(false);
     }

     /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setDialUpMode(boolean)
	 */
     @Override
	public  void setDialUpMode(boolean isDialUp) {
         set("POS_HOST_CONNECTION", isDialUp ? "D" : "N", "S", true);
     }
     
     /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setModemSpeakerMuted(boolean)
	 */
     @Override
	public  void setModemSpeakerMuted(boolean isMuted) {
         set("MODEM_SPEAKER_MUTED", isMuted ? "Y" : "N", "P",  true);
     }
     
     /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#isModemSpeakerMuted()
	 */
     @Override
	public  boolean isModemSpeakerMuted() {
         return get("MODEM_SPEAKER_MUTED", "N").equals("Y");
     }

     /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getAppMode()
	 */
     @Override
	public  int getAppMode() {
         if (get("APPLICATION_MODE", "N").equals("D")) {
             return DEMO_MODE;
         }
         else {
             return NORMAL_MODE;
         }
     }

     /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setAppMode(int)
	 */
     @Override
	public  void setAppMode(int mode) {
         set("APPLICATION_MODE", (mode == NORMAL_MODE ? "N" : "D"), "P", false);
     }
     /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#isPosAuthorized()
	 */
     @Override
	public  boolean isPosAuthorized() {
         if (profileInstance == null) 
             return true;
         else
             return get("POS_STATUS", "A").equals("A");
     }
     @Override
	public  boolean isPosDeactivated() {
        if (profileInstance == null) 
            return true;
        else
            return get("POS_STATUS", "X").equals("X");
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getInstance()
	 */
    @Override
	public  Profile getProfileInstance() {
        return profileInstance;
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setProfileChanged()
	 */
    @Override
	public  void setProfileChanged() {
        profileChanged = true;
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#isProfileChanged()
	 */
    @Override
	public  boolean isProfileChanged() {
        return profileChanged;
    }

    @Override
	public  void setNewDemoMode(boolean b) {
        newDemoMode = b;
    }

    @Override
	public  boolean isNewDemoMode() {
        return newDemoMode;
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setStateSaver(dw.io.State)
	 */
    @Override
	public  void setStateSaver(State ss) {
        stateSaver = ss;
        getCurrentPeriod(); // to make sure period is initialized
    }


    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#get(java.lang.String, java.lang.String)
	 */
    @Override
	public  String get(String path, String defaultValue) {
        if (profileInstance == null)
            return defaultValue;

        ProfileItem result = profileInstance.get(path);
        if (result == null) 
            return defaultValue;
        else
            return result.stringValue();
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#get(java.lang.String, java.math.BigDecimal)
	 */
    @Override
	public  BigDecimal get(String path, BigDecimal defaultValue) {
        if (profileInstance == null)
            return defaultValue;

        ProfileItem result = profileInstance.get(path);
        if (result == null)
            return defaultValue;
        else
            return result.bigDecimalValue();
    }
  
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#get(java.lang.String, int)
	 */
    @Override
	public  int get(String path, int defaultValue) {
        if (profileInstance == null)
            return defaultValue;

        ProfileItem result = profileInstance.get(path);
        if (result == null)
            return defaultValue;

        try {
            int value = Integer.parseInt(result.stringValue());
            return value;
        }
        catch (NumberFormatException e) {
            return defaultValue;
        }
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#set(java.lang.String, java.lang.String)
	 */
    @Override
	public  void set(String path, String value) {
        set(path, value, "V", true);
    }
    

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#set(java.lang.String, java.lang.String, java.lang.String, boolean)
	 */
    @Override
	public  void set(String path, String value, String mode, 
        boolean saveChanges) {
            
        boolean changed = false;
    
        if (profileInstance == null) 
            return;
    
        if (path.indexOf('/') != -1)                    
            throw new IllegalArgumentException(path);
    
        ProfileItem item = profileInstance.get(path);
        if (item == null) {
            changed = true;
            item = ProfileItem.create("ITEM");          
            item.setTag(path);
            item.setMode(mode);
            profileInstance.add(item);
        }
    
        if (! value.contentEquals(item.value)) {
            changed = true;
            item.setValue(value);
            // setValue calls setChanged, so we don't have to
        }
    
        if (changed && saveChanges) {
            try {
                saveChanges();
            }
            catch (IOException e) {
    			Debug.printStackTrace(e);
            }
        }
    }
      
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getGenericDocumentStoreName()
	 */
    @Override
	public  String getGenericDocumentStoreName() {
        return get("GENERIC_DOCUMENT_STORE", "");
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getUserID(java.lang.String)
	 */
    @Override
	public  int getUserID(final String userName) {
        if (userName == null)
            return 0;
        final Profile profile = profileInstance;
        final Iterator<ProfileItem> i = profile.children.iterator();
        while (i.hasNext()) {
            final Object item = i.next();
            if (!(item instanceof Profile))
                continue;
            final Profile child = (Profile)item;
            if (!child.element.equals("USER"))	
                continue;
            final ProfileItem nameItem = child.find("NAME");	
            if (nameItem == null)
                continue;
            final String childName = nameItem.stringValue();
            if (!userName.equalsIgnoreCase(childName))
                continue;
            final ProfileItem idItem = child.find("USER_ID");	
            if (idItem == null)
                return 0;
            return idItem.intValue();
        }
        return 0;
    }
            
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setInitialProfile(java.lang.String)
	 */    
    @Override
	public  void setInitialProfile(String profileString) {
        setNewProfile(profileString, false);
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getProfileModeP()
	 */
    @Override
	public  Profile getProfileModeP(){
        Profile profileClone = null;
        try {
            profileClone = (Profile) profileInstance.clone();
        }
        catch (CloneNotSupportedException e) {
			Debug.printStackTrace(e);
        }

        Iterator<ProfileItem> iter = profileClone.iterator();
        while(iter.hasNext()){
            ProfileItem profileItem = iter.next();
            if(profileItem.getMode() != null && !profileItem.getMode().equals("P")){
                iter.remove();
            }
            else{
                if(profileItem.getMode() == null){
                    Profile profile = (Profile) profileItem;
                    
                    Iterator<ProfileItem> profileIter = profile.iterator();
                    while (profileIter.hasNext()) {
                        ProfileItem pitem = profileIter.next();
                        if (pitem.getMode() != null && !pitem.getMode().equals("P")){
                            profileIter.remove();
                        }
                    }
                    if(profile.getNumberOfChildren() == 0)
                        iter.remove();
                }
            }
        }
        return profileClone;
    }


    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setNewProfile(java.lang.String)
	 */
    @Override
	public  void setNewProfile(String profileString) {
        setNewProfile(profileString, true);
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setNewProfile(java.lang.String, boolean)
	 */
    @Override
	public  void setNewProfile(String profileString, boolean saveChanges) {
        Profile superagentProfileItems = null;
        Profile superagentEmployees = null;
        boolean subAgentMode = false;
        try {
            // Check the value of training mode flag before and after the load so we
            //   can inform the POS that this has been changed by the host.

            boolean trainingCurrent =
                       profileInstance.find("TRAINING_MODE").booleanValue();	
            
            // retain superagent's connectivity settings if we are in multiagent mode
            //before loading new subagent profile. if doing a transmit while in subagent mode, then retain
            //all of subagent's connectivity values. this loop will be executed even if we are trying
            // to take over the superAgent profile as that is not known at this point.  
            if(isSubAgent())
                subAgentMode = true;
            if(this.takeOverSubagentprofile || subAgentMode){
            	 //getSuperAgentEmployees();
                superagentProfileItems = getSuperAgentProfileItems();
                superagentEmployees = getSuperAgentEmployees();
               
                
               // superagentProfileItems.add( getSuperAgentEmployees().get("PROFILE"));
               // System.err.println( superagentProfileItems);
            }
                        
            // load the new profile
            ByteArrayInputStream input =
                new ByteArrayInputStream(profileString.getBytes());
            ProfileLoader loader = new ProfileLoader();
            Profile newProfile = loader.load(input, false);

            // Get the mode P subset of the current profile and merge it
            // into the profile that was just downloaded.

            newProfile.merge(getProfileModeP());
            
            // has training mode been changed? If so, set the mode in the POS
            if(!isMultiAgentMode()){
                boolean trainingNew = newProfile.find("TRAINING_MODE").booleanValue();	
                if (trainingNew != trainingCurrent)
                      ErrorManager.setTrainingMode(trainingNew);
            }         
            profileInstance = newProfile;
            
            //reset subAgent flag to false if the profile we have taken over is a super agent.
            if(get("SUPERAGENT", "N").equals("Y"))
                subAgentMode = false;

			// sort profile items to fix P.E. bug where 
			// out of order/duplicate receipt rules
			// are sent to the POS
			profileInstance.sortChildren();

            profileChanged = false;
            if(this.takeOverSubagentprofile ||subAgentMode){
            	removeSubAgentEmployees();
            	 Iterator<ProfileItem> iter = superagentEmployees.iterator();
            	while(iter.hasNext()){
                    ProfileItem profileItem = iter.next();
                    if((profileItem.getTag().equals("USER")))
                    		{
                    	    ProfileItem t = profileItem.get("USER_ID") ;
                    	    newProfile.add(t.getParent());
                    	 
                    		}
                  
             }
                newProfile.mergeSuperagentProfileItems(superagentProfileItems);
                updateSubAgentProfileItems();
            }
                
            if (saveChanges) {
                changesApplied();
                saveChanges();
            }
        }
        catch (Exception e) {        //lots of them
            Debug.printStackTrace(e);
        }
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#changesApplied()
	 */
    @Override
	public  void changesApplied() {
        int newPeriod;
        try {
            TimeZone.setDefault(Scheduler.createLocalTimeZone());
            Debug.println("default time zone is now " 	
                    + TimeZone.getDefault().getDisplayName());
        }
        catch (Exception e) {
        	Debug.println("Exception thrown setting the TimeZone");
        }
        newPeriod = Period.computePeriod(period);
        if (newPeriod != period) {
            Debug.println("period recomputed, changed from " + period	
                   + " to " + newPeriod);  
            Period.openPeriod(newPeriod, true, 0);
        }
        
        Messages.setCurrentLocale(get("LANGUAGE", "en"));
        updateProxyProperties();
        FormatSymbols.initialize();
        MainPanelDelegate.setTransactionEnabled();
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getStartingSerialNumber()
	 */
    @Override
	public  long getStartingSerialNumber() {
        long serialNumber = 0;
        serialNumber = stateSaver.readLong(State.DISPENSER_1);

        return serialNumber;
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setStartingSerialNumber(long)
	 */
    @Override
	public  void setStartingSerialNumber(final long startingSerialNumber) {
        try {
            stateSaver.writeLong(startingSerialNumber, State.DISPENSER_1);
        }
        catch (IOException e) {
            Debug.println("Failed to save starting serial number");
            Debug.printStackTrace(e);
        }
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getEndingSerialNumber()
	 */
    @Override
	public  long getEndingSerialNumber() {
        long serialNumber = 0;
        serialNumber = stateSaver.readLong(State.DISPENSER_1 + 8);

        return serialNumber;
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setEndingSerialNumber(long)
	 */
    @Override
	public  void setEndingSerialNumber(final long endingSerialNumber) {
        try {
            stateSaver.writeLong(endingSerialNumber, State.DISPENSER_1 + 8);
        }
        catch (IOException e) {
            Debug.println("Failed to save ending serial number");
            Debug.printStackTrace(e);
        }
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setCurrentPeriod(byte)
	 */
    @Override
	public  void setCurrentPeriod(final byte period) {
       // Debug.println("setCurrentPeriod(" + period + ") " + TimeUtility.currentTime());	 
        try {
            this.period = period;
            stateSaver.writeByte(period, State.CURRENT_PERIOD);
            //ReportLog.addNewSummaryRecords();
        }
        catch (IOException e) {
            Debug.println("Failed to save current period");
            Debug.printStackTrace(e);
        }
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getLastCheckInTime()
	 */
    @Override
	public  Date getLastCheckInTime() {
        long date = 0;
        if (stateSaver == null) return new Date(0);
        
        date = stateSaver.readLong(State.LAST_CHECK_IN);
        String token = Long.toString(date);
        if (token != null) {
        	if (token.length() > 4) {
                Debug.println("TR[..."+token.substring(token.length()-4)+"]");
        	}
        	else {
                Debug.println("TR[..."+token+"]");
        	}
        }
        else {
            Debug.println("TR[null]");
        }
        return new Date(date);
    }
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setLastCheckInTime(java.util.Date)
	 */
    @Override
	public  void setLastCheckInTime(final Date lastCheckInTime) {
        setLastCheckInTime(lastCheckInTime.getTime());
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setLastCheckInTime(long)
	 */
    @Override
	public  void setLastCheckInTime(final long lastCheckInTime) {
        try {
            String token = Long.toString(lastCheckInTime);
            if (token != null) {
            	if (token.length() > 4) {
                    Debug.println("TW[..."+token.substring(token.length()-4)+"]");
            	}
            	else {
                    Debug.println("TW[..."+token+"]");
            	}
            }
            else {
                Debug.println("TW[null]");
            }
            stateSaver.writeLong(lastCheckInTime, State.LAST_CHECK_IN);
        }
        catch (IOException e) {
            Debug.println("Failed to save last check in time");
            Debug.printStackTrace(e);
        }
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#dailyTransmitSucceeded(java.util.Date)
	 */
    @Override
	public  void dailyTransmitSucceeded(Date completed) {
        setLastReauthTime(completed);
        ErrorManager.setAWOLStatus(false);
    }

    // Get the last reauthorization time
    private  Date getLastReauthTime() {
        long date = 0;
        date = stateSaver.readLong(State.LAST_REAUTH);

        if (date == 0) {
            date = TimeUtility.currentTimeMillis();
            setLastReauthTime(new Date(date));
        }
        return new Date(date);
    }

    // Set the last reauthorization time
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setLastReauthTime(java.util.Date)
	 */
    @Override
	public  void setLastReauthTime(final Date lastReauthTime) {
        try {
            stateSaver.writeLong(lastReauthTime.getTime(),State.LAST_REAUTH);
        }
        catch (IOException e) {
            Debug.println("Failed to save last reauth time");
            Debug.printStackTrace(e);
        }
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#isAWOLTooLong()
	 */
    @Override
	public  boolean isAWOLTooLong() {
        final Date lastReauthTime = getLastReauthTime();
        final int daysAllowedAWOL = profileInstance.find("DAYS_ALLOWED_AWOL").intValue();	
        return Scheduler.daysElapsed(lastReauthTime) > daysAllowedAWOL;
    }
    
    // Get the last daily transmit time
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getLastDailyTransmitTime()
	 */
    @Override
	public  Date getLastDailyTransmitTime() {
        long date = 0;
        date = stateSaver.readLong(State.LAST_DAILY_XMIT);

        return new Date(date);
    }
    
    // Set the last daily transmit time
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setLastDailyTransmitTime(java.util.Date)
	 */
    @Override
	public  void setLastDailyTransmitTime
                            (final Date lastDailyTransmitTime) {
        try {
            stateSaver.writeLong(lastDailyTransmitTime.getTime(), 
                                 State.LAST_DAILY_XMIT);
        }
        catch (IOException e) {
            Debug.println("Failed to save last daily xmit time");
            Debug.printStackTrace(e);
        }
    }
    
    // Get the last attempted daily transmit time
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getLastAttemptedDailyTransmitTime()
	 */
    @Override
	public  Date getLastAttemptedDailyTransmitTime() {
        long date = 0;
        date = stateSaver.readLong(State.LAST_ATTEMPTED_DAILY_XMIT);

        return new Date(date);
    }
    
    // Set the last attempted daily transmit time
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setLastAttemptedDailyTransmitTime(java.util.Date)
	 */
    @Override
	public  void setLastAttemptedDailyTransmitTime
                                 (final Date lastAttemptedDailyTransmitTime) {
        try {
            stateSaver.writeLong(lastAttemptedDailyTransmitTime.getTime(), 
                    State.LAST_ATTEMPTED_DAILY_XMIT);
        }
        catch (IOException e) {
            Debug.println("Failed to save last attempted daily xmit time");
            Debug.printStackTrace(e);
        }
    }
    
    // Get the last dispenser check time
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getLastDispenserCheckTime()
	 */
    @Override
	public  Date getLastDispenserCheckTime() {
        long date = 0;
        date = stateSaver.readLong(State.LAST_DISPENSER_CHECK);
        return new Date(date);
    }
    
    // Set the last dispenser check time
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setLastDispenserCheckTime(java.util.Date)
	 */
    @Override
	public  void setLastDispenserCheckTime
                                         (final Date lastDispenserCheckTime) {
        try {
            stateSaver.writeLong(lastDispenserCheckTime.getTime(), 
                    State.LAST_DISPENSER_CHECK);
        }
        catch (IOException e) {
            Debug.println("Failed to save last dispenser check time");
            Debug.printStackTrace(e);
        }
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getLastDailyTotalResetTime()
	 */
    @Override
	public  Date getLastDailyTotalResetTime() {
        long date = 0;
        date = stateSaver.readLong(State.LAST_DAILY_TOTAL_RESET);
        return new Date(date);
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setLastDailyTotalResetTime(java.util.Date)
	 */
    @Override
	public  void setLastDailyTotalResetTime
                            (final Date lastDailyTotalResetTime) {
        try {
            stateSaver.writeLong(lastDailyTotalResetTime.getTime(), 
                    State.LAST_DAILY_TOTAL_RESET);
        }
        catch (IOException e) {
            Debug.println("Failed to save last daily total reset time");
            Debug.printStackTrace(e);
        }
    }
    

    @Override
	public void setLastBillPayDailyTotalResetTime(final Date lastDailyBillPayTotalResetTime) {
        try {
            stateSaver.writeLong(lastDailyBillPayTotalResetTime.getTime(), 
                    State.LAST_DAILY_BILL_PAY_TOTAL_RESET);
        }
        catch (IOException e) {
            Debug.println("Failed to save last daily total reset time");
            Debug.printStackTrace(e);
        }
    }
    @Override
	public  Date getLastDailyBillPayTotalResetTime() {
        long date = 0;
        date = stateSaver.readLong(State.LAST_DAILY_BILL_PAY_TOTAL_RESET);
        return new Date(date);
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getUnitProfileID()
	 */
    @Override
	public  int getUnitProfileID() {
        if (get("SUBAGENT_MODE", "N").equals("Y")) {
            int id = 0;
            try {
                id = stateSaver.readInt(State.SUPERAGENT_PROFILE_ID);
            }
            catch (IOException e) {
                Debug.printStackTrace(e);
            }
            return id;
        }
        else {
            return get("PROFILE_ID", 0);
        }
    }
    
    // Added as part of AGG3 API Changes - new elements in Request 
    
    @Override
	public  String getChannelType() { 
        return get(DWValues.PROFILE_CHANNEL_TYPE, DWValues.PROFILE_DEFAULT_CHANNEL_TYPE);
    }
    
    @Override
	public  String getPoeType() { 
        return get(DWValues.PROFILE_POE_TYPE, DWValues.PROFILE_DEFAULT_POE_TYPE);
    }
    
    @Override
	public  String getTargetAudience() { 
        return get(DWValues.PROFILE_TARGET_AUDIENCE, 
        		DWValues.PROFILE_DEFAULT_TARGET_AUDIENCE);
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#saveSuperagentUnitProfileID()
	 */
    @Override
	public  void saveSuperagentUnitProfileID() {
        if (get("SUBAGENT_MODE", "N").equals("Y")) {
            return;
        }
        int id = get("PROFILE_ID", 0);
        try {
            stateSaver.writeInt(id, State.SUPERAGENT_PROFILE_ID);
        }
        catch (IOException e) {
            Debug.println("Failed to save superagent profile id");
            Debug.printStackTrace(e);
        }
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#saveChanges()
	 */
    @Override
	public  void saveChanges() throws IOException {
        if (profileInstance == null) 
            return;
        
        try {
            long fp = file.getEnd();
            byte[] buffer = profileInstance.toString().getBytes("Cp1252"); 
            file.write(buffer);
            file.setStart(fp);
            file.flush();
        }
        catch (UnsupportedEncodingException e) {
            // impossible
        }
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#saveChangesQuietly()
	 */
    @Override
	public  void saveChangesQuietly() {
        try {
            saveChanges();
        }
        catch (IOException e) {
			Debug.printStackTrace(e);
        }        
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#validatePin(java.lang.String)
	 */
    @Override
	public  boolean validatePin(String pin) {
        Iterator<ProfileItem> i;
	ProfileItem item;
        Profile userProfile;
        ProfileItem expired;
        boolean result = true;

        i = profileInstance.iterator();
	while (i.hasNext()) {
	    item = i.next();
	    if (item.matches(null, pin)) {      // matches USER profiles with this pin
                result = false;
                userProfile = (Profile) item;
                try {
                    expired = userProfile.find("PIN_EXPIRED");	
                    expired.setValue(true);
                }
                catch (NoSuchElementException e) {
                    //impossible because PIN_EXPIRED is mode P
                }
	    }
	}
        return result;
    }
      
    // Try to keep all these darn totals straight. The API represented
    // by the following methods is rather piecemeal, but that's because
    // I told folks to stub stuff in here that they needed elsewhere.
    

    private static Total total[] = new Total[NUMBER_OF_TOTALS];
// Prem: I am sticking this cooment to show how the total arry is used for various stuff    
//  Debug.println("Daily total:         " + state.getTotal(0));
//  Debug.println("Batch total MO:      " + state.getTotal(1));
//  Debug.println("Batch total VP:      " + state.getTotal(2));
//  Debug.println("Batch total GC:      " + state.getTotal(3));
//  Debug.println("Batch total DG:      " + state.getTotal(4));
//  Debug.println("Diag summary total:  " + state.getTotal(12));
//  Debug.println("Diag summary MO:     " + state.getTotal(13));
//  Debug.println("Diag summary VP:     " + state.getTotal(14));
//  Debug.println("Diag summary MGS:    " + state.getTotal(15));
//  Debug.println("Diag summary MGR:    " + state.getTotal(16));
//  Debug.println("Diag summary DG:     " + state.getTotal(17));
//  Debug.println("Diag summary XP:     " + state.getTotal(18));
//  Debug.println("Diag summary void:   " + state.getTotal(19));
//  Debug.println("Diag summary limbo:  " + state.getTotal(20));
//  Debug.println("Diag summary fee:    " + state.getTotal(11));
// Debug.println("Daily Bill Pay Total:    " + state.getTotal(50));

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getTotal(int)
	 */
    @Override
	public  Total getTotal(int index) {
        if (total[index] == null) {
            //Debug.println("UnitProfile creating total[" + index + "]");	 
            total[index] = new Total(stateSaver, State.TOTAL_START + 
                    index * State.BATCH_TOTAL_LENGTH);
        }
        return total[index];
    }
//    public  Total getBillPayTotal(int index) {
//        if (total[index] == null) {
//            //Debug.println("UnitProfile creating total[" + index + "]");	 
//            total[index] = new Total(stateSaver, State.TOTAL_START + 
//                    index * State.BATCH_TOTAL_LENGTH);
//        }
//        return total[index];
//    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getTotal(int, int)
	 */
    @Override
	public  Total getTotal(int app, int doc) {
        return getTotal(GRAND_TOTAL + (app * DW_TOTALS_PRODUCTS) + (doc - 1));
    }

    // the daily total is used by transactions and cleared by the scheduler
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getDailyTotal()
	 */
    @Override
	public  BigDecimal getDailyTotal() {
        return getTotal(DAILY_TOTAL).asBigDecimal();
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#addDailyTotal(java.math.BigDecimal)
	 */
    @Override
	public  void addDailyTotal(BigDecimal amount) {
        //Debug.println("addDailyTotal("+amount+") called from");	
        //Thread.dumpStack();
        getTotal(DAILY_TOTAL).incrementAmount(amount);
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#clearDailyTotal()
	 */
    @Override
	public  void clearDailyTotal() {
        //getTotal(DAILY_TOTAL).clear();
        Total tot = getTotal(DAILY_TOTAL);
        tot.clear();
        tot.saveState();
    }
    
    @Override
	public BigDecimal getDailyBillPayTotal() {
        
        return getTotal(DAILY_BILL_PAY_TOTAL).asBigDecimal();
      }
    @Override
	public void addDailyBillPayTotal(BigDecimal amount) {
        
        getTotal(DAILY_BILL_PAY_TOTAL).incrementAmount(amount);
      }
      
    @Override
	public void clearDailyBillPayTotal() {
        Total tot = getTotal(DAILY_BILL_PAY_TOTAL);
        tot.clear();
        tot.saveState();
      }
     
    // the daily total is used by transactions and cleared by the scheduler
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getDailyTotal()
	 */
    @Override
	public int getDailyCount(int docType) {
        return getTotal(DAILY_COUNT + docType).getCount();
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#addDailyTotal(java.math.BigDecimal)
	 */
    @Override
	public  void addDailyCount(int count, int docType) {
        getTotal(DAILY_COUNT + docType).incrementCount(count);
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#clearDailyTotal()
	 */
    @Override
	public  void clearDailyCount() {
        Total totalLoad = getTotal(DAILY_COUNT + UnitProfileInterface.DW_ID_TOTALS_CARD_LOAD);
        totalLoad.clear();
        totalLoad.saveState();
        Total totalPurchase = getTotal(DAILY_COUNT + UnitProfileInterface.DW_ID_TOTALS_CARD_PURCHASE);
        totalPurchase.clear();
        totalPurchase.saveState();
    }
    
    // Batch totals are incremented and decremented by ItemForwarder.
    // Document number for batch totals is 1-based.

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getBatchTotal(int)
	 */
    @Override
	public  BigDecimal getBatchTotal(int document) {
        return getTotal(BATCH_TOTAL + document - 1).asBigDecimal();
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#addBatchTotal(java.math.BigDecimal, int)
	 */
    @Override
	public  BigDecimal addBatchTotal(BigDecimal amount, int document) {
        //Debug.println("\naddBatchTotal("+amount+","+document+")");
        return getTotal(BATCH_TOTAL + document - 1).incrementAmount(amount).asBigDecimal();
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#clearBatchTotal(int)
	 */
    @Override
	public  void clearBatchTotal(int document) {
        getTotal(BATCH_TOTAL + document - 1).clearAmount();
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#addBatchCount(int, int)
	 */
    @Override
	public  int addBatchCount(int count, int document) {
        //Debug.println("\naddBatchCount("+count+","+document+")");
        /*
         * If in Demo mode, don't add to batch, since it will not be added
         * to the CTRI record, which will cause issues with demo mode transmits.
         */
        if (UnitProfile.getInstance().isNewDemoMode()) {
		    Debug.println("Skipping addBatchCount for demo server mode");	
        	return 0;
        }

        return getTotal(BATCH_TOTAL + document - 1).incrementCount(count).getCount();
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getBatchCount(int)
	 */
    @Override
	public  int getBatchCount(int document) {
        return getTotal(BATCH_TOTAL + document - 1).getCount();       
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#clearBatchCount(int)
	 */
    @Override
	public  void clearBatchCount(int document) {
        getTotal(BATCH_TOTAL + document - 1).clearCount();
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#clearBatchTotalsAndCounts()
	 */
    @Override
	public  void clearBatchTotalsAndCounts() {
        Total tot = null;
        for (int t = BATCH_TOTAL; t < SUMMARY_FEE_TOTAL; t++) {
            //getTotal(t).clear();
            tot = getTotal(t);
            tot.clear();
            tot.saveState();
        }
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getTransmissionCount(int)
	 */
    @Override
	public  int getTransmissionCount(int docSeq) {
        Profile prodProfile = (Profile) profileInstance.find("PRODUCT", docSeq);	
        try {
            ProfileItem item = prodProfile.find("XMIT_BATCH_COUNT");	
            return item.intValue();
        }
        catch (NoSuchElementException e) {
            Debug.println("No XMIT_BATCH_COUNT in profile...defaulting.");	
            return 10;
        }
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getTransmissionAmount(int)
	 */
    @Override
	public  BigDecimal getTransmissionAmount(int docSeq) {
        Profile prodProfile = (Profile) profileInstance.find("PRODUCT", docSeq);	
        try {
            ProfileItem item = prodProfile.find("XMIT_BATCH_AMOUNT");	
            return item.bigDecimalValue();
        }
        catch (NoSuchElementException e) {
            Debug.println("No XMIT_BATCH_AMOUNT in profile...defaulting.");	
            return new BigDecimal(2000.00);
        }
    }

    // Summary totals are incremented and cleared by DiagLog
    // and used in DiagSummaryMessage. Document numbers for summary
    // totals are zero-based.

//    /** Gets the summary count and total for the specified document
//      * type since the last diag log transmit. Valid document types
//      * are in DiagSummaryMessage. Currently 0=overall total through
//      * 8=limbo total.
//      */
//    public  CountAndAmount getDiagBatchCountAndTotal(int document) {
//        return getTotal(SUMMARY_TOTAL + document).getCountAndAmount();
//    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#addDiagBatchCount(int, int)
	 */
    @Override
	public  int addDiagBatchCount(int count, int document) {
        return getTotal(SUMMARY_TOTAL + document).incrementCount(count).getCount();
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#addDiagBatchAmount(java.math.BigDecimal, int)
	 */
    @Override
	public  BigDecimal addDiagBatchAmount(BigDecimal amount, int document) {
        //Debug.println("\naddDiagBatchAmount("+amount+","+document+") called from");
        //Debug.dumpStack();
    	Total value = getTotal(SUMMARY_TOTAL + document);
    	if (amount != null) {
    		value = value.incrementAmount(amount);
    	}
        return value.asBigDecimal();
    }

//    /** Gets the summary fee total. */
//    public  Amount getDiagBatchFeeTotal() {
//        return getTotal(SUMMARY_FEE_TOTAL).getAmount();
//    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#addDiagBatchFee(java.math.BigDecimal)
	 */
    @Override
	public  BigDecimal addDiagBatchFee(BigDecimal amount) {
        return getTotal(SUMMARY_FEE_TOTAL).incrementAmount(amount).asBigDecimal();
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#clearDiagBatchCountsAndTotals()
	 */
    @Override
	public  void clearDiagBatchCountsAndTotals() {
        Total tot = null;
        for (int t = SUMMARY_FEE_TOTAL; t < GRAND_TOTAL; t++) {
            //getTotal(t).clear();
            tot = getTotal(t);
            tot.clear();
            tot.saveState();
        }
    }

    // grand totals are incremented by transactions and never cleared

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getGrandTotal(int, int)
	 */
    @Override
	public  BigDecimal getGrandTotal(int app, int document) {
        return getTotal(app, document).asBigDecimal();
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#addGrandTotal(java.math.BigDecimal, int, int)
	 */
    @Override
	public  BigDecimal addGrandTotal(BigDecimal amount, int app, 
            int document) {
        return getTotal(app, document).incrementAmount(amount).asBigDecimal();
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#clearGrandTotal(int, int)
	 */
    @Override
	public  void clearGrandTotal(int app, int document) {
        getTotal(app, document).clearAmount();
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getGrandCount(int, int)
	 */
    @Override
	public  int getGrandCount(int app, int document) {
        return getTotal(app, document).getCount();
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#addGrandCount(int, int, int)
	 */
    @Override
	public  int addGrandCount(int count, int app, int document) {
        return getTotal(app, document).incrementCount(count).getCount();
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#clearGrandCount(int, int)
	 */
    @Override
	public  void clearGrandCount(int app, int document) {
        getTotal(app, document).clearCount();
    }

    public static void main(String[] args) {
        try {
            (new State()).createProfile();
            Debug.println("in bus ? " + UnitProfile.getInstance().isWithinBusinessHours());	
        }
        catch (Exception e) {
			Debug.printStackTrace(e);
        }
    }
    @Override
	public void addBillPayTotal(BigDecimal amount, int count,
            int application, int docType, boolean voided) {
        if (! voided) addDailyBillPayTotal(amount);

    }
/*    public  void main(String args[]) {
        State state = State.getInstance();
        UnitProfile.getUPInstance().setStateSaver(state);
        Debug.println("Daily total = " + UnitProfile.getUPInstance().getDailyTotal());	
    }*/

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#addTotalAndCount(java.math.BigDecimal, int, int, int, boolean)
	 */
    @Override
	public  void addTotalAndCount(BigDecimal amount, int count, 
            int application, int docType, boolean voided) {
        
        // I'm guessing voided items don't count toward the daily total
        if (! voided) addDailyTotal(amount);
        
        // Eric says the ItemForwarder takes care of the item batch totals

        // Eric agrees the DiagLog ought to take care of the summary totals

        // convert document type according to application
        if (application == DNET_ID_APP_DSPN) {
            if (voided) {
                docType = DW_ID_TOTALS_DSPN_VOID;
            }
        }
        else if (application == DNET_ID_APP_MGRAM) {
            if (voided) {
                return;
            }
            if (docType == 4) docType = DW_ID_TOTALS_MGRAM_RECV;
            else if (docType == 90) docType = DW_ID_TOTALS_MGRAM_SEND;
            else if (docType == 91) docType = DW_ID_TOTALS_MGRAM_XPAY;
        }
        else if (application == DNET_ID_APP_CARD) {
            if (voided) {
                return;
            }
            addDailyCount(count, docType);
//            if (docType == 4) {
//            	docType = DW_ID_TOTALS_CARD_PURCHASE;
//            	addDailyCount(count, docType);
//            }
//            else if (docType == 5) {
//            	docType = DW_ID_TOTALS_CARD_LOAD;
//            	addDailyCount(count, docType);
//            }
        }
        
        addGrandTotal(amount, application, docType);
        addGrandCount(count, application, docType);
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getBusinessStartTime(int)
	 */
    
    @Override
	public  String getBusinessStartTime(int day) {
        String profileHours = profileInstance.find(
                                    "AGENT_HOURS", day).stringValue();	
        // split the start and end time out
        StringTokenizer tok = new StringTokenizer(profileHours, ",");	
        String start = tok.nextToken().trim();
        return start;
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getBusinessEndTime(int)
	 */
    @Override
	public  String getBusinessEndTime(int day) {
        String profileHours = profileInstance.find(
                                    "AGENT_HOURS", day).stringValue();	
        // split the start and end time out
        StringTokenizer tok = new StringTokenizer(profileHours, ",");	
        tok.nextToken();
        String end = tok.nextToken().trim();
        return end;
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getBusinessEndTime()
	 */
    @Override
	public  String getBusinessEndTime() {
        Calendar localCalendar = Scheduler.getLocalCalendar();
        int currentDay = TimeUtility.getDwDayOfWeek(localCalendar);
        return getBusinessEndTime(currentDay);
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getBusinessStartTime()
	 */
    @Override
	public  String getBusinessStartTime() {
        Calendar localCalendar = Scheduler.getLocalCalendar();
        int currentDay = TimeUtility.getDwDayOfWeek(localCalendar);
        return getBusinessStartTime(currentDay);
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#isWithinBusinessHours()
	 */
    @Override
	public  boolean isWithinBusinessHours() {
        String start = getBusinessStartTime();
        String end = getBusinessEndTime();
        DateFormat df = new SimpleDateFormat(TimeUtility.TIME_24_HOUR_MINUTE_SECOND_FORMAT);
        df.setCalendar(Scheduler.getLocalCalendar());
        String currentTime = df.format(TimeUtility.currentTime());
        boolean within = false;

        if (start.compareTo(end) < 0) {
            // case 1 : start time less than end time...
            // must be between start and end
            within = currentTime.compareTo(start) >= 0 &&
                     currentTime.compareTo(end) <= 0;
        }
        else if (start.compareTo(end) > 0) {
            // case 2 : hours span midnight
            // must be greater than start and end
            within = currentTime.compareTo(start) >= 0 &&
                     currentTime.compareTo(end) >= 0;
        }
        else {
            // start and end time equal...always in business hours!
            return true;
        }

        java.text.SimpleDateFormat displayFormat =
            new java.text.SimpleDateFormat("HH:mm:ss z");	
        Debug.println("CHECKING BUSINESS HOURS : ");	
        displayFormat.setCalendar(Scheduler.getLocalCalendar());
        Debug.println("      " 	
                + displayFormat.format(TimeUtility.currentTime())
                + " is within : " + start + " to " + end + " ? " + within);
        return within;
    }

    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#addProfileChangedListener(dw.profile.ProfileChangedListener)
	 */
    @Override
	public  void addProfileChangedListener
                                            (final ProfileChangedListener l) {
        
        List<ProfileChangedListener> listeners = null;
        if (profileListenersMap.containsKey(l.getListenerKey())) 
            // This is not the first listener attempting to register for 
            // change notification on the particular profile item.  Obtain
            // the list of listeners and add this listener to the list.
            listeners = profileListenersMap.get(l.getListenerKey());
        else {
            // This is the first listener attempting to register for 
            // change notification on the particular  profile item.  Create a 
            // list of listeners and add this listener to the list.
            listeners = new ArrayList<ProfileChangedListener>();
            profileListenersMap.put(l.getListenerKey(), listeners);
        }
        listeners.add(l);
    } // END_METHOD
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#removeProfileChangedListener(dw.profile.ProfileChangedListener)
	 */
    @Override
	public  void removeProfileChangedListener
                                            (final ProfileChangedListener l) {
        
        if (profileListenersMap.containsKey(l.getListenerKey())) { 
            // Obtain the list of listeners and remove this listener 
            // from the list.
            List<ProfileChangedListener> listeners = profileListenersMap
                                                    .get(l.getListenerKey());
            listeners.remove(l);
        }
    } // END_METHOD
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#fireProfileChanged(dw.profile.ProfileChangedEvent)
	 */
    @Override
	public  void fireProfileChanged(final ProfileChangedEvent evt) {
        
        if (profileListenersMap.containsKey(evt.getListenerKey())) { 
            // Obtain the list of listeners and remove this listener 
            // from the list.
            List<ProfileChangedListener> listeners = profileListenersMap
                                                    .get(evt.getListenerKey());
            for (Iterator<ProfileChangedListener> iterator = listeners.iterator(); 
                                                        iterator.hasNext();) {
                ProfileChangedListener l = iterator.next();                                            
                l.profileChanged(evt);                                                                
            }                                                        
        }
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setLanguage(java.lang.String)
	 */
    @Override
	public  void setLanguage(String language) {
        set("LANGUAGE", language);
        Messages.setCurrentLocale(language);
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#setAltLanguage(java.lang.String)
	 */
    @Override
	public  void setAltLanguage(String language) {
        set("ALT_LANGUAGE", language);
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#getAlternateLanguage()
	 */
    @Override
	public  String getAlternateLanguage() {
        String lang = get("LANGUAGE", "en");
        String alt = get("ALT_LANGUAGE", "es");
        if((!lang.equals("en")) && (!alt.equals("en")) 
            || (lang.equals(alt)))
            alt= "en";
        if (lang.equals(alt))
            alt = "es";
        return alt;
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#updateProxyProperties()
	 */
    @Override
	public  void updateProxyProperties() {
        if (noproxy) {
            Debug.println("UnitProfile.updateProxyProperties() skipping because -noproxy is set");
            return;
        }
        Debug.println("updateProxyProperties() USE_PROXY=" 
            + get("USE_PROXY", "") 
            + " PROXY_TYPE=" + get("PROXY_TYPE", ""));
        
        System.setProperty("http.proxyHost", "");
        System.setProperty("http.proxyPort", "");
//        System.setProperty("http.proxyUser", "");
//        System.setProperty("http.proxyPassword", "");
        System.setProperty("https.proxyHost", "");
        System.setProperty("https.proxyPort", "");
//        System.setProperty("https.proxyUser", "");
//        System.setProperty("https.proxyPassword", "");
        System.setProperty("socksProxyHost", "");
        System.setProperty("socksProxyPort", "");
//        System.setProperty("java.net.socks.username", "");
//        System.setProperty("java.net.socks.password", "");
        
        if (get("USE_PROXY", "N").equals("Y")) {
            
            java.net.PasswordAuthentication auth = new java.net.PasswordAuthentication(
                    get("PROXY_USER", ""), get("PROXY_PASSWORD", "").toCharArray());
            AgentConnectAuthenticator.setPasswordAuthentication(auth);
            
            if (get("PROXY_TYPE", "").equals("HTTP")) {
                System.setProperty("http.proxyHost", get("PROXY_HOST", ""));
                System.setProperty("http.proxyPort", get("PROXY_PORT", ""));
//                System.setProperty("http.proxyUser", get("PROXY_USER", ""));
//                System.setProperty("http.proxyPassword", get("PROXY_PASSWORD", ""));
            }
            else if (get("PROXY_TYPE", "").equals("HTTPS")) {
                System.setProperty("https.proxyHost", get("PROXY_HOST", ""));
                System.setProperty("https.proxyPort", get("PROXY_PORT", ""));
//                System.setProperty("https.proxyUser", get("PROXY_USER", ""));
//                System.setProperty("https.proxyPassword", get("PROXY_PASSWORD", ""));
            }
            else if (get("PROXY_TYPE", "").equals("SOCKS")) {
                System.setProperty("socksProxyHost", get("PROXY_HOST", ""));
                System.setProperty("socksProxyPort", get("PROXY_PORT", ""));
//                System.setProperty("java.net.socks.username", get("PROXY_USER", ""));
//                System.setProperty("java.net.socks.password", get("PROXY_PASSWORD", ""));
            }
        }
    }
    
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#isSubAgentTransactionAllowed()
	 */
    @Override
	public  boolean isSubAgentTransactionAllowed() {
        if (profileInstance == null) 
            return true;
        else
           return  get("SA_RESTRICT_STATUS","A").equals("A");
    }
           
   	/* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#updateSubAgentProfileItems()
	 */
	@Override
	public  void updateSubAgentProfileItems() {
      //  if(get("SUPERAGENT","N").equals("N")){
            ProfileItem subAgentMode = find("SUBAGENT_MODE");
            subAgentMode.setValue("Y");
            subAgentMode.setChanged(false); 
       // }
      
        ProfileItem dualControl = find("DUAL_CONTROL");
        dualControl.setValue("N");
        dualControl.setChanged(false); 
//    	Commented by Prem for  SUBAGENT_MODE taken over by super Agent 
//        ProfileItem globalNamePinRequired = find("GLOBAL_NAME_PIN_REQ");
//        globalNamePinRequired.setValue("N");
//        globalNamePinRequired.setChanged(false); 
               
	}
  

	private Profile getSuperAgentEmployees() {
		Profile profileClone = null;
		try {
			profileClone = (Profile) profileInstance.clone();
		} catch (CloneNotSupportedException e) {
			Debug.printStackTrace(e);
		}
		Iterator<ProfileItem> iter = profileClone.iterator();
		while (iter.hasNext()) {
			ProfileItem profileItem = iter.next();
			if ((profileItem.getTag().equals("USER"))) {
				continue;
			} else
				iter.remove();

		}
		return profileClone;

	}

	private Profile removeSubAgentEmployees() {
		Profile profileClone = null;
		try {
			profileClone = profileInstance;
		} catch (Exception e) {
			Debug.printStackTrace(e);
		}
		Iterator<ProfileItem> iter = profileClone.iterator();
		while (iter.hasNext()) {
			ProfileItem profileItem = iter.next();
			if ((profileItem.getTag().equals("USER"))) {
				iter.remove();
			} else
				continue;

		}
		return profileClone;

	}

/**
 * @return Returns the subAgentTakeOverUserID.
 */
@Override
public  int getSubAgentTakeOverUserID() {
	return this.subAgentTakeOverUserID;
}
/**
 * @param subAgentTakeOverUserID The subAgentTakeOverUserID to set.
 */
@Override
public  void setSubAgentTakeOverUserID(int subAgentTakeOverUserID) {
	this.subAgentTakeOverUserID = subAgentTakeOverUserID;
}
    /**
     * Return a profile with Super agent's connectivity settings  
     * @return Profile.
     */
  
    private  Profile getSuperAgentProfileItems(){
        Profile profileClone = null;
            try {
                profileClone = (Profile) profileInstance.clone();
            }
            catch (CloneNotSupportedException e) {
    			Debug.printStackTrace(e);
            }
            Iterator<ProfileItem> iter = profileClone.iterator();
             while(iter.hasNext()){
                    ProfileItem profileItem = iter.next();
                   if((profileItem.getTag().equals("AC_NETWORK_URL"))           
                        ||(profileItem.getTag().equals("AC_DIALUP_URL"))        
                        ||(profileItem.getTag().equals("POS_HOST_CONNECTION"))  
                        ||(profileItem.getTag().startsWith("PROXY_"))           
                        ||(profileItem.getTag().endsWith("PROXY"))             
                        ||(profileItem.getTag().startsWith("PRIMARY"))          
                        ||(profileItem.getTag().startsWith("SECONDARY"))        
                        ||(profileItem.getTag().equals("HOST_SPECIAL_DIAL"))    
                        ||(profileItem.getTag().equals("SECONDARY_HOST_SPECIAL_DIAL"))    
                        ||(profileItem.getTag().equals("HOST_PHONE_TYPE"))
                        ||(profileItem.getTag().equals("CONNECTION_CLOSE_DELAY"))
                        ||(profileItem.getTag().equals("HOST_SECONDARY_PHONE"))
                        ||(profileItem.getTag().equals("HOST_PRIMARY_PHONE"))
                        ||(profileItem.getTag().startsWith("AGENT_PRINTER"))
                        ||(profileItem.getTag().endsWith("AGENT_PRINTER"))
                        ||(profileItem.getTag().endsWith("USER_PIN_TYPE"))
                        ||(profileItem.getTag().equals("SOFTWARE_DOWNLOAD_URL"))
                        ||(profileItem.getTag().endsWith("LANGUAGE"))
                        ||(profileItem.getTag().equals("TRAINING_MODE"))
                        ||(profileItem.getTag().equals("RESET_PASSWORD"))
                        ||(profileItem.getTag().equals("SOFTWARE_VERSION_ID"))
                        ||(profileItem.getTag().equals("RECEIPTS_ENABLE"))
                        ||(profileItem.getTag().endsWith("PASS_THROUGH_LOGON"))
                        ||(profileItem.getTag().startsWith("PRIMARY_SCRIPT_"))
                        ||(profileItem.getTag().startsWith("SECONDARY_SCRIPT_"))
                        ||(profileItem.getTag().startsWith("GLOBAL_NAME_PIN_REQ"))
                        ||(profileItem.getTag().equals(UnitProfileInterface.MFA_FLAG))
                        ||(profileItem.getTag().equals(UnitProfileInterface.MFA_TOKEN))
                        ||(profileItem.getTag().equals(UnitProfileInterface.SEND_REVERSAL_FLAG))
                        ||(profileItem.getTag().endsWith("_TIMEOUT"))){ // not required here fixed in DebugLog.java      
                            continue;
                        }
                     
                    else
                        iter.remove();     
             }
        return  profileClone; 
    }
	
        
    /* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#checkTransmitConditions()
	 */ 
    @Override
	public  boolean checkTransmitConditions() {
//        Debug.println("UnitProfile.checkTransmitConditions()");
//        Debug.println("not isPosAuthorized = " + (! isPosAuthorized()));
//        Debug.println("isForceTransmit = " + isForceTransmit());
//        Debug.println("isAWOLTooLong = " + isAWOLTooLong());
//        Debug.println("not isSubAgentTransactionAllowed = " + (! isSubAgentTransactionAllowed()));
//        Debug.println("newUserAddedOrPinReset = " + newUserAddedOrPinReset());
//        Debug.println("needReceiptTemplates = " + needReceiptTemplates());
//        Debug.println("isForceUpgrade = " + isForceUpgrade());
        
        if ( ! isPosAuthorized() ||
        		isForceTransmit() ||
                isAWOLTooLong() ||
                !isSubAgentTransactionAllowed() ||
                newUserAddedOrPinReset() ||
                needReceiptTemplates() ||
                isForceUpgrade())
            return true;
        else
            return false;   
     }
    public static final int FORCE_TRANSMIT_Y =1;
    public static final int FORCE_TRANSMIT_N =0;

	@Override
	public void setForceTransmit(boolean doForceTransmit) {
		try {
			if (doForceTransmit) {
				stateSaver.writeInt(FORCE_TRANSMIT_Y, State.FORCE_TRANSMIT);
			} else {
				stateSaver.writeInt(FORCE_TRANSMIT_N, State.FORCE_TRANSMIT);
			}
		} catch (IOException e) {
			Debug.println("Handled Error storing tramsit status in state.dat");
		}
	}

	@Override
	public boolean isForceTransmit() {
		int id = 0;
		try {
			id = stateSaver.readInt(State.FORCE_TRANSMIT);
			if (FORCE_TRANSMIT_Y == id) {
				return true;
			}
		} catch (IOException e) {
			Debug.println("Handled Error reading tramsit status in state.dat");
		}
		return false;
	}
	@Override
	public boolean isTakeOverSubagentprofile() {
		return takeOverSubagentprofile;
	}
	
	@Override
	public void setTakeOverSubagentprofile(boolean takeOverSubagentprofile) {
		this.takeOverSubagentprofile = takeOverSubagentprofile;
	}
	
	
	public boolean newUserAddedOrPinReset(){
	    boolean changed = false;
	    if(!UnitProfile.getInstance().isDemoMode()){
	    if("6CHAR".equalsIgnoreCase(UnitProfile.getInstance().get("USER_PIN_TYPE","3DIGIT"))){
	        Iterator<ProfileItem> i = UnitProfile.getInstance().getProfileInstance().iterator();
	        while (i.hasNext()) {         
	            ProfileItem item = i.next();
	            if (item.matches("USER")&& item.getStatus().equalsIgnoreCase("A")) {	
	                String value =item.get("PASSWORD").stringValue();
	                if(item.get("PASSWORD").isChanged()&& value.length()== 3){
	                    changed= true;
	                    break;
	                }

	            }
	        }
	    }
	    }
	   return changed;
	}
        
	@Override
	public void setAgentInfo(SubagentInfo  info){
		this.info = info;
		
	}
	@Override
	public SubagentInfo getAgentInfo(){
		return info;
	}
	/* (non-Javadoc)
	 * @see dw.profile.UnitProfileInterface#isProfileUseOurPaper()
	 */
	@Override
	public boolean isProfileUseOurPaper() {
		boolean retVal = false;
		if ("Y".equals(get("USE_OUR_PAPER", "Y")))
			retVal = true;
		return retVal;
	}
	@Override
	public boolean isForceUpgrade() {
		return forceUpgrade;
	}
	@Override
	public void setForceUpgrade(boolean forceUprade) {
		this.forceUpgrade = forceUprade;
	}
	
	@Override
	public boolean isMfaEnabled() {
		String value = get(UnitProfileInterface.MFA_FLAG, "N");
		return value.equals("Y");
	}
	
	@Override
	public String getMfaTokenId() {
		return get(UnitProfileInterface.MFA_TOKEN, "");
	}
	
	@Override
	public boolean isSendReversalEnabled() {
		String value = get(UnitProfileInterface.SEND_REVERSAL_FLAG, "Y");
		return value.equals("Y");
	}
	
	@Override
	public int getBroadcastMessageUpdateInterval() {
		return get(UnitProfileInterface.BROADCAST_MESSAGE_UPDATE_INTERVAL, 60);
	}
	
	@Override
	public int getAccountDepositTableUpdateInterval() {
		return get(UnitProfileInterface.ACCOUNT_DEPOSIT_TABLE_UPDATE_INTERVAL, 12);
	}
	
	@Override
	public List<String> getAllowedDeleiveryOptions() {
		Profile product = (Profile) getInstance().getProfileInstance().find("PRODUCT", 3); 

		Iterator<ProfileItem> iterator = product.iterator();
		List<String> retList = new ArrayList<String>();
		while (iterator.hasNext()) {
			ProfileItem item = iterator.next();
			if (item.tag.equals("ALLOW_DEL_OPT") && item.value!=null && item.value.toString().equals("Y")) {
				retList.add(String.valueOf(item.id));
			}
		}
		return retList;

	}
	
	private static final String DISABLE_LINE_COMPRESSION = "THERMAL_DISABLE_LINE_COMPRESSION";
	
	/*
	 * Initialize the EpsonLineCompression support.  It is also saved 
	 * in state.dat (so any changes done by the agent persist.  The
	 * state file may already have data there so use a string instead of just
	 * a byte flag to reduce the likelyhood of treating garbage as a valid value
	 */
	@Override
	public void initEpsonLineCompression() {
		/*
		 * Check for it in the profile, if we have a profile item, then we are
		 * done.
		 */
		String sDisableLineCompression = get(DISABLE_LINE_COMPRESSION, "N");
		if (sDisableLineCompression.length() == 0) {
        	ExtraDebug.println("DISABLE_LINE_COMPRESSION not found in profile");
        	// No state.dat retrieval is attempted here; initialize to default
			sDisableLineCompression = "N";
			ProfileItem prof = ProfileItem.create("ITEM");
			prof.setTag(DISABLE_LINE_COMPRESSION);
			prof.setMode("S");
			prof.setValue(sDisableLineCompression);
			prof.setChanged(true);
			profileInstance.add(prof);
		}
	}

	/*
	 * Read the EpsonLineCompression state from the profile.
	 */
	@Override
	public boolean useEpsonLineCompression() {
		String sDisableLineCompression = get(DISABLE_LINE_COMPRESSION, "");
		/*
		 * if something happened and it is not in the profile, try reintializing
		 */
		if (sDisableLineCompression.length() == 0) {
        	ExtraDebug.println("DISABLE_LINE_COMPRESSION not found in profile");
			initEpsonLineCompression();
			sDisableLineCompression = get(DISABLE_LINE_COMPRESSION, "N");
		}
		//Disabling the Line Compression results in NOT using Epson line compression
		return !sDisableLineCompression.equals("Y");
	}

	/*
	 * Set the EpsonLineCompression state in both the profile and the state
	 * file.
	 */
	@Override
	public void setEpsonLineCompression(boolean useLineCompression) {
		if (useLineCompression) {
			set(DISABLE_LINE_COMPRESSION, "N");
		} else {
			set(DISABLE_LINE_COMPRESSION, "Y");
		}
	}

	public void markEpsonLineCompressionAsUnchanged() {
		Profile profile = UnitProfile.getInstance().getProfileInstance();
		try {
			/*
			 * Mark Epson Line Compression as unchanged so it does not get sent 
			 * up to AC (since currently they are not expecting it)
			 */
			ProfileItem item = profile.find(DISABLE_LINE_COMPRESSION);
			item.setChanged(false);
		} catch (NoSuchElementException e) {
			/*
			 * If it does not exist, no need to mark it is unchanged
			 */
			Debug.println("Handled Error getting compression profile item");
		}
	}
	
	@Override
	public String getAgentCountryIsoCode() {
		return get("AGENT_COUNTRY_ISO_CODE", "USA");
	}
}
