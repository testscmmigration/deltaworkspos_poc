/*
 * Created on Apr 20, 2007
 *
 */
package dw.profile;

import java.util.NoSuchElementException;

import dw.utility.Debug;

/**
 * @author T025
 *
 */
public class ProfileAccessor {
	 private int MONEY_GRAM_RECEIVE_DOC_SEQ = 4;
	 private int BILL_PAYMENT_DOC_SEQ = 5;
	
	static ProfileAccessor instance = null;
	private ProfileAccessor(){
		
	}
	public static ProfileAccessor getInstance(){
		if (instance == null) {
			instance = new ProfileAccessor();
		}
		return instance;
	}
	
	   public boolean getTranAuth(int docSeq) {
        Profile profile = UnitProfile.getInstance().getProfileInstance();
        ProfileItem item = profile.find("PRODUCT", docSeq); 
        return item.getStatus().equalsIgnoreCase("A"); 
    }
    
	   public boolean getExpressPayAllowed() {
        int EP_PRODUCT_VARIANT = 1;
        ProfileItem item = getProductProfileOption("PRODUCT_VARIANTS",EP_PRODUCT_VARIANT,BILL_PAYMENT_DOC_SEQ) ;


        if ((null != item ) && (item.stringValue().equalsIgnoreCase("Y"))) {
            return true;
        } else {
            return false;
        }
        
    }
	   public boolean getUtilityBillPayAllowed() {
        int UTILITY_PRODUCT_VARIANT = 2;
       
        ProfileItem item = getProductProfileOption("PRODUCT_VARIANTS",UTILITY_PRODUCT_VARIANT,BILL_PAYMENT_DOC_SEQ) ;

        if ((null != item ) && (item.stringValue().equalsIgnoreCase("Y"))) {
            return true;
        } else {
            return false;
        }
        
    }
	   public boolean getPrePayAllowed() {
        int PREPAY_PRODUCT_VARIANT = 3;
        ProfileItem item = getProductProfileOption("PRODUCT_VARIANTS",PREPAY_PRODUCT_VARIANT,BILL_PAYMENT_DOC_SEQ) ;
        if ((null != item ) && (item.stringValue().equalsIgnoreCase("Y"))) {
            return true;
        } else {
            return false;
        }
        
    }
	   public ProfileItem getProductProfileOption(String tag, int prodVariant,int documentSequence ) {
        ProfileItem item = null;
        Profile productProfile = getProductProfile(documentSequence);
        try {
            item = productProfile.find(tag, prodVariant);
        }
        catch (NoSuchElementException e) {       	
//           Debug.println("No Value for profile tag : " + tag + " on document : " + documentSequence);	
        }
        return item;
    }

	   public ProfileItem getProductProfileOption(String tag, int documentSequence ) {
	        ProfileItem item = null;
	        Profile productProfile = getProductProfile(documentSequence);
	        try {
	            item = productProfile.find(tag);
	        }
	        catch (NoSuchElementException e) {       	
//	           Debug.println("No Value for profile tag : " + tag + " on document : " + documentSequence);	
	        }
	        return item;
	    }

	   
	   private Profile getProductProfile(int documentSequence) {
        Profile productProfile = null;
        try {
            productProfile = (Profile) UnitProfile.getInstance().getProfileInstance().find(
                                              "PRODUCT", documentSequence);	
        }
        catch (NoSuchElementException e) {
            Debug.println("Could not find product profile for doc seq : " +	
                                       documentSequence);
        }
        return productProfile;
    }
	   public boolean getStandardReceiveEnabled() {
	        ProfileItem item = getProductProfileOption("STANDARD_RECEIVE_ENABLED", MONEY_GRAM_RECEIVE_DOC_SEQ) ;
	        if ((null != item ) && (item.stringValue().equalsIgnoreCase("Y"))) {
	            return true;
	        } else {
	            return false;
	        }
	        
	    }


}
