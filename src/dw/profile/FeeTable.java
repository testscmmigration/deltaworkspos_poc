package dw.profile;

import java.math.BigDecimal;

public class FeeTable extends Profile {
	private static final long serialVersionUID = 1L;

    /*
     * For convenience, the fee table is indexed from 1 to 10.
     * The extra 0th element will only be set if a fee table item
     * is malformed.
     */
    public static final int MAX_BREAKPOINTS = 10;
    
    int lastIndex = 0;
    BigDecimal[] breakpoints;
    BigDecimal[] fees;
    
    public FeeTable(String tag) {
	super(tag);
	breakpoints = new BigDecimal[MAX_BREAKPOINTS + 1];
	fees = new BigDecimal[MAX_BREAKPOINTS + 1];
    }

    void setBreakpoint(int i, BigDecimal bp) {
	breakpoints[i] = bp;
	if (lastIndex < i) lastIndex = i;
    }

    void setFee(int i, BigDecimal f) {
	fees[i] = f;
	if (lastIndex < i) lastIndex = i;
    }

    public BigDecimal getFee(BigDecimal amount) {
	for (int i = 1; i <= lastIndex; i++) {
	    if (amount.compareTo(breakpoints[i]) <= 0)
		return fees[i];
	}
	return BigDecimal.ZERO;
    }
    
}
