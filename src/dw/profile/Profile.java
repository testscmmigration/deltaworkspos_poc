/*
 * Profile.java
 * 
 * $Revision$
 *
 * Copyright (c) 1999-2004 MoneyGram International
 */

package dw.profile;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import dw.model.adapters.CountryInfo;

/**
 * A profile is a collection of profile items. Profiles can be
 * nested, so a profile is itself a profile item.
 * @author Geoffrey Atkin
 */
public class Profile extends ProfileItem {
	private static final long serialVersionUID = 1L;

	/**
	 * Contains profile items contained within this profile.
	 */
	List<ProfileItem> children;

	/**
	 * Construct an empty profile.
	 */
	public Profile(String element) {
		super(element);
		children = new ArrayList<ProfileItem>(10);
	}

	/**
	 * Create a new profile which is a clone of this one.
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		Profile result = (Profile) super.clone();
		result.children = new ArrayList<ProfileItem>(children.size());
		Iterator<ProfileItem> i = this.iterator();
		while (i.hasNext()) {
			result.add((ProfileItem) (i.next()).clone());
		}
		return result;
	}

	/**
	 * Add a profile item to this profile.
	 */
	@Override
	public void add(ProfileItem item) {
		children.add(item);
		item.setParent(this);
	}

	/**
	 * Remove a profile item from this profile. RLC : New method needed to
	 *   remove user profiles in demo mode.
	 */
	public void remove(ProfileItem item) {
		children.remove(item);
	}

	/**
	 Sets the changed flag of this profile item.  Overrides the 
	 setChanged() message in ProfileIteam.  Profile object do not
	 have modes associated with them. 
	 */
	//    public void setChanged() {
	//	if (parent != null) {
	//    parent.setChanged();
	//}
	//}
	/**
	 * For Profile objects, this method will return true when any of its
	 *   child ProfileItem objects' isChanged() methods return true;
	 */
	@Override
	public boolean isChanged() {
		if (changed)
			return true;
		for (Iterator<ProfileItem> it = children.iterator(); it.hasNext();) {
			ProfileItem child = it.next();
			if (child.isChanged())
				return true;
		}
		return false;
	}

	/**
	 * Returns true if this is a user profile containing the given username
	 * and password. If the given username is null, it is ignored for
	 * comparison purposes.
	 */
	@Override
	public boolean matches(String username, String password) {
		ProfileItem item;

		if (!element.equalsIgnoreCase("USER")) 
			return false;
		try {
		    
			item = null;
			
			if (username != null) {
				item = this.find("NAME"); 

				if (!item.stringValue().equalsIgnoreCase(username))
					return false;
			}
			

			item = this.find("PASSWORD"); 
			return item.stringValue().equalsIgnoreCase(password);
		} catch (NoSuchElementException e) {
			return false;
		}
	}
	@Override
	public boolean matchesUserName(String username) {
	    ProfileItem item;

		if (!element.equalsIgnoreCase("USER")) 
			return false;
		try {
		    
			item = null;
			
			if (username != null) {
				item = this.find("NAME"); 

				if (item.stringValue().equalsIgnoreCase(username))
					return true;
				else return false;
			}
			

		} catch (NoSuchElementException e) {
			return false;
		}
		return false;
        }
	/** 
	 * Sets the value of a profile item to the value given, or inserts 
	 * the given item if it does not already exist.
	 * @author Geoff Atkin
	 */
	public void insertOrUpdate(String tag, ProfileItem anItem) {
		ProfileItem newItem;
		try {
			newItem = this.find(tag);
			newItem.setValue(anItem.stringValue()); //update
		} catch (NoSuchElementException e) {
			try {
				newItem = (ProfileItem) anItem.clone();
				newItem.setTag(tag);
				this.add(newItem); //insert
			} catch (CloneNotSupportedException e2) {
			}
		}
	}

	/**
	 * Find and return a profile item with the given tag.
	 * @param tag A String representing the tag attribute value to search
	 * on.
	 * @throws NoSuchElementException if no match is found
	 */
	public ProfileItem find(String tag) throws NoSuchElementException {
		Iterator<ProfileItem> i = children.iterator();
		ProfileItem item;

		while (i.hasNext()) {
			item = i.next();
			if (item.matches(tag)) {
				return item;
			}
		}
		throw new NoSuchElementException("No matching profile item : " + tag); 
	}
	
	/**
	 * Find and return the bigDecimal value of a profile item with the given tag.
	 * @param tag A String representing the tag attribute value to search
	 * on.
	 */
	public BigDecimal findDecimal(String tag) throws NoSuchElementException {
		Iterator<ProfileItem> i = children.iterator();
		ProfileItem item;

		while (i.hasNext()) {
			item = i.next();
			if (item.matches(tag)) {
				return item.bigDecimalValue();
			}
		}
		return BigDecimal.ZERO; 
	}
	
	/**
	 * Find and return the int value of a profile item with the given tag.
	 * @param tag A String representing the tag attribute value to search
	 * on.
	 */
	public int findInt(String tag) throws NoSuchElementException {
		Iterator<ProfileItem> i = children.iterator();
		ProfileItem item;

		while (i.hasNext()) {
			item = i.next();
			if (item.matches(tag)) {
				return item.intValue();
			}
		}
		return 0; 
	}
	
	/**
	 * Find and return the String value of a profile item with the given tag.
	 * @param tag A String representing the tag attribute value to search
	 * on.
	 */
	public String findString(String tag) throws NoSuchElementException {
		Iterator<ProfileItem> i = children.iterator();
		ProfileItem item;

		while (i.hasNext()) {
			item = i.next();
			if (item.matches(tag)) {
				return item.stringValue();
			}
		}
		return null; 
	}
	
	/**
	 * Find and return a profile item list with the given tag.
	 * @param tag A String representing the tag attribute value to search
	 * on.
	 * @throws NoSuchElementException if no match is found
	 */
	public List<String> findList(String tag)throws NoSuchElementException {
		Iterator<ProfileItem> i = children.iterator();
		List<String> list = new ArrayList<String>();
		ProfileItem item;
   try{
		while (i.hasNext()) {
			item = i.next();
			if (item.matches(tag)) {
				String value = item.stringValue();
				/*
				 * Don't bother adding it to the list, if it is empty
				 */
				if ((value != null) && (value.length() > 0)) {
					list.add(value);
				}
			}
		}
		return list;
   }catch(NoSuchElementException e){
		throw new NoSuchElementException("No matching profile item : " + tag); 
   }
	}
	/**
	 * Find and return a profile item with the given tag and id.
	 * @throws NoSuchElementException if no match is found
	 */
	public ProfileItem find(String tag, int id) throws NoSuchElementException {
		Iterator<ProfileItem> i = children.iterator();
		ProfileItem item;

		while (i.hasNext()) {
			item = i.next();
			if (item.matches(tag, id)) {
				return item;
			}
		}
		throw new NoSuchElementException("No matching profile item : " 
				+ tag + "," + id); 
	}

	
	/**
	 * Find and return a profile item with the given tag and currency Code.
	 * @throws NoSuchElementException if no match is found
	 */
	public ProfileItem findCurrencyValue(String tag, String currencyCode )
			throws NoSuchElementException {
		Iterator<ProfileItem> i = children.iterator();
		ProfileItem item = null;
		String currency = currencyCode.trim();
		
		if (currency.isEmpty()) {
			currency = CountryInfo.getAgentCountry().getBaseReceiveCurrency();
		}

		while (i.hasNext()) {
			item = i.next();
			if (item.curr != null) {
				if (item.curr.equalsIgnoreCase(currency)
						&& item.tag.equalsIgnoreCase(tag.trim()))

					return item;
			} else if (item.matches(tag)) {
				return item;
			}

		}

		throw new NoSuchElementException("No matching profile item : " 
				+ tag + "," + currencyCode); 
	}

	/**
	 * Find and return a BigDecimal value for the profile item with the given 
	 * tag and currency Code. Returns 0 if no match is found
	 */
	public BigDecimal findCurrencyDecimalValue(String tag, String currencyCode ){
		Iterator<ProfileItem> i = children.iterator();
		ProfileItem item = null;
		String currency = currencyCode.trim();
		
		if (currency.isEmpty()) {
			currency = CountryInfo.getAgentCountry().getBaseReceiveCurrency();
		}

		while (i.hasNext()) {
			item = i.next();
			if (item.curr != null) {
				if (item.curr.equalsIgnoreCase(currency)
						&& item.tag.equalsIgnoreCase(tag.trim()))

					return item.bigDecimalValue();
			} else if (item.matches(tag)) {
				return item.bigDecimalValue();
			}

		}
		return BigDecimal.ZERO;
	}

	/**
	 * Find and return a user profile with the given name and password.
	 * (If username is null, item will match on password.)
	 * If there is a matching profile but its status flag is D (for 
	 * deleted) then it doesn't count as a match.
	 * @throws NoSuchElementException if no match is found
	 */
	public Profile find(String username, String password)
			throws NoSuchElementException {
		Iterator<ProfileItem> i = children.iterator();
		ProfileItem item;

		while (i.hasNext()) {
			item = i.next();
			if (item.matches(username, password)) {
				if ("A".equals(item.getStatus())) { 
					return (Profile) item;
				}
			}
		}
		throw new NoSuchElementException("No matching user profile : " 
				+ username + "," + password); 
	}
	
	public Profile findName(String username) throws NoSuchElementException {
      Iterator<ProfileItem> i = children.iterator();
            ProfileItem item;

           while (i.hasNext()) {
	            item = i.next();
	           if (item.matchesUserName(username)) {
		           if ("A".equals(item.getStatus())) { 
			       return (Profile) item;
		 }
	 }
   }
          throw new NoSuchElementException("No matching user profile : " 
		+ username ); 
        }
	
	
	/**
	 * Finds an applicable profile item regarding additional text that should
	 * be printed at the bottom of a reciept. If there are no applicable
	 * profile items, an exception is thrown.
	 * @param country The country for which a receipt rule is being sought.
	 * @param deliveryOption The delivery option for which a receipt rule is being sought
	 * @throws NoSuchElementException if no match is found
	 */
	public ProfileItem receiptRule(String country, String deliveryOption)
			throws NoSuchElementException {
		Iterator<ProfileItem> i = children.iterator();
		ProfileItem item;
		// Fix for a null default value for delivery option when
		// Amount Tendered profile option is set to false
		// This should be fixed at the source.
		if (deliveryOption == null) {
			deliveryOption = "0"; 
		}

		while (i.hasNext()) {
			item = i.next();
			if (item
					.matches(
							"RECEIPT_RULE_MSG", "DEST", country, "DEL_OPT", deliveryOption)) {   
				return ((Profile) item).find("MSG"); 
			}
		}
		throw new NoSuchElementException(
				"No matching profile item: " + country + "." + deliveryOption);  
	}

	/**
	 * Determines whether a profile item fits two criteria. A field matches if its
	 * contents are the same as the corresponding parameter or if the field is
	 * blank.
	 * @param profileName The name of the profile containing other items.
	 * @param field1 The name of the tag to match within profileName
	 * @param matchText1 The value to match for field1
	 * @param field2 The name of the second tag to match within profileName
	 * @param matchText2 The value to match for field2
	 * @return True if the matching tags are found. False otherwise.
	 */
	@Override
	public boolean matches(String profileName, String field1,
			String matchText1, String field2, String matchText2) {
		ProfileItem item;

		if (!element.equalsIgnoreCase(profileName)) {
			return false;
		}

		try {
			item = null;

			if (matchText1 != null) {
				item = this.find(field1);

				if (!item.stringValue().equalsIgnoreCase(matchText1)
						&& !item.stringValue().equals("")) { 
					return false;
				}
			}
			if (matchText2 != null) {
				item = this.find(field2);
				return item.stringValue().equalsIgnoreCase(matchText2)
						|| item.stringValue().equals(""); 
			} else {
				return false;
			}
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	/**
	 * Obtains an Iterator object for iterating the Profiles children.
	 * @return A java.util.Iterator object.
	 */
	public Iterator<ProfileItem> iterator() {
		return children.iterator();
	}

	/**
	 * Returns the number of chidren ProfileItem object this Profile contains.
	 * @return A integer.
	 */
	public int getNumberOfChildren() {
		return children.size();
	}

	/** 
	 * Merge profile items from the given profile into this one.
	 * This is used to carry mode P items from the current profile over
	 * into one that has just been downloaded. Each item from oldProfile
	 * is added to this profile unless this profile already specifies
	 * a value for that item. 
	 * 
	 * The intent is to allow profile items to be introduced as mode P 
	 * items first, and then become real, middleware-supported items 
	 * later. When that transition happens, the value specified by the
	 * middleware takes precedence over the value specified in the local
	 * mode P item.
	 * 
	 * Finally, old mode P items that are not used by any currently-
	 * supported version of DeltaWorks are not carried over, to clean
	 * up the profile and avoid confusing situations (like people thinking
	 * USE_OUR_PAPER was being sent from the middleware when it wasn't.)
	 * This also gets rid of middleware-side mode P items which were once
	 * sent to us by mistake. (Otherwise we carry over every bit of junk
	 * the middleware ever sent us.)
	 * 
	 * @param oldProfile Profile data from the current profile to be
	 * merged into this profile.
	 */
	public void merge(Profile oldProfile) {

		if (!element.equalsIgnoreCase(oldProfile.element))
			return;

		Iterator<ProfileItem> i = oldProfile.iterator();
		while (i.hasNext()) {
			ProfileItem newpi = i.next();
			if (newpi instanceof Profile) {
				Profile newp = (Profile) newpi;
				Iterator<ProfileItem> j = this.iterator();
				while (j.hasNext()) {
					ProfileItem child = j.next();
					if (child instanceof Profile) {
						Profile p = (Profile) child;
						if (p.element.equalsIgnoreCase(newp.element)) {
							if (p.getID() != 0 && p.getID() == newp.getID())
								p.merge(newp);
						}
					}
				}
			} else {
				try {
					// before adding the profile item, see if it exist.
					if (newpi.getID() == 0)
						this.find(newpi.getTag());
					else
						this.find(newpi.getTag(), newpi.getID());
				} catch (NoSuchElementException e1) {
					try {
						// If the profile item did not exist, then add it.
						// But don't carry along obsolete profile items.
						if (!isObsolete(newpi.getTag())) {
							this.add((ProfileItem) newpi.clone());
						}
					} catch (CloneNotSupportedException e2) {
					}
				}
			}
		}
	}

	/** 
	 * Returns true if this profile item is an old POS mode P item that we
	 * haven't used since before version 4.0, or is an RTS middleware mode P
	 * item that we're holding because it was sent to us by mistake.
	 * 
	 * At the time of this writing, the only mode P items that return false
	 * are APPLICATION_MODE, MODEM_SPEAKER_MUTED, and RECEIPT_RULE_XML, but 
	 * additional items could be added in later versions, so this method only 
	 * returns true for items positively known to be obsolete. 
	 */
	private boolean isObsolete(String tag) {
		String list[] = {
		// shouldn't have been sent from middleware:
				"PASSWORD_FAILURES", "REQUIRE_GISU", "TOKEN_LIFETIME",
				"ALLOW_REVERSE_INDICATIVE_QUOTES", "USE_OUR_PAPER",
				"CLIENT_API_VERSION", "157", "POS_HAS_CURRENT_PROFILE",
				"APPLY_IMMEDIATELY", "PASSWORD_RETRIES",

				// no longer used:
				"PROFILE_EXCHANGE_MAGIC", "QA_HOST_IP_1", "QA_HOST_IP_2",
				"QA_HOST_NETWORK_URL", "QA_HOST_DIALUP_URL",
				"QA_PRIMARY_DUN_PASSWORD", "QA_PRIMARY_DUN_USER_ID",
				"QA_SECONDARY_DUN_PASSWORD", "QA_SECONDARY_DUN_USER_ID",
				"QA_HOST_PRIMARY_PHONE", "QA_HOST_SECONDARY_PHONE",
				"TRANSCEIVER_DTD",

				// referenced by dead code:
				"FTP_ADDRESS_NETWORK", "FTP_ADDRESS_DIALUP", "FTP_USER",
				"FTP_PASSWORD" };

		for (int i = 0; i < list.length; i++) {
			if (list[i].equals(tag)) {
				return true;
			}
		}
		//Debug.println("isObsolete(\"" + tag + "\") = false");
		return false;
	}

	/** 
	 * Looks up a profile item using a path syntax. Finds a child item that
	 * matches the path up to the first slash or end of string, then calls 
	 * get recursively. Uses find(String, int) if the initial path
	 * component contains square brackets, otherwise uses find(String).
	 */
	@Override
	public ProfileItem get(String path) {
		int slash, next, bracket1, bracket2, id;
		ProfileItem found;
		if (path == null)
			return null;

		if (path.length() == 0)
			return this;

		slash = path.indexOf('/');
		next = slash + 1;
		if (slash == -1) {
			slash = path.length();
			next = slash;
		}

		bracket1 = path.indexOf('[');
		bracket2 = path.indexOf(']');

		if (bracket2 < bracket1)
			bracket1 = -1;

		try {
			if ((bracket1 == -1) || (bracket1 > slash)) {
				found = this.find(path.substring(0, slash));
			} else {
				id = Integer.parseInt(path.substring(bracket1 + 1, bracket2));
				found = this.find(path.substring(0, bracket1), id);
			}
			return found.get(path.substring(next));
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	/** 
	 * Looks up a profile item using a path syntax. Finds a child item that
	 * matches the path up to the first slash or end of string, then calls 
	 * get recursively. Uses find(String, int) if the initial path
	 * component contains square brackets, otherwise uses find(String).
	 */
	public ProfileItem getCurr(String path, String curr) {
		int slash, next, bracket1, bracket2, id;
		ProfileItem found;
		if (path == null)
			return null;

		if (path.length() == 0)
			return this;

		slash = path.indexOf('/');
		next = slash + 1;
		if (slash == -1) {
			slash = path.length();
			next = slash;
		}

		bracket1 = path.indexOf('[');
		bracket2 = path.indexOf(']');

		if (bracket2 < bracket1)
			bracket1 = -1;

		try {
			if ((bracket1 == -1) || (bracket1 > slash)) {
				found = this.findCurrencyValue(path.substring(0, slash), curr);
			} else {
				id = Integer.parseInt(path.substring(bracket1 + 1, bracket2));
				found = this.find(path.substring(0, bracket1), id);
			}
			/*
			 * If no more slashes, assume we have our item, otherwise keep on 
			 * going.
			 */
			if (slash == next) {
				return found.get(path.substring(next));
			}
			else {
				return ((Profile)found).getCurr(path.substring(next), curr);
			}
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	/**
	 *	Constructs a stringified representation of the object.  Useful for
	 *  debugging purposes.
	 *  @return A String representing the state of the object for debugging
	 *  purposes.
	 */
	@Override
	public String dumpObject() {
		StringBuffer buf = new StringBuffer();
		buf.append("------------- Profile instance -------------\n\n"); 
		buf.append(super.dumpObject());
		Iterator<ProfileItem> iterator = children.iterator();

		while (iterator.hasNext()) {
			ProfileItem item = iterator.next();
			buf.append(item.dumpObject());
		}
		buf.append("\n--------- End of Profile instance ---------\n\n\n"); 
		return buf.toString();
	}

	/** Returns a string containing an XML representation. */
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		if (element.equalsIgnoreCase("PROFILE")) 
			result.append("<?xml version=\"1.0\"?>\n"); 

		result.append("<").append(element); 
		if (tag != null)
			result.append(" tag=\"").append(tag).append("\"");  
		if (mode != null)
			result.append(" mode=\"").append(mode).append("\"");  
		if (curr != null)
			result.append(" curr=\"").append(mode).append("\"");
		if (id != 0)
			result.append(" id=\"").append(id).append("\"");  
		if ((status != null) && (!status.equals(""))) 
			result.append(" status=\"").append(status).append("\"");  
		if (isChanged())
			result.append(" changed=\"true\"");  
		result.append(">\n"); 
		Iterator<ProfileItem> i = children.iterator();
		while (i.hasNext()) {
			result.append(i.next().toString());
		}
		result.append("</").append(element).append(">\n");  
		return result.toString();
	}

	/**
	 *	Sorts the ProfileItems contained within the profile.
	 */
	public void sortChildren() {
		Collections.sort(children, new ProfileItemComparator());
	}

	/**
	 *	An implementation of the Comparator that returns an unequal value only
	 * when a ReceiptProfile is involved. ReceiptProfiles with no country are
	 * considered larger than those with. ReceiptProfiles with no delivery option
	 * are considered larger than those with. ReceiptProfiles in general are
	 * considered larger than any other ProfileItem. When two ReceiptProfiles
	 * both contain destination and delivery option, they are considered equal.
	 */
	private class ProfileItemComparator implements Comparator<ProfileItem> {
		@Override
		public int compare(ProfileItem pi1, ProfileItem pi2) {
			int returnVal = 0;
			if (pi1 instanceof ReceiptProfile) {
				ReceiptProfile rp1 = (ReceiptProfile) pi1;
				if (pi2 instanceof ReceiptProfile) {
					ReceiptProfile rp2 = (ReceiptProfile) pi2;
					if (rp1.find("DEST").stringValue().equals("")) {  
						returnVal = 1;
					} else if (rp2.find("DEST").stringValue().equals("") ||  
							rp2.find("DEL_OPT").stringValue().equals("")) {  
						returnVal = -1;
					} else if (rp1.find("DEL_OPT").stringValue().equals("")) {  
						returnVal = 1;
					} else {
						returnVal = 0;
					}
				} else {
					returnVal = 1;
				}
			} else if (pi2 instanceof ReceiptProfile) {
				returnVal = -1;
			} else {
				returnVal = 0;
			}
			return returnVal;
		}

//		public boolean equals(Object obj) {
//			return this.equals(obj);
//		}
	}

	/**
	 * Update the subagent's connectivity settings with superagent's settings.
	 * Do not send these items to the host on a profile exchange.
	 */
	public void mergeSuperagentProfileItems(Profile oldProfile) {
		ProfileItem item;
		if (!element.equalsIgnoreCase(oldProfile.element))
			return;

		Iterator<ProfileItem> i = oldProfile.iterator();
		while (i.hasNext()) {
			ProfileItem newpi = i.next();
			try {
				item = UnitProfile.getInstance().find(newpi.getTag());
				item.setValue(newpi.stringValue());
				item.setChanged(false);
			} catch (NoSuchElementException e) {
				try {
					item = (ProfileItem) newpi.clone();
					item.setTag(tag);
					this.add(item);
					item.setChanged(false); //insert
				} catch (CloneNotSupportedException e2) {
				}
			}
		}
	}
	public static boolean isSixCharPinType(){
	    if("6CHAR".equalsIgnoreCase(UnitProfile.getInstance().get("USER_PIN_TYPE","3DIGIT")))
	        return true;
	        else return false;
	    
	}
}

