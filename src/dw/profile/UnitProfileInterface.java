package dw.profile;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import com.moneygram.agentconnect.SubagentInfo;

import dw.io.CircularFile;
import dw.io.State;
import dw.io.diag.DiagMessage;
import dw.io.info.DiagLogDWTotalsInfo;
import dw.utility.Total;

public interface UnitProfileInterface {

//	public static final int NORMAL_MODE = 0;
	public final int NORMAL_MODE = 0;

//	public static final int DEMO_MODE = 1;
	public final int DEMO_MODE = 1;
	
	public static final long MFA_DEAUTHORIZATION = 2L;
	
	public static final String MFA_FLAG = "MFA_FLAG";
	public static final String MFA_TOKEN = "MFA_TOKEN";
	
	public static final String SEND_REVERSAL_FLAG = "SEND_REVERSAL_FLAG";

	public static final String BROADCAST_MESSAGE_UPDATE_INTERVAL = "BROADCAST_MESSAGE_UPDATE_INTERVAL";
	public static final String ACCOUNT_DEPOSIT_TABLE_UPDATE_INTERVAL = "ACCOUNT_DEPOSIT_TABLE_UPDATE_INTERVAL";

	//public static final int QA_MODE = 2;

	/** For dev use only, called by main startup when undocumented command-line arg is given. */
	public void disableProxy();

	/**
	 * Loads the profile from the given circular file.
	 */
	public Profile load(CircularFile cf) throws IOException;

	/** Equivalent to getInstance.find(String). */
	public ProfileItem find(String tag) throws NoSuchElementException;

	/** Get the current period. */
	public byte getCurrentPeriod();

    public void setNeedReceiptTemplates(boolean pending);
    
    public boolean needReceiptTemplates();
    
    public void setNeedStateRegInfo(boolean pending);
    
    public boolean needStateRegInfo();
    
	public void setSoftwareVersionPending(boolean pending);

	public boolean isSoftwareVersionPending();

	public boolean isSoftwareVersionIdChanged();

	public String getDispenserId();

	public void setDispenserId(String id);

	/** Sets the service tag. */
	public void setServiceTag(String tag);


	/**
	 * Indicates if we're in new demo mode.
	 * @return true if we're in new demo mode.
	 */
	public boolean isNewDemoMode();
	
	public void setNewDemoMode(boolean b);
	
	/**
	 * Indicates if we're in demo mode.
	 * @return true if we're in demo mode.
	 */
	public boolean isDemoMode();

	/**
	 * Returns false because QA mode doesn't exist any more.
	 */
	public boolean isQaMode();

	/**
	 * Set the version number of the application that we are running. 
	 * This should only be called once, by main panel at startup.
	 * Stores the version locally for future comparison with pending
	 * downloads. If different from what's in profile, store it in the
	 * profile so the middleware will be informed what version we're
	 * running. (New in 2.9: avoid profile exchange if unchanged.)
	 * (New in 2.9: make sure saved version is set from parameter, 
	 * not profile, otherwise software downloads don't work right.)
	 */
	public void setVersionNumber(String version);

	public int compareVersions();

	/**
	 * Determine if we are running a different version than what is stored in
	 *   the profile. Return an int based on if we have the same, newer or
	 *   older version. Modified by Geoff to return 0 if there is an integer
	 *   parse error.
	 * @return 0 = same version, 1 = newer version, -1 = older version
	 */
	public int compareVersions(String currentVersion);

	/**
	 * Return the version number of the application. If withDecimal is true,
	 *   return it with the implied decimal inserted, otherwise, just return
	 *   the version as its integer String.
	 * Note : the middleware stores the profile item with leading zeros, so
	 *   strip them off when returning the version.
	 */
	public String getVersionNumber(boolean withDecimal);

	/** Returns true if we're in dialup mode. */
	public boolean isDialUpMode();

	/** Returns true if we're in superagent or subagent mode. */
	public boolean isMultiAgentMode();

	/** Returns true if we're in training mode. */
	public boolean isTrainingMode();

	/** Sets the training mode flag to the given value. */
	public void setTrainingMode(boolean mode);

	public void setDialUpMode(boolean isDialUp, boolean recordAsChanged);

	/** Sets the dialup flag to the given value. */
	public void setDialUpMode(boolean isDialUp);

	/** Sets the muted flag. */
	public void setModemSpeakerMuted(boolean isMuted);

	/** Returns true if modem speaker is muted. */
	public boolean isModemSpeakerMuted();

	/** Returns UnitProfile.getUPInstance().NORMAL_MODE or UnitProfile.getUPInstance().DEMO_MODE. */
	public int getAppMode();

	/** Set the application mode to NORMAL_MODE or DEMO_MODE. */
	public void setAppMode(int mode);

	/**
	 * Determine if this POS device is in an authorized state.
	 * If called before the unit profile is loaded, return true because
	 * as far as we know, we are. 
	 */
	public boolean isPosAuthorized();
	
	/**
	 * Determine if this POS device is in an deactivated state.
	 * If called before the unit profile is loaded, return true because
	 * as far as we know, we are. 
	 */
	public boolean isPosDeactivated();
	

	/**
	 * Returns the instance of Profile previously created with the load
	 * method.
	 */
	public Profile getProfileInstance();

	/**
	 * Indicate that a profile change has been signaled by the middleware
	 */
	public void setProfileChanged();

	/**
	 * Determine if a profile change has been signaled by the middleware
	 */
	public boolean isProfileChanged();

	/** Sets the state saver. */
	public void setStateSaver(State ss);

	/**
	 * Returns the value of a profile item using path syntax. This is a 
	 * more convenient way to retrieve profile values than the methods
	 * currently used. The value is returned as a string, and if there 
	 * is no profile item with the given path, the specified default 
	 * value is returned instead. This method should not be called
	 * before the unit profile is loaded, except in unit test code.
	 * @author Geoff Atkin
	 */
	public String get(String path, String defaultValue);

	/**
	 * Returns the decimal value of a profile item using path syntax. 
	 * See get(String, String) for details.
	 * @author Geoff Atkin
	 */
	public BigDecimal get(String path, BigDecimal defaultValue);

	/**
	 * Returns the integer value of a profile item using path syntax. 
	 * See get(String, String) for details.
	 * @author Geoff Atkin
	 */
	public int get(String path, int defaultValue);

	/** 
	 * Sets the string value of a profile item using path syntax.
	 * If the profile item does not exist, it is created.
	 * If the new value is different than the existing value, the 
	 * profile item is marked changed and the profile is saved.
	 * 
	 * If the profile has not been loaded yet, this method silently
	 * does nothing.
	 * 
	 * This method currently only works for normal top-level profile 
	 * items; in other words the full path syntax is not supported.
	 * 
	 * @throws IllegalArgumentException if path is not a top-level item. 
	 */
	public void set(String path, String value);

	/** 
	 * Sets the string value of a profile item using path syntax.
	 * If the profile item does not exist, it is created with the specified 
	 * mode. If the new value is different than the existing value, the 
	 * profile item is marked changed but the profile is not saved unless 
	 * saveChanges is true.
	 * 
	 * To save changes, call saveChanges() or saveChangesQuietly().
	 * 
	 * If the profile has not been loaded yet, this method silently
	 * does nothing.
	 * 
	 * This method currently only works for normal top-level profile 
	 * items; in other words the full path syntax is not supported.
	 * 
	 * @throws IllegalArgumentException if path is not a top-level item. 
	 */
	public void set(String path, String value, String mode, boolean saveChanges);

	/** 
	 * Returns the generic store name. Whoever is calling this could just call 
	 * get("GENERIC_DOCUMENT_STORE", "") instead.
	 */
	public String getGenericDocumentStoreName();

	/**
	 * Returns the user ID for the given user name, or 0 if not found.
	 * Warning: This method may return a recently-deleted user
	 * (one where the status=D). Use find(String, String) to do a 
	 * login routine. 
	 * This method is being used to record failed login attempts.
	 * I'm not sure it actually works even for that purpose, but nobody
	 * is complaining.
	 */
	public int getUserID(final String userName);

	/**
	 Sets the initial UnitProfile to the XML string parameter.  
	 @param profileString A String representing the UnitProfile in XML
	 markup.
	 */
	public void setInitialProfile(String profileString);

	/**
	 * gets the profileItems of mode P.
	 * 
	 * @return profile
	 */
	public Profile getProfileModeP();

	/**
	 * Replace the current profile with a new one, and save it.
	 */
	public void setNewProfile(String profileString);

	/**
	 * Replace the current profile with a new one, but only save
	 * it if the saveChanges parameter is true. If it is false,
	 * the caller should call changesApplied() and saveChanges()
	 * after it is done making changes to the profile. 
	 * 
	 * Most callers should set saveChanges to true. Setting it to
	 * false improves efficiency for callers that will be making 
	 * extensive changes to the unit profile after this call.
	 */
	public void setNewProfile(String profileString, boolean saveChanges);

	/** 
	 * Indicates that new values have been assigned. Recomputes
	 * anything that needs recomputing as a result. 
	 */
	public void changesApplied();

	/**
	 * Get the starting serial number.
	 * @deprecated this method only handles one dispenser.
	 */
	public long getStartingSerialNumber();

	/**
	 * Set the starting serial number.
	 * @deprecated this method only handles one dispenser, and
	 * the Dispenser class now handles this on its own.
	 */
	public void setStartingSerialNumber(final long startingSerialNumber);

	/**
	 * Get the ending serial number.
	 * @deprecated this method only handles one dispenser.
	 */
	public long getEndingSerialNumber();

	/**
	 * Set the ending serial number.
	 * @deprecated this method only handles one dispenser, and
	 * the Dispenser class now handles this on its own.
	 */
	public void setEndingSerialNumber(final long endingSerialNumber);

	/** Set the current period. */
	public void setCurrentPeriod(final byte period);

	/** 
	 * Get the last check in time that was sent via CheckInClientMsg.
	 * If state file hasn't been connected to this class yet, assume we're
	 * in unit testing code and return 0.
	 */
	public Date getLastCheckInTime();

	/** Set the last check in time that was sent via CheckInClientMsg */
	public void setLastCheckInTime(final Date lastCheckInTime);

	/** Set the last check in time that was sent via CheckInClientMsg */
	public void setLastCheckInTime(final long lastCheckInTime);

	/** Records that a successful daily transmit has taken place. 
	 * For AWOL, all that matters is that we completed a daily
	 * transmit. We may yet be deauthorized, just as with any
	 * transmission, but we're not AWOL any more.
	 * @param completed the date and time of the trasmit.
	 */
	public void dailyTransmitSucceeded(Date completed);

	// Set the last reauthorization time
	public void setLastReauthTime(final Date lastReauthTime);

	/** 
	 * Checks if days between reauthorization exceeds allowable
	 * limit (based on days AWOL in profile)
	 */
	public boolean isAWOLTooLong();

	// Get the last daily transmit time
	public Date getLastDailyTransmitTime();

	// Set the last daily transmit time
	public void setLastDailyTransmitTime(final Date lastDailyTransmitTime);

	// Get the last attempted daily transmit time
	public Date getLastAttemptedDailyTransmitTime();

	// Set the last attempted daily transmit time
	public void setLastAttemptedDailyTransmitTime(
			final Date lastAttemptedDailyTransmitTime);

	// Get the last dispenser check time
	public Date getLastDispenserCheckTime();

	// Set the last dispenser check time
	public void setLastDispenserCheckTime(final Date lastDispenserCheckTime);

	/** Gets the last daily total reset time. */
	public Date getLastDailyTotalResetTime();

	/** Sets the last daily total reset time. */
	public void setLastDailyTotalResetTime(final Date lastDailyTotalResetTime);
	
	/** Gets the last daily Bill Pay total reset time. */
	public Date getLastDailyBillPayTotalResetTime();
	/** Sets the last daily total reset time. */
	public void setLastBillPayDailyTotalResetTime(final Date lastDailyTotalResetTime);
	
	
	/** 
	 * Gets the unit profile ID for use in outgoing AC messages. 
	 * In the normal case, returns the value of PROFILE_ID from the 
	 * current profile. In multi-agent mode (subagent mode), returns
	 * the saved superagent unit profile ID from the state file. If
	 * the saved ID from the state file could not be read, returns 0.  
	 */
	public int getUnitProfileID();
	
	public String getChannelType();
	
	public String getPoeType();
	
	public String getTargetAudience();

	/** 
	 * Saves the current value of PROFILE_ID to the state file so
	 * that a superagent can revert back to the original profile 
	 * after loading a subagent profile. If aleady in subagent mode,
	 * do nothing.
	 */
	public void saveSuperagentUnitProfileID();

	/**
	 * Save local changes made to unit profile. 
	 */
	public void saveChanges() throws IOException;

	/** 
	 * Save local changes to unit profile without throwing exceptions.
	 * Converts profile to XML and writes it to the profile circular file.
	 */
	public void saveChangesQuietly();

	/** 
	 * Check to see if any user accounts are using this pin. If so, mark them
	 * expired so the owners of those accounts will have to change their pins.
	 * @return true if the pin is available, false if the pin is in use.
	 */
	public boolean validatePin(String pin);

	public static final int DAILY_TOTAL = 0;
	public static final int DAILY_BILL_PAY_TOTAL = 50;

	public static final int BATCH_TOTAL = 1;

	public static final int SUMMARY_FEE_TOTAL = 11;

	public static final int SUMMARY_TOTAL = 12;

	public static final int GRAND_TOTAL = 25;

	public static final int NUMBER_OF_TOTALS = 55;
	
	public static final int DAILY_COUNT = 30;

	/** Retreives one of the totals. */
	public Total getTotal(int index);

	/** Retreives one of the grand totals. Application ID is zero-based,
	 * while document type is one-based. */
	public Total getTotal(int app, int doc);

	/** Get the daily total */
	public BigDecimal getDailyTotal();	

	/** Increments the daily total */
	public void addDailyTotal(BigDecimal amount);

	/** Clears the daily total. */
	public void clearDailyTotal();
	
	/** Get the daily Bill Payment total */
	public BigDecimal getDailyBillPayTotal();	

	/** Increments the daily Bill Payment total */
	public void addDailyBillPayTotal(BigDecimal amount);

	/** Clears the daily Bill Payment total. */
	public void clearDailyBillPayTotal();

	/** Get the daily total */
	public int getDailyCount(int docType);

	/** Increments the daily total */
	public void addDailyCount(int count, int docType);

	/** Clears the daily total. */
	public void clearDailyCount();
	
	/** Gets the batch total for a document type. */
	public BigDecimal getBatchTotal(int document);

	/** Increments the batch total for given document type. */
	public BigDecimal addBatchTotal(BigDecimal amount, int document);

	/** Clears the batch total for the given document type. */
	public void clearBatchTotal(int document);

	/** Add this item count to the batch count for the given type. */
	public int addBatchCount(int count, int document);

	/** Gets the batch count. */
	public int getBatchCount(int document);

	/** Clears the batch count. */
	public void clearBatchCount(int document);

	/** Clears the batch totals and counts for all document types */
	public void clearBatchTotalsAndCounts();

	/**
	 * Get the transmission count for a particular document type. 1 for
	 *   Money Order, 2 for Vendor Payment.
	 * @return the number of items where transmission of items will be attempted
	 */
	public int getTransmissionCount(int docSeq);

	/**
	 * Get the transmission amount for a particular document type. 1 for
	 *   Money Order, 2 for Vendor Payment.
	 * @return the amount where transmission of items will be attempted
	 */
	public BigDecimal getTransmissionAmount(int docSeq);

	/** Increments the summary count for the given document type. */
	public int addDiagBatchCount(int count, int document);

	/** Increments the summary total for the given document type. */
	public BigDecimal addDiagBatchAmount(BigDecimal amount, int document);

	/** Increments the summary fee total. */
	public BigDecimal addDiagBatchFee(BigDecimal amount);

	/** Clears the summary counts and totals. This is called after a transmit */
	public void clearDiagBatchCountsAndTotals();

	/** Gets the grand total for the given application ID and document type.
	 * @param app the application ID, see DiagLogDWTotalsInfo DW_ID_APP_...
	 * @param document the document type, see DiagLogDWTotalsInfo DW_ID_TOTALS_...
	 */
	public BigDecimal getGrandTotal(int app, int document);

	/** Increments the grand total for the given application ID and document type.
	 * @param amount the amount to add
	 * @param app the application ID, see DiagLogDWTotalsInfo DW_ID_APP_...
	 * @param document the document type, see DiagLogDWTotalsInfo DW_ID_TOTALS_...
	 */
	public BigDecimal addGrandTotal(BigDecimal amount, int app, int document);

	/** Clears the grand total for the given application ID and document type.
	 * @param app the application ID, see DiagLogDWTotalsInfo DW_ID_APP_...
	 * @param document the document type, see DiagLogDWTotalsInfo DW_ID_TOTALS_...
	 */
	public void clearGrandTotal(int app, int document);

	/** Gets the grand count for the given applicatin ID and document type.
	 * @param app the application ID, see DiagLogDWTotalsInfo DW_ID_APP_...
	 * @param document the document type, see DiagLogDWTotalsInfo DW_ID_TOTALS_...
	 */
	public int getGrandCount(int app, int document);

	/** Increment the grand total count for the given application and document type.
	 * @param count the number of items to add
	 * @param app the application ID, see DiagLogDWTotalsInfo DW_ID_APP_...
	 * @param document the document type, see DiagLogDWTotalsInfo DW_ID_TOTALS_...
	 */
	public int addGrandCount(int count, int app, int document);

	/** Clears the grand count for the given application ID and document type.
	 * @param app the application ID, see DiagLogDWTotalsInfo DW_ID_APP_...
	 * @param document the document type, see DiagLogDWTotalsInfo DW_ID_TOTALS_...
	 */
	public void clearGrandCount(int app, int document);

	public final int DNET_ID_APP_DSPN = DiagMessage.DNET_ID_APP_DSPN;
	public final int DNET_ID_APP_MGRAM = DiagMessage.DNET_ID_APP_MGRAM;
	public final int DNET_ID_APP_CARD = DiagMessage.DNET_ID_APP_CARD;
	
	public final int DW_ID_TOTALS_DSPN_VOID = DiagLogDWTotalsInfo.DW_ID_TOTALS_DSPN_VOID;
	public final int DW_ID_TOTALS_MGRAM_RECV = DiagLogDWTotalsInfo.DW_ID_TOTALS_MGRAM_RECV;
	public final int DW_ID_TOTALS_MGRAM_SEND = DiagLogDWTotalsInfo.DW_ID_TOTALS_MGRAM_SEND;
	public final int DW_ID_TOTALS_MGRAM_XPAY = DiagLogDWTotalsInfo.DW_ID_TOTALS_MGRAM_XPAY;
	public final int DW_ID_TOTALS_CARD_PURCHASE = DiagLogDWTotalsInfo.DW_ID_TOTALS_CARD_PURCHASE;
	public final int DW_ID_TOTALS_CARD_LOAD = DiagLogDWTotalsInfo.DW_ID_TOTALS_CARD_LOAD;
	public final int DW_TOTALS_PRODUCTS = DiagLogDWTotalsInfo.DW_TOTALS_PRODUCTS;
	
	
	/** Adds a total and count to the daily and grand totals and counts */
	public void addTotalAndCount(BigDecimal amount, int count, int application,
			int docType, boolean voided);
	
	public void addBillPayTotal(BigDecimal amount, int count, int application,
			int docType, boolean voided);

	public String getBusinessStartTime(int day);

	public String getBusinessEndTime(int day);

	public String getBusinessEndTime();

	public String getBusinessStartTime();

	/**
	 * Determine whether or not the current time is within this agent's business
	 *   hours. This assumes the format of HH:MM:SS,HH:MM:SS in 24-hour format.
	 */
	public boolean isWithinBusinessHours();

	/**
	 * Register a ProfileChangedListener with the profile change event
	 * notification mechanism.
	 * @param l A ProfileChangedListener implementation.
	 * @author Christopher Bartling
	 */
	public void addProfileChangedListener(final ProfileChangedListener l); // END_METHOD

	/**
	 * Deregister a ProfileChangedListener with the profile change event 
	 * notification mechanism.
	 * @param l A ProfileChangedListener implementation.
	 * @author Christopher Bartling
	 */
	public void removeProfileChangedListener(final ProfileChangedListener l); // END_METHOD

	/**
	 * Fire a ProfileChangeEvent, notifying all listeners for the particular
	 * keyed profile item.  
	 * @param evt A ProfileChangedEvent object.
	 * @author Christopher Bartling
	 */
	public void fireProfileChanged(final ProfileChangedEvent evt);

	/**
	 * Saves the new language value in the unit profile so DW starts up
	 * in the same language we just switched to. Called from main menu
	 * screen when the user selects the toggle option.
	 */
	public void setLanguage(String language);

	public void setAltLanguage(String language);

	/**
	 * compare the LANGUAGE and ALT_LANGUAGE profile items. 
	 * Atleast one of them has to be English.
	 * If they are equal set the ALT_LANGUAGE to English("en").
	 * if both items are now "en" set the ALT_LANGUAGE to spanish ("es").
	 *  @return String altLanguage.
	 */
	public String getAlternateLanguage();

	/** 
	 * Copies proxy profile values into system properties.
	 */
	public void updateProxyProperties();

	public boolean isSubAgentTransactionAllowed();

	/**
	 * Make changes to subagent's profile before saving changes. 
	 * Set DUAL_CONTROL=N, GLOBAL_NAME_PIN_REQ=N and SUBAGENT_MODE=Y.
	 * NAME_PIN_REQUIRED is set to N in clientTransaction.
	 */
	public void updateSubAgentProfileItems();

	public boolean isSubAgent();
    
	public boolean isSuperAgent();
    
	/** 
	 * Returns true if transaction not allowed, due to a condition that 
	 * might be corrected by performing a transmit. 
	 */
	public boolean checkTransmitConditions();

	public boolean isTakeOverSubagentprofile();
	
	public void setTakeOverSubagentprofile(boolean takeOverSubagentprofile);
	public void setAgentInfo(SubagentInfo  info);
	public SubagentInfo getAgentInfo();
	public  int getSubAgentTakeOverUserID();
	public  void setSubAgentTakeOverUserID(int subAgentTakeOverUserID);
	public boolean isProfileUseOurPaper();
	public void setForceUpgrade(boolean forceUprade);
	public boolean isForceUpgrade();
	
	public boolean isMfaEnabled();
	public String getMfaTokenId();
	
	public int getBroadcastMessageUpdateInterval();
	public List<String> getAllowedDeleiveryOptions();
	public int getAccountDepositTableUpdateInterval();
	
	public void initEpsonLineCompression();
	public boolean useEpsonLineCompression();
	public void setEpsonLineCompression(boolean useLineCompression);
	
	public void setForceTransmit(boolean doForceTransmit);
	public boolean isForceTransmit();
	
	public boolean isSendReversalEnabled();

	public String getAgentCountryIsoCode();
}