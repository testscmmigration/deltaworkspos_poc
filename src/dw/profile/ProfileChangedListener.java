package dw.profile;

/**
 * Abstract listener class for profile item value changes.
 * @author Christopher Bartling
 */
public abstract class ProfileChangedListener {
    /**
     * Key value used for listener map within UnitProfile.getUPInstance().
     */
    private String listenerKey;
    
    public ProfileChangedListener(final ProfileItem item) {
        super();
        listenerKey = buildKey(item);
    } // END_CONSTRUCTOR
    
    /**
     * Recursively builds a Map key for use in mapping distinct listener 
     * lists in the event notification system.
     * @param item A ProfileItem object.
     * @author Christopher Bartling
     */
    final static String buildKey(final ProfileItem item) {
        final StringBuffer buf = new StringBuffer();
        if (item.getParent() != null) {
            // Not at the root yet. Traverse one level up.
            buf.append("." + buildKey(item.getParent()));	
        }
        else {
            // BASE CASE: Hit the root, so get it's tag.
            buf.append(item.getTag());
        }
        // Return the portion of the generated key
        return buf.toString();
    } // END_METHOD
    
    /** 
     * Callback message for notifying the subscriber/listener of profile change
     * events.
     * @param evt A ProfileChangedEvent object.
     * @author Christopher Bartling
     */
    abstract public void profileChanged(ProfileChangedEvent evt);
    
    /**
     * Returns the listener key for use in mapping the listener list in the 
     * profile value changed event notification system.
     * @return A String representing the listener key.
     * @author Christopher Bartling
     */
    final public String getListenerKey() {
        return listenerKey;
    } // END_METHOD

} // END_CLASS
