package dw.profile;

/**
 * Abstract listener class for profile item value changes.
 * @author Christopher Bartling
 */
final public class ProfileChangedEvent {
    /**
     * Key value used for listener map within UnitProfile.getUPInstance().
     */
    private String listenerKey;
    
    public ProfileChangedEvent(final ProfileItem item) {
        super();
        listenerKey = ProfileChangedListener.buildKey(item);
    } // END_CONSTRUCTOR
    
    /**
     * Returns the listener key for use in mapping the listener list in the 
     * profile value changed event notification system.
     * @return A String representing the listener key.
     * @author Christopher Bartling
     */
    final public String getListenerKey() {
        return listenerKey;
    } // END_METHOD

} // END_CLASS


