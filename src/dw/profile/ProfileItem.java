/*
 * ProfileItem.java
 * 
 * $Revision$
 *
 * Copyright (c) 2000-2004 MoneyGram, Inc. All rights reserved
 */

package dw.profile;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Locale;

import dw.NonObfuscatable;
import dw.utility.Debug;
import dw.utility.TimeUtility;

/**
 * Profile Item. Represent a discrete profile setting.
 * @author Geoffrey Atkin
 */
public class ProfileItem implements NonObfuscatable, Cloneable, Serializable{
	private static final long serialVersionUID = 1L;

	/**
     * The element for this profile item. For most things
     * in the profile, element is "ITEM".	
     */
    public String element;

    /**
     * The name of this profile item. When element is "ITEM",	
     * tag is the field name. When this item is actually
     * a profile of more items, tag is usually null.
     */
    String tag;

    /**
     * The value of this profile item.
     */
    StringBuffer value;

    /**
     * The profile which contains this profile item.
     */
    Profile parent;

    /**
     * The ID number of this profile item, if any.
     */
    public int id = 0;

    /**
     * Indicates the changability of this profile item.
     */
    String mode;
    
    /**
     * Indicates the changability of this profile item.
     */
    String curr;

    /**
     * Indicates the status of this profile item.
     */
    String status;
    /**
     * Indicates the whether password field is masked or not of this profile item.
     */
    String passwordMask;
    /**
      * Indicates whether this profile item has been changed locally.
      */
    boolean changed;

    /**
     * Construct a profile item with the given tag and an empty value.
     */
    public ProfileItem(String element) {
	this.element = element;
	// The tag value may be overridden if an attribute exists for it.
	// Chris, why does it make sense to set tag = element?
	// this.tag = element;
	value = new StringBuffer();
    }

    /**
     * Construct a profile item with the given element, name, mode, and value.
     */
//    public ProfileItem(String element, String tag, String mode, String value) {
//    	this(element, tag, mode, value, null);
//    }
    
    /**
     * Construct a profile item with the given element, name, mode, and value.
     */
    public ProfileItem(String element, String tag, String mode, String value, String curr) {
        this.element = element;
        this.tag = tag;
        this.mode = mode;
        this.curr = curr;
        this.value = new StringBuffer(value);
    }
    
    /**
     * Create a new profile item which is a clone of the given one.
     * The parent reference of the returned object is set to null, 
     * to disassociate it from this profile tree. Callers are 
     * generally expected to add the returned object into the copy
     * of the profile data structure they are creating.
     */
    @Override
	public Object clone() throws CloneNotSupportedException {
        ProfileItem result = (ProfileItem) super.clone();
        result.value = new StringBuffer(value.toString());
	result.parent = null;
        return result;
    }

    /**
     * Factory method to create an appropriate profile item for
     * the given tag.
     */
    public static ProfileItem create(String element) {
	if (element.equalsIgnoreCase("PROFILE")) {	
	    return new Profile(element);
	}
	else if (element.equalsIgnoreCase("DISPENSER")) {	
	    return new Profile(element);
	}
	else if (element.equalsIgnoreCase("PAYEE_TABLE")) {	
	    return new Profile(element);
	}
	else if (element.equalsIgnoreCase("PRODUCT")) {	
	    return new Profile(element);
	}
	else if (element.equalsIgnoreCase("FEE_TABLE")) {	
	    return new FeeTable(element);
	}
	else if (element.equalsIgnoreCase("FEE")) {	
	    return new FeeItem(element);
	}
	else if (element.equalsIgnoreCase("USER")) {	
	    return new Profile(element);
	}
	else if (element.equalsIgnoreCase("RECEIPT_RULE_MSG")) {	
	    return new ReceiptProfile(element);
	}
	else {
	    return new ProfileItem(element);
	}
    }
    
    /**
     * Does nothing.
     */    
    void add(ProfileItem item) {
    }

    /**
     * Appends a subset of a character array to the value of this
     * profile item.
     */
    void append(char buf [], int offset, int len) {
	value.append(buf, offset, len);
    }

    /**
     * Returns true if this item matches the given tag.
     */
    public boolean matches(String query_tag) {
	if (tag == null) {
	    return element.equalsIgnoreCase(query_tag);
	}
	else {
	    return tag.equalsIgnoreCase(query_tag);
	}
    }
    
    /**
     * Returns true if this item matches the given tag and id.
     */
    public boolean matches(String query_tag, int query_id) {
	if (tag == null) {
	    return (id == query_id) && element.equalsIgnoreCase(query_tag);
	}
	else {
    	    return (id == query_id) && tag.equalsIgnoreCase(query_tag);
	}
    }

    /**
     * Will be overridden in Profile. This version just returns false.
     */
    public boolean matches(String username, String password) {
	return false;
    }
    public boolean matchesUserName(String username) {
    	return false;
        }
    
    /**
     * Will be overridden in Profile. This version just returns false.
     */
    public boolean matches(String profile, 
                           String field1, String value1,
                           String field2, String value2) {
    return false;
    }

    /**
        Sets the parent Profile for this ProfileItem.
        @param p A Profile object.
     */
    public void setParent(Profile p) {
	parent = p;
    }
	
    /**
    	Retrieves the parent Profile for this ProfileItem object.
    	@return A Profile object which contains this ProfileItem as 
    	its child.
     */
    public Profile getParent() {
	return parent;
    }

    
    /**
     * Sets the status of this ProfileItem.
     * @param status A String representing the status attribute.
     */
    public void setStatus(String status) {
        if (status == null)
            this.status = "";	
        else
            this.status = status;
    }

    /**
     * Gets the status for this ProfileItem.
     * @return A String representing the status attribute for this ProfileItem.
     */
    public String getStatus() {
      	return this.status;
    }
    /**
     * Sets the password mask of this ProfileItem.
     * @param status A String representing the passwordMask attribute.
     */
    public void setPasswordMask(String mask) {
        if (mask == null)
            this.passwordMask = "";	
        else
            this.passwordMask = mask;
    }

    /**
     * Gets the status for this ProfileItem.
     * @return A String representing the status attribute for this ProfileItem.
     */
    public String getPasswordMask() {
      	return this.passwordMask;
    }

    /**
     * Sets the id for this ProfileItem.
     * @param id A String representing the id attribute for this ProfileItem.
     */
    public void setID(String id) {
	if (id == null)
	    this.id = 0;
	else
	    this.id = Integer.parseInt(id);
    }

    /** Used by Simple PM when creating a new user. */
    public void setID(int id) {
        this.id = id;
    }
    
    /**
     * Gets the id for this ProfileItem.
     * @return An integer representing the id attribute for this ProfileItem.
     */
    public int getID() {
	return this.id;
    }

    
    /**
     * Gets the tag for the ProfileItem. Added by Chris Bartling on 
     * 
     * return A String representing the tag attribute for this ProfileItem.
     */
    public String getTag() {
   	if (tag != null) return this.tag;
   	else return this.element;
    }

    
    /**
     * Sets the tag for the ProfileItem.
     * @param tag A String representing the tag attribute for this ProfileItem.
     */
    public void setTag(String tag) {
        if (tag == null) return;

        if (tag.equalsIgnoreCase("CTR_LIMIT")) 	
            this.tag = "HDV_LIMIT";	
        else if (tag.equalsIgnoreCase("CTR_OVERRIDE"))	
            this.tag = "HDV_OVERRIDE";	
        else
            this.tag = tag;
    }


    /**
     * Sets the mode for the ProfileItem.
     * @param mode A String representing the mode attribute for 
     * this ProfileItem.
     */
    public void setMode(String mode) {
	this.mode = mode;
    }

    /**
     * Gets the mode for this ProfileItem.
     * @return A String representing the mode attribute for this ProfileItem.
     */
    public String getMode() {
	return this.mode;
    }

    /**
     * Sets the mode for the ProfileItem.
     * @param mode A String representing the mode attribute for 
     * this ProfileItem.
     */
    public void setCurr(String curr) {
	this.curr = curr;
    }

    /**
     * Gets the mode for this ProfileItem.
     * @return A String representing the mode attribute for this ProfileItem.
     */
    public String getCurr() {
	return this.curr;
    }
    
    /** Will be overriden in ProfileFeeItem. This version does nothing.
     */
    void setBreakpoint(String bp) {
    }

    /** Will be overriden in ProfileFeeItem. This version does nothing.
     */
    void setFee(String fee) {
    }

    /**
      * Sets the changed flag of this profile item. This flag indicates we
      * have a pending change to send the middleware, and does not apply
      * to mode P items.
      */
    public void setChanged() {
        if (! "P".equals(mode))
            changed = true;
    }

    /**
     * Sets the changed flag of this profile item.  Added this method so
     * redundant profile changes are not recorded and sent to the middleware.
     * @param newChanged A boolean.
     * @author Christopher Bartling
     */
    public void setChanged(boolean newChanged) {
/*    	if ( mode != null && mode.equalsIgnoreCase("P") ) {	
    	    return;
    	}
    	else { */
    	    changed = newChanged;
//    	}
    } // END-METHOD

    /** Sets the changed flag of this profile item. Intended for use by
      * ProfileLoader. Unlike the other two versions of setChanged, sets
      * the change flag even if this item is mode P. Any non-null value
      * is true.
      */
    public void setChanged(String changed) {
        if (changed != null) {
            this.changed = true;
        }
    }

    /**
        Determines whether the object's state has changed.
        @return A boolean representing whether the object's state has changed.
      */
    public boolean isChanged() {
	return changed;
    }

    /**
     * Sets the value of this profile item.
     */
    public void setValue(boolean b) {
    	value.setLength(1);
    	value.setCharAt(0, (b ? 'Y' : 'N'));
    	this.setChanged();
    }
    
    /**
     * Sets the value of this profile item.
     */
    public void setValue(int i) {
    	value.setLength(0);
    	value.append(i);
    	this.setChanged();
    }

    /**
     * Sets the value of this profile item.
     */
    public void setValue(BigDecimal bd) {
    	value.setLength(0);
    	value.append(bd.toString());
    	this.setChanged();
    }

    /**
     * Sets the value of this profile item.
     */
    public void setValue(String s) {
        // Create new buffer to avoid leak if old value was longer.
        // (The other variations should be all right because the string
        // representation of numbers and booleans is never very long.)
        value = new StringBuffer(s);
        this.setChanged();
    }
    
    /** 
      * Returns the value of this profile item as a boolean.
      */
    public boolean booleanValue() {
	if ((value.length() == 1) &&
		((value.charAt(0) == 'Y') || (value.charAt(0) == 'y'))) {
	    return true;
	}
	else {
	    return false;
	}
    }

    /**
      * Returns the value of this profile item as an int.
      */
    public int intValue() {
	try {
	    return Integer.parseInt(value.toString(), 10);
	}
	catch (NumberFormatException e) {
	    return 0;
	}
    }

    /**
      * Returns the value of this profile item as a BigDecimal.
      */
    public BigDecimal bigDecimalValue() {
	try {
	    return new BigDecimal(value.toString());
	}
	catch (NumberFormatException e) {
	    return BigDecimal.ZERO;
	}		
    }

    /**
      * Returns the value of this profile item as a string.
      */
    public String stringValue() {
	return value.toString();
    }

    /**
     * Returns the length of the string value. Used by simple profile maint,
     * because StringBuffer.length()==0 is more efficient than 
     * StringBuffer.toString().equals("").	
     */
    public int stringLength() {
        return value.length();
    }

    /**
      * Returns the value of this profile item as a long milliseconds.
      */
    public long timeOfDayValue() {
        String timeString = value.toString();
        return TimeUtility.timeOfDayMillis(timeString);
    }

    /** 
     * Looks up a profile item using a path syntax. Provides access
     * to profile item via a call like get("PRODUCT[1]/DOCUMENT_NAME").	
     * Mostly implemented in subclasses, the implementation here is the
     * degenerate case. Returns <code>this</code> if path is non-null but 
     * empty. Returns null otherwise.
     * @author Geoff Atkin
     */
    public ProfileItem get(String path) {
        if ((path == null) || (path.length() != 0))
           return null;
        
        return this;
    }

    /**
     *	Constructs a stringified representation of the object.  Useful for
     *  debugging purposes.
     *  @return A String representing the state of the object for debugging
     *  purposes.
     *  @author Chris Bartling
     */
    public String dumpObject() {
	StringBuffer buf = new StringBuffer();
	buf.append("------------- ProfileItem instance -------------\n"); 
	buf.append("id: [" + id + "]\n");  
	buf.append("element: [" + element + "]\n");  
	buf.append("tag: [" + tag + "]\n");  
	buf.append("value: [" + value.toString() + "]\n");  
	buf.append("mode: [" + mode + "]\n");  
	buf.append("----------- End of ProfileItem instance --------\n"); 
	return buf.toString();
    }

    /**
     * Cleans all illegal XML characters from a string and returns a 
     * string buffer containing the cleaned characters.
     * @param s The String to be cleaned.
     * @return XML-safe characters.
     */
    public static StringBuffer sanitize(CharSequence s) {
        StringBuffer returnBuffer = new StringBuffer(s.length());
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '&') {
                returnBuffer.append("&amp;"); 
            }
            else if (c == '<') {
                returnBuffer.append("&lt;"); 
            }
            else if (c == '>') {
                returnBuffer.append("&gt;"); 
            }
            else if (c > 127) {
                returnBuffer.append("&#x"); 
                returnBuffer.append(Integer.toHexString(c).toUpperCase(Locale.US));
                returnBuffer.append(";");	
            }
            else {
                returnBuffer.append(c);
            }
        }
        return returnBuffer;
    }
    
    
    /** Returns the value with markup characters replaced by entities. */
    public StringBuffer sanitizedValue() {
        return sanitize(value);
    }
    
    /** Produces a string containing an XML representation.*/
    @Override
	public String toString() {
	StringBuffer result = new StringBuffer();
	result.append("<").append(element);	
        if (tag != null) result.append(" tag=\"").append(tag).append("\"");  
	if (mode != null) result.append(" mode=\"").append(mode).append("\"");  
	if (curr != null) result.append(" curr=\"").append(curr).append("\"");  
	if (id != 0) result.append(" id=\"").append(id).append("\"");  
	if ((status != null) && (! status.equals(""))) 	
	    result.append(" status=\"").append(status).append("\"");  
        if ((! "P".equals(mode)) && changed) result.append(" changed=\"true\"");  
	result.append(">").append(this.sanitizedValue()).append("</").append(element).append(">\n");   
	return result.toString();
    }

    public static void main(String args[]) {
        try {
            ProfileItem p1 = new ProfileItem("ITEM", "USER", "S", "hello", null);    
            ProfileItem p2 = (ProfileItem) p1.clone();
            p1.setValue(1);
            Debug.println(p2.toString());
            Debug.println("value == value ? " + (p1.value == p2.value));	
        }
        catch (Exception e) {
            Debug.println(e.toString());
        }
    }
}
