/*
   ClientInfo.java

   Copyright 2000 Moneygram

   4/5/2000 ESS initial version
   4/11/2000 GEA additions and cosmetic changes
   4/14/2000 JMA additions
   5/04/2000 JMA added mgAccountNumber
   5/15/2000 jma toString()
   5/30/2000 jma toString() tab replaced with 2 spaces--too wide for Diag dump.
*/

package dw.profile;

import java.util.HashMap;

import dw.model.adapters.CountryInfo;
import dw.model.adapters.CountryInfo.Country;
import dw.utility.StringUtility;

/**
  * Information about the client machine.
  *
  * @author Erik Stienmetz
  */
public class ClientInfo implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	private String profileID;
    private String accountNumber;
    private String agentID;
    private String columbusAgentID;
    private String agentName;
    private String agentAddress1;
    private String agentAddress2;
    private String agentAddress3;
    private String agentCity;
    private String agentState;
    private String agentZip;
    private String agentPhone;
    private String mgAccountNumber;
    private String unitNumber;
    private String posDeviceType;    
    private String taxID;
    private String accountingStartDay;
    private String accountingStartTime;
    private Country agentCountry;

    /**
      * Creates a ClientInfo by looking up information in the static Unit Profile.
      */
    public ClientInfo() {
        this(UnitProfile.getInstance().getProfileInstance());
    }

    public Country getAgentCountry() {
        return agentCountry;
    }
    
    /** Creates a ClientInfo given by looking up the information 
        in a UnitProfile.getUPInstance().  If the fields are not found, they are 
        left as null in the object.
        @param aUnitProfile The unit profile from which to gather information.
    */
    public ClientInfo( Profile aUnitProfile) {

        try { profileID = aUnitProfile.find("PROFILE_ID").stringValue(); }	
        catch( Exception e) { profileID = ""; }	
        
        try { accountNumber = aUnitProfile.find("MO_ACCOUNT").stringValue(); }	
        catch( Exception e) { accountNumber = ""; }	

        try { mgAccountNumber = aUnitProfile.find("MG_ACCOUNT").stringValue(); }	
        catch( Exception e) { mgAccountNumber = ""; }	

        try { agentID = aUnitProfile.find("MO_AGENT_ID").stringValue(); }	
        catch( Exception e) { agentID = ""; }	

        try { columbusAgentID = aUnitProfile.find("AGENT_ID").stringValue(); }	
        catch( Exception e) { columbusAgentID = ""; }	
        
        try { agentName = aUnitProfile.find("AGENT_NAME").stringValue(); }	
        catch( Exception e) { agentName = ""; }	

        try { agentAddress1 = aUnitProfile.find("AGENT_ADDRESS_1").stringValue(); } 
        catch( Exception e) { agentAddress1 = ""; }	

        try { agentAddress2 = aUnitProfile.find("AGENT_ADDRESS_2").stringValue(); } 
        catch( Exception e) { agentAddress2 = ""; }	

        try { agentAddress3 = aUnitProfile.find("AGENT_ADDRESS_3").stringValue(); } 
        catch( Exception e) { agentAddress3 = ""; }	

        try { agentCity = aUnitProfile.find("AGENT_CITY").stringValue(); }	
        catch( Exception e) { agentCity = ""; }	

        try { agentState = aUnitProfile.find("AGENT_STATE").stringValue(); }	
        catch( Exception e) { agentState = ""; }	

        try { agentZip = aUnitProfile.find("AGENT_ZIP").stringValue(); }	
        catch( Exception e) { agentZip = ""; }	

        try { agentPhone = aUnitProfile.find("AGENT_PHONE").stringValue(); }	
        catch( Exception e) { agentPhone = ""; }	


        try { unitNumber = aUnitProfile.find("DEVICE_ID").stringValue(); }	
        catch( Exception e) { unitNumber = ""; }	

        try { posDeviceType = aUnitProfile.find("DEVICE_TYPE").stringValue(); }	
        catch( Exception e) { posDeviceType = ""; }	

        try { taxID = "";  }	
        catch( Exception e) { taxID = ""; }	

        try { accountingStartDay = aUnitProfile.find("ACCOUNTING_START_DAY").stringValue(); }	
        catch( Exception e) { accountingStartDay = ""; }	

        try { accountingStartTime = aUnitProfile.find("ACCOUNTING_START_TIME").stringValue(); }	
        catch( Exception e) { accountingStartTime = ""; }	

        try {
            String country = aUnitProfile.find("AGENT_COUNTRY_ISO_CODE").stringValue();	
            agentCountry = CountryInfo.getAgentCountry(country);
        }
        catch ( Exception e ) { agentCountry = null; }

    }

    /**
     * Return a friendly-format description of the contents of this message.
     */
    @Override
	public String toString() {
        return "ClientInfo {" +	
                "\n  profileID            : " + profileID +	
                "\n  accountNumber        : " + accountNumber +	
                "\n  agentID              : " + agentID +	
                "\n  columbusAgentID      : " + columbusAgentID +	
                "\n  agentName            : " + agentName +	
                "\n  agentAddress1        : " + agentAddress1 +	
                "\n  agentAddress2        : " + agentAddress2 +	
                "\n  agentAddress3        : " + agentAddress3 +	
                "\n  agentCity            : " + agentCity +	
                "\n  agentState           : " + agentState +	
                "\n  agentZip             : " + agentZip +	
                "\n  agentPhone           : " + agentPhone +	
                "\n  mgAccountNumber      : " + mgAccountNumber +	
                "\n  unitNumber           : " + unitNumber +	
                "\n  posDeviceType        : " + posDeviceType    +	
                "\n  taxID                : " + taxID +	
                "\n  accountingStartDay   : " + accountingStartDay +	
                "\n  accountingStartTime  : " + accountingStartTime +	
                "\n} ClientInfo ";	
    }

    /** Returns the profile ID, which often replaces unit number. */
    public String getProfileID() { return profileID; }

    /** Returns the TEMG-assigned account number.
        This number is assigned at the time the agent is set up.  
        It includes relationship, location, check number and 
        TECI assigned store number.
    */
    public String getAccountNumber() { return( accountNumber);}
        
    /** Returns the MoneyGram account number.  */
    public String getMGAccountNumber() { return( mgAccountNumber);}

    /**
      * @deprecated Geoff wants to know why you want this.
      */
    public void setAccountNumber(String accountNumber) { 
        this.accountNumber = accountNumber;}
    
    /** Returns the unit number.
        This is the TEMG-assigned unit number of the remote PC issuing 
        the MoneyOrder item.
    */
    public String getUnitNumber() { return( unitNumber);}

    /**
      * @deprecated Geoff wants to know why you want this.
      */
    public void setUnitNumber( String unitNumber) { this.unitNumber = unitNumber;}
    
    /** Returns the POS Device Type.
        This is 'W' for DeltaWorks.
    */
    public String getPOSDeviceType() { return( posDeviceType);}

    /**
      * @deprecated Geoff wants to know why you want this.
      */
    public void setPOSDeviceType( String posdt) { this.posDeviceType = posdt;}
        
    /** Returns the tax ID for a moneyorder or vendor payment, the MoneyGram reference
      *  number in the case of a DeltaGram.
      *        deprecated Geoff thinks this belongs someplace else.
      *             removed deprecation flag until an alternative is provided
      */
    public String getTaxID() { return( taxID);}

    /**
      * @deprecated Geoff wants to know why you want this.
      */
    public void setTaxID( String taxID) { this.taxID = taxID;}
    
    /** Returns the number of the first accounting day of the week as a string.
        Sunday is '1', Saturday is '7'.
    */
    public String getAccountingStartDay() { return( accountingStartDay);}

    /**
     * deprecated Geoff wants to know why you want this
     */
    public void setAccountingStartDay( String accountingStartDay) { 
        this.accountingStartDay = accountingStartDay;}
    
    /** Returns the hour and minute of the first accounting period of a day.
        This need to be in hh:mm format.
    */
    public String getAccountingStartTime() { return( accountingStartTime);}

    /**
     * deprecated Geoff wants to know why you want this. 
     */
    public void setAccountingStartTime( String accountingStartTime) { 
        this.accountingStartTime = accountingStartTime;}
    
    /** Returns agent id. */
    public String getAgentId() { return agentID; }

    /** Returns agent id. */
    public String getColumbusAgentId() { return columbusAgentID; }
    
    /** Returns agent name. */
    public String getAgentName() { return agentName; }

    /** Returns Agent Address1. */
    public String getAgentAddress1() { return agentAddress1; }

    /** Returns Agent Address2. */
    public String getAgentAddress2() { return agentAddress2; }

    /** Returns Agent Address3. */
    public String getAgentAddress3() { return agentAddress3; }

    /** Returns Agent City. */
    public String getAgentCity() { return agentCity; }

    /** Returns Agent State. */
    public String getAgentState() { return agentState; }

    /** Returns Agent Zip. */
    public String getAgentZip() { return agentZip; }

    /** Returns Agent Phone. */
    public String getAgentPhone() { return agentPhone; }
    
    public void addFieldValues(final HashMap<String, Object> fieldMap) {
        fieldMap.put("agentAddress1",getAgentAddress1  ()); 
        fieldMap.put("agentAddress2",getAgentAddress2  ()); 
        fieldMap.put("agentAddress3",getAgentAddress3  ()); 
        fieldMap.put("agentCity"    ,getAgentCity      ()); 
        fieldMap.put("agentState"   ,getAgentState     ()); 
        fieldMap.put("agentZip"     ,getAgentZip       ()); 
        fieldMap.put("agentPhone"   ,getAgentPhone     ()); 
        fieldMap.put("teAgentId"    ,getAgentId        ()); 
        fieldMap.put("mgAgentId"    ,getMGAccountNumber()); 
        fieldMap.put("colAgentId"   ,getColumbusAgentId()); 
        fieldMap.put("agentId",StringUtility.getNonNullString(getAgentId()) + 
                               StringUtility.getNonNullString(getUnitNumber()));
    }

}
