package dw.agentdirectory;

import java.awt.Component;
import java.awt.event.KeyEvent;

import dw.framework.FlowPage;
import dw.framework.KeyMapManager;
import dw.framework.PageExitListener;
import dw.framework.PageNameInterface;

public class AgentDirectoryWizard extends AgentDirectoryTransactionInterface {
	private static final long serialVersionUID = 1L;
	
	public AgentDirectoryWizard(AgentDirectoryTransaction transaction,
            PageNameInterface naming) {
        super("agentdirectory/DAWizardPanel.xml", transaction, naming, false); 
        createPages();
	}
	
	@Override
	public void createPages() {
		addPage(AgentDirectoryWizardPage1.class, transaction, "directoryRequest", "DAW05", buttons);
		addPage(AgentDirectoryWizardPage2.class, transaction, "directoryResponse", "DAW10", buttons);
	}
	
    /**
     * Add the single keystroke mappings to the buttons. Use the function keys
     *   for back, next, cancel, etc.
     */
    private void addKeyMappings() {
        KeyMapManager.mapMethod(this, KeyEvent.VK_F2, 0, "f2SAR"); 
        buttons.addKeyMapping("expert", KeyEvent.VK_F4, 0); 
        buttons.addKeyMapping("back", KeyEvent.VK_F11, 0); 
        buttons.addKeyMapping("next", KeyEvent.VK_F12, 0); 
        buttons.addKeyMapping("cancel", KeyEvent.VK_ESCAPE, 0); 
    }
    
    @Override
	public void start() {
        transaction.clear();
        super.start();
        addKeyMappings();
    }
    
    /**
     * Page traversal logic here.
     */
    @Override
	public void exit(int direction) {

        if (direction == PageExitListener.DONOTHING){
            return;
        }

        super.exit(direction);
    }

    public void f2SAR() {
        Component current = cardLayout.getVisiblePage();
        if (current instanceof FlowPage) {
            ((FlowPage) current).updateTransaction();
        }
        transaction.f2SAR();
    }

}

