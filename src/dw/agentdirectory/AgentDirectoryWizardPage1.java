package dw.agentdirectory;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JPanel;
import javax.swing.JTextField;

import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.dwgui.MultiListComboBox;
import dw.dwgui.StateListComboBox;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.main.MainPanelDelegate;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.CountryInfo.Country;
import dw.model.adapters.CountryInfo.CountrySubdivision;
import dw.utility.StringUtility;

/**
 * The main page for the directory of agents functionality.
 * @author Chris Bartling,
 */
public class AgentDirectoryWizardPage1 extends AgentDirectoryFlowPage
                                    implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	// component references
    private JTextField areaCodeField = null;
    private JTextField zipCodeField = null;
    private JTextField cityField = null;
    private StateListComboBox<CountrySubdivision> stateBox = null;
    private MultiListComboBox<Country> countryComboBox = null;
    private JPanel areaCodePanel = null;
    private JPanel zipCodePanel = null;
    private JPanel locationPanel = null;
    private JPanel countryPanel = null;

   public AgentDirectoryWizardPage1(AgentDirectoryTransaction tran, 
            String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "agentdirectory/DirectoryOfAgents1.xml", name, pageCode, buttons);	
        collectComponents();
        
		DWTextPane tp = (DWTextPane) getComponent("contentMessage");
		tp.addListener(this, this);
    }

	@Override
	public void dwTextPaneResized() {
		DWTextPane contentMessage = (DWTextPane) getComponent("contentMessage");
		DWTextPane wildcardMessage = (DWTextPane) getComponent("wildcardMessage");
		JPanel panel2 = (JPanel) getComponent("locationPanel");
		contentMessage.setWidth(AgentDirectoryWizardPage1.this.getPreferredSize().width);
		wildcardMessage.setWidth(panel2.getPreferredSize().width);
		contentMessage.setListenerEnabled(false);
		wildcardMessage.setListenerEnabled(false);
	}

    /**
     * Initialization call.  Called by PageFlowContainer.
     * @param direction Determine in which direction the user is navigating.
     */
    @Override
	public void start(int direction) {
        flowButtons.reset();
        flowButtons.setVisible("expert", false);	
        flowButtons.setEnabled("expert", false);
        flowButtons.setVisible("back", false);
        flowButtons.setEnabled("back", false);
        flowButtons.setVisible("next", true);
        flowButtons.setEnabled("next", false);
        flowButtons.setAlternateText("next",Messages.getString("DirectoryOfAgents.11"));    
        flowButtons.setAlternateKeyMapping("next", KeyEvent.VK_F1, 0);	
        flowButtons.setVisible("cancel", true);
        flowButtons.setEnabled("cancel", true);
        flowButtons.setAlternateText("cancel",Messages.getString("OK"));    
        setupListeners();
        areaCodeField.setText(transaction.getAreaCode());	
        zipCodeField.setText(transaction.getZip());	
        cityField.setText(transaction.getCity());	
        countryComboBox.setSelectedItem(transaction.getCountry());
        Country country = transaction.getCountry();
        if ((country != null) && (country.getCountryCode() != null) && (! country.getCountryCode().isEmpty())) {
        	countryComboBox.setSelectedComboBoxItem(transaction.getCountry().getCountryCode());
        } else {
        	countryComboBox.setSelectedIndex(0);
        }
        countryComboBox.requestFocus(true);
        setAreaCodeComponentsEnabled(true);
        setZipCodeComponentsEnabled(true);
        
    	if (direction == PageExitListener.NEXT) {
    		flowButtons.setEnabled("next", false);
    		transaction.clear();
    		country = (Country) countryComboBox.getSelectedItem();
        	updateStateList(country, stateBox);
        	if (MainPanelDelegate.getIsiNfCommand())
        		MainPanelDelegate.setIsiAllowDataCollection(true);
    	}
    	else{
    		setZipCodeComponentsEnabled(!(zipCodeField.getText().isEmpty()));
    		setAreaCodeComponentsEnabled(!(areaCodeField.getText().isEmpty()));
    		if (areaCodeField.getText().length() >0)
    			areaCodeField.requestFocus(true);
    		else
    			countryComboBox.requestFocus(true);
    		
    		if (zipCodeField.getText().length() > 0) {
    		    stateBox.removeAllItems();
    			stateBox.setEnabled(false);
    		} else {
        		Country c = transaction.getCountry();
        		CountrySubdivision cs = transaction.getState();
                populateStateBox(stateBox, c, cs);
    		}
    		
    		flowButtons.setEnabled("next", true);
    	}
    }

    /**
     * Just notify my listeners that I am done.
     */
    @Override
	public void finish(int direction) {
    	if (direction == PageExitListener.NEXT) {
    		Country country = (Country) countryComboBox.getSelectedItem();
    		transaction.setCountry(country);
    		transaction.setZip(zipCodeField.getText());
    		transaction.setCity(cityField.getText());
    		if (stateBox.isEnabled()) {
        		transaction.setState(((CountrySubdivision) stateBox.getSelectedItem()));
    		} else {
        		transaction.setState(null);
    		}
    		transaction.setAreaCode(areaCodeField.getText());
    		this.getAgents();
    	} else {
    		PageNotification.notifyExitListeners(this, direction);
    	}
    }

    private void collectComponents() {
    	// Obtain references to all the GUI components.
        areaCodeField = (JTextField) getComponent("areaCodeField");	
        zipCodeField = (JTextField) getComponent("zipCodeField");	
        cityField = (JTextField) getComponent("cityField");	
        stateBox = (StateListComboBox<CountrySubdivision>) getComponent("stateBox");	
		countryComboBox = (MultiListComboBox<Country>) getComponent("countryComboBox");	

		areaCodePanel = (JPanel) getComponent("areaCodePanel");	
		zipCodePanel = (JPanel) getComponent("zipCodePanel");	
		locationPanel = (JPanel) getComponent("locationPanel");	
        countryPanel = (JPanel) getComponent("countryPanel");
        countryComboBox.addList(CountryInfo.getCountriesPlusBlank(CountryInfo.OK_FOR_ADDRESS), "countries");       
        populateStateBox(stateBox, CountryInfo.getAgentCountry(), CountryInfo.getAgentCountrySubdivision());
    }

    private void setupListeners() {
        addKeyListener(new KeyAdapter() {
                @Override
				public void keyPressed(KeyEvent e) {
                    handleKeyPressedEvent(e);
                }
            });
        addActionListener("areaCodeField", this, "search");	
        addActionListener("zipCodeField", this, "search");	
        addActionListener("cityField", this, "search");	

        addTextListener("areaCodeField", this);	
        addTextListener("zipCodeField", this);	
        addTextListener("cityField", this);	

        // setup country / state action listener
        ActionListener countryChangeListener = new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent e) {
            	
            	boolean zipFieldUsed = zipCodeField.getText().length() > 0;
            	boolean cityFieldUsed = cityField.getText().length() > 0;
            	
                if (! zipFieldUsed) {
                	Country country = (Country) countryComboBox.getSelectedItem();
                	updateStateList(country, stateBox);
                } else {
                	stateBox.removeAllItems();
                }
                
                stateBox.setEnabled(!zipFieldUsed && stateBox.getItemCount() > 0);
                
                if (zipFieldUsed || cityFieldUsed) {
                	flowButtons.getButton("next").setEnabled(countryComboBox.getSelectedIndex() > 0);                 		
                }
            }};
        addActionListener("countryComboBox", countryChangeListener);	
    }

    public void search() {
        flowButtons.getButton("next").doClick();	
    }   

    /**
     * Notify listeners that the text has changed in the areaCodeField
     * component.
     */
    public void areaCodeFieldTextValueChanged() {
    	if (areaCodeField.getText().length() > 0){
            countryComboBox.setSelectedIndex(0);
            stateBox.setSelectedItem(transaction.getState());      
    	}
    	this.setCountryComponentsEnabled(areaCodeField.getText().isEmpty());
 
		setZipCodeComponentsEnabled(areaCodeField.getText().isEmpty());
		setLocationComponentsEnabled(areaCodeField.getText().isEmpty());
		flowButtons.getButton("next").setEnabled((areaCodeField.getText().length() > 5));
    }

    /**
     * Notify listeners that the text has changed in the areaCodeField
     * component.
     */
    public void zipCodeFieldTextValueChanged() {
		setAreaCodeComponentsEnabled(zipCodeField.getText().isEmpty());
		setLocationComponentsEnabled(zipCodeField.getText().isEmpty());
		if (zipCodeField.getText().isEmpty())
			//Fire action performed so we repopulate the state box with the right values.
			countryComboBox.setSelectedIndex(countryComboBox.getSelectedIndex());
		else
		    stateBox.removeAllItems();
		//boolean status = ((zipCodeField.getText().length() == 0) || (this.countryComboBox.getSelectedItem().toString().trim().length() == 0));
		boolean b1 = ! zipCodeField.getText().isEmpty();
		boolean b2 = countryComboBox.getSelectedIndex() > 0;
		flowButtons.getButton("next").setEnabled(b1 && b2);
    }

    /**
     * Notify listeners that the text has changed in the areaCodeField
     * component.
     */
    public void cityFieldTextValueChanged() {
		setAreaCodeComponentsEnabled(checkLocationEnableState());
		setZipCodeComponentsEnabled(checkLocationEnableState());

		String city = StringUtility.getNonNullString(cityField.getText()).trim();
		int countryIndex = countryComboBox.getSelectedIndex();
		boolean flag = ((! city.isEmpty())) && (countryIndex > 0);
		
		flowButtons.getButton("next").setEnabled(flag);
    }

    /**
     * Enable or diasable all the area code searching constraint components.
     */
    private void setAreaCodeComponentsEnabled(boolean enabled) {
        setComponentEnabled(areaCodePanel, enabled, true);
    }

    /**
     * Enable or diasable all the zip code searching constraint components.
     */
    private void setZipCodeComponentsEnabled(boolean enabled) {
        setComponentEnabled(zipCodePanel, enabled, true);
    }
    
    /**
     * Enable or diasable all the country searching constraint components.
     */
    private void setCountryComponentsEnabled(boolean enabled) {
        setComponentEnabled(countryPanel, enabled, true);
    }

    /**
     * Enable or diasable all the location searching constraint components.
     */
    private void setLocationComponentsEnabled(boolean enabled) {
        setComponentEnabled(locationPanel, enabled, true);
        if (getComponent("cityPanel").isEnabled()) {
        	Country country = (Country) countryComboBox.getSelectedItem();
            updateStateList(country, stateBox);
        }
    }

	private boolean checkLocationEnableState() {
		return (cityField.getText().trim().isEmpty());// &&
	}

    @Override
	public void commComplete(int commTag, Object returnValue) {
        boolean b = ((Boolean) returnValue).booleanValue() && transaction.getDataReturned();
        if (b) {
            PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
        }
        else {
            PageNotification.notifyExitListeners(this, PageExitListener.DONOTHING);
            flowButtons.reset();
            flowButtons.setVisible("expert", false);	
            flowButtons.setEnabled("expert", false);
            flowButtons.setVisible("back", false);
            flowButtons.setEnabled("back", false);
            flowButtons.setVisible("next", true);
            flowButtons.setEnabled("next", true);
            flowButtons.setAlternateText("next",Messages.getString("DirectoryOfAgents.11"));    
            flowButtons.setAlternateKeyMapping("next", KeyEvent.VK_F1, 0);	
            flowButtons.setVisible("cancel", true);
            flowButtons.setEnabled("cancel", true);
            flowButtons.setAlternateText("cancel",Messages.getString("OK"));    
        }
    }

	private void getAgents() {
		if (areaCodeField.getText().trim().length() != 0) {
			// Search by area code prefix
            if (areaCodeField.getText().trim().length() == 6) {
    			transaction.getAgentsByAreaCodePrefix
    				(areaCodeField.getText(), this);
            }
            else{
                Toolkit.getDefaultToolkit().beep();
                areaCodeField.requestFocus();
            }           
		
		} else if (zipCodeField.getText().trim().length() != 0) {
			// Search by zip/postal code
			Country country = (Country) countryComboBox.getSelectedItem();

			transaction.getAgentsByZipCode(zipCodeField.getText(), country, this);
		
		} else if ((cityField.getText().trim().length() != 0)) {// &&
			// Search by city, state, and country
			// This call may need to be converted to a domain object which
			// then retrieves the country to use in the search.
			Country country = (Country) countryComboBox.getSelectedItem();
                        transaction.getAgentsByCity(cityField.getText(),
                            (CountrySubdivision) stateBox.getSelectedItem(), country, this);
		}
		// OLD-TO-DO Maintain a count of number of attempts and send this to the
		// diag log.
		//transaction.writeReportEventMessage(1, (agents != null));
	}
	
	/**
	 * Handles a key pressed KeyEvent.
	 * @param e A KeyEvent object.
	 */
	void handleKeyPressedEvent(KeyEvent e) {
	    e.getKeyCode();
	}
	
	
	void handleTableKeyEvent(KeyEvent e) {
	    if (e.getKeyCode() == KeyEvent.VK_TAB) {
            if (flowButtons.getButton("next").isEnabled()) 
            	flowButtons.getButton("next").requestFocus();
            else 
            	flowButtons.getButton("cancel").requestFocus();
	    }
	} // END_METHOD
}
