package dw.agentdirectory;

import java.util.List;
import java.util.Map;

import com.moneygram.agentconnect.Response;

import dw.comm.MessageFacade;
import dw.comm.MessageFacadeError;
import dw.comm.MessageFacadeParam;
import dw.comm.MessageLogic;
import dw.dialogs.Dialogs;
import dw.framework.CommCompleteInterface;
import dw.framework.DataCollectionPanel;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.FlowPage;
import dw.i18n.Messages;
import dw.io.info.ReportEventDiagInfo;
import dw.mgsend.MoneyGramClientTransaction;
import dw.model.adapters.ConsumerProfile;
import dw.model.adapters.CountryInfo.Country;
import dw.model.adapters.CountryInfo.CountrySubdivision;
import dw.model.adapters.CustomerInfo;
import dw.model.adapters.MoneyGramAgentInfo;
import dw.profile.Profile;
import dw.profile.ProfileItem;
import dw.profile.UnitProfile;
import dw.utility.ErrorManager;
import dw.utility.TimeUtility;

/**
 * AgentDirectoryTransaction encapsulates the directory of agents.
 */
public class AgentDirectoryTransaction extends MoneyGramClientTransaction {
	private static final long serialVersionUID = 1L;

	// Instance vars
	private static final String USER_ELEMENT_TAG = "USER";	
	private static final String USERNAME_ITEM_TAG = "NAME";	
    private MoneyGramAgentInfo[] agents = null;
    private Country country;
    private String zip ="";
    private String city = "";
    private CountrySubdivision state = null;
    private String areaCode = "";
    private boolean dataReturned = false;

    public MoneyGramAgentInfo[] getAgents() {
        return agents;
    }

    /**
     * Create a new AgentDirectoryTransaction.
     * Before calling this method, invoke UnitProfile.getUPInstance().load().
     */
    public AgentDirectoryTransaction(String tranName) {
        super(tranName, "");
        timeoutEnabled = true;
        dispenserNeeded = false;
        receiptPrinterNeeded = false;
        clear();
    }

    /**
     * Override of method in Client Transaction. Authentication is never
     *   needed for this transaction.
     */
    @Override
	public boolean isNamePasswordRequired() {
        return false;
    }

    /**
     * Override of method in Client Transaction. Authentication is never
     *   needed for this transaction.
     */
    @Override
	public boolean isPasswordRequired() {
        return false;
    }

    /**
     * Override of ClientTransaction method. There is only one type of this
     *   interface, so just default to wizard always.
     */
    @Override
	public boolean isUsingWizard() {
        return true;
    }

    /**
        Writes a report event message to the diagnostic log.
        @param numberOfAttempts The number of attempts made by the user to
        retrieve a directory of agents.
        @param successful A flag stating whether this attempt to retrieve the
        directory of agents was successful.
     */
    public void writeReportEventMessage(int numberOfAttempts, boolean successful) {

        // Get the user name using the userID from this transaction.
        String username = ""; 
        try {
            Profile userProfile =
                (Profile) UnitProfile.getInstance().getProfileInstance().find(USER_ELEMENT_TAG, userID);
            ProfileItem usernameItem = userProfile.find(USERNAME_ITEM_TAG);
            username = usernameItem.stringValue();
        }
        catch (java.util.NoSuchElementException e) {
        }

        try {
            new ReportEventDiagInfo(
                TimeUtility.currentTime(),
                userID,
                username,
                ReportEventDiagInfo.DIRECTORY_OF_AGENTS,
                numberOfAttempts,
                successful);
        }
        catch (Exception e) {
            ErrorManager.handle(e, "applicationError", Messages.getString("ErrorMessages.applicationError"), ErrorManager.ERROR); 
        }
    }

    /**
     * Retrieves MoneyGramAgentInfo objects using the
     * area code prefix.  Returns an array of MoneyGramAgentInfo objects.
     * @param areaCodePrefix A string representing the area code prefix to
     * use for the search.
     * @return An array of MoneyGramAgentInfo objects.
     */
    public void getAgentsByAreaCodePrefix(final String areaCodePrefix,
                                        final CommCompleteInterface callingPage) {
        final MessageFacadeParam parameters = new MessageFacadeParam (MessageFacadeParam.REAL_TIME);
        final MessageLogic logic = new MessageLogic() {
        
            @Override
			public Object run() {
        		agents = new MoneyGramAgentInfo[0];
                try {
                	setDataReturned(false);
//	               	new AgentDirectoryEventDiagInfo(TimeUtility.currentTime(), AgentDirectoryEventDiagInfo.PHONE_TYPE, 1, false);

                    agents = MessageFacade.getInstance().getAgentsByAreaCodePrefix(callingPage, parameters, areaCodePrefix);

//	                new AgentDirectoryEventDiagInfo(TimeUtility.currentTime(), AgentDirectoryEventDiagInfo.PHONE_TYPE, 1, true);
	                if (agents.length > 0)
	                	setDataReturned(true);
        	        return Boolean.TRUE;
		        }
                catch (MessageFacadeError mfe) {
                    return Boolean.FALSE;    
                }
                catch (Exception e) {
                    return Boolean.FALSE;
                }
            }
        };
        MessageFacade.run(logic, callingPage, 0, Boolean.TRUE);
    }



    /**
     * Retrieves MoneyGramAgentInfo objects using the
     * zip code.  Returns an array of MoneyGramAgentInfo objects.
     * @param zipCode A string representing the zip/postal code to
     * use for the search.
     * @return An array of MoneyGramAgentInfo objects.
     */
    public void getAgentsByZipCode(final String zipCode,
    		                       final Country country,
                                     final CommCompleteInterface callingPage) {
        final MessageFacadeParam parameters = new MessageFacadeParam (MessageFacadeParam.REAL_TIME);
        final MessageLogic logic = new MessageLogic() {
        
            @Override
			public Object run() {
            	setDataReturned(false);
                agents = new MoneyGramAgentInfo[0];
                try {
                    agents = MessageFacade.getInstance().getAgentsByZipCode(
                        callingPage,
                        parameters,
                        zipCode,
                        country);
                    if (agents.length > 0)
	                	setDataReturned(true);
                    return Boolean.TRUE;
                }
                catch (MessageFacadeError mfe) {
                    return Boolean.FALSE;    
                }
                catch (Exception e) {
                    return Boolean.FALSE;
                }
            }
        };
        MessageFacade.run(logic, callingPage, 0, Boolean.TRUE);
    }

    /**
     * Retrieves MoneyGramAgentInfo objects using the
     * city, state, and country.  Returns an array of MoneyGramAgentInfo objects.
     * @param city A string representing the city to
     * use for the search.
     * @param state A string representing the state to
     * use for the search.
     * @param country A string representing the country to
     * use for the search.
     * @return An array of MoneyGramAgentInfo objects.
     */
    public void getAgentsByCity (final String city, 
                                 final CountrySubdivision state, 
                                 final Country country,
                                 //final AgentDirectoryFlowPage callingPage) {                
        						 final AgentDirectoryWizardPage1 callingPage) {                
        final MessageFacadeParam parameters = new MessageFacadeParam (MessageFacadeParam.REAL_TIME);
        final MessageLogic logic = new MessageLogic() {
        
            @Override
			public Object run() {
                agents = new MoneyGramAgentInfo[0];
                setDataReturned(false);
                String[] cityList;
                try {
                   //perform a citylist request to get the list of cities.
                   cityList = MessageFacade.getInstance().getCityList(callingPage, parameters, city, state, country);
                   if( cityList.length < 1){
                       return Boolean.TRUE;
//               	   return Boolean.FALSE;
                   }

                   //if just one city is returned,get agents corresponding to that city.
                    if(cityList.length == 1){
                        agents = MessageFacade.getInstance().getAgentsByCity(callingPage, parameters, cityList[0], state, country, false);
                    }
                    else {
                        // we have multiple matching cities, so query user for the right one
                        String selectedCity = (String) Dialogs.showInput("dialogs/DialogCitySelection.xml",	
                                cityList, cityList[0]);
                        if (selectedCity == null)
                            agents = new MoneyGramAgentInfo[0];
                        else {
                        	setCity(selectedCity);
                            agents = MessageFacade.getInstance().getAgentsByCity(callingPage, 
                                    parameters, selectedCity, state, country, false);
                        }
                    }
                    if (agents.length > 0)
	                	setDataReturned(true);
                    return Boolean.TRUE;
                }
                catch (MessageFacadeError mfe) {
                    return Boolean.FALSE;
                }
                catch (Exception e) {
                    return Boolean.FALSE;
                }
            }
        };
        MessageFacade.run(logic, callingPage, 0, Boolean.TRUE);
    }

	@Override
	public boolean isFinancialTransaction() {
		return false;
	}

    @Override
	public void setFinancialTransaction(boolean b)
    {
        // Do nothing.
    }
    
	@Override
	public boolean isCheckinRequired() {
		return false;
	}
	
	// Country storage info
	public void setCountry(Country country){
		this.country = country;
	}
	
	public Country getCountry() {
		return country;
	}
	
	// Zip storage info
	public void setZip(String s){
		zip = s;
	}
	
	public String getZip() {
		return zip;
	}
	
	// City storage info
	public void setCity(String s){
		city = s;
	}
	
	public String getCity() {
		return city;
	}
	
	// State storage info
	public void setState(CountrySubdivision s){
		state = s;
	}
	
	public CountrySubdivision getState() {
		return state;
	}
	
	// State storage info
	public void setAreaCode(String s){
		areaCode = s;
	}
	
	public String getAreaCode() {
		return areaCode;
	}
	
	private void setDataReturned(boolean b){
		dataReturned = b;
	}
	
	public boolean getDataReturned(){
		return dataReturned;
	}
	
	@Override
	public void clear() {
		setStatus(IN_PROCESS);
		setCountry(null);
		setZip("");
		setCity("");
		setState(null);
		setAreaCode("");
		setDataReturned(false);
	}
	
	// Data Collection support method (not used by this type of transaction)

	@Override
	public List<DataCollectionPanel> getSupplementalDataCollectionPanels() {
		return null;
	}

	@Override
	public void setValidationStatus(DataCollectionStatus status) {
	}

	@Override
	public void validation(FlowPage callingPage) {
	}

	@Override
	public String nextDataCollectionScreen(String currentScreen) {
		return null;
	}

	@Override
	public String getDestinationCountry() {
		return null;
	}

	@Override
	public void setScreensToShow() {
	}

	@Override
	public void applyHistoryData(CustomerInfo customerInfo) {
	}

	@Override
	public CustomerInfo getCustomerInfo() {
		return null;
	}

	@Override
	public void applyTransactionLookupData() {
	}

	@Override
	public void applyProfileData(ConsumerProfile profile) {
	}

	@Override
	public boolean createOrUpdateConsumerProfile(ConsumerProfile profile, ProfileStatus status) {
		return false;
	}

	@Override
	public void createOrUpdateConsumerProfile(ConsumerProfile profile, CommCompleteInterface cci) {
	}

	@Override
	public void getConsumerProfile(ConsumerProfile profile, String mgiSessionID, int rule, Map<String, String> currentValues, CommCompleteInterface cci) {
	}

	@Override
	public Response getConsumerProfileResponse() {
		return null;
	}

	@Override
	public void applyCurrentData() {
	}
}
