package dw.agentdirectory;

import java.awt.SystemColor;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import dw.dwgui.DWTable;
import dw.dwgui.DWTable.DWTableListener;
import dw.dwgui.RowSelectCellRenderer;
import dw.dwgui.UneditableTableModel;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.io.report.DirectoryOfAgentsDetail;
import dw.io.report.ReportDetail;
import dw.main.DeltaworksMainPanel;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.CountryInfo.Country;
import dw.model.adapters.CountryInfo.CountrySubdivision;
import dw.model.adapters.MoneyGramAgentInfo;
import dw.printing.DirectoryOfAgentsReport;
import dw.printing.Report;
import dw.printing.TEPrinter;

/**
 * Page 2 of the AgentDirectory wizard. This page displays a list of agents
 *   that match the search criteria entered. 
 */
public class AgentDirectoryWizardPage2 extends AgentDirectoryFlowPage implements DWTableListener {
	private static final long serialVersionUID = 1L;

	private DWTable agentTable;

	private DWTable storeHoursTable;

	private DefaultTableModel agentTableModel;

	private DefaultTableModel storeHoursTableModel;

	private ArrayList<MoneyGramAgentInfo> agents = new ArrayList<MoneyGramAgentInfo>();

	private JRadioButton oneRadio;

	private JTextField numberOfRowsField;

	private static int DEFAULT_RECORDS_TO_PRINT = 5;

	private Collection<ReportDetail> printAgentData = new ArrayList<ReportDetail>();

	private Report report;
	
	private int MIN_SCREEN_WIDTH = 500;
	
	private JScrollPane scrollPane;	
	
	private boolean agentPrintingEnabled = transaction.isUseAgentPrinter()
			|| transaction.isReceiptReceived();

	private JPanel panel;
	private boolean listSelectionListener = false;
	
	private final Runnable PAPER_PRINT_HANDLER = new Runnable() {
		@Override
		public void run() {
			final TEPrinter printer = TEPrinter.getPrinter();
			if (printer.isReady(DeltaworksMainPanel.getMainPanel()))
				report.print(printer);
		}
	};

	public AgentDirectoryWizardPage2(AgentDirectoryTransaction tran,
			String name, String pageCode, PageFlowButtons buttons) {
		super(tran, "agentdirectory/DirectoryOfAgents2.xml", name, pageCode, buttons); 
		
		scrollPane = (JScrollPane) this.getComponent("scrollpane");
		panel = (JPanel) getComponent("panel");
		agentTable = (DWTable) this.getComponent("agentTablePane");
		storeHoursTable = (DWTable) this.getComponent("storeHoursTablePane");
		oneRadio = (JRadioButton) getComponent("oneRadio"); 
		numberOfRowsField = (JTextField) getComponent("numberOfRowsField");     	
		
		agentTable.addListener(this, this);
		
		final JButton cancelButton = flowButtons.getButton("cancel"); 

		KeyListener kl = new KeyAdapter() { 

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER
						|| e.getKeyCode() == KeyEvent.VK_TAB) {
					e.consume();
					if (agentPrintingEnabled) {
						oneRadio.requestFocus(true);
					} else
						flowButtons.getButton("back").requestFocus();
				}
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					e.consume();
					cancelButton.doClick();
				}
				if (e.getKeyCode() == KeyEvent.VK_F2) {
					e.consume();
					transaction.f2SAR();
				}
			}
		};

		agentTable.getTable().addKeyListener(kl);
		
		agentTable.getTable().getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {
					@Override
					public void valueChanged(ListSelectionEvent e) {
						if (listSelectionListener) {
							displayStoreHours();
						}
					}
				});
	}
	
	@Override
	public void dwTableResized() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				agentTable.resizeTable();
			} 
		});
	}

	private void setupComponents() {
		oneRadio.setSelected(true);
		addActionListener("printButton", this); 
		addFocusLostListener("numberOfRowsField", this, "validateNumberOfRows");  
		
		setupAgentTable();
		setupStoreHoursTable();
	}

	private void setupAgentTable() {
		String[] columnNames = {
				Messages.getString("AgentDirectoryFlowPage.1800"), 
				Messages.getString("AgentDirectoryFlowPage.1801"), 
				Messages.getString("AgentDirectoryFlowPage.1802") };   
		agentTableModel = new UneditableTableModel(columnNames, 0);
		agentTable.getTable().setModel(agentTableModel);
		agentTable.getTable().getTableHeader().setReorderingAllowed(false);
		
		agentTable.getTable().setFont(new java.awt.Font("SansSerif", java.awt.Font.PLAIN, 12)); 
		agentTable.getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		RowSelectCellRenderer.setAsDefaultRenderer(agentTable.getTable());
		agentTable.getTable().getSelectionModel().setSelectionInterval(0, 0);
		agentTable.getTable().getColumnModel().getColumn(0).setCellRenderer(agentTable.new DWTableCellRenderer(5));
		agentTable.getTable().getColumnModel().getColumn(1).setCellRenderer(agentTable.new DWTableCellRenderer(5));
		agentTable.getTable().getColumnModel().getColumn(2).setCellRenderer(agentTable.new DWTableCellRenderer(5));
	}

	private void setupStoreHoursTable() {
		String[] columnNames = { Messages.getString("DirectoryOfAgents2.2") }; 
		storeHoursTableModel = new UneditableTableModel(columnNames, 0);
		storeHoursTable.getTable().setModel(storeHoursTableModel);
		storeHoursTable.getTable().setRowSelectionAllowed(false);
		storeHoursTable.getTable().setFont(new java.awt.Font("Monospaced", java.awt.Font.PLAIN, 12)); 
		storeHoursTable.getTable().setBackground(SystemColor.control);
		storeHoursTable.getTable().setFocusable(false);
		storeHoursTable.getTable().getTableHeader().setReorderingAllowed(false);
		storeHoursTable.getTable().getTableHeader().setResizingAllowed(false);

		storeHoursTable.getTable().getColumnModel().getColumn(0).setCellRenderer(storeHoursTable.new DWTableCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));
		
		RowSelectCellRenderer.setAsDefaultRenderer(storeHoursTable.getTable());
	}

	/**
	 * Notify listeners that the user exited the numberOfRowsField.
	 */
	public void validateNumberOfRows() {
		int inputInt = 50;
		try {
			inputInt = Integer.parseInt(numberOfRowsField.getText());
		} catch (Exception e) {
		}
		int agentsInt = agents.size();

		if (inputInt > 50) {
			numberOfRowsField.setText("50");
		}

		if (inputInt > agentsInt) {
			numberOfRowsField.setText(String.valueOf(agentsInt));
		}
	}

	/**
	 * Notify listeners that the user wishes to print.
	 */
	public void printButtonAction() {

		printAgentData.clear();

		if (oneRadio.isSelected()) {
			MoneyGramAgentInfo mgai = agents
					.get(agentTable.getTable().getSelectedRow());
			printAgentData.add(new DirectoryOfAgentsDetail(mgai));
		} else {
//			int number = (new Integer(numberOfRowsField.getText())).intValue();
			int number = 0;
			try {
				number = Integer.parseInt(numberOfRowsField.getText());
			} catch (Exception e ) {
			}
			for (int i = 0; i < number; i++) {
				MoneyGramAgentInfo mgai = agents.get(i);
				final DirectoryOfAgentsDetail doad = new DirectoryOfAgentsDetail(
						mgai);
				printAgentData.add(doad);
			}
		}

		String city = transaction.getCity();
		Country country = transaction.getCountry();
		String zip = transaction.getZip();
		String areaCode = transaction.getAreaCode();

		report = new DirectoryOfAgentsReport(printAgentData, city, country,
				zip, areaCode);
		report.setXmlFileName("xml/reports/directoryofagentsdetailreport.xml");
		doPrint(PAPER_PRINT_HANDLER);

	}

	private void doPrint(final Runnable printHandler) {
		new Thread(printHandler).start();
	}

	public void numberOfRowsFieldFocusLost() {
		validateNumberOfRows();
	}

	/**
	 * Notify listeners that the user wishes to find stor hours.
	 */
	private void displayStoreHours() {
		int selectedRow = agentTable.getTable().getSelectedRow();
		if ((selectedRow < 0) || (selectedRow >= agents.size())) {
			return;
		}
		int rowCount = storeHoursTableModel.getRowCount();
		String[] data;
		for (int i = 0; i < rowCount; i++)
			storeHoursTableModel.removeRow(0);
		data = (agents.get(selectedRow)).getStoreHrs();
		for (int i = 0; i < data.length; i++) {
			String[] row = { data[i] };
			storeHoursTableModel.addRow(row);
		}
		storeHoursTable.initializeTable(scrollPane, panel);
		storeHoursTable.resizeTable();
	}

	@Override
	public void start(int direction) {
		listSelectionListener = false;
		setupComponents();
		flowButtons.reset();
		flowButtons.setVisible("expert", false); 
		flowButtons.setEnabled("next", false);

		if (!agentPrintingEnabled) {
			removeComponent("middleBox");
		}

		MoneyGramAgentInfo[] info = transaction.getAgents();
		for (int i = 0; i < info.length; i++) {
			agents.add(info[i]);
		}

		int startRecordsToPrint = DEFAULT_RECORDS_TO_PRINT;

		if (agents.size() < DEFAULT_RECORDS_TO_PRINT)
			startRecordsToPrint = agents.size();

		numberOfRowsField.setText(String.valueOf(startRecordsToPrint));

		int rowCount = agentTableModel.getRowCount();

		for (int i = 0; i < rowCount; i++) {
			agentTableModel.removeRow(0);
		}
		
		for (int i = 0; i < agents.size(); i++) {
			String location;
			MoneyGramAgentInfo mgai = agents.get(i);
	    	CountrySubdivision countrySubdivision = null;
	    	String countrySubdivisionCode = mgai.getCountrySubdivisionCode();
	    	if ((countrySubdivisionCode != null) && (! countrySubdivisionCode.isEmpty())) {
	    		countrySubdivision = CountryInfo.getCountrySubdivision(countrySubdivisionCode);
	    	}
			
			if (countrySubdivision != null) {
				location = mgai.getAddress() + ", " + mgai.getCity() + ", " + countrySubdivision.getLegacyCode();
			} else {
				location = mgai.getAddress() + ", " + mgai.getCity();
			}

			String[] row = { mgai.getAgentName(), location, mgai.getAgentPhone() };
			agentTableModel.addRow(row);
		}
		
		// select the top row : 'none'
		agentTable.getTable().setRowSelectionInterval(0, 0);
		agentTable.getTable().requestFocus();
		listSelectionListener = true;
		displayStoreHours();
		
		agentTable.initializeTable(scrollPane, panel, MIN_SCREEN_WIDTH, 10);
		agentTable.setColumnStaticWidth(2);
	}

	@Override
	public void finish(int direction) {
		if (direction == PageExitListener.BACK)
			if (agents != null)
				agents.clear();

		PageNotification.notifyExitListeners(this, direction);
	}
}
