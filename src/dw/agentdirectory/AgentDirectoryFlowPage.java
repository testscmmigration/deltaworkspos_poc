package dw.agentdirectory;

import dw.framework.FlowPage;
import dw.framework.PageFlowButtons;

public abstract class AgentDirectoryFlowPage extends FlowPage {
	private static final long serialVersionUID = 1L;
	
	protected AgentDirectoryTransaction transaction;
   
    public AgentDirectoryFlowPage(AgentDirectoryTransaction transaction,
                    String fileName, String name, String pageCode, PageFlowButtons buttons) {
        super(fileName, name, pageCode, buttons);
        this.transaction = transaction;
    }

    // do-nothing impl of commCompleteInterface...will be overridden by each
    //  specific page where comm is needed.
    @Override
	public void commComplete(int commTag, Object returnValue) {}

}
