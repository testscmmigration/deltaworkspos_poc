package dw;

/**
 * Marker interface for classes that we can't obfuscate because 
 * someone just had to use reflection. 
 */
public interface NonObfuscatable {
}
