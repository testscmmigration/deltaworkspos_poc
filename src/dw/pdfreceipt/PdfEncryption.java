package dw.pdfreceipt;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;

import dw.io.DiagLog;
import dw.io.diag.MustTransmitDiagMsg;
import dw.profile.UnitProfile;
import dw.utility.Debug;
import dw.utility.DwEncryptionAES;
import flags.CompileTimeFlags;

/**
 * Class for encrypting/decrypting PDF receipt files
 * 
 * @author w156
 * 
 */
public class PdfEncryption extends DwEncryptionAES {
	private static PdfEncryption singleton = null;

	static {
		getSingletonObject();
	}

	/**
	 * Method returns the single instance of the PdfEncryption object.
	 * 
	 * @return singleton PDF Encryption object
	 */
	private static PdfEncryption getSingletonObject() {
		if (singleton == null) {
			// None available call the constructor
			singleton = new PdfEncryption();
		}
		return singleton;
	}

	/**
	 * Method for making a decrypted copy of an encrypted file
	 * 
	 * @param pm_sEncryptedFileName
	 *            - String containing the file name of the encrypted file to
	 *            decrypt and copy
	 * @param os
	 *            - ByteArrayOutputStream to write the decrypted contents of the
	 *            encrypted file to.
	 * @return - true if the decryption succeeded, false otherwise
	 */
	protected static boolean bMakeDecryptedFile(String pm_sEncryptedFileName, ByteArrayOutputStream os) {
		boolean bRetValue = false;
		FileInputStream fisFileIn = null;
		CipherInputStream cis = null;
		try {
			fisFileIn = new FileInputStream(pm_sEncryptedFileName);
			/*
			 * Set the cipher to decryption mode.
			 */
			singleton.cipher.init(Cipher.DECRYPT_MODE, singleton.skeySpec,
					singleton.ivspec);
			final byte[] buf = new byte[1024];
			int len;
			if (!CompileTimeFlags.rcptNoTemplateEncryption()) {
				cis = new CipherInputStream(fisFileIn,
						singleton.cipher);
				while ((len = cis.read(buf)) > 0) {
					os.write(buf, 0, len);
					Debug.println("Reading " + len + " bytes from encrypted file " + pm_sEncryptedFileName + " and wrote to output stream");
				}
			} else {
				while ((len = fisFileIn.read(buf)) > 0) {
					os.write(buf, 0, len);
					Debug.println("Reading " + len + " bytes from file " + pm_sEncryptedFileName + " and wrote to output stream");
				}
				fisFileIn.close();
			}
			bRetValue = true;

		} catch (final FileNotFoundException e) {
			Debug.printException(e);
		} catch (final IOException e) {
			Debug.printException(e);
		} catch (InvalidAlgorithmParameterException e) {
			Debug.printStackTrace(e);
		} catch (final InvalidKeyException e) {
			/*
			 * If bad key, delete file, and force transmit
			 */
			final File fSavedFile = new File(pm_sEncryptedFileName);
			boolean b = fSavedFile.delete();
	        if (b) {
	        	Debug.println("Deleted file " + fSavedFile.getName());
	        } else {
	        	Debug.println("Deletion of file " + fSavedFile.getName() + " failed");
	        }
			DiagLog.getInstance().writeMustTransmit(
					MustTransmitDiagMsg.SECURITY_BREACH);
			UnitProfile.getInstance().setNeedStateRegInfo(true);
			Debug.printException(e);
		} finally {
			if (cis != null) {
				try {
					cis.close();
					Debug.println("Closing input stream to encrypted file " + pm_sEncryptedFileName);
				} catch (IOException e) {
					Debug.printException(e);
					Debug.println("Failed to close input stream to encrypted file " + pm_sEncryptedFileName);
				}
			}
			if (fisFileIn != null) {
				try {
					fisFileIn.close();
					Debug.println("Closing input stream to file " + pm_sEncryptedFileName);
				} catch (IOException e) {
					Debug.printException(e);
					Debug.println("Failed to close input stream to file " + pm_sEncryptedFileName);
				}
			}
			if (os != null) {
				try {
					os.flush();
					Debug.println("Flushing output stream");
				} catch (IOException e) {
					Debug.printException(e);
					Debug.println("Failed to flush output stream");
				}
				try {
					os.close();
					Debug.println("Closing output stream");
				} catch (IOException e) {
					Debug.printException(e);
					Debug.println("Failed to closed output stream");
				}
			}
		}
		return bRetValue;
	}

	/**
	 * Method for saving a String as an encrypted file
	 * 
	 * @param pm_sFileName
	 *            - String containing the file name to use for the encrypted
	 *            file
	 * @param baDecodedData
	 *            - byte array containing the data to write to the file.
	 * @return - true if the file creation succeeded, false otherwise
	 */
	protected static boolean bSaveFile(String pm_sFileName, byte[] baDecodedData) {
		boolean bValidFile = true;
		FileOutputStream fosFileOut = null;
		CipherOutputStream cos = null;
		try {
			fosFileOut = new FileOutputStream(pm_sFileName);
			/*
			 * Set the cipher to encryption mode.
			 */
			singleton.cipher.init(Cipher.ENCRYPT_MODE, singleton.skeySpec,
					singleton.ivspec);
			cos = new CipherOutputStream(fosFileOut, singleton.cipher);

			try {
				if (baDecodedData.length > 0) {
					if (!CompileTimeFlags.rcptNoTemplateEncryption()) {
						cos.write(baDecodedData);
						Debug.println("Writing " + baDecodedData.length + " bytes to encrypted file " + pm_sFileName);
					} else {
						fosFileOut.write(baDecodedData);
						Debug.println("Writing " + baDecodedData.length + " bytes to file " + pm_sFileName);
					}
				} else {
					Debug.println("bSaveFile: No data supplied");
					bValidFile = false;
				}
			} catch (final Exception e) {
				Debug.printException(e);
				return false;
			}
		} catch (final InvalidKeyException e) {
			Debug.printException(e);
			return false;
		} catch (InvalidAlgorithmParameterException e) {
			Debug.printStackTrace(e);
			return false;
		} catch (final FileNotFoundException e) {
			Debug.printException(e);
			return false;
		} finally {
			if (cos != null) {
				try {
					cos.flush();
					Debug.println("Flushed output stream to encrypted file " + pm_sFileName);
				} catch (IOException e) {
					Debug.printException(e);
					Debug.println("Failed to flush output stream to encrypted file " + pm_sFileName);
				}
			}
			if (fosFileOut != null) {
				try {
					fosFileOut.flush();
					Debug.println("Flushed output stream to file " + pm_sFileName);
				} catch (IOException e) {
					Debug.printException(e);
					Debug.println("Failed to output stream to flush file " + pm_sFileName);
				}
			}
			if (cos != null) {
				try {
					cos.close();
					Debug.println("Closed output stream to encrypted file " + pm_sFileName);
				} catch (IOException e) {
					Debug.printException(e);
					Debug.println("Failed to close output stream to encrypted file " + pm_sFileName);
				}
			}
			if (fosFileOut != null) {
				try {
					fosFileOut.close();
					Debug.println("Closed output stream to file " + pm_sFileName);
				} catch (IOException e) {
					Debug.printException(e);
					Debug.println("Failed to close output stream to file " + pm_sFileName);
				}
			}
		}
		return bValidFile;
	}

	/**
	 * Constructor for the PdfEncryption object.
	 */
	private PdfEncryption() {
		vInitialize(OFFSET_PDF_DATA, null);
	}
}
