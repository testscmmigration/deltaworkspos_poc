package dw.pdfreceipt;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.activation.DataHandler;

import com.moneygram.agentconnect.ReceiptSegmentType;

import dw.i18n.Messages;
import dw.reprintreceipt.ReprintFile;
import dw.utility.Debug;
import dw.utility.TimeUtility;

/**
 * Class for dealing with downloaded PDF Receipt files
 * 
 * @author w156
 * 
 */
public class PdfReceipt {
	/**
	 * Task to delete the file after its TTL expires.
	 */
	private static class ExpireFileTimerTask extends TimerTask {
		@Override
		public void run() {
			Debug.println("ExpireFileTimerTask:Run:");
			deletePdfFiles();
		}
	}
	
	public static final int PDF_AGENT_1 = 3;
	public static final int PDF_AGENT_2 = 4;
	public static final int PDF_CUST_1 = 5;
	public static final int PDF_CUST_2 = 6;
	public static final int PDF_DISC_1 = 1;
	public static final int PDF_DISC_2 = 2;
	
	public static final boolean PDF_DELETE_FILE = true;
	public static final boolean PDF_DONT_AUTO_DELETE_FILE = false;

	private static final long PDF_MAX_TIME = TimeUtility.MINUTE * 30;
	
	private static final int[] PDF_FN_TYPE_MAP = {0,1,2,1,2,3,4};

	protected static final String[] PDF_FILE_NAMES = { "0000.prf", "0001.prf",
			"0002.prf", "0003.prf", "0004.prf", "0005.prf", "0006.prf" };
	
	protected static final int PDF_TEMP = 0;
	
	private static final int PDF_MAX_TYPE = PDF_FN_TYPE_MAP.length;
	private static final int PDF_MIN_TYPE = 1;
	
	private static ExpireFileTimerTask taskExpireFileTimer;
	private static Timer timerExpireFile = new Timer();

	private boolean bLastPrintStatus;
	private String PDF_FILE;
	private String sExecuteDialogText;
	private ByteArrayOutputStream outputStream;

	public static synchronized void setReprintReceiptFileDeletion() {
		long time = 0;
		for (int i = 1; i < PDF_FILE_NAMES.length; i++) {
			File file = new File(PDF_FILE_NAMES[i]);
			if (file.isFile()) {
				time = Math.max(time, file.lastModified());
			}
		}
		
		if (time > 0) {
			time += PDF_MAX_TIME;
			if (time > System.currentTimeMillis()) {
				vSetupExpireTimer(time - System.currentTimeMillis());
			} else {
				deletePdfFiles();
			}
		}
	}

	/**
	 * Method to delete all PdfReceipt files.
	 */
	public static synchronized void deletePdfFiles() {
		/*
		 * Delete any/all pdf print files still present in the system
		 */
		for (int ii = PDF_MIN_TYPE; ii < PDF_MAX_TYPE; ii++) {
			deleteFile(ii);
		}
		
		/*
		 * Delete temp file if it exists.
		 */
		vDeleteTempFile();
    	ReprintFile.deleteFile(true);

		if (taskExpireFileTimer != null) {
			taskExpireFileTimer.cancel();
			taskExpireFileTimer = null;
		}
	}

	/**
	 * Method to delete the temporary file used for printing PDF receipts
	 */
	protected static void vDeleteTempFile() {
		final File fFileId = new File(PDF_FILE_NAMES[PDF_TEMP]);
		if (fFileId.isFile()) {
			if (fFileId.delete()) {
				Debug.println("Deleted file " + PDF_FILE_NAMES[PDF_TEMP] + ".");
			} else {
				Debug.println("Deletion of file " + PDF_FILE_NAMES[PDF_TEMP] + " failed.");
			}
		}
	}
	
	/**
	 * Method to determine if any active pdf print files are available for reprint
	 */
	public static boolean reprintAvailable() {
		/*
		 * examine timers for any pdf print files still present in the system
		 */
		for (int ii = PDF_MIN_TYPE; ii < PDF_MAX_TYPE; ii++) {
			if (PdfReceipt.isReprintReceiptFileAvailable(ii)) {
				return true;
			}
		}
		return false;
	}	

	/**
	 * PdfReceipt constructor if PDF file may already exist
	 * 
	 * @param pm_iRcptType
	 *            - int identifying the receipt type of the PDF file created
	 *            from sEncodedString
	 * @param pm_bDeleteIfFound
	 *            - boolean to control whether to automatically delete the file
	 *            if found.
	 */
	public PdfReceipt(int pm_iRcptType) {
		boolean bRetValue = (pm_iRcptType >= PDF_MIN_TYPE) && (pm_iRcptType <= PDF_MAX_TYPE);
		if (bRetValue) {
			PDF_FILE = PDF_FILE_NAMES[PDF_FN_TYPE_MAP[pm_iRcptType]];
		} else {
			PDF_FILE = null;
			String errorMessage = "Invalid PRT File Type [" + pm_iRcptType + "]";
			Debug.println(errorMessage);
			throw new RuntimeException(errorMessage);
		}
		
		bLastPrintStatus = false;
	}
	
	public static boolean isReprintReceiptFileAvailable(int receiptType) {
		int index = PDF_FN_TYPE_MAP[receiptType];
		String fileName = PDF_FILE_NAMES[index];
		return new File(fileName).isFile();
	}

	/**
	 * PdfReceipt constructor
	 * 
	 * @param pm_sEncodedString
	 *            - String containing a base64 encoded PDF file
	 * @param receiptType
	 *            - int identifying the receipt type of the PDF file created
	 *            from sEncodedString
	 */
	public PdfReceipt(List<ReceiptSegmentType> content, int receiptType) throws IOException {
		this(receiptType);
		byte[] buffer = new byte[1024];
		try {
			for (ReceiptSegmentType segment : content) {
				DataHandler mimeData = segment.getMimeData();
				if (mimeData != null ) {
					ByteArrayInputStream baip = (ByteArrayInputStream) segment.getMimeData().getContent();
					outputStream = new ByteArrayOutputStream();
					while (true) {
						int c = baip.read(buffer);
						if (c <= 0) {
							break;
						}
						Debug.println("Writing " + c + " bytes to internal buffer stream");
						outputStream.write(buffer, 0, c);
					}
				} else {
					Debug.println("ERROR: PdfReceipt: receiptMimeDataSegment with no mimeData ");
				}
			}
			Debug.println("Reading " + outputStream.size() + " bytes from AC response for receipt type " + receiptType);
			
			if (!bValidFileType(receiptType)) {
				final String errorMessage = "Invalid PDF File Type ["
						+ receiptType + "]";
				Debug.println(errorMessage);
				throw new RuntimeException(errorMessage);
			}
	
			bLastPrintStatus = false;
			if (outputStream.size() > 0) {
				deleteFile(receiptType);
				
				boolean bFileExists = PdfEncryption.bSaveFile(PDF_FILE, outputStream.toByteArray());
				if (bFileExists) {
					vSetupExpireTimer(PDF_MAX_TIME);
				}
			}
		} finally {
			try {
				outputStream.flush();
				Debug.println("Flushing internal buffer stream");
			} catch (Exception e) {
				Debug.printException(e);
				Debug.println("Flushing of internal buffer stream failed");
			}
			try {
				outputStream.close();
				Debug.println("Closing internal buffer stream");
			} catch (Exception e) {
				Debug.printException(e);
				Debug.println("Closing of internal buffer stream failed");
			}
		}
	}

	/**
	 * Method to delete the encrypted save file used for printing PDF receipts
	 */
	private static synchronized void deleteFile(int receiptType) {
		int index = PDF_FN_TYPE_MAP[receiptType];
		String fileName = PDF_FILE_NAMES[index];
		File fFileId = new File(fileName);
		if (fFileId.isFile()) {
			if (fFileId.delete()) {
				Debug.println("Deleted file " + fileName);
			} else {
				Debug.println("Deletion of file " + fileName + " failed");
			}
			/*
			 * If deleting agent receipt, also delete old style receipt data
			 */
			if (receiptType == PDF_AGENT_1) {
	        	ReprintFile.deleteFile(true);
			}
		}
	}

	/**
	 * Method to return the last print status for this instance
	 * 
	 * @return - true if last print was successful, false otherwise
	 */
	public boolean getLastPrintStatus() {
		return bLastPrintStatus;
	}

	/**
	 * Method to print a .PDF receipt file
	 * 
	 * @return - true if print was successful, false otherwise
	 */
	public boolean print() {
		if (outputStream == null) {
			outputStream = new ByteArrayOutputStream();
			bLastPrintStatus = PdfEncryption.bMakeDecryptedFile(PDF_FILE, outputStream);
		} else {
			bLastPrintStatus = true;
		}
		if (bLastPrintStatus) {
			bLastPrintStatus = PdfPrint.print(outputStream, sExecuteDialogText);
		}
		return bLastPrintStatus;
	}

	/**
	 * Verifies if the entered file type is valid
	 * 
	 * @param pm_iFileType
	 *            - int containing a prospective file type
	 * 
	 * @return true if the entered file type is valid
	 */
	private boolean bValidFileType(int pm_iRcptType) {
		final boolean bRetValue = (pm_iRcptType >= PDF_MIN_TYPE)
				&& (pm_iRcptType <= PDF_MAX_TYPE);
		if (bRetValue) {
			PDF_FILE = PDF_FILE_NAMES[PDF_FN_TYPE_MAP[pm_iRcptType]];
		} else {
			PDF_FILE = null;
		}
		
		switch (pm_iRcptType) {
			case 1: sExecuteDialogText = Messages.getString("PdfPrint.printing1");
					break;
			case 2: sExecuteDialogText = Messages.getString("PdfPrint.printing2");
					break;
			case 3: sExecuteDialogText = Messages.getString("PdfPrint.printing3");
					break;
			case 4: sExecuteDialogText = Messages.getString("PdfPrint.printing4");
					break;
			case 5: sExecuteDialogText = Messages.getString("PdfPrint.printing5");
					break;
			case 6: sExecuteDialogText = Messages.getString("PdfPrint.printing6");
					break;
			default: sExecuteDialogText = "";
		}
		
		return bRetValue;
	}

	/**
	 * Setup a timer to delete the PDF file
	 * 
	 * @param pm_lTimeToLive
	 *            - Long value containing the time to live for the PDF file.
	 */
	private static synchronized void vSetupExpireTimer(long pm_lTimeToLive) {
		//
		// Get rid of any old timer tasks
		//
		if (taskExpireFileTimer != null) {
			taskExpireFileTimer.cancel();
		}
		//
		// Setup expireFileTimerTask to delete the PDF file
		// after its expiration time.
		//
		taskExpireFileTimer = new ExpireFileTimerTask();
		
		timerExpireFile.schedule(taskExpireFileTimer, pm_lTimeToLive);
	}
}
