package dw.pdfreceipt;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import dw.profile.UnitProfile;
import dw.utility.Debug;
import dw.utility.ErrorManager;
import dw.utility.ExecuteCommand;
import dw.utility.MD5;

/**
 * Class for printing PDF receipt files
 * 
 * @author w156
 * 
 */
public class PdfPrint {
	private static final String PDF_PRINTER = "sumatraPDF.exe";
	private static final String PDF_PRINTER_MD5 = "3a230a0f5c046a133f6bf2923d663bdb";//07838a3ecc365aa7bf5c4338289ce07e - compressed version
	private static final String PDF_TEMP_FILE = PdfReceipt.PDF_FILE_NAMES[PdfReceipt.PDF_TEMP];

	public static final String TEMPORARY_FILE_PREFIX = "PRT";
	public static final String TEMPORARY_FILE_SUFFIX = ".pdf";
	
	/**
	 * Method to verify that the external program SumatraPDF has not been
	 * compromised.
	 * 
	 * @return - true if SumatraPdf has been verified, false if SumatraPDF is
	 *         determined to be compromised.
	 */
	private static boolean bVerifySumatra() {
		final boolean bRetValue = MD5
				.validateFile(PDF_PRINTER, PDF_PRINTER_MD5);
		return bRetValue;
	}

	/**
	 * Method to print a PDF file.
	 * 
	 * @param outputStream
	 *            - ByteArrayOutputStream containing the data to write to the printer.
	 * @param pm_sExecuteText
	 *            - String containing the execute dialog text.
	 * @return - true if print was successful.
	 */
	protected static boolean print(ByteArrayOutputStream outputStream, String pm_sExecuteText) {
		boolean bRetValue = bVerifySumatra();
		if (bRetValue) {
			FileOutputStream os = null;
			try {
				String sPrinter = UnitProfile.getInstance().get("AGENT_PRINTER_NAME", "");
				
				os = new FileOutputStream(PDF_TEMP_FILE);
				os.write(outputStream.toByteArray());
				Debug.println("Writing " + outputStream.size() + " bytes to file " + PDF_TEMP_FILE);
				
				try {
					os.flush();
					Debug.println("Flushing output stream to file " + PDF_TEMP_FILE);
				} catch (Exception e) {
					Debug.printException(e);
					Debug.println("Flushing of output stream to file " + PDF_TEMP_FILE + " failed");
				}
				os.close();
				
				Debug.println("Printing file " + PDF_TEMP_FILE + " with " + PDF_PRINTER);
				Integer exitValue = ExecuteCommand.executePdfPrint(PDF_PRINTER, sPrinter, PDF_TEMP_FILE, pm_sExecuteText);

				if ((exitValue == null) || (exitValue.intValue() != 0)) {
					int iExitCode = -1;
					if (exitValue != null) {
						iExitCode = exitValue.intValue();
					}
					Debug.println("ERROR - sumatra exit code[" + iExitCode
							+ "]");
					bRetValue = false;
				}

				PdfReceipt.vDeleteTempFile();
	            ErrorManager.updateReprintReceiptState();

			} catch (final IOException ioe) {
				Debug.println("sumatra startup error!!");
				Debug.printException(ioe);
				bRetValue = false;
				PdfReceipt.vDeleteTempFile();
			} catch (final Exception ex) {
				Debug.printException(ex);
				bRetValue = false;
				PdfReceipt.vDeleteTempFile();
			} finally {
				if (os != null) {
					try {
						os.close();
						Debug.println("Closed output stream to file " + PDF_TEMP_FILE);
					} catch (IOException e) {
						Debug.printException(e);
						Debug.println("Failed to close output stream to file " + PDF_TEMP_FILE);
					}
				}
				try {
					File printFile = new File(PDF_TEMP_FILE);
					boolean d = printFile.delete();
					if (d) {
						Debug.println("Deleted file " + printFile.getName());
					} else {
						printFile.deleteOnExit();
						Debug.println("File " + printFile.getName() + " marked to be deleted when DeltaWorks is shutdown");
					}
				} catch (Exception e) {
					Debug.printException(e);
				}
			}
		}
		return bRetValue;
	}
}
