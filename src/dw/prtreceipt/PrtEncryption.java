package dw.prtreceipt;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;

import dw.io.DiagLog;
import dw.io.diag.MustTransmitDiagMsg;
import dw.printing.GenericPrinter;
import dw.printing.ScreenPrinter;
import dw.printing.TEPrinter;
import dw.profile.UnitProfile;
import dw.utility.Debug;
import dw.utility.DwEncryptionAES;
import flags.CompileTimeFlags;

/**
 * Class for encrypting/decrypting PRT receipt files
 * 
 * @author w156
 * 
 */
public class PrtEncryption extends DwEncryptionAES {

	private static PrtEncryption singleton = null;

	static {
		getSingletonObject();
	}

	/**
	 * Method returns the single instance of the PrtEncryption object.
	 * 
	 * @return singleton PRT Encryption object
	 */
	private static PrtEncryption getSingletonObject() {
		if (singleton == null) {
			// None available call the constructor
			singleton = new PrtEncryption();
		}
		return singleton;
	}

	/**
	 * Method for making a decrypted copy of an encrypted file
	 * 
	 * @param pm_sEncryptedFileName
	 *            - String containing the file name of the encrypted file to
	 *            decrypt and copy
	 * @param os
	 *            - ByteArrayOutputStream to write the decrypted contents of the
	 *            encrypted file to.
	 * @return - true if the file copy succeeded, false otherwise
	 */
	protected static boolean bMakeDecryptedFile(String pm_sEncryptedFileName, ByteArrayOutputStream os) {
		boolean bRetValue = false;
		FileInputStream fisFileIn = null;
		CipherInputStream cis = null;
		try {
			fisFileIn = new FileInputStream(pm_sEncryptedFileName);
			/*
			 * Set the cipher to decryption mode.
			 */
			singleton.cipher.init(Cipher.DECRYPT_MODE, singleton.skeySpec,
					singleton.ivspec);
			byte[] buf = new byte[1024];
			int len;
			if (!CompileTimeFlags.rcptNoTemplateEncryption()) {
				cis = new CipherInputStream(fisFileIn, singleton.cipher);
				while ((len = cis.read(buf)) > 0) {
					os.write(buf, 0, len);
					Debug.println("Reading " + len + " bytes from encrypted file " + pm_sEncryptedFileName + " and wrote to output stream");
				}
			} else {				
				while ((len = fisFileIn.read(buf)) > 0) {
					os.write(buf, 0, len);
					Debug.println("Reading " + len + " bytes from file " + pm_sEncryptedFileName + " and wrote to output stream");
				}
			}
			bRetValue = true;

		} catch (final FileNotFoundException e) {
			Debug.printException(e);
		} catch (final IOException e) {
			Debug.printException(e);
		} catch (InvalidAlgorithmParameterException e) {
			Debug.printStackTrace(e);
		} catch (final InvalidKeyException e) {
			/*
			 * If bad key, delete file, and force transmit
			 */
			final File fSavedFile = new File(pm_sEncryptedFileName);
			boolean b = fSavedFile.delete();
	        if (b) {
	        	Debug.println("Deleted file " + pm_sEncryptedFileName);
	        } else {
	        	Debug.println("File " + pm_sEncryptedFileName + " could not be deleted");
	        }
			DiagLog.getInstance().writeMustTransmit(
					MustTransmitDiagMsg.SECURITY_BREACH);
			UnitProfile.getInstance().setNeedStateRegInfo(true);
			Debug.printException(e);
		} finally {
			if (cis != null) {
				try {
					cis.close();
					Debug.println("Closed input stream for encrypted file " + pm_sEncryptedFileName);
				} catch (IOException e) {
					Debug.printException(e);
					Debug.println("Failed to close input stream for encrypted file " + pm_sEncryptedFileName);
				}
			}
			if (fisFileIn != null) {
				try {
					fisFileIn.close();
					Debug.println("Closed input stream for file " + pm_sEncryptedFileName);
				} catch (IOException e) {
					Debug.printException(e);
					Debug.println("Failed to close input stream for file " + pm_sEncryptedFileName);
				}
			}
			if (os != null) {
				try {
					os.flush();
					Debug.println("Flushing internal output stream");
				} catch (IOException e) {
					Debug.printException(e);
					Debug.println("Failed to flush internal output stream");
				}
				try {
					os.close();
					Debug.println("Closed internal output stream");
				} catch (IOException e) {
					Debug.printException(e);
					Debug.println("Failed to close internal output stream");
				}
			}
		}
		return bRetValue;
	}

	/**
	 * Method for saving a String as an encrypted file
	 * 
	 * @param pm_sFileName
	 *            - String containing the file name to use for the encrypted
	 *            file
	 * @param pm_sEncodedString
	 *            - String containing the data to write to the file.
	 * @return - true if the file creation succeeded, false otherwise
	 */
	protected static boolean bSaveFile(final String pm_sFileName,
			final String pm_sEncodedString) {

		boolean bValidFile = true;
		byte[] baDecodedData = new byte[0];

		try {
			TEPrinter printer = TEPrinter.getPrinter();
			if (printer instanceof GenericPrinter || printer instanceof ScreenPrinter) {
				baDecodedData = pm_sEncodedString.getBytes("UTF-8");	
			} else {
				// thermal printer codes utilize CP437
				baDecodedData = pm_sEncodedString.getBytes("CP437");
			}

		} catch (UnsupportedEncodingException e1) {
			Debug.printStackTrace(e1);
		}
		Debug.println("Converted printer content to byte array. Length = " + baDecodedData.length);
		
		FileOutputStream fosFileOut = null;
		CipherOutputStream cos = null;

		try {
			fosFileOut = new FileOutputStream(pm_sFileName);
			/*
			 * Set the cipher to encryption mode.
			 */
			singleton.cipher.init(Cipher.ENCRYPT_MODE, singleton.skeySpec,
					singleton.ivspec);
			cos = new CipherOutputStream(fosFileOut, singleton.cipher);

			try {
				if (baDecodedData.length > 0) {
					if (!CompileTimeFlags.rcptNoTemplateEncryption()) {
						cos.write(baDecodedData, 0, baDecodedData.length);
						Debug.println("Encrypted and wrote printer content to file " + pm_sFileName);
					} else {
						fosFileOut.write(baDecodedData, 0, baDecodedData.length);
						Debug.println("Wrote printer content to file " + pm_sFileName);
					}
				} else {
					Debug.println("No data to write to file " + pm_sFileName);
					bValidFile = false;
				}
			} catch (final IOException e) {
				Debug.printException(e);
				bValidFile = false;
			} catch (final Exception e) {
				Debug.printException(e);
				return false;
			}
		} catch (final InvalidKeyException e) {
			Debug.printException(e);
			bValidFile = false;
		} catch (InvalidAlgorithmParameterException e) {
			Debug.printStackTrace(e);
			bValidFile = false;
		} catch (final FileNotFoundException e) {
			Debug.printException(e);
			bValidFile = false;
		} finally {
			if (cos != null) {
				try {
					cos.flush();
					Debug.println("Flushing output stream for encrypted file " + pm_sFileName);
				} catch (IOException e) {
					Debug.printException(e);
					Debug.println("Failed to flush output stream for encrypted file " + pm_sFileName);
				}
				try {
					cos.close();
					Debug.println("Closed output stream for encrypted file " + pm_sFileName);
				} catch (IOException e) {
					Debug.printException(e);
					Debug.println("Failed to close output stream for encrypted file " + pm_sFileName);
				}
			}
			if (fosFileOut != null) {
				try {
					fosFileOut.flush();
					Debug.println("Flushing output stream for file " + pm_sFileName);
				} catch (IOException e) {
					Debug.printException(e);
					Debug.println("Failed to flush output stream for close file " + pm_sFileName);
				}
				try {
					fosFileOut.close();
					Debug.println("Closed output stream for file " + pm_sFileName);
				} catch (IOException e) {
					Debug.printException(e);
					Debug.println("Failed to close output stream for file " + pm_sFileName);
				}
			}
		}
		return bValidFile;
	}

	/**
	 * Constructor for the PrtEncryption object.
	 */
	private PrtEncryption() {
		vInitialize(OFFSET_PRT_DATA, null);
	}
}
