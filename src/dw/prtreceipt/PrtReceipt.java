package dw.prtreceipt;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.moneygram.agentconnect.ReceiptSegmentType;

import dw.printing.GenericPrinter;
import dw.printing.ScreenPrinter;
import dw.printing.TEPrinter;
import dw.reprintreceipt.ReprintFile;
import dw.utility.Debug;
import dw.utility.TimeUtility;

/**
 * Class for dealing with downloaded PRT Receipt files
 * 
 * @author w156
 * 
 */
public class PrtReceipt {
	/**
	 * Task to delete the file after its TTL expires.
	 */
	private static class ExpireFileTimerTask extends TimerTask {
		@Override
		public void run() {
			Debug.println("ExpireFileTimerTask:Run:");
			deletePrtFiles();
		}
	}
	
	public static final int PRT_AGENT_1 = 3;
	public static final int PRT_AGENT_2 = 4;
	public static final int PRT_CUST_1 = 5;
	public static final int PRT_CUST_2 = 6;
	public static final int PRT_DISC_1 = 1;
	public static final int PRT_DISC_2 = 2;
	
	public static final boolean PRT_DELETE_FILE = true;
	public static final boolean PRT_DONT_AUTO_DELETE_FILE = false;

	private static final long PRT_MAX_TIME = TimeUtility.MINUTE * 30;
	
	private static final int[] PRT_FN_TYPE_MAP = {0,1,2,1,2,3,4};

	protected static final String[] PRT_FILE_NAMES = { "0000.prt", "0001.prt",
			"0002.prt", "0003.prt", "0004.prt", "0005.prt", "0006.prt" };
	
	private static final int PRT_MAX_TYPE = PRT_FN_TYPE_MAP.length;
	private static final int PRT_MIN_TYPE = 1;
	
	private static ExpireFileTimerTask taskExpireFileTimer = null;
	private static Timer timerExpireFile = new Timer();

	private boolean bLastPrintStatus;
	private String PRT_FILE;
	private ByteArrayOutputStream outputStream;

	public static synchronized void setReprintReceiptFileDeletion() {
		long time = 0;
		for (int i = 1; i < PRT_FILE_NAMES.length; i++) {
			File file = new File(PRT_FILE_NAMES[i]);
			if (file.isFile()) {
				time = Math.max(time, file.lastModified());
			}
		}
		
		if (time > 0) {
			time += PRT_MAX_TIME;
			if (time > System.currentTimeMillis()) {
				vSetupExpireTimer(time - System.currentTimeMillis());
			} else {
				deletePrtFiles();
			}
		}
	}

	/**
	 * Method to delete all PrtReceipt files.
	 */
	public static synchronized void deletePrtFiles() {
		/*
		 * Delete any/all prt print files still present in the system
		 */
		for (int ii = PRT_MIN_TYPE; ii < PRT_MAX_TYPE; ii++) {
			deleteFile(ii);
		}
		/*
		 * Delete temp file if it exists.
		 */
    	ReprintFile.deleteFile(true);
		//
		// Get rid of any old timer tasks
		//
		if (taskExpireFileTimer != null) {
			taskExpireFileTimer.cancel();
			taskExpireFileTimer = null;
		}
	}

	/**
	 * Method to determine if any active prt print files are available for reprint
	 */
	public static boolean reprintAvailable() {
		/*
		 * examine timers for any prt print files still present in the system
		 */
		for (int ii = PRT_MIN_TYPE; ii < PRT_MAX_TYPE; ii++) {
			if (PrtReceipt.isReprintReceiptFileAvailable(ii)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * PrtReceipt constructor if PRT file may already exist
	 * 
	 * @param pm_iRcptType
	 *            - int identifying the receipt type of the PRT file created
	 *            from sEncodedString
	 * @param pm_bDeleteIfFound
	 *            - boolean to control whether to automatically delete the file
	 *            if found.
	 */
	public PrtReceipt(int pm_iRcptType) {
		
		boolean bRetValue = (pm_iRcptType >= PRT_MIN_TYPE) && (pm_iRcptType <= PRT_MAX_TYPE);
		if (bRetValue) {
			PRT_FILE = PRT_FILE_NAMES[PRT_FN_TYPE_MAP[pm_iRcptType]];
		} else {
			PRT_FILE = null;
			String errorMessage = "Invalid PRT File Type [" + pm_iRcptType + "]";
			Debug.println(errorMessage);
			throw new RuntimeException(errorMessage);
		}
		
		bLastPrintStatus = false;
	}
	
	public static boolean isReprintReceiptFileAvailable(int receiptType) {
		int index = PRT_FN_TYPE_MAP[receiptType];
		String fileName = PRT_FILE_NAMES[index];
		return new File(fileName).isFile();
	}

	/**
	 * PrtReceipt constructor
	 * 
	 * @param content
	 *            - ArrayList of ReceiptSegmentType objects containing the receipt data
	 * @param receiptType
	 *            - int identifying the receipt type of the PRT file created
	 *            from sEncodedString
	 */
	public PrtReceipt(List<ReceiptSegmentType> content, int receiptType) throws IOException {
		this(receiptType);
		String sDecodedString = "";
		byte[] baDecodedData = new byte[1];

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		for (ReceiptSegmentType segment : content) {
			ByteArrayInputStream baip = (ByteArrayInputStream) segment.getMimeData().getContent();
			while (true) {
				int c = baip.read(buffer);
				if (c <= 0) {
					break;
				}
				baos.write(buffer, 0, c);
			}
			Debug.println("Reading " + baos.size() + " bytes from AC response for receipt type " + receiptType);
		}
		if (baos.size() > 0) {
			
			try {

			try {
				TEPrinter printer = TEPrinter.getPrinter();
				if (printer instanceof GenericPrinter || printer instanceof ScreenPrinter) {
					
					sDecodedString = baos.toString("UTF-8");
					baDecodedData = sDecodedString.getBytes("UTF-8");	
					
				} else {
					// thermal printer codes utilize CP437
					sDecodedString = baos.toString("CP437");
					baDecodedData = sDecodedString.getBytes("CP437");
				}

			} catch (UnsupportedEncodingException e1) {
				Debug.printStackTrace(e1);
				return;
			}
			
			outputStream = new ByteArrayOutputStream();
			outputStream.write(baDecodedData);
			
			Debug.println("Decoded printer content. Length = " + sDecodedString.length());

			boolean bFileExists = PrtEncryption.bSaveFile(PRT_FILE, sDecodedString);
			if (bFileExists) {
				vSetupExpireTimer(PRT_MAX_TIME);
			}
			} catch (Exception e) {
				Debug.printException(e);
			} finally {
				try {
					outputStream.flush();
					Debug.println("Flushing the internal buffer stream");
				} catch (Exception e) {
					Debug.println("Flushing of the internal buffer stream failed");
				}
				try {
					outputStream.close();
					Debug.println("Closing the internal buffer stream");
				} catch (Exception e) {
					Debug.println("Closing of the internal buffer stream failed");
				}
			}
		}
	}

	/**
	 * Method to delete the encrypted save file used for printing PRT receipts
	 */
	private static synchronized void deleteFile(int receiptType) {
		int index = PRT_FN_TYPE_MAP[receiptType];
		String fileName = PRT_FILE_NAMES[index];
		File fFileId = new File(fileName);
		if (fFileId.isFile()) {
			if (!fFileId.delete()) {
				Debug.println("Deletion of file " + fileName + " failed");
			} else {
				Debug.println("Deleted file " + fileName);
			}
			/*
			 * If deleting agent receipt, also delete old style receipt data
			 */
			if (receiptType == PRT_AGENT_1) {
	        	ReprintFile.deleteFile(true);
			}
		}
	}

	/**
	 * Method to return the last print status for this instance
	 * 
	 * @return - true if last print was successful, false otherwise
	 */
	public boolean getLastPrintStatus() {
		return bLastPrintStatus;
	}

	/**
	 * Method to print a .PRT receipt file
	 * 
	 * @return - true if print was successful, false otherwise
	 */
	public boolean print() {
		if (outputStream == null) {
			outputStream = new ByteArrayOutputStream();
			bLastPrintStatus = PrtEncryption.bMakeDecryptedFile(PRT_FILE, outputStream);
		} else {
			bLastPrintStatus = true;
		}
		if (bLastPrintStatus) {
			bLastPrintStatus = PrtPrint.print(outputStream);
		}
		return bLastPrintStatus;
	}

	/**
	 * Setup a timer to delete the PRT file
	 * 
	 * @param pm_lTimeToLive
	 *            - Long value containing the time to live for the PRT file.
	 */
	private static synchronized void vSetupExpireTimer(long pm_lTimeToLive) {
		//
		// Get rid of any old timer tasks
		//
		if (taskExpireFileTimer != null) {
			taskExpireFileTimer.cancel();
		}
		//
		// Setup expireFileTimerTask to delete the PRT file
		// after its expiration time.
		//
		taskExpireFileTimer = new ExpireFileTimerTask();

		timerExpireFile.schedule(taskExpireFileTimer, pm_lTimeToLive);
	}
}
