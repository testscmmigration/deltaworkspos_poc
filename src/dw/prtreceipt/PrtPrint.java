package dw.prtreceipt;

import java.io.ByteArrayOutputStream;

import dw.printing.EpsonSpooledPrinter;
import dw.printing.EpsonTMT88II;
import dw.printing.TEPrinter;
import dw.utility.Debug;
import dw.utility.ErrorManager;

/**
 * Class for printing PRT receipt files
 * 
 * @author w156
 * 
 */
public class PrtPrint {

	/**
	 * Method to print a PRT file.
	 * 
	 * @param outputStream
	 *            - ByteArrayOutputStream containing the data to send to the printer.
	 * @return - true if print was successful.
	 */
	
	protected static boolean print(ByteArrayOutputStream outputStream) {
		boolean bRetValue = true;

		TEPrinter printer = TEPrinter.getPrinter();
		Debug.println("Printer class: " + printer.getClass().getName());

		try {
			printer.write(outputStream.toByteArray(), 0, outputStream.size());
			Debug.println("Writing " + outputStream.size() + " bytes to the printer");
			printer.fireOffPrintJob();
			// initialize needed for thermal printer (no public visibility)
			if ((printer instanceof EpsonSpooledPrinter) || (printer instanceof EpsonTMT88II)) {
				printer.initialize();
				Debug.println("Initialize the printer");
			}
		} catch (Exception ex) {
			Debug.printException(ex);
			bRetValue = false;
		} finally {
			try {
				outputStream.flush();
				Debug.println("Flushing input stream to the printer");
			} catch (Exception e) {
				Debug.println("Failed to flush input stream to the printer");
				Debug.printException(e);
			}
			try {
				outputStream.close();
				Debug.println("Closed input stream to the printer");
			} catch (Exception e) {
				Debug.println("Failed to closed input stream to the printer");
				Debug.printException(e);
			}
		}

		try {
	        ErrorManager.updateReprintReceiptState();
		} catch (Exception e) {
			Debug.printException(e);
		}

		return bRetValue;
	}
}
