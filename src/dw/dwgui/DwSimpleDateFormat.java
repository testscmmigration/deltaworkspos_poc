package dw.dwgui;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class DwSimpleDateFormat extends SimpleDateFormat{
	private static final long serialVersionUID = 1L;

	static final List<String> BAD_LOCALES = Arrays.asList(new String[] {
			"ar"
		});
	
	static Locale currentLocale = Locale.getDefault();
	
	private static Locale goodLocale() {
		Locale locale;
		if (BAD_LOCALES.contains(currentLocale.getLanguage().toLowerCase(Locale.US))) {
			locale = new Locale("en");
			
		} else {
			locale = Locale.getDefault();
		}
		return locale;
	}
	
    public DwSimpleDateFormat(String pattern, Locale locale)
    {
    	super(pattern, locale);
    }
    
    public DwSimpleDateFormat(String pattern)
    {
        this(pattern, goodLocale());
    }


}
