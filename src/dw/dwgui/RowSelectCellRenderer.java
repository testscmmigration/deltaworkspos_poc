package dw.dwgui;

import java.awt.Component;
import java.util.Enumeration;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

/**
 * Table cell rendererer that allows whole rows to be selected with no editing
 *   box.
 */
public class RowSelectCellRenderer extends DefaultTableCellRenderer {
	private static final long serialVersionUID = 1L;

	@Override
	public Component getTableCellRendererComponent(JTable table,
                                               Object value,
                                               boolean isSelected,
                                               boolean hasFocus,
                                               int row,
                                               int column) {
        return super.getTableCellRendererComponent(table, value,
                                              isSelected, false, row, column);
    }

    public static void setAsDefaultRenderer(JTable table) {
        for (Enumeration<TableColumn> cols = table.getColumnModel().getColumns();
                                                 cols.hasMoreElements(); ) {
            TableColumn col = cols.nextElement();
            col.setCellRenderer(new RowSelectCellRenderer());
        }
    }
}
