package dw.dwgui;

import java.awt.Color;
import java.awt.Component;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import dw.i18n.Messages;

/**
 * HelpBox is a panel the color of the Windows help color. It contains
 *   multiple labels and a Help button lined up vertically.
 */
public class HelpBox extends JPanel {
	private static final long serialVersionUID = 1L;

    static final Color helpColor = new Color(254, 255, 219);
    JButton button = new JButton(Messages.getString("HelpBox.1903")); 
    Vector<JLabel> labels = new Vector<JLabel>();

    public HelpBox() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        setBackground(helpColor);
        setBorder(BorderFactory.createLineBorder(Color.black, 1));
    }

    /**
     * Set up the layout of the components in the panel. Arrange all the labels
     *   vertically, and the help button at the bottom.
     */
    private void config() {
        removeAll();
        addComponent(Box.createVerticalStrut(5));
        for (int i = 0; i < labels.size(); i++) {
            addComponent(labels.elementAt(i));
        }
        addComponent(Box.createVerticalGlue());
        addComponent(button);
        addComponent(Box.createVerticalStrut(5));
    }

    /**
     * Add a component to the help box. If the component to add is a button,
     *   just re-set the reference of the help button to the new button. If it
     *   is anything else, add it normally.
     */
    @Override
	public Component add(Component component) {
        if (component instanceof JButton) {
            button = (JButton)component;
            config();
            return component;
        }
        else {
            return super.add(component);
        }
    }

    /**
     * Add a new label to the help box. Put it in the list and reconfigure the
     *   layout.
     */
    public void addLabel(String text) {
        labels.add(new JLabel(text));
        config();
    }

    /**
     * Put the given component in a horizontal box and center it. Then add the
     *   box to the panel.
     */
    public void addComponent(Component component) {
        JPanel labelPanel = new JPanel();
        labelPanel.setBackground(helpColor);
        labelPanel.setLayout(new BoxLayout(labelPanel, BoxLayout.X_AXIS));
        labelPanel.add(Box.createHorizontalGlue());
        labelPanel.add(component);
        labelPanel.add(Box.createHorizontalGlue());
        add(labelPanel);
    }
}
