package dw.dwgui;


import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.SystemColor;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import dw.dwgui.DWTable.DWTableCellRendererInterface;

/**
 *
 */
public class CustomTableCellRenderer extends DefaultTableCellRenderer implements DWTableCellRendererInterface {
	private static final long serialVersionUID = 1L;

	public static final int MONETARY_HEADER = 0;
    public static final int MONETARY_CELL = 1;
    public static final int CENTERED_HEADER = 2;
    public static final int CENTERED_CELL = 3;
    public static final int LEFT_HEADER = 4;
    public static final int LEFT_CELL = 5;
    public static final int RIGHT_CELL = 6;
    private int type = 0;
    private int leftRightPadding = 0;
    
    private int cellMargin = 0;

    public CustomTableCellRenderer(int type, int leftRightPadding) {
        this.type = type;
        this.leftRightPadding = leftRightPadding;
        this.cellMargin = leftRightPadding;
    }


	@Override
	public int getCellMargin() {
		return cellMargin;
	}

	@Override
	public void setCellMargin(int margin) {
		this.cellMargin = margin;
	}

	@Override
	public Component getTableCellRendererComponent(JTable table,
                                               Object value,
                                               boolean isSelected,
                                               boolean hasFocus,
                                               int row,
                                               int column) {
        Component renderer = null;
        switch (type) {
            case (MONETARY_HEADER) :
                JLabel label = new JLabel((String) value);
//                label.setHorizontalAlignment(JLabel.RIGHT);
                label.setHorizontalAlignment(SwingConstants.CENTER);
                JPanel header = new MarginPanel(label, leftRightPadding, 0);
                header.setBorder(BorderFactory.createRaisedBevelBorder());
                renderer = header;
                break;
            case (MONETARY_CELL) :
                MoneyField mfield = new MoneyField();
                mfield.setText((String) value);
                mfield.setEditable(false);
                mfield.setBorder(BorderFactory.createEmptyBorder());
                mfield.setBorder(BorderFactory.createEmptyBorder(0, cellMargin, 0, cellMargin));
                renderer = mfield;
                break;
            case (CENTERED_HEADER) :
                break;
            case (CENTERED_CELL) :
               /* if (renderer instanceof JComponent) {
                	((JComponent) renderer).setBorder(BorderFactory.createEmptyBorder(0, cellMargin, 0, cellMargin));
                }*/
                break;
            case (LEFT_HEADER) :
                JLabel label2 = new JLabel((String) value);
                label2.setHorizontalAlignment(SwingConstants.CENTER);
                JPanel header2 = new MarginPanel(label2, leftRightPadding, 0);
                header2.setBorder(BorderFactory.createRaisedBevelBorder());
                renderer = header2;
                break;
            case (LEFT_CELL) :
                JTextField tfield = new JTextField();
                tfield.setText((String) value);
                tfield.setEditable(false);
                tfield.setBorder(BorderFactory.createEmptyBorder());
                tfield.setBorder(BorderFactory.createEmptyBorder(0, cellMargin, 0, cellMargin));
                renderer = tfield;
                break;
            case (RIGHT_CELL) :
                tfield = new JTextField();
                tfield.setText((String) value);
                tfield.setEditable(false);
                tfield.setHorizontalAlignment(SwingConstants.RIGHT);
                tfield.setBorder(BorderFactory.createEmptyBorder());
                tfield.setBorder(BorderFactory.createEmptyBorder(0, cellMargin, 0, cellMargin));
                renderer = tfield;
                break;
        }
        if (isSelected) {
            renderer.setBackground(SystemColor.textHighlight);
            renderer.setForeground(SystemColor.textHighlightText);
        }
        //renderer.repaint();
        return renderer;
    }
}

class MarginPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	public MarginPanel(Component component, int leftRightPad, int topBottomPad) {
        setLayout(new BorderLayout());
        JPanel borderPanel = new JPanel();
        borderPanel.setLayout(new BorderLayout());
        borderPanel.setBorder(BorderFactory.createEmptyBorder(
                      topBottomPad, leftRightPad, topBottomPad, leftRightPad));
        borderPanel.add(BorderLayout.CENTER, component);
        add(BorderLayout.CENTER, borderPanel);
    }
}
