package dw.dwgui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JTextField;

public class DWTextField extends JTextField implements DWComponentInterface {
	private static final long serialVersionUID = 1L;

	public static final Color GRAY_PLACEHOLDER_COLOR = new Color(160, 160, 160);
	public static final Color RED_PLACEHOLDER_COLOR = Color.RED;
	
    private boolean handleKeyEvents = true;
    private boolean handleEnter = HANDLE_ENTER;
    private String placeholderText;
    private Color placeholderColor;
    
    @Override
	public void processKeyEvent(KeyEvent e) {
        
        if (!(handleKeyEvents && DWComponent.canProcessKeyEvent(e,handleEnter)))
            super.processKeyEvent(e);
    }
    
    public void addSafeKeyListener(KeyListener l) {
        super.addKeyListener(l);
    }
    
    @Override
	public void addKeyListener(KeyListener l) {
        handleKeyEvents = false;
        super.addKeyListener(l);
    }
    
    public void addSafeActionListener(ActionListener l) {
        super.addActionListener(l);
    }
    
    @Override
	public void addActionListener(ActionListener l) {
        handleKeyEvents = false;
        super.addActionListener(l);
    }

    @Override
	public void setDoNotHandleEnter() {
        handleEnter = DO_NOT_HANDLE_ENTER;
    }
    
    @Override
	public void setHandleEnter() {
        handleEnter = HANDLE_ENTER;
    }

	@Override
	protected void paintComponent(final Graphics pG) {
		super.paintComponent(pG);

		
		if (placeholderText==null || placeholderText.length() == 0 || getText().length() > 0) {
			return;
		}

		final Graphics2D g = (Graphics2D) pG;
		g.setColor(placeholderColor);
		g.drawString(placeholderText, getInsets().left, pG.getFontMetrics().getMaxAscent() + getInsets().top);
	}

	public void setPlaceholder(String placeholder, Color placeholderColor) {
		this.placeholderText = placeholder;
		this.placeholderColor = placeholderColor;
	}
}
