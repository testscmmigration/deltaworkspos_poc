package dw.dwgui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

public class DWTable extends JScrollPane {
	private static final long serialVersionUID = 1L;

	public static final int DEFAULT_COLUMN_MARGIN = 5;
	
	private JTable table;
	private boolean enableFlag;
	public boolean listenerFlag = false;
	private boolean columnMarginChangedEnabled = false;
	private List<DWTableColumn> dwTableColumns;
	private List<DWTableColumn> resizableColumns;
	private int tableWidth;
	private int scrollPaneWidth;
	private JScrollPane screenScrollPane;
	private int horizontalMargin;
	private JPanel screenPanel;
	private int bottomMargin;
	private int actualTableHeight;
	private int minimumTableWidth;
	private int maximumTableHeight;
	private int screenHeight;
	private boolean hasVerticalScrollbar;
	private int defaultTableWidth;
	private List<JTable> supplementalTables;
	private JPanel tablePanel;
	private JScrollPane tableScrollPane;
	private int headerHeight;

	public interface DWTableListener {
		public void dwTableResized();
	}
	
	public interface DWTableCellRendererInterface {
		public int getCellMargin();
		public void setCellMargin(int margin);
	}
	
	public class DWTableCellRenderer extends DefaultTableCellRenderer implements DWTableCellRendererInterface {
		private static final long serialVersionUID = 1L;

		private int margin;
		private int minMargin = 0;
		
		public DWTableCellRenderer(int minMargin) {
			this.minMargin = minMargin;
		}
		
		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			Border b = this.getBorder();
			Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			
			if (row == -1) {
				setBorder(b);
			} else {
				int m = Math.max(margin, minMargin);
				setBorder(BorderFactory.createEmptyBorder(0, m, 0, m));
			}
			return c;
		}

		@Override
		public int getCellMargin() {
			return margin;
		}

		@Override
		public void setCellMargin(int margin) {
			this.margin = margin;
		}
	}
	
	private class DWTableColumn {
		private TableColumn column;
		private boolean resizable;
		private int actualColumnWidth;
		private int defaultColumnWidth;
		private int dataWidth;
	}
	
	public DWTable() {
    	this.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
    	this.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

    	table = new JTable();
    	table.setFocusable(true);
    	table.setOpaque(false);
    	table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    	this.headerHeight = 0;
        
    	this.setViewportView(table);
    	this.setBorder(BorderFactory.createEmptyBorder());
    	table.setBorder(BorderFactory.createEmptyBorder());
    	
    	enableFlag = true;
    	
		table.getTableHeader().setReorderingAllowed(false);
		TableColumnModelListener tcml = new TableColumnModelListener() {

			@Override
			public void columnAdded(TableColumnModelEvent e) {
			}

			@Override
			public void columnRemoved(TableColumnModelEvent e) {
			}

			@Override
			public void columnMoved(TableColumnModelEvent e) {
			}

			@Override
			public void columnMarginChanged(ChangeEvent e) {
				if (columnMarginChangedEnabled) {
					DWTable.this.columnMarginChanged();
				}
			}

			@Override
			public void columnSelectionChanged(ListSelectionEvent e) {
			}
		};
		table.getColumnModel().addColumnModelListener(tcml);
	}
	
	public JTable getTable() {
		return table;
	}
	
	public void addListener(final JPanel panel, final DWTableListener listener) {
		if (! this.listenerFlag) {
			panel.addComponentListener(new ComponentAdapter() {
				@Override
				public void componentShown(ComponentEvent ce) {
					if (enableFlag) {
						listener.dwTableResized();
					}
				}
				
				@Override
				public void componentResized(ComponentEvent ce) {
					if (enableFlag) {
						listener.dwTableResized();
					}
				}
			});
			listenerFlag = true;
		}
	}
	
	public void setListenerEnabled(boolean enableFlag) {
		this.enableFlag = enableFlag;
	}
	
	public void addSupplementalTable(JTable table) {
		if (supplementalTables == null) {
			supplementalTables = new ArrayList<JTable>();
		}
		supplementalTables.add(table);
	}
	
	private int getTableHeight() {
		int tableHeight = table.getTableHeader().getPreferredSize().height;
		if (this.supplementalTables == null) {
			tableHeight += table.getRowCount() * table.getRowHeight() + 2;
		} else {
			JLabel l = new JLabel("t");
			for (int t = 0; t < supplementalTables.size(); t++) {
				JTable supplementalTable = supplementalTables.get(t);
				tableHeight += l.getPreferredSize().height + supplementalTable.getPreferredSize().height;
			}
		}
		return tableHeight;
	}
	
	private int getColumnDataWidth(int column) {
		int width = 0;
		for (int row = 0; row < table.getRowCount(); row++) {
			TableCellRenderer cellRenderer = table.getCellRenderer(row, column);
			Component c = table.prepareRenderer(cellRenderer, row, column);
			int cellWidth = c.getPreferredSize().width + table.getIntercellSpacing().width;
			width = Math.max(cellWidth, width);
		}
		return width;
	}

	public void initializeTable(JScrollPane screenScrollPane, JPanel screenPanel) {
		initializeTable(screenScrollPane, screenPanel, null, null, 0, 0);
	}
	
	public void initializeTable(JScrollPane screenScrollPane, JPanel screenPanel, int minWidth, int scrollPaneMargin) {
		initializeTable(screenScrollPane, screenPanel, null, null, minWidth, scrollPaneMargin);
	}
	
	public void initializeTable(JScrollPane screenScrollPane, JPanel screenPanel, JScrollPane tableScrollPane, JPanel tablePanel, int minWidth, int horizontalMargin) {
		this.screenScrollPane = screenScrollPane;
		this.screenPanel = screenPanel;
		this.tableScrollPane = tableScrollPane;
		this.tablePanel = tablePanel;
		this.horizontalMargin = horizontalMargin;
		this.minimumTableWidth = minWidth;
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		int maxWidth = screenScrollPane.getVisibleRect().width - horizontalMargin;
		this.resizableColumns = new ArrayList<DWTableColumn>();

		dwTableColumns = new ArrayList<DWTableColumn>();
		
		defaultTableWidth = 0;

		for (int column = 0; column < table.getColumnCount(); column++) {
			
			TableColumn tableColumn = table.getColumnModel().getColumn(column);
			TableCellRenderer renderer = tableColumn.getCellRenderer();
			if (renderer instanceof DWTableCellRendererInterface) {
				((DWTableCellRendererInterface) renderer).setCellMargin(DEFAULT_COLUMN_MARGIN);
			}
			
			int defaultColumnWidth = 0;

			TableCellRenderer headerRenderer = tableColumn.getHeaderRenderer();
			if ((headerRenderer == null) && (table.getTableHeader() != null)) {
				headerRenderer = table.getTableHeader().getDefaultRenderer();
			}

			if (headerRenderer != null) {
				Component c = headerRenderer.getTableCellRendererComponent(table, tableColumn.getHeaderValue(), false, false, -1, column);
				int headerWidth = c.getPreferredSize().width + table.getIntercellSpacing().width;
				defaultColumnWidth = headerWidth;
				this.headerHeight = Math.max(this.headerHeight, c.getPreferredSize().height);
			}

			DWTableColumn dwColumn = new DWTableColumn();

			dwColumn.dataWidth = getColumnDataWidth(column);
			defaultColumnWidth = Math.max(defaultColumnWidth, dwColumn.dataWidth);

			dwColumn.column = tableColumn;
			dwColumn.defaultColumnWidth = defaultColumnWidth;
			dwColumn.resizable = true;
			dwTableColumns.add(dwColumn);
			
			defaultTableWidth += defaultColumnWidth;
		}
		
		resizableColumns.addAll(dwTableColumns);
		
		// Set header size.
		
		tableWidth = Math.max(minWidth, Math.min(defaultTableWidth, maxWidth));
		Dimension d = new Dimension(tableWidth, this.headerHeight);
		table.getTableHeader().setPreferredSize(d);

		// Set table size.
		
		actualTableHeight = this.getTableHeight();
		d = new Dimension(tableWidth, actualTableHeight);
		table.setPreferredSize(d);
	}
	
	public void setColumnStaticWidth(int column) {
		DWTableColumn dwColumn = dwTableColumns.get(column);
		dwColumn.resizable = false;
		resizableColumns.remove(dwColumn);
	}

	public void resizeTable() {

		// If no columns have been defined for the table, do nothing.
		
		if (dwTableColumns == null) {
			return;
		}
		
		// Calculate the width of the table when all columns have the default margin and no text has been truncated.
		
		for (int i = 0; i < dwTableColumns.size(); i++) {
			DWTableColumn dwColumn = dwTableColumns.get(i);
			TableCellRenderer renderer = dwColumn.column.getCellRenderer();
			if (renderer instanceof DWTableCellRendererInterface) {
				((DWTableCellRendererInterface) renderer).setCellMargin(DEFAULT_COLUMN_MARGIN);
			}
		}		

		tableWidth = defaultTableWidth;

		// Calculate the maximum and actual width of the table based upon the 
		// current screen width and the minimum width of the screens content.
		
		final int maxWidth = screenScrollPane.getVisibleRect().width - horizontalMargin;
		final int actualWidth = Math.min(Math.max(tableWidth, minimumTableWidth), maxWidth);
		
		// Disable the table resize listener and the column margin change listener 
		// so they will not be called during the table resize process.
		// Also save the current value of the table resize listener so it can 
		// be restore once the table resize process is complete.
		
		boolean currentEnableFlag = enableFlag;
		enableFlag = false;
		columnMarginChangedEnabled = false;
		
		// If the minimum table width was specified as 0, then the table is not
		// resizeable. Just set the size to it's default size.
		
		if (minimumTableWidth == 0) {
			int height = this.getTableHeight();
			Dimension tableSize = new Dimension(tableWidth, height);
			setPreferredSize(tableSize);
			table.setPreferredSize(tableSize);
			enableFlag = currentEnableFlag;
			return;
		}
		
		// Turn off the displaying of the vertical scroll bar so the screen height 
		// dimensions can properly be calculated.
		
		this.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

		// Calculate the screen height dimensions
		
		screenHeight = this.getTableHeight() + screenScrollPane.getPreferredSize().height - this.getPreferredSize().height;
		int tableViewportHeight = screenScrollPane.getVisibleRect().height;
		bottomMargin = tableViewportHeight - screenHeight;

		// Check the height of the table and determine if a vertical scroll bar
		// is need. If a vertical scroll bar is need, the width of the table will
		// also need to be adjusted.
		
		hasVerticalScrollbar = false;
		if (tableViewportHeight < screenHeight) {
			this.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			int scrollBarWidth = getVerticalScrollBar().getPreferredSize().width;
			if ((maxWidth - actualWidth) > scrollBarWidth) {
				tableWidth = actualWidth;
				scrollPaneWidth = actualWidth + scrollBarWidth;
			} else {
				tableWidth = maxWidth - scrollBarWidth;
				scrollPaneWidth = maxWidth;
			}
			hasVerticalScrollbar = true;
		} else {
			tableWidth = actualWidth;
			scrollPaneWidth = actualWidth;
		}

		// When the table is resized, the horizontal scroll bar will never show
		// because the width of any resizeable column will be changed so the 
		// table can fit on the screen.
		
		setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		// Check the width of the table.
		
		if (tableWidth > defaultTableWidth) {
			
			// If the desired width of the table is wider than the default width
			// of the table, then add extra margin space to each column to extend
			// the width of the table.

			int widthUnder = tableWidth - defaultTableWidth;
			for (int i = 0; i < dwTableColumns.size(); i++) {
				
				DWTableColumn dwColumn = dwTableColumns.get(i);

				int n = dwTableColumns.size() - i;
				int extraMargin = widthUnder / n;
				int columnMargin = extraMargin / 2;
				if (dwColumn.dataWidth < dwColumn.defaultColumnWidth) {
					columnMargin += ((dwColumn.defaultColumnWidth - dwColumn.dataWidth) / 2) + DEFAULT_COLUMN_MARGIN;
				}
				widthUnder -= extraMargin;
				
				TableCellRenderer renderer = dwColumn.column.getCellRenderer();
				if (renderer instanceof DWTableCellRendererInterface) {
					((DWTableCellRendererInterface) renderer).setCellMargin(columnMargin);
				}
				int columnWidth = dwColumn.defaultColumnWidth + extraMargin;
				dwColumn.column.setMinWidth(columnWidth);
				dwColumn.column.setMaxWidth(columnWidth);
				dwColumn.column.setWidth(columnWidth);
				dwColumn.column.setResizable(false);
			}
		} else {
			
			// If the desired width of the table is less than the default width
			// of the table, then reduce the width of a resizable column. The
			// table can have any number of resizable columns and the widest
			// column will be reduced is width.
			
			int widthOver = defaultTableWidth - tableWidth;
			for (int i = 0; i < dwTableColumns.size(); i++) {
				DWTableColumn dwColumn = dwTableColumns.get(i);
				dwColumn.actualColumnWidth = dwColumn.defaultColumnWidth;
			}
			
			resizeColumns(resizableColumns, widthOver);

			for (int i = 0; i < dwTableColumns.size(); i++) {
				DWTableColumn dwColumn = dwTableColumns.get(i);
				if (dwColumn.resizable) {
					dwColumn.column.setMinWidth(dwColumn.actualColumnWidth);
					dwColumn.column.setMaxWidth(dwColumn.defaultColumnWidth);
					dwColumn.column.setPreferredWidth(dwColumn.actualColumnWidth);
					dwColumn.column.setResizable(dwColumn.actualColumnWidth != dwColumn.defaultColumnWidth);
				} else {
					dwColumn.column.setMinWidth(dwColumn.defaultColumnWidth);
					dwColumn.column.setMaxWidth(dwColumn.defaultColumnWidth);
					dwColumn.column.setPreferredWidth(dwColumn.defaultColumnWidth);
					dwColumn.column.setResizable(false);
				}
			}
		}
		
		// Calculate the maximum height of the table based upon the current screen size.
		
		maximumTableHeight = actualTableHeight - screenHeight + tableViewportHeight; 
		
		// Set the preferred size of the scroll pane and table. 
		
		if (! hasVerticalScrollbar) {
			Dimension tableSize = new Dimension(tableWidth, actualTableHeight);
			Dimension paneSize = new Dimension(scrollPaneWidth, actualTableHeight);
			table.setPreferredSize(tableSize);
			this.setPreferredSize(paneSize);
		} else {
			Dimension tableSize = new Dimension(tableWidth, actualTableHeight - table.getTableHeader().getPreferredSize().height);
			Dimension paneSize = new Dimension(scrollPaneWidth, maximumTableHeight);
			table.setPreferredSize(tableSize);
			this.setPreferredSize(paneSize);
		}
		screenPanel.validate();

		// Set the width of the viewport that displays the table.
		
		this.getVisibleRect().width = screenScrollPane.getVisibleRect().width - horizontalMargin;
		this.revalidate();
		
		// Enable the column marign change listener and restore table resize listener to it's original value.
		
		columnMarginChangedEnabled = true;
		enableFlag = currentEnableFlag;
	}
	
	public void resizeSupplementalTables() {
		if (supplementalTables != null) {
			
			// Loop through each supplemental table.
			
			for (int t = 0; t < supplementalTables.size(); t++) {
				JTable supplementalTable = supplementalTables.get(t);
				
				// Set the column size.
				
				for (int c = 0; c < dwTableColumns.size(); c++) {
					TableColumn column1 = (dwTableColumns.get(c)).column;
					TableColumn column2 = supplementalTable.getColumnModel().getColumn(c);
					column2.setMaxWidth(column1.getMaxWidth());
					column2.setMinWidth(column1.getMinWidth());
					column2.setPreferredWidth(column1.getPreferredWidth());
					column2.setResizable(column1.getResizable());
				}
				
				//
				
				int h = supplementalTable.getPreferredSize().height;
				Dimension d = new Dimension(this.getPreferredSize().width, h);
				supplementalTable.setPreferredSize(d);
			}
		}
	}
	
	private void columnMarginChanged() {
		if (supplementalTables == null) {

			// Set the preferred size of the table to null to allow resizing.
			
			table.setPreferredSize(null);
			
			int tableWidth = table.getPreferredSize().width;
			
			if (tableWidth > this.tableWidth) {
				setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
				int scrollBarHeight = getHorizontalScrollBar().getPreferredSize().height;
				if (bottomMargin > scrollBarHeight) {
					Dimension paneSize = new Dimension(scrollPaneWidth, actualTableHeight + scrollBarHeight);
					this.setPreferredSize(paneSize);
				} else {
					Dimension paneSize = new Dimension(scrollPaneWidth, maximumTableHeight);
					this.setPreferredSize(paneSize);
				}
			} else {
				setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
				if (! hasVerticalScrollbar) {
					Dimension paneSize = new Dimension(scrollPaneWidth, actualTableHeight);
					this.setPreferredSize(paneSize);
				} else {
					Dimension paneSize = new Dimension(scrollPaneWidth, maximumTableHeight);
					this.setPreferredSize(paneSize);
				}
			}
			screenPanel.validate();
		} else {
			tablePanel.setPreferredSize(null);
			for (int t = 0; t < supplementalTables.size(); t++) {
				JTable supplementalTable = supplementalTables.get(t);
				supplementalTable.setPreferredSize(null);
			}
			
			int tableWidth = tableScrollPane.getViewport().getPreferredSize().width;
			
			if (tableWidth > this.tableWidth) {
				tableScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
				int scrollBarHeight = tableScrollPane.getHorizontalScrollBar().getPreferredSize().height;
				if (bottomMargin > scrollBarHeight) {
					Dimension paneSize = new Dimension(this.tableWidth, actualTableHeight + scrollBarHeight);
					tableScrollPane.setPreferredSize(paneSize);
				} else {
					Dimension paneSize = new Dimension(this.tableWidth, maximumTableHeight);
					tableScrollPane.setPreferredSize(paneSize);
				}
				
			} else {
				tableScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
				if (! hasVerticalScrollbar) {
					Dimension paneSize = new Dimension(this.tableWidth, actualTableHeight);
					tableScrollPane.setPreferredSize(paneSize);
				} else {
					Dimension paneSize = new Dimension(this.tableWidth, maximumTableHeight);
					tableScrollPane.setPreferredSize(paneSize);
				}
			}
			tablePanel.doLayout();
			tablePanel.revalidate();
			screenPanel.doLayout();
			screenPanel.revalidate();
		}
	}

	public void resizeTablePanel(JScrollPane scrollPane2, JPanel tablePanel) {
		
		// If no columns have been defined for the table, do nothing.
		
		if (dwTableColumns == null) {
			return;
		}
		
		// Calculate the width of the table when all columns have the default margin and no text has been truncated.
		
		for (int i = 0; i < dwTableColumns.size(); i++) {
			DWTableColumn dwColumn = dwTableColumns.get(i);
			TableCellRenderer renderer = dwColumn.column.getCellRenderer();
			if (renderer instanceof DWTableCellRendererInterface) {
				((DWTableCellRendererInterface) renderer).setCellMargin(DEFAULT_COLUMN_MARGIN);
			}
		}		

		tableWidth = defaultTableWidth;

		// Calculate the maximum and actual width of the table based upon the 
		// current screen width and the minimum width of the screens content.
		
		final int maxWidth = screenScrollPane.getVisibleRect().width - horizontalMargin;
		final int actualWidth = Math.min(Math.max(tableWidth, minimumTableWidth), maxWidth);
		
		// Disable the table resize listener and the column margin change listener 
		// so they will not be called during the table resize process.
		// Also save the current value of the table resize listener so it can 
		// be restore once the table resize process is complete.
		
		boolean currentEnableFlag = enableFlag;
		enableFlag = false;
		columnMarginChangedEnabled = false;
		
//		// If the minimum table width was specified as 0, then the table is not
//		// resizeable. Just set the size to it's default size.
//		
//		if (minimumTableWidth == 0) {
//			int height = this.getTableHeight(true);
//			Dimension tableSize = new Dimension(tableWidth, height);
//			setPreferredSize(tableSize);
//			table.setPreferredSize(tableSize);
//			enableFlag = currentEnableFlag;
//			return;
//		}
		
		// Turn off the displaying of the vertical scroll bar so the screen height 
		// dimensions can properly be calculated.
		
		scrollPane2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		screenPanel.invalidate();

		// Calculate the screen height dimensions
		
		screenHeight = tablePanel.getPreferredSize().height + screenScrollPane.getPreferredSize().height - scrollPane2.getPreferredSize().height;
		int tableViewportHeight = screenScrollPane.getVisibleRect().height - scrollPane2.getColumnHeader().getPreferredSize().height;
		bottomMargin = tableViewportHeight - screenHeight;

		// Calculate the screen height dimensions
		

		// Check the height of the table and determine if a vertical scroll bar
		// is need. If a vertical scroll bar is need, the width of the table will
		// also need to be adjusted.
		
		hasVerticalScrollbar = false;
		if (tableViewportHeight < screenHeight) {
			scrollPane2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			int scrollBarWidth = getVerticalScrollBar().getPreferredSize().width;
			if ((maxWidth - actualWidth) > scrollBarWidth) {
				tableWidth = actualWidth;
				scrollPaneWidth = actualWidth + scrollBarWidth;
			} else {
				tableWidth = maxWidth - scrollBarWidth;
				scrollPaneWidth = maxWidth;
			}
			hasVerticalScrollbar = true;
		} else {
			tableWidth = actualWidth;
			scrollPaneWidth = actualWidth;
		}

		// When the table is resized, the horizontal scroll bar will never show
		// because the width of any resizeable column will be changed so the 
		// table can fit on the screen.
		
		tableScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		// Check the width of the table.
		
		if (tableWidth > defaultTableWidth) {
			
			// If the desired width of the table is wider than the default width
			// of the table, then add extra margin space to each column to extend
			// the width of the table.

			int widthUnder = tableWidth - defaultTableWidth;
			for (int i = 0; i < dwTableColumns.size(); i++) {
				
				DWTableColumn dwColumn = dwTableColumns.get(i);

				int n = dwTableColumns.size() - i;
				int extraMargin = widthUnder / n;
				int columnMargin = extraMargin / 2;
				if (dwColumn.dataWidth < dwColumn.defaultColumnWidth) {
					columnMargin += ((dwColumn.defaultColumnWidth - dwColumn.dataWidth) / 2) + DEFAULT_COLUMN_MARGIN;
				}
				widthUnder -= extraMargin;
				
				TableCellRenderer renderer = dwColumn.column.getCellRenderer();
				if (renderer instanceof DWTableCellRendererInterface) {
					((DWTableCellRendererInterface) renderer).setCellMargin(columnMargin);
				}
				int columnWidth = dwColumn.defaultColumnWidth + extraMargin;
				dwColumn.column.setMinWidth(columnWidth);
				dwColumn.column.setMaxWidth(columnWidth);
				dwColumn.column.setWidth(columnWidth);
				dwColumn.column.setResizable(false);
			}
		} else {
			
			// If the desired width of the table is less than the default width
			// of the table, then reduce the width of a resizable column. The
			// table can have any number of resizable columns and the widest
			// column will be reduced is width.
			
			int widthOver = defaultTableWidth - tableWidth;
			for (int i = 0; i < dwTableColumns.size(); i++) {
				DWTableColumn dwColumn = dwTableColumns.get(i);
				dwColumn.actualColumnWidth = dwColumn.defaultColumnWidth;
			}
			
			resizeColumns(resizableColumns, widthOver);

//			while (widthOver > 0) {
//				int widestColumnWidth = Integer.MIN_VALUE;
//				DWTableColumn widestColumn = null;
//				for (int i = 0; i < dwTableColumns.size(); i++) {
//					DWTableColumn dwColumn = (DWTableColumn) dwTableColumns.get(i);
//					if (dwColumn.resizable) {
//						if (dwColumn.actualColumnWidth > widestColumnWidth) {
//							widestColumn = dwColumn;
//							widestColumnWidth = dwColumn.actualColumnWidth;
//						}
//					}
//				}				
//				widestColumn.actualColumnWidth--;
//				widthOver--;
//			}
			for (int i = 0; i < dwTableColumns.size(); i++) {
				DWTableColumn dwColumn = dwTableColumns.get(i);
				if (dwColumn.resizable) {
					dwColumn.column.setMinWidth(dwColumn.actualColumnWidth);
					dwColumn.column.setMaxWidth(dwColumn.defaultColumnWidth);
					dwColumn.column.setPreferredWidth(dwColumn.actualColumnWidth);
					dwColumn.column.setResizable(dwColumn.actualColumnWidth != dwColumn.defaultColumnWidth);
				} else {
					dwColumn.column.setMinWidth(dwColumn.defaultColumnWidth);
					dwColumn.column.setMaxWidth(dwColumn.defaultColumnWidth);
					dwColumn.column.setPreferredWidth(dwColumn.defaultColumnWidth);
					dwColumn.column.setResizable(false);
				}
			}
		}
		
		// Calculate the maximum height of the table based upon the current screen size.
		
		maximumTableHeight = actualTableHeight - screenHeight + tableViewportHeight; 
		
		// Set the preferred size of the scroll pane and table. 
		
		if (! hasVerticalScrollbar) {
			Dimension tableSize = new Dimension(tableWidth, actualTableHeight - scrollPane2.getColumnHeader().getPreferredSize().height);
			Dimension paneSize = new Dimension(scrollPaneWidth, actualTableHeight);
			tablePanel.setPreferredSize(tableSize);
			scrollPane2.setPreferredSize(paneSize);
		} else {
			Dimension tableSize = new Dimension(tableWidth, actualTableHeight - scrollPane2.getColumnHeader().getPreferredSize().height);
			Dimension paneSize = new Dimension(scrollPaneWidth, maximumTableHeight);
			tablePanel.setPreferredSize(tableSize);
			scrollPane2.setPreferredSize(paneSize);
		}
		if (this.supplementalTables != null) {
			resizeSupplementalTables();
		}
		screenPanel.validate();

		// Set the width of the viewport that displays the table.
		
		this.getVisibleRect().width = screenScrollPane.getVisibleRect().width - horizontalMargin;
		this.revalidate();
		
		// Enable the column marign change listener and restore table resize listener to it's original value.
		
		columnMarginChangedEnabled = true;
		enableFlag = currentEnableFlag;
	}
	
	private void resizeColumns(List<DWTableColumn> resizableColumns, int widthOver) {
		
		if (resizableColumns.size() == 0) {
			return;
		}
		
		// Loop while the table needs to be reduced in width.
		
		while (widthOver > 0) {
			int widestWidth = 0;
			int margin = Integer.MAX_VALUE;
			int widestCount = 0;
			
			// Determine the widest column width, the number of columns that are 
			// that width, and the different width from the next widest column. 
			
			for (int i = 0; i < resizableColumns.size(); i++) {
				DWTableColumn dwColumn = resizableColumns.get(i);
				if (dwColumn.actualColumnWidth == widestWidth) {
					widestCount++;
				} else {
					margin = Math.min(margin, Math.abs(widestWidth - dwColumn.actualColumnWidth));
					if (dwColumn.actualColumnWidth > widestWidth) {
						widestCount = 1;
						widestWidth = dwColumn.actualColumnWidth;
					} 
				}
			}
			
			// Determine how much to reduce the widest columns.

			int reduceWidth = Math.min(((widthOver + widestCount - 1) / widestCount), margin);
			
			// Loop through the resizable columns and reduce the width of the widest columns.
			
			for (int i = 0; i < resizableColumns.size(); i++) {
				DWTableColumn dwColumn = resizableColumns.get(i);
				if (dwColumn.actualColumnWidth == widestWidth) {
					reduceWidth = Math.min(reduceWidth, widthOver);
					dwColumn.actualColumnWidth -= reduceWidth;
					widthOver -= reduceWidth;
				}
			}
		}
	}
}
