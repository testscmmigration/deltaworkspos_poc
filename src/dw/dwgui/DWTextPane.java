package dw.dwgui;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.Locale;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.plaf.basic.BasicHTML;
import javax.swing.text.View;

import dw.gui.GuiElement;
import dw.i18n.Messages;
import dw.utility.Debug;

public class DWTextPane extends JLabel implements DWTextString {
	private static final long serialVersionUID = 1L;
	
	private static final Font FONT = (new JLabel()).getFont();
	private static final String HTML_DOCUMENT_START = "<HTML><HEAD></HEAD><BODY>";
	private static final String HTML_DOCUMENT_END = "</BODY></HTML>";
	private static final String BODY_FONT_RULE = "body { font-family: " + FONT.getFamily() + "; " + "font-size: " + FONT.getSize() + "pt; }";
	private static final String CODE_FONT_RULE = "code { font-family: " + FONT.getFamily() + "; " + "font-size: " + FONT.getSize() + "pt; }";
	private static final String STYLE_TAG = "<STYLE> " + BODY_FONT_RULE + " " + CODE_FONT_RULE + " </STYLE>";
	private static final Pattern BR_TAG = Pattern.compile("<[Bb][Rr]\\s*/>");
	private static final Pattern HR_TAG = Pattern.compile("<[Hh][Rr]\\s*/>");
	private static final Pattern SPAN_START_TAG = Pattern.compile("<[Ss][Pp][Aa][Nn]");
	private static final Pattern SPAN_END_TAG = Pattern.compile("</[Ss][Pp][Aa][Nn]>");
	private static final String EMPTY_HTML = "<HTML><HEAD></HEAD><BODY></BODY></HTML>";
	
	private String text;
	private int textLength;
	private boolean enableFlag;
	private ComponentListener componentListener;
	private boolean knownWidth = false;
	private boolean invalidContentDisplayFlag = false;

	public interface DWTextPaneListener {
		public void dwTextPaneResized();
	}

	public DWTextPane() {
		setFocusable(false);
		setOpaque(false);
		setVisible(true);
		setBorder(BorderFactory.createEmptyBorder());
		enableFlag = true;
	}

	/**
	 * Returns the preferred size to set a component based on the text length
	 * and preferred component width.
	 */
	public Dimension getPreferredSize(int prefComponentWidth) {
		if (this.text == null) {
			return new Dimension(1, 1);
		}
		this.revalidate();
		
		// Attempt to create a view renderer to use in calculating the preferred size.
		
		View view = null;
		try {
			view = BasicHTML.createHTMLView(this, this.text);
			BasicHTML.updateRenderer(this, this.text);
			
		// If the creation of the view renderer failed, change the text so a 
		// message is displayed indicating the content of the message is invalid.
		
		} catch (Exception e1) {
			try {
				if (! this.text.equals(EMPTY_HTML)) {
					Debug.println("Invalid HTML: " + this.text);
				}
				if (this.invalidContentDisplayFlag) {
					this.text = HTML_DOCUMENT_START + Messages.getString("BroadcastMessagesWizardPage1.5") + HTML_DOCUMENT_END;
				} else {
					this.text = "";
				}
				view = BasicHTML.createHTMLView(this, this.text);
				BasicHTML.updateRenderer(this, this.text);
			} catch (Exception e2) {
			}
		}
		
		// Set the size in the view renderer so it will calculate the preferred 
		// size of the text pane. 

		view.setSize(prefComponentWidth, Integer.MAX_VALUE);
		
		// Get the preferred size of the text pane.

		float widthSpanedByText = view.getPreferredSpan(View.X_AXIS);
		float heightSpanedByText = view.getPreferredSpan(View.Y_AXIS);
		
		// If the view renderer returns a height less than or equal to 0 and 
		// there is text associated with the text pane, change the text so a 
		// message is displayed indicating the content of the message is invalid. 
		
		if ((heightSpanedByText <= 0.0) && (this.textLength > 0)) {
			if (! this.text.equals(EMPTY_HTML)) {
				Debug.println("Invalid HTML: " + this.text);
			}
			if (this.invalidContentDisplayFlag) {
				this.text = HTML_DOCUMENT_START + Messages.getString("BroadcastMessagesWizardPage1.5") + HTML_DOCUMENT_END;
			}
			try {
				view = BasicHTML.createHTMLView(this, this.text);
				BasicHTML.updateRenderer(this, this.text);
				heightSpanedByText = view.getPreferredSpan(View.Y_AXIS);
			} catch (Exception e) {
			}
		}
		
		// Return the size of the text pane as a Dimension object.
		
		return new Dimension((int) Math.ceil(widthSpanedByText), (int) Math.ceil(heightSpanedByText));
	}
	
	@Override
	public void setText(String text) {
		
		String string = text != null ? text : "";
		
		this.textLength = string.length();
		String html;
		
		// If the content of the text pane is specified as HTML, then perform 
		// some changes to make the HTML display correct in a JLabel component.
		
		if (string.toUpperCase(Locale.US).startsWith("<HTML")) {
			html = string.replaceAll(BR_TAG.pattern(), "<BR>");
			html = html.replaceAll(HR_TAG.pattern(), "<HR>");
			html = html.replaceAll(SPAN_START_TAG.pattern(), "<CODE");
			html = html.replaceAll(SPAN_END_TAG.pattern(), "</CODE>");
			
			int i = html.toLowerCase().indexOf("</head>");
			if (i != -1) {
				html = html.substring(0, i) + STYLE_TAG + html.substring(i);
			}
		
		// If the content of the text pane is specified as text, then add HTML 
		// document tags to the begining and end of the text
		
		} else {
			html = HTML_DOCUMENT_START + string.trim() + HTML_DOCUMENT_END;
		}
		
		this.text = html;
		if (knownWidth) {
			super.setText(html);
		}
	}

	public void setWidth(int width) {
		if (width <= 0) {
			return;
		}
		Dimension dmn = getPreferredSize(width);
		setSize(dmn);
		if (! knownWidth) {
			super.setText(this.text);
		}
		setSize(width, Short.MAX_VALUE);
		setPreferredSize(dmn);
		repaint();
		knownWidth = true;
	}

	public ComponentListener addListener(final JPanel panel, final DWTextPaneListener listener) {

		componentListener = new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent ce) {
				if (enableFlag) {
					listener.dwTextPaneResized();
				}
			}
		};
		panel.addComponentListener(componentListener);
		return componentListener;
	}

	public void removeListener(JPanel panel) {
		if (componentListener != null) {
			panel.removeComponentListener(componentListener);
		}
	}

	public void setListenerEnabled(boolean enableFlag) {
		this.enableFlag = enableFlag;
	}

	public void setInvalidContentDisplayFlag(boolean invalidContentDisplayFlag) {
		this.invalidContentDisplayFlag = invalidContentDisplayFlag;
	}
	
	public void setBold(boolean flag) {
		this.setFont(flag ? GuiElement.BOLD_FONT : GuiElement.PLAIN_FONT);
	}
}
