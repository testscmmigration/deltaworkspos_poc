package dw.dwgui;

import javax.swing.table.DefaultTableModel;

public class UneditableTableModel extends DefaultTableModel {
	private static final long serialVersionUID = 1L;


    public UneditableTableModel(Object[] colNames, int rows) {
        super(colNames, rows);
    }

    public UneditableTableModel(Object[][] data, Object[] colNames) {
        super(data, colNames);
    }

    @Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

}
