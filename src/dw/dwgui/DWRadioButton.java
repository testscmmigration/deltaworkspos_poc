package dw.dwgui;

import java.awt.event.KeyEvent;

import javax.swing.JRadioButton;

public class DWRadioButton extends JRadioButton implements DWComponentInterface {
	private static final long serialVersionUID = 1L;

    private boolean handleEnter = HANDLE_ENTER;

    @Override
	public void processKeyEvent(KeyEvent e) {
        if (!DWComponent.canProcessKeyEvent(e,handleEnter))
            super.processKeyEvent(e);
    }

    @Override
	public void setDoNotHandleEnter() {
        handleEnter = DO_NOT_HANDLE_ENTER;
    }
    
    @Override
	public void setHandleEnter() {
        handleEnter = HANDLE_ENTER;
    }
}
