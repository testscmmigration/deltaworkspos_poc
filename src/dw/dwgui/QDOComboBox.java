/*
 * QDOComboBox.java
 * 
 * Copyright (c) 2005 MoneyGram International
 */

package dw.dwgui;

import java.util.HashMap;
import java.util.List;

import javax.swing.DefaultComboBoxModel;

import dw.model.adapters.CountryInfo.Country;
import dw.utility.ServiceOption;

/**
 * Drop down list that displays a different set of delivery options 
 * depending on the selected country.
 * @author Geoff Atkin
 */
public class QDOComboBox extends AbstractMultiListComboBox<ServiceOption> {
	private static final long serialVersionUID = 1L;

	private Country currentCountry = null; 
    private HashMap<String, DefaultComboBoxModel> map = new HashMap<String, DefaultComboBoxModel>();

    /**
     * Constructs a new QDO combo box.
     */
    public QDOComboBox() {
        super();
        setEditable(false);
    }

    /* (non-Javadoc)
     * @see javax.swing.JComboBox#removeAllItems()
     */
    @Override
	public void removeAllItems() {
        setCurrentList(null);
    }   
    
    /**
     * Changes the dropdown to show the delivery options appropriate to the
     * given country. If the given key is null, change the dropdown to an 
     * empty list.
     * @throws ClassCastException if key is not an instance of CountryInfo
     */
    @Override
	public void setCurrentList(Object key) {
        if (key == null) {
            currentCountry = null;
            //setModel(null);
            setModel(new DefaultComboBoxModel());
            map = new HashMap<String, DefaultComboBoxModel>();
            return;
        }
        
        Country country = (Country) key;
        if (country == currentCountry) {
            return;
        }
        String countryCode = country.getCountryCode();
        
        DefaultComboBoxModel model = map.get(countryCode);
        if (model == null) {
            model = loadModel(countryCode);
            map.put(countryCode, model);
        }
        setModel(model);
        currentCountry = country;
    }

    /**
     * Selects the given QDO. Has no effect if the QDO is not on the list 
     * of available qualified delivery options for the current country.  
     */
    public void setSelectedQDO(ServiceOption qdo) {
        setSelectedItem(qdo);
    }
    
    /**
     * Selects the given QDO. Has no effect if the QDO is not on the list 
     * of available qualified delivery options for the current country.  
     */
    public void setHistoryQDO(ServiceOption qdo) {
    	int index = 0;
    	Object oldSelection = selectedItemReminder;
    	if (oldSelection == null || !oldSelection.equals(qdo)) {
    	    if (qdo != null && !isEditable()) {
	    		// For non editable combo boxes, an invalid selection
	    		// will be rejected.
	    		for (int i = 0; i < dataModel.getSize(); i++) {
	    		    if (qdo.toString().trim().equals(dataModel.getElementAt(i).toString().trim())) {
	    		    	index= i;
	    				break;
	    		    }
	    		}
    	    }
    	    
    	}
    	setSelectedIndex(index);
    }
    
    /** Returns the selected QDO. */
    public ServiceOption getSelectedQDO() {
        return (ServiceOption) getSelectedItem();
    }
    
    /**
     * Returns the combo box model for the given country.
     */
    private DefaultComboBoxModel loadModel(String countryCode) {
        List<ServiceOption> list = ServiceOption.getServiceOptions(countryCode);
        DefaultComboBoxModel model = new DefaultComboBoxModel(list.toArray());
        
        return model;
    }
}
