package dw.dwgui;

import javax.swing.JLabel;

public class DWLabel extends JLabel implements DWTextString {
	private static final long serialVersionUID = 1L;

    private String textProperty;

    public String getTextProperty() {
		return textProperty;
	}

	public void setTextProperty(String textProperty) {
		this.textProperty = textProperty;
	}

}
