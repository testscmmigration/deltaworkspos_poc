package dw.dwgui;

import dw.profile.ProfileItem;
import dw.simplepm.SimplePMTransaction;

public class FeeField extends MoneyField{
	private static final long serialVersionUID = 1L;

    /** Construct a new money field with the default value. */
    public FeeField() {
        super(); 
    }

    /** 
     * Set path to profile value 
     */
    @Override
	public void setPath(String path) {
        super.setPath(path);
        if (path != null) {
        	ProfileItem profileItem = SimplePMTransaction.getCurrentInstance().get(path);
        	if (profileItem != null) {
                setValue(profileItem.bigDecimalValue(), profileItem.getMode());
        	}
        }
    }

}
