/*
 * MoneyField.java
 * 
 * $Revision$
 *
 * Copyright (c) 2001-2004 MoneyGram International
 */

package dw.dwgui;

import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.math.BigDecimal;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

import dw.i18n.FormatSymbols;
import dw.model.adapters.CountryInfo;
import dw.simplepm.SimplePMTransaction;
import dw.utility.Currency;
import dw.utility.Debug;

/**
 * Specialized text field for monetary values.
 * Includes settable number of decimal places.
 *
 * Implementation note: this is a complete redesign. Some of the behavior 
 * written into the previous version of MoneyField seemed like a good idea
 * on paper, but turned out to be confusing and flakey. This version is
 * much simpler, and behaves more like the margin fields in the Page Setup 
 * dialogs in (for example) Windows Notepad and Internet Explorer. The 
 * whole field is selected (highlighted) when you tab into it, and the 
 * value is turned into a valid, properly formatted number when you tab
 * out of it.
 *
 * @author Geoff Atkin
 */
public class MoneyField extends DWTextField {
	private static final long serialVersionUID = 1L;

    private int decimalPrecision = 2;
    private static String defaultValue = "0.00";	
    private String path = null;
    private static char decimalSeperator = '.';
    private BigDecimal maxExclusive;
    
    /** Construct a new money field with the default value. */
    public MoneyField() {
        this(defaultValue); 
    }

    /** Construct a new money field with the given value. */
    public MoneyField(String value) {
    	this(value, CountryInfo.getAgentCountry().getBaseReceiveCurrency());
    }

    /** Construct a new money field with the given value. */
    public MoneyField(String value, String currency) {
    	setCurrency(currency);
        decimalSeperator = FormatSymbols.getDecimalSeperator();
        setHorizontalAlignment(SwingConstants.RIGHT);
        setText(value);
        addFocusListener(new FocusListener() {
            @Override
			public void focusGained(FocusEvent e) {
                selectAll();
            }
            @Override
			public void focusLost(FocusEvent e) {
                reformat();
            }
        });
    	
    }

    /** 
     * Change the entered value to the valid monetary amount most closely
     * resembling what the user entered. See reformat(String) for details.
     *
     * Added feature for the new simple profile maintenance screens: if a
     * path has been set, inform the profile transaction of the new value.
     */
    public void reformat() {
        super.setText(reformat(super.getText()));
        
        if ((path != null) && (SimplePMTransaction.getCurrentInstance() != null)) {
            SimplePMTransaction.getCurrentInstance().userChange(path, super.getText()); 
        }
    }

    /**
     * Returns a string containing the valid monetary amount most closely
     * resembling the given string. The text is parsed as a BigDecimal,
     * with the scale set to decimalPlaces. If a decimal point was entered,
     * digits will be truncated or padded as needed. If a decimal point was
     * not entered, it is implied by moving the decimal point to the left.
     *
     * This implementation tries to construct a BigDecimal from the given 
     * string. If that fails, it tries using smaller and smaller substrings
     * by dropping characters from the right. If no such substring works, 
     * it returns the default value.
     * 
     * New feature: If the resulting BigDecimal is greater than or equal to
     * the value of this.maximum, reformat() returns the default value.
     */
    public String reformat(String value) {
        BigDecimal bd = null;
        String convertedToDecimalValue = value;
        String result = defaultValue;
        String seperator = String.valueOf(FormatSymbols.getDecimalSeperator());
        
         if (value.indexOf(seperator) >= 0){
        	convertedToDecimalValue = value.replace(FormatSymbols.getDecimalSeperator(), '.');
        }
        
        int decimalPosition = convertedToDecimalValue.indexOf('.');

        for (int i = value.length(); i > 0; i--) {
            try {
                bd = new BigDecimal(convertedToDecimalValue.substring(0, i));
                if ((decimalPosition >= 0) && (decimalPosition < i)) {
                    bd = bd.setScale(decimalPrecision, BigDecimal.ROUND_DOWN);
                }
                else {
                    bd = bd.movePointLeft(decimalPrecision);
                }
                result = bd.toString();
                break;
            }
            catch (NumberFormatException ignore) {
                // just go round the loop again to try a smaller substring
            }
        }
        
        if ((bd != null) && (maxExclusive != null) && (bd.compareTo(maxExclusive) >= 0)) {
            Toolkit.getDefaultToolkit().beep();
            result = FormatSymbols.convertDecimal(defaultValue);
        }
        String decimalResult = result;
        
        if (result.indexOf(".") >= 0)
        	result = decimalResult.replace('.', FormatSymbols.getDecimalSeperator());
        
        return result;
    }
    
    /**
     * Set the currency to the given value and turn on the currency label. 
     * This method is still being used, but at one time had a deprecation
     * comment which said:
     * No longer supported. Peter says Rob said this wasn't 
     * needed any more, and made changes on 01-29-2001 which disabled it.
     * 
     * **********************************************************************
     * Now required for multi-currency support, since the moneyField may not 
     * be in the agent's base currency.
     * **********************************************************************
     */ 
    public void setCurrency(String currency) {
        decimalPrecision = Currency.getCurrencyPrecision(currency);
        switch (decimalPrecision) {
            case 0:
              defaultValue = "0";
              break;
            case 1:
              defaultValue = "0.0";
              break;
            case 2:
              defaultValue = "0.00";
              break;
            case 3:
              defaultValue = "0.000";
              break;
            default:
              defaultValue = "0.00";
        }
    }

    /**
     * Set the field to the string representation of the given value. 
     * Note: since doubles aren't exact, this method is only predictable
     * when value=0.
     */
    public void setValue(double value) {
        setValue(new BigDecimal(value));
    } 

    /** Set the field to the string representation of the given value. */    
    public void setValue(BigDecimal value) {
		if (value != null) {
			setText(value.toString());
		}
    }

    /** Returns the field text as a BigDecimal. */
    public BigDecimal getValue() {
        return new BigDecimal(getText());
    }
    
    /** 
     * Overrides to reformat after setting the text. 
     * If the specified text does not contain a decimal point,
     * one is added to the end. This means there is no implied
     * decimal as long as the text is parsable as a big decimal.
     */
    @Override
	public void setText(String text) {
        super.setText(convertToLocalFormat(text) + FormatSymbols.getDecimalSeperator());	
        reformat();
        if (hasFocus()) selectAll();
    }

    private String convertToLocalFormat(String text){
    	String retValue = text;
        if (text.indexOf(".") >= 0){
        	retValue = text.replace('.', FormatSymbols.getDecimalSeperator());
        }
    	return retValue;
    }
    
    /** Overrides to reformat before returning the text. */
    @Override
	public String getText() {
        //reformat();   // can't mutate because we might be in a listener 
        return FormatSymbols.convertDecimal(reformat(super.getText()));
    }
    

    /** 
     * Set the number of decimal places that a valid amount is supposed
     * to have. This is the number of places implied if no decimal point
     * is entered. The reformat method fills or truncates as needed to
     * get this many places.
     */
    public void setDecimalPlaces(int p) {
        decimalPrecision = p;
    }

    /** Overridden to return a new instance of our customized Document. */
    @Override
	protected Document createDefaultModel() {
        return new MoneyDocument();
    }

    /** 
     * Set path to profile value 
     */
    public void setPath(String path) {
        this.path = path;
    }

    /** 
     * Set amount of money field 
     */
    public void setValue(BigDecimal value, String itemMode) {
        value = value.setScale(decimalPrecision, BigDecimal.ROUND_DOWN);
        super.setText(FormatSymbols.convertDecimal(value.toString()));

        if ("V".equals(itemMode)) {	
            setEnabled(false);
            setDisabledTextColor(SystemColor.controlText);
            setBackground(SystemColor.control);
        }
    }

    /** Returns true if setPath has been called. */
    public boolean isPathSet() {
        return (path != null);
    }

    /** Sets an upper limit to the value that can be entered. */
    public void setMaxExclusive(String s) {
        try  {
            maxExclusive = new BigDecimal(s);
        }
        catch (NumberFormatException e) {
            //ignore
        }
    }    
    
    /** Customized Document which only allows digits and period. */
    static class MoneyDocument extends PlainDocument {
    	private static final long serialVersionUID = 1L;


        /** Filters out unwanted characters. */
        @Override
		public void insertString(int offs, String str, AttributeSet a)
                throws BadLocationException {
                    
            StringBuffer sb = new StringBuffer();
            char ch;
            if (str == null)
                return;
            for (int i = 0; i < str.length(); i++) {
                ch = str.charAt(i);
                //if ((ch == '.') || Character.isDigit(ch)) {
                if ((ch == decimalSeperator) || Character.isDigit(ch)) {
                    sb.append(ch);
                }
            }
            super.insertString(offs, sb.toString(), a);
        }
    }
    
    /** For testing. */
    public static void main(String[] args) {
        JFrame f = new JFrame();
        f.addWindowListener(new WindowAdapter() {
            @Override
			public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        Debug.println("getDecimalPrecision() = " + FormatSymbols.getDecimalPrecision());
        java.awt.Container c = f.getContentPane();
        c.setLayout(new java.awt.GridLayout(2, 2, 5, 5));
        c.add(new MoneyField());
        c.add(new MoneyField());
        c.add(new MoneyField());
        MoneyField m = new MoneyField();
        m.setMaxExclusive("1000");
        c.add(m);
        f.setSize(300, 100);
        f.setTitle("MoneyField"); 
        f.setVisible(true);
    }
}
