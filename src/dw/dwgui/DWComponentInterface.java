package dw.dwgui;

public interface DWComponentInterface {
    
    public static final boolean        HANDLE_ENTER = true;
    public static final boolean DO_NOT_HANDLE_ENTER = false;

    public void setDoNotHandleEnter();
    
    public void setHandleEnter();

}
