package dw.dwgui;

import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import dw.framework.DataCollectionScreenToolkit.DWComboBoxItem;

/**
 * ComboBox that allows multiple lists to be stored while displaying only
 *   one of them. This will be convenient for switching the view of the
 *   box quickly instead of re-loading it all the time. Ex : lists of US states,
 *   Canadian Provinces, Mexican states. Note : The default list will be the
 *   first one that is added.
 */
public class MultiListComboBox<E> extends JComboBox implements DWComponentInterface {
	private static final long serialVersionUID = 1L;

    private HashMap<String, Object[]> listMap = new HashMap<String, Object[]>();  // holds all the lists of Obj[]
    private HashMap<String, Object> defaultMap = new HashMap<String, Object>(); // holds defaults for each list
    private String currentList = null;
    private String defaultList = null;

    private boolean handleEnter = HANDLE_ENTER;

    public MultiListComboBox() {
        super();
    }

    /**
     * Put the component back into a default state...default list & default item
     */
    public void reset() {
        setCurrentList(defaultList);
    }

    /**
     * Add a list with the specified default item.
     */
    public void addList(Object[] list, Object defaultItem, String listName) {
        // use the first item as default if default is not in the given list
        boolean containsDefault = false;
        for (int i = 0; i < list.length; i++) {
            if (list[i].equals(defaultItem)) {
                containsDefault = true;
                break;
            }
        }
        listMap.put(listName, list);
        if (containsDefault)
            defaultMap.put(listName, defaultItem);
        else
        	if (list.length >= 1) {
            defaultMap.put(listName, list[0]);
        	}
        if (currentList == null)
            setCurrentList(listName);
        if (defaultList == null)
            defaultList = listName;
    }
    
    public void addList(List<? extends DWComboBoxItem> list, Object defaultItem, String listName) {
    	addList(list.toArray(), defaultItem, listName);
    }

    public void addList(Object[] list, String listName) {
        addList(list, list[0], listName);
    }
    
    public void addList(List<? extends DWComboBoxItem> list, String listName) {
    	currentList = null;
        addList(list.toArray(), list.get(0), listName);
    }

    public void setDefaultList(String listName) {
        this.defaultList = listName;
    }

    public void setDefaultItem(String listName, Object item) {
        defaultMap.put(listName, item);
    }

    /**
     * Swap in the given list and set its default selection
     */
    public void setCurrentList(String listName) {
        currentList = listName;
        Object[] list = listMap.get(listName);
        if (list != null) {
            setModel(new DefaultComboBoxModel(list));
            setSelectedItem(defaultMap.get(listName));
            setEditable(false);
        }
        else {
            setModel(new DefaultComboBoxModel(new DWComboBoxItem[0]));
            setEditable(true);
        }
    }

    public String getCurrentList() {
        return currentList;
    }
    public int getListLength(String listName) {
        currentList = listName;
        Object[] list = listMap.get(listName);
        if (list != null) {
            return list.length;
        }
        else {
            return 0;
        }
    }
    @Override
	public void processKeyEvent(final KeyEvent e) {
        if (!DWComponent.canProcessKeyEvent(e,handleEnter))
            super.processKeyEvent(e);
    }

    @Override
	public void setDoNotHandleEnter() {
        handleEnter = DO_NOT_HANDLE_ENTER;
    }
    
    @Override
	public void setHandleEnter() {
        handleEnter = HANDLE_ENTER;
    }
    
	public void setSelectedComboBoxItem(String value) {
		int c = getItemCount();
		for (int i = 0; i < c; i++) {
			DWComboBoxItem cbi = (DWComboBoxItem) getItemAt(i);
			if (cbi.getValue().equals(value)) {
				setSelectedIndex(i);
				break;
			}
		}
	}
}
