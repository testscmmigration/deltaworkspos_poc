package dw.dwgui;

import java.awt.GridLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 * A text field which captures the multiple field input of a magnetic swipe
 *   card. This component will only display a single field from the card, but
 *   will internally capture all the fields entered by the card.
 * Fields on both MoneySaver cards and dispenser form packs use a '^' as a
 *   field delimiter, and a '?' as an end-of-input delimiter.
 *      MoneySaver fields:
 *          [0] - MoneySaver id number.
 *      Dispenser form pack fields :
*           [0] - Card Type (1 = MO Serial Number Load Card)
 *          [1] - starting serial #
 *          [2] - # forms per pack
 *          [3] - money order type (single digit)
 *          [4] - stub format (ex : '4LNE')
 * NOTE : Enter key consuming logic is for handing incorrectly configured
 *   keyboards. Some keyboards put an enter between tracks and some do not.
 *   We don't want to let the action happen on the field before we catch
 *   everything the card is going to throw.
 */
public class CardSwipeTextField extends JTextField {
    private CardSwipeDocument doc = null;
    private boolean onFirstTrack = true;
    private boolean normalInputMethod = true;
    private boolean allowKeyboardEntry = true;
    private boolean bankCard = false;

    public CardSwipeTextField() {
        this("", 0);	
    }

    public CardSwipeTextField(String text, int displayIndex) {
        doc = new CardSwipeDocument();
        setDisplayIndex(displayIndex);
        setDocument(doc);
        setText(text);
        addKeyListener(new KeyAdapter() {
			 @Override
             public void keyPressed(KeyEvent e) {
                 if (! normalInputMethod && e.getKeyCode() == KeyEvent.VK_ENTER) {
                     // consume the enter press if we are still on track 1
                     if (onFirstTrack)
                         e.consume();
                 }}});
    }

    public void setMaxFieldLength(int len) {
        doc.setMaxFieldLength(len);
    }

    /**
     * Set the index of the field which is to be displayed as it is entered.
     */
    public void setDisplayIndex(int index) {
        doc.setDisplayIndex(index);
    }

    public int getDisplayIndex() {
        return doc.getDisplayIndex();
    }

    public boolean isNormalInputMethod(){
        return normalInputMethod;
    }

    /**
     * Set the string which contains all the allowed characters for this field.
     */
    public void setValidChars(String valid) {
        doc.setValidChars(valid);
    }

    /**
     * Get a particular field that was entered.
     */
    public String getField(int i) {
        Vector fields = doc.getFields();
        if (i >= fields.size())
            return null;
        else
            return ((StringBuffer) fields.elementAt(i)).toString();
    }

    /**
     * Get the number of fields that were entered.
     */
    public int getNumFields() {
        return doc.getFields().size();
    }

	@Override
    public void setText(String text) {
        onFirstTrack = true;
        doc.reset();
        super.setText(text);
    }

    public void setAllowKeyboardEntry(boolean b) {
    	allowKeyboardEntry = b;
    } 
    
    public void setBankCard(boolean b) {
    	bankCard = b;
    } 
    
    public void reset() {
    	doc.reset();
    } 
    
    public static void main(String[] args) {
        JFrame f = new JFrame();
        f.getContentPane().setLayout(new GridLayout(2, 1));
        f.getContentPane().add(new CardSwipeTextField("", 0));	
        CardSwipeTextField cs2 = new CardSwipeTextField("", 1);	
        cs2.setValidChars("0123456789LNE"); 
        f.getContentPane().add(cs2);
        f.setSize(500, 100);
        f.setVisible(true);
    }

/**
 * Text document which captures only the valid characters from a MoneySaver card
 *   or a dispenser form pack card.
 */
class CardSwipeDocument extends PlainDocument {
     private String validChars = "0123456789";	

     private AttributeSet a = null;
     private int fieldIndex = 0;
     private char trackDelimiter = ';';
     private char fieldDelimiter = '^';
     private char endChar = '?';
     private boolean done = false;
     private int displayIndex = 0 ;
     private Vector fields = new Vector(); // of StringBuffers
     private int maxFieldLength = 0;

     /**
      * Set the field back to its original state, ready to capture fields.
      */
     public void reset() {
         fields = new Vector();
         fieldIndex = 0;
         done = false;
         onFirstTrack = true;
         normalInputMethod = true;
         allowKeyboardEntry = true;
         bankCard = false;
     }

     public void setMaxFieldLength(int len) {
        maxFieldLength = len;
     }

     /**
      * Set the index of the field which should be displayed as it is captured.
      *   Only one field may be displayed.
      */
     public void setDisplayIndex(int i) {
         displayIndex = i;
     }

     public int getDisplayIndex() {
         return displayIndex;
     }

     public void setValidChars(String valid) {
         this.validChars = valid;
     }

     /**
      * Get a list of all the fields captured.
      * @return a Vector of StringBuffers of captured fields.
      */
     public Vector getFields() {
         return fields;
     }

     public AttributeSet getAttr() {
         return a;
     }

     /**
      * When s string is inserted into the field, check its validity before
      *   adding it to the text. If a swipe card field delimiter is reached,
      *   add a new field to the list and start adding any following text to
      *   that field.
      */
	 @Override
     public void insertString(int offs, String str, AttributeSet a)
                                        throws BadLocationException {
         if (str.length() > 0 && str.charAt(0) == trackDelimiter) {
             onFirstTrack = false;
             normalInputMethod = false;
             return;
         }
         // end of card input has been signaled. do not accept anything more
         if (done)
             return;
         StringBuffer sb;
         // get the current field to add text to. add a new one if needed
         if (fields.size() == fieldIndex) {
             sb = new StringBuffer();
             fields.add(sb);
         }
         sb = (StringBuffer) fields.elementAt(fieldIndex);

         // Is this a normal string coming from the text field?
         if (str.length() > 0 && str.charAt(0) == '%') {
             normalInputMethod = false;
             /*
              * If this is the account number field and we have a start
              * sentinel, then erase any existing field data.
              */
             if (fieldIndex == 0) {
                 super.remove(0, offs);
             }
         }

         // check that each char of the input string is valid before adding it
         for (int i = 0; i < str.length(); i++) {
             if (!normalInputMethod) {
                 // end of field reached, start a new field.
                 if (str.charAt(i) == fieldDelimiter && sb.length() > 0) {
                     fieldIndex++;
                     return;
                 }
                 // end of the input reached. make sure last field not empty.
                 else if (str.charAt(i) == endChar) {
                     done = true;
                     StringBuffer last = (StringBuffer)
                                             fields.elementAt(fields.size() - 1);
                     if (last.length() == 0)
                         fields.remove(last);
                     return;
                 }
             }
             else {
             	if (! allowKeyboardEntry)
             	   return;
             }
             
             /*
              * For bank cards don't allow non-digits in the account number (this
              * will strip out the leading 'B' for swiped cards.
              */
             if (bankCard && (fieldIndex == 0) 
            		 && !Character.isDigit(str.charAt(i))) {
                 /*
                  * Check if the character we are looking at is at the end of
                  * the string (remembering index starts at 0).
                  */
            	 if ((i+1) >= str.length() ) {
            		 /*
            		  * If the character is the only character in the string
            		  * just exit, otherwise strip off the last character of
            		  * the string
            		  */
            		 if (i == 0) {
                         return;
            		 }
            		 else {
                    	 str = str.substring(0, i);
            		 }
            	 }
            	 else {
                	 str = str.substring(0, i) + str.substring(i+1);
            	 }
             }
            	 
             
             if (validChars.indexOf(str.charAt(i)) < 0) 
                 return;
         }
         sb.append(str);

         int currentLength = this.getLength();

         if (normalInputMethod || fieldIndex <= displayIndex)
             // skip the insert if we have too many characters
             if ((maxFieldLength == 0) || (currentLength < maxFieldLength))
                 super.insertString(offs, str, a);
     }


     /**
      * When characters are removed, check for the length of the remaining
      *   text. If the field is now empty, reset the field input.
      */
     @Override
	public void remove(int offs, int len) throws BadLocationException {
           super.remove(offs, len);
           if (getText(0, getLength()).equals(""))	
               reset();
     }
}
}
