package dw.dwgui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JLabel;

public class ObnoxiousLabel extends JLabel {
	private static final long serialVersionUID = 1L;

	boolean keepFlashing = false;
    boolean on = true;
    int numBeeps = 0;
    int beepInt = 0;
    int flashInt = 0;
    Color color;

    public ObnoxiousLabel(String text, int numberOfBeeps, int beepInterval,
                          final int flashInterval, final Color textColor) {
        super(text);
        beepInt = beepInterval;
        numBeeps = numberOfBeeps;
        flashInt = flashInterval;
        color = textColor;
        setForeground(color);
        setFont(new Font("sanserif", Font.BOLD, 14));	
    }

    public void activate() {
        if (keepFlashing)
            return;
        keepFlashing = true;
        Thread flasher = new Thread(new Runnable() {
                @Override
				public void run() {
                    beep();
                    while (keepFlashing) {
                        try {
                            Thread.sleep(flashInt);
                        }
                        catch (InterruptedException e) {}
                        on = ! on;
                        if (on)
                            setForeground(color);
                        else
                            setForeground(Color.black);
                        repaint();
                    }
                }});
        flasher.start();
    }

    public void deactivate() {
        keepFlashing = false;
    }

    public void beep() {
        Toolkit.getDefaultToolkit().beep();
        Thread t = new Thread(new Runnable() {
                @Override
				public void run() {
                    for (int i = 0; i < numBeeps; i++) {
                        Toolkit.getDefaultToolkit().beep();
                        try {
                            Thread.sleep(beepInt);
                        }
                        catch (InterruptedException ie) {}
                    }}});
        t.start();
    }

    @Override
	public void finalize() {
        keepFlashing = false;
    }
}
