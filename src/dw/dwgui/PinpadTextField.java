package dw.dwgui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.JPasswordField;

import dw.i18n.Messages;
import dw.mgreceive.MoneyGramReceiveWizard;
import dw.pinpad.Pinpad;
import dw.pinpad.PinpadInterface;
import dw.pinpad.PinpadInterface.PinpadListener;
import dw.pinpad.PinpadInterface.PinpadReturnCode;
import dw.utility.Debug;

public class PinpadTextField extends JPasswordField implements PinpadListener {
	private static final long serialVersionUID = 1L;
    
	private static final int DISPLAY_MAX_PIN_INPUT_LENGTH = 4;
	private static final int DISPLAY_MAX_REFERENCE_INPUT_LENGTH = 8;
    private static final int MAXIMUM_ATTEMPTS_FOR_USER_INPUT = 3;
    
    private PinpadStatus pinpadStatus;
    private boolean focusListenerEnabled;
    private int referenceNumberAttemptCount = 0;
    private int pinAttemptCount = 0;
    private String placeholderText;
    private Color placeholderColor;
    
	public interface PinpadTextFieldListener {
		public void callback(ReturnStatus returnCode);
	}
	
    enum PinpadStatus {
    	INITIALIZED,
    	GETTING_REFERENCE_NUMBER,
    	GETTING_PIN,
    	COMPLETE
    }
    
    public enum ReturnStatus {
    	GOOD,
    	CANCELLED,
    	BAD
    }
    
    private String referenceNumber;
	private String pin;
	private PinpadTextFieldListener pinpadTextFieldListener;
    
    static {
    	if (Pinpad.INSTANCE.getIsPinpadRequired()) {
	    	PinpadListener listener = new PinpadListener() {
				@Override
				public void callback(PinpadReturnCode returnCode) {
				}
	    	};
	    	
	    	Pinpad.INSTANCE.setPinpadListener(listener);
	    	
		    int c = 0;
		    while (true) {
				if (Pinpad.INSTANCE.getPinpadStatus() < 0) {
			    	MoneyGramReceiveWizard.interuptshowPinpadMessagesThread();
			    	Pinpad.INSTANCE.initialize();
		    	}
				c++;
				if (Pinpad.INSTANCE.getPinpadStatus() >= 0) {
					Pinpad.INSTANCE.sendDisplayMessageRequest(Messages.getString("MGRWizardPage1.PinpadIngenico_MoneygramInternational"));
					break;
				} else if (c >= MAXIMUM_ATTEMPTS_FOR_USER_INPUT) {
					break;
				}
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					Debug.printStackTrace(e);
				}
		    }
    	}
    }
    
    public PinpadTextField() {
    	Pinpad.INSTANCE.setPinpadListener(this);
    	focusListenerEnabled = false;
	    if (! Pinpad.INSTANCE.getIsPinpadRequired()) {
	    	this.setEchoChar((char) 0);
	    	this.setEditable(true);
	    	this.setFocusable(true);
	    } else {
	    	this.setEchoChar('*');
	    	this.setEditable(false);
	    	this.setFocusable(true);
	    
			this.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent event) {
					if (focusListenerEnabled) {
						focusListenerEnabled = false;
						
						Thread t = new Thread() {
							@Override
							public void run() {
								PinpadTextField.this.getCursor();
						    	MoneyGramReceiveWizard.interuptshowPinpadMessagesThread();
								pinpadStatus = PinpadStatus.GETTING_REFERENCE_NUMBER;
							    referenceNumberAttemptCount = 0;
							    pinAttemptCount = 0;
							    PinpadTextField.this.setPlaceholder(Messages.getString("PinpadTextField.1"), DWTextField.GRAY_PLACEHOLDER_COLOR);
							    PinpadTextField.this.repaint();
							    Pinpad.INSTANCE.sendBeepToneRequest(PinpadInterface.BEEP_DURATION_MINIMUM);
							    Pinpad.INSTANCE.sendDisplayPromptRequest(
										Messages.getString("MGRWizardPage1.PinpadIngenico_EnterReferenceNumber"),
										PinpadInterface.MODE_PASSWORD_NUMERIC_BUFFER,
										PinpadInterface.DISPLAY_AT_LINE1_INPUT_AT_LINE3,
										DISPLAY_MAX_REFERENCE_INPUT_LENGTH);
							}
						};
						t.start();
					}
				}
			});
	    }
    }
    
	public String getReferenceNumber() {
		if (Pinpad.INSTANCE.getIsPinpadRequired()) {
			return referenceNumber;
		} else {
			return this.getText();
		}
	}

	public String getPin() {
		return pin;
	}

    @Override
	public void callback(PinpadReturnCode returnCode) {
		if (returnCode.equals(PinpadReturnCode.PINPAD_INIITIALIZED)) {

		} else if (returnCode.equals(PinpadReturnCode.PINPAD_INPUT_AVAIABLE)) {
			if (pinpadStatus == PinpadStatus.GETTING_REFERENCE_NUMBER) {
				
				// Get the reference 
				
				String rn = Pinpad.INSTANCE.getUserInput();
				
	        	if ((rn.length() != DISPLAY_MAX_REFERENCE_INPUT_LENGTH) || (rn.startsWith("0"))) {
	        		referenceNumberAttemptCount++;
	        		if (referenceNumberAttemptCount < MAXIMUM_ATTEMPTS_FOR_USER_INPUT) {
	        			Pinpad.INSTANCE.sendDisplayPromptRequest(Messages.getString("MGRWizardPage1.PinpadIngenico_InvalidRefTryAgain"), PinpadInterface.MODE_PASSWORD_NUMERIC_BUFFER, PinpadInterface.DISPLAY_AT_LINE1_INPUT_AT_LINE3, DISPLAY_MAX_REFERENCE_INPUT_LENGTH);
						return;
	        		} else {
	        			Pinpad.INSTANCE.sendBeepToneRequest(PinpadInterface.BEEP_DURATION_MINIMUM);
	        			Pinpad.INSTANCE.sendDisplayMessageRequest(Messages.getString("MGRWizardPage1.PinpadIngenico_MoneygramInternational"));
	        			pinpadTextFieldListener.callback(ReturnStatus.BAD);
	        			return;
	        		}
	        	}
	        	
	        	referenceNumber = rn;
	        	this.setText(rn);
				
		        if (Pinpad.INSTANCE.getIsPinRequired()) {
		        	pinpadStatus = PinpadStatus.GETTING_PIN;
		        	Pinpad.INSTANCE.sendDisplayPromptRequest(
	     					Messages.getString("MGRWizardPage1.PinpadIngenico_EnterPin"),
	     					PinpadInterface.MODE_PASSWORD_NUMERIC_BUFFER,
	     					PinpadInterface.DISPLAY_AT_LINE1_INPUT_AT_LINE3,
	     					DISPLAY_MAX_PIN_INPUT_LENGTH);
	     			return;
		        } else {
		        	pinpadStatus = PinpadStatus.COMPLETE;
		        	Pinpad.INSTANCE.sendDisplayMessageRequest(Messages.getString("MGRWizardPage1.PinpadIngenico_ProcessingTransaction"));
					if (pinpadTextFieldListener != null) {
						pinpadTextFieldListener.callback(ReturnStatus.GOOD);
					}
		        }
				
		    } else if (pinpadStatus == PinpadStatus.GETTING_PIN) {
		    	String p = Pinpad.INSTANCE.getUserInput();
	        	
	        	if (p.length() != DISPLAY_MAX_PIN_INPUT_LENGTH) {
	        		pinAttemptCount++;
	        		if (pinAttemptCount < MAXIMUM_ATTEMPTS_FOR_USER_INPUT) {
	        			Pinpad.INSTANCE.sendDisplayPromptRequest(Messages.getString("MGRWizardPage1.PinpadIngenico_InvalidPinTryAgain"), PinpadInterface.MODE_PASSWORD_NUMERIC_BUFFER, PinpadInterface.DISPLAY_AT_LINE1_INPUT_AT_LINE3, DISPLAY_MAX_REFERENCE_INPUT_LENGTH);
						return;
	        		} else {
	        			Pinpad.INSTANCE.sendBeepToneRequest(PinpadInterface.BEEP_DURATION_MINIMUM);
	        			Pinpad.INSTANCE.sendDisplayMessageRequest(Messages.getString("MGRWizardPage1.PinpadIngenico_MoneygramInternational"));
	        			pinpadTextFieldListener.callback(ReturnStatus.BAD);
	        			return;
	        		}
	        	}
		    	
		    	pin = p;
		    	pinpadStatus = PinpadStatus.COMPLETE;
		    	Pinpad.INSTANCE.sendDisplayMessageRequest(Messages.getString("MGRWizardPage1.PinpadIngenico_ProcessingTransaction"));
				if (pinpadTextFieldListener != null) {
					pinpadTextFieldListener.callback(ReturnStatus.GOOD);
				}
		    }

		} else if (returnCode.equals(PinpadReturnCode.CANCELED)) {
			referenceNumber = null;
			pin = null;
			this.setPlaceholder(Messages.getString("PinpadTextField.2"), DWTextField.GRAY_PLACEHOLDER_COLOR);
			this.repaint();
			if (pinpadTextFieldListener != null) {
				pinpadTextFieldListener.callback(ReturnStatus.CANCELLED);
			}
		}
	}

	public void setPinpadListener(PinpadTextFieldListener pinpadTextFieldListener) {
		this.pinpadTextFieldListener = pinpadTextFieldListener;
	}
	
	public void setFocusListenerEnabled(boolean b) {
		this.focusListenerEnabled = b;
	}

	@Override
	protected void paintComponent(final Graphics pG) {
		super.paintComponent(pG);

		
		if (placeholderText==null || placeholderText.length() == 0 || getText().length() > 0) {
			return;
		}

		final Graphics2D g = (Graphics2D) pG;
		g.setColor(placeholderColor);
		g.drawString(placeholderText, getInsets().left, pG.getFontMetrics().getMaxAscent() + getInsets().top);
	}

	public void setPlaceholder(String placeholder, Color placeholderColor) {
		this.placeholderText = placeholder;
		this.placeholderColor = placeholderColor;
	}
}
