package dw.dwgui;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JCheckBox;

public class DWCheckBox extends JCheckBox implements DWComponentInterface {
	private static final long serialVersionUID = 1L;

    private boolean handleKeyEvents = true;
    private boolean handleEnter = HANDLE_ENTER;

    @Override
	public void processKeyEvent(KeyEvent e) {
        if (!(handleKeyEvents && DWComponent.canProcessKeyEvent(e,handleEnter)))
            super.processKeyEvent(e);
    }
    
    @Override
	public void addKeyListener(KeyListener l) {
        handleKeyEvents = false;
        super.addKeyListener(l);
    }

    @Override
	public void setDoNotHandleEnter() {
        handleEnter = DO_NOT_HANDLE_ENTER;
    }
    
    @Override
	public void setHandleEnter() {
        handleEnter = HANDLE_ENTER;
    }
}
