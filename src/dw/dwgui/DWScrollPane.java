package dw.dwgui;

import java.awt.event.KeyEvent;

import javax.swing.JScrollPane;

/**
 * Extension of JScrollPane to allow public access to processKeyEvent
 */
public class DWScrollPane extends JScrollPane {
	private static final long serialVersionUID = 1L;

    @Override
	public void processKeyEvent(final KeyEvent e) {
        super.processKeyEvent(e);
    }
}
