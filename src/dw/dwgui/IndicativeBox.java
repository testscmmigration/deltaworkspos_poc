package dw.dwgui;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.border.Border;

/**
 * HelpBox is a panel the color of the Windows help color. It contains
 *   multiple labels lined up vertically.
 */
public class IndicativeBox extends JPanel {
	private static final long serialVersionUID = 1L;

    static final Color helpColor = new Color(254, 255, 219);

    public IndicativeBox() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setBackground(helpColor);
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder((Border) null), BorderFactory.createEmptyBorder(2, 2, 2, 2)));
    }
}
