package dw.dwgui;

import java.awt.Color;
import java.awt.Dimension;
import java.io.Serializable;

public interface DWTextString extends Serializable {
	public void invalidate();
	public void setPreferredSize(Dimension d);
	public Dimension getPreferredSize();
	public void setText(String text);
	public String getText();
	public void setVisible(boolean visible);
	public void setEnabled(boolean flag);
	public Color getBackground();
	public void setBackground(Color c);
}
