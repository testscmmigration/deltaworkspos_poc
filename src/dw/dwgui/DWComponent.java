package dw.dwgui;

import java.awt.Component;
import java.awt.event.KeyEvent;

public class DWComponent {
    
    public static boolean canProcessKeyEvent(  final KeyEvent e,
                                               final boolean handleEnter) {
        if ((e.getID()      == KeyEvent.KEY_PRESSED) &&
            (e.getKeyCode() == KeyEvent.VK_ENTER)) { 
//            final javax.swing.FocusManager focusManager =
//                javax.swing.FocusManager.getCurrentManager();
            final java.awt.KeyboardFocusManager focusManager = java.awt.KeyboardFocusManager.getCurrentKeyboardFocusManager();
            final Component component = e.getComponent();
            if (e.isShiftDown()) {
                e.consume();
                focusManager.focusPreviousComponent(component);
                return true;
            }
            else
                if (handleEnter) {
                    e.consume();
                    focusManager.focusNextComponent(component);
                    return true;
                }
                else
                    return false;
        }    
        else
            return false;
    }
}
