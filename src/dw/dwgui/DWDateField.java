package dw.dwgui;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.text.DateFormatter;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

import dw.utility.Debug;
import dw.utility.TimeUtility;

public class DWDateField extends JFormattedTextField implements DWComponentInterface {
	private static final long serialVersionUID = 1L;

	private boolean handleKeyEvents = true;
    private boolean handleEnter = HANDLE_ENTER;
    private DefaultFormatterFactory dff = new MyFactory(); 

    public DWDateField(){
        try {
            dff.setDefaultFormatter(new DateFormatter(DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.US)));
            dff.setDisplayFormatter(new MaskFormatter("##/##/####"));
            dff.setEditFormatter(new MaskFormatter("##/##/####"));
            setFormatterFactory(dff);
            setHorizontalAlignment(SwingConstants.LEFT);
            /**
             * JFormattedTextField.COMMIT
             *  Commits the current value. If the value being edited is not considered a 
             *  legal value by the AbstractFormatter that is, a ParseException is thrown,
             *  then the value will not change, and then edited value will persist.
             */
            setFocusLostBehavior(JFormattedTextField.COMMIT);
            setInputVerifier(new Verifier());
            addFocusListener(new FocusListener() {
                @Override
				public void focusGained(FocusEvent e) {
                    selectAll();
                }
                @Override
				public void focusLost(FocusEvent e) {
                    //setText(getText());
                }
            });
        }
        catch (ParseException e) {
            Debug.println(""+e);
        }
    }
    
    public String getFormattedText(){
        String dateString = "";
        dateString = getDateStringYY(getText().trim());
        return dateString.replaceAll("/", "");
    }

    @Override
	public void setText(String newText){
        String dateString = null;
        if(newText == null || newText.length()==0){
            dateString = "  /  /    ";
        }
        else{
            dateString = getDateStringYYYY(newText);
            // validate if old month and day equal new month and day.
            String newMonth = dateString.replaceAll("/","").substring(0,2);
            String newDay = dateString.replaceAll("/","").substring(2,4);
            String oldMonth = newText.replaceAll("/","").substring(0,2);
            String oldDay = newText.replaceAll("/","").substring(2,4);
            if((!oldMonth.equalsIgnoreCase(newMonth)) || (!oldDay.equalsIgnoreCase(newDay))){
                dateString = newText;
            }
        }
        super.setText(dateString);
    }
    
    private String getDateStringYY(String date){
        try {
            Date dt = new SimpleDateFormat(TimeUtility.DATE_MONTH_DAY_LONG_YEAR_FORMAT).parse(date);
            date = new SimpleDateFormat(TimeUtility.DATE_MONTH_DAY_SHORT_YEAR_FORMAT).format(dt);
        }
        catch (NullPointerException npe){
            date = "  /  /  ";
        }
        catch (ParseException e) {
            date = "  /  /  ";
        }
        return date;
    }

    private String getDateStringYYYY(String date){
    	DateFormat df = new SimpleDateFormat(TimeUtility.DATE_MONTH_DAY_LONG_YEAR_FORMAT);
        try {
            Date dt = new SimpleDateFormat(TimeUtility.DATE_MONTH_DAY_SHORT_YEAR_FORMAT).parse(date);
            date = df.format(dt);
        }
        catch (NullPointerException npe){
            date = "  /  /    ";
        }
        catch (ParseException e) {
            try {
                Date dt = new SimpleDateFormat(TimeUtility.DATE_MONTH_DAY_SHORT_YEAR_NO_DELIMITER_FORMAT).parse(date);
                date = df.format(dt);
            }
            catch (ParseException e1) {
                date = "  /  /    ";
            }
        }
        return date;
    }

    public String getDateStringMMddyy(String date){
        try {
            Date dt = (new SimpleDateFormat(TimeUtility.DATE_MONTH_DAY_LONG_YEAR_FORMAT)).parse(date);
            date = (new SimpleDateFormat(TimeUtility.DATE_MONTH_DAY_SHORT_YEAR_NO_DELIMITER_FORMAT)).format(dt);
        }
        catch (Exception e){
            date = "  /  /    ";
        }
        return date;
    }

    @Override
	public void processKeyEvent(KeyEvent e) {
        if (!(handleKeyEvents && DWComponent.canProcessKeyEvent(e,handleEnter)))
            super.processKeyEvent(e);
     }

    // inherited abstract methods from DWCompentInterface.
    @Override
	public void setDoNotHandleEnter() {
        handleEnter = DO_NOT_HANDLE_ENTER;
    }
    
    @Override
	public void setHandleEnter() {
        handleEnter = HANDLE_ENTER;
    }
    
    class MyFactory extends DefaultFormatterFactory {
		private static final long serialVersionUID = 1L;

		@Override
		public JFormattedTextField.AbstractFormatter getFormatter(
                         JFormattedTextField source) {
            JFormattedTextField.AbstractFormatter format = null;
            if (source == null) {
                return null;
            }
            Object value = source.getValue();
            if (value == null) {
                format = getNullFormatter();
            }
            if (format == null) {
                if (source.hasFocus()) {
                    format = getEditFormatter();
                }
                else {
                    String text = getText();
                    try {
                        format = getDisplayFormatter();
                        if(text.trim().compareTo("")==0){
                            return format;
                        }
                        DateFormat df = new SimpleDateFormat(TimeUtility.DATE_MONTH_DAY_LONG_YEAR_FORMAT);
                        Date dt = df.parse(text.trim());
                        String dateString = df.format(dt);
                        Date now = new Date();
                        if(dt.after(now))
                            throw new ParseException("Date cannot be in future.",7);
                        else{
                            if(!dateString.equalsIgnoreCase(getText())){
                                throw new ParseException("Parsed date not the same as entered date.",7);
                            }
                        }
                        df = new SimpleDateFormat(TimeUtility.DATE_MONTH_DAY_SHORT_YEAR_FORMAT);
                        dt = df.parse(text.trim());
                        source.setText(df.format(dt));
                    }
                    catch (ParseException pe) {
//                        Dialogs.showWarning("dialogs/DialogInvalidData.xml", 
//                           Resources.getString("ComplianceDialog.Dates_must_be_in_the_format_of_MMDDYYYY_71")); 
                        if(!text.equals("  /  /    ")){
                            requestFocus();
                            selectAll();
                        }
                    }
                    format = getDisplayFormatter();
                }
                if (format == null)
                    format = getDefaultFormatter();
            }
            return format;
        }
    }

    public class Verifier extends InputVerifier {
        
        @Override
		public boolean verify(JComponent input) {
            JFormattedTextField ftf = (JFormattedTextField) input;
            AbstractFormatter formatter = dff.getFormatter(ftf);
            String text = ftf.getText();
            setText(text);
            text = ftf.getText();
            
            try {
                formatter.stringToValue(text);
                return true;
            }
            catch (ParseException pe) {
                if(text.replaceAll("/","").trim().length()>0)
                    return false;
                else
                    return true;
            }
        }
        @Override
		public boolean shouldYieldFocus(JComponent input) {
            return verify(input);
        }
    }
}
