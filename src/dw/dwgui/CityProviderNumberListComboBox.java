package dw.dwgui;

import dw.utility.CityProviderNumberInfo;

/**
 * A custom combo box that stores and displays CityProviderNumberInfo objects. When the
 *   selected item is requested, it returns just the provider number to be stored.
 */

public class CityProviderNumberListComboBox extends MultiListComboBox<CityProviderNumberInfo> { 
	private static final long serialVersionUID = 1L;


	/**
     * Try to set the index of the list. If the list is empty, ignore
     */
    @Override
	public void setSelectedIndex(int index) {
        if (super.getItemCount() == 0)
            return;
        super.setSelectedIndex(index);
    }

    /**
     * Set the selected provider number in the list. If the provider number is not in the list,
     *   select the first one.
     */
    public void setSelectedNumber(String number) {
        for (int i = 0; i < getItemCount(); i++) {
        	CityProviderNumberInfo info = (CityProviderNumberInfo) getItemAt(i);
            if (info.getNumber().equalsIgnoreCase(number)) {
                super.setSelectedItem(info);
                return;
            }
        }
        if (super.getItemCount() > 0)
            setSelectedIndex(0);
    }

    /**
     * Set the selected city Phone Provider in the list. If the city Phone Provider is not in the list,
     *   select the first one.
     */
    public void setSelectedItem(String cityPhoneProvider) {
        for (int i = 0; i < getItemCount(); i++) {
        	CityProviderNumberInfo info = (CityProviderNumberInfo) getItemAt(i);
            if (info.toString().equalsIgnoreCase(cityPhoneProvider)) {
                super.setSelectedItem(info);
                return;
            }
        }
        if (super.getItemCount() > 0)
            setSelectedIndex(0);
    }
    
    /**
     * Return the number of the provider that is currently displayed in the
     *   combo box. If the list was empty return the text
     *   that was typed in the box.
     */
    public String getNumber() {
        if (super.getItemCount() == 0) {
            return (String) getSelectedItem();
        }
        CityProviderNumberInfo info = (CityProviderNumberInfo) super.getSelectedItem();
        if (info != null)
            return info.getNumber();
        return "";	
    }
    
    /**
     * Return the script of the provider that is currently displayed in the
     *   combo box. If the list was empty return the text
     *   that was typed in the box.
     */
    public String getScript() {
        if (super.getItemCount() == 0) {
            return (String) getSelectedItem();
        }
        CityProviderNumberInfo info = (CityProviderNumberInfo) super.getSelectedItem();
        if (info != null)
            return info.getScript();
        return "";	
    }
    
    @Override
	public Object getSelectedItem() {
        Object item = super.getSelectedItem();
        if (item == null)
            return "";	
        return item;
    }
}