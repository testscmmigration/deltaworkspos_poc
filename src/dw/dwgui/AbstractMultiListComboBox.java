/*
 * AbstractMultiListComboBox.java
 * 
 * $Revision$
 *
 * Copyright (c) 2005 MoneyGram International
 */

package dw.dwgui;

import java.awt.event.KeyEvent;

import javax.swing.JComboBox;

/**
 * Abstract superclass for classes like MultiListComboBox that allow 
 * multiple lists to be stored while displaying only one of them. This
 * class implements the mix-in behavior specified by DWComponentInterface,
 * but makes no assumptions about how the multilist data is stored.
 */
public abstract class AbstractMultiListComboBox<E> extends JComboBox 
        implements DWComponentInterface {
	private static final long serialVersionUID = 1L;

	protected Object defaultList = null;
    protected boolean handleEnter = HANDLE_ENTER;

    public AbstractMultiListComboBox() {
        super();
    }

    /**
     * Put the component back into a default state.
     */
    public void reset() {
        setCurrentList(defaultList);
    }

    public void setDefaultList(Object key) {
        this.defaultList = key;
    }

    /**
     * Swap in the given list and set its default selection.
     */
    public abstract void setCurrentList(Object key);

	@Override
    public void processKeyEvent(final KeyEvent e) {
        if (!DWComponent.canProcessKeyEvent(e,handleEnter))
            super.processKeyEvent(e);
    }

	@Override
    public void setDoNotHandleEnter() {
        handleEnter = DO_NOT_HANDLE_ENTER;
    }
    
	@Override
    public void setHandleEnter() {
        handleEnter = HANDLE_ENTER;
    }
}
