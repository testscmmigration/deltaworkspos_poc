package dw.dwgui;

import java.awt.Dimension;
import java.awt.SystemColor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;

import dw.dialogs.Dialogs;
import dw.utility.DWValues;
import dw.utility.Debug;

public class HelpText extends JLabel implements MouseListener {
	private static final long serialVersionUID = 1L;

	private String helpText;
	private ImageIcon icon = new ImageIcon(this.getClass().getResource("/xml/images/faqIcon24.png"));

	public HelpText() {
		setVisible(false);
		addMouseListener(this);
	}

	private void showDialog() {

		if (helpText != null && helpText.length() > 0) {

			try {
				JTextPane pane = new JTextPane();
				pane.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
				pane.setText(helpText);
				pane.setEditable(false);
				pane.setBackground(SystemColor.control);
				JScrollPane scPane = new JScrollPane(pane);
				scPane.setPreferredSize(new Dimension(400, 200));
				scPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
				scPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
				JOptionPane optionPane = new JOptionPane(scPane);
				JDialog dialog = optionPane.createDialog(Dialogs.getParent(), DWValues.HELP_TEXT);
				dialog.setResizable(true);
				
				dialog.setVisible(true);
			} catch (Exception e) {
				Debug.printException(e);
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		showDialog();

	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	/**
	 * @return the helpText
	 */
	public String getHelpText() {
		return helpText;
	}

	/**
	 * @param helpText
	 *            the helpText to set
	 */
	public void setHelpText(String helpText) {
		this.helpText = helpText;
		if(helpText!=null && helpText.length()>0)
		{
			setIcon(icon);
			setVisible(true);
		}else
		{
			setVisible(false);
		}
	}

}
