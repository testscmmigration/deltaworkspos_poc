package dw.dwgui;

import dw.model.adapters.CountryInfo;
import dw.model.adapters.CountryInfo.CountrySubdivision;

/**
 * A custom combo box that stores and displays StateInfo objects. When the
 *   selected item is requested, it returns just the abbreviation to be stored
 *   in the customer object.
 */
public class StateListComboBox<E> extends MultiListComboBox<E> { 
	private static final long serialVersionUID = 1L;

	/**
     * Try to set the index of the list. If the list is empty, ignore
     */
    @Override
	public void setSelectedIndex(int index) {
        if (super.getItemCount() == 0)
            return;
        super.setSelectedIndex(index);
    }

    /**
     * Set the selected state in the list. If the state is not in the list,
     *   select the first one.
     */
    public void setSelectedItem(String abbrev) {
        for (int i = 0; i < getItemCount(); i++) {
        	CountrySubdivision info = (CountrySubdivision) getItemAt(i);
            if (info.getCountrySubdivisionCode().equalsIgnoreCase(abbrev)) {
                super.setSelectedItem(info);
                return;
            }
        }
        if (super.getItemCount() > 0)
            setSelectedIndex(0);
    }

    /**
     * Return the abbreviation of the state that is currently displayed in the
     *   combo box. If the list was empty (not US, CA or MX) return the text
     *   that was typed in the box.
     */
    public String getState() {
        if (super.getItemCount() == 0) {
            return (String) getSelectedItem();
        }
        CountrySubdivision info = (CountrySubdivision) super.getSelectedItem();
        if (info != null)
            return info.getCountrySubdivisionCode();
        return "";	
    }

    @Override
	public Object getSelectedItem() {
        Object item = super.getSelectedItem();
        if (item == null)
            return CountryInfo.getBlankCountrySubdivision();	
        return item;
    }
}
