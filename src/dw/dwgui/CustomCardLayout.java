package dw.dwgui;

import java.awt.CardLayout;
import java.awt.Component;

import dw.framework.PageFlowContainer;

/**
 * CustomCardLayout adds more accessibility features to the layout. It stores
 *   pages by name, and allows access to pages by name, index, etc.
 */
public class CustomCardLayout extends CardLayout {
	private static final long serialVersionUID = 1L;

	PageFlowContainer delegate;

    public void setDelegate(PageFlowContainer pfc) {
        delegate = pfc;
    }

    /** Compatibility for calls for methods refactored to PageFlowContainer. */
    
    public Component getVisiblePage() {
        //Debug.println("CCL.getVisiblePage() = " + delegate.getVisiblePage());	
        return delegate.getVisiblePage();
    }

    public String getVisiblePageName() {
        //Debug.println("CCL.getVisiblePageName() = " + delegate.getVisiblePageName());	
        return delegate.getVisiblePageName();
    }

    public Component getComponent(String name) {
        //Debug.println("CCL.getCardComponent("+ name+ ") = " + delegate.getCardComponent(name));	 
        return delegate.getCardComponent(name);
    }

    public String getComponentName(int i) {
        //Debug.println("CCL.getComponentName("+i+") = " + delegate.getComponentName(i));	 
        return delegate.getComponentName(i);
    }
}
