package dw.dwgui;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 * A custom text field that may be delimited to allow input limited to a
 *   specific length or a specific characted set.
 */
public class DelimitedTextField extends DWTextField {
	private static final long serialVersionUID = 1L;

    private DelimitedDocument doc;

	class DelimitedDocument extends PlainDocument {
		private static final long serialVersionUID = 1L;

	    private AttributeSet a = null;
	    private String characterSet = null;
	    private int maxFieldLength = 0;
	
	    public void setMaxFieldLength(int length) {
	        this.maxFieldLength = length;
	    }
	    
	    public int getMaxFieldLength() {
	        return this.maxFieldLength;
	    }
	
	    public String getAllowedCharacters() {
	        return this.characterSet;
	    }
	
	    public void setAllowedCharacters(String characters) {
	        this.characterSet = characters;
	    }
	
	    public AttributeSet getAttr() {
	        return a;
	    }
	
	    @Override
		public void insertString(int offs, String str, AttributeSet a)
	                  throws BadLocationException {
	        this.a = a;
	        if (str == null || str.length() > 1) {
	            super.insertString(offs, str, a);
	            return;
	        }
	        // ignore input if we have enough characters already
	        if ((maxFieldLength > 0) && (getLength() + str.length() > maxFieldLength)) {
	            return;
	        }
	        // ignore characters if they are not in the character set
	        if (characterSet != null && characterSet.indexOf(str) < 0)
	            return;
	        super.insertString(offs, str, a);
	    }
	}

	public DelimitedTextField() {
        doc = new DelimitedDocument();
        doc.setMaxFieldLength(0);
        setDocument(doc);
    }

    public void setMaxFieldLength(int length) {
        doc.setMaxFieldLength(length);
    }
    
    public void setValidChars(String characters) {
        doc.setAllowedCharacters(characters);
    }
//
//	@Override
//	public int getDataComponentHierarchyLevel() {
//		return hierarchyLevel;
//	}
//
//	@Override
//	public void setDataComponentHierarchyLevel(int level) {
//		this.hierarchyLevel = level;
//	}
}
