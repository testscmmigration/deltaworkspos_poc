package dw.dwgui;

import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPasswordField;

public class DWPasswordField extends JPasswordField implements DWComponentInterface {
	private static final long serialVersionUID = 1L;

    private boolean handleKeyEvents = true;
    private boolean handleEnter = HANDLE_ENTER;
    
    @Override
	public void processKeyEvent(KeyEvent e) {
        if (!(handleKeyEvents && DWComponent.canProcessKeyEvent(e,handleEnter)))
            super.processKeyEvent(e);
    }
    
    @Override
	public void addKeyListener(KeyListener l) {
        handleKeyEvents = false;
        super.addKeyListener(l);
    }
    
    @Override
	public void addActionListener(ActionListener l) {
        handleKeyEvents = false;
        super.addActionListener(l);
    }

    @Override
	public void setDoNotHandleEnter() {
        handleEnter = DO_NOT_HANDLE_ENTER;
    }

//    /** Does nothing. Allowing the echo char to actually be specified
//     * in the XML is a little dangerous because the XML isn't encrypted
//     * and certain values might actually turn masking off. But not having
//     * a method with this signature leads to "No Such Method" errors in
//     * the debug log.
//     * @author Geoff Atkin (and what a lot of work it was)
//     */
//    public void setEchoChar(String s) {
//    }
    
    @Override
	public void setHandleEnter() {
        handleEnter = HANDLE_ENTER;
    }
}
