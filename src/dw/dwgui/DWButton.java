package dw.dwgui;

import java.awt.event.KeyEvent;

import javax.swing.JButton;

public class DWButton extends JButton {
	private static final long serialVersionUID = 1L;

	private boolean eatEscape = false;
    private String textProperty;
    
    @Override
	public void processKeyEvent(KeyEvent e) {
        if (!DWComponent.canProcessKeyEvent(e, DWComponentInterface.DO_NOT_HANDLE_ENTER)){
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                e.setKeyChar(' ');
                e.setKeyCode(KeyEvent.VK_SPACE);
            }
            if (eatEscape && (e.getKeyCode() == KeyEvent.VK_ESCAPE)){
                e.consume();
                return;
            }
            super.processKeyEvent(e);
        }
    }
    
    public void setEatEscape(boolean b){
        eatEscape = b;
    }
    public boolean getEatEscape(){
        return eatEscape;
    }

	public String getTextProperty() {
		return textProperty;
	}

	public void setTextProperty(String textProperty) {
		this.textProperty = textProperty;
	}
}
