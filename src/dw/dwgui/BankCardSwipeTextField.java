package dw.dwgui;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import dw.utility.Debug;

/**
 * A text field which captures the multiple field input of a magnetic swipe
 * card. This component will only display a single field from the card, but will
 * internally capture all the fields entered by the card. Fields on both
 * MoneySaver cards and dispenser form packs use a '^' as a field delimiter, and
 * a '?' as an end-of-input delimiter. 
 *      MoneySaver fields:
 *          [0] - MoneySaver id number.
 *      Dispenser form pack fields :
 *          [0] - single digit (don't know what this is for)
 *          [1] - starting serial #
 *          [2] - # forms per pack
 *          [3] - money order type (single digit)
 *          [4] - stub format (ex : '4LNE')
 *          
 * NOTE : Enter key consuming logic is for handing incorrectly configured
 * keyboards. Some keyboards put an enter between tracks and some do not. We
 * don't want to let the action happen on the field before we catch everything
 * the card is going to throw.
 */
public class BankCardSwipeTextField extends JTextField {
	private static final long serialVersionUID = 1L;

	private static final long SWIPE_MAX_TIME = 250; // .25 second

	private CardSwipeDocument doc = null;
	private boolean bankCard = false;

	private boolean onFirstTrack = true;
	private boolean swipeDone = false;
	private Timer swipeDoneTimer;
	private SwipeDoneTimerTask swipeDoneTimerTask;

	/*
	 * Task to mark the swipe as being done after allowing for additional
	 * characters to be processed.
	 */
	private class SwipeDoneTimerTask extends TimerTask {
		@Override
		public void run() {
			Debug.println("SwipeDone!");
			swipeDone = true;
		}
	}

	private void cancelSwipeDoneTimer() {
		if (swipeDoneTimerTask != null) {
			swipeDoneTimerTask.cancel();
			swipeDoneTimerTask = null;
		}
		if (swipeDoneTimer != null) {
			swipeDoneTimer.cancel();
			swipeDoneTimer = null;
		}
	}

	/*
	 * Setup a timer to mark the swipe as being done.
	 */
	private void setupSwipeDoneTimer() {
		// Debug.println("setupSwipeDoneTimer");
		//
		// Get rid of any old timers
		//
		cancelSwipeDoneTimer();

		//
		// Setup swipeDoneTimerTask to mark the swipe as being done after
		// waiting for additional characters to come in.
		//
		swipeDoneTimer = new Timer();
		swipeDoneTimerTask = new SwipeDoneTimerTask();

		long maxIdleTime = SWIPE_MAX_TIME;

		if (! onFirstTrack) {
			maxIdleTime = 6 * maxIdleTime;
		}
		swipeDoneTimer.schedule(swipeDoneTimerTask, maxIdleTime);
	}

	private String nullToSpace(Object data) {
		if (data == null) {
			data = " ";
		}
		return (String) data;
	}

	private String maskString(String s) {
		s = nullToSpace(s);
		StringBuffer sb = new StringBuffer(s);
		for (int i = 0; i < sb.length() - 4; i++) {
			sb.replace(i, i + 1, "*");
		}
		s = sb.toString();
		return s;
	}

	public BankCardSwipeTextField() {
		this("", 0);
	}

	public BankCardSwipeTextField(String text, int displayIndex) {
		doc = new CardSwipeDocument();
		setDisplayIndex(displayIndex);
		setDocument(doc);
		setText(text);
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					// consume the enter press if we are still on track 1
					if (onFirstTrack)
						e.consume();
				}
			}
		});
	}

	public void setMaxFieldLength(int len) {
		doc.setMaxFieldLength(len);
	}

	/**
	 * Set the index of the field which is to be displayed as it is entered.
	 */
	public void setDisplayIndex(int index) {
		doc.setDisplayIndex(index);
	}

	public int getDisplayIndex() {
		return doc.getDisplayIndex();
	}

	public boolean isSwipeDone() {
		// Debug.println("isSwipeDone["+swipeDone+"]");
		return swipeDone;
	}

	/**
	 * Set the string which contains all the allowed characters for this field.
	 */
	public void setValidChars(String valid) {
		doc.setValidChars(valid);
	}

	/**
	 * Get a particular field that was entered.
	 */
	public String getField(int i) {
		Vector<StringBuffer> fields = doc.getFields();
		if (i >= fields.size()) {
			return null;
		} else {
			return (fields.elementAt(i)).toString();
		}
	}

	/**
	 * Get the number of fields that were entered.
	 */
	public int getNumFields() {
		return doc.getFields().size();
	}

	private void setOnFirstTrack(boolean b) {
		onFirstTrack = b;
	}

	@Override
	public void setText(String text) {
		setOnFirstTrack(true);
		doc.reset();
		super.setText(text);
	}

	public void reset() {
		doc.reset();
	}

	// public static void main(String[] args) {
	// JFrame f = new JFrame();
	// f.getContentPane().setLayout(new GridLayout(2, 1));
	// f.getContentPane().add(new CardSwipeTextField("", 0));
	// CardSwipeTextField cs2 = new CardSwipeTextField("", 1);
	// cs2.setValidChars("0123456789LNE");
	// f.getContentPane().add(cs2);
	// f.setSize(500, 100);
	// f.setVisible(true);
	// }

	/**
	 * Text document which captures only the valid characters from a MoneySaver
	 * card or a dispenser form pack card.
	 */
	class CardSwipeDocument extends PlainDocument {
		private static final long serialVersionUID = 1L;

		private String validChars = "0123456789";

		private int fieldIndex = 0;
		private char trackDelimiter = ';';
		private char fieldDelimiter = '^';
		private char endChar = '?';
		private boolean done = false;
		private int displayIndex = 0;
		private Vector<StringBuffer> fields = new Vector<StringBuffer>(); // of
																			// StringBuffers
		private int maxFieldLength = 0;

		/**
		 * Set the field back to its original state, ready to capture fields.
		 */
		public void reset() {
			fieldIndex = 0;
			done = false;
			onFirstTrack = true;
			bankCard = false;
			swipeDone = false;
			cancelSwipeDoneTimer();
		}

		public void newSwipe() {
			Debug.println("newSwipe");
			fields = new Vector<StringBuffer>();
			fieldIndex = 0;
			done = false;
			bankCard = true;
			onFirstTrack = true;
			cancelSwipeDoneTimer();
		}

		public void setMaxFieldLength(int len) {
			maxFieldLength = len;
		}

		/**
		 * Set the index of the field which should be displayed as it is
		 * captured. Only one field may be displayed.
		 */
		public void setDisplayIndex(int i) {
			displayIndex = i;
		}

		public int getDisplayIndex() {
			return displayIndex;
		}

		public void setValidChars(String valid) {
			this.validChars = valid;
		}

		/**
		 * Get a list of all the fields captured.
		 * 
		 * @return a Vector of StringBuffers of captured fields.
		 */
		public Vector<StringBuffer> getFields() {
			return fields;
		}

		/**
		 * When s string is inserted into the field, check its validity before
		 * adding it to the text. If a swipe card field delimiter is reached,
		 * add a new field to the list and start adding any following text to
		 * that field.
		 */
		@Override
		public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
			// Debug.println("insertString["+str+"],["+normalInputMethod+"]");

			/*
			 * If we are still getting data, reset the swipe done timer
			 */
			if (swipeDoneTimerTask != null) {
				setupSwipeDoneTimer();
			}
			/*
			 * If we think this is a card swipe, Ignore all input unit we see
			 * the "%"
			 */
			if (! bankCard) {
				int start = str.lastIndexOf("%");
				if (start != -1) {
					bankCard = true;
					str = str.substring(start);
				} else {
					return;
				}
			}

			if (str.length() > 0 && str.charAt(0) == trackDelimiter) {
				onFirstTrack = false;
				setupSwipeDoneTimer();
				return;
			}
			/*
			 * for bankcards just read track 1
			 */
			if (bankCard && str.charAt(0) == endChar) {
				onFirstTrack = false;
				setupSwipeDoneTimer();
				return;
			}
			// end of card input has been signaled. do not accept anything more
			if (done) {
				// Debug.println("Early Exit");
				return;
			}

			// Check for the start sentinel, if so assume card swipe
			if (str.length() > 0 && str.charAt(0) == '%') {
				swipeDone = false;
				// Debug.println("Card Swipe");
				super.remove(0, super.getLength());
				newSwipe();
			}

			StringBuffer sb;
			// get the current field to add text to. add a new one if needed
			if (fields.size() == fieldIndex) {
				sb = new StringBuffer();
				fields.add(sb);
			}
			sb = fields.elementAt(fieldIndex);

			// check that each char of the input string is valid before adding
			// it
			for (int i = 0; i < str.length(); i++) {
				// end of field reached, start a new field.
				if (str.charAt(i) == fieldDelimiter && sb.length() > 0) {
					if ((fieldIndex == 0) && bankCard) {
						String acctNbr = getField(0);
						super.remove(0, super.getLength());
						super.insertString(0, maskString(acctNbr), a);
					}
					fieldIndex++;
					return;
				}
				// end of the input reached. make sure last field not empty.
				else if (str.charAt(i) == endChar) {
					done = true;
					StringBuffer last = fields.elementAt(fields.size() - 1);
					if (last.length() == 0) {
						fields.remove(last);
					}
					return;
				}

				/*
				 * For bank cards don't allow non-digits in the account number
				 * (this will strip out the leading 'B' for swiped cards.
				 */
				if (bankCard && (fieldIndex == 0) && ! Character.isDigit(str.charAt(i))) {
					/*
					 * Check if the character we are looking at is at the end of
					 * the string (remembering index starts at 0).
					 */
					if ((i + 1) >= str.length()) {
						/*
						 * If the character is the only character in the string
						 * just exit, otherwise strip off the last character of
						 * the string
						 */
						if (i == 0) {
							return;
						} else {
							str = str.substring(0, i);
						}
					} else {
						str = str.substring(0, i) + str.substring(i + 1);
					}
				}

				if (validChars.indexOf(str.charAt(i)) < 0) {
					return;
				}
			}

			int currentLength = this.getLength();

			if (currentLength < maxFieldLength) {
				sb.append(str);
			}
		}

		/**
		 * When characters are removed, check for the length of the remaining
		 * text. If the field is now empty, reset the field input.
		 */
		@Override
		public void remove(int offs, int len) throws BadLocationException {
			StringBuffer sb;
			sb = fields.elementAt(0);
			sb.delete(offs, offs + len);
			super.remove(offs, len);
			/*
			 * If modifying swipe data, change input method to keyboard and
			 * delete other swipe data.
			 */
			// if (!normalInputMethod) {
			// normalInputMethod = true;
			for (int ii = 1; ii < fields.size(); ii++) {
				sb = fields.elementAt(ii);
				sb.delete(0, sb.length());
			}
			if (getText(0, getLength()).equals("")) {
				fields = new Vector<StringBuffer>();
				reset();
			}
		}
	}
}
