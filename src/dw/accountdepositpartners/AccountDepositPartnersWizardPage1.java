package dw.accountdepositpartners;

import java.awt.Component;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;

import dw.dwgui.DWTable;
import dw.dwgui.DWTable.DWTableCellRenderer;
import dw.dwgui.DWTable.DWTableListener;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.dwgui.UneditableTableModel;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.CountryInfo.Country;
import dw.utility.ServiceOption;

/**
 * Page Code: ADP01
 * 
 * Page 1 of the AccountDepositParters wizard. This page displays a list of the
 * account deposit partners.
 */
public class AccountDepositPartnersWizardPage1 extends
		AccountDepositPartnersFlowPage implements DWTextPaneListener, DWTableListener {
	private static final long serialVersionUID = 1L;
	
	private static final int MINIMUM_SCREEN_WIDTH = 500;
	private static final int DATA_OBJ_INDX = 2;

	private DWTable partnerTable;
	private JScrollPane scrollPane;
	private DefaultTableModel partnerTableModel;
	private List<ServiceOption> partners;
	private int selectedRow = 0;
	private JButton cancelButton;
	private JPanel panel1;
	private DWTextPane contentMessage;
	private JLabel noContentMessage;
	
	public AccountDepositPartnersWizardPage1(
			AccountDepositPartnersTransaction tran, String name,
			String pageCode, PageFlowButtons buttons) {
		super(tran, "accountdepositpartners/AccountDepositPartners1.xml", name, pageCode, buttons);
		scrollPane = (JScrollPane) getComponent("partnersScrollpane");
		partnerTable = (DWTable) getComponent("partnerTable");
		contentMessage = (DWTextPane) getComponent("contentMessage");
		panel1 = (JPanel) getComponent("panel1");
		noContentMessage = (JLabel) getComponent("noContentMessage");
		partnerTable.addListener(this, this);
		contentMessage.addListener(this, this);
	}

	private class ADPTableCellRenderer extends DWTableCellRenderer {
		private static final long serialVersionUID = 1L;
		
		public ADPTableCellRenderer(DWTable table, int minMargin) {
			table.super(minMargin);
		}

        @Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        	JComponent rend = (JComponent) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            rend.setBorder(BorderFactory.createEmptyBorder(0, getCellMargin(), 0, getCellMargin()));
            return rend;
        }
	}
	
	@Override
	public void dwTableResized() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				partnerTable.resizeTable();
			} 
		});
	}
	
	@Override
	public void dwTextPaneResized() {
		contentMessage.setWidth(partnerTable.getPreferredSize().width);
		contentMessage.setListenerEnabled(false);
	}

	private void setupComponents() {
		setupPartnerTable();
	}

	private void setupPartnerTable() {
		String[] columnNames = {
				Messages.getString("AccountDepositPartnersWizardPage1.2"),
				Messages.getString("AccountDepositPartnersWizardPage1.4") ,""};
		partnerTable.getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		ListSelectionModel selectionModel = partnerTable.getTable().getSelectionModel();
		selectionModel.addListSelectionListener(new SharedListSelectionHandler());
		partnerTable.getTable().setSelectionModel(selectionModel);			
		
		DefaultTableColumnModel columnModel = new DefaultTableColumnModel();
		partnerTable.getTable().setColumnModel(columnModel);
		
		partnerTableModel = new UneditableTableModel(columnNames, 0);
		partnerTable.getTable().setModel(partnerTableModel);
		
		partnerTable.getTable().getTableHeader().setReorderingAllowed(false);
		partnerTable.getTable().getTableHeader().setResizingAllowed(false);
		
		partnerTable.getTable().getColumnModel().getColumn(0).setCellRenderer(new ADPTableCellRenderer(partnerTable, DWTable.DEFAULT_COLUMN_MARGIN));
		partnerTable.getTable().getColumnModel().getColumn(1).setCellRenderer(new ADPTableCellRenderer(partnerTable, DWTable.DEFAULT_COLUMN_MARGIN));
		partnerTable.getTable().getColumnModel().getColumn(2).setCellRenderer(new ADPTableCellRenderer(partnerTable, DWTable.DEFAULT_COLUMN_MARGIN));
		partnerTable.getTable().removeColumn(partnerTable.getTable().getColumnModel().getColumn(DATA_OBJ_INDX));
		partnerTable.setFont(new Font("SansSerif", Font.PLAIN, 12));

		partnerTable.setFocusable(false);
	}

	@Override
	public void start(int direction) {
		setupComponents();
		
		PageNotification.notifyExitListeners(this, PageExitListener.DONOTHING);
		flowButtons.reset();
		flowButtons.setVisible("expert", false);
		
		flowButtons.setEnabled("back", false);
		cancelButton = flowButtons.getButton("cancel"); 
		
		KeyListener nexter = new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					e.consume();
					tryNext();
				}else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					e.consume();
					cancelButton.doClick();
				}
			}
		};
		addKeyListener(partnerTable.getTable(), nexter); 
		
		List<ServiceOption> data = ServiceOption.getDirectDepositServiceOptions();
		Collections.sort(data, new Comparator<ServiceOption>() {
			@Override
			public int compare(ServiceOption o1, ServiceOption o2) {
				String cc1 = o1 != null ? o1.getCountryAndCurrency() : "";
				String cc2 = o2 != null ? o2.getCountryAndCurrency() : "";
				int c = cc1.compareTo(cc2);
				if (c == 0) {
					String so1 = "";
					if (o1 != null && o1.getServiceOptionDisplayName() != null) {
						so1 = o1.getServiceOptionDisplayName();
					}
					String so2 = "";
					if (o2 != null && o2.getServiceOptionDisplayName() != null) {
						so2 = o2.getServiceOptionDisplayName();
					}
					c = so1.compareTo(so2);
				}
				return c;
			}
		});
		
		if (! data.isEmpty()) {
			for (ServiceOption serviceOption : data) {
				String countryName = CountryInfo.getCountryName(serviceOption.getDestinationCountry());
				String serviceOptionName = serviceOption.getServiceOptionDisplayName();
				String countryCurrencyPair = countryName + " - " + serviceOption.getReceiveCurrency();
				partnerTableModel.addRow(new Object[] {countryCurrencyPair, serviceOptionName, serviceOption});
			}
			noContentMessage.setVisible(false);
			partnerTable.getTable().setRowSelectionInterval(selectedRow, selectedRow);
			flowButtons.getButton("next").setEnabled(true);
			
			partnerTable.initializeTable(scrollPane, panel1, MINIMUM_SCREEN_WIDTH, 10);
		} else {
			contentMessage.setVisible(false);
			scrollPane.setVisible(false);
			flowButtons.getButton("next").setEnabled(false);			
		}
		
		partnerTable.getTable().requestFocus();
	}

	public void tryNext() {
		flowButtons.getButton("next").doClick(); 
	}
	/* (non-Javadoc)
	 * @see dw.framework.FlowPage#finish(int)
	 */
	@Override
	public void finish(int direction) {
		if (partners != null) {
			partners.clear();
		}
		PageNotification.notifyExitListeners(this, direction);
		ServiceOption deliveryInfo = transaction.getSelectedDeliveryInfo();
		if (deliveryInfo != null) {
			Country country = CountryInfo.getCountry(deliveryInfo.getDestinationCountry());
			transaction.setDestination(country);
			ServiceOption qdo = new ServiceOption();
			qdo.setServiceOption(deliveryInfo.getServiceOption()); 
			qdo.setDestinationCountry(deliveryInfo.getDestinationCountry());
			qdo.setReceiveCurrency(deliveryInfo.getReceiveCurrency());
			qdo.setPayoutCurrency(deliveryInfo.getPayoutCurrency());
			qdo.setReceiveAgentID(deliveryInfo.getReceiveAgentID());
			qdo.setReceiveAgentAbbreviation(deliveryInfo.getReceiveAgentAbbreviation());
			transaction.setQualifiedDeliveryOption(qdo);
			transaction.setHistoryQDO(qdo);
			transaction.setServiceOptionChanged(false);
		}
	}
	
	private class SharedListSelectionHandler implements ListSelectionListener {
		@Override
		public void valueChanged(ListSelectionEvent e) {
			int tempselectedRow = partnerTable.getTable().getSelectedRow();
			if (tempselectedRow >= 0) {				
				ServiceOption partner = (ServiceOption) partnerTableModel.getValueAt(tempselectedRow, DATA_OBJ_INDX);
				selectedRow = tempselectedRow;
				transaction.setSelectedDeliveryInfo(partner);
			}
		}
	}
}
