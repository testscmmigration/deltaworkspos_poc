package dw.accountdepositpartners;

import dw.framework.ClientTransaction;
import dw.framework.PageNameInterface;
import dw.framework.TransactionInterface;

public abstract class AccountDepositPartnersTransactionInterface extends
		TransactionInterface {
	private static final long serialVersionUID = 1L;

	protected AccountDepositPartnersTransaction transaction;

	public AccountDepositPartnersTransactionInterface(String fileName,
			AccountDepositPartnersTransaction transaction,
			PageNameInterface naming, boolean sideBar) {
		super(fileName, naming, sideBar);
		this.transaction = transaction;
	}

	@Override
	public ClientTransaction getTransaction() {
		return transaction;
	}
}
