package dw.accountdepositpartners;

import dw.mgsend.MoneyGramSendTransaction;
import dw.utility.ServiceOption;

public class AccountDepositPartnersTransaction extends MoneyGramSendTransaction	{
	private static final long serialVersionUID = 1L;

	public AccountDepositPartnersTransaction(String tranName, String tranReceiptPrefix, int docSeq) {
		super(tranName, tranReceiptPrefix, docSeq);
		setSendToAccount(true);
		init();
	}
	
	public AccountDepositPartnersTransaction(String tranName) {
		super(tranName, "");
		setAccountDepositPartners(true);
		init();
	}

	private void init() {
		timeoutEnabled = true;
		dispenserNeeded = false;
		receiptPrinterNeeded = false;
		clear();
	}
	
	@Override
	public boolean isFinancialTransaction() {
		return true;
	}

	@Override
	public void setFinancialTransaction(boolean b) {
	}

	@Override
	public boolean isTransmitRequired(boolean flag) {
		if (super.isTransmitRequired(flag)) {
			return true;
		} else {
			return ServiceOption.needTransmit();
		}
	}

	@Override
	public boolean isCheckinRequired() {
		if (isSendToAccount()) {
			return super.isCheckinRequired();
		}
		return false;
	}

	@Override
	public boolean isNamePasswordRequired() {
		if (isSendToAccount()) {
			return super.isNamePasswordRequired();
		}
		return false;
	}

	@Override
	public boolean isPasswordRequired() {
		if (isSendToAccount()) {
			return super.isPasswordRequired();
		}
		return false;
	}

	@Override
	public boolean isUsingWizard() {
		return true;
	}
}
