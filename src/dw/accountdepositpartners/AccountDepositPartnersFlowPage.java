package dw.accountdepositpartners;

import dw.framework.FlowPage;
import dw.framework.PageFlowButtons;

public abstract class AccountDepositPartnersFlowPage extends FlowPage {
	private static final long serialVersionUID = 1L;

	protected AccountDepositPartnersTransaction transaction;

	public AccountDepositPartnersFlowPage(
			AccountDepositPartnersTransaction transaction, String fileName,
			String name, String pageCode, PageFlowButtons buttons) {
		super(fileName, name, pageCode, buttons);
		this.transaction = transaction;
	}

	// do-nothing impl of commCompleteInterface...will be overridden by each
	// specific page where comm is needed.
	@Override
	public void commComplete(int commTag, Object returnValue) {
	}
}
