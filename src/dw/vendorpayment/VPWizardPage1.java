package dw.vendorpayment;


import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.moneyorder.MoneyOrderFlowPage;
import dw.moneyorder.MoneyOrderTransaction;
import dw.moneyorder.MoneyOrderTransaction40;

/**
 * Page 1 of the VendorPayment Wizard.
 * This page prompts the user to select a vendor from a list of available
 *  vendors. The user must select one and press the next button to continue.
 */
public class VPWizardPage1 extends MoneyOrderFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	private JList vendorList = null;
    private DefaultListModel listModel = null;
    public String vendor = null;

    public VPWizardPage1(MoneyOrderTransaction40 tran, String name, String pageCode, PageFlowButtons buttons) {
            this((MoneyOrderTransaction) tran, name, pageCode, buttons);
    }
    
    public VPWizardPage1(MoneyOrderTransaction tran,
                       String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "vendorpayment/VPWizardPage1.xml", name, pageCode, buttons);	
        // collect the component references
        vendorList = (JList) getComponent("vendorList");	
        listModel = (DefaultListModel) vendorList.getModel();
        
		DWTextPane text = (DWTextPane) getComponent("text");
		text.addListener(this, this);
    }
    
	@Override
	public void dwTextPaneResized() {
		DWTextPane text = (DWTextPane) this.getComponent("text");
		JScrollPane sp = (JScrollPane) this.getComponent("scrollpane");
		JPanel panel1 = (JPanel) this.getComponent("panel1");
		JLabel label = (JLabel) this.getComponent("label");
		
		sp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		int width = Math.max(vendorList.getPreferredSize().width, 400);
		
		int w = label.getFontMetrics(label.getFont()).getWidths()[' '];
		int n = (width - label.getPreferredSize().width) / w;
		StringBuffer sb = new StringBuffer();
		sb.append(label.getText());
		for (int i = 0; i < n; i++) {
			sb.append(' ');
		}
		label.setText(sb.toString());
		
		sp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

		int viewPortHeight = sp.getVisibleRect().height;
		int listHeight = vendorList.getPreferredSize().height;
		vendorList.setPreferredSize(new Dimension(width, listHeight));
		
		Dimension d1;
		if (viewPortHeight < listHeight) {
			sp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			int newHeight = vendorList.getPreferredSize().height - listHeight + viewPortHeight;
			d1 = new Dimension(width + sp.getVerticalScrollBar().getPreferredSize().width, newHeight);
		} else {
			d1 = new Dimension(width, vendorList.getPreferredSize().height + 2);
		}

		panel1.setPreferredSize(d1);
		sp.setPreferredSize(d1);
		
		vendorList.invalidate();
		this.doLayout();
		this.repaint();
		
		text.setWidth(width);
	}
    
    @Override
	public void start(int direction) {
        getRootPane().setDefaultButton(flowButtons.getButton("next"));	
        flowButtons.reset();
        flowButtons.setEnabled("back", false);	
        vendorList.requestFocus();
        // fill the list with the available vendors
        String[] vendors = transaction.getVendorList();
        listModel.removeAllElements();
        for (int i = 0; i < vendors.length; i++) {
            listModel.addElement(vendors[i]);
        }
        // select the top vendor in the list if visitng this screen first time.
        //otherwise set it to the one that was selected. tracker #597.
        if (vendor == null)
            vendorList.setSelectedIndex(0);
        else
            vendorList.setSelectedValue(vendor,true);    
        JComponent[] focusOrder = {vendorList, flowButtons.getButton("expert"),	
                                   flowButtons.getButton("back"),	
                                   flowButtons.getButton("next"),	
                                   flowButtons.getButton("cancel")};	
        setFocusOrder(focusOrder);

        final JButton nextButton = flowButtons.getButton("next");	
        addKeyListener("vendorList", new KeyAdapter() {	
                     @Override
					public void keyPressed(KeyEvent e) {
                         if (e.getKeyCode() == KeyEvent.VK_ENTER){
                             e.consume();
                             nextButton.doClick();
                         }
                     }}); 
    }

    public void vendorAction() {
        flowButtons.getButton("next").doClick();	
    }

    @Override
	public void finish(int direction) {
        if (direction == PageExitListener.NEXT) {
            vendor = (String) vendorList.getSelectedValue();
            if (vendor == null)
                vendor = transaction.getEmptyItemText();
            transaction.setSelectedVendor(vendor);
        }
        PageNotification.notifyExitListeners(this, direction);
    }
}
