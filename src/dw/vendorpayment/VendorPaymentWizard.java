package dw.vendorpayment;

import java.awt.event.KeyEvent;

import dw.framework.ClientTransaction;
import dw.framework.KeyMapManager;
import dw.framework.PageExitListener;
import dw.framework.PageNameInterface;
import dw.moneyorder.MOWizardPage1;
import dw.moneyorder.MOWizardPage2;
import dw.moneyorder.MOWizardPage5;
import dw.moneyorder.MOWizardPage6;
import dw.moneyorder.MoneyOrderTransaction;
import dw.moneyorder.MoneyOrderTransaction40;
import dw.moneyorder.MoneyOrderTransactionInterface;

/**
 * The Wizard Interface for issuing Money Orders.
 */
public class VendorPaymentWizard extends MoneyOrderTransactionInterface {
	private static final long serialVersionUID = 1L;


    public VendorPaymentWizard(MoneyOrderTransaction transaction,
                               PageNameInterface naming) {
        super("vendorpayment/VPWizardPanel.xml", transaction, naming, true);	
        createPages();
    }

    /**
     * Add custom page traversal logic here. If no custom logic needed, just
     *   call super.exit(direction) to do default traversal.
     */
    @Override
	public void exit(int direction) {
        String currentPage = cardLayout.getVisiblePageName();
        // special cases here
    /*    if (currentPage.equals("itemTable")){
           ((MoneyOrderTransaction40) transaction).lockCciLog();
            showPage("summary", PageExitListener.NEXT); 
            return;
                 
        }  */
        if (direction == PageExitListener.CANCELED){
            transaction.escSAR();
            ((MoneyOrderTransaction40) transaction).unlockCciLog();
            super.exit(direction);
            return;
        }
        if (direction == PageExitListener.COMPLETED){
            transaction.f2NAG();
            ((MoneyOrderTransaction40) transaction).unlockCciLog();
            super.exit(direction);
            return;
        }
        if (direction == PageExitListener.TIMEOUT) {
            ((MoneyOrderTransaction40) transaction).unlockCciLog();
            super.exit(direction);
            return;
        }
        if (direction == PageExitListener.FAILED) {
            ((MoneyOrderTransaction40) transaction).unlockCciLog();
        }
        if (currentPage.equals("summary") &&    
            transaction.getStatus() != ClientTransaction.COMPLETED &&
                 transaction.isTransactionPrinted()){
            showPage("printCanceled", PageExitListener.NEXT); 
            return;
        }
       
        // default traversal if no special case
        super.exit(direction);
    }
    /* 
     * DW40 - OldMOWizardPage1 and OldMOWizardPage2 which use MOneyOrderInfo should be used.
     *
     */
    @Override
	protected void createPages() {
        addPage(VPWizardPage1.class, transaction, "vendorSelection", "VPW05", buttons);	
       // addPage(OldMOWizardPage1.class, transaction, "firstAmount", "VPW10", buttons);	
        addPage(MOWizardPage1.class, transaction, "firstAmount", "VPW10", buttons);   
        //addPage(OldMOWizardPage2.class, transaction, "additionalAmounts", "VPW15", buttons);	
        addPage(MOWizardPage2.class, transaction, "additionalAmounts", "VPW15", buttons);    
        addPage(VPWizardPage2.class, transaction, "itemTable", "VPW20", buttons);	
        addPage(VPWizardPage3.class, transaction, "summary", "VPW25", buttons);	
        addPage(MOWizardPage5.class, transaction, "printSuccess", "VPW30", buttons);	
       // addPage(OldMOWizardPage6.class, transaction, "printCanceled", "VPW35", buttons);	
        addPage(MOWizardPage6.class, transaction, "printCanceled", "VPW35", buttons); 
    }

    /**
     * Add the single keystroke mappings to the buttons. Use the function keys
     *   for back, next, cancel, etc.
     */
    private void addKeyMappings() {
        KeyMapManager.mapMethod(this, KeyEvent.VK_F2, 0, "f2SAR");	
        buttons.addKeyMapping("expert", KeyEvent.VK_F4, 0);	
        buttons.addKeyMapping("back", KeyEvent.VK_F11, 0);	
        buttons.addKeyMapping("next", KeyEvent.VK_F12, 0);	
        buttons.addKeyMapping("cancel", KeyEvent.VK_ESCAPE, 0);	
    }

    @Override
	public void start() {
        super.start();
        addKeyMappings();
    }
    
    public void f2SAR() {
        transaction.f2SAR();
    }
}
