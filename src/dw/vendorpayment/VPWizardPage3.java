package dw.vendorpayment;

import java.awt.event.KeyEvent;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.moneyorder.MoneyOrderFlowPage;
import dw.moneyorder.MoneyOrderTransaction;
import dw.moneyorder.MoneyOrderTransaction40;

/**
 * Page 3 of the Vendor Payment Wizard.
 * This page displays the amounts of the vendor payments.
 *   When the print button is activated, the money orders will print and a
 *   progress window will display to show the status of the operation.
 */
public class VPWizardPage3 extends MoneyOrderFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	public static final int HORIZONTAL_NO_EXPANSION_UPON_RESIZE = 275;
	private JLabel totalLabel = null;
    private JLabel vendorLabel = null;

    public VPWizardPage3(MoneyOrderTransaction40 tran, String name, String pageCode, PageFlowButtons buttons) {
        this((MoneyOrderTransaction) tran, name, pageCode, buttons);
    }
    
    public VPWizardPage3(MoneyOrderTransaction tran,
                       String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "vendorpayment/VPWizardPage3.xml", name, pageCode, buttons);	
        totalLabel = (JLabel) getComponent("totalLabel");	
        vendorLabel = (JLabel) getComponent("vendorLabel");	
        
		DWTextPane tp = (DWTextPane) getComponent("screenInstr");
		tp.addListener(this, this);
    }


	@Override
	public void dwTextPaneResized() {
		DWTextPane tp = (DWTextPane) getComponent("screenInstr");
		JPanel panel = (JPanel) getComponent("main");
		int width = panel.getPreferredSize().width;
		if (width < HORIZONTAL_NO_EXPANSION_UPON_RESIZE) {
			width = HORIZONTAL_NO_EXPANSION_UPON_RESIZE;
		}
		tp.setWidth(width);
		tp.setListenerEnabled(false);
	}

    @Override
	public void start(int direction) {
        flowButtons.reset();
        flowButtons.setVisible("expert", false);	
        flowButtons.setEnabled("expert", false);	
        flowButtons.setAlternateText("next", Messages.getString("VPWizardPage3.printText"));	
        flowButtons.setAlternateKeyMapping("next", KeyEvent.VK_F5, 0);	
        totalLabel.setText(transaction.getTotal(
                              null, false).toString());
        vendorLabel.setText(transaction.getSelectedVendor());
        flowButtons.setSelected("next");	

        JComponent[] focusOrder = {flowButtons.getButton("back") ,	
                                   flowButtons.getButton("next"),	
                                   flowButtons.getButton("cancel")};	
        setFocusOrder(focusOrder);

        flowButtons.setSelected("next");	
    }

    @Override
	public void finish(int direction) {
        if (direction == PageExitListener.NEXT) {
            //lock the CCI Log before starting to print.
            ((MoneyOrderTransaction40) transaction).lockCciLog(); 
            // try the print and handle the results
            flowButtons.disable();
            printMoneyOrders();
        }
        PageNotification.notifyExitListeners(this, direction);
    }
}
