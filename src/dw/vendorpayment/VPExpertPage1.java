/*
 * VPExpertPage1.java
 * 
 * $Revision$
 *
 * Copyright (c) 2000-2005 MoneyGram International
 */

package dw.vendorpayment;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.moneygram.agentconnect.MoneyOrderInfo;

import dw.dialogs.Dialogs;
import dw.dwgui.CustomTableCellRenderer;
import dw.dwgui.DWTable;
import dw.dwgui.DWTable.DWTableListener;
import dw.dwgui.MoneyField;
import dw.dwgui.RowSelectCellRenderer;
import dw.dwgui.UneditableTableModel;
import dw.framework.ClientTransaction;
import dw.framework.KeyMapManager;
import dw.framework.PageExitListener;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.moneyorder.MoneyOrderFlowPage;
import dw.moneyorder.MoneyOrderTransaction;
import dw.moneyorder.MoneyOrderTransaction40;
import dw.moneyorder.PrintCanceledDialogMO;

/**
 * Page 1 (and only page) of the Money Order Expert interface.
 * This page allows the user to enter multiple money orders. Each money order
 *   entered will be displayed in the money order table. If the up arrow key
 *   is pressed at any time on this page, the last item in the money order
 *   table will be highlighted and the amount will be populated in the amount
 *   field. The amount may be edited at this time and will then be updated in
 *   the table. When all money orders have been entered, The user may activate
 *   print button to print the money orders. After the print process is finished
 *   the interface will exit back to the main menu. If printing fails, a dialog
 *   window will be displayed with details of what printed and what didn't.
 *   Based on a profile setting, the user may have the option of issuing a money
 *   order with no fee, and also may have the option of entering the amount
 *   tendered to get the change amount due.
 */
public class VPExpertPage1 extends MoneyOrderFlowPage implements DWTableListener  {
	private static final long serialVersionUID = 1L;

	// component references
    private MoneyField amountField = null; 
    private JLabel totalLabel = null;
    private JButton cancelButton = null;
    private JButton totalButton = null;
    private JButton printButton = null;
    private JComboBox vendorList = null;

    // table information
    private DWTable table = null;
    private DefaultTableModel tableModel = null;
    private int tableRowSelected = -1;
    
    // previous amount and vendor repeat functionality
    private BigDecimal previousAmount = BigDecimal.ZERO;	
    private String previousVendor = "";	
    
    private List<MoneyOrderInfo> vendorPayments = null;
	private JScrollPane scrollpane1;
	private JPanel panel1;
    
    public VPExpertPage1(MoneyOrderTransaction40 tran, String name, String pageCode) {
        this((MoneyOrderTransaction) tran, name, pageCode);
    }

    public VPExpertPage1(MoneyOrderTransaction tran, String name, String pageCode) {
        super(tran, "vendorpayment/VPExpertPage1.xml", name, pageCode, null);	
		scrollpane1 = (JScrollPane) this.getComponent("scrollpane");	
		panel1 = (JPanel) this.getComponent("panel1");
        table = (DWTable) getComponent("venderPaymentTable");
        collectComponents();
    }

    private void collectComponents() {
        amountField = (MoneyField) getComponent("amountField");	
        cancelButton = (JButton) getComponent("cancelButton");	
        totalButton = (JButton) getComponent("totalButton");	
        printButton = (JButton) getComponent("printButton");	
        vendorList = (JComboBox) getComponent("vendorList");	
        totalLabel = (JLabel) getComponent("totalLabel");	
        String[] focusOrder = {
           "vendorList", "amountField", "removeButton", "repeatButton", 
           "totalButton","wizardButton", "printButton", "cancelButton"}; 
        setFocusOrder(focusOrder);
        setupTable();
    }

	@Override
	public void dwTableResized() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.resizeTable();
				panel1.revalidate();
				scrollpane1.repaint();
			} 
		});
	}
    
    private void setupListeners() {
        vendorList.setEditable(false);
        addActionListener("wizardButton", this);	
        addActionListener("cancelButton", this);	
        addActionListener("amountField", this);	
        addActionListener("repeatButton", this);	
        addActionListener("removeButton", this);	
        addActionListener("totalButton", this);	
        addActionListener("printButton", this);	
        addFocusGainedListener("amountField", this, "disablePrintButton");	
        KeyMapManager.mapMethod(this, KeyEvent.VK_UP, 0, "upArrowAction");	
        KeyMapManager.mapMethod(this, KeyEvent.VK_DOWN, 0, "downArrowAction");	
        KeyMapManager.mapComponent(this, KeyEvent.VK_ESCAPE, 0, cancelButton);
        table.getTable().getSelectionModel().addListSelectionListener(
                      new ListSelectionListener() {
                            @Override
							public void valueChanged(ListSelectionEvent e) {
                                selectTableRow(table.getTable().getSelectedRow());
                            }
                      });
        vendorList.addKeyListener(new KeyAdapter() {
                     @Override
					public void keyPressed(KeyEvent e) {
                         if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                             e.consume();
                             cancelButton.doClick();
                         }
                         else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                             e.consume();
                             amountField.requestFocus();
                         }
                     }});
    }

    /**
     * Create and set up the vendor payment table. Add the table to the
     *   scrollpane defined in the XML.
     */
    private void setupTable() {
        String[][] data = {};
        String[] colNames = {Messages.getString("VPExpertPage1.idText"), Messages.getString("VPExpertPage1.amountText"),	
        		Messages.getString("VPExpertPage1.vendorText")};	
        tableModel = new UneditableTableModel(data, colNames);
        table.getTable().setModel(tableModel);
        RowSelectCellRenderer.setAsDefaultRenderer(table.getTable());
        TableColumnModel tcm = table.getTable().getColumnModel();

        // set up the table column renderers
        
        TableColumn idCol = tcm.getColumn(0);
        TableCellRenderer tcr = new CustomTableCellRenderer(CustomTableCellRenderer.LEFT_CELL, DWTable.DEFAULT_COLUMN_MARGIN);
        idCol.setCellRenderer(tcr);
        tcr = new CustomTableCellRenderer(CustomTableCellRenderer.LEFT_HEADER, DWTable.DEFAULT_COLUMN_MARGIN);
        idCol.setHeaderRenderer(tcr);
        
        TableColumn amountCol = tcm.getColumn(1);
        tcr = new CustomTableCellRenderer(CustomTableCellRenderer.MONETARY_CELL, DWTable.DEFAULT_COLUMN_MARGIN);
        amountCol.setCellRenderer(tcr);
        tcr = new CustomTableCellRenderer(CustomTableCellRenderer.MONETARY_HEADER, DWTable.DEFAULT_COLUMN_MARGIN);
        amountCol.setHeaderRenderer(tcr);
        
        TableColumn vendorCol = tcm.getColumn(2);
        tcr = new CustomTableCellRenderer(CustomTableCellRenderer.LEFT_CELL, DWTable.DEFAULT_COLUMN_MARGIN);
        vendorCol.setCellRenderer(tcr);
        
        table.getTable().setEnabled(true);

        table.addListener(this, this);
		table.setListenerEnabled(false);
		
		table.getTable().setFocusable(true);
    }

    @Override
	public void start(int direction) {
        setButtonsEnabled(true);
        previousAmount = BigDecimal.ZERO;	
        previousVendor = "";	
        amountField.setText("0");	
        totalLabel.setText("0.00");	
        setupListeners();
        refreshTable();
        disablePrintButton();
        // get the vendors and add them to the vendor list
        vendorList.removeAllItems();
        String[] vendors = transaction.getVendorList();
        for (int i = 0; i < vendors.length; i++)
            vendorList.addItem(vendors[i]);

        String[] focusOrder = {"vendorList", "amountField", "removeButton", 
                               "repeatButton", "totalButton", "wizardButton",
                               "printButton", "cancelButton"}; 
        setFocusOrder(focusOrder);
        amountField.requestFocus();

		table.setListenerEnabled(true);
    }

    public void disablePrintButton() {
        printButton.setEnabled(false);
    }

    /**
     * Get the money order info from the transaction and populate the table.
     */
    private void refreshTable() {
        int rowCount = tableModel.getRowCount();
        // clear out the table
        for (int i = 0; i < rowCount; i++) {
            tableModel.removeRow(0);
        }
       // java.util.List vendorPaymentList = transaction.getMoneyOrders();
        vendorPayments = transaction.getMoneyOrderList();
        // add all the transaction's vendor payments to the table
        for (int i = 0; i < vendorPayments.size(); i++){
        	MoneyOrderInfo vendorPayment = vendorPayments.get(i);
            addToTable(vendorPayment);
        }
        
        tableRowSelected = -1;
        amountField.requestFocus();
        scrollToRow(tableModel.getRowCount() -1);
        // update the total field
        totalLabel.setText(getTotal().toString());
		table.initializeTable(scrollpane1, panel1, 350, 10);
		dwTableResized();
        repaint();
    }

    private void scrollToRow(int row) {
    	if ((row > 0) && (row < table.getTable().getRowCount())) {
    		table.getTable().scrollRectToVisible(table.getTable().getCellRect(row, 0, true));
    	}
    }

    /**
     * Get the total of all the money orders.
     */
    private BigDecimal getTotal() {
        BigDecimal total = BigDecimal.ZERO;	
       // java.util.List vendorPayments = transaction.getMoneyOrders();
       vendorPayments = transaction.getMoneyOrderList();
        for (Iterator<MoneyOrderInfo> i = vendorPayments.iterator(); i.hasNext(); ) {
        	MoneyOrderInfo vendorPayment = i.next();
            BigDecimal itemAmount = vendorPayment.getItemAmount();
            total = total.add(itemAmount);
        }
        return total;
    }

    private void removeVendorPayment(int index) {
        transaction.deleteMoneyOrder(index);
        tableModel.removeRow(index);
		dwTableResized();
    }
    /**
     * Set the values of a particular money order. Update the values in the
     *   transaction and refresh the table.
     */
    private void setVendorPayment(String amt, String vendor, int index) {
        BigDecimal amount = new BigDecimal(amt);
        setInTransaction(amount, false, vendor, index);
        refreshTable();
    }

    /**
     * Add money order(s) to the transaction and refresh the table. The number
     *   of money orders to add is based on the value in the quantity field. If
     *   money order amount was 0, return false
     */
    private boolean addVendorPayment(String amt, String vendor) {
        BigDecimal amount = new BigDecimal(amt);
        if (amount.compareTo(BigDecimal.ZERO) == 0)
            return false;
        setInTransaction(amount, false, vendor);
        refreshTable();
        return true;
    }

    /**
     * Add a money order to the money order table. Update the total and change
     *   due fields accordingly.
     */
    private void addToTable(MoneyOrderInfo moneyOrder) {
        previousAmount = moneyOrder.getItemAmount();
        BigInteger vendorNumber = moneyOrder.getVendorNumber();
       // previousVendor = moneyOrder.getPayeeName();
       previousVendor = ((MoneyOrderTransaction40) transaction).getPayeeName(vendorNumber.toString());
       String[] data = {vendorNumber.toString(),
                         moneyOrder.getItemAmount().toString(),
                         previousVendor};
        tableModel.addRow(data);
        // clear out the amount field
        amountField.setValue(BigDecimal.ZERO);
        amountField.requestFocus();
		dwTableResized();
    }

    /**
     * Just notify my listeners that I am done.
     */
    @Override
	public void finish(int direction) {
        PageNotification.notifyExitListeners(this, direction);
		table.setListenerEnabled(false);
    }

    /**
     * Attempt to add money orders. If the money orders were not added, return
     *   false. This indicates that there was no amount entered.
     */
    private boolean doAddAction() {
        if (tableRowSelected > -1) {
            setVendorPayment(amountField.getText(),
                             vendorList.getSelectedItem().toString(),
                             tableRowSelected);
            return true;
        }
        else {
            return addVendorPayment(amountField.getText(),
                                   vendorList.getSelectedItem().toString());
        }
    }

    /**
     * Add a money order(s) to the table if the amount in the field is not 0. If
     *   it is 0, shift focus to the amount tendered field. If it was not 0,
     *   shift focus back to the amount field.
     */
    public void amountFieldAction() {
        if (doAddAction())
            amountField.requestFocus();
        else
            totalButton.doClick();
    }

    /**
     * Repeat the last action. Add the same quantity, amount and fee of the last
     *   money order action entered.
     */
    public void repeatButtonAction() {
        if (tableRowSelected > -1){           
            addVendorPayment(amountField.getText(), vendorList.getSelectedItem().toString());
        }
        else {
            addVendorPayment(previousAmount.toString(), previousVendor);
        }
        amountField.requestFocus();
    }

    /**
     * Delete the money order that is highlighted in the table.
     */
    public void removeButtonAction() {
        if (tableRowSelected > -1) {
            removeVendorPayment(tableRowSelected);
            refreshTable();
            amountField.setValue(BigDecimal.ZERO);
        }
    }

    /**
     * Notify my listeners that the user wishes to go the wizard interface.
     */
    public void wizardButtonAction() {
        if (transaction.getMoneyOrderCount() > 0) {
            int result = Dialogs.showConfirm("dialogs/DialogModeSwitch.xml");	
            if (result != Dialogs.YES_OPTION)
                return;
        }
        finish(PageExitListener.WIZARD);
    }

    /**
     * Notify listeners that the user wishes to cancel the transaction.
     */
    public void cancelButtonAction() {
        transaction.setStatus(ClientTransaction.CANCELED);
        finish(PageExitListener.CANCELED);
    }

    /**
     * Try to print the money orders. If not all the money orders printed, show
     *   a dialog with the details of the print. Exit back to the main menu.
     */
    public synchronized void printButtonAction() {
        // disable buttons to remove multiple-activations
        setButtonsEnabled(false);
        
        // try to add a money order if there is one ready in the amount field
        doAddAction();
        // try the print and handle results
        printMoneyOrders();
        this.requestFocus();
        if (transaction.getStatus() == ClientTransaction.COMPLETED)
            finish(PageExitListener.COMPLETED);
        else {
            Dialogs.showPanel(new PrintCanceledDialogMO(transaction, true));
            cancelButtonAction();
        }
    }

    /**
     * Add a money order with the amount that is currently entered in the amount
     *   field and then shift focus to the tendered field if it is visible, or
     *   the print button if not.
     */
    public void totalButtonAction() {
        if (tableModel.getRowCount() == 0 && amountField.getValue().compareTo(BigDecimal.ZERO) == 0) {
            amountField.requestFocus();
            return;
        }
        doAddAction();
        printButton.setEnabled(true);
        printButton.requestFocus();
    }

    /**
     * If there is not a currently selected money order in the table, select the
     *   last one. If there is one selected, just move the selection up one (or
     *   wrap around if necessary).
     */
    public void upArrowAction() {
        if (tableRowSelected <= 0)
            selectTableRow(tableModel.getRowCount() - 1);
        else
            selectTableRow(tableRowSelected - 1);
        amountField.requestFocus();
    }

    /**
     * If a money order is currently selected in the table, move the selection
     *   down one (or wrap if necessary). If there is none selected, do nothing.
     */
    public void downArrowAction() {
        if (tableRowSelected > -1)
            selectTableRow((tableRowSelected + 1) % tableModel.getRowCount());
    }

    /**
     * Select a row in the money order table. Get the amount value for the money
     *   order selected and place it in the amount field.
     */
    private void selectTableRow(int row) {
        if (row < 0 || row >= tableModel.getRowCount()) {
            tableRowSelected = -1;
            return;
        }
        tableRowSelected = row;
        ListSelectionModel tableSelectionModel =
                                           table.getTable().getSelectionModel();
        tableSelectionModel.setSelectionInterval(row, row);
        amountField.setText((String) table.getTable().getValueAt(row, 1));
        vendorList.setSelectedItem(table.getTable().getValueAt(row, 2));
        scrollToRow(row);
    }
}
