package dw.vendorpayment;

import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNameInterface;
import dw.moneyorder.MoneyOrderTransaction;
import dw.moneyorder.MoneyOrderTransaction40;
import dw.moneyorder.MoneyOrderTransactionInterface;

public class VendorPaymentExpert extends MoneyOrderTransactionInterface {
	private static final long serialVersionUID = 1L;

	public VendorPaymentExpert(MoneyOrderTransaction transaction,
                               PageNameInterface naming) {
        super("EmptyPanel.xml", transaction, naming, false);	
        createPages();
    }

    @Override
	public void createPages() {
        addPage(VPExpertPage1.class, transaction, "vpepage1", "VPX05");	
    }

    // override for default wizard buttons...create an empty set
    @Override
	public PageFlowButtons createButtons() {
        javax.swing.JButton[] buttons = {};
        String[] names = {};
        return new PageFlowButtons(buttons, names, this);
    }
    
    @Override
	public void start() {
        super.start();
        ((MoneyOrderTransaction40) transaction).lockCciLog();
        // addKeyMappings();
        }

    /**
     * Vendor Payment Expert is only one screen, so just call super.exit();
     */
    @Override
	public void exit(int direction) {
        if (direction == PageExitListener.CANCELED){
            transaction.escSAR();
            ((MoneyOrderTransaction40) transaction).unlockCciLog();
            super.exit(direction);
            return;
        }
        if (direction == PageExitListener.COMPLETED){
            transaction.f2NAG();
            ((MoneyOrderTransaction40) transaction).unlockCciLog();
            super.exit(direction);
            return;
        }
        if (direction == PageExitListener.TIMEOUT || direction == PageExitListener.FAILED) {
            ((MoneyOrderTransaction40) transaction).unlockCciLog();
            super.exit(direction);
            return;
        }
        super.exit(direction);
    }
    
    public void f2SAR() {
        transaction.f2SAR();
    }
}
