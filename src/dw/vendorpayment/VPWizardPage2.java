package dw.vendorpayment;

import java.awt.Color;
import java.awt.Component;
import java.awt.SystemColor;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import com.moneygram.agentconnect.MoneyOrderInfo;

import dw.dwgui.DWTable;
import dw.dwgui.DWTable.DWTableCellRenderer;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.dwgui.MoneyField;
import dw.framework.KeyMapManager;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.moneyorder.MoneyOrderFlowPage;
import dw.moneyorder.MoneyOrderTransaction;
import dw.moneyorder.MoneyOrderTransaction40;

/**
 * Page 2 of the Vendor Payment Wizard.
 * This page displays a list of the money orders entered in a table and allows
 *   the user to edit the amounts in the table before continuing.
 */
public class VPWizardPage2 extends MoneyOrderFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

    private JTextField totalField = null;
    private JLabel vendorLabel = null;
    private DWTable table = null;
    private DefaultTableModel tableModel = null;
    private final int ITEM_COLUMN = 0;
    private final int AMOUNT_COLUMN = 1;
    private JScrollPane scrollpane;
    private JPanel main;
    
    private final int SCREEN_WIDTH = 350;

    public VPWizardPage2(MoneyOrderTransaction40 tran, String name, String pageCode, PageFlowButtons buttons) {
        this((MoneyOrderTransaction) tran, name, pageCode, buttons);
    }
   
    public VPWizardPage2(MoneyOrderTransaction tran,
                       String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "vendorpayment/VPWizardPage2.xml", name, pageCode, buttons);	
        totalField = (JTextField) getComponent("totalField");	
        vendorLabel = (JLabel) getComponent("vendorLabel");	
		scrollpane = (JScrollPane) this.getComponent("scrollpane1");	
		main = (JPanel) this.getComponent("main");
        
		DWTextPane tp = (DWTextPane) getComponent("textpane");
		tp.addListener(this, this);
        setupTable();
    }

	@Override
	public void dwTextPaneResized() {
		int width = Math.max(table.getPreferredSize().width, SCREEN_WIDTH);
		DWTextPane tp = (DWTextPane) getComponent("textpane");
		tp.setWidth(width);
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.resizeTable();
			} 
		});
	}

    private void setupTable() {
        // get the column names from the XML params
        String[] colNames = {Messages.getString("VPWizardPage2.itemText") , Messages.getString("VPWizardPage2.amountText")};	
        tableModel = new VendorPaymentTableModel(colNames, 10, AMOUNT_COLUMN,
                                                 transaction);
        
        table = (DWTable) this.getComponent("vendorPaymentTable");
        table.getTable().setModel(tableModel);
        TableColumnModel colModel = table.getTable().getColumnModel();
        colModel.getColumn(AMOUNT_COLUMN).setCellEditor(
                                     new DefaultCellEditor(new MoneyField()));
        colModel.getColumn(ITEM_COLUMN).setCellRenderer(new VendorPaymentCellRenderer(table, DWTable.DEFAULT_COLUMN_MARGIN));
        colModel.getColumn(AMOUNT_COLUMN).setCellRenderer(new VendorPaymentCellRenderer(table, DWTable.DEFAULT_COLUMN_MARGIN));
        
		table.getTable().getTableHeader().setReorderingAllowed(false);
		table.getTable().getTableHeader().setResizingAllowed(false);
    }

    @Override
	public void start(int direction) {
        flowButtons.reset();
        flowButtons.setVisible("expert", false);	
        flowButtons.setEnabled("expert", false);	

        refreshTable();
        KeyMapManager.mapMethod(this, KeyEvent.VK_UP, 0, "upArrowAction");	
        final JButton nextButton = flowButtons.getButton("next");	
        final JButton cancelButton = flowButtons.getButton("cancel");	
        vendorLabel.setText(transaction.getSelectedVendor());
        // handle ESC from the table
        addKeyListener("vendorPaymentTable", new KeyAdapter() {	
           @Override
		public void keyPressed(KeyEvent e) {
              if (e.getKeyCode() == KeyEvent.VK_ESCAPE ||
                               e.getKeyCode() == KeyEvent.VK_TAB ||
                               e.getKeyCode() == KeyEvent.VK_ENTER) {
                    e.consume();
                    table.getTable().editingStopped(new javax.swing.event.ChangeEvent(this));
                    table.getTable().getSelectionModel().clearSelection();
                    if (e.getKeyCode() == KeyEvent.VK_TAB)
                        focusNextComponent("vendorPaymentTable");	
                    else if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
                        cancelButton.doClick();
                    else
                        nextButton.requestFocus();
              }}});

        JComponent[] focusOrder = {table, flowButtons.getButton("back"),	
                                   flowButtons.getButton("next"),	
                                   flowButtons.getButton("cancel")};	
        setFocusOrder(focusOrder);
        
        table.getTable().getSelectionModel().clearSelection();

        addFocusGainedListener("vendorPaymentTable", this, "tableFocus");	
        nextButton.requestFocus();
		
		dwTextPaneResized();
    }

    public void tableFocus() {
        table.getTable().setRowSelectionInterval(0, 0);
        table.getTable().setColumnSelectionInterval(1, 1);
    }

    /**
     * Populate the table with values from the transaction's money orders.
     */
    public void refreshTable() {
        totalField.setText(transaction.getTotal(
                             null, false).toString());
       // java.util.List payments = transaction.getMoneyOrders();
        List<MoneyOrderInfo> payments = ((MoneyOrderTransaction40) transaction).getMoneyOrderList();
        int rowCount = tableModel.getRowCount();
        for (int i = 0; i < rowCount; i++) {
            tableModel.removeRow(0);
        }
        for (int i = 0; i < payments.size(); i++) {
        	MoneyOrderInfo payment = payments.get(i);
          //  String[] row = {"" + (i+1), payment.getAmount().toString()};	
            String[] row = {"" + (i+1), payment.getItemAmount().toString()};  
            tableModel.addRow(row);
        }
        table.initializeTable(scrollpane, main, SCREEN_WIDTH, 10);
    }

    @Override
	public void finish(int direction) {
        table.getTable().editingStopped(new javax.swing.event.ChangeEvent(this));
        PageNotification.notifyExitListeners(this, direction);
    }

    /**
     * Put selection in the last amount field of the table.
     */
    public void upArrowAction() {
        table.getTable().requestFocus();
        table.getTable().setRowSelectionInterval(table.getTable().getRowCount() - 1,
                                      table.getTable().getRowCount() - 1);
        table.getTable().setColumnSelectionInterval(AMOUNT_COLUMN, AMOUNT_COLUMN);
    }

    /**
     * Custom TableModel to allow editing only of specified columns.
     */
    class SemiEditableTableModel extends DefaultTableModel {
		private static final long serialVersionUID = 1L;
		int[] editableCols;

        public SemiEditableTableModel(Object[] colNames, int numRows,
                                      int[] editableCols) {
            super(colNames, numRows);
            this.editableCols = editableCols;
        }

        public SemiEditableTableModel(Object[] colNames, int numRows,
                                      int editableCol) {
            this(colNames, numRows, null);
            int[] editCols = {editableCol};
            editableCols = editCols;
        }

        @Override
		public boolean isCellEditable(int row, int col) {
            for (int i = 0; i < editableCols.length; i++)
                if (editableCols[i] == col)
                    return true;
            return false;
        }
    }

    class VendorPaymentTableModel extends SemiEditableTableModel {
		private static final long serialVersionUID = 1L;
		MoneyOrderTransaction transaction;

        public VendorPaymentTableModel(Object[] colNames, int numRows,
                   int amountCol, MoneyOrderTransaction tran) {
            super(colNames, numRows, amountCol);
            this.transaction = tran;
        }

        /**
         * When a value is set in the model, update the corresponding money
         *   order in the transaction. If a value of 0 is set, ignore the change
         *   and revert back to the previous value.
         */
        @Override
		public void setValueAt(Object value, int row, int col) {
            BigDecimal newAmount = new BigDecimal((String) value);
            if (newAmount.compareTo(BigDecimal.ZERO) == 0)
                return;
            String payee = transaction.getSelectedVendor();
            if (setInTransaction(newAmount, false, payee, row)) {
                super.setValueAt(value, row, col);
                totalField.setText(transaction.getTotal(
                                  null, false).toString());
            }
        }
    }

    /**
     * Custom TableCellRenderer for the money order table. When selecting the
     *   item or fee columns, do not highlight the cell and make the text gray
     *   to indicate that it is not editable. If it is the amount column, make
     *   the cell highlighted to indicate it may be edited.
     */
    class VendorPaymentCellRenderer extends DWTableCellRenderer {
		private static final long serialVersionUID = 1L;

		public VendorPaymentCellRenderer(DWTable table, int minMargin) {
			table.super(minMargin);
		}

		private int margin = 0;

        @Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        	JComponent rend;
            if (column == AMOUNT_COLUMN) {
                rend = new MoneyField(value.toString());
            } else {
            	rend = (JComponent) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            	((JLabel) rend).setHorizontalAlignment(SwingConstants.RIGHT);
            }
            if (isSelected && hasFocus) {
                rend.setBackground(Color.white);
                rend.setForeground(Color.black);
            } else if (isSelected) {
                rend.setBackground(SystemColor.textHighlight);
                rend.setForeground(Color.white);
            } else {
                rend.setBackground(Color.white);
                rend.setForeground(Color.black);
            }
            rend.setBorder(BorderFactory.createEmptyBorder(0, margin, 0, margin));
            return rend;
        }

        @Override
		public int getCellMargin() {
			return margin;
		}
	
		@Override
		public void setCellMargin(int margin) {
			this.margin = margin;
		}
    }
}
