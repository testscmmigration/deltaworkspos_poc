package dw.billpayment;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import com.moneygram.agentconnect.BillerInfo;
import com.moneygram.agentconnect.FeeType;
import com.moneygram.agentconnect.ProductVariantType;

import dw.dwgui.DWTable;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.dwgui.MoneyField;
import dw.dwgui.RowSelectCellRenderer;
import dw.dwgui.UneditableTableModel;
import dw.framework.FieldKey;
import dw.framework.DataCollectionSet;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.mgsend.MoneyGramSendFlowPage;
import dw.mgsend.MoneyGramSendTransaction;
import dw.utility.DWValues;

/**
 * Page 2 of the Directory of Billers/BillPayment wizard. This page displays 
 *   a list of companies that match the search criteria entered. The user must 
 *   choose one of the companies listed to continue.
 */
public class BPWizardPage2 extends MoneyGramSendFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

    private DWTable table;
    private DefaultTableModel tableModel;
    private MoneyField amountField;
    private List<BillerInfo> billers;
    private JPanel selectedBillerPanel;
    private JLabel selectedBiller;
	private DWTextPane screenInstructions2;
	private JPanel panel2;
	private JScrollPane scrollPane;
	private static int MIN_SCREEN_WIDTH=575;
	
    private String[] columnNames = {Messages.getString("BPWizardPage2.1904"), 
    		Messages.getString("BPWizardPage2.1916"),
    		Messages.getString("BPWizardPage2.1905"),
    		Messages.getString("BPWizardPage2.1906")};

    public BPWizardPage2(MoneyGramSendTransaction tran, String name,
                          String pageCode, PageFlowButtons buttons) {
        super(tran, "billpayment/BPWizardPage2.xml", name, pageCode, buttons);	
        amountField = (MoneyField) getComponent("amountField");
        selectedBillerPanel = (JPanel) getComponent("selectedBillerPanel");
        selectedBiller = (JLabel) getComponent("selectedBiller");

    	// set up the Text Pane listener
		screenInstructions2 = (DWTextPane) getComponent("screenInstructions2");
		screenInstructions2.addListener(this, this);
		screenInstructions2.setListenerEnabled(false);
		panel2 = (JPanel) getComponent("panel2");
		scrollPane = (JScrollPane) getComponent("scrollPane1");
        
        table = (DWTable) getComponent("matchingCompanyTable");
    }


	@Override
	public void dwTextPaneResized() {
		
		int tableWidth = table.getTable().getPreferredSize().width + 2;
		int screenWidth = scrollPane.getVisibleRect().width - 20;
		int actualWidth = Math.min(Math.max(tableWidth, MIN_SCREEN_WIDTH), screenWidth);
		
		screenInstructions2.setWidth(actualWidth);

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.resizeTable();
			} 
		});
	}

	private void setupTable() {
        tableModel = new UneditableTableModel(columnNames, 0);
        
        
		tableModel = new DefaultTableModel(columnNames, 0) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		
        table.getTable().setModel(tableModel);
        table.getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        RowSelectCellRenderer.setAsDefaultRenderer(table.getTable());
		table.getTable().getTableHeader().setReorderingAllowed(false);

        for (int c = 0; c < columnNames.length; c++) {
        	TableColumn column = table.getTable().getColumnModel().getColumn(c); 
        	column.setCellRenderer(table.new DWTableCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));
        }

        table.getTable().getSelectionModel().setSelectionInterval(0, 0);
    }

    public void updateFields() {
        int selectedRow = table.getTable().getSelectedRow();
        if ((selectedRow >= 0) && (selectedRow < tableModel.getDataVector().size())) {
        	if (billers.get(selectedRow).isSendFixedAmountFlag()) {
    	        transaction.setAmount(BigDecimal.ZERO);
    	        
                amountField.setEnabled(false);        			
                amountField.setEditable(false);
                amountField.setText("0.00");
     
        	} else {
                amountField.setEnabled(true);        			
                amountField.setEditable(true);
        	}
        }
    }

    @Override
	public void start(int direction) {
        flowButtons.reset();
        flowButtons.setVisible("expert", false);	
        if (direction == PageExitListener.NEXT) {	      
        	if (transaction.isBillerGroupingNeeded()) {
        		selectedBillerPanel.setVisible(true);
        		String groupname = transaction.getSelectedBillerName().get(0).getBillerGroupName();
        		String billername = transaction.getSelectedBillerName().get(0).getBillerName();
        		if (groupname.trim().length() > 0) {
        			selectedBiller.setText(groupname);
        		} else if (billername.trim().length() > 0) {
        			selectedBiller.setText(billername);
        		} else {
        			selectedBillerPanel.setVisible(false);
        		}
       			
        		billers = transaction.getSelectedBillerName();
        	} else {
        		selectedBillerPanel.setVisible(false);
        		billers = transaction.getFullBillerInfoList();
        	}
        		
	        setupTable();
	        int rowCount = tableModel.getRowCount();
	        for (int i = 0; i < rowCount; i++){
	            tableModel.removeRow(0);
	        }
	        String recvCode = "";
	        String tempVal = "";
	        String feeDisplay  = "";
	        FeeType feeType = null;
	        BigDecimal feeAmount = null;
	        String currency = transaction.agentBaseCurrency();
	        for (BillerInfo billerInfo : billers) {
	        	if (billerInfo.getProductVariant().equals(ProductVariantType.PREPAY)) {
	        		tempVal = billerInfo.getServiceOffering();
	        		if ((tempVal == null) || (tempVal.isEmpty())) {
	        			tempVal = billerInfo.getBillerNotes();
	        		}
	        	} else { 
	        		tempVal = billerInfo.getBillerNotes();
	        	}
				
	        	feeType = billerInfo.getFeeType();
	        	feeAmount = billerInfo.getMinimumFeeAmt();
	        	recvCode = billerInfo.getReceiveCode();
	        	
	        	if (feeType == null) {
	        		feeDisplay = "Not Available";
	        	} else if (feeType.equals(FeeType.VARIABLE)) {
	        		feeDisplay = "Fees Vary";
	        	} else if (feeType.equals(FeeType.MINIMUM)) {
	        		feeDisplay = feeAmount + " " + currency + " & up";
	        	} else if (feeType.equals(FeeType.NOT_FOUND)) {
	        		feeDisplay = "Not Available";
	        	} else {
	        		feeDisplay = feeAmount+ " " + currency;
	        	}
	        	
	            String[] row = {billerInfo.getBillerName(),
	            		        recvCode,
								tempVal,
								feeDisplay};
	            tableModel.addRow(row);
	            
	        }
	        // select the top row : 'none'
	        table.getTable().setRowSelectionInterval(0, 0);
	        
	        table.initializeTable(scrollPane, panel2, MIN_SCREEN_WIDTH, 10);
	        table.setColumnStaticWidth(2);
	        table.setColumnStaticWidth(3);
        } 
        
        if (transaction.getAmount() != null) {
			amountField.setText(transaction.getAmount().toString());
		}
		table.getTable().requestFocus();

        // set up the enter key for the table
        final JButton nextButton = flowButtons.getButton("next");	
        final JButton cancelButton = flowButtons.getButton("cancel");	

        addActionListener("amountField", this);	

        KeyAdapter listener = new KeyAdapter() {
            @Override
			public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    e.consume();
                    if (amountField.isEnabled())
                    	amountField.requestFocus(true);
                    else
                    	nextButton.doClick();
                }
                if (e.getKeyCode() == KeyEvent.VK_TAB) {
                    e.consume();
                    focusNextComponent(table.getTable());
                }
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    e.consume();
                    cancelButton.doClick();
                }
            }};
        
        addKeyListener(table.getTable(), listener);
        
        table.getTable().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
			public void valueChanged(ListSelectionEvent e) {
            	updateFields();
            }
        });
        updateFields();
        
        // Clear account number.
        
        DataCollectionSet data = transaction.getDataCollectionData();
        if (data != null) {
        	Map<FieldKey, Object> valueMap = data.getFieldValueMap();
        	if (valueMap != null) {
        		valueMap.put(FieldKey.BILLER_ACCOUNTNUMBER_KEY, "");
        		valueMap.put(FieldKey.VALIDATE_ACCOUNT_NBR_KEY, "");
        	}
        }

        screenInstructions2.setListenerEnabled(true);
        dwTextPaneResized();
    }

	public void amountFieldAction() {
		flowButtons.getButton("next").doClick();	
    }

    @Override
	public void finish(int direction) {
        if (direction == PageExitListener.NEXT) {
            int selectedRow = table.getTable().getSelectedRow();
            ProductVariantType productVariant = billers.get(selectedRow).getProductVariant();
            transaction.setBillerInfo(billers.get(selectedRow));
            transaction.setProductVariant(productVariant);
            BigDecimal enteredAmount = new BigDecimal(amountField.getText());
        /*
			 * If PPC load, don't bother with fee lookup, that will happen later
			 */
			if (productVariant.equals(ProductVariantType.EP)) {
				String cOT = transaction.getBillerInfo().getClassOfTradeCode();
            
				if ((cOT != null) && cOT.equalsIgnoreCase(DWValues.PPC_CLASS_OF_TRADE)) {
					transaction.setAmount(new BigDecimal(amountField.getText()));
					PageNotification.notifyExitListeners(this, direction);
					return;
				}
			}
            
            if ((productVariant.equals(ProductVariantType.EP)) ||
            	(productVariant.equals(ProductVariantType.UBP))){
            	if (enteredAmount.compareTo(BigDecimal.ZERO) <= 0) {
	                amountField.requestFocus();
	                amountField.selectAll();
	                flowButtons.setButtonsEnabled(true);
	                return;
            	}
            }
            if(!setAmount(new BigDecimal(amountField.getText()))){
            	if (transaction.isManagerOverrideFailed()){
                	PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
                	return;
            	} else {
	                amountField.requestFocus();
	                amountField.selectAll();
	                flowButtons.setButtonsEnabled(true);
	                return;
            	}
            }
            transaction.setAmount(new BigDecimal(amountField.getText()));
            
            //PageNotification.notifyExitListeners(this, direction);
            
            transaction.getFeeCalculation(null, this);
           // transaction.getSendFieldsForProduct(this);
        } else {        	
        	PageNotification.notifyExitListeners(this, direction);
        }
        
        screenInstructions2.setListenerEnabled(false);
    }

    @Override
	public void commComplete(int commTag, Object returnValue) {
        boolean b = ((Boolean) returnValue).booleanValue();       	
        if (b) {
        	PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
        } else { 
            start(PageExitListener.NEXT);
        }
    }
}
