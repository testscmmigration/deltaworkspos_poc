/*
 * MGSWizardPage11.java
 * 
 * $Revision$
 *
 * Copyright (c) 2000-2005 MoneyGram International
 */
 
package dw.billpayment;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.DataCollectionField;
import dw.framework.DataCollectionScreenToolkit;
import dw.framework.DataCollectionSet;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.mgsend.MoneyGramSendFlowPage;
import dw.mgsend.MoneyGramSendTransaction;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;
import dw.utility.ExtraDebug;

/**
 * Page 11 of the MoneyGram Send Wizard. This page prompts the user for the
 * recipient information if this is not a selected existing recipient. The test
 * question and answer are disabled if not applicable based on profile.
 */
public class BPWizardPage7b extends MoneyGramSendFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	private JScrollPane scrollPane1;
	private JScrollPane scrollPane2;
	private JPanel panel2;
	private DataCollectionScreenToolkit toolkit = null;
	private DWTextPane text1;
	private JLabel verifyLabel1;
	private JLabel verifyLabel2;
	private Object dataCollectionSetChangeFlag;
	private JButton nextButton;
	
	public BPWizardPage7b(MoneyGramSendTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super(tran, "billpayment/BPWizardPage7b.xml", name, pageCode, buttons); 
		scrollPane1 = (JScrollPane) getComponent("scrollPane1");
		scrollPane2 = (JScrollPane) getComponent("scrollPane2");
		panel2 = (JPanel) getComponent("panel2");
		text1 = (DWTextPane) getComponent("text1");
		text1.setText(Messages.getString("BPWizardPage7b.infoLabel"));
		verifyLabel1 = (JLabel) getComponent("verifyLabel1");
		verifyLabel2 = (JLabel) getComponent("verifyLabel2");
		nextButton = this.flowButtons.getButton("next");
	}

	@Override
	public void dwTextPaneResized() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				int width = scrollPane2.getVisibleRect().width;
				text1.setWidth(width);
			}
		});
	}

	private void setupComponents() {
		
		// Check if the GFFP information has changed.
		
		DataCollectionSet data = this.transaction.getDataCollectionData();

		if (this.dataCollectionSetChangeFlag != this.transaction.getDataCollectionData().getChangeFlag()) {
	    	this.dataCollectionSetChangeFlag = this.transaction.getDataCollectionData().getChangeFlag();
	    	
	    	// Rebuild the data collection items for the screen.
	    	
	    	ExtraDebug.println("Rebuilding Screen " + getPageCode() + " from FTC data");
	    	
			int flags = DataCollectionScreenToolkit.SHORT_LABEL_VALUES_FLAG + DataCollectionScreenToolkit.DISPLAY_REQUIRED_FLAG;
			JButton backButton = this.flowButtons.getButton("back");
			this.toolkit = new DataCollectionScreenToolkit(this, this.scrollPane1, this, this.scrollPane2, this.panel2, flags, transaction.getScreen30Status().getDataCollectionPanels(data), this.transaction, backButton, nextButton, null);
		}
		
		// Check if the screen needs to be laid out.
		
		if (transaction.getScreen30Status().needLayout(data)) {
			toolkit.populateDataCollectionScreen(panel2, transaction.getScreen30Status().getDataCollectionPanels(data), false);
		}
	}

	@Override
	public void start(int direction) {
		setupComponents();
		
		flowButtons.reset();
		flowButtons.setVisible("expert", false); 
		flowButtons.setEnabled("expert", false);        

		if (direction == PageExitListener.NEXT || direction == PageExitListener.BACK) {
			DataCollectionScreenToolkit.restoreDataCollectionFields(this.toolkit.getScreenFieldList(), this.transaction.getDataCollectionData());
		}
		
		verifyLabel1.setVisible(false);
		verifyLabel2.setVisible(false);
		this.toolkit.moveCursor(this.toolkit.getScreenFieldList(), nextButton);

        // Enable and call the resize listener.

    	text1.setListenerEnabled(true);
		toolkit.enableResize();
		toolkit.getComponentResizeListener().componentResized(null);
	}

	@Override
	public void finish(int direction) {
		
    	// Disable the resize listener for this screen.

		toolkit.disableResize();
		
		if (direction == PageExitListener.NEXT) {

			// If the flow direction is to the next screen, check the validity of the
	    	// data. If a value is invalid or a required item does not have a value,
	    	// return to the screen.
	    	
        	String tag = toolkit.validateData(toolkit.getScreenFieldList(), DataCollectionField.DISPLAY_ERRORS_VALIDATION);
        	if (tag != null) {
				flowButtons.setButtonsEnabled(true);
				toolkit.enableResize();
        		return;
        	}
		}
		
    	// If the flow direction is to the next screen or back to the previous
    	// screen, save the value of the current items in a data structure.

        if ((direction == PageExitListener.BACK) || (direction == PageExitListener.NEXT)) {
        	DataCollectionScreenToolkit.saveDataCollectionFields(toolkit.getScreenFieldList(), transaction.getDataCollectionData());
        }
		
		if ((direction == PageExitListener.NEXT) && (transaction.nextDataCollectionScreen(BillPaymentWizard.BPW30_COMPLIANCE).isEmpty()) && transaction.getDataCollectionData().isAdditionalInformationScreenValid()) {
	  		transaction.bpValidation(this, ValidationType.SECONDARY);
		} else {
			PageNotification.notifyExitListeners(this, direction);
		}
	}

	/**
	 * Called by transaction after it receives response from middleware.
	 * 
	 * @param commTag
	 *            is function index.
	 * @param returnValue
	 *            is Boolean set to true if response is good. Otherwise, it is
	 *            set to false.
	 */
	@Override
	public void commComplete(int commTag, Object returnValue) {
		boolean b = ((Boolean) returnValue).booleanValue();
		if (b) {
			PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
		} else {
			start(PageExitListener.NEXT);
		}
	}
}