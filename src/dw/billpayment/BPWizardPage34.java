package dw.billpayment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;

import com.moneygram.agentconnect.BillerInfo;
import com.moneygram.agentconnect.ProductVariantType;

import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.i18n.Messages;
import dw.mgsend.MoneyGramSendFlowPage;
import dw.mgsend.MoneyGramSendTransaction;
import dw.mgsend.MoneyGramClientTransaction;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;

	/**
	 * Page 34 of the PPC Card Reload Wizard. This page displays a summary of the
	 * biller payment information and allows the user to confirm the biller and
	 * fee information before moving on.
	 */
public class BPWizardPage34 extends MoneyGramSendFlowPage {
	private static final long serialVersionUID = 1L;

	private JLabel amountTagLabel;
	private JLabel totalToCollectTagLabel;
	
	public BPWizardPage34(MoneyGramSendTransaction tran, String name,
			String pageCode, PageFlowButtons buttons) {
		super(tran, "billpayment/BPWizardPage2c.xml", name, pageCode, buttons); 

		amountTagLabel = (JLabel) this.getComponent("amountTagLabel");
		totalToCollectTagLabel = (JLabel) this.getComponent("totalToCollectTagLabel");
	}

	@Override
	public void start(int direction) {
		flowButtons.reset();
		flowButtons.setVisible("expert", false);
		
		BillerInfo biller = transaction.getBillerInfo();

    	List<JLabel> tags = new ArrayList<JLabel>();
		
		this.displayLabelItem("name", biller.getBillerName(), tags);
		this.displayLabelItem("receiveCode", biller.getReceiveCode(), tags);

		String paymentType = null;
		String descOffering = null;
		if (transaction.getProductVariant().equals(ProductVariantType.EP)) {
			paymentType = Messages.getString("BPWizardPage34.paymentType1");
			descOffering = biller.getBillerNotes();
		} else if (transaction.getProductVariant().equals(ProductVariantType.UBP)) {
			paymentType = Messages.getString("BPWizardPage34.paymentType2");
			descOffering = biller.getBillerNotes();
		} else if (transaction.getProductVariant().equals(ProductVariantType.PREPAY)) {
			if (biller.getServiceOffering().trim().length() > 0 ) {
				descOffering = biller.getServiceOffering();
			} else { 
				descOffering = biller.getBillerNotes();
			}
			paymentType = Messages.getString("BPWizardPage34.paymentType3");
		} else {
			paymentType = Messages.getString("BPWizardPage34.paymentType4");
		}

		this.displayLabelItem("paymentType", paymentType, false, tags);
		this.displayLabelItem("descOffering", descOffering, false, tags);

		
		amountTagLabel.setText(Messages.getString("BPWizardPage34.7"));
		totalToCollectTagLabel.setText(Messages.getString("BPWizardPage34.9"));

		BigDecimal amount = transaction.getAmount();
		BigDecimal fee = null;
		BigDecimal totalToCollect = null;
		
		boolean hasFeeAmount = transaction.getFeeSendTotalWithTaxBp(true) != null;
		fee = hasFeeAmount ? transaction.getFeeSendTotalWithTaxBp(true) : null;
		boolean hasCollectAmount = transaction.getCurrentFeeInfo().getTotalAmount() != null;
		totalToCollect = hasCollectAmount ? transaction.getCurrentFeeInfo().getTotalAmount() : null;
		String currency = transaction.agentBaseCurrency();
		this.displayMoney("amount", amount, currency, tags);
		this.displayMoney("fee", fee, currency, tags);
		this.displayMoney("totalToCollect", totalToCollect, currency, tags);

		this.setTagLabelWidths(tags, MINIMUM_TAG_WIDTH);

		flowButtons.setSelected("next");
	}

    /**
	 * Clear label contents upon entry into the screen.
	 */
	
	@Override
	public void finish(int direction) {
        if (direction == PageExitListener.NEXT) { 
            transaction.getAccountNumberHistory(this, transaction.getBpAccountNumber());
        } else {        	
        	PageNotification.notifyExitListeners(this, direction);
        }
    }

	
    @Override
	public void commComplete(int commTag, Object returnValue) {
        boolean b = ((Boolean) returnValue).booleanValue();
        
        if (commTag == MoneyGramClientTransaction.CONSUMER_HISTORY_LOOKUP) {
        	/*
        	 * If we received a history, then it is not a new customer.
        	 */
    		transaction.setCustomerInfo(transaction.getHistoricCustomerInfo());
        	if (transaction.getCustomerInfo() == null) {
        		transaction.setNewCustomer(false);
        	}
			if (transaction.hasBillerData() && transaction.isOKToPrintDF()) {
				if (transaction.getBankCardSwiped() && (!transaction.isTempCard())
						&& (transaction.getBillerInfo().isExpeditedEligibleFlag())) {
					transaction.setMgiTransactionSessionId(transaction.getCurrentFeeInfo().getMgiSessionID());
					transaction.bpValidation(this, ValidationType.INITIAL_EXPEDITIED_LOAD);
				} else {
					transaction.bpValidation(this, ValidationType.INITIAL_NON_FORM_FREE);
				}
			}
			flowButtons.setButtonsEnabled(true);
        } else if (commTag == MoneyGramClientTransaction.BP_VALIDATE) {
        	if (b) {
				DataCollectionStatus validationStatus = transaction.getValidationStatus();
				String nextPage = transaction.nextDataCollectionScreen(BillPaymentWizard.BPW34_PPC_VERIFY);
				int businessErrorCount = transaction.getDataCollectionData().getBusinessErrors() != null ? transaction.getDataCollectionData().getBusinessErrors().size() : 0;
				int additionalDataFieldCount = (transaction.getDataCollectionData().getAdditionalDataCollectionFields() != null ? transaction.getDataCollectionData().getAdditionalDataCollectionFields().size() : 0);
				boolean rtc = transaction.getBpValidationResponsePayload() != null ? transaction.getBpValidationResponsePayload().isReadyForCommit() : false;
        		
				if (validationStatus.equals(DataCollectionStatus.ERROR)) {
					PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
					flowButtons.setButtonsEnabled(true);
					flowButtons.setVisible("expert", false);
				} else if ((nextPage.isEmpty()) && (businessErrorCount == 0) && (additionalDataFieldCount == 0) && (! rtc)) {
					transaction.bpValidation(this, ValidationType.SECONDARY);
				} else {
					PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
				} 
        	} else {
				start(PageExitListener.NEXT);
        	}
        }
    }
}
