package dw.billpayment;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import com.moneygram.agentconnect.BillerInfo;
import com.moneygram.agentconnect.FeeType;

import dw.dwgui.DWTable;
import dw.dwgui.DWTable.DWTableListener;
import dw.dwgui.RowSelectCellRenderer;
import dw.dwgui.UneditableTableModel;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.mgsend.MoneyGramSendFlowPage;
import dw.mgsend.MoneyGramSendTransaction;
import dw.mgsend.MoneyGramClientTransaction;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;

	/**
	 * Page 22 of the Directory of Bins/BillPayment wizard. This page displays 
	 *   a list of companies that match the entered BIN. The user must 
	 *   choose one of the companies listed to continue.  This screen is only 
	 *   seen if PPC EP transaction is being processed 
	 */
	public class BPWizardPage32 extends MoneyGramSendFlowPage implements DWTableListener {
		private static final long serialVersionUID = 1L;

	    private DWTable table;
	    private DefaultTableModel tableModel;
	    private List<BillerInfo> billers;

	    public BPWizardPage32(MoneyGramSendTransaction tran, String name,
	                          String pageCode, PageFlowButtons buttons) {
	        super(tran, "billpayment/BPWizardPage32.xml", name, pageCode, buttons);
	        table = (DWTable) getComponent("matchingCompanyTable");
	        table.addListener(this, this);
	        table.setListenerEnabled(false);
	    }

	    private void setupTable() {
	        String[] columnNames = {Messages.getString("BPWizardPage32.BillerName"),
	        		                Messages.getString("BPWizardPage32.Fee")}; 
	        tableModel = new UneditableTableModel(columnNames, 0);
	        table.getTable().setModel(tableModel);
	        table.getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	        RowSelectCellRenderer.setAsDefaultRenderer(table.getTable());
	        
	        TableColumn tc = table.getTable().getColumnModel().getColumn(0);
	        tc.setCellRenderer(table.new DWTableCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));
	        tc = table.getTable().getColumnModel().getColumn(1);
	        tc.setCellRenderer(table.new DWTableCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));
	        table.getTable().getSelectionModel().setSelectionInterval(0, 0);
	    }

	    @Override
		public void start(int direction) {
	        flowButtons.reset();
	        flowButtons.setVisible("expert", false);	
	        
	        if (direction == PageExitListener.NEXT) {	
		        setupTable();
		        int rowCount = tableModel.getRowCount();
		        for (int i = 0; i < rowCount; i++){
		            tableModel.removeRow(0);
		        }
		        String feeDisplay = "";
		        FeeType feeType = null;
		        BigDecimal feeAmount = null;
		        String currency = transaction.agentBaseCurrency();
		        billers = transaction.getFullBillerInfoList();
		        for (int i = 0; i < billers.size(); i++) {
		        	feeType = billers.get(i).getFeeType();
		        	feeAmount = billers.get(i).getMinimumFeeAmt();
					if (FeeType.VARIABLE.equals(feeType)) {
						feeDisplay = "Fees Vary";
					} else if (FeeType.MINIMUM.equals(feeType)) {
						feeDisplay = feeAmount + " " + currency + " & up";
					} else if (FeeType.NOT_FOUND.equals(feeType)) {
						feeDisplay = "Not Available";
					} else {
						feeDisplay = feeAmount + " " + currency;
					}

		            String[] row = {billers.get(i).getBillerName(),
									feeDisplay};
		            tableModel.addRow(row);
		        }
		        // select the top row : 'none'
		        table.getTable().setRowSelectionInterval(0, 0);
	        } 
	        table.getTable().requestFocus();
	        
			JScrollPane sp = (JScrollPane) getComponent("scrollPane1");
			JPanel panel = (JPanel) getComponent("panel");
	        table.initializeTable(sp, panel, 400, 10);
	        table.setColumnStaticWidth(1);

	        // set up the enter key for the table
	        final JButton nextButton = flowButtons.getButton("next");	
	        final JButton cancelButton = flowButtons.getButton("cancel");	
	        
	        KeyAdapter listener = new KeyAdapter() {	
	            @Override
				public void keyPressed(KeyEvent e) {
	                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
	                    e.consume();
                    	nextButton.doClick();
	                }
	                if (e.getKeyCode() == KeyEvent.VK_TAB) {
	                    e.consume();
	                    focusNextComponent(table.getTable());
	                }
	                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
	                    e.consume();
	                    cancelButton.doClick();
	                }
	            }};
	        
	        table.getTable().addKeyListener(listener);
	        
	        table.setListenerEnabled(true);
	    }
	   
		/**
		 * Called by transaction after it receives response from middleware.
		 * 
		 * @param commTag
		 *            is function index.
		 * @param returnValue
		 *            is Boolean set to true if response is good. Otherwise, it is
		 *            set to false.
		 */
		@Override
		public void commComplete(int commTag, Object returnValue) {
			boolean b = ((Boolean) returnValue).booleanValue();
			if (commTag == MoneyGramClientTransaction.FEE_LOOKUP){
				if (b) {
					/*
					 * If it is a possible expedited transaction,
					 * try an early validation to see if it is a temp card.
					 */
		            int selectedRow = table.getTable().getSelectedRow();
					if (transaction.getBankCardSwiped() && billers.get(selectedRow).isExpeditedEligibleFlag()) {
						transaction.setBillerInfo(billers.get(selectedRow));
						transaction.setProductVariant(billers.get(selectedRow).getProductVariant());
			    		transaction.getSender().setCountry("");
			        	transaction.setMgiTransactionSessionId(null);
						transaction.bpValidation(this, ValidationType.INITIAL_NON_FORM_FREE);
						return;
					}
					PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
			}
				else
					start(PageExitListener.NEXT);
			}
			/*
			 * Determine if temporary/permanent card and set expedited eligible 
			 * appropriately.
			 */
		    else if (commTag == MoneyGramClientTransaction.BP_VALIDATE) {
				if (!b) {
					transaction.setTempCard(true);
				}
				PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
		    }
		}

	    @Override
		public void finish(int direction) {
	        if (direction == PageExitListener.NEXT) {
	            int selectedRow = table.getTable().getSelectedRow();
	        	transaction.setBillerInfo(transaction.getSelectedBillerInfo(selectedRow).get(0));
	            transaction.setProductVariant(billers.get(selectedRow).getProductVariant());
				transaction.getFeeCalculation(null, this);
	        } else {
	        	PageNotification.notifyExitListeners(this, direction);
	        }
	        table.setListenerEnabled(false);
	    }

		@Override
		public void dwTableResized() {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					table.resizeTable();
				} 
			});
		}
}
