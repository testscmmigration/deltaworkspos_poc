package dw.billpayment;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.moneygram.agentconnect.BillerInfo;
import com.moneygram.agentconnect.BillerSearchType;
import com.moneygram.agentconnect.ProductVariantType;

import dw.dialogs.Dialogs;
import dw.dwgui.DWTable;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.dwgui.MoneyField;
import dw.framework.DataCollectionSet;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.DataCollectionSet.DataCollectionType;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.mgsend.MGSHistoryPage;
import dw.mgsend.MoneyGramClientTransaction;
import dw.mgsend.MoneyGramSendFlowPage;
import dw.mgsend.MoneyGramSendTransaction;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;
import dw.model.adapters.CompanyInfo;
import dw.model.adapters.CustomerInfo;
import dw.model.adapters.SenderInfo;
import dw.utility.DWValues;

/**
 * Page 3 of the MoneyGram Send Wizard. This page displays a table of customers
 *   that match the home phone number given. The user may choose zero or one
 *   of the customers listed in the table and continue.
 */
public class BPWizardPage6 extends MoneyGramSendFlowPage implements MGSHistoryPage, DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	private static final int SEND_FROM = 0;
	private static final int SEND_ADDRESS = 1;
	private static final int BILLER = 2;
	private static final int PRODUCT = 3;
	
	private static final int MINIMUM_SCREEN_WIDTH = 450;

	private DWTable table = null;
	private DefaultTableModel tableModel = null;
	private List<CustomerInfo> customers = null;
	private MoneyField amountField;
	private JLabel currencyLabel;
	private ProductVariantType productVariant = null;
	private ListSelectionListener listListener;
	private DWTextPane screenInstructions1;
	private JScrollPane scrollpane;
	private JPanel panel1;
	private int prevCustIdx;
	
	public BPWizardPage6(MoneyGramSendTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super(tran, "billpayment/BPWizardPage6.xml", name, pageCode, buttons); 
		init();
	}
	
	private void init() {
		amountField = (MoneyField) getComponent("amountField");
		currencyLabel = (JLabel) getComponent("currencyLabel");
		screenInstructions1 = (DWTextPane) getComponent("screenInstructions1");
		screenInstructions1 = (DWTextPane) getComponent("screenInstructions1");
		screenInstructions1.addListener(this, this);
		screenInstructions1.setListenerEnabled(false);
		
		scrollpane = (JScrollPane) getComponent("scrollPane1");
		panel1 = (JPanel) getComponent("panel1");
	}

	@Override
	public void dwTextPaneResized() {
		int width1 = scrollpane.getVisibleRect().width;
		int width2 = table.getPreferredSize().width;
		int width = Math.max(Math.min(width1, width2), MINIMUM_SCREEN_WIDTH);

		screenInstructions1.setWidth(width);
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.resizeTable();
			} 
		});
	}

	@Override
	public void start(int direction) {	
		if (direction == PageExitListener.NEXT) {
			transaction.setDataCollectionData(new DataCollectionSet(DataCollectionType.VALIDATION));
			prevCustIdx = -1;
		} else {
			prevCustIdx = transaction.getSelectedCustomerIndex();
		}

		flowButtons.reset();
		flowButtons.setVisible("expert", false); 
		JLabel phoneLabel = (JLabel) getComponent("phoneLabel"); 

    	// set up the Text Pane listener

		if (direction == PageExitListener.NEXT) {
			setupTable();
			customers = transaction.getUniqueCustomers();

			phoneLabel.setText(transaction.getCardPhoneNumber());
			boolean bpAccountNumber = false;
			if((transaction.getBpAccountNumber() == null || transaction.getBpAccountNumber().isEmpty()))
			{
				bpAccountNumber = true;
			}
			populatePeopleTable(customers, bpAccountNumber);
			amountField.setText(transaction.getAmount().toString());
			currencyLabel.setText(transaction.agentBaseCurrency());
		}

		table.getTable().requestFocus();
		table.getTable().getSelectionModel().addListSelectionListener(
				listListener = new ListSelectionListener() {
					@Override
					public void valueChanged(ListSelectionEvent e) {
						updateFields();
					}
				});
		
		// set up the enter key for the table
		final JButton nextButton = flowButtons.getButton("next"); 
		final JButton cancelButton = flowButtons.getButton("cancel"); 
//		final JButton backButton = flowButtons.getButton("back"); 

		KeyAdapter listener = new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent e) {
					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						e.consume();
						if (amountField.isEnabled())
							amountField.requestFocus();
						else
							nextButton.doClick();
					}
					if (e.getKeyCode() == KeyEvent.VK_TAB) {
						e.consume();
						focusNextComponent(table.getTable());
					}
					if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
						e.consume();
						cancelButton.doClick();
					}
				}
			};
		table.getTable().addKeyListener(listener);

		table.getTable().setRowSelectionInterval(transaction.getSelectedCustomerIndex(),
				transaction.getSelectedCustomerIndex());
		addActionListener("amountField", this); 
		screenInstructions1.setListenerEnabled(true);
        dwTextPaneResized();
	}

	public void amountFieldAction() {
		flowButtons.getButton("next").doClick(); 
	}

	private void setupTable() {
		String[] headers = { Messages.getString("BPWizardPage6.1998"),
				Messages.getString("BPWizardPage6.1999"),
				Messages.getString("BPWizardPage6.2000"),
				Messages.getString("BPWizardPage6.2001") };     

		tableModel = new DefaultTableModel(headers, 0) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		table = (DWTable) getComponent("matchingCustomerTable");
		table.getTable().setModel(tableModel);
		TableColumnModel model = table.getTable().getColumnModel();
		TableColumn col = null;
		table.getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		col = model.getColumn(SEND_FROM);
		col.setCellRenderer(table.new DWTableCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));

		col = model.getColumn(SEND_ADDRESS);
		col.setCellRenderer(table.new DWTableCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));

		col = model.getColumn(BILLER);
		col.setCellRenderer(table.new DWTableCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));

		col = model.getColumn(PRODUCT);
		col.setCellRenderer(table.new DWTableCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));

	}

	public void populatePeopleTable(List<CustomerInfo> people, 
			boolean addOther) {
		clearComponent(table);
		transaction.populateBillPaymentPeopleTable(this, people, addOther);
		// not sure why expert page 1 doesn't do this also
		transaction.setSelectedCustomerIndex(0);
		table.initializeTable(scrollpane, panel1, MINIMUM_SCREEN_WIDTH, 10);
		table.setColumnStaticWidth(2);
		table.setColumnStaticWidth(3);
	}

	public void updateFields() {
		int selectedRow = table.getTable().getSelectedRow();
		if (selectedRow < 1) {
			amountField.setEnabled(false);
			amountField.setEditable(false);
		} else {
			ProductVariantType productType=null;
			boolean billerNameOther=false;
			if (customers.get(selectedRow - 1).getCompanyInfo() != null) {
				productType = customers.get(selectedRow - 1).getCompanyInfo().getProductVariant();
				if (customers.get(selectedRow - 1).getCompanyInfo().getAgencyName() != null) {
					billerNameOther = customers.get(selectedRow - 1).getCompanyInfo().getAgencyName().isEmpty();
				}
			}
			if (productType == null) {
				transaction.setAmount(BigDecimal.ZERO);
				amountField.setEnabled(false);
				amountField.setEditable(false);

			} else {
				if (productType.equals(ProductVariantType.EP)) {
					if (billerNameOther) {
						transaction.setAmount(BigDecimal.ZERO);
						amountField.setText(transaction.getAmount().toString());
					}
					amountField.setEnabled(!billerNameOther);
					amountField.setEditable(!billerNameOther);

				} else if (productType.equals(ProductVariantType.PREPAY)) {
					transaction.setAmount(BigDecimal.ZERO);
					amountField.setText(transaction.getAmount().toString());
					amountField.setEnabled(false);
					amountField.setEditable(false);

				} else if (productType.equals(ProductVariantType.UBP)) {
					if (billerNameOther) {
						transaction.setAmount(BigDecimal.ZERO);
						amountField.setText(transaction.getAmount().toString());
					}
					amountField.setEnabled(!billerNameOther);
					amountField.setEditable(!billerNameOther);
				} else {
					amountField.setEnabled(true);
					amountField.setEditable(true);
				}
			}
		}
	}

	@Override
	public void finish(int direction) {
		productVariant = null;
		int selectedRow = table.getTable().getSelectedRow();
		/*
		 * if new selection, then new dataCollectionSet
		 */
		if (prevCustIdx != selectedRow) {
			transaction.setDataCollectionData(new DataCollectionSet(DataCollectionType.VALIDATION));
		}
		if (selectedRow > 0) {
			transaction.setSelectedCustomerIndex(selectedRow);
			BigDecimal amount;
			screenInstructions1.setListenerEnabled(false);
			try {
				amount = new BigDecimal(amountField.getText());
			} catch (Exception e) {
				amount = BigDecimal.ZERO;
			}
			if (direction == PageExitListener.NEXT) {
				transaction.setCustomerInfo(customers.get(selectedRow - 1));
				// if (selectedRow == (table.getRowCount() - 1)) {
				ProductVariantType productType = null;
				boolean billerNameOther = false;
				if (selectedRow != 0) {
					CustomerInfo customer = customers.get(selectedRow - 1);
					transaction.setDataFromHistory(true);
					if (customer.getCompanyInfo() != null) {
						productType = customer.getCompanyInfo().getProductVariant();
						if (customer.getCompanyInfo().getAgencyName() != null) {
							billerNameOther = customer.getCompanyInfo().getAgencyName().isEmpty();
						}
					}
					if (ProductVariantType.EP.equals(productType) || ProductVariantType.UBP.equals(productType)) {
						if (!billerNameOther) {
			            	if (amount.compareTo(BigDecimal.ZERO) <= 0) {
								amountField.requestFocus();
								amountField.selectAll();
								flowButtons.setButtonsEnabled(true);
								return;
							}
						}
					}
				}

				CustomerInfo customer = customers.get(selectedRow - 1);
				SenderInfo sender = customer.getSender();
				transaction.setSender(sender);
				transaction.setRewardsFirstName(sender.getFirstName());
				transaction.setRewardsLastName(sender.getLastName());

				// Special set function for EP, so Receiver Name is not clobbered,
				// so if one was entered for this transaction it is retained even
				// after navigating back and forth.
				productVariant = customer.getCompanyInfo().getProductVariant();
				if ((productVariant != null) && (productVariant.equals(ProductVariantType.EP))) {
					transaction.setReceiverEP(customer.getReceiver());
				} else {
					transaction.setReceiver(customer.getReceiver());
				}
				transaction.setRegistrationNumber(customer.getReceiver().getCustomerReceiveNumber());// TO-CHECK
				transaction.setProductVariant(customer.getCompanyInfo().getProductVariant());
				transaction.setAgencyId(customer.getCompanyInfo().getReceiveCode());
				transaction.setReceiveAgentID(customer.getReceiver().getReceiveAgentID());
				transaction.setNewCustomer(false);
				transaction.setDirection1(customer.getReceiver().getDirection1());
				transaction.setDirection2(customer.getReceiver().getDirection2());
				transaction.setDirection3(customer.getReceiver().getDirection3());
				if (!transaction.isDirectoryOfBillers()) {
					transaction.setReceivingCompany(
							transaction.getRecentCompanies()[table.getTable().getSelectedRow() - 1]);
				} else {
					// If the transaction is a fee query, the destination is already set.
					if (!transaction.isFeeQuery()) {
						transaction.setDestination(customer.getReceiver().getDestCountry());
					}
				}
				if (!(setAmount(amountField.getValue()))) {
					if (transaction.isManagerOverrideFailed()) {
						PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
						return;
					} else {
						amountField.requestFocus();
						amountField.selectAll();
						flowButtons.setButtonsEnabled(true);
						return;
					}
				}
				transaction.setAmount(amount);
				// EnumeratedValue ev = (EnumeratedValue) thirdPartyTypeBox.getSelectedItem();
				// transaction.setThirdPartyType(ThirdPartyType.valueOf(ev.getValue()));

				if ((transaction.getReceivingCompany().getAgencyName() == null)
						|| (!transaction.getReceivingCompany().getAgencyName().isEmpty())) {
					productVariant = customer.getCompanyInfo().getProductVariant();
				} else {
					transaction.setProductVariant(null);
				}

				if (productVariant != null) {
					String receiveAgentId = customers.get(selectedRow - 1).getCompanyInfo().getReceiveAgentID();
					if (ProductVariantType.EP.equals(productVariant)) {
						transaction.searchForBillers(BillerSearchType.CODE, transaction.getProductVariant(), null,
								transaction.getAgencyId(), transaction.getReceivingCompany().getAgencyName(),
								receiveAgentId, null, this);
					} else if (ProductVariantType.UBP.equals(productVariant)
							|| ProductVariantType.PREPAY.equals(productVariant)) {
						transaction.searchForBillers(BillerSearchType.ID, transaction.getProductVariant(), null, null,
								transaction.getReceivingCompany().getAgencyName(), receiveAgentId,
								null, this);
					} else {
						PageNotification.notifyExitListeners(this, direction);
					}
				} else {
					table.getTable().getSelectionModel().removeListSelectionListener(listListener);
					PageNotification.notifyExitListeners(this, direction);
				}
			} else if (direction == PageExitListener.BACK || direction == PageExitListener.CANCELED) {
				table.setListenerEnabled(false);
				PageNotification.notifyExitListeners(this, direction);
			}
		} else {
			transaction.clearSender();
			transaction.clearReceiver();
			transaction.clearBiller();
			transaction.setCustomerInfo(null);
			transaction.setNewCustomer(true);
			transaction.setProductVariant(null);
			transaction.setReceiveAgentID("");
			if (!transaction.isDirectoryOfBillers()) {
				transaction.setReceivingCompany(new CompanyInfo());
			}
			transaction.setSelectedCustomerIndex(0);
			PageNotification.notifyExitListeners(this, direction);			
		}
	}

	@Override
	public JTable getTable() {
		return table.getTable();
	}

	@Override
	public DefaultTableModel getTableModel() {
		return tableModel;
	}

	/**
	 * Callback from transaction after the comm is finished. If the comm was
	 *   successful, go to the next page. Otherwise, go back to the card number
	 *   field.
	 */
	@Override
	public void commComplete(int commTag, Object returnValue) {
		boolean b = ((Boolean) returnValue).booleanValue();

		if (b) {
			if ((commTag == MoneyGramClientTransaction.BILLER_SEARCH) && (! productVariant.equals(ProductVariantType.PREPAY))) {
				transaction.setBillerInfo(transaction.getMatchingBillers()[0]);
				/*
				 * If PPC load, don't bother with fee lookup, that will happen
				 * later
				 */
				BillerInfo bi = transaction.getBillerInfo();
				productVariant = bi.getProductVariant();

				if (productVariant.equals(ProductVariantType.EP)) {
					String cOT = bi.getClassOfTradeCode();
					if ((cOT != null)
							&& cOT.equalsIgnoreCase(DWValues.PPC_CLASS_OF_TRADE)) {
						PageNotification.notifyExitListeners(this,
								PageExitListener.NEXT);
						return;
					}
				}
				transaction.getFeeCalculation(null, this);
			
			} else if (commTag == MoneyGramClientTransaction.FEE_LOOKUP) {
	        	String mgiSessionID = transaction.getReturnedFeeInfo().getFeeInfo().getMgiSessionID();
	        	transaction.setMgiTransactionSessionId(mgiSessionID);
				if (transaction.hasBillerData() && transaction.isOKToPrintDF()) {
					transaction.bpValidation(this, ValidationType.INITIAL_NON_FORM_FREE);
		        } else {		        	
					flowButtons.setButtonsEnabled(true);
		        }
			} else if (commTag == MoneyGramClientTransaction.BP_VALIDATE) {
				DataCollectionStatus validationStatus = transaction.getValidationStatus();
				String nextPage = transaction.nextDataCollectionScreen(BillPaymentWizard.BPW16_SENDER_SELECTION);
				boolean hasCollectionData = transaction.getDataCollectionData() != null
						&& !validationStatus.equals(DataCollectionStatus.ERROR);
				int businessErrorCount = hasCollectionData
						&& transaction.getDataCollectionData().getBusinessErrors() != null
								? transaction.getDataCollectionData().getBusinessErrors().size() : 0;
				int additionalDataFieldCount = hasCollectionData
						&& transaction.getDataCollectionData().getAdditionalDataCollectionFields() != null
								? transaction.getDataCollectionData().getAdditionalDataCollectionFields().size() : 0;

				if (hasCollectionData && (! validationStatus.equals(DataCollectionStatus.VALIDATED))
						&& (nextPage.isEmpty()) && (businessErrorCount == 0) && (additionalDataFieldCount == 0)) {
					transaction.bpValidation(this, ValidationType.SECONDARY);
				} else {					
					PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
				}
				
			} else {
				PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
			}
		} else {
			if (commTag == MoneyGramClientTransaction.BILLER_SEARCH) {
				if (! transaction.hasBillerData()) {
					Dialogs.showWarning(
						"dialogs/DialogNoBillerDataFound.xml",
						Messages.getString("DialogNoBillerDataFound.keyword1"),
						Messages.getString("DialogNoBillerDataFound.keyword2"));
				}
			}
			start(PageExitListener.NEXT);
		}
	}
}
