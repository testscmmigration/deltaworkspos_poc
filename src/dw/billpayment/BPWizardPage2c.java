package dw.billpayment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;

import com.moneygram.agentconnect.BillerInfo;
import com.moneygram.agentconnect.ProductVariantType;

import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.mgsend.MoneyGramSendFlowPage;
import dw.mgsend.MoneyGramSendTransaction;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;

/**
 * Page 2c of the Bill Payment Wizard. This page displays a summary of the
 * biller payment information and allows the user to confirm the biller and
 * fee information before moving on.
 */
public class BPWizardPage2c extends MoneyGramSendFlowPage {
	private static final long serialVersionUID = 1L;
	
	private JLabel amountTagLabel;
	private JLabel totalToCollectTagLabel;

	public BPWizardPage2c(MoneyGramSendTransaction tran, String name,
			String pageCode, PageFlowButtons buttons) {
		super(tran, "billpayment/BPWizardPage2c.xml", name, pageCode, buttons);
		
		amountTagLabel = (JLabel) this.getComponent("amountTagLabel");
		totalToCollectTagLabel = (JLabel) this.getComponent("totalToCollectTagLabel");
	}

	@Override
	public void start(int direction) {
		flowButtons.reset();
		flowButtons.setVisible("expert", false);
		
    	List<JLabel> tags = new ArrayList<JLabel>();
		
		BillerInfo biller = transaction.getBillerInfo();

		this.displayLabelItem("name", biller.getBillerName(), tags);
		this.displayLabelItem("receiveCode", biller.getReceiveCode(), tags);

		String paymentType = null;
		String descOffering = null;
		if (transaction.getProductVariant().equals(ProductVariantType.EP)) {
			paymentType = Messages.getString("BPWizardPage2c.paymentType1");
			descOffering = biller.getBillerNotes();
		} else if (transaction.getProductVariant().equals(ProductVariantType.UBP)) {
			paymentType = Messages.getString("BPWizardPage2c.paymentType2");
			descOffering = biller.getBillerNotes();
		} else if (transaction.getProductVariant().equals(ProductVariantType.PREPAY)) {
			if (biller.getServiceOffering().trim().length() > 0 ) {
				descOffering = biller.getServiceOffering();
			} else { 
				descOffering = biller.getBillerNotes();
			}
			paymentType = Messages.getString("BPWizardPage2c.paymentType3");
		} else {
			paymentType = Messages.getString("BPWizardPage2c.paymentType4");
		}
		
		this.displayLabelItem("paymentType", paymentType, false, tags);
		this.displayLabelItem("descOffering", descOffering, false, tags);

		BigDecimal amount = null;
		BigDecimal fee = null;
		BigDecimal totalToCollect = null;
		if (transaction.getAmount() != null) {
			amountTagLabel.setText(Messages.getString("BPWizardPage2c.7"));
			amount = transaction.getAmount();
		}
		if (transaction.getReturnedFeeInfo() != null && transaction.getReturnedFeeInfo().getFeeSendTotalWithTax(true,true)!=null) {
			fee = transaction.getReturnedFeeInfo().getFeeSendTotalWithTax(true,true);
		}
		if (transaction.getReturnedFeeInfo() != null && transaction.getReturnedFeeInfo().getFeeInfo() != null
				&& transaction.getReturnedFeeInfo().getFeeInfo().getSendAmounts() != null
				&& transaction.getReturnedFeeInfo().getFeeInfo().getSendAmounts()
						.getTotalAmountToCollect() != null) {
			totalToCollectTagLabel.setText(Messages.getString("BPWizardPage2c.9"));
			totalToCollect = transaction.getReturnedFeeInfo().getFeeInfo().getSendAmounts().getTotalAmountToCollect();
		}
		String currency = transaction.agentBaseCurrency();
		this.displayMoney("amount", amount, currency, tags);
		this.displayMoney("fee", fee, currency, tags);
		this.displayMoney("totalToCollect", totalToCollect, currency, tags);

		this.setTagLabelWidths(tags, MINIMUM_TAG_WIDTH);

		flowButtons.setSelected("next");
	}

	@Override
	public void finish(int direction) {
        if (direction == PageExitListener.NEXT) {
	        if (transaction.isOKToPrintDF()) {
	        	transaction.bpValidation(this, ValidationType.INITIAL_NON_FORM_FREE);
	        } else {
				flowButtons.setButtonsEnabled(true);
	        }
        } else {        	
        	PageNotification.notifyExitListeners(this, direction);
        }
    }
	
    @Override
	public void commComplete(int commTag, Object returnValue) {
        boolean b = ((Boolean) returnValue).booleanValue();       	
        if (b)
        	PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
        else 
            flowButtons.setButtonsEnabled(true);
    }
}
