/*
 * MGSWizardPage39.java
 * 
 * $Revision$
 *
 * Copyright (c) 2000-2010 MoneyGram International
 */

package dw.billpayment;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import dw.framework.DataCollectionField;
import dw.framework.DataCollectionScreenToolkit;
import dw.framework.DataCollectionSet;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.mgsend.MoneyGramSendFlowPage;
import dw.mgsend.MoneyGramSendTransaction;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;
import dw.utility.ExtraDebug;

/**
 * Page 11 of the MoneyGram Send Wizard. This page prompts the user for the
 * recipient information if this is not a selected existing recipient. The test
 * question and answer are disabled if not applicaple based on profile.
 */
public class BPWizardPage39 extends MoneyGramSendFlowPage {
	private static final long serialVersionUID = 1L;

	private JScrollPane scrollPane1;
	private JScrollPane scrollPane2;
	private JPanel panel2;
	private DataCollectionScreenToolkit toolkit = null;
	private Object dataCollectionSetChangeFlag;
	private JButton nextButton;
	
	public BPWizardPage39(MoneyGramSendTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
		super(tran, "billpayment/BPWizardPage39.xml", name, pageCode, buttons); 
		scrollPane1 = (JScrollPane) getComponent("scrollPane1");
		scrollPane2 = (JScrollPane) getComponent("scrollPane2");
		panel2 = (JPanel) getComponent("panel2"); 
		nextButton = this.flowButtons.getButton("next");
	}

	private void setupComponents() {
		
		// Check if the GFFP information has changed.
		
		DataCollectionSet data = this.transaction.getDataCollectionData();

		if (this.dataCollectionSetChangeFlag != this.transaction.getDataCollectionData().getChangeFlag()) {
	    	this.dataCollectionSetChangeFlag = this.transaction.getDataCollectionData().getChangeFlag();
	    	
	    	// Rebuild the data collection items for the screen.
	    	
	    	ExtraDebug.println("Rebuilding Screen " + getPageCode() + " from FTC data");
	    	
	    	
			int flags = DataCollectionScreenToolkit.SHORT_LABEL_VALUES_FLAG + DataCollectionScreenToolkit.DISPLAY_REQUIRED_FLAG;
			JButton backButton = this.flowButtons.getButton("back");
			this.toolkit = new DataCollectionScreenToolkit(this, this.scrollPane1, this, this.scrollPane2, this.panel2, flags, transaction.getScreen39Status().getDataCollectionPanels(data), this.transaction, backButton, nextButton, null);
		}
		
		// Check if the screen needs to be laid out.
		
		if (transaction.getScreen39Status().needLayout(data)) {
			toolkit.populateDataCollectionScreen(panel2, transaction.getScreen39Status().getDataCollectionPanels(data), false);
		}
	}

	@Override
	public void start(int direction) {
		setupComponents();
		
		flowButtons.reset();
		flowButtons.setVisible("expert", false); 
		flowButtons.setEnabled("expert", false);        
		
		if ((direction == PageExitListener.NEXT) || (direction == PageExitListener.BACK)) {
			DataCollectionScreenToolkit.restoreDataCollectionFields(this.toolkit.getScreenFieldList(), this.transaction.getDataCollectionData());
		}

		// Move the cursor to 1) the first required item with no value or item 
		// with an invalid value or 2) the first optional item with no value.
		
		this.toolkit.moveCursor(this.toolkit.getScreenFieldList(), nextButton);

        // Enable and call the resize listener.

		toolkit.enableResize();
		toolkit.getComponentResizeListener().componentResized(null);
	}

	@Override
	public void finish(int direction) {

    	// Disable the resize listener for this screen.

		toolkit.disableResize();

    	// If the flow direction is to the next screen, check the validity of the
    	// data. If a value is invalid or a required item does not have a value,
    	// return to the screen.
    	
    	if (direction == PageExitListener.NEXT) {
        	String tag = toolkit.validateData(toolkit.getScreenFieldList(), DataCollectionField.DISPLAY_ERRORS_VALIDATION);
        	if (tag != null) {
				flowButtons.setButtonsEnabled(true);
				toolkit.enableResize();
        		return;
        	}
        }
    	
        if ((direction == PageExitListener.BACK) || (direction == PageExitListener.NEXT)) {
        	DataCollectionScreenToolkit.saveDataCollectionFields(toolkit.getScreenFieldList(), transaction.getDataCollectionData());
        }
    	
    	// If the flow direction is to the next screen or back to the previous
    	// screen, save the value of the current items in a data structure.

    	if ((direction == PageExitListener.NEXT) && (transaction.nextDataCollectionScreen(BillPaymentWizard.BPW39_PPC_COMPLIANCE).isEmpty()) && transaction.getDataCollectionData().isAdditionalInformationScreenValid()) {
        	DataCollectionScreenToolkit.saveDataCollectionFields(toolkit.getScreenFieldList(), transaction.getDataCollectionData());
	  		transaction.bpValidation(this, ValidationType.SECONDARY);
		} else {
			PageNotification.notifyExitListeners(this, direction);
		}
	}

	/**
	 * Called by transaction after it receives response from middleware.
	 * 
	 * @param commTag
	 *            is function index.
	 * @param returnValue
	 *            is Boolean set to true if response is good. Otherwise, it is
	 *            set to false.
	 */
	@Override
	public void commComplete(int commTag, Object returnValue) {
		boolean b = ((Boolean) returnValue).booleanValue();
		if (b) {
			PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
		} else {
			start(PageExitListener.NEXT);
		}
	}
}