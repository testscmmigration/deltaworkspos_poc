package dw.billpayment;

import java.awt.event.KeyEvent;
import java.util.Arrays;
import java.util.List;

import com.moneygram.agentconnect.ProductVariantType;

import dw.framework.AdditionalInformationWizardPage;
import dw.framework.ClientTransaction;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.KeyMapManager;
import dw.framework.PageExitListener;
import dw.framework.PageNameInterface;
import dw.framework.SupplementalInformationWizardPage;
import dw.mgsend.MoneyGramSendTransaction;
import dw.mgsend.MoneyGramSendTransactionInterface;
import dw.utility.DWValues;

public class BillPaymentWizard extends MoneyGramSendTransactionInterface {
	private static final long serialVersionUID = 1L;

	private static final String BPW06_CARD_NUMBER_ENTRY = "cardNumberEntry";
    private static final String BPW31_PPC_ENTRY = "ppcEntry";
    private static final String BPW32_PPC_MATCHING_BINS = "ppcMatchingBins1";
    public static final String BPW34_PPC_VERIFY = "ppcVerify";
    public static final String BPW36_PPC_SENDER_DETAIL = "ppcSenderDetail";
    public static final String BPW38_PPC_THIRD_PARTY = "ppcThirdParty";
    public static final String BPW39_PPC_COMPLIANCE = "ppcComplianceInfo";
    private static final String BPW05_DB_COMPANY_SEARCH = "dbCompanySearch";
    private static final String BPW07_DB_MATCHING_COMPANIES_1 = "dbMatchingCompanies1";
    private static final String BPW10_DB_MATCHING_COMPANIES_2 = "dbMatchingCompanies2";
    public static final String BPW14_DB_MATCHING_COMPANIES_3 = "dbMatchingCompanies3";
    public static final String BPW16_SENDER_SELECTION = "senderSelection";
    public static final String BPW25_SENDER_DETAIL = "bpSenderDetail";
    public static final String BPW30_COMPLIANCE = "bpComplianceInfo";
    public static final String BPW40_SUPPLEMENTAL_BP_INFORMATION = "bpSupplementalInformation";
    public static final String BPW50_ADDITIONAL_BP_INFORMATION = "bpAdditionalBpInformation";
    public static final String BPW70_SEND_VERIFICATION = "bpSendVerification";
    public static final String BPW75_RECEIPT_PRINTING = "bpReceiptPrinting";
    public static final String BPW80_ERROR_PAGE = "bpErrorPage";
    
    List<String> BASE_PAGE_LIST = Arrays.asList(BPW06_CARD_NUMBER_ENTRY, BPW05_DB_COMPANY_SEARCH, BPW07_DB_MATCHING_COMPANIES_1, BPW10_DB_MATCHING_COMPANIES_2, BPW14_DB_MATCHING_COMPANIES_3, BPW14_DB_MATCHING_COMPANIES_3, BPW16_SENDER_SELECTION); 
	
	public BillPaymentWizard(MoneyGramSendTransaction transaction, PageNameInterface naming, int entryType) {
    	super("billpayment/BPWizardPanel.xml", transaction, naming, false);	
    	/*
    	 * Record how where/how this Bill Payment transaction was entered since
    	 * that can affect screen flow.
    	 */
    	transaction.setTranEntryType(entryType);
    	createPages();
	}
  
    public BillPaymentWizard(MoneyGramSendTransaction transaction, PageNameInterface naming) {
    	super("billpayment/BPWizardPanel.xml", transaction, naming, false);	
    	/*
    	 * Assume default/traditional entry point/screen flow
    	 */
    	transaction.setTranEntryType(MoneyGramSendTransaction.TRAN_ENTRY_UBP);
    	createPages();
    }

	@Override
	public void createPages() {
		/*
		 * If this transaction was not entered via the Prepaid Card button, 
		 * with the Rewards Card search data screen
		 */
		addPage(BPWizardPage5.class, transaction, BPW06_CARD_NUMBER_ENTRY, "BPW06", buttons);	 
        addPage(BPWizardPage31.class, transaction, BPW31_PPC_ENTRY, "BPW31", buttons);	 
        addPage(BPWizardPage32.class, transaction, BPW32_PPC_MATCHING_BINS, "BPW32", buttons);	 
        addPage(BPWizardPage34.class, transaction, BPW34_PPC_VERIFY, "BPW34", buttons);	 
        addPage(BPWizardPage36.class, transaction, BPW36_PPC_SENDER_DETAIL, "BPW36", buttons);	 
        addPage(BPWizardPage38.class, transaction, BPW38_PPC_THIRD_PARTY, "BPW38", buttons);	 
        addPage(BPWizardPage39.class, transaction, BPW39_PPC_COMPLIANCE, "BPW39", buttons);
        addPage(BPWizardPage1.class, transaction, BPW05_DB_COMPANY_SEARCH, "BPW05", buttons);	 
        addPage(BPWizardPage2a.class, transaction, BPW07_DB_MATCHING_COMPANIES_1, "BPW07", buttons);
        addPage(BPWizardPage2.class, transaction, BPW10_DB_MATCHING_COMPANIES_2, "BPW10", buttons);	 
        addPage(BPWizardPage2c.class, transaction, BPW14_DB_MATCHING_COMPANIES_3, "BPW14", buttons);	 
        addPage(BPWizardPage6.class, transaction, BPW16_SENDER_SELECTION, "BPW16", buttons);	 
        addPage(BPWizardPage7.class, transaction, BPW25_SENDER_DETAIL, "BPW25", buttons);	 
        addPage(BPWizardPage7b.class, transaction, BPW30_COMPLIANCE, "BPW30", buttons);
        addPage(SupplementalInformationWizardPage.class, transaction, BPW40_SUPPLEMENTAL_BP_INFORMATION, "BPW40", buttons);
        addPage(AdditionalInformationWizardPage.class, transaction, BPW50_ADDITIONAL_BP_INFORMATION, "BPW50", buttons);
        addPage(BPWizardPage16.class, transaction, BPW70_SEND_VERIFICATION, "BPW70", buttons);	 
        addPage(BPWizardPage17.class, transaction, BPW75_RECEIPT_PRINTING, "BPW75", buttons);	 
        addPage(BPWizardPage18.class, transaction, BPW80_ERROR_PAGE, "BPW80", buttons, true);	 
    }

    /**
     * Add the single keystroke mappings to the buttons. Use the function keys
     *   for back, next, cancel, etc.
     */
    private void addKeyMappings() {
        KeyMapManager.mapMethod(this, KeyEvent.VK_F2, 0, "f2SAR");	
        buttons.addKeyMapping("back", KeyEvent.VK_F11, 0);	
        buttons.addKeyMapping("next", KeyEvent.VK_F12, 0);	
        buttons.addKeyMapping("cancel", KeyEvent.VK_ESCAPE, 0);	
    }

    @Override
	public void start() {
        transaction.clear();
        if (transaction.isDirectoryOfBillers()) {
        	super.start(BPW05_DB_COMPANY_SEARCH); //dbCompanySearch is 8th page added in createPages()
        } else {        	
        	if (transaction.getTranEntryType() != MoneyGramSendTransaction.TRAN_ENTRY_PPC) {
        		super.start(BPW06_CARD_NUMBER_ENTRY);
        	} else {
        		super.start(BPW31_PPC_ENTRY);
        	}
            transaction.setReceivingCompany(null);
        }
        addKeyMappings();
    }
    
    public void f2SAR(){
        transaction.f2SAR();
    }

    /**
     * Page traversal logic here.
     */
    @Override
	public void exit(int direction) {
        String currentPage = cardLayout.getVisiblePageName();
        String nextPage = null;
        
        if (direction == PageExitListener.CANCELED) {
            transaction.escSAR();
            super.exit(direction);
            return;
        }
        
        if (direction == PageExitListener.COMPLETED) {
            transaction.f2NAG();
            super.exit(direction);
            return;
        }
        
        if (direction == PageExitListener.BACK) {
			transaction.setDataCollecionReview(true);
        	if (currentPage.equals(BPW34_PPC_VERIFY)) {
        		if (transaction.getFullBillerInfoList().size() == 1) {
    	        	showPage(BPW31_PPC_ENTRY, direction);
        		}
        	} else if (currentPage.equals(BPW31_PPC_ENTRY)) {
	        	showPage(BPW06_CARD_NUMBER_ENTRY, direction);
        	} else if (currentPage.equals(BPW25_SENDER_DETAIL)) {
        		popHistoryUntil(BASE_PAGE_LIST);
        	} else if (currentPage.equals(BPW30_COMPLIANCE) || 
        			currentPage.equals(BPW36_PPC_SENDER_DETAIL) || 
        			currentPage.equals(BPW38_PPC_THIRD_PARTY) || 
        			currentPage.equals(BPW39_PPC_COMPLIANCE) || 
        			currentPage.equals(BPW40_SUPPLEMENTAL_BP_INFORMATION) || 
        			currentPage.equals(BPW50_ADDITIONAL_BP_INFORMATION) || 
        			currentPage.equals(BPW70_SEND_VERIFICATION)) {
        		nextPage = this.previousDataCollectionScreen(currentPage);
	        	showPage(nextPage, direction);
        	} else {
        		super.exit(direction);
        	}
            return;
        }
        
        if (direction != PageExitListener.NEXT) {
        	super.exit(direction);
            return;
        }
             
        if (currentPage.equals(BPW06_CARD_NUMBER_ENTRY)) { 
			if ((transaction.getCustomers() == null) || (transaction.getCustomers().size() == 0)) {
				nextPage = BPW05_DB_COMPANY_SEARCH; 
			} else {
				nextPage = BPW16_SENDER_SELECTION; 
			}
		
        } else if (currentPage.equals(BPW16_SENDER_SELECTION)) {	
        	ProductVariantType productVariant = transaction.getProductVariant();
   		
   		  	if ( productVariant == null) {
   		  		nextPage = BPW05_DB_COMPANY_SEARCH;  
   		  	} else if (productVariant.equals(ProductVariantType.PREPAY)) {
   		  		if (transaction.isBillerGroupingNeeded()) {
   		  			nextPage = BPW07_DB_MATCHING_COMPANIES_1;
   		  		} else	{
   		  			nextPage = BPW10_DB_MATCHING_COMPANIES_2;
   		  		}
   		  	} else {
   		  		if (productVariant.equals(ProductVariantType.EP) || (productVariant.equals(ProductVariantType.UBP))) {
					if (transaction.hasBillerData()) {

						String cOT = transaction.getBillerInfo().getClassOfTradeCode();

						if ((cOT != null) && cOT.equalsIgnoreCase(DWValues.PPC_CLASS_OF_TRADE)) {
							nextPage = BPW31_PPC_ENTRY;
						} else {
							nextPage = this.nextDataCollectionScreen(currentPage);
						}
					} else {
						transaction.setValidationStatus(DataCollectionStatus.RETRY);
						nextPage = currentPage;
					}
   		  		}
   		  	}
        
        } else if (currentPage.equals(BPW05_DB_COMPANY_SEARCH)) {	
        	if (transaction.isBillerGroupingNeeded()) {
        		nextPage = BPW07_DB_MATCHING_COMPANIES_1;
        	} else {
        		nextPage = BPW10_DB_MATCHING_COMPANIES_2;       
        	}
      
        } else if (currentPage.equals(BPW14_DB_MATCHING_COMPANIES_3) || 
        		currentPage.equals(BPW25_SENDER_DETAIL) || 
        		currentPage.equals(BPW30_COMPLIANCE) || 
        		currentPage.equals(BPW40_SUPPLEMENTAL_BP_INFORMATION) || 
        		currentPage.equals(BPW50_ADDITIONAL_BP_INFORMATION)) {
        	nextPage = this.nextDataCollectionScreen(currentPage);

        } else if (currentPage.equals(BPW70_SEND_VERIFICATION)) { 
			if (transaction.getStatus() == ClientTransaction.FAILED) {
				nextPage = BPW80_ERROR_PAGE; 
			} else if (transaction.getStatus() == ClientTransaction.CANCELED) {
				nextPage = BPW70_SEND_VERIFICATION; 
			} else {
				nextPage = BPW75_RECEIPT_PRINTING;
			}
      /*
       * If the selected biller/transaction is EP and PPC, instead of the normal
       * screen flow, go to the PPC card swipe screen.
       */
        } else if (currentPage.equals(BPW10_DB_MATCHING_COMPANIES_2)) {
        	String cOT = transaction.getBillerInfo().getClassOfTradeCode();
    	  
        	if ((cOT != null) && cOT.equalsIgnoreCase(DWValues.PPC_CLASS_OF_TRADE) && (transaction.getProductVariant().equals(ProductVariantType.EP))) {
        		nextPage = BPW31_PPC_ENTRY;
        	}
      /*
       * If no Biller data was returned from the BIN lookup, stay on the PPC 
       * card swipe screen.  If 1 entry was returned, jump to the Verify screen
       * Otherwise do the BIN search result list page.
       */
        } else if (currentPage.equals(BPW31_PPC_ENTRY)) {
        	if (! transaction.hasBillerData()) {
            	nextPage = BPW31_PPC_ENTRY;
        	} else {
        		if (transaction.getFullBillerInfoList().size() == 1) {
        			nextPage = BPW34_PPC_VERIFY;
        		} else {
        			nextPage = BPW32_PPC_MATCHING_BINS;
        		}
        	}
      
        } else if (currentPage.equals(BPW34_PPC_VERIFY) || currentPage.equals(BPW36_PPC_SENDER_DETAIL) || currentPage.equals(BPW38_PPC_THIRD_PARTY) || currentPage.equals(BPW39_PPC_COMPLIANCE)) {
        	nextPage = this.nextDataCollectionScreen(currentPage);
        }
      
        if (nextPage != null) {
        	showPage(nextPage, direction);
        } else {
        	super.exit(direction);
        }
    }
    
	private String previousDataCollectionScreen(String currentPage) {
		String nextPage = transaction.previousDataCollectionScreen(currentPage);
		
		if (nextPage.isEmpty()) {
			if (transaction.isPrepayType()) {
				nextPage = BPW34_PPC_VERIFY;
			} else {
				nextPage = BPW14_DB_MATCHING_COMPANIES_3;
			}
		}
		
		return nextPage;
	}

	private String nextDataCollectionScreen(String currentPage) {
		
		String nextPage = transaction.nextDataCollectionScreen(currentPage);
		if ((! nextPage.isEmpty()) && (transaction.getDataCollectionData().isAfterValidationAttempt())) {
			nextPage = BPW50_ADDITIONAL_BP_INFORMATION;
			transaction.getDataCollectionData().setAfterValidationAttempt(false);
		}
		
		// If no more data needs to be collected, then a send validation should have been performed. 
		// Display the next screen based upon the status of the validation.

		if (nextPage.equals("")) {
			DataCollectionStatus status = transaction.getValidationStatus();
			if ((status.equals(DataCollectionStatus.NEED_MORE_DATA))
					|| (status.equals(DataCollectionStatus.NOT_VALIDATED))) {
				nextPage = BPW50_ADDITIONAL_BP_INFORMATION;
			} else if (status.equals(DataCollectionStatus.VALIDATED)) {
				nextPage = BPW70_SEND_VERIFICATION;
			} else if (status.equals(DataCollectionStatus.RETRY)) {
				nextPage = currentPage;
			} else {
        		if (transaction.getDataCollectionData().isAdditionalInformationScreenValid()) {
        			nextPage = BPW80_ERROR_PAGE;
        		} else {
        			nextPage = BPW50_ADDITIONAL_BP_INFORMATION;
        		}
			}
		}
		
		return nextPage;
	}
}
