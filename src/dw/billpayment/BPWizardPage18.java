package dw.billpayment;

import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.mgsend.MoneyGramSendFlowPage;
import dw.mgsend.MoneyGramSendTransaction;

/**
 * Page 15 of the MoneyGram Send Wizard. This page displays instructions on
 *   what to do if the transaction fails.
 */
public class BPWizardPage18 extends MoneyGramSendFlowPage {
	private static final long serialVersionUID = 1L;

    private boolean wizard = false;

    public BPWizardPage18(MoneyGramSendTransaction tran,
                       String name, String pageCode, PageFlowButtons buttons, boolean wizard) {
        super(tran, "billpayment/BPWizardPage18.xml", name, pageCode, buttons);	
        this.wizard  = wizard;
        if (wizard)
            removeComponent("okButton");	
    }

    @Override
	public void start(int direction) {
        if (! wizard) {
            addActionListener("okButton", this);	
            getComponent("okButton").requestFocus();	
        }
        else {
            flowButtons.reset();
            flowButtons.setVisible("expert", false);	
            flowButtons.setEnabled("next", false);	
            flowButtons.setEnabled("back", false);	
           // flowButtons.setAlternateText("cancel", "OK");	 
           flowButtons.setAlternateText("cancel",Messages.getString("OK"));  
            flowButtons.setSelected("cancel");	
        }
    }

    public void okButtonAction() {
        finish(PageExitListener.NEXT);
    }

    @Override
	public void finish(int direction) {
        PageNotification.notifyExitListeners(this, PageExitListener.FAILED);
    }
}
