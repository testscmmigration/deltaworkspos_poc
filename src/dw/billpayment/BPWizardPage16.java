package dw.billpayment;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import com.moneygram.agentconnect.ProductVariantType;
import com.moneygram.agentconnect.SessionType;

import dw.dialogs.Dialogs;
import dw.dialogs.FraudAlertDialog;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.dwgui.MoneyField;
import dw.framework.ClientTransaction;
import dw.framework.DataCollectionSet;
import dw.framework.FieldKey;
import dw.framework.KeyMapManager;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.main.DeltaworksMainPanel;
import dw.mgsend.MoneyGramSendFlowPage;
import dw.mgsend.MoneyGramSendTransaction;
import dw.mgsend.MoneyGramClientTransaction;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;
import dw.profile.UnitProfile;
import dw.utility.DWValues;
import dw.utility.Debug;

/**
 * Page 13 of the MoneyGram Send Wizard. This page displays a summary of the
 * send information and allows the user to send the transaction.
 */
public class BPWizardPage16 extends MoneyGramSendFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	// constants
	private static int PRINT_OK = 0;
	private static int PRINT_CANCELED = 1;
	private static int PRINT_RETRY = 2;
	
	// common
	private JLabel amountLabel;
	private JLabel atAmountLabel;
	private JScrollPane scrollPane1;
	private JScrollPane scrollPane2;
	private JPanel scrollee;
	private boolean autoPrintConfirmationSlip = false;
	private boolean hasNotBeenPrinted = true;
	private boolean hasPrintBeenStarted = false;
	private Object hasNotBeenPrintedLock = new Object();
	private boolean reprint = false;

	// calculation
	private JLabel feeLabel;
	private JLabel totalToCollectLabel;
	private JLabel printConfirmationLabel;
	private JButton printConfirmationButton;
	private MoneyField amountTenderedField;
	private MoneyField changeDueField;
	private JPanel amountTenderedMainPanel;
	private BigDecimal amount;
	// Net send
	private JLabel subagentCreditLimitValueLabel;
	private JLabel subagentCreditLimitTextLabel;
	private JComponent subagentCreditLimitBorder;	
	private DWTextPane nonCancelationNoticeLabel;
	private DWTextPane feeInstructionLabel;
	private int tagWidth;
	private ArrayList<DWTextPane> textPanes;

	
	public BPWizardPage16(MoneyGramSendTransaction tran, String name,
			String pageCode, PageFlowButtons buttons) {
		super(tran, "billpayment/BPWizardPage16.xml", name, pageCode, buttons); 
		amountLabel = (JLabel) getComponent("amountTagLabel"); 
		atAmountLabel = (JLabel) getComponent("amountAmountLabel"); 
		feeLabel = (JLabel) getComponent("feeAmountLabel");
		totalToCollectLabel = (JLabel) getComponent("totalToCollectAmountLabel"); 
		amountTenderedField = (MoneyField) getComponent("tenderedAmountLabel"); 
		changeDueField = (MoneyField) getComponent("changeDueAmountLabel"); 
		amountTenderedMainPanel = (JPanel) getComponent("amountTenderedMainPanel");      

		printConfirmationLabel = (JLabel) getComponent("printConfirmationLabel");
		printConfirmationButton = (JButton) getComponent("printConfirmationButton");

		// Net Sends
		subagentCreditLimitValueLabel = (JLabel) getComponent("subagentCreditLimitValueLabel");
        subagentCreditLimitTextLabel  = (JLabel) getComponent("subagentCreditLimitTextLabel");
        subagentCreditLimitBorder = (JComponent)getComponent("subagentCreditLimitBorder");

        // Non Cancelation Notice
        nonCancelationNoticeLabel = (DWTextPane) getComponent("nonCancelationNoticeLabel");
        nonCancelationNoticeLabel.addListener(this, this);
        nonCancelationNoticeLabel.setListenerEnabled(false);
        feeInstructionLabel =  (DWTextPane) getComponent("feeInstructionLabel");
        
		scrollPane1 = (JScrollPane) getComponent("scrollPane1");
		scrollPane2 = (JScrollPane) getComponent("scrollPane2");
		scrollPane2.setBorder(BorderFactory.createEmptyBorder());
		scrollPane2.setViewportBorder(BorderFactory.createEmptyBorder(0, 5,
				0, 5));
		scrollee = (JPanel) getComponent("scrollee"); 
	}

	public void calculateChangeDue() {
		amount = new BigDecimal(amountTenderedField.getText());
		if ((amount.compareTo(BigDecimal.ZERO) != 0)
				&& (amount.compareTo(transaction.getTotalToCollect()) > 0)) {
			BigDecimal changeDue = transaction
					.getChangeDue(
					// totalToCollectLabel.getText(),
							FormatSymbols.convertDecimal(totalToCollectLabel
									.getText()), amountTenderedField.getText());
			changeDueField.setText(changeDue.toString());
		} else
			changeDueField.setText("0"); 
	}

	@Override
	public void start(int direction) {
		flowButtons.reset();
		flowButtons.setVisible("expert", false); 
	     
		setupAutoscroll(scrollPane2, scrollee, new JComponent[] {
				amountTenderedField, 
				changeDueField
			});

		textPanes = new ArrayList<DWTextPane>();
    	List<JLabel> tags = new ArrayList<JLabel>();
		
		/*
	  	 * For PPC use "Load F5", else use "Send F5"
	  	 */
  	    String cOT = transaction.getBillerInfo().getClassOfTradeCode();
	    if ((cOT != null) && cOT.equalsIgnoreCase(DWValues.PPC_CLASS_OF_TRADE)) {
			flowButtons.setAlternateText("next", Messages.getString("BPWizardPage16.loadButtonText"));  
	    } else {
			flowButtons.setAlternateText("next", Messages.getString("BPWizardPage16.sendButtonText"));  
	    }
		flowButtons.setAlternateKeyMapping("next", KeyEvent.VK_F5, 0); 
		
        DataCollectionSet data = transaction.getDataCollectionData();
        
		// Sender Name
		
        String name;
        if (transaction.isFormFreeTransaction() || transaction.isPrepayType()) {
	        name = DWValues.formatName(data.getValue(FieldKey.SENDER_FIRSTNAME_KEY), data.getValue(FieldKey.SENDER_MIDDLENAME_KEY), data.getValue(FieldKey.SENDER_LASTNAME_KEY), data.getValue(FieldKey.SENDER_LASTNAME2_KEY));
        } else if (transaction.getProductVariant().equals(ProductVariantType.UBP)) {
			name = "";
		} else {
	        name = DWValues.formatName(data.getFieldStringValue(FieldKey.SENDER_FIRSTNAME_KEY), data.getFieldStringValue(FieldKey.SENDER_MIDDLENAME_KEY), data.getFieldStringValue(FieldKey.SENDER_LASTNAME_KEY), data.getFieldStringValue(FieldKey.SENDER_LASTNAME2_KEY));
		
	  	   // For PPC change amount to load amount

	        if ((cOT != null)  && cOT.equalsIgnoreCase(DWValues.PPC_CLASS_OF_TRADE)) {
				amountLabel.setText(Messages.getString("BPWizardPage16.1162a"));
			}
		}
        this.displayPaneItem("name", name, tags, textPanes);
		
		String currency = transaction.agentBaseCurrency();

		// Third Party
		
		String thirdPartyOrg = data.getFieldStringValue(FieldKey.THIRDPARTY_SENDER_ORGANIZATION_KEY);
		String thirdPartyFirstName = data.getFieldStringValue(FieldKey.THIRDPARTY_SENDER_FIRSTNAME_KEY);
		String thirdPartyMiddleName = data.getFieldStringValue(FieldKey.THIRDPARTY_SENDER_MIDDLENAME_KEY);
		String thirdPartyLastName = data.getFieldStringValue(FieldKey.THIRDPARTY_SENDER_LASTNAME_KEY);
		String thirdPartyLastName2 = data.getFieldStringValue(FieldKey.THIRDPARTY_SENDER_LASTNAME2_KEY);
		String thirdPartyName;
		if (thirdPartyOrg.length() > 0) {
			thirdPartyName = thirdPartyOrg;
		} else if (thirdPartyLastName.length() > 0) {
			thirdPartyName = transaction.formatFullName(thirdPartyFirstName, thirdPartyMiddleName, thirdPartyLastName, thirdPartyLastName2);
		} else {
			thirdPartyName = "";
		}
        this.displayPaneItem("thirdParty", thirdPartyName, tags, textPanes);

		// Account Number

		StringBuffer accountNumber;
        if (transaction.isFormFreeTransaction()) {
        	accountNumber = new StringBuffer(data.getCurrentValue(FieldKey.BILLER_ACCOUNTNUMBER_KEY.getInfoKey()));
        } else if ((transaction.getBpAccountNumber() != null) && (! transaction.getBpAccountNumber().isEmpty())) {
    		accountNumber = new StringBuffer(transaction.getBpAccountNumber());
    	} else {
    		accountNumber = new StringBuffer(data.getCurrentValue(FieldKey.BILLER_ACCOUNTNUMBER_KEY.getInfoKey()));
        }
		int l = accountNumber.length();
		if (l > 4) {
			for (int i = 0; i < l - 4; i++) {
				accountNumber.setCharAt(i, '*');
			}
		}
//		accountLabel.setText(accountNumber.toString());
		this.displayLabelItem("account", accountNumber.toString(), tags);
		
		// Biller Name

		String epAgencyName;
		if (transaction.isFormFreeTransaction()) {
			epAgencyName = data.getCurrentValue(FieldKey.RECEIVE_AGENTNAME_KEY.getInfoKey()); 
		} else {
			epAgencyName = transaction.getBillerInfo().getBillerName();
		}
        this.displayPaneItem("billerName", epAgencyName, tags, textPanes);
		
		// Receiver Name
		
        String rName = DWValues.formatName(data.getFieldStringValue(FieldKey.RECEIVER_FIRSTNAME_KEY), data.getFieldStringValue(FieldKey.RECEIVER_MIDDLENAME_KEY), data.getFieldStringValue(FieldKey.RECEIVER_LASTNAME_KEY), data.getFieldStringValue(FieldKey.RECEIVER_LASTNAME2_KEY));
        this.displayPaneItem("receiverName", rName, tags, textPanes);
		
		// Receive Code
		
        String receiveCode;
		if (transaction.isFormFreeTransaction()) {
			receiveCode = data.getFieldStringValue(FieldKey.RECEIVE_CODE_KEY);
		} else {
			receiveCode = transaction.getBillerInfo().getReceiveCode();
		}
		this.displayLabelItem("receiveCode", receiveCode, tags);
		
		String billerNotes = transaction.getBillerInfo().getBillerNotes();
		String serviceOffering = transaction.getBillerInfo().getServiceOffering();
		
		String paymentTypeTag = null;
		String postingInfo = null;
		if (transaction.getProductVariant().equals(ProductVariantType.EP)) {
			paymentTypeTag = Messages.getString("BPWizardPage16.paymentType1");
			postingInfo = billerNotes;
		} else if (transaction.getProductVariant().equals(ProductVariantType.UBP)) {
			paymentTypeTag = Messages.getString("BPWizardPage16.paymentType2");
			serviceOffering = "";
			postingInfo = billerNotes;
		} else if (transaction.getProductVariant().equals(ProductVariantType.PREPAY)) {
			postingInfo = billerNotes;
			paymentTypeTag = Messages.getString("BPWizardPage16.paymentType3");
		} else {
			paymentTypeTag = Messages.getString("BPWizardPage16.paymentType4");
			serviceOffering = "";
		}
		
		this.displayLabelItem("paymentType", paymentTypeTag, false, tags);
		this.displayLabelItem("postingInfo", postingInfo, false, tags);
		this.displayLabelItem("serviceLevel", serviceOffering, false, tags);
		
		this.displayMoney("amount", BigDecimal.ZERO, currency, tags);
		this.displayMoney("fee", BigDecimal.ZERO, currency, tags);
		this.displayMoney("totalToCollect", BigDecimal.ZERO, currency, tags);
		this.displayMoney("tendered", BigDecimal.ZERO, currency, tags);
		this.displayMoney("changeDue", BigDecimal.ZERO, currency, tags);
		
		if (transaction.isReceiptReceived() && transaction.isReceiveBPConfirmation()) {
			printConfirmationButton.setVisible(true);
			printConfirmationLabel.setVisible(true);
			printConfirmationButton.setEnabled(true);
			printConfirmationLabel.setEnabled(true);
			autoPrintConfirmationSlip = transaction.isAutoPrintBPConfirmation();
		} else {
			printConfirmationButton.setVisible(false);
			printConfirmationLabel.setVisible(false);
			autoPrintConfirmationSlip = false;
		}
		atAmountLabel.setText(transaction.getAmount().toString());

		if (transaction.isAmountTenderedEnabled()) {
			if (! transaction.isFormFreeTransaction()) {
				feeLabel.setText(transaction.getFeeSendTotalWithTaxBp(true) != null
						? FormatSymbols.convertDecimal(transaction.getFeeSendTotalWithTaxBp(true).toString()) : "");
				BigDecimal tatc = transaction.getCurrentFeeInfo().getSendAmounts().getTotalAmountToCollect();
				totalToCollectLabel.setText(FormatSymbols.convertDecimal(tatc.toString()));
			} else {
				BigDecimal fee = transaction.getTransactionLookupResponsePayload().getSendAmounts().getTotalSendFees();
				BigDecimal tax = transaction.getTransactionLookupResponsePayload().getSendAmounts().getTotalSendTaxes();
				feeLabel.setText(fee.add(tax).toString());
				totalToCollectLabel.setText(transaction.getTransactionLookupResponsePayload().getSendAmounts().getTotalAmountToCollect().toString());
			}
			amountTenderedField.requestFocus();
		} else {
			amountTenderedMainPanel.setVisible(false);
			flowButtons.setSelected("next"); 
		}

		addFocusLostListener("tenderedAmountLabel", this, "calculateChangeDue");          

		addActionListener("printConfirmationButton", this, "printConfirmationReceipt");
		KeyMapManager.mapComponent(this, KeyEvent.VK_F10, 0,
				getComponent("printConfirmationButton"));

		amountTenderedField.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				flowButtons.getButton("next").requestFocus(); 
			}
		});
			
		if (! UnitProfile.getInstance().isMultiAgentMode()) {
			 subagentCreditLimitBorder.setVisible(false);
			 subagentCreditLimitValueLabel.setVisible(false);
			 subagentCreditLimitTextLabel.setVisible(false);
			 nonCancelationNoticeLabel.setVisible(true);
		} else {
			 nonCancelationNoticeLabel.setVisible(false);
			 BigDecimal saLimit = transaction.getSaLimitAvailable();
			 if (saLimit != null) {
				 subagentCreditLimitValueLabel.setText(FormatSymbols.convertDecimal(saLimit.toString()) + "		 " + currency);
			 }
		}

		if (autoPrintConfirmationSlip && hasNotBeenPrinted) {
			// reentrant processing
			synchronized (hasNotBeenPrintedLock) {
				if (hasNotBeenPrinted && !hasPrintBeenStarted)
					printConfirmationReceipt();
				hasNotBeenPrinted = false;
			}
		}

        transaction.doEarlyISI();

        nonCancelationNoticeLabel.setListenerEnabled(true);

        tagWidth = this.setTagLabelWidths(tags, MINIMUM_TAG_WIDTH);
		
		dwTextPaneResized();
	}

	/**
	 * Called by transaction after it receives response from middleware.
	 * 
	 * @param direction
	 *            is a wizard direction.
	 */
	@Override
	public void finish(int direction) {
		// call setButtonsEnabled to avoid duplicated transactions.
		flowButtons.setButtonsEnabled(false);
		amount = new BigDecimal(amountTenderedField.getText());
		nonCancelationNoticeLabel.setListenerEnabled(false);
		
		if (direction == PageExitListener.NEXT) {
			
			// if amount tendered is less than total to collect or greater than
			// daily limit don't allow to continue.
			if ((amount.compareTo(BigDecimal.ZERO) != 0) && (amount.compareTo(transaction.getTotalToCollect()) < 0)) {
				Dialogs.showWarning("dialogs/DialogInsufficientAmount.xml"); 
				amountTenderedField.requestFocus();
				amountTenderedField.setText("0"); 
				changeDueField.setText("0"); 
				flowButtons.setButtonsEnabled(true);
				return;
			} else if (amount.compareTo(transaction.getDailyLimit()) > 0) {
				java.awt.Toolkit.getDefaultToolkit().beep();
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						amountTenderedField.requestFocus();
					}
				});
				amountTenderedField.setText("0"); 
				changeDueField.setText("0"); 
				flowButtons.setButtonsEnabled(true);
				return;
			}

			// first make sure the agent has the money
			if (transaction.isFraudAlertNeeded(transaction.agentBaseCurrency())) {
                FraudAlertDialog fraudDialog = new FraudAlertDialog();
                fraudDialog.showPanel();
				if (fraudDialog.isCanceled()) {
					finish(PageExitListener.CANCELED);
					return;
				} else if (!fraudDialog.isAccepted()) {
					flowButtons.setButtonsEnabled(true);
					flowButtons.setSelected("next"); 
					return;
				}
			}
			transaction.completeSession(this, SessionType.BP, null, transaction.getReferenceNumber());

		} else {
			synchronized (hasNotBeenPrintedLock) {
				hasNotBeenPrinted = true;
				hasPrintBeenStarted = false;
			}
			PageNotification.notifyExitListeners(this, direction);
		}
	}

    @Override
	public void printConfirmationReceipt() {
    	// invoked from "print confirmation" button, or from "auto print" PE specification
        if (autoPrintConfirmationSlip && hasNotBeenPrinted)  {
	    	if (printConfirmationRcpt() == PRINT_CANCELED) {
	    		return;
	    	}
		} else {
			// primary/secondary specification already determined
			if (printConfirmationRcpt() == PRINT_CANCELED) {
				return;
			} 
		}
	}

    private int printConfirmationRcpt() {
    	hasPrintBeenStarted = true;
		int option;
		String cTran;
		do {
			super.printConfirmationReceipt();
			option = printDone();
			cTran = DeltaworksMainPanel.getMainPanel().getVisiblePageName();
		} while (option == PRINT_RETRY && !cTran.equalsIgnoreCase("mainMenu"));
		if (option == PRINT_OK) {
			hasNotBeenPrinted = false;
		} else {
			/*
			 * If disclosure was required, then on cancel abort transaction,
			 * otherwise basically ignore the cancel.
			 */
			if (autoPrintConfirmationSlip) {
				PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
				return PRINT_CANCELED;
			} else {
				return PRINT_OK;
			}
		}
        printConfirmationButton.setText(Messages.getString("MGSWizardPage13.1177a"));
        reprint = true;
        return PRINT_OK;
    }

	private int printDone() {
		int bRetValue = PRINT_OK;
       	if (!transaction.wasDiscPrintSucessful()) {
       		if (cancelPrint()) {
       			bRetValue = PRINT_CANCELED;
       		} else {
       			bRetValue = PRINT_RETRY;
       		}
       	}
       	hasPrintBeenStarted = false;
		return bRetValue;
	}

	private boolean cancelPrint() {
		String sMessage, sMessage2, sMessage3, sMessage4;
		
		if (reprint) {
			if (autoPrintConfirmationSlip) {
				sMessage = Messages.getString("DialogReceiptReprintFailurePre.Text1");
				sMessage2 = Messages.getString("DialogReceiptReprintFailurePre.Text2");
				sMessage3 = Messages.getString("DialogReceiptReprintFailurePre.Text3");
				sMessage4 = Messages.getString("DialogReceiptReprintFailurePre.Text4");
			} else {
				sMessage = Messages.getString("DialogReceiptReprintFailurePreNR.Text1");
				sMessage2 = Messages.getString("DialogReceiptReprintFailurePreNR.Text2");
				sMessage3 = Messages.getString("DialogReceiptReprintFailurePreNR.Text3");
				sMessage4 = Messages.getString("DialogReceiptReprintFailurePreNR.Text4");
			}
		} else {
			if (autoPrintConfirmationSlip) {
				sMessage = Messages.getString("DialogReceiptPrintFailurePre.Text1");
				sMessage2 = Messages.getString("DialogReceiptPrintFailurePre.Text2");
				sMessage3 = Messages.getString("DialogReceiptPrintFailurePre.Text3");
				sMessage4 = Messages.getString("DialogReceiptPrintFailurePre.Text4");
			} else {
				sMessage = Messages.getString("DialogReceiptPrintFailure.Text1");
				sMessage2 = Messages.getString("DialogReceiptPrintFailure.Text2");
				sMessage3 = Messages.getString("DialogReceiptPrintFailure.Text3");
				sMessage4 = Messages.getString("DialogReceiptPrintFailure.Text4");
			}
		}

		boolean bRetValue = Dialogs.showPrintCancel("dialogs/BlankDialog3.xml",
				sMessage, sMessage2, sMessage3, sMessage4) != Dialogs.OK_OPTION;
		return bRetValue;
	}

	/**
	 * Called by transaction after it receives response from middleware.
	 * 
	 * @param commTag
	 *            is function index.
	 * @param returnValue
	 *            is Boolean set to true if response is good. Otherwise, it is
	 *            set to false.
	 */
	@Override
	public void commComplete(int commTag, Object returnValue) {
		if (returnValue.equals(Boolean.TRUE)) {
			/*
			 * Ignore any responses other than a commit response
			 */
			switch (commTag) {

//			case MoneyGramSendTransaction.BP_VALIDATE:
//				if (autoPrintConfirmationSlip && hasNotBeenPrinted) {
//					printConfirmationReceipt();
//					hasNotBeenPrinted = false;
//				}
//				transaction.sendCommit(this);
//				break;
			
			case MoneyGramClientTransaction.COMPLETE_SESSION:
				PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
				break;
			default:
				Debug.println(getPageCode()+ " - commComplete, unsupported commTag[" + commTag + "]");
				start(PageExitListener.NEXT);
				break;
			}
		} else {
			if (transaction.getStatus() == ClientTransaction.NEEDS_RECOMMIT) {
				transaction.bpValidation(this, ValidationType.INITIAL_NON_FORM_FREE);
			} else if (transaction.getStatus() == ClientTransaction.CANCELED) {
				flowButtons.setButtonsEnabled(true);
			} else {
				PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
			}
		}
	}

	@Override
	public void dwTextPaneResized() {
		nonCancelationNoticeLabel.setWidth(FIELD_TOTAL_WIDTH);
		feeInstructionLabel.setWidth(FIELD_TOTAL_WIDTH - 40);

		int w = FIELD_TOTAL_WIDTH - tagWidth - 40;
		for (DWTextPane pane : textPanes) {
			pane.setWidth(w);
		}
		
		this.scrollpaneResized(scrollPane1, scrollPane2, scrollee);
	}
}
