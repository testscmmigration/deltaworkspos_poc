package dw.billpayment;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;

import com.moneygram.agentconnect.ProductVariantType;

import dw.framework.KeyMapManager;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.mgsend.MoneyGramSendFlowPage;
import dw.mgsend.MoneyGramSendTransaction;
import dw.utility.DWValues;
import dw.utility.IdleBackoutTimer;

/**
 *  Page 14 of the MoneyGram Send Wizard. This page displays the results of the
 *   send info and prints the agent and customer receipts.
 */
/**  
 *  Modified to print both receipts on same screen as in expert mode.
 */
public class BPWizardPage17 extends MoneyGramSendFlowPage {
	private static final long serialVersionUID = 1L;

    private JLabel amountLabel;
    private JButton reprintButton;
    private JLabel reprintLabel;
    private JLabel printingLabel;
    private JLabel instruction1Label;
    private JLabel instruction2Label;
    private JLabel instruction3Label;
    private JLabel rule3continued;
    private JLabel instruction4Label;
            
    public BPWizardPage17(MoneyGramSendTransaction tran,
    String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "billpayment/BPWizardPage17.xml", name, pageCode, buttons);	
        amountLabel = (JLabel) getComponent("amountAmountLabel");	
        reprintButton = (JButton) getComponent("reprintButton");
        reprintLabel = (JLabel) getComponent("reprintLabel");
        printingLabel = (JLabel) getComponent("printingLabel");
        instruction1Label = (JLabel) getComponent("instruction1Label");
        instruction2Label = (JLabel) getComponent("instruction2Label");
        instruction3Label = (JLabel) getComponent("instruction3Label");        
        rule3continued= (JLabel) getComponent("rule3continued");
        instruction4Label = (JLabel) getComponent("instruction4Label");
    }
    
    @Override
	public void start(int direction) {
        flowButtons.reset();
        flowButtons.setVisible("expert", false);	
        flowButtons.setEnabled("expert", false);	
        flowButtons.setEnabled("back", false);	
        flowButtons.setEnabled("next",false );	
        flowButtons.setVisible("back", true);	
        flowButtons.setVisible("next",true );	
        rule3continued.setText("     ".concat(rule3continued.getText()));
        
        /*
         * For PPC change amount to load amount
         */
        String cOT = transaction.getBillerInfo().getClassOfTradeCode();
        if ((cOT != null) 
        		&& cOT.equalsIgnoreCase(DWValues.PPC_CLASS_OF_TRADE)) {
        	amountLabel.setText(Messages.getString("BPWizardPage17.1188a"));
        }
        
        if (transaction.isReceiptReceived()) {
            reprintButton.setEnabled(true);
            reprintLabel.setEnabled(true);
            printingLabel.setText(Messages.getString("BPWizardPage17.IntructionTitle"));
            instruction1Label.setText(Messages.getString("BPWizardPage17.Instruction1")); 
            instruction2Label.setText(Messages.getString("BPWizardPage17.Instruction2")); 
            instruction3Label.setText(Messages.getString("BPWizardPage17.Instruction3")); 
            instruction1Label.setVisible(true);
            instruction2Label.setVisible(true);
            instruction3Label.setVisible(true);
            if (! transaction.getProductVariant().equals(ProductVariantType.EP)) {
            	rule3continued.setText("     ".concat(Messages.getString("BPWizardPage17.Instruction3d")));
            	Font f2 = new Font(rule3continued.getFont().getFontName(),Font.BOLD,rule3continued.getFont().getSize());
            	rule3continued.setFont(f2);
            	rule3continued.setForeground(Color.RED);
            	rule3continued.setVisible(true);
            } else {
            	rule3continued.setVisible(false);
            }
            instruction4Label.setText(Messages.getString("BPWizardPage17.Instruction4")); //$NON-NLS-1
            instruction4Label.setVisible(true);
        } else {
            reprintButton.setEnabled(false);
            reprintLabel.setEnabled(false);
            printingLabel.setText(Messages.getString("BPWizardPage17.IntructionTitleAlt"));
            instruction1Label.setText(Messages.getString("BPWizardPage17.Instruction2_1")); 
            instruction1Label.setVisible(true);
            instruction2Label.setVisible(false); 
            instruction3Label.setVisible(false); 
            rule3continued.setVisible(false);
            instruction4Label.setVisible(false); 
        }
        
    	List<JLabel> tags = new ArrayList<JLabel>();

    	String paymentType;
        if (transaction.getProductVariant().equals(ProductVariantType.EP)) {
        	paymentType = Messages.getString("BPWizardPage17.paymentType1");
        } else if (transaction.getProductVariant().equals(ProductVariantType.UBP)) {
        	paymentType = Messages.getString("BPWizardPage17.paymentType2");
        } else if (transaction.getProductVariant().equals(ProductVariantType.PREPAY)) {
        	paymentType = Messages.getString("BPWizardPage17.paymentType3");
        } else {
        	paymentType = Messages.getString("BPWizardPage17.paymentType4");
        }
        
        this.displayLabelItem("paymentType", paymentType, false, false, tags);
        
        addActionListener("reprintButton", this, "reprintReceipt");	 
              
        KeyMapManager.mapComponent(this, KeyEvent.VK_F5, 0,
        getComponent("reprintButton"));	
       
        String sendCurrency = transaction.agentBaseCurrency();
		
		this.displayLabelItem("referenceNumber", transaction.getCommitReferenceNumber(), false, false, tags);
		
		BigDecimal fee;
		BigDecimal tatc;
		if (transaction.isPrepayType()) {
			fee = transaction.getCurrentFeeInfo().getSendAmounts().getTotalSendFees();
			tatc = transaction.getCurrentFeeInfo().getSendAmounts().getTotalAmountToCollect();
		} else if (! transaction.isFormFreeTransaction()) {
			BigDecimal f = transaction.getCurrentFeeInfo().getSendAmounts().getTotalSendFees();
			BigDecimal t = transaction.getCurrentFeeInfo().getSendAmounts().getTotalSendTaxes();
	        fee = f.add(t);
			tatc = transaction.getBpValidationResponsePayload().getSendAmounts().getTotalAmountToCollect();
		} else {
			BigDecimal f = transaction.getBpValidationResponsePayload().getSendAmounts().getTotalSendFees();
			BigDecimal t = transaction.getBpValidationResponsePayload().getSendAmounts().getTotalSendTaxes();
	        fee = f.add(t);
			tatc = transaction.getBpValidationResponsePayload().getSendAmounts().getTotalAmountToCollect();
		}
		this.displayMoney("totalAmount", tatc, sendCurrency, tags);
		this.displayMoney("amount", transaction.getAmount(), sendCurrency, tags);
		this.displayMoney("fee", fee, sendCurrency, tags);

        this.setTagLabelWidths(tags, MINIMUM_TAG_WIDTH);

        flowButtons.disable();

        if (transaction.isReceiveAgentAndCustomerReceipts()) {
        	// agent (non-reprint) receipt
            printReceipt(false, false, null);
            // followed by customer receipt
            printReceipt(true, false, null);
		} else if (transaction.isReceiveCustomerReceipt()) {
			printReceipt(true, false, null);
		} else if (transaction.isReceiveAgentReceipt()) {
			printReceipt(false, false, null);
		}
        
        flowButtons.setEnabled("cancel", true);	
        //flowButtons.setAlternateText("cancel", "OK");	 
        flowButtons.setAlternateText("cancel",Messages.getString("OK"));  
        flowButtons.setSelected("cancel");	
         
        // last receipt has been printed, so allow backout timer again
        IdleBackoutTimer.start();
        
    }
        
    /** Send Agent and customer receipts to the Epson printer when user clicks 
     *  the reprint button
     * 
     */
    public void reprintReceipt() {
    	reprint(transaction);
    }
    
	public static void reprint(MoneyGramSendTransaction tran) {
		if (tran.isReceiveAgentAndCustomerReceipts()) {
			tran.printReceipt(false, true);
			tran.printReceipt(true, true);
		} else if (tran.isReceiveCustomerReceipt()) {
			tran.printReceipt(true, true);
		} else if (tran.isReceiveAgentReceipt()) {
			tran.printReceipt(false, true);
		}
	}
   
    @Override
	public void finish(int direction) {
        flowButtons.disable();
        if (direction == PageExitListener.CANCELED) // Cancel is now OK button
            direction = PageExitListener.COMPLETED;
        PageNotification.notifyExitListeners(this, direction);
    }
}
