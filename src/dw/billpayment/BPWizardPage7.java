package dw.billpayment;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import com.moneygram.agentconnect.ProductVariantType;

import dw.dialogs.Dialogs;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.DataCollectionField;
import dw.framework.FieldKey;
import dw.framework.DataCollectionScreenToolkit;
import dw.framework.DataCollectionSet;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.mgsend.MoneyGramSendFlowPage;
import dw.mgsend.MoneyGramSendTransaction;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;
import dw.utility.ExtraDebug;
import dw.utility.StringUtility;

/**
 * Page 4 of the MoneyGram Send Wizard. This page displays the information about
 *   the currently selected sender for the user to verify, or prompts the user
 *   to enter all the information if this is not an existing sender.
 */
public class BPWizardPage7 extends MoneyGramSendFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	private JScrollPane scrollPane1;
	private JScrollPane scrollPane2;
	private JPanel panel2;
	private DWTextPane text1;
	private DataCollectionScreenToolkit toolkit = null;
	private String selectText;
	private Object dataCollectionSetChangeFlag = new Object();
	private JButton nextButton;
	
    public BPWizardPage7(MoneyGramSendTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "billpayment/BPWizardPage7.xml", name, pageCode, buttons);	
		scrollPane1 = (JScrollPane) getComponent("scrollPane1");
		scrollPane2 = (JScrollPane) getComponent("scrollPane2");
		panel2 = (JPanel) getComponent("panel2"); 
		text1 = (DWTextPane) getComponent("text1");
		nextButton = this.flowButtons.getButton("next");
    }

	@Override
	public void dwTextPaneResized() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				int width = scrollPane2.getVisibleRect().width;
				text1.setWidth(width);
			}
		});
	}

	private void setupComponents() {
		
		// Check if the GFFP information has changed.
		
		DataCollectionSet data = this.transaction.getDataCollectionData();

		if (this.dataCollectionSetChangeFlag != this.transaction.getDataCollectionData().getChangeFlag()) {
	    	this.dataCollectionSetChangeFlag = this.transaction.getDataCollectionData().getChangeFlag();
	    	
	    	// Rebuild the data collection items for the screen.
	    	
	    	ExtraDebug.println("Rebuilding Screen " + getPageCode() + " from FTC data");
	    	
			int flags = DataCollectionScreenToolkit.SHORT_LABEL_VALUES_FLAG + DataCollectionScreenToolkit.DISPLAY_REQUIRED_FLAG;
			JButton backButton = this.flowButtons.getButton("back");
			this.toolkit = new DataCollectionScreenToolkit(this, this.scrollPane1, this, this.scrollPane2, this.panel2, flags, transaction.getScreen25Status().getDataCollectionPanels(data), this.transaction, backButton, nextButton, null);
		}
		
		// Check if the screen needs to be laid out.
		
		if (transaction.getScreen25Status().needLayout(data)) {
			toolkit.populateDataCollectionScreen(panel2, transaction.getScreen25Status().getDataCollectionPanels(data), false);
		}
	}

    @Override
	public void start(int direction) {
    	setupComponents();
    	
    	selectText = Messages.getString("BPWizardPage7.item1");
        
    	flowButtons.reset();
        flowButtons.setVisible("expert", false);	
        flowButtons.setEnabled("expert", false);	

        // Set the text of the instruction label.

        if (transaction.isNewCustomer()) {
        	text1.setText(Messages.getString("BPWizardPage7.enterLabel1")); 
        } else {
        	text1.setText(Messages.getString("BPWizardPage7.verifyLabel1")); 
        }

        DataCollectionField senderHomePhoneField = toolkit.getScreenField(FieldKey.SENDER_PRIMARYPHONE_KEY);
        if (senderHomePhoneField != null) {
            String accountNumber = transaction.getBpAccountNumber();
            String phoneNumber = transaction.getSender().getHomePhone();
            if ((accountNumber == null) || (accountNumber.isEmpty())) {
		        if ((phoneNumber == null || phoneNumber.isEmpty()) && (! transaction.isUsingCard())) {
		            senderHomePhoneField.setValue(transaction.getCardPhoneNumber());
		        } else {
		            senderHomePhoneField.setValue(phoneNumber);
		        }
            }
        }

        if (direction == PageExitListener.BACK) {
			DataCollectionScreenToolkit.restoreDataCollectionFields(this.toolkit.getScreenFieldList(), this.transaction.getDataCollectionData());
		}
		
        /*
         * If we did an account number lookup, use that account number.
         */
        if (direction == PageExitListener.NEXT) {
            String lookupAcctNbr = transaction.getBpAccountNumber();
            DataCollectionField billerAcctNbrField = toolkit.getScreenField(FieldKey.BILLER_ACCOUNTNUMBER_KEY);
            if ((lookupAcctNbr != null) && (! lookupAcctNbr.isEmpty()) && (billerAcctNbrField != null)) {
                billerAcctNbrField.setValue(lookupAcctNbr);
            }
		}
		
        if (direction == PageExitListener.BACK) {
        	transaction.getSender().setMoneySaverID(transaction.getMoneySaverID());
        }

		// Move the cursor to 1) the first required item with no value or item 
		// with an invalid value or 2) the first optional item with no value.
		
		this.toolkit.moveCursor(this.toolkit.getScreenFieldList(), nextButton);

        // Enable and call the resize listener.

    	text1.setListenerEnabled(true);
		toolkit.enableResize();
		toolkit.getComponentResizeListener().componentResized(null);
    }

    @Override
	public void finish(int direction) {

    	// Disable the resize listener for this screen.

		toolkit.disableResize();

    	// If the flow direction is to the next screen, check the validity of the
    	// data. If a value is invalid or a required item does not have a value,
    	// return to the screen.
    	
    	if (direction == PageExitListener.NEXT) {
        	String tag = toolkit.validateData(toolkit.getScreenFieldList(), DataCollectionField.DISPLAY_ERRORS_VALIDATION);
        	if (tag != null) {
				flowButtons.setButtonsEnabled(true);
				toolkit.enableResize();
        		return;
        	}

	    	DataCollectionField purposeOfFundItem = toolkit.getScreenField(FieldKey.PURPOSEOFFUND_KEY);
	    	if ((purposeOfFundItem != null) && (purposeOfFundItem.getValue().equals(selectText))) {
	    		purposeOfFundItem.focus(scrollPane2, panel2, true);
	    		flowButtons.setButtonsEnabled(true);
	    		return;
	        }
	
	        if ((transaction.getProductVariant().equals(ProductVariantType.EP)) && (transaction.getSenderFirstName() != null) && (transaction.getSenderFirstName().trim().length() != 0)) {
	            if (! validateMSCardSenderName()) {
	            	flowButtons.setButtonsEnabled(true);
	            	return;
	            }
	        }

			DataCollectionField validateAccountNumberField = toolkit.getScreenField(FieldKey.VALIDATE_ACCOUNT_NBR_KEY);
	        if (! verifyAccountNumberField() && validateAccountNumberField != null) {
	        	Dialogs.showWarning("dialogs/DialogInvalidAccountNumber.xml");
	        	validateAccountNumberField.setValue("");
	        	validateAccountNumberField.focus(scrollPane2, panel2, true);
	        	flowButtons.setButtonsEnabled(true);
	        	return;
	        }
    	}

    	// If the flow direction is to the next screen or back to the previous
    	// screen, save the value of the current items in a data structure.

        if ((direction == PageExitListener.BACK) || (direction == PageExitListener.NEXT)) {
        	DataCollectionScreenToolkit.saveDataCollectionFields(toolkit.getScreenFieldList(), transaction.getDataCollectionData());
        }

		if ((direction == PageExitListener.NEXT) && (transaction.nextDataCollectionScreen(BillPaymentWizard.BPW25_SENDER_DETAIL).isEmpty()) && transaction.getDataCollectionData().isAdditionalInformationScreenValid()) {
	  		transaction.bpValidation(this, ValidationType.SECONDARY);
	  	} else {
	    	PageNotification.notifyExitListeners(this, direction);
	  	}
    }

    /**
	 * Called by transaction after it receives response from middleware.
	 *
	 * @param commTag
	 *            is function index.
	 * @param returnValue
	 *            is Boolean set to true if response is good. Otherwise, it is
	 *            set to false.
	 */
	@Override
	public void commComplete(int commTag, Object returnValue) {
		boolean b = ((Boolean) returnValue).booleanValue();
		if (b) {
			PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
		} else {
			start(PageExitListener.NEXT);
		}
	}

	private boolean validateMSCardSenderName() {
		DataCollectionField senderFirstNameItem = toolkit.getScreenField(FieldKey.SENDER_FIRSTNAME_KEY);
		DataCollectionField senderLastNameItem = toolkit.getScreenField(FieldKey.SENDER_LASTNAME_KEY);

		if ((senderFirstNameItem == null) || (senderLastNameItem == null)) {
			return false;
		}

		String actualFName = transaction.getSenderFirstName().trim();
		String suppliedFname = senderFirstNameItem.getValue();
		String actualLName = transaction.getSenderLastName().trim();
		String suppliedLname = senderLastNameItem.getValue();
		if ((isMatchingName(actualFName, suppliedFname)) && (isMatchingName(actualLName, suppliedLname))) {
			return true;
		}

		int rc = Dialogs.showOkCancel("dialogs/DialogMSCardNameDoesNotMatchNoAutoEnrollEP.xml");  
		if (rc == Dialogs.OK_OPTION) {
			transaction.getSender().setMoneySaverID("");
			return true;
		}
		return false;
	}

    private boolean isMatchingName(String actual, String supplied){
    	String first3CharsActual = "";
    	String first3CharsSupplied = "";
    	if (actual.length() >= 3) {
    		first3CharsActual= actual.substring(0,3);
    	} else {
    		first3CharsActual=actual;
    	}

    	if (supplied.length() >= 3) {
    		first3CharsSupplied= supplied.substring(0,3);
    	} else {
    		first3CharsSupplied=supplied;
    	}

    	return first3CharsActual.equalsIgnoreCase(first3CharsSupplied);
    }

	private boolean verifyAccountNumberField() {
		DataCollectionField billerAccountNumberField = toolkit.getScreenField(FieldKey.BILLER_ACCOUNTNUMBER_KEY);
		if (transaction.getReceiveDetail() != null) {
			if (billerAccountNumberField == null && transaction.getReceiveDetail().getBillerAccountNumber() != null) {
				billerAccountNumberField = new DataCollectionField(FieldKey.BILLER_ACCOUNTNUMBER_KEY);
				billerAccountNumberField.setDataComponent(new javax.swing.JTextField());
				String billerAccountNumber = StringUtility
						.getNonNullString(this.transaction.getReceiveDetail().getBillerAccountNumber());
				billerAccountNumberField.setValue(billerAccountNumber);
			}
		}
		DataCollectionField validateAccountNumberField = toolkit.getScreenField(FieldKey.VALIDATE_ACCOUNT_NBR_KEY);
		
		if ((billerAccountNumberField == null) || (validateAccountNumberField == null)) {
			return false;
		}

		String value1 = billerAccountNumberField.getValue();
		String value2 = validateAccountNumberField.getValue();

		if ((value1.equals(value2)) && (! value1.equals(""))) {
			return true;
		}

		return false;
	}
}
