package dw.billpayment;

import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import com.moneygram.agentconnect.BillerSearchType;

import dw.dialogs.Dialogs;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.DataCollectionSet;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.framework.DataCollectionSet.DataCollectionType;
import dw.i18n.Messages;
import dw.mgsend.MoneyGramClientTransaction;
import dw.mgsend.MoneyGramSendFlowPage;
import dw.mgsend.MoneyGramSendTransaction;

/**
 * Page 1 of the Directory of Billers.  This page prompts the user for a 
 *   keyword/biller code OR a company name and industry to search by OR a 
 *   search by DMA industry. When next or industry button is pressed, the 
 *   matching convience payments will be requested from the host.
 */
public class BPWizardPage1 extends MoneyGramSendFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	private static int MIN_SCREEN_WIDTH = 425;
	private static int MAX_SCREEN_WIDTH = 575;
    
	private JTextField nameField;
	private DWTextPane searchPrompt;
	private DWTextPane screenInstructions;
	private DWTextPane screenInstructions1;
	private DWTextPane screenInstructions2;
	private DWTextPane screenInstructions3;
	private JScrollPane nameIndustryPanel;
	private int lastWidth = -1;
	
	public BPWizardPage1(MoneyGramSendTransaction tran, String name,
                          String pageCode, PageFlowButtons buttons) {
        super(tran, "billpayment/BPWizardPage1.xml", name, pageCode, buttons);	
        nameField = (JTextField) getComponent("nameField");	
        nameIndustryPanel = (JScrollPane) getComponent("nameIndustryPanel");
    }

	@Override
	public void dwTextPaneResized() {
		int width = nameIndustryPanel.getPreferredSize().width;
		if (width < MIN_SCREEN_WIDTH) {
			width = MIN_SCREEN_WIDTH;
		}
		if (width > MAX_SCREEN_WIDTH) {
			width = MAX_SCREEN_WIDTH;
		}

		if (width != this.lastWidth) {
			this.lastWidth = width;
		}
		final int panelWidth = width;

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				searchPrompt.setWidth(panelWidth);
				screenInstructions.setWidth(panelWidth);
				screenInstructions1.setWidth(panelWidth);
				screenInstructions2.setWidth(panelWidth);
				screenInstructions3.setWidth(panelWidth);
			}
		});
	}

    @Override
	public void start(int direction) {
    	
		flowButtons.setButtonsEnabled(true);
		
        if (direction == PageExitListener.NEXT) {
        	transaction.setDataCollectionData(new DataCollectionSet(DataCollectionType.VALIDATION));
        }

		searchPrompt = (DWTextPane) getComponent("searchPrompt");

    	// set up the Text Pane listener
		screenInstructions = (DWTextPane) getComponent("screenInstructions");
		screenInstructions1 = (DWTextPane) getComponent("screenInstructions1");
		screenInstructions2 = (DWTextPane) getComponent("screenInstructions2");
		screenInstructions3 = (DWTextPane) getComponent("screenInstructions3");

		screenInstructions.addListener(this, this);
    	        
        // set up the listeners for the search boxes and buttons
        addTextListener(nameField, this, "setBoxStates");	
        addActionListener("nameField",this,"doSearch");	 
        // start the focus order
        nameField.requestFocus();

        // disable the next button until some criteria is entered
        setBoxStates();
        
        dwTextPaneResized();
    }

    public void doSearch() {
    	if (nameField.hasFocus() && nameField.getText().length() == 0)
        	nameField.requestFocus();
        else
            flowButtons.getButton("next").doClick();	
    }

    public void setBoxStates() {
        setComponentEnabled(getComponent("nameIndustryPanel"), true, true);	
        // keep next button disabled unless there is something to search for
        flowButtons.setEnabled("next",	
             (nameField.getText().length() > 0));
    	if (transaction.isDirectoryOfBillers()) 
            flowButtons.setEnabled("back", false);
    	else
    		flowButtons.setEnabled("back", true);
        flowButtons.setEnabled("expert",false);	
        flowButtons.setVisible("expert",false);	
    }

    @Override
	public void finish(int direction) {
        flowButtons.setButtonsEnabled(false);
        transaction.getDataCollectionData().setAfterValidationAttempt(false);        
        
        if (direction == PageExitListener.NEXT) {
        	if (nameField.getText().trim().length() > 0) { 
                // search for the companies with the given name/city
              JTextField[] searchFields = {nameField};
                if (! validateFields(searchFields)) {
                    return;
                }
                
                transaction.searchForBillers(BillerSearchType.NAME, null, 
                		null,
                		null, nameField.getText(),
                		null, null, this);
            }
            else {
                transaction.searchForBillers(BillerSearchType.IND, null,
                		transaction.getSelectedBillerSearchButton(),null,
                		null, null, null, this);            	
            }
        }
        else {
            PageNotification.notifyExitListeners(this, direction);
        }
    }

    @Override
	public void commComplete(int commTag, Object returnObj) {
        boolean b = ((Boolean) returnObj).booleanValue();
        switch (commTag) {
            case MoneyGramClientTransaction.BILLER_SEARCH :
                if (! b) {
                	if (! transaction.hasBillerData()) {
                		if (nameField != null  && nameField.getText().trim().length() > 0) {
   	                    Dialogs.showWarning(
    	                            "dialogs/DialogNoBillerDataFound.xml",
    	                            Messages.getString("DialogNoBillerDataFound.name1"),
    	                            Messages.getString("DialogNoBillerDataFound.name2"));
               			
                		}

                		
                	}
                    // start the focus order
                    nameField.requestFocus();
                    flowButtons.setButtonsEnabled(true);
                    setBoxStates();
                    return;
                }
                PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
                break;
        }
    }
}
