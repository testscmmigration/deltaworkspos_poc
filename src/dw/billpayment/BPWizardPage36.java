package dw.billpayment;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import com.moneygram.agentconnect.ProductVariantType;

import dw.dialogs.Dialogs;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.DataCollectionField;
import dw.framework.DataCollectionScreenToolkit;
import dw.framework.DataCollectionSet;
import dw.framework.FieldKey;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.mgsend.MoneyGramSendFlowPage;
import dw.mgsend.MoneyGramSendTransaction;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;
import dw.utility.ExtraDebug;

/**
 * Page 4 of the MoneyGram Send Wizard. This page displays the information about
 *   the currently selected sender for the user to verify, or prompts the user
 *   to enter all the information if this is not an existing sender.
 */
public class BPWizardPage36 extends MoneyGramSendFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	private JScrollPane scrollPane1;
	private JScrollPane scrollPane2;
	private DWTextPane text1;
	private JPanel panel2;
	private DataCollectionScreenToolkit toolkit = null;
	private Object dataCollectionSetChangeFlag;
	private JButton nextButton;
	
    public BPWizardPage36(MoneyGramSendTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "billpayment/BPWizardPage36.xml", name, pageCode, buttons);	

        scrollPane1 = (JScrollPane) getComponent("scrollPane1");
	    scrollPane2 = (JScrollPane) getComponent("scrollPane2");
	    text1 = (DWTextPane) getComponent("text1");
		panel2 = (JPanel) getComponent("panel2");
		nextButton = this.flowButtons.getButton("next");
    }

	@Override
	public void dwTextPaneResized() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				int width = scrollPane2.getVisibleRect().width;
				text1.setWidth(width);
			}
		});
	}
    
	private void setupComponents() {
		
		// Check if the GFFP information has changed.
		
		DataCollectionSet data = this.transaction.getDataCollectionData();

		if (this.dataCollectionSetChangeFlag != this.transaction.getDataCollectionData().getChangeFlag()) {
	    	this.dataCollectionSetChangeFlag = this.transaction.getDataCollectionData().getChangeFlag();
	    	
	    	// Rebuild the data collection items for the screen.
	    	
	    	ExtraDebug.println("Rebuilding Screen " + getPageCode() + " from FTC data");
	    	
			int flags = DataCollectionScreenToolkit.SHORT_LABEL_VALUES_FLAG + DataCollectionScreenToolkit.DISPLAY_REQUIRED_FLAG;
			JButton backButton = this.flowButtons.getButton("back");
			this.toolkit = new DataCollectionScreenToolkit(this, this.scrollPane1, this, this.scrollPane2, this.panel2, flags, transaction.getScreen36Status().getDataCollectionPanels(data), this.transaction, backButton, nextButton, null);
		}
		
		// Check if the screen needs to be laid out.
		
		if (transaction.getScreen36Status().needLayout(data)) {
			toolkit.populateDataCollectionScreen(panel2, transaction.getScreen36Status().getDataCollectionPanels(data), false);
		}
	}

    @Override
	public void start(int direction) {
    	setupComponents();
        
    	flowButtons.reset();
        flowButtons.setVisible("expert", false);	
        flowButtons.setEnabled("expert", false);	
        
        if (transaction.isNewCustomer()) {
            String text = Messages.getString("BPWizardPage36.enterLabel1");
            text1.setText(text);
        } else {
            String text = Messages.getString("BPWizardPage36.verifyLabel1");
            text1.setText(text);
        }
        
        DataCollectionField senderHomePhoneItem = toolkit.getScreenField(FieldKey.SENDER_PRIMARYPHONE_KEY);
        if (senderHomePhoneItem != null) {
            String number = transaction.getSender().getHomePhone();
	        if (((number == null) || (number.equals(""))) && (! transaction.isUsingCard())) { 
	            senderHomePhoneItem.setValue(transaction.getCardPhoneNumber());
	        } else {
	            senderHomePhoneItem.setValue(number);
	        }
        }

    	// If the flow direction is to the next screen or back to the previous
    	// screen, save the value of the current items in a data structure.

        if ((direction == PageExitListener.BACK) || (direction == PageExitListener.NEXT)) {
			DataCollectionScreenToolkit.restoreDataCollectionFields(this.toolkit.getScreenFieldList(), this.transaction.getDataCollectionData());
        }
		
		// Move the cursor to 1) the first required item with no value or item 
		// with an invalid value or 2) the first optional item with no value.
		
		this.toolkit.moveCursor(this.toolkit.getScreenFieldList(), nextButton);

        // Enable and call the resize listener.

    	text1.setListenerEnabled(true);
		toolkit.enableResize();
		toolkit.getComponentResizeListener().componentResized(null);
    }
    
    @Override
	public void finish(int direction) {
		toolkit.disableResize();

    	// If the flow direction is to the next screen, check the validity of the
    	// data. If a value is invalid or a required item does not have a value,
    	// return to the screen.
    	
    	if (direction == PageExitListener.NEXT) {
        	String tag = toolkit.validateData(toolkit.getScreenFieldList(), DataCollectionField.DISPLAY_ERRORS_VALIDATION);
        	if (tag != null) {
				flowButtons.setButtonsEnabled(true);
				toolkit.enableResize();
        		return;
        	}
    	}
    	
        if ((direction == PageExitListener.BACK) || (direction == PageExitListener.NEXT)) {
        	DataCollectionScreenToolkit.saveDataCollectionFields(toolkit.getScreenFieldList(), transaction.getDataCollectionData());
        }
    	
    	if (direction == PageExitListener.NEXT) {
            if ((transaction.getProductVariant().equals(ProductVariantType.EP)) && (transaction.getSenderFirstName() != null) && (transaction.getSenderFirstName().trim().length() != 0) && (! validateMSCardSenderName())) {
        		flowButtons.setButtonsEnabled(true);
        		return;
            }
            
			if (transaction.nextDataCollectionScreen(BillPaymentWizard.BPW36_PPC_SENDER_DETAIL).isEmpty() && transaction.getDataCollectionData().isAdditionalInformationScreenValid()) {
		  		transaction.bpValidation(this, ValidationType.SECONDARY);
		  		return;
		  	}
        }
    	PageNotification.notifyExitListeners(this, direction);
    }
    
	/**
	 * Called by transaction after it receives response from middleware.
	 * 
	 * @param commTag
	 *            is function index.
	 * @param returnValue
	 *            is Boolean set to true if response is good. Otherwise, it is
	 *            set to false.
	 */
	@Override
	public void commComplete(int commTag, Object returnValue) {
		boolean b = ((Boolean) returnValue).booleanValue();
		if (b)
			PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
		else {
			start(PageExitListener.NEXT);
		}
	}

	private boolean validateMSCardSenderName() {
		 
		// If no card number, assume name OK.
		
		if (transaction.getCardNumber().length() == 0) {
			return true;
		}

		// If we do not have a rewards name to check against, assume the 
		// name is valid, or we will never get a rewards Bill Payment trx into
		// the system.
		 
		String rewardsFname = transaction.getRewardsFirstName().trim();
		String rewardsLname = transaction.getRewardsLastName().trim();
		if ((rewardsFname.length() == 0) && (rewardsLname.length() == 0)) {
			return true;
		}
		
        DataCollectionField senderFirstNameItem = toolkit.getScreenField(FieldKey.SENDER_FIRSTNAME_KEY);
        DataCollectionField senderLastNameItem = toolkit.getScreenField(FieldKey.SENDER_LASTNAME_KEY);
		String suppliedFname = senderFirstNameItem.getValue().trim();
		String suppliedLname = senderLastNameItem.getValue().trim();
		if (transaction.isMatchingName(rewardsFname, suppliedFname) && transaction.isMatchingName(rewardsLname, suppliedLname)) {
			return true;
		}
		
		int rc = showDialog();
		if (rc == Dialogs.OK_OPTION) {
			transaction.setNewCustomer(true);
			transaction.setMoneySaverID("");
			transaction.getSender().setMoneySaverID("");
			return true;
		}

		return false;
	}
    
    private int showDialog(){
    	int option =-1;
    	option = Dialogs.showOkCancel("dialogs/DialogMSCardNameDoesNotMatchNoAutoEnrollEP.xml");  	    	
    	return option;	   	
    }
}
