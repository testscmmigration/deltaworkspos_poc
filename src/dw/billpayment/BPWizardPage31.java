package dw.billpayment;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;

import com.moneygram.agentconnect.BillerInfo;
import com.moneygram.agentconnect.BillerSearchType;
import com.moneygram.agentconnect.FeeType;

import dw.dialogs.Dialogs;
import dw.dwgui.BankCardSwipeTextField;
import dw.dwgui.DelimitedTextField;
import dw.dwgui.MoneyField;
import dw.framework.DataCollectionSet;
import dw.framework.FieldKey;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.framework.DataCollectionSet.DataCollectionType;
import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.mgsend.MoneyGramClientTransaction;
import dw.mgsend.MoneyGramSendFlowPage;
import dw.mgsend.MoneyGramSendTransaction;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.TimeUtility;

public class BPWizardPage31 extends MoneyGramSendFlowPage {
	private static final long serialVersionUID = 1L;

	
	private static final int CARD_EXP_FIELD_SIZE = 7;
	private static final int CARD_EXP_FIELD_DELIMITER_OFFSET = 2;
	private static final int CARD_MIN_ACCT_LEN = DWValues.BIN_SIZE;

	private BankCardSwipeTextField cardNumberField;
	private DelimitedTextField cardExpDateField;
	private MoneyField loadAmountField;
    private JLabel currencyLabel1;
    private	String cardExpDate;
	private List<BillerInfo> billers;
	private BillerInfo oldBiller;
	private int billerIdx;

    AbstractDocument doc;
    
    private class AmountKeyListener implements KeyListener {
    	
    	private static final long DISABLE_TIME = 1000;
    	private boolean disabled = false;
    	
		@Override
		public void keyTyped(KeyEvent e) {
			if (disabled) {
				e.consume();
			}
		}

		@Override
		public void keyPressed(KeyEvent e) {
			if (disabled) {
				e.consume();
			} else {
			
				char key = e.getKeyChar();
				
				if ((key == '%') || (key == ';')) {
					e.consume();
					
					synchronized(this) {
						disabled = true;
					}
					
					Thread t = new Thread() {
						@Override
						public void run() {
							try {
								Thread.sleep(DISABLE_TIME);
							} catch (Exception e) {
								Debug.printException(e);
							} finally {
								synchronized(this) {
									disabled = false;
								}
							}
						}
					};
					t.start();
				}
			}
		}

		@Override
		public void keyReleased(KeyEvent e) {
			if (disabled) {
				e.consume();
			}
		}
    }
    
    public BPWizardPage31   (
    		MoneyGramSendTransaction tran,
	        String name,
	        String pageCode,
	        PageFlowButtons buttons) {
        super(tran, "billpayment/BPWizardPage31.xml", name, pageCode, buttons);	
        cardNumberField = (BankCardSwipeTextField) getComponent("cardNumberField");
        cardExpDateField = (DelimitedTextField) getComponent("cardExpDateField");
        loadAmountField = (MoneyField) getComponent("loadAmountField");
        loadAmountField.addKeyListener(new AmountKeyListener());
        currencyLabel1 = (JLabel) getComponent("currencyLabel1");
        
        Document styledDoc = cardExpDateField.getDocument();
        if (styledDoc instanceof AbstractDocument) {
            doc = (AbstractDocument)styledDoc;
            doc.setDocumentFilter(new CardExpDateFilter());
        } 
    } 

    public class CardExpDateFilter extends DocumentFilter {

        public CardExpDateFilter() {
        }

        @Override
		public void insertString(FilterBypass fb, int offs,
                                 String str, AttributeSet a)
            throws BadLocationException {
        	int ii;
        	
        	int    textLen = fb.getDocument().getLength();
        	String text   = fb.getDocument().getText(0,textLen);
        	
//          	Debug.println("in CardExpDateFilter's insertString method["
//          			+ str + "]");
          	
        	/*
        	 * Strip out any bad characters.
        	 */
        	for (ii = str.length(); ii >= 0 ;ii--) {
        		if (!Character.isDigit(str.charAt(ii))) {
        			if ((str.charAt(ii) == '/') 
        					&& ii+offs == CARD_EXP_FIELD_DELIMITER_OFFSET) {
        				continue;
        			}
        			else {
        				if (ii < str.length()) {
            				str = str.substring(0, ii) + str.substring(ii+1); 
        				}
        				else {
            				str = str.substring(0, ii) ; 
        				}
        			}
        		}
        	}
        	
        	if (str.length() <= 0) {
        		return;
        	}
            
        	if ( (text.indexOf('/') < 0) && textLen == 2) {
                super.insertString(fb, offs, str + "/", a);
        	}
        	else {
                super.insertString(fb, offs, str, a);
        	}
        	
        }
        
        @Override
		public void replace(FilterBypass fb, int offs,
                            int length, 
                            String str, AttributeSet a)
            throws BadLocationException {
        	int ii;
        	
        	int    textLen = fb.getDocument().getLength();
        	String text   = fb.getDocument().getText(0,textLen);
        	int    textDelimiter = text.indexOf('/');

//          	Debug.println("in CardExpDateFilter's replace method["
//          			+ str + "]");
        	/*
        	 * Strip out any bad characters.
        	 */
        	for (ii = 0; ii < str.length() ;ii++) {
        		char ch = str.charAt(ii);
        		if ((!Character.isDigit(ch))&&(ch != '*')) {
        			if ((str.charAt(ii) == '/') 
        					&& ii+offs == CARD_EXP_FIELD_DELIMITER_OFFSET) {
        				continue;
        			}
        			else {
        				if (ii < str.length()) {
            				str = str.substring(0, ii) + str.substring(ii+1); 
        				}
        				else {
            				str = str.substring(0, ii) ; 
        				}
        			}
        		}
        	}
        	
        	if ( (textDelimiter < 0) && (str.indexOf('/') < 0) 
        			&& offs == 1) {
                super.replace(fb, offs, length, str + "/", a);
        	}
        	else if ( (textDelimiter < 0)  && (str.indexOf('/') < 0) 
        			&& offs == CARD_EXP_FIELD_DELIMITER_OFFSET) {
                super.replace(fb, offs, length, "/" + str , a);
        	}
        	else if ( (textDelimiter < CARD_EXP_FIELD_DELIMITER_OFFSET)  
        			&& (str.indexOf('/') < CARD_EXP_FIELD_DELIMITER_OFFSET)
        			&& offs  > 0) {
                super.replace(fb, 0, textLen, "0" + text + str , a);
        	}
        	else {
                super.replace(fb, offs, length, str, a);
        	}
        	
        	/*
        	 * Verify result
        	 */
        	textLen = fb.getDocument().getLength();
        	text    = fb.getDocument().getText(0,textLen);
        	textDelimiter = text.indexOf('/');
        	int textDelimiterLast = text.lastIndexOf('/');
        	boolean changed = false;
        	
        	/*
        	 * If string is big enough to need a delimiter and the delimiter is 
        	 * not in the right place, remove any existing delimiters and then
        	 * add one in the right place. 
        	 */
        	if ((textLen > CARD_EXP_FIELD_DELIMITER_OFFSET) 
        			&& ( (textDelimiter != CARD_EXP_FIELD_DELIMITER_OFFSET )
        			    ||(textDelimiterLast != CARD_EXP_FIELD_DELIMITER_OFFSET))) {
        		while (textDelimiter >= 0) {
        			text = text.substring(0, textDelimiter) 
        					+ text.substring(textDelimiter + 1);
        			textDelimiter = text.indexOf('/');
        		}
      			text = text.substring(0, CARD_EXP_FIELD_DELIMITER_OFFSET)
        					+"/" + text.substring(CARD_EXP_FIELD_DELIMITER_OFFSET);
      			changed = true;
        	}
        	
        	if (textLen > CARD_EXP_FIELD_SIZE || changed) {
        		if (text.length() > CARD_EXP_FIELD_SIZE) {
                    super.replace(fb, 0, textLen, 
                    		text.substring(0, CARD_EXP_FIELD_SIZE), a);
        		}
        		else {
                    super.replace(fb, 0, textLen, text, a);
        		}
        	}
        }
    }
    
	private void copyBillerInfoData(BillerInfo fromBillerInfo, BillerInfo toBillerInfo) {
		toBillerInfo.setReceiveAgentID(fromBillerInfo.getReceiveAgentID());
		toBillerInfo.setReceiveCode(fromBillerInfo.getReceiveCode());
		toBillerInfo.setInternationalBankAccountNumber(fromBillerInfo.getInternationalBankAccountNumber());
		toBillerInfo.setBillerGroupID(fromBillerInfo.getBillerGroupID());
		toBillerInfo.setBillerGroupName(fromBillerInfo.getBillerGroupName());
		toBillerInfo.setBillerName(fromBillerInfo.getBillerName());
		toBillerInfo.setAddress1(fromBillerInfo.getAddress1());
		toBillerInfo.setAddress2(fromBillerInfo.getAddress2());
		toBillerInfo.setAddress3(fromBillerInfo.getAddress3());
		toBillerInfo.setBillerCity(fromBillerInfo.getBillerCity());
		toBillerInfo.setBillerState(fromBillerInfo.getBillerState());
		toBillerInfo.setServiceOfferingID(fromBillerInfo.getServiceOfferingID());
		toBillerInfo.setServiceOffering(fromBillerInfo.getServiceOffering());
		toBillerInfo.setServiceOfferingSecondary(fromBillerInfo.getServiceOfferingSecondary());
		toBillerInfo.setServiceOfferingShort(fromBillerInfo.getServiceOfferingShort());
		toBillerInfo.setServiceOfferingShortSecondary(fromBillerInfo.getServiceOfferingShortSecondary());
		toBillerInfo.getIndustryID().addAll(fromBillerInfo.getIndustryID());
		toBillerInfo.setExpectedPostingTimeFrame(fromBillerInfo.getExpectedPostingTimeFrame());
		toBillerInfo.setExpectedPostingTimeFrameSecondary(fromBillerInfo.getExpectedPostingTimeFrameSecondary());
		toBillerInfo.setBillerNotes(fromBillerInfo.getBillerNotes());
		toBillerInfo.setBillerNotesSecondary(fromBillerInfo.getBillerNotesSecondary());
		toBillerInfo.setSecondaryLang(fromBillerInfo.getSecondaryLang());
		toBillerInfo.setProductVariant(fromBillerInfo.getProductVariant());
		toBillerInfo.setBillerCutoffTime(fromBillerInfo.getBillerCutoffTime());
		toBillerInfo.setBillerWebsite(fromBillerInfo.getBillerWebsite());
		toBillerInfo.setBillerPhoneNumber(fromBillerInfo.getBillerPhoneNumber());
		toBillerInfo.setMaskAccountNumber(fromBillerInfo.isMaskAccountNumber());
		toBillerInfo.setNationalBillerFlag(fromBillerInfo.isNationalBillerFlag());
		toBillerInfo.setUseStandardUBPFields(fromBillerInfo.isUseStandardUBPFields());
		toBillerInfo.setAcctNumberNumericOnly(fromBillerInfo.isAcctNumberNumericOnly());
		toBillerInfo.setSendFixedAmountFlag(fromBillerInfo.isSendFixedAmountFlag());
		toBillerInfo.setCancelWarnFlag(fromBillerInfo.isCancelWarnFlag());
		toBillerInfo.setRefundWarnFlag(fromBillerInfo.isRefundWarnFlag());
		toBillerInfo.setDoubleAcctNumberEntryFlag(fromBillerInfo.isDoubleAcctNumberEntryFlag());
		toBillerInfo.setMinimumFeeAmt(fromBillerInfo.getMinimumFeeAmt());
		toBillerInfo.setFeeType(fromBillerInfo.getFeeType());
		toBillerInfo.setConsolidatorLocationId(fromBillerInfo.getConsolidatorLocationId());
		toBillerInfo.setConsolidatorName(fromBillerInfo.getConsolidatorName());
		toBillerInfo.setClassOfTradeCode(fromBillerInfo.getClassOfTradeCode());
		toBillerInfo.setExpeditedEligibleFlag(fromBillerInfo.isExpeditedEligibleFlag());
	}    
    
    @Override
	public void start(int direction) {
    	boolean ppcStart = transaction.getTranEntryType()
    	                   == MoneyGramSendTransaction.TRAN_ENTRY_PPC;
    	   	   	
        flowButtons.reset();
        flowButtons.setVisible("expert", false);	
        flowButtons.setEnabled("expert", false);
        flowButtons.setEnabled("back", !ppcStart);

        if (direction == PageExitListener.NEXT) {
        	transaction.setDataCollectionData(new DataCollectionSet(DataCollectionType.VALIDATION));
        }

        /*
         * If we came back to this screen, do not treat as a new customer
         * no matter what, and do not go back, since we have done a new
         * search so the results from the UBP flow are not longer available.
         */
        if (direction != PageExitListener.BACK) {
            transaction.setNewCustomer(ppcStart);
            transaction.getDataCollectionData().getCurrentValueMap().clear();
        } else {
	        flowButtons.setEnabled("back", false);
        }
        /*
         * If this transaction started as a non Prepaid Card load, save the 
         * biller info in case the user wants to go back to the previous
         * screen.
         */
        if (!ppcStart) {
        	 oldBiller = new BillerInfo();
        	 copyBillerInfoData(transaction.getBillerInfo(), oldBiller);
        }
		transaction.clearReceiver();
		transaction.clearBiller();
    	cardNumberField.setText("");
        addFocusGainedListener("cardNumberField", this, "cardNumberInit");
        addFocusLostListener("cardNumberField", this, "storeAndHide");
        cardNumberField.setValidChars("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ -/");
        cardNumberField.setEditable(true);
        cardNumberField.setEnabled(true);
        cardNumberField.requestFocus();
        
    	cardExpDateField.setText("");
    	cardExpDateField.setEditable(false);
    	cardExpDateField.setFocusable(false);
        cardExpDateField.setEditable(true);
        cardExpDateField.setEnabled(false);
    	
     	loadAmountField.setText(transaction.getAmount().toString());
     	loadAmountField.setEditable(false);
     	loadAmountField.setEnabled(false);

        currencyLabel1.setText(transaction.agentBaseCurrency());
                
        addActionListener("cardNumberField", this);	
        addActionListener("loadAmountField", this);
        addTextListener("cardNumberField", this, "cardNumberListener");	
//        addTextListener("cardExpDateField", this, "cardExpDateListener");
        
    }
   
    public void cardNumberInit() {
    	cardNumberField.reset();
    }
    
    public void storeAndHide() {
		Debug.println("storeAndHide");
		String acctNbr = cardNumberField.getField(0);
		String additionalData = cardNumberField.getField(2);

    	/*
    	 * If the Card Number is not a number, assume a bad swipe that did
    	 * not get recognized as a swipe
    	 */
    	if ((acctNbr != null) && !isNumber(acctNbr)) {
       		badCardSwipe();
    		return;
    	}
    	
		/*
		 * If card swipe and data
		 */
		if ((acctNbr != null) && (! acctNbr.isEmpty())) {
	    	/*
	    	 * If the Card Number is not big enough to be a BIN, then bad
	    	 * card swipe
	    	 */
	    	if (acctNbr.length() < CARD_MIN_ACCT_LEN) {
	    		badCardSwipe();
	    	}
	    	
			cardExpDateField.setText("");
			/*
			 * If enough additional data is available to be an expiration date,
			 * format it and display it.
			 */
			try {
				if ((additionalData != null) && (additionalData.length() >= 4) && (Integer.parseInt(additionalData.substring(0, 4)) > 0)) {
					
					this.cardNumberField.setEnabled(false);
					
					cardExpDateField.setText("**/****");
					cardExpDateField.setEditable(false);
					cardExpDateField.setEnabled(false);
					//		        cardExpDateField.setBackground(SystemColor.control);
					loadAmountField.setEditable(true);
					loadAmountField.setEnabled(true);
					loadAmountField.requestFocus();
				}
				else {
					badCardSwipe();
				}
			} catch (NumberFormatException e) {
				badCardSwipe();
			}
		}
	}
    
    public void cardNumberListener() {
		/*
		 * If swipe is done, move on
		 */
    	if (cardNumberField.isSwipeDone()){
    		Debug.println("cardNumberListener - Call storeAndHide");
    		storeAndHide();
    	}
    }
    
//    public void cardExpDateListener() {
//    	boolean enableState = (cardExpDateField.getText().length() >= 7);
//        setComponentEnabled(loadAmountField, enableState);
//		loadAmountField.setEditable(enableState);
//		loadAmountField.setEnabled(enableState);
//    }
    
    public void cardExpDateFieldAction() {
    	String text = cardExpDateField.getText();
    	int    length = text.length();
    	
    	if (length == 0) {
    		return;
    	} else if (!Character.isDigit(text.charAt(length-1))) {
    		if (length == 1) {
        		cardExpDateField.setText("");
    		}
    		else {
        		cardExpDateField.setText(text.substring(0, length-1));
    		}
    	} 
    	if ( (text.indexOf('/') < 0) && length == 2) {
    		cardExpDateField.setText(text + "/");
    	} 
    }
       
    public void cardNumberFieldAction() {
		long cTime = TimeUtility.currentTimeMillis();
		/*
		 * Wait to make sure card swipe has finished (or 1 second has passed)
		 */
		while (! cardNumberField.isSwipeDone() ) {
			if (TimeUtility.currentTimeMillis() - cTime > 1000) {
				break;
			}
		}
   		Debug.println("cardNumberFieldAction - Call storeAndHide");
   		storeAndHide();
    }

    public void loadAmountFieldAction() {
    	flowButtons.getButton("next").doClick();	
    }
    
    private static String LastDayOfMonth(int pm_iMonth, int pm_iYear) {

        String numDays="0";

        switch (pm_iMonth) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                numDays = "31";
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                numDays = "30";
                break;
            case 2:
                if ( ((pm_iYear % 4 == 0) && !(pm_iYear % 100 == 0))
                     || (pm_iYear % 400 == 0) )
                    numDays = "29";
                else
                    numDays = "28";
                break;
            default:
                break;
        }
        return numDays;
    }

    private void badCardSwipe(){
    	Debug.println("badCardSwipe");
    	Toolkit.getDefaultToolkit().beep();
    	SwingUtilities.invokeLater(new Runnable() {@Override
		public void run() {
    		long cTime = TimeUtility.currentTimeMillis();
    		/*
    		 * Wait to make sure card swipe has finished (or 1 second has passed)
    		 */
    		while (!cardNumberField.isSwipeDone()) {
    			if (TimeUtility.currentTimeMillis() - cTime > 1000) {
    				break;
    			}
    		}
        	cardExpDateField.setText("");

        	cardNumberField.setText("");
        	cardNumberField.reset();
        	cardNumberField.setEditable(true);
        	cardNumberField.setEnabled(true); 
			cardNumberField.requestFocus();
    		Dialogs.showWarning("dialogs/DialogBadCardSwipe.xml"); 
    	}});
    }
   
    private static final boolean isNumber(final String s)
    {
    	for (int x = 0; x < s.length(); x++) {
    		final char c = s.charAt(x);
    		if ((c >= '0') && (c <= '9')) continue;  // 0 - 9
    		return false; // invalid
    	}
    	return true; // valid
    }

    private boolean doneWithBillerList() {
    	billers = transaction.filterPpcBillerInfoList(DWValues.PPC_CLASS_OF_TRADE);
		/*
		 * All returned billers must have been filtered out because
		 * of bad Class Of Trade.
		 */
		if (billers.size() == 0) {
			Dialogs.showWarning(
					"dialogs/DialogNoBillerDataFound.xml",
					Messages.getString("DialogNoBillerDataFound.card1"),
					Messages.getString("DialogNoBillerDataFound.card2"));
			start(PageExitListener.NEXT);
			return false;
		}
		if (billers.size() == 1) {
			transaction.setBillerInfo(billers.get(0));
			transaction.setProductVariant(billers.get(0).getProductVariant());
			billerIdx = 0;
			/*
			 * Get the 'real' fee now, since we are skipping the 
			 * selection screen with only 1 biller.
			 */
			transaction.getFeeCalculation(null, this);
			return false;
		}
    	return true;
    }
    
    
    private boolean validateScreenEntries() {
    	
    	BigDecimal loadAmount = new BigDecimal(loadAmountField.getText());   	   	
    	String cardNumber = String.valueOf(cardNumberField.getField(0));  
    	Date expDate;
    	Date currDate = TimeUtility.currentTime();
    	
		String cardEDate = cardNumberField.getField(2);
		if ((cardEDate == null) || (cardEDate.length() < 4)) {
    		badCardSwipe();
			return false;
		}
		cardExpDate = cardEDate.substring(2,4)+"/20"+ cardEDate.substring(0,2); 
    	
    	if (cardExpDate.length() < 7) {
    		expDate = null;
    	}
    	else {
    		try {
            	expDate = FormatSymbols
        		.convertTextToDate(cardExpDate.substring(0,3)
        				+ LastDayOfMonth(Integer.parseInt(cardExpDate.substring(0,2)),
        						Integer.parseInt(cardExpDate.substring(3))) 
        				+ cardExpDate.substring(2), true, Locale.US);
			} 
    		/*
    		 *  If the expiration date is not a number, assume a bad swipe that did
        	 *  not get recognized as a swipe  
    		 */
   		    catch (NumberFormatException e) {
				badCardSwipe();
				return false;
			}
    	}
    	
    	if (cardNumber.length() < CARD_MIN_ACCT_LEN) { 
			Toolkit.getDefaultToolkit().beep();
			Dialogs.showWarning("dialogs/DialogInvalidReloadCardNumber1.xml"); 
        	cardNumberField.setText("");
        	cardNumberField.setEditable(true);
        	cardNumberField.setEnabled(true); 
			cardNumberField.requestFocus();
			return false;
    	}
    	else if (cardExpDate.length() < 7) {
			Toolkit.getDefaultToolkit().beep();
			cardExpDateField.requestFocus();
			cardExpDateField.selectAll();
			Dialogs.showWarning("dialogs/DialogInvalidData.xml", 
					Messages.getString("BPWizardPage31.invalidDate"));
			flowButtons.setButtonsEnabled(true);
			return false;
		}
    	else if (FormatSymbols.invalidDateString) {
			Toolkit.getDefaultToolkit().beep();
			if (cardExpDateField.isEnabled()) {
				cardExpDateField.requestFocus();
				cardExpDateField.selectAll();
			} else {
	        	cardNumberField.setText("");
	        	cardNumberField.setEditable(true);
	        	cardNumberField.setEnabled(true); 
				cardNumberField.requestFocus();
			}
			Dialogs.showWarning("dialogs/DialogInvalidData.xml", 
					Messages.getString("BPWizardPage31.invalidDate"));
			flowButtons.setButtonsEnabled(true);
			return false;
		}
    	else if (expDate.compareTo(currDate) <= 0){
			Toolkit.getDefaultToolkit().beep();
			if (cardExpDateField.isEnabled()) {
				cardExpDateField.requestFocus();
				cardExpDateField.selectAll();
			} else {
	        	cardNumberField.setText("");
	        	cardNumberField.setEditable(true);
	        	cardNumberField.setEnabled(true); 
				cardNumberField.requestFocus();
			}
			Dialogs.showWarning("dialogs/DialogInvalidData.xml", 
					Messages.getString("BPWizardPage31.oldDate"));
			flowButtons.setButtonsEnabled(true);
			return false;
		
    	} else if (loadAmount.compareTo(BigDecimal.ZERO) <= 0) {
			Toolkit.getDefaultToolkit().beep();
			loadAmountField.requestFocus();
			loadAmountField.selectAll();
    		return false;
    	}
    	else if (loadAmount.compareTo(transaction.getMaxItemAmount()) == 1) {
			Toolkit.getDefaultToolkit().beep();
			Dialogs.showWarning("dialogs/DialogInvalidMaxReloadAmount.xml", 
					transaction.getMaxItemAmount().toString(),
					"(" + transaction.agentBaseCurrency() + ").");
			loadAmountField.requestFocus();
			loadAmountField.selectAll();
    		return false;
    	}
    	else if (!setAmount(loadAmount)) {
			if (transaction.isManagerOverrideFailed()) {
				PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
				return false;
			} else {
				loadAmountField.requestFocus();
				loadAmountField.selectAll();
				flowButtons.setButtonsEnabled(true);
				return false;
			}
		} else {
    		return true;
    	}
    }
    
    @Override
	public void finish(int direction) {
       	
        transaction.getDataCollectionData().setAfterValidationAttempt(false);        
    	if (direction == PageExitListener.NEXT) {
    		if ( ! validateScreenEntries()) {
    	        flowButtons.reset();
    	        flowButtons.setVisible("expert", false);	
    	        flowButtons.setEnabled("expert", false);
    	        flowButtons.setEnabled("back", 
    	        		transaction.getTranEntryType() 
    	        		!= MoneyGramSendTransaction.TRAN_ENTRY_PPC);
    			return;
    		} else {
				
				transaction.setBpAccountNumber(String.valueOf(cardNumberField.getField(0)));
				transaction.setAmount(new BigDecimal(loadAmountField.getText()));
				transaction.setBankCardSwiped(true);
				transaction.getDataCollectionData().getCurrentValueMap().put(FieldKey.CARD_EXPIRATION_MONTH_KEY.getInfoKey(), cardExpDate.substring(0,2));
				transaction.getDataCollectionData().getCurrentValueMap().put(FieldKey.CARD_EXPIRATION_YEAR_KEY.getInfoKey(), cardExpDate.substring(3));
				handleName();
				
//				Debug.println("Account Nbr["+transaction.getBpAccountNumber()+"]");				
//				Debug.println("ExpDate["+transaction.getBankCardExpDateMonth()+
//						"/"+transaction.getBankCardExpDateYear()+"]");				
//				Debug.println("Amount["+transaction.getAmount()+"]");				
//				Debug.println("Swiped?["+transaction.getBankCardSwiped()+"]");				
//				Debug.println("Sender Last Name["+transaction.getBpSenderLastName()
//						+"], "
//						+"Sender First Name["+transaction.getBpSenderFirstName()+"], "				
//						+"Sender Middle Name["+transaction.getBpSenderMiddleName()+"]");	
			   transaction.setTempCard(false);
               transaction.searchForBillers(BillerSearchType.BIN, null,
            		   	transaction.getSelectedBillerSearchButton(),null,
               			null, null, transaction.getBpAccountNumber().substring(
               					0, DWValues.BIN_SIZE), this);            	
			}
    	} else {
    		/*
    		 * Restore the old biller information
    		 */
    		if (direction == PageExitListener.BACK) {
        		copyBillerInfoData(oldBiller, transaction.getBillerInfo());
    		}
    		PageNotification.notifyExitListeners(this, direction);
    	}
    }
    
    @Override
	public void commComplete(int commTag, Object returnValue) {

		boolean b = ((Boolean) returnValue).booleanValue();

		if (commTag == MoneyGramClientTransaction.BILLER_SEARCH) {
			if (b) {
				billers = transaction.getFullBillerInfoList();
				if (! doneWithBillerList()) {
					return;
				}
			} else {
				if (! transaction.hasBillerData() ) {
					Dialogs.showWarning(
									"dialogs/DialogNoBillerDataFound.xml",
									Messages.getString("DialogNoBillerDataFound.card1"),
									Messages.getString("DialogNoBillerDataFound.card2"));
					PageNotification.notifyExitListeners(this,
							PageExitListener.NEXT);
					return;
				} else {
					start(PageExitListener.NEXT);
					return;
				}
			}
		
			/*
		 * Determine if temporary/permanent card and set expedited eligible 
		 * appropriately, then move on to next screen.
		 */

		} else if (commTag == MoneyGramClientTransaction.BP_VALIDATE) {
			if (!b) {
				transaction.setTempCard(true);
			}
			PageNotification.notifyExitListeners(this,
					PageExitListener.NEXT);
			return;
	    
		} else if (commTag == MoneyGramClientTransaction.FEE_LOOKUP) {
			if (b) {
				billers.get(billerIdx).setFeeType(FeeType.EXACT);
				if (transaction.getFeeSendTotalWithTaxBp(true) != null) {
					billers.get(billerIdx).setMinimumFeeAmt(transaction.getFeeSendTotalWithTaxBp(true));
				} else {
					billers.get(billerIdx).setMinimumFeeAmt(BigDecimal.ZERO);
				}
			} else {
				billers.get(billerIdx).setFeeType(FeeType.NOT_FOUND);
			}
		} 
		
		/*
		 * Check returned billers for any fee types that were found, but 
		 * were not exact, for these the fee needs to be calculated based on
		 * the actual transaction amount (fee lookup request).
		 */

		for (int ii = 0; ii < billers.size(); ii++) {		
			if ((billers.get(ii).getFeeType().compareTo(FeeType.EXACT) != 0) && (billers.get(ii).getFeeType().compareTo(FeeType.NOT_FOUND) != 0)) {
				billerIdx = ii;
				transaction.setBillerInfo(billers.get(ii));
				transaction.setProductVariant(billers.get(ii).getProductVariant());
				transaction.getFeeCalculation(null, this);
				return;
			}
		}
		PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
	}
    
	private void handleName() {
		String name = cardNumberField.getField(1);
		/*
		 * If a name is available, parse out the First, Middle and Last name.
		 */
		if (name != null) {
			String lastName = "";
			String firstName = "";
			String middleName = "";

			int nameLength = name.length();

			if (nameLength > 0) {
				int nameSep = name.indexOf('/');
				if (nameSep == -1) {
					nameSep = nameLength;
				} 
				lastName = name.substring(0, nameSep).trim();
				/*
				 * If name separator + 1 (since it is an index starting at 0) is
				 * equal or greater than the field length, there is no room for
				 * a first or middle name, so examine the last name, to see if 
				 * that might contain a first (and middle) name.
				 */
				if ((nameSep+1) < nameLength) {
					int nameSep2 = nameSep + name.substring(nameSep).indexOf(' ');
					if (nameSep2 < nameSep) {
						nameSep2 = nameLength;
					}
					firstName = name.substring(nameSep + 1, nameSep2);
					/*
					 * If second name separator is less than the
					 * field length, parse out the middle name.
					 */
					if (nameSep2 < nameLength) {
						middleName = name.substring(nameSep2 + 1).trim();
						int spaceIdx = middleName.indexOf(' ');
						if (spaceIdx != -1) {
							middleName = middleName.substring(0, spaceIdx);
						}
					}
				} else {
					/*
					 * Look to see
					 */
					int spaceIdx = lastName.indexOf(' ');
					if (spaceIdx != -1) {
						firstName = lastName.substring(0, spaceIdx);
						lastName = lastName.substring(spaceIdx).trim();
						/*
						 * look for a middle name
						 */
						spaceIdx = lastName.indexOf(' ');
						if (spaceIdx != -1) {
							middleName = lastName.substring(0, spaceIdx);
							lastName = lastName.substring(spaceIdx).trim();
							/*
							 * Check for (and strip off) any suffixes Jr. etc
							 */
							spaceIdx = lastName.indexOf(' ');
							if (spaceIdx != -1) {
								lastName = lastName.substring(0, spaceIdx);
							}
						}
					}
				}
				/*
				 * For now, limit middle name to 1 character.
				 */
				if (middleName.length() > 1) {
					middleName = middleName.substring(0, 1);	
				}

				transaction.getDataCollectionData().getCurrentValueMap().put(FieldKey.SENDER_FIRSTNAME_KEY.getInfoKey(), firstName);
				transaction.getDataCollectionData().getCurrentValueMap().put(FieldKey.SENDER_MIDDLENAME_KEY.getInfoKey(), middleName);
				transaction.getDataCollectionData().getCurrentValueMap().put(FieldKey.SENDER_LASTNAME_KEY.getInfoKey(), lastName);
				transaction.setDataFromHistory(true);
			}
		}
	}
}
