package dw.billpayment;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.moneygram.agentconnect.SessionType;

import dw.dialogs.Dialogs;
import dw.dwgui.CardSwipeTextField;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.DataCollectionSet;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.framework.DataCollectionSet.DataCollectionType;
import dw.i18n.Messages;
import dw.mgsend.MoneyGramClientTransaction;
import dw.mgsend.MoneyGramSendFlowPage;
import dw.mgsend.MoneyGramSendTransaction;

/**
 * Page 1 of the MoneyGram BillPayment Wizard. This page prompts the user for the
 *   MoneySaver card from the customer. If the field is left blank, they will
 *   continue and the home phone page will be shown.
 * Modified code to show the associate discount instructions to user depending uopn profile option
 */
public class BPWizardPage5 extends MoneyGramSendFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;
	
    private JTextField acctNumberField;
    private CardSwipeTextField cardNumberField;
    private JTextField phoneNumberField;
    private JPanel cardPanel;
    private JPanel phoneNumberPanel;
    private JPanel acctNumberPanel;
    private boolean acctNumberLookupEnabled;
    private boolean inMoneySaverProgram;
    private boolean phoneNumberLookupEnabled;
    private DWTextPane message1;
    private JLabel acctOrLabel;
    private JLabel phoneOrLabel;

    public BPWizardPage5(
        MoneyGramSendTransaction tran,
        String name,
        String pageCode,
        PageFlowButtons buttons) {
        super(tran, "billpayment/BPWizardPage5.xml", name, pageCode, buttons);	
        cardNumberField = (CardSwipeTextField) getComponent("cardNumberField");	
        phoneNumberPanel = (JPanel) getComponent("phoneNumberPanel");
        phoneNumberField = (JTextField) getComponent("phoneNumberField"); 
        cardPanel = (JPanel) getComponent("cardNumberPanel");	
        acctNumberPanel = (JPanel) getComponent("acctNumberPanel");
        acctNumberField = (JTextField) getComponent("acctNumberField"); 
        message1 = (DWTextPane)getComponent("message1");
        acctOrLabel = (JLabel)getComponent("acctOrLabel");
        phoneOrLabel = (JLabel)getComponent("orLabel");
        message1.addListener(this, this);
        message1.setListenerEnabled(false);
    }

    @Override
	public void start(int direction) {

       if (direction == PageExitListener.NEXT) {
     	   transaction.clear(true);
    	   transaction.setDataCollectionData(new DataCollectionSet(DataCollectionType.VALIDATION));
       }
       transaction.setManagerOverrideID(-1);
       transaction.setMaxEmployeeLimitExceeded(false);

       acctNumberLookupEnabled = transaction.isAcctNumberLookupEnabled();
       inMoneySaverProgram = transaction.isMoneySaverEnabled();
       phoneNumberLookupEnabled = transaction.isPhoneNumberLookupEnabled();

       transaction.setDataFromHistory(false);

       message1.setVisible(false);
       acctOrLabel.setVisible(false);
       acctNumberPanel.setVisible(false);
       
	   cardPanel.setVisible(false);
	   phoneOrLabel.setVisible(false);
	   phoneNumberPanel.setVisible(false);
       acctOrLabel.setVisible(false);
	   acctNumberPanel.setVisible(false);
	   
       if (acctNumberLookupEnabled && inMoneySaverProgram && phoneNumberLookupEnabled) {
		   message1.setText(Messages.getString("MGSWizardPage1.ARP1")); 
    	   message1.setVisible(true);
    	   cardPanel.setVisible(true);
    	   phoneOrLabel.setVisible(true);
    	   phoneNumberPanel.setVisible(true);
           acctOrLabel.setVisible(true);
    	   acctNumberPanel.setVisible(true);
    	   
       } else if (acctNumberLookupEnabled && inMoneySaverProgram) {
    	   message1.setText(Messages.getString("MGSWizardPage1.AR"));
    	   message1.setVisible(true);
    	   cardPanel.setVisible(true);
           acctOrLabel.setVisible(true);
    	   acctNumberPanel.setVisible(true);
    	   
       } else if (acctNumberLookupEnabled && phoneNumberLookupEnabled) {
    	   message1.setText(Messages.getString("MGSWizardPage1.AP"));
    	   message1.setVisible(true);
    	   phoneNumberPanel.setVisible(true);
           acctOrLabel.setVisible(true);
    	   acctNumberPanel.setVisible(true);
    	   
       } else if (inMoneySaverProgram && phoneNumberLookupEnabled) {
    	   message1.setText(Messages.getString("MGSWizardPage1.1128"));
    	   message1.setVisible(true);
    	   phoneNumberPanel.setVisible(true);
    	   cardPanel.setVisible(true);
    	   phoneOrLabel.setVisible(true);
    	   phoneNumberPanel.setVisible(true);
    	   
       } else if (acctNumberLookupEnabled) {
    	   message1.setText(Messages.getString("MGSWizardPage1.ACCT"));
    	   message1.setVisible(true);
    	   acctNumberPanel.setVisible(true);
    	   
       } else if (inMoneySaverProgram) {
    	   message1.setText(Messages.getString("MGSWizardPage1.1128f"));
    	   message1.setVisible(true);
    	   cardPanel.setVisible(true);
    	   
       } else if (phoneNumberLookupEnabled) {
    	   message1.setText(Messages.getString("MGSWizardPage1.1128e"));
    	   message1.setVisible(true);
    	   phoneNumberPanel.setVisible(true);
    	   
       } else {
    	   if (direction == PageExitListener.NEXT) {
               PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
    	   } else {
               PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
    	   }
           return;
       }

       if (direction == PageExitListener.NEXT || direction == PageExitListener.BACK) {
            setFieldStates();
            
            DataCollectionSet data = transaction.getDataCollectionData();
            if (data != null) {
            	data.getFieldValueMap().clear();
            }
        }

        flowButtons.reset();
        if (!transaction.isFeeQuery() && !transaction.isDirectoryOfBillers()) {
            flowButtons.setEnabled("back", false);	
    	}
        flowButtons.setEnabled("expert", false);	
        flowButtons.setVisible("expert", false);	

        if (acctNumberPanel.isEnabled()){
        	acctNumberField.requestFocus();
        } else if (cardPanel.isEnabled()){
        	cardNumberField.requestFocus();
        } else if (phoneNumberPanel.isEnabled()){
            phoneNumberField.requestFocus();
        }

        if (transaction.getCardPhoneNumber() != null) {
        	// field is also populated with an empty string
            if (phoneNumberField.isEnabled()) {
                phoneNumberField.setText(transaction.getCardPhoneNumber());
            } else {
                cardNumberField.setText(transaction.getCardPhoneNumber());
            }
        	transaction.setBpAccountNumber("");
        }

        addActionListener("acctNumberField", this); 
        addActionListener("cardNumberField", this);	
        addActionListener("phoneNumberField", this); 
        addTextListener("acctNumberField", this, "setFieldStates");	 
        addTextListener("cardNumberField", this, "setFieldStates");	 
        addTextListener("phoneNumberField", this, "setFieldStates");  
        
        message1.setListenerEnabled(true);
    }

    public void acctNumberFieldAction() {
        if (cardNumberField.isEnabled()){
        	cardNumberField.requestFocus();
        } else if (phoneNumberField.isEnabled()){
        	phoneNumberField.requestFocus();
        } else{
        	flowButtons.getButton("next").doClick();	
        }
    }

    public void cardNumberFieldAction() {
        if (phoneNumberField.isEnabled()){
        	phoneNumberField.requestFocus();
        } else{
        	flowButtons.getButton("next").doClick();	
        }
    }

    public void phoneNumberFieldAction() {
       	flowButtons.getButton("next").doClick();
    }

    public void setFieldStates() {
		boolean acctNbrPresent = !(acctNumberField.getText().length() == 0);
		boolean cardNbrPresent = !(cardNumberField.getText().length() == 0);
		boolean phoneNbrPresent = !(phoneNumberField.getText().length() == 0);

    	setComponentEnabled(acctNumberPanel, acctNumberLookupEnabled && !phoneNbrPresent && !cardNbrPresent, true);
    	setComponentEnabled(cardPanel, inMoneySaverProgram && !acctNbrPresent && !phoneNbrPresent, true);
		setComponentEnabled(phoneNumberPanel, phoneNumberLookupEnabled && !acctNbrPresent && !cardNbrPresent, true);
    }

    /**
     * Get the MoneySaver card number from the field and validate it if it has
     *   been entered. If it is not valid, display an error to the user.
     */

    @Override
	public void finish(int direction) {
        // call setButtonsEnabled to avoid duplicated transactions.
        flowButtons.setButtonsEnabled(false);
        message1.setListenerEnabled(false);
//        transaction.getDataCollectionData().setAfterValidationAttempt(false);        

        if (direction == PageExitListener.NEXT) {
        	message1.removeListener(this);
			String AcctNbr = acctNumberField.getText();
			if (! AcctNbr.isEmpty()) {
				transaction.resetCardPhoneNumber();
				transaction.setBpAccountNumber(AcctNbr);
	            transaction.getAccountNumberHistory(this, AcctNbr);
			
			} else if (! cardNumberField.getText().isEmpty()) {
				transaction.setCardPhoneNumber(stripNumber(cardNumberField.getText().trim()), true, SessionType.BP, null, this);
			
			} else if (! phoneNumberField.getText().isEmpty()) {
				transaction.setCardPhoneNumber(stripNumber(phoneNumberField.getText().trim()), false, SessionType.BP, null, this);
			} else {
				// no history involved; clear prior results
				transaction.clearCustomers();
				transaction.resetCardPhoneNumber();
	        	transaction.setBpAccountNumber("");
	            PageNotification.notifyExitListeners(this, direction);
			}
        
        } else {
            PageNotification.notifyExitListeners(this, direction);
        }
    }

    /**
     * Callback from transaction after the comm is finished. If the comm was
     *   successful, go to the next page. Otherwise, go back to the card number
     *   field.
     */
    @Override
	public void commComplete(int commTag, Object returnValue) {
        boolean b = ((Boolean) returnValue).booleanValue();
        if (b) {
	        if (commTag == MoneyGramClientTransaction.CONSUMER_HISTORY_LOOKUP) {
	        	if ((transaction.getCardPhoneNumber() == null) || (transaction.getCardPhoneNumber().isEmpty())) {
	        		// treat as an account number history lookup
					if (transaction.getCustomers() == null || transaction.getCustomers().isEmpty()) {
	                    int rr = Dialogs.showOkCancel("dialogs/DialogNoTrxFound.xml");
	                    if (rr != Dialogs.OK_OPTION) {
	                        repaint();
	                        acctNumberField.requestFocus();
	                        flowButtons.setButtonsEnabled(true);
	                        if (!transaction.isFeeQuery() && !transaction.isDirectoryOfBillers()) {
	                            flowButtons.setEnabled("back", false);
	                    	}
	                        return;
	                    }
					} else if (transaction.getCustomers().size() > 2) {
						// restrict to a single sender (plus empty biller)
						transaction.clearCustomers();
	                    int rr = Dialogs.showOkCancel("dialogs/DialogMultiTrxFound.xml");
	                    //return if with focus
	                    if (rr != Dialogs.OK_OPTION) {
	                        repaint();
	                        acctNumberField.requestFocus();
	                        flowButtons.setButtonsEnabled(true);
	                        if (!transaction.isFeeQuery() && !transaction.isDirectoryOfBillers()) {
	                            flowButtons.setEnabled("back", false);
	                    	}
	                        return;
	                    }
					}
					// save acct number for verification data entry
					transaction.setBillerAccountNumber(acctNumberField.getText());
					PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
	        	} else {
	        		// lookup is based on Plus card number or a phone number
					transaction.setBillerAccountNumber("");
					if (transaction.getCustomers() == null || transaction.getCustomers().isEmpty()) {
						if (cardNumberField.getText().length() > 0) {
							Dialogs.showWarning(
									"dialogs/DialogCustomerNotFound.xml",
									Messages.getString("MoneyGramSendTransaction.2005"),
									Messages.getString("MoneyGramSendTransaction.2006"));
	                        repaint();
	                        cardNumberField.requestFocus();
	                        flowButtons.setButtonsEnabled(true);
	                        if (!transaction.isFeeQuery() && !transaction.isDirectoryOfBillers()) {
	                            flowButtons.setEnabled("back", false);	
	                    	}
	                        return;
						} else {
		                    int rr = Dialogs.showOkCancel("dialogs/DialogNoTrxFound.xml");
		                    if (rr != Dialogs.OK_OPTION) {
		                        repaint();
		                        if (cardNumberField.isEnabled()) {
		                        	cardNumberField.requestFocus();
		                        }
		                        else if (phoneNumberField.isEnabled()) {
		                        	phoneNumberField.requestFocus();
		                        }
		                        flowButtons.setButtonsEnabled(true);
		                        if (!transaction.isFeeQuery() && !transaction.isDirectoryOfBillers()) {
		                            flowButtons.setEnabled("back", false);	
		                    	}
		                        return;
		                    }
						}
					}
					PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
	        	}
			}
        } else {
        	start(PageExitListener.NEXT);
        }
    }
    
    @Override
	public void dwTextPaneResized() {
    	JPanel panel1 = (JPanel) getComponent("panel1");
    	int width = panel1.getSize().width;
    	message1.setWidth(width);
	}
}
