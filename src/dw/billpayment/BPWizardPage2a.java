package dw.billpayment;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import com.moneygram.agentconnect.BillerInfo;

import dw.dwgui.DWTable;
import dw.dwgui.DWTable.DWTableListener;
import dw.dwgui.UneditableTableModel;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.mgsend.MoneyGramSendFlowPage;
import dw.mgsend.MoneyGramSendTransaction;

/**
 * Page 2a of the Directory of Billers/BillPayment wizard. This page displays 
 *   a list of companies that match the search criteria entered. The user must 
 *   choose one of the companies listed to continue.  This screen is only seen
 *   if an industry button search is performed and grouping is needed
 */
public class BPWizardPage2a extends MoneyGramSendFlowPage implements DWTableListener {
	private static final long serialVersionUID = 1L;

    private DWTable table;
    private DefaultTableModel tableModel;
    private ArrayList<ArrayList<BillerInfo>> billerNameList;
	private JScrollPane scrollpane1;
	private JPanel panel1;

    public BPWizardPage2a(MoneyGramSendTransaction tran, String name,
                          String pageCode, PageFlowButtons buttons) {
        super(tran, "billpayment/BPWizardPage2a.xml", name, pageCode, buttons);
        table = (DWTable) getComponent("matchingCompanyTable");
		panel1 = (JPanel) getComponent("panel1");
		scrollpane1 = (JScrollPane) getComponent("scrollPane1");
        table.addListener(this, this);
        table.setListenerEnabled(false);
    }

    private void setupTable() {
        String[] columnNames = {Messages.getString("BPWizardPage2.1904")}; 
        tableModel = new UneditableTableModel(columnNames, 0);
        table.getTable().setModel(tableModel);
        table.getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        TableColumn tc = table.getTable().getColumnModel().getColumn(0);
        tc.setCellRenderer(table.new DWTableCellRenderer(DWTable.DEFAULT_COLUMN_MARGIN));
        table.getTable().getSelectionModel().setSelectionInterval(0, 0);
    }

    @Override
	public void start(int direction) {
        flowButtons.reset();
        flowButtons.setVisible("expert", false);	
        
        if (direction == PageExitListener.NEXT) {	
        	billerNameList = transaction.getBillerNameList();
	        setupTable();
	        String groupName = null;
	        String billerName = null;

	        int rowCount = tableModel.getRowCount();
	        for (int i = 0; i < rowCount; i++){
	            tableModel.removeRow(0);
	        }

	        for (int i = 0; i < billerNameList.size(); i++) {
	        	groupName = billerNameList.get(i).get(0).getBillerGroupName();
	        	billerName = billerNameList.get(i).get(0).getBillerName();
   	        	String name = "";
	        	if (groupName.length() > 0)
	        		name = groupName;
	        	else
	        		name = billerName;
	        	
	        	String[] row = {name};
	            tableModel.addRow(row);
	        }
	        // select the top row : 'none'
	        table.getTable().setRowSelectionInterval(0, 0);
        } 
       	
        table.getTable().requestFocus();

        // set up the enter key for the table
        final JButton nextButton = flowButtons.getButton("next");	
        final JButton backButton = flowButtons.getButton("back");	
        
        if (table.getTable().getKeyListeners().length == 0) {
	        KeyListener nexter = new KeyAdapter() {
	            @Override
				public void keyPressed(KeyEvent e) {
	                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
	                    e.consume();
	                    nextButton.doClick();
	                }
	                if (e.getKeyCode() == KeyEvent.VK_TAB) {
	                    e.consume();
	                    backButton.requestFocus(true);
	                }
	                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
	                    e.consume();
	                    finish(PageExitListener.CANCELED);
	                }
	            }
	        };
	        table.getTable().addKeyListener(nexter);
        }
        
        table.initializeTable(scrollpane1, panel1, 300, 10);
        
        table.setListenerEnabled(true);
        dwTableResized();
    }
   
    @Override
	public void finish(int direction) {
        if (direction == PageExitListener.NEXT) {
        	transaction.setSelectedBillerName(transaction.getSelectedBillerInfo(table.getTable().getSelectedRow()));
        	PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
        } else {
        	PageNotification.notifyExitListeners(this, direction);
        }
        table.setListenerEnabled(false);
    }

	@Override
	public void dwTableResized() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.resizeTable();
			} 
		});
	}
}
