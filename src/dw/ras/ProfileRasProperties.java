package dw.ras;


import java.util.NoSuchElementException;

import dw.profile.Profile;
import dw.profile.ProfileItem;
import dw.profile.UnitProfile;
import dw.utility.Debug;


/**
 * Dial up networking properties implementation.  This implementation 
 * uses the UnitProfile object.  
 *
 * @author Christopher Bartling
 */
public class ProfileRasProperties implements IRasProperties {

    // CONSTANTS
    public static final String PRIMARY_DUN_USERNAME = 
        "PRIMARY_DUN_USER_ID";	
    public static final String PRIMARY_DUN_PW_IDENTIFIER = 
        "PRIMARY_DUN_PASSWORD";	
    public static final String SECONDARY_DUN_USERNAME = 
        "SECONDARY_DUN_USER_ID";	
    public static final String SECONDARY_DUN_PW_IDENTIFIER= 
        "SECONDARY_DUN_PASSWORD";	
    public static final String PRIMARY_PHONE_NUMBER = 
        "HOST_PRIMARY_PHONE";	
    public static final String SECONDARY_PHONE_NUMBER = 
        "HOST_SECONDARY_PHONE";	
    public static final String DIALING_PREFIX = 
        "HOST_SPECIAL_DIAL";	
    public static final String PULSE_DIAL = 
        "HOST_PHONE_TYPE";	
    public static final String CONNECTION_CLOSE_DELAY = 
        "CONNECTION_CLOSE_DELAY";	


    /**
     * Voids out the RAS testing params.
     */
    @Override
	public void reset() { /* Do-nothing behavior */ }

    /**
     * Accessor for the primary phone number profile item.  This string 
     * contains both the dialing prefix and the optional pulse dialing
     * flag character, so you do not need to build your own string.
     * @return String object representing the primary phone number 
     * profile item.
     * @author Christopher Bartling
     */
    @Override
	public String getPrimaryPhoneNumber() {
    
        StringBuffer completePhoneNumber = new StringBuffer();

        String phoneNumber = getProfileItem(PRIMARY_PHONE_NUMBER);

        // Added this to handle internal Moneygram numbers.  The
        // Profile Editor does not handle 4-digit internal extension phone
        // numbers.
        if (phoneNumber.startsWith("555-555-")) {	
            phoneNumber = phoneNumber.substring(8);
        }
        
        completePhoneNumber.append(phoneNumber);

        return completePhoneNumber.toString();
    }
    
    /**
     * Accessor for the secondary phone number profile item.  This string 
     * contains both the dialing prefix and the optional pulse dialing
     * flag character, so you do not need to build your own string.
     * @return String object representing the secondary phone number 
     * profile item.
     * @author Christopher Bartling
     */
    @Override
	public String getSecondaryPhoneNumber() {

        StringBuffer completePhoneNumber = new StringBuffer();

        String phoneNumber = getProfileItem(SECONDARY_PHONE_NUMBER);

        // Added this to handle internal MoneyGram numbers.  The
        // Profile Editor does not handle 4-digit internal extension phone
        // numbers.
        if (phoneNumber.startsWith("555-555-")) {	
            phoneNumber = phoneNumber.substring(8);
        }
        
        completePhoneNumber.append(phoneNumber);

        return completePhoneNumber.toString();
    }
    
    /**
     * Accessor for the primary DUN username profile item.
     * @return String object representing the username 
     * profile item.
     * @author Christopher Bartling
     */
    @Override
	public String getPrimaryUsername() {
                    
        return getProfileItem(PRIMARY_DUN_USERNAME);
    }
    
    /**
     * Accessor for the primary DUN password profile item.
     * @return String object representing the password 
     * profile item.
     * @author Christopher Bartling
     */
    @Override
	public String getPrimaryPassword() {
            
        return getProfileItem(PRIMARY_DUN_PW_IDENTIFIER);
    }
    
    /**
     * Accessor for the secondary DUN username profile item.
     * @return String object representing the username 
     * profile item.
     * @author Christopher Bartling
     */
    @Override
	public String getSecondaryUsername() {
                    
        return getProfileItem(SECONDARY_DUN_USERNAME);
    }
    
    /**
     * Accessor for the secondary DUN password profile item.
     * @return String object representing the password 
     * profile item.
     * @author Christopher Bartling
     */
    @Override
	public String getSecondaryPassword() {
            
        return getProfileItem(SECONDARY_DUN_PW_IDENTIFIER);
    }
    
    /**
     * Accessor for the dialing prefix profile item.
     * @return String object representing the dialing prefix 
     * profile item.
     * @author Christopher Bartling
     */
    @Override
	public String getDialingPrefix() {

        return getProfileItem(DIALING_PREFIX);
    }
    
    /**
     * Accessor for the pulse dialing boolean flag profile item.
     * @return A boolean representing the pulse dialing boolean flag 
     * profile item.
     * @author Christopher Bartling
     */
    @Override
	public boolean isPulseDialing() {
        
        boolean result = false;
        if (getProfileItem(PULSE_DIAL).equalsIgnoreCase("P")) 	
            result = true;
        return result;
    }
    
    /**
     * Accessor for the connection close delay.
     * @return An integer value representing the connection close sleep 
     * interval. Rewritten to take advantage of path-based accessors in 
     * UnitProfile so that missing or invalid values are handled more
     * gracefully.
     * @author Christopher Bartling
     * @author Geoff Atkin
     */
    @Override
	public int getCloseConnectionDelay() {
        return UnitProfile.getInstance().get(CONNECTION_CLOSE_DELAY, 30);
    }

    /**
     * Obtains a profile item value from the UnitProfile.getUPInstance().
     * @param profileItemTag A String representing the tag for the profile 
     * item.
     * @return The profile item value as a String.
     * @author Christopher Bartling
     */
    public String getProfileItem(String profileItemTag) {

        String result = "";	
        try {
            Profile root = UnitProfile.getInstance().getProfileInstance();
            ProfileItem item = root.find(profileItemTag); 
            if (item != null) 
                result = item.stringValue();
        }
        catch (NoSuchElementException e) {
            Debug.println("Could not find RAS profile item: [" + 	
                profileItemTag + "].");	
        }

        return result;
    } 
    
} // END-CLASS


    
