
package dw.ras;


/**
 * Encapsulates the RASDIALPARAMS structure.  This structure contains 
 * parameters that are used by RasDial to establish a remote access 
 * connection. 
 * @author Christopher Bartling
 */
public class RasDialParams {

    /** 
     * Phone book entry name.  Corresponds to the <code>szEntryName</code> 
     * in the <code>RASDIALPARAMS</code> structure.
     */
    private String entryName;
    
    /** 
     * Phone number to call for a connection.  
     * Corresponds to the <code>szPhoneNumber</code> 
     * in the <code>RASDIALPARAMS</code> structure.
     */
    private String phoneNumber;
    
    /** 
     * User name to use for connection authentication.  
     * Corresponds to the <code>szUserName</code> 
     * in the <code>RASDIALPARAMS</code> structure.
     */
    private String username;
    
    /** 
     * Password to use for connection authentication.  
     * Corresponds to the <code>szPassword</code> 
     * in the <code>RASDIALPARAMS</code> structure.
     */
    private String password;

    /** 
     * Domain on which authentication is to occur.  
     * Corresponds to the <code>szDomain</code> 
     * in the <code>RASDIALPARAMS</code> structure.
     */
    private String authenticationDomain;
    
    
    /**
     * Constructor.
     * @param newPhoneNumber A String representing the phone number for 
     * the connection params.
     * @param newUsername A String representing the user name for 
     * the connection params.
     * @param newPassword A String representing the password for 
     * the connection params.
     * @author Christopher Bartling
     */
    public RasDialParams(String newPhoneNumber,
                         String newUsername,
                         String newPassword) {
                         
        this("", newPhoneNumber, newUsername, newPassword, "");
    }                         

    /**
     * Constructor.
     *
     * @param newEntryName A String representing the entry name for 
     * the connection params.
     * @param newPhoneNumber A String representing the phone number for 
     * the connection params.
     * @param newUsername A String representing the user name for 
     * the connection params.
     * @param newPassword A String representing the password for 
     * the connection params.
     * @param newAuthenticationDomain A String representing the authentication
     * domain for the connection params.
     * @author Christopher Bartling
     */
    public RasDialParams(String newEntryName,
                         String newPhoneNumber,
                         String newUsername,
                         String newPassword,
                         String newAuthenticationDomain) {
                         
        super();
        this.setEntryName(newEntryName);
        this.setPhoneNumber(newPhoneNumber);
        this.setUsername(newUsername);
        this.setPassword(newPassword);
        this.setAuthenticationDomain(newAuthenticationDomain);                         
    }                         
                          
    
    /** 
     * Accessor for the entry name attribute.  
     * @return A String representing the entry name for the connection params.
     * @author Christopher Bartling
     */
    public String getEntryName() {
        return this.entryName;
    }
    
    /** 
     * Mutator for the entry name attribute.  
     * @param newEntryName A String representing the entry name for 
     * the connection params.
     * @author Christopher Bartling
     */
    public void setEntryName(String newEntryName) {
        this.entryName = newEntryName;
    }

    
    /** 
     * Accessor for the phone number attribute.  
     * @return A String representing the phone number for the connection params.
     * @author Christopher Bartling
     */
    public String getPhoneNumber() {
        return this.phoneNumber;
    }
    
    /** 
     * Mutator for the phone number attribute.  
     * @param newPhoneNumber A String representing the phone number for 
     * the connection params.
     * @author Christopher Bartling
     */
    public void setPhoneNumber(String newPhoneNumber) {
        this.phoneNumber = newPhoneNumber;
    }
    
    /** 
     * Accessor for the user name attribute.  
     * @return A String representing the user name for the connection params.
     * @author Christopher Bartling
     */
    public String getUsername() {
        return this.username;
    }
    
    /** 
     * Mutator for the user name attribute.  
     * @param newUsername A String representing the user name for 
     * the connection params.
     * @author Christopher Bartling
     */
    public void setUsername(String newUsername) {
        this.username = newUsername;
    }
    
    /** 
     * Accessor for the password attribute.  
     * @return A String representing the password for the connection params.
     * @author Christopher Bartling
     */
    public String getPassword() {
        return this.password;
    }
    
    /** 
     * Mutator for the password attribute.  
     * @param newPassword A String representing the password for 
     * the connection params.
     * @author Christopher Bartling
     */
    public void setPassword(String newPassword) {
        this.password = newPassword;
    }
    
    /** 
     * Accessor for the authentication domain attribute.  
     * @return A String representing the authentication domain for the 
     * connection params.
     * @author Christopher Bartling
     */
    public String getAuthenticationDomain() {
        return this.authenticationDomain;
    }
    
    /** 
     * Mutator for the authentication domain attribute.  
     * @param newAuthenticationDomain A String representing the authentication
     * domain for the connection params.
     * @author Christopher Bartling
     */
    public void setAuthenticationDomain(String newAuthenticationDomain) {
        this.authenticationDomain = newAuthenticationDomain;
    }

} // END-CLASS
