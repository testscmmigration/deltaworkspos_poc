package dw.ras;

/**
 * Dial up networking properties.  This class containing all static methods,
 * delegating the implementation of each method to the dynamically loaded
 * RasProperties implementation.  I used dynamic class loading here so I could
 * circumvent hardcoding the testing packages into the import statements.
 *
 * @author Christopher Bartling
 */
public class RasProperties {

    public static void setCustomImplName() {
    }

    private static boolean muted = true;

    public static boolean isMuted() {
        return muted;
    }

    public static void setMuted(boolean m) {
        muted = m;
    }

    
    private static boolean productionFlag = true;
    
    /** 
     * The IRasProperties implementation.  Public interface calls are delegated to
     * this instance. 
     */
    private static IRasProperties delegate;

    /**
     * Disallow instantiation of this class.
     */
    private RasProperties() {}
    
    /**
     * Voids out the RAS testing params.
     */
    public static void reset() {
        checkImpl();
        delegate.reset();
    }


    /**
     * Sets the class into production or testing mode.
     * @param newProductionFlag A boolean.
     * @author Christopher Bartling
     */
    public static void setProduction(final boolean newProductionFlag) {
        productionFlag = newProductionFlag;
    }

    /**
     * Sets the class into production or testing mode.
     * @param newProductionFlag A boolean.
     * @author Christopher Bartling
     */
    public static boolean isProduction() {
        return productionFlag;
    }   


    /**
     * Accessor for the primary phone number profile item.  This string 
     * contains both the dialing prefix and the optional pulse dialing
     * flag character, so you do not need to build your own string.
     * @return String object representing the primary phone number 
     * profile item.
     * @author Christopher Bartling
     */
    public static String getPrimaryPhoneNumber() {
        checkImpl();
        return delegate.getPrimaryPhoneNumber();
    }
    
    /**
     * Accessor for the secondary phone number profile item.  This string 
     * contains both the dialing prefix and the optional pulse dialing
     * flag character, so you do not need to build your own string.
     * @return String object representing the secondary phone number 
     * profile item.
     * @author Christopher Bartling
     */
    public static String getSecondaryPhoneNumber() {
        checkImpl();
        return delegate.getSecondaryPhoneNumber();
    }
    
    /**
     * Accessor for the primary DUN username profile item.
     * @return String object representing the username 
     * profile item.
     * @author Christopher Bartling
     */
    public static String getPrimaryUsername() {
        checkImpl();
        return delegate.getPrimaryUsername();
    }
    
    /**
     * Accessor for the primary DUN password profile item.
     * @return String object representing the password profile item.
     * @author Christopher Bartling
     */
    public static String getPrimaryPassword() {
        checkImpl();
        return delegate.getPrimaryPassword();
    }
    
    /**
     * Accessor for the secondary DUN username profile item.
     * @return String object representing the username 
     * profile item.
     * @author Christopher Bartling
     */
    public static String getSecondaryUsername() {
        checkImpl();
        return delegate.getSecondaryUsername();
    }
    
    /**
     * Accessor for the secondary DUN password profile item.
     * @return String object representing the password profile item.
     * @author Christopher Bartling
     */
    public static String getSecondaryPassword() {
        checkImpl();
        return delegate.getSecondaryPassword();
    }
    
    /**
     * Accessor for the dialing prefix profile item.
     * @return String object representing the dialing prefix profile item.
     * @author Christopher Bartling
     */
    public static String getDialingPrefix() {
        checkImpl();
        return delegate.getDialingPrefix();
    }
    
    /**
     * Accessor for the pulse dialing boolean flag profile item.
     * @return A boolean representing the pulse dialing boolean flag 
     * profile item.
     * @author Christopher Bartling
     */
    public static boolean isPulseDialing() {
        checkImpl();
        return delegate.isPulseDialing();
    }

    
    /**
     * Accessor for the connection close sleep time.
     * @return An integer value representing the connection close sleep 
     * interval.
     * @author Christopher Bartling
     */
    public static int getCloseConnectionDelay() {
        checkImpl();
        return delegate.getCloseConnectionDelay();
    }
    
    /**
     *  Make sure the delegate instance has been loaded. Simplified version of
     *  the above method. The dynamic class load is no longer needed because
     *  I've eliminated the test impl.
     */
    static void checkImpl() {
        if (delegate == null) {
            delegate = new ProfileRasProperties();
        }
    }
}


    
