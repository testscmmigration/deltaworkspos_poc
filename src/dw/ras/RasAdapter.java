
package dw.ras;


/**
 * Event listener implementation for notification of RAS events.  This 
 * implementation contains only do-nothing behavior.
 * @author Christopher Bartling
 */
public class RasAdapter implements RasListener {

    /**
     * Device connected event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void deviceConnected(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * Open port event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void openPort(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * Port opened event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void portOpened(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * Connect device event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void connectDevice(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * All devices connected event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void allDevicesConnected(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * Authenticate event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void authenticate(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * Authenticate notify event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void authNotify(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * Authenticate retry event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void authRetry(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * Authenticate callback event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void authCallback(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * Authenticate change password event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void authChangePassword(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * Authenticate project event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void authProject(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * Authenticate link speed event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void authLinkSpeed(final int rasErrorCode) { /* Do-nothing behavior */ }


    /**
     * Authentication acknowledgement event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void authAck(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * Re-authenticated event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void reAuthenticate(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * Authenticated event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void authenticated(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * Prepare for callback event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void prepareForCallback(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * Wait for modem reset event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void waitForModemReset(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * Wait for callback event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void waitForCallback(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * Projected event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void projected(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * Subentry connected event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void subentryConnected(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * Subentry disconnected event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void subentryDisconnected(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * Interactive (paused) event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void interactive(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * Retry authentication event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void retryAuthentication(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * Callback set by caller event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void callbackSetByCaller(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * Password expired event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void passwordExpired(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * Invoke EAP UI event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void invokeEAPUI(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * Connected event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void connected(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * Disconnected event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    @Override
	public void disconnected(final int rasErrorCode) { /* Do-nothing behavior */ }

    /**
     * States that the entire dialing sequence failed.
	 * @author Christopher Bartling
	 */
    @Override
	public void attemptSequenceFailed() { /* Do-nothing behavior */ }


} // END-CLASS
