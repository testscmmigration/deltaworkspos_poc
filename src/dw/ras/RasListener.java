
package dw.ras;


/**
 * Event listener interface for notification of RAS events.
 * @author Geoff Atkin
 * @author Christopher Bartling
 */
public interface RasListener {

    /**
     * Device connected event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void deviceConnected(final int rasErrorCode);

    /**
     * Open port event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void openPort(final int rasErrorCode);

    /**
     * Port opened event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void portOpened(final int rasErrorCode);

    /**
     * Connect device event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void connectDevice(final int rasErrorCode);

    /**
     * All devices connected event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void allDevicesConnected(final int rasErrorCode);

    /**
     * Authenticate event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void authenticate(final int rasErrorCode);

    /**
     * Authenticate notify event callback method.  This callback will be used
     * with a RAS error code > 0 to notify the client that an invalid 
     * password was encountered during authentication.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void authNotify(final int rasErrorCode);

    /**
     * Authenticate retry event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void authRetry(final int rasErrorCode);

    /**
     * Authenticate callback event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void authCallback(final int rasErrorCode);

    /**
     * Authenticate change password event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void authChangePassword(final int rasErrorCode);

    /**
     * Authenticate project event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void authProject(final int rasErrorCode);

    /**
     * Authenticate link speed event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void authLinkSpeed(final int rasErrorCode);


    /**
     * Authentication acknowledgement event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void authAck(final int rasErrorCode);

    /**
     * Re-authenticated event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void reAuthenticate(final int rasErrorCode);

    /**
     * Authenticated event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void authenticated(final int rasErrorCode);

    /**
     * Prepare for callback event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void prepareForCallback(final int rasErrorCode);

    /**
     * Wait for modem reset event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void waitForModemReset(final int rasErrorCode);

    /**
     * Wait for callback event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void waitForCallback(final int rasErrorCode);

    /**
     * Projected event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void projected(final int rasErrorCode);

    /**
     * Subentry connected event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void subentryConnected(final int rasErrorCode);

    /**
     * Subentry disconnected event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void subentryDisconnected(final int rasErrorCode);

    /**
     * Interactive (paused) event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void interactive(final int rasErrorCode);

    /**
     * Retry authentication event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void retryAuthentication(final int rasErrorCode);

    /**
     * Callback set by caller event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void callbackSetByCaller(final int rasErrorCode);

    /**
     * Password expired event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void passwordExpired(final int rasErrorCode);

    /**
     * Invoke EAP UI event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void invokeEAPUI(final int rasErrorCode);

    /**
     * Connected event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void connected(final int rasErrorCode);

    /**
     * Disconnected event callback method.
	 * @param rasError An integer value representing the RAS error code.
	 * @author Christopher Bartling
	 */
    public void disconnected(final int rasErrorCode);


    /**
     * States that the entire dialing sequence failed.
	 * @author Christopher Bartling
	 */
    public void attemptSequenceFailed();

}
