package dw.ras;


/**
 * Dial up networking properties interface.
 * @author Christopher Bartling
 */
public interface IRasProperties {

    /**
     * Voids out the RAS testing params.
     */
    public void reset();

    /**
     * Accessor for the primary phone number profile item.  This string 
     * contains both the dialing prefix and the optional pulse dialing
     * flag character, so you do not need to build your own string.
     * @return String object representing the primary phone number 
     * profile item.
     * @author Christopher Bartling
     */
    public String getPrimaryPhoneNumber();
    
    /**
     * Accessor for the secondary phone number profile item.  This string 
     * contains both the dialing prefix and the optional pulse dialing
     * flag character, so you do not need to build your own string.
     * @return String object representing the secondary phone number 
     * profile item.
     * @author Christopher Bartling
     */
    public String getSecondaryPhoneNumber();
    
    /**
     * Accessor for the primary DUN username profile item.
     * @return String object representing the username 
     * profile item.
     * @author Christopher Bartling
     */
    public String getPrimaryUsername();
    
    /**
     * Accessor for the primary DUN password profile item.
     * @return String object representing the password 
     * profile item.
     * @author Christopher Bartling
     */
    public String getPrimaryPassword();

    /**
     * Accessor for the secondary DUN username profile item.
     * @return String object representing the username 
     * profile item.
     * @author Christopher Bartling
     */
    public String getSecondaryUsername();
    
    /**
     * Accessor for the secondary DUN password profile item.
     * @return String object representing the password 
     * profile item.
     * @author Christopher Bartling
     */
    public String getSecondaryPassword();
    
    /**
     * Accessor for the dialing prefix profile item.
     * @return String object representing the dialing prefix 
     * profile item.
     * @author Christopher Bartling
     */
    public String getDialingPrefix();
    
    /**
     * Accessor for the pulse dialing boolean flag profile item.
     * @return A boolean representing the pulse dialing boolean flag 
     * profile item.
     * @author Christopher Bartling
     */
    public boolean isPulseDialing();
    
    /**
     * Accessor for the connection close sleep time.
     * @return An integer value representing the connection close sleep 
     * interval.
     * @author Christopher Bartling
     */
    public int getCloseConnectionDelay();
    
} // END-CLASS


    
