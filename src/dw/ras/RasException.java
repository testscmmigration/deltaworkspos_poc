

package dw.ras;


/**
 * A general abstraction of the RAS errors coming across the JNI wrapped 
 * interface.
 * @author Christopher Bartling
 */
public class RasException extends Exception {
	private static final long serialVersionUID = 1L;

	/** The error code coming from the RAS error. */
    protected int rasErrorCode = -1;
    
    /** The RAS error message for use in debugging. */
    protected String rasErrorMessage;

    /**
     * Constructor.
     * @param msg A message about the exception.
     * @author Christopher Bartling
     */
    public RasException() {
        super();
    }

    /**
     * Constructor.
     * @param msg A message about the exception.
     * @param newRasErrorCode The RAS error code that was thrown/occurred.
     * @author Christopher Bartling
     */
    public RasException(String msg, int newRasErrorCode) {
		super(msg);
        this.rasErrorCode = newRasErrorCode;
        this.rasErrorMessage = msg;
    }
    
    /**
     * Retrieves the error message from this exception.  This error message
     * is coming from the Microsoft RAS environment.
     * @return A String representing the error message.
     * @author Christopher Bartling
     */
    @Override
	public String getMessage() {
        return this.rasErrorMessage;
    }
    
    /**
     * Retrieves the error code from this exception.  This error code
     * corresponds to the codes in <code>raserror.h</code>.
     * @return An integer representing the error code in RAS.
     * @author Christopher Bartling
     */
    public int getErrorCode() {
        return this.rasErrorCode;
    }

} // END-CLASS
