package dw.dispenser;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.comm.CommPortIdentifier;
import javax.comm.PortInUseException;
import javax.comm.SerialPort;

import com.moneygram.agentconnect.ComplianceTransactionRequest;
import com.moneygram.agentconnect.ComplianceTransactionRequestType;
import com.moneygram.agentconnect.MoneyOrderInfo;
import com.moneygram.agentconnect.MoneyOrderPrintStatusType;
import com.moneygram.agentconnect.MoneyOrderVoidReasonCodeType;

import dw.dialogs.Dialogs;
import dw.io.CCILog;
import dw.io.DiagLog;
import dw.io.IsiReporting;
import dw.io.State;
import dw.io.StateListener;
import dw.io.report.ReportLog;
import dw.main.MainPanelDelegate;
import dw.moneyorder.MoneyOrderInfomation;
import dw.profile.ClientInfo;
import dw.profile.UnitProfile;
import dw.utility.Debug;
import dw.utility.ExtraDebug;
import dw.utility.Scheduler;
import dw.utility.TimeUtility;
//import dw.dispenserload.DispenserLoadTransaction;

public final class Dispenser extends OutputStream implements Runnable, DispenserInterface {

    /*
     *  Possible values for extended status. Bits 0 and 7 are not used,
     *  and bits 18 through 22 are a document counter.
     */
    private static final int PRINT_HEAD_JAM = 1 << 1;
    private static final int MISSED_TOP_OF_FORM = 1 << 2;
    private static final int INCORRECT_PASSWORD = 1 << 3;
    private static final int AMOUNT_NOT_VALID = 1 << 4;
//  private static final int MID_MARK_DETECTED = 1 << 5;
    private static final int GRAPHICS_AVAILABLE = 1 << 6;
    private static final int POWER_FAIL = 1 << 8;
    private static final int IMPROPER_3RD_MARK = 1 << 9;
    private static final int COMPARTMENT_HAS_BEEN_OPENED = 1 << 10;
    private static final int TEST_MODE = 1 << 11;
    private static final int NOT_IDLE_OR_FAIL = 1 << 12;
    private static final int LAST_ITEM_VOID = 1 << 13;
//    private static final int MEMORY_INITIALIZATION = 1 << 14;
    private static final int COMPARTMENT_IS_OPEN = 1 << 15;
    private static final int MID_MARK_ERROR = 1 << 16;
//    private static final int PRINTER_ON_HOLD = 1 << 17;
    private static final int TOP_OF_FORM = 1 << 23;
//    private static final int PLUNGER_IS_UP = 1 << 24;
    private static final int ROM_FAILED = 1 << 25;
    private static final int RAM_FAILED = 1 << 26;
    private static final int THIRD_MARK_ERROR = 1 << 27;
    private static final int SWITCH_ONE_FAILED = 1 << 29;
    private static final int SWITCH_TWO_FAILED = 1 << 30;

    private static final int ASCII_ACK = 6;
    private static final int ASCII_LF = 10;
    private static final int ASCII_CR = 13;
    private static final int ASCII_SPACE = 32;
    private static final int ASCII_ZERO = 48;
    private static final int ASCII_ONE = 49;
    private static final int ASCII_NINE = 57;
    private static final int ASCII_AT = 64;
    private static final int ASCII_R = 82;
    private static final int ASCII_BSLASH = 92;

    private static final long WAIT_FOR_COMMAND = 500;
    // 0.5 second for serial I/O
    private static final long WAIT_FOR_PRINT = 20000;
    // 20 seconds to print an item
    private static final long WAIT_FOR_BACKOUT = 5000;
    // 5 seconds to unload and release
    private static final long WAIT_FOR_MOTION = 45000;
    // 45 seconds for any other paper motion

    private int status;
    private int extendedStatus;
    private int wantedStatus;               // status we're waiting for
    private int hardwareItemCount;
    private int error;                      // as reported by the R command
    private int graphicsChecksum;

    // A load is not actually required at application
    // startup, but this class needs to be told the starting
    // and ending serial numbers, which is the same thing.

    private boolean loadRequired = true;
    private boolean verifyRequired = true;
    private boolean verifyOnRetract = false;
    private int retries;

    private int reloadVerifyCounter = 0;
    private boolean reloadVerifyDone = false;

    private CommPortIdentifier identifier;
    private SerialPort port;
    private OutputStream output;
    private InputStream input;
    private Thread reader;

    private byte buffer[];
    private int bufptr;
    private int bslashCounter;
    private int csum;

    private boolean parametersAvailable = false;
    private String dispenserSerialNumber;
    private String model;
    private String manufactureDate;
    private String mlc;

    private long nextSerialNumber; // without check digit
    private long remaining = 0;

    private StateListener listener;
    private static final int USED_PAIRS = 5;
    private static final int STATE_DATA_LENGTH =
        State.DISPENSER_2 - State.DISPENSER_1;
    private long usedStart[] = { 0, 0, 0, 0, 0 };
    private long usedEnd[] = { 0, 0, 0, 0, 0 };

    private static final boolean DEBUG = false;
    private String lastCommand;

    private byte[] voidLogo =
        {(byte) 0x48, (byte) 0x19, (byte) 0x68, (byte) 0x13,
        // x, y, width, height
        (byte) 0x00,
            (byte) 0x1F,
            (byte) 0xF8,
            (byte) 0xFF,
            (byte) 0x80,
            (byte) 0xFF,
            (byte) 0x00,
            (byte) 0xFF,
            (byte) 0xEF,
            (byte) 0xFF,
            (byte) 0xE0,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x07,
            (byte) 0xE0,
            (byte) 0x3E,
            (byte) 0x03,
            (byte) 0xC3,
            (byte) 0xC0,
            (byte) 0x3F,
            (byte) 0x83,
            (byte) 0xF0,
            (byte) 0x7C,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x03,
            (byte) 0xE0,
            (byte) 0x3C,
            (byte) 0x0F,
            (byte) 0x81,
            (byte) 0xF0,
            (byte) 0x1F,
            (byte) 0x01,
            (byte) 0xF0,
            (byte) 0x1F,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x03,
            (byte) 0xE0,
            (byte) 0x38,
            (byte) 0x1F,
            (byte) 0x00,
            (byte) 0xF8,
            (byte) 0x1F,
            (byte) 0x01,
            (byte) 0xF0,
            (byte) 0x0F,
            (byte) 0x80,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x01,
            (byte) 0xF0,
            (byte) 0x78,
            (byte) 0x3F,
            (byte) 0x00,
            (byte) 0xFC,
            (byte) 0x1F,
            (byte) 0x01,
            (byte) 0xF0,
            (byte) 0x0F,
            (byte) 0xC0,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x01,
            (byte) 0xF0,
            (byte) 0x70,
            (byte) 0x3E,
            (byte) 0x00,
            (byte) 0x7C,
            (byte) 0x1F,
            (byte) 0x01,
            (byte) 0xF0,
            (byte) 0x07,
            (byte) 0xC0,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x01,
            (byte) 0xF8,
            (byte) 0xF0,
            (byte) 0x7E,
            (byte) 0x00,
            (byte) 0x7E,
            (byte) 0x1F,
            (byte) 0x01,
            (byte) 0xF0,
            (byte) 0x07,
            (byte) 0xE0,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0xF8,
            (byte) 0xF0,
            (byte) 0x7E,
            (byte) 0x00,
            (byte) 0x7E,
            (byte) 0x1F,
            (byte) 0x01,
            (byte) 0xF0,
            (byte) 0x07,
            (byte) 0xE0,
            (byte) 0x00,
            (byte) 0xFF,
            (byte) 0xC0,
            (byte) 0xF8,
            (byte) 0xE0,
            (byte) 0x7E,
            (byte) 0x00,
            (byte) 0x7E,
            (byte) 0x1F,
            (byte) 0x01,
            (byte) 0xF0,
            (byte) 0x07,
            (byte) 0xE3,
            (byte) 0xFF,
            (byte) 0xFF,
            (byte) 0xC0,
            (byte) 0xFD,
            (byte) 0xE0,
            (byte) 0x7E,
            (byte) 0x00,
            (byte) 0x7E,
            (byte) 0x1F,
            (byte) 0x01,
            (byte) 0xF0,
            (byte) 0x07,
            (byte) 0xE3,
            (byte) 0xFF,
            (byte) 0xFF,
            (byte) 0xC0,
            (byte) 0x7D,
            (byte) 0xE0,
            (byte) 0x7E,
            (byte) 0x00,
            (byte) 0x7E,
            (byte) 0x1F,
            (byte) 0x01,
            (byte) 0xF0,
            (byte) 0x07,
            (byte) 0xE3,
            (byte) 0xFF,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x7D,
            (byte) 0xC0,
            (byte) 0x7E,
            (byte) 0x00,
            (byte) 0x7E,
            (byte) 0x1F,
            (byte) 0x01,
            (byte) 0xF0,
            (byte) 0x07,
            (byte) 0xE0,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x7F,
            (byte) 0xC0,
            (byte) 0x7E,
            (byte) 0x00,
            (byte) 0x7E,
            (byte) 0x1F,
            (byte) 0x01,
            (byte) 0xF0,
            (byte) 0x07,
            (byte) 0xE0,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x3F,
            (byte) 0x80,
            (byte) 0x3E,
            (byte) 0x00,
            (byte) 0x7C,
            (byte) 0x1F,
            (byte) 0x01,
            (byte) 0xF0,
            (byte) 0x07,
            (byte) 0xC0,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x3F,
            (byte) 0x80,
            (byte) 0x3F,
            (byte) 0x00,
            (byte) 0xFC,
            (byte) 0x1F,
            (byte) 0x01,
            (byte) 0xF0,
            (byte) 0x0F,
            (byte) 0xC0,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x3F,
            (byte) 0x80,
            (byte) 0x1F,
            (byte) 0x00,
            (byte) 0xF8,
            (byte) 0x1F,
            (byte) 0x01,
            (byte) 0xF0,
            (byte) 0x0F,
            (byte) 0x80,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x1F,
            (byte) 0x00,
            (byte) 0x0F,
            (byte) 0x81,
            (byte) 0xF0,
            (byte) 0x1F,
            (byte) 0x01,
            (byte) 0xF0,
            (byte) 0x1F,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x1F,
            (byte) 0x00,
            (byte) 0x03,
            (byte) 0xC3,
            (byte) 0xC0,
            (byte) 0x3F,
            (byte) 0x83,
            (byte) 0xF0,
            (byte) 0x7C,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0x0E,
            (byte) 0x00,
            (byte) 0x00,
            (byte) 0xFF,
            (byte) 0x00,
            (byte) 0xFF,
            (byte) 0xEF,
            (byte) 0xFF,
            (byte) 0xE0,
            (byte) 0x00,
            (byte) 0x00 };

    private byte[] const0 = { 5, 11, 7, 9, 6, 0, 7, 4, 0, 4, 11, 1 }; // random
    private byte[] const1 = { 6, 7, 5, 9, 0, 1, 1, 0, 8, 7, 6, 8 };
    private byte[] const2 = { 5, 0, 8, 4, 2, 4, 3, 8, 3, 4, 8, 6 };
    private byte[] const3 = { 5, 6, 8, 2, 1, 9, 11, 10, 3, 0, 7, 4 };

    private String[] extstatus = new String[32];

    {
        if (DEBUG) {
            extstatus[1] = "*print head jammed"; //1 JAM 
            extstatus[2] = " missed top of form"; //2 TOF 
            extstatus[3] = "*incorrect password"; //3 PASS 
            extstatus[4] = "*invalid amount"; //4 AMT 
            extstatus[5] = " mid or 3rd mark detected"; //5 LS 
            extstatus[6] = " graphics available"; //6 GL 
            extstatus[8] = "*power fail"; //8 PWR 
            extstatus[9] = "*improper 3rd/4th mark"; //9 3/4 
            extstatus[10] = "*compartment has been opened"; //10 UNL 
            extstatus[11] = "*test mode"; //11 TEST 
            extstatus[12] = " fail or power fail, not idle"; //12 NIF 
            extstatus[13] = " last item was voided"; //13 VOID 
            extstatus[14] = " memory initialization"; //14 MEM 
            extstatus[15] = " compartment is open"; //15 OPN 
            extstatus[16] = "*midmark error"; //16 MID 
            extstatus[17] = " printer on hold"; //17 PD 
            extstatus[23] = " top of form mark detected"; //23 RS 
            extstatus[24] = " plunger is up"; //24 PU 
            extstatus[25] = "*ROM failed"; //25 ROM 
            extstatus[26] = "*RAM failed"; //26 RAM 
            extstatus[29] = "*switch one failed"; //29 S1 
            extstatus[30] = "*switch two failed"; //30 S2 
        }
    }

    /**
      * Uses the communications API to open and write to a port connected
      * to the dispenser.
      * @param portName name of the port to open, such as COM1 or COM2
      */
    public Dispenser(String portName) {
        try {
            identifier = CommPortIdentifier.getPortIdentifier(portName);
            port = (SerialPort) identifier.open("Dispenser", 10); 
            output = port.getOutputStream();
            input = port.getInputStream();
            buffer = new byte[256];
            bufptr = 0;
            reader = new Thread(this);
            reader.start();
            write202();
            //saveLimboBeenUsed();

            if (port.isCTS()) {
                setStatus(LOAD_REQUIRED);
            }
            else {
            	Debug.println("Dispenser: No CTS");
                setStatus(NO_CTS);
            }
        }
        catch (PortInUseException e1) {
        	String sPortId = identifier.getName();
        	Debug.println("Dispenser: PortInUseException:" + sPortId);
            Dialogs.showError("dialogs/DialogDispenserPortInUse.xml", " "+sPortId);
            setStatus(NOT_INITIALIZED);
        }
        catch (Exception e) {
            setStatus(NOT_INITIALIZED);
        }
    }

    /**
     * Sets the current status. The status indicates both the state of
     * the connection to the dispenser and the readiness to print an
     * item. If the status is set to the value another thread (the writing
     * thread) is waiting for, then notify that thread. If the status is
     * set to a value which indicates the other thread is never going to
     * get the status it is waiting for, then notify that thread.
     */
    private synchronized void setStatus(int status) {
        this.status = status;
        if ((status == wantedStatus)
            || (status == EOF)
            || (status == ERROR)
            || (status == BAD_RESPONSE)) {
            notify();
        }
    }

    /**
     * Waits for the status to change to the desired value. Called by the
     * writing thread, waits for notification from the reading thread.
     * @param wantedStatus the status to wait for
     * @param millis the maximum wait time in milliseconds
     */
    private synchronized void waitFor(int wantedStatus, long millis) {
        long start, end;
        this.wantedStatus = wantedStatus;

        if (DEBUG) {
            start = System.currentTimeMillis();
        }

        try {
            wait(millis);
        }
        catch (InterruptedException e) {
        }

        if (DEBUG) {
            end = System.currentTimeMillis();
            Debug.println("waitFor(" 
                + wantedStatus + "," 
                + millis + ") command = " 
                + lastCommand + " status = " 
                + status + " elapsed time = " 
                + (end - start) + " milliseconds"); 
        }
    }

    @Override
	public int reOpen() {
        try {
            try {
                input.close();
            }
            catch (IOException e) {
            }
            try {
                output.close();
            }
            catch (IOException e) {
            }
            port.close();
            Thread.sleep(200);

            port = (SerialPort) identifier.open("Dispenser", 10); 
            output = port.getOutputStream();
            input = port.getInputStream();

            reader = new Thread(this);
            reader.start();
            write202();

            if (port.isCTS()) {
                setStatus(LOAD_REQUIRED);
            }
            else {
            	Debug.println("Dispenser: No CTS");
            	ExtraDebug.println("reOpen:");
                setStatus(NO_CTS);
            }
        }
        catch (Exception e) {
            setStatus(NOT_INITIALIZED);
        }
        return status;
    }

    /**
     * Returns the dispenser status, which can be one of the constants
     * defined above. This is the status of the last call that interacted
     * with the dispenser.
     */
    @Override
	public int getStatus() {
        return status;
    }

    /**
      * Returns an extended status value such as COMPARTMENT_IS_OPEN.
      * @param mask bitwise or of the extended status bits to check.
      */
    private boolean statusBit(int mask) {
        return (extendedStatus & mask) != 0;
    }

    /**
      * Sets the load-required flag and adjusts status and
      * parametersAvailable accordingly.
      */
    @Override
	public void setLoadRequired() {
        loadRequired = true;
        parametersAvailable = false;
        setStatus(LOAD_REQUIRED);
    }

    /**
      * Sets the verify-required flag and adjusts status
      * accordingly. Internal use only.
      */
    private void setVerifyRequired() {
        verifyRequired = true;
        setStatus(VERIFY_REQUIRED);
    }

    /**
     * Sets the verify-required flag.
     */
    @Override
	public void verifyRequired() {
        verifyRequired = true;
    }

    /**
     *  Send a message to the dispenser to update the status, then return it.
     */
    @Override
	public int queryStatus() {

        if (!isOn() && UnitProfile.getInstance().isDemoMode())
            return OK;

        if (DEBUG)
            Debug.println("queryStatus()"); 

        if (internalQueryStatus() == OK) {

            if (nextSerialNumber == 0) {
                setLoadRequired();
            }

            if (remaining <= 0) {
                setLoadRequired();
            }

            if (loadRequired) {
                setStatus(LOAD_REQUIRED);
            }
            else if (statusBit(COMPARTMENT_HAS_BEEN_OPENED)) {
                setLoadRequired();
            }
            else if (statusBit(COMPARTMENT_IS_OPEN)) {
                setLoadRequired();
            }
            else if (statusBit(TOP_OF_FORM) == false) {
                // this is a compromise: returns load_required but
                // doesn't make it permanent by setting the flag.
                setStatus(LOAD_REQUIRED);
            }
            else if (statusBit(LAST_ITEM_VOID)) {
                setStatus(VOIDED);
            }
            else if (statusBit(POWER_FAIL)) {
                //setLoadRequired(); 			// per Joe 4/21/00
                setVerifyRequired(); // per Tim 4/25/00
            }
            else if (verifyRequired) {
                setStatus(VERIFY_REQUIRED);
            }
        }
        return status;
    }

    private int internalQueryStatus() {
        return writeCommandWaitFor("S,0", OK); 
    }

    /**
      * Sends a message to the dispenser to update the status, and
      * performs an additional check. This method should be called instead
      * of queryStatus when the application intends to print an item after
      * getting the updated status. This method does what queryStatus does,
      * and additionally checks to see if it is time for the extra
      * VERIFY_REQUIRED that happens on the third document following
      * a load. (To be precise, on the first transaction after the
      * second document has been printed.)
      *
      * If the extended status bits indicate an error, a dispenser
      * status diag is written. After the extended status bits are
      * examined, they are cleared by sending the dispenser the
      * reset status command.
      *
      * @return the status of the original query (not the reset command)
      */
    @Override
	public int queryStatusWithIntent() {
        int result;
        queryStatus();
        if (statusBit(MISSED_TOP_OF_FORM)) {
            // FUTURE there's a bug here that could be fixed someday.
            // If the status is already LOAD REQD we probably shouldn't
            // set it to VERIFY REQD like this.
            setVerifyRequired();
        }

        if ((status == OK)
            || (status == VERIFY_REQUIRED)
            || (status == VOIDED)) {
            if ((!reloadVerifyDone) && (reloadVerifyCounter >= 2)) {
                setVerifyRequired();
                reloadVerifyDone = true;
            }
        }

        result = status;

        // If I'm reading the DNet source right, this is the set of
        // conditions that deserve a 203 diag. (From check_printer_status
        // in printer.c)
        if (statusBit(NOT_IDLE_OR_FAIL
            | IMPROPER_3RD_MARK
            | THIRD_MARK_ERROR
            | MID_MARK_ERROR
            | MISSED_TOP_OF_FORM
            | INCORRECT_PASSWORD
            | PRINT_HEAD_JAM
            | AMOUNT_NOT_VALID
            | COMPARTMENT_HAS_BEEN_OPENED
            | COMPARTMENT_IS_OPEN
            | POWER_FAIL)) {
            write203();
        }

        // Don't have to reset all the time, just if a resetable bit is set.
        if (statusBit(PRINT_HEAD_JAM
            | INCORRECT_PASSWORD
            | AMOUNT_NOT_VALID
            | POWER_FAIL
            | IMPROPER_3RD_MARK
            | COMPARTMENT_HAS_BEEN_OPENED
            | TEST_MODE
            | LAST_ITEM_VOID
            | MID_MARK_ERROR
            | ROM_FAILED
            | RAM_FAILED
            | SWITCH_ONE_FAILED
            | SWITCH_TWO_FAILED)) {
            resetStatus();
        }

        setStatus(result);
        return status;
    }

    /**
      *  Send a message to the dispenser to reset the status, then return it.
      */
    private int resetStatus() {
        if (DEBUG)
            Debug.println("resetStatus()"); 

        if (writeCommandWaitFor("S,1", ACK) == ACK) { 
            setStatus(OK);
        }
        return status;
    }

    /**
      * Fetch the dispenser parameters.
      */
    public int getParameters() {
        if (writeCommandWaitFor("P", OK) == OK) { 
            cp();
            parametersAvailable = true;
            UnitProfile.getInstance().setDispenserId(dispenserSerialNumber);
        }
        return status;
    }

    /**
      * Download the "VOID" logo if is hasn't been already.	
      *
      * The logo is downloaded if (1) the extended status shows that
      * graphics are not available, (2) an attempt to query the checksum
      * of the available graphics fails, or (3) the checksum returned
      * for the available graphics doesn't match the one we want.
      *
      * Right now, only the VOID logo is supported. The checksum for
      * the VOID logo is 23435. Other logos will have different checksums,
      * for example the GC logo's checksum is 13634.
      */
    public int downloadLogo() {
        String p;
        StringBuffer sb = new StringBuffer();
        if (!parametersAvailable) {
            getParameters();
            queryStatus();
        }

        p = new String(const0);
        if ((statusBit(GRAPHICS_AVAILABLE) == false)
            || (writeCommandWaitFor("G," + p + ",0", OK) != OK)  
            || (graphicsChecksum != 23435)) {

            // download the logo
            sb.append("G," + p + ",1");  
            for (int i = 0; i < voidLogo.length; i++) {
                sb.append(","); 
                sb.append(Byte.toString(voidLogo[i]));
            }
            if (DEBUG) {
                Debug.println("downloadLogo()"); 
            }
            if (writeCommandWaitFor(sb.toString(), ACK) == ACK) {
                setStatus(OK);
            }
        }
        else {
            setStatus(OK);
        }
        return status;
    }

    /**
      * Backs out the form currently loaded in the dispenser.
      * @return OK if the back-out succeeds, but a subsequent call to
      * getStatus() will return LOAD_REQUIRED.
      */
    @Override
	public int backOutForm() {
        String p;

        if (!parametersAvailable)
            getParameters();

        p = new String(const0);
        setLoadRequired();
        if ((writeCommandWaitFor("M," + p + ",-255", ACK) == ACK)  
            && (writeCommandAndWait("C," + p + ",1", ACK, WAIT_FOR_BACKOUT)  
            == ACK)) {
            setStatus(LOAD_REQUIRED);
            return OK;
        }
        else {
            return status;
        }
    }

    /**
      * Ejects the form currently loaded in the dispenser. This should
      * only be called when the current form is the last form.
      * @return OK on success, but a subsequent call to getStatus()
      * will return LOAD_REQUIRED.
      */
    @Override
	public int ejectForm() {
        String p;

        if (!parametersAvailable)
            getParameters();

        p = new String(const0);
        setLoadRequired();
        if (writeCommandWaitFor("M," + p + ",+250", ACK) == ACK) {  
            return OK;
        }
        else {
            return status;
        }
    }

    /**
      * Voids the form currently in the dispenser. Does not check to
      * see if a form is actually loaded first.
      * @return VOIDED if the form is sucessfully voided.
      */
    private int quickVoid() {
        if (UnitProfile.getInstance().isDemoMode() && getStatus() ==DispenserInterface.NO_CTS){
            // Do not increment the serial number
        } else
            incrementSerialNumber();

        writeCommandWaitFor("Q,2", ACK); 
        waitUntilDone();

        if (statusBit(LAST_ITEM_VOID)) {
            setStatus(VOIDED);
        }
        return status;
    }

    /**
      * Voids the form currently in the dispenser. Makes sure
      * a form is actually loaded first.
      * @return VOIDED if the form is sucessfully voided.
      */
    private int voidForm() {
        if (isFormDetected()) {
            quickVoid();
        }
        else {
            setLoadRequired();
        }
        return status;
    }

    /**
     * Print a test void or a last item void.
     * This is for compatibility with DeltaworksMainPanel and ClientTransaction.
     * It would be nice to migrate to the new void methods added in 4.2.
     */
    @Override
	public int fullVoidForm(int userID, boolean doQuickly, MoneyOrderVoidReasonCodeType reason) {
        int printStatus;
        String sn = getNextSerialNumber();

        if (doQuickly)
            printStatus = quickVoid();
        else
            printStatus = voidForm();

      if (printStatus == VOIDED) {

          setupTestVoidIsi(userID, sn);
      }

      if (printStatus == VOIDED) {
          writeVoidRecordToLogs(sn, userID, reason);
      }

        return printStatus;
    }

    /**
     *  writeVoidRecordToLogs
     */
	@Override
	public void writeVoidRecordToLogs(String serialNumber, int userID, MoneyOrderVoidReasonCodeType reason) {

        // setup data in moneyorderinfo40 and moneyorderdisplayfields
        MoneyOrderInfo mo = new MoneyOrderInfo();
        mo.setEmployeeID(BigInteger.valueOf(userID));
        mo.setSerialNumber(new BigInteger(serialNumber));
        mo.setVoidFlag(true);
        mo.setItemAmount(BigDecimal.ZERO);
        mo.setItemFee(BigDecimal.ZERO);
        mo.setDocumentType(BigInteger.valueOf(ReportLog.VOID_TYPE));
        mo.setVoidReasonCode(reason); // dispenser_error 
        mo.setPrintStatus(MoneyOrderPrintStatusType.CONFIRMED_PRINTED);
        mo.setDateTimePrinted(TimeUtility.toXmlCalendar(new Date()));
        mo.setPeriodNumber(new BigInteger(String.valueOf(UnitProfile.getInstance().getCurrentPeriod())));
        mo.setSaleIssuanceTag(BigInteger.ZERO);
        mo.setDiscountPercentage(BigDecimal.ZERO);
        mo.setDiscountAmount(BigDecimal.ZERO);
        mo.setAccountingStartDay(new BigInteger(UnitProfile.getInstance().get("ACCOUNTING_START_DAY", "0")));
        MoneyOrderDisplayFields modf = new MoneyOrderDisplayFields();
        modf.setTrainingFlag(UnitProfile.getInstance().isTrainingMode());
        if (UnitProfile.getInstance().isDemoMode()) // skip all report log writes in demo mode
            return;
        // write to report log
        ReportLog.logMoneyOrder(mo, modf);

        ComplianceTransactionRequest ctri = new ComplianceTransactionRequest();
        ctri.setRequestType(ComplianceTransactionRequestType.COMPLETED_WITH_CHANGES);
        ctri.getMoneyOrder().clear();
        ctri.getMoneyOrder().add(mo);
        // write to CCILog
        try {
            CCILog.getInstance().write(ctri);
        }
        catch (IOException e) {
            Debug.printStackTrace(e);
        }
    }

    // Write entry for each MoneyOrder into the MGRAM.TRN file. Unlike
    // the setupISI functions in MoneyGramSendTransaction and
    // MoneyGramReceiveTransaction no records are written to the DWISI.TRN
    // or DWISI.CSV files.
    private void setupTestVoidIsi(int userID, String sn) {
        String[] moItems = {
            "1",    // Document Type
            "",     // Document Number
            "V",    // Void Flag
            "L",    // Limbo Flag
            "",     // Period
            sn,     // Serial Number
            "0",    // Item Face Amount
            "0",    // Item Fee
            "",     // Sales Issuance Tag
            "",     // Reserved for future use
            "",     // Fee Discount Amount
            "",     // Reserved for future use
            "0",    // Fee Discount Percentage
            "",     // Vendor Payment, Payee Number
            "",     // Vendor Payment, Payee name
            "",     // Purchaser
            ""      // Reference Number
        };

        List<String> fields = new ArrayList<String>();
        IsiReporting isi = new IsiReporting();

        fields.add("DOC");     // Record Type
        getIsiHeader(userID, fields);

        for(int i=0; i<moItems.length; i++){
            fields.add(moItems[i]);
        }

        isi.print(fields, isi.ISI_LOG);
        if (MainPanelDelegate.getIsiExeCommand())
            isi.print(fields, isi.EXE_LOG);
  }
  public void getIsiHeader(int userID, List<String> fields) {
	  String[] headerItems = {(UnitProfile.getInstance().isTrainingMode() ? "T" : (UnitProfile.getInstance().isDemoMode() ? "D" : "P")),
			  new SimpleDateFormat(TimeUtility.ISI_DATE_TIME_FORMAT).format(TimeUtility.currentTime()),
          Integer.toString(userID<0 ? 0 : userID),
          "",
          UnitProfile.getInstance().get("MG_ACCOUNT","NO_MG_ACCOUNT"),
          UnitProfile.getInstance().get("AGENT_ID","NO_AGENT_ID")};

      for(int i=0; i<headerItems.length; i++){
          fields.add(headerItems[i]);
      }

      return;
  }

    /**
     * Void the current form. But if the cover is open, back it out instead.
     * And if the cover is closed but has been open, check for form presence
     * before voiding it, to prevent air voids.
     *
     * @return true if current form was voided ok
     */
    @Override
	public boolean voidFirstRemainingForm() {
        queryStatus();
        if (statusBit(COMPARTMENT_IS_OPEN)) {
            backOutForm();
            return false;
        }
        else if (statusBit(COMPARTMENT_HAS_BEEN_OPENED)) {
            if (! isFormDetected()) {
                return false;
            }
        }

        return quickVoid() == VOIDED;
    }

    /**
     * Void the current form unless the cover is open.
     *
     * @return true if current form was voided ok
     */
    @Override
	public boolean voidNextRemainingForm() {
        // Don't need another queryStatus because previous call
        // should have been to voidFirstRemainingForm, which
        // ends with quickVoid, which does a queryStatus.

        if (statusBit(COMPARTMENT_IS_OPEN)) {
            return false;
        }
        return quickVoid() == VOIDED;
    }

//    public int fullVoidForm(
//        final int userID,
//        final boolean doQuickly,
//        final short reason,
//        boolean commit) {
//
//        int printStatus;
//        final String serialNumber = getNextSerialNumber();
//
//        ClientInfo co = new ClientInfo();
//        MoneyOrderInfo mo =
//            new MoneyOrderInfo(
//                ReportLog.VOID_TYPE,
//                userID,
//                ZERO_DECIMAL,
//                ZERO_DECIMAL);
//
//        mo.setSerialNumber(serialNumber);
//        mo.setVoidFlag(true, reason);
//        mo.setDocumentType(String.valueOf(ReportLog.VOID_TYPE));
//        //mo.setVoidReason(String.valueOf( DispenserInterface.VOIDED));
//        mo.setPrinted(false); // in limbo
//        mo.setLimbo(true);
//
//        try {
//            TempLog.writeTempLogMoneyOrder(mo, co);
//        }
//        catch (java.io.InvalidClassException e) {
//            ErrorManager.handle(e, "moWriteTempLogMoneyOrderIOException", 
//            ErrorManager.ERROR, true);
//        }
//        catch (java.io.NotSerializableException e) {
//            ErrorManager.handle(e, "moWriteTempLogMoneyOrderIOException", 
//            ErrorManager.ERROR, true);
//        }
//        catch (java.io.IOException e) {
//            ErrorManager.handle(e, "moWriteTempLogMoneyOrderIOException", 
//            ErrorManager.ERROR, true);
//        }
//
//        if (doQuickly)
//            printStatus = quickVoid();
//        else
//            printStatus = voidForm();
//
//        switch (printStatus) {
//            case DispenserInterface.VOIDED :
//            case DispenserInterface.OK :
//                DiagLog.getInstance().writeFinancialDocVoid(
//                    (short) 0,
//                    MoneyOrderInfo.fDocVoidDiagReasonCode[reason],
//                    (short) userID,
//                    serialNumber);
//                if (commit)
//                    TempLog.commitTempLog();
//        }
//        return printStatus;
//    }

    /**
      * Gets the number of items that (we think) are left in the dispenser.
      */
    @Override
	public int getRemainingCount() {
        if (!isOn() && UnitProfile.getInstance().isDemoMode())
            remaining = 1000;
        return (int) remaining;
    }

    /**
      * Returns true if this is a valid serial number. The check digit is
      * computed and compared to the last digit of the given number.
      */
    @Override
	public boolean isValid(long serial) {
        long withoutCheck = serial / 10;
        long check = serial % 10;
        if (check == ((withoutCheck % 11) % 10))
            return true;
        else
            return false;
    }

    /**
      * Returns true if the given number matches the last four digits
      * of the next serial number. If they do match, the status is
      * updated to clear a VERIFY_REQUIRED condition.
      * @return true if the numbers match the next serial number
      * either with or without the check digit.
      * After 3 tries, set LOAD_REQUIRED.
      */
    @Override
	public boolean verify(long last4) {
        long n = nextSerialNumber;
        if (DEBUG) {
            Debug.println("verify() last4 = " + last4); 
            Debug.println("verify() n % 10000 = " + (n % 10000)); 
        }
        if (((n % 10000) == last4)
            || (((n * 10) + checkDigit(n)) % 10000) == last4) {
            verifyOnRetract = true;
            return true;
        }
        else {
            verifyOnRetract = false;
            if (++retries >= 3)
                setLoadRequired();
            return false;
        }
    }

    /**
     * Computes check digit.
     */
    private static long checkDigit(long serial) {
        return (serial % 11) % 10;
    }

    /**
     * Adds check digit to the end of a serial number.
     */
    private static String withCheckDigit(long serial) {
        String s = Long.toString(serial);
        return s + checkDigit(serial);
    }

    /**
     * Returns the serial number that the next item will print on.
     * The check digit is not included.
     */
    @Override
	public String getNextSerialNumber() {
        return Long.toString(nextSerialNumber);
    }

    /**
     * Returns the serial number that the next item will print on.
     * The check digit is included.
     */
    @Override
	public String getNextSerialNumberWithCheckDigit() {
        return withCheckDigit(nextSerialNumber);
    }

    /**
     * Returns the last serial number loaded in the dispenser, according
     * an earlier call to setSerialNumberRange.
     * The check digit is included.
     */
    @Override
	public String getLastSerialNumberWithCheckDigit() {
        return withCheckDigit(nextSerialNumber + remaining);
    }

    /**
     * Increment our saved value of the next serial number.
     * Note: In versions 2.9 thru 4.0 this method did not mark the
     * serial number used, forcing the caller to call beenUsed().
     * In version 4.2 this method calls beenUsed again.
     */
    private void incrementSerialNumber() {
        beenUsed(nextSerialNumber);
        ++nextSerialNumber;
        --remaining;
        if (!reloadVerifyDone)
            ++reloadVerifyCounter;

        saveState();
    }

    /**
     * Decrement the saved value of the next serial number.
     * Called after incrementSerialNumber() to indicate that the number
     * wasn't used after all.
     */
    private void decrementSerialNumber() {
        --nextSerialNumber;
        notBeenUsed(nextSerialNumber);
        ++remaining;
        if (!reloadVerifyDone)
            --reloadVerifyCounter;

        saveState();
    }

    /**
      * Sets the serial number range and talks to the device to return the
      * updated status. This has the effect of clearing a LOAD_REQUIRED
      * condition. Check digits must be included on these serial numbers.
      * @return VERIFY_REQUIRED unless there is a more serious problem
      * like NO_CTS. Return LOAD_REQUIRED if there is a match against
      * previously printed serial numbers.
      */
    @Override
	public int setSerialNumberRange(long s1, long s2) {
        long temp;
        if (s1 > s2) {
            temp = s1;
            s1 = s2;
            s2 = temp;
        }

        if (inUse(s1, s2)) {
            setLoadRequired();
            return status;
        }

        nextSerialNumber = s1 / 10;
        remaining = (s2 / 10) - nextSerialNumber;
        reloadVerifyCounter = 0;
        reloadVerifyDone = false;
        verifyRequired = true;
        loadRequired = false;
        saveState();
        resetStatus();
        return queryStatus();
    }

    /** This turns off the extra verification that happens after a load
      * operation. The purpose is to skip an unnecessary verify at application
      * startup time when the serial numbers are read from saved state.
      */
    @Override
	public void skipExtraVerify() {
        reloadVerifyDone = true;
    }

    /**
      * Indicates that the last 4 digits of the serial number
      * have been verified. Talks to the device to return the
      * updated status. This has the effect of clearing a
      * VERIFY_REQUIRED condition.
      */
    @Override
	public int verified() {
        verifyRequired = false;
        loadRequired = false;
        resetStatus();
        return queryStatus();
    }

    /**
      * Talks to the device to see if it's locked.
      * @return true if the cover is locked; false if it isn't.
      */
    @Override
	public boolean isLocked() {
        queryStatus();
        if (statusBit(COMPARTMENT_IS_OPEN))
            return false;
        else
            return true;
    }

    /**
      * Advance the form 1 inch, then retract it to determine if a
      * form is actually loaded.
      * @return true if a form is detected
      */
    public boolean isFormDetected() {
        boolean result = false;

        if (!parametersAvailable)
            getParameters();

        writeCommandWaitFor("M," + new String(const0) + ",+72", ACK);  
        waitUntilDone();
        if (statusBit(TOP_OF_FORM) == false)
            result = true;
        retractTOF();
        return result;
    }

    @Override
	public int extend() {
        verifyOnRetract = false;
        retries = 0;
        if (writeCommandWaitFor("D,+99", ACK) == ACK) { 
            setStatus(OK);
        }
        return status;
    }

    @Override
	public int retract() {
        // This command is unusual in that it takes a while
        // before the ACK comes back.
        writeCommandAndWait("D,-", ACK, WAIT_FOR_MOTION); 

        if (status == ACK) {
            setStatus(OK);

            if (verifyOnRetract) {
                verifyOnRetract = false;
                verified();
            }
        }

        return status;
    }

    public int retractTOF() {
        writeCommandWaitFor("B", ACK); 

        if (status == ACK)
            waitUntilDone();

        return status;
    }

    /**
     * Issue a status command, and wait 45 seconds for a reply.
     * This is used to wait until a paper-movement command has
     * completed. They take a while, but return ACK immediately.
     */
    private int waitUntilDone() {
        if (DEBUG)
            Debug.println("waitUntilDone()"); 

        writeCommandAndWait("S,0", OK, WAIT_FOR_MOTION); 
        return status;
    }

    @Override
	public String getDispenserSerialNumber() {
        // return uncached value so transactions can use
        // to double-check that the dispenser hasn't been
        // swapped
        //if (! parametersAvailable)
        //    getParameters();
        dispenserSerialNumber = ""; 
        getParameters();
        return dispenserSerialNumber;
    }

    @Override
	public String getModel() {
        if (!parametersAvailable)
            getParameters();
        return model;
    }

    @Override
	public String getManufactureDate() {
        if (!parametersAvailable)
            getParameters();
        return manufactureDate;
    }

    @Override
	public String getMlc() {
        if (!parametersAvailable)
            getParameters();
        return mlc;
    }

    /**
      * Registers the state listener which will record loaded and used
      * serial numbers. Only one state listener is currently supported,
      * so the given listener will replace any existing listener.
      */
    @Override
	public void addStateListener(StateListener listener) {
        this.listener = listener;
    }

    /**
      * Loads the saved serial number information.
      */
    @Override
	public void loadState(byte state[]) {
        ByteArrayInputStream bais = new ByteArrayInputStream(state);
        DataInputStream in = new DataInputStream(bais);

        try {
            nextSerialNumber = in.readLong();
            remaining = in.readLong();

            for (int i = 0; i < USED_PAIRS; i++) {
                usedStart[i] = in.readLong();
                usedEnd[i] = in.readLong();
            }

            reloadVerifyCounter = 0;
            reloadVerifyDone = false;
            verifyRequired = true;
            loadRequired = false;
        }
        catch (IOException e) {
            nextSerialNumber = 0;
            remaining = 0;
            for (int i = 0; i < USED_PAIRS; i++) {
                usedStart[i] = 0;
                usedEnd[i] = 0;
            }
        }
    }

    /** Saves state information. */
    private void saveState() {
        if (listener == null)
            return;

        ByteArrayOutputStream baos =
            new ByteArrayOutputStream(STATE_DATA_LENGTH);
        DataOutputStream out = new DataOutputStream(baos);
        try {
            out.writeLong(nextSerialNumber);
            out.writeLong(remaining);
            for (int i = 0; i < USED_PAIRS; i++) {
                out.writeLong(usedStart[i]);
                out.writeLong(usedEnd[i]);
            }
            listener.stateChanged(baos.toByteArray(), 0, out.size());
            out.close();
        }
        catch (IOException e) {
            // Probably ought to hook into general error handling,
            // but it's no big deal, because there's no money or critical
            // data involved. And it would confuse the caller, who's only
            // really interested in whether the document printed.
        }
    }

    /**
     * Return true if s1 is within any range marked as being used.
     * @param s1 the starting serial number to check, with check digit
     * @param s2 the ending serial number to check, with check digit
     */
    @Override
	public boolean inUse(long s1, long s2) {
        Debug.println("Dispenser.inUse(" + s1 + ", " + s2 + ")");
        s1 = s1 / 10;
        s2 = s2 / 10;   // drop check digits

        for (int i = 0; i < USED_PAIRS; i++) {
            if ((usedStart[i] <= s1) && (usedEnd[i] >= s1)) {
                return true;
            }
            if ((usedStart[i] <= s2) && (usedEnd[i] >= s2)) {
                return true;
            }
            if ((s1 < usedStart[i]) && (usedEnd[i] < s2)) {
                return true;
            }
        }

        return ExtraRanges.getInstance().inUse(s1, s2);
    }

    /**
     * Marks the given serial number as having been used.
     * @param s serial number without check digit
     */
    private void beenUsed(long s) {
        long start, end;
        int i, j;
        if (s <= 0) {
            return;
        }

        // The way this works is that all serial numbers between
        // usedStart[n] and usedEnd[n] inclusive have been used.

        if (usedEnd[0] == s - 1) {
            ++usedEnd[0];
            return;
        }

        // The pair we want isn't first, so find it or create
        // it and shove everything else down.

        start = s;
        end = s;

        for (i = 1; i < USED_PAIRS; i++) {
            if (usedEnd[i] == s - 1) { // found it
                start = usedStart[i];
                end = s;
                break;
            }
        }

        if (i == USED_PAIRS) { // not found, have to set up a new pair
            --i;
            // move the oldest range to ExtraRanges before it rolls off
            ExtraRanges.getInstance().addRange(usedStart[i], usedEnd[i]);
        }

        for (j = i; j > 0; j--) {
            usedStart[j] = usedStart[j - 1];
            usedEnd[j] = usedEnd[j - 1];
        }

        // The arrays are sorted in most-recently-used
        // order, with the current one first.
        usedStart[0] = start;
        usedEnd[0] = end;

        return;
    }

    /**
     * Marks a serial number as not having been used after all.
     * This method is only expected to be called where the given
     * serial number was the parameter to the previous call to
     * beenUsed().
     */
    void notBeenUsed(long s) {
        if (s != usedEnd[0]) {
            Debug.println("notBeenUsed(" + s + ") call out of order");
            return;
        }

        if (usedStart[0] == s) {    // must have just set up new pair
            usedStart[0] = 0;
            usedEnd[0] = 0;
        }
        else {
            --usedEnd[0];
        }
    }

    /**
     * Prints a money order.
     * Returns ITEM_OK if the document printed, ITEM_LIMBO if the print status
     * is unknown, ITEM_VOIDED if there was a dispenser void (the dispenser
     * detected an error during print, retracted the document, and voided it),
     * and ITEM_NOT_PRINTED if the document did not print. To reduce confusion
     * and for compatibility with existing code such as MoneyOrderInfo.isLimbo(),
     * the serial number is set back to the empty string if the document did
     * not print.
     *
     * The previous return values were as follows (delete this after done):
     * A return value of OK means the document printed. LOAD_REQUIRED
     * and VERIFY_REQUIRED mean the document did not print. VOIDED
     * means the document was printed but unexpectedly voided. Any other
     * return means the method can not be sure if the document printed.
     */
    @Override
	public synchronized int printItem(
        MoneyOrderInfomation mo,
        ClientInfo co,
        String storename) {

        StringBuffer command = new StringBuffer();
        StringBuffer temp = new StringBuffer();
        String serialWithoutCheckDigit = getNextSerialNumber();
        String p;

        if (!isOn() && UnitProfile.getInstance().isDemoMode()) {
            return ITEM_OK; // seems odd, but that's what it's been doing
        }

        if (this.nextSerialNumber == 0)
            setLoadRequired();

        if (remaining < 1)
            setLoadRequired();

        if (loadRequired) {
            setStatus(LOAD_REQUIRED);
            return ITEM_NOT_PRINTED;
        }

        if (verifyRequired) {
            setStatus(VERIFY_REQUIRED);
            return ITEM_NOT_PRINTED;
        }

        if (!parametersAvailable)
            getParameters();

        if (mo.getVoidFlag() || mo.getTrainingFlag()) {
            if (downloadLogo() != OK) {
                setLoadRequired();
                return ITEM_NOT_PRINTED;
            }
        }

        mo.setSerialNumber(new BigInteger(serialWithoutCheckDigit));

        p = new String(const0);
        command.append("A,"); 
        command.append(p);
        command.append(","); 
        String dispenserAmount = mo.getAmount().movePointRight(2).toString();
        if(dispenserAmount.indexOf('.') > 0)
            dispenserAmount = dispenserAmount.substring(0,dispenserAmount.indexOf('.'));
        command.append(dispenserAmount);
        command.append(","); 

        //decide if there should be a mark or not
        if (DEBUG) {
            Debug.println("printItem() nextSerialNumber = " 
                + nextSerialNumber + " 3rd mark = " 
                + ((nextSerialNumber % 3 == 0) ? "Y" : "N"));  
        }

        command.append((nextSerialNumber % 3 == 0) ? "Y" : "N");  
        command.append(","); 
        command.append(co.getAccountNumber());
        command.append(","); 

        temp.setLength(0);
        temp.append(co.getUnitNumber());
        temp.append("        "); 
        command.append(temp.substring(3, 8));

        temp.setLength(0);
        temp.append(mo.getEmployeeID());
        temp.append("        "); 
        command.append(temp.substring(0, 2));

        command.append(new SimpleDateFormat(TimeUtility.DAY_OF_YEAT_FORMAT).format(new Date(mo.getCreationDateTime())));
        command.append(Scheduler.formatHHM(mo.getCreationDateTime()));

        temp.setLength(0);
        temp.append(serialWithoutCheckDigit);
        command.append(temp.substring(temp.length() - 3, temp.length()));
        command.append(","); 

        command.append("2,"); 
        command.append(serialWithoutCheckDigit);
        command.append(" "); 
        command.append(mo.getDeltagramFlag());
        command.append(","); 
        DateFormat df = new SimpleDateFormat(TimeUtility.DATE_MONTH_DAY_LONG_YEAR_FORMAT);
        command.append(df.format(new Date(mo.getCreationDateTime())));
        command.append(","); 
        command.append(mo.getDocumentName());
        command.append(","); 
        command.append(((mo.getVoidFlag() || mo.getTrainingFlag()) ? "Y" : "N"));  
        command.append(","); 

        // stub line 1
        temp.setLength(0);
        temp.append(co.getAccountNumber());
        rightJustify(temp, mo.getEmployeeID());
        command.append(temp);
        command.append(","); 

        // stub line 2 is blank
        command.append(","); 

        // stub line 3
        temp.setLength(0);
        temp.append(Scheduler.formatHHM(mo.getCreationDateTime()));
        temp.append("   "); 
        temp.append(mo.getMultiIssueIndicator());
        temp.append(mo.getLimitIndicator());
        rightJustify(temp, "$" + mo.getAmount().toString()); 
        command.append(temp);
        command.append(","); 

        //stub line 4
        temp.setLength(0);
        temp.append(serialWithoutCheckDigit);
        rightJustify(temp, df.format(new Date(mo.getCreationDateTime())));
        command.append(temp);
        command.append(","); 

        command.append(sanitize(mo.getPayeeName()));
        command.append(",,"); 
        command.append(storename);
        command.append(",,"); 

        int previousHardwareItemCount = hardwareItemCount;

        if (DEBUG) {
            Debug.println("printItem() debug sleep before print command"); 
            try {
                Thread.sleep(2000);
            }
            catch (InterruptedException e) {
            }
        }

        // This last-minute check should immediately preceed writeCommandWaitFor().
        if (!goodToGo()) {
            mo.setSerialNumber(null); // see above	
            return ITEM_NOT_PRINTED;
        }

        // have to do this now in case they pull the plug during printing
        incrementSerialNumber();

        if (writeCommandWaitFor(command.toString(), ACK) != ACK) {
            Debug.println("expected ACK, got status = " + status); 
        }

        boolean updatedStatus = false;
        boolean voidFailed = false;

        if (DEBUG)
            Debug.println("printItem() checking status"); 
        writeCommandAndWait("S,0", OK, WAIT_FOR_PRINT); 
        if (DEBUG)
            Debug.println("printItem() status = " + status); 

        if (status == OK) {
            updatedStatus = true;
            voidFailed = handleErrorConditions(p);
        }
        else {
            // repeat the status command
            Debug.println("checking status again"); 
            writeCommandWaitFor("S,0", OK); 
            if (DEBUG)
                Debug.println("printItem() status = " + status); 
            if (status == OK) {
                updatedStatus = true;
                voidFailed = handleErrorConditions(p);
            }
        }

        /*
         * Update the attempted flag and finalize the serial number. If
         * the status command worked and the hardware count incremented,
         * we either printed ok or we tried to void. If the void failed,
         * we're limbo. If we couldn't get the status command to work,
         * we're limbo. If the status command worked and the hardware
         * count is unchanged, nothing printed.
         */

        if (updatedStatus
            && (previousHardwareItemCount == hardwareItemCount)) {
            mo.setSerialNumber(null); // see above	
            decrementSerialNumber();
            return ITEM_NOT_PRINTED;
        }
        else {
            //beenUsed(nextSerialNumber - 1);
            mo.setAttempted(true);

            if (!updatedStatus) {
                return ITEM_LIMBO;
            }

            if (voidFailed) {
                return ITEM_LIMBO;
            }

            if (statusBit(LAST_ITEM_VOID)) {
                return ITEM_VOIDED;
            }

            return ITEM_OK;
        }
    }

    /**
     * For compatibility with current implementation of MoneyOrderTransaction40.
     */
    @Override
	public int printItem(
			MoneyOrderInfo mo,
        MoneyOrderDisplayFields modf,
        ClientInfo co,
        String storename) {

        // Renuka: copy the next three lines into MOT40, add a line
        // to MOT40 to set the training flag, and call printItem(mo, modf)
        // instead of this method.

        modf.setAccountNumber(co.getAccountNumber());
        modf.setUnitNumber(co.getUnitNumber());
        modf.setPurchaserName(storename);

        return printItem(mo, modf);
    }

    /**
     * Prints a money order.
     * Returns ITEM_OK if the document printed, ITEM_LIMBO if the print status
     * is unknown, ITEM_VOIDED if there was a dispenser void (the dispenser
     * detected an error during print, retracted the document, and voided it),
     * and ITEM_NOT_PRINTED if the document did not print. To reduce confusion
     * and for compatibility with existing code such as MoneyOrderInfo.isLimbo(),
     * the serial number is set back to the empty string if the document did
     * not print.
     *
     * Note: This method used to take a ClientInfo co and a String storename.
     * The co was being used to pass in the account number and unit number.
     * These are now in the MoneyOrderDisplayFields, along with the store name
     * (as the purchaser) and a new training flag.
     */
    @Override
	public synchronized int printItem(
        MoneyOrderInfo mo,
        MoneyOrderDisplayFields modf) {

        StringBuffer command = new StringBuffer();
        StringBuffer temp = new StringBuffer();
        String serialWithoutCheckDigit = getNextSerialNumber();
        String p;

        if (!isOn() && UnitProfile.getInstance().isDemoMode()) {
            return ITEM_OK; // seems odd, but that's what it's been doing
        }

        if (this.nextSerialNumber == 0)
            setLoadRequired();

        if (remaining < 1)
            setLoadRequired();

        if (loadRequired) {
            setStatus(LOAD_REQUIRED);
            return ITEM_NOT_PRINTED;
        }

        if (verifyRequired) {
            setStatus(VERIFY_REQUIRED);
            return ITEM_NOT_PRINTED;
        }

        if (!parametersAvailable)
            getParameters();

        if (mo.isVoidFlag() || modf.isTrainingFlag()) {
            if (downloadLogo() != OK) {
                setLoadRequired();
                return ITEM_NOT_PRINTED;
            }
        }

        mo.setSerialNumber(new BigInteger(serialWithoutCheckDigit));

        p = new String(const0);
        command.append("A,"); 
        command.append(p);
        command.append(","); 
        String dispenserAmount = mo.getItemAmount().movePointRight(2).toString();
        if(dispenserAmount.indexOf('.') > 0)
            dispenserAmount = dispenserAmount.substring(0,dispenserAmount.indexOf('.'));
        command.append(dispenserAmount);
//        command.append(
//            new BigDecimal(mo.getItemAmount()).movePointRight(2).toString());
        command.append(","); 

        //decide if there should be a mark or not
        if (DEBUG) {
            Debug.println("printItem() nextSerialNumber = " 
                + nextSerialNumber + " 3rd mark = " 
                + ((nextSerialNumber % 3 == 0) ? "Y" : "N"));  
        }

        command.append((nextSerialNumber % 3 == 0) ? "Y" : "N");  
        command.append(","); 
        command.append(modf.getAccountNumber());
        command.append(","); 

        temp.setLength(0);
        temp.append(modf.getUnitNumber());
        temp.append("        "); 
        command.append(temp.substring(3, 8));

        temp.setLength(0);
        temp.append(mo.getEmployeeID());
        temp.append("        "); 
        command.append(temp.substring(0, 2));

        command.append(new SimpleDateFormat(TimeUtility.DAY_OF_YEAT_FORMAT).format(mo.getDateTimePrinted().toGregorianCalendar().getTime()));
        command.append(Scheduler.formatHHM(mo.getDateTimePrinted().toGregorianCalendar().getTimeInMillis()));

        temp.setLength(0);
        temp.append(serialWithoutCheckDigit);
        command.append(temp.substring(temp.length() - 3, temp.length()));
        command.append(","); 

        command.append("2,"); 
        command.append(serialWithoutCheckDigit);
        command.append(" "); 
        command.append(modf.getDeltagramFlag());
        command.append(","); 
        DateFormat df = new SimpleDateFormat(TimeUtility.DATE_MONTH_DAY_LONG_YEAR_FORMAT);
        command.append(df.format(mo.getDateTimePrinted().toGregorianCalendar().getTime()));
        command.append(","); 
        command.append(modf.getDocumentName());
        command.append(","); 
        command.append((mo.isVoidFlag() || modf.isTrainingFlag()) ? "Y" : "N");  
        command.append(","); 

        // stub line 1
        temp.setLength(0);
        temp.append(modf.getAccountNumber());
        rightJustify(temp, mo.getEmployeeID().toString());
        command.append(temp);
        command.append(","); 

        // stub line 2 is blank
        command.append(","); 

        // stub line 3
        temp.setLength(0);
        temp.append(Scheduler.formatHHM(mo.getDateTimePrinted().toGregorianCalendar().getTimeInMillis()));
        temp.append("   "); 
        temp.append(modf.getMultiIssueIndicator());
        temp.append(modf.getLimitIndicator());
        rightJustify(temp, "$" + mo.getItemAmount()); 
        command.append(temp);
        command.append(","); 

        //stub line 4
        temp.setLength(0);
        temp.append(serialWithoutCheckDigit);
        
        rightJustify(
            temp,
            new SimpleDateFormat(TimeUtility.DATE_MONTH_DAY_LONG_YEAR_FORMAT).format(mo.getDateTimePrinted().toGregorianCalendar().getTime()));
        command.append(temp);
        command.append(","); 

        command.append(sanitize(modf.getPayeeName()));
        command.append(",,"); 
        command.append(modf.getPurchaserName());
        command.append(",,"); 

        int previousHardwareItemCount = hardwareItemCount;

        if (DEBUG) {
            Debug.println("printItem() debug sleep before print command"); 
            try {
                Thread.sleep(2000);
            }
            catch (InterruptedException e) {
            }
        }

        // This last-minute check should immediately preceed writeCommandWaitFor().
        if (!goodToGo()) {
            mo.setSerialNumber(null); // see above	
            return ITEM_NOT_PRINTED;
        }

        // have to do this now in case they pull the plug during printing
        incrementSerialNumber();

        if (writeCommandWaitFor(command.toString(), ACK) != ACK) {
            Debug.println("expected ACK, got status = " + status); 
        }

        boolean updatedStatus = false;
        boolean voidFailed = false;

        if (DEBUG)
            Debug.println("printItem() checking status"); 
        writeCommandAndWait("S,0", OK, WAIT_FOR_PRINT); 
        if (DEBUG)
            Debug.println("printItem() status = " + status); 

        if (status == OK) {
            updatedStatus = true;
            voidFailed = handleErrorConditions(p);
        }
        else {
            // repeat the status command
            Debug.println("checking status again"); 
            writeCommandWaitFor("S,0", OK); 
            if (DEBUG)
                Debug.println("printItem() status = " + status); 
            if (status == OK) {
                updatedStatus = true;
                voidFailed = handleErrorConditions(p);
            }
        }

        /*
         * Update the attempted flag and finalize the serial number. If
         * the status command worked and the hardware count incremented,
         * we either printed ok or we tried to void. If the void failed,
         * we're limbo. If we couldn't get the status command to work,
         * we're limbo. If the status command worked and the hardware
         * count is unchanged, nothing printed.
         */

        if (updatedStatus
            && (previousHardwareItemCount == hardwareItemCount)) {
            mo.setSerialNumber(null); // see above	
            decrementSerialNumber();
            return ITEM_NOT_PRINTED;
        }
        else {
            if (!updatedStatus) {
                return ITEM_LIMBO;
            }

            if (voidFailed) {
                return ITEM_LIMBO;
            }

            if (statusBit(LAST_ITEM_VOID)) {
                return ITEM_VOIDED;
            }

            return ITEM_OK;
        }
    }
    /**
     * Try to put the dispenser back in ready-to-print status. Try to
     * recover from common errors, and set flags based on readiness to
     * print another item. Void a just-printed item if appropriate. If
     * such a void fails, return true.
     */
    private boolean handleErrorConditions(String p) {
        if (statusBit(IMPROPER_3RD_MARK)) { // did not print
            setVerifyRequired();
        }
        /* Detect printer jam while printing in progess. Attempt to void the
           form by backing up to top-of-form and doing a quick void, but if
           that also fails the item will have to be treated as a limbo.

           Same logic applies to power fail while printing in progress, though
           that usually causes a mid mark error anyway and the quick void is
           more likely to fail by missing top of form.

           To test, clamp down on both ends of the form during a print, and
           don't let go until the dispenser stops making noise. This causes
           a midmark error on the print, and a missed TOF error on the void.
        */
        else if (statusBit(MID_MARK_ERROR) || statusBit(NOT_IDLE_OR_FAIL)) {
            setVerifyRequired();
            writeCommandWaitFor("B", ACK); 
            waitUntilDone();
            writeCommandWaitFor("S,1", ACK); 
            writeCommandWaitFor("Q,2", ACK); 
            waitUntilDone();
            if (statusBit(LAST_ITEM_VOID)) {
                setStatus(VOIDED);
            }
            else {
                setLoadRequired(); // for example, retract missed TOF
                return true;
            }
        }
        /* The Dnet responds to MISSED_TOP_OF_FORM by sending
           command B (backup to TOF) and returning success.
           But if we missed the TOF mark once, aren't we likely
           to miss it again? In an experiment, I tricked the DNet
           into sucking a valid, printed money order back to TOF,
           and that can't be good.

           So instead, I'm responding to MISSED_TOP_OF_FORM by
           retracting back to where TOF ought to be, and I added
           a TOF test to queryStatus so the problem (if it still
           exists) will be caught on the next print. Best case,
           it looks like nothing's wrong. Worst case, this is
           no worse that what Dnet does.
         */
        else if (statusBit(MISSED_TOP_OF_FORM)) {
            //manual move back to where TOF should have been
            //move back 106/72 = 1 7/16 inches
            if (DEBUG) {
                Debug.println("MISSED_TOP_OF_FORM"); 
            }
            writeCommandWaitFor("M," + p + ",-104", ACK);  
            waitUntilDone();
            writeCommandWaitFor("S,1", ACK); 
            setStatus(OK);
        }
        else if (statusBit(INCORRECT_PASSWORD)) {
            setLoadRequired();
        }
        else if (
            statusBit(LAST_ITEM_VOID)) { // I don't think this ever happens
            setStatus(VOIDED);
        }
        return false;
    }

    @Override
	public synchronized int printItem(MoneyOrderInfomation mo, ClientInfo co) {
        return printItem(mo, co, ""); 
    }

    private static void rightJustify(StringBuffer sb, String s) {
        int n = 28 - sb.length() - s.length(); // number of blanks to pad
        if (n < 0) {
            sb.setLength(sb.length() + n); // shorten sb if too long
        }
        else {
            for (int i = 0; i < n; i++) {
                sb.append(" "); 
            }
        }
        sb.append(s);
    }

    private static String sanitize(String s) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '\\')
                c = '/';
            if (c == ',')
                c = '^';
            sb.append(c);
        }
        return sb.toString();
    }

    /**
      * Writes a single byte to the port's output stream.
      * @see java.io.OutputStream
      */
    @Override
	public void write(int b) throws IOException {
        output.write(b);
    }

    /**
     * Does last-minute checking, returns true if ok to write the command.
     * Refactored from writeCommandWaitFor(), so printItem can just return
     * NOT_PRINTED if appropriate.
     */
    private boolean goodToGo() {
        // This used to also check (status == WAITING) but I took that out so
        // I could retry the status command if it times out after a print.

        if ((status == NOT_INITIALIZED) || (status == EOF)) {
            return false;
        }

        if (port == null) {
            setStatus(NOT_INITIALIZED);
            return false;
        }

        if (!port.isCTS()) {
        	Debug.println("Dispenser: No CTS");
        	ExtraDebug.println("goodToGo:");
            setStatus(NO_CTS);
            return false;
        }

        return true;
    }

    /**
      * Write a command and wait for an ack or a response.
      * @param command the command to send to the dispenser.
      * @param waitfor the status to wait for, typically ACK or OK.
    */
    private synchronized int writeCommandWaitFor(String command, int waitFor) {
        return writeCommandAndWait(command, waitFor, WAIT_FOR_COMMAND);
    }

    /**
      * Write a command and wait for an ack or a response.
      * @param command the command to send to the dispenser.
      * @param waitfor the status to wait for, typically ACK or OK.
      * @param millis the max amount of time to wait before returning.
      */
    private synchronized int writeCommandAndWait(
        String command,
        int waitfor,
        long millis) {

        if (!goodToGo())
            return status;

        try {
            writeCommand(command);
            waitFor(waitfor, millis);
        }
        catch (NullPointerException e) { // don't know how, but saw it once
            setStatus(NOT_INITIALIZED);
        }
        catch (IOException e) {
            setStatus(ERROR);
        }

        return status;
    }

    /**
      * Write a command to the port's output stream.
      */
    private void writeCommand(String s) throws IOException {
        if (DEBUG) {
            lastCommand = s;
        }
        writeCommand(s.getBytes());
    }

    /**
      * Write a command to the port's output stream.
      */
    private void writeCommand(byte[] data) throws IOException {
        int csum;

        setStatus(WAITING);

        write(ASCII_AT);
        csum = ASCII_AT;

        write(ASCII_BSLASH);
        csum += ASCII_BSLASH;

        for (int i = 0; i < data.length; i++) {
            write(data[i]);
            csum += data[i];
        }
        write(ASCII_BSLASH);
        csum += ASCII_BSLASH;

        write(Integer.toString(csum).getBytes());
        write(ASCII_CR);
        write(ASCII_LF);
    }

//    /**
//     * Copy source.length bytes from source to dest, starting at the
//     * given offset in dest.
//     */
//    private static void arrayCopy(byte[] dest, byte[] source, int offset) {
//        for (int i = 0; i < source.length; i++) {
//            dest[i + offset] = source[i];
//        }
//    }

    @Override
	public void run() {
        int i;
        try {
            while ((i = input.read()) != -1) {
                if (i == ASCII_ACK) {
                    setStatus(ACK);
                }
                process(i);
            }
            setStatus(EOF);
        }
        catch (Exception e) {
            Debug.println("Dispenser.run killed by exception:"); 
            Debug.printStackTrace(e);
            setStatus(NOT_INITIALIZED);
        }
    }

    private void process(int i) {
        String checkSum = ""; 
        int newStatus = OK;

        if (i == ASCII_ACK) {
            bufptr = 0;
            csum = 0;
            bslashCounter = 0;
        }
        else {
            buffer[bufptr++] = (byte) i;
            if (bslashCounter < 2) {
                csum += i;
            }
            if (DEBUG) {
                System.out.write(i);
            }
        }

        if (i == ASCII_BSLASH) {
            ++bslashCounter;
        }

        if ((i == ASCII_LF)
            && ((buffer[0] == ASCII_AT)
                || ((buffer[0] == ASCII_SPACE) && (buffer[1] == ASCII_R)))) {
            Tokenizer t = new Tokenizer(buffer, 0, bufptr);

            String s = t.nextToken(); // at sign
            s = t.nextToken(); // cmd letter

            if (s.equals("P")) { 
                dispenserSerialNumber = t.nextToken();
                model = t.nextToken();
                manufactureDate = t.nextToken();
                mlc = t.nextToken();
            }
            else if (s.equals("S")) { 
                extendedStatus = 0;
                for (int j = 4; j < 32; ++j) {
                    if (buffer[j] == ASCII_ONE) {
                        extendedStatus |= (1 << (j - 3));
                        if (DEBUG) {
                            if (extstatus[j - 3] != null) {
                                Debug.println(extstatus[j - 3]);
                            }
                        }
                    }
                }
                hardwareItemCount = 0;
                for (int k = 21; k < 26; ++k) {
                    hardwareItemCount *= 10;
                    hardwareItemCount += (buffer[k] - ASCII_ZERO);
                }
                if (DEBUG) {
                    Debug.println("hardwareItemCount = " + hardwareItemCount); 
                }
                s = t.nextToken();
            }
            else if (s.equals("R")) { 
                error = Integer.parseInt(t.nextToken());
                if (error == 11) {
                    parametersAvailable = false;
                }
                newStatus = ERROR;
            }
            else if (s.equals("G")) { 
                graphicsChecksum = Integer.parseInt(t.nextToken());
            }
            else {
                if (DEBUG) {
                    Debug.println("Unrecognized response"); 
                }
            }

            checkSum = t.nextToken();
            if (checkSum.equals(Integer.toString(csum))) {
                setStatus(newStatus);
            }
            else {
                if (DEBUG) {
                    Debug.println("csum = " + csum); 
                    Debug.println("checkSum = " + checkSum); 
                }
                setStatus(BAD_RESPONSE);
            }
        }
    }

    // add one of the constants to a byte array of digits
    private void add(byte[] a, byte[] b, byte[] result) {
        byte c = 0;
        for (int i = 11; i >= 0; i--) {
            result[i] = (byte) (a[i] + b[i] + c);
            if (result[i] > ASCII_NINE) {
                c = 1;
                result[i] -= 10;
            }
            else {
                c = 0;
            }
        }
    }

    // swap around the digits in a byte array of digits
    private void swap(byte[] a, byte[] b, byte[] result) {
        for (int i = 0; i <= 11; i++) {
            result[i] = a[b[i]];
        }
    }

    // compute password
    private void cp() {
        byte[] temp = dispenserSerialNumber.getBytes();
        add(temp, const1, const0);
        swap(const0, const3, temp);
        add(temp, const2, const0);
    }

    //dispenser port open diagnostic record
    private void write202() {
        short dispenserID = 0;
        DiagLog.getInstance().writeDispenserOpen(dispenserID);
    }

    //dispenser status diagnostic record
    private void write203() {
        short dispenserID = 0;
        DiagLog.getInstance().writeDispenserStatus(
            dispenserID,
            extendedStatus,
            (short) hardwareItemCount);
    }

    /**
     * Returns the dispenser's power on status.
     */
    @Override
	public boolean isOn() {
        if (port == null)
            return false;
        return port.isCTS();
    }

    /**
     * Testing.
     */
    public static void main(String args[]) {
        Dispenser d = null;

        if (args.length < 2) {
            d = new Dispenser("COM1"); 
            //d.voidForm();
            //Debug.println("voided, status = " + d.status);	

            d.queryStatusWithIntent();
            Debug.println("queried, status = " + d.status); 

            //Debug.println("isFormDetected = " + d.isFormDetected());	
            //System.exit(0);
        }
        else {
            d = new Dispenser("COM1"); 
            d.queryStatus();
            Debug.println("status = " + d.status); 

            if (d.status != LOAD_REQUIRED)
                System.exit(0);

            try {

                long start = Long.parseLong(args[0]);
                long end = Long.parseLong(args[1]);

                if (!d.isValid(start)) {
                    Debug.println("start is not valid"); 
                    System.exit(0);
                }

                if (!d.isValid(end)) {
                    Debug.println("end is not valid"); 
                    System.exit(0);
                }

                d.setSerialNumberRange(start, end);
                Debug.println("set range, status = " 
                    + d.getStatus() + ", remaining = " 
                    + d.getRemainingCount());

                d.extend();
                Debug.println("extended, status = " + d.getStatus()); 

                if (args.length > 2) {
                    long ver = Long.parseLong(args[2]);
                    Debug.println("d.verify = " + d.verify(ver)); 
                    Debug.println("verified, status = " + d.getStatus()); 
                }

                d.retract();
                Debug.println("retracted, status = " + d.getStatus()); 

                if (args.length <= 2) {
                    d.verified();
                    Debug.println("verified, status = " + d.getStatus()); 
                }

                //		dw.profile.UnitProfile.getUPInstance().load();
                State state = new State();
                state.createProfile();

                MoneyOrderInfomation mo = new MoneyOrderInfomation(ReportLog.MONEY_ORDER_TYPE, 0,
                    new BigDecimal("25.00"), 
                    BigDecimal.ZERO); 
                mo.setVoidFlag(false);
                //mo.setDeltagramFlag("CU");	
                d.queryStatusWithIntent();
                Debug.println("queried, status = " + d.status); 

                Debug.println("------------------------------"); 
                System.out.flush();

                d.printItem(mo, new ClientInfo());
                Debug.println("printed, status = " + d.getStatus()); 

                //		Thread.sleep(15000);
                //		d.queryStatus();
                //		Debug.println("queried, status = " + d.status);	

                d.queryStatusWithIntent();
                Debug.println("queried, status = " + d.status); 

                //d.printItem(mo, new ClientInfo());
                //Debug.println("printed, status = " + d.getStatus());	

                //d.queryStatusWithIntent();
                //Debug.println("queried, status = " + d.status);	

                //d.printItem(mo, new ClientInfo());
                //Debug.println("printed, status = " + d.getStatus());	

                //d.downloadLogo();
                //Debug.println("logo, status = " + d.getStatus());	

                System.exit(0);
            }
            catch (Exception e) {
                Debug.printStackTrace(e);
            } finally {
            	if (d != null) {
            		try {
            			d.close();
            		} catch (Exception e) {
            			Debug.printException(e);
            		}
            	}
            }
        }
    }
}

/*
		    Debug.println();
		    d.queryStatus();
		    Debug.println("queried, status = " + d.status);	

		    Debug.println();
		    d.resetStatus();
		    Debug.println("reset, status = " + d.status);	

		    Debug.println();
		    d.queryStatus();
		    Debug.println("queried, status = " + d.status);	

		    //Debug.println("password = " + d.const0);	

		    Debug.println();
		    d.extend();
		    Debug.println("extended, status = " + d.status);	
		    Thread.sleep(5);

		    Debug.println();
		    d.verified();
		    Debug.println("verified, status = " + d.status);	

		    Debug.println();
		    d.retract();
		    Debug.println("retracted, status = " + d.status);	

		    Debug.println();
		    d.queryStatus();
		    Debug.println("queried, status = " + d.status);	

		    Debug.println();
		    d.voidForm();
		    Debug.println("voided, status = " + d.status);	

		    Debug.println();
		    d.queryStatus();
		    Debug.println("queried, status = " + d.status);	

		}
	    }
	    catch (Exception e) {
		e.printStackTrace();
	    }
	}
    }


*/
