/*
 * ExtraRanges.java
 * 
 * $Revision$
 *
 * Copyright (c) 2004 MoneyGram, Inc. All rights reserved
 */
 
package dw.dispenser;

import java.io.IOException;
import java.util.BitSet;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import dw.io.report.ReportLog;
import dw.io.report.ReportLogConstants;
import dw.io.report.UnifiedReportDetail;
import dw.utility.Debug;

/**
 * Records serial numbers that have been used, as a supplement to the
 * last-five ranges maintained by Dispenser in the State file. The
 * instance should be initialized at startup time after the report log.
 * 
 * The data structure is a map of bit sets, where the key represents the
 * serial number represented by the low bit in the set, and a true bit
 * means the serial number has been used. For the volume of data this 
 * class will have to handle (per design, the report log holds 90 days
 * worth of detail records) this data structure is more efficient than
 * the arrays of start and end values used in Dispenser.
 * 
 * There is a tradeoff between memory and run-time performance, which can
 * be adjusted by changing the value of MASK.
 * 
 * This class is not thread-safe.
 *  
 * @author Geoff Atkin
 */
public class ExtraRanges {
    
    /** Singleton. */
    private static ExtraRanges instance;
    
    /** 
     * Controls the dividing point between portion of serial number used for
     * key and portion used to select bit in bitset. Should be one less than
     * a power of two.
     */
    private static final long MASK = 0x01ff;
    //private static final long MASK = 0x03;      // for debugging
    
    /** Contains bitsets mapped by partial serial numbers. */
    private TreeMap<Long, BitSet> map;
    
    /** 
     * The common case is that serial numbers will be used in sequential or
     * at least near-sequential order, so beenUsed() can be optimized by
     * caching the most-recently used key and bitset.
     */ 
    private long lastKeyMarked = -1;
    private BitSet lastValueMarked = null;
    
    /** Same applies to inUse() but different interpretation null value. */
    private long lastKeyChecked = -1;
    private BitSet lastValueChecked = null;
    
    /** 
     * Private constructor for singleton pattern. Reads the detail 
     * records from the report log, takes the serial number field 
     * from each document record, and calls beenUsed() on it. 
     */
    private ExtraRanges() {
        map = new TreeMap<Long, BitSet>();
        try {
            Iterator<UnifiedReportDetail> i = ReportLog.getInstance().detailIterator();
            while (i.hasNext()) {
                UnifiedReportDetail urd = i.next();
                if (urd.getRecordType() == ReportLogConstants.DOCUMENT_RECORD) {
                    beenUsed(urd.getLong(ReportLogConstants.SERIAL_NUMBER));
                }
            }
        }
        catch (IOException e) {
            Debug.printStackTrace(e);
        }
    }
    
    /** Returns the singleton instance. */
    public static ExtraRanges getInstance() {
        if (instance == null) {
            instance = new ExtraRanges();
        }
        return instance;
    }
    
    /** 
     * Records all the serial numbers in this range as used.
     * The range is s1 (inclusive) through s2 (inclusive). 
     */
    void addRange(long s1, long s2) {
        if (s1 == 0L || s2 == 0L || s1 > s2) {
            return;
        }
        // Could be made more efficient by taking advantage of 
        // BitSet.set(fromIndex, toIndex) but then code would be
        // needed to handle the case where the range spans more
        // than one bitset; i.e. (s1 & ~MASK) != (s2 & ~MASK).
        
        for (long s = s1; s <= s2; s++) {
            beenUsed(s);
        }
    }
    
    /** 
     * Records the fact that the given serial number has been used. 
     * Typical case is that successive calls will be in the 
     * same bitset, so this method skips a bunch of code if k 
     * is the same as it was last time.
     */
    void beenUsed(long s) {
//        Debug.println("beenUsed(" + s + ")");
        if (s < 1) {
            Debug.println("beenUsed(" + s + ")");
            Debug.dumpStack();
            return;
        }
        long k = s & (~MASK);
        if (k != lastKeyMarked) {
            Long key = Long.valueOf(k);
            lastKeyMarked = k;
            lastValueMarked = map.get(key);
            if (lastValueMarked == null) {
                lastValueMarked = new BitSet();
                map.put(key, lastValueMarked);
            }
        }
        lastValueMarked.set((int) (s & MASK));
    }
    
    /** 
     * Checks to see if a given serial number has been used.
     * @param s1 the serial number to check, without check digit
     * @param s2 currently ignored 
     */
    boolean inUse(long s1, long s2) {
        long k = s1 & (~MASK);
        if (k != lastKeyChecked) {
            lastKeyChecked = k;
            Long key = Long.valueOf(k);
            lastValueChecked = map.get(key);
        }
        
        if (lastValueChecked == null) {
            return false;
        }
        else {
            return lastValueChecked.get((int) (s1 & MASK));
        }
    }

//    boolean inUse_debug(long s1, long s2) {
//        boolean result = inUse(s1, s2);
//        Debug.println("ExtraRanges.inUse(" + s1 + "," + s2 +") = " + result);
//        return result;
//    }

    
//    boolean inUse(long s1, long s2) {
//        Long key = new Long(s1 & (~MASK));
//        BitSet value = (BitSet) map.get(key);
//        if (value == null) {
//            return false;
//        }
//        else {
//            return value.get((int) (s1 & MASK));
//        }
//    }
    
    /** For debugging. */
    @Override
	public String toString() {
        StringBuffer result = new StringBuffer();
        Iterator<Map.Entry<Long, BitSet>> i = map.entrySet().iterator();
        while (i.hasNext()) {
            Map.Entry<Long, BitSet> entry = i.next();
            result.append(entry.toString());
            result.append("\n");
        }
        return result.toString();
    }

    /** For debugging. */    
    public static void main(String args[]) {
//        ExtraRanges er = ExtraRanges.getInstance();
//        Debug.println(er);
    }

}
