/*
 * DispenserFactory.java
 * 
 * $Revision$
 *
 * Copyright (c) 2000-2005 MoneyGram International
 */

package dw.dispenser;

import java.io.IOException;

import dw.i18n.Messages;
import dw.io.State;
import dw.profile.UnitProfile;
import dw.utility.ErrorManager;

/**
  * Creator for the dispenser.
  */

public class DispenserFactory {
    private static DispenserInterface[] dispensers = new DispenserInterface[2];
    private static boolean loaded[] = {false, false};
    
    /**
      * Loads a dispenser object. This method will do for phase 1, but
      * when we add multiple dispenser support, whoever is doing the 
      * loading should use loadDispenser(int, int) instead. This
      * version assumes that dispenser 1 is on COM1 and dispenser 2 is
      * on COM2.
      * @param i the dispenser number, either 1 or 2.
      */
    public static void loadDispenser(int i) {
        int j = -1;  // If we don't have a dispenser set the dispensers comm port to -1
        String port = UnitProfile.getInstance().get("COM_PORT", "COM1");
        if (dispenserConnected()) {       	
           //dispensers[i - 1] = new Dispenser("COM" + i);   
        	// set to the default COM1 if the profile isn't the correct length.  This
        	// profile item used to be only a number.
        	if (port.length() > 3) {
        		int index = 1;
        		try {
        			index = Integer.parseInt(port.substring(3));
        		} catch (Exception e) {
        		}
        		dispensers[i - 1] = new Dispenser("COM" + index);
        	} else {
        		dispensers[i - 1] = new Dispenser("COM1");
        	}
        }
        else
            dispensers[i - 1] = new Dispenser("COM" + j);   
        loaded[i - 1] = true;
        addStateListener(i);
    }

//    /**
//      * Loads a dispenser object. This version specifies both the dispenser
//      * number and the port number.
//      * @param i the dispenser number, either 1 or 2.
//      * @param port the port number, either 1 or 2, or possibly higher.
//      */
//    public static void loadDispenser(int i, int port) {
//        dispensers[i - 1] = new Dispenser("COM" + port);    
//        dispensers[i - 1] = new Dispenser("COM" + port);    
//        loaded[i - 1] = true;
//        addStateListener(i);
//    }
    
    /**
      * Returns a previously loaded dispenser object.
      * @param i the dispenser number, either 1 or 2.
      */
    public static DispenserInterface getDispenser(int i) {
        if (! loaded[i - 1])
            loadDispenser(i);

        // attempt to recover from random glitches
        int status = dispensers[i - 1].getStatus();
        if ((status == DispenserInterface.NOT_INITIALIZED) || 
                (status == DispenserInterface.EOF)) {
            dispensers[i - 1].reOpen();
        }
        
        return dispensers[i - 1];
    }

    private static void addStateListener(int i) {
        long fp;
        DispenserInterface d = dispensers[i - 1];
        byte buffer[] = new byte[State.DISPENSER_2 - State.DISPENSER_1];
        
        if (i == 1) 
            fp = State.DISPENSER_1;
        else
            fp = State.DISPENSER_2;
        
        try {
            State.getInstance().read(buffer, fp);
            d.loadState(buffer);
        }
        catch (IOException e) {
            // This is a fatal error, so propagate an appropriate message
            // to the ErrorHandler.
            ErrorManager.handle(e, "stateLoadError", Messages.getString("ErrorMessages.stateLoadError"), ErrorManager.FATAL, true);	
        }

        d.addStateListener(new State.StateAdapter(fp));
    }
    /** 
     * Returns true if dispenser transactions should be allowed. 
     * The implementation is based on whether a valid-looking account
     * number appears in the profile item MO_ACCOUNT. In demo mode,
     * dispenser transactions are allowed regardless.
     */
    private static boolean dispenserConnected() {
        if (UnitProfile.getInstance().isDemoMode())
            return true;
        
        boolean retval = false;
        // A valid-looking account number is one with at least one
        // digit in the range 1-9; in other words, something other
        // than empty string, all-spaces, or all-zeros. 
        String moAccount = UnitProfile.getInstance().get("MO_ACCOUNT", "");
        for (int i = 0; i < moAccount.length(); i++){
            char c = moAccount.charAt(i);
            if (c >= '1' && c <='9'){
                retval = true;
                break;
            }
        }
        return retval;
    }

    /**
     * Sets the verify required status on any dispensers that have been
     * instanciated, but does not instanciate any that haven't been. 
     */
    public static void verifyRequired() {
        if (loaded[0] && dispensers[0] != null)
            dispensers[0].verifyRequired();

//        if (loaded[1] && dispensers[1] != null)
//            dispensers[1].verifyRequired();
        
    }

}

