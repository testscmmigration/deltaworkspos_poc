package dw.dispenser;

import com.moneygram.agentconnect.MoneyOrderInfo;
import com.moneygram.agentconnect.MoneyOrderVoidReasonCodeType;

import dw.io.StateListener;
import dw.moneyorder.MoneyOrderInfomation;
import dw.profile.ClientInfo;

/**
  * API to the dispenser. In general, caller is expected to call queryStatus
  * first, then getNextSerialNumber, then printItem.
  * 
  * The queryStatus() method returns one of the following status values:
  * <PRE>
  * OK = 0;                     // ready to print
  * NOT_INITIALIZED = 1;	// constructor failed
  * NO_CTS = 2;			// offline or power off
  * WAITING = 3;		// read in progress
  * EOF = 4;			// reading thread done
  * ACK = 5;			// read in progress
  * BAD_RESPONSE = 6;		// reply was garbled	
  * ERROR = 7;			// unknown error
  * LOAD_REQUIRED = 8;		// load required
  * VOIDED = 9;			// printItem voided it
  * VERIFY_REQUIRED = 11;	// need last-4 check
  * </PRE>
  * Starting in 2.9, printItem() returns a different set of status values, to
  * distinguish more clearly our readiness to perform a print command with the
  * result of the previous print command.
  * <PRE> 
  * ITEM_OK = 0;                // print success
  * ITEM_LIMBO = 3;             // print status unknown
  * ITEM_VOIDED = 9;            // print error caused dispenser void
  * ITEM_NOT_PRINTED = 10;      // print failed but no item was consumed
  * </PRE>
  *
  * @author Geoff Atkin
  * @author Erik S. Steinmetz  
  */
public interface DispenserInterface {

    public static final int OK = 0;                     // ready to print
    public static final int NOT_INITIALIZED = 1;	// constructor failed
    public static final int NO_CTS = 2;			// offline or power off
    public static final int WAITING = 3;		// read in progress
    public static final int EOF = 4;			// reading thread done
    public static final int ACK = 5;			// read in progress
    public static final int BAD_RESPONSE = 6;		// reply was garbled	
    public static final int ERROR = 7;			// unknown error
    public static final int LOAD_REQUIRED = 8;		// load required
    public static final int VOIDED = 9;			// printItem voided it
    public static final int NOT_PRINTED = 10;           // obsolete, see below
    public static final int VERIFY_REQUIRED = 11;	// need last-4 check

    public static final int ITEM_OK = 0;                // print success
    public static final int ITEM_LIMBO = 3;             // print status unknown
    public static final int ITEM_VOIDED = 9;            // print error caused dispenser void
    public static final int ITEM_NOT_PRINTED = 10;      // print failed but no item was consumed
    
    public static final String STATUS_TEXT[] = { "OK", "NOT_INIT", "NO_CTS", "LIMBO", "EOF", 
        "ACK", "BAD", "ERROR", "LOAD_REQ", "VOIDED", "NOT_PRINTED", "VERIFY_REQ"};
    
    /**
      * Returns the status value from the more recent call that talked to
      * the device. This method does not talk to the dispenser itself.
      * This method can be used in place of queryStatus in situations where
      * the latter would be redundant. For example, the dispenser loading
      * procedure could call getStatus to learn more about the reason for 
      * the load. In that context:
      * <pre>
      * NO_CTS           off-line or powered off; display a suitable message
      * EOF              an internal thread ended; should be reopened
      * NOT_INITIALIZED  the contructor failed; should be reopened
      * etc.
      * </pre>
      */
    public int getStatus();		

    /**
      * Talks to the device and returns an updated status value. This should 
      * be called before or at the beginning of each transaction, before the 
      * operator has entered any transaction data. When used this way, the 
      * appropriate actions are:
      * <pre>
      * OK              go ahead
      * LOAD_REQUIRED   do a load first
      * VERIFY_REQUIRED do a verify first
      * VOIDED          do a verify first
      * Anything else   do a load to try to clear the problem
      * </pre>
      * VOIDED means there was a problem on the previous document that forced
      * it to be voided unexpectedly. A verify is required to make sure the 
      * dispenser recovered correctly.
      */
    public int queryStatus();

    /** 
      * Sends a message to the dispenser to update the status, and 
      * performs an additional check. This method should be called instead
      * of queryStatus when the application intends to print an item after
      * getting the updated status. This method does what queryStatus does,
      * and additionally checks to see if it is time for the extra
      * VERIFY_REQUIRED that happens on the third document following
      * a load. (To be precise, on the first transaction after the 
      * second document has been printed after a load.)
      */
    public int queryStatusWithIntent();
	
    /**
      * Returns the number of items that we think are loaded in the 
      * dispenser. Does not talk to the dispenser or update the status.
      * The return value is one less than the number of physical forms
      * loaded; the last form in the pack doesn't count because the 
      * dispenser can't print on it.
      */
    public int getRemainingCount();
    
    /**
      * Returns the serial number that the next item will print on.
      * The check digit is not included.
      */
    public String getNextSerialNumber();

    /**
      * Returns the serial number that the next item will print on.
      * The check digit is included.
      */
    public String getNextSerialNumberWithCheckDigit();

    /**
      * Prints the money order with info given. Updates status and 
      * returns it. Blocks until item is printed or voided, an error is
      * detected, or the device times out. If the given MonerOrderInfo
      * indicates that the item is to be intentionally voided, a "VOID"	
      * bitmap is printed at the top, and OK is returned (unless there is
      * an error, of course.) This is different from a hardware void 
      * which prints VOID over the amount and returns ITEM_VOIDED.
      * The returned status indicates the appropriate action:
      */
    public int printItem(MoneyOrderInfomation mo, ClientInfo co);

    public int printItem(MoneyOrderInfomation mo, ClientInfo co, String storename);
    
    /**
     * New printItem to take a MoneyOrderInfo40. 
     * This gets called from MoneyorderTransaction40. 
     */
    public int printItem(MoneyOrderInfo mo, MoneyOrderDisplayFields modf);
    
    /** 
     * For compatibility with current implementation of MoneyOrderTransaction40.
     */
    public int printItem(MoneyOrderInfo mo, MoneyOrderDisplayFields modf,
    	ClientInfo co, String storename);
    
    /**
      * Talks to the device to see if it's locked.
      * @return true if the cover is locked; false if it isn't.
      */
    public boolean isLocked();

    /**
      * Talks to the device to determine if a form is actually 
      * loaded in the dispenser. This is used by the loading procedure
      * and not by ordinary transactions.
      */
    //public boolean isFormDetected();
    
    /**
      * Returns true if this is a valid serial number. The check digit is
      * computed and compared to the last digit of the given number.
      */
    public boolean isValid(long serial);

    /**
      * Returns true if the given number matches the last four digits
      * of the next serial number. If they do match, the status is
      * updated to clear a VOIDED or VERIFY_REQUIRED condition. 
      * @return true if the numbers match the next serial number 
      * either with or without the check digit.
      */
    public boolean verify(long last4);

    /**
      * Sets the serial number range and talks to the device to return the
      * updated status. This has the effect of clearing a LOAD_REQUIRED
      * condition. Check digits must be included on these serial numbers.
      * @return VERIFY_REQUIRED unless there is a more serious problem 
      * like NO_CTS.
      */
    public int setSerialNumberRange(long startSerialNumber,
	    long endSerialNumber);
    
    /**
      * Indicates that the last 4 digits of the serial number
      * have been verified. Talks to the device to return the
      * updated status. This has the effect of clearing a VOIDED
      * or VERIFY_REQUIRED condition.
      */
    public int verified();
    
    /**
      * Tells the device to extend the form so the serial number can
      * be read for verification. If a retract command is not received
      * within 99 seconds, the form may be retracted automatically.
      * @return OK if the command succeeds, even though a subsequent call
      * to queryStatus would return something else.
      */
    public int extend();
    
    /**
      * Tells the device and to retract the form that was extended so
      * the serial number could be read for verification.
      * @return OK if the command succeeds, even though a subsequent call
      * to queryStatus would return something else.
      */
    public int retract();

    /**
      * Returns the dispenser's power on status. 
      */
    public boolean isOn();

    /**
      * Returns the dispenser's unit ID. 
      */
    public String getDispenserSerialNumber();

    /**
      * Returns the dispenser's make and model.
      */
    public String getModel();

    /**
      * Returns the dispenser's manufacture date as a six-digit string.
      * The format is MMDDYY.
      */
    public String getManufactureDate();

    /**
      * Returns the dispenser's MLC number. 
      */
    public String getMlc();
    
    
    /**
      * Backs out the form currently loaded in the dispenser. 
      * @return OK if the back-out suceeds, but a subsequent call to 
      * getStatus() will return LOAD_REQUIRED.
      */
    public int backOutForm();
    
    /**
      * Ejects the form currently loaded in the dispenser. This should
      * only be called when the current form is the last form.
      * @return OK if the back-out succeeds, but a subsequent call to 
      * getStatus() will return LOAD_REQUIRED.
      */
    public int ejectForm();
    
    /**
      * Voids the form currently in the dispenser.
      * Called by DMP.printTestVoid() and CT.voidIfLastForm() 
      * @return dispenser status.
      */
    public int fullVoidForm(final int userID, final boolean doQuickly, final MoneyOrderVoidReasonCodeType reason);
    
    /**
      * Close and then reestablish the serial connection to the dispenser.
      * Intended for recovery from error states NOT_INITIALIZED and EOF.
      * This method has not been tested.
      */
    public int reOpen();

    /** This turns off the extra verification that happens after a load
      * operation. The purpose is to skip an unnecessary verify at application
      * startup time when the serial numbers are read from saved state.
      */
    public void skipExtraVerify();
    
    /**
      * Returns the last serial number loaded in the dispenser, according
      * an earlier call to setSerialNumberRange.
      * The check digit is included.
      */
    public String getLastSerialNumberWithCheckDigit();
    
    /** 
      * Sets the dispenser to the load required state. This method 
      * should be called if for example a load operation is started but
      * not finished.
      */
    public void setLoadRequired();

    /**
      * Sets the dispenser to the verify-required state.
      * Forces a serial number verify on the next dispenser item.
      */
    public void verifyRequired();
    
    /**
      * Registers the state listener which will record loaded and used
      * serial numbers. Only one state listener is currently supported,
      * so the given listener will replace any existing listener.
      */
    public void addStateListener(StateListener listener);

    /** 
      * Loads the saved serial number information.
      */
    public void loadState(byte state[]);

    /** Return true if s1 is within any range marked as being used. */
    public boolean inUse(long s1, long s2);

    /** 
     * Void the current form. But if the cover is open, back it out instead.
     * And if the cover is closed but has been open, check for form presence
     * before voiding it, to prevent air voids.
     *
     * @return true if current form was voided ok
     */
    public boolean voidFirstRemainingForm();
    
    /**
     * writeVoidRecordToLog write a record to the reportlog and
     * to the ccilog.  This method is used for voids printed by
     * test void and void remaining forms.
     * 
     * @param s serial number
     * @param i user id
     * @param r void reason
     */
    public void writeVoidRecordToLogs(String s, int i, MoneyOrderVoidReasonCodeType r);

    /**
     * Void the current form unless the cover is open. 
     * 
     * @return true if current form was voided ok
     */
    public boolean voidNextRemainingForm();


}

