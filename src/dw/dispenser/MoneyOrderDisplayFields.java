package dw.dispenser;

import java.io.Serializable;

/**
 * Additional information shown on the face of a printed money order
 * but not traditionally stored in log files.
 */
public class MoneyOrderDisplayFields implements Serializable{
	private static final long serialVersionUID = 1L;

	// Three lines on the left side (optional)
	
	private String payeeName="";		// typically vendor name	
	private String specialMessage="";	// not used in Deltaworks	
	private String purchaserName="";	// typically blank or store name	
	
	// Two lines under the date
	
	private String deltagramFlag="";	// CU=customer, AG=agent	
	private String documentName="";		// e.g. "MONEY ORDER"	
	
	// Two Y/N flags in line two of the stub
	
	private String multiIssueIndicator="N";	// Y=sold as larger group	
	private String limitIndicator="N";		// Y=manager approved HDV override	

	// Two strings of numbers under the amount (account number is also on the stub)
	
	private String accountNumber="";	
	private String unitNumber="";	
	
	// Controls the "-VOID-" bitmap for training mode
	
	private boolean trainingFlag=false;		// true=training mode, show VOID logo
		
	public String getDeltagramFlag() {
		return deltagramFlag;
	}

	public String getDocumentName() {
		return documentName;
	}

	public String getLimitIndicator() {
		return limitIndicator;
	}

	public String getMultiIssueIndicator() {
		return multiIssueIndicator;
	}

	public String getPayeeName() {
		return payeeName;
	}

	public String getPurchaserName() {
		return purchaserName;
	}

	public String getSpecialMessage() {
		return specialMessage;
	}

	public void setDeltagramFlag(String string) {
		deltagramFlag = string;
	}

	public void setDocumentName(String string) {
		documentName = string;
	}

	public void setLimitIndicator(String string) {
		limitIndicator = string;
	}

	public void setMultiIssueIndicator(String string) {
		multiIssueIndicator = string;
	}

	public void setPayeeName(String string) {
		payeeName = string;
	}

	public void setPurchaserName(String string) {
		purchaserName = string;
	}

	public void setSpecialMessage(String string) {
		specialMessage = string;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public String getUnitNumber() {
		return unitNumber;
	}

	public void setAccountNumber(String string) {
		accountNumber = string;
	}

	public void setUnitNumber(String string) {
		unitNumber = string;
	}

	public boolean isTrainingFlag() {
		return trainingFlag;
	}

	public void setTrainingFlag(boolean b) {
		trainingFlag = b;
	}

}

