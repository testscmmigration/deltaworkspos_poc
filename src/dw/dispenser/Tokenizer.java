package dw.dispenser;

import dw.utility.Debug;

/**
  * Helper class for Dispenser. Divides a buffer of data into fields
  * delimited by comma and backslash. The methods hasMoreTokens and
  * nextToken are similar to java.util.StringTokenizer. The difference
  * is that leading, trailing, and adjacent delimiter characters indicate
  * empty fields.
  */
class Tokenizer {

    static final int ASCII_LF = 10;
    static final int ASCII_CR = 13;
    static final int ASCII_COMMA = 44;
    static final int ASCII_BSLASH = 92;
    
    byte[] buffer;
    int offset, max;

    Tokenizer(byte[] buf, int off, int len) {
	buffer = buf;
	offset = off;
	max = off + len;
    }

    boolean hasMoreTokens() {
	return offset <= max;
    }

    String nextToken() {
	String s;
	int p = offset;
	while ((p < max) && (buffer[p] != ASCII_COMMA)
		&& (buffer[p] != ASCII_BSLASH)
		&& (buffer[p] != ASCII_CR)
		&& (buffer[p] != ASCII_LF)) {
	    ++p;
	}
	s = new String(buffer, offset, p - offset);
	offset = p + 1;
	return s;
    }

    public static void main(String args[]) {
	for (int i = 0; i < args.length; i++) {
	    Tokenizer t = 
		new Tokenizer(args[i].getBytes(), 0, args[i].length());

	    while (t.hasMoreTokens()) {
		Debug.println("'" + t.nextToken() + "'");	 
	    }
	    //Debug.println();
	}
    }
}
