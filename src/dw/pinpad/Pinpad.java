/*
 * Package for the Ingenico iPP320 pinpad
 * 
 * Known issues and future things to do:
 * 1. Ingenico pinpad restriction: can't use 'PIN' or 'NIP' at end 
 * 	  of first line (see the CPX documentation for "58.1" command
 * 2. Ingenico function key response for F2 and F3 returns the same 
 *    value ('B') for the "58.1"
 * 3. Not sure you tell the iPP320 and iPP350 apart (or need to)
 * 4. Ingenico serial numbers don't seem to match the back of the pinpad.
 * 5. There seems to be no good way to identify a pinpad except by sending
 * 		a command request. Perhaps using WMI which is in Win Dev Kit.
 * 6. Javax.comm.CommPortIdentifier caching(?) issue.
 * 		Starting DW with the pinpad not connected,, then connecting it
 * 	 	after javax.comm.CommPortIdentifier is executed, then retrying
 * 		javax.comm.CommPortIdentifier. It will not give updated information.
 * 		Note: it only fails if USB; it works if COM 1.
 * 	  Workaround: Start up DW after pinpad has been powered-up and is
 * 		at its idle prompt in CPX.
 * 7. Some times IPP320 pinpad gets associated with COM3 and doesn't work.
 * 		Is this really true? Investigate this issue. 
 * 8. JRE 1.6 cannot be used for DW because of its size to download. 
 * 		When JRE 1.6 can be used, use enums and change StringBuffers 
 * 		to StringBuilders
 * 9. When another pinpad model is added, change Pinpad to Ingenico_iPP3x0
 *10. Four "blue screens of death" (BSOD) on PC during development (Edgeport?)
 *
 * TO DO:
 * 1: Do shutdown when DeltaWorks exits to make initialization better
 * 2: How to prevent pinpad being owned by someone )DW) else?
 * 3. Check whether pinpad logic can recovery from errors and being disconnected
 * 4. Why does getCharacters() inputStream.available() get >= 128 characters?
 * 5. Check code for int to hex digit (like beep duration index
 * 6. Scan all code and comment
 */
package dw.pinpad;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.TooManyListenersException;

//Sun's serial communication port driver using comm.jar
import javax.comm.CommPortIdentifier;
import javax.comm.NoSuchPortException;
import javax.comm.PortInUseException;
import javax.comm.SerialPort;
import javax.comm.SerialPortEvent;
import javax.comm.SerialPortEventListener;
import javax.comm.UnsupportedCommOperationException;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import dw.comm.AgentConnect;
import dw.dialogs.Dialogs;
import dw.framework.XmlDefinedPanel;
import dw.i18n.Messages;
import dw.main.DeltaworksMainMenu;
import dw.pinpad.constants.AsciiControlChars;
import dw.profile.Profile;
import dw.profile.ProfileItem;
import dw.profile.UnitProfile;
import dw.utility.Debug;
import dw.utility.ExtraDebug;

public class Pinpad implements PinpadInterface {
	
	//***** Pinpad Constants *****
	
	private class PinpadStatuses {
		//----------Added pinpad fatal statuses-------------------
		private static final int NO_PINPAD = -1;
		//----------Actual pinpad (iPP320/iPP350) statuses--------
		private static final int SUCCESS = 0;
		private static final int UNUSED_ERROR = 10;
		//----------Added pinpad error statuses-------------------
		private static final int BUSY = 12;
		private static final int TIMEOUT = 13;
		private static final int RESPONSE_ERROR = 14;
	}

	private class PinpadStates {
		private static final int IDLE = 0;
		private static final int WAITING_FOR_ENQ_1 = 1; 
		private static final int WAITING_FOR_ENQ_2 = 2; 
		private static final int WAITING_FOR_ENQ_3 = 3;
		private static final int REQUEST_FOR_CANCEL = 4; 
		private static final int REQUEST_FOR_SERIAL_NUMBER = 5; 
		private static final int REQUEST_FOR_BEEP_TONE = 6; 
		private static final int REQUEST_FOR_DISPLAY_CLEAR = 7;
		private static final int REQUEST_FOR_DISPLAY_MESSAGE = 8;
		private static final int REQUEST_FOR_DISPLAY_MESSAGE_FOR_KEY = 9; 
		private static final int REQUEST_FOR_DISPLAY_PROMPT = 10;
	}

	private class PinpadEventStates {
		private static final int WAITING_FOR_SINGLE_CHAR = 0;
		private static final int WAITING_FOR_STX_PACKET_RESPONSE_BEGIN = 1;
		private static final int WAITING_FOR_STX_PACKET_RESPONSE_END = 2;
	}

	private static final String PINPAD_DEVICE_INGENICO_IPP320	= "Ingenico IPP320"; 
	private static final String PINPAD_DEVICE_INGENICO_IPP350	= "Ingenico IPP350"; 
	private static final String PINPAD_TYPE_4_DIGIT_SURNAME		= "4 Digit Surname"; 
	
	//This constant, also in InstallWizardPage2.java, should be made public some day
	private static int MONEY_GRAM_RECEIVE_DOC_SEQ = 4;
	
	//Strings for Pinpad Requests
	private static final String CANCEL_COMMAND  = 
		Character.toString(AsciiControlChars.STX) + "50."; 
	private static final String SERIAL_NUMBER_COMMAND = 
		Character.toString(AsciiControlChars.STX) + "51.S"; 
	private static final String BEEP_TONE_COMMAND = 
		Character.toString(AsciiControlChars.STX) + "5B."; 
	private static final String DISPLAY_CLEAR_COMMAND = 
		Character.toString(AsciiControlChars.STX) + "59."; 
	private static final String DISPLAY_PROMPT_COMMAND  = 
		Character.toString(AsciiControlChars.STX) + "58."; 

	//Array of Strings for Pinpad Responses index by PinpadStates
	private static final String[] strResponses = {
		"", "", "", "",      //dummy for easy indexing
		//Responses must all be the same length
		Character.toString(AsciiControlChars.ACK) + Character.toString(AsciiControlChars.STX) + "50.0", 
		Character.toString(AsciiControlChars.ACK) + Character.toString(AsciiControlChars.STX) + "51.0", 
		Character.toString(AsciiControlChars.ACK) + Character.toString(AsciiControlChars.STX) + "5B.0", 
		Character.toString(AsciiControlChars.ACK) + Character.toString(AsciiControlChars.STX) + "59.0", 
		Character.toString(AsciiControlChars.ACK) + Character.toString(AsciiControlChars.STX) + "58.0", 
		Character.toString(AsciiControlChars.ACK) + Character.toString(AsciiControlChars.STX) + "58.0",  //function key
		Character.toString(AsciiControlChars.ACK) + Character.toString(AsciiControlChars.STX) + "58.0"  
	};
	
	private static final int INPUT_BUFFER_SIZE 					= 128;
	private static final int OUTPUT_BUFFER_SIZE 				= 128;
	private static final int IO_CIRCULAR_BUFFER_LOG_SIZE		= 1024;

	private static final int SLEEP_WAIT_INTERVAL_IN_MSEC		= 100;			//.1 seconds 
	private static final int SLEEP_WAIT_FOR_ENQ_IN_MSEC			= 1000 * 1;		// 1 seconds
	private static final int TIMEOUT_WAIT_FOR_NO_INPUT_IN_MSEC	= 1000 * 2;		// 2 seconds

	//Singleton pinpad instance
	public static final Pinpad INSTANCE = new Pinpad();
	
	//Singleton string array of available COM ports
	private static String[] saAvailableComPorts; 

	//Singleton string array of available COM port objects
	private static ComPort[] comPorts;

	//Index into comPorts for pinpad COM port
	private static int iPinpadComPortIndex;

	//Boolean true when searching for pinpad
	private boolean isPinpadInitializing;

	//For pinpad's main to test without having the profile pinpad options
    private static boolean isAllowMockProfilePinpadOptions;

    //Pinpad's caller page object for notifying
	private PinpadListener pinpadListener;

	//Pinpad's private variables
    private boolean isPinpadRequired;
    private boolean isPinRequired;
	private StringBuffer sbUserInput = new StringBuffer();
	private StringBuffer sbSerialNumber = new StringBuffer();
    private JFrame checkForPinpadFrame;
    //Errors statistics kept for Pinpad Class
	private int numberOfErrors_PinpadSearch_Run_InitialziationFailures;
	private int numberOfErrors_waitForIdleWithTimeout_InterruptedException;
	private int numberOfErrors_outputDisplayRequest_IoException;

	/*
	 * ComPortOutputTimeoutThread: Start a timer to prevent the COM thread from blocking
	 */
	public class ComPortOutputTimeoutThread extends Thread {
		int iIndex;
		
		/*
		 * * ComPortOutputTimeoutThread(int, String) constructor
		 */
		public ComPortOutputTimeoutThread(int pm_iComPortIndex) {
			iIndex = pm_iComPortIndex;
		}
		
		@Override
		public void run() {
			try {
				Thread.sleep(TIMEOUT_WAIT_FOR_NO_INPUT_IN_MSEC);
			} catch (InterruptedException e) {
				comPorts[iIndex].numberOfErrors_isComPortConnectedToPinpad_InterruptedException++;
			}
			
			if(comPorts[iIndex].isAlive()) {
				// Device has timed out; unblock by shutting down to make thread ready-to-run.
				Debug.println("##ComPortOutputTimeoutThread: Shutting down Comm port:" + comPorts[iIndex].strComName);
				comPorts[iIndex].shutdownComPort();
			}
		} 	
	}

	/*
	 * ComPort: Pinpad()'s sub-class for individual COM port objects
	 */
	private class ComPort extends Thread implements SerialPortEventListener {
		private int iComPortIndex;
		private int iNumberCharsExpected;
		private int iNumberCharsReceived;
		private int iNumberCharsUserPinpadInput;
		private int iUserInputStart;
		private int IoCircularBufferLogIndex = IO_CIRCULAR_BUFFER_LOG_SIZE;
		private int pinpadEventState = PinpadEventStates.WAITING_FOR_SINGLE_CHAR;
		private int pinpadState = PinpadStates.IDLE;
		private int pinpadStatus = PinpadStatuses.NO_PINPAD;

		private byte[] byInputBuffer = new byte[INPUT_BUFFER_SIZE];
		private byte[] byOutputBuffer = new byte[OUTPUT_BUFFER_SIZE];
		private int[] iIoCircularBufferLog = new int[IO_CIRCULAR_BUFFER_LOG_SIZE];
	    private byte[] baRequestCommand = new byte[OUTPUT_BUFFER_SIZE];
		private StringBuffer sbRequestCommand = new StringBuffer();
		private String strComName;
		private CommPortIdentifier iComPortId;
		private InputStream inputStream;
		private OutputStream outputStream;
		private SerialPort serialPort;
	    
		//Statistics kept for ComPort sub-class
		private int numberOfPinpadRequestsBegun;
		private int numberOfPinpadRequestsCompleted;
		private int numberOfPinpadAcksReceived;
		private int numberOfPinpadPowerUps;
		private int numberOfPinpadNonPowerUps;
		private int numberOfPinpadSuccessInitializations;

		//Error statistics kept for ComPort sub-class
		private int numberOfErrors_sendAcknowledge_IoException;
		private int numberOfErrors_getCharacters_Available_InvalidEvent;
		private int numberOfErrors_getCharacters_tooManyAvailable_InvalidEvent;
		private int numberOfErrors_getCharacters_Available_IoException;
		private int numberOfErrors_getCharacters_Read_EndOfFile;
		private int numberOfErrors_getCharacters_Read_IoException;
		private int numberOfErrors_validateResponseAndAck_ChecksumError;
		private int numberOfErrors_processSingleCharReceived_UnexpectedEnq;
		private int numberOfErrors_processSingleCharReceived_Nak;
		private int numberOfErrors_processSingleCharReceived_UnexpectedStx;
		private int numberOfErrors_processSingleCharReceived_NotAckEnqNakStx;
		private int numberOfErrors_processStxPacketResponseBegin_InvalidResponse;
		private int numberOfErrors_processStxPacketResponseBegin_InvalidChars;
		private int numberOfErrors_processStxPacketResponseBegin_StatusError;
		private int numberOfErrors_processStxPacketResponseBegin_InvalidPromptLength;
		private int numberOfErrors_SerialEvent_InvalidEvent;
		private int numberOfErrors_serialEvent_invalid_switch_value;
		private int numberOfErrors_isComPortConnectedToPinpad_InterruptedException;
		private int numberOfErrors_isComPortConnectedToPinpad_sendDisplayMessageRequest;
		private int numberOfErrors_isComPortConnectedToPinpad_getSerialNumberRequest;

		/*
		 * ComPort(int, String) constructor
		 */ 
		private ComPort(int pm_iComPortIndex, String pm_strComName) {
			iComPortIndex = pm_iComPortIndex;
			strComName = pm_strComName;
		}

		/*
		 * sendAcknowledge()
		 */
		private void sendAcknowledge() {
			try {
				outputStream.write(AsciiControlChars.ACK);
			} catch (IOException ioe) {
				numberOfErrors_sendAcknowledge_IoException++;
				Debug.printStackTrace(ioe);
				return;
			}
			
			IoCircularBufferLogIndex = 
				(IoCircularBufferLogIndex >= IO_CIRCULAR_BUFFER_LOG_SIZE) ? 0 : IoCircularBufferLogIndex + 1;
			iIoCircularBufferLog[IoCircularBufferLogIndex] = AsciiControlChars.ACK; 
		}
		
		/*
		 * getCharacters(int) //This method must be lean and mean
		 */
		private int getCharacters() {
			int iChar = 0;
			int iNumberInputCharsAvailable = 0;

			while(true) {
				try {
					iNumberInputCharsAvailable = inputStream.available();
					if(iNumberInputCharsAvailable < 0) {
						numberOfErrors_getCharacters_Available_InvalidEvent++;
						Debug.dumpStack();
						return -2;
					}
					else if(iNumberInputCharsAvailable + iNumberCharsReceived >= INPUT_BUFFER_SIZE) {
						//This has occurred for some reason (why?); attempt recovery by ignoring 
						numberOfErrors_getCharacters_tooManyAvailable_InvalidEvent++;
						inputStream.skip(iNumberInputCharsAvailable);
						return -3;
					}
				
				} catch (IOException ioe) {
					numberOfErrors_getCharacters_Available_IoException++;
					Debug.printStackTrace(ioe);
					return -4;
				}

				if(iNumberInputCharsAvailable == 0) {
					return 0;
				}
				
				//Input number of available characters (might be previously received characters)
				for(int ii = iNumberCharsReceived; 
						ii < iNumberInputCharsAvailable + iNumberCharsReceived; ii++) {
					try {
						iChar = inputStream.read();
						
						if(iChar == -1) {
							numberOfErrors_getCharacters_Read_EndOfFile++;
							Debug.dumpStack();
							return -1;
						}
					} catch (IOException ioe) {
						numberOfErrors_getCharacters_Read_IoException++;
						Debug.printStackTrace(ioe);
						return -5;
					}
			
					byInputBuffer[ii] = (byte)iChar;
					if(++IoCircularBufferLogIndex >= IO_CIRCULAR_BUFFER_LOG_SIZE) { 
							IoCircularBufferLogIndex = 0;
					}
					iIoCircularBufferLog[IoCircularBufferLogIndex] = -(iChar & 0xFF);
				}
				iNumberCharsReceived += iNumberInputCharsAvailable;
			}
		}
		
		/*
		 * verifyEnqEnquiry(int)
		 */
		private void verifyEnqEnquiry(int pm_nextPinpadState) {
			// We got an ENQ, send an acknowledge to the pinpad
			sendAcknowledge();
			
			if(pm_nextPinpadState != PinpadStates.IDLE) {
				//Expect the next ENQ
				iNumberCharsExpected = 1; 
				pinpadState = pm_nextPinpadState;
			} else {
				//Got last ENQ, wait for user requests
				iNumberCharsExpected = 0;
			}
		}

		/*
		 * validateResponseAndAck()
		 */
		private boolean validateResponseAndAck() {
			//Checksum response and verify (skip ACK and STX)
			byte byChecksum = 0;
			for(int ii = 2; ii < iNumberCharsReceived; ii++) {
				byChecksum ^= byInputBuffer[ii];
			}
			
			if(byChecksum != 0) {
				numberOfErrors_validateResponseAndAck_ChecksumError++;
				return false;
			}
			
			//Acknowledge that data has been received correctly
			sendAcknowledge();

			//Log the input response
			StringBuffer dbgLine  = new StringBuffer(strComName+"-"+numberOfPinpadRequestsCompleted + "<-Pinpad:") ;
			for (int ii = 1; ii < iNumberCharsReceived; ii++) {
				dbgLine.append(AsciiControlChars.toString((char)byInputBuffer[ii]));
			}
			Debug.print(dbgLine.toString());
			Debug.println(""); 

			numberOfPinpadRequestsCompleted++;
			return true;
		}
		
		/*
		 * processSingleCharReceived()()
		 */
		private boolean processSingleCharReceived() {
			//Process a single character: ACK, ENQ, NAK, or unexpected one 
			if(getCharacters() < 0) {
				//An error occurred (already noted)
				pinpadStatus = PinpadStatuses.RESPONSE_ERROR;
				return false;
			}
			
			//Check for ACK, ENQ, NAK, STX, or invalid character
			if(byInputBuffer[0] == AsciiControlChars.ACK) {
				//Character is a ACK (occurs after a request and before a response)
				numberOfPinpadAcksReceived++;
				return true;
			} 
			
			//Process the first character, but ignore the rest.
			iNumberCharsReceived = 0;
			if(byInputBuffer[0] == AsciiControlChars.ENQ) {
				//Character is an ENQ: see if an initialization
				switch (pinpadState) {
				case PinpadStates.WAITING_FOR_ENQ_1:
					verifyEnqEnquiry(PinpadStates.WAITING_FOR_ENQ_2);
					break;
				case PinpadStates.WAITING_FOR_ENQ_2:
					verifyEnqEnquiry(PinpadStates.WAITING_FOR_ENQ_3);
					break;
				case PinpadStates.WAITING_FOR_ENQ_3:
					verifyEnqEnquiry(PinpadStates.IDLE);
					break;
				default:
					if(isPinpadInitializing == true) {
						//Pinpad mostly likely just powered during an initialization cancel request
						iNumberCharsExpected = 1; 
						pinpadState = PinpadStates.WAITING_FOR_ENQ_1;
					} else {
						//unexpected ENQ received in an unexpected state
						numberOfErrors_processSingleCharReceived_UnexpectedEnq++;
						Debug.println("processSingleCharReceived() unexpected ENQ, PinpadState=" + pinpadState); 
						Debug.dumpStack();
					}
					break;
				}

			} else if(byInputBuffer[0] == AsciiControlChars.NAK) {
				//Character is a NAK; this is unexpected
				numberOfErrors_processSingleCharReceived_Nak++;
				pinpadStatus = PinpadStatuses.RESPONSE_ERROR;
				Debug.println("PINPAD: NAK Received");
				pinpadState = PinpadStates.IDLE;
			} else if(byInputBuffer[0] == AsciiControlChars.STX) {
				numberOfErrors_processSingleCharReceived_UnexpectedStx++;
			} else {
				numberOfErrors_processSingleCharReceived_NotAckEnqNakStx++;
			}
			return false;
		}

		/*
		 * processStxPacketResponseBegin()()
		 */
		private boolean processStxPacketResponseBegin() {
			//Last character was a ACK, get the number of characters expected
			if(getCharacters() < 0) {
				//An error occurred (already noted)
				pinpadStatus = PinpadStatuses.RESPONSE_ERROR;
				return false;
			}
			
			if(iNumberCharsReceived < iNumberCharsExpected) {
			//More characters expected, exit and wait for more characters
				return true;
			}

			// Enough characters: check response (except for status)
			String strResponse = strResponses[pinpadState];
			int iResponseIndex = strResponse.length();
			if(iResponseIndex == 0) {
				numberOfErrors_processStxPacketResponseBegin_InvalidResponse++;
				pinpadStatus = PinpadStatuses.RESPONSE_ERROR;
				Debug.println("PINPAD: ZeroLength Response");
				return false;
			}
			for(int ii = 0; ii < iResponseIndex - 1; ii++) {
				if((char)(byInputBuffer[ii]) != strResponse.charAt(ii)) {
					numberOfErrors_processStxPacketResponseBegin_InvalidChars++;
					pinpadStatus = PinpadStatuses.RESPONSE_ERROR;
					Debug.println("PINPAD: Bad Character");
					return false;
				}
			}

			//Check response's status
			int iPinpadStatusOrdinal = byInputBuffer[iResponseIndex - 1] - '0';
			if(iPinpadStatusOrdinal > PinpadStatuses.UNUSED_ERROR) {
				iPinpadStatusOrdinal = PinpadStatuses.UNUSED_ERROR;
			}
			pinpadStatus = iPinpadStatusOrdinal; 
			if(iPinpadStatusOrdinal != PinpadStatuses.SUCCESS) {		
				numberOfErrors_processStxPacketResponseBegin_StatusError++;
				if(pinpadState == PinpadStates.REQUEST_FOR_DISPLAY_PROMPT) {
					//On error, set the user input length to zero
					sbUserInput.setLength(0);

					if(isAllowMockProfilePinpadOptions == false) {
						//Notify even though there is an error
						if (pinpadListener != null) {
							Thread t = new Thread() {
								@Override
								public void run() {
									pinpadListener.callback(PinpadReturnCode.PINPAD_INPUT_AVAIABLE);
								}
							};
							t.start();
						}
					}
					return false;
				}
			}
			
			//Adjust expected by the current request
			if(pinpadState == PinpadStates.REQUEST_FOR_DISPLAY_PROMPT) {
				//Update number of characters expected
				if(byInputBuffer[iResponseIndex] < '0' && byInputBuffer[iResponseIndex] > '9' &&
						byInputBuffer[iResponseIndex + 1] < '0' && byInputBuffer[iResponseIndex + 1] > '9') {
					//Illegal length for input prompt size
					numberOfErrors_processStxPacketResponseBegin_InvalidPromptLength++;
					Debug.println("PINPAD: Bad Length");
					pinpadStatus = PinpadStatuses.RESPONSE_ERROR;
					return false;
				} else {
					iNumberCharsUserPinpadInput = (byInputBuffer[iResponseIndex] - '0') * 10 + 
						byInputBuffer[iResponseIndex + 1] - '0';

					iNumberCharsExpected +=  iNumberCharsUserPinpadInput + 3;
					iUserInputStart = iResponseIndex + 2; 
				}
			} else if((pinpadState == PinpadStates.REQUEST_FOR_DISPLAY_MESSAGE) ||
						(pinpadState == PinpadStates.REQUEST_FOR_DISPLAY_MESSAGE_FOR_KEY)) {
					iNumberCharsExpected += 1;
					iUserInputStart = iResponseIndex; 
			} else if(pinpadState == PinpadStates.REQUEST_FOR_SERIAL_NUMBER) {
					iNumberCharsExpected += SERIAL_NUMBER_LENGTH - 1 + 2;
					iUserInputStart = iResponseIndex + 1; 
			}
			return true;
		}

		/*
		 * processStxPacketResponseEnd()
		 */
		private boolean processStxPacketResponseEnd() {
			if(pinpadState == PinpadStates.REQUEST_FOR_DISPLAY_PROMPT) {
				//Read rest of the response (<InputLength-2bytes><#bytes><CR><ETC><LRC>)
				//If less than one, there was an error
				if(getCharacters() >= 0) {
					if(iNumberCharsReceived < iNumberCharsExpected) {
						//More characters expected, exit and wait for more characters
						return true;
					} else if (validateResponseAndAck() == true) {
						//Success for this request
						sbUserInput.setLength(0);
						for( int ii = 0; ii < iNumberCharsUserPinpadInput; ii++){
							sbUserInput.append((char)(byInputBuffer[iUserInputStart + ii]));
						}

						if(isAllowMockProfilePinpadOptions == false) {
							//Notify that input is available
							if (pinpadListener != null) {
								Thread t = new Thread() {
									@Override
									public void run() {
										pinpadListener.callback(PinpadReturnCode.PINPAD_INPUT_AVAIABLE);
									}
								};
								t.start();
							}
						}
						return true;
					}
				}
			} else if(pinpadState == PinpadStates.REQUEST_FOR_DISPLAY_MESSAGE_FOR_KEY) {
				//Read rest of the response (LRC)
				//If less than one, there was an error
				if(getCharacters() >= 0) {
					if(iNumberCharsReceived < iNumberCharsExpected) {
						//More characters expected, exit and wait for more characters
						return true;
					} else if (validateResponseAndAck() == true) {
						//Success for this request
						sbUserInput.setLength(0);
						sbUserInput.append((char)(byInputBuffer[iUserInputStart]));

						if(isAllowMockProfilePinpadOptions == false) {
							//Notify that input is available
							if (pinpadListener != null) {
								Thread t = new Thread() {
									@Override
									public void run() {
										pinpadListener.callback(PinpadReturnCode.PINPAD_INPUT_AVAIABLE);
									}
								};
								t.start();
							}
						}
						return true;
					}
				}
			} else if(pinpadState == PinpadStates.REQUEST_FOR_SERIAL_NUMBER) {
				//The character 'S' and first character of the serial number have already 
				//been read; read rest of the response (rest of serial number, ETX, LRC)
				//If less than one, there was an error
				if(getCharacters() >= 0) {
					if(iNumberCharsReceived < iNumberCharsExpected) {
						//More characters expected, exit and wait for more characters
						return true;
					} else if (validateResponseAndAck() == true) {
						//Success for this request
						sbSerialNumber.setLength(0);
						for(int ii = 0; ii < SERIAL_NUMBER_LENGTH; ii++) {
							sbSerialNumber.append((char)(byInputBuffer[iUserInputStart + ii]));
						}
						return true;
					}
				}
			} else {
				//If less than one, there was an error
				if(getCharacters() >= 0) {
					if(iNumberCharsReceived < iNumberCharsExpected) {
						//More characters expected, exit and wait for more characters
						return true;
					} else if (validateResponseAndAck() == true) {
						//Success for this request
						return true;
					}
				}
			}
		
			pinpadStatus = PinpadStatuses.RESPONSE_ERROR;
			return false;
		}

		/*
		 * serialEvent(SerialPortEvent) 
		 * 
		 * This listener method (and the methods it calls) must be 
		 * lean and mean so that pinpad events will not be lost
		 */
		@Override
		public void serialEvent(SerialPortEvent pm_event) {
			switch (pm_event.getEventType()) {
			case SerialPortEvent.BI:
			case SerialPortEvent.OE:
			case SerialPortEvent.FE:
			case SerialPortEvent.PE:
			case SerialPortEvent.CD:
			case SerialPortEvent.CTS:
			case SerialPortEvent.DSR:
			case SerialPortEvent.RI:
			case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
				numberOfErrors_SerialEvent_InvalidEvent++;
				break;

			case SerialPortEvent.DATA_AVAILABLE:
				switch (pinpadEventState) {
				case  PinpadEventStates.WAITING_FOR_SINGLE_CHAR:
					if(processSingleCharReceived() == false) {
						//Either waiting for another ENQ or ignoring error that was already noted
						break;
					} else {
						//ACK has been received, setup to process the STX packet
						pinpadEventState = PinpadEventStates.WAITING_FOR_STX_PACKET_RESPONSE_BEGIN;
						if(iNumberCharsReceived < iNumberCharsExpected) {
							//More characters expected, wait for next event
							break;
						}
						//No break here: process more characters received from this event
					}
				case PinpadEventStates.WAITING_FOR_STX_PACKET_RESPONSE_BEGIN:
					if(processStxPacketResponseBegin() == false) {
						//Error: ignore any more characters (error already noted)
						pinpadEventState = PinpadEventStates.WAITING_FOR_SINGLE_CHAR;
						pinpadState = PinpadStates.IDLE;
						break;
					} else {
						//Most, if not all,of the STX packer has been received, setup to process the rest
						pinpadEventState = PinpadEventStates.WAITING_FOR_STX_PACKET_RESPONSE_END;
						if(iNumberCharsReceived < iNumberCharsExpected) {
							//More characters expected, wait for next event
							break;
						}
						//No break here: process more characters received from this event
					}
				case PinpadEventStates.WAITING_FOR_STX_PACKET_RESPONSE_END:
					//If false, ignore any more characters (error already noted)
					if(processStxPacketResponseEnd() == true) {
						if(iNumberCharsReceived < iNumberCharsExpected) {
							//More characters expected, wait for next event
							//EEO: Can this really happen???
							break;
						}
					}

					pinpadState = PinpadStates.IDLE;
					pinpadEventState = PinpadEventStates.WAITING_FOR_SINGLE_CHAR;
					break;
				default:
					numberOfErrors_serialEvent_invalid_switch_value++;
					Debug.dumpStack();
					break;
				}
			}
		}
		
		/*
		 * shutdownComPort()
		 */
		private synchronized void shutdownComPort() {
			if(outputStream != null) {
				try {outputStream.close();} catch (IOException ioe) {}
				outputStream = null;
			}

			if(inputStream != null) {
				try {inputStream.close();} catch (IOException ioe) {}
				inputStream = null;
			}

			if(serialPort != null) {
				serialPort.close();
				serialPort = null;
			}
		}

		/*
		 * setPinpadComPortIndex(int)
		 */ 
		private synchronized boolean setPinpadComPortIndex(int pm_iComPortIndex) {
			if(iPinpadComPortIndex >= 0) {
				//Already found a pinpad
				return false;
			} else {
				//Success: use this COM port for pinpad
				iPinpadComPortIndex = pm_iComPortIndex;
				return true;
			}
		}

		/*
		 * isComPortConnectedToPinpad()
		 */
		private boolean isComPortConnectedToPinpad() {
			try {
				iComPortId = CommPortIdentifier.getPortIdentifier(strComName);
				Debug.println("isComPortConnectedToPinpad: " + strComName);
			} catch (NoSuchPortException nspe) {
				//No such pinpad port: check for next COM port 
				Debug.println("***PINPAD ERROR: getPortIndentifier(" + strComName + "), " + nspe);
				return false;
			}
		
			//Check if someone already owns it
			if(iComPortId.isCurrentlyOwned() == true) {
				//Pinpad is in use by someone else: check for next COM port
				Debug.println("***PINPAD ERROR: iComPortId.isCurrentlyOwned(), " +   
					iComPortId.getName() + ", CurrentOwner=" + iComPortId.getCurrentOwner());  
				return false;
			}
			
			//Open the pinpad
			try {
				serialPort = (SerialPort) iComPortId.open("PinpadControl", 2000);  
			} catch (PortInUseException piue) {
				//Pinpad is in use by someone else: check for next COM port
				Debug.println("***PINPAD ERROR: iComPortId.open, " + strComName +  
						", " + iComPortId + ", " + piue); 
				return false;
			}
		
			//Setup the communication parameters.
			try {
				serialPort.setSerialPortParams(9600, SerialPort.DATABITS_7,
						SerialPort.STOPBITS_1, SerialPort.PARITY_EVEN);
				serialPort.setDTR(false);
				serialPort.setRTS(false);
			} catch (UnsupportedCommOperationException ucoe) {
				//Something wrong with pinpad's parameters: check for next COM port
				Debug.println("***PINPAD ERROR: setSerialPortParams(), " + ucoe);  
				return false;
			}
			
			//Open the pinpad's input stream
			try {
				inputStream = serialPort.getInputStream();
			} catch (IOException ioe) {
				//Could not open the pinpad's input stream: check for next COM port
				Debug.println("***PINPAD ERROR: getInputStream(), " + ioe);  
				return false;
			}
		
			//Open the pinpad's output stream
			try {
				outputStream = serialPort.getOutputStream();
			} catch (IOException ioe) {
				//Could not open the pinpad's output stream: check for next COM port
				Debug.println("***PINPAD ERROR: getOutputStream(), " + ioe);  
				return false;
			}
			
			//Add the pinpad's event listener
			try {
				serialPort.addEventListener(this);
			} catch (TooManyListenersException tmle) {
				//Too many listeners: check for next COM port
				Debug.println("***PINPAD ERROR: addEventListener(), " + tmle);  
				return false;
			}
			
			//Initialize variables for pinpad initialization.
			pinpadEventState = PinpadEventStates.WAITING_FOR_SINGLE_CHAR;
			pinpadState = PinpadStates.WAITING_FOR_ENQ_1;
			iNumberCharsExpected = 1; // Expected the very first pinpad character (ENQ)
		
			//Allow the pinpad serialEvent() listener thread to check for data available.
			serialPort.notifyOnDataAvailable(true);
		
			//Check if the pinpad is sending ENQS (i.e., it was just powered-up)
			int ii;
			for(ii = 0; ii < SLEEP_WAIT_FOR_ENQ_IN_MSEC; ii += SLEEP_WAIT_INTERVAL_IN_MSEC) {
				if(pinpadState == PinpadStates.IDLE) {
					break;
				}
				try {
					Thread.sleep(SLEEP_WAIT_INTERVAL_IN_MSEC);
				} catch (InterruptedException ie) {
					numberOfErrors_isComPortConnectedToPinpad_InterruptedException++;
					Debug.printStackTrace(ie);
				}
			}
			if(ii < SLEEP_WAIT_FOR_ENQ_IN_MSEC) {
				//ENQS have been received (i.e., just powered-up): a pinpad is on this COM port
				numberOfPinpadPowerUps++;
			} else {
				//No ENQS, either previously initialized or there is not pinpad on this COM port
				pinpadState = PinpadStates.IDLE;
				numberOfPinpadNonPowerUps++;
			}
			
			/*
			 * Assume all is well with pinpad: either it was just powered-up
			 * or it was already in the idle state from a previous life.
			 * Set status to no pinpad until connection is proven.  
			 */
			pinpadStatus = PinpadStatuses.NO_PINPAD;

			//Try to display the welcome message
			sendDisplayMessageRequest(Messages.getString(
				"MGRWizardPage1.PinpadIngenico_MoneygramInternational"), iComPortIndex); 
			if(pinpadStatus != PinpadStatuses.SUCCESS) {
				numberOfErrors_isComPortConnectedToPinpad_sendDisplayMessageRequest++;
				return false;
			}
			
			//Try to get the pinpad's serial number
			getSerialNumberRequest(iComPortIndex);
			if(pinpadStatus != PinpadStatuses.SUCCESS) {
				numberOfErrors_isComPortConnectedToPinpad_getSerialNumberRequest++;
				return false;
			}

			return true;
		}
		
		/*
		 * run() for a COM Port thread
		 */ 
		@Override
		public void run() {
			//Check if passed-in COM port is a pinpad
			Debug.println("Comport run: " + strComName);
			if(isComPortConnectedToPinpad() == false) {
				//Failure: This COM port is not a pinpad
				shutdownComPort();
				Debug.println("***" + strComName + " is NOT a pinpad");  

			} else if(setPinpadComPortIndex(iComPortIndex) == false) {
				//Failure: Already found a pinpad (use the first one)
				shutdownComPort();
				Debug.println("***" + strComName + " is a pinpad. BUT there was another pinpad");  

			} else {
				//Success: The pinpad has been found and successfully initialized
				numberOfPinpadSuccessInitializations++;
				Debug.println("+++PINPAD SUCCESS: " + strComName + " is a pinpad and is initialized");  
			}
		}
	}

	/*
	 * PinpadSearch: Pinpad's sub-class for searching for a pinpad by
	 * 		creating COM threads, waiting for each COM thread to ascertain 
	 * 		if it is connected to a pinpad, and, if no pinpad found, allowing 
	 * 		the user to retry again or to give up. It also checks the pinpad 
	 * 		profile options and puts up a "Checking pinpad, please wait" pop up. 
	 */
	public class PinpadSearch extends Thread {
		/*
		 * run() for the pinpad search thread
		 */ 
		@Override
		public void run() {
		    int checkForPinpadFrameWidth = 450; //At least this size for the French language
		    int checkForPinpadFrameHeight = 100;
			
		    //Create and set up the window frame.
			checkForPinpadFrame = new JFrame(Messages.getString("AboutBox.13")); 
			checkForPinpadFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
	        checkForPinpadFrame.setSize(checkForPinpadFrameWidth, checkForPinpadFrameHeight);
	        checkForPinpadFrame.setLocation(XmlDefinedPanel.getLocationForCenter(checkForPinpadFrame));
	      	checkForPinpadFrame.setResizable(false);
		    String iconFileName = "xml/images/temg_window.gif"; 
			java.net.URL url = ClassLoader.getSystemResource(iconFileName);
			ImageIcon icon = null;
			if (url != null)
			    icon = new ImageIcon(url);
			else
			    icon = new ImageIcon(iconFileName);
			checkForPinpadFrame.setIconImage(icon.getImage());
	
	        JLabel label = new JLabel(Messages.getString(
	    			"MGRWizardPage1.PinpadCheckingForPinpad")); 
	        label.setPreferredSize(new Dimension(checkForPinpadFrameWidth, checkForPinpadFrameHeight));
	        label.setHorizontalAlignment(SwingConstants.CENTER);
	        checkForPinpadFrame.getContentPane().add(label, BorderLayout.CENTER);
	
	        checkForPinpadFrame.setVisible(true);
	        checkForPinpadFrame.repaint();
			
			while(true) {
				isPinpadInitializing = true;
				if(iPinpadComPortIndex >= 0) {
					comPorts[iPinpadComPortIndex].shutdownComPort();
				}
				setPinpadComPortIndex(-1);
	
				//Find all available communication ports (COM and USB)
				Debug.print("PortList =");  
				for (int ii = 0; ii < saAvailableComPorts.length; ii++) {
					Debug.print(" " + saAvailableComPorts[ii]); 
				}
				Debug.println(""); 
	
				//Look at each communication port simultaneously for a supported pinpad.
				for (int ii = 0; ii < saAvailableComPorts.length; ii++) {
					//Start COM port thread to check the next possibility for a pinpad
					final ComPort comPort = new ComPort(ii, saAvailableComPorts[ii]);
					comPorts[ii] = comPort;
			        Debug.println("Calling Start for: " + saAvailableComPorts[ii]);
			        comPort.start();
				}
	
				//Wait for all threads to complete
				for (int ii = 0; ii < saAvailableComPorts.length; ii++) {
					try {
						comPorts[ii].join();
					} catch (InterruptedException e) {
						Debug.println("Join interrupted\n"); 
					}
				}
			
				//Check if we found a pinpad
				if(iPinpadComPortIndex >= 0) {
					//Pinpad found: initialization is completed 
					isPinpadInitializing = false;

					//Close the "Checking pinpad, please wait" pop up window
					checkForPinpadFrame.dispose();
					
					if(isAllowMockProfilePinpadOptions == false) {
						//Notify that the pinpad has been initialized for a COM port
						if (pinpadListener != null) {
							Thread t = new Thread() {
								@Override
								public void run() {
									pinpadListener.callback(PinpadReturnCode.PINPAD_INIITIALIZED);
								}
							};
							t.start();
						}
					}
					return;
				}
	
				numberOfErrors_PinpadSearch_Run_InitialziationFailures++;
				Debug.println("***PINPAD ERROR: No pinpad found or could not connect to a pinpad");  
				/*
				 *  Make sure Main Menu has been started before displaying
				 *  error dialog, or it can get hidden behind the main menu. 
				 */
                while (!DeltaworksMainMenu.isStarted()) {
                    try {
                        // 100 milliseconds = 1/10 second
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }
                
				//The pinpad cannot been initialized and a pinpad is required: ask user to retry or cancel
				int iDialogOption = Dialogs.showConfirm("dialogs/DialogPinpadConnectionError.xml", 
						Messages.getString("MGRWizardPage1.PinpadConnectionErrorExplanation"), 
						Messages.getString("MGRWizardPage1.PinpadConnectionErrorInstructions1"),	 
						Messages.getString("MGRWizardPage1.PinpadConnectionErrorInstructions2"),	
						Messages.getString("MGRWizardPage1.PinpadConnectionErrorInstructions3")); 	
				if(iDialogOption != Dialogs.OK_OPTION) {
					isPinpadInitializing = false;

					//Close the "Checking pinpad, please wait" pop up window
			        checkForPinpadFrame.dispose();

			        if(isAllowMockProfilePinpadOptions == false) {
						//Notify that the pinpad has not been found
						if (pinpadListener != null) {
							Thread t = new Thread() {
								@Override
								public void run() {
									pinpadListener.callback(PinpadReturnCode.CANCELED);
								}
							};
							t.start();
						}
					}
					return;
				}
				checkForPinpadFrame.toFront();
			}
		}
	}

	
	//***** Pinpad Private Methods *****
	
	/*
	 * Pinpad() constructor
	 */
	private Pinpad() {
		//Initialize the "initialization" variables
		isPinpadInitializing = false;
		setPinpadComPortIndex(-1);
		
    	//Get the list of serial (and USB) COM port names
        ArrayList<String> portNames = new ArrayList<String>();
    	Enumeration<CommPortIdentifier> portIds = CommPortIdentifier.getPortIdentifiers ();    	
    	while (portIds.hasMoreElements ()) {
    		CommPortIdentifier portId = portIds.nextElement ();
    		if (portId.getPortType () == CommPortIdentifier.PORT_SERIAL) {
    			portNames.add(portId.getName());
    		} 
    	}

    	//Allocate number available COM port names and objects
    	saAvailableComPorts = portNames.toArray(new String[portNames.size()]);
		comPorts = new ComPort[saAvailableComPorts.length];

		//Get the pinpad profile options
		updateProfileOptions();
	}

	/*
	 * waitForIdleWithTimeout(int, int)
	 */
	private boolean waitForIdleWithTimeout(int pm_iMilliseconds, int pm_iComPortIndex) {
		//Wait for pinpad state to go idle because a response receive; if not, handle timeout 
		for(int ii = 0; ii < pm_iMilliseconds; ii += SLEEP_WAIT_INTERVAL_IN_MSEC) {
			if(comPorts[pm_iComPortIndex].pinpadState == PinpadStates.IDLE) {
				return true;
			}
			try {
				Thread.sleep(SLEEP_WAIT_INTERVAL_IN_MSEC);
			} catch (InterruptedException ie) {
				numberOfErrors_waitForIdleWithTimeout_InterruptedException++;
				Debug.printStackTrace(ie);
			}
		}
		
		/*
		 * A timeout occurred for one of two of the following reasons: 
		 * 		1. Initialization is occurring and this is a non-pinpad COM port
		 * 		2. Or, something is very wrong with the pinpad connection.
		 * So set status to a timeout, then check the which reason
		 */
		comPorts[pm_iComPortIndex].pinpadStatus = PinpadStatuses.TIMEOUT;
		if(isPinpadInitializing == true) {
			//Initializing: probably a non-pinpad COM port, exit 
			comPorts[pm_iComPortIndex].pinpadState = PinpadStates.IDLE;
			return false;
		} else {
			/*
			 * Bad news: a pinpad was previously found, but it is now not
			 * working. So try to initialize again and hope to find a pinpad.
			 */
			final PinpadSearch pinpadSearch = new PinpadSearch();
		    pinpadSearch.start();
		    return false;
		}
	}

	/*
	 * formatDisplayLine(byte[][], int, int)
	 */
	private void formatDisplayLine(byte[][] byOutputMessage, int iOutputIndex, int iLineIndex) {
		int iSpacesToAdd = (DISPLAY_LINE_LENGTH_IN_CHARACTERS - iOutputIndex + 1) / 2; 
		
		//Add spaces at the end
		for(int ii = iOutputIndex; ii < DISPLAY_LINE_LENGTH_IN_CHARACTERS; ii++) {
			byOutputMessage[iLineIndex][ii] = ' ';
		}

		//Center the line
		if(iSpacesToAdd != 0) {
			for(int ii = iOutputIndex - 2; ii >= 0 ; ii--) {
				byOutputMessage[iLineIndex][ii + iSpacesToAdd] = 
					byOutputMessage[iLineIndex][ii];
			}
		}
		
		//Add spaces in front of the message
		for(int ii = 0; ii < iSpacesToAdd; ii++) {
			byOutputMessage[iLineIndex][ii] = ' '; 
		}
	}

	/*
	 * formatDisplayScreen(String, int)
	 */
	private String formatDisplayScreen(String pm_strMessage, int pm_iNumberOfLines) {
		int iLineIndex = 0;
		int iInputIndex = 0;
		int iOutputIndex = 0;
		int iIndexStartOfWord;
		StringBuffer sbReturnMessage = new StringBuffer();
		byte[][] byOutputMessage = new byte
			[DISPLAY_MAXIMUM_NUMBER_OF_LINES][DISPLAY_LINE_LENGTH_IN_CHARACTERS];
		byte[] byInputMessage = pm_strMessage.getBytes();

		//Format the input message to be centered trying to not to wrap a word
		//unless it is bigger than one line. A message that can't fit is truncated.
		FormatScreen:while(iLineIndex < pm_iNumberOfLines) {
			//Format the next line
			while(iInputIndex < byInputMessage.length) {
				//Ignore any leading spaces
				for(; iInputIndex < byInputMessage.length; iInputIndex++) {
					if(byInputMessage[iInputIndex] != ' ') {
						break;
					}
				}
				
				//Get next word (terminated by a space)
				iIndexStartOfWord = iInputIndex;
				for(; iInputIndex < byInputMessage.length; iInputIndex++) {
					if(byInputMessage[iInputIndex] == ' ') {
						break;
					}
				}
				
				//Is the word larger than the display length or even more that a multiple number of lines?
				while(iInputIndex - iIndexStartOfWord > DISPLAY_LINE_LENGTH_IN_CHARACTERS) {
					//The word is larger than display length, can't avoid wrapping the word
					for(; iOutputIndex < DISPLAY_LINE_LENGTH_IN_CHARACTERS; 
							iIndexStartOfWord ++, iOutputIndex++) {
						byOutputMessage[iLineIndex][iOutputIndex] = 
							byInputMessage[iIndexStartOfWord];
					}
					//Is this the last line?
					iOutputIndex = 0;
					if(++iLineIndex >= pm_iNumberOfLines) {
						//No more words can be fit on the screen
						break FormatScreen;
					}
				}

				//Can the word (or partial word) fit on the current line?
				int iWordLength = iInputIndex - iIndexStartOfWord;
				if( iWordLength > DISPLAY_LINE_LENGTH_IN_CHARACTERS - iOutputIndex) {
					//No, the word (or partial word) can't fit; pad current line with spaces at the end, if needed
					formatDisplayLine(byOutputMessage, iOutputIndex, iLineIndex);
							
					//Is this the last line?
					if(++iLineIndex >= pm_iNumberOfLines) {
						//No more words can be fit on the screen
						break FormatScreen;
					}

					//Format the next line; 
					iInputIndex = iIndexStartOfWord;
					iOutputIndex = 0;
					continue;
				}
					
				//Yes, the word can fit on the current line or on the next line; copy the word to the current line
				for(int ii = 0; ii < iWordLength; ii++, iOutputIndex++, iIndexStartOfWord++) {
					byOutputMessage[iLineIndex][iOutputIndex] = byInputMessage[iIndexStartOfWord];
				}

				//Add a space if not at end of line
				if(iOutputIndex < DISPLAY_LINE_LENGTH_IN_CHARACTERS) {
					byOutputMessage[iLineIndex][iOutputIndex++] = ' ';
				}
			}

		//No more characters to display; exit
			break; 
		}
		
		//Fill rest of display with ending spaces if needed
		if(iLineIndex < pm_iNumberOfLines) {
			formatDisplayLine(byOutputMessage, iOutputIndex, iLineIndex++);
		}
		for(; iLineIndex < pm_iNumberOfLines; iLineIndex++) {
			for(iOutputIndex = 0; iOutputIndex < DISPLAY_LINE_LENGTH_IN_CHARACTERS; iOutputIndex++) {
				byOutputMessage[iLineIndex][iOutputIndex] = ' '; 
			}
		}

		for(int ii = 0; ii < pm_iNumberOfLines; ii++) {
			sbReturnMessage.append(new String(byOutputMessage[ii]));
		}
		return sbReturnMessage.toString();
	}

	/*
	 * formatDisplayMessageOrFunctionKeyRequest(String, int, int)
	 */
	private void formatDisplayMessageOrFunctionKeyRequest(String pm_strMessage, int pm_iMode, int pm_iComPortIndex) {
		int ii;
		String strMessage;

		//Clear request command buffer, store STX and command
		comPorts[pm_iComPortIndex].sbRequestCommand.setLength(0);
		for (ii = 0; ii < DISPLAY_PROMPT_COMMAND.length(); ii++) {		//Message type
			comPorts[pm_iComPortIndex].sbRequestCommand.append(DISPLAY_PROMPT_COMMAND.substring(ii, ii + 1));
		}

		//Store, mode, toggle, number of lines, and starting line number
		comPorts[pm_iComPortIndex].sbRequestCommand.append(pm_iMode);		 						//Response request flag
		comPorts[pm_iComPortIndex].sbRequestCommand.append(0); 									//toggle

		strMessage = formatDisplayScreen(pm_strMessage, DISPLAY_MAXIMUM_NUMBER_OF_LINES);
																		//Number of lines
		comPorts[pm_iComPortIndex].sbRequestCommand.append(strMessage.length()/DISPLAY_LINE_LENGTH_IN_CHARACTERS);
		comPorts[pm_iComPortIndex].sbRequestCommand.append("1"); 	 					//Start line (design decision: always line 1)

		//Store the display or prompt message
		for (ii = 0; ii < strMessage.length(); ii++) {					//Prompt (1-4, lines 2-4 are optional) 
			comPorts[pm_iComPortIndex].sbRequestCommand.append(strMessage.substring(ii, ii + 1));
		}
	}
	
	/*
	 * outputDisplayRequest(int, int)
	 */
	private synchronized void outputDisplayRequest(int pm_pinpadState, int pm_iComPortIndex) { 
		comPorts[pm_iComPortIndex].pinpadState = pm_pinpadState;
		comPorts[pm_iComPortIndex].pinpadStatus = PinpadStatuses.BUSY; 
		comPorts[pm_iComPortIndex].sbRequestCommand.append(AsciiControlChars.ETX);					//ETC

		//Compute LRC checksum without the STX
		char chChecksum = 0;
		for (int ii = 1; ii < comPorts[pm_iComPortIndex].sbRequestCommand.length(); ii++) {
			chChecksum ^= comPorts[pm_iComPortIndex].sbRequestCommand.charAt(ii);
		}
		comPorts[pm_iComPortIndex].sbRequestCommand.append(chChecksum);							//LRC

		//Copy the request to the output buffer
		comPorts[pm_iComPortIndex].baRequestCommand = comPorts[pm_iComPortIndex].sbRequestCommand.toString().getBytes();

		StringBuffer dbgLine  = new StringBuffer(comPorts[pm_iComPortIndex].strComName + "-" +comPorts[pm_iComPortIndex].numberOfPinpadRequestsBegun + "->Pinpad:") ;
		for (int ii = 0; ii < comPorts[pm_iComPortIndex].baRequestCommand.length; ii++) {
			dbgLine.append(AsciiControlChars.toString((char)comPorts[pm_iComPortIndex].baRequestCommand[ii]));
		}

		//Save request in the circular buffer for debugging
		comPorts[pm_iComPortIndex].numberOfPinpadRequestsBegun++;
		for (int ii = 0; ii < comPorts[pm_iComPortIndex].baRequestCommand.length; ii++) {
			if(++comPorts[pm_iComPortIndex].IoCircularBufferLogIndex >= IO_CIRCULAR_BUFFER_LOG_SIZE) {
				comPorts[pm_iComPortIndex].IoCircularBufferLogIndex = 0;
			}
			comPorts[pm_iComPortIndex].byOutputBuffer[ii] =comPorts[pm_iComPortIndex]. baRequestCommand[ii];
			comPorts[pm_iComPortIndex].iIoCircularBufferLog[comPorts[pm_iComPortIndex].IoCircularBufferLogIndex] = 
				comPorts[pm_iComPortIndex].byOutputBuffer[ii];
		}

		//Expect: ACK, STX, <data>, ETX, and LRC
		comPorts[pm_iComPortIndex].iNumberCharsReceived = 0;
		comPorts[pm_iComPortIndex].iNumberCharsExpected = strResponses[pm_pinpadState].length() + 2;
		sbSerialNumber.setLength(0);

		comPorts[pm_iComPortIndex].pinpadEventState = PinpadEventStates.WAITING_FOR_SINGLE_CHAR;
		for(int ii = 0; ii < comPorts[pm_iComPortIndex].byInputBuffer.length; ii++) {
			comPorts[pm_iComPortIndex].byInputBuffer[ii] = 0;
		}
		
		/*
		 * Start a timeout thread to prevent any COM output from blocking forever. 
		 * One such device that blocks forever is the Intel Active Management 
		 * Technology - SOL (Serial Over LAN) console-redirection COM device.
		 */
		Debug.print(dbgLine.toString()); 
		Debug.println(""); 
		Runnable r = new ComPortOutputTimeoutThread(pm_iComPortIndex);
		Thread t = new Thread(null, r, "ComPortOutputTimeoutThread");
		t.start(); 

		//Output the request to the pinpad
		try {
			comPorts[pm_iComPortIndex].outputStream.write(comPorts[pm_iComPortIndex].baRequestCommand, 0, comPorts[pm_iComPortIndex].baRequestCommand.length);
		} catch (IOException ioe) {
			numberOfErrors_outputDisplayRequest_IoException++;
			Debug.printException(ioe);
		} catch (Exception e) {
			Debug.printException(e);
		}
	}
	
	/*
	 * sendCancelRequest(int)
	 * Request format: <STX>50.<ETC><LRC>
	 * Expected response: <STX>50.|0|<ETC><LRC>
	 */
	private boolean sendCancelRequest(int pm_iComPortIndex) {
		//If this is main testing or a failure occurred, return a failure
		if((isAllowMockProfilePinpadOptions == true) || (pm_iComPortIndex < 0)) {
			return false;
		}
		
		//Format the request
		comPorts[pm_iComPortIndex].sbRequestCommand.setLength(0);
		for (int ii = 0; ii < CANCEL_COMMAND.length(); ii++) {			//Message type
			comPorts[pm_iComPortIndex].sbRequestCommand.append(CANCEL_COMMAND.substring(ii, ii + 1));
		}

		outputDisplayRequest(PinpadStates.REQUEST_FOR_CANCEL, pm_iComPortIndex);
		return(waitForIdleWithTimeout(TIMEOUT_WAIT_FOR_NO_INPUT_IN_MSEC, pm_iComPortIndex));
	}
	
	/*
	 * getSerialNumberRequest(int)
	 * Request format: <STX>51.S<ETC><LRC>
	 * Expected response: <STX>51.0S<SerialNumber-16bytes><ETC><LRC>
	 */
	private void getSerialNumberRequest(int pm_iComPortIndex) {
		if(sendCancelRequest(pm_iComPortIndex) == false) {
			return;
		} else if(isPinpadInitializing == false) {
			 pm_iComPortIndex = iPinpadComPortIndex;
		}

		//Format the request
		comPorts[pm_iComPortIndex].sbRequestCommand.setLength(0);
		for (int ii = 0; ii < SERIAL_NUMBER_COMMAND.length(); ii++) {	//Message type
			comPorts[pm_iComPortIndex].sbRequestCommand.append(SERIAL_NUMBER_COMMAND.substring(ii, ii + 1));
		}
		
		//Send the request, wait for response with timeout, and return status
		outputDisplayRequest(PinpadStates.REQUEST_FOR_SERIAL_NUMBER, pm_iComPortIndex);
		waitForIdleWithTimeout(TIMEOUT_WAIT_FOR_NO_INPUT_IN_MSEC, pm_iComPortIndex);
	}
	
	/*
	 * sendBeepToneRequest(int, int) //0x0 = 1/8 second, ..., 0xF = 2 seconds
	 * Request format: <STX>5B.|<pm_chDurationIndex>|<ETC><LRC>
	 * Expected response: <STX>5B.|0|<ETC><LRC>
	 */
	private void sendBeepToneRequest(int pm_iDurationIndex, int pm_iComPortIndex) {
		//Defensive check
		pm_iDurationIndex %= 0xF;
		
		if(sendCancelRequest(pm_iComPortIndex) == false) {
			return;
		} else if(isPinpadInitializing == false) {
			 pm_iComPortIndex =  iPinpadComPortIndex;
		}

		//Format the request
		comPorts[pm_iComPortIndex].sbRequestCommand.setLength(0);
		for (int ii = 0; ii < BEEP_TONE_COMMAND.length(); ii++) {		//Message type
			comPorts[pm_iComPortIndex].sbRequestCommand.append(BEEP_TONE_COMMAND.substring(ii, ii + 1));
		}
		comPorts[pm_iComPortIndex].sbRequestCommand.append(pm_iDurationIndex);						//Beep duration
		
		//Send the request, wait for response with timeout, and return status
		outputDisplayRequest(PinpadStates.REQUEST_FOR_BEEP_TONE, pm_iComPortIndex);
		waitForIdleWithTimeout(TIMEOUT_WAIT_FOR_NO_INPUT_IN_MSEC, pm_iComPortIndex); 
	}
	
	
	/*
	 * sendDisplayClearRequest(int)
	 * Request format: <STX>59.|0=all lines|<ETC><LRC>
	 * Expected response: <STX>59.|0|<ETX><LRC>
	 */
	private void sendDisplayClearRequest(int pm_iComPortIndex) {
		if(sendCancelRequest(pm_iComPortIndex) == false) {
			return;
		} else if(isPinpadInitializing == false) {
			 pm_iComPortIndex =  iPinpadComPortIndex;
		}

		//Format the request
		comPorts[pm_iComPortIndex].sbRequestCommand.setLength(0);
		for (int ii = 0; ii < DISPLAY_CLEAR_COMMAND.length(); ii++) {	//Message type
			comPorts[pm_iComPortIndex].sbRequestCommand.append(DISPLAY_CLEAR_COMMAND.substring(ii, ii + 1));
		}
		comPorts[pm_iComPortIndex].sbRequestCommand.append("0");  					//Line (design decision: clear all lines)

		//Send the request, wait for response with timeout, and return status
		outputDisplayRequest(PinpadStates.REQUEST_FOR_DISPLAY_CLEAR, pm_iComPortIndex);
		waitForIdleWithTimeout(TIMEOUT_WAIT_FOR_NO_INPUT_IN_MSEC, pm_iComPortIndex); 
	}
	
	/*
	 * sendDisplayMessageRequest(String, int)
	 * Request format: <STX>58.|0|0|<iNumberLines>|1|<pm_strMessage>|<ETC><LRC>
	 * Expected response: <STX>58.|00|<ETC><LRC>
	 */
	private void sendDisplayMessageRequest(String pm_strMessage, int pm_iComPortIndex) {
		//Defensive check
		Debug.println("###sendDisplayMessageRequest: " + comPorts[pm_iComPortIndex].strComName);  
		if(pm_strMessage == null || pm_strMessage.length() == 0) {
			pm_strMessage = " "; 
		}
		
		if(sendCancelRequest(pm_iComPortIndex) == false) {
			return;
		} else if(isPinpadInitializing == false) {
			 pm_iComPortIndex =  iPinpadComPortIndex;
		}
		
		//Format the request, send it, wait for the response with timeout, and return status
		formatDisplayMessageOrFunctionKeyRequest(pm_strMessage, MODE_NO_RESPONSE, pm_iComPortIndex);
		outputDisplayRequest(PinpadStates.REQUEST_FOR_DISPLAY_MESSAGE, pm_iComPortIndex);
		waitForIdleWithTimeout(TIMEOUT_WAIT_FOR_NO_INPUT_IN_MSEC, pm_iComPortIndex); 
	}

	/*
	 * sendDisplayFunctionKeyRequest(String, int)
	 * Request format: <STX>58.|1|0|<iNumberLines>|1|<pm_strMessage>|<ETC><LRC>
	 * Expected response: <STX>58.|00|<FctKey-1byte><ETC><LRC>
	 */
	private void sendDisplayFunctionKeyRequest(String pm_strMessage, int pm_iComPortIndex) {
		//Defensive check
		if(pm_strMessage == null || pm_strMessage.length() == 0) {
			pm_strMessage = "Press a function key"; 
		}

		if(sendCancelRequest(pm_iComPortIndex) == false) {
			return;
		} else if(isPinpadInitializing == false) {
			 pm_iComPortIndex =  iPinpadComPortIndex;
		}
		
		//Format the request, send it, wait for the response with timeout, and return status
		formatDisplayMessageOrFunctionKeyRequest(pm_strMessage, MODE_FUNCTION_KEY_RESPONSE,pm_iComPortIndex);
		outputDisplayRequest(PinpadStates.REQUEST_FOR_DISPLAY_MESSAGE_FOR_KEY, pm_iComPortIndex);
		
		/*
		 * User gets the function key when notified via PageExitListener.PINPAD_INPUT_AVAIABLE
		 * and then checking that the getUserInput().length() is non-zero. 
		 */
		return;
	}
	
	/*
	 * sendDisplayPromptRequest(String, int, int, int, int)
	 * Request format: <STX>58.|<pm_chMode=,2,3,5,6, or 7>|0|<pm_chLineNumber>|
	 * 					<pm_strPrompt(must be 32 chars exact)>|<pm_strMaxInputLength><ETC><LRC>
	 * Expected response for mode 2,3,5,6,7: <STX>58.|00|<InputLength-2bytes><#bytes><CR><ETC><LRC>
	 */
	private void sendDisplayPromptRequest(String pm_strPrompt, 
			int pm_iMode, int pm_iLineNumber, int pm_iMaxInputLength, int pm_iComPortIndex) {
		String strPrompt;

		//Defensive checks
		if(pm_strPrompt == null || pm_strPrompt.length() == 0) {
			pm_strPrompt = "Press ENTER"; 
		}
		pm_iMode %= 8;
		if(pm_iLineNumber != DISPLAY_AT_LINE1_INPUT_AT_LINE2 && 
				pm_iLineNumber != DISPLAY_AT_LINE1_INPUT_AT_LINE3 && 
				pm_iLineNumber != DISPLAY_AT_LINE2_INPUT_AT_LINE4) {
			pm_iLineNumber = DISPLAY_AT_LINE1_INPUT_AT_LINE2;
		}
		pm_iMaxInputLength %= DISPLAY_MAX_INPUT_LENGTH;

		if(sendCancelRequest(pm_iComPortIndex) == false) {
			return;
		} else if(isPinpadInitializing == false) {
			 pm_iComPortIndex =  iPinpadComPortIndex;
		}
		
		//Format the request
		comPorts[pm_iComPortIndex].sbRequestCommand.setLength(0);
		for (int ii = 0; ii < DISPLAY_PROMPT_COMMAND.length(); ii++) {	//Message type
			comPorts[pm_iComPortIndex].sbRequestCommand.append(DISPLAY_PROMPT_COMMAND.substring(ii, ii + 1));
		}

		comPorts[pm_iComPortIndex].sbRequestCommand.append(pm_iMode); 								//Response Request flag
		comPorts[pm_iComPortIndex].sbRequestCommand.append(pm_iLineNumber);						//Start line
		comPorts[pm_iComPortIndex].sbRequestCommand.append('0'); 									//Start position (ignored because MIL below)

		strPrompt = formatDisplayScreen(pm_strPrompt, DISPLAY_PROMPT_MAXIMUM_LINES);
																		//Prompt (must be 32 characters)
		for (int ii = 0; ii < DISPLAY_PROMPT_MAXIMUM_LINES * DISPLAY_LINE_LENGTH_IN_CHARACTERS; ii++) {
			comPorts[pm_iComPortIndex].sbRequestCommand.append(strPrompt.substring(ii, ii + 1));
		}
		comPorts[pm_iComPortIndex].sbRequestCommand.append("FF"); 	 					//Prompt index (design decision: use the default, "FF"
		if(pm_iMaxInputLength < 10) {
			comPorts[pm_iComPortIndex].sbRequestCommand.append("0"); 					//Leading zero
		}
		comPorts[pm_iComPortIndex].sbRequestCommand.append(pm_iMaxInputLength);					//Maximum input length (MIL)

		//Send the request and return idle status (port listener will notify)
		outputDisplayRequest(PinpadStates.REQUEST_FOR_DISPLAY_PROMPT, pm_iComPortIndex);
		
		/*
		 * Return without the requested input prompt: 
		 * User gets the input input when notified via PageExitListener.PINPAD_INPUT_AVAIABLE
		 * and then checking that the getUserInput().length() is non-zero. 
		 */
		return;
	}

	/*
	 * initialize()
	 */
	@Override
	public void initialize() {
		//Update Pinpad options in case they have changed.
		updateProfileOptions();
		
		//Only do pinpad initialization if pinpad is required
		if(isPinpadRequired == false) {
			Debug.println("***Pinpad is NOT required***");  
			return;
		}
		
		final PinpadSearch pinpadSearch = new PinpadSearch();
	    pinpadSearch.start();
	}

	/*
	 * getIsPinpadRequired()
	 */
	@Override
	public boolean getIsPinpadRequired() {
	    return isPinpadRequired;
	}

	/*
	 * getIsPinRequired()
	 */
	@Override
	public boolean getIsPinRequired() {
	    return isPinRequired;
	}

	/*
	 * getPinpadStatus()
	 */
	@Override
	public int getPinpadStatus() {
		if(iPinpadComPortIndex < 0) {
			return PinpadStatuses.NO_PINPAD;
		} else {
			return comPorts[iPinpadComPortIndex].pinpadStatus;
		}
	}

	/*
	 * getSerialNumber()
	 */
	@Override
	public String getSerialNumber() {
		return sbSerialNumber.toString();
	}

	/*
	 * clearUserInput()
	 */
	public void clearUserInput() {
		sbUserInput.setLength(0);
	}

	/*
	 * getUserInput()
	 */
	@Override
	public String getUserInput() {
		return sbUserInput.toString();
	}

	/*
	 * logStatistics()
	 */
	@Override
	public void logStatistics() {
		if(iPinpadComPortIndex >= 0) {
			ExtraDebug.println("***Pinpad statistics start***"); 
		
			ExtraDebug.println("Number of pinpad requests begun=" +  
					comPorts[iPinpadComPortIndex].numberOfPinpadRequestsBegun);
			comPorts[iPinpadComPortIndex].numberOfPinpadRequestsBegun = 0;
			ExtraDebug.println("Number of pinpad requests completed=" +  
					comPorts[iPinpadComPortIndex].numberOfPinpadRequestsCompleted);
			comPorts[iPinpadComPortIndex].numberOfPinpadRequestsCompleted = 0;
			ExtraDebug.println("Number of pinpad power ups=" +  
					comPorts[iPinpadComPortIndex].numberOfPinpadPowerUps);
			comPorts[iPinpadComPortIndex].numberOfPinpadPowerUps = 0;
			ExtraDebug.println("Number of pinpad non-power ups=" +  
					comPorts[iPinpadComPortIndex].numberOfPinpadNonPowerUps);
			comPorts[iPinpadComPortIndex].numberOfPinpadNonPowerUps = 0;
			ExtraDebug.println("Number of pinpad successful initializations=" +  
					comPorts[iPinpadComPortIndex].numberOfPinpadSuccessInitializations);
			comPorts[iPinpadComPortIndex].numberOfPinpadSuccessInitializations = 0;
		
			//Error statistics
			if(numberOfErrors_PinpadSearch_Run_InitialziationFailures != 0) {
				ExtraDebug.println("numberOfErrors_PinpadSearch_Run_InitialziationFailures=" +  
						numberOfErrors_PinpadSearch_Run_InitialziationFailures);
				numberOfErrors_PinpadSearch_Run_InitialziationFailures = 0;
			}
			if(numberOfErrors_waitForIdleWithTimeout_InterruptedException != 0) {
				ExtraDebug.println("numberOfErrors_waitForIdleWithTimeout_InterruptedException=" +  
						numberOfErrors_waitForIdleWithTimeout_InterruptedException);
				numberOfErrors_waitForIdleWithTimeout_InterruptedException = 0;
			}
			if(numberOfErrors_outputDisplayRequest_IoException != 0) {
				ExtraDebug.println("numberOfErrors_outputDisplayRequest_IoException=" +  
						numberOfErrors_outputDisplayRequest_IoException);
				numberOfErrors_outputDisplayRequest_IoException = 0;
			}

			if(comPorts[iPinpadComPortIndex].numberOfPinpadRequestsBegun != 0) {
				ExtraDebug.println("numberOfPinpadRequestsBegun=" +  
						comPorts[iPinpadComPortIndex].numberOfPinpadRequestsBegun);
				comPorts[iPinpadComPortIndex].numberOfPinpadRequestsBegun = 0;
			}
			if(comPorts[iPinpadComPortIndex].numberOfPinpadRequestsCompleted != 0) {
				ExtraDebug.println("numberOfPinpadRequestsCompleted=" +  
						comPorts[iPinpadComPortIndex].numberOfPinpadRequestsCompleted);
				comPorts[iPinpadComPortIndex].numberOfPinpadRequestsCompleted = 0;
			}
			if(comPorts[iPinpadComPortIndex].numberOfPinpadAcksReceived != 0) {
				ExtraDebug.println("numberOfPinpadAcksReceived=" +  
						comPorts[iPinpadComPortIndex].numberOfPinpadAcksReceived);
				comPorts[iPinpadComPortIndex].numberOfPinpadAcksReceived = 0;
			}
			if(comPorts[iPinpadComPortIndex].numberOfPinpadPowerUps != 0) {
				ExtraDebug.println("numberOfPinpadPowerUps=" +  
						comPorts[iPinpadComPortIndex].numberOfPinpadPowerUps);
				comPorts[iPinpadComPortIndex].numberOfPinpadPowerUps = 0;
			}
			if(comPorts[iPinpadComPortIndex].numberOfPinpadNonPowerUps != 0) {
				ExtraDebug.println("numberOfPinpadNonPowerUps=" +  
						comPorts[iPinpadComPortIndex].numberOfPinpadNonPowerUps);
				comPorts[iPinpadComPortIndex].numberOfPinpadNonPowerUps = 0;
			}
			if(comPorts[iPinpadComPortIndex].numberOfPinpadSuccessInitializations != 0) {
				ExtraDebug.println("numberOfPinpadSuccessInitializations=" +  
						comPorts[iPinpadComPortIndex].numberOfPinpadSuccessInitializations);
				comPorts[iPinpadComPortIndex].numberOfPinpadSuccessInitializations = 0;
			}

			if(comPorts[iPinpadComPortIndex].numberOfErrors_sendAcknowledge_IoException != 0) {
				ExtraDebug.println("numberOfErrors_sendAcknowledge_IoException=" +  
						comPorts[iPinpadComPortIndex].numberOfErrors_sendAcknowledge_IoException);
				comPorts[iPinpadComPortIndex].numberOfErrors_sendAcknowledge_IoException = 0;
			}
			if(comPorts[iPinpadComPortIndex].numberOfErrors_getCharacters_Available_InvalidEvent != 0) {
				ExtraDebug.println("numberOfErrors_getCharacters_Available_InvalidEvent=" +  
						comPorts[iPinpadComPortIndex].numberOfErrors_getCharacters_Available_InvalidEvent);
				comPorts[iPinpadComPortIndex].numberOfErrors_getCharacters_Available_InvalidEvent = 0;
			}
			if(comPorts[iPinpadComPortIndex].numberOfErrors_getCharacters_tooManyAvailable_InvalidEvent != 0) {
				ExtraDebug.println("numberOfErrors_getCharacters_tooManyAvailable_InvalidEvent=" +  
						comPorts[iPinpadComPortIndex].numberOfErrors_getCharacters_tooManyAvailable_InvalidEvent);
				comPorts[iPinpadComPortIndex].numberOfErrors_getCharacters_tooManyAvailable_InvalidEvent = 0;
			}
			if(comPorts[iPinpadComPortIndex].numberOfErrors_getCharacters_Available_IoException != 0) {
				ExtraDebug.println("numberOfErrors_getCharacters_Available_IoException=" +  
						comPorts[iPinpadComPortIndex].numberOfErrors_getCharacters_Available_IoException);
				comPorts[iPinpadComPortIndex].numberOfErrors_getCharacters_Available_IoException = 0;
			}
			if(comPorts[iPinpadComPortIndex].numberOfErrors_getCharacters_Read_EndOfFile != 0) {
				ExtraDebug.println("numberOfErrors_getCharacters_Read_EndOfFile=" +  
						comPorts[iPinpadComPortIndex].numberOfErrors_getCharacters_Read_EndOfFile);
				comPorts[iPinpadComPortIndex].numberOfErrors_getCharacters_Read_EndOfFile = 0;
			}
			if(comPorts[iPinpadComPortIndex].numberOfErrors_getCharacters_Read_IoException != 0) {
				ExtraDebug.println("numberOfErrors_getCharacters_Read_IoException=" +  
						comPorts[iPinpadComPortIndex].numberOfErrors_getCharacters_Read_IoException);
				comPorts[iPinpadComPortIndex].numberOfErrors_getCharacters_Read_IoException = 0;
			}
			if(comPorts[iPinpadComPortIndex].numberOfErrors_validateResponseAndAck_ChecksumError != 0) {
				ExtraDebug.println("numberOfErrors_validateResponseAndAck_ChecksumError=" +  
						comPorts[iPinpadComPortIndex].numberOfErrors_validateResponseAndAck_ChecksumError);
				comPorts[iPinpadComPortIndex].numberOfErrors_validateResponseAndAck_ChecksumError = 0;
			}
			if(comPorts[iPinpadComPortIndex].numberOfErrors_processSingleCharReceived_UnexpectedEnq != 0) {
				ExtraDebug.println("numberOfErrors_processSingleCharReceived_UnexpectedEnq=" +  
						comPorts[iPinpadComPortIndex].numberOfErrors_processSingleCharReceived_UnexpectedEnq);
				comPorts[iPinpadComPortIndex].numberOfErrors_processSingleCharReceived_UnexpectedEnq = 0;
			}
			if(comPorts[iPinpadComPortIndex].numberOfErrors_processSingleCharReceived_Nak != 0) {
				ExtraDebug.println("numberOfErrors_processSingleCharReceived_Nak=" +  
						comPorts[iPinpadComPortIndex].numberOfErrors_processSingleCharReceived_Nak);
				comPorts[iPinpadComPortIndex].numberOfErrors_processSingleCharReceived_Nak = 0;
			}
			if(comPorts[iPinpadComPortIndex].numberOfErrors_processSingleCharReceived_UnexpectedStx != 0) {
				ExtraDebug.println("numberOfErrors_processSingleCharReceived_UnexpectedStx=" +  
						comPorts[iPinpadComPortIndex].numberOfErrors_processSingleCharReceived_UnexpectedStx);
				comPorts[iPinpadComPortIndex].numberOfErrors_processSingleCharReceived_UnexpectedStx = 0;
			}
			if(comPorts[iPinpadComPortIndex].numberOfErrors_processSingleCharReceived_NotAckEnqNakStx != 0) {
				ExtraDebug.println("numberOfErrors_processSingleCharReceived_NotAckEnqNakStx=" +  
						comPorts[iPinpadComPortIndex].numberOfErrors_processSingleCharReceived_NotAckEnqNakStx);
				comPorts[iPinpadComPortIndex].numberOfErrors_processSingleCharReceived_NotAckEnqNakStx = 0;
			}
			if(comPorts[iPinpadComPortIndex].numberOfErrors_processStxPacketResponseBegin_InvalidResponse != 0) {
				ExtraDebug.println("numberOfErrors_processStxPacketResponseBegin_InvalidResponse=" +  
						comPorts[iPinpadComPortIndex].numberOfErrors_processStxPacketResponseBegin_InvalidResponse);
				comPorts[iPinpadComPortIndex].numberOfErrors_processStxPacketResponseBegin_InvalidResponse = 0;
			}
			if(comPorts[iPinpadComPortIndex].numberOfErrors_processStxPacketResponseBegin_InvalidChars != 0) {
				ExtraDebug.println("numberOfErrors_processStxPacketResponseBegin_InvalidChars=" +  
						comPorts[iPinpadComPortIndex].numberOfErrors_processStxPacketResponseBegin_InvalidChars);
				comPorts[iPinpadComPortIndex].numberOfErrors_processStxPacketResponseBegin_InvalidChars = 0;
			}
			if(comPorts[iPinpadComPortIndex].numberOfErrors_processStxPacketResponseBegin_StatusError != 0) {
				ExtraDebug.println("numberOfErrors_processStxPacketResponseBegin_StatusError=" +  
						comPorts[iPinpadComPortIndex].numberOfErrors_processStxPacketResponseBegin_StatusError);
				comPorts[iPinpadComPortIndex].numberOfErrors_processStxPacketResponseBegin_StatusError = 0;
			}
			if(comPorts[iPinpadComPortIndex].numberOfErrors_processStxPacketResponseBegin_InvalidPromptLength != 0) {
				ExtraDebug.println("numberOfErrors_processStxPacketResponseBegin_InvalidPromptLength=" +  
						comPorts[iPinpadComPortIndex].numberOfErrors_processStxPacketResponseBegin_InvalidPromptLength);
				comPorts[iPinpadComPortIndex].numberOfErrors_processStxPacketResponseBegin_InvalidPromptLength = 0;
			}
			if(comPorts[iPinpadComPortIndex].numberOfErrors_SerialEvent_InvalidEvent != 0) {
				ExtraDebug.println("numberOfErrors_SerialEvent_InvalidEvent=" +  
						comPorts[iPinpadComPortIndex].numberOfErrors_SerialEvent_InvalidEvent);
				comPorts[iPinpadComPortIndex].numberOfErrors_SerialEvent_InvalidEvent = 0;
			}
			if(comPorts[iPinpadComPortIndex].numberOfErrors_serialEvent_invalid_switch_value != 0) {
				ExtraDebug.println("numberOfErrors_serialEvent_invalid_switch_value=" +  
						comPorts[iPinpadComPortIndex].numberOfErrors_serialEvent_invalid_switch_value);
				comPorts[iPinpadComPortIndex].numberOfErrors_serialEvent_invalid_switch_value = 0;
			}
			if(comPorts[iPinpadComPortIndex].numberOfErrors_isComPortConnectedToPinpad_sendDisplayMessageRequest != 0) {
				ExtraDebug.println("numberOfErrors_isComPortConnectedToPinpad_sendDisplayMessageRequest=" +  
						comPorts[iPinpadComPortIndex].numberOfErrors_isComPortConnectedToPinpad_sendDisplayMessageRequest);
				comPorts[iPinpadComPortIndex].numberOfErrors_isComPortConnectedToPinpad_sendDisplayMessageRequest = 0;
			}
			if(comPorts[iPinpadComPortIndex].numberOfErrors_isComPortConnectedToPinpad_InterruptedException != 0) {
				ExtraDebug.println("numberOfErrors_isComPortConnectedToPinpad_InterruptedException=" +  
						comPorts[iPinpadComPortIndex].numberOfErrors_isComPortConnectedToPinpad_InterruptedException);
				comPorts[iPinpadComPortIndex].numberOfErrors_isComPortConnectedToPinpad_InterruptedException = 0;
			}
			if(comPorts[iPinpadComPortIndex].numberOfErrors_isComPortConnectedToPinpad_getSerialNumberRequest != 0) {
				ExtraDebug.println("numberOfErrors_isComPortConnectedToPinpad_getSerialNumberRequest=" +  
						comPorts[iPinpadComPortIndex].numberOfErrors_isComPortConnectedToPinpad_getSerialNumberRequest);
				comPorts[iPinpadComPortIndex].numberOfErrors_isComPortConnectedToPinpad_getSerialNumberRequest = 0;
			}

			ExtraDebug.println("***Pinpad statistics end***"); 
		}
	}
	
	/*
	 * sendBeepToneRequest(int) //0x0 = 1/8 second, ..., 0xF = 2 seconds
	 * Request format: <STX>5B.|<pm_chDurationIndex>|<ETC><LRC>
	 * Expected response: <STX>5B.|0|<ETC><LRC>
	 */
	@Override
	public void sendBeepToneRequest(int pm_iDurationIndex) {
		sendBeepToneRequest(pm_iDurationIndex, iPinpadComPortIndex);
	}
	
	/*
	 * sendDisplayClearRequest()
	 * Request format: <STX>59.|0=all lines|<ETC><LRC>
	 * Expected response: <STX>59.|0|<ETX><LRC>
	 */
	@Override
	public void sendDisplayClearRequest() {
		sendDisplayClearRequest(iPinpadComPortIndex);
	}
	
	/*
	 * sendDisplayMessageRequest(String)
	 * Request format: <STX>58.|0|0|<iNumberLines>|1|<pm_strMessage>|<ETC><LRC>
	 * Expected response: <STX>58.|00|<ETC><LRC>
	 */
	@Override
	public void sendDisplayMessageRequest(String pm_strMessage) {
		sendDisplayMessageRequest(pm_strMessage, iPinpadComPortIndex);
	}

	/*
	 * sendDisplayFunctionKeyRequest(String)
	 * Request format: <STX>58.|1|0|<iNumberLines>|1|<pm_strMessage>|<ETC><LRC>
	 * Expected response: <STX>58.|00|<FctKey-1byte><ETC><LRC>
	 */
	@Override
	public void sendDisplayFunctionKeyRequest(String pm_strMessage) {
		sendDisplayFunctionKeyRequest(pm_strMessage, iPinpadComPortIndex);
	}
	
	/*
	 * sendDisplayPromptRequest(String, int, int, int)
	 * Request format: <STX>58.|<pm_chMode=,2,3,5,6, or 7>|0|<pm_chLineNumber>|
	 * 					<pm_strPrompt(must be 32 chars exact)>|<pm_strMaxInputLength><ETC><LRC>
	 * Expected response for mode 2,3,5,6,7: <STX>58.|00|<InputLength-2bytes><#bytes><CR><ETC><LRC>
	 */
	@Override
	public void sendDisplayPromptRequest(String pm_strPrompt,
			int pm_iMode, int pm_iLineNumber, int pm_iMaxInputLength) {
		sendDisplayPromptRequest(pm_strPrompt, 
				pm_iMode, pm_iLineNumber, pm_iMaxInputLength, iPinpadComPortIndex);
	}
	
	/*
	 * setObjectPage(Object)
	 */
	@Override
	public void setPinpadListener(PinpadListener pinpadListener) {
		//Save object page for notifying caller when initialized or user input
		this.pinpadListener = pinpadListener;
	}
	
	private static void setPinpadComPortIndex(int i) {
		iPinpadComPortIndex = i;
	}
	
	/*
	 * updateProfileOptions()
	 */
	@Override
	public void updateProfileOptions() {
		if(isAllowMockProfilePinpadOptions == true) {
			//For pinpad's main testing: set profile options and do initialization 
			isPinpadRequired = true;
			isPinRequired = true;
			if(iPinpadComPortIndex >= 0) {
				comPorts[iPinpadComPortIndex].shutdownComPort();
			}
			setPinpadComPortIndex(-1);
		} else {
            boolean bPinpadRequiredPrevious = isPinpadRequired;

            isPinpadRequired = false;
			isPinRequired = false;
				
		    //Check if profile editor profile parameters are available
			Profile profile = UnitProfile.getInstance().getProfileInstance();
			if(profile != null) {
			    //Check if profile editor profile product MoneyGram receive parameters are available
				ProfileItem item = profile.find("PRODUCT", MONEY_GRAM_RECEIVE_DOC_SEQ);
				if(item != null) {
				    //Check if profile editor pinpad parameters are available
				    ProfileItem piPinDevice = item.get("PIN_DEVICE");
				    if(piPinDevice != null) {
					    String sPinDevice = piPinDevice.stringValue();	
					    //See if pinpad is required and/or pin is required
					    isPinpadRequired = (sPinDevice.equalsIgnoreCase(PINPAD_DEVICE_INGENICO_IPP320) ||
					    		sPinDevice.equalsIgnoreCase(PINPAD_DEVICE_INGENICO_IPP350)) ? true : false; 
					
					    ProfileItem piPinType = item.get("PIN_TYPE");
					    if (piPinType != null) {
						    String sPinType = piPinType.stringValue();	
						    isPinRequired = sPinType.equalsIgnoreCase(PINPAD_TYPE_4_DIGIT_SURNAME) ? true : false;
					    } else {
					    	isPinRequired = false;
					    }

					    //Pinpad is newly required: need to do initialization
			            if((isPinpadRequired == true) && (bPinpadRequiredPrevious == false)) {
			    			if(iPinpadComPortIndex >= 0) {
			    				comPorts[iPinpadComPortIndex].shutdownComPort();
			    			}
			    			setPinpadComPortIndex(-1);
			            }
				    }
				}
			}
		}
	}

	
	//***** Main Methods *****
	
	/*
	 * sleep(int)
	 */
	private static void sleep(int pm_iMilliseconds) {
		try {
			Thread.sleep(pm_iMilliseconds);
		} catch (InterruptedException ie) {
			Debug.println("sleep(), InterruptedException" + ie);  
			Debug.dumpStack();
		}
	}

	private static void waitAndCheckStatusAndGetUserInput(PinpadInterface pm_pi) {
		int iPinpadStatus;

		while(true ) {
			sleep(SLEEP_WAIT_INTERVAL_IN_MSEC);

			iPinpadStatus = pm_pi.getPinpadStatus();
			if(iPinpadStatus != PinpadStatuses.BUSY) {
				if((iPinpadStatus == PinpadStatuses.SUCCESS) && 
						(pm_pi.getUserInput().length() != 0)) {
					Debug.println("    User input=" + pm_pi.getUserInput()); 
				} else {
					Debug.println("    ***MAIN ERROR, iPinpadStatus=" + iPinpadStatus); 
				}
				break;
			}
		}
	}


	/*
	 * main(String[])
	 */
	public static void main(String[] args) {
		final int DISPLAY_MAX_PIN_INPUT_LENGTH 		= 4;
		final int DISPLAY_MAX_REFERENCE_INPUT_LENGTH = 8;
		
		// Test strings
		final String DISPLAY_WELCOME = "Welcome to DeltaWorks. Please wait for instructions"; 
		final String DISPLAY_ENTER_FUNCTION_KEY = "Press a function key"; 
		final String DISPLAY_ENTER_ALPHANUMERIC_PIN = "Input alphanumeric pin"; 
		final String DISPLAY_ENTER_AMOUNT = "Input amount"; 
		final String DISPLAY_INPUT_ALPHANUMERIC_DATA = "Input data in alphanumeric"; 
		final String DISPLAY_INPUT_PIN = "Please enter pin to proceed"; 
		final String DISPLAY_INPUT_REFERENCE_NUMBER = "Input reference number"; 
		final String DISPLAY_BIG_WELCOME = 
			"1234567891123456789212345678931234567894 BIG WELCOME"; 
		final String DISPLAY_TOO_BIG_WELCOME = 
			"12345678911234567892123456789312345678941234567895123456789612345678971234567898 TOO BIG WELCOME"; 

	    //This must be done so we get debug output that is not encrypted
		AgentConnect.enableDebugEncryption();

		Debug.println("Pinpad main() test start"); 

		//Use mock profile options.
		isAllowMockProfilePinpadOptions = true;

		//Initialize pinpad
		INSTANCE.initialize();

		//Kludge for now, wait a bit for pinpad to initialize
		sleep(10000);
		
		//1. Beep
		INSTANCE.sendBeepToneRequest(BEEP_DURATION_MINIMUM);

		if(INSTANCE.getPinpadStatus() != PinpadStatuses.SUCCESS) {
			Debug.println("Pinpad main() test abort, " + 
					"pinpadStatus=" + INSTANCE.getPinpadStatus()); 
			System.exit(INSTANCE.getPinpadStatus());
		}

		//2. Display message
		INSTANCE.sendDisplayMessageRequest(DISPLAY_WELCOME);
		sleep(2500);
		
		//3. Display message asking for a function key
		INSTANCE.sendDisplayFunctionKeyRequest(DISPLAY_ENTER_FUNCTION_KEY);
		waitAndCheckStatusAndGetUserInput(INSTANCE);
		
		//4. Display prompt for alphanumeric data
		INSTANCE.sendDisplayPromptRequest(
			DISPLAY_INPUT_ALPHANUMERIC_DATA, 
			MODE_ALPHANUMERIC_BUFFER, DISPLAY_AT_LINE1_INPUT_AT_LINE3, 
			DISPLAY_MAX_REFERENCE_INPUT_LENGTH);
		waitAndCheckStatusAndGetUserInput(INSTANCE);

		//5. Display prompt for numeric data
		INSTANCE.sendDisplayPromptRequest(
			DISPLAY_INPUT_REFERENCE_NUMBER, 
			MODE_NUMERIC_BUFFER, DISPLAY_AT_LINE1_INPUT_AT_LINE3,  
			DISPLAY_MAX_REFERENCE_INPUT_LENGTH);
		waitAndCheckStatusAndGetUserInput(INSTANCE);

		//6. Display prompt for alphanumeric pin
		INSTANCE.sendDisplayPromptRequest(
			DISPLAY_ENTER_ALPHANUMERIC_PIN,	
			MODE_PASSWORD_ALPHANUMERIC_BUFFER, DISPLAY_AT_LINE1_INPUT_AT_LINE3,  
			DISPLAY_MAX_PIN_INPUT_LENGTH );
		waitAndCheckStatusAndGetUserInput(INSTANCE);

		//7. Display prompt for formatted amount
		INSTANCE.sendDisplayPromptRequest(
			DISPLAY_ENTER_AMOUNT, 
			MODE_FORMATTED_AMOUNT_BUFFER, DISPLAY_AT_LINE1_INPUT_AT_LINE2,  
			DISPLAY_MAX_REFERENCE_INPUT_LENGTH);
		waitAndCheckStatusAndGetUserInput(INSTANCE);

		//8. Display prompt for numeric pin
		INSTANCE.sendDisplayPromptRequest(
			DISPLAY_INPUT_PIN, 
			MODE_PASSWORD_NUMERIC_BUFFER, DISPLAY_AT_LINE1_INPUT_AT_LINE3, 
			DISPLAY_MAX_PIN_INPUT_LENGTH );
		waitAndCheckStatusAndGetUserInput(INSTANCE);

		//9. Display message with a big word
		INSTANCE.sendDisplayMessageRequest(DISPLAY_BIG_WELCOME);
		sleep(2500);

		//10. Clear the screen
		INSTANCE.sendDisplayClearRequest();
		sleep(2500);

		//11. Display message that is larger than the screen
		INSTANCE.sendDisplayMessageRequest(DISPLAY_TOO_BIG_WELCOME);
		sleep(2500);

		//12. Display message
		INSTANCE.sendDisplayMessageRequest(DISPLAY_WELCOME);

		//All done
		Debug.println("Pinpad main() test complete"); 
		System.exit(0);
	}
}