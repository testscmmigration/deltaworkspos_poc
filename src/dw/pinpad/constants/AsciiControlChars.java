package dw.pinpad.constants;

//Use when JRE 1.6 can be used.
//public enum AsciiControlChars {
//    NUL("NUL"), SOH("SOH"), STX("STX"), ETX("ETX"), 
//    EOT("EOT"), ENQ("ENQ"), ACK("ACK"), BEL("ACK"),
//    BS("BS"),   HT("HT"),   LF("LF"),   VT("VT"),
//    FF("FF"),   CR("CR"),   SO("SO"),   SI("SI"),
//    DLE("DLE"), DC1("DC1"), DC2("DC2"), DC3("DC3"), 
//    DC4("DC4"), NAK("NAK"), SYN("SYN"), ETB("ETB"),
//    CAN("CAN"), EM("EM"),   SUB("SUB"), ESC("ESC"),
//    FS("FS"),   GS("GS"),   RS("RS"),   US("US");
//
//	private final String name;
//
//    AsciiControlChars(String name) {
//    	this.name = name;
//    }
//
//    public static String toString(char ch) {
//        if (ch >= ' ')
//            return "" + ch;
//        else
//            return ("<" + AsciiControlChars.values()[ch] + ">");
//    }
//}
//Delete next lines when JRE 1.6 can be used.	
public class AsciiControlChars {
    public static final char NUL = 0;
    public static final char SOH = (char) 0x01;
    public static final char STX = (char) 0x02;
    public static final char ETX = (char) 0x03;
    public static final char EOT = (char) 0x04;
    public static final char ENQ = (char) 0x05;
    public static final char ACK = (char) 0x06;
    public static final char BEL = (char) 0x07;
    public static final char BS = (char) 0x08;
    public static final char HT = (char) 0x09;
    public static final char LF = (char) 0x0a;
    public static final char VT = (char) 0x0b;
    public static final char FF = (char) 0x0c;
    public static final char CR = (char) 0x0d;
    public static final char SO = (char) 0x0e;
    public static final char SI = (char) 0x0f;
    public static final char DLE = (char) 0x10;
    public static final char DC1 = (char) 0x11;
    public static final char DC2 = (char) 0x12;
    public static final char DC3 = (char) 0x13;
    public static final char DC4 = (char) 0x14;
    public static final char NAK = (char) 0x15;
    public static final char SYN = (char) 0x16;
    public static final char ETB = (char) 0x17;
    public static final char CAN = (char) 0x18;
    public static final char EM = (char) 0x19;
    public static final char SUB = (char) 0x1a;
    public static final char ESC = (char) 0x1b;
    public static final char FS = (char) 0x1c;
    public static final char GS = (char) 0x1d;
    public static final char RS = (char) 0x1e;
    public static final char US = (char) 0x1f;
//End of delete for JRE 1.6-------------

    private static final String names[] = { 
        "NUL", "SOH", "STX", "ETX", "EOT", "ENQ", "ACK", "BEL", 
        "BS",  "HT",  "LF",  "VT",  "FF",  "CR",  "SO",  "SI",  
        "DLE", "DC1", "DC2", "DC3", "DC4", "NAK", "SYN", "ETB", 
        "CAN", "EM",  "SUB", "ESC", "FS",  "GS",  "RS",  "US"
    };

    public static String toString(char ch) {
		if (ch >= ' ')
		    return "" + ch;
		else
		    return "<" + names[ch] + ">";
    }
}
