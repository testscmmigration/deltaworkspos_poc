package dw.pinpad;

/**
  * API for the pinpad.
  * 
  * Note: The pinpad is a singleton object, instantiate it with the following:
  *		public|private final Pinpad pinpad = Pinpad.getInstance();
  *
  * Example:
  * 	private final Pinpad pinpad = Pinpad.getInstance(); 
  * 	...
  *		pinpad.initialize();
  *		...
  *		pinpad.setObjectPage(this);	//For specifying page when receiving page notification listener information
  *		...
  *		pinpad.sendDisplayMessageRequest(Messages.getString("Hello World")); 
  *		...
  */

public interface PinpadInterface {
	/*
	 * Pinpad constants:
	 */
	public final int MODE_NO_RESPONSE 					= 0;
	public final int MODE_FUNCTION_KEY_RESPONSE 		= 1;
	public final int MODE_ALPHANUMERIC_BUFFER			= 2;
	public final int MODE_NUMERIC_BUFFER 		   		= 3;
	public final int MODE_PASSWORD_ALPHANUMERIC_BUFFER	= 5;
	public final int MODE_FORMATTED_AMOUNT_BUFFER		= 6;
	public final int MODE_PASSWORD_NUMERIC_BUFFER 		= 7;
	
	//Beep durations: 0x0 = 1/8 second, ..., 0xF = 2 seconds
	public final int BEEP_DURATION_MINIMUM     			= 0;
	public final int BEEP_DURATION_MIDRANGE    			= 0;
	public final int BEEP_DURATION_MAXIMUM     			= 0xF;
	
	public final int CLEAR_ALL_LINES               		= 0;
	public final int DISPLAY_AT_LINE1_INPUT_AT_LINE2 	= 0;
	public final int DISPLAY_AT_LINE1_INPUT_AT_LINE3 	= 5;
	public final int DISPLAY_AT_LINE2_INPUT_AT_LINE4 	= 6;
	public final int DISPLAY_MAX_INPUT_LENGTH			= 16;
	public final int DISPLAY_PROMPT_MAXIMUM_LINES 		= 2;
	public final int DISPLAY_MAXIMUM_NUMBER_OF_LINES	= 4;
	public final int DISPLAY_LINE_LENGTH_IN_CHARACTERS 	= 16;
	public final int DISPLAY_SIZE_IN_CHARACTERS   		=
		DISPLAY_MAXIMUM_NUMBER_OF_LINES * DISPLAY_LINE_LENGTH_IN_CHARACTERS; 
	public final int DISPLAY_PROMPT_SIZE_IN_CHARACTERS	=
		DISPLAY_PROMPT_MAXIMUM_LINES * DISPLAY_LINE_LENGTH_IN_CHARACTERS; 
	public final int SERIAL_NUMBER_LENGTH				= 16;

	public enum PinpadReturnCode {
		PINPAD_INPUT_AVAIABLE,
		PINPAD_INIITIALIZED,
		CANCELED
	}
	
	public interface PinpadListener {
		public void callback(PinpadReturnCode returnCode);
	}
	
	/*
	 * Pinpad methods:
	 */
	public void initialize();
	public boolean getIsPinpadRequired();
	public boolean getIsPinRequired();
	public int getPinpadStatus();
	public String getSerialNumber();
	public String getUserInput();
	public void logStatistics();
	public void sendBeepToneRequest(int pm_iLoudnessIndex);
	public void sendDisplayClearRequest();
	public void sendDisplayMessageRequest(String pm_strMessage);
	public void sendDisplayFunctionKeyRequest(String pm_strMessage);
	public void sendDisplayPromptRequest(String pm_strPrompt, 
		int pm_iMode, int pm_iLineNumber, int pm_iMaxInputLength);
	public void setPinpadListener(PinpadListener pm_objectPage);
	public void updateProfileOptions();
}