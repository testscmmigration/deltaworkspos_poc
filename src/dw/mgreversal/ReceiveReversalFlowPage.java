package dw.mgreversal;

import dw.framework.PageFlowButtons;
import dw.mgreceive.MoneyGramReceiveFlowPage;

public abstract class ReceiveReversalFlowPage extends MoneyGramReceiveFlowPage {
	private static final long serialVersionUID = 1L;

	protected ReceiveReversalTransaction receiveReversalTransaction;

    public ReceiveReversalFlowPage(ReceiveReversalTransaction tran,
    		String xmlFileName, String name,
			String pageCode, PageFlowButtons buttons) {
		super(tran, xmlFileName, name, pageCode, buttons);
		this.receiveReversalTransaction = tran;
	}

    // subclass pages will override this to provide their own return value
    // handling.
	@Override
	public void commComplete(int commTag, Object returnValue) {
	}
}
