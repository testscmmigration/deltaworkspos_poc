package dw.mgreversal;

import dw.framework.ClientTransaction;
import dw.framework.PageNameInterface;
import dw.framework.TransactionInterface;

public abstract class ReceiveReversalTransactionInterface extends TransactionInterface {
	private static final long serialVersionUID = 1L;

	protected ReceiveReversalTransaction transaction;
    
    public ReceiveReversalTransactionInterface(String fileName,
    							 ReceiveReversalTransaction transaction,
                                 PageNameInterface naming,
                                 boolean sideBar) {
        super(fileName, naming, sideBar);
        this.transaction = transaction;
    }

    @Override
	public ClientTransaction getTransaction() {
        return transaction;
    }
}
