package dw.mgreversal;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.moneygram.agentconnect.BusinessError;
import com.moneygram.agentconnect.InfoBase;
import com.moneygram.agentconnect.SendReversalValidationResponse;

import dw.comm.MessageFacade;
import dw.comm.MessageFacadeError;
import dw.comm.MessageFacadeParam;
import dw.comm.MessageLogic;
import dw.framework.ClientTransaction;
import dw.framework.DataCollectionPanel;
import dw.framework.DataCollectionSet;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.FieldKey;
import dw.framework.FlowPage;
import dw.io.IsiReporting;
import dw.io.report.ReportLog;
import dw.main.DeltaworksStartup;
import dw.main.MainPanelDelegate;
import dw.mgreceive.MoneyGramReceiveTransaction;
import dw.model.adapters.SendReversalType;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.ExtraDebug;
import dw.utility.TimeUtility;
/**
 * SendReversalTransaction encapsulates the information needed to reverse a 
 *   MoneyGram Send from the POS. The message to the middleware uses the 
 *   SendReversalInfoMC object and contains a flag indicating a Reversal Type.
 *   A ReversalType is either a CANCEL or REFUND.  All reversals will be initially
 *   sent to the host as a CANCEL.  If successful, the transaction will be marked 
 *   CANCELED.  If the cancel fails because the Send transaction has run through
 *   settlement on the mainframe, then the POS will resubmit the SendReversalInfoMC
 *   message with a ReversalType of REFUND.  If this message is successful, then
 *   DeltaGram check(s) will print and a MoneyGram Receive Receipt will print.
 */
public class SendReversalTransaction extends MoneyGramReceiveTransaction {
	private static final long serialVersionUID = 1L;
	// possible return codes for setting amounts

    private String reversalReferenceNumber;
    private boolean refundFee = false;
    
    private boolean showScreenRVW15;
    
	private BigDecimal sendAmount;
	private BigDecimal feeAmount;
	private BigDecimal totalAmount;
	
	private boolean supplementalInformationNeeded;
	private boolean payoutInformationCollected = false;

	public SendReversalTransaction(String tranName, String tranReceiptPrefix, int docSeq) {
    	super (tranName, tranReceiptPrefix, docSeq); 
    }

    @Override
	public void clear() {
        // clear out all the transaction information
        super.clear();

        // set defaults
        reversalReferenceNumber = "";
        payoutInformationCollected = false;
    }

//    public void setTryRefund(){
//        tryRefund = true;
//    }
    
//    public boolean getTryRefund(){
//        return tryRefund;
//    }

    public void setRefundFee(boolean b){
        refundFee = b;
    }
    
    public boolean getRefundFee(){
        return refundFee;
    }

//    public void setCustomerCheckAmount(BigDecimal amount){
//        customerCheckAmount = amount;
//    }
    
//    public BigDecimal getCustomerCheckAmount(){
//        return customerCheckAmount;
//    }

    public String getReversalReferenceNumber() {
         return reversalReferenceNumber;
     }

    public void setReversalReferenceNumber(String reversalReferenceNumber) {
        this.reversalReferenceNumber = reversalReferenceNumber;
    }

    public void setupReversalISI() {
    	
    	String refFee = null;
 
    	String checkOneType="";
    	String checkTwoType="";
    	
    	if (modf1 != null  && getProfileMoneyGramChecks().equalsIgnoreCase("DG")){
    		checkOneType=modf1.getDeltagramFlag();
    	}
    	
    	if (modf2 != null && getProfileMoneyGramChecks().equalsIgnoreCase("DG")) {
    		checkTwoType=modf2.getDeltagramFlag();
    	}

		if (getRefundFee()) {
			refFee = "Y";
		} else {
			refFee = "N";
		}
    	
        String[] headerItems = {
                reversalReferenceNumber,
                this.getSendAmountInfo().getSendAmount().toString(),
                this.getSendAmountInfo().getTotalSendFees().toString(),
                refFee,
                checkOneType,
                checkTwoType
        	};

        List<String> fields = new ArrayList<String>();
        IsiReporting isi = new IsiReporting();
    	fields.add("RFN");
        getIsiHeader(fields);

        for (int i = 0; i < headerItems.length; i++) {
            fields.add(headerItems[i]);
        }

        isi.print(fields, isi.ISI_LOG);
        if (MainPanelDelegate.getIsiExeCommand()) {
            isi.print(fields, isi.EXE_LOG);
        }
    }
    
    public void setupSameDayCancelISI() {
    	
        String[] headerItems = {
                reversalReferenceNumber,
                this.getSendAmountInfo().getSendAmount().toString(),
                this.getSendAmountInfo().getTotalSendFees().toString(),
                "Y"
        	};

        List<String> fields = new ArrayList<String>();
        IsiReporting isi = new IsiReporting();
    	fields.add("CAN");
        getIsiHeader(fields);

        for (int i = 0; i < headerItems.length; i++) {
            fields.add(headerItems[i]);
        }

        isi.print(fields, isi.ISI_LOG);
        if (MainPanelDelegate.getIsiExeCommand()) {
            isi.print(fields, isi.EXE_LOG);
        }
    }

    @Override
	public boolean isFinancialTransaction() {
        return true;
    }

    @Override
	public boolean isCheckinRequired() {
        return !(DeltaworksStartup.getInstance().haveConnected());
    }

//    public void setReasonCode(String code){
//        this.reasonCode = code;
//    }

//    public String getReasonCode(){
//        return this.reasonCode;
//    }

	public void sendReversalValidation(final FlowPage callingPage, final ValidationType validationType) {

		final MessageFacadeParam parameters = new MessageFacadeParam(
				MessageFacadeParam.REAL_TIME,
				MessageFacadeParam.USER_CANCEL,
				MessageFacadeParam.MG_FEE_RATE_CHANGED
			);

		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {

				setStatus(ClientTransaction.IN_PROCESS);
				getDataCollectionData().setValidationStatus(DataCollectionStatus.ERROR);

				// Operator Name

				String opName = null;
				if (isPasswordRequired()) {
					opName = userProfile.find("NAME").stringValue();
					if ((opName != null) && (opName.length() > 7)) {
						opName = userProfile.find("NAME").stringValue().substring(0, 7);
					}
					getDataCollectionData().setFieldValue(FieldKey.OPERATORNAME_KEY, opName);
				}

				if (validationType.equals(ValidationType.SECONDARY)) {
					setPayout(getPayoutMethod());
				}

				// Get the receipt languages - static primary language & dynamic secondary language selection

	        	getDataCollectionData().setFieldValue(FieldKey.MF_VALID_LANG_RCPT_PRIME_KEY, DWValues.sConvertLang3ToLang5(getPrimaryReceiptSelectedLanguage()));
	        	getDataCollectionData().setFieldValue(FieldKey.MF_VALID_LANG_RCPT_SECOND_KEY, DWValues.sConvertLang3ToLang5(getSecondaryReceiptSelectedLanguage()));

				SendReversalValidationResponse response = null;

				try {

					// Make the AC call to validate the send transaction.

					boolean refundFee = getRefundFee();
					response = MessageFacade.getInstance().sendReversalValidation(parameters, getMgiTransactionSessionID(), getSendAmountInfo().getSendAmount(), 
							getSendAmountInfo().getSendCurrency(), refundFee, opName, getDataCollectionData(), validationType);
					
					// Check if a valid response was returned from the receiveValidation call.
					
					if (response != null) {
						SendReversalValidationResponse.Payload payload = response.getPayload().getValue();
						if (validationType.equals(ValidationType.INITIAL_NON_FORM_FREE)) {
							getDataCollectionData().clear();
						}
						
						List<InfoBase> fieldsToCollect = payload.getFieldsToCollect() != null ? payload.getFieldsToCollect().getFieldToCollect() : null;
						List<BusinessError> errors = response.getErrors() != null ? response.getErrors().getError() : null;
						processValidationResponse(validationType, fieldsToCollect, errors, payload.isReadyForCommit(), DataCollectionSet.SEND_REVERSAL_EXCLUDE);

//		                if ((payload.isReadyForCommit()) && ((! initialCallFlag) || (getDataCollectionData().getFieldMap().size() == 0))) {
//		                	getDataCollectionData().setValidationStatus(DataCollectionSet.VALIDATED);
//		                } else {
//		                	getDataCollectionData().setValidationStatus(DataCollectionSet.NEED_MORE_DATA);
//		                }
		                
		                BigDecimal refundTotal = payload.getTotalReversalAmount(); 
		                BigDecimal face = payload.getSendAmount();
		                BigDecimal fee = payload.getTotalSendFees();
		                
		                SendReversalTransaction.this.setSendAmount(face);
		                SendReversalTransaction.this.setFeeAmount(fee);
		                SendReversalTransaction.this.setTotalAmount(refundTotal);
					}
					return Boolean.TRUE;
				} catch (MessageFacadeError mfe) {
					if (mfe.hasErrorCode(MessageFacadeParam.USER_CANCEL)) {
						setStatus(ClientTransaction.FAILED);
					} else {
						setStatus(ClientTransaction.CANCELED);
					}
					return Boolean.FALSE;
				}
			}
		};
		MessageFacade.run(logic, callingPage, SEND_REVERSAL_VALIDATION, Boolean.FALSE);
	}
	
	@Override
	public void setScreensToShow() {
		this.setExcludePanelNames(new HashSet<String>(DataCollectionSet.SEND_REVERSAL_EXCLUDE));
		List<DataCollectionPanel> supplementalDataCollectionPanels = getDataCollectionData().getSupplementalDataCollectionPanels(this.getExcludePanelNames());
		setSupplementalDataCollectionPanels(supplementalDataCollectionPanels);
		setSupplementalInfoNeeded(supplementalDataCollectionPanels.size() > 0);
		showScreenRVW15 = supplementalDataCollectionPanels.size() > 0;
		ExtraDebug.println("Receiver Supplemental Data To Collect = " + isSupplementalInfoNeeded());
		
		Debug.println("showScreenRVW15 = " + showScreenRVW15);
	}

	private boolean isSupplementalInfoNeeded() {
		return supplementalInformationNeeded;
	}

	private void setSupplementalInfoNeeded(boolean b) {
		this.supplementalInformationNeeded = b;
	}

	public boolean isShowScreenRVW15() {
		return showScreenRVW15;
	}

	public BigDecimal getSendAmount() {
		return sendAmount;
	}

	public void setSendAmount(BigDecimal sendAmount) {
		this.sendAmount = sendAmount;
	}

	public BigDecimal getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(BigDecimal feeAmount) {
		this.feeAmount = feeAmount;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	@Override
	protected void logISIEntry() {
		if (getSendReversalType() == SendReversalType.R) {
			setupReversalISI();
		} else {
			setupSameDayCancelISI();
		}
	}

	@Override
	public void validation(FlowPage callingPage) {
		this.sendReversalValidation(callingPage, ValidationType.SECONDARY);
	}
	
	@Override
	public String nextDataCollectionScreen(String currentPage) {
		
		String nextPage = "";
		
		if (currentPage.equals(SendReversalTransactionWizard.RVW10_RECEIVE_DETAIL)) {
			if (this.showScreenRVW15) {
				nextPage = SendReversalTransactionWizard.RVW15_RECEIVE_REVERSAL_DATA;
			}
			
		} else if (currentPage.equals(SendReversalTransactionWizard.RVW15_RECEIVE_REVERSAL_DATA)) {
			if (this.dataCollectionData.getValidationStatus().equals(DataCollectionStatus.NEED_MORE_DATA)) {
				nextPage = SendReversalTransactionWizard.RVW20_RECEIVE_REVERSAL_VALIDATION;
			}
			if (dataCollectionData.getBusinessErrors().size() > 0) {
				nextPage = SendReversalTransactionWizard.RVW20_RECEIVE_REVERSAL_VALIDATION;
			}
		}

		return nextPage;
	}
	
	@Override
	public void applyTransactionLookupData() {
	}
	
	@Override
	public void logTransaction() {
		if (isCancelAllowed()) {
			String firstName = this.getDataCollectionData().getCurrentValue(FieldKey.SENDER_FIRSTNAME_KEY.getInfoKey());
			String lastName = this.getDataCollectionData().getCurrentValue(FieldKey.SENDER_LASTNAME_KEY.getInfoKey());
			String timeSent = this.getDataCollectionData().getCurrentValue(FieldKey.DATETIMESENT_KEY.getInfoKey());
			String operatorName = getDataCollectionData().getFieldStringValue(FieldKey.OPERATORNAME_KEY);
			Date date = null;
			try {
				XMLGregorianCalendar d = DatatypeFactory.newInstance().newXMLGregorianCalendar(timeSent.trim());
				date = TimeUtility.toDate(d);
			} catch (Exception e) {
				Debug.printException(e);
			}
	        ReportLog.logMoneyGramReversal(date, this.getTransactionLookupResponsePayload().getSendAmounts(), firstName, lastName, operatorName, getReversalReferenceNumber(), userID);
		} else {
			super.logTransaction();
        }
	}
	
    public boolean isPayoutInformationCollected() {
		return payoutInformationCollected;
	}

	public void setPayoutInformationCollected(boolean payoutInformationCollected) {
		this.payoutInformationCollected = payoutInformationCollected;
	}
}
