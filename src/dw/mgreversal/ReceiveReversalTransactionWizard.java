package dw.mgreversal;

import java.awt.event.KeyEvent;

import dw.framework.AdditionalInformationWizardPage;
import dw.framework.ClientTransaction;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.PageExitListener;
import dw.framework.PageNameInterface;
import dw.framework.SupplementalInformationWizardPage;
import dw.utility.Debug;

public class ReceiveReversalTransactionWizard extends 
		ReceiveReversalTransactionInterface {
	private static final long serialVersionUID = 1L;
	private static final String RVW05_RECEIVE_SEARCH = "receiveSearch";
	public static final String RVW10_RECEIVE_DETAIL = "receiveDetail";	
	public static final String RVW15_RECEIVE_REVERSAL_DATA = "receiveReversalDataCollection";
	public static final String RVW20_RECEIVE_REVERSAL_VALIDATION = "receiveReversalValidation";	
	
    public ReceiveReversalTransactionWizard(ReceiveReversalTransaction transaction, PageNameInterface naming) {
        super("moneygramreceive/MGRWizardPanel.xml", transaction, naming, false);	
        createPages();
    }

    @Override
	public void createPages() {
    	
        addPage(RRTWizardPage1.class, transaction, RVW05_RECEIVE_SEARCH, "RRW05", buttons);	 
        addPage(RRTWizardPage2.class, transaction, RVW10_RECEIVE_DETAIL, "RRW10", buttons);	 
        addPage(SupplementalInformationWizardPage.class, transaction, RVW15_RECEIVE_REVERSAL_DATA, "RVW15", buttons);	 
        addPage(AdditionalInformationWizardPage.class, transaction, RVW20_RECEIVE_REVERSAL_VALIDATION, "RVW20", buttons);	 
        //addPage(MGRWizardPage14.class, transaction, MRW90_COMMUNICATION_ERROR, "MRW90", buttons);     
        
    }

    /**
     * Add the single keystroke mappings to the buttons. Use the function keys
     *   for back, next, cancel, etc.
     */
    private void addKeyMappings() {
        buttons.addKeyMapping("back", KeyEvent.VK_F11, 0);	
        buttons.addKeyMapping("next", KeyEvent.VK_F12, 0);	
        buttons.addKeyMapping("cancel", KeyEvent.VK_ESCAPE, 0);	
    }

    @Override
	public void start() {
        transaction.clear();
        super.start();
        addKeyMappings();
    }

    /**
     * Page traversal logic here.
     */
    @Override
	public void exit(int direction) {
        String currentPage = cardLayout.getVisiblePageName();
        String nextPage = null;

//    	boolean readyForCommit = (transaction.getValidationStatus() == DataCollectionSet.VALIDATED);
//    	SendReversalType reversalType = transaction.getSendReversalType();
    	
    	Debug.println("ReceiveReversalTransactionWizard: direction, currentPage, status = " + direction + ", " + currentPage + ", " + transaction.getValidationStatus());
        
    	if (direction == PageExitListener.NEXT) {
            if (currentPage.equals(RVW05_RECEIVE_SEARCH)) {
            	nextPage = RVW10_RECEIVE_DETAIL;
            
            } else if (currentPage.equals(RVW10_RECEIVE_DETAIL)) {
            	if (((transaction.getValidationStatus().equals(DataCollectionStatus.NEED_MORE_DATA)) || (transaction.getValidationStatus().equals(DataCollectionStatus.NOT_VALIDATED))) && (transaction.isShowScreenRVW15())) {
            		nextPage = RVW15_RECEIVE_REVERSAL_DATA;
        		}else if (transaction.getStatus() == ClientTransaction.CANCELED){
                    direction = PageExitListener.COMPLETED;
                    nextPage = null;
                } else {
                	 direction = PageExitListener.CANCELED;
                     nextPage = null;
                }
            	
            } else if (currentPage.equals(RVW15_RECEIVE_REVERSAL_DATA)) {
            	if ((transaction.getValidationStatus().equals(DataCollectionStatus.NEED_MORE_DATA)) || (transaction.getValidationStatus().equals(DataCollectionStatus.NOT_VALIDATED))) {
            		nextPage = RVW20_RECEIVE_REVERSAL_VALIDATION;
            	} else if (transaction.getValidationStatus().equals(DataCollectionStatus.COMPLETED)) {
        			nextPage = RVW10_RECEIVE_DETAIL;
        		}
            
            } else if (currentPage.equals(RVW20_RECEIVE_REVERSAL_VALIDATION)) {
        		if ((transaction.getValidationStatus().equals(DataCollectionStatus.NEED_MORE_DATA)) || (transaction.getValidationStatus().equals(DataCollectionStatus.NOT_VALIDATED))) {
        			nextPage = RVW20_RECEIVE_REVERSAL_VALIDATION;
        		} else if (transaction.getValidationStatus().equals(DataCollectionStatus.COMPLETED)) {
        				nextPage = RVW10_RECEIVE_DETAIL;
        		}
            
            }
        
    	} else if (direction == PageExitListener.BACK ) {
        	if (currentPage.equals(RVW10_RECEIVE_DETAIL)) {   
        		nextPage = RVW05_RECEIVE_SEARCH;
            } else if (currentPage.equals(RVW15_RECEIVE_REVERSAL_DATA)) {
        		nextPage = RVW10_RECEIVE_DETAIL;
            } 
        
        } 
        
        if (nextPage != null) {
            showPage(nextPage, direction);
        } else {
            super.exit(direction);
        }
    }
}
