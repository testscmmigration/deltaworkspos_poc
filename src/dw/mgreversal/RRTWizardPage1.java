package dw.mgreversal;

import javax.swing.JPanel;
import javax.swing.JTextField;

import com.moneygram.agentconnect.TransactionLookupResponse;
import com.moneygram.agentconnect.TransactionStatusType;

import dw.dialogs.Dialogs;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.utility.DWValues;

public class RRTWizardPage1 extends ReceiveReversalFlowPage implements
		DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	private JTextField referenceField;

    public RRTWizardPage1(ReceiveReversalTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "framework/ReferenceNumberWizardPage.xml", name,pageCode, buttons);	
        referenceField = (JTextField) getComponent("referenceNumber"); 	

        DWTextPane tp = (DWTextPane) getComponent("text");
        tp.addListener(this, this);
	}

    public void doSearch() {
       flowButtons.getButton("next").doClick();	
    }

    public void setBoxStates() {
        // set the state of the next button...only allowed if criteria entered
        flowButtons.setEnabled("next", true);	
    }

    @Override
	public void start(int direction) {

        flowButtons.reset();
        flowButtons.setEnabled("back", false);	
        flowButtons.setEnabled("expert", false);	
		flowButtons.setVisible("expert", false);	

        // install the listeners
        addTextListener("referenceField", this, "setBoxStates");	 
        addActionListener("referenceField", this, "doSearch");	 

        // set up the instructions corresponding to the screen we are on
        DWTextPane tp = (DWTextPane) getComponent("text");

		tp.setText(Messages.getString("RRTWizardPage1.screen1Text").trim()); 

        referenceField.requestFocus();
        referenceField.selectAll();
    }

	@Override
	public void dwTextPaneResized() {
		DWTextPane tp = (DWTextPane) getComponent("text");
		JPanel panel = (JPanel) getComponent("referenceNumberPanel");
		int width = panel.getPreferredSize().width;
		tp.setWidth(width);
		tp.setListenerEnabled(true);
	}

    @Override
	public void commComplete(int commTag, Object returnValue) {
        boolean b = ((Boolean) returnValue).booleanValue();
        if (b) {
        	TransactionLookupResponse response = transaction.getTransactionLookupResponse();
        	TransactionStatusType status = ((response != null) && (response.getPayload() != null) && (response.getPayload().getValue() != null)) ? response.getPayload().getValue().getTransactionStatus() : null;
        	if (status != null) {
                PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
                return;
        	}
            Dialogs.showError("dialogs/DialogInvalidTransactionStatus.xml");
        }

    	flowButtons.setButtonsEnabled(true);
        flowButtons.setEnabled("back", false);	
    }

	@Override
	public void finish(int direction) {
        // call setButtonsEnabled to avoid duplicated transactions.
        flowButtons.setButtonsEnabled(false);

        if (direction == PageExitListener.NEXT) {
        	//receiveReversalTransaction.clear();
            if (referenceField.getText().length() > 0) {
            	receiveReversalTransaction.setReversalReferenceNumber(referenceField.getText());
            	receiveReversalTransaction.getDataCollectionData().clear();
            	receiveReversalTransaction.getDataCollectionData().clearCurrentValues();            	
            	
                receiveReversalTransaction.transactionLookup(receiveReversalTransaction.getReversalReferenceNumber().trim(), null, this, DWValues.TRX_LOOKUP_RECEIVE_REV, true);                                
            }
            else {
                flowButtons.setButtonsEnabled(true);
                flowButtons.setEnabled("back", false);	
            }
        }
        else {
            PageNotification.notifyExitListeners(this, direction);
        }
 	}
}
