package dw.mgreversal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.xml.bind.JAXBElement;

import com.moneygram.agentconnect.CompleteSessionResponse;
import com.moneygram.agentconnect.KeyValuePairType;
import com.moneygram.agentconnect.SendAmountInfo;
import com.moneygram.agentconnect.TransactionLookupResponse;
import com.moneygram.agentconnect.TransactionStatusType;

import dw.dialogs.Dialogs;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.FieldKey;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.mgsend.MoneyGramClientTransaction;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;
import dw.model.adapters.ReceiveDetailInfo;
import dw.profile.UnitProfile;
import dw.utility.DWValues;
import dw.utility.StringUtility;

/**
 * Page 3 of the MoneyGram Receive Wizard. This page displays the details of the
 *   selected transaction. If the transaction is not 'Ok to receive', the user
 *   will not be allowed to continue. 
 */

public class SRTWizardPage2 extends SendReversalFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;
	
	private static final int MINIMUM_SCREEN_WIDTH = 425;
	
    private JLabel authCodeLabel;
    private DWTextPane line1Label;
    private JPanel authCodePanel;
    private JCheckBox refundFeeBox;
    private TransactionLookupResponse lookupResponse;
    private JLabel refundFeeLabel;
    private JButton reprintButton;
	private List<DWTextPane> textPanes;
	private int tagWidth;
    
	public SRTWizardPage2(SendReversalTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "moneygramsend/SRTWizardPage2.xml", name, pageCode, buttons);	
        init();
    }
	
	private void init() {
		textPanes = new ArrayList<DWTextPane>();
        authCodeLabel = (JLabel) getComponent("authCodeLabel");	
        authCodePanel = (JPanel) getComponent("authCodePanel");	
        line1Label = (DWTextPane) getComponent("line1Label");	
        refundFeeBox = (JCheckBox) getComponent("refundFeeBox"); 
        refundFeeLabel = (JLabel) getComponent("refundFeeLabel");
        reprintButton = (JButton) getComponent("reprintButton");
        transactionRefund.setRefundFee(false);
        
        line1Label.addListener(this, this);
	}

	@Override
	public void dwTextPaneResized() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				line1Label.setWidth(MINIMUM_SCREEN_WIDTH);

				int w = FIELD_TOTAL_WIDTH - tagWidth;
				for (DWTextPane pane : textPanes) {
					pane.setWidth(w);
				}
			}
		});
	}

	
	/**
	 * Check if map contain key and matching value
	 * @param map
	 * @param key
	 * @param elemVal
	 * @return true if map contain key and value. Otherwise, return false
	 */
	private boolean matchKeyAndValue(Map<String, JAXBElement<String>> map, String key, String elemVal) {
		boolean keyVal = false;
		JAXBElement<String> pairValue = map.get(key);
		if ((pairValue != null) && StringUtility.getNonNullString(pairValue.getValue()).equals(elemVal)) {
			keyVal = true;
		}

		return keyVal;
	}

	@Override
	public void start(int direction) {
        flowButtons.reset();
        flowButtons.setVisible("expert", false); 
		/*
		 * note: dw.framework.XmlDefinedPanel.addActionListener verifies against
		 * the defined Callbacks list of methods
		 */
        addActionListener("refundFeeBox", this, "setRefundFee");   
		addActionListener("reprintButton", this, "reprint");  

        TransactionLookupResponse response = transactionRefund.getTransactionLookupResponse();
        
        if (direction == PageExitListener.NEXT) {
            lookupResponse = transactionRefund.getTransactionLookupResponse();            
            populateScreen();
        } else if (direction == PageExitListener.BACK){
        } else {
            flowButtons.setSelected("next"); 
        }

        flowButtons.setEnabled("next", response.getPayload().getValue().getTransactionStatus().equals(TransactionStatusType.AVAIL)); 

		boolean allowRefund = (UnitProfile.getInstance().get("ALLOW_REFUND", "N")).equalsIgnoreCase("Y");
		boolean allowFeeRefund = (UnitProfile.getInstance().get("ALLOW_FEE_REFUND", "N").equalsIgnoreCase("Y"));
		boolean RefundRequired = false;
		boolean isCancelAllowedFlag = false;
		JAXBElement<TransactionLookupResponse.Payload> payload = lookupResponse.getPayload();
		List<KeyValuePairType> valueList = payload.getValue() != null && payload.getValue().getCurrentValues() != null
				? payload.getValue().getCurrentValues().getCurrentValue() : new ArrayList<KeyValuePairType>();
		if (valueList.size() > 0) {
			Map<String, JAXBElement<String>> keyMap = new HashMap<String, JAXBElement<String>>();
			for (KeyValuePairType kp : valueList) {
				keyMap.put(kp.getInfoKey(), kp.getValue());
			}
			allowRefund = matchKeyAndValue(keyMap, "isRefundAllowedFlag", "true");
			RefundRequired = matchKeyAndValue(keyMap, "feeMustBeRefundedFlag", "true");
			isCancelAllowedFlag = matchKeyAndValue(keyMap, "isCancelAllowedFlag", "true");

		}
		
		if (RefundRequired) {
			transactionRefund.setRefundFee(true);
			refundFeeBox.setEnabled(false);
		} else if (allowRefund && allowFeeRefund) {
			refundFeeBox.setEnabled(true);
		} else {
			refundFeeBox.setVisible(false);
			refundFeeLabel.setVisible(false);
		}
		
		if (allowRefund || isCancelAllowedFlag) {
			flowButtons.setEnabled("next", true);
		}
		
        refundFeeBox.setSelected(transactionRefund.getRefundFee());
        
        // If the send reversal transaction has already been validated:
        //	1. Disable the Back and Next buttons.
        //	2. Disable the refund fee checkbox.
        //	3. Note: the agent/customer receipts are initially printed from the completeSession transaction processing.
        
        if (transactionRefund.getDataCollectionData().getValidationStatus().equals(DataCollectionStatus.COMPLETED)) {
        	flowButtons.setEnabled("back", false);
        	flowButtons.setEnabled("next", false);
        	flowButtons.getButton("cancel").setText(Messages.getString("Dialogs.OK"));
        	
        	refundFeeBox.setEnabled(false);
        	refundFeeLabel.setEnabled(false);
        	
            if (transactionRefund.isReceiptReceived()) {
	        	reprintButton.setEnabled(true);
	        	reprintButton.setVisible(true);
            } else {
	        	reprintButton.setEnabled(false);
	        	reprintButton.setVisible(false);
            }
        	
        } else {
        	reprintButton.setEnabled(false);
        	reprintButton.setVisible(false);
        }
    }
    
    private void populateScreen(){
        ReceiveDetailInfo details = transactionRefund.getReceiveDetail();
        
    	List<JLabel> tags = new ArrayList<JLabel>();
        
        String fullName = DWValues.formatName(details.getSenderFirstName(), details.getSenderMiddleName(), details.getSenderLastName(), details.getSenderLastName2());
		this.displayPaneItem("senderName", fullName, tags, textPanes);
        
		String tag = transaction.getTransactionLookupInfoLabel(FieldKey.SENDER_PRIMARYPHONE_KEY.getInfoKey());
        this.displayLabelItem("senderHomePhone", tag, details.getSenderHomePhone(), tags);
        
		tag = transaction.getTransactionLookupInfoLabel("dateTimeSent");
		String s = details.getDateTimeSent().toString();
        this.displayLabelItem("sent", tag, FormatSymbols.formatDateTimeForLocaleWithTimeZone(s), tags);
        
        fullName = DWValues.formatName(details.getReceiverFirstName(), details.getReceiverMiddleName(), details.getReceiverLastName(), details.getReceiverLastName2());
		this.displayPaneItem("receiverName", fullName, tags, textPanes);

		String message1 = details.getMessageField1() != null ? details.getMessageField1() : "";
		String message2 =details.getMessageField2() != null ? details.getMessageField2() : "";
        String message = (message1 + " " + message2).trim(); 
		this.displayPaneItem("message", message, tags, textPanes);
        
        this.displayLabelItem("reference", transactionRefund.getReversalReferenceNumber(), tags);
        
        String status;
        if (transactionRefund.getDataCollectionData().getValidationStatus().equals(DataCollectionStatus.COMPLETED)) {
        	status = getTranactionStatus(details.getTransactionStatus(), transaction.getDataCollectionData().getCurrentValueMap(), false, false);
        } else {
        	status = getTranactionStatus(details.getTransactionStatus(), transaction.getDataCollectionData().getCurrentValueMap(), true, false);
        }
        this.displayPaneItem("status", status, false, tags, textPanes);
        
        CompleteSessionResponse response = transactionRefund.getCompleteSessionResponse();
        CompleteSessionResponse.Payload payload = response != null && response.getPayload() != null ? response.getPayload().getValue() : null;
        String authCode = payload != null ? payload.getAgentCheckAuthorizationNumber() : null;
        this.displayLabelItem("authCode", authCode, tags);
        
        SendAmountInfo sendAmountInfo = transactionRefund.getSendAmountInfo();
        String sendCurrency = sendAmountInfo.getSendCurrency();
        
        this.displayMoney("sendAmount", sendAmountInfo.getSendAmount(), sendCurrency, tags);
        this.displayMoney("feeAmount", sendAmountInfo.getTotalSendFees(), sendCurrency, tags);
        this.displayMoney("taxAmount", sendAmountInfo.getTotalSendTaxes(), sendCurrency, tags);
        this.displayMoney("totalAmount", sendAmountInfo.getTotalAmountToCollect(), sendCurrency, tags);
        
        if (transactionRefund.getValidationStatus().equals(DataCollectionStatus.VALIDATION_NOT_PERFORMED) && details.getTransactionStatus().equals(TransactionStatusType.AVAIL)) {
            line1Label.setText(Messages.getString("SRTWizardPage2.okLine1")); 
        } else if ((transactionRefund.getValidationStatus().equals(DataCollectionStatus.COMPLETED)) && (details.getTransactionStatus().equals(TransactionStatusType.CANCL) || details.getTransactionStatus().equals(TransactionStatusType.REFND))) {
            line1Label.setText(Messages.getString("SRTWizardPage2.completeLine1")); 
        } else {
            line1Label.setText(Messages.getString("SRTWizardPage2.badLine1")); 
        }
        
		tagWidth = this.setTagLabelWidths(tags, MINIMUM_TAG_WIDTH);

		dwTextPaneResized();
    }

	/**
	 * reprint() defined in dw.framework.Callbacks for reflection and also in
	 * proguard.cfg for prohibiting code obfuscation; whereas reprintReceipts()
	 * defined alone in proguard.cfg is not enough
	 */
	public void reprint() {
		printReceipts();
	}

    public void printReceipts() {   
    	if (transactionRefund.isReceiveAgentAndCustomerReceipts() || transactionRefund.isReceiveAgentReceipt()) {
    		transactionRefund.printReceipt(false, true);
    	}
    	if (transactionRefund.isReceiveAgentAndCustomerReceipts() || transactionRefund.isReceiveCustomerReceipt()) {
    		transactionRefund.printReceipt(true, true);
    	}
    }
    
    public void setRefundFee(){
        transactionRefund.setRefundFee(refundFeeBox.isSelected());
        transactionRefund.setPayoutInformationCollected(false);
    }

    @Override
	public void finish(int direction) {
        if (direction == PageExitListener.NEXT) {
        	if (transactionRefund.isCancelAllowed() || transactionRefund.isRefundAllowed()) {
                transactionRefund.setRefundFee(refundFeeBox.isSelected());
    	        if (transactionRefund.isOKToPrint()) {
                    transactionRefund.sendReversalValidation(this, ValidationType.INITIAL_NON_FORM_FREE);
    	        } else {
    				PageNotification.notifyExitListeners(this, PageExitListener.DONOTHING);
    				flowButtons.setButtonsEnabled(true);
    				flowButtons.setVisible("expert", false);
    	        }
        	} else {
        		Dialogs.showWarning("dialogs/DialogTransactionCannotBeReversed.xml");
				PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
        	}
		} else if (direction == PageExitListener.CANCELED
				&& flowButtons.getButton("cancel").getText().equals(Messages.getString("OK"))) {
			PageNotification.notifyExitListeners(this, PageExitListener.COMPLETED);
        } else {
        	PageNotification.notifyExitListeners(this, direction);
        }
    }

    @Override
	public void commComplete(int commTag, Object returnValue) {
        boolean b = ((Boolean) returnValue).booleanValue();
        switch (commTag) {
            case MoneyGramClientTransaction.TRANSACTION_LOOKUP:
                if (b) {
                    if (UnitProfile.getInstance().isDemoMode()){
                    	// In demo mode this could not be fixed int the demo server
                    	// because we don't have an input value for getLookupDetail that
                    	// we can manipulate to accomplish the two different status values
                    	// for the same input call.
                    	if (! transactionRefund.getReversalReferenceNumber().equals("87654321")) {
                    		lookupResponse.getPayload().getValue().setTransactionStatus(TransactionStatusType.CANCL);
                    	}
                    }

                    populateScreen();
                    /*
                     * transaction lookup after successful sendReversal - changes
                     * "cancel" to "OK", and screen display allows button selection to reprint the
                     * receipts
                     */
                    flowButtons.setAlternateText("cancel", Messages.getString("OK"));  
                    flowButtons.setEnabled("back", false); 
                    flowButtons.getButton("cancel").requestFocus(); 

                } else {
                    PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
                    return;
                }
                break;

            case MoneyGramClientTransaction.SEND_REVERSAL_VALIDATION:
                if (b) {
                    PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
                } else {
                    PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
                }
                return;

            default :
                return;
        }
    }
}
