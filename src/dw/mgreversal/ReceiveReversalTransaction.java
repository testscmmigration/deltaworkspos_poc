package dw.mgreversal;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import com.moneygram.agentconnect.BusinessError;
import com.moneygram.agentconnect.InfoBase;
import com.moneygram.agentconnect.MoneyOrderInfo;
import com.moneygram.agentconnect.ReceiveReversalValidationResponse;
import com.moneygram.agentconnect.TransactionLookupResponse;
import dw.framework.DataCollectionPanel;
import dw.comm.MessageFacade;
import dw.comm.MessageFacadeError;
import dw.comm.MessageFacadeParam;
import dw.comm.MessageLogic;
import dw.framework.ClientTransaction;
import dw.framework.DataCollectionSet;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.FieldKey;
import dw.framework.FlowPage;
import dw.io.IsiReporting;
import dw.io.info.MoneyGramReceiveDiagInfo;
import dw.io.report.AuditLog;
import dw.io.report.ReportLog;
import dw.main.DeltaworksStartup;
import dw.main.MainPanelDelegate;
import dw.mgreceive.MoneyGramReceiveTransaction;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.TimeUtility;

/**
 * ReceiveReversalTransaction encapsulates the information needed to reverse a
 * MoneyGram Receive.
 */
public class ReceiveReversalTransaction extends MoneyGramReceiveTransaction {
	private static final long serialVersionUID = 1L;
	
    private String reversalReferenceNumber;
    
    private boolean showScreenRVW15;   
    
    public ReceiveReversalTransaction(String tranName, String tranReceiptPrefix, int docSeq) {
    	super (tranName, tranReceiptPrefix, docSeq); 
		dispenserNeeded = false;
    }

    @Override
	public void clear() {
        // clear out all the transaction information
        super.clear();

        // set defaults
        transactionLookupResponse = new TransactionLookupResponse();
        reversalReferenceNumber = "";
    }

    public String getReversalReferenceNumber() {
         return reversalReferenceNumber;
     }

    public void setReversalReferenceNumber(String reversalReferenceNumber) {
        this.reversalReferenceNumber = reversalReferenceNumber;
    }

    @Override
	public boolean isFinancialTransaction() {
        return true;
    }

    @Override
	public boolean isCheckinRequired() {
        return !(DeltaworksStartup.getInstance().haveConnected());
    }
    
	@Override
    protected void logISIEntry() {
			setupRRTISI();
	}    

    public void setupRRTISI() {
        Object[] receiveItems = {
        	getReversalReferenceNumber(),
        	transactionLookupResponsePayload.getReceiveAmounts().getReceiveAmount()		
        };

        List<String> fields = new ArrayList<String>();
        IsiReporting isi = new IsiReporting();
        fields.add("RRT");
        getIsiHeader(fields);
        for(int i=0; i<receiveItems.length; i++){
            fields.add(nullToSpace(receiveItems[i]));
        }

        isi.print(fields, isi.ISI_LOG);
        if (MainPanelDelegate.getIsiExeCommand())
            isi.print(fields, isi.EXE_LOG);
    }
    
	public void receiveReversalValidation(final FlowPage callingPage, final ValidationType validationType) {

		final MessageFacadeParam parameters = new MessageFacadeParam(
				MessageFacadeParam.REAL_TIME,
				MessageFacadeParam.USER_CANCEL,
				MessageFacadeParam.MG_FEE_RATE_CHANGED
			);

		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {

				setStatus(ClientTransaction.IN_PROCESS);
				getDataCollectionData().setValidationStatus(DataCollectionStatus.ERROR);

				// Operator Name

				String opName = null;
				if (isPasswordRequired()) {
					opName = userProfile.find("NAME").stringValue();
					if ((opName != null) && (opName.length() > 7)) {
						opName = userProfile.find("NAME").stringValue().substring(0, 7);
					}
					getDataCollectionData().setFieldValue(FieldKey.OPERATORNAME_KEY, opName);
				}

				if (validationType.equals(ValidationType.SECONDARY)) {
					setPayout(getPayoutMethod());
				}

				// Get the receipt languages - static primary language & dynamic secondary language selection

	        	getDataCollectionData().setFieldValue(FieldKey.MF_VALID_LANG_RCPT_PRIME_KEY, DWValues.sConvertLang3ToLang5(getPrimaryReceiptSelectedLanguage()));
	        	getDataCollectionData().setFieldValue(FieldKey.MF_VALID_LANG_RCPT_SECOND_KEY, DWValues.sConvertLang3ToLang5(getSecondaryReceiptSelectedLanguage()));

	        	ReceiveReversalValidationResponse response = null;

				try {

					// Make the AC call to validate the receive transaction
					
					BigDecimal receiveAmount = transactionLookupResponsePayload.getReceiveAmounts().getReceiveAmount();
					String receiveCurrency = transactionLookupResponsePayload.getReceiveAmounts().getReceiveCurrency();
					response = MessageFacade.getInstance().receiveReversalValidation(parameters, getMgiTransactionSessionID(), receiveAmount, receiveCurrency, opName, getDataCollectionData(), validationType);
					
					// Check if a valid response was returned from the receiveValidation call.
					
					if (response != null) {
						ReceiveReversalValidationResponse.Payload payload = response.getPayload().getValue();
						if (validationType.equals(ValidationType.INITIAL_NON_FORM_FREE)) {
							getDataCollectionData().clear();
						}
						
						List<InfoBase> fieldsToCollect = payload.getFieldsToCollect() != null ? payload.getFieldsToCollect().getFieldToCollect() : null;
						List<BusinessError> errors = response.getErrors() != null ? response.getErrors().getError() : null;
						processValidationResponse(validationType, fieldsToCollect, errors, payload.isReadyForCommit(), DataCollectionSet.SEND_REVERSAL_EXCLUDE);
		                
//		                BigDecimal refundTotal = payload.getTotalReversalAmount(); 
//		                BigDecimal face = payload.getSendAmount();
//		                BigDecimal fee = payload.getTotalSendFees();
		                
//		                ReceiveReversalTransaction.this.setSendAmount(face);
//		                ReceiveReversalTransaction.this.setFeeAmount(fee);
//		                ReceiveReversalTransaction.this.setTotalAmount(refundTotal);
					}
					return Boolean.TRUE;
				} catch (MessageFacadeError mfe) {
					if (mfe.hasErrorCode(MessageFacadeParam.USER_CANCEL)) {
						setStatus(ClientTransaction.FAILED);
					} else {
						setStatus(ClientTransaction.CANCELED);
					}
					return Boolean.FALSE;
				}
			}
		};
		MessageFacade.run(logic, callingPage, RECEIVE_REVERSAL_VALIDATION, Boolean.FALSE);
	}
	
	@Override
	public void setScreensToShow() {
		this.setExcludePanelNames(new HashSet<String>(DataCollectionSet.SEND_REVERSAL_EXCLUDE));
		List<DataCollectionPanel> supplementalDataCollectionPanels = getDataCollectionData().getSupplementalDataCollectionPanels(this.getExcludePanelNames());
		setSupplementalDataCollectionPanels(supplementalDataCollectionPanels);
		setShowScreenRVW15( supplementalDataCollectionPanels.size() > 0 );

		Debug.println("showScreenRVW15 = " + isShowScreenRVW15());
	}	
	
	@Override
	public void validation(FlowPage callingPage) {
		this.receiveReversalValidation(callingPage, ValidationType.SECONDARY);
	}
	
	@Override
	public String nextDataCollectionScreen(String currentPage) {
		
		String nextPage = "";
		
		if (currentPage.equals(ReceiveReversalTransactionWizard.RVW10_RECEIVE_DETAIL)) {
			if (this.isShowScreenRVW15()) {
				nextPage = ReceiveReversalTransactionWizard.RVW15_RECEIVE_REVERSAL_DATA;
			}
			
		} else if (currentPage.equals(ReceiveReversalTransactionWizard.RVW15_RECEIVE_REVERSAL_DATA)) {
			if (this.dataCollectionData.getValidationStatus().equals(DataCollectionStatus.NEED_MORE_DATA)) {
				nextPage = ReceiveReversalTransactionWizard.RVW20_RECEIVE_REVERSAL_VALIDATION;
			}
			if (dataCollectionData.getBusinessErrors().size() > 0) {
				nextPage = ReceiveReversalTransactionWizard.RVW20_RECEIVE_REVERSAL_VALIDATION;
			}
		}

		return nextPage;
	}	
    
	public boolean isShowScreenRVW15() {
		return showScreenRVW15;
	}
	
	public void setShowScreenRVW15(boolean bool) {
		showScreenRVW15 = bool;
	}
	
	@Override
	public void applyTransactionLookupData() {
	}
	
	@Override
	public void logTransaction() {
		BigDecimal receiveAmount = this.transactionLookupResponsePayload.getReceiveAmounts().getReceiveAmount();
        AuditLog.writeMgrReverseRecord(userID, this.getReversalReferenceNumber(), receiveAmount.toString());

        Date date = new Date(TimeUtility.currentTimeMillis());
        MoneyGramReceiveDiagInfo mgrdi = new MoneyGramReceiveDiagInfo(date, userID, userName, 1, tranSuccessful);
        
        MoneyOrderInfo mo1 = getMoneyOrderInfo(0);
        mo1.setSerialNumber(BigInteger.ZERO);
        mo1.setDateTimePrinted(TimeUtility.toXmlCalendar(date));
        mo1.setDiscountAmount(BigDecimal.ZERO); 
        mo1.setDiscountPercentage(BigDecimal.ZERO);
        mo1.setDocumentSequenceNbr(BigInteger.ZERO); 
        mo1.setDocumentType(BigInteger.valueOf(ReportLog.MONEYGRAM_RECEIVE_TYPE));
//        mo1.setEmployeeID(sOpName);
        mgrdi.setAmount(receiveAmount.negate());
        mgrdi.setReferenceNumber(this.getReversalReferenceNumber());
        if (modf1 == null) {
            modf1 = mot.getMoneyOrderDisplayFields(0);
        }
        String payeeName = this.getDataCollectionData().getCurrentValue(FieldKey.RECEIVER_LASTNAME_KEY.getInfoKey());
        ReportLog.logMoneyGramReceive(mgrdi, mo1, payeeName, this.transactionLookupResponsePayload.getReceiveAmounts().getReceiveCurrency()); 
        setStatus(ClientTransaction.COMPLETED);
	}
}
