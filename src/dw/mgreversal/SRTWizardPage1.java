package dw.mgreversal;

import javax.swing.JPanel;
import javax.swing.JTextField;

import com.moneygram.agentconnect.TransactionLookupResponse;
import com.moneygram.agentconnect.TransactionStatusType;

import dw.dialogs.Dialogs;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.utility.DWValues;

/**
 * Page 1 of the MoneyGram Send Reversal Wizard. This page prompts the user to enter
 *   some information to search for the desired receive transaction.
 */
//public class SRTWizardPage1 extends ReversalTransactionFlowPage {
public class SRTWizardPage1 extends SendReversalFlowPage implements DWTextPaneListener {
	private static final long serialVersionUID = 1L;

	private JTextField referenceField;
    private JPanel referencePanel;

    public SRTWizardPage1(SendReversalTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "framework/ReferenceNumberWizardPage.xml", name,pageCode, buttons);	
        referenceField = (JTextField) getComponent("referenceNumber"); 	
        referencePanel = (JPanel) getComponent("referenceNumberPanel");	
        
        DWTextPane line1Label = (DWTextPane) getComponent("text");	
        line1Label.addListener(this, this);
	}

	@Override
	public void dwTextPaneResized() {
        DWTextPane line1Label = (DWTextPane) getComponent("text");	
		int l = referencePanel.getPreferredSize().width;
		line1Label.setWidth(l);
		line1Label.setListenerEnabled(false);
	}

    public void doSearch() {
       flowButtons.getButton("next").doClick();	
    }

    public void setBoxStates() {
        // set the state of the next button...only allowed if criteria entered
        flowButtons.setEnabled("next", true);	
    }

    @Override
	public void start(int direction) {

        flowButtons.reset();
        flowButtons.setEnabled("back", false);	
        flowButtons.setEnabled("expert", false);	
		flowButtons.setVisible("expert", false);	

        // install the listeners
        addTextListener("referenceField", this, "setBoxStates");	 
        addActionListener("referenceField", this, "doSearch");	 

        // set up the instructions corresponding to the screen we are on
        DWTextPane l1 = (DWTextPane) getComponent("text");	

        String text = Messages.getString("SRTWizardPage1.screen1Text");
        l1.setText(text); 
        referenceField.requestFocus();
        referenceField.selectAll();
    }

    @Override
	public void commComplete(int commTag, Object returnValue) {
        boolean b = ((Boolean) returnValue).booleanValue();
        if (b) {
        	TransactionLookupResponse response = transactionRefund.getTransactionLookupResponse();
        	TransactionStatusType status = ((response != null) && (response.getPayload() != null) && (response.getPayload().getValue() != null)) ? response.getPayload().getValue().getTransactionStatus() : null;
        	if (status != null) {
                PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
                return;
        	}
            Dialogs.showError("dialogs/DialogInvalidTransactionStatus.xml");
        }

    	flowButtons.setButtonsEnabled(true);
        flowButtons.setEnabled("back", false);	
    }

    @Override
	public void finish(int direction) {
        // call setButtonsEnabled to avoid duplicated transactions.
        flowButtons.setButtonsEnabled(false);
        
        if (direction == PageExitListener.NEXT) {
        	transactionRefund.clear();
            if (! referenceField.getText().isEmpty()) { 
                transactionRefund.setReversalReferenceNumber(referenceField.getText().trim());
                transactionRefund.getDataCollectionData().clear();
                transactionRefund.getDataCollectionData().clearCurrentValues();
                transactionRefund.transactionLookup(referenceField.getText().trim(), null, this, DWValues.TRX_LOOKUP_SENDREV, true);
            } else {
                flowButtons.setButtonsEnabled(true);
                flowButtons.setEnabled("back", false);	
            }
        } else {
            PageNotification.notifyExitListeners(this, direction);
        }
    }
}
