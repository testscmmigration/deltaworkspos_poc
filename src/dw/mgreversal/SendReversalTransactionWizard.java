package dw.mgreversal;

import java.awt.event.KeyEvent;

import dw.framework.AdditionalInformationWizardPage;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.PageExitListener;
import dw.framework.PageNameInterface;
import dw.framework.SupplementalInformationWizardPage;
import dw.mgreceive.MGRWizardPage14;
import dw.mgreceive.MGRWizardPage7;
import dw.mgreceive.MGRWizardPage7b;
import dw.mgreceive.MGRWizardPage8;
import dw.mgreceive.MGRWizardPage9;
import dw.mgreceive.MoneyGramReceiveTransaction;
import dw.model.adapters.SendReversalType;
import dw.utility.Debug;

public class SendReversalTransactionWizard extends SendReversalTransactionInterface {
	private static final long serialVersionUID = 1L;
   
	private static final String RVW05_RECEIVE_SEARCH = "sendSearch";
	public static final String RVW10_RECEIVE_DETAIL = "sendDetail";
	public static final String RVW15_RECEIVE_REVERSAL_DATA = "sendReversalDataCollection";
	public static final String RVW20_RECEIVE_REVERSAL_VALIDATION = "sendReversalValidation";
	private static final String MRW55_PAYOUT = "payout";
	private static final String MRW57_PAYOUT_CHECK= "payoutCheck";
	private static final String MRW60_REVIEW = "review";
	private static final String MRW65_PRINTING = "printing";
	private static final String MRW90_COMMUNICATION_ERROR = "communicationError";
	
    public SendReversalTransactionWizard(SendReversalTransaction transaction, MoneyGramReceiveTransaction mgrTransaction, PageNameInterface naming) {
        super("moneygramreceive/MGRWizardPanel.xml", transaction, mgrTransaction, naming, false);	
        createPages();
    }

    @Override
	public void createPages() {
        addPage(SRTWizardPage1.class, transaction, RVW05_RECEIVE_SEARCH, "RVW05", buttons);	 
        addPage(SRTWizardPage2.class, transaction, RVW10_RECEIVE_DETAIL, "RVW10", buttons);	 
        addPage(SupplementalInformationWizardPage.class, transaction, RVW15_RECEIVE_REVERSAL_DATA, "RVW15", buttons);	 
        addPage(AdditionalInformationWizardPage.class, transaction, RVW20_RECEIVE_REVERSAL_VALIDATION, "RVW20", buttons);	 
        addPage(MGRWizardPage7.class, transaction, MRW55_PAYOUT, "MRW55", buttons);     
        addPage(MGRWizardPage7b.class, transaction, MRW57_PAYOUT_CHECK, "MRW57", buttons);     
        addPage(MGRWizardPage8.class, transaction, MRW60_REVIEW, "MRW60", buttons);  
        addPage(MGRWizardPage9.class, transaction, MRW65_PRINTING, "MRW65", buttons);    
        addPage(MGRWizardPage14.class, transaction, MRW90_COMMUNICATION_ERROR, "MRW90", buttons);     
    }

    /**
     * Add the single keystroke mappings to the buttons. Use the function keys
     *   for back, next, cancel, etc.
     */
    private void addKeyMappings() {
        buttons.addKeyMapping("back", KeyEvent.VK_F11, 0);	
        buttons.addKeyMapping("next", KeyEvent.VK_F12, 0);	
        buttons.addKeyMapping("cancel", KeyEvent.VK_ESCAPE, 0);	
    }

    @Override
	public void start() {
        transaction.clear();
//        mgrTransaction.clear();
        super.start();
        addKeyMappings();
    }

    /**
     * Page traversal logic here.
     */
    @Override
	public void exit(int direction) {
        String currentPage = cardLayout.getVisiblePageName();
        String nextPage = null;

    	boolean readyForCommit = (transaction.getValidationStatus() == DataCollectionStatus.VALIDATED);
    	SendReversalType reversalType = transaction.getSendReversalType();
    	boolean payoutRequired = reversalType != null ? reversalType.equals(SendReversalType.R) : false;
    	int payoutType = transaction.getPayoutMethod();
    	
    	Debug.println("SendReversalTransactionWizard: direction, currentPage, status, payoutRequired = " + direction + ", " + currentPage + ", " + transaction.getValidationStatus() + ", " + payoutRequired);
        
    	if (direction == PageExitListener.NEXT) {
            if (currentPage.equals(RVW05_RECEIVE_SEARCH)) {
            	transaction.setPayoutInformationCollected(false);
            	nextPage = RVW10_RECEIVE_DETAIL;
            
            } else if (currentPage.equals(RVW10_RECEIVE_DETAIL)) {
            	if (transaction.isShowScreenRVW15()) {
            		nextPage = RVW15_RECEIVE_REVERSAL_DATA;
        		} else if (transaction.getValidationStatus().equals(DataCollectionStatus.ERROR)) {
        			nextPage = MRW90_COMMUNICATION_ERROR;
            	} else if (readyForCommit) {
            		nextPage = MRW60_REVIEW;
            	} else if (payoutRequired) {
            		if (transaction.getProfileMoneyGramChecks().equals("DG")) {
            			nextPage = MRW55_PAYOUT; 
            		} else {
            			nextPage = MRW57_PAYOUT_CHECK; 
            		}
            	}
            
            } else if (currentPage.equals(RVW15_RECEIVE_REVERSAL_DATA)) {
            	if (payoutRequired) {
	            	if ((! transaction.isPayoutInformationCollected()) && (payoutType != MoneyGramReceiveTransaction.PAYOUT_CASH)) {
	    				if (transaction.getProfileMoneyGramChecks().equals("DG")) {
	    					nextPage = MRW55_PAYOUT; 
	    				} else {
	    					nextPage = MRW57_PAYOUT_CHECK;
	    				}
	            	} else {
	            		nextPage = MRW60_REVIEW;
	            	}
            	} else if ((transaction.getValidationStatus().equals(DataCollectionStatus.NEED_MORE_DATA)) || (transaction.getValidationStatus().equals(DataCollectionStatus.NOT_VALIDATED))) {
            		nextPage = RVW20_RECEIVE_REVERSAL_VALIDATION;
            	} else if (transaction.getValidationStatus().equals(DataCollectionStatus.COMPLETED)) {
        			nextPage = RVW10_RECEIVE_DETAIL;
        		} else {
        			nextPage = MRW90_COMMUNICATION_ERROR;
                }
            
            } else if (currentPage.equals(RVW20_RECEIVE_REVERSAL_VALIDATION)) {
        		if ((transaction.getValidationStatus().equals(DataCollectionStatus.NEED_MORE_DATA)) || (transaction.getValidationStatus().equals(DataCollectionStatus.NOT_VALIDATED))) {
        			nextPage = RVW20_RECEIVE_REVERSAL_VALIDATION;
        		} else if (transaction.getValidationStatus().equals(DataCollectionStatus.ERROR)) {
        			nextPage = MRW90_COMMUNICATION_ERROR;
        		} else if (payoutRequired) {
        			if (readyForCommit) {
        				nextPage = MRW60_REVIEW;
        			} else {
            			nextPage = MRW90_COMMUNICATION_ERROR;
        			}
        		} else {
        			if (transaction.getValidationStatus().equals(DataCollectionStatus.COMPLETED)) {
        				nextPage = RVW10_RECEIVE_DETAIL;
        			} else {
            			nextPage = MRW90_COMMUNICATION_ERROR;
        			}
        		}
            
            } else if (currentPage.equals(MRW55_PAYOUT) || currentPage.equals(MRW57_PAYOUT_CHECK)) {   
            	transaction.setPayoutInformationCollected(true);
        		if ((transaction.getValidationStatus().equals(DataCollectionStatus.NEED_MORE_DATA)) || (transaction.getValidationStatus().equals(DataCollectionStatus.NOT_VALIDATED))) {
        			nextPage = RVW20_RECEIVE_REVERSAL_VALIDATION;
        		} else if (transaction.getValidationStatus().equals(DataCollectionStatus.ERROR)) {
        			nextPage = MRW90_COMMUNICATION_ERROR;
        		} else {
        			nextPage = MRW60_REVIEW;
        		}
            
            } else if (currentPage.equals(MRW60_REVIEW)) {
            	if (transaction.getValidationStatus().equals(DataCollectionStatus.ERROR)) {
        			nextPage = MRW90_COMMUNICATION_ERROR;
        		} else if (payoutRequired) {
            		nextPage = MRW65_PRINTING;
            	} else {
            		nextPage = RVW10_RECEIVE_DETAIL;
            	}
            
            } else if (currentPage.equals(MRW65_PRINTING)) {  
//                if (transaction.getStatus() == ClientTransaction.COMPLETED) {
//                    nextPage = MRW65_PRINTING;  
//                } else if (transaction.getStatus() == ClientTransaction.FAILED) {
//                    nextPage = MRW90_COMMUNICATION_ERROR;
//                }
            	nextPage = RVW10_RECEIVE_DETAIL;
            }
        
    	} else if (direction == PageExitListener.BACK ) {
        	if (currentPage.equals(RVW10_RECEIVE_DETAIL)) {   
        		nextPage = RVW05_RECEIVE_SEARCH;
            } else if (currentPage.equals(RVW15_RECEIVE_REVERSAL_DATA)) {
        		nextPage = RVW10_RECEIVE_DETAIL;
            } else if (currentPage.equals(MRW55_PAYOUT) || currentPage.equals(MRW57_PAYOUT_CHECK)) {
            	if (transaction.isShowScreenRVW15()) {
            		nextPage = RVW15_RECEIVE_REVERSAL_DATA;
            	} else {
            		nextPage = RVW10_RECEIVE_DETAIL;
            	}
            } else if (currentPage.equals(MRW60_REVIEW)) {
            	if (payoutRequired) {
	                if ((transaction.getProfileMoneyGramChecks().equals("DG"))) {
	                    nextPage = MRW55_PAYOUT; 
	                } else {
	                    nextPage = MRW57_PAYOUT_CHECK; 
	                }
            	} else if (transaction.isShowScreenRVW15()) {
            		nextPage = RVW15_RECEIVE_REVERSAL_DATA;
            	} else {
            		nextPage = RVW10_RECEIVE_DETAIL;
            	}
            } else if (currentPage.equals(MRW65_PRINTING)) {
        		nextPage = MRW60_REVIEW;
            }
        
        } else if (direction == PageExitListener.FAILED) {
            if (currentPage.equals(MRW65_PRINTING)) { 
               nextPage = RVW05_RECEIVE_SEARCH;  
            }
        }
        
        if (nextPage != null) {
            showPage(nextPage, direction);
        } else {
            super.exit(direction);
        }
    }
}
