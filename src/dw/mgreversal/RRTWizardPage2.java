package dw.mgreversal;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JScrollPane;

import com.moneygram.agentconnect.TransactionLookupResponse;
import com.moneygram.agentconnect.TransactionLookupResponse.Payload;
import com.moneygram.agentconnect.TransactionStatusType;

import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.FieldKey;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.mgreceive.MoneyGramReceiveTransaction;
import dw.mgsend.MoneyGramClientTransaction;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.ReceiveDetailInfo;
import dw.profile.UnitProfile;
import dw.utility.DWValues;

public class RRTWizardPage2 extends ReceiveReversalFlowPage implements
		DWTextPaneListener {
	private static final long serialVersionUID = 1L;

    private String formattedSendAmount;
	private static int NAME_BOX_WIDTH_SCROLL_NO = 200;
	private static int NAME_BOX_WIDTH_SCROLL_YES = 284;
	private TransactionLookupResponse lookupResponse;
	private DWTextPane tp;
	private List<DWTextPane> textPanes;
	private int tagWidth;

    public RRTWizardPage2(ReceiveReversalTransaction tran,
                       String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "moneygramreceive/RRTWizardPage2.xml", name, pageCode, buttons);	
		textPanes = new ArrayList<DWTextPane>();
		tp = (DWTextPane) getComponent("lineLabel");
        tp.addListener(this, this);
    }

	@Override
	public void dwTextPaneResized() {

		DWTextPane tp = (DWTextPane) getComponent("lineLabel");
		JScrollPane scrollPane = (JScrollPane) getComponent("scrollPane");		
		
		int width = (scrollPane.getPreferredSize().width > 0 ? scrollPane
				.getPreferredSize().width - NAME_BOX_WIDTH_SCROLL_NO
				: NAME_BOX_WIDTH_SCROLL_YES);

		tp.setWidth(width);
		tp.setListenerEnabled(true);

		int w = FIELD_TOTAL_WIDTH - tagWidth;
		for (DWTextPane pane : textPanes) {
			pane.setWidth(w);
		}
		
		this.validate();
		this.repaint();
	}

    @Override
	public void start(int direction) {
        flowButtons.reset();
        flowButtons.setVisible("expert", false); 
        flowButtons.setEnabled("next", false);         

        if (direction == PageExitListener.NEXT) {        	        	        	        	
            populateScreen();
        } else {
            flowButtons.setSelected("next"); 
        }
    }

    private void populateScreen(){
        ReceiveDetailInfo receiveDetailInfo = receiveReversalTransaction.getReceiveDetail();
        Map<String, String> values = receiveReversalTransaction.getDataCollectionData().getCurrentValueMap();

    	List<JLabel> tags = new ArrayList<JLabel>();
		
		String tag = transaction.getTransactionLookupInfoLabel(FieldKey.SENDER_PRIMARYPHONE_KEY.getInfoKey());
        this.displayLabelItem("senderHomePhone", tag, receiveDetailInfo.getSenderHomePhone(), tags);
		
        tag = transaction.getTransactionLookupInfoLabel(FieldKey.ORIGINATINGCOUNTRY_KEY.getInfoKey());
        this.displayLabelItem("sendCountry", tag, CountryInfo.getCountryName(receiveDetailInfo.getOriginatingCountry()), tags);

        String s = receiveDetailInfo.getDateTimeSent();
		tag = transaction.getTransactionLookupInfoLabel("dateTimeSent");
        this.displayLabelItem("sent", tag, FormatSymbols.formatDateTimeForLocaleWithTimeZone(s), tags);
        
        String sNameFirst = values.get(FieldKey.SENDER_FIRSTNAME_KEY.getInfoKey());
        String sNameMiddle = values.get(FieldKey.SENDER_MIDDLENAME_KEY.getInfoKey());
        String sNameLast = values.get(FieldKey.SENDER_LASTNAME_KEY.getInfoKey());
        String sNameLast2 = values.get(FieldKey.SENDER_LASTNAME2_KEY.getInfoKey());
        String name = DWValues.formatName(sNameFirst, sNameMiddle, sNameLast, sNameLast2);
		this.displayPaneItem("senderName", name, tags, textPanes);
        
        String rNameFirst = values.get(FieldKey.RECEIVER_FIRSTNAME_KEY.getInfoKey());
        String rNameMiddle = values.get(FieldKey.RECEIVER_MIDDLENAME_KEY.getInfoKey());
        String rNameLast = values.get(FieldKey.RECEIVER_LASTNAME_KEY.getInfoKey());
        String rNameLast2 = values.get(FieldKey.RECEIVER_LASTNAME2_KEY.getInfoKey());
        name = DWValues.formatName(rNameFirst, rNameMiddle, rNameLast, rNameLast2);
		this.displayPaneItem("receiverName", name, tags, textPanes);
        
        String message = (receiveDetailInfo.getMessageField1() == null ? "" : receiveDetailInfo.getMessageField1()) + 	
        		(receiveDetailInfo.getMessageField2() == null ? "" : " " + receiveDetailInfo.getMessageField2()); 
		this.displayPaneItem("message", message, tags, textPanes);
        
        this.displayLabelItem("reference", receiveReversalTransaction.getReversalReferenceNumber(), tags);
           
        Payload payload = receiveReversalTransaction.getTransactionLookupResponsePayload();
        TransactionStatusType statusType = (payload != null) ? payload.getTransactionStatus() : null;
        String status = statusType != null ? getTranactionStatus(statusType, transaction.getDataCollectionData().getCurrentValueMap(), true, false) : null;
        this.displayPaneItem("status", status, false, tags, textPanes);

        BigDecimal receiveAmount = null;
        if ((transaction.getTransactionLookupResponsePayload() != null) && (transaction.getTransactionLookupResponsePayload().getReceiveAmounts() != null)) {
        	receiveAmount = transaction.getTransactionLookupResponsePayload().getReceiveAmounts().getReceiveAmount();
        }
		this.displayMoney("receiveAmount", receiveAmount, receiveDetailInfo.getReceiveCurrency(), tags);

        // set status-specific values and instruction labels

    	if ((statusType != null) && (statusType.equals(TransactionStatusType.RECVD))) {
            tp.setText(Messages.getString("RRTWizardPage2.okLine").trim()); 
        	flowButtons.setEnabled("next", true);
    	} else if ((statusType != null) && (statusType.equals(TransactionStatusType.AVAIL))) {
    		tp.setText("");
            flowButtons.setAlternateText("cancel", Messages.getString("OK"));  
            flowButtons.setEnabled("back", false); 
            flowButtons.getButton("cancel").requestFocus();     		
    	} else {
            tp.setText(Messages.getString("RRTWizardPage2.badLine")); 
        	flowButtons.setEnabled("next", false);
    	}

    	tagWidth = this.setTagLabelWidths(tags, MINIMUM_TAG_WIDTH);
    }

    @Override
	public void commComplete(int commTag, Object returnValue) {
        boolean b = ((Boolean) returnValue).booleanValue();
        switch (commTag) {
        case MoneyGramClientTransaction.TRANSACTION_LOOKUP:
            if (b) {

                if (UnitProfile.getInstance().isDemoMode()|| UnitProfile.getInstance().isTrainingMode()){
                	lookupResponse = transaction.getTransactionLookupResponse();
                	
                	// In demo mode this could not be fixed in the demo server
                	// because we don't have an input value for getLookupDetail that
                	// we can manipulate to accomplish the two different status values
                	// for the same input call.
                	if (!receiveReversalTransaction.getReversalReferenceNumber().equals("11223344"))
                		lookupResponse.getPayload().getValue().setTransactionStatus(TransactionStatusType.AVAIL);
                }
                populateScreen();
                flowButtons.setAlternateText("cancel", Messages.getString("OK"));  
                flowButtons.setEnabled("back", false); 
                flowButtons.getButton("cancel").requestFocus(); 
                //reverseButton.setEnabled(false);
            }
            else {
                PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
                return;
            }
            break;

		 case MoneyGramClientTransaction.RECEIVE_REVERSAL_VALIDATION:
             if (b) {
                 PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
             } else {
                 PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
             }
             return;			

		default:
			return;
        }
    }

    /** Agent and customer receive reversal receipts to the Epson printer
     * 
     */
    public void reprintReceipt() {
       	reprint(transaction);
    }

	/*
	 *  Print/Reprint of receive reversal receipt(s).
	 */
	public void reprint(MoneyGramReceiveTransaction tran) {
		tran.resetPrintFailure();
		if (tran.isReceiveAgentAndCustomerReceipts()) {
			tran.printReceipt(false, true);
			if (!tran.wasPrintFailure()) {
				tran.printReceipt(true, true);
			}
		} else {
			if (tran.isReceiveCustomerReceipt()) {
				tran.printReceipt(true, true);
			} else if (tran.isReceiveAgentReceipt()) {
				tran.printReceipt(false, true);
			}
		}
	}

    @Override
	public void finish(int direction) {
        if (direction == PageExitListener.NEXT) {
            flowButtons.setButtonsEnabled(true);
			flowButtons.setVisible("expert", false);  
			receiveReversalTransaction.receiveReversalValidation(this, ValidationType.INITIAL_NON_FORM_FREE);
            return;
        }
        else if (direction == PageExitListener.CANCELED && flowButtons.getButton("cancel").getText().equals(Messages.getString("OK")))
            direction = PageExitListener.COMPLETED;

        PageNotification.notifyExitListeners(this, direction);
    }
}
