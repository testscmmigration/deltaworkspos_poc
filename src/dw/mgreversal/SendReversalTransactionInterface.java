package dw.mgreversal;

import dw.framework.ClientTransaction;
import dw.framework.PageNameInterface;
import dw.framework.TransactionInterface;
import dw.mgreceive.MoneyGramReceiveTransaction;

public abstract class SendReversalTransactionInterface extends TransactionInterface {
	private static final long serialVersionUID = 1L;

	protected SendReversalTransaction transaction;
//    protected MoneyGramReceiveTransaction mgrTransaction;
    
    public SendReversalTransactionInterface(String fileName,
                                 SendReversalTransaction transaction,
                                 MoneyGramReceiveTransaction mgrTransaction, 
                                 PageNameInterface naming,
                                 boolean sideBar) {
        super(fileName, naming, sideBar);
//        this.mgrTransaction =mgrTransaction;
        this.transaction = transaction;
    }

    @Override
	public ClientTransaction getTransaction() {
        return transaction;
    }
}
