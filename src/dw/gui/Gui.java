/*                                          
 * Gui.java
 *
 * Copyright (c) 1999-2002 MoneyGram, Inc.
 *
 */

package dw.gui;

import java.awt.Component;
import java.awt.Container;
import java.io.File;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Stack;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import dw.utility.Debug;

/**
 * This class creates a collection of GUI elements based on the contents
 * of an XML file. The application can retrieve specific GUI elements using
 * the <code>get()</code> method, for example to register event handlers.
 * <p>
 * It follows that the application needs to know something about the types
 * of the GUI elements specified in the XML file, in order to register
 * appropriate event handlers. But it need not distinguish between buttons
 * and menu items, for example, nor need it know how the various GUI elements
 * are arranged.
 * <p>
 * The public methods inherited from <code>DocumentHandler</code> are used
 * by the XML parser to populate the collection. They should not be used by
 * applications.
 *
 * @version 0.3 12/10/1999
 * @author Geoffrey Atkin
 */
public class Gui {

    /**
     * The collection of GUI objects.
     */
    Map<String, Object> components;

    /**
     * Conceptually, an XML file maps naturally onto a tree, with nested tags
     * represented as children. But we don't need the whole tree in memory,
     * just the current GUI element and all its parents, so a stack is
     * sufficient. Each opening tag pushes an element onto this stack, and
     * the matching closing tag pops it off.
     */
    private Stack<GuiElement> stack;

    /**
     * A collection that maps component names to the component labels.
     * This is used to generate calls to label.setLabelFor(), which have
     * to be delayed because labels come first.
     */
    private Map<String, JLabel> labels;

    /**
     * Whether or not XML validation was requested.
     */
    private boolean validating;

    /**
     * Create the GUI elements from the XML input source.
     * @param input the InputSource to read the XML info from.
     * @param validate indicates whether to use a validating parser.
     */
    private void create(String url, boolean validate) throws GuiException {
        validating = validate;
        components = new HashMap<String, Object>();
        SAXParserFactory spf = SAXParserFactory.newInstance();
        
        
        try {
            spf.setFeature("http://xml.org/sax/features/external-general-entities", false);
            spf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            spf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true); 
            spf.setValidating(validate);
            SAXParser sp = spf.newSAXParser();
            sp.parse(url, new Handler());
        }
        catch (SAXParseException e) {
            //Debug.printStackTrace(e.getException());
            throw new GuiException("SAXParseException in XML file : " + url + 
                    " at line " + e.getLineNumber() + 
                    " column " + e.getColumnNumber() 
                    + ":\n\t" + e.getMessage(), e); 
        }
        catch (Exception e) {
            throw new GuiException("Embedded exception " + e.getMessage(), e); 
        }
    }
    
    /**
     * Creates a collection of GUI objects by parsing an XML file.
     * @param filename the name of the XML file to parse.
     * @param validate indicates whether to use a validating parser.
     */
    public Gui(File file, boolean validate) throws GuiException
    {
        create("file:" + file.getAbsolutePath(), validate); 
    }

    /**
     * Creates a collection of GUI objects by parsing an XML file.
     * @param url the URL of the XML file to parse.
     * @param validate indicates whether to use a validating parser.
     */
    public Gui(URL url, boolean validate) throws GuiException
    {
        create(url.toString(), validate);
    }

    /**
     * Creates a collection of GUI objects by parsing an XML file.
     * @param filename the name of the XML file to parse.
     */
    public Gui(File file) throws GuiException {
        this(file, false);
    }

    /**
     * Creates a collection of GUI objects by parsing an XML file.
     * @param url the URL of the XML file to parse.
     */
    public Gui(URL fileUrl) throws GuiException {
        this(fileUrl, false);
    }

    /**
     * Add the component to the container specified.
     */
    public void addComponent(Component comp, String componentName,
            String containerName) {
        JComponent cont = null;
        cont = (JComponent) components.get(containerName);
        if (cont != null) {
            if (cont instanceof JScrollPane) {
                ((JScrollPane) cont).getViewport().add(comp);
                components.put(componentName, comp);
            }
            if (cont instanceof JPanel) {
                cont.add(comp);
                components.put(componentName, comp);
            }
        }
    }

    /**
     * Returns a GUI element specified by the name given in the XML file.
     */
    public Object get(String key) {
        return components.get(key);
    }

    public Object get(StringBuffer key) {
        return components.get(key.toString());
    }

    public String getParamValue(String name) {
        StringBuffer value = (StringBuffer) components.get(name);
        if (value == null) {
            Debug.println("No param found : " + name);	
            value = new StringBuffer("");	
        }
        return value.toString();
    }

    public Object get(Object key) {
        if (key instanceof String)
            return get((String) key);
        else if (key instanceof StringBuffer)
            return get((StringBuffer) key);
        else
            return components.get(key);
    }

    /**
     * Indicates if there is a GUI element with the given name.
     */
    public boolean containsKey(String key) {
        return components.containsKey(key);
    }

    /**
     * Replace component in the container. This method will remove old component, 
     * create new one and put new component into the same location on the panel where
     * old one were. New component will be of the same class as old one was.
     * @param oldComponentKey is the key use by xmlDefinedPanel to reference component.
     * Note: This method is tested only with text fields. It is created to work around
     * dual focus problem/lost focus problem on a PageFlow(s).
     */
    public void replaceComponent(String oldComponentKey){
        try{
            JComponent oldComponent=(JComponent)get(oldComponentKey);       
            Class<? extends JComponent> oldComponentClass = oldComponent.getClass();
            JComponent newComponent = oldComponentClass.newInstance();
            newComponent.setMinimumSize(oldComponent.getMinimumSize());
            newComponent.setMaximumSize(oldComponent.getMaximumSize());
            newComponent.setPreferredSize(oldComponent.getPreferredSize());
            newComponent.setEnabled(oldComponent.isEnabled()); 
            newComponent.setBackground(oldComponent.getBackground()); 
            newComponent.setName(oldComponent.getName());            
            newComponent.setVisible(oldComponent.isVisible());
            
            Container container = oldComponent.getParent();
            int compIndex = -1;
        
            for (int i=0; i<container.getComponentCount(); i++){
                if (container.getComponent(i) == oldComponent){
                    compIndex = i;
                    container.remove(oldComponent);
                    container.add(newComponent, null, compIndex);
                    components.put(oldComponentKey, newComponent);                
                    break;
                }   
            }               
        }catch(Exception e){
            Debug.println("Replace component failed");	
            Debug.printStackTrace(e);
        }    
    }    

    /**
     * Get a collection of all the components defined in this panel.
     * Used by XmlDefinedPanel.
     */
    public Collection<Object> getAllComponents() {
        return components.values();
    }

    private class Handler extends DefaultHandler {

        @Override
		public void startDocument() {
            stack = new Stack<GuiElement>();
            labels = new HashMap<String, JLabel>();
        }
        
        @Override
		public void endDocument() {
            Iterator<String> i = labels.keySet().iterator();
            String name;
            JLabel label;
            JComponent labeled;
            
            while (i.hasNext()) {
                name = i.next();
                labeled = (JComponent) components.get(name);
                label = labels.get(name);
                label.setLabelFor(labeled);
            }
            
            stack = null;
            labels = null;
        }
        
        /*
         * Called when the parser recognizes a starting tag. Create a GuiElement
         * and handle its attributes: "name" and "labelFor" are special cases,
         * the rest are deferred to GuiElement.
         *
         */
        @Override
		public void startElement(String namespace, String local, String qName,
                Attributes attrs) {
            /*
                Debug.println("startElement(" + namespace + "," + local + "," 
                    + qName + ",...)");	
                for (int i = 0; i < attrs.getLength(); ++i) {
                    Debug.println("\t(" + attrs.getLocalName(i) + "," 	 
                        + attrs.getQName(i) + "," + attrs.getValue(i) + ")");	 
                }
            */
            
            String tag = qName;
            String name = null;
            String labelFor = null;
            GuiElement g = null;
            JComponent c = null;
            StringBuffer sb;
            int i;
            
            if (tag.equals("GUI"))	
                return;
            
            if (attrs != null) {
                name = attrs.getValue("name"); 
                labelFor = attrs.getValue("labelFor"); 
            }
            
            g = GuiElement.create(tag);
            stack.push(g);
            
            c = g.getComponent();
            sb = g.getStringBuffer();
            
            if (name != null) {
                if (c != null){
                    components.put(name, c);
                    c.setName(name);
                }    
                else if (sb != null)
                    components.put(name, sb);
            }
            
            if ((c instanceof JLabel) && (labelFor != null)) {
                labels.put(labelFor, (JLabel) c);
            }
            if (attrs != null) {
	            // call setProperty() for all attributes except labelFor
	            for (i = 0; i < attrs.getLength(); i++) {
	                if (! attrs.getQName(i).equals("labelFor"))	
	                    g.setProperty(attrs.getQName(i), attrs.getValue(i));
	            }
            }
        }
        
        @Override
		public void endElement(String namespace, String local, String qName) {

            //Debug.println("endElement(" + namespace + "," + local + "," + qName + ")");

            String tag = qName;
            GuiElement top, g;

            if (tag.equals("GUI"))	
                return;
            
            g = stack.pop();
            
            if (! stack.empty()) {
                top = stack.peek();
                top.add(g, tag);
            }
        }

        @Override
		public void characters(char buf[], int offset, int len) {
            GuiElement g;
                
            if (! stack.empty()) {
                g = stack.peek();
                g.append(buf, offset, len);
            }
        }

        /** 
         * SAX callback for external entities. All we have is the DTD file
         * mentioned in the DOCTYPE. We can ignore it if not validating.
         */
        @Override
		public InputSource resolveEntity (String publicId, String systemId) {
            //Debug.println("resolveEntity(" + publicId + ", " + systemId + ")");
            if (! validating) {
                return new InputSource(new java.io.StringReader(""));	
            }
            return null;
        } 
        
        @Override
		public void error(SAXParseException e) {
            Debug.println("** Error: " + e.getMessage()	
                    + "\nline " + e.getLineNumber()	
                    + " col " + e.getColumnNumber()); 
        }
        
        @Override
		public void warning(SAXParseException err) {
            Debug.println ("** Warning: " + err.getMessage()	
                    + "\nline " + err.getLineNumber()	
                    + " col " + err.getColumnNumber()); 
        }
    }   
} 


