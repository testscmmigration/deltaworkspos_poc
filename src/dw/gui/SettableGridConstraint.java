package dw.gui;

import java.awt.GridBagConstraints;

public class SettableGridConstraint extends GridBagConstraints {
	private static final long serialVersionUID = 1L;

	/**
	 * @param anchor
	 *            the anchor to set
	 */
	public void setAnchor(String anchor) {
		if ("FIRST_LINE_START".equalsIgnoreCase(anchor)) {
			this.anchor = GridBagConstraints.FIRST_LINE_START;
		} else if ("PAGE_START".equalsIgnoreCase(anchor)) {
			this.anchor = GridBagConstraints.PAGE_START;
		} else if ("FIRST_LINE_END".equalsIgnoreCase(anchor)) {
			this.anchor = GridBagConstraints.FIRST_LINE_END;
		} else if ("LINE_START".equalsIgnoreCase(anchor)) {
			this.anchor = GridBagConstraints.LINE_START;
		} else if ("CENTER".equalsIgnoreCase(anchor)) {
			this.anchor = GridBagConstraints.CENTER;
		} else if ("LINE_END".equalsIgnoreCase(anchor)) {
			this.anchor = GridBagConstraints.LINE_END;
		} else if ("LAST_LINE_START".equalsIgnoreCase(anchor)) {
			this.anchor = GridBagConstraints.LAST_LINE_START;
		} else if ("PAGE_END".equalsIgnoreCase(anchor)) {
			this.anchor = GridBagConstraints.PAGE_END;
		} else if ("LAST_LINE_END".equalsIgnoreCase(anchor)) {
			this.anchor = GridBagConstraints.LAST_LINE_END;
		} else if ("WEST".equalsIgnoreCase(anchor)) {
			this.anchor = GridBagConstraints.WEST;
		} else if ("NORTH".equalsIgnoreCase(anchor)) {
			this.anchor = GridBagConstraints.NORTH;
		} else if ("NORTHWEST".equalsIgnoreCase(anchor)) {
			this.anchor = GridBagConstraints.NORTHWEST;
		} else if ("NORTHEAST".equalsIgnoreCase(anchor)) {
			this.anchor = GridBagConstraints.NORTHEAST;
		} else if ("EAST".equalsIgnoreCase(anchor)) {
			this.anchor = GridBagConstraints.EAST;
		} else if ("SOUTHEAST".equalsIgnoreCase(anchor)) {
			this.anchor = GridBagConstraints.SOUTHEAST;
		} else if ("SOUTH".equalsIgnoreCase(anchor)) {
			this.anchor = GridBagConstraints.SOUTH;
		} else if ("SOUTHWEST".equalsIgnoreCase(anchor)) {
			this.anchor = GridBagConstraints.SOUTHWEST;
		} else if ("CENTER".equalsIgnoreCase(anchor)) {
			this.anchor = GridBagConstraints.CENTER;
		}

	}

	/**
	 * @param fill
	 *            the fill to set
	 */
	public void setFill(String fill) {
		if ("HORIZONTAL".equalsIgnoreCase(fill)) {
			this.fill = GridBagConstraints.HORIZONTAL;
		} else if ("VERTICAL".equalsIgnoreCase(fill)) {
			this.fill = GridBagConstraints.VERTICAL;
		} else if ("BOTH".equalsIgnoreCase(fill)) {
			this.fill = GridBagConstraints.BOTH;
		} else if ("NONE".equalsIgnoreCase(fill)) {
			this.fill = GridBagConstraints.NONE;
		}
	}

	/**
	 * @param gridheight
	 *            the gridheight to set
	 */
	public void setGridheight(int gridheight) {
		this.gridheight = gridheight;
	}

	/**
	 * @param gridwidth
	 *            the gridwidth to set
	 */
	public void setGridwidth(int gridwidth) {
		this.gridwidth = gridwidth;
	}

	/**
	 * @param gridx
	 *            the gridx to set
	 */
	public void setGridx(int gridx) {
		this.gridx = gridx;
	}

	/**
	 * @param gridy
	 *            the gridy to set
	 */
	public void setGridy(int gridy) {
		this.gridy = gridy;
	}

	/**
	 * @param insetBottom
	 *            the insetBottom to set
	 */
	public void setInsetbottom(int insetBottom) {
		insets.bottom = insetBottom;
	}

	/**
	 * @param insetLeft
	 *            the insetLeft to set
	 */
	public void setInsetleft(int insetLeft) {
		insets.left = insetLeft;
	}

	/**
	 * @param insetRight
	 *            the insetRight to set
	 */
	public void setInsetright(int insetRight) {
		insets.right = insetRight;
	}

	/**
	 * @param insetTop
	 *            the insetTop to set
	 */
	public void setInsettop(int insetTop) {
		insets.top = insetTop;
	}

	/**
	 * @param ipadx
	 *            the ipadx to set
	 */
	public void setIpadx(int ipadx) {
		this.ipadx = ipadx;
	}

	/**
	 * @param ipady
	 *            the ipady to set
	 */
	public void setIpady(int ipady) {
		this.ipady = ipady;
	}

	/**
	 * @param weightx
	 *            the weightx to set
	 */
	public void setWeightx(double weightx) {
		this.weightx = weightx;
	}

	/**
	 * @param weighty
	 *            the weighty to set
	 */
	public void setWeighty(double weighty) {
		this.weighty = weighty;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[gridx:-" + gridx + "::gridy:-" + gridy + " weightx:-"
				+ weightx + " weighty:-" + weighty + "]";
	}
}
