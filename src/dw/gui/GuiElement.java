/*
 * GuiElement.java
 * 
 * $Revision$
 *
 * Copyright (c) 1999-2005 MoneyGram International
 */


package dw.gui;

import java.awt.Adjustable;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.SystemColor;
import java.lang.reflect.Method;
import java.util.Locale;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.Scrollable;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.table.JTableHeader;
import javax.swing.text.JTextComponent;

import dw.dwgui.BankCardSwipeTextField;
import dw.dwgui.CardSwipeTextField;
import dw.dwgui.CityProviderNumberListComboBox;
import dw.dwgui.CustomCardLayout;
import dw.dwgui.DWButton;
import dw.dwgui.DWCheckBox;
import dw.dwgui.DWDateField;
import dw.dwgui.DWLabel;
import dw.dwgui.DWPasswordField;
import dw.dwgui.DWRadioButton;
import dw.dwgui.DWScrollPane;
import dw.dwgui.DWTable;
import dw.dwgui.DWTextField;
import dw.dwgui.DWTextPane;
import dw.dwgui.DelimitedTextField;
import dw.dwgui.FeeField;
import dw.dwgui.HelpBox;
import dw.dwgui.HelpText;
import dw.dwgui.IndicativeBox;
import dw.dwgui.MaskedCardSwipeTextField;
import dw.dwgui.MoneyField;
import dw.dwgui.MultiListComboBox;
import dw.dwgui.QDOComboBox;
import dw.dwgui.StateListComboBox;
import dw.i18n.Messages;
import dw.model.adapters.CountryInfo.CountrySubdivision;
import dw.simplepm.PayeeField;
import dw.simplepm.ProfilePanel;
import dw.utility.Debug;

/**
 * This is a helper class for <code>Gui</code>. A GuiElement corresponds
 * a node in the XML parse tree. A GuiElement contains a Swing JComponent,
 * a string buffer, or both.
 * <p>
 * For example, the following XML data:
 *
 * <pre><code>
 *    &lt;MENUBAR name="menubar"&gt;
 *      &lt;MENU&gt;&lt;TEXT&gt;File&lt;/TEXT&gt;
 *        &lt;MENUITEM&gt;&lt;TEXT&gt;Exit&lt;/TEXT&gt;
 *          &lt;MNEMONIC&gt;x&lt;/MNEMONIC&gt;&lt;/MENUITEM&gt;
 *      &lt;/MENU&gt;
 *    &lt;/MENUBAR&gt;
 * </code></pre>
 *
 * corresponds to the following parse tree:
 *
 * <pre><code>
 *              GuiElement(JMenuBar,null)
 *                         |
 *               GuiElement(JMenu, null)
 *                      /      \
 *  GuiElement(null,"File")  GuiElement(JMenuItem,null)
 *                                  /      \
 *              GuiElement(null,"Exit")  GuiElement(null,"x")
 * </code></pre>
 * <p>
 * The <code>create</code> method is used to create the nodes of the tree.
 * The <code>add</code> method establishes the correct relationship between
 * a child node and its parent node by calling (for example)
 * <code>setText</code> or <code>setMnemonic</code> as appropriate.
 *
 * @author Geoff Atkin
 */

public class GuiElement {
    private JComponent c;
    private StringBuffer sb;
    private SettableDimension d;
    private String tabName;
    private SettableGridConstraint gConstraint;    
    private static ButtonGroup buttongroup = null;

    private static final Dimension tfPreferred = new Dimension(117, 23);
    private static final Dimension tfMinimum = new Dimension(23, 23);
    private static final Dimension tfMaximum=new Dimension(Short.MAX_VALUE, 23);
    private static final Dimension csfPreferred = new Dimension(122, 23);
    private static final Dimension csfMinimum = new Dimension(23, 23);
    private static final Dimension csfMaximum=new Dimension(Short.MAX_VALUE, 23);
    private static final Dimension commandPreferred = new Dimension(75, 23);
    private static final Dimension commandPreferredEs = new Dimension(99, 23);
    private static final Dimension commandMaximum = commandPreferred;
    private static final Dimension commandMaximumEs =new Dimension(125, 23);
    private static final int typicalSpacing = 5;

    private static final Class<?>[] paramString = new Class[] {String.class};
    private static final Class<?>[] paramBoolean = new Class[] {Boolean.TYPE};
    private static final Class<?>[] paramInteger = new Class[] {Integer.TYPE};
    private static final Class<?>[] paramFloat = new Class[] {Float.TYPE};
    private static final Class<?>[] paramDouble = new Class[] {Double.TYPE};
    private static final Class<?>[] paramColor = new Class[] {Color.class};
    private static final Class<?>[] paramIcon = new Class[] {Icon.class};

    // Static constants for fonts so we don't keep allocating the same ones over 
    // and over. Note the old code tried to respect user font size preferences by
    // creating a JLabel and using its default font as a starting point. But DMP
    // overrides font preferences anyway because they don't work right in JDK 1.4.
    //    JLabel label = new JLabel();
    //    Font labelFont = label.getFont();
    //    label.setFont(new Font(labelFont.getName(), Font.BOLD,
    //    labelFont.getSize()));

    private static final Font BIG_FONT = new Font("sanserif", Font.BOLD, 16);	
    public static final Font PLAIN_FONT = new Font("Dialog", Font.PLAIN, 12);	
    public static final Font BOLD_FONT = new Font("Dialog", Font.BOLD, 12);	

    // Static constants for parameter lists which will be passed to invoke().
    // Saves having to constantly allocate the same small arrays repeatedly.

    private static final Object[] ARG_BOOLEAN_TRUE = new Object[] { Boolean.TRUE };
    private static final Object[] ARG_BOOLEAN_FALSE = new Object[] { Boolean.FALSE };
    
    private static final Object[] ARG_FLOAT_BOTTOM = new Object[] { Float.valueOf(Component.BOTTOM_ALIGNMENT) };
    private static final Object[] ARG_FLOAT_CENTER = new Object[] { Float.valueOf(Component.CENTER_ALIGNMENT) };
    private static final Object[] ARG_FLOAT_LEFT = new Object[] { Float.valueOf(Component.LEFT_ALIGNMENT) };
    private static final Object[] ARG_FLOAT_RIGHT = new Object[] { Float.valueOf(Component.RIGHT_ALIGNMENT) };
    private static final Object[] ARG_FLOAT_TOP = new Object[] { Float.valueOf(Component.TOP_ALIGNMENT) };

    private static final Object[] ARG_INTEGER_BOTTOM = new Object[] { Integer.valueOf(SwingConstants.BOTTOM) };
    private static final Object[] ARG_INTEGER_CENTER = new Object[] { Integer.valueOf(SwingConstants.CENTER) };
    private static final Object[] ARG_INTEGER_HORIZONTAL = new Object[] { Integer.valueOf(SwingConstants.HORIZONTAL) };
    private static final Object[] ARG_INTEGER_HORIZONTAL_SPLIT = new Object[] { Integer.valueOf(JSplitPane.HORIZONTAL_SPLIT) };
    private static final Object[] ARG_INTEGER_LEADING = new Object[] { Integer.valueOf(SwingConstants.LEADING) };
    private static final Object[] ARG_INTEGER_LEFT = new Object[] { Integer.valueOf(SwingConstants.LEFT) };
    private static final Object[] ARG_INTEGER_RIGHT = new Object[] { Integer.valueOf(SwingConstants.RIGHT) };
    private static final Object[] ARG_INTEGER_TOP = new Object[] { Integer.valueOf(SwingConstants.TOP) };
    private static final Object[] ARG_INTEGER_TRAILING = new Object[] { Integer.valueOf(SwingConstants.TRAILING) };
    private static final Object[] ARG_INTEGER_VERTICAL = new Object[] { Integer.valueOf(SwingConstants.VERTICAL) };
    private static final Object[] ARG_INTEGER_VERTICAL_SPLIT = new Object[] { Integer.valueOf(JSplitPane.VERTICAL_SPLIT) };
    private static final Object[] ARG_INTEGER_NORTH = new Object[] { Integer.valueOf(SwingConstants.NORTH) };
    private static final Object[] ARG_INTEGER_NORTH_EAST = new Object[] { Integer.valueOf(SwingConstants.NORTH_EAST) };
    private static final Object[] ARG_INTEGER_NORTH_WEST = new Object[] { Integer.valueOf(SwingConstants.NORTH_WEST) };
    private static final Object[] ARG_INTEGER_SOUTH = new Object[] { Integer.valueOf(SwingConstants.SOUTH) };
    private static final Object[] ARG_INTEGER_SOUTH_EAST = new Object[] { Integer.valueOf(SwingConstants.SOUTH_EAST) };
    private static final Object[] ARG_INTEGER_SOUTH_WEST = new Object[] { Integer.valueOf(SwingConstants.SOUTH_WEST) };
    private static final Object[] ARG_INTEGER_EAST = new Object[] { Integer.valueOf(SwingConstants.EAST) };
    private static final Object[] ARG_INTEGER_WEST = new Object[] { Integer.valueOf(SwingConstants.WEST) };
    private static final Object[] ARG_INTEGER_MULTIPLE_INTERVAL_SELECTION = new Object[] { Integer.valueOf(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION) };
    private static final Object[] ARG_INTEGER_SINGLE_INTERVAL_SELECTION = new Object[] { Integer.valueOf(ListSelectionModel.SINGLE_INTERVAL_SELECTION) };
    private static final Object[] ARG_INTEGER_SINGLE_SELECTION = new Object[] { Integer.valueOf(ListSelectionModel.SINGLE_SELECTION) };
    private static final Object[] ARG_INTEGER_HORIZONTAL_SCROLLBAR_ALWAYS = new Object[] { Integer.valueOf(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS) };
    private static final Object[] ARG_INTEGER_HORIZONTAL_SCROLLBAR_AS_NEEDED = new Object[] { Integer.valueOf(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED) };
    private static final Object[] ARG_INTEGER_HORIZONTAL_SCROLLBAR_NEVER = new Object[] { Integer.valueOf(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER) };
    private static final Object[] ARG_INTEGER_VERTICAL_SCROLLBAR_ALWAYS = new Object[] { Integer.valueOf(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS) };
    private static final Object[] ARG_INTEGER_VERTICAL_SCROLLBAR_AS_NEEDED = new Object[] { Integer.valueOf(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED) };
    private static final Object[] ARG_INTEGER_VERTICAL_SCROLLBAR_NEVER = new Object[] { Integer.valueOf(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER) };
    
    private static final Object[] ARG_COLOR_WHITE = new Object[] { Color.white };
    private static final Object[] ARG_COLOR_RED = new Object[] { Color.red };
    private static final Object[] ARG_COLOR_YELLOW = new Object[] { Color.yellow };
    private static final Object[] ARG_COLOR_HELPYELLOW = new Object[] { new Color(254, 255, 219) };
    private static final Object[] ARG_COLOR_BLUE = new Object[] { Color.blue };
    private static final Object[] ARG_COLOR_GREEN = new Object[] { Color.green };
    private static final Object[] ARG_COLOR_CYAN = new Object[] { Color.cyan };
    private static final Object[] ARG_COLOR_CONTROL = new Object[] { SystemColor.control };
    private static final Object[] ARG_COLOR_CONTROL_SHADOW = new Object[] { SystemColor.controlShadow };
    private static final Object[] ARG_COLOR_DESKTOP = new Object[] { SystemColor.desktop };
    
    public GuiElement() {
        this (null, null);
    }
    
    /**
     * Create a GuiElement containing the given JComponent and StringBuffer,
     * either of which may be null.
     */
    private GuiElement(JComponent c, StringBuffer sb) {
      	this.c = c;
        this.sb = sb;
        this.d = null;
    }
    
    private GuiElement(JComponent c, StringBuffer sb, SettableDimension d) {
        this.c = c;
        this.sb = sb;
        this.d = d;
    }
    private GuiElement(JComponent c, StringBuffer sb, SettableGridConstraint gConstraint) {
    
        this.c = c;
        this.sb = sb;
        this.gConstraint = gConstraint;
    }
    /**
     * Returns the JComponent contained in this GuiElement.
     */
    public JComponent getComponent() {
        return c;
    }
    
    /**
     * Returns the Dimension contained in this GuiElement.
     */
    public SettableDimension getDimension() {
        return d;
    }

    public SettableGridConstraint getGridConstraint()
    {
    	return gConstraint;
    }
    /**
     * Returns the StringBuffer contained in this GuiElement.
     */
    public StringBuffer getStringBuffer() {
        return sb;
    }

    public String getTabName() {
        return tabName;
    }

    /**
     * Returns the String represented by the StringBuffer contained in
     * this GuiElement.
     */
    public String getString() {
        if (sb != null)
            return sb.toString();
        else
            return "";	
    }
    
    /**
     * Returns the first character of the StringBuffer contained in this
     * GuiElement.
     */
    public char getChar() {
        return sb.charAt(0);
    }

    /**
     * Appends the given arrary of characters to the StringBuffer contained
     * in this GuiElement.
     */
    public void append(char buf[], int offset, int len) {
        if (sb != null)
            sb.append(buf, offset, len);
    }

    /**
     * Set the given property on the component, string buffer, or dimension
     * contained in this GuiElement.
     * @param property the name of the property to set.
     * @param value the value to set the property to.
     */
    public void setProperty(String property, String value) {
      	if (sb != null) {
            if (property.equals("value"))	
                sb.append(value);
            return;
        }
        
        if (c != null) {
            setProperty(c, property, value);
        }
        else if (d != null) {
            setProperty(d, property, value);
        }else if (gConstraint != null) {
            setProperty(gConstraint, property, value);
        }
    }
    
    /**
     * Set the given property on the specified object.
     * @param target the component whose property to set.
     * @param property the name of the property to set.
     * @param value the value to set the property to.
     */
    public void setProperty(Object target, String property, String value) {
        String methodname;
        Method m;
        Class<?> c;
        
        if (target == null)
            return;
        
        // Given a property named "foo", search for a method named
        // setFoo(), convert the argument as needed, and invoke it.
        
        c = target.getClass();
        methodname = "set" + property.substring(0, 1).toUpperCase(Locale.US) 
            + property.substring(1);
        
        try {
            try {
                m = c.getMethod(methodname, paramBoolean);
                m.invoke(target, argBoolean(value));
            }
            catch (NoSuchMethodException e1) {
                try {
                    m = c.getMethod(methodname, paramString);
                    m.invoke(target, new Object[] {value});
                }
                catch (NoSuchMethodException e2) {
                    try {
                        m = c.getMethod(methodname, paramInteger);
                        m.invoke(target, argInteger(value));
                    }
                    catch (NoSuchMethodException e3) {
                        try {
                            m = c.getMethod(methodname, paramFloat);
                            m.invoke(target, argFloat(value));
                        }
                        catch (NoSuchMethodException e4) {
                            try {
                                m = c.getMethod(methodname, paramColor);
                                m.invoke(target, argColor(value));
                            }
                            catch (NoSuchMethodException e41) {                                
	                            try {
	                                m = c.getMethod(methodname, paramDouble);
	                                m.invoke(target, argDouble(value));
	                            }
	                            catch (NoSuchMethodException e5) {
	                                try {
	                                    m = c.getMethod(methodname, paramIcon);
	                                    m.invoke(target, new Object[]
	                                            {argImageIcon(value)});
	                                }
	                                catch (NoSuchMethodException e6) {
	                                    // Last resort, maybe its a property of the
	                                    // layout manager.
	                                    if (target instanceof JPanel) {
	                                        JPanel p = (JPanel) target;
	                                        setProperty(p.getLayout(), property,
	                                                value);
	                                    }
	                                    else
	                                        Debug.println("No Such Method: "	
	                                                + methodname + " in " 
	                                                + target.getClass().getName());
	                                }
	                            }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e) {
            Debug.printStackTrace(e);
        }
    }
    
    /** Convert string representing a boolean value to parameter arrary. */
    private Object [] argBoolean(String s) {
        s = s.toUpperCase(Locale.US);
        if (s.equals("TRUE"))	
            return ARG_BOOLEAN_TRUE;
        else
            return ARG_BOOLEAN_FALSE;
    }

    private Object[] argFloat(String s) {
        s = s.toUpperCase(Locale.US);
                                        
        if (s.equals("BOTTOM"))             return ARG_FLOAT_BOTTOM;	
        else if (s.equals("CENTER"))        return ARG_FLOAT_CENTER;	
        else if (s.equals("FLOWLEFT"))      return ARG_FLOAT_LEFT;	
        else if (s.equals("FLOWRIGHT"))     return ARG_FLOAT_RIGHT;	
        else if (s.equals("LEFT"))          return ARG_FLOAT_LEFT;	
        else if (s.equals("RIGHT"))         return ARG_FLOAT_RIGHT;	
        else if (s.equals("TOP"))           return ARG_FLOAT_TOP;	
        else return new Object[] { Float.valueOf(s) };
    }
    
    private Object[] argInteger(String s) {
      	s = s.toUpperCase(Locale.US);
        if (s.equals("BOTTOM"))              return ARG_INTEGER_BOTTOM;	
        else if (s.equals("CENTER"))         return ARG_INTEGER_CENTER;	
        else if (s.equals("HORIZONTAL"))     return ARG_INTEGER_HORIZONTAL;	
        else if (s.equals("HORIZONTAL_SPLIT"))  return ARG_INTEGER_HORIZONTAL_SPLIT;	
        else if (s.equals("LEADING"))        return ARG_INTEGER_LEADING;	
        else if (s.equals("LEFT"))           return ARG_INTEGER_LEFT;	
        else if (s.equals("RIGHT"))          return ARG_INTEGER_RIGHT;	
        else if (s.equals("TOP"))            return ARG_INTEGER_TOP;	
        else if (s.equals("TRAILING"))       return ARG_INTEGER_TRAILING;	
        else if (s.equals("VERTICAL"))       return ARG_INTEGER_VERTICAL;	
        else if (s.equals("VERTICAL_SPLIT")) return ARG_INTEGER_VERTICAL_SPLIT;	
        else if (s.equals("NORTH"))          return ARG_INTEGER_NORTH;	
        else if (s.equals("NORTH_EAST"))     return ARG_INTEGER_NORTH_EAST;	
        else if (s.equals("NORTH_WEST"))     return ARG_INTEGER_NORTH_WEST;	
        else if (s.equals("SOUTH"))          return ARG_INTEGER_SOUTH;	
        else if (s.equals("SOUTH_EAST"))     return ARG_INTEGER_SOUTH_EAST;	
        else if (s.equals("SOUTH_WEST"))     return ARG_INTEGER_SOUTH_WEST;	
        else if (s.equals("EAST"))           return ARG_INTEGER_EAST;	
        else if (s.equals("WEST"))           return ARG_INTEGER_WEST;	
        else if (s.equals("MULTIPLE_INTERVAL_SELECTION"))   return ARG_INTEGER_MULTIPLE_INTERVAL_SELECTION;	
        else if (s.equals("SINGLE_INTERVAL_SELECTION"))     return ARG_INTEGER_SINGLE_INTERVAL_SELECTION;	
        else if (s.equals("SINGLE_SELECTION"))              return ARG_INTEGER_SINGLE_SELECTION;	
        else if (s.equals("HORIZONTAL_SCROLLBAR_ALWAYS"))   return ARG_INTEGER_HORIZONTAL_SCROLLBAR_ALWAYS;	
        else if (s.equals("HORIZONTAL_SCROLLBAR_AS_NEEDED"))return ARG_INTEGER_HORIZONTAL_SCROLLBAR_AS_NEEDED;	
        else if (s.equals("HORIZONTAL_SCROLLBAR_NEVER"))    return ARG_INTEGER_HORIZONTAL_SCROLLBAR_NEVER;	
        else if (s.equals("VERTICAL_SCROLLBAR_ALWAYS"))     return ARG_INTEGER_VERTICAL_SCROLLBAR_ALWAYS;	
        else if (s.equals("VERTICAL_SCROLLBAR_AS_NEEDED"))  return ARG_INTEGER_VERTICAL_SCROLLBAR_AS_NEEDED;	
        else if (s.equals("VERTICAL_SCROLLBAR_NEVER"))      return ARG_INTEGER_VERTICAL_SCROLLBAR_NEVER;	
        else return new Object[] { Integer.valueOf(s) };      
    }
    
    private Object[] argColor(String s) {
        if (s.equals("white"))              return ARG_COLOR_WHITE;	
        else if (s.equals("red"))           return ARG_COLOR_RED;	
        else if (s.equals("yellow"))        return ARG_COLOR_YELLOW;    
        else if (s.equals("helpyellow"))    return ARG_COLOR_HELPYELLOW;    
        else if (s.equals("blue"))          return ARG_COLOR_BLUE;	
        else if (s.equals("green"))         return ARG_COLOR_GREEN;	
        else if (s.equals("cyan"))          return ARG_COLOR_CYAN;	
        else if (s.equals("control"))       return ARG_COLOR_CONTROL;	
        else if (s.equals("controlShadow")) return ARG_COLOR_CONTROL_SHADOW;	
        else if (s.equals("desktop"))       return ARG_COLOR_DESKTOP;	
        else return new Object[] { Color.decode(s) };
    }
    private Object[] argDouble(String s) {
      	s = s.toUpperCase();
        return new Object[] { Double.valueOf(s) };      
    }   
    private ImageIcon argImageIcon(String s) {
        return ImageIconCache.getImageIcon(s);
    }

    /**
     * Add the given GuiElement to this one. In the general case, simply
     * calls the <code>add</code> method of the component contained in
     * this GuiElement, passing it the component contained in the given
     * GuiElement.
     * <p>
     * There are a number of special cases where the GuiElement being
     * added is intended to set an attribute of the component rather than
     * be contained by it. These are identified by the element's XML tag.
     *
     * @param g the GuiElement to be added to this one.
     * @param tag the XML tag of GuiElement g.
     */
    public void add(GuiElement g, String tag) {
      	AbstractButton ab;
      	JLabel jl;
      	JTextArea jta;
      	JTextComponent jtc;
      	JList jlist;
      	JComboBox jcb;
      	DefaultListModel dlmodel;
      	DefaultComboBoxModel dcbmodel;
        HelpBox helpBox;

        try {
            if (tag.equals("MNEMONIC")) {	
                if (c instanceof AbstractButton) {
                    ab = (AbstractButton) c;
                    ab.setMnemonic(g.getChar());
                }
                if (c instanceof JLabel) {
                    jl = (JLabel) c;
                    jl.setDisplayedMnemonic(g.getChar());
                }
                else if (c instanceof JTextComponent) {
                    jtc = (JTextComponent) c;
                    jtc.setFocusAccelerator(g.getChar());
                }
            }
            else if (tag.equals("TEXT")) {	
                if (c instanceof AbstractButton) {
                    ab = (AbstractButton) c;
                    ab.setText(g.getString());
                }
                else if (c instanceof JLabel) {
                    jl = (JLabel) c;
                    jl.setText(g.getString());
                }
                else if (c instanceof JTextArea) {
                    jta = (JTextArea) c;
                    jta.append(g.getString());
                }
                else if (c instanceof JTextComponent) {
                    jtc = (JTextComponent) c;
                    jtc.setText(g.getString());
                }
                else if (c instanceof JList) {
                    jlist = (JList) c;
                    dlmodel = (DefaultListModel) jlist.getModel();
                    dlmodel.addElement(g.getString());
                }
                else if (c instanceof JComboBox) {
                    jcb = (JComboBox) c;
                    dcbmodel = (DefaultComboBoxModel) jcb.getModel();
                    dcbmodel.addElement(g.getString());
                }
                else if (c instanceof HelpBox) {
                    helpBox = (HelpBox) c;
                    helpBox.addLabel(g.getString());
                }
                else if (c instanceof JPanel) {
                    tabName = g.getString();
                }
            }
            //DW42 -Renuka- If the tag is a PROP get the text from properties file.
            else if (tag.equals("PROP")) {     
                if (c instanceof AbstractButton) {
                    ab = (AbstractButton) c;
                    ab.setText(Messages.getString(g.getString()));
                    if (c instanceof DWButton) {
                    	((DWButton) c).setTextProperty(g.getString());
                    }
                } 
                else if (c instanceof JLabel) {
					jl = (JLabel) c;
					if (c instanceof HelpText) {
						((HelpText) c).setHelpText(Messages.getString(g.getString()));
					} else {
						jl.setText(Messages.getString(g.getString()));
						if (c instanceof DWLabel) {
							((DWLabel) c).setTextProperty(g.getString());
						}
					}
                } else if (c instanceof DWTextPane) {
               	 	((DWTextPane) c).setText(Messages.getString(g.getString()));
                
                } else if (c instanceof JTextArea) {
                     jta = (JTextArea) c;
                     jta.setText(Messages.getString(g.getString()));
                 }
                 else if (c instanceof JTextComponent) {
                    jtc = (JTextComponent) c;
                    jtc.setText(Messages.getString(g.getString()));
                 }
                 else if (c instanceof JList) {
                    jlist = (JList) c;
                    dlmodel = (DefaultListModel) jlist.getModel();
                    dlmodel.addElement(Messages.getString(g.getString()));
                 }
                 else if (c instanceof JComboBox) {
                     jcb = (JComboBox) c;
                     dcbmodel = (DefaultComboBoxModel) jcb.getModel();
                     dcbmodel.addElement(Messages.getString(g.getString()));
                 }
                else if (c instanceof HelpBox) {
                   helpBox = (HelpBox) c;
                   helpBox.addLabel(Messages.getString(g.getString()));
                }
                 else if (c instanceof JPanel) {
                    tabName = Messages.getString(g.getString());
                 }
            } else if (tag.equals("PLACEHOLDER")) {     
				if (c instanceof DWTextField) {
					DWTextField dwTf = (DWTextField) c;
					dwTf.setPlaceholder(Messages.getString(g.getString()), DWTextField.GRAY_PLACEHOLDER_COLOR);
				}                
            } 
            else if (tag.equals("BEVELBORDER")) {	
                c.setBorder(BorderFactory.createLoweredBevelBorder());
            }
            else if (tag.equals("LINEBORDER")) {	
                c.setBorder(BorderFactory.createLineBorder(Color.black));
            }else if (tag.equals("GREYBORDER")) {	
                c.setBorder(BorderFactory.createLineBorder(SystemColor.controlShadow));
            }
            else if (tag.equals("TITLEDBORDER")) {	
                c.setBorder(BorderFactory.createCompoundBorder(
                    BorderFactory.createTitledBorder(
                        (g.getString().isEmpty() ?  "" : Messages.getString(g.getString()))),
                    BorderFactory.createEmptyBorder(0,4,0,4)));
            }
            else if (tag.equals("TITLEDBORDERBOLD")) {	
            	TitledBorder title = BorderFactory.createTitledBorder(
                        (g.getString().isEmpty() ?  "" : Messages.getString(g.getString())));
            	title.setTitleFont(new Font("Serif", Font.BOLD,13));
                c.setBorder(BorderFactory.createCompoundBorder(title,                 
                    BorderFactory.createEmptyBorder(0,4,0,4)));
            } 
            else if (tag.equals("NULLBORDER")) {	
                c.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
            }
            else if (tag.equals("EMPTYBORDER")) {	
                c.setBorder(BorderFactory.createEmptyBorder(typicalSpacing,
                            typicalSpacing, typicalSpacing, typicalSpacing));
            }
            else if (tag.equals("BIGEMPTYBORDER")) {	
                c.setBorder(BorderFactory.createEmptyBorder(2 * typicalSpacing,
                            2 * typicalSpacing, 2 * typicalSpacing, 2 * typicalSpacing));
            }
            else if (tag.equals("TOOLTIP"))	
                c.setToolTipText(g.getString());
            else if (tag.equals("SIZE"))	
                c.setSize(g.getDimension());
            else if (tag.equals("MAXSIZE"))	
                c.setMaximumSize(g.getDimension());
            else if (tag.equals("MINSIZE"))	
                c.setMinimumSize(g.getDimension());
            else if (tag.equals("PREFSIZE"))	
                c.setPreferredSize(g.getDimension());
            else if (tag.equals("CENTER"))	
                c.add(g.getComponent(), BorderLayout.CENTER);
            else if (tag.equals("NORTH"))	
                c.add(g.getComponent(), BorderLayout.NORTH);
            else if (tag.equals("SOUTH"))	
                c.add(g.getComponent(), BorderLayout.SOUTH);
            else if (tag.equals("EAST"))	
                c.add(g.getComponent(), BorderLayout.EAST);
            else if (tag.equals("WEST"))	
                c.add(g.getComponent(), BorderLayout.WEST);
            else if (tag.equals("TOP"))	
                c.add(g.getComponent(), JSplitPane.TOP);
            else if (tag.equals("BOTTOM"))	
                c.add(g.getComponent(), JSplitPane.BOTTOM);
            else if (tag.equals("LEFT"))	
                c.add(g.getComponent(), JSplitPane.LEFT);
            else if (tag.equals("RIGHT"))	
                c.add(g.getComponent(), JSplitPane.RIGHT);
            else if (tag.equals("HGLUE"))	
                c.add(Box.createHorizontalGlue());
            else if (tag.equals("HSKIP"))	
                c.add(Box.createRigidArea(g.getDimension()));
            else if (tag.equals("HSTRUT"))	
                c.add(Box.createHorizontalStrut(g.getDimension().width()));
            else if (tag.equals("VGLUE"))	
                c.add(Box.createVerticalGlue());
            else if (tag.equals("VSKIP"))	
                c.add(Box.createRigidArea(g.getDimension()));
            else if (tag.equals("VSTRUT"))	
                c.add(Box.createVerticalStrut(g.getDimension().height()));
            else if (tag.equals("BIGVSKIP"))	
                c.add(Box.createRigidArea(g.getDimension()));
            else if (tag.equals("VIEWPORT")) {	
                JScrollPane jsp = (JScrollPane) c;
                jsp.getViewport().setView(g.getComponent());
            }
            else if (tag.equals("PROFILEITEM")) {	
                ProfilePanel pp = (ProfilePanel) g.getComponent();
                if (! pp.isHidden()) {
                    c.add(g.getComponent());
                }
            }
            else if (tag.equals("COMPACTGRID")) {
                ((CompactGrid) g).makeCompactGrid(c);
            } 
            else if (tag.equals("GRIDBAGCONSTRAINT")) {
            	gConstraint=g.getGridConstraint();
            }
            else if (c == null) {
                c = g.getComponent();
            }
            else if (c instanceof JTabbedPane) {
                c.add(g.getComponent(), g.getTabName());
            }
            else if ((c instanceof JScrollPane) && (g.getComponent() != null)
                    && (g.getComponent() instanceof Scrollable)) {
                        JScrollPane jsp = (JScrollPane) c;
                        jsp.getViewport().setView(g.getComponent());
            }       
            else if (g.getComponent() != null) {
				if (c instanceof JPanel) {
					if (c.getLayout() instanceof GridBagLayout) {
						c.add(g.getComponent(), g.getGridConstraint());
					} else {
						c.add(g.getComponent());
					}
				} else {
					c.add(g.getComponent());
				}
            }
        }
        catch (Exception e) {
            Debug.println("Adding " + tag + " "	 
                    + g.toString() + " to " + this.toString()); 
            Debug.printStackTrace(e);
            //System.exit(0);
        }
    }
    
    /**
     * Produces a string representation of this GuiElement.
     */
    @Override
	public String toString() {
        StringBuffer result = new StringBuffer();
        
        result.append("[");	
        
        if (c == null)
            result.append("null"); 
        else
            result.append(c.getClass().getName());
        
        result.append(",");	
        result.append(sb);
        result.append(",");	
        result.append(gConstraint);
        result.append("]");	
        
      	return result.toString();
    }

    /**
     * Creates a GuiElement appropriate for the given XML tag.
     * Factory method.
     */
    public static GuiElement create(String tag) {
        if (tag.equals("GUI"))	
            return new GuiElement(null, null);
        else if (tag.equals("BEVELBORDER"))	
            return new GuiElement(null, null);
        else if (tag.equals("BIGEMPTYBORDER"))	
            return new GuiElement(null, null);
        else if (tag.equals("BIGLABEL")) {	
            JLabel jl = new DWLabel();
            jl.setFont(BIG_FONT);
            jl.setMaximumSize(tfMaximum);
            jl.setHorizontalTextPosition(SwingConstants.LEFT);
            jl.setAlignmentX(Component.LEFT_ALIGNMENT);
            return new GuiElement(jl, null);
        }
        else if (tag.equals("BIGBOLDLABEL")) {	
            JLabel jl = new DWLabel();
            jl.setFont(BIG_FONT);
            return new GuiElement(jl, null);
        }
        else if (tag.equals("BIGVSKIP"))	
            return new GuiElement(null, null, new SettableDimension(0, 23));
        else if (tag.equals("BOLDLABEL")) {	
            JLabel label = new DWLabel();
            label.setFont(BOLD_FONT);
            return new GuiElement(label, null);
        }
        else if (tag.equals("SIZEDBOLDLABEL")) {	
            JLabel label = new DWLabel();
            label.setFont(BOLD_FONT);
            label.setMaximumSize(tfPreferred);
            return new GuiElement(label, null);
        }
        else if (tag.equals("BORDERLAYOUT"))	
            return new GuiElement(new JPanel(new BorderLayout()), null);
        else if (tag.equals("BUTTON"))	
            return new GuiElement(new DWButton(), null);
        else if (tag.equals("CARDLAYOUT"))	
            return new GuiElement(new JPanel(new CustomCardLayout()), null);
        else if (tag.equals("BANKCARDSWIPEFIELD")) {	
            BankCardSwipeTextField csf = new BankCardSwipeTextField();
            csf.setMinimumSize(csfMinimum);
            csf.setMaximumSize(csfMaximum);
            csf.setPreferredSize(csfPreferred);
            return new GuiElement(csf, null);
        }
        else if (tag.equals("CARDSWIPEFIELD")) {	
            CardSwipeTextField csf = new CardSwipeTextField();
            csf.setMinimumSize(csfMinimum);
            csf.setMaximumSize(csfMaximum);
            csf.setPreferredSize(csfPreferred);
            return new GuiElement(csf, null);
        }
        else if (tag.equals("MASKEDCARDSWIPEFIELD")) {	
            MaskedCardSwipeTextField csf = new MaskedCardSwipeTextField();
            csf.setMinimumSize(csfMinimum);
            csf.setMaximumSize(csfMaximum);
            csf.setPreferredSize(csfPreferred);
            return new GuiElement(csf, null);
        }
        else if (tag.equals("CENTER"))	
            return new GuiElement(null, null);
        else if (tag.equals("CHECKBOX"))	
            return new GuiElement(new DWCheckBox(), null);
        else if (tag.equals("COMBOBOX")){	
            MultiListComboBox<? extends Object> mlcb = new MultiListComboBox<Object>();
            mlcb.setMinimumSize(tfMinimum);
            mlcb.setMaximumSize(tfMaximum);
            mlcb.setPreferredSize(tfPreferred);
            return new GuiElement(mlcb, null);
        }
        else if (tag.equals("STATEBOX")){	
        	StateListComboBox<CountrySubdivision> slcb = new StateListComboBox<CountrySubdivision>();
            slcb.setMinimumSize(tfMinimum);
            slcb.setMaximumSize(tfMaximum);
            slcb.setPreferredSize(tfPreferred);
            return new GuiElement(slcb, null);
        }
        else if (tag.equals("QDOBOX")) {
            QDOComboBox cb = new QDOComboBox();
            cb.setMinimumSize(tfMinimum);
            cb.setMaximumSize(tfMaximum);
            cb.setPreferredSize(tfPreferred);
            return new GuiElement(cb, null);
        }        
        else if (tag.equals("CITYPROVIDERBOX")){	
        	CityProviderNumberListComboBox slcb = new CityProviderNumberListComboBox();
            slcb.setMinimumSize(tfMinimum);
            slcb.setMaximumSize(tfMaximum);
            slcb.setPreferredSize(tfPreferred);
            return new GuiElement(slcb, null);
        }
        else if (tag.equals("CHECKBOXMENUITEM"))	
            return new GuiElement(new JCheckBoxMenuItem(), new StringBuffer());
        else if (tag.equals("COMMAND")) {	// a BUTTON with set sizes	
            JButton jb = new DWButton();
            if(Messages.getCurrentLocale().equals(Locale.ENGLISH)){
                jb.setPreferredSize(commandPreferred);
                jb.setMaximumSize(commandMaximum);
                jb.setHorizontalTextPosition(SwingConstants.RIGHT);
            }
            else{
                jb.setPreferredSize(commandPreferredEs);
                jb.setMaximumSize(commandMaximumEs);
                jb.setHorizontalTextPosition(SwingConstants.RIGHT);
            }    
            return new GuiElement(jb, null);
        }
        else if (tag.equals("COMPACTGRID")) {
            return new CompactGrid();
        }
        else if (tag.equals("COMPASS"))	
            return new GuiElement(new JPanel(new BorderLayout()), null);
        else if (tag.equals("DELIMITEDFIELD")) {    
            DelimitedTextField dtf = new DelimitedTextField();
            dtf.setMinimumSize(tfMinimum);
            dtf.setMaximumSize(tfMaximum);
            dtf.setPreferredSize(tfPreferred);
            return new GuiElement(dtf, null);
        }
        else if (tag.equals("DATEFIELD")) { 
            JTextField jtf = new DWDateField();
            jtf.setMinimumSize(tfMinimum);
            jtf.setMaximumSize(tfMaximum);
            jtf.setPreferredSize(tfPreferred);
            return new GuiElement(jtf, null);
        }
        else if (tag.equals("EAST")) {	
            return new GuiElement(null, null);
        
        } else if (tag.equals("TEXTPANE")) {
            return new GuiElement(new DWTextPane(), null);
		
        } else if (tag.equals("NULLBORDER")) {
            return new GuiElement(null, null);
            
        } else if (tag.equals("EMPTYBORDER"))	
            return new GuiElement(null, null);
        else if (tag.equals("EXPERTBUTTON")) {	// a COMMAND which looks like a BOLDLABEL	
            JButton jb = new DWButton();
            jb.setFont(BOLD_FONT);
            jb.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
            return new GuiElement(jb, null);
        }
		else if (tag.equals("FEEFIELD")) {	
			// a money field whose pref size is smaller but still common
			FeeField ff = new FeeField();
			ff.setMinimumSize(tfMinimum);
			ff.setMaximumSize(tfMaximum);
			ff.setPreferredSize(commandPreferred);
			return new GuiElement(ff, null);
		}
		else if (tag.equals("FILLER")) {
            Dimension d = new Dimension(1, 1);
			return new GuiElement(new Box.Filler(d, d, d), null);
		}
        else if (tag.equals("PAYEEFIELD")) {	
            PayeeField pf = new PayeeField();
            pf.setMinimumSize(tfMinimum);
            pf.setMaximumSize(tfMaximum);
            pf.setPreferredSize(commandPreferred);
            return new GuiElement(pf, null);
        }
        else if (tag.equals("FLOWLAYOUT"))	
            return new GuiElement(new JPanel(), null);
        else if (tag.equals("FLOWLEFT"))	
            return new GuiElement(
                    new JPanel(new FlowLayout(FlowLayout.LEFT)), null);
        else if (tag.equals("FLOWRIGHT"))	
            return new GuiElement(
                    new JPanel(new FlowLayout(FlowLayout.RIGHT)), null);
        else if (tag.equals("GRIDLAYOUT"))	
            return new GuiElement(new JPanel(new GridLayout()), null);
        else if (tag.equals("HBOX")) {	
            JPanel p = new JPanel();
            p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
            return new GuiElement(p, null);
        }
        else if (tag.equals("HELPBOX")) {   
            HelpBox h = new HelpBox();
            return new GuiElement(h, null);
        } else if (tag.equals("HELPTEXT")) {   
            HelpText h = new HelpText();
            return new GuiElement(h, null);
        }
        else if (tag.equals("INDICATIVEBOX")) {   
            IndicativeBox ib = new IndicativeBox();
            return new GuiElement(ib, null);
        }
        else if (tag.equals("HGLUE"))	
            return new GuiElement(null, null);
        else if (tag.equals("HSKIP"))	
            return new GuiElement(null, null, new SettableDimension(5, 0));
        else if (tag.equals("HSTRUT"))	
            return new GuiElement(null, null, new SettableDimension(0, 0));
        else if (tag.equals("LABEL"))	{
            JLabel label = new DWLabel();
            return new GuiElement(label, null);
        }
        else if (tag.equals("SIZEDLABEL"))	{
            JLabel label = new DWLabel();
           label.setMaximumSize(tfPreferred);
            return new GuiElement(label, null);
        }
        else if (tag.equals("LEFT"))	
            return new GuiElement(null, null);
        else if (tag.equals("LINEBORDER"))	
            return new GuiElement(null, null);
        else if (tag.equals("GREYBORDER"))	
            return new GuiElement(null, null);
        else if (tag.equals("LIST"))	
            return new GuiElement(new JList(new DefaultListModel()), null);
        else if (tag.equals("MAXSIZE"))	
            return new GuiElement(null, null, new SettableDimension());
        else if (tag.equals("MENU"))	
            return new GuiElement(new JMenu(), null);
        else if (tag.equals("MENUBAR"))	
            return new GuiElement(new JMenuBar(), null);
        else if (tag.equals("MENUITEM"))	
            return new GuiElement(new JMenuItem(), new StringBuffer());
        else if (tag.equals("MINSIZE"))	
            return new GuiElement(null, null, new SettableDimension());
        else if (tag.equals("MNEMONIC"))	
            return new GuiElement(null, new StringBuffer());
        else if (tag.equals("MONEYFIELD")) {	
            MoneyField mf = new MoneyField();
            mf.setMinimumSize(tfMinimum);
            mf.setMaximumSize(tfMaximum);
            mf.setPreferredSize(tfPreferred);
            return new GuiElement(mf, null);
        }
        else if (tag.equals("TABFREEMONEYFIELD")) {	
            MoneyField mf = new MoneyField(){
				private static final long serialVersionUID = 1L;

				/**
                 * Indicates whether this component can be traversed using
                 * Tab or Shift-Tab keyboard focus traversal 
                 */
//                public boolean isFocusTraversable(){
                @Override
				public boolean isFocusable(){
                    return false;
                }    
            };
            mf.setMinimumSize(tfMinimum);
            mf.setMaximumSize(tfMaximum);
            mf.setPreferredSize(tfPreferred);
            return new GuiElement(mf, null);
        }
        else if (tag.equals("NORTH"))	
            return new GuiElement(null, null);
        else if (tag.equals("PANEL"))	
            return new GuiElement(new JPanel(), null);
        else if (tag.equals("PARAM"))	
            return new GuiElement(null, new StringBuffer());
        else if (tag.equals("PREFSIZE")){	
            GuiElement ge = new GuiElement(null, null, new SettableDimension());
            return ge;
        }
        else if (tag.equals("PASSWORDFIELD")) {	
            JPasswordField jpwf = new DWPasswordField();
            jpwf.setMinimumSize(tfMinimum);
            jpwf.setMaximumSize(tfMaximum);
            jpwf.setPreferredSize(tfPreferred);
            return new GuiElement(jpwf, null);
        }
        else if (tag.equals("PROFILEITEM")) { // HBOX linked to profile	
            JPanel p = new ProfilePanel();
            p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
            return new GuiElement(p, null);
        }
        else if (tag.equals("PROFILETABLE")) { // VBOX linked to profile	
            JPanel p = new ProfilePanel();
            p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
            return new GuiElement(p, null);
        }
        else if (tag.equals("PROGRESSBAR"))	
            return new GuiElement(
                    new JProgressBar(SwingConstants.HORIZONTAL), null);
        else if (tag.equals("RADIOBUTTON")) {	
            JRadioButton rb = new DWRadioButton();
            if (buttongroup == null)
                buttongroup = new ButtonGroup();
            buttongroup.add(rb);
            return new GuiElement(rb, null);
        }
        else if (tag.equals("READONLY"))	
            return new GuiElement(null, null);
        else if (tag.equals("RIGHT"))	
            return new GuiElement(null, null);
        else if (tag.equals("SCROLLBAR"))	
            return new GuiElement(new JScrollBar(Adjustable.VERTICAL), null);
        else if (tag.equals("SCROLLPANE"))	
            return new GuiElement(new DWScrollPane(), null);
        else if (tag.equals("SEPARATOR")) {	
            if (buttongroup != null)
                buttongroup = new ButtonGroup();
            return new GuiElement(new JSeparator(), null);
        }
        else if (tag.equals("SIZE"))	
            return new GuiElement(null, null, new SettableDimension());
        else if (tag.equals("SLIDER"))	
            return new GuiElement(new JSlider(SwingConstants.HORIZONTAL), null);
        else if (tag.equals("SOUTH"))	
            return new GuiElement(null, null);
        else if (tag.equals("SPLITPANE"))	
            return new GuiElement(new JSplitPane(), null);
        else if (tag.equals("SPRINGLAYOUT"))
            return new GuiElement(new JPanel(new SpringLayout()), null);
        else if (tag.equals("TAB"))	
            return new GuiElement(new JPanel(new BorderLayout()), null);
        else if (tag.equals("TABBEDPANE"))	
            return new GuiElement(new JTabbedPane(), null);
        else if (tag.equals("TABLE"))	
            return new GuiElement(new DWTable(), null);
        else if (tag.equals("TABLEHEADER"))	
            return new GuiElement(new JTableHeader(), null);
        else if (tag.equals("TEXT"))	
            return new GuiElement(null, new StringBuffer());
        else if (tag.equals("PROP"))    
            return new GuiElement(null, new StringBuffer());
        else if (tag.equals("PLACEHOLDER"))    
            return new GuiElement(null, new StringBuffer()); 
        else if (tag.equals("BOLD"))	
            return new GuiElement(null, new StringBuffer());
        else if (tag.equals("TEXTAREA"))	
            return new GuiElement(new JTextArea(), null);
        else if (tag.equals("MESSAGETEXTAREA")) {    
            JTextArea jta = new JTextArea();
            jta.setLineWrap(true);
            jta.setWrapStyleWord(true);
            jta.setFocusable(false);
            return new GuiElement(jta, null);
        }
        else if (tag.equals("TEXTFIELD")) {	
            JTextField jtf = new DWTextField();
            jtf.setMinimumSize(tfMinimum);
            jtf.setMaximumSize(tfMaximum);
            jtf.setPreferredSize(tfPreferred);
            return new GuiElement(jtf, null);
        }
        else if (tag.equals("TITLEDBORDER"))	
            return new GuiElement(null, new StringBuffer());
        else if (tag.equals("TITLEDBORDERBOLD"))	
            return new GuiElement(null, new StringBuffer());
        else if (tag.equals("TOGGLEBUTTON"))	
            return new GuiElement(new JToggleButton(), null);
        else if (tag.equals("TOOLTIP"))	
            return new GuiElement(null, new StringBuffer());
        else if (tag.equals("VBOX")) {	
            JPanel p = new JPanel();
            p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
            return new GuiElement(p, null);
        }
        else if (tag.equals("VBUTTON")) {	// a BUTTON with VBOX layout	
            JButton jb = new DWButton();
            jb.setLayout(new BoxLayout(jb, BoxLayout.Y_AXIS));
            return new GuiElement(jb, null);
        }
        else if (tag.equals("VGLUE"))	
            return new GuiElement(null, null);
        else if (tag.equals("VSKIP"))	
            return new GuiElement(null, null, new SettableDimension(0, 5));
        else if (tag.equals("VSTRUT"))	
            return new GuiElement(null, null, new SettableDimension(0, 0));
        else if (tag.equals("VIEWPORT"))	
            return new GuiElement(null, null);
        else if (tag.equals("WEST"))	
            return new GuiElement(null, null);
        else if (tag.equals("GRIDBAGCONSTRAINT")){	
        	SettableGridConstraint c = new SettableGridConstraint();
            GuiElement ge = new GuiElement(null, null, c);
            return ge;
        }else if (tag.equals("GRIDBAGLAYOUT"))	
            return new GuiElement(new JPanel(new GridBagLayout()), null);
        else {
            Debug.println("Warning: unknown tag " + tag);	
            return new GuiElement(null, null);
        }
    }
}
