/*
 * ImageIconCache.java
 *
 * Copyright (c) 2003 MoneyGram, Inc.
 *
 */

package dw.gui;

import java.net.URL;
import java.util.TreeMap;

import javax.swing.ImageIcon;

/** 
 * Keeps a reference to every graphic we use so we aren't constantly
 * classloading the GIFs from the jar file and decoding them.
 */
public class ImageIconCache {
    private static TreeMap<String, ImageIcon> cache = new TreeMap<String, ImageIcon>();

    /** 
     * Get the icon with the given name. If it is in the cache,
     * return a reference to it. If the name is known to the
     * class loader as a resource (like in a jar file) get it
     * from there. Otherwise treat the name as a filename.
     */ 
    public static ImageIcon getImageIcon(String name) {
        ImageIcon result;
        if (cache.containsKey(name)) {
            result = cache.get(name);
        }
        else {
            URL url = ClassLoader.getSystemResource(name);
            if (url != null)
                result = new ImageIcon(url);
            else
                result = new ImageIcon(name);

            cache.put(name, result);
        }

        return result;
    }
}

