/*
 * SettableDimension.java
 *
 * Copyright (c) 1999-2002 MoneyGram, Inc.
 *
 */

package dw.gui;

import java.awt.Dimension;

/**
 *  Allows XML specifications to use width and height as attributes. 
 *  This class is non-obfuscatable due to the use of reflection, but we 
 *  don't need to implement the NonObfuscatable marker interface because
 *  java.awt.Dimension is serializable anyway.
 */
public class SettableDimension extends Dimension {
	private static final long serialVersionUID = 1L;

	public SettableDimension() {
        super();
    }
    
    public SettableDimension(int w, int h) {
        super(w, h);
    }
    
    public void setWidth(int width) {
        this.setSize(width, this.height);
    }
    
    public void setHeight(int height) {
        this.setSize(this.width, height);
    }

    public int width() {
        return this.width;
    }

    public int height() {
        return this.height;
    }   
}

