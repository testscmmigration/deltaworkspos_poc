/*                                          
 * GuiException.java
 *
 * Copyright (c) 1999-2002 MoneyGram, Inc.
 *
 */

package dw.gui;

/** 
 * Indicates an error reading or parsing an XML file describing a GUI.
 * GuiExceptions will generally be unrecoverable in production, but may
 * be informative in development.
 */
public class GuiException extends Exception {
	private static final long serialVersionUID = 1L;

	private Exception embeddedException;

    /**
     * Create a GuiException wrapping an existing exception.
     */
    public GuiException(String message, Exception e) {
	      super(message);
	      embeddedException = e;
    }

    /**
     * Create a GuiException wrapping an existing exception.
     */
    public GuiException(Exception e) {
      	super();
      	embeddedException = e;
    }

    /**
     * Create a GuiException.
     */
    public GuiException(String message) {
      	super(message);
      	embeddedException = null;
    }

    /**
     * Return the embedded exception, if any.
     */
    public Exception getException() {
      	return embeddedException;
    }
}

