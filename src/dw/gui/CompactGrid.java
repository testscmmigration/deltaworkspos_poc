/*
 * CompactGrid.java
 * 
 * $Revision$
 *
 * Copyright (c) 2005 MoneyGram International
 * 
 * This file includes code derived from a portion of SpringUtilities, 
 * which is a code example in The Java Tutorial, a free online resource 
 * on Sun's web site. That tutorial is Copyright (c) 1995-2004 Sun 
 * Microsystems, Inc. The limited use of that material here is consistent 
 * with the fair and intended use of such code examples.  
 */
package dw.gui;

import java.awt.Component;
import java.awt.Container;

import javax.swing.Spring;
import javax.swing.SpringLayout;

import dw.utility.Debug;

/**
 * Layout utility for SpringLayout. 
 */
public class CompactGrid extends GuiElement {
    
    /** Number of rows in the grid. */
    int rows;

    /** Number of columns in the grid. */
    int cols;

    /** Starting X location. */
    int initX;

    /** Starting Y location. */
    int initY;

    /** Horizontal padding. */
    int xPad = 5;

    /** Vertical padding. */
    int yPad = 5;

    public void setCols(int cols) {
        this.cols = cols;
    }

    public void setInitX(int initialX) {
        this.initX = initialX;
    }

    public void setInitY(int initialY) {
        this.initY = initialY;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public void setXPad(int pad) {
        xPad = pad;
    }

    public void setYPad(int pad) {
        yPad = pad;
    }
    
    /** Overrides GuiElement. */
    @Override
	public void setProperty(String property, String value) {
        setProperty(this, property, value);
    }
    
    /* Used by makeCompactGrid. */
    private static SpringLayout.Constraints getConstraintsForCell(int row,
            int col, Container parent, int cols) {
        SpringLayout layout = (SpringLayout) parent.getLayout();
        Component c = parent.getComponent(row * cols + col);
        return layout.getConstraints(c);
    }

    /**
     * Aligns the first <code>rows</code> * <code>cols</code>
     * components of <code>parent</code> in
     * a grid. Each component in a column is as wide as the maximum
     * preferred width of the components in that column;
     * height is similarly determined for each row.
     * The parent is made just big enough to fit them all.
     */
    public void makeCompactGrid(Container parent) {
        //Debug.println("CompactGrid.makeCompactGrid() rows=" + rows + " cols=" + cols);
        SpringLayout layout;
        try {
            layout = (SpringLayout) parent.getLayout();
        }
        catch (ClassCastException exc) {
            Debug.println("makeCompactGrid requires SpringLayout.");
            return;
        }

        //Align all cells in each column and make them the same width.
        Spring x = Spring.constant(initX);
        for (int c = 0; c < cols; c++) {
            Spring width = Spring.constant(0);
            for (int r = 0; r < rows; r++) {
                width = Spring.max(width, getConstraintsForCell(r, c, parent,
                    cols).getWidth());
            }
            for (int r = 0; r < rows; r++) {
                SpringLayout.Constraints constraints = getConstraintsForCell(r,
                    c, parent, cols);
                constraints.setX(x);
                constraints.setWidth(width);
            }
            x = Spring.sum(x, Spring.sum(width, Spring.constant(xPad)));
        }

        //Align all cells in each row and make them the same height.
        Spring y = Spring.constant(initY);
        for (int r = 0; r < rows; r++) {
            Spring height = Spring.constant(0);
            for (int c = 0; c < cols; c++) {
                height = Spring.max(height, getConstraintsForCell(r, c, parent,
                    cols).getHeight());
            }
            for (int c = 0; c < cols; c++) {
                SpringLayout.Constraints constraints = getConstraintsForCell(r,
                    c, parent, cols);
                constraints.setY(y);
                constraints.setHeight(height);
            }
            y = Spring.sum(y, Spring.sum(height, Spring.constant(yPad)));
        }

        //Set the parent's size.
        SpringLayout.Constraints pCons = layout.getConstraints(parent);
        
        // I want xPad and yPad to only apply between components, not between
        // last component and edge of layout panel, so remove extra padding.
                
        x = Spring.sum(x, Spring.minus(Spring.constant(xPad)));
        y = Spring.sum(y, Spring.minus(Spring.constant(yPad)));
        
        pCons.setConstraint(SpringLayout.SOUTH, y);
        pCons.setConstraint(SpringLayout.EAST, x);
    }

}