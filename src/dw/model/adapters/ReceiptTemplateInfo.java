package dw.model.adapters;

import java.util.ArrayList;
import java.util.List;

public class ReceiptTemplateInfo {
//	private String language;
	private String mD5;
	private String type;
	private String version;
	private String longLanguageCode;
	private List<String> additionalLanguages;

	public void setLanguage(String s) {
		if (s.length() == 3) {
			setLongLanguageCode(s);
		} else {
			
			setLongLanguageCode( s.substring(0, 3));
			getAdditionalLanguages().add(s.substring(4));
		}
	}

	/**
	 * @return the mD5
	 */
	public String getmD5() {
		return mD5;
	}

	/**
	 * @param mD5 the mD5 to set
	 */
	public void setmD5(String mD5) {
		this.mD5 = mD5;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * @return the longLanguageCode
	 */
	public String getLongLanguageCode() {
		return longLanguageCode;
	}

	/**
	 * @param longLanguageCode the longLanguageCode to set
	 */
	public void setLongLanguageCode(String longLanguageCode) {
		this.longLanguageCode = longLanguageCode;
	}

	/**
	 * @return the additionalLanguages
	 */
	public List<String> getAdditionalLanguages() {
		if(additionalLanguages == null) {
			additionalLanguages = new ArrayList<String>();
		}
		return additionalLanguages;
	}

	/**
	 * @param additionalLanguages the additionalLanguages to set
	 */
	public void setAdditionalLanguages(List<String> additionalLanguages) {
		this.additionalLanguages = additionalLanguages;
	}

//	/**
//	 * @return the language
//	 */
//	public String getLanguage() {
//		return language;
//	}
}
