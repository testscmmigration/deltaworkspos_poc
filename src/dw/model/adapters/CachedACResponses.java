package dw.model.adapters;

import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.moneygram.agentconnect.BusinessError;
import com.moneygram.agentconnect.CreateOrUpdateProfileReceiverResponse;
import com.moneygram.agentconnect.CreateOrUpdateProfileSenderResponse;
import com.moneygram.agentconnect.InfoBase;
import com.moneygram.agentconnect.SearchConsumerProfilesResponse;
import com.moneygram.agentconnect.Response;

import dw.comm.AgentConnect;
import dw.comm.MessageFacade;
import dw.dialogs.Dialogs;
import dw.profile.UnitProfile;
import dw.utility.Debug;
import dw.utility.DwEncryptionAES;
import dw.utility.TimeUtility;

public class CachedACResponses implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private static final String OBJECT_FILE_NAME = "car.obj";
	private static final long UPDATE_PERIOD = TimeUtility.HOUR * 23;

	private long lastTimeUpdated;
	private SearchConsumerProfilesResponse searchConsumerProfilesResponse;
	private CreateOrUpdateProfileSenderResponse createOrUpdateProfileSenderResponse;
	private CreateOrUpdateProfileReceiverResponse createOrUpdateProfileReceiverResponse;
	
	private static CachedACResponses getCurrentObjectFile(boolean handleError) {
		CachedACResponses car = null;
		try {
			DwEncryptionAES oe = new DwEncryptionAES();
			String language = UnitProfile.getInstance().get("LANGUAGE", "en");
			oe.vInitialize(DwEncryptionAES.OFFSET_CACHED_AC_RESPONSES, OBJECT_FILE_NAME);
			Map<String, CachedACResponses> map = (Map<String, CachedACResponses>) oe.readObject();
			car = (CachedACResponses) map.get(language);
		} catch (Exception e) {
			Debug.printException(e);
			deleteObjectFile();
			if (handleError) {
				UnitProfile.getInstance().setForceTransmit(true);
				Dialogs.showRequest("dialogs/DialogCannotCreateUpdateCustomerInformation.xml");
			}
		}
		return car;
	}

	public static SearchConsumerProfilesResponse getSearchConsumerProfilesResponse() {
		CachedACResponses car = getCurrentObjectFile(true);
		return car != null ? car.searchConsumerProfilesResponse : null;
	}
	
	public static CreateOrUpdateProfileSenderResponse getCreateOrUpdateProfileSenderResponse() {
		CachedACResponses car = getCurrentObjectFile(true);
		return car != null ? car.createOrUpdateProfileSenderResponse : null;
	}
	
	public static CreateOrUpdateProfileReceiverResponse getCreateOrUpdateProfileReceiverResponse() {
		CachedACResponses car = getCurrentObjectFile(true);
		return car != null ? car.createOrUpdateProfileReceiverResponse : null;
	}

	public static void cacheACResponses(boolean unconditional) {
		CachedACResponses car = null;
		DwEncryptionAES oe = new DwEncryptionAES();
		String language = UnitProfile.getInstance().get("LANGUAGE", "en");
		oe.vInitialize(DwEncryptionAES.OFFSET_CACHED_AC_RESPONSES, OBJECT_FILE_NAME);
		Map<String, CachedACResponses> map = null;
		try {
			map = (Map<String, CachedACResponses>) oe.readObject();
		} catch (Exception e) {
			map = new HashMap<String, CachedACResponses>();
		}
		car = (CachedACResponses) map.get(language);
		if (car == null) {
			car = new CachedACResponses();
			map.put(language, car);
		}
		
		// Check if cached responses are newer than the update period.
		
		long time = System.currentTimeMillis() - car.lastTimeUpdated;
		if ((time < UPDATE_PERIOD) && (car.searchConsumerProfilesResponse != null) && (car.createOrUpdateProfileSenderResponse != null) && (car.createOrUpdateProfileReceiverResponse != null)) {
			return;
		}
		
		// Get new AC responses and save then in the cached AC responses file.
		
		try {
			boolean searchUpdated = false;
			boolean senderUpdated = false;
			boolean receiverUpdated = false;
			
			try {
				SearchConsumerProfilesResponse searchConsumerProfilesResponse = MessageFacade.getInstance().searchConsumerProfiles(null, AgentConnect.ALWAYS_LIVE, null);
				if ((searchConsumerProfilesResponse != null) && (searchConsumerProfilesResponse.getPayload() != null) && (searchConsumerProfilesResponse.getPayload().getValue().getFieldsToCollect() != null)) {
					List<InfoBase> ftc = searchConsumerProfilesResponse.getPayload().getValue().getFieldsToCollect().getFieldToCollect();
					if ((! ftc.isEmpty()) && ((! hasErrors(searchConsumerProfilesResponse)))) {
						car.searchConsumerProfilesResponse = searchConsumerProfilesResponse;
						searchUpdated = true;
					}
				}
			} catch (Exception e) {
				Debug.printException(e);
			}
			
			try {
				CreateOrUpdateProfileSenderResponse createOrUpdateProfileSenderResponse = MessageFacade.getInstance().createOrUpdateProfileSender(new ConsumerProfile("UCPID"), null, false);
				if ((createOrUpdateProfileSenderResponse != null) && (createOrUpdateProfileSenderResponse.getPayload() != null) && (createOrUpdateProfileSenderResponse.getPayload().getValue().getFieldsToCollect() != null)) {
					List<InfoBase> ftc = createOrUpdateProfileSenderResponse.getPayload().getValue().getFieldsToCollect().getFieldToCollect();
					if ((! ftc.isEmpty()) && ((! hasErrors(createOrUpdateProfileSenderResponse)))) {
						car.createOrUpdateProfileSenderResponse = createOrUpdateProfileSenderResponse;
						senderUpdated = true;
					}
				}
			} catch (Exception e) {
				Debug.printException(e);
			}
			
			try {
				CreateOrUpdateProfileReceiverResponse createOrUpdateProfileReceiverResponse = MessageFacade.getInstance().createOrUpdateProfileReceiver(new ConsumerProfile("UCPID"), null, false);
				if ((createOrUpdateProfileReceiverResponse != null) && (createOrUpdateProfileReceiverResponse.getPayload() != null) && (createOrUpdateProfileReceiverResponse.getPayload().getValue().getFieldsToCollect() != null)) {
					List<InfoBase> ftc = createOrUpdateProfileReceiverResponse.getPayload().getValue().getFieldsToCollect().getFieldToCollect();
					if ((! ftc.isEmpty()) && ((! hasErrors(createOrUpdateProfileReceiverResponse)))) {
						car.createOrUpdateProfileReceiverResponse = createOrUpdateProfileReceiverResponse;
						receiverUpdated = true;
					}
				}
			} catch (Exception e) {
				Debug.printException(e);
			}
			
			if (searchUpdated && senderUpdated && receiverUpdated) {
				car.lastTimeUpdated = System.currentTimeMillis();
			}
			
			oe.vWriteSaveFile(map);
		} catch (Exception e) {
			Debug.printException(e);
		}
	}
	
	public static boolean hasErrors(Response response) {
		return (! ((response.getErrors() == null) || (response.getErrors().getError().isEmpty())));
	}
	
	public static void deleteObjectFile() {
		DwEncryptionAES obj = new DwEncryptionAES();
		File file = new File(OBJECT_FILE_NAME);
		obj.deleteFile(file, null, null);
	}
	
	private static boolean isSearchConsumerPrilesResponseInvalid(CachedACResponses car) {
		SearchConsumerProfilesResponse response = car != null ? car.searchConsumerProfilesResponse : null;
		SearchConsumerProfilesResponse.Payload payload =  ((response != null) && (response.getPayload() != null)) ? response.getPayload().getValue() : null;
		List<InfoBase> list1 = (payload != null && payload.getFieldsToCollect() != null) ? payload.getFieldsToCollect().getFieldToCollect() : null;
		if ((list1 == null) || (list1.isEmpty())) {
			return true;
		}
		List<BusinessError> list2 = ((response != null) && (response.getErrors() != null)) ? response.getErrors().getError() : null;
		if ((list2 != null) && (! list2.isEmpty())) {
			return true;
		}
		
		return false;
	}
	
	public static boolean isSendResponsesInvalid() {
		CachedACResponses car = getCurrentObjectFile(false);
		
		if (isSearchConsumerPrilesResponseInvalid(car)) {
			return true;
		}
		
		CreateOrUpdateProfileSenderResponse response = car != null ? car.createOrUpdateProfileSenderResponse : null;
		CreateOrUpdateProfileSenderResponse.Payload payload =  ((response != null) && (response.getPayload() != null)) ? response.getPayload().getValue() : null;
		List<InfoBase> list1 = (payload != null && payload.getFieldsToCollect() != null) ? payload.getFieldsToCollect().getFieldToCollect() : null;
		if ((list1 == null) || (list1.isEmpty())) {
			return true;
		}
		List<BusinessError> list2 = ((response != null) && (response.getErrors() != null)) ? response.getErrors().getError() : null;
		if ((list2 != null) && (! list2.isEmpty())) {
			return true;
		}
		
		return false;
	}
	
	public static boolean isReceiveResponsesInvalid() {
		CachedACResponses car = getCurrentObjectFile(false);
		
		if (isSearchConsumerPrilesResponseInvalid(car)) {
			return true;
		}
		
		CreateOrUpdateProfileReceiverResponse response = car != null ? car.createOrUpdateProfileReceiverResponse : null;
		CreateOrUpdateProfileReceiverResponse.Payload payload =  ((response != null) && (response.getPayload() != null)) ? response.getPayload().getValue() : null;
		List<InfoBase> list1 = (payload != null && payload.getFieldsToCollect() != null) ? payload.getFieldsToCollect().getFieldToCollect() : null;
		if ((list1 == null) || (list1.isEmpty())) {
			return true;
		}
		List<BusinessError> list2 = ((response != null) && (response.getErrors() != null)) ? response.getErrors().getError() : null;
		if ((list2 != null) && (! list2.isEmpty())) {
			return true;
		}
		
		return false;
	}
}
