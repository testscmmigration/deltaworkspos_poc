package dw.model.adapters;

public enum SendReversalType {
    C,
    R;

    public String value() {
        return name();
    }

    public static SendReversalType fromValue(String v) {
        return valueOf(v);
    }
}
