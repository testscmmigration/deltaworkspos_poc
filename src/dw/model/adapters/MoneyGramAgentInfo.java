/*
 * MoneyGramAgentInfo.java
 *
 * Generated on Wed May 12 10:25:44 CDT 2004
 */

package dw.model.adapters;

import java.util.List;

import com.moneygram.agentconnect.AgentInfo;
import com.moneygram.agentconnect.StoreHourInfo;

public class MoneyGramAgentInfo {
	private String countrySubdivisionCode;
	private String city;
	private boolean receiveCapability;
	private String agentName;
	private boolean sendCapability;
	private String address;
	private String agentPhone;
	private List<StoreHourInfo> storeHours;

	public MoneyGramAgentInfo(AgentInfo agentInfo) {
		this.setCountrySubdivisionCode(agentInfo.getCountrySubdivisionCode());
		this.setCity(agentInfo.getCity());
		this.setReceiveCapability(agentInfo.isReceiveCapability());
		this.setAgentName(agentInfo.getAgentName());
		this.setSendCapability(agentInfo.isSendCapability());
		this.setAddress(agentInfo.getAddress());
		this.setAgentPhone(agentInfo.getAgentPhone());
		this.setStoreHours(agentInfo.getStoreHours());
	}

	/**
	 * Emulates the old behavior where the store hours information is mashed
	 * together into one big string. But slightly prettier.
	 */
	public String[] getStoreHrs() {
		String[] hoursArray = new String[7];
		if (storeHours != null) {
			int cnt = 0;
			for (StoreHourInfo storeHourInfo : storeHours) {
				try {
					StringBuffer sb = new StringBuffer();
					if (!storeHourInfo.isClosed()) {
						sb.append(storeHourInfo.getDayOfWeek());
						sb.append("     ");
						sb.append(storeHourInfo.getOpenTime().toString().substring(0, 5));
						sb.append("  -  ");
						sb.append(storeHourInfo.getCloseTime().toString().substring(0, 5));
						sb.append(" ");
					} else {
						sb.append(storeHourInfo.getDayOfWeek());
						sb.append("     ");
						sb.append("Closed");
						sb.append(" ");
					}

					hoursArray[cnt++] = sb.toString();
				} catch (IndexOutOfBoundsException e) {
					// ignore
				}
			}
		}
		return hoursArray;
	}

	/**
	 * @return the state
	 */
	public String getCountrySubdivisionCode() {
		return countrySubdivisionCode;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setCountrySubdivisionCode(String state) {
		this.countrySubdivisionCode = state;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the receiveCapability
	 */
	public boolean isReceiveCapability() {
		return receiveCapability;
	}

	/**
	 * @param receiveCapability
	 *            the receiveCapability to set
	 */
	public void setReceiveCapability(boolean receiveCapability) {
		this.receiveCapability = receiveCapability;
	}

	/**
	 * @return the agentName
	 */
	public String getAgentName() {
		return agentName;
	}

	/**
	 * @param agentName
	 *            the agentName to set
	 */
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	/**
	 * @return the sendCapability
	 */
	public boolean isSendCapability() {
		return sendCapability;
	}

	/**
	 * @param sendCapability
	 *            the sendCapability to set
	 */
	public void setSendCapability(boolean sendCapability) {
		this.sendCapability = sendCapability;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the agentPhone
	 */
	public String getAgentPhone() {
		return agentPhone;
	}

	/**
	 * @param agentPhone
	 *            the agentPhone to set
	 */
	public void setAgentPhone(String agentPhone) {
		this.agentPhone = agentPhone;
	}

	/**
	 * @return the storeHours
	 */
	public List<StoreHourInfo> getStoreHours() {
		return storeHours;
	}

	/**
	 * @param storeHours
	 *            the storeHours to set
	 */
	public void setStoreHours(List<StoreHourInfo> storeHours) {
		this.storeHours = storeHours;
	}
}
