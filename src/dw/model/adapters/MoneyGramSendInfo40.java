/*
 * MoneyGramSendInfo40.java
 */

package dw.model.adapters;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

import com.moneygram.agentconnect.KeyValuePairType;
import com.moneygram.agentconnect.ProductVariantType;

import dw.framework.DataCollectionSet;
import dw.framework.FieldKey;
import dw.model.adapters.CountryInfo.CountrySubdivision;
import dw.utility.ServiceOption;

/**
 * Adapter for both moneyGramSendRequest and billPaySendRequest. These were
 * covered by the same message in the Deltaworks API, (just as they are still
 * both MC04 in the mainframe) so our transaction logic expects a single class
 * to have all the request fields for both. Some fields pertain to only one or
 * the other, so not all combinations of field values are valid. The current
 * implementation of MessageFacade will silently drop fields that don't belong
 * in the outgoing request.
 */
public class MoneyGramSendInfo40 implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String account;
	private String activeFreqCustCardNumber;
	private String mgCustomerReceiveNumber;

	private BigDecimal amount;

	private String answer;

	private String bSAAccountNumber;

	private String cardExpDateMonth;
	private String cardExpDateYear;
	private boolean cardSwiped;

	private String confirmationNumber;

	private String consumerId;

	private String customerReceiveNumber;

	private String deliveryOption;

	private String deliveryOptionDisplayName;

	private String deliveryOptionID;

	private String destinationCountry;

	private CountrySubdivision destinationState;

	private String direction1;
	private String direction2;

	private String direction3;

	private String[] enteredPromoArray;

	private String expectedReceiveAmount;

	private String freqCustCardNumber;
	private String message1;
	private String message2;
	private ProductVariantType productVariant;
	private String[] promoCodes;
	private ServiceOption qdo = null;
	private ServiceOption qualifiedDeliveryOption;
	private String question;
	private String receiveAgentID;
	private String receiveCountry;
	private String receiveCurrency;
	private ReceiverInfo receiver = null;
	private String receiverFirstName;
	private String receiverLastName;
	private String receiverMiddleName;
	private String registrationNumber;
	private String sendCurrency;
	private String senderAddress;
	private String senderAddress2;
	private String senderAddress3;
	private String senderAddress4;
	private String senderBankAccountNumber;
	private String senderCity;
	private String senderCountry;
	private String senderFirstName;
	private String senderHomePhone;
	private SenderInfo senderInfo = null;
	private String senderLastName;
	private String senderLastName2;
	private String senderMiddleName;
	private String senderState;
	private String senderZipCode;
	private String smsCountryDialCode;
	private String smsPhoneNumber;
	private PersonalInfo thirdParty = null;
	private String validateAccountNumber;

	/** Replaces setSender(new PersonalInfo()). */
	public void clearSender(DataCollectionSet gffpDataCollectionSet) {
		String s = "";
		setFreqCustCardNumber(null);
		setActiveFreqCustCardNumber(null);
		setConsumerId(null);
		setSenderFirstName(null);
		setSenderMiddleName(null);
		setSenderLastName(null);
		setSenderLastName2(null);
		setSenderAddress(null);
		setSenderAddress2(null);
		setSenderAddress3(null);
		setSenderAddress4(null);
		setSenderCity(null);
		setSenderState(null);
		setSenderZipCode(null);
		setSenderCountry(null);
		setSenderHomePhone(null);
		setRegistrationNumber(null);
		setReceiverFirstName(null);
		setReceiverMiddleName(null);
		setReceiverLastName(null);
		setSmsPhoneNumber(null);
		setSmsCountryDialCode(null);

		if (gffpDataCollectionSet != null) {
			gffpDataCollectionSet.setFieldValue(FieldKey.MGIREWARDSNUMBER_KEY, s);
			gffpDataCollectionSet.setFieldValue(FieldKey.ACTIVE_FREQ_CUST_CARD_NUMBER_KEY, s);
			gffpDataCollectionSet.setFieldValue(FieldKey.SEND_CONSUMERID_KEY, s); 
			gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_FIRSTNAME_KEY, s);
			gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_MIDDLENAME_KEY, s);
			gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_LASTNAME_KEY, s);
			gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_LASTNAME2_KEY, s);
			gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_ADDRESS_KEY, s);
			gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_ADDRESS2_KEY, s);
			gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_ADDRESS3_KEY, s);
			gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_ADDRESS4_KEY, s);
			gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_CITY_KEY, s);
			gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_COUNTRY_SUBDIVISIONCODE_KEY, s);
			gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_POSTALCODE_KEY, s);
			gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_COUNTRY_KEY, s);
			gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_PRIMARYPHONE_KEY, s);
			gffpDataCollectionSet.setFieldValue(FieldKey.REGISTRATION_NUMBER_KEY, s);
			gffpDataCollectionSet.setFieldValue(FieldKey.RECEIVER_FIRSTNAME_KEY, s);
			gffpDataCollectionSet.setFieldValue(FieldKey.RECEIVER_MIDDLENAME_KEY, s);
			gffpDataCollectionSet.setFieldValue(FieldKey.RECEIVER_LASTNAME_KEY, s);
			gffpDataCollectionSet.setFieldValue(FieldKey.SMS_PHONE_NUMBER_KEY, s); 
			gffpDataCollectionSet.setFieldValue(FieldKey.SMS_COUNTRY_DIAL_CODE_KEY, s);
		}
	}

	/**
	 * @return the account
	 */
	public String getAccount() {
		return account;
	}

	/**
	 * @return the activeFreqCustCardNumber
	 */
	private String getActiveFreqCustCardNumber() {
		return activeFreqCustCardNumber;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @return the answer
	 */
	public String getAnswer() {
		return answer;
	}

	/**
	 * @return the bSAAccountNumber
	 */
	public String getBSAAccountNumber() {
		return bSAAccountNumber;
	}

	/**
	 * @return the cardExpDateMonth
	 */
	public String getCardExpDateMonth() {
		return cardExpDateMonth;
	}

	/**
	 * @return the cardExpDateYear
	 */
	public String getCardExpDateYear() {
		return cardExpDateYear;
	}

	/**
	 * @return the confirmationNumber
	 */
	public String getConfirmationNumber() {
		return confirmationNumber;
	}

	/**
	 * @return the consumerId
	 */
	private String getConsumerId() {
		return consumerId;
	}

	/** Not used by DeltaWorks. */

	public String getCustomerReceiveNumber() {
		/*
		 * If not found as customerReciveNumber, check to see if stored as
		 * registrationNumber.
		 */
		if ((customerReceiveNumber == null) || (customerReceiveNumber.isEmpty())) {
			return registrationNumber;
		}
		return customerReceiveNumber;
	}

	/**
	 * @return the deliveryOption
	 */
	public String getDeliveryOption() {
		return deliveryOption;
	}

	/**
	 * @return the deliveryOptionDisplayName
	 */
	public String getDeliveryOptionDisplayName() {
		return deliveryOptionDisplayName;
	}

	/**
	 * @return the deliveryOptionID
	 */
	public String getDeliveryOptionID() {
		return deliveryOptionID;
	}

	/** Gets the country to which the transaction is being sent. */
	public String getDestinationCountry() {
		if (this.destinationCountry == null || (this.destinationCountry.isEmpty())) {
			return receiveCountry;
		}
		return this.destinationCountry;
	}

	/**
	 * @return the destinationState
	 */
	public CountrySubdivision getDestinationState() {
		return destinationState;
	}

	/**
	 * @return the direction1
	 */
	public String getDirection1() {
		return direction1;
	}

	/**
	 * @return the direction2
	 */
	public String getDirection2() {
		return direction2;
	}

	/**
	 * @return the direction3
	 */
	public String getDirection3() {
		return direction3;
	}

	/**
	 * @return the enteredPromoArray
	 */
	public String[] getEnteredPromoArray() {
		return enteredPromoArray;
	}

	/**
	 * @return the expectedReceiveAmount
	 */
	public String getExpectedReceiveAmount() {
		return expectedReceiveAmount;
	}

	/**
	 * @return the freqCustCardNumber
	 */
	private String getFreqCustCardNumber() {
		return freqCustCardNumber;
	}

	/**
	 * Returns the QDO data contained in this object. Since this class is an XML
	 * wrapper, the result is just another XML wrapper for some of the fields in
	 * this object.
	 */
	public ServiceOption getHistoryQDO() {
		return this.qdo;
	}

	/**
	 * @return the message1
	 */
	public String getMessage1() {
		return message1;
	}

	/**
	 * @return the message2
	 */
	public String getMessage2() {
		return message2;
	}

	/**
	 * @return the productVariant
	 */
	public ProductVariantType getProductVariant() {
		return productVariant;
	}

	/**
	 * @return the promoCodes
	 */
	public String[] getPromoCodes() {
		return promoCodes;
	}


	/**
	 * Returns the QDO data contained in this object. Since this class is an XML
	 * wrapper, the result is just another XML wrapper for some of the fields in
	 * this object.
	 */
	public ServiceOption getQualifiedDeliveryOption() {
		return qualifiedDeliveryOption;
	}

	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}

	/**
	 * @return the receiveAgentID
	 */
	public String getReceiveAgentID() {
		return receiveAgentID;
	}

	/**
	 * @return the receiveCurrency
	 */
	public String getReceiveCurrency() {
		return receiveCurrency;
	}

	/** Returns PersonalInfo view of receiver data. */
	public ReceiverInfo getReceiver() {
		if (receiver == null) {
			receiver = new ReceiverInfo();
		}
		return receiver;
	}

	/**
	 * @return the receiverFirstName
	 */
	public String getReceiverFirstName() {
		return receiverFirstName;
	}
private String purpose;
	/**
	 * @return the receiverLastName
	 */
	public String getReceiverLastName() {
		return receiverLastName;
	}

	/**
	 * @return the receiverMiddleName
	 */
	public String getReceiverMiddleName() {
		return receiverMiddleName;
	}

	/**
	 * @return the registrationNumber
	 */
	public String getRegistrationNumber() {
		return registrationNumber;
	}

	/**
	 * @return the sendCurrency
	 */
	public String getSendCurrency() {
		return sendCurrency;
	}

	/** Returns PersonalInfo view of sender data. */
	public SenderInfo getSender() {
		if (senderInfo == null) {
			senderInfo = new SenderInfo(new ArrayList<KeyValuePairType>());
		}
		return senderInfo;
	}

	/**
	 * @return the senderAddress
	 */
	private String getSenderAddress() {
		return senderAddress;
	}

	/**
	 * @return the senderAddress2
	 */
	private String getSenderAddress2() {
		return senderAddress2;
	}

	/**
	 * @return the senderAddress3
	 */
	private String getSenderAddress3() {
		return senderAddress3;
	}

	/**
	 * @return the senderAddress4
	 */
	private String getSenderAddress4() {
		return senderAddress4;
	}

	/**
	 * @return the senderBankAccountNumber
	 */
	public String getSenderBankAccountNumber() {
		return senderBankAccountNumber;
	}

	/**
	 * @return the senderCity
	 */
	private String getSenderCity() {
		return senderCity;
	}

	/**
	 * @return the senderCountry
	 */
	private String getSenderCountry() {
		return senderCountry;
	}

	/**
	 * @return the senderFirstName
	 */
	public String getSenderFirstName() {
		return senderFirstName;
	}

	/**
	 * @return the senderHomePhone
	 */
	private String getSenderHomePhone() {
		return senderHomePhone;
	}

	/**
	 * @return the senderLastName
	 */
	public String getSenderLastName() {
		return senderLastName;
	}

	/**
	 * @return the senderLastName2
	 */
	private String getSenderLastName2() {
		return senderLastName2;
	}

	/**
	 * @return the senderMiddleName
	 */
	public String getSenderMiddleName() {
		return senderMiddleName;
	}

	/**
	 * @return the senderState
	 */
	private String getSenderState() {
		return senderState;
	}

	/**
	 * @return the senderZipCode
	 */
	private String getSenderZipCode() {
		return senderZipCode;
	}

	/**
	 * @return the smsCountryDialCode
	 */
	private String getSmsCountryDialCode() {
		return smsCountryDialCode;
	}

	/**
	 * @return the smsPhoneNumber
	 */
	private String getSmsPhoneNumber() {
		return smsPhoneNumber;
	}

	/** Returns PersonalInfo view of third-party data. */
	public PersonalInfo getThirdParty() {
		if (thirdParty == null) {
			thirdParty = new PersonalInfo();
		}
		return thirdParty;
	}

	/**
	 * @return the validateAccountNumber
	 */
	public String getValidateAccountNumber() {
		return validateAccountNumber;
	}

	/**
	 * @return the cardSwiped
	 */
	public boolean isCardSwiped() {
		return cardSwiped;
	}

	/**
	 * @param account
	 *            the account to set
	 */
	public void setAccount(String account) {
		this.account = account;
	}

	/**
	 * @param activeFreqCustCardNumber
	 *            the activeFreqCustCardNumber to set
	 */
	private void setActiveFreqCustCardNumber(String activeFreqCustCardNumber) {
		this.activeFreqCustCardNumber = activeFreqCustCardNumber;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @param answer
	 *            the answer to set
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}

	/**
	 * @param cardExpDateMonth
	 *            the cardExpDateMonth to set
	 */
	public void setCardExpDateMonth(String cardExpDateMonth) {
		this.cardExpDateMonth = cardExpDateMonth;
	}

	/**
	 * @param cardExpDateYear
	 *            the cardExpDateYear to set
	 */
	public void setCardExpDateYear(String cardExpDateYear) {
		this.cardExpDateYear = cardExpDateYear;
	}

	/**
	 * @param cardSwiped
	 *            the cardSwiped to set
	 */
	public void setCardSwiped(boolean cardSwiped) {
		this.cardSwiped = cardSwiped;
	}

	/**
	 * @param confirmationNumber
	 *            the confirmationNumber to set
	 */
	public void setConfirmationNumber(String confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}

	/**
	 * @param consumerId
	 *            the consumerId to set
	 */
	private void setConsumerId(String consumerId) {
		this.consumerId = consumerId;
	}

	/**
	 * @param customerReceiveNumber
	 *            the customerReceiveNumber to set
	 */
	public void setCustomerReceiveNumber(String customerReceiveNumber) {
		this.customerReceiveNumber = customerReceiveNumber;
	}

	/**
	 * @param deliveryOption
	 *            the deliveryOption to set
	 */
	public void setDeliveryOption(String deliveryOption) {
		this.deliveryOption = deliveryOption;
	}

	/**
	 * @param deliveryOptionID
	 *            the deliveryOptionID to set
	 */
	public void setDeliveryOptionID(String deliveryOptionID) {
		this.deliveryOptionID = deliveryOptionID;
	}

	/** Sets the country to which the transaction being sent. */
	public void setDestinationCountry(String s) {
		/*
		 * in the process of switching from receiveCountry to destinationCountry
		 * so for now, set both just in case.
		 */
		this.receiveCountry = s;
		this.destinationCountry = s;
	}

	/**
	 * @param destinationState
	 *            the destinationState to set
	 */
	public void setDestinationState(CountrySubdivision destinationState) {
		this.destinationState = destinationState;
	}

	private String receiverAccountNumber;
	/**
	 * @param direction2
	 *            the direction2 to set
	 */
	public void setDirection2(String direction2) {
		this.direction2 = direction2;
	}

	/**
	 * @param enteredPromoArray
	 *            the enteredPromoArray to set
	 */
	public void setEnteredPromoArray(String[] enteredPromoArray) {
		this.enteredPromoArray = enteredPromoArray;
	}

	/**
	 * @param expectedReceiveAmount
	 *            the expectedReceiveAmount to set
	 */
	public void setExpectedReceiveAmount(String expectedReceiveAmount) {
		this.expectedReceiveAmount = expectedReceiveAmount;
	}

	/**
	 * @param freqCustCardNumber
	 *            the freqCustCardNumber to set
	 */
	private void setFreqCustCardNumber(String freqCustCardNumber) {
		this.freqCustCardNumber = freqCustCardNumber;
	}

	public void setHistoryQDO(ServiceOption qdo) {
		this.qdo = qdo;
	}

	/**
	 * @param message1
	 *            the message1 to set
	 */
	public void setMessage1(String message1) {
		this.message1 = message1;
	}

	/**
	 * @param productVariant
	 *            the productVariant to set
	 */
	public void setProductVariant(ProductVariantType productVariant) {
		this.productVariant = productVariant;
	}

	/**
	 * @param promoCodes
	 *            the promoCodes to set
	 */
	public void setPromoCodes(String[] promoCodes) {
		this.promoCodes = promoCodes;
	}

	/**
	 * Copies the given QDO data into this object. Conceptually, it is as if
	 * this class contained a reference to a QDO... <code>
	 * class MoneyGramSendInfo40 {
	 *     QualifiedDeliveryOption qdo;
	 * }
	 * </code> ...and had the usual set method. But since this is an XML wrapper
	 * class it isn't actually implemented that way.
	 */
	public void setQualifiedDeliveryOption(ServiceOption qdo, DataCollectionSet dataCollectionSet) {
		this.qualifiedDeliveryOption = qdo;
		if (qualifiedDeliveryOption != null) {
//			dataCollectionSet.setFieldValue(FieldKey.SERVICEOPTION_KEY, qdo.getServiceOption());
//	        dataCollectionSet.setFieldValue(FieldKey.DELIVERY_OPTION_DISPLAY_NAME_KEY, ServiceOption.getServiceOptionDisplayName(qdo.getServiceOption())); 
////	        dataCollectionSet.setFieldValue(FieldKey.DELIVERYOPTIONID_KEY, getDeliveryOptionID());
//	        dataCollectionSet.setFieldValue(FieldKey.RECEIVE_AGENTID_KEY, qdo.getReceiveAgentID());
//	        dataCollectionSet.setFieldValue(FieldKey.RECEIVE_CURRENCY_KEY, qdo.getPayoutCurrency());
//	        dataCollectionSet.setFieldValue(FieldKey.RECEIVE_AGENTABBREVIATION_KEY, qdo.getReceiveAgentAbbreviation());
//	        dataCollectionSet.setFieldValue(FieldKey.MG_DIRECTED_SEND_COUNTRY_KEY, qdo.isRegistrationAllowed() ? "true" : "false");
		}
	}

	/**
	 * @param question
	 *            the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}

	/**
	 * @param receiveCurrency
	 *            the receiveCurrency to set
	 */
	public void setReceiveCurrency(String receiveCurrency) {
		this.receiveCurrency = receiveCurrency;
	}

	public void setReceiver(ReceiverInfo pi, DataCollectionSet gffpDataCollectionSet) {
		if (pi != null) {
			ReceiverInfo e = (ReceiverInfo) pi;
			setReceiverFirstName(e.getFirstName());
			setReceiverMiddleName(e.getMiddleName());
			setReceiverLastName(e.getLastName());
			gffpDataCollectionSet.setFieldValue(FieldKey.RECEIVER_FIRSTNAME_KEY, e.getFirstName());
			gffpDataCollectionSet.setFieldValue(FieldKey.RECEIVER_MIDDLENAME_KEY, e.getMiddleName());
			gffpDataCollectionSet.setFieldValue(FieldKey.RECEIVER_LASTNAME_KEY, e.getLastName());
			setReceiverEP(pi, gffpDataCollectionSet);
		}
	}

	/** Sets the subset of receiver info that is not used by EP. */
	public void setReceiverEP(ReceiverInfo pi, DataCollectionSet gffpDataCollectionSet) {
		if (pi != null) {
			ReceiverInfo e = (ReceiverInfo) pi;
			setRegistrationNumber(e.getRegistrationNumber());
			gffpDataCollectionSet.setFieldValue(FieldKey.RECEIVER_LASTNAME2_KEY, e.getSecondLastName());
			gffpDataCollectionSet.setFieldValue(FieldKey.RECEIVER_ADDRESS_KEY, e.getAddress());
			gffpDataCollectionSet.setFieldValue(FieldKey.RECEIVER_ADDRESS2_KEY, e.getAddress2());
			gffpDataCollectionSet.setFieldValue(FieldKey.RECEIVER_ADDRESS3_KEY, e.getAddress3());
			gffpDataCollectionSet.setFieldValue(FieldKey.RECEIVER_ADDRESS4_KEY, e.getAddress4());
			gffpDataCollectionSet.setFieldValue(FieldKey.COLONIA_KEY, e.getColonia());
			gffpDataCollectionSet.setFieldValue(FieldKey.MUNICIPIO_KEY, e.getMunicipio());
			gffpDataCollectionSet.setFieldValue(FieldKey.REGISTRATION_NUMBER_KEY, e.getRegistrationNumber());
			gffpDataCollectionSet.setFieldValue(FieldKey.RECEIVER_CITY_KEY, e.getCity());
			gffpDataCollectionSet.setFieldValue(FieldKey.RECEIVER_COUNTRY_SUBDIVISIONCODE_KEY, e.getState());
			gffpDataCollectionSet.setFieldValue(FieldKey.RECEIVER_POSTALCODE_KEY, e.getZip());
			gffpDataCollectionSet.setFieldValue(FieldKey.RECEIVER_COUNTRY_KEY, e.getCountry());
			// gffpDataCollectionSet.setItemValue("receiverPhone",
			// e.getChildText("receiverPhone"));
			// gffpDataCollectionSet.setItemValue(DWValues.MF_MGI_REWARDS_NBR,
			// e.getChildText(DWValues.MF_MGI_REWARDS_NBR));
		}
	}

	/**
	 * @param receiverFirstName
	 *            the receiverFirstName to set
	 */
	public void setReceiverFirstName(String receiverFirstName) {
		this.receiverFirstName = receiverFirstName;
	}

	/**
	 * @param receiverLastName
	 *            the receiverLastName to set
	 */
	public void setReceiverLastName(String receiverLastName) {
		this.receiverLastName = receiverLastName;
	}

	/**
	 * @param receiverMiddleName
	 *            the receiverMiddleName to set
	 */
	public void setReceiverMiddleName(String receiverMiddleName) {
		this.receiverMiddleName = receiverMiddleName;
	}

	/**
	 * @param registrationNumber
	 *            the registrationNumber to set
	 */
	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	/**
	 * @param sendCurrency
	 *            the sendCurrency to set
	 */
	public void setSendCurrency(String sendCurrency) {
		this.sendCurrency = sendCurrency;
	}

	/* AgentID ,AgentAbbrevation are already declared ** */
	/* End of Adding of fields */
	public void setSender(SenderInfo pi, DataCollectionSet gffpDataCollectionSet) {
		gffpDataCollectionSet.setFieldValue(FieldKey.MGIREWARDSNUMBER_KEY, getFreqCustCardNumber());
		gffpDataCollectionSet.setFieldValue(FieldKey.ACTIVE_FREQ_CUST_CARD_NUMBER_KEY, getActiveFreqCustCardNumber());
		gffpDataCollectionSet.setFieldValue(FieldKey.SEND_CONSUMERID_KEY, getConsumerId());
		gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_FIRSTNAME_KEY, getSenderFirstName());
		gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_MIDDLENAME_KEY, getSenderMiddleName());
		gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_LASTNAME_KEY, getSenderLastName());
		gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_LASTNAME2_KEY, getSenderLastName2());
		gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_ADDRESS_KEY, getSenderAddress());
		gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_ADDRESS2_KEY, getSenderAddress2());
		gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_ADDRESS3_KEY, getSenderAddress3());
		gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_ADDRESS4_KEY, getSenderAddress4());
		gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_CITY_KEY, getSenderCity());
		gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_COUNTRY_SUBDIVISIONCODE_KEY, getSenderState());
		gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_POSTALCODE_KEY, getSenderZipCode());
		gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_COUNTRY_KEY, getSenderCountry());
		gffpDataCollectionSet.setFieldValue(FieldKey.SENDER_PRIMARYPHONE_KEY, getSenderHomePhone());
		gffpDataCollectionSet.setFieldValue(FieldKey.REGISTRATION_NUMBER_KEY, getRegistrationNumber());
		gffpDataCollectionSet.setFieldValue(FieldKey.RECEIVER_FIRSTNAME_KEY, getReceiverFirstName());
		gffpDataCollectionSet.setFieldValue(FieldKey.RECEIVER_MIDDLENAME_KEY, getReceiverMiddleName());
		gffpDataCollectionSet.setFieldValue(FieldKey.RECEIVER_LASTNAME_KEY, getReceiverLastName());
		gffpDataCollectionSet.setFieldValue(FieldKey.SMS_PHONE_NUMBER_KEY, getSmsPhoneNumber());
		gffpDataCollectionSet.setFieldValue(FieldKey.SMS_COUNTRY_DIAL_CODE_KEY, getSmsCountryDialCode());
	}

	/**
	 * @param senderAddress
	 *            the senderAddress to set
	 */
	private void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}

	/**
	 * @param senderAddress2
	 *            the senderAddress2 to set
	 */
	private void setSenderAddress2(String senderAddress2) {
		this.senderAddress2 = senderAddress2;
	}

	/**
	 * @param senderAddress3
	 *            the senderAddress3 to set
	 */
	private void setSenderAddress3(String senderAddress3) {
		this.senderAddress3 = senderAddress3;
	}

	/**
	 * @param senderAddress4
	 *            the senderAddress4 to set
	 */
	private void setSenderAddress4(String senderAddress4) {
		this.senderAddress4 = senderAddress4;
	}

	/**
	 * @param senderBankAccountNumber
	 *            the senderBankAccountNumber to set
	 */
	public void setSenderBankAccountNumber(String senderBankAccountNumber) {
		this.senderBankAccountNumber = senderBankAccountNumber;
	}

	/**
	 * @param senderCity
	 *            the senderCity to set
	 */
	private void setSenderCity(String senderCity) {
		this.senderCity = senderCity;
	}

	/**
	 * @param senderCountry
	 *            the senderCountry to set
	 */
	public void setSenderCountry(String senderCountry) {
		this.senderCountry = senderCountry;
	}

	/**
	 * @param senderFirstName
	 *            the senderFirstName to set
	 */
	public void setSenderFirstName(String senderFirstName) {
		this.senderFirstName = senderFirstName;
	}

	/**
	 * @param senderHomePhone
	 *            the senderHomePhone to set
	 */
	private void setSenderHomePhone(String senderHomePhone) {
		this.senderHomePhone = senderHomePhone;
	}

	/**
	 * @param senderInfo
	 *            the senderInfo to set
	 */
//	public void setSenderInfo(PersonalInfo senderInfo) {
//		this.senderInfo = (SenderInfo) senderInfo;
//	}

	/**
	 * @param senderLastName
	 *            the senderLastName to set
	 */
	public void setSenderLastName(String senderLastName) {
		this.senderLastName = senderLastName;
	}

	/**
	 * @param senderLastName2
	 *            the senderLastName2 to set
	 */
	private void setSenderLastName2(String senderLastName2) {
		this.senderLastName2 = senderLastName2;
	}

	/**
	 * @param senderMiddleName
	 *            the senderMiddleName to set
	 */
	public void setSenderMiddleName(String senderMiddleName) {
		this.senderMiddleName = senderMiddleName;
	}

	/**
	 * @param senderState
	 *            the senderState to set
	 */
	private void setSenderState(String senderState) {
		this.senderState = senderState;
	}

	/**
	 * @param senderZipCode
	 *            the senderZipCode to set
	 */
	private void setSenderZipCode(String senderZipCode) {
		this.senderZipCode = senderZipCode;
	}

	/**
	 * @param smsCountryDialCode
	 *            the smsCountryDialCode to set
	 */
	private void setSmsCountryDialCode(String smsCountryDialCode) {
		this.smsCountryDialCode = smsCountryDialCode;
	}

	/**
	 * @param smsPhoneNumber
	 *            the smsPhoneNumber to set
	 */
	private void setSmsPhoneNumber(String smsPhoneNumber) {
		this.smsPhoneNumber = smsPhoneNumber;
	}

	/**
	 * @param validateAccountNumber
	 *            the validateAccountNumber to set
	 */
	public void setValidateAccountNumber(String validateAccountNumber) {
		this.validateAccountNumber = validateAccountNumber;
	}

	/**
	 * Returns a string in the form "delivery option - agent - currency" for use
	 * in drop-down lists, verification screens, and receipts.
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(25);
		sb.append(getDeliveryOptionDisplayName());
		
		String agentID = ServiceOption.getReceiveAgentID(this.getDeliveryOption(), this.destinationCountry, this.receiveCurrency, this.receiveAgentID);

		if (!"".equals(agentID))
			sb.append(" - ").append(agentID);

		if (!"".equals(getReceiveCurrency()))
			sb.append(" - ").append(getReceiveCurrency());

		return sb.toString();
	}

	/**
	 * @return the mgCustomerReceiveNumber
	 */
	public String getMgCustomerReceiveNumber() {
		return mgCustomerReceiveNumber;
	}

	/**
	 * @param mgCustomerReceiveNumber the mgCustomerReceiveNumber to set
	 */
	public void setMgCustomerReceiveNumber(String mgCustomerReceiveNumber) {
		this.mgCustomerReceiveNumber = mgCustomerReceiveNumber;
	}


	/**
	 * @return the purpose
	 */
	public String getPurpose() {
		return purpose;
	}

	/**
	 * @param purpose the purpose to set
	 */
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	/**
	 * @return the receiverAccountNumber
	 */
	public String getReceiverAccountNumber() {
		return receiverAccountNumber;
	}

	/**
	 * @param receiverAccountNumber the receiverAccountNumber to set
	 */
	public void setReceiverAccountNumber(String receiverAccountNumber) {
		this.receiverAccountNumber = receiverAccountNumber;
	}
}
