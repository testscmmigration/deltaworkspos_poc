/*
 * GetInitialSetupMsg.java
 *
 * Generated on Wed Aug 18 14:28:14 CDT 2004
 */

package dw.model.adapters;

import com.moneygram.agentconnect.DwInitialSetupResponse;

public class GetInitialSetupMsg {
	
	private String returnProfile;

	public GetInitialSetupMsg(DwInitialSetupResponse response) {
		setReturnProfile(response.getPayload().getValue().getProfile());
	}

	/**
	 * @return the returnProfile
	 */
	public String getReturnProfile() {
		return returnProfile;
	}

	/**
	 * @param returnProfile the returnProfile to set
	 */
	public void setReturnProfile(String returnProfile) {
		this.returnProfile = returnProfile;
	}
}
