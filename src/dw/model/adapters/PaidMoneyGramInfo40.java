/*
 * PaidMoneyGramInfo40.java
 *
 * Generated on Thu Aug 12 13:13:20 CDT 2004
 */

package dw.model.adapters;

import java.math.BigInteger;

import dw.profile.ClientInfo;


public class PaidMoneyGramInfo40 {

	private ReceiverInfo receiver;

	private BigInteger agentCheckNumber;
	private String agentAmount;
	private String referenceNumber;
	private BigInteger customerCheckNumber;
	private String customerAmount;
	private String currency;
	private String idDateOfIssue;
	private String idCityOfIssue;
	private String idCountryOfIssue;
	private String otherPayoutType;
	private String otherPayoutAmount;
	private String otherPayoutId;
	private boolean otherPayoutSwiped;

	/**
	 * @return the agentCheckNumber
	 */
	public BigInteger getAgentCheckNumber() {
		return agentCheckNumber;
	}

	/**
	 * @param agentCheckNumber
	 *            the agentCheckNumber to set
	 */
	public void setAgentCheckNumber(BigInteger agentCheckNumber) {
		this.agentCheckNumber = agentCheckNumber;
	}

	/**
	 * @return the agentAmount
	 */
	public String getAgentAmount() {
		return agentAmount;
	}

	/**
	 * @param agentAmount
	 *            the agentAmount to set
	 */
	public void setAgentAmount(String agentAmount) {
		this.agentAmount = agentAmount;
	}

	/**
	 * @return the referenceNumber
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}

	/**
	 * @param referenceNumber
	 *            the referenceNumber to set
	 */
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	/**
	 * @return the customerCheckNumber
	 */
	public BigInteger getCustomerCheckNumber() {
		return customerCheckNumber;
	}

	/**
	 * @param customerCheckNumber
	 *            the customerCheckNumber to set
	 */
	public void setCustomerCheckNumber(BigInteger customerCheckNumber) {
		this.customerCheckNumber = customerCheckNumber;
	}

	/**
	 * @return the customerAmount
	 */
	public String getCustomerAmount() {
		return customerAmount;
	}

	/**
	 * @param customerAmount
	 *            the customerAmount to set
	 */
	public void setCustomerAmount(String customerAmount) {
		this.customerAmount = customerAmount;
	}

	/**
	 * @return the receiver
	 */
	public ReceiverInfo getReceiver() {
		return receiver;
	}

	/**
	 * @param receiver
	 *            the receiver to set
	 */
	public void setReceiver(ReceiverInfo receiver) {
		this.receiver = receiver;
	}

	/**
	 * @param payoutCurrency
	 *            the currency being paid to the customer
	 */
    public void setCurrency(String payoutCurrency) {
        this.currency = payoutCurrency;
    }

	/**
	 * @return the currency
	 */
	 //BUG 799 - SLS 1-7-06 - First check to see if the currency is already set internally.
    //If so, then use it, otherwise get it from code tables.
    public String getCurrency() {
        if ( currency != null && currency.length() > 0)
            return currency;
        
        return (new ClientInfo().getAgentCountry().getBaseReceiveCurrency());
    }

	/**
	 * @return the idDateOfIssue
	 */
	public String getIdDateOfIssue() {
		return idDateOfIssue;
	}

	/**
	 * @return the idCityOfIssue
	 */
	public String getIdCityOfIssue() {
		return idCityOfIssue;
	}

	/**
	 * @return the idCountryOfIssue
	 */
	public String getIdCountryOfIssue() {
		return idCountryOfIssue;
	}

	/**
	 * @return the otherPayoutType
	 */
	public String getOtherPayoutType() {
		return otherPayoutType;
	}

	/**
	 * @param otherPayoutType
	 *            the otherPayoutType to set
	 */
	public void setOtherPayoutType(String otherPayoutType) {
		this.otherPayoutType = otherPayoutType;
	}

	/**
	 * @return the otherPayoutAmount
	 */
	public String getOtherPayoutAmount() {
		return otherPayoutAmount;
	}

	/**
	 * @param otherPayoutAmount
	 *            the otherPayoutAmount to set
	 */
	public void setOtherPayoutAmount(String otherPayoutAmount) {
		this.otherPayoutAmount = otherPayoutAmount;
	}

	/**
	 * @return the otherPayoutId
	 */
	public String getOtherPayoutId() {
		return otherPayoutId;
	}

	/**
	 * @param otherPayoutId
	 *            the otherPayoutId to set
	 */
	public void setOtherPayoutId(String otherPayoutId) {
		this.otherPayoutId = otherPayoutId;
	}

	/**
	 * @return the otherPayoutSwiped
	 */
	public boolean isOtherPayoutSwiped() {
		return otherPayoutSwiped;
	}

	/**
	 * @param otherPayoutSwiped
	 *            the otherPayoutSwiped to set
	 */
	public void setOtherPayoutSwiped(boolean otherPayoutSwiped) {
		this.otherPayoutSwiped = otherPayoutSwiped;
	}
}
