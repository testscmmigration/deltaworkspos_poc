/**
 * 
 */
package dw.model.adapters;

/**
 * @author aep4
 *
 */
import java.io.Serializable;
import java.math.BigDecimal;

import com.moneygram.agentconnect.RedirectInfo;
import com.moneygram.agentconnect.TransactionStatusType;

public class ReceiveDetailInfo extends Response implements Serializable {
	private static final long serialVersionUID = 1L;

    private String senderFirstName;
    private String senderMiddleName;
    private String senderLastName;
    private String senderLastName2;
    private String senderHomePhone;
    private String senderAddress;
    private String senderAddress2;
    private String senderAddress3;
    private String senderAddress4;
    private String senderCity;
    private String senderState;
    private String senderZipCode;
    private String senderCountry;
    private String receiverFirstName;
    private String receiverMiddleName;
    private String receiverLastName;
    private String receiverLastName2;
    private String receiverAddress;
    private String receiverAddress2;
    private String receiverAddress3;
    private String receiverAddress4;
    private String receiverCity;
    private String receiverState;
    private String receiverCountry;
    private String receiverZipCode;
    private String receiverPhone;
    private String messageField1;
    private String messageField2;
    private String agentCheckAuthorizationNumber;
    private boolean okForAgent;
    private String deliveryOption;
    private TransactionStatusType transactionStatus;
    private String dateTimeSent;
    private String receiveCurrency;
    private BigDecimal receiveAmount;
    private String originatingCountry;
    private String destinationCountry;
    private String indicativeReceiveAmount;
    private String indicativeReceiveCurrency;
    private String indicativeExchangeRate;
    private boolean redirectIndicator;
    private String billerAccountNumber;
    
    private RedirectInfo redirectInfo;
    
	/**
	 * @return the senderFirstName
	 */
	public String getSenderFirstName() {
		return senderFirstName;
	}
	/**
	 * @param senderFirstName the senderFirstName to set
	 */
	public void setSenderFirstName(String senderFirstName) {
		this.senderFirstName = senderFirstName;
	}
	/**
	 * @return the senderMiddleName
	 */
	public String getSenderMiddleName() {
		return senderMiddleName;
	}
	/**
	 * @param senderMiddleName the senderMiddleName to set
	 */
	public void setSenderMiddleName(String senderMiddleName) {
		this.senderMiddleName = senderMiddleName;
	}
	/**
	 * @return the senderLastName
	 */
	public String getSenderLastName() {
		return senderLastName;
	}
	/**
	 * @param senderLastName the senderLastName to set
	 */
	public void setSenderLastName(String senderLastName) {
		this.senderLastName = senderLastName;
	}
	/**
	 * @return the senderLastName2
	 */
	public String getSenderLastName2() {
		return senderLastName2;
	}
	/**
	 * @param senderLastName2 the senderLastName2 to set
	 */
	public void setSenderLastName2(String senderLastName2) {
		this.senderLastName2 = senderLastName2;
	}
	/**
	 * @return the senderHomePhone
	 */
	public String getSenderHomePhone() {
		return senderHomePhone;
	}
	/**
	 * @param senderHomePhone the senderHomePhone to set
	 */
	public void setSenderHomePhone(String senderHomePhone) {
		this.senderHomePhone = senderHomePhone;
	}
	/**
	 * @return the senderAddress
	 */
	public String getSenderAddress() {
		return senderAddress;
	}
	/**
	 * @param senderAddress the senderAddress to set
	 */
	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}
	/**
	 * @return the senderAddress2
	 */
	public String getSenderAddress2() {
		return senderAddress2;
	}
	/**
	 * @param senderAddress2 the senderAddress2 to set
	 */
	public void setSenderAddress2(String senderAddress2) {
		this.senderAddress2 = senderAddress2;
	}
	/**
	 * @return the senderAddress3
	 */
	public String getSenderAddress3() {
		return senderAddress3;
	}
	/**
	 * @param senderAddress3 the senderAddress3 to set
	 */
	public void setSenderAddress3(String senderAddress3) {
		this.senderAddress3 = senderAddress3;
	}
	/**
	 * @return the senderAddress4
	 */
	public String getSenderAddress4() {
		return senderAddress4;
	}

	/**
	 * @return the senderCity
	 */
	public String getSenderCity() {
		return senderCity;
	}
	/**
	 * @param senderCity the senderCity to set
	 */
	public void setSenderCity(String senderCity) {
		this.senderCity = senderCity;
	}
	/**
	 * @return the senderState
	 */
	public String getSenderState() {
		return senderState;
	}
	/**
	 * @param senderState the senderState to set
	 */
	public void setSenderState(String senderState) {
		this.senderState = senderState;
	}
	/**
	 * @return the senderZipCode
	 */
	public String getSenderZipCode() {
		return senderZipCode;
	}
	/**
	 * @param senderZipCode the senderZipCode to set
	 */
	public void setSenderZipCode(String senderZipCode) {
		this.senderZipCode = senderZipCode;
	}
	/**
	 * @return the senderCountry
	 */
	public String getSenderCountry() {
		return senderCountry;
	}
	/**
	 * @param senderCountry the senderCountry to set
	 */
	public void setSenderCountry(String senderCountry) {
		this.senderCountry = senderCountry;
	}
	/**
	 * @return the receiverFirstName
	 */
	public String getReceiverFirstName() {
		return receiverFirstName;
	}
	/**
	 * @param receiverFirstName the receiverFirstName to set
	 */
	public void setReceiverFirstName(String receiverFirstName) {
		this.receiverFirstName = receiverFirstName;
	}
	/**
	 * @return the receiverMiddleName
	 */
	public String getReceiverMiddleName() {
		return receiverMiddleName;
	}
	/**
	 * @param receiverMiddleName the receiverMiddleName to set
	 */
	public void setReceiverMiddleName(String receiverMiddleName) {
		this.receiverMiddleName = receiverMiddleName;
	}
	/**
	 * @return the receiverLastName
	 */
	public String getReceiverLastName() {
		return receiverLastName;
	}
	/**
	 * @param receiverLastName the receiverLastName to set
	 */
	public void setReceiverLastName(String receiverLastName) {
		this.receiverLastName = receiverLastName;
	}
	/**
	 * @return the receiverLastName2
	 */
	public String getReceiverLastName2() {
		return receiverLastName2;
	}
	/**
	 * @param receiverLastName2 the receiverLastName2 to set
	 */
	public void setReceiverLastName2(String receiverLastName2) {
		this.receiverLastName2 = receiverLastName2;
	}
	/**
	 * @return the receiverAddress
	 */
	public String getReceiverAddress() {
		return receiverAddress;
	}

	/**
	 * @return the receiverAddress2
	 */
	public String getReceiverAddress2() {
		return receiverAddress2;
	}

	/**
	 * @return the receiverAddress3
	 */
	public String getReceiverAddress3() {
		return receiverAddress3;
	}

	/**
	 * @return the receiverAddress4
	 */
	public String getReceiverAddress4() {
		return receiverAddress4;
	}

	/**
	 * @return the receiverCity
	 */
	public String getReceiverCity() {
		return receiverCity;
	}

	/**
	 * @return the receiverState
	 */
	public String getReceiverState() {
		return receiverState;
	}
	
	/**
	 * @return the receiverCountry
	 */
	public String getReceiverCountry() {
		return receiverCountry;
	}
	/**
	 * @param receiverCountry the receiverCountry to set
	 */
	public void setReceiverCountry(String receiverCountry) {
		this.receiverCountry = receiverCountry;
	}
	/**
	 * @return the receiverZipCode
	 */
	public String getReceiverZipCode() {
		return receiverZipCode;
	}

	/**
	 * @return the receiverPhone
	 */
	public String getReceiverPhone() {
		return receiverPhone;
	}

	/**
	 * @return the messageField1
	 */
	public String getMessageField1() {
		return messageField1;
	}
	/**
	 * @param messageField1 the messageField1 to set
	 */
	public void setMessageField1(String messageField1) {
		this.messageField1 = messageField1;
	}
	/**
	 * @return the messageField2
	 */
	public String getMessageField2() {
		return messageField2;
	}
	/**
	 * @param messageField2 the messageField2 to set
	 */
	public void setMessageField2(String messageField2) {
		this.messageField2 = messageField2;
	}

	/**
	 * @return the agentCheckAuthorizationNumber
	 */
	public String getAgentCheckAuthorizationNumber() {
		return agentCheckAuthorizationNumber;
	}
	/**
	 * @param agentCheckAuthorizationNumber the agentCheckAuthorizationNumber to set
	 */
	public void setAgentCheckAuthorizationNumber(
			String agentCheckAuthorizationNumber) {
		this.agentCheckAuthorizationNumber = agentCheckAuthorizationNumber;
	}

	/**
	 * @return the okForAgent
	 */
	public boolean isOkForAgent() {
		return okForAgent;
	}

	/**
	 * @return the deliveryOption
	 */
	public String getDeliveryOption() {
		return deliveryOption;
	}

	/**
	 * @return the transactionStatus
	 */
	public TransactionStatusType getTransactionStatus() {
		return transactionStatus;
	}
	/**
	 * @param transactionStatus the transactionStatus to set
	 */
	public void setTransactionStatus(TransactionStatusType transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	/**
	 * @return the dateTimeSent
	 */
	public String getDateTimeSent() {
		return dateTimeSent;
	}
	/**
	 * @param dateTimeSent the dateTimeSent to set
	 */
	public void setDateTimeSent(String dateTimeSent) {
		this.dateTimeSent = dateTimeSent;
	}
	/**
	 * @return the receiveCurrency
	 */
	public String getReceiveCurrency() {
		return receiveCurrency;
	}
	/**
	 * @param receiveCurrency the receiveCurrency to set
	 */
	public void setReceiveCurrency(String receiveCurrency) {
		this.receiveCurrency = receiveCurrency;
	}
	/**
	 * @return the receiveAmount
	 */
	public BigDecimal getReceiveAmount() {
		return receiveAmount;
	}
	/**
	 * @param receiveAmount the receiveAmount to set
	 */
	public void setReceiveAmount(BigDecimal receiveAmount) {
		this.receiveAmount = receiveAmount;
	}

	/**
	 * @return the originatingCountry
	 */
	public String getOriginatingCountry() {
		return originatingCountry;
	}
	/**
	 * @param originatingCountry the originatingCountry to set
	 */
	public void setOriginatingCountry(String originatingCountry) {
		this.originatingCountry = originatingCountry;
	}
	/**
	 * @param destinationCountry the destinationCountry to set
	 */
	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}
	/**
	 * @return the destinationCountry
	 */
	public String getDestinnationCountry() {
		return destinationCountry;
	}

	/**
	 * @return the indicativeReceiveAmount
	 */
	public String getIndicativeReceiveAmount() {
		if (indicativeReceiveAmount != null) {
			return indicativeReceiveAmount;
		} else {
			return "0";
		}
	}
	/**
	 * @param indicativeReceiveAmount the indicativeReceiveAmount to set
	 */
	public void setIndicativeReceiveAmount(String indicativeReceiveAmount) {
		this.indicativeReceiveAmount = indicativeReceiveAmount;
	}
	/**
	 * @return the indicativeReceiveCurrency
	 */
	public String getIndicativeReceiveCurrency() {
		return indicativeReceiveCurrency;
	}

	/**
	 * @return the indicativeExchangeRate
	 */
	public String getIndicativeExchangeRate() {
		return indicativeExchangeRate;
	}
	/**
	 * @param indicativeExchangeRate the indicativeExchangeRate to set
	 */
	public void setIndicativeExchangeRate(String indicativeExchangeRate) {
		this.indicativeExchangeRate = indicativeExchangeRate;
	}
	/**
	 * @return the redirectIndicator
	 */
	public boolean isRedirectIndicator() {
		return redirectIndicator;
	}
	/**
	 * @set the redirectIndicator
	 */
	public void setRedirectIndicator(boolean redirectIndicator) {
		this.redirectIndicator = redirectIndicator;
	}
	/**
	 * @set the redirectIndicator
	 */
	public void setRedirectInfo(RedirectInfo redirectInfo) {
		this.redirectInfo = redirectInfo;
	}
	/**
	 * @return the redirectInfo
	 */
	public RedirectInfo getRedirectInfo() {
		return redirectInfo;
	}
	/**
	 * @set the billerAccountNumber
	 */
	public void setBillerAccountNumber(String billerAccountNumber) {
		this.billerAccountNumber = billerAccountNumber;
	}
	/**
	 * @return the billerAccountNumber
	 */
	public String getBillerAccountNumber() {
		return billerAccountNumber;
	}
	
}