package dw.model.adapters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.moneygram.agentconnect.CategoryInfo;
import com.moneygram.agentconnect.ConsumerProfileSearchInfo;
import com.moneygram.agentconnect.EnumeratedIdentifierInfo;
import com.moneygram.agentconnect.FieldInfo;
import com.moneygram.agentconnect.InfoBase;
import com.moneygram.agentconnect.KeyValuePairType;

import dw.framework.DataCollectionField;
import dw.framework.FieldKey;
import dw.i18n.FormatSymbols;
import dw.model.adapters.Data.DataValue;
import dw.utility.ServiceOption;

public class ConsumerProfile implements Comparable<ConsumerProfile> {
	
	private String id;
	private String type;
	private String mgiSessionId;
	private Map<String, String> searchValues;
	private Map<String, String> currentValues;
	private Map<String, String> originalValues;
	private Map<String, FieldInfo> infoMap;
	private List<EntryInfo> receivers;
	private List<EntryInfo> originalReceivers;
	private List<EntryInfo> billers;
	private List<EntryInfo> originalBillers;
	private EntryInfo currentReceiver;
	private EntryInfo currentBiller;
	private String firstName;
	private String lastName;
	private NotificationOptionDisplayStatus notificationOptionDisplayStatus;
	private boolean modified;
	
	public enum NotificationOptionDisplayStatus {
		NOT_DISPLAYED,
		DISPLAYING_IN_REVIEW,
		DISPLAYING_IN_EDIT,
		ALlREADY_DISPLAYED
	}
	
	public class EntryInfo implements Comparable<EntryInfo> {
		private Map<String, String> values;
		private String sort1;
		private String sort2;
		
		private EntryInfo() {
			values = new HashMap<String, String>();
		}
		
		public Map<String, String> getValues() {
			return this.values;
		}
		
		public void setSort1(String sort1) {
			this.sort1 = sort1;
		}
		
		public void setSort2(String sort2) {
			this.sort2 = sort2;
		}
		
		public String getReceiverName() {
			String firstName = values.get(FieldKey.RECEIVER_FIRSTNAME_KEY.getInfoKey()); 
			String middleName = values.get(FieldKey.RECEIVER_MIDDLENAME_KEY.getInfoKey());
			String lastName1 = values.get(FieldKey.RECEIVER_LASTNAME_KEY.getInfoKey());
			String lastName2 = values.get(FieldKey.RECEIVER_LASTNAME2_KEY.getInfoKey());
			
			return formatName(firstName, middleName, lastName1, lastName2);
		}
		

		public String getAddress() {
			String address1 = values.get(FieldKey.RECEIVER_ADDRESS_KEY.getInfoKey()); 
			String address2 = values.get(FieldKey.RECEIVER_ADDRESS2_KEY.getInfoKey());
			String address3 = values.get(FieldKey.RECEIVER_ADDRESS3_KEY.getInfoKey());
			String address4 = values.get(FieldKey.RECEIVER_ADDRESS4_KEY.getInfoKey());
			
			return formatText(address1, address2, address3, address4);
		}

		public String getCity() {
			return values.get(FieldKey.RECEIVER_CITY_KEY.getInfoKey()); 
		}

		public String getDirections() {
			String direction1 = values.get(FieldKey.DIRECTION1_KEY.getInfoKey()); 
			String direction2 = values.get(FieldKey.DIRECTION2_KEY.getInfoKey());
			String direction3 = values.get(FieldKey.DIRECTION3_KEY.getInfoKey());
			
			return formatText(direction1, direction2, direction3, null);
		}

		public String getDestination() {
			String value1 = values.get(FieldKey.DESTINATION_COUNTRY_KEY.getInfoKey()); 
			String value2 = values.get(FieldKey.RECEIVE_COUNTRY_KEY.getInfoKey()); 
			return value1 != null ? value1 : value2;
		}
		
		public String getServiceOption() {
			String serviceOption = values.get(FieldKey.SERVICEOPTION_KEY.getInfoKey());
			String destinationCountry = values.get(FieldKey.DESTINATION_COUNTRY_KEY.getInfoKey());
			String receiveCountry = values.get(FieldKey.RECEIVE_COUNTRY_KEY.getInfoKey());
			String country = ((destinationCountry != null) && (! destinationCountry.isEmpty())) ? destinationCountry : receiveCountry;
			String receiveCurrency = values.get(FieldKey.RECEIVE_CURRENCY_KEY.getInfoKey());
			String receiveAgentId = values.get(FieldKey.RECEIVE_AGENTID_KEY.getInfoKey());
			ServiceOption so = ServiceOption.getServiceOption(serviceOption, country, receiveCurrency, receiveAgentId);
			return so != null ? so.getServiceOptionDisplayName() : "";
		}
		
		public String getServiceOptionDescription() {
			String serviceOption = values.get(FieldKey.SERVICEOPTION_KEY.getInfoKey());
			String destinationCountry = values.get(FieldKey.DESTINATION_COUNTRY_KEY.getInfoKey());
			String receiveCountry = values.get(FieldKey.RECEIVE_COUNTRY_KEY.getInfoKey());
			String country = ((destinationCountry != null) && (! destinationCountry.isEmpty())) ? destinationCountry : receiveCountry;
			String receiveCurrency = values.get(FieldKey.RECEIVE_CURRENCY_KEY.getInfoKey());
			String receiveAgentId = values.get(FieldKey.RECEIVE_AGENTID_KEY.getInfoKey());
			ServiceOption so = ServiceOption.getServiceOption(serviceOption, country, receiveCurrency, receiveAgentId);
			return so != null ? so.getServiceOptionDisplayDescription() : "";
		}
		
		public String getAccountNumber() {
			String value = values.get(FieldKey.DISPLAY_ACCOUNT_ID_KEY.getInfoKey());
			return value != null ? value : "";
		}

		@Override
		public int compareTo(EntryInfo ei) {
			String sort1a = this.sort1 != null ? this.sort1 : "";
			String sort1b = ei.sort1 != null ? ei.sort1 : "";
			int c = sort1a.compareToIgnoreCase(sort1b);
			if (c == 0) {
				String sort2a = this.sort2 != null ? this.sort2 : "";
				String sort2b = ei.sort2 != null ? ei.sort2 : "";
				c = sort2a.compareToIgnoreCase(sort2b);
			}
			return c;
		}
	}

	public ConsumerProfile(ConsumerProfileSearchInfo info) {
		this(info.getConsumerProfileIDType());
		this.id = info.getConsumerProfileID();
//		addValues(info.getCurrentValues().getCurrentValue());
		for (KeyValuePairType pair : info.getCurrentValues().getCurrentValue()) {
			String key = pair.getInfoKey();
			String value = pair.getValue().getValue();
			searchValues.put(key, value);
		}
		
		firstName = searchValues.get(FieldKey.CONSUMER_FIRSTNAME_KEY.getInfoKey());
		lastName = searchValues.get(FieldKey.CONSUMER_LASTNAME_KEY.getInfoKey()); 
		modified = false;
	}
	
	public ConsumerProfile(CustomerInfo info) {
		this("UCPID");
		if (info.getSender() != null) {
			if (info.getSender().getData() != null) {
				for (FieldKey key : info.getSender().getData().keySet()) {
					DataValue value = info.getSender().getData().get(key);
					if (value != null) {
						currentValues.put(key.getInfoKey(), value.getValue());
					}
				}
			}
			
			if (info.getReceiver() != null) {
				EntryInfo receiver = new EntryInfo();
				this.currentReceiver = receiver;
				this.receivers.add(receiver);
				this.originalReceivers.add(receiver);
				for (FieldKey key : info.getReceiver().getData().keySet()) {
					DataValue value = info.getReceiver().getData().get(key);
					if (value != null) {
						receiver.values.put(key.getInfoKey(), value.getValue());
					}
				}
				
				String lastName1 = receiver.values.get(FieldKey.RECEIVER_LASTNAME_KEY.getInfoKey()); 
				String lastName2 = receiver.values.get(FieldKey.RECEIVER_LASTNAME2_KEY.getInfoKey()); 
				receiver.setSort1(lastName1 + (lastName2 != null ? " " + lastName2 : ""));
				String firstName = receiver.values.get(FieldKey.RECEIVER_FIRSTNAME_KEY.getInfoKey()); 
				String middleName = receiver.values.get(FieldKey.RECEIVER_MIDDLENAME_KEY.getInfoKey()); 
				receiver.setSort2(firstName + (middleName != null ? " " + middleName : ""));
			}
			
			Collections.sort(receivers);
			Collections.sort(originalReceivers);
		}
	}
	
	public ConsumerProfile(String type) {
		this.type = type;
		searchValues = new HashMap<String, String>();
		currentValues = new HashMap<String, String>();
		originalValues = new HashMap<String, String>();
		infoMap = new HashMap<String, FieldInfo>();
		receivers = new ArrayList<EntryInfo>();
		originalReceivers = new ArrayList<EntryInfo>();
		billers = new ArrayList<EntryInfo>();
		originalBillers = new ArrayList<EntryInfo>();
		notificationOptionDisplayStatus = NotificationOptionDisplayStatus.NOT_DISPLAYED;
		modified = false;
	}
	
	public List<EntryInfo> getReceivers() {
		return receivers;
	}

	public List<EntryInfo> getBillers() {
		return billers;
	}

	public List<EntryInfo> getOriginalReceivers() {
		return originalReceivers;
	}

	public List<EntryInfo> getOriginalBillers() {
		return originalBillers;
	}

	public void addValues(List<KeyValuePairType> valueList) {
		for (KeyValuePairType pair : valueList) {
			String key = pair.getInfoKey();
			String value = pair.getValue().getValue();
			originalValues.put(key, value);
			currentValues.put(key, value);
		}
	}
	
	public void addInfoValues(List<InfoBase> infos) {
		if (infos != null) {
			for (InfoBase info : infos) {
				if (info instanceof CategoryInfo) {
					addInfoValues(((CategoryInfo) info).getInfos().getValue().getInfo());
				} else if (info instanceof FieldInfo) {
					FieldInfo field = (FieldInfo) info;
					String key = field.getInfoKey();
					infoMap.put(key, field);
				}
			}
		}
	}

	public void setValue(String key, String value) {
		currentValues.put(key, value);
	}
	
	public void addReceiver(List<KeyValuePairType> valuesList) {
		addEntry(valuesList, this.receivers, this.originalReceivers);
	}
	
	public void addBiller(List<KeyValuePairType> valuesList) {
		addEntry(valuesList, this.billers, this.originalBillers);
	}
	
	private void addEntry(List<KeyValuePairType> valuesList, List<EntryInfo> entries, List<EntryInfo> originalEntries) {
		if ((valuesList != null) && (! valuesList.isEmpty())) {
			EntryInfo entry = new EntryInfo();
			for (KeyValuePairType pair : valuesList) {
				String key = pair.getInfoKey();
				String value = pair.getValue().getValue();
				entry.values.put(key, value);
			}
			
			String lastName1 = entry.values.get(FieldKey.RECEIVER_LASTNAME_KEY.getInfoKey()); 
			String lastName2 = entry.values.get(FieldKey.RECEIVER_LASTNAME2_KEY.getInfoKey()); 
			entry.setSort1(lastName1 + (lastName2 != null ? " " + lastName2 : ""));
			String firstName = entry.values.get(FieldKey.RECEIVER_FIRSTNAME_KEY.getInfoKey()); 
			String middleName = entry.values.get(FieldKey.RECEIVER_MIDDLENAME_KEY.getInfoKey()); 
			entry.setSort2(firstName + (middleName != null ? " " + middleName : ""));
			
			entries.add(entry);
			originalEntries.add(entry);
		}
	}
	
	public String getProfileId() {
		return id;
	}
	
	public String getProfileType() {
		return type;
	}
	
	public String getName() {
		String firstName = searchValues.get(FieldKey.CONSUMER_FIRSTNAME_KEY.getInfoKey()); 
		String middleName = searchValues.get(FieldKey.CONSUMER_MIDDLENAME_KEY.getInfoKey());
		String lastName1 = searchValues.get(FieldKey.CONSUMER_LASTNAME_KEY.getInfoKey());
		String lastName2 = searchValues.get(FieldKey.CONSUMER_LASTNAME2_KEY.getInfoKey());
		
		return formatName(firstName, middleName, lastName1, lastName2);
	}
	
	private String formatName(String firstName, String middleName, String lastName1, String lastName2) {
		StringBuffer sb = new StringBuffer();

		if (lastName1 != null) {
			sb.append(lastName1.toUpperCase().trim());
		}
		if (lastName2 != null) {
			if (sb.length() > 0) {
				sb.append(" ");
			}
			sb.append(lastName2.toUpperCase().trim());
		}
		if (firstName != null) {
			if (sb.length() > 0) {
				sb.append(", ");
			}
			sb.append(firstName.toUpperCase().trim());
		}
		if (middleName != null) {
			if (sb.length() > 0) {
				sb.append(" ");
			}
			sb.append(middleName.toUpperCase().trim());
		}
		
		return sb.toString();
	}
	
	private String formatText(String text1, String text2, String text3, String text4) {
		StringBuffer sb = new StringBuffer();

		if (text1 != null) {
			sb.append(text1.toUpperCase().trim());
		}
		if (text2 != null) {
			if (sb.length() > 0) {
				sb.append(", ");
			}
			sb.append(text2.toUpperCase().trim());
		}
		if (text3 != null) {
			if (sb.length() > 0) {
				sb.append(", ");
			}
			sb.append(text3.toUpperCase().trim());
		}
		if (text4 != null) {
			if (sb.length() > 0) {
				sb.append(", ");
			}
			sb.append(text4.toUpperCase().trim());
		}
		
		return sb.toString();
	}
	
	public String getAddress() {
		StringBuffer sb = new StringBuffer();
		String address = searchValues.get(FieldKey.CONSUMER_ADDRESS_KEY.getInfoKey()); 
		String city = searchValues.get(FieldKey.CONSUMER_CITY_KEY.getInfoKey()); 
		String postalCode = searchValues.get(FieldKey.CONSUMER_POSTALCODE_KEY.getInfoKey()); 
		String country = searchValues.get(FieldKey.CONSUMER_COUNTRY_KEY.getInfoKey()); 

		if (address != null) {
			sb.append(address.toUpperCase().trim());
		}
		if (city != null) {
			if (sb.length() > 0) {
				sb.append(", ");
			}
			sb.append(city.toUpperCase().trim());
		}
		if (postalCode != null) {
			if (sb.length() > 0) {
				sb.append(", ");
			}
			sb.append(postalCode.toUpperCase().trim());
		}
		if (country != null) {
			if (sb.length() > 0) {
				sb.append(" ");
			}
			sb.append(country.toUpperCase().trim());
		}
		
		return sb.toString();
	}

	public String getMgiSessionId() {
		return mgiSessionId;
	}

	public void setMgiSessionId(String mgiSessionId) {
		this.mgiSessionId = mgiSessionId;
	}
	
	public String getBirthDate() {
		String dob = searchValues.get(FieldKey.CONSUMER_DOB_KEY.getInfoKey()); 
		try {
			return FormatSymbols.formatDateForLocale(FormatSymbols.convertCCYYMMDDToDate(dob));
		} catch (Exception e) {
			return "";
		}
	}
	
	public String getProfileValue(String key) {
		return this.currentValues.get(key);
	}

	public EntryInfo getCurrentReceiver() {
		return currentReceiver;
	}
	
	public void setReceiver(EntryInfo receiver) {
		this.currentReceiver = receiver;
	}

	public void setCurrentReceiver(EntryInfo currentReceiver) {
		this.currentReceiver = currentReceiver;
	}

	public EntryInfo getCurrentBiller() {
		return currentBiller;
	}

	public void setCurrentBiller(EntryInfo biller) {
		this.currentBiller = biller;
	}

	@Override
	public int compareTo(ConsumerProfile p) {
		String lastName1 = this.lastName != null ? this.lastName : "";
		String lastName2 = p.lastName != null ? p.lastName : "";
		int c = lastName1.compareToIgnoreCase(lastName2);
		if (c == 0) {
			String firstName1 = this.firstName != null ? this.firstName : "";
			String firstName2 = p.firstName != null ? p.firstName : "";
			c = firstName1.compareToIgnoreCase(firstName2);
		}
		return c;
	}

	public void setReceiverIndex(int i) {
		this.currentReceiver = this.receivers.get(i);
	}

	public void clearCurrentValues() {
		currentValues.clear();
		this.currentReceiver = null;
		this.currentBiller = null;
	}

	public void restoreOriginalValues() {
		currentValues.clear();
		for (Map.Entry<String, String> entry : originalValues.entrySet()) {
			currentValues.put(entry.getKey(), entry.getValue());
		}
		receivers.clear();
		for (EntryInfo info : originalReceivers) {
			receivers.add(info);
		}
		billers.clear();
		for (EntryInfo info : originalBillers) {
			billers.add(info);
		}
	}

	public void updateValues(Map<FieldKey, DataCollectionField> fieldMap) {
		for (String key : currentValues.keySet()) {
			DataCollectionField field = fieldMap.get(FieldKey.key(key));
			if (field != null) {
				String value = field.getValue();
				currentValues.put(key, value);
				originalValues.put(key, value);
			}
		}
	}

	public Map<String, String> getCurrentValuesMap() {
		return currentValues;
	}
	
	public Map<String, FieldInfo> getInfoMap() {
		return this.infoMap;
	}
	
	public String getLabel(String infoKey) {
		FieldInfo info = infoMap.get(infoKey);
		return info != null ? info.getLabel() : "";
	}

	public void setProfileId(String id) {
		this.id = id;
	}
	
	public String getEnumValue(String infoKey, String value1, String value2) {
		String value = "";
		if ((value1 != null) && (! value1.isEmpty())) {
			if (! value1.equalsIgnoreCase("OTHER")) {
				FieldInfo info = infoMap.get(infoKey);
				if ((info != null) && (info.getEnumeration() != null) && (info.getEnumeration().getEnumeratedItems() != null)) {
					List<EnumeratedIdentifierInfo> list = info.getEnumeration().getEnumeratedItems().getEnumeratedItem();
					for (EnumeratedIdentifierInfo item : list) {
						if (item.getIdentifier().equals(value1)) {
							value = item.getLabel();
							break;
						}
					}
				}
			} else {
				value = value2;
			}
		}
		return value;
	}
	
	public Map<String, String> getSearchValues() {
		return this.searchValues;
	}

	public void setCurrentValuesMap(Map<String, String> currentValues) {
		this.currentValues = currentValues;
		this.originalValues = currentValues;
	}

	public String getPhoneNumber() {
		String phoneNumber = searchValues.get(FieldKey.CONSUMER_HOMEPHONE_KEY.getInfoKey()); 
		return phoneNumber;
	}

	public NotificationOptionDisplayStatus getNotificationsDisplayed() {
		return this.notificationOptionDisplayStatus;
	}

	public void setNotificationsDisplayed(NotificationOptionDisplayStatus notificationOptionDisplayStatus) {
		this.notificationOptionDisplayStatus = notificationOptionDisplayStatus;
	}
	
	public void setModified(boolean modified) {
		this.modified = modified;
	}
	
	public boolean isModified() {
		return this.modified;
	}
}
