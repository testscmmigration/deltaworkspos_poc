/*
 * SendFieldsForDeliveryOptionInfo.java
 *
 * Generated on Wed Feb 16 10:38:24 CST 2005
 */

package dw.model.adapters;

import com.moneygram.agentconnect.PromotionInfo;

public final class PromotionalSlipInfo extends PromotionInfo{
	private static final long serialVersionUID = 1L;

	private String messageID;
	private String variable1;
	private String variable2;
	private String variable3;
	private String variable4;

	/**
	 * @return the messageID
	 */
	public String getMessageID() {
		return messageID;
	}

	/**
	 * @param messageID
	 *            the messageID to set
	 */
	public void setMessageID(String messageID) {
		this.messageID = messageID;
	}

	/**
	 * @return the variable1
	 */
	public String getVariable1() {
		return variable1;
	}

	/**
	 * @param variable1
	 *            the variable1 to set
	 */
	public void setVariable1(String variable1) {
		this.variable1 = variable1;
	}

	/**
	 * @return the variable2
	 */
	public String getVariable2() {
		return variable2;
	}

	/**
	 * @param variable2
	 *            the variable2 to set
	 */
	public void setVariable2(String variable2) {
		this.variable2 = variable2;
	}

	/**
	 * @return the variable3
	 */
	public String getVariable3() {
		return variable3;
	}

	/**
	 * @param variable3
	 *            the variable3 to set
	 */
	public void setVariable3(String variable3) {
		this.variable3 = variable3;
	}

	/**
	 * @return the variable4
	 */
	public String getVariable4() {
		return variable4;
	}

	/**
	 * @param variable4
	 *            the variable4 to set
	 */
	public void setVariable4(String variable4) {
		this.variable4 = variable4;
	}

}
