/**
 * 
 */
package dw.model.adapters;

import java.io.Serializable;
import java.util.List;

import com.moneygram.agentconnect.BusinessError;

/**
 * @author aep4
 *
 */
public class Response implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8171503823659662609L;
	
	private String timeStamp;
	protected List<BusinessError> errors;
	/**
	 * @return the timeStamp
	 */
	public String getTimeStamp() {
		return timeStamp;
	}
	/**
	 * @param timeStamp the timeStamp to set
	 */
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	/**
	 * @return the errors
	 */
	public List<BusinessError> getErrors() {
		return errors;
	}
	/**
	 * @param errors the errors to set
	 */
	public void setErrors(List<BusinessError> errors) {
		this.errors = errors;
	}	
}
