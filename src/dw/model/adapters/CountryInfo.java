/*
 * CountryInfo.java
 * 
 * Copyright (c) 2004 MoneyGram, Inc. All rights reserved
 */

package dw.model.adapters;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.moneygram.agentconnect.CountryInfo.CountryDialCodes;
import com.moneygram.agentconnect.CountryInfo.LookupKeys;
import com.moneygram.agentconnect.CountryInfo.ReceiveCurrencies;
import com.moneygram.agentconnect.CountrySubdivisionInfo;
import com.moneygram.agentconnect.SubdivisionInfo;

import dw.comm.MessageFacade;
import dw.comm.MessageLogic;
import dw.framework.CommCompleteInterface;
import dw.framework.DataCollectionScreenToolkit.DWComboBoxItem;
import dw.framework.WaitToken;
import dw.i18n.Messages;
import dw.mgsend.MoneyGramClientTransaction;
import dw.profile.UnitProfile;
import dw.utility.AccessNumbers;
import dw.utility.Debug;
import dw.utility.DwEncryptionAES;
import dw.utility.ExtraDebug;
import dw.utility.TimeUtility;
import dw.utility.DwEncryptionAES.DwEncryptionFileException;

public class CountryInfo implements CommCompleteInterface, Serializable {
	
	private static final long serialVersionUID = 1L;

	private static final Country DEFAULT_COUNTRY;
	private static final Country BLANK_COUNTRY;
	private static final CountrySubdivision BLANK_COUNTRY_SUBDIVISION;
	private static WaitToken countryWait = new WaitToken();
	private static WaitToken countrySubdivisionWait = new WaitToken();
	
	public static final int MANUAL_TRANSMIT_RETERIVAL = 1;
	public static final int AUTOMATIC_TRANSMIT_RETERIVAL = 2;
	public static final int INFORMATION_ACCESS_RETERIVAL = 3;
	
	private static final Pattern UNICODE_REGULAR_EXPRESSION = Pattern.compile("\\\\u[0-9a-f]{4}" , Pattern.CASE_INSENSITIVE);

	private static volatile CountryData countryData = null;
	private static boolean reterivalErrorFlag = false;
	private static CountryInfo instance;
	private static volatile Boolean isAgentKorean = null;
	
	public static final String OK_FOR_ADDRESS = "okForAddress";
	public static final String OK_FOR_IDENTIFICATION = "okForIdentification";
	
	public static final String KEY_SND_ACTIVE = "sendActive"; 
	public static final String KEY_RECV_ACTIVE = "receiveActive";
	public static final String KEY_DIRECT_SEND_ACTIVE = "directedSendsActive";
	public static final String KEY_IND_COUNTRY = "indicativeRateActive";
	
	public static final String DEPRECATED_KEY_IND_COUNTRY ="indicativeCountry"; 
	public static final String DEPRECATED_KEY_RECV_ACTIVE = "okForReceive"; 	
	public static final String DEPRECATED_KEY_SEND_ACTIVE = "okForSend";
		
	public static final List<String> LOOKUP_KEYS_LIST = Arrays.asList("KEY_SND_ACTIVE, KEY_RECV_ACTIVE, HAS_BASE, OK_NATIONALITY, OK_ID, OK_ADDR, OK_BIRTH, OK_CITIZEN, KEY_IND_COUNTRY"); 
	public static final List<String> RECV_COUNTRIES = Arrays.asList("KEY_SND_ACTIVE");

    private static Country agentCountry = null;
    
    static {
    	instance = new CountryInfo();
    	
    	DEFAULT_COUNTRY = new Country();
    	DEFAULT_COUNTRY.countryCode = "USA";
    	DEFAULT_COUNTRY.countryLegacyCode = "US";
    	DEFAULT_COUNTRY.baseReceiveCurrency = "USD";
    	DEFAULT_COUNTRY.order = Integer.valueOf(0);
    	
    	BLANK_COUNTRY = new Country();
    	BLANK_COUNTRY.countryCode = "";
    	BLANK_COUNTRY.countryLegacyCode = "";
    	BLANK_COUNTRY.baseReceiveCurrency = "";
    	BLANK_COUNTRY.order = Integer.valueOf(0);
    	
    	BLANK_COUNTRY_SUBDIVISION = new CountrySubdivision();
    	BLANK_COUNTRY_SUBDIVISION.country = DEFAULT_COUNTRY;
    	BLANK_COUNTRY_SUBDIVISION.countrySubdivisionCode = "";
    	BLANK_COUNTRY_SUBDIVISION.countrySubdivisionName = "";
    }
    
    private static class CountryData implements Serializable {
		private static final long serialVersionUID = 1L;
    	private CountryData() {
    		this.language = "";
    		this.lastTimeUpdated = 0;
    		this.updated = false;
    		this.countryMap = new HashMap<String, Country>();
    		this.countryDialCodeList = new ArrayList<CountryDialCode>();
    		this.countrySubdivisionMap = new HashMap<String, CountrySubdivision>(); 
    	}
    	
    	private String language;
    	private long lastTimeUpdated;
    	private boolean updated;
    	private Map<String, Country> countryMap;
    	private Map<String, CountrySubdivision> countrySubdivisionMap;
    	private List<CountryDialCode> countryDialCodeList;
    	private String countryVersion;
    	private String countrySubdivisionVersion;
    }
    
    public static class Country extends DWComboBoxItem implements Comparable<Country>, Serializable {
    	private static final long serialVersionUID = 1L;
    	
        private String countryCode;
        private String countryName;
        private String countryLegacyCode;
        private String baseReceiveCurrency;
        private Integer order;
        private String prettyName;
        private Map<String, CountrySubdivision> countrySubdivisionMap;
        private List<String> lookupKeys;
        private List<String> receiveCurrencies;
        
        public Country() {
        }
        
        public Country(String countryCode, String label) {
        	this.countryCode = countryCode;
        	this.prettyName = label;
		}

		public String getCountryCode() {
        	return this.countryCode;
        }

        public String getCountryName() {
        	return this.countryName;
        }

        public String getCountryLegacyCode() {
        	return this.countryLegacyCode;
        }

        public String getPrettyName() {
            return this.prettyName;
        }    
        
        public String getBaseReceiveCurrency() {
			return this.baseReceiveCurrency;
		}

        @Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((countryCode == null) ? 0 : countryCode.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Country other = (Country) obj;
			if (countryCode == null) {
				if (other.countryCode != null)
					return false;
			} else if (!countryCode.equals(other.countryCode))
				return false;
			return true;
		}

		@Override
    	public String toString() {
            return this.prettyName;
        }

    	@Override
    	public String getLabel() {
    		return this.prettyName;
    	}

    	@Override
    	public String getValue() {
    		return this.countryCode;
    	}

    	@Override
    	public String getLabelShort() {
    		return this.prettyName;
    	}

    	@Override
    	public String getIdentifier() {
    		return this.countryCode;
    	}

    	@Override
		public int compareTo(Country ci) {
    		int c = 0;
			if (this.order == null) {
				c = -1;
			} else if (ci == null || ci.order == null) {
				c = 1;
			} else if (this.countryCode.equals(agentCountry.countryCode)) {
				c = -1;
			} else if (ci.countryCode.equals(agentCountry.countryCode)) {
				c = 1;
			} else {
				c = this.order.compareTo(ci.order);
			}
			return c;
		}
      	
    	public List<CountrySubdivision> getCountrySubdivisionList() {
    		if (this.countrySubdivisionMap == null) {
    			return new ArrayList<CountrySubdivision>();
    		}
    		Set<CountrySubdivision> list1 = new HashSet<CountrySubdivision>(this.countrySubdivisionMap.values());
    		List<CountrySubdivision> list2 = new ArrayList<CountrySubdivision>(list1);
    		Collections.sort(list2);
    		return list2;
    	}
    	
    	public CountrySubdivision getCountrySubdivision(String state) {
    		if (this.countrySubdivisionMap == null) {
    			return BLANK_COUNTRY_SUBDIVISION;
    		} 
    		
    		CountrySubdivision cs = this.countrySubdivisionMap.get(state);
    		if (cs == null) {
    			return BLANK_COUNTRY_SUBDIVISION;
    		} 
    		return cs;
    	}
    }
    
    public static class CountrySubdivision extends DWComboBoxItem implements Comparable<CountrySubdivision>, Serializable {
    	private static final long serialVersionUID = 1L;

    	private String countrySubdivisionName;
    	private String countrySubdivisionCode;
    	private String prettyName;
    	private Country country;
    	
    	public CountrySubdivision() {
    		this.country = BLANK_COUNTRY;
    	}
		
    	@Override
		public int compareTo(CountrySubdivision cs) {
			return this.countrySubdivisionName.compareTo(cs.countrySubdivisionName);
		}
		
    	@Override
		public String getLabel() {
			return this.prettyName;
		}
		
    	@Override
		public String getValue() {
			return this.countrySubdivisionCode;
		}
    	
    	public String getLegacyCode() {
			String code = null;
			if (this.getCountrySubdivisionCode() != null) {
				int i = this.getCountrySubdivisionCode().indexOf('-') + 1;
				if (i >= 0) {
					code = this.getCountrySubdivisionCode().substring(i);
				}
			}
			return code;
    	}
		
    	@Override
		public String getLabelShort() {
			return getLabel();
		}
		
    	@Override
		public String getIdentifier() {
			return this.countrySubdivisionCode;
		}

    	@Override
		public String toString() {
			return getLabel();
		}

		public String getCountrySubdivisionName() {
			return countrySubdivisionName;
		}

		public String getCountrySubdivisionCode() {
			return countrySubdivisionCode;
		}
		
		public String getSubdivisionCode() {
			int i = countrySubdivisionCode.indexOf("-");
			if (i >= 0) {
				return countrySubdivisionCode.substring(i + 1);
			} else {
				return countrySubdivisionCode;
			}
		}

		public String getPrettyName() {
			return prettyName;
		}

		public void setPrettyName(String prettyName) {
			this.prettyName = prettyName;
		}

		public void setCountrySubdivisionName(String countrySubdivisionName) {
			this.countrySubdivisionName = countrySubdivisionName;
		}

		public void setCountrySubdivisionCode(String countrySubdivisionCode) {
			this.countrySubdivisionCode = countrySubdivisionCode;
		}
		
    	@Override
		public boolean equals(Object o) {
    		if (! (o instanceof CountrySubdivision)) {
    			return false;
    		}
    		CountrySubdivision cs = (CountrySubdivision) o;
    		return ((this.country.countryCode.equals(cs.country.countryCode)) && (this.countrySubdivisionCode.equals(cs.countrySubdivisionCode)));
		}

		@Override
		public int hashCode() {
			return countrySubdivisionCode.hashCode();
		}
    } 
    
    public static class CountryDialCode extends DWComboBoxItem implements Comparable<CountryDialCode>, Serializable {
    	private static final long serialVersionUID = 1L;

    	private String countryDialCode;
    	private Country country;
    	
    	public CountryDialCode() {
    		this.country = BLANK_COUNTRY;
    	}
		
		@Override
    	public int compareTo(CountryDialCode cdc) {
    		
    		int c = 0;
    		if (agentCountry.countryCode.equals(this.country.countryCode)) {
    			c = -1;
    		} else if (agentCountry.countryCode.equals(cdc.country.countryCode)) {
    			c = 1;
    		} else {
    			c = this.country.countryName.compareTo(cdc.country.countryName);
        		if (c == 0) {
    				c = this.countryDialCode.compareTo(cdc.countryDialCode);
        		}
    		}
    		return c;
		}
		
    	@Override
		public String getLabel() {
    		return this.country.countryName + " - " + countryDialCode;
		}
		
    	@Override
		public String getValue() {
			return this.countryDialCode;
		}
		
    	@Override
		public String getLabelShort() {
			return getLabel();
		}
		
    	@Override
		public String getIdentifier() {
			return this.countryDialCode;
		}

    	@Override
		public String toString() {
			return getLabel();
		}

		public String getCountryDialCode() {
			return countryDialCode;
		}
		
		public String getDialCode() {
			return countryDialCode;
		}

		public void setCountryDialCode(String countryDialCode) {
			this.countryDialCode = countryDialCode;
		}
		
    	@Override
		public boolean equals(Object o) {
    		if (! (o instanceof CountryDialCode)) {
    			return false;
    		}
    		CountryDialCode cdc = (CountryDialCode) o;
    		return ((this.country.countryCode.equals(cdc.country.countryCode)) && (this.countryDialCode.equals(cdc.countryDialCode)));
		}

		@Override
		public int hashCode() {
			return countryDialCode.hashCode();
		}
    }

    private CountryInfo() { }

    private static void setAgentCountry() {
    	if (agentCountry == null) {
			agentCountry = CountryInfo.getAgentCountry();
    	}
    }

	public static void parseCountryInfo(List<com.moneygram.agentconnect.CountryInfo> countryInfoTags, String newCountryVersion) {
		
		String language = UnitProfile.getInstance().get("LANGUAGE", "en");
		
		CountryData data = getCountryData();
		data.lastTimeUpdated = System.currentTimeMillis();
		data.updated = true;
		
		String oldCountryVersion = data.countryVersion;
		
		if ((oldCountryVersion != null) && (newCountryVersion.equals(oldCountryVersion))) {
			Debug.println("Country data not updated because the response version of " + oldCountryVersion + " is the same as the request version");
			return;
		}

		if ((countryInfoTags == null) || (countryInfoTags.size() == 0)) {
			Debug.println("Country information for " + language + " up-to-date (version " + newCountryVersion + ")");
			return;
		}
		
		data.countryVersion = newCountryVersion;
		data.countrySubdivisionVersion = null;
		Debug.println("Updating country information for " + language + " to version " + newCountryVersion);

		data.countryMap.clear();
		data.countryDialCodeList.clear();
		data.countrySubdivisionMap.clear();
		data.lastTimeUpdated = System.currentTimeMillis();

		data.language = language;
		
		Integer order = Integer.valueOf(1);
		for (com.moneygram.agentconnect.CountryInfo countryInfoTag : countryInfoTags) {
			String countryCode = countryInfoTag.getCountryCode();
			String countryName = countryInfoTag.getCountryName();
			String baseCurrency = countryInfoTag.getBaseCurrency();

			Country ci = new Country();
			data.countryMap.put(countryCode, ci);
			data.countryMap.put(countryName, ci);

			ci.countryCode = countryCode;
			ci.countryName = countryName;
			ci.baseReceiveCurrency = baseCurrency;			
			
			LookupKeys lookupKeys = countryInfoTag.getLookupKeys();
			if (lookupKeys != null) {
				ci.lookupKeys = lookupKeys.getLookupKey();
			} else {
				ci.lookupKeys = new ArrayList<String>();
			}
			
			ReceiveCurrencies receiveCurrencies = countryInfoTag.getReceiveCurrencies();
			if (receiveCurrencies != null) {
				ci.receiveCurrencies = receiveCurrencies.getReceiveCurrency();
			} else {
				ci.receiveCurrencies = new ArrayList<String>();
			}
			
			CountryDialCodes countryDialCodes = countryInfoTag.getCountryDialCodes();
			if (countryDialCodes != null) {
				for (String dialCode : countryDialCodes.getCountryDialCode()) {
					CountryDialCode cdc = new CountryDialCode();
					cdc.setCountryDialCode(dialCode);
					cdc.country = ci;
					data.countryDialCodeList.add(cdc);
				}
			}
			
			ci.prettyName = formatPrettyName(ci.countryName, countryCode);
			ci.order = order;
			order = Integer.valueOf(order.intValue() + 1);
		}
		
		setAgentCountry();
		Collections.sort(data.countryDialCodeList);
	}
	
	public static void parseCountrySubdivisions(List<CountrySubdivisionInfo> countrySubdivisionTags, String countrySubdivisionVersion) {
		
		String language = UnitProfile.getInstance().get("LANGUAGE", "en");
		
		CountryData data = getCountryData();
		data.lastTimeUpdated = System.currentTimeMillis();
		data.updated = true;
		
		if (getCountryData().countryMap == null) {
			getCountryData().countryMap = new HashMap<String, Country>();
		}
		
		if (getCountryData().countrySubdivisionMap == null) {
			getCountryData().countrySubdivisionMap = new HashMap<String, CountrySubdivision>();
		}

		if ((countrySubdivisionTags == null) || (countrySubdivisionTags.size() == 0)) {
			if (! getCountryData().countrySubdivisionVersion.equals(countrySubdivisionVersion)) {
				getCountryData().countrySubdivisionVersion = countrySubdivisionVersion;
				updateCache(language);
			}
			Debug.println("Country Suddivision Information for " + language + " up-to-date (Version " + countrySubdivisionVersion + ")");
			return;
		}

		getCountryData().countrySubdivisionVersion = countrySubdivisionVersion;
		Debug.println("Updating country subdivision information for " + language + " to version " + countrySubdivisionVersion);
		
		for (CountrySubdivisionInfo countrySubdivisionTag : countrySubdivisionTags) {
			String countryCode = countrySubdivisionTag.getCountryCode();
			Country country = getCountryData().countryMap.get(countryCode);
			
			if (countrySubdivisionTag.getSubdivisions() != null) {
				for (SubdivisionInfo subdivision : countrySubdivisionTag.getSubdivisions().getSubdivision()) {
					String countrySubdivisionCode = subdivision.getCountrySubdivisionCode();
					
					if (country != null) {
						CountrySubdivision countrySubdivision = new CountrySubdivision();
						countrySubdivision.country = country;
						countrySubdivision.countrySubdivisionCode = countrySubdivisionCode;
						countrySubdivision.countrySubdivisionName = subdivision.getCountrySubdivisionName();
						countrySubdivision.prettyName = formatPrettyName(countrySubdivision.countrySubdivisionName, countrySubdivision.countrySubdivisionCode);
						if (country.countrySubdivisionMap == null) {
							country.countrySubdivisionMap = new HashMap<String, CountrySubdivision>();
						}
						country.countrySubdivisionMap.put(countrySubdivision.countrySubdivisionCode, countrySubdivision);
						country.countrySubdivisionMap.put(countrySubdivision.countrySubdivisionName.toUpperCase(), countrySubdivision);
						
						getCountryData().countrySubdivisionMap.put(countrySubdivision.countrySubdivisionCode, countrySubdivision);
					} else {
						Debug.println("Country Subdivision " + subdivision.getCountrySubdivisionName() + " could not be attached to country " + countryCode);
					}
				}
			}
		}
		getCountryData().updated = true;
	}
	
	private static String getCacheFileName(String language) {
		if ((Messages.getCurrentLocale().getLanguage() != null) && (! Messages.getCurrentLocale().getLanguage().isEmpty())) {
			return "ci-" + language + ".obj";
		} else {
			return "ci-xx.obj";
		}
	}
	
	private static void updateCache(String language) {
		ExtraDebug.println("Updating country information cache file. Versions = " + countryData.countryVersion + " and " + countryData.countrySubdivisionVersion);
		try {
			DwEncryptionAES oe = new DwEncryptionAES();
			oe.vInitialize(DwEncryptionAES.OFFSET_CI_DATA, getCacheFileName(language));
			oe.vWriteSaveFile(countryData);
		} catch (Exception e) {
			Debug.printException(e);
		}
	}
	
	public static synchronized void checkCountryInformation(int retrievalType) {
		String language = UnitProfile.getInstance().get("LANGUAGE", "en");
		
		// Check if the country information data for the current language is in memory.
		// If it is not, attempt to read it from the cache file.
	
		if ((! getCountryData().language.equals(language)) || (getCountryData().countryMap.isEmpty())) {
			countryData = null;
			File file = new File(getCacheFileName(language));
			if (file.exists() && file.isFile()) {
				DwEncryptionAES oe = new DwEncryptionAES();
				oe.vInitialize(DwEncryptionAES.OFFSET_CI_DATA, file.getName());
				try {
					countryData = (CountryData) oe.readObject();
				} catch (DwEncryptionFileException e) {
					Debug.printException(e);
				}
			}
		}
		
		// If there is not country information in memory check the age.
		
		if (countryData != null) {
			long age = System.currentTimeMillis() - getCountryData().lastTimeUpdated;
			ExtraDebug.println("Country data age: " + age + ", maximum age until refresh: " + TimeUtility.DAY);
			if (age < TimeUtility.DAY) {
				return;
			}
		} else {
			getCountryData();
		}
		
		// Get the current country data by making AC calls.

		synchronized(countryWait) {
	        try {
	        	if (! CountryInfo.reterivalErrorFlag) {
	        		countryWait.setWaitFlag(true);
	        		instance.getCountryInfo(retrievalType, instance, countryData.countryVersion);
	        		while (countryWait.isWaitFlag()) {
	        			countryWait.wait();
	        		}
	        		getCountryData().language = language;
	        	}
	        } catch (Exception e) {
	        	Debug.printException(e);
	        }
		}
		synchronized(countrySubdivisionWait) {
	        try {
	        	if (! CountryInfo.reterivalErrorFlag) {
	        		countrySubdivisionWait.setWaitFlag(true);
					instance.countrySubdivision(retrievalType, instance, countryData.countrySubdivisionVersion);
					while (countrySubdivisionWait.isWaitFlag()) {
						countrySubdivisionWait.wait();
					}
	        	}
	        } catch (Exception e) {
	        	Debug.printException(e);
	        }
		}
		if (countryData.updated) {
			countryData.updated = false;
			updateCache(language);
		}
	}
	
	public static Country getCountry(String countryCode) {
		Country ci = null;
		checkCountryInformation(0);
		
		CountryData data = getCountryData();
		
		if (data.countryMap != null) {
			ci = data.countryMap.get(countryCode);
		}
		return ci;
	}
	
	public static CountrySubdivision getCountrySubdivision(String countrySubdivisionCode) {
		CountrySubdivision csd = null;
		checkCountryInformation(0);
		
		CountryData data = getCountryData();
		
		if (data.countrySubdivisionMap != null) {
			csd = data.countrySubdivisionMap.get(countrySubdivisionCode);
		}
		return csd;
	}
	
	public static Country getAgentCountry(String agentCountry) {
		
		Country ci = null;
		CountryData data = getCountryData();
		
		if ((countryData != null) && (data.countryMap != null)) {
			ci = data.countryMap.get(agentCountry);
		} 
		if (ci == null) {
			ci = DEFAULT_COUNTRY;
		}
		return ci;
	}

	public static String getCountryName(String countryCode) {
		
		checkCountryInformation(0);
		CountryData data = getCountryData();
		
		if (data.countryMap != null) {
			Country ci = data.countryMap.get(countryCode);
			if (ci != null) {
				return ci.countryName;
			}
		}
		return "";
	}

	public static List<Country> getCountries(String lookupKey) {
		checkCountryInformation(0);
		CountryData data = getCountryData();
		
		if ((data.countryMap != null) && (lookupKey != null)) {
			Set<Country> countrySet = new HashSet<Country>();
			for (Country c : data.countryMap.values()) {
				if ((c.lookupKeys != null) && (c.lookupKeys.contains(lookupKey))) {
					countrySet.add(c);
				}
			}
			List<Country> countryList = new ArrayList<Country>(countrySet);
			setAgentCountry();
			Collections.sort(countryList);
			return countryList;
		}
		return null;
	}
	
	public static List<Country> getCountries(String lookupKey, boolean internationalFlag) {
		return (internationalFlag) ? CountryInfo.getCountries(lookupKey) : CountryInfo.getDomesticCountries();
	}
	
	public static List<Country> getCountriesPlusBlank(String lookupKey) {
		List<Country> list = new ArrayList<Country>();
		Country blankCountry = new Country();
		blankCountry.countryName = "";
		blankCountry.countryCode = "";
		blankCountry.prettyName = Messages.getString("DWValues.Select");
		list.add(blankCountry);
		list.addAll(getCountries(lookupKey));
		return list;
	}

	public static List<Country> getDomesticCountries() {
		List<Country> list = new ArrayList<Country>();
		list.add(getCountry("USA"));
		return list;
	}

	public static Country getAgentCountry() {
		
		String countryCode = UnitProfile.getInstance().getAgentCountryIsoCode();
		Country ci = null;
		
		CountryData data = getCountryData();
		
		if ((countryData != null) && (data.countryMap != null)) {
			ci = data.countryMap.get(countryCode);
		}
		if (ci == null) {
			ci = DEFAULT_COUNTRY;
		}
		return ci;
	}

	private void getCountryInfo(final int retrievalType, final CommCompleteInterface callingPage, final String countryVersion) {
		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {

				Boolean rc = Boolean.TRUE;
				try {
					MessageFacade.getInstance().getCountryInfo(retrievalType, countryVersion);
				} catch (Exception e) {
					rc = Boolean.FALSE;
					reterivalErrorFlag = true;
					Debug.printException(e);
				} finally {
					callingPage.commComplete(MoneyGramClientTransaction.GET_COUNTRY_INFO, rc);
				}
				return rc;
			}
		};
		MessageFacade.run(logic, callingPage, MoneyGramClientTransaction.GET_COUNTRY_INFO, Boolean.TRUE);
	}
	
	private void countrySubdivision(final int retrievalType, final CommCompleteInterface callingPage, final String countrySubdivisionVersion) {
		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {

				Boolean rc = Boolean.TRUE;
				try {
					MessageFacade.getInstance().countrySubdivision(retrievalType, countrySubdivisionVersion);
				} catch (Exception e) {
					rc = Boolean.FALSE;
					reterivalErrorFlag = true;
					Debug.printException(e);
				} finally {
					callingPage.commComplete(MoneyGramClientTransaction.GET_COUNTRY_SUBDIVISION, rc);
				}
				return rc;
			}
		};
		MessageFacade.run(logic, callingPage, MoneyGramClientTransaction.GET_COUNTRY_SUBDIVISION, Boolean.TRUE);
	}
	
	@Override
	public void commComplete(int commTag, Object returnValue) {
		if (commTag == MoneyGramClientTransaction.GET_COUNTRY_INFO) {
			synchronized(countryWait) {
				countryWait.setWaitFlag(false);
				countryWait.notifyAll();
			}
		} else if (commTag == MoneyGramClientTransaction.GET_COUNTRY_SUBDIVISION) {
			synchronized(countrySubdivisionWait) {
				countrySubdivisionWait.setWaitFlag(false);
				countrySubdivisionWait.notifyAll();
			}
		}
	}

	public static CountrySubdivision getAgentCountrySubdivision() {
		Country agentCountry = CountryInfo.getAgentCountry();
		CountrySubdivision agentState = null;
        String state = UnitProfile.getInstance().getProfileInstance().find("AGENT_STATE").stringValue();
        
        if ((agentCountry != null) && (agentCountry.countrySubdivisionMap != null) && (state != null)) {
        	agentState = agentCountry.countrySubdivisionMap.get(state);
        }
        if (agentState == null) {
        	agentState = BLANK_COUNTRY_SUBDIVISION;
        }
		return agentState;
	}

	public static List<Country> getCodeTableStateListCountries() {
		Set<Country> countryList = new HashSet<Country>();
		if (getCountryData().countryMap != null) {
			for (Country ci : getCountryData().countryMap.values()) {
				if (ci.getCountrySubdivisionList().size() > 0) {
					countryList.add(ci);
				}
			}
		}
		return new ArrayList<Country>(countryList);
	}
	
	public static CountrySubdivision getBlankCountrySubdivision() {
		return BLANK_COUNTRY_SUBDIVISION;
	}

	public static boolean isAgentKorean() {
		if (isAgentKorean == null) {
			String countryCode = UnitProfile.getInstance().getAgentCountryIsoCode();
			isAgentKorean = Boolean.valueOf(countryCode.equalsIgnoreCase("KOR"));
		}
		return isAgentKorean.booleanValue();
	}
	
	public static Country getDefaultCountry() {
		return DEFAULT_COUNTRY;
	}
	
	private static String formatPrettyName(String name, String code) {

		StringBuffer prettyName = new StringBuffer(name);
		int i = 0;
		while (true) {
			Matcher m = UNICODE_REGULAR_EXPRESSION.matcher(prettyName.toString());
			if (m.find(i)) {
				int i1 = m.start();
				int i2 = m.end();
				try {
					String h = prettyName.substring(i1 + 2, i2);
					char c = (char) Integer.parseInt(h, 16);
					prettyName.replace(i1, i2, String.valueOf(c));
				} catch (Exception e) {
					Debug.printException(e);
					prettyName.replace(i1, i2, "?");
				}
			} else {
				break;
			}
		}

		if (prettyName.length() > 0) {
			if (code.length() == 3) {
				prettyName.append(" - ");
				prettyName.append(code);
			} else {
				i = code.indexOf('-');
				prettyName.insert(0, " - ");
				prettyName.insert(0, code.substring(i + 1));
			}
		}		
		return prettyName.toString();
	}
	
	public static List<Country> getAccessCountryList() {

		List<CountryDialupInfo> tagList = AccessNumbers.getInstance().getDialInfos();
		Map<String, Country> countryMap = new HashMap<String, Country>();

		for (CountryDialupInfo e : tagList) {
			String countryCode = e.getCountryCode();
			if (countryMap.get(countryCode) != null) {
				continue;
			}
			
			Country country = CountryInfo.getCountry(countryCode);
			
			if (country == null) {
				country = new Country();
				country.countryCode = countryCode;
				country.countryName = e.getCountry();
				country.prettyName = country.countryName;
			}			
			countryMap.put(countryCode, country);
		}
		
		List<Country> countryList = new ArrayList<Country>(countryMap.values());
		Collections.sort(countryList, new Comparator<Country>() {
			@Override
			public int compare(Country c1, Country c2) {
				if (c1.countryCode.equals("USA") && (! c2.countryCode.equals("USA"))) {
					return -1;
				} 
				if (! c1.countryCode.equals("USA") && (c2.countryCode.equals("USA"))) {
					return 1;
				} 
				if (c1.countryCode.equals("MEX") && (! c2.countryCode.equals("MEX"))) {
					return -1;
				} 
				if (! c1.countryCode.equals("MEX") && (c2.countryCode.equals("MEX"))) {
					return 1;
				} 
				if (c1.countryCode.equals("MOV") && (! c2.countryCode.equals("MOV"))) {
					return 1;
				} 
				if (! c1.countryCode.equals("MOV") && (c2.countryCode.equals("MOV"))) {
					return -1;
				} 
				return c1.countryCode.compareTo(c2.countryCode);
			}
		});

		return countryList;
	}
	
	public static List<CountrySubdivision> getAccessCountrySubdivisionList(Country country) {
		List<CountryDialupInfo> tagList = AccessNumbers.getInstance().getDialInfos();
		Map<String, CountrySubdivision> countrySubdivisionMap = new HashMap<String, CountrySubdivision>();

		for (CountryDialupInfo e : tagList) {
			String countryCode = e.getCountryCode();
			if (! country.countryCode.equals(countryCode)) {
				continue;
			}
			
			String countrySubdivision = e.getState();
			CountrySubdivision cs = country.countrySubdivisionMap.get(countrySubdivision);
			
			if (cs == null) {
				cs = new CountrySubdivision();
				cs.country = country;
				cs.countrySubdivisionCode = countrySubdivision;
				cs.countrySubdivisionName = countrySubdivision;
				cs.prettyName = countrySubdivision;
			}			
			countrySubdivisionMap.put(countrySubdivision, cs);
		}		
		List<CountrySubdivision> countrySubdivisionList = new ArrayList<CountrySubdivision>(countrySubdivisionMap.values());
		Collections.sort(countrySubdivisionList);

		return countrySubdivisionList;
	}
	
	public static List<CountryDialCode> getCountryDialCodesList() {
        return getCountryData().countryDialCodeList;
	}
	
	public static List<CountrySubdivision> getBlankCountrySubdivisionList() {
		List<CountrySubdivision> list = new ArrayList<CountrySubdivision>();
		list.add(BLANK_COUNTRY_SUBDIVISION);
		return list;
	}
	
	private static CountryData getCountryData() {
		if (countryData == null) {
			countryData = new CountryData();
		}
		return countryData;
	}
	
	/**
	 * Returns the three-letter currency code of the payout currency for the
	 * given country. If the payout currency depends on the delivery option,
	 * returns the payout currency of the first delivery option mentioned in the
	 * CountryInfoResponse. (Which means there's no guarantee which one is returned.)
	 * Returns the empty string if no match found in the CountryInfoResponse.
	 * 
	 * @param code
	 *            Three-letter ISO country code
	 * @return
	 */
	
	public static String getPayoutCurrencyForCountry(String countryCode) {	
		
		Country country = CountryInfo.getCountry(countryCode);
		String receiveCurrency = "";
		
		List<String> receiveCurrencies = country.receiveCurrencies;
		if (receiveCurrencies != null) {
			Iterator<String> itrReceiveCurrencies = receiveCurrencies.iterator();
			while(itrReceiveCurrencies.hasNext()){
				receiveCurrency = (String)itrReceiveCurrencies.next();		
			}				
		}		
		return receiveCurrency;
	}
	
	public static String getPayoutCurrenciesForCountry(String countryCode) {			
		Country country = CountryInfo.getCountry(countryCode);
		StringBuffer receiveCurrency = new StringBuffer();
		
		List<String> receiveCurrencies = country.receiveCurrencies;
		if (receiveCurrencies != null) {
			Iterator<String> itrReceiveCurrencies = receiveCurrencies.iterator();			
			while(itrReceiveCurrencies.hasNext()){
				if(receiveCurrency.length() >0){
						receiveCurrency.append(",");	
						receiveCurrency.append((String)itrReceiveCurrencies.next());
					}	
				else
					receiveCurrency.append((String)itrReceiveCurrencies.next());	
			}
		}
		return receiveCurrency.toString();
	}

	public static boolean isSendCapability(String countryCode) {
		
		Country country = CountryInfo.getCountry(countryCode);
		List<String> lookupKeys = country.lookupKeys;
		if(lookupKeys != null){
			Iterator<String> itrLookupKeys = lookupKeys.iterator();
			while(itrLookupKeys.hasNext()){
				String lookupkey = (String)itrLookupKeys.next();
				if (KEY_SND_ACTIVE.equalsIgnoreCase(lookupkey)) {
					return true;
				}
			}
		}
		return false;				
	}

	public static boolean isReceiveCapability(String countryCode) {
		
		Country country = CountryInfo.getCountry(countryCode);
		List<String> lookupKeys = country.lookupKeys;
		if(lookupKeys != null){
			Iterator<String> itrLookupKeys = lookupKeys.iterator();
			while(itrLookupKeys.hasNext()){
				String lookupkey = (String)itrLookupKeys.next();
				if (KEY_RECV_ACTIVE.equalsIgnoreCase(lookupkey)) {
					return true;					
				}
			}
		}
		return false;				
	}
	
	public static String getIndicativeCountry(String countryCode) {
		
		Country country = CountryInfo.getCountry(countryCode);
		List<String> lookupKeys = country.lookupKeys;
		if(lookupKeys != null){
			Iterator<String> itrLookupKeys = lookupKeys.iterator();
			while(itrLookupKeys.hasNext()){
				String lookupkey = itrLookupKeys.next();
				if (KEY_IND_COUNTRY.equalsIgnoreCase(lookupkey)) {
					return "true";
				}
			}
		}
		return "false";				
	}
}