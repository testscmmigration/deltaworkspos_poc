/*
 * CompanyInfo.java
 *
 * Generated on Fri Aug 20 13:39:33 CDT 2004
 */

package dw.model.adapters;

import java.io.Serializable;

import com.moneygram.agentconnect.ProductVariantType;

import dw.framework.FieldKey;

public class CompanyInfo extends Data implements Serializable {
	private static final long serialVersionUID = 1L;

	public String getAgencyName() {
		return getValue(FieldKey.RECEIVE_AGENTNAME_KEY);
	}

	public String getBillerName() {
		return getValue(FieldKey.BILLER_NAME_KEY);
	}
	
	public void setBillerName(String billerName) {
		putValue(FieldKey.BILLER_NAME_KEY, billerName);
	}

	public void setAgencyName(String agencyName) {
		putValue(FieldKey.RECEIVE_AGENTNAME_KEY, agencyName);
	}

	public String getAgencyCity() {
		return getValue(FieldKey.AGENT_CITY);
	}

	public String getBillerCity() {
		return getValue(FieldKey.BILLER_CITY_KEY);
	}

	public void setAgencyCity(String agencyCity) {
		putValue(FieldKey.AGENT_CITY, agencyCity);
	}

	public void setBillerCity(String billerCity) {
		putValue(FieldKey.BILLER_CITY_KEY, billerCity);
	}

	public String getReceiveCode() {
		return getValue(FieldKey.RECEIVE_CODE_KEY);
	}

	public void setReceiveCode(String receiveCode) {
		putValue(FieldKey.RECEIVE_CODE_KEY, receiveCode);
	}

	public ProductVariantType getProductVariant() {
		String value = getValue(FieldKey.PRODUCTVARIANT_KEY);
		return value != null ? ProductVariantType.fromValue(value) : null;
	}

	public void setProductVariant(ProductVariantType productVariant) {
		putValue(FieldKey.PRODUCTVARIANT_KEY, productVariant.value());
	}

	public String getReceiveAgentID() {
		return getValue(FieldKey.RECEIVE_AGENTID_KEY);
	}
}
