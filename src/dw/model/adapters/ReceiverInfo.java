/*
 * ReceiverInfo.java
 * 
 * $Revision$
 *
 * Copyright (c) 2004 MoneyGram, Inc. All rights reserved
 */

package dw.model.adapters;

import java.io.Serializable;
import java.util.Date;

import dw.framework.FieldKey;
import dw.i18n.FormatSymbols;

/**
 * Adapter for PersonalInfo containing receiver information.
 * 
 * @author Geoff Atkin
 */
public class ReceiverInfo extends Data implements Serializable {
	private static final long serialVersionUID = 1L;

	public String getAddress() {
		return getValue(FieldKey.RECEIVER_ADDRESS_KEY);
	}

	public String getAddress2() {
		return getValue(FieldKey.RECEIVER_ADDRESS2_KEY);
	}
	
	public String getAddress3() {
		return getValue(FieldKey.RECEIVER_ADDRESS3_KEY);
	}

	public String getAddress4() {
		return getValue(FieldKey.RECEIVER_ADDRESS4_KEY);
	}
	
	public String getCity() {
		return getValue(FieldKey.RECEIVER_CITY_KEY);
	}
	
	public String getColonia() {
		return getValue(FieldKey.COLONIA_KEY);
	}

	public String getCountry() {
		return getValue(FieldKey.RECEIVER_COUNTRY_KEY);
	}
	
	public String getCustomerReceiveNumber() {
		return getValue(FieldKey.CUSTOMERRECEIVENUMBER_KEY);
	}

	public String getDeliveryOption() {
		return getValue(FieldKey.SERVICEOPTION_KEY);
	}

	public String getDestCountry() {
		return getValue(FieldKey.DESTINATION_COUNTRY_KEY);
	}
	
	public String getDirection1() {
		return getValue(FieldKey.DIRECTION1_KEY);
	}

	public String getDirection2() {
		return getValue(FieldKey.DIRECTION2_KEY);
	}
	
	public String getDirection3() {
		return getValue(FieldKey.DIRECTION3_KEY);
	}

	public String getFirstName() {
		return getValue(FieldKey.RECEIVER_FIRSTNAME_KEY);
	}

	public String getFullName() {
		String firstName = getValue(FieldKey.RECEIVER_FIRSTNAME_KEY);
		String middleName = getValue(FieldKey.RECEIVER_MIDDLENAME_KEY);
		String lastName1 = getValue(FieldKey.RECEIVER_LASTNAME_KEY);
		String lastName2 = getValue(FieldKey.RECEIVER_LASTNAME2_KEY);
		return this.formatName(firstName, middleName, lastName1, lastName2);
	}
	
	public String getHomePhone() {
		return getValue(FieldKey.RECEIVER_PRIMARYPHONE_KEY);
	}

	public String getLastName() {
		return getValue(FieldKey.RECEIVER_LASTNAME_KEY);
	}
	
	public Date getLegalIDDOB() {
		String value = getValue(FieldKey.RECEIVER_PERSONALID2_ISSUE_YEAR_KEY);
		return FormatSymbols.convertCCYYMMDDToDate(value);
	}

	public String getLegalIDNumber() {
		return getValue(FieldKey.RECEIVER_PERSONALID2_NUMBER_KEY);
	}
	
	public String getLegalIDType() {
		return getValue(FieldKey.RECEIVER_PERSONALID2_TYPE_KEY);
	}

	public String getMiddleName() {
		return getValue(FieldKey.RECEIVER_MIDDLENAME_KEY);
	}
	
	public String getMoneySaverID() {
		return getValue(FieldKey.MGIREWARDSNUMBER_KEY);
	}

	public String getMunicipio() {
		return getValue(FieldKey.MUNICIPIO_KEY);
	}
	
	public String getOccupation() {
		return getValue(FieldKey.RECEIVER_OCCUPATION_KEY);
	}

	public String getOrganization() {
		return getValue(FieldKey.RECEIVER_ORGANIZATION_KEY);
	}
	
	public String getPayoutCurrency() {
		return getValue(FieldKey.PAYOUT_CURRENCY_KEY);
	}

	public String getPhotoIDCountry() {
		return getValue(FieldKey.RECEIVER_PERSONALID1_ISSUE_COUNTRY_KEY);
	}
	
	public String getPhotoIDNumber() {
		return getValue(FieldKey.RECEIVER_PERSONALID1_NUMBER_KEY);
	}

	public String getPhotoIDState() {
		return getValue(FieldKey.RECEIVER_PERSONALID1_COUNTRY_SUBDIVISIONCODE_KEY);
	}
	
	public String getPhotoIDType() {
		return getValue(FieldKey.RECEIVER_PERSONALID1_TYPE_KEY);
	}

	public String getReceiveAgentAbbreviation() {
		return getValue(FieldKey.RECEIVE_AGENTABBREVIATION_KEY);
	}
	
	public String getReceiveAgentID() {
		return getValue(FieldKey.RECEIVE_AGENTID_KEY);
	}

	public String getReceiveCountry() {
		return getValue(FieldKey.RECEIVE_COUNTRY_KEY); 
	}

	public String getReceiveCurrency() {
		return getValue(FieldKey.RECEIVE_CURRENCY_KEY);
	}
	
	public String getRegistrationNumber() {
		return getValue(FieldKey.REGISTRATION_NUMBER_KEY);
	}

	public String getSecondLastName() {
		return getValue(FieldKey.RECEIVER_LASTNAME2_KEY);
	}
	
	public String getSendCurrency() {
		return getValue(FieldKey.SEND_CURRENCY_KEY);
	}

	public String getState() {
		return getValue(FieldKey.RECEIVER_COUNTRY_SUBDIVISIONCODE_KEY);
	}

	public String getZip() {
		return getValue(FieldKey.RECEIVER_POSTALCODE_KEY);
	}

	public void setAddress(String receiverAddress) {
		putValue(FieldKey.RECEIVER_ADDRESS_KEY, receiverAddress);
	}

	public void setAddress2(String string) {
		putValue(FieldKey.RECEIVER_ADDRESS2_KEY, string);
	}

	public void setAddress3(String string) {
		putValue(FieldKey.RECEIVER_ADDRESS3_KEY, string);
	}

	public void setAddress4(String string) {
		putValue(FieldKey.RECEIVER_ADDRESS4_KEY, string);
	}

	public void setCity(String receiverCity) {
		putValue(FieldKey.RECEIVER_CITY_KEY, receiverCity);
	}

	public void setColonia(String string) {
		putValue(FieldKey.COLONIA_KEY, string);
	}

	public void setCountry(String receiverCountry) {
		putValue(FieldKey.RECEIVER_COUNTRY_KEY, receiverCountry);
	}

	public void setCustomerReceiveNumber(String customerReceiveNumber) {
		putValue(FieldKey.MGCUSTOMERRECEIVENUMBER_KEY, customerReceiveNumber);
	}

	public void setFirstName(String string) {
		putValue(FieldKey.RECEIVER_FIRSTNAME_KEY, string);
	}

	public void setHomePhone(String senderHomePhone) {
		putValue(FieldKey.RECEIVER_PRIMARYPHONE_KEY, senderHomePhone);
	}
	public void setLastName(String defaultValue) {
		putValue(FieldKey.RECEIVER_LASTNAME_KEY, defaultValue);
	}

	public void setMessage1(String messageField1) {
		putValue(FieldKey.MESSAGEFIELD1_KEY, messageField1);
	}
	
	public void setMessage2(String string) {
		putValue(FieldKey.MESSAGEFIELD2_KEY, string);
	}

	public void setMiddleName(String string) {
		putValue(FieldKey.RECEIVER_MIDDLENAME_KEY, string);
	}

	public void setMunicipio(String string) {
		putValue(FieldKey.MUNICIPIO_KEY, string);
	}

	public void setSecondLastName(String senderLastName2) {
		putValue(FieldKey.RECEIVER_LASTNAME2_KEY, senderLastName2);
	}

	public void setState(String receiverState) {
		putValue(FieldKey.RECEIVER_COUNTRY_SUBDIVISIONCODE_KEY, receiverState);
	}

	public void setZip(String receiverZipCode) {
		putValue(FieldKey.RECEIVER_POSTALCODE_KEY, receiverZipCode);
	}

	public void setDestCountry(String destCountry) {
		putValue(FieldKey.DESTINATION_COUNTRY_KEY, destCountry);
	}

	public void setDeliveryOptionDisplayName(String deliveryOptionDisplayName) {
		putValue(FieldKey.DELIVERY_OPTION_DISPLAY_NAME_KEY, deliveryOptionDisplayName);
	}

	public String getReceiverAccountDisplayableID() {
		return getValue(FieldKey.DISPLAY_ACCOUNT_ID_KEY);
	}
}
