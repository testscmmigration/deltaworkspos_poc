/*
 * PersonalInfo.java
 * 
 * $Revision$
 *
 * Copyright (c) 2004 MoneyGram, Inc. All rights reserved
 */

package dw.model.adapters;

import java.io.Serializable;
import java.util.Date;

import com.moneygram.agentconnect.LegalIdType;
import com.moneygram.agentconnect.PhotoIdType;

/**
 * Replaces com.travelersexpress.deltaworks.message.info.PersonalInfo,
 * but because the AgentConnect API doesn't use this concept, it can't 
 * be instanciated directly. Methods like getSender() will return a 
 * subclass of this class.
 * 
 * @author Geoff Atkin
 */
public class PersonalInfo extends Data implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String firstName;
	private String lastName;
	private String secondLastName;
	private String middleName;
	private String address;
	private String address2;
	private String address3;
	private String address4;
	private String city;
	private String state;
	private String zip;
	private String homePhone;
	private LegalIdType legalIDType;
	private String legalIDNumber;
	private Date legalIDDOB;
	private PhotoIdType photoIDType;
	private String photoIDNumber;
	private String photoIDState;
	private String photoIDCountry;
	private String occupation;
	private String organization;
	private String moneySaverID;
	private String country;

    /** Note: be careful what you do with this object. */
    public PersonalInfo() {
    }
    
    private void apendNameComponent(StringBuffer pm_sbName, String pm_sComponent) {
    	if (pm_sComponent != null) {
    		String sTrimmedComponent = pm_sComponent.trim();
    		if (sTrimmedComponent.length() > 0) {
    			pm_sbName.append(" ").append(sTrimmedComponent);
    		}
    	}
    }
    
    /** Returns "First Middle Last Last2". */
    public String getFullName() {
        StringBuffer sb = new StringBuffer();
        sb.append(getFirstName());
        apendNameComponent(sb, getMiddleName());
        apendNameComponent(sb, getLastName());
        apendNameComponent(sb, getSecondLastName());
        return sb.toString();
    }
    
	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getSecondLastName() {
		return secondLastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public String getAddress() {
		return address;
	}

	public String getAddress2() {
		return address2;
	}

	public String getAddress3() {
		return address3;
	}

	public String getAddress4() {
		return address4;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public String getZip() {
		return zip;
	}

	public String getHomePhone() {
		return homePhone;
	}

	public LegalIdType getLegalIDType() {
		return legalIDType;
	}

	public String getLegalIDNumber() {
		return legalIDNumber;
	}

	public Date getLegalIDDOB() {
		return legalIDDOB;
	}

	public PhotoIdType getPhotoIDType() {
		return photoIDType;
	}

	public String getPhotoIDNumber() {
		return photoIDNumber;
	}

	public String getPhotoIDState() {
		return photoIDState;
	}

	public String getPhotoIDCountry() {
		return photoIDCountry;
	}

	public String getOccupation() {
		return occupation;
	}

	public String getOrganization() {
		return organization;
	}

	public String getMoneySaverID() {
		return moneySaverID;
	}

	public String getCountry() {
		return country;
	}
}
