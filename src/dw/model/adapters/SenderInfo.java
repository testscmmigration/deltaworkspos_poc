/*
 * SenderInfo.java
 * 
 * $Revision$
 *
 * Copyright (c) 2004 MoneyGram, Inc. All rights reserved
 */

package dw.model.adapters;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.moneygram.agentconnect.KeyValuePairType;

import dw.framework.FieldKey;
import dw.i18n.FormatSymbols;

/**
 * Adapter for PersonalInfo containing sender information.
 * 
 * @author Geoff Atkin
 */
public class SenderInfo extends Data implements Serializable {
	private static final long serialVersionUID = 1L;
	
	String firstName;
	String middleName;
	String lastName1;
	String lastName2;
	String city;
	String state;
	String country;
	
	public SenderInfo(List<KeyValuePairType> values) {
		for (KeyValuePairType pair : values) {
			FieldKey key = FieldKey.key(pair.getInfoKey());
			if (pair.getValue() != null) {
				DataValue value = DataValue.value(pair.getValue().getValue());
				getData().put(key, value);
			}
		}
		
		firstName = getValue(FieldKey.SENDER_FIRSTNAME_KEY);
		if (firstName == null) {
			firstName = "";
		}
		middleName = getValue(FieldKey.SENDER_MIDDLENAME_KEY);
		if (middleName == null) {
			middleName = "";
		}
		lastName1 = getValue(FieldKey.SENDER_LASTNAME_KEY);
		if (lastName1 == null) {
			lastName1 = "";
		}
		lastName2 = getValue(FieldKey.SENDER_LASTNAME2_KEY);
		if (lastName2 == null) {
			lastName2 = "";
		}
		city = getValue(FieldKey.SENDER_CITY_KEY);
		if (city == null) {
			city = "";
		}
		state = getValue(FieldKey.SENDER_COUNTRY_SUBDIVISIONCODE_KEY);
		if (state == null) {
			state = "";
		}
		country = getValue(FieldKey.SENDER_COUNTRY_KEY);
		if (country == null) {
			country = "";
		}
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof SenderInfo) {
			SenderInfo si = (SenderInfo) o;
			return (this.firstName.equals(si.firstName) && this.middleName.equals(si.middleName) && this.lastName1.equals(si.lastName1) && this.lastName2.equals(si.lastName2) && this.city.equals(si.city) && this.state.equals(si.state) && this.country.equals(si.country));
		} else {
			return false;
		}
	}
	
	@Override
	public int hashCode() {
		return firstName.hashCode() & lastName1.hashCode();
	}

	public String getActiveFreqCustCardNumber() {
		return getValue(FieldKey.SENDER_ACTIVE_FREQ_CUST_CARD_NUMBER);
	}

	public String getAddress() {
		return getValue(FieldKey.SENDER_ADDRESS_KEY);
	}

	public String getAddress2() {
		return getValue(FieldKey.SENDER_ADDRESS2_KEY);
	}
	
	public String getAddress3() {
		return getValue(FieldKey.SENDER_ADDRESS3_KEY);
	}

	public String getAddress4() {
		return getValue(FieldKey.SENDER_ADDRESS4_KEY);
	}
	
	public String getCity() {
		return getValue(FieldKey.SENDER_CITY_KEY);
	}

	public String getConsumerId() {
		return getValue(FieldKey.SEND_CONSUMERID_KEY); 
	}
	
	public String getCountry() {
		return getValue(FieldKey.SENDER_COUNTRY_KEY);
	}

	public String getFirstName() {
		return getValue(FieldKey.SENDER_FIRSTNAME_KEY);
	}

	public String getFullName() {
		String firstName = getValue(FieldKey.SENDER_FIRSTNAME_KEY);
		String middleName = getValue(FieldKey.SENDER_MIDDLENAME_KEY);
		String lastName1 = getValue(FieldKey.SENDER_LASTNAME_KEY);
		String lastName2 = getValue(FieldKey.SENDER_LASTNAME2_KEY);
		return this.formatName(firstName, middleName, lastName1, lastName2);
	}
	
	public String getHomePhone() {
		return getValue(FieldKey.SENDER_PRIMARYPHONE_KEY);
	}

	public String getLastName() {
		return getValue(FieldKey.SENDER_LASTNAME_KEY);
	}

	public Date getLegalIDDOB() {
		String value = getValue(FieldKey.SENDER_DOB_KEY);
		if (value != null) {
			return FormatSymbols.convertCCYYMMDDToDate(value);
		} else {
			return null;
		}
	}
	
//	public String getLegalIDExpDate() {
//		return getValue(FieldKey.SENDER_LEGAL_ID_EXP_DATE_KEY);
//	}
//
//	public String getLegalIDIssueDate() {
//		return getValue(FieldKey.SENDER_LEGAL_ID_ISSUE_DATE_KEY);
//	}
	
	public String getLegalIDNumber() {
		return getValue(FieldKey.SENDER_PERSONALID2_NUMBER_KEY);
	}

	public String getLegalIDType() {
		return getValue(FieldKey.SENDER_PERSONALID2_TYPE_KEY);
	}

	public String getMiddleName() {
		return getValue(FieldKey.SENDER_MIDDLENAME_KEY);
	}
	
	public String getMoneySaverID() {
		return getValue(FieldKey.MGIREWARDSNUMBER_KEY); 
	}
	
	public String getOccupation() {
		return getValue(FieldKey.SENDER_OCCUPATION_KEY);
	}
	
	public String getOrganization() {
		return getValue(FieldKey.SENDER_ORGANIZATION);
	}

	public String getPhotoIDCountry() {
		return getValue(FieldKey.SENDER_PERSONALID1_ISSUE_COUNTRY_KEY);
	}
	
	public String getPhotoIDNumber() {
		return getValue(FieldKey.SENDER_PERSONALID1_NUMBER_KEY);
	}

	public String getPhotoIDState() {
		return getValue(FieldKey.SENDER_PERSONALID1_COUNTRY_SUBDIVISIONCODE_KEY);
	}
	
	public String getPhotoIDType() {
		return getValue(FieldKey.SENDER_PERSONALID1_TYPE_KEY);
	}
	
	public String getSecondLastName() {
		return getValue(FieldKey.SENDER_LASTNAME2_KEY);
	}

	public String getState() {
		return getValue(FieldKey.SENDER_COUNTRY_SUBDIVISIONCODE_KEY);
	}

	public String getZip() {
		return getValue(FieldKey.SENDER_POSTALCODE_KEY);
	}

	public void setActiveFreqCustCardNumber(String rewardsCardNumber) {
		putValue(FieldKey.SENDER_ACTIVE_FREQ_CUST_CARD_NUMBER, rewardsCardNumber);
	}

	public void setAddress(String string) {
		putValue(FieldKey.SENDER_ADDRESS_KEY, string);
	}

	public void setCity(String string) {
		putValue(FieldKey.SENDER_CITY_KEY, string);
	}

	public void setConsumerId(String string) {
		putValue(FieldKey.SEND_CONSUMERID_KEY, string);
	}

	public void setCountry(String string) {
		putValue(FieldKey.SENDER_COUNTRY_KEY, string);
	}

	public void setFirstName(String senderFirstName) {
		putValue(FieldKey.SENDER_FIRSTNAME_KEY, senderFirstName);
	}

	public void setHomePhone(String senderHomePhone) {
		putValue(FieldKey.SENDER_PRIMARYPHONE_KEY, senderHomePhone);
	}

	public void setLastName(String senderLastName) {
		putValue(FieldKey.SENDER_LASTNAME_KEY, senderLastName);
	}

	public void setMiddleName(String senderMiddleName) {
		putValue(FieldKey.SENDER_MIDDLENAME_KEY, senderMiddleName);
	}

	public void setMoneySaverID(String autoMoneySaverCard) {
		putValue(FieldKey.MGIREWARDSNUMBER_KEY, autoMoneySaverCard);
	}

	public void setState(String string) {
		putValue(FieldKey.SENDER_COUNTRY_SUBDIVISIONCODE_KEY, string);
	}

	public void setZip(String string) {
		putValue(FieldKey.SENDER_POSTALCODE_KEY, string);
	}
}