package dw.model.adapters;

import java.util.List;
import java.util.Locale;

import com.moneygram.agentconnect.TextTranslationType;

import dw.utility.DWValues;

public final class PromoCodeInfo extends com.moneygram.agentconnect.PromotionInfo {
	
	private static final long serialVersionUID = 1L;

	public boolean hasError() {
		String sErrCode = getPromotionErrorCode();
		return (DWValues.nonZero(sErrCode));
	}

	public boolean isDisplayablePromoCode() {
		String iCat = getPromotionCategoryId();
		return (!hasError()
				&& (!iCat.equals(DWValues.PROMO_CAT_LOYALTY_DISC) && !iCat.equals(DWValues.PROMO_CAT_WM_EMP_DISC)));
	}

	public String getTextForLang(String lang) {
		if (getPromotionErrorMessages().getPromotionErrorMessage() != null) {
			List<TextTranslationType> errorMessages = getPromotionErrorMessages().getPromotionErrorMessage();
			if (errorMessages != null) {
				for (TextTranslationType textTranslation : errorMessages) {
					if ((textTranslation.getLongLanguageCode() != null) && textTranslation.getLongLanguageCode().toUpperCase(Locale.US).startsWith(lang)) {
						return DWValues.handleSpecialChars(textTranslation.getTextTranslation());
					}
				}
			}
		}
		return null;
	}

	public String getPromotionErrorMsg(String pm_sLang) {
		return getTextForLang(pm_sLang.toUpperCase(Locale.US));
	}
}
