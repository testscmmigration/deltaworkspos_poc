package dw.model.adapters;

import java.util.HashMap;
import java.util.Map;

import dw.framework.FieldKey;

public abstract class Data {
	
	public static class DataValue {
		private String value;
		
		private DataValue(String value) {
			this.value = value;
		}
		
		public static DataValue value(String value) {
			return new DataValue(value);
		}
		
		public String getValue() {
			return this.value;
		}
	}
	
	private Map<FieldKey, DataValue> data = new HashMap<FieldKey, DataValue>();
	
	public Map<FieldKey, DataValue> getData() {
		return data;
	}

	public String getValue(FieldKey key) {
		DataValue value = data.get(key);
		return (value != null) ? value.value : null;
	}
	
	public void putValue(FieldKey key, String value) {
		if (value != null) {
			data.put(key, DataValue.value(value));
		}
	}
	
	public void putValue(FieldKey key, DataValue value) {
		if (value != null) {
			data.put(key, value);
		}
	}
	
    public String formatName(String firstName, String middleName, String lastName1, String lastName2) {
    	StringBuffer sb = new StringBuffer();
    	if (firstName != null) {
    		sb.append(firstName);
    	}
		if ((middleName != null) && (! middleName.equals(""))) {
			if (sb.length() > 0) {
				sb.append(" ");
			}
			sb.append(middleName);
		}
		if (lastName1 != null) {
			if (sb.length() > 0) {
				sb.append(" ");
			}
			sb.append(lastName1);
		}
		if ((lastName2 != null) && (! lastName2.equals(""))) {
			if (sb.length() > 0) {
				sb.append(" ");
			}
			sb.append(lastName2);
		}
		return sb.toString();
    }
	
}
