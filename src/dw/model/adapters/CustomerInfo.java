package dw.model.adapters;

import java.io.Serializable;

public class CustomerInfo extends Data implements Serializable {
	private static final long serialVersionUID = 1L;

	private SenderInfo sender;
	private ReceiverInfo receiver;
	private CompanyInfo companyInfo;

	public SenderInfo getSender() {
		return sender;
	}

	public void setSender(SenderInfo sender) {
		this.sender = sender;
	}

	public ReceiverInfo getReceiver() {
		return receiver;
	}

	public void setReceiver(ReceiverInfo receiver) {
		this.receiver = receiver;
	}

	public CompanyInfo getCompanyInfo() {
		return companyInfo;
	}

	public void setCompanyInfo(CompanyInfo companyInfo) {
		this.companyInfo = companyInfo;
	}
}
