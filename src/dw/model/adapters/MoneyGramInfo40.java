/*
 * MoneyGramInfo40.java
 *
 * Generated on Wed Aug 11 14:59:47 CDT 2004
 */

package dw.model.adapters;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.moneygram.agentconnect.AmountInfo;
import com.moneygram.agentconnect.BPValidationResponse;
import com.moneygram.agentconnect.CompleteSessionResponse;
import com.moneygram.agentconnect.FeeInfo;
import com.moneygram.agentconnect.SendValidationResponse;
import com.moneygram.agentconnect.TextTranslationType;

import dw.mgsend.MoneyGramSendTransaction;
import dw.utility.DWValues;

public class MoneyGramInfo40 implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private CompleteSessionResponse commitTransactionInfo;
	private String mgiRewardsNumber;
	private FeeInfo feeInfo;
	private com.moneygram.agentconnect.SendValidationResponse.Payload sendValidationResponsePayload;
	private com.moneygram.agentconnect.BPValidationResponse.Payload bpValidationResponsePayload;
	private com.moneygram.agentconnect.CompleteSessionResponse.Payload commitTransactionInfoPayload;
	
	public void setSendValidationResponsePayload(com.moneygram.agentconnect.SendValidationResponse.Payload payload) {
		sendValidationResponsePayload = payload;
	}
	
	public com.moneygram.agentconnect.SendValidationResponse.Payload getSendValidationResponsePayload() {
		return sendValidationResponsePayload;
	}
	
	public void setBPValidationResponsePayload(com.moneygram.agentconnect.BPValidationResponse.Payload payload) {
		bpValidationResponsePayload = payload;
	}

	public com.moneygram.agentconnect.BPValidationResponse.Payload getBPValidationResponsePayload() {
		return bpValidationResponsePayload;
	}

	/**
	 * @return the commitTransactionInfo
	 */
	public CompleteSessionResponse getCommitTransactionInfo() {
		return commitTransactionInfo;
	}

	public com.moneygram.agentconnect.CompleteSessionResponse.Payload getCommitTransactionInfoPayload() {
		return commitTransactionInfoPayload;
	}

	/**
	 * @param commitTransactionInfo
	 *            the commitTransactionInfo to set
	 */
	public void setCommitTransactionInfo(CompleteSessionResponse commitTransactionInfo) {
		this.commitTransactionInfo = commitTransactionInfo;
		if (commitTransactionInfo.getPayload() != null) {
			this.commitTransactionInfoPayload = commitTransactionInfo.getPayload().getValue();
		}
	}
	
	public BigDecimal getFeeSendMgiOrigNoTax(boolean isBillPay, boolean isFeeInfo) {
		return getDetailSendItemAmt(DWValues.MF_SEND_DETAIL_ORIG_MGI_SEND_FEE, isBillPay, isFeeInfo);
	}
	
	/**
	 * @return the mgiRewardsNumber
	 */
	public String getMgiRewardsNumber() {
		return mgiRewardsNumber;
	}
	
	public String getPartnerConfirmationNumber() {
		return null;
	}
	
	public String getAutoEnrollMoneySaverID() {
		if (getMgiRewardsNumber() != null) {
			return getMgiRewardsNumber();
		} else {
			return null;
		}
	}
	
	public BigDecimal getSaLimitAvailable() {
		return null;
	}
	
	public BigDecimal getFeeSendTotalWithTax(boolean isBillPay, boolean isFeeInfo) {
		return getDetailSendItemAmt(DWValues.MF_SEND_DETAIL_TOTAL_SEND_FEE_TAX, isBillPay, isFeeInfo);
	}
	
	public BigDecimal getFeeSendNonMgi(boolean isBillPay, boolean isFeeInfo) {
		return getDetailSendItemAmt(DWValues.MF_SEND_DETAIL_OTHER_SEND_FEE, isBillPay, isFeeInfo);
	}
	
	private BigDecimal getDetailSendItemAmt(String amountType, boolean isBillPay, boolean isFeeInfo) {
		if (!isFeeInfo) {
			if (isBillPay) {
				if (getBpValidationResponsePayload() != null && getBpValidationResponsePayload().getSendAmounts() != null) {
					List<AmountInfo> amounts = getBpValidationResponsePayload().getSendAmounts().getDetailSendAmounts().getDetailSendAmount();
					return getDetailAmt(amountType, amounts);
				}
			} else {
				if (getSendValidationResponsePayload() != null && getSendValidationResponsePayload().getSendAmounts() != null) {
					List<AmountInfo> amounts = getSendValidationResponsePayload().getSendAmounts().getDetailSendAmounts().getDetailSendAmount();
					return getDetailAmt(amountType, amounts);
				}
			}
		} else {
			if (getFeeInfo() != null && getFeeInfo().getSendAmounts() != null) {
				List<AmountInfo> amounts = getFeeInfo().getSendAmounts().getDetailSendAmounts().getDetailSendAmount();
				return getDetailAmt(amountType, amounts);
			}
		}
		return null;
	}
	
	private BigDecimal getDetailAmt(String amountType, List<AmountInfo> amounts) {
		if (amounts != null) {
			for (AmountInfo amountInfo : amounts) {
				if (amountInfo.getAmountType().equalsIgnoreCase(amountType)) {
					return amountInfo.getAmount();
				}
			}
		}
		return null;
	}
	
	private BigDecimal getDetailRecvItemAmt(String amountType, boolean isBillPay, boolean isFeeInfo) {
		if (!isFeeInfo) {
			if (isBillPay) {
				if (getBpValidationResponsePayload() != null && getBpValidationResponsePayload().getReceiveAmounts() != null) {
					List<AmountInfo> amounts = getBpValidationResponsePayload().getReceiveAmounts().getDetailReceiveAmounts().getDetailReceiveAmount();
					return getDetailAmt(amountType, amounts);
				}
			} else {
				if (getSendValidationResponsePayload() != null && getSendValidationResponsePayload().getSendAmounts() != null) {
					List<AmountInfo> amounts = getSendValidationResponsePayload().getReceiveAmounts().getDetailReceiveAmounts().getDetailReceiveAmount();
					return getDetailAmt(amountType, amounts);
				}
			}
		} else {
			if (getFeeInfo() != null && getFeeInfo().getReceiveAmounts() != null) {
				List<AmountInfo> amounts = getFeeInfo().getReceiveAmounts().getDetailEstimatedReceiveAmounts().getDetailEstimatedReceiveAmount();
				return getDetailAmt(amountType, amounts);
			}
		}
		return null;
	}
	
	/**
	 * @return the feeInfo
	 */
	public FeeInfo getFeeInfo() {
		return feeInfo;
	}
	
	/**
	 * @param feeInfo the feeInfo to set
	 */
	public void setFeeInfo(FeeInfo feeInfo) {
		this.feeInfo = feeInfo;
	}
	
	public BigDecimal getFee(boolean isBillPay, boolean isFeeInfo) {
		return getDetailSendItemAmt(DWValues.MF_SEND_DETAIL_FINAL_MGI_SEND_FEE, isBillPay, isFeeInfo);
	}
	
	public com.moneygram.agentconnect.BPValidationResponse.Payload getBpValidationResponsePayload() {
		return bpValidationResponsePayload;
	}

	public BigDecimal getFeeSendTotalOrigNoTax(boolean isBillPay, boolean isFeeInfo) {
		return getDetailSendItemAmt(DWValues.MF_SEND_DETAIL_TOTAL_ORIG_SEND_FEE, isBillPay, isFeeInfo);
	}
	
	public BigDecimal getFeeSendTotalFinalNoTax(boolean isBillPay, boolean isFeeInfo) {
		return getDetailSendItemAmt(DWValues.MF_SEND_DETAIL_FINAL_MGI_SEND_FEE, isBillPay, isFeeInfo);
	}
	
	public BigDecimal getTaxSendMgi(boolean isBillPay, boolean isFeeInfo) {
		return getDetailSendItemAmt(DWValues.MF_SEND_TAX_MGI, isBillPay, isFeeInfo);
	}
	
	public BigDecimal getTaxSendNonMgi(boolean isBillPay, boolean isFeeInfo) {
		return getDetailSendItemAmt(DWValues.MF_SEND_DETAIL_OTHER_SEND_TAX, isBillPay, isFeeInfo);
	}
	
	public BigDecimal getFeeSendMgiCollectedAndTax(boolean isBillPay, boolean isFeeInfo) {
		return getDetailSendItemAmt(DWValues.MF_SEND_AMT_TOTAL_FEE_TAX, isBillPay, isFeeInfo);
	}
	
	public BigDecimal getTotalAmountMgi(boolean isBillPay, boolean isFeeInfo) {
		return getDetailSendItemAmt(DWValues.MF_SEND_AMT_TOTAL_COLLECT_MGI, isBillPay, isFeeInfo);
	}
	
	public BigDecimal getFeeSendNonMgiWithTax(boolean isBillPay, boolean isFeeInfo) {
		return getDetailSendItemAmt(DWValues.MF_SEND_DETAIL_TOTAL_SEND_OTHER_FEE_TAX, isBillPay, isFeeInfo);
	}
	
	public BigDecimal getFeeRecvMgi(boolean isBillPay, boolean isFeeInfo) {
		return getDetailRecvItemAmt(DWValues.MF_RECV_FEE_MGI, isBillPay, isFeeInfo);
	}
	
	public BigDecimal getFeeRecvOther(boolean isBillPay, boolean isFeeInfo) {
		return getDetailRecvItemAmt(DWValues.MF_RECV_FEE_NON_MGI, isBillPay, isFeeInfo);
	}
	
	public BigDecimal getTaxRecvMgi(boolean isBillPay, boolean isFeeInfo) {
		return getDetailRecvItemAmt(DWValues.MF_RECV_TAX_MGI, isBillPay, isFeeInfo);
	}
	
	public BigDecimal getTaxRecvOther(boolean isBillPay, boolean isFeeInfo) {
		return getDetailRecvItemAmt(DWValues.MF_RECV_TAX_NON_MGI, isBillPay, isFeeInfo);
	}
	
	/** 
	 * The value of one unit of the sender country's transaction currency
	 * in the destination country's transaction currency. 
	 */
	public String getForeignExchange() {
		return MoneyGramSendTransaction.applyDoddFrank(getSendValidationResponsePayload().getExchangeRateApplied() + "");
	}
	
	private String getTextForLang(String lang, List<TextTranslationType> errorMessages) {
		if (errorMessages != null) {
			for (TextTranslationType textTranslation : errorMessages) {
				if (lang.compareToIgnoreCase(textTranslation.getLongLanguageCode()) == 0) {
					return DWValues.handleSpecialChars(textTranslation.getTextTranslation());
				}
			}
		}
		return null;
	}
	
	/**
	 * Gets the disclaimer text for a specified language
	 * @param pm_sLang language for which disclaimer text is required
	 */
	public String getRcptDisclosureText(String pm_sLang) {
		if ((getSendValidationResponsePayload() != null) && (getSendValidationResponsePayload().getReceiptInfo() != null) && (getSendValidationResponsePayload().getReceiptInfo().getDisclosureTexts() != null)) {
			StringBuffer sb = new StringBuffer();
			for (TextTranslationType textTranslations : getSendValidationResponsePayload().getReceiptInfo().getDisclosureTexts().getDisclosureText()) {
				sb.append(textTranslations.getTextTranslation());
			}
//			return DWValues.handleSpecialChars(getTextForLang(pm_sLang, getSendValidationResponsePayload().getDisclosureText()));
			return sb.toString();
		}
		return null;
	}
	
	/**
	 * Gets the dynamic receipt text for a specified language
	 * @param pm_sLang language for which disclaimer text is required
	 */
	public String getRcptDynamicText(String pm_sLang) {
		if (getCommitTransactionInfo() != null) {
			return DWValues.handleSpecialChars(getTextForLang(pm_sLang, getCommitTransactionInfoPayload().getReceiptInfo().getReceiptTextInfos().getReceiptTextInfo()));
		}
		return null;
	}
}