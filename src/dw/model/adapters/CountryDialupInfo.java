/*
 * Created on Jan 27, 2005
 *
 */
package dw.model.adapters;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author T014
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CountryDialupInfo", propOrder = { "country", "countryName", "state", "city", "provider", "countryCode",
		"areaCode", "phoneNumber", "scriptName" })
@XmlRootElement(name = "countryDialupInfo")
public class CountryDialupInfo {

	private String country;
	private String state;
	private String city;
	private String provider;
	private String countryCode;
	private String phoneNumber;
	private String scriptName;

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @return the provider
	 */
	public String getProvider() {
		return provider;
	}

	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @return the scriptName
	 */
	public String getScriptName() {
		return scriptName;
	}
}
