package dw.model;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import dw.comm.AgentConnect;
import dw.utility.Debug;

public class XmlUtil<T> {
	private Class<T> type;
	protected URI uri;

	public XmlUtil(Class<T> type) {
		this.type = type;
	}

	protected T load(Class<T> docClass, final InputStream in) {
		T o = null;
		try {
			o = unmarshal(docClass, in);
		} catch (JAXBException ex) {
			ex.printStackTrace();
		}
		return o;
	}

	public T unmarshal(Class<T> docClass, InputStream inputStream) throws JAXBException {
		String packageName = docClass.getPackage().getName();
		JAXBContext jc = JAXBContext.newInstance(packageName);
		Unmarshaller u = jc.createUnmarshaller();
		JAXBElement<T> doc = u.unmarshal(new StreamSource(inputStream), docClass);
		return doc.getValue();
	}

	public T unmarshal(Node node) throws JAXBException {
		return unmarshal(type, node);
	}

	public T unmarshal(Class<T> docClass, Node node) throws JAXBException {
		try {
			return unmarshal(docClass, node, null);
		} catch (ParserConfigurationException e) {
			throw new JAXBException(e);
		} catch (XMLStreamException e) {
			throw new JAXBException(e);
		} catch (FactoryConfigurationError e) {
			throw new JAXBException(e);
		}
	}

	public T unmarshal(Class<T> docClass, Node node, String name)
			throws JAXBException, ParserConfigurationException, XMLStreamException, FactoryConfigurationError {
//		DocumentBuilderFactory icFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
		dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
		dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
		
		DocumentBuilder icBuilder = dbf.newDocumentBuilder();
		Document doc = icBuilder.newDocument();
		Element mainRootElement = doc.createElementNS(AgentConnect.TARGET_NAMESPACE, node.getNodeName());
		doc.appendChild(mainRootElement);
		if (node.getChildNodes() != null) {
			for (int i = 0; i < node.getChildNodes().getLength(); i++) {
				Node cNode = node.getChildNodes().item(i);
				Node copiedNode = doc.importNode(cNode, true);
				doc.getDocumentElement().appendChild(copiedNode);

			}
		}
		String nodeData = nodeToString(doc);
		Reader reader = new StringReader(nodeData);
		XMLInputFactory factory = XMLInputFactory.newInstance(); // Or newFactory()
		XMLStreamReader xsr = factory.createXMLStreamReader(reader);
		xsr.nextTag();
		if (name != null && !name.isEmpty()) {
			while (!xsr.getLocalName().equals(name)) {
				xsr.nextTag();
			}
		}
		JAXBContext jc = JAXBContext.newInstance(docClass);
		Unmarshaller unmarshaller = jc.createUnmarshaller();
		// unmarshaller.setEventHandler(new XmlValidationEventHandler());
		JAXBElement<T> jb = unmarshaller.unmarshal(xsr, docClass);
		xsr.close();
		return jb.getValue();
	}

	public List<T> getInfo(Document doc, String expStr) {
		try {
			NodeList nodes = getNodeList(doc, expStr);
			return getInfo(nodes);
		} catch (JAXBException e) {
			Debug.printException(e);
		}
		return null;
	}

	public List<T> getInfo(NodeList nodes) throws JAXBException {
		if (nodes != null) {
			Debug.println("found nodes");			
			List<T> returnInfos = new ArrayList<T>();
			for (int i = 0; i < nodes.getLength(); i++) {
				Node node = nodes.item(i);
				T obj = unmarshal(type, node);
				returnInfos.add(obj);
			}
			return returnInfos;
		}
		return null;
	}

	public static NodeList getNodeList(Document doc, String expStr) {
		try {
			XPath xPath = getXpath();
			XPathExpression exp = xPath.compile(expStr);
			NodeList nodes = (NodeList) exp.evaluate(doc, XPathConstants.NODESET);
			return nodes;
		} catch (XPathExpressionException e) {
			Debug.printException(e);
		}
		return null;
	}

	public static String getNodeText(Document xmlDocument, String expression) {
		try {
			XPath xPath = getXpath();
			// read a string value
			String str = xPath.compile(expression).evaluate(xmlDocument);
			if (str == null || str.isEmpty()) {
				return null;
			}
			return str;
		} catch (XPathExpressionException e) {
			Debug.printException(e);
			Debug.printStackTrace(e);
		}
		return null;
	}

	private static XPath getXpath() {
		XPath xPath = XPathFactory.newInstance().newXPath();
		return xPath;
	}

	public static void writeTofile(JAXBElement<?> repsonse, Class<?> docClass, String filePath) {
		try {
			String packageName = docClass.getPackage().getName();
			javax.xml.bind.JAXBContext jaxbCtx = javax.xml.bind.JAXBContext.newInstance(packageName);
			javax.xml.bind.Marshaller marshaller = jaxbCtx.createMarshaller();
			marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_ENCODING, "UTF-8"); // NOI18N
			marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			File file = new File(filePath);
			marshaller.marshal(repsonse, file);
		} catch (javax.xml.bind.JAXBException ex) {
			Debug.printException(ex);
		}
	}

	public static StringBuffer sanitizeXml(StringBuffer buffer, String s) {
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c == '&')
				buffer.append("&amp;");
			else if (c == '<')
				buffer.append("&lt;");
			else if (c == '>')
				buffer.append("&gt;");
			else if (c > 127)
				buffer.append("&#x").append(Integer.toHexString(c).toUpperCase(Locale.US)).append(";");
			else
				buffer.append(c);
		}
		return buffer;
	}

	public static String nodeToString(Node node) {
		StringWriter sw = new StringWriter();
		try {
			Transformer t = TransformerFactory.newInstance().newTransformer();
			t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			t.setOutputProperty(OutputKeys.INDENT, "yes");
			t.transform(new DOMSource(node), new StreamResult(sw));
		} catch (TransformerException te) {
			Debug.println("nodeToString Transformer Exception");
		}
		return sw.toString();
	}

	public static Document loadFile(String filename, String resource, String error) {
		File f = null;
		Document result = null;

		try {
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
			dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
			dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
			
			
			f = new File(filename);
			if (f.exists()) {
				result = dbf.newDocumentBuilder().parse(f);
				if (result == null) {
					Debug.println(error);
				}
			}
			if (result == null) {
				String path = resource;
				InputStream in = XmlUtil.class.getResourceAsStream(path);
				if (in != null) {
					result = dbf.newDocumentBuilder().parse(in);
				}
			}
			if (result == null) {
				String path = resource;
				if (!resource.startsWith("/")) {
					path = "/" + resource;
				}
				InputStream in = XmlUtil.class.getResourceAsStream(path);
				if (in != null) {
					result = dbf.newDocumentBuilder().parse(in);
				}
			}

			if (result == null) {
				String path = resource;
				InputStream in = XmlUtil.class.getClassLoader().getResourceAsStream(path);
				if (in != null) {
					result = dbf.newDocumentBuilder().parse(in);
				}
			}

			if (result == null) {
				String path = resource;
				if (!resource.startsWith("/")) {
					path = "/" + resource;
				}
				InputStream in = XmlUtil.class.getClassLoader().getResourceAsStream(path);
				result = dbf.newDocumentBuilder().parse(in);
			}
			if (result != null) {
				result.getDocumentElement().normalize();
			}
		} catch (SAXException e) {
			Debug.printException(e);
		} catch (IOException e) {
			Debug.printException(e);
		} catch (ParserConfigurationException e) {
			Debug.printException(e);
		}
		return result;
	}
	/* Used for debuging JAXB **/

	/*public static class XmlValidationEventHandler implements ValidationEventHandler {
	
		public boolean handleEvent(ValidationEvent event) {
			Debug.println("\nEVENT");
			Debug.println("SEVERITY:  " + event.getSeverity());
			Debug.println("MESSAGE:  " + event.getMessage());
			Debug.println("LINKED EXCEPTION:  " + event.getLinkedException());
			Debug.println("LOCATOR");
			Debug.println("    LINE NUMBER:  " + event.getLocator().getLineNumber());
			Debug.println("    COLUMN NUMBER:  " + event.getLocator().getColumnNumber());
			Debug.println("    OFFSET:  " + event.getLocator().getOffset());
			Debug.println("    OBJECT:  " + event.getLocator().getObject());
			Debug.println("    NODE:  " + event.getLocator().getNode());
			Debug.println("    URL:  " + event.getLocator().getURL());
			return true;
		}
	}*/

}