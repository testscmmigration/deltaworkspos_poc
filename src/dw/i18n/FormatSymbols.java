/*
 * FormatSymbols.java
 * 
 * Copyright (c) 2005 MoneyGram International
 */

package dw.i18n;

import java.text.DateFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import dw.model.adapters.CountryInfo;
import dw.utility.Currency;
import dw.utility.Debug;
import dw.utility.TimeUtility;

/**
 * Current agent location's currency characters, currency precision, and 
 * currency. Send currency is determined by the AGENT_CURRENCY value in the
 * profile; currency precision depends on that currency.  The symbols used
 * for decimal points and date field separators depend on the operating
 * system locale, which is not intended to be editable in simplepm or profile
 * editor.
 *
 * @author Scott Kostelecky
 * @author Renuka Easwar
 */
public class FormatSymbols {
    
    private static char DECIMAL_SEPERATOR;
    private static int DECIMAL_PRECISION;
    public static boolean invalidDateString = false;

    static {
    	initialize();
    }

    public static void initialize() {
    	DecimalFormatSymbols dfs = new DecimalFormatSymbols(getLocale());
    	DECIMAL_SEPERATOR = dfs.getDecimalSeparator();
        String agentCurrency = CountryInfo.getAgentCountry().getBaseReceiveCurrency();
        DECIMAL_PRECISION = Currency.getCurrencyPrecision(agentCurrency);
    }    
    
    public static String convertDecimal(String value) {

        if (value.indexOf(".") >= 0)
            return value.replace('.', DECIMAL_SEPERATOR);
        else
            return value.replace(DECIMAL_SEPERATOR, '.');
    }

    public static char getDecimalSeperator() {
        return DECIMAL_SEPERATOR;
    }

    public static int getDecimalPrecision() {
        return DECIMAL_PRECISION;
    }

    /** 
     * Returns the operating system locale, which is the same as the Java
     * default locale (because we never change the default).
     */
    public static Locale getLocale() {
        return Locale.getDefault();
    }

    /**
     *  Format a long date/date time instance according to locale.   
     *  dd/mm/yy HH:mmm and mm/dd/yy HH:MM are valid formats.
     *  
     */
    public static String formatDateForLocale(long startTime) {
        Date date = new Date(startTime);
        
        if (CountryInfo.isAgentKorean()) {
        	DateFormat df = new SimpleDateFormat(TimeUtility.KOREAN_DATE_TIME_FORMAT_2);
        	return df.format(date);
        } else {   
        	DateFormat df = DateFormat.getDateTimeInstance(DateFormat.SHORT,DateFormat.SHORT, getLocale());
        	return df.format(date);
        }
    }
    
    public static String formatTimeFor24Hrs(Date date) {
          String retVal ="";
            retVal = new SimpleDateFormat(TimeUtility.TIME_24_ZERO_BASED_HOUR_MINUTE_FORMAT).format(date);
            if(retVal.length() ==5){
            String hh = retVal.substring(0,2);
            try{
            if(Integer.parseInt(hh)==24){
            	retVal="00:"+retVal.substring(3,5);
              }        
            }catch(Exception e){
           	
           }
            }
            return (retVal);
    }

    public static String formatDateTimeForLocaleWithTimeZone(String date) {
    	String value = "";
        try {
        	int i1 = date.lastIndexOf('.'); 
        	int i2 = date.lastIndexOf('-');
        	int i3 = date.lastIndexOf(':');
        	String s = date.substring(0, i1) + ".000-" + date.substring(i2 + 1, i3) + date.substring(i3 + 1);
			Date dt = new SimpleDateFormat(TimeUtility.SERVER_DATE_TIME_FORMAT_1).parse(s);
		    value = FormatSymbols.formatDateTimeForLocaleWithTimeZone(dt);
        } catch (Exception e) {
        	Debug.println("Could not determine the date and time from '" + value + "'");
			Debug.printException(e);
        }
    	return value;
    }

    /**
     * append time zone to the date before returning to caller.
     * @param date
     * @return
     */
    public static String formatDateTimeForLocaleWithTimeZone(Date date) {
    	DateFormat df;
        if (CountryInfo.isAgentKorean()) {
        	df = new SimpleDateFormat(TimeUtility.KOREAN_DATE_TIME_FORMAT_1);
        } else {
        	df = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.LONG, getLocale());
        }
    	return df.format(date);
    }

    /**
     * Return Time as hh:mm for the given locale.
     * @param date
     */
    public static String formatTimeForLocale(Date date) {
    	DateFormat df;
        if (CountryInfo.isAgentKorean()) {
        	df = new SimpleDateFormat(TimeUtility.TIME_12_HOUR_MINUTE_FORMAT);
        } else {   
        	df = DateFormat.getTimeInstance(DateFormat.SHORT, getLocale());
            return df.format(date);
        }
        return df.format(date);
    }

    /**
     * Return Time as hh:mm for the given locale.
     * @param date
     */
    public static String formatTimeForLocale24(Date date) {
        DateFormat df = DateFormat.getTimeInstance(DateFormat.SHORT);
        Date dt = new Date();
        try {
            dt = new SimpleDateFormat(TimeUtility.TIME_24_HOUR_MINUTE_FORMAT).parse(df.format(date));
        }
        catch (ParseException e) {
            Debug.printStackTrace(e);
        }
        return new SimpleDateFormat(TimeUtility.TIME_24_HOUR_MINUTE_FORMAT).format(dt);
    }

    public static String formatDateForLocale(Date date) {
        
    	if (null == date)
            return null;
        else {
            if (CountryInfo.isAgentKorean()) {
            	DateFormat df = new SimpleDateFormat(TimeUtility.KOREAN_DATE_YEAR_MONTH_DAY_FORMAT);
            	return df.format(date);
            } else {
            	DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, getLocale());
            	SimpleDateFormat sdf = (SimpleDateFormat) df;
            	sdf.applyPattern(sdf.toPattern().replaceFirst("yy","yyyy"));
                return df.format(date);
            }
        }
    }

    /**
     * Format a given date as mm/dd hh:mm or dd/mm hh:mm as per locale.
     * @param date
     * @return String
     */
    public static String formatDateTimeForReportDetail(Date date) {
        
    	DateFormat df;
    	DateFormat tf;
    	
        if (CountryInfo.isAgentKorean()) {
        	df = new SimpleDateFormat(TimeUtility.KOREAN_DATE_MONTH_DAY_FORMAT);
        	tf = new SimpleDateFormat(TimeUtility.TIME_12_HOUR_MINUTE_FORMAT);
        } else  {	    	
        	df = DateFormat.getDateInstance(DateFormat.SHORT, getLocale());
        	tf = DateFormat.getTimeInstance(DateFormat.SHORT, getLocale());
        }
        String d1 = df.format(date);
        String d2 = tf.format(date);

        String[] dateValues;
        String seperator;

        if (d1.indexOf("/") > 0) {
            dateValues = d1.split("/");
            seperator = "/";
        }
        else if (d1.indexOf("-") > 0) {
            dateValues = d1.split("-");
            seperator = "-";
        }
        else {
            dateValues = d1.split("\\.");
            seperator = ".";
        }

        if (dateValues[0].length() == 1) dateValues[0] = "0" + dateValues[0];
        if (dateValues[1].length() == 1) dateValues[1] = "0" + dateValues[1];

        String dateTime = dateValues[0] + seperator + dateValues[1] + " " + d2;

        return dateTime;
    }
    
    /**
     * Convert the text entered in a text field such as the dob to a date
     * @author w164
     */

    public static Date convertTextToDate(String text, boolean pm_bAllowFutureDate, Locale locale) {
        if (text.equals("")) {
        	return null;
        }
        DateFormat formatter = DateFormat.getDateInstance(DateFormat.SHORT, locale);
        
        formatter.setLenient(false);
        Date date = null;
        try {
            date = formatter.parse(text);
            if (!pm_bAllowFutureDate && date.after(TimeUtility.currentTime())) {
                invalidDateString = true;
            } else {
                invalidDateString = false;
            }
        }
        catch (ParseException e) {
            Debug.println("This string " + text
                    + " cannot be converted to a date");
            invalidDateString = true;
        }
        return date;
    }

    /**
     * Format a given date as ccyymmdd for messages between POS and Middleware.
     * @author a405
     */

    public static String formatDateNoSeperatorCCYYMMDD(Date date) {
        if (date != null) {
            return new SimpleDateFormat(TimeUtility.DATE_LONG_YEAR_MONTH_DAY_FORMAT).format(date);
        }
        else {
            return null;
        }
    }

    public static Date convertCCYYMMDDToDate(String string) {
        if ((string == null) || (string.isEmpty())) {
            return null;
        } else {

            Date d = null;
            try {
                d = new SimpleDateFormat(TimeUtility.DATE_YEAR_MONTH_SHORT_YEAR_FORMAT).parse(string);
            }
            catch (ParseException e) {
                //e.printStackTrace();
                Debug.println("This string " + string
                        + "cannot be converted to a date");
            }
            return d;
        }
    }
}
