/*
 * Messages.java
 * 
 * Copyright (c) 2004 MoneyGram, Inc. All rights reserved
 */

package dw.i18n;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;

import dw.main.MainPanelDelegate;
import dw.utility.Debug;


/**
 * Provides access to localized text based on the currently selected display
 * language or a selected receipt language. This class stores the selected
 * language as a Locale object, but it is independent of the current operating
 * system locale. The idea is that changing the display language from the main
 * menu or the receipt language from the profile editor should have no effect
 * on such behavior as decimal amount entry (period vs comma) or date field
 * order. 
 * 
 * @author Renuka Easwar
 */
public class Messages {
	
	private static final String dwVersion = "17.11";
	private static final String drop      = "1";

	private static final String[] languagesToTranslate  = {"de", "es", "fr", "cn", "pl", "ru", "pt"};

    private static String BUNDLE_NAME = "xml.i18n.deltaworks"; 

    private static ResourceBundle RESOURCE_BUNDLE;
    private static Locale bundleLocale = null;

    private static Locale english = new Locale("en");	//was Locale.ENGLISH;

    private static Locale french = new Locale("fr");	//was Locale.FRENCH;

    private static Locale german = new Locale("de");	//was Locale.GERMAN;

    private static Locale spanish = new Locale("es");

    private static Locale chinese = new Locale("cn");

    private static Locale polish = new Locale("pl");

    private static Locale russian = new Locale("ru");
    
    private static Locale portuguese = new Locale("pt");

    private static Locale currentLocale;

    private static Locale receiptLocale = Locale.ENGLISH;

    private static String currentLanguage = "";

    /**
     * Default constructor
     */
    private Messages() {
    }

    /**
     *  Return the String value from the properties file
     *  corresponding to the key. 
     */
    public static String getString(String key) {
        Locale locale = getCurrentLocale();

        /*
         * The ResourceBundle Accessor class loads the deltaworks.properties 
         * file corresponding to the Locale set by the Language profile item.
         */
        try {
        	if ((bundleLocale == null) || (! bundleLocale.equals(locale))) {
        		RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME, locale);
        		bundleLocale = locale;
        	}
            return RESOURCE_BUNDLE.getString(key);
        }
        catch (MissingResourceException e) {
            return '!' + key + '!';
        }
        catch (Exception e) { 
        	Debug.println("Error in Resources.getString() " + e);
        	return '!' + key + '!';
        }
    }

    /**
     *  Return the String value from the properties file
     *  corresponding to the key. 
     */
    public static String getReceiptString(String key) {

        try {
        	if ((bundleLocale == null) || (! bundleLocale.equals(receiptLocale))) {
                RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME, receiptLocale);
        		bundleLocale = receiptLocale;
        	}
            return RESOURCE_BUNDLE.getString(key);
        }
        catch (MissingResourceException e) {
            return '!' + key + '!';
        }
    }

    /**
     * Query the UnitProfile for the Language profile item 
     * and set the locale correspondingly. 
     */
    public static Locale getCurrentLocale() {
        if (currentLocale == null) {
            currentLocale = english;
        }
        return currentLocale;
    }

    /**
     * Set the CurrentReceiptLocale to the Language in Unit Profile
     */
    public static void setCurrentReceiptLocale(String receiptLanguage) {
    	String language = "";
    	if (receiptLanguage.length() > 2)
    		language = receiptLanguage.substring(1,3);
    	else    	
    		language = receiptLanguage.substring(1);
        receiptLocale = new Locale(language);
    }

    public static Locale getCurrentReceiptLocale() {
        return receiptLocale;
    }
    
//    /**
//     * Set the CurrentLocale to the Language in Unit Profile
//     * and refresh main Panel and comm dialog.
//     */
//    public static void setCurrentLocale() {
//        setCurrentLocale(UnitProfile.getUPInstance().get("LANGUAGE", "en"));
//    }

    public static void setCurrentLocale(String language) {
        MainPanelDelegate.refreshToggleButton();
        if (!currentLanguage.equals(language)) {
            currentLanguage = language;
            if (language.equals("es"))
                currentLocale = spanish;
            else if (language.equals("de"))
                currentLocale = german;
            else if (language.equals("fr"))
                currentLocale = french;
            else if (language.equals("cn"))
                currentLocale = chinese;
            else if (language.equals("pl"))
                currentLocale = polish;
            else if (language.equals("ru"))
                currentLocale = russian;
            else if (language.equals("pt"))
                currentLocale = portuguese;
            else
                currentLocale = english;
            
            MainPanelDelegate.refreshMainPanel();
            MainPanelDelegate.languageChanged();

        }
    }
    
    public static String getCurrentLanguage() {
    	return currentLanguage;
    }

    /** 
     * Quick program to identify subset of properties file that needs to be 
     * retranslated. Compares two versions of deltaworks.properties, looking 
     * for property keys that are new or that contain changes to the text. 
     */
    public static void main(String[] args) {
        String requestType = args[0];
        
        boolean generateUATList = false;
        Properties previousProps = new Properties();
        Properties currentProps = new Properties();
        Properties changedProps = new Properties();
        Properties UATProps = new Properties();
        Properties UATScreenMap = new Properties();
       
        if (requestType.equals("C")) {
        	if (args.length > 1 && args[1].equals("Y")) 
        			generateUATList = true;
            identifyChanges(previousProps, currentProps, changedProps, UATScreenMap, UATProps, generateUATList);
        }
        else if (requestType.equals("M"))
            mergeFiles();

    }

    public static void identifyChanges(Properties previousProps,
            Properties currentProps, Properties changedProps, Properties UATScreenMap, Properties UATProps, boolean generateUATList) {
    	
        String previousFilename = "C:/translations/" + dwVersion + "/drop" + drop + " changes/deltaworks_en_old.properties";
        String currentFilename  = "C:/translations/" + dwVersion + "/drop" + drop + " changes/deltaworks_en_new.properties";
        String changedFilename  = "C:/translations/" + dwVersion + "/drop" + drop + " changes/deltaworks_en_totranslator.properties";
        
        String screenMapFilename  = "C:/translations/" + dwVersion + "/drop" + drop + " changes/deltaworks_screenmap.properties";
        String changedUATFilename = "C:/translations/" + dwVersion + "/drop" + drop + " changes/deltaworks_en_UAT_totranslator.properties";


        //French Checked in to CVS on 04/12/06

        try {
            previousProps.load(new FileInputStream(previousFilename));
            currentProps.load(new FileInputStream(currentFilename));
            if (generateUATList){
            	UATScreenMap.load(new FileInputStream(screenMapFilename));
            }

            for (Map.Entry<Object, Object> entry : currentProps.entrySet()) {
                String key = (String) entry.getKey();
                String currentValue = (String) entry.getValue();
                String previousValue = previousProps.getProperty(key);
                // previousValue will be null if key wasn't present in old file
                if (!currentValue.equals(previousValue)) {
                    changedProps.setProperty(key, currentValue);
                    if (generateUATList){
                    	String UATkey = key.substring(0, key.indexOf("."));
                    	String UATvalue = UATScreenMap.getProperty(UATkey, UATkey + " Screen Not Found ");
                    	UATProps.setProperty(key, "***" + UATvalue + "***  " + currentValue);
                    }
                    		
                }
                
            }
            changedProps.store(new FileOutputStream(changedFilename),
                "Changes between " + previousFilename + " and "
                        + currentFilename);
            
            if (generateUATList) {
                UATProps.store(new FileOutputStream(changedUATFilename),
                        "Changes between " + previousFilename + " and "
                                + currentFilename + " in UAT helper format");
            }

            Debug.println("****ALL CHANGES IDENTIFIED........RUN COMPLETE !!");

        }
        catch (Exception e) {
            Debug.printStackTrace(e);
        }

    }

    public static void mergeFiles() {

        //Before running make sure the changes file is not in UTF-16 format
        //If it is run: native2ascii -encoding UTF-16 changes_?.txt changes_?.properties
    	//Note: for a Chinese file, a batch file, build\NativeToAscii.bat, to convert it to unicode.

    	for( int ii=0; ii < languagesToTranslate.length; ii++) {
            Properties previousProps = new Properties();
            Properties changedProps = new Properties();
            String previousFilename = "c:/translations/" + dwVersion + "/drop" + drop 
            	+ " results/deltaworks_prev_"+languagesToTranslate[ii]+".properties";
	        String changesFilename = "C:/translations/" + dwVersion + "/drop" + drop 
	        	+ " results/deltaworks_tran_"+languagesToTranslate[ii]+".properties";
	        String mergedFilename = "c:/translations/" + dwVersion + "/drop" + drop 
	        	+ " merged/deltaworks_merged_"+languagesToTranslate[ii]+".properties";
	
	        try {
	            previousProps.load(new FileInputStream(previousFilename));
	            changedProps.load(new FileInputStream(changesFilename));
	
	            for (Map.Entry<Object, Object> entry : changedProps.entrySet()) {
	                String key = (String) entry.getKey();
	                String changedValue = (String) entry.getValue();
	                previousProps.setProperty(key, changedValue);
	            }
	            FileOutputStream mergedFile = new FileOutputStream(mergedFilename);
	            previousProps.store(mergedFile, "Merges between "
	                    + previousFilename + " and " + changesFilename);
	
	            Debug.println("*****Merge process complete !!! ");
	
	        }
	        catch (Exception e) {
	            Debug.printStackTrace(e);
	        }
    	}
    }

    /**
     * Returns a string in the format described by 
     * SimpleDateFormat.toLocalizedPattern(), containing one-letter 
     * abbreviations for the components of a date/time string.
     */
    public static String getDateFormatSymbols() {
        if (getCurrentLocale().getLanguage().equals(french.getLanguage()))
            return "GaMjkHmdSEDFwWahKzZ";
        else if (getCurrentLocale().getLanguage().equals(german.getLanguage()))
            return "GjMtkSmzSEDFwWahKzZ";
        else if (getCurrentLocale().getLanguage().equals(spanish.getLanguage()))
            return "GaMdkHmsSEDFwWahKzZ";
        else if (getCurrentLocale().getLanguage().equals(chinese.getLanguage()))
            return "GyMdkHmsSEDFwWahKzZ";	//See http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6289803
        else if (getCurrentLocale().getLanguage().equals(polish.getLanguage()))
            return "GjMtkSmzSEDFwWahKzZ";
        else if (getCurrentLocale().getLanguage().equals(russian.getLanguage()))
            return "GjMtkSmzSEDFwWahKzZ";
        else if (getCurrentLocale().getLanguage().equals(portuguese.getLanguage()))
            return "GjMtkSmzSEDFwWahKzZ";
        else
            return "GyMdkHmsSEDFwWahKzZ";
    }

}