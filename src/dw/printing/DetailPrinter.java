package dw.printing;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.xml.sax.Attributes;

import dw.io.report.MoneyGramDetail;
import dw.io.report.MoneyGramReverseDetail;
import dw.io.report.ReportDetail;
import dw.profile.ProfileAccessor;
import dw.profile.UnitProfile;
    
public abstract class DetailPrinter {
	 private int MONEY_GRAM_SEND_DOC_SEQ = 3;
	 private int MONEY_GRAM_RECEIVE_DOC_SEQ = 4;
	 private int BILL_PAYMENT_DOC_SEQ = 5;
    private String xmlFileName;
    private boolean noDetailFlag = true;
    private boolean receiveCurrencySummary = false;
    private boolean sendCurrencySummary = false;
    private boolean sendReversalCurrencySummary = false;
    boolean superAgentMode = false;
    protected Report report;
    private Collection<ReportDetail> detailItems;
        
    public DetailPrinter(   
    		final Report report,
    		final Collection<ReportDetail> detailItems,
    		final Attributes atts) {
        this.report = report;
        this.detailItems = detailItems;
        xmlFileName = atts.getValue("xmlFileName"); 
    }
    
    public DetailPrinter(   
    		final Report report,
            final Collection<ReportDetail> detailItems,
            final Attributes atts,
			final boolean rcs,final boolean scs,boolean src) {
    	
    	this(report, detailItems, atts);
    	this.receiveCurrencySummary = rcs;
    	this.sendCurrencySummary = scs;
    	sendReversalCurrencySummary = src;
    }
    
    public void print(boolean hostReport) {
    	List<List<ReportDetail>> agentMap = new ArrayList<List<ReportDetail>>();
    	List<List<ReportDetail>> currencyMap = new ArrayList<List<ReportDetail>>();
		List<List<List<ReportDetail>>> accumulatedAgentRecord = new ArrayList<List<List<ReportDetail>>>();
		List<ReportDetail> currencyRecord = new ArrayList<ReportDetail>();
		List<ReportDetail> agentRecord = new ArrayList<ReportDetail>();

		final boolean showDetail = report.getRsc().getShowDetail();
		int totalDetailItems = 0;
		noDetailFlag = showDetail;		
		if (report instanceof DispenserItemsReport) {
	        final boolean showDispDetail = report.getRsc().getShowDetail();
	        noDetailFlag = showDetail;
	        if (showDispDetail || receiveCurrencySummary) {
	            final Iterator<ReportDetail> it = detailItems.iterator();
	            while (it.hasNext()) {
	                checkDetail(it.next(),hostReport);
	            }
	        }
	        
	        if (receiveCurrencySummary && (totalDetailItems > 0))
	        	report.addFlag("nosummarydetail", false);
	        else
	        	report.addFlag("nosummarydetail", true);
	        
	        report.addFlag("nodetail",noDetailFlag); 
	        report.addFlag("hasdetail",!noDetailFlag); 
	        footer();
		} 
		else {			
			List<ReportDetail> detailList = new ArrayList<ReportDetail>();
			detailList.addAll(detailItems);
			Collections.sort(detailList, new itemComparator());	
			if (detailList.size() > 0) {			
				formatDetail(
						agentMap, 
						currencyMap, 
						accumulatedAgentRecord,
						currencyRecord,
						agentRecord, 
						detailList);
	
				if (showDetail) {
					showDetail(accumulatedAgentRecord,
							currencyMap,
							agentMap,detailList,hostReport);				
				}
				
			} 
			else {
				printNoDetailLine();
	
			}
		
			if (receiveCurrencySummary || sendCurrencySummary ||sendReversalCurrencySummary ) {
				
				final Iterator<ReportDetail> it = detailItems.iterator();
	          
				while (it.hasNext()) {
					totalDetailItems++;
					checkDetail(it.next(), hostReport);
					
				}
				
				
			}

	  
//			if (receiveCurrencySummary && (totalDetailItems > 0)){
//				report.addFlag("nosummarydetail", false);			
//			}			
//			else
//				report.addFlag("nosummarydetail", true);

			report.addFlag("nodetail", noDetailFlag); 
			report.addFlag("hasdetail", !noDetailFlag); 
			boolean gstSelector = UnitProfile.getInstance()
					.get("ALLOW_TAX_REPORT", "N").equals("Y");
			if (gstSelector) {
				report.addFlag("hasdetailWithTax", !noDetailFlag); 				
			} else {
				report.addFlag("hasdetailNoTax", !noDetailFlag); 					
			}
		}

	}
    
    public void printDoa() {
        final Iterator<ReportDetail> it = detailItems.iterator();
        while (it.hasNext()) {
            checkDetail(it.next(),false);
        }
        footer();   
    }
    
    public void showDetail(
    		List<List<List<ReportDetail>>> accumulatedAgentRecord,
    		List<List<ReportDetail>> currencyMap,
    		List<List<ReportDetail>> agentMap,
			List<ReportDetail> detailList,
			boolean hostReport) {
    	int totalDetailItems = 0;
		report.addFlag("hasdetail", true); 	
		// Display, if the collection has agent id and currency
		if (accumulatedAgentRecord.size() > 0) {
			final Iterator<List<List<ReportDetail>>> agentsItr = accumulatedAgentRecord
					.iterator();
			while (agentsItr.hasNext()) { // agent Iterator
				checkAgentNameHeader(true);
				List<List<ReportDetail>> currencies = agentsItr.next();
				final Iterator<List<ReportDetail>> currItr = currencies.iterator();
				while (currItr.hasNext()) { // currency Iterator
					checkDetailHeader(true);				
					List<ReportDetail> record = currItr.next();
					final Iterator<ReportDetail> recordItr = record.iterator();
					while (recordItr.hasNext()) { //data Iterator
						totalDetailItems++;
						if (totalDetailItems == record.size())
							checkFooter(true);
						else
							checkFooter(false);
						checkDetail(recordItr.next(), hostReport);
						checkDetailHeader(false);
						checkAgentNameHeader(false);

					}
					totalDetailItems = 0;
				}
			}
			currencyMap.clear();
			agentMap.clear();
		}
		// Display, if the collection has no agent id but only currency
		else if (currencyMap.size() > 0) {
			totalDetailItems = 0;
			final Iterator<List<ReportDetail>> currItr = currencyMap.iterator();
			checkAgentNameHeader(true);

			while (currItr.hasNext()) { //currency Iterator
				checkDetailHeader(true);
				List<ReportDetail> record = currItr.next();
				final Iterator<ReportDetail> recordItr = record.iterator();
				while (recordItr.hasNext()) { //data Iterator
					totalDetailItems++;
					if (totalDetailItems == record.size())
						checkFooter(true);
					else
						checkFooter(false);
					checkDetail(recordItr.next(), hostReport);
					checkAgentNameHeader(false);
					checkDetailHeader(false);

				}
				totalDetailItems = 0;
			}
		}
		//Display, if the collection has only agent id but no currency
		else if (agentMap.size() > 0) {
			totalDetailItems = 0;
			final Iterator<List<ReportDetail>> agentItr = agentMap.iterator();
			while (agentItr.hasNext()) {
				checkAgentNameHeader(true);
				checkDetailHeader(true);
				List<ReportDetail> record = agentItr.next();
				final Iterator<ReportDetail> recordItr = record.iterator();
				while (recordItr.hasNext()) {

					totalDetailItems++;
					if (totalDetailItems == record.size())
						checkFooter(true);
					else
						checkFooter(false);
					checkDetail(recordItr.next(), hostReport);
					checkDetailHeader(false);
					checkAgentNameHeader(false);

				}
				totalDetailItems = 0;

			}

		}
		//Display, if the collection has no agent id and no currency
		else {
			totalDetailItems = 0;
			final Iterator<ReportDetail> kt = detailList.iterator();
			checkAgentNameHeader(true);
			checkDetailHeader(true);
			while (kt.hasNext()) { //data
				totalDetailItems++;
				if (totalDetailItems == detailList.size())
					checkFooter(true);
				else
					checkFooter(false);
				checkDetail(kt.next(), hostReport);
				checkDetailHeader(false);
				checkAgentNameHeader(false);

			}
			totalDetailItems = 0;
		}
	
    }
 
    public void formatDetail(
    		List<List<ReportDetail>> agentMap, 
    		List<List<ReportDetail>> currencyMap, 
    		List<List<List<ReportDetail>>> accumulatedAgentRecord, 
    		List<ReportDetail> currencyRecord, 
    		List<ReportDetail> agentRecord, 
    		List<ReportDetail> detailList) {
    	
    	if( detailList.get(0) instanceof MoneyGramDetail){
    		formatMoneyGramDetail(
    				agentMap, 
					currencyMap, 
					accumulatedAgentRecord,
					currencyRecord,
					agentRecord, 
					detailList);
    	}
    	else
    		if( detailList.get(0) instanceof MoneyGramReverseDetail){
    			formatMoneyGramReversalDetail(
    					agentMap, 
   					 	currencyMap, 
   					 	accumulatedAgentRecord,
   						currencyRecord,
   						agentRecord, 
   						detailList);
    		}
    }
    
     public void formatMoneyGramDetail(
    		List<List<ReportDetail>> agentMap, 
    		List<List<ReportDetail>> currencyMap, 
    		List<List<List<ReportDetail>>> accumulatedAgentRecord,
    		List<ReportDetail> currencyRecord,
			List<ReportDetail> agentRecord,
			List<ReportDetail> detailList) {
     	
     	String currCurrency;
		String nextCurrency;
		String currentAgentId;
		String nextAgentId;
		int size = detailList.size();
        //Break the sorted collection based on agent number when the record
		// has agent id
		if (detailList.size() > 1
				&& ((MoneyGramDetail) detailList.get(0)).getAgentID() != null) {
			for (int k = 0; k < size; k++) {
				currentAgentId = ((MoneyGramDetail) detailList.get(k))
						.getAgentID();
				if (k + 1 == size)
					nextAgentId = " ";
				else
					nextAgentId = ((MoneyGramDetail) detailList.get(k + 1))
							.getAgentID();

				if (currentAgentId.equals(nextAgentId)) {
					agentRecord.add(detailList.get(k));

				} else {

					agentRecord.add(detailList.get(k));
					agentMap.add(agentRecord);

					if (k < size)
						agentRecord = new ArrayList<ReportDetail>();

				}

			}
		}

		// Break each agent collection based on currency if the currency is
		// available

		if (agentMap.size() > 0) {
			for (int k = 0; k < agentMap.size(); k++) {
				List<ReportDetail> currencyList = agentMap.get(k);
				if (((MoneyGramDetail) currencyList.get(0))
						.getCurrencyCode() != null) {
					for (int m = 0; m < currencyList.size(); m++) {
						currCurrency = ((MoneyGramDetail) currencyList
								.get(m)).getCurrencyCode();
						if (m + 1 == currencyList.size())
							nextCurrency = " ";
						else
							nextCurrency = ((MoneyGramDetail) currencyList
									.get(m + 1)).getCurrencyCode();

						if (currCurrency.equals(nextCurrency)) {
							currencyRecord.add(currencyList.get(m));

						} else {

							currencyRecord.add(currencyList.get(m));
							currencyMap.add(currencyRecord);
							if (m < currencyList.size())
								currencyRecord = new ArrayList<ReportDetail>();
						}
					}
					accumulatedAgentRecord.add(currencyMap);
					currencyMap = new ArrayList<List<ReportDetail>>();
				}
			}
		}
		//if no agent id present look for currencies and break based on
		// Currency
		else {

			for (int k = 0; k < detailList.size(); k++) {
				if (((MoneyGramDetail) detailList.get(k)).getCurrencyCode() != null) {
					currCurrency = ((MoneyGramDetail) detailList.get(k))
							.getCurrencyCode();
					if (k + 1 == size)
						nextCurrency = " ";
					else {
						nextCurrency = ((MoneyGramDetail) detailList
								.get(k + 1)).getCurrencyCode();

						if (nextCurrency == null) {
							// anomaly when using local record data
							continue;
						}
					}

					if (currCurrency.trim().equals(nextCurrency.trim())) {
						currencyRecord.add(detailList.get(k));

					} else {

						currencyRecord.add(detailList.get(k));
						currencyMap.add(currencyRecord);
						if (k < size)
							currencyRecord = new ArrayList<ReportDetail>();

					}

				}
			}

		}
     }

     public void formatMoneyGramReversalDetail(
    		List<List<ReportDetail>> agentMap, 
    		List<List<ReportDetail>> currencyMap, 
    		List<List<List<ReportDetail>>> accumulatedAgentRecord,
    		List<ReportDetail> currencyRecord,
			List<ReportDetail> agentRecord,
			List<ReportDetail> detailList) {
     	
     	String currCurrency;
		String nextCurrency;
		String currentAgentId;
		String nextAgentId;
		int size = detailList.size();
        //Break the sorted collection based on agent number when the record
		// has agent id
		if (detailList.size() > 1
				&& ((MoneyGramReverseDetail) detailList.get(0)).getAgentID() != null) {
			for (int k = 0; k < size; k++) {
				currentAgentId = ((MoneyGramReverseDetail) detailList.get(k))
						.getAgentID();
				if (k + 1 == size)
					nextAgentId = " ";
				else
					nextAgentId = ((MoneyGramReverseDetail) detailList.get(k + 1))
							.getAgentID();

				if (currentAgentId.equals(nextAgentId)) {
					agentRecord.add(detailList.get(k));

				} else {

					agentRecord.add(detailList.get(k));
					agentMap.add(agentRecord);

					if (k < size)
						agentRecord = new ArrayList<ReportDetail>();

				}
			}
		}

		// Break each agent collection based on currency if the curency is
		// available
		if (agentMap.size() > 0) {
			for (int k = 0; k < agentMap.size(); k++) {
				List<ReportDetail> currencyList = agentMap.get(k);
				if (((MoneyGramReverseDetail) currencyList.get(0))
						.getCurrencyCode() != null) {
					for (int m = 0; m < currencyList.size(); m++) {
						currCurrency = ((MoneyGramReverseDetail) currencyList
								.get(m)).getCurrencyCode();
						if (m + 1 == currencyList.size())
							nextCurrency = " ";
						else
							nextCurrency = ((MoneyGramReverseDetail) currencyList
									.get(m + 1)).getCurrencyCode();

						if (currCurrency.equals(nextCurrency)) {
							currencyRecord.add(currencyList.get(m));

						} else {

							currencyRecord.add(currencyList.get(m));
							currencyMap.add(currencyRecord);
							if (m < currencyList.size())
								currencyRecord = new ArrayList<ReportDetail>();
						}
					}
					accumulatedAgentRecord.add(currencyMap);
					currencyMap = new ArrayList<List<ReportDetail>>();
				}
			}

		}
		//if no agent id present look for currencies and break based on
		// currecy
		else {

			for (int k = 0; k < detailList.size(); k++) {
				if (((MoneyGramReverseDetail) detailList.get(k)).getCurrencyCode() != null) {

					currCurrency = ((MoneyGramReverseDetail) detailList.get(k))
							.getCurrencyCode();
					if (k + 1 == size)
						nextCurrency = " ";
					else
						nextCurrency = ((MoneyGramReverseDetail) detailList
								.get(k + 1)).getCurrencyCode();

					if (currCurrency.trim().equals(nextCurrency.trim())) {
						currencyRecord.add(detailList.get(k));

					} else {

						currencyRecord.add(detailList.get(k));
						currencyMap.add(currencyRecord);
						if (k < size)
							currencyRecord = new ArrayList<ReportDetail>();

					}

				}
			}

		}
     }
     
    protected abstract void checkDetail(final Object detail,boolean hostReport);
    protected abstract void printNoDetailLine();
  
    protected void noDetail(String direction) {   	
    	superAgentMode = UnitProfile.getInstance().isSuperAgent();
		report.addFlag("hasdetail", false); 
		report.addFlag("detailFooterFlag", false); 
		report.addFlag("agentNameHeaderFlag", false);
		report.addField("currency","");
		checkDetailHeader(false);
		ProfileAccessor authObj = ProfileAccessor.getInstance();
		if (direction.equals("send")) {
			if (authObj.getTranAuth(MONEY_GRAM_SEND_DOC_SEQ) || (authObj.getExpressPayAllowed()
					&& authObj.getTranAuth(BILL_PAYMENT_DOC_SEQ))) {
				checkDetailHeader(true);
			}
			else if(superAgentMode){
				checkDetailHeader(true);
				}
			else {
				
				checkDetailHeader(false);

			}
		} else if (direction.equals("receive")) {
			if (authObj.getTranAuth(MONEY_GRAM_RECEIVE_DOC_SEQ)) {
				checkDetailHeader(true);
			} 
			else if(superAgentMode){
				checkDetailHeader(true);
				}
			else {
				checkDetailHeader(false);
			}
		} else if (direction.equals("receiveToCard")) {
			if (authObj.getTranAuth(MONEY_GRAM_RECEIVE_DOC_SEQ)) {
				checkDetailHeader(true);
			} 
			else if(superAgentMode){
				checkDetailHeader(true);
				}
			else {
				checkDetailHeader(false);
			}
		} else if (direction.equals("prepaid")) {
			if (authObj.getTranAuth(BILL_PAYMENT_DOC_SEQ)) {
				checkDetailHeader(true);
			} 
			else if(superAgentMode){
				checkDetailHeader(true);
				}
			else {
				checkDetailHeader(false);

			}
		} else if (direction.equals("utility")) {
			if (authObj.getTranAuth(BILL_PAYMENT_DOC_SEQ)) {
				checkDetailHeader(true);
			}
			else if(superAgentMode){
				checkDetailHeader(true);
			}
			
			else {
				checkDetailHeader(false);

			}
		} else if (direction.equals("reversal")) {
			if ( authObj.getTranAuth(BILL_PAYMENT_DOC_SEQ)
					|| authObj.getTranAuth(MONEY_GRAM_SEND_DOC_SEQ)) {
				checkDetailHeader(true);
			} 
			else if(superAgentMode){
				checkDetailHeader(true);
				}
			else {
				checkDetailHeader(false);

			}
		} else {
			checkDetailHeader(false);
		}
		report.parseFile(xmlFileName);
		checkDetailHeader(false);

	}
        
    protected void printLine() {
        noDetailFlag = false;
        report.parseFile(xmlFileName);
    }
    
    protected void footer() {
    }
    protected void checkFooter(boolean b) {
    }
    protected void checkAgentNameHeader(boolean b) {
    }
    protected void checkDetailHeader(boolean b) {
    }
  
}

class itemComparator implements Comparator<ReportDetail> {
	

	@Override
	public int compare(ReportDetail dr1, ReportDetail dr2) {
		String agentId1 = "";
		String agentId2 = "";
		String currency1 = "";
		String currency2 = "";
		String emp1 = "";
		String emp2 = "";
		long date1 = 0;
		long date2 = 0;
		if (dr1 instanceof MoneyGramReverseDetail) {
			agentId1 = ((MoneyGramReverseDetail) dr1).getAgentID();
			emp1 = ((MoneyGramReverseDetail) dr1).getEmployeeNumber();
			date1 = ((MoneyGramReverseDetail) dr1).getDateTime();
		} else if(dr1 instanceof MoneyGramDetail){

			agentId1 = ((MoneyGramDetail) dr1).getAgentID();
			emp1 = ((MoneyGramDetail) dr1).getEmployeeNumber();
			date1 = ((MoneyGramDetail) dr1).getDateTime();
			currency1 = ((MoneyGramDetail) dr1).getCurrencyCode();
		}
		

		if (dr2 instanceof MoneyGramReverseDetail) {
			agentId2 = ((MoneyGramReverseDetail) dr2).getAgentID();
			emp2 = ((MoneyGramReverseDetail) dr2).getEmployeeNumber();
			date2 = ((MoneyGramReverseDetail) dr2).getDateTime();
		} else if(dr2 instanceof MoneyGramDetail){

			agentId2 = ((MoneyGramDetail) dr2).getAgentID();
			emp2 = ((MoneyGramDetail) dr2).getEmployeeNumber();
			date2 = ((MoneyGramDetail) dr2).getDateTime();
			currency2 = ((MoneyGramDetail) dr2).getCurrencyCode();
		}
		
		int agentIdCompare = compareAgentId(agentId1, agentId2);
		if (agentIdCompare != 0)
			return agentIdCompare;

		int currencyCompare = compareCurrency(currency1, currency2);
		if (currencyCompare != 0)
			return currencyCompare;

		int userCompare = compareUser(emp1, emp2);
		if (userCompare != 0)
			return userCompare;

		int dateCompare = compareTime(Long.valueOf(date1), Long.valueOf(date2));
		if (dateCompare != 0)
			return dateCompare;

		return 0;
	}

//	public boolean equals(Object o) {
//		return this.equals(o);
//	}

	public int compareAgentId(String id1, String id2) {

		if (id1 == null || id2 == null)
			return 0;
		try {
			final int agentCompare = id1.compareTo(id2);
			if (agentCompare != 0)
				return agentCompare;
		} catch (Exception e) {
		}
		return 0;
	}

	public int compareCurrency(String c1, String c2) {
		if (c1 == null || c2 == null)
			return 0;
		try {

			final int currencyCompare = c1.trim().compareTo(c2.trim());
			if (currencyCompare != 0)
				return currencyCompare;
		} catch (Exception e) {
		}
		return 0;
	}

	public int compareUser(String u1, String u2) {

		if (u1 == null || u2 == null)
			return 0;
		try {
			final int userCompare = u1.compareTo(u2);
			if (userCompare != 0)
				return userCompare;
		} catch (Exception e) {
		}
		return 0;
	}

	public int compareTime(Long date1, Long date2) {

		final int dateCompare = date1.compareTo(date2);
		if (dateCompare != 0)
			return dateCompare;
		return 1;
	}
		
}
