package dw.printing;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.comm.CommPort;
import javax.comm.CommPortIdentifier;
import javax.comm.NoSuchPortException;
import javax.comm.ParallelPort;
import javax.comm.PortInUseException;
import javax.comm.UnsupportedCommOperationException;

import dw.dialogs.Dialogs;
import dw.framework.WaitToken;
import dw.i18n.Messages;
import dw.main.MainPanelDelegate;
import dw.profile.UnitProfile;
import dw.utility.Debug;
import dw.utility.ErrorManager;
import dw.utility.ExtraDebug;
import dw.utility.IdleBackoutTimer;
import flags.CompileTimeFlags;

/**
 * Represents the Epson thermal printer (TM-T88II) for printing receipts.
 *
 * NOTE:  Sun's comm.ParallelPort does not return any kind of errors.  
 * Instead the program hangs on the line that sends the data to the printer.
 * (Even C code was not returning the boolean the WinAPI says it's supposed to return.)
 *
 * NOTE:  When running this application, the javax.comm.properties file must be 
 * in the same directory as the comm.jar file, and this must be
 * on the same drive as the application.  Otherwise it will not find the LPT1 port.
 *
 * Consequently, the actual sending of data to the parallel port is done in
 * a separate thread so that the rest of the code can continue.  The thread
 * writes data in buf to the printer periodically (500 millis now).
 *
 * NOTE:  Until Sun's comm.ParallelPort returns error codes, this will only 
 * check its private variable.  See run() and isReady() methods.
 *
 * Note: check port in getInstance() in case it loses it somehow
 * return error codes (needs Sun's driver or C/C++ code)
 * vertical spacing (we now use extra newline for large fonts)
 *
 * @version 3/15/2000
 * @author John Allen
 * Copyright (c) 2000 MoneyGram, Inc.
 */

public final class EpsonTMT88II extends TEPrinter implements Runnable {

    /** Single Instance of this printer */
    private static EpsonTMT88II instance = null;
    private EpsonChecker epsonChecker;

    /** The central class for access to communications ports. */
    private static CommPortIdentifier identifier;

    /** Parallel or serial port. */
    private static volatile CommPort port;

    /** OutputStream associated with the port; the only way to write to it. */
    private OutputStream outPort;

    private static boolean result;

    /** Thread used to do the actual send to the port. */
    private Thread thread;

    /** Used to turn off the thread's run loop. */
    private boolean printerActive;

    /** Period of time the thread waits between each check and sending of data. */
    static final int waitTime = 500;

    private int currentFont;
    
    private WaitToken wait = new WaitToken();
    
//    private int[] logo;         // bitmap in Epson form
    
//    static public boolean logoFound;

    // printer commands (default/package visibility for now)
    static final String INITIALIZE = "\033@";	
    static final String LINE_SPACING_SMALL = "\0333\007";	
    static final String LINE_SPACING_DEFAULT = "\0332";	

    static final String PRINT_LOGO = "\034p\001\000";	
    //static final String EJECT_PAPER = "\035V\102\050\n\n\n";  //feed and cut	
    static final String HORIZONTAL_VERTICAL_MOTION = "\035P\100\100";  //Set horizontal and vertical motion units	
    static final String EJECT_PAPER = "\035V\102\060\033J\044";  //feed and cut	
    static final String TEST_PRINT = "\035(A2012";	
    // fonts
    static final String SET_FONT = "\033!";	
    static final String SET_REVERSE_OFF = "\035B0";	
    static final String SET_REVERSE_ON = "\035B1";	
    // combine by adding:  setFont(FONT_A + EMPHASIZED + DOUBLE_HEIGHT);
    public static final int FONT_A = 0;        // 12 x 24
    public static final int FONT_B = 1;        //  9 x 17
    static final int EMPHASIZED = 8;
    static final int DOUBLE_HEIGHT = 16;
    static final int DOUBLE_WIDTH = 32;
    static final int UNDERLINED = 128;
    static final int REVERSE_ON = 1000;

    // justification
    static final String SET_JUST = "\033a";	
    static final int LEFT = 0;
    static final int CENTERED = 1;
    
    // justification
    private boolean centered = false;
    private boolean paragraph = false;
    private boolean left = false;
    private StringBuffer paragraphSB = null;
    private boolean useCompression = true;


    public static final int FONT_A_CHARS_PER_LINE = 42;
    public static final int FONT_B_CHARS_PER_LINE = 56;

    //-------------------------------------------------------------------------
    // constructor
    //-------------------------------------------------------------------------
    /** 
     * Uses the communications API to open and write to a port connected
     * to the Epson thermal printer (TM-T88II).  It also initializes the printer.
     * @param portName name of the port to open
     * @throws java.io.IOException
     * @throws javax.comm.NoSuchPortException
     * @throws javax.comm.PortInUseException
     * @throws javax.comm.UnsupportedCommOperationException
     */
    public EpsonTMT88II(String portName) throws NoSuchPortException, 
        PortInUseException, IOException, UnsupportedCommOperationException {
        if(port == null){
            identifier = CommPortIdentifier.getPortIdentifier(portName);
            port = identifier.open("EpsonPrinter", 10);
        }
            
        outPort = port.getOutputStream();
        printerActive = true;
        defaultFont();
        initialize();
        thread = new Thread(EpsonTMT88II.this);
        Debug.println("Starting printer Thread..." + thread);	
        thread.start();
        epsonChecker = new EpsonChecker();
        epsonChecker.start();

        // give thread time to check the port
        try {
            Thread.sleep(waitTime*4);  // sleep for 2 seconds
        } catch (InterruptedException e) { }

        if (count > 0 && UnitProfile.getInstance().isDemoMode()){
            // stop the thread from running
            printerActive = false;
        }
       // downloadLogo("temglogo.dat"); 
        //moved to getInstance() so we only download thelogo if the printer is ready.
      //  downloadLogo("moneygramlogo.dat"); 
        setPrinter(this);
        useCompression = UnitProfile.getInstance().useEpsonLineCompression(); 
    }

    /**
     * Create an instance of the Printer and catch the PortInUseException.
     */
    public static boolean isPortInUse() {
        result = false;
        try{
            identifier = CommPortIdentifier.getPortIdentifier("LPT1"); 
            result = identifier.isCurrentlyOwned();
        }
        catch (NoSuchPortException e){
            result = true;
        }
        if (!result)        // if the port is not currently opened,
            try{            // then open it.
                port = identifier.open("EpsonPrinter", 10); 
            }
            catch(PortInUseException piue){}

        return result;


/*        if (instance != null)
            return true;
        try {
            instance = new EpsonTMT88II("LPT1");
        }
        catch (PortInUseException piue) {
            return true;
        }
        catch (Exception e) {
            ErrorManager.handle(e, "printerInstantiationError",	
                                                    ErrorManager.ERROR, true);
        }
        return false;
*/ 
    }

    /**
     * Returns the Singleton object.
     * @return The sole instance of the EpsonTMT88II class.
     */
    public synchronized static EpsonTMT88II getInstance() {
        if (instance == null) {
            try {
                instance = new EpsonTMT88II("LPT1"); 
                if (instance.isReady())
                	instance.downloadLogo("moneygramlogo.dat"); 
            }
            catch (Exception e) {
                ErrorManager.handle(e, "printerInstantiationError", Messages.getString("ErrorMessages.printerInstantiationError"),	
                                                      ErrorManager.ERROR, true);
            }
        }
        return instance;
    }

    public static void clearInstance(){
    	if (instance != null) {
            try {
                Debug.println("ClearInstance - Closing printer on thread..." + instance.thread);	
                instance.close();
                instance.thread = null;		//  Remove reference to thread
            }
            catch (IOException e) {
                ErrorManager.handle(e, "printerCloseError", Messages.getString("ErrorMessages.printerCloseError"), ErrorManager.ERROR, true);	
            }
    	}
    	instance = null;
   }
    
    // CEB: finalize should **not** be used to release system resources, as it
    // cannot be guaranteed to be run before the garbage collector collects
    // this object.  Just some food for thought.
    @Override
	public void finalize() {
        try {
            Debug.println("Finalize - Closing printer");	
            close();
        }
        catch (IOException e) {
            ErrorManager.handle(e, "printerCloseError", Messages.getString("ErrorMessages.printerCloseError"), ErrorManager.ERROR, 	
                                                                        true);
            //Debug.println("Could not close printer on finalize...");	
        }
    }

    /**
     * Check the status of the printer and return the result. If the status is
     *   not ready, display an error dialog and allow the user to retry the
     *   status check. Repeat until they choose to cancel or status becomes ok.
     * @param parent the parent component to use for displaying dialog over
     */
    @Override
	public boolean isReady(javax.swing.JComponent parent) {
        // is the printer ready to go?
        try {
            useCompression = UnitProfile.getInstance().useEpsonLineCompression(); 
        	if (UnitProfile.getInstance().isDemoMode() || isReady())
                return true;
        }
        catch (IOException e) {
            ErrorManager.handle(e, "printerNotReadyError", Messages.getString("ErrorMessages.printerNotReadyError"), ErrorManager.ERROR, true);	
            Debug.printStackTrace(e);
        }

        // Old code was passing a parameter to XmlDefinedDialog.show()
        // telling it to pause the idle backout timer. The replacement
        // for XmlDefinedDialog doesn't have that because it has nothing
        // to do with dialogs and isn't that common. So the pause has 
        // to be done here. --gea
        
        boolean timerRunning = IdleBackoutTimer.isRunning();
        if (timerRunning) 
            IdleBackoutTimer.pause();

        //Dialogs.showWarning("dialogs/DialogPrinterNotReady.xml");	
        int result = Dialogs.showOkCancel("dialogs/DialogPrinterNotReady.xml");
        if (result != 0){
        	// transaction canceled output isi cancel record with substatus code 01
        	MainPanelDelegate.setupCancelISI("01");
	      	if ("Y".equals(UnitProfile.getInstance().get("ISI_ENABLE", "N"))){
	       		  String programName = "";
	       		  if (MainPanelDelegate.getIsiExeProgramArguments() != null)
	       			  programName = MainPanelDelegate.getIsiExeProgramArguments().trim();
	       		  if (programName.length() > 0 && !(programName.startsWith("-EXE"))) {
  	  	  			  MainPanelDelegate.executeIsiProgram(programName);
	       		  }
	       	  }
	       	  MainPanelDelegate.setIsiTransactionInProgress(false);
	       	  MainPanelDelegate.setIsiExeCommand(false);
	       	  MainPanelDelegate.setIsiExe2Command(false);
	       	  MainPanelDelegate.setIsiExeProgramArguments("");
	       	  MainPanelDelegate.setIsiRpCommandValid(false);
        } else {
        	// Retry the check
        	return (isReady(parent));
        }

        if (timerRunning)
            IdleBackoutTimer.start();
        
        return false;
    }

    /* (non-Javadoc)
     * @see dw.printing.TEPrinter#fireOffPrintJob()
     */
    @Override
	public void fireOffPrintJob() {
    }
    
    /**
     * Closes this port and releases any system resources associated with it.
     * It the printer is not hung, it waits till the buffer is empty, 
     * although this object will also hang if its thread is hung.
     * @throws java.io.IOException
     */
    @Override
	public void close() throws IOException {
        try {

            // if no error, wait till buffer is empty
            while (isReady() && count > 0) {
                Thread.sleep(waitTime);
            }
            printerActive = false;      // turn off thread
            epsonChecker.kill();

        } catch (InterruptedException e) { }
        
        // if no error, close port
        if (isReady()) {
            outPort.close();
        }
        super.close();
    }

    /**
     * Checks the buffer and writes anything in it to the printer.
     * The actual write to the printer must be in a separate thread, because
     * it hangs if there is an error.
     */
    @Override
	public void run() {
        while(printerActive) {
            if (count > 0) {
                writeToPort();
            }
            try {
                Thread.sleep(waitTime);
            }
            catch (InterruptedException e) {}
        }
    }

    /* Sends this object's bytes to the port.  Must be synchronized 
     * because of the reset.  */
    private synchronized boolean writeToPort() {
        boolean ret = false;
        try {
            writeTo(outPort);
        }
        catch (IOException e) {
            if (!printerActive && !UnitProfile.getInstance().isDemoMode()){
                ret = true;
                ErrorManager.handle(e, "printerWriteToPortError", Messages.getString("ErrorMessages.printerWriteToPortError"), 	
                                                    ErrorManager.ERROR, true);
            //Debug.println("Error writing bytes to printer:  printerError set.");	
            }
        }
        reset();
        return ret;
    }

    /* Returns number of characters per line with the current font.  Only font
     * a or b and double width is significant.  */
    private int getCharsPerLine() {
        // default to font a
        int r = FONT_A_CHARS_PER_LINE;
        // check if font b
        if (currentFont % 2 != FONT_A) {        // i.e., if odd
            r = FONT_B_CHARS_PER_LINE;
        }

        // check if DOUBLE_WIDTH
        if (currentFont >= REVERSE_ON) {
            currentFont-= REVERSE_ON;
        }
        if (currentFont >= UNDERLINED) {
            currentFont-= UNDERLINED;
        }
        if (currentFont >= DOUBLE_WIDTH) {
            r /= 2;
        }

        return r;
    }

    // ---------------------------------------------------------------------
    // overloaded writes  (methods below call these methods)
    // ---------------------------------------------------------------------

    /**
     * Writes a string to the buffer.
     * @param s = a string to print
     * @throws java.io.IOException
     */
    @Override
	public void write(String s) throws IOException {
//      Debug.println("write(String) length = " + s.length()+",["+s+"]");	
        if (printerActive) {
            if (paragraph){
                paragraphSB.append(s);
            }
            else
                write(convertBytes(s), 0, s.length());
        }
    }

    /** Buffer used by convertBytes, out here for memory efficiency. */
    private byte[] convertBuffer = null;

    /** 
     * Same as s.getBytes("Cp437") without using charsets.jar library. 
     * Not as clean as getBytes(charset), but allows us to reduce the 
     * JRE download size by removing charsets.jar from the download.
     * The code in the loop is called frequently, so it has to be fast
     * and efficient. The return buffer is overwritten on each call,
     * so this method is not thread safe.
     * @author Geoff Atkin
     */
    private byte[] convertBytes(String s) {
        int n = s.length();
        if ((convertBuffer == null) || (n > convertBuffer.length)) {
            convertBuffer = new byte[n];
        }
        for (int i = 0; i < n; i++) {
            char c = s.charAt(i);
            byte b;
            if (c < 0x80) {
                b = (byte) c;
            }
            else {
                b = 0x20;   // default to ASCII space if no mapping
                switch (c) {
                    // generated from cp437.txt
                    case 0x00c7: b = (byte) 0x80; break; // latin capital letter c with cedilla
                    case 0x00fc: b = (byte) 0x81; break; // latin small letter u with diaeresis
                    case 0x00e9: b = (byte) 0x82; break; // latin small letter e with acute
                    case 0x00e2: b = (byte) 0x83; break; // latin small letter a with circumflex
                    case 0x00e4: b = (byte) 0x84; break; // latin small letter a with diaeresis
                    case 0x00e0: b = (byte) 0x85; break; // latin small letter a with grave
                    case 0x00e5: b = (byte) 0x86; break; // latin small letter a with ring above
                    case 0x00e7: b = (byte) 0x87; break; // latin small letter c with cedilla
                    case 0x00ea: b = (byte) 0x88; break; // latin small letter e with circumflex
                    case 0x00eb: b = (byte) 0x89; break; // latin small letter e with diaeresis
                    case 0x00e8: b = (byte) 0x8a; break; // latin small letter e with grave
                    case 0x00ef: b = (byte) 0x8b; break; // latin small letter i with diaeresis
                    case 0x00ee: b = (byte) 0x8c; break; // latin small letter i with circumflex
                    case 0x00ec: b = (byte) 0x8d; break; // latin small letter i with grave
                    case 0x00c4: b = (byte) 0x8e; break; // latin capital letter a with diaeresis
                    case 0x00c5: b = (byte) 0x8f; break; // latin capital letter a with ring above
                    case 0x00c9: b = (byte) 0x90; break; // latin capital letter e with acute
                    case 0x00e6: b = (byte) 0x91; break; // latin small ligature ae
                    case 0x00c6: b = (byte) 0x92; break; // latin capital ligature ae
                    case 0x00f4: b = (byte) 0x93; break; // latin small letter o with circumflex
                    case 0x00f6: b = (byte) 0x94; break; // latin small letter o with diaeresis
                    case 0x00f2: b = (byte) 0x95; break; // latin small letter o with grave
                    case 0x00fb: b = (byte) 0x96; break; // latin small letter u with circumflex
                    case 0x00f9: b = (byte) 0x97; break; // latin small letter u with grave
                    case 0x00ff: b = (byte) 0x98; break; // latin small letter y with diaeresis
                    case 0x00d6: b = (byte) 0x99; break; // latin capital letter o with diaeresis
                    case 0x00dc: b = (byte) 0x9a; break; // latin capital letter u with diaeresis
                    case 0x00a2: b = (byte) 0x9b; break; // cent sign
                    case 0x00a3: b = (byte) 0x9c; break; // pound sign
                    case 0x00a5: b = (byte) 0x9d; break; // yen sign
                    case 0x20a7: b = (byte) 0x9e; break; // peseta sign
                    case 0x0192: b = (byte) 0x9f; break; // latin small letter f with hook
                    case 0x00e1: b = (byte) 0xa0; break; // latin small letter a with acute
                    case 0x00ed: b = (byte) 0xa1; break; // latin small letter i with acute
                    case 0x00f3: b = (byte) 0xa2; break; // latin small letter o with acute
                    case 0x00fa: b = (byte) 0xa3; break; // latin small letter u with acute
                    case 0x00f1: b = (byte) 0xa4; break; // latin small letter n with tilde
                    case 0x00d1: b = (byte) 0xa5; break; // latin capital letter n with tilde
                    case 0x00aa: b = (byte) 0xa6; break; // feminine ordinal indicator
                    case 0x00ba: b = (byte) 0xa7; break; // masculine ordinal indicator
                    case 0x00bf: b = (byte) 0xa8; break; // inverted question mark
                    case 0x2310: b = (byte) 0xa9; break; // reversed not sign
                    case 0x00ac: b = (byte) 0xaa; break; // not sign
                    case 0x00bd: b = (byte) 0xab; break; // vulgar fraction one half
                    case 0x00bc: b = (byte) 0xac; break; // vulgar fraction one quarter
                    case 0x00a1: b = (byte) 0xad; break; // inverted exclamation mark
                    case 0x00ab: b = (byte) 0xae; break; // left-pointing double angle quotation mark
                    case 0x00bb: b = (byte) 0xaf; break; // right-pointing double angle quotation mark
                    case 0x2591: b = (byte) 0xb0; break; // light shade
                    case 0x2592: b = (byte) 0xb1; break; // medium shade
                    case 0x2593: b = (byte) 0xb2; break; // dark shade
                    case 0x2502: b = (byte) 0xb3; break; // box drawings light vertical
                    case 0x2524: b = (byte) 0xb4; break; // box drawings light vertical and left
                    case 0x2561: b = (byte) 0xb5; break; // box drawings vertical single and left double
                    case 0x2562: b = (byte) 0xb6; break; // box drawings vertical double and left single
                    case 0x2556: b = (byte) 0xb7; break; // box drawings down double and left single
                    case 0x2555: b = (byte) 0xb8; break; // box drawings down single and left double
                    case 0x2563: b = (byte) 0xb9; break; // box drawings double vertical and left
                    case 0x2551: b = (byte) 0xba; break; // box drawings double vertical
                    case 0x2557: b = (byte) 0xbb; break; // box drawings double down and left
                    case 0x255d: b = (byte) 0xbc; break; // box drawings double up and left
                    case 0x255c: b = (byte) 0xbd; break; // box drawings up double and left single
                    case 0x255b: b = (byte) 0xbe; break; // box drawings up single and left double
                    case 0x2510: b = (byte) 0xbf; break; // box drawings light down and left
                    case 0x2514: b = (byte) 0xc0; break; // box drawings light up and right
                    case 0x2534: b = (byte) 0xc1; break; // box drawings light up and horizontal
                    case 0x252c: b = (byte) 0xc2; break; // box drawings light down and horizontal
                    case 0x251c: b = (byte) 0xc3; break; // box drawings light vertical and right
                    case 0x2500: b = (byte) 0xc4; break; // box drawings light horizontal
                    case 0x253c: b = (byte) 0xc5; break; // box drawings light vertical and horizontal
                    case 0x255e: b = (byte) 0xc6; break; // box drawings vertical single and right double
                    case 0x255f: b = (byte) 0xc7; break; // box drawings vertical double and right single
                    case 0x255a: b = (byte) 0xc8; break; // box drawings double up and right
                    case 0x2554: b = (byte) 0xc9; break; // box drawings double down and right
                    case 0x2569: b = (byte) 0xca; break; // box drawings double up and horizontal
                    case 0x2566: b = (byte) 0xcb; break; // box drawings double down and horizontal
                    case 0x2560: b = (byte) 0xcc; break; // box drawings double vertical and right
                    case 0x2550: b = (byte) 0xcd; break; // box drawings double horizontal
                    case 0x256c: b = (byte) 0xce; break; // box drawings double vertical and horizontal
                    case 0x2567: b = (byte) 0xcf; break; // box drawings up single and horizontal double
                    case 0x2568: b = (byte) 0xd0; break; // box drawings up double and horizontal single
                    case 0x2564: b = (byte) 0xd1; break; // box drawings down single and horizontal double
                    case 0x2565: b = (byte) 0xd2; break; // box drawings down double and horizontal single
                    case 0x2559: b = (byte) 0xd3; break; // box drawings up double and right single
                    case 0x2558: b = (byte) 0xd4; break; // box drawings up single and right double
                    case 0x2552: b = (byte) 0xd5; break; // box drawings down single and right double
                    case 0x2553: b = (byte) 0xd6; break; // box drawings down double and right single
                    case 0x256b: b = (byte) 0xd7; break; // box drawings vertical double and horizontal single
                    case 0x256a: b = (byte) 0xd8; break; // box drawings vertical single and horizontal double
                    case 0x2518: b = (byte) 0xd9; break; // box drawings light up and left
                    case 0x250c: b = (byte) 0xda; break; // box drawings light down and right
                    case 0x2588: b = (byte) 0xdb; break; // full block
                    case 0x2584: b = (byte) 0xdc; break; // lower half block
                    case 0x258c: b = (byte) 0xdd; break; // left half block
                    case 0x2590: b = (byte) 0xde; break; // right half block
                    case 0x2580: b = (byte) 0xdf; break; // upper half block
                    case 0x03b1: b = (byte) 0xe0; break; // greek small letter alpha
                    case 0x00df: b = (byte) 0xe1; break; // latin small letter sharp s
                    case 0x0393: b = (byte) 0xe2; break; // greek capital letter gamma
                    case 0x03c0: b = (byte) 0xe3; break; // greek small letter pi
                    case 0x03a3: b = (byte) 0xe4; break; // greek capital letter sigma
                    case 0x03c3: b = (byte) 0xe5; break; // greek small letter sigma
                    case 0x00b5: b = (byte) 0xe6; break; // micro sign
                    case 0x03c4: b = (byte) 0xe7; break; // greek small letter tau
                    case 0x03a6: b = (byte) 0xe8; break; // greek capital letter phi
                    case 0x0398: b = (byte) 0xe9; break; // greek capital letter theta
                    case 0x03a9: b = (byte) 0xea; break; // greek capital letter omega
                    case 0x03b4: b = (byte) 0xeb; break; // greek small letter delta
                    case 0x221e: b = (byte) 0xec; break; // infinity
                    case 0x03c6: b = (byte) 0xed; break; // greek small letter phi
                    case 0x03b5: b = (byte) 0xee; break; // greek small letter epsilon
                    case 0x2229: b = (byte) 0xef; break; // intersection
                    case 0x2261: b = (byte) 0xf0; break; // identical to
                    case 0x00b1: b = (byte) 0xf1; break; // plus-minus sign
                    case 0x2265: b = (byte) 0xf2; break; // greater-than or equal to
                    case 0x2264: b = (byte) 0xf3; break; // less-than or equal to
                    case 0x2320: b = (byte) 0xf4; break; // top half integral
                    case 0x2321: b = (byte) 0xf5; break; // bottom half integral
                    case 0x00f7: b = (byte) 0xf6; break; // division sign
                    case 0x2248: b = (byte) 0xf7; break; // almost equal to
                    case 0x00b0: b = (byte) 0xf8; break; // degree sign
                    case 0x2219: b = (byte) 0xf9; break; // bullet operator
                    case 0x00b7: b = (byte) 0xfa; break; // middle dot
                    case 0x221a: b = (byte) 0xfb; break; // square root
                    case 0x207f: b = (byte) 0xfc; break; // superscript latin small letter n
                    case 0x00b2: b = (byte) 0xfd; break; // superscript two
                    case 0x25a0: b = (byte) 0xfe; break; // black square
                    case 0x00a0: b = (byte) 0xff; break; // no-break space
                }                    
            }
            convertBuffer[i] = b;
        }
        return convertBuffer;
    }
    
    // ---------------------------------------------------------------------
    // Methods for the printer itself.
    // ---------------------------------------------------------------------
    
    /**
     * Clears the print buffer and resets modes to power-on defaults.
     * This is called in the constructor but is a separate function so that it
     * can also be called at any time to initialize the printer.
     * @throws java.io.IOException
     */
    @Override
	public void initialize() throws IOException {
        write(INITIALIZE);
        write(HORIZONTAL_VERTICAL_MOTION);
    }

//    /**
//     * Downloads a bit image logo to the printer from a variable.
//     * Epson documentation says this should not be done more than
//     * 10 times a day with this printer.
//     * @throws java.io.IOException
//     */
//    public void downloadLogo() throws IOException {
//        write(logo);    // variable includes command and data
//    }

    /**
     * Download a bit image logo to the printer directly from a file.
     * Epson documentation says this should not be done more than
     * 10 times a day with this printer.
     * @param fname = name of file with a bit image to send to the printer.
     * @throws java.io.IOException
     */
    public void downloadLogo(String fname) throws IOException {
//        write(LOAD_LOGO);     // comment out if file includes command
        try {
            FileInputStream file = new FileInputStream(fname);
            int c;
            while ((c = file.read()) != -1){
                if (printerActive)
                    write(c);
            }
            logoFound = true;
            file.close();
        }
        catch (Exception FileNotFoundException) {
            logoFound = false;
        }
    }

    /**
     * Prints the downloaded logo.
     * @throws java.io.IOException
     */
    @Override
	public void printLogo() throws IOException {
        if (logoFound) {
        	lineSpacingDefault();
        	write(PRINT_LOGO);
        }
    }

    /**
     * Sets the font to one of the static values of this class.
     * This treats the black/white reverse printing more like the others.
     * I.e., it does not remain on until being turned off specifically.
     * It is turned off with any call to this method.
     * @param font = sum of one or more static variables defined above
     * @throws java.io.IOException
     */
    public void setFont(int font) throws IOException {
        currentFont = font;
        write(SET_REVERSE_OFF);
        if (font >= REVERSE_ON) {
            font -= REVERSE_ON;
            write(SET_REVERSE_ON);
        }
        write(SET_FONT);
        if (printerActive)
            write(font);
    }

    /**
     * Returns font.
     */
    public int getFont(){
    	return currentFont;
    }

    /**
     * Sets justification to left, centered, right.
     * @param justif = one or more static variables defined above
     * @throws java.io.IOException
     */
    public void setJustification(int justif) throws IOException {
        write(SET_JUST);
        if (printerActive)
            write(justif);
    }

    /**
     * Ejects and cuts the paper.
     * @throws java.io.IOException
     */
    @Override
	public void eject() throws IOException {
        write(EJECT_PAPER);
        write(INITIALIZE);
    }

    /**
     * Executes a test print (and resets the printer to the power-on defaults).
     * @throws java.io.IOException
     */
    public void testPrint() throws IOException {
        write(TEST_PRINT);
    }

    // ---------------------------------------------------------------------
    // NOTE:  Sun's Win32OS driver underneath the commapi package does not yet
    // read the error conditions.  The program just hangs if it cannot succeed 
    // in the write.
    //
    // (When Sun does get it working, these should also work.)
    //
    // (Even C/C++ code hangs.  Win32OS must depend on the printer subsystem
    // to catch and report the errors.)
    // ---------------------------------------------------------------------

    /**
     * Checks all errors at once.
     * This method is provided so that the calling program does not have to check
     * for each error unless it needs it.
     * @throws java.io.IOException
     */
    public boolean isReady() throws IOException {
        
        return epsonChecker.status(epsonChecker.STATUS_CHECK_DELAY);
    
    }

    /**
     * Checks if the port is indicating an "Out of Paper" state.
     * @throws java.io.IOException
     */
    public boolean isPaperOut() throws IOException {
        return ((ParallelPort)port).isPaperOut();
    }

    /**
     * Checks if the port is indicating a "Printer Busy" state.
     * @throws java.io.IOException
     */
    public boolean isPrinterBusy() throws IOException {
        return ((ParallelPort)port).isPrinterBusy();
    }

    /**
     * Checks if the printer has encountered an error.
     * @throws java.io.IOException
     */
    public boolean isPrinterError() throws IOException {
        return ((ParallelPort)port).isPrinterError();
    }

    /**
     * Checks if the printer is in selected state.
     * @throws java.io.IOException
     */
    public boolean isPrinterSelected() throws IOException {
        return ((ParallelPort)port).isPrinterSelected();
    }

    /**
     * Checks if the printer has timed out.
     * @throws java.io.IOException
     */
    public boolean isPrinterTimedOut() throws IOException {
        return ((ParallelPort)port).isPrinterTimedOut();
    }


    //-------------------------------------------------------------------------
    // Implementation of TEPrinter
    //-------------------------------------------------------------------------

    /** Prints text in the largest font for headings.
     * @throws java.io.IOException
     */
    @Override
	public void head1() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("[head1]" );	
    	}
        setFont(FONT_A + EMPHASIZED + DOUBLE_HEIGHT + DOUBLE_WIDTH);
    	lineSpacingDefault();
    }

    /** Ends printing text in the largest font for headings.
     * @throws java.io.IOException
     */
    @Override
	public void head1End() throws IOException {
        defaultFont();
    }

    /** Prints text in the second-largest font for headings.
     * @throws java.io.IOException
     */
    @Override
	public void head2() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("[head2]" );	
    	}
        //setFont(FONT_B + EMPHASIZED + DOUBLE_HEIGHT + DOUBLE_WIDTH);
        setFont(FONT_B + DOUBLE_HEIGHT + DOUBLE_WIDTH);
    	lineSpacingDefault();
    }

    /** Ends printing text in the second-largest font for headings.
     * @throws java.io.IOException
     */
    @Override
	public void head2End() throws IOException {
        defaultFont();
    }

    /** Prints text in the third-largest font for headings.
     * @throws java.io.IOException
     */
    @Override
	public void head3() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("[head3]" );	
    	}
        setFont(FONT_A + DOUBLE_WIDTH);
    	lineSpacingDefault();
    }

    /** Ends printing text in the third-largest font for headings.
     * @throws java.io.IOException
     */
    @Override
	public void head3End() throws IOException {
        defaultFont();
    }

    /** Prints text in the fourth-largest font for headings.
     * @throws java.io.IOException
     */
    @Override
	public void head4() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("[head4]" );	
    	}
        setFont(FONT_B + DOUBLE_WIDTH);
    	lineSpacingDefault();
    }

    /** Ends printing text in the fourth-largest font for headings.
     * @throws java.io.IOException
     */
    @Override
	public void head4End() throws IOException {
        defaultFont();
    }

    /** Prints text in large font.
     * @throws java.io.IOException
     */
    @Override
	public void large() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("[large]" );	
    	}
        setFont(FONT_A);
    	lineSpacingDefault();
    }

    /** Ends printing text in large font.
     * @throws java.io.IOException
     */
    @Override
	public void largeEnd() throws IOException {
        defaultFont();
    }

// reverse print
    @Override
	public void reverse() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("[reverse]" );	
    	}
        write(SET_REVERSE_ON);
    }

    /** Ends printing text in large font.
     * @throws java.io.IOException
     */
    @Override
	public void reverseEnd() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("[reverseEnd]" );	
    	}
        write(SET_REVERSE_OFF);
    }

    /** Prints text in bold in the large font.
     * @throws java.io.IOException
     */
    @Override
	public void largeBold() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("[largeBold]" );	
    	}
        setFont(FONT_A + EMPHASIZED);
    	lineSpacingDefault();
    }

    /** Prints text in bold in the small font.
     * @throws java.io.IOException
     */
    @Override
	public void bold() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("[bold]" );	
    	}
        setFont(FONT_B + EMPHASIZED);
    	lineSpacingSmall();
    }

    /** Ends printing text in bold in the large font.
     * @throws java.io.IOException
     */
    @Override
	public void largeBoldEnd() throws IOException {
        defaultFont();
    }

    /** Prints text in the default font.
     * @throws java.io.IOException
     */
    @Override
	public void defaultFont() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("[defaultFont]" );	
    	}
        setFont(FONT_B);
        lineSpacingSmall();    
    }

    /** Prints a paragraph of text.
     * @throws java.io.IOException
     */
    @Override
	public void paragraph() throws IOException {
        //reset justification
        left();
        paragraphSB = new StringBuffer();
        paragraph = true;
    }

    /** Prints a paragraph of text.
     * @throws java.io.IOException
     */
    @Override
	public void paragraphEnd() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("paragraphEnd["+paragraph+"]" );	
    	}
        if(paragraph && paragraphSB != null && paragraphSB.length() > 0){
            String s = paragraphSB.toString();
            int point = 0;
            int cr = 0;
            int ch = ' ';
            String ss = "";
            int cpl = getCharsPerLine();
            cr = s.indexOf('\n');
            while (s.length() > cpl || (cr > 0)) {
                point = s.lastIndexOf(ch, cpl);
                if ((cr > 0) && (cr < cpl)) {
                	point = cr;
                }
                if (point < 0)
                    point = cpl;
                point++;
                
                if (point > s.length()) {
                	point = s.length();
                }
                ss = s.substring(0, point);
                if (!ss.endsWith("\n")) {
                    ss = ss.trim() + "\r\n";
                }
                if (centered) {
                    ss= SET_JUST + CENTERED + ss;
                }
                write(convertBytes(ss), 0, ss.length());
                s = s.substring(point, s.length());
                cr = s.indexOf('\n');
            }
            s = s + "\r\n";
            if (centered) {
                s = SET_JUST + CENTERED + s;
            }
            write(convertBytes(s), 0, s.length());
            paragraphSB = null;
        }
        paragraph = false;
    }

    /** Prints a centered line of text.
     * @throws java.io.IOException
     */
    @Override
	public void center() throws IOException {
    	centered = true;
        setJustification(CENTERED);
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("centered["+centered+"]" );	
    	}
    }

    /** Ends printing a centered line of text.
     * @throws java.io.IOException
     */
    @Override
	public void centerEnd() throws IOException {
    	centered = false;
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("centerEnd["+centered+"]" );	
    	}
    }

    /** Prints a line of underlines based on characters per line. */
    @Override
	public void underline() throws IOException {
        StringBuffer ulSB = new StringBuffer();
        int ul = getCharsPerLine();
        for (int i=0; i<ul ; i++){
            ulSB.append('_');
        }
        String s = ulSB.toString();
        s = s + "\r\n";
        write(convertBytes(s), 0, s.length());
    }

    /** Prints the default justification:  left-justified text.
     * @throws java.io.IOException
     */
    @Override
	public void left() throws IOException {
        left = true;
        setJustification(LEFT);
    }

    /** Ends printing with the default justification:  left-justified text.
     * @throws java.io.IOException
     */
    @Override
	public void leftEnd() throws IOException {
        if(left){
            left = false;
//            write("\n");	
        }
    }

    /** Prints a blank line.
     * @throws java.io.IOException
     */
    @Override
	public void vskip() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("[vskip]" );	
    	}
        write(" \n");	
    }

    /* Something like this might work for avoiding hardcoded columns in the XML file.
     *
     *
     * Prints formatted text.
     * Note:  Strings later in the list may overwrite those earlier in the list.
     *
     * Example:
     * 123456789012345678901234567890123456789
     * Label                 $1000.00       12
     * strList  Label   $1000.00    12
     * colList  1       10          0
     * jusList  left    right       right
     *
     * @param strList = list of strings to print.
     * @param jusList = list of "left" or "right" for justification of each string to its column.
     * @param colList = list of columns from the left or right margins at which to print.
     * @throws java.io.IOException
     */
    /*
    public void format(Vector strList, Vector jusList, int[] colList) throws IOException {
        int len;
        int max = getCharsPerLine();
        StringBuffer s = new StringBuffer(max);
        s.replace(0, max, " ");	
        Debug.println("max=" + max);	

        for (int i = 0; i < strList.size(); i++) {
            len = strList.elementAt(i).toString().length();
            Debug.println("len=" + len);	
            Debug.println("s=" + s+ ".");	 
            if (jusList.elementAt(i).toString().equals("left")) {	
                s.delete(colList[i], len);
                Debug.println("s=" + s+ ".");	 
                s.insert(colList[i], strList.elementAt(i));
                Debug.println("s=" + s+ ".");	 
            }
            else { // right
                Debug.println("max-col-len=" + (max - colList[i] - len));	
                s.replace((max - colList[i] - len-1), (max - colList[i]), strList.elementAt(i));
            }
        }
        write(s.toString());
    }
    */

    /** Prints text in a field of a specific width. 
     * It will truncate a string too long for the line or field. 
     * @param str = the string to write in this field
     * @param width = the width of the field in spaces, a negative number
     * meaning left-justified.
     * @throws java.io.IOException
     */
    @Override
	public void writeInField(String str, int width) throws IOException {
        //marker
        int maxwidth;
        int spaces;
        String justif;
        StringBuffer s = new StringBuffer(str);

        /* int w = width.indexOf("max");
        if (w >= 0) {
            ws.delete(w, 3);
            ws.insert(w, getCharsPerLine());
        } */

        // get justification and convert to positive
        if (width >= 0) {
            justif = "right"; 
        }
        else {
            justif = "left"; 
            width *= -1;
        }

        // truncate field width to line width
        maxwidth = getCharsPerLine();
        if (width > maxwidth)
            width = maxwidth;

        // truncate string width to field width
        if (s.length() > width) {
            s.setLength(width);
        }

        // prepend or append spaces to string
        spaces = width - s.length();
        if (justif.equals("left")) {	
            for (int i = 0; i < spaces; i++)
                s.append(' ');
        }
        else {
            for (int i = 0; i < spaces; i++)
                s.insert(0, ' ');
        }

        write(s.toString());
    }


    //-------------------------------------------------------------------------
    // main
    //-------------------------------------------------------------------------
    /**
     * Test/demo.
     */
     
     public static void testEpsonReadyStatus() {
        try {
            //Debug.println();
            //Debug.println();
            
            if (EpsonTMT88II.isPortInUse()){
               Debug.println("Port is in use");	
               return;
            }               
            
            EpsonTMT88II printer = EpsonTMT88II.getInstance();
            
            Debug.println("To test Epson printer ready status, turn it on/off during the test.");	
            Debug.println("The output will indicate when printer is on/off.");	
           // Debug.println();
            if (printer == null) {
                Debug.println("Make sure printer is online before test begins"); 	
                return;
            }
            
            for (int i=0; i<15; i++){
                Debug.println("Test #"+i+" Printer is "+ (printer.isReady()?"on":"off"));    
                Thread.sleep(1000);
                
                if (i == 10) {
                   // Debug.println();
                    Debug.println("Killing EpsonCheker. Make sure printer is on.");	
                   // Debug.println();
                    printer.close(); 
                }    
            }    
        }catch(Exception e){
            Debug.println("Epson ready status test failed");	
           // Debug.println(e);
        }    
    } 
    public static void main(String[] args) {
        try {
            testEpsonReadyStatus();
            if (true) return;
//            EpsonTMT88II printer = EpsonTMT88II.getInstance();
//
//            // Download logo only if printer's Non-Volatile memory loses it.
//            // User program might have option to reload logo.
//            //printer.downloadLogo();
//            if (!printer.isReady()) {
//                throw new IOException(Resources.getString("EpsonTMT88II.Printer_not_ready._11")); 
//            }
//
//            //printer.testPrint();
//
//            printer.initialize();
//            //printer.printLogo();
//
//            printer.head3();
//            printer.write("Testing eject.\n"); 
//
//            /*  print upper ASCII chart
//            printer.setJustification(LEFT);
//            printer.write("   + 0  1  2  3  4  5  6  7  8  9\n");
//            for (int i = 120; i <= 256; i++) {
//                if (i % 10 == 0)
//                    printer.write(Integer.toString(i) + "  ");	
//                else
//                    printer.write("  ");	
//                printer.write(i);
//                if (i % 10 == 9)
//                    printer.write("\n");	
//            } */
//
//            //marker
//            /* writeInField test
//            printer.setJustification(LEFT);
//            printer.large();
//            // str, width (<0=left), offset (left justified from left margin, right from right)
//            printer.writeInField("MoneyGram Receive", -22);
//            printer.writeInField("$5000.00", 12);	
//            printer.writeInField("100", 8);	
//            printer.vskip();
//            printer.defaultFont();
//            printer.writeInField("MoneyGram Receive", -22);
//            printer.writeInField("$5000.00", 12);	
//            printer.writeInField("100", 8);	
//            */
//
//            /*
//            Vector sv = new Vector();
//            sv.addElement(new String("Label"));
//            sv.addElement(new String("$9876.54"));	
//            sv.addElement(new String("123"));	
//            Vector jv = new Vector();
//            jv.addElement(new String("left"));
//            jv.addElement(new String("right"));
//            jv.addElement(new String("right"));
//            int[] ca = new int[] { 1, 12, 0 };
//            printer.format(sv, jv, ca);
//            */
//
//            /* box
//            printer.setJustification(LEFT);
//            printer.setFont(FONT_A + DOUBLE_HEIGHT + DOUBLE_WIDTH);
//            printer.head1();
//            printer.write("\u250c\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2510\n");	
//            printer.write("\u2502    MoneySaver     \u2502\n");	
//            printer.write("\u2502    Enrollment     \u2502\n");	
//            printer.write("\u2502                   \u2502\n");	
//            printer.write("\u2502                   \u2502\n");	
//            printer.write("\u2502                   \u2502\n");	
//            printer.write("\u2502                   \u2502\n");	
//            printer.write("\u2502                   \u2502\n");	
//            printer.write("\u2502                   \u2502\n");	
//            printer.write("\u2514\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2518\n");	
//            */
//
//            /* fonts
//            printer.write("Heading 1\n\n");
//            printer.head2();
//            printer.write("Heading 2\n\n");
//            printer.head3();
//            printer.write("Heading 3\n\n");
//            printer.head4();
//            printer.write("Heading 4\n\n");
//            printer.large();
//            printer.write("large\n\n");
//            printer.defaultFont();
//            printer.write("defaultFont\n\n");
//            */
//
//            /*
//            printer.setJustification(CENTERED);
//            printer.setFont(FONT_B + EMPHASIZED + DOUBLE_HEIGHT + DOUBLE_WIDTH + UNDERLINED);
//            printer.write("BillPayment Receipt\n\n");
//            printer.defaultFont();
//            printer.write("A payment for\n\n");
//            //printer.setFont(FONT_A + EMPHASIZED + DOUBLE_HEIGHT + DOUBLE_WIDTH + REVERSE_ON);
//            //printer.setFont(FONT_A + EMPHASIZED + DOUBLE_HEIGHT + DOUBLE_WIDTH);
//            printer.head1();
//            printer.write("$1,234.50\n\n");
//            //printer.setFont(FONT_B + EMPHASIZED);
//            printer.head4();
//            printer.write("was received from\n\n");
//            //printer.setFont(FONT_B + DOUBLE_WIDTH);
//            printer.head3();
//            printer.write("John Q. Sample\n");
//            printer.setFont(FONT_B);
//            printer.write("1122 Main St.\n");
//            printer.write("Anytown, MN 55432\n");
//            printer.write("Phone:  (123) 456-7890\n\n");
//            printer.setFont(FONT_B + EMPHASIZED);
//            printer.setJustification(LEFT);
//            printer.write("Account #: ");
//            printer.setFont(FONT_B + DOUBLE_WIDTH);
//            printer.write("AX1-ZY2-BB3-0000001\n");
//            printer.setFont(FONT_B + EMPHASIZED);
//            printer.write("Message:   ");
//            printer.setFont(FONT_B);
//            printer.write("This is the first line of the message.\n");
//            printer.write("\tThis is the second line.\n");	
//            printer.setFont(FONT_B + EMPHASIZED);
//            printer.write("Ref. #:    ");
//            printer.setFont(FONT_B + DOUBLE_WIDTH);
//            printer.write("A1B2C3D4\n");
//            printer.setFont(FONT_B + EMPHASIZED);
//            printer.write("Doc. #:    ");
//            printer.setFont(FONT_B + DOUBLE_WIDTH);
//            printer.write("1234567890ABC\n");
//            printer.setFont(FONT_B + EMPHASIZED);
//            printer.write("Received:  ");
//            printer.setFont(FONT_B);
//            printer.write("02/23/2000 08:20A CST\n\n");
//            printer.setJustification(CENTERED);
//            printer.write("Original\n");
//            printer.setJustification(LEFT);
//            //printer.write("\0333\046");                 // line spacing 1/9 inches	
//            printer.setFont(FONT_A);
//            printer.write("You, the customer, are entitled . . . .\n");
//            */
//
//            printer.eject();
//            printer.close();
        }
        catch (Exception e) {
            //Debug.println(e);
			Debug.printStackTrace(e);
        }
    }
    
    /**
     * EpsonChecker checks status (online/offline) of Epson printer
     * @author Boris Makhlin
     */
    private class EpsonChecker extends  Thread {
        private boolean status=false;
        private boolean killed = false;
        private Object lock = new Object();
        //Wait time before status is checked.
        private static final int STATUS_CHECK_DELAY = 6000; 
        
       /**
        * Stop EpsonChecker from running
        */
        public void kill(){
            killed = true;  
            this.interrupt();
        }    
        
       /**
        * runs EpsonChecker
        */
        @Override
		public void run(){
            while (!killed){
                try{  
                    synchronized(wait){
                    	wait.setWaitFlag(true);
                    	while (wait.isWaitFlag()) {
                    		wait.wait();
                    	}
                    }  
                    status = false;
                    defaultFont(); //send something to the printer
                    instance.writeToPort();
                    status = true;
                }catch(InterruptedException e){
                    killed = true;
                }catch(IOException e1){
                    status = false;
                }   
                synchronized (lock) {
                    lock.notify();
                }
                    
            }    
        }
        
       /**
        * Checks ready status of EpsonChecker
        * @param millis - wait time in milliseconds before status is checked.
        * @return true if EpsonChecker is online.
        */        
        private boolean status(long millis){
            synchronized(wait){
            	wait.setWaitFlag(false);
                wait.notify();
            }    
            try{
                synchronized(lock) {
                    lock.wait(millis);
                }
            }catch(InterruptedException e){}  
            

            return this.status;
        }
        
    }
    
    private void lineSpacingDefault() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("[lineSpacingDefault]");	
    	}
        write(LINE_SPACING_DEFAULT);
    }
     
	private void lineSpacingSmall() throws IOException {
		if (useCompression) {
			if (CompileTimeFlags.rcptLogTemplateParsing()) {
				ExtraDebug.println("[lineSpacingSmall]");
			}
			write(LINE_SPACING_SMALL);
		} else {
			ExtraDebug.println("[Ignore lineSpacingSmall]");
		}
	}
}
