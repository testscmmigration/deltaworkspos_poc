package dw.printing;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.swing.JComponent;

/**
 * TEPrinter is the superclass for all MoneyGram printers except the dispenser.
 *
 * This class extends ByteArrayOutputStream, since bytes are sent to the ports.
 * All of the write methods simply add to the byte[] buf inherited from
 * ByteArrayOutputStream.
 * @see  ByteArrayOutputStream
 *
 * @author jma
 * @version 4/10/2000
 */
public abstract class TEPrinter extends ByteArrayOutputStream {

    public static TEPrinter printer;
    public boolean logoFound;
    
    public static void setPrinter(TEPrinter tep){
        printer = tep;
    }
    
    public static TEPrinter getPrinter(){
        return printer;
    }
    
    public boolean isReady(JComponent c){
        return printer.isReady(c);
    }

    /** Clears the print buffer and resets modes to power-on defaults. */
    abstract public void initialize() throws IOException;

    /** Prints logo. */
    abstract void printLogo() throws IOException;
    
    /** Prints text in the largest font. */
    abstract void head1() throws IOException;
    
    /** Ends printing text in the largest font. */
    abstract void head1End() throws IOException;

    /** Prints text in the second-largest font. */
    abstract void head2() throws IOException;

    /** Ends printing text in the second-largest font. */
    abstract void head2End() throws IOException;

    /** Prints text in the third-largest font. */
    abstract void head3() throws IOException;

    /** Ends printing text in the third-largest font. */
    abstract void head3End() throws IOException;

    /** Prints text in the fourth-largest font. */
    abstract void head4() throws IOException;

    /** Ends printing text in the fourth-largest font. */
    abstract void head4End() throws IOException;

    /** Prints text in large font. */
    abstract void large() throws IOException;

    /** Ends printing text in large font. */
    abstract void largeEnd() throws IOException;

    /** Prints text in bold in the large font. */
    abstract void largeBold() throws IOException;
    
    /** Prints text in bold in the large font. */
    abstract void bold() throws IOException;

    /** Ends printing text in bold in the large font. */
    abstract void largeBoldEnd() throws IOException;

    /** Prints text in the default (smaller than large) font.  */
    abstract void defaultFont() throws IOException;

    /** Prints a paragraph of text. */
    abstract void paragraph() throws IOException;

    /** Prints a paragraph of text. */
    abstract void paragraphEnd() throws IOException;

    /** Prints a centered line of text. */
    abstract void center() throws IOException;

    /** Prints a centered line of text. */
    abstract void centerEnd() throws IOException;

    /** Prints a centered line of text. */
    abstract void reverse() throws IOException;

    /** Prints a centered line of text. */
    abstract void reverseEnd() throws IOException;

    /** Prints the default justification:  left-justified text. */
    abstract void left() throws IOException;

    /** Ends printing with the default justification:  left-justified text. */
    abstract void leftEnd() throws IOException;

    /** Prints a blank line. */
    abstract void vskip() throws IOException;

    /** Prints a line of underlines width of chararacters per line. */
    abstract void underline() throws IOException;

    /** Writes a string to the buffer.  */
    abstract void write(String s) throws IOException;

    /** Prints text in a field of a specific width. 
     * It will truncate a string too long for the line or field. 
     * @param str = the string to write in this field
     * @param width = the width of the field in spaces, a negative number
     * meaning left-justified.
     */
    abstract void writeInField(String str, int width) throws IOException;

    /** Ejects and cuts the paper. */
    abstract void eject() throws IOException;


    /** For subclasses that use print jobs (GenericPrinter) this triggers the print. */
    public abstract void fireOffPrintJob();

    /* Something like this might work for avoiding hardcoded columns in the XML file.
     * The advantage is not having to put in field widths that match the line width.
     * (The problem is that the whole line must be formatted before being printed,
     * so it makes it difficult to put setFont's in there.)
     * For now use printInField()
     *
     * Prints formatted text.
     * Note:  Strings later in the list may overwrite those earlier in the list.
     *
     * Example:
     * 123456789012345678901234567890123456789
     * Label                 $1000.00       12
     * strList  Label   $1000.00    12
     * jusList  left    right       right
     * colList  0       10          0
     *
     * @param strList = list of strings to print.
     * @param jusList = list of "left" or "right" for justification of each string to its column.
     * @param colList = list of columns from the left or right margins at which to print.
     */
    //abstract void format(Vector strList, Vector jusList, int[] colList) throws IOException;
    
    public byte[] getByteStream() {
    	return buf;    	
    }
}
