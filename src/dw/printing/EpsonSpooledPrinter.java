package dw.printing;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.comm.PortInUseException;
import javax.comm.UnsupportedCommOperationException;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.event.PrintJobAdapter;
import javax.print.event.PrintJobEvent;

import dw.dialogs.Dialogs;
import dw.i18n.Messages;
import dw.main.MainPanelDelegate;
import dw.profile.UnitProfile;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.ErrorManager;
import dw.utility.ExtraDebug;
import dw.utility.IdleBackoutTimer;
import flags.CompileTimeFlags;

/**
 * Represents a EpsonTMT99I printer using the Windows Spooler for printing receipts.
 *
 * NOTE:  When running this application, the javax.comm.properties file must be 
 * in the same directory as the comm.jar file, and this must be
 * on the same drive as the application.  Otherwise it will not find the LPT1 port.
 *
 * Consequently, the actual sending of data to the parallel port is done in
 * a separate thread so that the rest of the code can continue.  The thread
 * writes data in buf to the printer periodically (500 millis now).
 *
 * @version 3/29/2006
 * @author Steve Emrud
 * Copyright (c) 2006 MoneyGram
 */

public class EpsonSpooledPrinter extends TEPrinter  {
    /** Single Instance of this printer */
    private static EpsonSpooledPrinter instance = null;

    /** Thread used to do the actual send to the port. */
    //private Thread thread;

    /** Used to turn off the thread's run loop. */
    private boolean printerActive;

    /** Period of time the thread waits between each check and sending of data. */
    static final int waitTime = 500;


    // printer commands (default/package visibility for now)
    private int currentFont;
    static final String INITIALIZE = "\033@";	
    static final String LINE_SPACING_SMALL = "\0333\007";	
    static final String LINE_SPACING_DEFAULT = "\0332";	
    static final String AUTO_STATUS_BACK_OFF = "\035a0";	
    static final String PAPER_END_SIGNAL = "\033c38";   //near end disabled, end signal enabled (ignored for serial)	
    static final String PAPER_END_STOP = "\033c40";     //near end disabled, end signal disabled	

    static final String LOAD_LOGO = "\034q\001\100\000\020\000";	
    static final String PRINT_LOGO = "\034p\001\000";	
    //static final String EJECT_PAPER = "\035V\102\050\n\n\n";  //feed and cut	
    static final String HORIZONTAL_VERTICAL_MOTION = "\035P\100\100";  //Set horizontal and vertical motion units	
    static final String EJECT_PAPER = "\035V\102\060\033J\044";  //feed and cut	
    static final String TEST_PRINT = "\035(A2012";	
    // fonts
    static final String SET_FONT = "\033!";	
    static final String SET_REVERSE_OFF = "\035B0";	
    static final String SET_REVERSE_ON = "\035B1";	
    // combine by adding:  setFont(FONT_A + EMPHASIZED + DOUBLE_HEIGHT);
    public static final int FONT_A = 0;        // 12 x 24
    public static final int FONT_B = 1;        //  9 x 17
    static final int EMPHASIZED = 8;
    static final int DOUBLE_HEIGHT = 16;
    static final int DOUBLE_WIDTH = 32;
    static final int UNDERLINED = 128;
    static final int REVERSE_ON = 1000;

    // justification
    static final String SET_JUST = "\033a";	
    static final int LEFT = 0;
    static final int CENTERED = 1;
    static final int RIGHT = 2;
    
    // justification
    private boolean centered = false;
    private boolean paragraph = false;
    private boolean left = false;
    private StringBuffer paragraphSB = null;
    private boolean useCompression = true;
    
    public static final int FONT_A_CHARS_PER_LINE = 42;
    public static final int FONT_B_CHARS_PER_LINE = 56;

    //-------------------------------------------------------------------------
    // constructor
    //-------------------------------------------------------------------------
    /** 
     * Uses the communications API to open and write to a printer.  
     * It also initializes the printer.
     * @throws java.io.IOException
     * @throws javax.comm.NoSuchPortException
     * @throws javax.comm.PortInUseException
     * @throws javax.comm.UnsupportedCommOperationException
     */
    public EpsonSpooledPrinter() throws PortInUseException, IOException, UnsupportedCommOperationException {
        printerActive = true;
//        thread = new Thread(GenericPrinter.this);
//        thread.start();

        // give thread time to check the port
        try {
            Thread.sleep(waitTime*4);  // sleep for 2 seconds
        } catch (InterruptedException e) { }

        if (count > 0 && UnitProfile.getInstance().isDemoMode()){
            // stop the thread from running
            printerActive = false;
        }
        setPrinter(this);
        useCompression = UnitProfile.getInstance().useEpsonLineCompression(); 
    }

   /**
     * Returns the Singleton object.
     * @return The sole instance of the EpsonTMT88IISpooler class.
     */
    public synchronized static EpsonSpooledPrinter getInstance() {
        if (instance == null) {
            try {
                instance = new EpsonSpooledPrinter(); 
    	        instance.defaultFont();
            	instance.initialize();
              	instance.downloadLogo("moneygramlogo.dat");
              	instance.fireOffPrintJob();
            }
            catch (Exception e) {
                ErrorManager.handle(e, "printerInstantiationError", Messages.getString("ErrorMessages.printerInstantiationError"),	
                                                      ErrorManager.ERROR, true);
            }
        }
        return instance;
    }

    public static void clearInstance(){
        instance = null;
    }

    /**
     * Check for a default printer setup by windows and returns true if one is found.
     *
     */
    @Override
	public boolean isReady(javax.swing.JComponent c) {
        // is the printer ready to go?
        useCompression = UnitProfile.getInstance().useEpsonLineCompression(); 
        if (UnitProfile.getInstance().isDemoMode() || DWValues.getDefaultPrinter() != null)
            return true;

        boolean timerRunning = IdleBackoutTimer.isRunning();
        if (timerRunning) 
            IdleBackoutTimer.pause();

        //Dialogs.showWarning("dialogs/DialogPrinterNotReady.xml");	
        int result = Dialogs.showOkCancel("dialogs/DialogPrinterNotReady.xml");
        if (result != 0){
        	// transaction canceled output isi cancel record with substatus code 01
        	MainPanelDelegate.setupCancelISI("01");
	      	if ("Y".equals(UnitProfile.getInstance().get("ISI_ENABLE", "N"))){
	       		  String programName = "";
	       		  if (MainPanelDelegate.getIsiExeProgramArguments() != null)
	       			  programName = MainPanelDelegate.getIsiExeProgramArguments().trim();
	       		  if (programName.length() > 0 && !(programName.startsWith("-EXE"))) {
  	  	  			MainPanelDelegate.executeIsiProgram(programName);
	       		  }
	       	  }
	       	  MainPanelDelegate.setIsiTransactionInProgress(false);
	       	  MainPanelDelegate.setIsiExeCommand(false);
	       	  MainPanelDelegate.setIsiExe2Command(false);
	       	  MainPanelDelegate.setIsiExeProgramArguments("");
	       	  MainPanelDelegate.setIsiRpCommandValid(false);
        } else {
        	// Retry the check
        	return (isReady(c));
        }

        if (timerRunning)
            IdleBackoutTimer.start();
        
        return false;
    }

    /** To start printing, we just call writeToPort, right? */
    @Override
	public void fireOffPrintJob() {
        writeToPort();
    }

    /* Sends this object's bytes to the port.  Must be synchronized 
     * because of the reset.  */
    private synchronized boolean writeToPort() {
        boolean ret = false;

        /*
         * there seems to be a problem in javax.print where a DocFlavor
         * of "text/plain" returns nothing.
         */
        DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
        PrintService[] services = PrintServiceLookup.lookupPrintServices(flavor, null);

        // verify there is at least one printer set up.
        if(services.length == 0)
            return true;

        PrintService service = services[DWValues.getSelectedPrinter()];
 
        try {
            PrintRequestAttributeSet prAttributes = new HashPrintRequestAttributeSet();
            
            // setup the data to be printed
            byte[] temp = new byte[count];
            System.arraycopy(buf, 0, temp, 0, count);
    		InputStream in = new ByteArrayInputStream(temp);
            Doc doc = new SimpleDoc(in, flavor, null);
            // Create the print job
            DocPrintJob job = service.createPrintJob();
 
            // Monitor print job events; for the implementation of PrintJobWatcher,
            PrintJobWatcher3 pjDone = new PrintJobWatcher3(job);
    
            // Print it
            job.print(doc, prAttributes);
    
            // Wait for the print job to be done
            Debug.println("Before wait for print completion");
            pjDone.waitForDone();
            Debug.println("After wait for print completion");
    
            // It is now safe to close the input stream
        }
        catch (PrintException e) {
            Debug.println("PrintException: " + e);
        }

        reset();
        return ret;
    }

    /* Returns number of characters per line with the current font.  Only font
     * a or b and double width is significant.  */
    private int getCharsPerLine() {
        // default to font a
        int r = FONT_A_CHARS_PER_LINE;
        // check if font b
        if (currentFont % 2 != FONT_A) {        // i.e., if odd
            r = FONT_B_CHARS_PER_LINE;
        }

        // check if DOUBLE_WIDTH
        if (currentFont >= REVERSE_ON) {
            currentFont-= REVERSE_ON;
        }
        if (currentFont >= UNDERLINED) {
            currentFont-= UNDERLINED;
        }
        if (currentFont >= DOUBLE_WIDTH) {
            r /= 2;
        }

        return r;
    }

    // ---------------------------------------------------------------------
    // overloaded writes  (methods below call these methods)
    // ---------------------------------------------------------------------

    /**
     * Writes a string to the buffer.
     * @param s = a string to print
     * @throws java.io.IOException
     */
    @Override
	public void write(String s) throws IOException {
//      Debug.println("write(String) length = " + s.length()+",["+s+"]");	
        if (printerActive) {
            if (paragraph){
                paragraphSB.append(s);
            }
            else
                write(convertBytes(s), 0, s.length());
        }
    }

    /**
     * Writes an array of ints to the buffer.
     * This was added in order to write the logo bit image to the printer.
     * @param ia = a bit image to send to the printer
     * @throws java.io.IOException
     */
    public void write(int[] ia) throws IOException {
        if (printerActive)
            for (int i = 0; i < ia.length; i++)
                write(ia[i]);
    }

    /** Buffer used by convertBytes, out here for memory efficiency. */
    private byte[] convertBuffer = null;

    /** 
     * Same as s.getBytes("Cp437") without using charsets.jar library. 
     * Not as clean as getBytes(charset), but allows us to reduce the 
     * JRE download size by removing charsets.jar from the download.
     * The code in the loop is called frequently, so it has to be fast
     * and efficient. The return buffer is overwritten on each call,
     * so this method is not thread safe.
     * @author Geoff Atkin
     */
    private byte[] convertBytes(String s) {
        int n = s.length();
        if ((convertBuffer == null) || (n > convertBuffer.length)) {
            convertBuffer = new byte[n];
        }
        for (int i = 0; i < n; i++) {
            char c = s.charAt(i);
            byte b;
            if (c < 0x80) {
                b = (byte) c;
            }
            else {
                b = 0x20;   // default to ASCII space if no mapping
                switch (c) {
                    // generated from cp437.txt
                    case 0x00c7: b = (byte) 0x80; break; // latin capital letter c with cedilla
                    case 0x00fc: b = (byte) 0x81; break; // latin small letter u with diaeresis
                    case 0x00e9: b = (byte) 0x82; break; // latin small letter e with acute
                    case 0x00e2: b = (byte) 0x83; break; // latin small letter a with circumflex
                    case 0x00e4: b = (byte) 0x84; break; // latin small letter a with diaeresis
                    case 0x00e0: b = (byte) 0x85; break; // latin small letter a with grave
                    case 0x00e5: b = (byte) 0x86; break; // latin small letter a with ring above
                    case 0x00e7: b = (byte) 0x87; break; // latin small letter c with cedilla
                    case 0x00ea: b = (byte) 0x88; break; // latin small letter e with circumflex
                    case 0x00eb: b = (byte) 0x89; break; // latin small letter e with diaeresis
                    case 0x00e8: b = (byte) 0x8a; break; // latin small letter e with grave
                    case 0x00ef: b = (byte) 0x8b; break; // latin small letter i with diaeresis
                    case 0x00ee: b = (byte) 0x8c; break; // latin small letter i with circumflex
                    case 0x00ec: b = (byte) 0x8d; break; // latin small letter i with grave
                    case 0x00c4: b = (byte) 0x8e; break; // latin capital letter a with diaeresis
                    case 0x00c5: b = (byte) 0x8f; break; // latin capital letter a with ring above
                    case 0x00c9: b = (byte) 0x90; break; // latin capital letter e with acute
                    case 0x00e6: b = (byte) 0x91; break; // latin small ligature ae
                    case 0x00c6: b = (byte) 0x92; break; // latin capital ligature ae
                    case 0x00f4: b = (byte) 0x93; break; // latin small letter o with circumflex
                    case 0x00f6: b = (byte) 0x94; break; // latin small letter o with diaeresis
                    case 0x00f2: b = (byte) 0x95; break; // latin small letter o with grave
                    case 0x00fb: b = (byte) 0x96; break; // latin small letter u with circumflex
                    case 0x00f9: b = (byte) 0x97; break; // latin small letter u with grave
                    case 0x00ff: b = (byte) 0x98; break; // latin small letter y with diaeresis
                    case 0x00d6: b = (byte) 0x99; break; // latin capital letter o with diaeresis
                    case 0x00dc: b = (byte) 0x9a; break; // latin capital letter u with diaeresis
                    case 0x00a2: b = (byte) 0x9b; break; // cent sign
                    case 0x00a3: b = (byte) 0x9c; break; // pound sign
                    case 0x00a5: b = (byte) 0x9d; break; // yen sign
                    case 0x20a7: b = (byte) 0x9e; break; // peseta sign
                    case 0x0192: b = (byte) 0x9f; break; // latin small letter f with hook
                    case 0x00e1: b = (byte) 0xa0; break; // latin small letter a with acute
                    case 0x00ed: b = (byte) 0xa1; break; // latin small letter i with acute
                    case 0x00f3: b = (byte) 0xa2; break; // latin small letter o with acute
                    case 0x00fa: b = (byte) 0xa3; break; // latin small letter u with acute
                    case 0x00f1: b = (byte) 0xa4; break; // latin small letter n with tilde
                    case 0x00d1: b = (byte) 0xa5; break; // latin capital letter n with tilde
                    case 0x00aa: b = (byte) 0xa6; break; // feminine ordinal indicator
                    case 0x00ba: b = (byte) 0xa7; break; // masculine ordinal indicator
                    case 0x00bf: b = (byte) 0xa8; break; // inverted question mark
                    case 0x2310: b = (byte) 0xa9; break; // reversed not sign
                    case 0x00ac: b = (byte) 0xaa; break; // not sign
                    case 0x00bd: b = (byte) 0xab; break; // vulgar fraction one half
                    case 0x00bc: b = (byte) 0xac; break; // vulgar fraction one quarter
                    case 0x00a1: b = (byte) 0xad; break; // inverted exclamation mark
                    case 0x00ab: b = (byte) 0xae; break; // left-pointing double angle quotation mark
                    case 0x00bb: b = (byte) 0xaf; break; // right-pointing double angle quotation mark
                    case 0x2591: b = (byte) 0xb0; break; // light shade
                    case 0x2592: b = (byte) 0xb1; break; // medium shade
                    case 0x2593: b = (byte) 0xb2; break; // dark shade
                    case 0x2502: b = (byte) 0xb3; break; // box drawings light vertical
                    case 0x2524: b = (byte) 0xb4; break; // box drawings light vertical and left
                    case 0x2561: b = (byte) 0xb5; break; // box drawings vertical single and left double
                    case 0x2562: b = (byte) 0xb6; break; // box drawings vertical double and left single
                    case 0x2556: b = (byte) 0xb7; break; // box drawings down double and left single
                    case 0x2555: b = (byte) 0xb8; break; // box drawings down single and left double
                    case 0x2563: b = (byte) 0xb9; break; // box drawings double vertical and left
                    case 0x2551: b = (byte) 0xba; break; // box drawings double vertical
                    case 0x2557: b = (byte) 0xbb; break; // box drawings double down and left
                    case 0x255d: b = (byte) 0xbc; break; // box drawings double up and left
                    case 0x255c: b = (byte) 0xbd; break; // box drawings up double and left single
                    case 0x255b: b = (byte) 0xbe; break; // box drawings up single and left double
                    case 0x2510: b = (byte) 0xbf; break; // box drawings light down and left
                    case 0x2514: b = (byte) 0xc0; break; // box drawings light up and right
                    case 0x2534: b = (byte) 0xc1; break; // box drawings light up and horizontal
                    case 0x252c: b = (byte) 0xc2; break; // box drawings light down and horizontal
                    case 0x251c: b = (byte) 0xc3; break; // box drawings light vertical and right
                    case 0x2500: b = (byte) 0xc4; break; // box drawings light horizontal
                    case 0x253c: b = (byte) 0xc5; break; // box drawings light vertical and horizontal
                    case 0x255e: b = (byte) 0xc6; break; // box drawings vertical single and right double
                    case 0x255f: b = (byte) 0xc7; break; // box drawings vertical double and right single
                    case 0x255a: b = (byte) 0xc8; break; // box drawings double up and right
                    case 0x2554: b = (byte) 0xc9; break; // box drawings double down and right
                    case 0x2569: b = (byte) 0xca; break; // box drawings double up and horizontal
                    case 0x2566: b = (byte) 0xcb; break; // box drawings double down and horizontal
                    case 0x2560: b = (byte) 0xcc; break; // box drawings double vertical and right
                    case 0x2550: b = (byte) 0xcd; break; // box drawings double horizontal
                    case 0x256c: b = (byte) 0xce; break; // box drawings double vertical and horizontal
                    case 0x2567: b = (byte) 0xcf; break; // box drawings up single and horizontal double
                    case 0x2568: b = (byte) 0xd0; break; // box drawings up double and horizontal single
                    case 0x2564: b = (byte) 0xd1; break; // box drawings down single and horizontal double
                    case 0x2565: b = (byte) 0xd2; break; // box drawings down double and horizontal single
                    case 0x2559: b = (byte) 0xd3; break; // box drawings up double and right single
                    case 0x2558: b = (byte) 0xd4; break; // box drawings up single and right double
                    case 0x2552: b = (byte) 0xd5; break; // box drawings down single and right double
                    case 0x2553: b = (byte) 0xd6; break; // box drawings down double and right single
                    case 0x256b: b = (byte) 0xd7; break; // box drawings vertical double and horizontal single
                    case 0x256a: b = (byte) 0xd8; break; // box drawings vertical single and horizontal double
                    case 0x2518: b = (byte) 0xd9; break; // box drawings light up and left
                    case 0x250c: b = (byte) 0xda; break; // box drawings light down and right
                    case 0x2588: b = (byte) 0xdb; break; // full block
                    case 0x2584: b = (byte) 0xdc; break; // lower half block
                    case 0x258c: b = (byte) 0xdd; break; // left half block
                    case 0x2590: b = (byte) 0xde; break; // right half block
                    case 0x2580: b = (byte) 0xdf; break; // upper half block
                    case 0x03b1: b = (byte) 0xe0; break; // greek small letter alpha
                    case 0x00df: b = (byte) 0xe1; break; // latin small letter sharp s
                    case 0x0393: b = (byte) 0xe2; break; // greek capital letter gamma
                    case 0x03c0: b = (byte) 0xe3; break; // greek small letter pi
                    case 0x03a3: b = (byte) 0xe4; break; // greek capital letter sigma
                    case 0x03c3: b = (byte) 0xe5; break; // greek small letter sigma
                    case 0x00b5: b = (byte) 0xe6; break; // micro sign
                    case 0x03c4: b = (byte) 0xe7; break; // greek small letter tau
                    case 0x03a6: b = (byte) 0xe8; break; // greek capital letter phi
                    case 0x0398: b = (byte) 0xe9; break; // greek capital letter theta
                    case 0x03a9: b = (byte) 0xea; break; // greek capital letter omega
                    case 0x03b4: b = (byte) 0xeb; break; // greek small letter delta
                    case 0x221e: b = (byte) 0xec; break; // infinity
                    case 0x03c6: b = (byte) 0xed; break; // greek small letter phi
                    case 0x03b5: b = (byte) 0xee; break; // greek small letter epsilon
                    case 0x2229: b = (byte) 0xef; break; // intersection
                    case 0x2261: b = (byte) 0xf0; break; // identical to
                    case 0x00b1: b = (byte) 0xf1; break; // plus-minus sign
                    case 0x2265: b = (byte) 0xf2; break; // greater-than or equal to
                    case 0x2264: b = (byte) 0xf3; break; // less-than or equal to
                    case 0x2320: b = (byte) 0xf4; break; // top half integral
                    case 0x2321: b = (byte) 0xf5; break; // bottom half integral
                    case 0x00f7: b = (byte) 0xf6; break; // division sign
                    case 0x2248: b = (byte) 0xf7; break; // almost equal to
                    case 0x00b0: b = (byte) 0xf8; break; // degree sign
                    case 0x2219: b = (byte) 0xf9; break; // bullet operator
                    case 0x00b7: b = (byte) 0xfa; break; // middle dot
                    case 0x221a: b = (byte) 0xfb; break; // square root
                    case 0x207f: b = (byte) 0xfc; break; // superscript latin small letter n
                    case 0x00b2: b = (byte) 0xfd; break; // superscript two
                    case 0x25a0: b = (byte) 0xfe; break; // black square
                    case 0x00a0: b = (byte) 0xff; break; // no-break space
                }                    
            }
            convertBuffer[i] = b;
        }
        return convertBuffer;
    }
    
    /**
     * Clears the print buffer and resets modes to power-on defaults.
     * This is called in the constructor but is a separate function so that it
     * can also be called at any time to initialize the printer.
     * @throws java.io.IOException
     */
    @Override
	public void initialize() throws IOException {
        write(INITIALIZE);
        write(HORIZONTAL_VERTICAL_MOTION);
    }

    /**
     * Downloads a bit image logo to the printer directly from a file.
     * Epson documentation says this should not be done more than
     * 10 times a day with this printer.
     * @param fname = name of file with a bit image to send to the printer.
     * @throws java.io.IOException
     */
    public void downloadLogo(String fname) throws IOException {
//        write(LOAD_LOGO);     // comment out if file includes command
        try {
            FileInputStream file = new FileInputStream(fname);
            int c;
            while ((c = file.read()) != -1){
                if (printerActive)
                    write(c);
            }
            logoFound = true;
            file.close();
        }
        catch (Exception FileNotFoundException) {
            logoFound = false;
        }
    }

    /**
     * Prints the downloaded logo.
     * @throws java.io.IOException
     */
    @Override
	public void printLogo() throws IOException {
        if (logoFound) {
        	lineSpacingDefault();
        	write(PRINT_LOGO);
        }
    }

    /**
     * Sets the font to one of the static values of this class.
     * This treats the black/white reverse printing more like the others.
     * I.e., it does not remain on until being turned off specifically.
     * It is turned off with any call to this method.
     * @param font = sum of one or more static variables defined above
     * @throws java.io.IOException
     */
    public void setFont(int font) throws IOException {
        currentFont = font;
        write(SET_REVERSE_OFF);
        if (font >= REVERSE_ON) {
            font -= REVERSE_ON;
            write(SET_REVERSE_ON);
        }
        write(SET_FONT);
        if (printerActive)
            write(font);
    }

    /**
     * Returns font.
     */
    public int getFont(){
    	return currentFont;
    }

    /**
     * Sets justification to left, centered, right.
     * @param justif = one or more static variables defined above
     * @throws java.io.IOException
     */
    public void setJustification(int justif) throws IOException {
        write(SET_JUST);
        if (printerActive)
            write(justif);
    }

    /**
     * Ejects and cuts the paper.
     * @throws java.io.IOException
     */
    @Override
	public void eject() throws IOException {
        write(EJECT_PAPER);
        write(INITIALIZE);
    }

    /**
     * Executes a test print (and resets the printer to the power-on defaults).
     * @throws java.io.IOException
     */
    public void testPrint() throws IOException {
        write(TEST_PRINT);
    }
    /** Prints text in the largest font for headings.
     * @throws java.io.IOException
     */
    @Override
	public void head1() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("[head1]" );	
    	}
        setFont(FONT_A + EMPHASIZED + DOUBLE_HEIGHT + DOUBLE_WIDTH);
    	lineSpacingDefault();
    }

    /** Ends printing text in the largest font for headings.
     * @throws java.io.IOException
     */
    @Override
	public void head1End() throws IOException {
        defaultFont();
    }

    /** Prints text in the second-largest font for headings.
     * @throws java.io.IOException
     */
    @Override
	public void head2() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("[head2]" );	
    	}
        //setFont(FONT_B + EMPHASIZED + DOUBLE_HEIGHT + DOUBLE_WIDTH);
        setFont(FONT_B + DOUBLE_HEIGHT + DOUBLE_WIDTH);
    	lineSpacingDefault();
    }

    /** Ends printing text in the second-largest font for headings.
     * @throws java.io.IOException
     */
    @Override
	public void head2End() throws IOException {
        defaultFont();
    }

    /** Prints text in the third-largest font for headings.
     * @throws java.io.IOException
     */
    @Override
	public void head3() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("[head3]" );	
    	}
        setFont(FONT_A + DOUBLE_WIDTH);
    	lineSpacingDefault();
    }

    /** Ends printing text in the third-largest font for headings.
     * @throws java.io.IOException
     */
    @Override
	public void head3End() throws IOException {
        defaultFont();
    }

    /** Prints text in the fourth-largest font for headings.
     * @throws java.io.IOException
     */
    @Override
	public void head4() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("[head4]" );	
    	}
        setFont(FONT_B + DOUBLE_WIDTH);
    	lineSpacingDefault();
    }

    /** Ends printing text in the fourth-largest font for headings.
     * @throws java.io.IOException
     */
    @Override
	public void head4End() throws IOException {
        defaultFont();
    }

    /** Prints text in large font.
     * @throws java.io.IOException
     */
    @Override
	public void large() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("[large]" );	
    	}
        setFont(FONT_A);
    	lineSpacingDefault();
    }

    /** Ends printing text in large font.
     * @throws java.io.IOException
     */
    @Override
	public void largeEnd() throws IOException {
        defaultFont();
    }

// reverse print
    @Override
	public void reverse() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("[reverse]" );	
    	}
        write(SET_REVERSE_ON);
    }

    /** Ends printing text in large font.
     * @throws java.io.IOException
     */
    @Override
	public void reverseEnd() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("[reverseEnd]" );	
    	}
        write(SET_REVERSE_OFF);
    }

    /** Prints text in bold in the large font.
     * @throws java.io.IOException
     */
    @Override
	public void largeBold() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("[largeBold]" );	
    	}
        setFont(FONT_A + EMPHASIZED);
    	lineSpacingDefault();
    }

    /** Prints text in bold in the small font.
     * @throws java.io.IOException
     */
    @Override
	public void bold() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("[bold]" );	
    	}
        setFont(FONT_B + EMPHASIZED);
    	lineSpacingSmall();
    }

    /** Ends printing text in bold in the large font.
     * @throws java.io.IOException
     */
    @Override
	public void largeBoldEnd() throws IOException {
        defaultFont();
    }

    /** Prints text in the default font.
     * @throws java.io.IOException
     */
    @Override
	public void defaultFont() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("[defaultFont]" );	
    	}
        setFont(FONT_B);
        lineSpacingSmall();    
    }

    /** Prints a paragraph of text.
     * @throws java.io.IOException
     */
    @Override
	public void paragraph() {
        paragraphSB = new StringBuffer();
        paragraph = true;
    }

    /** Prints a paragraph of text.
     * @throws java.io.IOException
     */
    @Override
	public void paragraphEnd() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("paragraphEnd["+paragraph+"]" );	
    	}
        if(paragraph && paragraphSB != null && paragraphSB.length() > 0){
            String s = paragraphSB.toString();
            int point = 0;
            int cr = 0;
            int ch = ' ';
            String ss = "";
            int cpl = getCharsPerLine();
            cr = s.indexOf('\n');
            while (s.length() > cpl || (cr > 0)) {
                point = s.lastIndexOf(ch, cpl);
                if ((cr > 0) && (cr < cpl)) {
                	if (s.charAt(cr) != '\r') {
                		s = s.substring(0, cr) + '\r' + s.substring(cr);
                		cr++;
                	}
                	point = cr;
                }
                if (point < 0)
                    point = getCharsPerLine();
                point++;
                if (point > s.length()) {
                	point = s.length();
                }
                ss = s.substring(0, point);
                if (!ss.endsWith("\n")) {
                    ss = ss.trim() + "\r\n";
                }
                write(convertBytes(ss), 0, ss.length());
//                write(ss.getBytes(), 0, ss.length());
                s = s.substring(point, s.length());
                cr = s.indexOf('\n');
            }
            s = s + "\r\n";
            write(convertBytes(s), 0, s.length());
//            write(s.getBytes(), 0, s.length());
        }
        paragraphSB = null;
        paragraph = false;
    }
    /** Prints a centered line of text.
     * @throws java.io.IOException
     */
    @Override
	public void center() throws IOException {
        setJustification(CENTERED);
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("centered["+centered+"]" );	
    	}
    }

    /** Ends printing a centered line of text.
     * @throws java.io.IOException
     */
    @Override
	public void centerEnd() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("centerEnd["+centered+"]" );	
    	}
    }

    /** Prints a line of underlines based on characters per line. */
    @Override
	public void underline() throws IOException {
        StringBuffer ulSB = new StringBuffer();
        int ul = getCharsPerLine();
        for (int i=0; i<ul ; i++){
            ulSB.append('_');
        }
        String s = ulSB.toString();
        s = s + "\r\n";
        write(convertBytes(s), 0, s.length());
    }

    /** Prints the default justification:  left-justified text.
     * @throws java.io.IOException
     */
    @Override
	public void left() throws IOException {
        left = true;
        setJustification(LEFT);
    }

    /** Ends printing with the default justification:  left-justified text.
     * @throws java.io.IOException
     */
    @Override
	public void leftEnd() throws IOException {
        if(left){
            left = false;
//            write("\n");	
        }
    }

    /** Prints a blank line.
     * @throws java.io.IOException
     */
    @Override
	public void vskip() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("[vskip]" );	
    	}
        write(" \n");	
    }
    /** Prints text in a field of a specific width. 
     * It will truncate a string too long for the line or field. 
     * @param str = the string to write in this field
     * @param width = the width of the field in spaces, a negative number
     * meaning left-justified.
     * @throws java.io.IOException
     */
    @Override
	public void writeInField(String str, int width) throws IOException {
        //marker
        int maxwidth;
        int spaces;
        String justif;
        StringBuffer s = new StringBuffer(str);

        /* int w = width.indexOf("max");
        if (w >= 0) {
            ws.delete(w, 3);
            ws.insert(w, getCharsPerLine());
        } */

        // get justification and convert to positive
        if (width >= 0) {
            justif = "right"; 
        }
        else {
            justif = "left"; 
            width *= -1;
        }

        // truncate field width to line width
        maxwidth = getCharsPerLine();
        if (width > maxwidth)
            width = maxwidth;

        // truncate string width to field width
        if (s.length() > width) {
            s.setLength(width);
        }

        // prepend or append spaces to string
        spaces = width - s.length();
        if (justif.equals("left")) {	
            for (int i = 0; i < spaces; i++)
                s.append(' ');
        }
        else {
            for (int i = 0; i < spaces; i++)
                s.insert(0, ' ');
        }

        write(s.toString());
    }

    class PrintJobWatcher3 {
        // true iff it is safe to close the print job's input stream
        boolean done = false;

        PrintJobWatcher3(DocPrintJob job) {
            // Add a listener to the print job
            job.addPrintJobListener(new PrintJobAdapter() {
                @Override
				public void printJobCanceled(PrintJobEvent pje) {
                    Debug.println("printJobCanceled");
                    allDone();
                }
                @Override
				public void printJobCompleted(PrintJobEvent pje) {
                    Debug.println("printJobCompleted");
                    allDone();
                }
                @Override
				public void printJobFailed(PrintJobEvent pje) {
                    Debug.println("printJobFailed");
                    allDone();
                }
                @Override
				public void printJobNoMoreEvents(PrintJobEvent pje) {
                    Debug.println("printJobNoMoreEvents");
                    allDone();
                }
                void allDone() {
                    synchronized (PrintJobWatcher3.this) {
                        done = true;
                        PrintJobWatcher3.this.notify();
                    }
                }
            });
        }
        
        public synchronized void waitForDone() {
            try {
                while (!done) {
                    wait();
                }
            }
            catch (InterruptedException e) {
            }
        }
    }

    private void lineSpacingDefault() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("[lineSpacingDefault]");	
    	}
        write(LINE_SPACING_DEFAULT);
    }
     
	private void lineSpacingSmall() throws IOException {
		if (useCompression) {
			if (CompileTimeFlags.rcptLogTemplateParsing()) {
				ExtraDebug.println("[lineSpacingSmall]");
			}
			write(LINE_SPACING_SMALL);
		} else {
			ExtraDebug.println("[Ignore lineSpacingSmall]");
		}
	}

}
