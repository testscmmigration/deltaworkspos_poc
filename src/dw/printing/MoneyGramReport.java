package dw.printing;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;

import org.xml.sax.Attributes;

import com.moneygram.agentconnect.BillPaymentDetailInfo;
import com.moneygram.agentconnect.MoneyGramReceiveDetailInfo;
import com.moneygram.agentconnect.MoneyGramSendDetailInfo;
import com.moneygram.agentconnect.MoneyGramSendWithTaxDetailInfo;
import com.moneygram.agentconnect.PaymentType;
import com.moneygram.agentconnect.ProductVariantType;
import com.moneygram.agentconnect.TransactionStatusType;

import dw.comm.MessageFacade;
import dw.comm.MessageFacadeError;
import dw.comm.MessageFacadeParam;
import dw.comm.MessageLogic;
import dw.framework.CommCompleteInterface;
import dw.framework.WaitToken;
import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.io.info.ReportEventDiagInfo;
import dw.io.report.AuditLog;
import dw.io.report.MoneyGramBillPayReverseDetail;
import dw.io.report.MoneyGramDetail;
import dw.io.report.MoneyGramReverseDetail;
import dw.io.report.ReportDetail;
import dw.io.report.ReportLog;
import dw.io.report.ReportLogConstants;
import dw.io.report.ReportSelectionCriteria;
import dw.io.report.ReportSummary;
import dw.profile.ProfileAccessor;
import dw.profile.UnitProfile;
import dw.utility.CountAndAmount;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.StringUtility;
import dw.utility.TimeUtility;

public class MoneyGramReport extends Report {
	 private int MONEY_GRAM_SEND_DOC_SEQ = 3;
	 private int MONEY_GRAM_RECEIVE_DOC_SEQ = 4;
	 private int BILL_PAYMENT_DOC_SEQ = 5;
	 
	 private static final String QUOTE = "\"";
	 private static final String COMMA = ",";
	 private static final String QUOTE_COMMA_QUOTE = "\",\"";
	 private static final String QUOTE_COMMA_COMMA_QUOTE = "\",,\"";
	 private static final String QUOTE_COMMA_COMMA_COMMA_QUOTE = "\",,,\"";

	 private Collection<ReportDetail> reversalDetailItems;
    private Collection<ReportDetail> sendDetailItems;
    private Collection<ReportDetail> utilityDetailItems;
    private Collection<ReportDetail> prepaidDetailItems;
    boolean superAgentMode = false;
    private Collection<ReportDetail> receiveDetailItems;
    private Collection<ReportDetail> receiveToCardDetailItems;
    private Collection<ReportDetail> receiveCurrencyDetailItems;
    private Collection<ReportDetail> receiveToCardCurrencyDetailItems;
    private Collection<ReportDetail> sendCurrencyDetailItems;
    private Collection<ReportDetail> sendReversalCurrencyDetailItems;
    private ReportSummary reportSummary = null;
    private int userID;
    private boolean isSourceHost=false;
    boolean gstSelector = false;
    
    private class MoneyGramReportData {
    	private MoneyGramReceiveDetailInfo[] rdri;
    	private MoneyGramSendDetailInfo[] sdri;
    	private MoneyGramSendWithTaxDetailInfo[] sdriwt;
    	private BillPaymentDetailInfo[] bpdri;
    }

    public MoneyGramReport(int userID) {
        super(null);
    	this.userID = userID;
    	gstSelector = UnitProfile.getInstance().get("ALLOW_TAX_REPORT", "N").equals("Y");
    }

	@Override
    public int print(final String fileName,
            final ReportSelectionCriteria rsc) {
        DateFormat df = new SimpleDateFormat(TimeUtility.DATE_MONTH_DAY_SHORT_YEAR_NO_DELIMITER_FORMAT);
		int status = createCsvReportFile(fileName, rsc, gstSelector, df);
		return status;
    }
    
    @Override
	public int print(final TEPrinter printer,
                      final ReportSelectionCriteria rsc) {
        status = OKAY;
        initSelectionCriteria(printer, rsc);
        String reportSource = "";	
        reportSummary = null;
        addFlag("local", !(rsc.getAccessHost())); 
        final String activityDate =  FormatSymbols.formatDateForLocale(new Date(timeStamp));                     
        addField("activityDate",activityDate+" 00:00"); 
      
        addFlag("nosend",false);
        try {
            if (inDemoMode) {
                rsc.setAccessHost(true);
            }
            
            DateFormat df = new SimpleDateFormat(TimeUtility.DATE_MONTH_DAY_SHORT_YEAR_NO_DELIMITER_FORMAT);

            isSourceHost = rsc.getAccessHost();
            if (isSourceHost) { // get data from host
                reportSource = inDemoMode ? "Demo" : "Host";
                reportSummary = new ReportSummary();

                boolean success = false;

				if (gstSelector) {
					success = getHostDataWithTax(timeStamp, df);
				} else {
					success = getHostData(timeStamp, df);
				}
                writeReportEventMessage(
                        ReportEventDiagInfo.MONEYGRAM_AND_BILL_PAYMENT_REPORT,
                        success);

                if (! success) {
                    status = HOST_CONNECT_ERROR;
                    return status;
                }         
            }
            else {
                final Collection<ReportDetail> mixedDetailItems = ReportLog.getDetailRecords(rsc);
                detailItems = ReportLog.getNewDetailRecordCollection();
                sendDetailItems = ReportLog.getNewDetailRecordCollection();
                utilityDetailItems = ReportLog.getNewDetailRecordCollection();
                prepaidDetailItems = ReportLog.getNewDetailRecordCollection();
                receiveDetailItems = ReportLog.getNewDetailRecordCollection();
                receiveCurrencyDetailItems = ReportLog.getNewDetailRecordCollection();
                receiveToCardDetailItems = ReportLog.getNewDetailRecordCollection();
                receiveToCardCurrencyDetailItems = ReportLog.getNewDetailRecordCollection();
                sendCurrencyDetailItems = ReportLog.getNewDetailRecordCollection();
                sendReversalCurrencyDetailItems = ReportLog.getNewDetailRecordCollection();
                reversalDetailItems = ReportLog.getNewDetailRecordCollection();               

                final Iterator<ReportDetail> it = mixedDetailItems.iterator();
                while (it.hasNext()) {
                    final Object detailItem = it.next();
                    if (detailItem instanceof MoneyGramDetail) {
                        final MoneyGramDetail moneyGramDetail = (MoneyGramDetail)detailItem;
                        if (moneyGramDetail.getType() == ReportLog.MONEYGRAM_RECEIVE_TYPE) {
                            receiveDetailItems.add(moneyGramDetail);
                        }
                        else if (moneyGramDetail.getType() == ReportLog.MONEYGRAM_RECEIVE_TYPE_PPC) {
                        	receiveToCardDetailItems.add(moneyGramDetail);
                        }
                        else if (moneyGramDetail.getType() == ReportLog.UTILITY_PAYMENT_TYPE) {
                        	utilityDetailItems.add(moneyGramDetail);
                        }
                        else if (moneyGramDetail.getType() == ReportLog.PREPAID_SERVICES_TYPE) {
                        	prepaidDetailItems.add(moneyGramDetail);
                        }
                        else {
                            sendDetailItems.add(moneyGramDetail);
                        }
                        detailItems.add(moneyGramDetail);
                    }
                    
                    else if (detailItem instanceof MoneyGramReverseDetail
                             ) {
                        final MoneyGramReverseDetail moneyGramReverseDetail = (MoneyGramReverseDetail)detailItem;
                        moneyGramReverseDetail.setDateTime(moneyGramReverseDetail.getTime());
                        reversalDetailItems.add(moneyGramReverseDetail);
                        detailItems.add(moneyGramReverseDetail);
                        final Iterator<ReportDetail> send_it = sendDetailItems.iterator();
                        while(send_it.hasNext()){
                            final Object sendDetailItem = send_it.next();
                            String rn = ((MoneyGramDetail) sendDetailItem).getReferenceNumber();
                            if (moneyGramReverseDetail.getReferenceNumber().equalsIgnoreCase(rn)){
                                sendDetailItems.remove(sendDetailItem);
                                break;
                            }
                        }
                        final Iterator<ReportDetail> detail_it = detailItems.iterator();
                        while(detail_it.hasNext()){
                            final Object dItem = detail_it.next();
                            if (dItem instanceof MoneyGramDetail){
                                String rn = ((MoneyGramDetail) dItem).getReferenceNumber();
                                if (moneyGramReverseDetail.getReferenceNumber().equalsIgnoreCase(rn)){
                                    detailItems.remove(dItem);
                                    break;
                                }
                            }
                        }
                    }
                }
                setPrintFlag(sendDetailItems,    
                		     utilityDetailItems,  
						     prepaidDetailItems,  
						     receiveDetailItems, 
						     receiveToCardDetailItems, 
 						     reversalDetailItems );
                reportSummary = new ReportSummary(detailItems);
            }

            // right now the host returns MS and EP together
            final CountAndAmount expressPaymentCountAndAmount =
                                        reportSummary.getExpressPaymentCountAndAmount();
            reportSummary.getMoneyGramSendCountAndAmount().
                                                      add(expressPaymentCountAndAmount);
            expressPaymentCountAndAmount.setToZero();
            setBadLogins(reportSummary.getBadLogs());

            // client source (in case host fails)
            if (!isSourceHost) {
                reportSource = "Local";
                writeReportEventMessage(ReportEventDiagInfo.MONEYGRAM_AND_BILL_PAYMENT_REPORT , true);
            }
        }
        finally {
            addField("reportSource",reportSource); 
            reportSummary.addFieldValues(fieldMap);
        }      
        AuditLog.writeReportRecord(userID, rsc.getStartDateTime(), "RPT");
        return super.print(printer, rsc);
    }

	private int createCsvReportFile(String fileName,
			ReportSelectionCriteria rsc, boolean gstData, DateFormat df) {
        
    	status = OKAY;
        initSelectionCriteriaCsv(rsc);
        Collection<ReportDetail> csvDetailItems;
    	
        if (inDemoMode) 
        	rsc.setAccessHost(true);

		if (rsc.getAccessHost()) {
			if (gstData) {
				csvDetailItems = getHostDataForCsvWithTax(timeStamp, rsc, df);
			} else {
				csvDetailItems = getHostDataForCsv(timeStamp, df);
			}
		} else
			csvDetailItems = getLocalReportDataForCsv(rsc);

        
        createCsvReport(rsc, fileName, csvDetailItems, gstData);
        AuditLog.writeReportRecord(userID, rsc.getStartDateTime(), "CRPT");
    	return status;
    }
    
	/**
	 * @param csvDetailItems
	 */
	private void createCsvReport(ReportSelectionCriteria rsc, String fileName,
			Collection<ReportDetail> csvDetailItems, boolean gstData) {
        String source;
        String moAgentID = Messages.getString("CsvHeader.1") + ci.getAgentId() + "\r\n";
        String mgAgentID = Messages.getString("CsvHeader.2") + ci.getMGAccountNumber() + "\r\n";
        String agentName = Messages.getString("CsvHeader.3") + UnitProfile.getInstance().getGenericDocumentStoreName() + "\r\n";
        String todayDateTime = Messages.getString("CsvHeader.4") + FormatSymbols.formatDateForLocale(TimeUtility.currentTime()) + " at " +  
							   FormatSymbols.formatTimeForLocale(TimeUtility.currentTime()) + "\r\n";
        String activityDate = Messages.getString("CsvHeader.5") + FormatSymbols.formatDateForLocale(timeStamp) + "\r\n";
        if(rsc.getAccessHost())
        	source = Messages.getString("CsvHeader.6") + "\r\n";
        else
        	source = Messages.getString("CsvHeader.7") + "\r\n";

		try {
			/*
			 * For Chinese we need to write the file as UTF-8 (since Chinese 
			 * characters require unicode).  Also in order for Excel to handle
			 * a CSV file in UTF-8 we have to include a BOM(byte order marks), 
			 * which can cause problems with other programs.
			 * 
			 */
			String sLang = Messages.getCurrentLocale().getLanguage();
			if (sLang.compareToIgnoreCase("cn") == 0
					|| sLang.compareToIgnoreCase("ru") == 0) {
				Writer out;
				FileOutputStream fos = new FileOutputStream(fileName);
				byte[] bom = new byte[] { (byte)0xEF, (byte)0xBB, (byte)0xBF };
				fos.write(bom);

				// open UTF8 writer
				out = new OutputStreamWriter(fos, "UTF-8");
				out.write(agentName);
				out.write(moAgentID);
				out.write(mgAgentID);
				out.write(todayDateTime);
				out.write(activityDate);
				out.write(source);
				
				if (gstData) {
					out.write((createCvsDetailHeaderWithTax().append("\r\n")
							.toString()));
				} else {
					out.write((createCvsDetailHeader().append("\r\n")
							.toString()));
				}
		        final Iterator<ReportDetail> it = csvDetailItems.iterator();
		        while (it.hasNext()) {
		        	StringBuffer csvString;
		        	final Object detailItem = it.next();
					if (gstData) {
						csvString = constructCsvStringWithTax(detailItem);
					} else {
						csvString = constructCsvString(detailItem);
					}
		    		if (csvString != null)
		    		    out.write((csvString.append("\r\n").toString()));
		        }
			    out.close();
			} else {
		        FileOutputStream out;
				out = new FileOutputStream(fileName);
				out.write(agentName.getBytes());
				out.write(moAgentID.getBytes());
				out.write(mgAgentID.getBytes());
				out.write(todayDateTime.getBytes());
				out.write(activityDate.getBytes());
				out.write(source.getBytes());
				if (gstData) {
					out.write((createCvsDetailHeaderWithTax().append("\r\n")
							.toString()).getBytes());
				} else {
					out.write((createCvsDetailHeader().append("\r\n")
							.toString()).getBytes());
				}

		        final Iterator<ReportDetail> it = csvDetailItems.iterator();
		        while (it.hasNext()) {
		        	StringBuffer csvString = new StringBuffer();
		        	final Object detailItem = it.next();     		
		        	csvString = constructCsvString(detailItem);
		    		if (csvString != null)
		    		    out.write((csvString.append("\r\n").toString()).getBytes());
		        }
			    out.close();
			}
        }
		catch (FileNotFoundException e) {
			Debug.printStackTrace(e);
		} catch (IOException e) {
			Debug.printStackTrace(e);
		}
	}

	private StringBuffer createCvsDetailHeader() {
		StringBuffer sb = new StringBuffer();
		sb.append(Messages.getString("CsvHeader.8"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.9"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.10"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.11"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.12"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.13"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.14"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.15"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.16"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.17"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.18"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.19"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.20"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.21"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.22"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.23"));
		return sb;
	}

	private StringBuffer createCvsDetailHeaderWithTax() {
		StringBuffer sb = new StringBuffer();
		sb.append(Messages.getString("CsvHeader.8"))
			.append(COMMA) 
			.append(Messages.getString("CsvHeader.9"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.10"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.11"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.12"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.13"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.14"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.15"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.16"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.17"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.18"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.19"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.20"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.25"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.21"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.22"))
			.append(COMMA)
			.append(Messages.getString("CsvHeader.23"));
		return sb;
	}

	private StringBuffer constructCsvString(Object item) {

		StringBuffer sb = new StringBuffer();
		
    	if (item instanceof MoneyGramDetail) {
    		final MoneyGramDetail detail = (MoneyGramDetail) item;
    		BigDecimal amount = detail.getAmount();
    		BigDecimal fee = detail.getFee();
    		BigDecimal total = amount.add(fee);
    		sb.append(QUOTE)
				.append(detail.getAgentID())
				.append(QUOTE_COMMA_QUOTE)
				.append(detail.getCurrencyCode())
				.append(QUOTE_COMMA_QUOTE)
				.append(FormatSymbols.formatTimeForLocale24(new Date(detail.getDateTime())))
				.append(QUOTE_COMMA_QUOTE)
				.append(detail.getEmployeeNumber())
				.append(QUOTE_COMMA_QUOTE)
				.append(detail.getName())
				.append(QUOTE_COMMA_QUOTE)
				.append(detail.getReferenceNumber())
				.append(QUOTE_COMMA_QUOTE)
				.append(detail.getSerialNumber())
				.append(QUOTE_COMMA_QUOTE)
				.append(getType(detail.getType()))
				.append(QUOTE_COMMA_QUOTE)
				.append(detail.getUserId())
				.append(QUOTE_COMMA_QUOTE)
				.append(detail.getAmount())
				.append(QUOTE_COMMA_QUOTE)
				.append(detail.getExchange())
				.append(QUOTE_COMMA_QUOTE)
				.append(detail.getFee())
				.append(QUOTE_COMMA_QUOTE)
				.append(detail.getProcessingFee())
				.append(QUOTE)
				.append(COMMA)
				.append(COMMA)
				.append(COMMA)
				.append(total.toString());
    	} else if (item instanceof MoneyGramReverseDetail) {
    		reverseDetailString((MoneyGramReverseDetail) item, sb);

    	} else if (item instanceof MoneyGramBillPayReverseDetail) {
    		reverseBpDetailString((MoneyGramBillPayReverseDetail) item, sb);
    	}
    	
    	while (true) {
    		int i = sb.indexOf("\"null\"");
    		if (i == -1) {
    			break;
    		}
    		sb.replace(i, i + 6, "");
    	}
    	return sb;
	}

	private StringBuffer constructCsvStringWithTax(Object item) {

		StringBuffer sb = new StringBuffer();
		
    	if (item instanceof MoneyGramDetail) {
    		final MoneyGramDetail detail = (MoneyGramDetail) item;
    		BigDecimal amount = detail.getAmount();
    		BigDecimal fee = detail.getFee();
    		BigDecimal total = amount.add(fee);
			total = total.add(detail.getTax());
    		    		
    		sb.append(QUOTE)
				.append(detail.getAgentID())
				.append(QUOTE_COMMA_QUOTE)
				.append(detail.getCurrencyCode())
				.append(QUOTE_COMMA_QUOTE)
				.append(FormatSymbols.formatTimeForLocale24(new Date(detail.getDateTime())))
				.append(QUOTE_COMMA_QUOTE)
				.append(detail.getEmployeeNumber())
				.append(QUOTE_COMMA_QUOTE)
				.append(detail.getName())
				.append(QUOTE_COMMA_QUOTE)
				.append(detail.getReferenceNumber())
				.append(QUOTE_COMMA_QUOTE)
				.append(detail.getSerialNumber())
				.append(QUOTE_COMMA_QUOTE)
				.append(getType(detail.getType()))
				.append(QUOTE_COMMA_QUOTE)
				.append(detail.getUserId())
				.append(QUOTE_COMMA_QUOTE)
				.append(detail.getAmount())
				.append(QUOTE_COMMA_QUOTE)
				.append(detail.getExchange())
				.append(QUOTE_COMMA_QUOTE)
				.append(detail.getFee())
				.append(QUOTE_COMMA_QUOTE)
				.append(detail.getProcessingFee())
				.append(QUOTE_COMMA_QUOTE)
				.append(detail.getTax())
				.append(QUOTE)
				.append(COMMA)
				.append(COMMA)
				.append(COMMA)
				.append(total.toString());
    	
    	} else if (item instanceof MoneyGramReverseDetail) {
    		reverseDetailStringWithTax((MoneyGramReverseDetail) item, sb);

    	} else if (item instanceof MoneyGramBillPayReverseDetail) {
    		reverseBpDetailStringWithTax((MoneyGramBillPayReverseDetail) item, sb);
    	}
    
    	while (true) {
    		int i = sb.indexOf("\"null\"");
    		if (i == -1) {
    			break;
    		}
    		sb.replace(i, i + 6, "");
    	}
    	return sb;
	}

	private void reverseDetailString(MoneyGramReverseDetail detail, StringBuffer sb) {
		sb.append(QUOTE)
			.append(detail.getAgentID())
			.append(QUOTE_COMMA_COMMA_QUOTE)
			.append(FormatSymbols.formatTimeForLocale24(new Date(detail.getTime())))
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getEmployeeNumber())
			.append(QUOTE_COMMA_COMMA_QUOTE)
			.append(detail.getReferenceNumber())
			.append(QUOTE_COMMA_COMMA_QUOTE)
			.append(getType(detail.getType()))
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getUserId())
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getAmount())
			.append(QUOTE_COMMA_COMMA_QUOTE)
			.append(detail.getFee())
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getProcessingFee())
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getSender())
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getUser())
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getTotal())
			.append(QUOTE);
	}

	private void reverseDetailStringWithTax(MoneyGramReverseDetail detail, StringBuffer sb) {
		sb.append(QUOTE)
			.append(detail.getAgentID())
			.append(QUOTE_COMMA_COMMA_QUOTE)
			.append(FormatSymbols.formatTimeForLocale24(new Date(detail.getTime())))
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getEmployeeNumber())
			.append(QUOTE_COMMA_COMMA_QUOTE)
			.append(detail.getReferenceNumber())
			.append(QUOTE_COMMA_COMMA_QUOTE)
			.append(getType(detail.getType()))
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getUserId())
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getAmount())
			.append(QUOTE_COMMA_COMMA_QUOTE)
			.append(detail.getFee())
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getProcessingFee())
			.append(QUOTE_COMMA_COMMA_COMMA_QUOTE)
			.append(detail.getSender())
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getUser())
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getTotal())
			.append(QUOTE);
	}

	private void reverseBpDetailString(MoneyGramBillPayReverseDetail detail, StringBuffer sb) {
		sb.append(QUOTE)
			.append(detail.getAgentID())
			.append(QUOTE_COMMA_COMMA_QUOTE)
			.append(FormatSymbols.formatTimeForLocale24(new Date(detail.getDateTime())))
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getEmployeeNumber())
			.append(QUOTE_COMMA_COMMA_QUOTE)
			.append(detail.getReferenceNumber())
			.append(QUOTE_COMMA_COMMA_QUOTE)
			.append(getType(detail.getType()))
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getUserId())
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getAmount())
			.append(QUOTE_COMMA_COMMA_QUOTE)
			.append(detail.getFee())
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getProcessingFee())
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getSender())
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getUser())
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getTotal())
			.append(QUOTE);
	}

	private void reverseBpDetailStringWithTax(MoneyGramBillPayReverseDetail detail, StringBuffer sb) {
		sb.append(QUOTE)
			.append(detail.getAgentID())
			.append(QUOTE_COMMA_COMMA_QUOTE)
			.append(FormatSymbols.formatTimeForLocale24(new Date(detail.getDateTime())))
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getEmployeeNumber())
			.append(QUOTE_COMMA_COMMA_QUOTE)
			.append(detail.getReferenceNumber())
			.append(QUOTE_COMMA_COMMA_QUOTE)
			.append(getType(detail.getType()))
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getUserId())
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getAmount())
			.append(QUOTE_COMMA_COMMA_QUOTE)
			.append(detail.getFee())
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getProcessingFee())
			.append(QUOTE_COMMA_COMMA_COMMA_QUOTE)
			.append(detail.getSender())
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getUser())
			.append(QUOTE_COMMA_QUOTE)
			.append(detail.getTotal())
			.append(QUOTE);
	}

	private String getType(int type) {
		String typeString = "";
        if (type == ReportLog.VOID_TYPE) typeString = "Void"; 
        else if (type == ReportLog.MONEY_ORDER_TYPE) typeString = "Money Order"; 
        else if (type == ReportLog.VENDOR_PAYMENT_TYPE) typeString = "Vendor Payment"; 
        else if (type == ReportLog.MONEYGRAM_SEND_TYPE) typeString = "MoneyGram Send"; 
        else if (type == ReportLog.MONEYGRAM_CARD_RELOAD_TYPE) typeString = "MoneyGram Card Re-load"; 
        else if (type == ReportLog.MONEYGRAM_CARD_PURCH_TYPE) typeString = "MoneyGram Card Purchase"; 
        else if (type == ReportLog.MONEYGRAM_RECEIVE_TYPE) typeString = "MoneyGram Receive"; 
        else if (type == ReportLog.MONEYGRAM_RECEIVE_TYPE_PPC) typeString = "MoneyGram Receive to Prepaid Card"; 
        else if (type == ReportLog.MONEYGRAM_REVERSAL_TYPE) typeString = "MoneyGram Reversal"; 
        else if (type == ReportLog.BILL_PAYMENT_TYPE) typeString = "Bill Payment"; 
        else if (type == ReportLog.DELTAGRAM_TYPE) typeString = "DeltaGram"; 
        else if (type == ReportLog.DISPENSER_LOAD_TYPE) typeString = "Dispenser Load"; 
        else if (type == ReportLog.INVALID_LOGIN_TYPE) typeString = "Invalid Login"; 
        else if (type == ReportLog.EXPRESS_PAYMENT_TYPE) typeString = "Express Payment"; 
        else if (type == ReportLog.UTILITY_PAYMENT_TYPE) typeString = "Utility Bill Payment"; 
        else if (type == ReportLog.PREPAID_SERVICES_TYPE) typeString = "Prepaid Services"; 
        else typeString = "Invalid Transaction Type"; 
        
        return typeString;
	}
	
	private Collection<ReportDetail> getLocalReportDataForCsv(final ReportSelectionCriteria rsc) {

        final Collection<ReportDetail> mixedDetailItems = ReportLog.getDetailRecords(rsc);
        detailItems = ReportLog.getNewDetailRecordCollection();          

        final Iterator<ReportDetail> it = mixedDetailItems.iterator();
        while (it.hasNext()) {
            final Object detailItem = it.next();
            if (detailItem instanceof MoneyGramDetail) {
                final MoneyGramDetail moneyGramDetail = (MoneyGramDetail)detailItem;
                detailItems.add(moneyGramDetail);
            }               
            else if (detailItem instanceof MoneyGramReverseDetail) {
                final MoneyGramReverseDetail moneyGramReverseDetail = (MoneyGramReverseDetail)detailItem;
                moneyGramReverseDetail.setDateTime(moneyGramReverseDetail.getTime());
                detailItems.add(moneyGramReverseDetail);
                final Iterator<ReportDetail> detail_it = detailItems.iterator();
                while(detail_it.hasNext()){
                    final Object dItem = detail_it.next();
                    if (dItem instanceof MoneyGramDetail){
                        String rn = ((MoneyGramDetail) dItem).getReferenceNumber();
                        if (moneyGramReverseDetail.getReferenceNumber().equalsIgnoreCase(rn)){
                            detailItems.remove(dItem);
                            break;
                        }
                    }
                }
            }
        }
     
        return detailItems;		
	}

    private interface MoneyGramReportMessageLogic extends MessageLogic {
    	public Object[] getData();
    }
    
	private final WaitToken wait = new WaitToken();

    private final CommCompleteInterface callingPage = new CommCompleteInterface() {
        @Override
		public void commComplete(int commTag, Object retValue) {
        	wait.setWaitFlag(false);
            synchronized(wait) {
            	wait.notify();
            }
        }
    };
	
    private MoneyGramReceiveDetailInfo[] getMoneyGramReceiveDetailReport(final MessageFacadeParam parameters) {
        
    	final MoneyGramReportMessageLogic logic = new MoneyGramReportMessageLogic() {
            MoneyGramReceiveDetailInfo[] rdri = null;
        	
            @Override
			public Object run() {
                boolean results = true;
                Date activityDate = new Date(timeStamp);
               
                try {
                    rdri = MessageFacade.getInstance().getMoneyGramReceiveDetailReport(parameters, activityDate, userID);
                } catch (MessageFacadeError e) {
                    rdri = null;
                }
                
                return results;
            }

			@Override
			public Object[] getData() {
				return rdri;
			}
        };

        MessageFacade.run(logic, callingPage, 0, Boolean.FALSE);
        
        wait.setWaitFlag(true);
        while (wait.isWaitFlag()) {
            synchronized(wait) {
            	try {
            		wait.wait();
            	} catch (Exception e) {
            		Debug.printException(e);
            	}
            }
        }

        return (MoneyGramReceiveDetailInfo[]) logic.getData();
    }
    
    private MoneyGramSendDetailInfo[] getMoneyGramSendDetailReport(final MessageFacadeParam parameters) {
        final MoneyGramReportMessageLogic logic = new MoneyGramReportMessageLogic() {
            private MoneyGramSendDetailInfo[] sdri = null;
            @Override
			public Object run() {
                boolean results = true;
                Date activityDate = new Date(timeStamp);
               
                try {
                    sdri = MessageFacade.getInstance().getMoneyGramSendDetailReport(parameters, activityDate, userID);
                } catch (MessageFacadeError e) {
                    sdri = null;
                }
                
                return results;
            }

			@Override
			public Object[] getData() {
				return sdri;
			}
        };
        MessageFacade.run(logic, callingPage, 0, Boolean.FALSE);
        
        wait.setWaitFlag(true);
        while (wait.isWaitFlag()) {
            synchronized(wait) {
            	try {
            		wait.wait();
            	} catch (Exception e) {
            		Debug.printException(e);
            	}
            }
        }

        return (MoneyGramSendDetailInfo[]) logic.getData();
    }
    
    private MoneyGramSendWithTaxDetailInfo[] getMoneyGramSendDetailReportWithTax(final MessageFacadeParam parameters) {
        final MoneyGramReportMessageLogic logic = new MoneyGramReportMessageLogic() {
            private MoneyGramSendWithTaxDetailInfo[] sdri = null;
            @Override
			public Object run() {
                boolean results = true;
                Date activityDate = new Date(timeStamp);
               
                try {
                    sdri = MessageFacade.getInstance().getMoneyGramSendDetailReportWithTax(parameters, activityDate, userID);
                } catch (MessageFacadeError sdriex) {
                    sdri = null;
                }
                
                return results;
            }

			@Override
			public Object[] getData() {
				return sdri;
			}
        };

        MessageFacade.run(logic, callingPage, 0, Boolean.FALSE);
        
        wait.setWaitFlag(true);
        while (wait.isWaitFlag()) {
            synchronized(wait) {
            	try {
            		wait.wait();
            	} catch (Exception e) {
            		Debug.printException(e);
            	}
            }
        }

        return (MoneyGramSendWithTaxDetailInfo[]) logic.getData();
    }
    
    private BillPaymentDetailInfo[] getMoneyGramBillPayDetailReport(final MessageFacadeParam parameters) {
        final MoneyGramReportMessageLogic logic = new MoneyGramReportMessageLogic() {
            private BillPaymentDetailInfo[] bpdri = null;
            @Override
			public Object run() {
                boolean results = true;
                Date activityDate = new Date(timeStamp);
               
                try {
                    bpdri = MessageFacade.getInstance().getMoneyGramBillPayDetailReport(parameters, activityDate);
					if (bpdri.length == 0) {
						bpdri = null;
					}
                    
                } catch (MessageFacadeError bpdriex) {
                    bpdri = null;
                }

                return results;
            }

			@Override
			public Object[] getData() {
				return bpdri;
			}
        };

        MessageFacade.run(logic, callingPage, 0, Boolean.FALSE);
        
        wait.setWaitFlag(true);
        while (wait.isWaitFlag()) {
            synchronized(wait) {
            	try {
            		wait.wait();
            	} catch (Exception e) {
            		Debug.printException(e);
            	}
            }
        }

        return (BillPaymentDetailInfo[]) logic.getData();
    }
    
    /*
     * Gets data from the middleware for the MoneyGram Sales report.
     * On failure return false.
     */
    
    private MoneyGramReportData getMoneyGramReportData(boolean withTax) {
    	MoneyGramReportData data = new MoneyGramReportData();

        MessageFacadeParam parameters = new MessageFacadeParam(MessageFacadeParam.REAL_TIME,
				 MessageFacadeParam.DO_NOT_HIDE,
                MessageFacadeParam.NO_HOST_RECORDS_FOUND, 
                MessageFacadeParam.NO_HOST_SEND_RECORDS_FOUND);

        data.rdri = this.getMoneyGramReceiveDetailReport(parameters);
        if (MessageFacade.getInstance().isReportCanceled()) {
            MessageFacade.getInstance().resetReportCanceled();
            data = null;
        }
        
        if (data != null) {
        	if (! withTax) {
	            data.sdri = this.getMoneyGramSendDetailReport(parameters);
        	} else {
        		data.sdriwt = this.getMoneyGramSendDetailReportWithTax(parameters);
        	}
            
            if (MessageFacade.getInstance().isReportCanceled()) {
                MessageFacade.getInstance().resetReportCanceled();
                data = null;
            }
        }
     
        if (data != null) {
        	data.bpdri = this.getMoneyGramBillPayDetailReport(parameters);

            if (MessageFacade.getInstance().isReportCanceled()) {
                MessageFacade.getInstance().resetReportCanceled();
                data = null;
            }
        }

        MessageFacade.hideDialogNow();
    	
    	return data;
    }

    private boolean getHostData(final long timeStamp, final DateFormat df) {
        MoneyGramReportData data = getMoneyGramReportData(false);
        Date activityDate = new Date(timeStamp);

        // put data into variables
        
        if ((data != null) && (status == OKAY)) {
            sendDetailItems = ReportLog.getNewDetailRecordCollection();                   
            receiveDetailItems = ReportLog.getNewDetailRecordCollection();
            receiveToCardDetailItems = ReportLog.getNewDetailRecordCollection();
            reversalDetailItems = ReportLog.getNewDetailRecordCollection();
            receiveCurrencyDetailItems = ReportLog.getNewDetailRecordCollection();
            receiveToCardCurrencyDetailItems = ReportLog.getNewDetailRecordCollection();
            detailItems = ReportLog.getNewDetailRecordCollection();
            sendCurrencyDetailItems = ReportLog.getNewDetailRecordCollection();
            sendReversalCurrencyDetailItems = ReportLog.getNewDetailRecordCollection();
            utilityDetailItems = ReportLog.getNewDetailRecordCollection();
            prepaidDetailItems = ReportLog.getNewDetailRecordCollection();
                               
            if (data.sdri != null) {
                for (int i = 0; i < data.sdri.length; i++) {
                    final MoneyGramSendDetailInfo sdriRec = data.sdri[i];
                    if (!sdriRec.getPaymentType().equals(PaymentType.CANCL)) {	
                        sendDetailItems.add(new MoneyGramDetail(df.format(activityDate),sdriRec));                                
                        detailItems.add(new MoneyGramDetail(df.format(activityDate),sdriRec));
                    } else {
                    	MoneyGramReverseDetail mgrd = new MoneyGramReverseDetail(df.format(activityDate), sdriRec);                                 
                        reversalDetailItems.add(mgrd);                                    
                        detailItems.add(mgrd);
                    }
                }
            }
            if (data.bpdri != null) {
                for (int i = 0; i < data.bpdri.length; i++) {
                    final BillPaymentDetailInfo updriRec = data.bpdri[i];                           
 
                    if(!updriRec.getStatus().equals(TransactionStatusType.CANCL)){
                        if(updriRec.getProductVariant().equals(ProductVariantType.UBP)){
                        utilityDetailItems.add(new MoneyGramDetail(df.format(activityDate),updriRec,false));                                
                        detailItems.add(new MoneyGramDetail(df.format(activityDate),updriRec,false));
                        }
                        if(updriRec.getProductVariant().equals(ProductVariantType.PREPAY)){
                            prepaidDetailItems.add(new MoneyGramDetail(df.format(activityDate),updriRec,true));                                
                            detailItems.add(new MoneyGramDetail(df.format(activityDate),updriRec,true)); 
                        }
                    } else{
                    	MoneyGramReverseDetail mgbprd = new MoneyGramReverseDetail(df.format(activityDate), updriRec);  
                        reversalDetailItems.add(mgbprd);                                    
                        detailItems.add(mgbprd);
                    }                            
                }
            }
           
            if (data.rdri != null) {
                MoneyGramDetail lastDetail = null;
                for (int i = 0; i < data.rdri.length; i++) {
                    final MoneyGramDetail curDetail =
                        new MoneyGramDetail(df.format(activityDate), data.rdri[i]);
                    if ((lastDetail != null) &&
                        lastDetail.getReferenceNumber().equals(
                        curDetail.getReferenceNumber())) {
                        lastDetail.setAmount(
                            lastDetail.getAmount().add(curDetail.getAmount()));
                    } else {
                        if (lastDetail != null) {
                        	if (lastDetail.getType() 
                        			== ReportLogConstants.MONEYGRAM_RECEIVE_TYPE_PPC) {
                                receiveToCardDetailItems.add(lastDetail);                                       
                        	} else {
                                receiveDetailItems.add(lastDetail);                                       
                        	}
                            detailItems.add(lastDetail);
                        }
                        lastDetail = curDetail;
                    }
                }
				if (lastDetail != null) {
					if (lastDetail.getType() 
							== ReportLogConstants.MONEYGRAM_RECEIVE_TYPE_PPC) {
						receiveToCardDetailItems.add(lastDetail);
					} else {
						if (!UnitProfile.getInstance().isProfileUseOurPaper()) {
							lastDetail.setSerialNumber("");
						}
						receiveDetailItems.add(lastDetail);
					}
					detailItems.add(lastDetail);
				}
            }
        }
        
        if (data != null) {
            setPrintFlag(sendDetailItems,    
           		     	 utilityDetailItems,  
					     prepaidDetailItems,  
					     receiveDetailItems , 
					     receiveToCardDetailItems, 
					     reversalDetailItems );
        	 
        	reportSummary = new ReportSummary(detailItems);
        	return true;
        } else {
        	return false;
        }
    }

    /*
     * Gets data from the middleware for the MoneyGram Sales report with non-MGI tax.
     * On failure return false.
     */
    private boolean getHostDataWithTax(final long timeStamp, final DateFormat df) {
        MoneyGramReportData data = getMoneyGramReportData(true);
        Date activityDate = new Date(timeStamp);

        // put data into variables
        if ((data != null) && (status == OKAY)) {
            sendDetailItems = ReportLog.getNewDetailRecordCollection();                   
            receiveDetailItems = ReportLog.getNewDetailRecordCollection();
            receiveToCardDetailItems = ReportLog.getNewDetailRecordCollection();
            reversalDetailItems = ReportLog.getNewDetailRecordCollection();
            receiveCurrencyDetailItems = ReportLog.getNewDetailRecordCollection();
            receiveToCardCurrencyDetailItems = ReportLog.getNewDetailRecordCollection();
            detailItems = ReportLog.getNewDetailRecordCollection();
            sendCurrencyDetailItems = ReportLog.getNewDetailRecordCollection();
            sendReversalCurrencyDetailItems = ReportLog.getNewDetailRecordCollection();
            utilityDetailItems = ReportLog.getNewDetailRecordCollection();
            prepaidDetailItems = ReportLog.getNewDetailRecordCollection();
                               
            if (data.sdriwt != null) {
                for (int i = 0; i < data.sdriwt.length; i++) {
                    final MoneyGramSendWithTaxDetailInfo sdriRec = data.sdriwt[i];
					if (!sdriRec.getPaymentType().equals(
							PaymentType.CANCL)) { 
						sendDetailItems.add(new MoneyGramDetail(df.format(activityDate), sdriRec));
						detailItems.add(new MoneyGramDetail(df.format(activityDate), sdriRec));
					} else {
						MoneyGramReverseDetail mgrd = new MoneyGramReverseDetail(df.format(activityDate), sdriRec);
						reversalDetailItems.add(mgrd);
						detailItems.add(mgrd);
					}  
                }
            }
            if (data.bpdri != null) {
                for (int i = 0; i < data.bpdri.length; i++) {
                    final BillPaymentDetailInfo updriRec = data.bpdri[i];                           
 
                    if(!updriRec.getStatus().equals(TransactionStatusType.CANCL)){
                        if(updriRec.getProductVariant().equals(ProductVariantType.UBP)){
                        utilityDetailItems.add(new MoneyGramDetail(df.format(activityDate),updriRec,false));                                
                        detailItems.add(new MoneyGramDetail(df.format(activityDate),updriRec,false));
                        }
                        if(updriRec.getProductVariant().equals(ProductVariantType.PREPAY)){
                            prepaidDetailItems.add(new MoneyGramDetail(df.format(activityDate),updriRec,true));                                
                            detailItems.add(new MoneyGramDetail(df.format(activityDate),updriRec,true)); 
                        }
                    }
                    else{
                    	MoneyGramReverseDetail mgbprd = new MoneyGramReverseDetail(df.format(activityDate), updriRec);  
                        reversalDetailItems.add(mgbprd);                                    
                        detailItems.add(mgbprd);
                    }
                }
            }

            if (data.rdri != null) {
                MoneyGramDetail lastDetail = null;
                for (int i = 0; i < data.rdri.length; i++) {
					final MoneyGramDetail curDetail = new MoneyGramDetail(
							df.format(activityDate), data.rdri[i]);
                    if ((lastDetail != null) &&
                        lastDetail.getReferenceNumber().equals(
                        curDetail.getReferenceNumber())) {
                        lastDetail.setAmount(
                            lastDetail.getAmount().add(curDetail.getAmount()));
                    }
                    else {
                        if (lastDetail != null) {
                        	if (lastDetail.getType() 
                        			== ReportLogConstants.MONEYGRAM_RECEIVE_TYPE_PPC) {
                                receiveToCardDetailItems.add(lastDetail);                                       
                        	} else {
                                receiveDetailItems.add(lastDetail);                                       
                        	}
                            detailItems.add(lastDetail);
                        }
                        lastDetail = curDetail;
                    }
                }
				if (lastDetail != null) {
					if (lastDetail.getType() 
							== ReportLogConstants.MONEYGRAM_RECEIVE_TYPE_PPC) {
						receiveToCardDetailItems.add(lastDetail);
					} else {
						if (!UnitProfile.getInstance().isProfileUseOurPaper()) {
							lastDetail.setSerialNumber("");
						}
						receiveDetailItems.add(lastDetail);
					}
					detailItems.add(lastDetail);
				}
            }
        }
        
        if (data != null) {
        	setPrintFlag(sendDetailItems,    
       		     	 utilityDetailItems,  
				     prepaidDetailItems,  
				     receiveDetailItems , 
				     receiveToCardDetailItems, 
				     reversalDetailItems );
        	 
        	reportSummary = new ReportSummary(detailItems);
            return true;
        } else {
        	return Boolean.FALSE;
        }
    }

    private Collection<ReportDetail> getHostDataForCsv(final long timeStamp, final DateFormat df) {
        MoneyGramReportData data = getMoneyGramReportData(false);
		Date activityDate = new Date(timeStamp);
		
		// put data into variables
		if ((data != null) && (status == OKAY)) {
			sendDetailItems = ReportLog.getNewDetailRecordCollection();                   
			detailItems = ReportLog.getNewDetailRecordCollection();
			                   
			if (data.sdri != null) {
			    for (int i = 0; i < data.sdri.length; i++) {
			        final MoneyGramSendDetailInfo sdriRec = data.sdri[i];
			        if (!sdriRec.getPaymentType().equals(PaymentType.CANCL)) {                         
			            detailItems.add(new MoneyGramDetail(df.format(activityDate),sdriRec));
			        }
			        else {
			        	MoneyGramReverseDetail mgrd = new MoneyGramReverseDetail(df.format(activityDate), sdriRec);                              
			            detailItems.add(mgrd);
			        }
			    }
			}
			if (data.bpdri != null) {
			    for (int i = 0; i < data.bpdri.length; i++) {
			        final BillPaymentDetailInfo updriRec = data.bpdri[i];                           
			
			        if(!updriRec.getStatus().equals(TransactionStatusType.CANCL)){
			            if(updriRec.getProductVariant().equals(ProductVariantType.UBP))					                                           
			            	detailItems.add(new MoneyGramDetail(df.format(activityDate),updriRec,false));
			            if(updriRec.getProductVariant().equals(ProductVariantType.PREPAY))					                                               
			                detailItems.add(new MoneyGramDetail(df.format(activityDate),updriRec,true)); 					            
			        }
			        else{
			            MoneyGramBillPayReverseDetail mgbprd = new MoneyGramBillPayReverseDetail(df.format(activityDate), updriRec);                                  
			            detailItems.add(mgbprd);
			        }                            
			    }
			}
			
			receiveDetailItems = ReportLog.getNewDetailRecordCollection();
			receiveToCardDetailItems = ReportLog.getNewDetailRecordCollection();
			if (data.rdri != null) {
			    MoneyGramDetail lastDetail = null;
			    for (int i = 0; i < data.rdri.length; i++) {
			        final MoneyGramDetail curDetail =
			            new MoneyGramDetail(df.format(activityDate), data.rdri[i]);
			        if ((lastDetail != null) &&
			            lastDetail.getReferenceNumber().equals(
			            curDetail.getReferenceNumber())) {
			            lastDetail.setAmount(
			                lastDetail.getAmount().add(curDetail.getAmount()));
			        }
			        else {
			            if (lastDetail != null) {                                      
                        	if (lastDetail.getType() 
                        			== ReportLogConstants.MONEYGRAM_RECEIVE_TYPE_PPC) {
                                receiveToCardDetailItems.add(lastDetail);                                       
                        	} else {
                                receiveDetailItems.add(lastDetail);                                       
                        	}
			                detailItems.add(lastDetail);
			            }
			            lastDetail = curDetail;
			        }
			    }
			    if (lastDetail != null) {                            	
			        detailItems.add(lastDetail);
			    }
			}
		}
				
		return detailItems;
	}

	private Collection<ReportDetail> getHostDataForCsvWithTax(final long timeStamp,
			final ReportSelectionCriteria rsc, final DateFormat df) {

        MoneyGramReportData data = getMoneyGramReportData(true);
		Date activityDate = new Date(timeStamp);

		// put data into variables
		if ((data != null) && (status == OKAY)) {
			sendDetailItems = ReportLog.getNewDetailRecordCollection();
			detailItems = ReportLog.getNewDetailRecordCollection();

			if (data.sdriwt != null) {
				for (int i = 0; i < data.sdriwt.length; i++) {
					final MoneyGramSendWithTaxDetailInfo sdriRec = data.sdriwt[i];
					if (!sdriRec.getPaymentType().equals(PaymentType.CANCL)) {
						detailItems.add(new MoneyGramDetail(
								df.format(activityDate), sdriRec));
					} else {
						MoneyGramReverseDetail mgrd = new MoneyGramReverseDetail(
								df.format(activityDate), sdriRec);
						detailItems.add(mgrd);
					}
				}
			}
			if (data.bpdri != null) {
				for (int i = 0; i < data.bpdri.length; i++) {
					final BillPaymentDetailInfo updriRec = data.bpdri[i];

					if (!updriRec.getStatus().equals(
							TransactionStatusType.CANCL)) {
						if (updriRec.getProductVariant().equals(
								ProductVariantType.UBP))
							detailItems.add(new MoneyGramDetail(
									df.format(activityDate),
									updriRec, false));
						if (updriRec.getProductVariant().equals(
								ProductVariantType.PREPAY))
							detailItems.add(new MoneyGramDetail(
									df.format(activityDate),
									updriRec, true));
					} else {
						MoneyGramBillPayReverseDetail mgbprd = new MoneyGramBillPayReverseDetail(
								df.format(activityDate),
								updriRec);
						detailItems.add(mgbprd);
					}
				}
			}

			receiveDetailItems = ReportLog
					.getNewDetailRecordCollection();
			receiveToCardDetailItems = ReportLog
					.getNewDetailRecordCollection();
			
			if (data.rdri != null) {
				MoneyGramDetail lastDetail = null;
				for (int i = 0; i < data.rdri.length; i++) {
					final MoneyGramDetail curDetail = new MoneyGramDetail(
							df.format(activityDate), data.rdri[i]);
					if ((lastDetail != null)
							&& lastDetail.getReferenceNumber().equals(
									curDetail.getReferenceNumber())) {
						lastDetail.setAmount(lastDetail.getAmount()
								.add(curDetail.getAmount()));
					} else {
						if (lastDetail != null) {
							if (lastDetail.getType() == ReportLogConstants.MONEYGRAM_RECEIVE_TYPE_PPC) {
								receiveToCardDetailItems
										.add(lastDetail);
							} else {
								receiveDetailItems.add(lastDetail);
							}
							detailItems.add(lastDetail);
						}
						lastDetail = curDetail;
					}
				}
				if (lastDetail != null) {
					detailItems.add(lastDetail);
				}
			}
		}
		return detailItems;
	}
    
    /**
     * Call back method for subclasses to provide handling for additional names
     */
    @Override
	protected void subStartElement(final String name, final Attributes attrs) {
    	boolean rtcEnabled = UnitProfile.getInstance().get(DWValues.RECV_OTHER_PAYOUT_TYPE, "NONE")
		.equalsIgnoreCase(DWValues.RECV_TO_CARD); 
    	
        if (name.equals("mgsenddetail")){	
            new MoneyGramDetailPrinter(this,sendDetailItems,attrs,"send").print(isSourceHost); 
        }
        else if (name.equals("mgutilitydetail")) {	      	
        	new MoneyGramDetailPrinter(this,utilityDetailItems,attrs,"utility").print(isSourceHost);
        }
        else if (name.equals("mgprepaiddetail")) {	      	
        	new MoneyGramDetailPrinter(this,prepaidDetailItems,attrs,"prepaid").print(isSourceHost);
        }
        else if (name.equals("mgreceivedetail")&& UnitProfile.getInstance().isProfileUseOurPaper()) {	      	
        	new MoneyGramDetailPrinter(this,receiveDetailItems,attrs,"receive").print(isSourceHost);
        }
        else if (name.equals("mgreceivedetailWithoutChecknum")&& !UnitProfile.getInstance().isProfileUseOurPaper()) {	      	
        	new MoneyGramDetailPrinter(this,receiveDetailItems,attrs,"receive").print(isSourceHost);
        }
        else if (rtcEnabled && name.equals("mgreceivetocarddetail")) {	      	
        	new MoneyGramDetailPrinter(this,receiveToCardDetailItems,attrs,"receiveToCard").print(isSourceHost);
        }
//        else if (rtcEnabled && name.equals("mgreceivetocarddetailWithoutChecknum")
//        		&& !UnitProfile.getInstance().isProfileUseOurPaper()) {	      	
//        	new MoneyGramDetailPrinter(this,receiveToCardDetailItems,attrs,"receiveToCard").print(isSourceHost);
//        }
        else if (name.equals("mgreceivecurrencydetail")) {
        	setReceiveCurrencyDetailItems();	        	
        	new MoneyGramCurrencyDetailPrinter(this,receiveCurrencyDetailItems,attrs,"receive").print(isSourceHost);
        }
        else if (name.equals("mgreceivetocardcurrencydetail")) {
        	setReceiveToCardCurrencyDetailItems();	        	
        	new MoneyGramCurrencyDetailPrinter(this,receiveToCardCurrencyDetailItems,attrs,"receivetocard").print(isSourceHost);
        }
        else if (name.equals("mgsendcurrencydetail")) {
        	setSendCurrencyDetailItems();	        	
        	new MoneyGramCurrencyDetailPrinter(this,sendCurrencyDetailItems,attrs,"send").print(isSourceHost);
        }
        
        /*
         *  public MoneyGramCurrencyDetailPrinter(  final Report report,
            final Collection detailItems,
            final Attributes attrs,
            final String direction,
			final boolean receiveCurrencySummary,
			final boolean sendCurrencySummary,
			final boolean sendReversalCurrencySummary) {
         */
        else if (name.equals("mgsendcurrencysummary")) {
        	setSendCurrencyDetailItems();     	        	
        	new MoneyGramCurrencyDetailPrinter(this,sendCurrencyDetailItems,attrs,"send",false,true,false).print(isSourceHost);
        }
        else if (name.equals("mgreceivecurrencysummary")) {
        	setReceiveCurrencyDetailItems();     	        	
        	new MoneyGramCurrencyDetailPrinter(this,receiveCurrencyDetailItems,attrs,"receive",true,false,false).print(isSourceHost);
        }
        else if (name.equals("mgreceivetocardcurrencysummary")) {
        	setReceiveToCardCurrencyDetailItems();     	        	
        	new MoneyGramCurrencyDetailPrinter(this,receiveToCardCurrencyDetailItems,attrs,"receiveToCard",true,false,false).print(isSourceHost);
        }
        else if (name.equals("mgreversalCurrencysummary")){	
        	setSendReversalCurrencyDetailItems();
        new MoneyGramCurrencyDetailPrinter(this,sendReversalCurrencyDetailItems,attrs,"reversal",false,false,true).print(isSourceHost); 
          }
        else if (name.equals("mgreversaldetail"))	
            new MoneyGramDetailPrinter(this,reversalDetailItems,attrs,"reversal").print(isSourceHost); 
    }
    
    private Collection<ReportDetail> setReceiveCurrencyDetailItems() {
    	
    	for (Map.Entry<String, CountAndAmount> entry : reportSummary.getReceiveCurrencyMap().entrySet()) {
    		CountAndAmount ca = entry.getValue();
			MoneyGramDetail mgd = new MoneyGramDetail();
			mgd.setCurrencyCode(entry.getKey());
			mgd.setAmount(ca.getBigDecimalAmount());
			mgd.setReceiveCurrencyCount(String.valueOf(ca.getCount()));
    		receiveCurrencyDetailItems.add(mgd);
    	}   
    	return receiveCurrencyDetailItems;
    }
    
    private Collection<ReportDetail> setReceiveToCardCurrencyDetailItems() {
    	
    	for (Map.Entry<String, CountAndAmount> entry : reportSummary.getReceiveToCardCurrencyMap().entrySet()) {
    		String code = entry.getKey();
    		CountAndAmount ca = entry.getValue();
			MoneyGramDetail mgd = new MoneyGramDetail();
			mgd.setCurrencyCode(code);
			mgd.setAmount(ca.getBigDecimalAmount());
			mgd.setReceiveCurrencyCount(String.valueOf(ca.getCount()));
    		receiveToCardCurrencyDetailItems.add(mgd);
    	}   
    	return receiveToCardCurrencyDetailItems;
    }
    
    private Collection<ReportDetail> setSendCurrencyDetailItems() {
    	
    	for (Map.Entry<String, CountAndAmount> entry : reportSummary.getSendCurrencyMap().entrySet()) {
    		String code = entry.getKey();
    		CountAndAmount ca = entry.getValue();
			MoneyGramDetail mgd = new MoneyGramDetail();
			mgd.setCurrencyCode(code);
			mgd.setAmount(ca.getBigDecimalAmount());
			mgd.setSendCurrencyCount(String.valueOf(ca.getCount()));
			mgd.setFee(ca.getBigDecimalFeeAmount());
    		sendCurrencyDetailItems.add(mgd);
    	}   
    	return sendCurrencyDetailItems;
    }
    
    private Collection<ReportDetail> setSendReversalCurrencyDetailItems() {
    	
    	for (Map.Entry<String, CountAndAmount> entry : reportSummary.getSendReversalCurrencyMap().entrySet()) {
    		String code = entry.getKey();
    		CountAndAmount ca = entry.getValue();
			MoneyGramDetail mgd = new MoneyGramDetail();
			mgd.setCurrencyCode(code);
			mgd.setAmount(ca.getBigDecimalAmount());
			mgd.setSendReversalCurrencyCount(String.valueOf(ca.getCount()));
			mgd.setFee(ca.getBigDecimalFeeAmount());
    		sendReversalCurrencyDetailItems.add(mgd);
    	}   
    	return sendReversalCurrencyDetailItems;
    }
    
    private void setPrintFlag(Collection<ReportDetail> sendDetailItems,
			Collection<ReportDetail> utilityDetailItems, Collection<ReportDetail> prepaidDetailItems,
			Collection<ReportDetail> receiveDetailItems, Collection<ReportDetail> receiveToCardDetailItems,
			Collection<ReportDetail> reversalDetailItems) {
    	ProfileAccessor authObj = ProfileAccessor.getInstance();
		superAgentMode = UnitProfile.getInstance().isSuperAgent();

		// General Services and Tax indicator
		addFlag("gstReport", gstSelector);
		addFlag("nonGstReport", !gstSelector);

    	boolean rtcEnabled = UnitProfile.getInstance().get(DWValues.RECV_OTHER_PAYOUT_TYPE, "NONE")
		.equalsIgnoreCase(DWValues.RECV_TO_CARD); 
    	
		if (utilityDetailItems != null && utilityDetailItems.size() > 0
				|| prepaidDetailItems != null && prepaidDetailItems.size() > 0)
			addFlag("utilityHeader", true);
		else if(superAgentMode){
			addFlag("utilityHeader", true);
		}
		else {
			if (authObj.getTranAuth(BILL_PAYMENT_DOC_SEQ))
				addFlag("utilityHeader", true);
			else
				addFlag("utilityHeader", false);
		}
		addFlag("nosendsummarydetail", true);	
		if (sendDetailItems != null && sendDetailItems.size() > 0) {
			addFlag("isSendData", true);
			addFlag("nosendsummarydetail", false);
			addFlag("isNonTaxAgent", !gstSelector);
			addFlag("isMalayAgent", gstSelector);
			addFlag("isNotMalayAgent", !gstSelector);
		}
		else if(superAgentMode){
			addFlag("isSendData", true);
			addFlag("sendnodetail", true); 
		}
		else {
			if (authObj.getTranAuth(MONEY_GRAM_SEND_DOC_SEQ)
					|| (authObj.getExpressPayAllowed() && authObj
							.getTranAuth(BILL_PAYMENT_DOC_SEQ))) {
				addFlag("isSendData", true);
				addFlag("sendnodetail", true); 
			}

			else {
				addFlag("isSendData", false); 
				addFlag("sendnodetail", false); 
			}
		}

		if (utilityDetailItems != null && utilityDetailItems.size() > 0)
			addFlag("isUtilityData", true);
		else if(superAgentMode){
			addFlag("isUtilityData", true);
			addFlag("utilitynodetail", true); 
			}
				
		else {
			if (authObj.getTranAuth(BILL_PAYMENT_DOC_SEQ)) {
				addFlag("isUtilityData", true);
				addFlag("utilitynodetail", true); 
			} else {
				addFlag("isUtilityData", false);
				addFlag("utilitynodetail", false); 
			}
		}

		if (prepaidDetailItems != null && prepaidDetailItems.size() > 0)  
			addFlag("isPrepaidData", true);
		
		else if(superAgentMode){
			addFlag("isPrepaidData", true);
			addFlag("prepaidnodetail", true); 
			}
		else {
			if (authObj.getTranAuth(BILL_PAYMENT_DOC_SEQ)) {
				addFlag("isPrepaidData", true);
				addFlag("prepaidnodetail", true); 
			} else {
				addFlag("isPrepaidData", false);
				addFlag("prepaidnodetail", false); 
			}
		}
		addFlag("nosummarydetail", true);
		if (receiveDetailItems != null && receiveDetailItems.size() > 0){
			addFlag("isReceiveData", true);
			addFlag("nosummarydetail", false);
			addFlag("hasdetailCurrencyTotal", true);
		}
		else if(superAgentMode){
			addFlag("receivenodetail", true); 
			addFlag("isReceiveData", true);
			addFlag("hasdetailCurrencyTotal", true);
			}
		else {
			if (authObj.getTranAuth(MONEY_GRAM_RECEIVE_DOC_SEQ)) {
				addFlag("receivenodetail", true); 
				addFlag("isReceiveData", true);
			} else {
				addFlag("receivenodetail", false); 
				addFlag("isReceiveData", false);
			}
		}
		
		if (rtcEnabled && receiveToCardDetailItems != null
				&& receiveToCardDetailItems.size() > 0) {
			addFlag("isReceiveToCardData", true);
			addFlag("hasdetailCurrencyTotalRTC", true);
		} else if (superAgentMode) {
			addFlag("receivetocarddetail", true); 
			addFlag("isReceiveToCardData", true);
			addFlag("nortcsummarydetail", true);
			addFlag("hasdetailCurrencyTotalRTC", true);
		}
		else {
			/*
			 * Add RTC section to report if RTC enabled, or if we have RCT
			 * items. 
			 */
			if (rtcEnabled 
					|| (receiveToCardDetailItems != null 
							&& receiveToCardDetailItems.size() > 0)) {
				addFlag("receivetocarddetail", true); 
				addFlag("isReceiveToCardData", true);
				addFlag("nortcsummarydetail", false);
				addFlag("hasdetailCurrencyTotalRTC", true);
			} else {
				addFlag("receivetocarddetail", false); 
				addFlag("isReceiveToCardData", false);
			}
		}
		
		addFlag("noreversalsummarydetail", true);
		if (reversalDetailItems != null && reversalDetailItems.size() > 0){
			addFlag("noreversalsummarydetail", false);
			addFlag("isReversalData", true);
		}
		else if(superAgentMode){
			addFlag("reversalnodetail", true); 
			addFlag("isReversalData", true);
			}
		else {
			if ( authObj
					.getTranAuth(BILL_PAYMENT_DOC_SEQ)
					|| authObj.getTranAuth(MONEY_GRAM_SEND_DOC_SEQ)) {
				addFlag("isReversalData", true);
				addFlag("reversalnodetail", true); 
			} else {
				addFlag("isReversalData", false);
				addFlag("reversalnodetail", false); 
			}
		}

	}
   
}

class MoneyGramDetailPrinter extends DetailPrinter {
    private BigDecimal faceAmtTotal = BigDecimal.ZERO;
    private BigDecimal feeTotal = BigDecimal.ZERO;
    private BigDecimal taxTotal = BigDecimal.ZERO;
    private BigDecimal count = BigDecimal.ZERO;
    private String direction;
    private boolean detailFlag = false;
    private boolean footerFlag = false;
    private boolean agentNameFlag = false; 
    boolean multiAgentMode = false;   
    boolean nonMultiAgentMode = false;
    boolean noTaxFlag = false;

    public MoneyGramDetailPrinter(  final Report report,
                                    final Collection<ReportDetail> detailItems,
                                    final Attributes attrs,
                                    final String direction) {

        super(report,detailItems,attrs);
        this.direction = direction;
        noTaxFlag = !UnitProfile.getInstance().get("ALLOW_TAX_REPORT", "N").equals("Y");
    }

    @Override
	public void checkDetail(final Object detail,boolean hostReport) {
    	 final HashMap<String, Boolean> flagMap = report.getFlagMap();

		flagMap.put("hasdetailNoTax", Boolean.valueOf(noTaxFlag));
		flagMap.put("hasdetailWithTax", Boolean.valueOf(!noTaxFlag));
		flagMap.put("detailFooterFlag", Boolean.valueOf(footerFlag));
		flagMap.put("detailFooterFlagWithTax", Boolean.valueOf(footerFlag
				&& !noTaxFlag));
		flagMap.put("detailFooterFlagWithNoTax", Boolean.valueOf(footerFlag
				&& noTaxFlag));
		flagMap.put("agentNameHeaderFlag", Boolean.valueOf(agentNameFlag));

        if (detail instanceof MoneyGramReverseDetail){
            final MoneyGramReverseDetail mgd = (MoneyGramReverseDetail) detail;
            final Date time = new Date(mgd.getTime());
           // final SimpleDateFormat formatter = new DwSimpleDateFormat("hh:mma");	
           
            final BigDecimal total = mgd.getTotal();
            final HashMap<String, Object> fieldMap = report.getFieldMap();   
            String agentId =mgd.getAgentID(); 
            String agentName = mgd.getAgentName();
            if(agentId== null )
            	agentId=UnitProfile.getInstance().get("MG_ACCOUNT", "");
            if(agentName==null)
            	agentName=UnitProfile.getInstance().get("AGENT_NAME", "");
              fieldMap.put("agentName",agentId+" "+agentName);
            if (UnitProfile.getInstance().isMultiAgentMode())
                fieldMap.put("emp", mgd.getAgentID()); 
            else
                fieldMap.put("emp", mgd.getEmployeeNumber()); 
            
            fieldMap.put("time", FormatSymbols.formatTimeFor24Hrs(time)); 
            fieldMap.put("sname", mgd.getSender()); 
            fieldMap.put("userid",mgd.getUser()); 
            fieldMap.put("refnum", mgd.getReferenceNumber()); 
            fieldMap.put("total", FormatSymbols.convertDecimal(total.toString())); 
            faceAmtTotal = faceAmtTotal.add(total);
           
            if (mgd.getCurrencyCode() == null || mgd.getCurrencyCode().equals("0") ||
            	    mgd.getCurrencyCode().trim().length() == 0)           	
            		fieldMap.put("currency", "("+UnitProfile.getInstance().get("AGENT_CURRENCY", "")+")");
            	else{
            		//fieldMap.put("currency", mgd.getCurrencyCode());  
            		fieldMap.put("currency", "( "+mgd.getCurrencyCode()+" )");
            	}
        }else
          if (detail instanceof MoneyGramBillPayReverseDetail){
            final MoneyGramBillPayReverseDetail mgd = (MoneyGramBillPayReverseDetail) detail;
            final Date time = new Date(mgd.getTime());
           // final SimpleDateFormat formatter = new DwSimpleDateFormat("hh:mma");	
            
            final BigDecimal total = mgd.getTotal();
            final HashMap<String, Object> fieldMap = report.getFieldMap();  
           
            
            if (UnitProfile.getInstance().isMultiAgentMode())
                fieldMap.put("emp", mgd.getAgentID()); 
            else
                fieldMap.put("emp", mgd.getEmployeeNumber()); 
            
            fieldMap.put("time", FormatSymbols.formatTimeFor24Hrs(time)); 
            fieldMap.put("sname", mgd.getSender()); 
            fieldMap.put("userid",mgd.getUser()); 
            fieldMap.put("refnum", mgd.getReferenceNumber()); 
            fieldMap.put("total", FormatSymbols.convertDecimal(total.toString())); 
            faceAmtTotal = faceAmtTotal.add(total);
            fieldMap.put("currency", "("+UnitProfile.getInstance().get("AGENT_CURRENCY", "")+")");
        }
        else{
            final MoneyGramDetail mgd = (MoneyGramDetail) detail;
            final Date time = new Date(mgd.getDateTime());
            final BigDecimal amount = mgd.getAmount();
            final BigDecimal fee = mgd.getFee();
            final BigDecimal tax = mgd.getTax();
            final HashMap<String, Object> fieldMap = report.getFieldMap();
           String agentId =mgd.getAgentID(); 
           String agentName = mgd.getAgentName();
           if(agentId== null )
           	agentId=UnitProfile.getInstance().get("MG_ACCOUNT", "");
           if(agentName==null)
           	agentName=UnitProfile.getInstance().get("AGENT_NAME", "");
             fieldMap.put("agentName",agentId+" "+agentName);
            if (UnitProfile.getInstance().isMultiAgentMode())
                fieldMap.put("emp", mgd.getAgentID()); 
            else
                fieldMap.put("emp", mgd.getEmployeeNumber());
            fieldMap.put("time", FormatSymbols.formatTimeFor24Hrs(time)); 
            fieldMap.put("name", mgd.getName()); 
            fieldMap.put("refnum", mgd.getReferenceNumber()); 
            if(!UnitProfile.getInstance().isProfileUseOurPaper())
               fieldMap.put("chknum",""); 
            else
            	fieldMap.put("chknum", mgd.getSerialNumber()); 
            fieldMap.put("amount", FormatSymbols.convertDecimal(amount.toString()));
            if (mgd.getType() == ReportLog.MONEYGRAM_RECEIVE_TYPE) {
            	if (mgd.getCurrencyCode() == null || mgd.getCurrencyCode().equals("0") ||
            	    mgd.getCurrencyCode().trim().length() == 0)           	
            		fieldMap.put("currency", "("+UnitProfile.getInstance().get("AGENT_CURRENCY", "")+")");
            	else{
            		//fieldMap.put("currency", mgd.getCurrencyCode());  
            		fieldMap.put("currency", "( "+mgd.getCurrencyCode()+" )");
            	}
            }
            if (mgd.getType() == ReportLog.MONEYGRAM_RECEIVE_TYPE_PPC) {
            	if (mgd.getCurrencyCode() == null || mgd.getCurrencyCode().equals("0") ||
            	    mgd.getCurrencyCode().trim().length() == 0)           	
            		fieldMap.put("currency", "("+UnitProfile.getInstance().get("AGENT_CURRENCY", "")+")");
            	else{
            		//fieldMap.put("currency", mgd.getCurrencyCode());  
            		fieldMap.put("currency", "( "+mgd.getCurrencyCode()+" )");
            	}
            	fieldMap.put("cardnum", StringUtility.stringToMaskedString(
            			mgd.getAccountNumber())); 
            }
            if ((mgd.getType() == ReportLog.MONEYGRAM_SEND_TYPE) ||
            	(mgd.getType() == ReportLog.UTILITY_PAYMENT_TYPE)||
            	(mgd.getType() == ReportLog.EXPRESS_PAYMENT_TYPE)||
            	(mgd.getType() == ReportLog.PREPAID_SERVICES_TYPE)) {
            	if (mgd.getCurrencyCode() == null || mgd.getCurrencyCode().equals("0")
            		|| mgd.getCurrencyCode().trim().length() == 0)
            		fieldMap.put("currency", "("+UnitProfile.getInstance().get("AGENT_CURRENCY", "")+")");
            	else{
            		//fieldMap.put("currency", mgd.getCurrencyCode());
            		fieldMap.put("currency", "( "+mgd.getCurrencyCode()+" )");
            	}
            	    
            }   
            fieldMap.put("fee", FormatSymbols.convertDecimal(fee.toString())); 
			if (mgd.getType() == ReportLog.MONEYGRAM_SEND_TYPE) {
				if (noTaxFlag) {
					fieldMap.put("tax", "NA");
				} else {
					fieldMap.put("tax",
							FormatSymbols.convertDecimal(tax.toString()));
					taxTotal = taxTotal.add(tax);
				}
				fieldMap.put("total", FormatSymbols.convertDecimal((amount
						.add(fee).add(tax)).toString()));
			} else {
				fieldMap.put("total", FormatSymbols.convertDecimal((amount
						.add(fee)).toString()));
			}
            faceAmtTotal = faceAmtTotal.add(amount);
            feeTotal = feeTotal.add(fee);
            count = count.add(new BigDecimal(1));
            fieldMap.put("count", count.toString()); 
            
        }
      if(footerFlag)
        footer();
        printLine();
    }
    
    @Override
	protected void printNoDetailLine(){
    	   noDetail(this.direction);	
    }
    
    @Override
	protected void footer() {   	
        BigDecimal total = faceAmtTotal.add(feeTotal);
        final HashMap<String, Object> fieldMap = report.getFieldMap();
        fieldMap.put("facetotal", FormatSymbols.convertDecimal(faceAmtTotal.toString()));
        fieldMap.put("feetotal", FormatSymbols.convertDecimal(feeTotal.toString())); 
        fieldMap.put("taxtotal", FormatSymbols.convertDecimal(taxTotal.toString())); 
        fieldMap.put(direction + "total",FormatSymbols.convertDecimal(total.toString())); 
        
        faceAmtTotal= BigDecimal.ZERO;
        feeTotal= BigDecimal.ZERO;
        taxTotal = BigDecimal.ZERO;
        count= BigDecimal.ZERO;
    }
    
    @Override
	protected void checkFooter(boolean flag) {
    	this.footerFlag = flag;
    }
    
    @Override
	protected void checkAgentNameHeader(boolean flag) {
    	this.agentNameFlag = flag;
    	
    }
    @Override
	protected void checkDetailHeader(boolean header) {
		multiAgentMode = UnitProfile.getInstance().isMultiAgentMode();
		nonMultiAgentMode = !UnitProfile.getInstance().isMultiAgentMode();
		this.detailFlag = header;
		final HashMap<String, Boolean> flagMap = report.getFlagMap();

		if (header) {
			if (!noTaxFlag) {
				flagMap.put("detailHeaderFlagWithTax", Boolean.TRUE);
				if (multiAgentMode) {
					flagMap.put("detailHeaderFlagSuperWithTax", Boolean.TRUE);
					flagMap.put("detailHeaderFlagSuperNoTax", Boolean.FALSE);
				} else {
					flagMap.put("detailHeaderFlagNonSuperWithTax", Boolean.TRUE);
					flagMap.put("detailHeaderFlagNonSuperNoTax", Boolean.FALSE);
				}
			} else {
				flagMap.put("detailHeaderFlagWithNoTax", Boolean.TRUE);
				flagMap.put("detailHeaderFlagSuperNoTax", Boolean.valueOf(
						(detailFlag && multiAgentMode)));
				flagMap.put("detailHeaderFlagNonSuperNoTax", Boolean.valueOf(
						detailFlag && nonMultiAgentMode));
			}
			flagMap.put("detailHeaderFlagSuper", Boolean.valueOf(
					(detailFlag && multiAgentMode)));
			flagMap.put("detailHeaderFlagNonSuper", Boolean.valueOf(detailFlag
					&& nonMultiAgentMode));
		} else {
			flagMap.put("detailHeaderFlagWithTax", Boolean.FALSE);
			flagMap.put("detailHeaderFlagWithNoTax", Boolean.FALSE);
			flagMap.put("detailHeaderFlagSuper", Boolean.FALSE);
			flagMap.put("detailHeaderFlagNonSuper", Boolean.FALSE);
			flagMap.put("detailHeaderFlagSuperNoTax", Boolean.FALSE);
			flagMap.put("detailHeaderFlagSuperWithTax", Boolean.FALSE);
			flagMap.put("detailHeaderFlagNonSuperNoTax", Boolean.FALSE);
			flagMap.put("detailHeaderFlagNonSuperWithTax", Boolean.FALSE);
		}

		flagMap.put("detailHeaderFlag", Boolean.valueOf(detailFlag));
    }
}

class MoneyGramCurrencyDetailPrinter extends DetailPrinter {

    private String direction;
    final HashMap<String, Object> fieldMap = report.getFieldMap();
    
    public MoneyGramCurrencyDetailPrinter(  final Report report,
                                    final Collection<ReportDetail> detailItems,
                                    final Attributes attrs,
                                    final String direction) {

        super(report,detailItems,attrs);
        this.direction = direction;
    }
    
    public MoneyGramCurrencyDetailPrinter(  final Report report,
            final Collection<ReportDetail> detailItems,
            final Attributes attrs,
            final String direction,
			final boolean receiveCurrencySummary,
			final boolean sendCurrencySummary,
			final boolean sendReversalCurrencySummary) {

    	super(report,detailItems,attrs,receiveCurrencySummary,sendCurrencySummary,sendReversalCurrencySummary);
    	this.direction = direction;
}

    @Override
	public void checkDetail(final Object detail,boolean hostReport) {
    	
    	final MoneyGramDetail mgd = (MoneyGramDetail) detail;
        fieldMap.put("currency", mgd.getCurrencyCode());
        if(direction.equals("send")) {
            fieldMap.put("count", mgd.getSendCurrencyCount());
        	fieldMap.put("amount", mgd.getAmount());
        	fieldMap.put("fee", mgd.getFee());
			fieldMap.put("tax", mgd.getTax());
			BigDecimal total = mgd.getAmount();
			if (mgd.getFee() != null) {
				total = total.add(mgd.getFee());
			}
			if (mgd.getTax() != null) {
				total = total.add(mgd.getTax());
			}
        	fieldMap.put("totalamount", total);  
        }
        else if(direction.equals("reversal")) {
            fieldMap.put("count", mgd.getSendReversalCurrencyCount());
        	fieldMap.put("amount", mgd.getAmount());
        	fieldMap.put("fee", mgd.getFee());
			if (mgd.getTax() != null) {
				fieldMap.put("tax", mgd.getTax());
			}
        	BigDecimal total = (mgd.getAmount()).add(mgd.getFee());
        	fieldMap.put("totalamount", total);  
        }
        else {
            fieldMap.put("count", mgd.getReceiveCurrencyCount());
        	fieldMap.put("amount", mgd.getAmount());
        }
        printLine();
    }
    
    @Override
	protected void printNoDetailLine(){
    }
}
