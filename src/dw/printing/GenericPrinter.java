package dw.printing;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import javax.comm.PortInUseException;
import javax.comm.UnsupportedCommOperationException;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.event.PrintJobAdapter;
import javax.print.event.PrintJobEvent;

import dw.dialogs.Dialogs;
import dw.i18n.Messages;
import dw.main.MainPanelDelegate;
import dw.profile.UnitProfile;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.ErrorManager;
import dw.utility.ExtraDebug;
import dw.utility.IdleBackoutTimer;
import flags.CompileTimeFlags;

/**
 * Represents a Generic printer for printing receipts.
 *
 * NOTE:  When running this application, the javax.comm.properties file must be 
 * in the same directory as the comm.jar file, and this must be
 * on the same drive as the application.  Otherwise it will not find the LPT1 port.
 *
 * Consequently, the actual sending of data to the parallel port is done in
 * a separate thread so that the rest of the code can continue.  The thread
 * writes data in buf to the printer periodically (500 millis now).
 *
 * @version 7/7/2004
 * @author John Allen
 *  modified by Patrick Ottman
 * Copyright (c) 2004 MoneyGram, Inc.
 */

public class GenericPrinter extends TEPrinter implements Printable  {
    /** Single Instance of this printer */
    private static volatile GenericPrinter instance = null;

    /** Thread used to do the actual send to the port. */
    //private Thread thread;

    /** Used to turn off the thread's run loop. */
    private boolean printerActive;

    /** Period of time the thread waits between each check and sending of data. */
    static final int waitTime = 500;

    // justification
    private boolean centered = false;
    private boolean paragraph = false;
    private boolean left = false;
    private StringBuffer paragraphSB = null;
    private StringBuffer leftSB = null;
    private StringBuffer centerSB = null;

    // character set - default to Cp1252; set to UTF-8 from external sources

    //-------------------------------------------------------------------------
    // constructor
    //-------------------------------------------------------------------------
    /** 
     * Uses the communications API to open and write to a printer.  
     * It also initializes the printer.
     * @throws java.io.IOException
     * @throws javax.comm.NoSuchPortException
     * @throws javax.comm.PortInUseException
     * @throws javax.comm.UnsupportedCommOperationException
     */
    public GenericPrinter() throws PortInUseException, IOException, UnsupportedCommOperationException {
        printerActive = true;

        // give thread time to check the port
        try {
            Thread.sleep(waitTime*4);  // sleep for 2 seconds
        } catch (InterruptedException e) { }

        if (count > 0 && UnitProfile.getInstance().isDemoMode()){
            // stop the thread from running
            printerActive = false;
        }
        setPrinter(this);
    }

   /**
     * Returns the Singleton object.
     * @return The sole instance of the EpsonTMT88II class.
     */
    public static GenericPrinter getInstance() {
        if (instance == null) {
            try {
                instance = new GenericPrinter(); 
            }
            catch (Exception e) {
                ErrorManager.handle(e, "printerInstantiationError", Messages.getString("ErrorMessages.printerInstantiationError"),	
                                                      ErrorManager.ERROR, true);
            }
        }
        return instance;
    }

    public static void clearInstance(){
        instance = null;
    }

    /**
     * Check for a default printer setup by windows and returns true if one is found.
     *
     */
    @Override
	public boolean isReady(javax.swing.JComponent c) {
        // is the printer ready to go?
        if (UnitProfile.getInstance().isDemoMode() || DWValues.getDefaultPrinter() != null)
            return true;

        boolean timerRunning = IdleBackoutTimer.isRunning();
        if (timerRunning) 
            IdleBackoutTimer.pause();

        //Dialogs.showWarning("dialogs/DialogPrinterNotReady.xml");	
        int result = Dialogs.showOkCancel("dialogs/DialogPrinterNotReady.xml");
        if (result != 0){
        	// transaction canceled output isi cancel record with substatus code 01
        	MainPanelDelegate.setupCancelISI("01");
	      	if ("Y".equals(UnitProfile.getInstance().get("ISI_ENABLE", "N"))){
	       		  String programName = "";
	       		  if (MainPanelDelegate.getIsiExeProgramArguments() != null)
	       			  programName = MainPanelDelegate.getIsiExeProgramArguments().trim();
	       		  if (programName.length() > 0 && !(programName.startsWith("-EXE"))) {
  	  	  			  MainPanelDelegate.executeIsiProgram(programName);
	       		  }
	       	  }
	       	  MainPanelDelegate.setIsiTransactionInProgress(false);
	       	  MainPanelDelegate.setIsiExeCommand(false);
	       	  MainPanelDelegate.setIsiExe2Command(false);
	       	  MainPanelDelegate.setIsiExeProgramArguments("");
	       	  MainPanelDelegate.setIsiRpCommandValid(false);
        } else {
        	// Retry the check
        	return (isReady(c));
        }

        if (timerRunning)
            IdleBackoutTimer.start();
        
        return false;
    }

    /** To start printing, we just call writeToPort, right? */
    @Override
	public void fireOffPrintJob() {
        writeToPort();
    }

    /* Sends this object's bytes to the port.  Must be synchronized 
     * because of the reset.  */
    private synchronized boolean writeToPort() {
        boolean ret = false;

        /*
         * there seems to be a problem in javax.print where a DocFlavor
         * of "text/plain" returns nothing.
         */
        DocFlavor flavor = new DocFlavor("application/x-java-jvm-local-objectref", "java.awt.print.Printable");
        PrintService[] services = PrintServiceLookup.lookupPrintServices(flavor, null);

        // verify there is at least one printer set up.
        if(services.length == 0)
            return true;

        PrintService service = services[DWValues.getSelectedPrinter()];    
 
        try {
            PrintRequestAttributeSet prAttributes = new HashPrintRequestAttributeSet();
            
            // setup the data to be printed
            Doc doc = new SimpleDoc(this, flavor, null);
            // Create the print job
            DocPrintJob job = service.createPrintJob();
 
            // Monitor print job events; for the implementation of PrintJobWatcher,
            PrintJobWatcher2 pjDone = new PrintJobWatcher2(job);
    
            job.print(doc, prAttributes);
    
            // Wait for the print job to be done
            Debug.println("Before wait for print completion");
            pjDone.waitForDone();
            Debug.println("After wait for print completion");
    
            // It is now safe to close the input stream
        }
        catch (PrintException e) {
            Debug.println("PrintException: " + e);
        }

        reset();
        return ret;
    }

    /* 
     * Returns number of characters per line set up in the profile.
     */
    private int getCharsPerLine() {
        int r = UnitProfile.getInstance().get("AGENT_PRINTER_COLUMNS",42);
        return r;
    }

    // ---------------------------------------------------------------------
    // overloaded writes  (methods below call these methods)
    // ---------------------------------------------------------------------

    /**
     * Writes a string to the buffer.
     * @param s = a string to print
     * @throws java.io.IOException
     */
    @Override
	public void write(String s) throws IOException {
        //Debug.println("printerActive: "+printerActive+" write("+s+") length = " + s.length());  
        if (printerActive) {
            if (paragraph){
                paragraphSB.append(s);
            }
            else if (centered){
                centerSB.append(s);
            }
            else if (left){
                leftSB.append(s);
            }
            else{
                write(s.getBytes(), 0, s.length());
            }
        }
    }

    /**
     * Writes an array of ints to the buffer.
     * This was added in order to write the logo bit image to the printer.
     * @param ia = a bit image to send to the printer
     * @throws java.io.IOException
     */
    public void write(int[] ia) throws IOException {
        if (printerActive)
            for (int i = 0; i < ia.length; i++)
                write(ia[i]);
    }

    /**
     * Clears the print buffer and resets modes to power-on defaults.
     * This is called in the constructor but is a separate function so that it
     * can also be called at any time to initialize the printer.
     * @throws java.io.IOException
     */
    @Override
	public void initialize() throws IOException {
    }

    /**
     * Ejects and cuts the paper.
     * @throws java.io.IOException
     */
    @Override
	public void eject() throws IOException {
        write(0x0C);
    }

    /** Prints a paragraph of text.
     * @throws java.io.IOException
     */
    @Override
	public void paragraph() {
        paragraphSB = new StringBuffer();
        paragraph = true;
    }

    /** Prints a paragraph of text.
     * @throws java.io.IOException
     */
	@Override
	public void paragraphEnd() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("paragraphEnd["+paragraph+"]" );	
    	}
		if (paragraph && paragraphSB != null && paragraphSB.length() > 0) {
			String s = paragraphSB.toString();
			int point = 0;
			int cr = 0;
			int ch = ' ';
			String ss = "";
			int cpl = UnitProfile.getInstance()
					.get("AGENT_PRINTER_COLUMNS", 42);
			cr = s.indexOf('\n');
			while (s.length() > cpl || (cr > 0)) {
				point = s.lastIndexOf(ch, cpl);
				if ((cr > 0) && (cr < cpl)) {
					if (s.charAt(cr) != '\r') {
						s = s.substring(0, cr) + '\r' + s.substring(cr);
						cr++;
					}
					point = cr;
				}
				if (point < 0)
					point = getCharsPerLine();
				point++;
				if (point > s.length()) {
					point = s.length();
				}
				ss = s.substring(0, point);
				if (!ss.endsWith("\n")) {
					ss = ss.trim() + "\r\n";
				}
				if (centered) {
					ss = centerThisString(ss);
				}
				write(ss.getBytes(), 0, ss.length());
				s = s.substring(point, s.length());
				cr = s.indexOf('\n');
			}
			s = s + "\r\n";
			if (centered) {
				s = centerThisString(s);
			}
			write(s.getBytes(), 0, s.length());
		} 
		paragraphSB = null;
		paragraph = false;
	}

    /** Prints a centered line of text.
     * @throws java.io.IOException
     */
    @Override
	public void center() {
        centered = true;
        centerSB = new StringBuffer();
    }

    /** Ends printing a centered line of text.
     * @throws java.io.IOException
     */
    @Override
	public void centerEnd() throws IOException {
        if(centered && centerSB != null && centerSB.length() > 0){
            String s = centerSB.toString();

            int point = 0;
            int cr = 0;
            int ch = ' ';
            String ss = "";
            int cpl = UnitProfile.getInstance().get("AGENT_PRINTER_COLUMNS",42);
            cr = s.indexOf('\n');
            while (s.length() > cpl || (cr > 0)) {
                point = s.lastIndexOf(ch, cpl);
                if ((cr > 0) && (cr < cpl)) {
                	if (s.charAt(cr) != '\r') {
                		s = s.substring(0, cr) + '\r' + s.substring(cr);
                		cr++;
                	}
                 	point = cr;
                }
                if (point < 0)
                    point = getCharsPerLine();
                point++;
                if (point > s.length()) {
                	point = s.length();
                }
                ss = s.substring(0, point);
                if (!ss.endsWith("\n")) {
                    ss = ss + "\r\n";
                }
                //send string thru centering routine.
                ss = centerThisString(ss);

                write(ss.getBytes(), 0, ss.length());
                s = s.substring(point, s.length());
                cr = s.indexOf('\n');
            }

            s = centerThisString(s);

            write(s.getBytes(), 0, s.length());
        }
        centerSB = null;
        centered = false;
    }

    /** Center a string based on the length in getCharsPerLine(). */
    private String centerThisString(String s){
        StringBuffer sb = new StringBuffer(s);
        int spaces = (getCharsPerLine() - sb.length())/2;
        for (int i = 0; i < spaces; i++){
//            sb.append(' ');
            sb.insert(0,' ');
        }
        return sb.toString();
    }
    
    /** Prints a line of underlines based on characters per line. */
    @Override
	public void underline() throws IOException {
        StringBuffer ulSB = new StringBuffer();
        int ul = getCharsPerLine();
        for (int i=0; i<ul ; i++){
            ulSB.append('_');
        }
        String s = ulSB.toString();
        s = s + "\r\n";
        write(s.getBytes(), 0, s.length());
    }

    /** Prints the default justification:  left-justified text.
     * @throws java.io.IOException
     */
    @Override
	public void left() throws IOException {
        left = true;
        leftSB = new StringBuffer();
    }

    /** Ends printing with the default justification:  left-justified text.
     * @throws java.io.IOException
     */
    @Override
	public void leftEnd() throws IOException {
        //write("\r\n");	
        // remove ending marker.
        if(left && leftSB != null && leftSB.length()>0){
            int count = 0;
            int index = 0;
            while(true){
                index = leftSB.indexOf("\t",index);
                if(index >0){
                    count++;
                    index++;
                }
                else
                    break;
            }
            if(count>0){
                if(leftSB.lastIndexOf("\t") == leftSB.length()-1){
                    leftSB.deleteCharAt(leftSB.lastIndexOf("\t"));
                    count--;
                }
                if (count>0){
                    int spacesToInsert = (getCharsPerLine()-(leftSB.length()-count))/count;
                    while(leftSB.indexOf("\t")>0){
                        for(int i=0; i<spacesToInsert; i++){
                            leftSB.insert(leftSB.lastIndexOf("\t")," ");        
                        }
                        leftSB.deleteCharAt(leftSB.lastIndexOf("\t"));
                    }
                }
            }
//            if (leftSB.length() > 0) {
//            	leftSB.append("\r\n");
//            }
            write(leftSB.toString().getBytes(), 0, leftSB.toString().length());
        }
        leftSB = null;
        left = false;
    }


    /** Prints a blank line.
     * @throws java.io.IOException
     */
    @Override
	public void vskip() throws IOException {
    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
    		ExtraDebug.println("[vskip]" );	
    	}
        write("\r\n");	
    }

    /* (non-Javadoc)
     * @see dw.printing.TEPrinter#defaultFont()
     */
    @Override
	void defaultFont() throws IOException {
    }

    /* (non-Javadoc)
     * @see dw.printing.TEPrinter#head1()
     */
    @Override
	void head1() throws IOException {
    }

    /* (non-Javadoc)
     * @see dw.printing.TEPrinter#head1End()
     */
    @Override
	void head1End() throws IOException {
    }

    /* (non-Javadoc)
     * @see dw.printing.TEPrinter#head2()
     */
    @Override
	void head2() throws IOException {
    }

    /* (non-Javadoc)
     * @see dw.printing.TEPrinter#head2End()
     */
    @Override
	void head2End() throws IOException {
    }

    /* (non-Javadoc)
     * @see dw.printing.TEPrinter#head3()
     */
    @Override
	void head3() throws IOException {
    }

    /* (non-Javadoc)
     * @see dw.printing.TEPrinter#head3End()
     */
    @Override
	void head3End() throws IOException {
    }

    /* (non-Javadoc)
     * @see dw.printing.TEPrinter#head4()
     */
    @Override
	void head4() throws IOException {
    }

    /* (non-Javadoc)
     * @see dw.printing.TEPrinter#head4End()
     */
    @Override
	void head4End() throws IOException {
    }

    /* (non-Javadoc)
     * @see dw.printing.TEPrinter#large()
     */
    @Override
	void large() throws IOException {
    }

    /* (non-Javadoc)
     * @see dw.printing.TEPrinter#largeBold()
     */
    @Override
	void largeBold() throws IOException {
    }
   

    /* (non-Javadoc)
     * @see dw.printing.TEPrinter#largeBoldEnd()
     */
    @Override
	void largeBoldEnd() throws IOException {
    }
    
    /** Prints text in bold in the small font.
     * @throws java.io.IOException
     */
    @Override
	public void bold() throws IOException {
       
    }

    /* (non-Javadoc)
     * @see dw.printing.TEPrinter#largeEnd()
     */
    @Override
	void largeEnd() throws IOException {
    }

    /* (non-Javadoc)
     * @see dw.printing.TEPrinter#printLogo()
     */
    @Override
	void printLogo() throws IOException {
    }

    /* (non-Javadoc)
     * @see dw.printing.TEPrinter#reverse()
     */
    @Override
	void reverse() throws IOException {
    }

    /* (non-Javadoc)
     * @see dw.printing.TEPrinter#reverseEnd()
     */
    @Override
	void reverseEnd() throws IOException {
    }

    /* (non-Javadoc)
     * @see dw.printing.TEPrinter#writeInField(java.lang.String, int)
     */
    @Override
	void writeInField(String str, int width) throws IOException {
        //marker
        int spaces;
        String justif;
        StringBuffer s = new StringBuffer(str);

        // get justification and convert to positive
        if (width >= 0) {
            justif = "right"; 
        }
        else {
            justif = "left"; 
            width *= -1;
        }

        // truncate field width to line width
        
        getCharsPerLine();
        
        // prepend or append spaces to string
        spaces = width - s.length();
        if (justif.equals("left")) {    
            for (int i = 0; i < spaces; i++)
                s.append(' ');
        }
        else {
            for (int i = 0; i < spaces; i++)
                s.insert(0, ' ');
        }
        s.append("\t");
        write(s.toString());
    }

    /* (non-Javadoc)
     * @see java.awt.print.Printable#print(java.awt.Graphics, java.awt.print.PageFormat, int)
     */
    @Override
	public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {

       // Font courier = new Font("Courier", Font.PLAIN, 10);
        Font courier = new Font("Courier", Font.PLAIN, 8);
        graphics.setFont(courier);

        byte[] temp = new byte[count];
        System.arraycopy(buf, 0, temp, 0, count);
        BufferedReader reader;
		try {
			reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(temp), "UTF-8"));
		} catch (UnsupportedEncodingException e2) {
			Debug.printStackTrace(e2);
			throw new PrinterException(e2.getMessage());
		}        

        int lineNumber = 0;
       int linesPerPage = 64;
        //int linesPerPage = 70;
        String s = null;
       
        try {
            s = reader.readLine();
            for(int i=1; i<=pageIndex*linesPerPage; i++){           
                s = reader.readLine();
                if(s == null)
                    return NO_SUCH_PAGE;
            }
            int x = 90; 
            int y = 90;
            int incrementValue = 10;
            while(s!=null && lineNumber<linesPerPage){
                s = s.replace('\0',' ');
                s = s.replace('\t',' ');
                s = s.replace('\r',' ');
                s = s.replace('\f',' ');
                graphics.drawString(s,x,y);
                y = y + incrementValue;
                s = reader.readLine();               
                lineNumber++;
                
            }
        }
        catch (IOException e1) {
            s = null;
            Debug.printStackTrace(e1);
        }
        finally {
            try {
                reader.close();
            }
            catch (IOException e) {
    			Debug.printStackTrace(e);
            }
        }
        
        return PAGE_EXISTS;
    }

}

class PrintJobWatcher2 {
    // true iff it is safe to close the print job's input stream
    boolean done = false;

    PrintJobWatcher2(DocPrintJob job) {
        // Add a listener to the print job
        job.addPrintJobListener(new PrintJobAdapter() {
            @Override
			public void printJobCanceled(PrintJobEvent pje) {
                Debug.println("printJobCanceled");
                allDone();
            }
            @Override
			public void printJobCompleted(PrintJobEvent pje) {
                Debug.println("printJobCompleted");
                allDone();
            }
            @Override
			public void printJobFailed(PrintJobEvent pje) {
                Debug.println("printJobFailed");
                allDone();
            }
            @Override
			public void printJobNoMoreEvents(PrintJobEvent pje) {
                Debug.println("printJobNoMoreEvents");
                allDone();
            }
            void allDone() {
                synchronized (PrintJobWatcher2.this) {
                    done = true;
                    PrintJobWatcher2.this.notify();
                }
            }
        });
    }
    
    public synchronized void waitForDone() {
        try {
            while (!done) {
                wait();
            }
        }
        catch (InterruptedException e) {
        }
    }
}
