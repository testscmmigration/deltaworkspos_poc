package dw.printing;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JEditorPane;

/**
 * Implementation of TEPrinter that prints the output in HTML to a dialog box
 *   for online reporting.
 */
public class ScreenPrinter extends TEPrinter {
    private JEditorPane editor;
    private StringBuffer output = new StringBuffer();

    public ScreenPrinter(JEditorPane editor) {
        this.editor = editor;
        editor.setContentType("text/html"); 
        editor.setEditable(false);
        out("<FONT FACE=\"MONOSPACED\">");	 
    }

    public ScreenPrinter() {}

    private void out(String text) {
        output.append(text);
    }

    /** Clears the print buffer and resets modes to power-on defaults. */
    @Override
	public void initialize() throws IOException {}
    @Override
	public void printLogo() throws IOException {}
    
    /** Sets the font */
    private void setFont(final int fontSize) {
        out("<FONT size=\"" + fontSize + "\">");  
    }
    
    /** Ends the font setting */
    private void setFontEnd() {
        out("</FONT>"); 
    }

    /** Prints text in the largest font. */
    @Override
	public void head1() throws IOException {
        setFont(5);
    }

    /** Ends printing text in the largest font. */
    @Override
	public void head1End() throws IOException {
        setFontEnd();
    }

    /** Prints text in the second-largest font. */
    @Override
	public void head2() throws IOException {
        setFont(4);
    }

    /** Ends printing text in the second-largest font. */
    @Override
	public void head2End() throws IOException {
        setFontEnd();
    }

    /** Prints text in the third-largest font. */
    @Override
	public void head3() throws IOException {
        setFont(4);
    }

    /** Ends printing text in the third-largest font. */
    @Override
	public void head3End() throws IOException {
        setFontEnd();
    }

    /** Prints text in the fourth-largest font. */
    @Override
	public void head4() throws IOException {
        setFont(4);
    }

    /** Ends printing text in the fourth-largest font. */
    @Override
	public void head4End() throws IOException {
        setFontEnd();
    }

    /** Prints text in large font. */
    @Override
	public void large() throws IOException {
        setFont(3);
    }

    /** Ends printing text in large font. */
    @Override
	public void largeEnd() throws IOException {
        setFontEnd();
    }

    /** Prints text in bold in the large font. */
    @Override
	public void largeBold() throws IOException {
        setFont(3);
        out("<B>"); 
    }
    /** Prints text in bold in the large font. */
    @Override
	public void bold() throws IOException {
        setFont(3);
        out("<B>"); 
    }

    /** Ends printing text in bold in the large font. */
    @Override
	public void largeBoldEnd() throws IOException {
        out("</B>"); 
        setFontEnd();
    }

    /** Prints text in the default (smaller than large) font.  */
    @Override
	public void defaultFont() throws IOException {
    }

    /** Prints a centered line of text. */
    @Override
	public void center() throws IOException {
        out("<CENTER>"); 
    }

    /** Ends printing a centered line of text. */
    @Override
	public void centerEnd() throws IOException {
        out("</CENTER>"); 
    }

    /** Prints a line of underlines based on chararacters per line. */
    @Override
	public void underline() throws IOException {
        StringBuffer ulSB = new StringBuffer();
        int ul = 56;
        for (int i=0; i<ul ; i++){
            ulSB.append('_');
        }
        String s = ulSB.toString();
        out(s);
    }

    /** Prints a reverse line of text. */
    @Override
	public void reverse() throws IOException {
        out("<REVERSE>"); 
    }

    /** Ends printing a reverse line of text. */
    @Override
	public void reverseEnd() throws IOException {
        out("</REVERSE>"); 
    }

    /** Prints the default justification:  left-justified text. */
    @Override
	public void left() throws IOException {
    }

    /** Ends printing the default justification:  left-justified text. */
    @Override
	public void leftEnd() throws IOException {
//        vskip();
    }

    /** Prints a blank line. */
    @Override
	public void vskip() throws IOException {
        out("<BR>\n"); 
    }

    /** Prints a paragraph of text.
     * @throws java.io.IOException
     */
    @Override
	public void paragraph() throws IOException {
    }

    /** Prints a paragraph of text.
     * @throws java.io.IOException
     */
    @Override
	public void paragraphEnd() throws IOException {
    }

    /** Writes a string to the buffer.  */
    @Override
	public void write(String s) throws IOException {
        final StringBuffer sb = new StringBuffer(s);
        for (int i = 0; i < s.length(); i++) {
            final char c = sb.charAt(i);
            if (c == ' ')
                output.append("&nbsp;"); 
            else
                output.append(c);
        }
    }

    private static final String NON_BREAKING_SPACE = "&nbsp;"; 
    
    private String formatPre(String str, int width) {
        StringBuffer buf = new StringBuffer(str);
        if (width < 0)
            // format with trailing spaces
            for (int i = 0; i < (-width) - str.length(); i++)
                buf.append(NON_BREAKING_SPACE);
        else
            // format with leading spaces
            for (int i = 0; i < width - str.length(); i++)
                buf.insert(0, NON_BREAKING_SPACE);
        return buf.toString();
    }

    /** Prints text in a field of a specific width.
     * It will truncate a string too long for the line or field.
     * @param str = the string to write in this field
     * @param width = the width of the field in spaces, a negative number
     * meaning left-justified.
     */
    @Override
	public void writeInField(String str, int width) throws IOException {
        out(formatPre(str, width));
    }

    /** Dumps the output into the text component. */
    @Override
	public void eject() throws IOException {
        out("</FONT>"); 
        final FileWriter fw = new FileWriter("screen.html"); 
        final BufferedWriter bw = new BufferedWriter(fw);
        bw.write(output.toString());
        bw.close();
        editor.setText(output.toString());
    }

    /* (non-Javadoc)
     * @see dw.printing.TEPrinter#fireOffPrintJob()
     */
    @Override
	public void fireOffPrintJob() {
    }
}
