package dw.printing;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeSet;

import org.xml.sax.Attributes;

import dw.i18n.FormatSymbols;
import dw.io.info.ReportEventDiagInfo;
import dw.io.report.MoneyOrderDetail;
import dw.io.report.ReportDetail;
import dw.io.report.ReportLog;
import dw.io.report.ReportSelectionCriteria;
import dw.io.report.ReportSummary;
import dw.moneyorder.MoneyOrderInfomation;
import dw.utility.Debug;
import dw.utility.ExtraDebug;
import flags.CompileTimeFlags;

public class DispenserItemsReport extends Report {
    private MoneyOrderInfomation demoMoi[] = {
    		new MoneyOrderInfomation (ReportLog.MONEY_ORDER_TYPE, 50, new BigDecimal("100.00"), new BigDecimal("1.00")),	 
            new MoneyOrderInfomation (ReportLog.MONEY_ORDER_TYPE, 50, new BigDecimal("210.00"), new BigDecimal("1.00")),	 
            new MoneyOrderInfomation (ReportLog.VENDOR_PAYMENT_TYPE, 50, new BigDecimal("150.00"), BigDecimal.ZERO),	 
            new MoneyOrderInfomation (ReportLog.VENDOR_PAYMENT_TYPE, 50, new BigDecimal("75.99"), BigDecimal.ZERO),	 
            new MoneyOrderInfomation (ReportLog.MONEYGRAM_RECEIVE_TYPE, 50, new BigDecimal("300.25"), BigDecimal.ZERO),	 
            new MoneyOrderInfomation (ReportLog.VOID_TYPE, 50, new BigDecimal("22.00"),  BigDecimal.ZERO),
		};

    public DispenserItemsReport(final String xmlFileName) {
        super(xmlFileName);
    }

    @Override
	public int print(final TEPrinter printer, 
                           final ReportSelectionCriteria rsc) {
        initSelectionCriteria(printer, rsc);
        setPeriodClosed();
        // demo mode canned report
        if (inDemoMode) {
//            final ReportSummary demoReportSummary = new ReportSummary();
//            demoReportSummary.setDispenserLoads        ((byte)  2);
//            demoReportSummary.setSerialBeg             (992564419);
//            demoReportSummary.setSerialEnd             (992564424);
//            demoReportSummary.setMoneyOrderAmount      (    31000);
//            demoReportSummary.setMoneyOrderCount       (        2);
//            demoReportSummary.setVendorPaymentAmount   (    22599);
//            demoReportSummary.setVendorPaymentCount    (        2);
//            demoReportSummary.setMoneyGramReceiveAmount(new BigDecimal(300.00));
//            demoReportSummary.setMoneyGramReceiveCount (        1);
//            demoReportSummary.setVoidAmount            (new BigDecimal(334.00));
//            demoReportSummary.setVoidCount             (        1);
//            demoReportSummary.setMOFeeAmount           (      200);
//            demoReportSummary.setMOFeeCount            (        2);
//            demoReportSummary.addFieldValues(fieldMap);
            final TreeSet<ReportDetail> demoDetail = ReportLog.getNewDetailRecordCollection();
            final long demoDate = rsc.getSummaryDateTime();
            for (int i=0; i < demoMoi.length; i++){
                demoMoi[i].setLimbo(false);
                demoMoi[i].setSerialNumber(BigInteger.valueOf(demoSerialStart + i));
                try {
                    final MoneyOrderDetail mod = new MoneyOrderDetail(demoMoi[i]);
                    mod.setDateTime(demoDate + i);
                    if (mod.getType() == ReportLog.MONEY_ORDER_TYPE ||
                        mod.getType() == ReportLog.VENDOR_PAYMENT_TYPE)
                        mod.setLimboFlag(true);
                    if (rsc.getEmployeeNumber() != ReportSelectionCriteria.INT_NOT_SPECIFIED)
                        mod.setUserId((byte) rsc.getEmployeeNumber());
                    demoDetail.add(mod);
                }
                catch (Exception e) {
                    Debug.println("***** Exception " + e);	
                }
            }
            addFlag("showdetailheader", rsc.getShowDetail()); 
            detailItems = demoDetail;
          
            ReportSummary reportSummary = new ReportSummary(demoDetail);
            reportSummary.setMoneyGramReceiveAmount(new BigDecimal(300.25));
            reportSummary.setMoneyGramReceiveCount (        1);
            reportSummary.setVoidAmount            (new BigDecimal(334.00));
            reportSummary.setVoidCount             (        1);

            reportSummary.addFieldValues(fieldMap);

            addDispenserFields();
            return super.print(printer);
        } // end demo report values

        // find record
        detailItems = ReportLog.getDetailRecords(rsc);
        ReportSummary reportSummary = new ReportSummary(detailItems);

//        if (reportSummary == null) {
//            reportSummary = new ReportSummary();
//            reportSummary.setDispenserLoads((byte)0);
//            reportSummary.setSerialBeg     (      0);
//            reportSummary.setSerialEnd     (      0);
//        }
        setBadLogins(reportSummary.getBadLogs());
        writeReportEventMessage(ReportEventDiagInfo.DISPENSER_ITEMS_REPORT, true);
        reportSummary.addFieldValues(fieldMap);
        addFlag("showdetailheader", rsc.getShowDetail()); 
        addDispenserFields();
        return super.print(printer, rsc);
    }
    
    private void addDispenserFields() {
        addPrintDetailValues(rsc);
        boolean noEmployee = true;
        boolean noBeginSerial = true;
        boolean noEndSerial = true;
        boolean noMinAmount = true;
        boolean noMaxAmount = true;
        if (rsc.getEmployeeNumber() == ReportSelectionCriteria.INT_NOT_SPECIFIED)
           noEmployee = false;
        if (rsc.getStartSerial() == ReportSelectionCriteria.INT_NOT_SPECIFIED && rsc.getEndSerial() == ReportSelectionCriteria.INT_NOT_SPECIFIED){
           noBeginSerial = false;
           noEndSerial = false;
        }   
        if ((rsc.getStartAmount().compareTo(ReportSelectionCriteria.INTBD_NOT_SPECIFIED) == 0) && 
            (rsc.getEndAmount().compareTo(ReportSelectionCriteria.INTBD_NOT_SPECIFIED) == 0)){
           noMinAmount = false;
           noMaxAmount = false;
        }   
        addFlag("noEmployee", noEmployee); 
        addFlag("noBeginSerial", noBeginSerial); 
        addFlag("noEndSerial", noEndSerial); 
        addFlag("noMinAmount", noMinAmount); 
        addFlag("noMaxAmount", noMaxAmount); 
    }

    private void addPrintDetailValues(ReportSelectionCriteria rsc) {
        fieldMap.put("employeeID",Integer.valueOf(rsc.getEmployeeNumber())); 
     // fieldMap.put("selectedStartDate", 
     //     Scheduler.formatMMDDYYYY(rsc.getStartDateTime()));
     // fieldMap.put("selectedEndDate", 
    //      Scheduler.formatMMDDYYYY(rsc.getEndDateTime()));
    //  fieldMap.put("selectedStartTime", 
    //      Scheduler.formatHHMM(rsc.getStartDateTime()));
    //  fieldMap.put("selectedEndTime", 
    //      Scheduler.formatHHMM(rsc.getEndDateTime()));
        
        fieldMap.put("selectedStartDate", 
            FormatSymbols.formatDateForLocale(rsc.getStartDateTime()));
        fieldMap.put("selectedEndDate", 
                    FormatSymbols.formatDateForLocale(rsc.getEndDateTime()));
                       
        fieldMap.put("beginSerial",Long.valueOf(rsc.getStartSerial())); 
        fieldMap.put("endSerial",Long.valueOf(rsc.getEndSerial())); 
        fieldMap.put("minAmount", FormatSymbols.convertDecimal(rsc.getStartAmount().toString())); 
        fieldMap.put("maxAmount", FormatSymbols.convertDecimal(rsc.getEndAmount().toString())); 
    }
    
    /**
     * Call back method for subclasses to provide handling for additional names
     */
    @Override
	protected void subStartElement(final String name, final Attributes attrs) {
        // ------------
        // detail field
        // the detail tag is use to iterate through a collection
        // to output detail lines according to the format in the
        // detailFileName.xml
        if (name.equals("detail"))	
            new DispenserDetailPrinter(this,detailItems,attrs).print(false);
    }
}
    
class DispenserDetailPrinter extends DetailPrinter {
    private int type2;
     
    public DispenserDetailPrinter(  final Report report,
                                    final Collection<ReportDetail> detailItems,
                                    final Attributes attrs) {
        super(report,detailItems,attrs);
    }
        
    @Override
	public void checkDetail(final Object detail,boolean hostReport) {
        if  (detail instanceof MoneyOrderDetail) {
            final MoneyOrderDetail mod = (MoneyOrderDetail) detail;
            final int type = mod.getType();
        	if (CompileTimeFlags.reportLogging()) {
                ExtraDebug.println("checkDetail MoneyOrderDetail=[" + mod + "] ");
        	}
        	
            if (mod.getLimboFlag() ||
                    ((type != ReportLog.MONEY_ORDER_TYPE) &&
                     (type != ReportLog.VENDOR_PAYMENT_TYPE) &&
                     (type != ReportLog.DELTAGRAM_TYPE))) {
                        //do not print non limbo MoneyOrders or vendor Payments

                if (mod.getVoidFlag())
                    type2 = ReportLog.VOID_TYPE;
                else
                    type2 = type;
                    
                final HashMap<String, Object> fieldMap = report.getFieldMap();
                fieldMap.put("type", Report.exportTag[type2]); 
                final Date startDate = new Date(mod.getDateTime());
               // final SimpleDateFormat formatter = new DwSimpleDateFormat("MM/dd hh:mma");	
                String dateTime = FormatSymbols.formatDateTimeForReportDetail(startDate);
                fieldMap.put("serialnum", Long.valueOf(mod.getSerialNumber())); 
                // if mg item is voided, concatenate a "-".	
                if (!mod.getVoidFlag())
                    fieldMap.put("amount", FormatSymbols.convertDecimal(mod.getAmount().toString()).concat(" "));	 
                else
                    fieldMap.put("amount", FormatSymbols.convertDecimal(mod.getAmount().toString()).concat("-"));	 
                fieldMap.put("datetime", dateTime); 
                fieldMap.put("period", Byte.valueOf(mod.getPeriod())); 
                fieldMap.put("emp", Byte.valueOf(mod.getUserId())); 
                printLine();
            } else if ((! mod.getLimboFlag()) && mod.getVoidFlag()) {
                type2 = ReportLog.VOID_TYPE;
                    
                final HashMap<String, Object> fieldMap = report.getFieldMap();
                fieldMap.put("type", Report.exportTag[type2]); 
                final Date startDate = new Date(mod.getDateTime());
              //  final SimpleDateFormat formatter = new DwSimpleDateFormat("MM/dd hh:mma");	
               
                String dateTime = FormatSymbols.formatDateTimeForReportDetail(startDate);
                
                fieldMap.put("serialnum", Long.valueOf(mod.getSerialNumber())); 
                fieldMap.put("amount", FormatSymbols.convertDecimal(mod.getAmount().toString()).concat("-"));	 
                //fieldMap.put("datetime", formatter.format(startDate)); 
                fieldMap.put("datetime", dateTime); 
                fieldMap.put("period", Byte.valueOf(mod.getPeriod())); 
                fieldMap.put("emp", Byte.valueOf(mod.getUserId())); 
                printLine();
            }
        }
    }

	/* (non-Javadoc)
	 * @see dw.printing.DetailPrinter#printNoDetailLine()
	 */
	@Override
	protected void printNoDetailLine() {
	}
}
