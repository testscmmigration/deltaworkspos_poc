package dw.printing;

import java.util.Collection;
import java.util.HashMap;

import org.xml.sax.Attributes;

import dw.io.report.DirectoryOfAgentsDetail;
import dw.io.report.ReportDetail;
import dw.model.adapters.CountryInfo.Country;

public class DirectoryOfAgentsReport extends Report {
	
	private Collection<ReportDetail> printAgentData;
    private String city;
    private Country country;
    private String zip;
    private String areaCode;
	
    public DirectoryOfAgentsReport(Collection<ReportDetail> printAgentData, String city,
    		     Country country, String zip, String areaCode) {
        super();
        this.printAgentData = printAgentData;
        this.city = city;
        this.country = country;
        this.zip = zip;
        this.areaCode = areaCode;
        
    }
    
    @Override
	public int print(final TEPrinter printer) {
    	status = OKAY;
        initDoaData(printer, city, country, zip, areaCode);        
        return super.print(printer);           
    }
   
    /**
     * Call back method for subclasses to provide handling for additional names
     */
    @Override
	protected void subStartElement(final String name, final Attributes attrs) {
        
    	if (name.equals("doadetail")){	   		
            new DirectoryOfAgentsDetailPrinter(this,printAgentData,attrs).printDoa(); 
        }
    }
}

class DirectoryOfAgentsDetailPrinter extends DetailPrinter {

    public DirectoryOfAgentsDetailPrinter(  final Report report,
                                    final Collection<ReportDetail> printAgentData,
                                    final Attributes attrs) {

        super(report,printAgentData,attrs);
    }

    @Override
	public void checkDetail(final Object detail,boolean hostReport) {

    	final DirectoryOfAgentsDetail doad = (DirectoryOfAgentsDetail) detail;
        final HashMap<String, Object> fieldMap = report.getFieldMap();
        fieldMap.put("name", doad.getName());
        fieldMap.put("address", doad.getAddress());
        fieldMap.put("phone", doad.getPhone());       
        printLine();
    }

	/* (non-Javadoc)
	 * @see dw.printing.DetailPrinter#printNoDetailLine()
	 */
	@Override
	protected void printNoDetailLine() {
	}
}
