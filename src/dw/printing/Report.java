package dw.printing;

import java.awt.Dimension;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.io.DiagLog;
import dw.io.State;
import dw.io.info.ReportEventDiagInfo;
import dw.io.report.ReportDetail;
import dw.io.report.ReportLog;
import dw.io.report.ReportSelectionCriteria;
import dw.io.report.ReportSummary;
import dw.io.report.ReportSummary.InvalidLogin;
import dw.model.adapters.CountryInfo.Country;
import dw.profile.ClientInfo;
import dw.profile.UnitProfile;
import dw.utility.Debug;
import dw.utility.ErrorManager;
import dw.utility.ExtraDebug;
import dw.utility.Period;
import dw.utility.ReceiptTemplates;
import dw.utility.TimeUtility;
import flags.CompileTimeFlags;

/**
 * Generates a receipt or report based on data in the profile, in an
 * active transaction object, and on an XML file describing its format.
 *
 * Static methods for each type of receipt or report creates the Report
 * object which parses the xml file.  It uses a SAX parser.
 *
 * @version 1.0  04-05-2000
 * @author John Allen
 */
public class Report extends DefaultHandler {

    // statuses
    public static final int OKAY = 0;
    public static final int PARSING_ERROR = 1;
    public static final int PRINTER_ERROR = 2;
    public static final int HOST_CONNECT_ERROR = 7;
    public static final int SETUP_ERROR = 9;

    protected static final boolean includeCheckDigit = true;

    protected int status = OKAY;

    protected TEPrinter printer;
    protected ClientInfo ci;
    protected HashMap<String, Object> fieldMap;
    protected HashMap<String, Boolean> flagMap;
    private boolean printThisLine = true;

    // for reports
    private ArrayList<String> badLogins = null;
    protected long timeStamp;
    protected Collection<ReportDetail> detailItems;
    protected boolean inDemoMode;
    private boolean inTrainingMode;
    protected static final String[] exportTag = { "VOID", "MO", "VP", "MS", "MR", "EP", "MG" };       
    protected ReportSelectionCriteria rsc;
    protected int demoSerialStart = 992564419;
    private String xmlFileName;


    //-------------------------------------------------------------------------
    //  static calls for printing receipts and reports
    //-------------------------------------------------------------------------

    /**
     * Exports ReportLog data to a flat file in a comma-delimited, backward-compatible format.
     * @param timeStamp = milliseconds time stamp for date/period for export (dumps all now)
     * @return status code
     */
    //    public static int printRawExport(long timeStamp) {
    //        return ReportLog.exportDetailRecords(EpsonTMT88II.getInstance(), timeStamp);
    //    }

    /**
     * Returns name of exported data file name just created.
     * @return exported file name
     */
    //    public static String getExportedFileName() {
    //        return ReportLog.getExportedFileName();
    //    }

    /**
     * This is a stub method for reports that do not put output
     * to a file.
     */
    public String getOutputFileName() {
        return "DW_REPORT.TXT"; 
    }

    public String getCsvFileName() {
		JFileChooser chooser = new JFileChooser();
		Dimension d = new Dimension(550, 325);
		chooser.setPreferredSize(d);		
		String filename = "c:moneygram_rpt";  
		chooser.setSelectedFile(new File(filename));
		chooser.setMultiSelectionEnabled(false);
		chooser.addChoosableFileFilter(new SimpleFileFilter("csv",
				"Excel CSV file (*.csv)"));
		int option = chooser.showSaveDialog(null);
		if (option == JFileChooser.APPROVE_OPTION) {
			String fileName = chooser.getSelectedFile().getAbsolutePath();
			if (! fileName.toUpperCase(Locale.US).endsWith(".CSV")) {
				fileName += ".csv";
			}
			File f = new File(fileName);
			if (f.exists()) {
				int response = JOptionPane.showConfirmDialog(null,
						"Overwrite existing file?",
						"Confirm Overwrite",
						JOptionPane.OK_CANCEL_OPTION,
						JOptionPane.QUESTION_MESSAGE);
				if (response == JOptionPane.OK_OPTION)
					return fileName;
			}
			else
				return fileName;
		}
		return null;
    }
    
    protected HashMap<String, Object> getFieldMap() {
        return fieldMap;
    }
    
    protected HashMap<String, Boolean> getFlagMap() {
        return flagMap;
    }

    public ReportSelectionCriteria getRsc() {
        return rsc;
    }

    /*
     * Writes a report event message to the diagnostic log.  (Copied from
     * AgentDirectoryTransaction.)
     * @param messageType as defined by ReportEventDiagInfo.
     * @param success if report was successfully printed or not.
     */
	protected static void writeReportEventMessage(int messageType,
			boolean success) {
		try {
			ReportEventDiagInfo reportEvDiagInfo = new ReportEventDiagInfo(
					TimeUtility.currentTime(), 0, "", messageType, 1, success); 
			//(new Date(), 0, "", messageType, 1, success);	
			DiagLog.getInstance().writeDWReport(reportEvDiagInfo);
		} catch (Exception e) {
			// ErrorManager.handle(e, "diagLogError", ErrorManager.ERROR);	
			ErrorManager.handle(e, "applicationError", Messages.getString("ErrorMessages.applicationError"), ErrorManager.ERROR); 
		}
	}

    /**
     * Parses XML file for all types of receipts.
     */
    private void parseReceipt(InputSource pm_sFileName) {
        SAXParserFactory spf = SAXParserFactory.newInstance();

        try {
            spf.setFeature("http://xml.org/sax/features/external-general-entities", false);
            spf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            spf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true); 
            spf.setValidating(false);
            SAXParser sp = spf.newSAXParser();
            printer.initialize();
            printer.defaultFont();
            sp.parse(pm_sFileName, this);
        }
        catch (SAXParseException err) {
            status = PARSING_ERROR;
            Debug.println("** Parsing error" 
            +", line " + err.getLineNumber() 
            +", filename " + err.getSystemId()); 
            Debug.println("   " + err.getMessage()); 
        }
        catch (SAXException e) {
            status = PARSING_ERROR;
            Debug.println("** Parsing error " + e); 
            if (e.getException() != null) {
                Debug.printStackTrace(e.getException());
            }
        }
        catch (Throwable t) {
            status = PARSING_ERROR;
            Debug.println("** Parsing error " + t); 
            Debug.printStackTrace (t);
        }
    }

    /**
     * Parses xml file for all types of receipts.
     */
    public void parseFile(String filename) {
        java.net.URL fileURL = ClassLoader.getSystemResource(filename);
        InputSource is = new InputSource(fileURL.toString());
        parseReceipt(is);
    }

    /**
     * Parses xml template file for all types of receipts.
     */
    public void parseTemplate(int pm_sTemplateType, String pm_sLanguage) {
		if (ReceiptTemplates.getTemplateInputStream(pm_sTemplateType, pm_sLanguage).getByteStream() != null) {
			parseReceipt(ReceiptTemplates.getTemplateInputStream(pm_sTemplateType, pm_sLanguage));
		} else {
			status = SETUP_ERROR;
		}
    }

    //-------------------------------------------------------------------------
    // object methods
    //-------------------------------------------------------------------------

    /*
     * Constructs a Report object for summary/detail reports
     */
    protected Report(final String xmlFileName) {
        super();
        this.xmlFileName = xmlFileName;
        status = OKAY;
        initialize(null);
    }

    protected Report() {
        this(null);
    }

    /*sets field data based on selection critera */
    public int print(final TEPrinter printer, final ReportSelectionCriteria rsc) {
        if (status == OKAY || status == HOST_CONNECT_ERROR){
        	parseFile(xmlFileName);
            if(!(printer instanceof ScreenPrinter)){
                TEPrinter.getPrinter().fireOffPrintJob();
            }
        }    
        return status;
    }
    
    public int print() {
        return status;
    }
    
    /*sets field data based on selection critera */
    public int print(final TEPrinter printer) {
    	return print(printer, null);
    	
    }

    public void initSelectionCriteria(final TEPrinter printer, final ReportSelectionCriteria rsc) {
        this.rsc = rsc;
        timeStamp = rsc.getSummaryDateTime();
        initFields();
        if (printer == null)
            this.printer = TEPrinter.getPrinter();
        else
            this.printer = printer;
    }

    public void initSelectionCriteriaCsv(final ReportSelectionCriteria rsc) {
        this.rsc = rsc;
        timeStamp = rsc.getSummaryDateTime();
    }
    
    public void initDoaData(final TEPrinter printer, String city, Country country,
    						String zip, String areaCode) {
        initFields();
        if (city.trim().length() == 0)
        	addField("city", "N/A");
        else 
        	addField("city", city);

        if (country == null)
        	addField("country", "N/A");
        else 
        	addField("country", country.getCountryCode());
        
        if (zip.trim().length() == 0)
        	addField("zip", "N/A");
        else 
        	addField("zip", zip);
        
        if (areaCode.trim().length() == 0)
        	addField("areaCode", "N/A");
        else 
        	addField("areaCode", areaCode);


        if (printer == null)
            this.printer = TEPrinter.getPrinter();
        else
            this.printer = printer;
    }

    // Report() constructor for printRawExport moved to ReportLog

    //--------------------------------------------------------------------------
    // end of constructors
    //--------------------------------------------------------------------------

    public void addFlag(final String key, final boolean value) {
    	if (CompileTimeFlags.rcptLogFlags()) {
    		ExtraDebug.println("AddFlag["+key+"],["+value+"]");
    	}
        flagMap.put(key, Boolean.valueOf(value));
    }

    protected void addField(final String key, final String value) {
    	if (CompileTimeFlags.rcptLogFlags()) {
        	ExtraDebug.println("AddField["+key+"],["+value+"]");
    	}
        fieldMap.put(key, value);
    }

    protected void initialize(final TEPrinter printer) {
        this.printer = printer;
        ci = new ClientInfo();
        inDemoMode = UnitProfile.getInstance().isDemoMode();
        inTrainingMode = UnitProfile.getInstance().isTrainingMode();
        initFields();
    }

    private void initFields() {
        fieldMap = new HashMap<String, Object>();
        flagMap = new HashMap<String, Boolean>();
		if (TEPrinter.getPrinter() != null) {
			addFlag("nologo", !TEPrinter.getPrinter().logoFound); 
		}
        addFlag("training", inTrainingMode); 
        addFlag("demo", inDemoMode); 
        ci.addFieldValues(fieldMap);
        addField("agentName", UnitProfile.getInstance().getGenericDocumentStoreName()); 
        setTodayDateTime();
        addField("badLogin", "badLogin");  
    }


    /* Removes the check digit from the serial number. */
    public static String checkDigit(String serialIn) {
        if (includeCheckDigit)
            return serialIn;
        else
            return serialIn.substring(0, serialIn.length() - 1);
    }

    /*
     * Fills the ArrayList with the Strings for Invalid Logins.
     * The xml has only one line for this list, so we need to construct the
     * lines here.
     */
    protected void setBadLogins(ArrayList<InvalidLogin> badlogs) {
        // put both in line width = 42
        //Epson TMT88II large = 42 cpl ------------|
        //00000000011111111112222222222333333333344444444445555555555
        //12345678901234567890123456789012345678901234567890123456789
        //99          -- 29 --           12:50:02 PM

        badLogins = new ArrayList<String>();
        String spaces = "                             "; 

        // demo mode    CANNED
        if (inDemoMode) {
            String empId = "02"; 
            String badTime = "12:04:54 PM"; 
            badLogins.add(empId + spaces + badTime);
            // add a second
            empId = "51"; 
            badTime = "12:53:32 PM"; 
            badLogins.add(empId + spaces + badTime);
            return;
        }

        if (badlogs == null)
            return;
        ReportSummary.InvalidLogin login;
        StringBuffer line;

        for (int i = 0; i < badlogs.size(); i++) {
            login = badlogs.get(i);
            line = new StringBuffer(login.getUserId() + spaces);
            // add extra space user for ids < 2 digits
            for (int j = line.length(); j < (spaces.length() + 2); j++) {
                line.append(" "); 
            }
            line.append("\t");
            line.append(new SimpleDateFormat(TimeUtility.TIME_12_HOUR_MINUTE_SECOND_FORMAT).format(new Date(login.getTimeStamp())));
            badLogins.add(line.toString());
        }
    }

    /**
     * Set the string and boolean if and/or when this period is closed.
     * This sets the following object variables:
     * periodOpened
     * periodClosed
     * activityDate
     * period
     * @param timeStamp = milliseconds time stamp within period for report
     */
    protected void setPeriodClosed() {
        final ReportSummary reportSummary = ReportLog.getCurrentSummaryRecord(timeStamp);
        if (reportSummary == null)
            return;

        final long startTime = reportSummary.getPeriodOpened();
        final long endTime = reportSummary.getPeriodClosed();

     //   SimpleDateFormat formatter = new DwSimpleDateFormat("MM/dd/yyyy 'at' hh:mm a"); 
        // set periodOpened
     //   final String periodOpened = Scheduler.formatMMDDYYYYHHMM(startTime);
        final String periodOpened =  FormatSymbols.formatDateForLocale(startTime);
        // set periodClosed
        String periodClosed;
        boolean isPeriodOpen;
        if (!reportSummary.isForCurrentPeriod()) {
            isPeriodOpen = false;
          //  periodClosed = Scheduler.formatMMDDYYYYHHMM(endTime);
            periodClosed = FormatSymbols.formatDateForLocale(endTime);
        }
        else {
            isPeriodOpen = true;
            periodClosed = "Currently Open";
        }

        // set period
        final long absPeriod = rsc.getStartPeriod();
        int period;
        if (absPeriod == ReportSelectionCriteria.INT_NOT_SPECIFIED)
            try {
                period = Period.computePeriod(new Date(startTime), 0);
            }
            catch (Exception e) {
                period = 0;
            }
        else
            period = Period.getRelativePeriod(absPeriod);

        addFlag("period", isPeriodOpen); 
        addField("period", Integer.toString(period)); 
        addField("periodOpened", periodOpened); 
        addField("periodClosed", periodClosed); 
    }

    /* Sets today's date and time in formatted string variables.  */
    private void setTodayDateTime() {
        final String todayDate =  FormatSymbols.formatDateForLocale(TimeUtility.currentTime());  
        addField("todayDate", todayDate); 
        final String todayTime =  FormatSymbols.formatTimeFor24Hrs(TimeUtility.currentTime());
        addField("todayTime", todayTime); 
    }

    public void setXmlFileName(final String fileName) {
        xmlFileName = fileName;
    }

    /** 
     * Utility method needed because new AC adapters never return null.
     * Returns true if the given string is not null and not the empty string. 
     */
    public static boolean notNull(String s) {
        return ((s != null) && (! s.equals("")));
    }
    
    //-------------------------------------------------------------------------
    // SAX DocumentHandler methods
    // (in logical order)
    //-------------------------------------------------------------------------

    //    public void startElement(java.lang.String name, Attributes atts) {
    @Override
	public void startElement(
        String namespaceURI,
        String localName,
        String qName,
        Attributes attrs) {
        String name = qName;
        try {
            // ----------------
            // text formatting
            // NOTE:  these can and should precede "if"; only text/fields can set
            // printThisLine to true and because no text is actually printed
            // ----------------
            if (name.equals("logo")) { 
                printer.printLogo();
            }
            else if (name.equals("head1")) { 
                printer.head1();
            }
            else if (name.equals("head2")) { 
                printer.head2();
            }
            else if (name.equals("head3")) { 
                printer.head3();
            }
            else if (name.equals("head4")) { 
                printer.head4();
            }
            else if (name.equals("largeBold")) { 
                printer.largeBold();
            }
            else if (name.equals("bold")) { 
                printer.bold();
            }
            else if (name.equals("large")) { 
                printer.large();
            }
            else if (name.equals("center")) { 
                printer.center();
            }
            else if (name.equals("left")) { 
                printer.left();
            }
            else if (name.equals("vskip")) { 
                printer.vskip();
            }
            else if (name.equals("largevskip")) { 
                printer.vskip();
                printer.vskip();
            }
            else if (name.equals("eject")) { 
                printer.eject();
            }
            else if (name.equals("reverse")) { 
                printer.reverse();
            }
            else if (name.equals("paragraph")){
                printer.paragraph();
            }
            else if (name.equals("underline")){
                printer.underline();
            }

            // ------------
            // ifdata
            // ------------
            else if (name.equals("if")) { 
                printThisLine = false;
            }
            else if (name.equals("if1")) { 
                printThisLine = false;
            }
            // ------------
            // data fields
            // The data tag will have field or text
            // and may have bool and/or width also.
            // * If BOTH text and field are included,
            //      text is printed before field, and 
            //      width is is applied to the field string.
            // (We don't know if we should print text until we find out about field.)
            // ------------
            // examples:
            // <data bool="agent"/>
            // <data field="agentName"/>
            // <data field=moTotal width="12"/>	
            // <data text="Money Orders" width="-22"/>	
            // <data text="Money Orders" field="agentName"/>
            else if (name.equals("data")) {       
                String bool = attrs.getValue("bool");                
                String field = attrs.getValue("field"); 
                String text = attrs.getValue("text"); 
                String prop = attrs.getValue("prop"); 
                String prop1 = attrs.getValue("prop1"); 
                String width = attrs.getValue("width"); // checked at end 
                String widthp = attrs.getValue("widthp"); 
                String has = attrs.getValue("has"); 
                String s = ""; // holds field string to be printed	
                String ts = ""; // holds text string to be printed	

                if (bool != null) {
                	if (CompileTimeFlags.rcptLogTemplateParsing()) {
                		ExtraDebug.println("bool["+bool+"]" );	
                	}

                    final Boolean flagValueObject = flagMap.get(bool);
                    if (flagValueObject != null) {
                        final Boolean flagValue = (Boolean) flagValueObject;
                        final boolean flagVal = flagValue.booleanValue();
                        
                    	if (CompileTimeFlags.rcptLogTemplateParsing()) {
                    		ExtraDebug.println("bool Content["+flagVal+"]" );	
                    	}
                        if (flagVal){
                            printThisLine = true;
                        }
                    }

                    //----------------------------------------------
                    // if bool and false, nothing else should print;
                    if (!printThisLine){
                        return;
                    }
                    //----------------------------------------------

                }
                
                // has
                if (has != null) {
                	if (CompileTimeFlags.rcptLogTemplateParsing()) {
                        ExtraDebug.println("has["+has+"]" );
                	}

                    final Object fieldValueObject = fieldMap.get(has);
                    if (fieldValueObject != null) {
                        final String sField = fieldValueObject.toString();
                        if (sField.length() > 0) {
                        	if (CompileTimeFlags.rcptLogTemplateParsing()) {
                                ExtraDebug.println("has Content["+sField+"]" );
                        	}
                            printThisLine = true;
                        }
                    }
                    //----------------------------------------------
                    // if field did not exist or was empty, nothing else should 
                    // print;
                    if (!printThisLine){
                        return;
                    }
                    //----------------------------------------------
                }
                
                // text
                if (text != null) {
                	if (CompileTimeFlags.rcptLogTemplateParsing()) {
                        ExtraDebug.println("text["+text+"]" );
                	}
                    if (text.length() > 0) {
                        ts = text;
                    }
                }

                if (prop != null) {
                	if (CompileTimeFlags.rcptLogTemplateParsing()) {
                        ExtraDebug.println("prop["+prop+"]" );	
                	}
                    if (prop.length() > 0) {
                        ts = Messages.getReceiptString(prop);
                    }
                }

                if (prop1 != null) {
                	if (CompileTimeFlags.rcptLogTemplateParsing()) {
                        ExtraDebug.println("prop1["+prop1+"]" );	
                	}
                    if (prop1.length() > 0) {
                        ts = Messages.getString(prop1);
                    }
                }

                // field
                s = ""; 
                if (field != null) {
                	if (CompileTimeFlags.rcptLogTemplateParsing()) {
                        ExtraDebug.println("field["+field+"]" );
                	}
                    final Object fieldValueObject = fieldMap.get(field);
                    if (fieldValueObject != null)
                        s = fieldValueObject.toString();
                	if (CompileTimeFlags.rcptLogTemplateParsing()) {
                        ExtraDebug.println("field content["+s+"]" );
                	}
                }

                // print text
                if (field != null) {
                    if (s != null && s.length() > 0) {
                        printThisLine = true;
                        if (ts != null && ts.length() > 0) {
                        	if (widthp == null)
                        		printer.write(ts);
                        	else {
                        		int wp = 0;
                        		try {
                        			wp = Integer.parseInt(widthp);
                        		} catch (Exception e) {
                        		}
                        		printer.writeInField(ts, wp);
                        	}
                        }
                    }
                }
                else {
                    if (ts != null && ts.length() > 0) {
                        printThisLine = true;
                        s = ts;
                    }
                }

                // print the data field
                if (printThisLine && s != null) {
                    if (s.equals("badLogin")) { 
                        s = ""; 
                        if (badLogins == null) {
                            printThisLine = false;
                        }
                        else {
                            Iterator<String> i = badLogins.iterator();
                            while (i.hasNext()) {
                                s = i.next();
                                printer.left();
                                printer.write(s);
                                printer.leftEnd();
                                printer.vskip();
                            }
                        }
                    }
                    else if (width == null) {
                        printer.write(s);
                    }
                    // loop through badlogins  (have to hardcode width for now)
                    else {
                        printer.writeInField(s, ((Integer.parseInt(width))));
                    }
                }

            } // end else if (name.equals("data"))	
            else {
                subStartElement(name, attrs);
            }

        }
        catch (Exception e) {
            status = PRINTER_ERROR;
            Debug.printStackTrace(e);
        }
    }

    /**
     * Call back method for subclasses to provide handling for additional names
     */
    protected void subStartElement(final String name, final Attributes atts) {
    }

    @Override
	public void characters(char[] chars, int iStart, int iLen) {
        //marker
        String s = String.valueOf(chars, iStart, iLen);

        if (!printThisLine) {
            return;
        }

        try {
            if (s.indexOf('\r') < 0 && s.indexOf('\n') < 0) {
                printer.write(s);
            }
        }
        catch (Exception e) {
            status = PRINTER_ERROR;
            //Debug.printStackTrace(e);
        }
    }

    // reset defaults at the end of each line
    //    public void endElement(java.lang.String name) {
    @Override
	public void endElement(String uri, String localName, String qName) {
        String name = qName;
        //marker
        try {
            // send these commands whether or not <if>
            if (name.equals("head1")) { 
                printer.head1End();
            }
            else if (name.equals("head2")) { 
                printer.head2End();
            }
            else if (name.equals("head3")) { 
                printer.head3End();
            }
            else if (name.equals("head4")) { 
                printer.head4End();
            }
            else if (name.equals("largeBold")) { 
                printer.largeBoldEnd();
            }
            else if (name.equals("bold")) { 
                printer.largeBoldEnd();
            }
            else if (name.equals("large")) { 
                printer.largeEnd();
            }
            else if (name.equals("reverse")) { 
                printer.reverseEnd();
            }

            else if (name.equals("if")) { 
                printer.centerEnd();
                printer.leftEnd();
                printer.paragraphEnd();
                printThisLine = true;
            }

            // these tags print newlines
            else if (name.equals("center")) { 
                printer.centerEnd();
                if (printThisLine)
                    printer.vskip();
            }
            else if (name.equals("paragraph")){ 
                printer.paragraphEnd();
                printThisLine = false;  //ParagraphEnd will add its own CR
            }
            else if (printThisLine) {
                if (name.equals("left")) { 
                	if (CompileTimeFlags.rcptLogTemplateParsing()) {
                		ExtraDebug.println("[leftEnd1]" );	
                	}
                    printer.leftEnd();
                    if (printThisLine)
                        printer.vskip();
                }
            }
            else if (name.equals("left")) { 
            	if (CompileTimeFlags.rcptLogTemplateParsing()) {
            		ExtraDebug.println("[leftEnd2]" );	
            	}
                printer.leftEnd();
            }

        }
        catch (Exception e) {
            status = PRINTER_ERROR;
            //Debug.printStackTrace(e);
        }
    }

    class SimpleFileFilter extends FileFilter {
    	
    	String extension;
    	String description;
	    public SimpleFileFilter(String ext, String desc)
	    {
	    	  extension = ext.toLowerCase(Locale.US);
	    	  description = (desc == null ? ext +"files " : desc );
	    	
	    }
	    @Override
		public boolean accept(File f)
	    {
	    	if(f.isDirectory()){return true;}
	    	String name = f.getName().toLowerCase(Locale.US);
	    	if(name.endsWith(extension)){
	    		return true;
	    	}
	    	return false;
	    }

    	@Override
		public String getDescription(){ return description;}
    }
    //-------------------------------------------------------------------------
    /** main */
    public static void main(String[] args) {

        try {
            final State state = State.getInstance();
            state.createProfile();

        }
        catch (Exception e) {
			Debug.printStackTrace(e);
        }
    }

	public int print(String csvFileName, ReportSelectionCriteria rsc2) {
		return status;
	}
}
