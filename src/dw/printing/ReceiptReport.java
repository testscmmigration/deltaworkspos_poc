package dw.printing;
import java.math.BigInteger;
import java.text.SimpleDateFormat;

import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.PaidMoneyGramInfo40;
import dw.model.adapters.PersonalInfo;
import dw.model.adapters.ReceiverInfo;
import dw.model.adapters.SenderInfo;
import dw.utility.ReceiptTemplates;
import dw.utility.StringUtility;
import dw.utility.TimeUtility;

public class ReceiptReport extends Report {
	
	public static final String ACCNT_ID_KEY = "999999990";
	public static final String DS_ACCT_ID_PLACE_HOLDER = "~ACCT_ID~";

    /**
     * Prints a MoneyGram receipt on the Epson (TMT88II) thermal printer.
     * @param receiptFormat = base name of the XML format file to use
     * @param isReprint = true/false
     * @param isCustomerReceipt = true/false if for agent
     * @param ci = ClientInfo object
     * @param report = the report object
     * @return status code
     */
    private int printMGReceipt(
    		int pm_sTemplateType, String pm_sLanguage,
        boolean isReprint,
        boolean isCustomerReceipt) {
        if (status == OKAY) {
        	parseTemplate(pm_sTemplateType, pm_sLanguage);
			if (status == OKAY)
				TEPrinter.getPrinter().fireOffPrintJob();
        }
        return status;
    }

    /** 
     * Prints the OFAC Failure receipt on the Epson (TMT88II) thermal printer. 
     */

    public int printOfacErrorReceipt(String receiptFormat, boolean isCustomerReceipt) {
        if (status == OKAY) {
            String fileName;
            if (isCustomerReceipt)
                fileName = "xml/receipts/" + receiptFormat + "cu.xml";	 
            else
                fileName = "xml/receipts/" + receiptFormat + "ag.xml";	 

            parseFile(fileName);
            TEPrinter.getPrinter().fireOffPrintJob();
        }
        return status;
    }

    /**
     * Prints a MoneyGram Receive receipt on the Epson (TMT88II) thermal printer.
     * @param receiptFormat = base name of the XML format file to use
     * @param isReprint = true/false
     * @param isCustomerReceipt = true/false if for agent
     * @param pmgi = PaidMoneyGramInfo object
     * @param rdi = ReceiveDetailInfo object
     * @return status code
     */
    public static int printWarningMGReceipt(String receiptFormat, PaidMoneyGramInfo40 pmgi) {
        final ReceiptReport report = new ReceiptReport(TEPrinter.getPrinter(), pmgi);
        // check for hybrid - if so, use secondary language for template access
		String sLanguage = ReceiptTemplates.getReceiptLanguageWarning().indexOf("/") > 0
				? ReceiptTemplates.getReceiptLanguageWarning().substring(4)
				: ReceiptTemplates.getReceiptLanguageWarning();
	
		if (receiptFormat.indexOf("ns") > 0) {
			return report.printMGReceipt(ReceiptTemplates.RECEIPT_WARNING_NS,
					sLanguage, false, false);
		} else {
			return report.printMGReceipt(ReceiptTemplates.RECEIPT_WARNING,
					sLanguage, false, false);
		}
    }

    //Ofac error report
	public static int printMoOfacErrorReceipt(String receiptFormat, boolean isCustomerReceipt, String sourceNumber) {
		
		final ReceiptReport report = new ReceiptReport(TEPrinter.getPrinter(), isCustomerReceipt, sourceNumber);
		return report.printOfacErrorReceipt(receiptFormat, isCustomerReceipt);
	}

    /*
     * Constructs a Warning Receipt object for: printWarningMGSendReceipt
     */
    private ReceiptReport(TEPrinter printer, PaidMoneyGramInfo40 pmgi) {
        super();

        initialize(printer);

        String agentIdValue = StringUtility.getNonNullString(ci.getAgentId());
        addField("agentId", agentIdValue); 

        // get warning info
        if (pmgi != null) {
            addField("refNumber", (pmgi.getReferenceNumber()).toLowerCase()); 
			addField("itemOneNumber", 
					trimLastDigit((pmgi.getAgentCheckNumber() == null ? "0" : pmgi.getAgentCheckNumber().toString())));
			addField("itemTwoNumber", 
					trimLastDigit(
							(pmgi.getAgentCheckNumber() == null || pmgi.getCustomerCheckNumber().toString().equals("")
									? "0" : pmgi.getCustomerCheckNumber().toString())));
            addFlag("itemTwo", !pmgi.getCustomerCheckNumber().equals(BigInteger.ZERO));	 
            //addFlag("logo", EpsonTMT88II.logoFound);
        }
    }

    /**
     * Constructs a Warning Receipt object for:
     * Ofac Failure receipt
     */
    public ReceiptReport(TEPrinter printer, boolean isCustomerReceipt, String sourceNumber) {

        super();
        initializeReceipt(printer, false, isCustomerReceipt, null, null, null);
        addField("currentDate", (new SimpleDateFormat(TimeUtility.RECEIPT_DATE_TIME_FORMAT).format(TimeUtility.currentTime()))); 
        addField("sourceNumber", sourceNumber); 
    }

    private void initializeReceipt(
        final TEPrinter printer,
        final boolean isReprint,
        final boolean isCustomerReceipt,
        final ReceiverInfo receiver,
        final SenderInfo sender,
        final PersonalInfo third) {
        initialize(printer);
        addFlag("agent", !isCustomerReceipt); 
        addFlag("customer", isCustomerReceipt); 
        addFlag("reprint", isReprint); 
        addPersonalInfoFieldValues(receiver, sender, third);
    }

    private void addPersonalInfoFieldValues(final SenderInfo pi, final String prefix) {
        if (pi != null) {
            addField(prefix + "First", pi.getFirstName()); 
            final String lastName = pi.getLastName();
            addField(prefix + "Last", lastName); 
            addField(prefix + "SecondLast", pi.getSecondLastName()); 
            addField(prefix + "Middle", pi.getMiddleName());             
            addField(prefix + "FullName", pi.getFullName());    
            if (pi.getFullName().trim().length() == 0)
            	addField(prefix + "FullNameTrimmed", null);
            else
            	addField(prefix + "FullNameTrimmed", pi.getFullName());            	
            addField(prefix + "MoneySaverID", pi.getMoneySaverID()); 
            addField(prefix + "Address", pi.getAddress()); 
            addField(prefix + "Address2", pi.getAddress2()); 
            addField(prefix + "Address3", pi.getAddress3()); 
            addField(prefix + "Address4", pi.getAddress4()); 
            addField(prefix + "City", pi.getCity()); 
            addField(prefix + "State", pi.getState()); 
            addField(prefix + "Zip", pi.getZip()); 
            addField(prefix + "CityStateZip", pi.getCity() + " " + pi.getState() + " " + pi.getZip());
            if (((pi.getCity() != null) && (! pi.getCity().isEmpty())) || ((pi.getZip() != null) && (! pi.getZip().isEmpty()))) {
                addFlag("has" + prefix + "CityStateZip", true);
            }
            addField(prefix + "Country", pi.getCountry()); 
            addField(prefix + "CountryVerbose", CountryInfo.getCountryName(pi.getCountry())); 
            addField(prefix + "Phone", pi.getHomePhone()); 
            addField(prefix + "Dob", FormatSymbols.formatDateForLocale(pi.getLegalIDDOB())); 
            addField(prefix + "Occ", pi.getOccupation()); 
            addField(prefix + "Org", pi.getOrganization()); 
            StringBuffer photoId = new StringBuffer();
            if (pi.getPhotoIDType()!=null)
                photoId.append(pi.getPhotoIDType()).append(" ");
            if (notNull(pi.getPhotoIDNumber()))
                photoId.append(pi.getPhotoIDNumber()).append(" ");
            if (notNull(pi.getPhotoIDState()))
                photoId.append(pi.getPhotoIDState()).append(" ");
            photoId.append(StringUtility.getNonNullString(pi.getPhotoIDCountry()));
            addField(prefix + "PhotoId", photoId.toString()); 
            StringBuffer legalId = new StringBuffer();
            if (pi.getLegalIDType()!=null)
                legalId.append(pi.getLegalIDType()).append(" ");
            legalId.append(StringUtility.getNonNullString(pi.getLegalIDNumber()));
            addField(prefix + "LegalId", legalId.toString()); 
        }
    }

    private void addPersonalInfoFieldValues(final ReceiverInfo pi, final String prefix) {
        if (pi != null) {
            addField(prefix + "First", pi.getFirstName()); 
            final String lastName = pi.getLastName();
            addField(prefix + "Last", lastName); 
            addField(prefix + "SecondLast", pi.getSecondLastName()); 
            addField(prefix + "Middle", pi.getMiddleName());             
            addField(prefix + "FullName", pi.getFullName());    
            if (pi.getFullName().trim().length() == 0)
            	addField(prefix + "FullNameTrimmed", null);
            else
            	addField(prefix + "FullNameTrimmed", pi.getFullName());            	
            addField(prefix + "MoneySaverID", pi.getMoneySaverID()); 
            addField(prefix + "Address", pi.getAddress()); 
            addField(prefix + "Address2", pi.getAddress2()); 
            addField(prefix + "Address3", pi.getAddress3()); 
            addField(prefix + "Address4", pi.getAddress4()); 
            addField(prefix + "City", pi.getCity()); 
            addField(prefix + "State", pi.getState()); 
            addField(prefix + "Zip", pi.getZip()); 
            addField(prefix + "CityStateZip", pi.getCity() + " " + pi.getState() + " " + pi.getZip());
            if (((pi.getCity() != null) && (! pi.getCity().isEmpty())) || ((pi.getZip() != null) && (! pi.getZip().isEmpty()))) {
                addFlag("has" + prefix + "CityStateZip", true);
            }
            addField(prefix + "Country", pi.getCountry()); 
            addField(prefix + "CountryVerbose", CountryInfo.getCountryName(pi.getCountry())); 
            addField(prefix + "Phone", pi.getHomePhone()); 
            addField(prefix + "Dob", FormatSymbols.formatDateForLocale(pi.getLegalIDDOB())); 
            addField(prefix + "Occ", pi.getOccupation()); 
            addField(prefix + "Org", pi.getOrganization()); 
            StringBuffer photoId = new StringBuffer();
            if (pi.getPhotoIDType()!=null)
                photoId.append(pi.getPhotoIDType()).append(" ");
            if (notNull(pi.getPhotoIDNumber()))
                photoId.append(pi.getPhotoIDNumber()).append(" ");
            if (notNull(pi.getPhotoIDState()))
                photoId.append(pi.getPhotoIDState()).append(" ");
            photoId.append(StringUtility.getNonNullString(pi.getPhotoIDCountry()));
            addField(prefix + "PhotoId", photoId.toString()); 
            StringBuffer legalId = new StringBuffer();
            if (pi.getLegalIDType()!=null)
                legalId.append(pi.getLegalIDType()).append(" ");
            legalId.append(StringUtility.getNonNullString(pi.getLegalIDNumber()));
            addField(prefix + "LegalId", legalId.toString()); 
        }
    }

    private void addPersonalInfoFieldValues(final PersonalInfo pi, final String prefix) {
        if (pi != null) {
            addField(prefix + "First", pi.getFirstName()); 
            final String lastName = pi.getLastName();
            addField(prefix + "Last", lastName); 
            addField(prefix + "SecondLast", pi.getSecondLastName()); 
            addField(prefix + "Middle", pi.getMiddleName());             
            addField(prefix + "FullName", pi.getFullName());    
            if (pi.getFullName().trim().length() == 0)
            	addField(prefix + "FullNameTrimmed", null);
            else
            	addField(prefix + "FullNameTrimmed", pi.getFullName());            	
            addField(prefix + "MoneySaverID", pi.getMoneySaverID()); 
            addField(prefix + "Address", pi.getAddress()); 
            addField(prefix + "Address2", pi.getAddress2()); 
            addField(prefix + "Address3", pi.getAddress3()); 
            addField(prefix + "Address4", pi.getAddress4()); 
            addField(prefix + "City", pi.getCity()); 
            addField(prefix + "State", pi.getState()); 
            addField(prefix + "Zip", pi.getZip()); 
            addField(prefix + "CityStateZip", pi.getCity() + " " + pi.getState() + " " + pi.getZip());
            if (((pi.getCity() != null) && (! pi.getCity().isEmpty())) || ((pi.getZip() != null) && (! pi.getZip().isEmpty()))) {
                addFlag("has" + prefix + "CityStateZip", true);
            }
            addField(prefix + "Country", pi.getCountry()); 
            addField(prefix + "CountryVerbose", CountryInfo.getCountryName(pi.getCountry())); 
            addField(prefix + "Phone", pi.getHomePhone()); 
            addField(prefix + "Dob", FormatSymbols.formatDateForLocale(pi.getLegalIDDOB())); 
            addField(prefix + "Occ", pi.getOccupation()); 
            addField(prefix + "Org", pi.getOrganization()); 
            StringBuffer photoId = new StringBuffer();
            if (pi.getPhotoIDType()!=null)
                photoId.append(pi.getPhotoIDType()).append(" ");
            if (notNull(pi.getPhotoIDNumber()))
                photoId.append(pi.getPhotoIDNumber()).append(" ");
            if (notNull(pi.getPhotoIDState()))
                photoId.append(pi.getPhotoIDState()).append(" ");
            photoId.append(StringUtility.getNonNullString(pi.getPhotoIDCountry()));
            addField(prefix + "PhotoId", photoId.toString()); 
            StringBuffer legalId = new StringBuffer();
            if (pi.getLegalIDType()!=null)
                legalId.append(pi.getLegalIDType()).append(" ");
            legalId.append(StringUtility.getNonNullString(pi.getLegalIDNumber()));
            addField(prefix + "LegalId", legalId.toString()); 
        }
    }

    private void addPersonalInfoFieldValues(
        final ReceiverInfo receiver,
        final SenderInfo sender,
        final PersonalInfo third) {
        addPersonalInfoFieldValues(sender, "sender"); 
        addPersonalInfoFieldValues(receiver, "receiver"); 
        addPersonalInfoFieldValues(third, "third"); 
    }

    public static String getTextAccountNumberLabelFromDeliveryOption(String pm_sDeliveryOptionID) {
		String translatedText = getTextAccountNumberTextFromDeliveryOption(pm_sDeliveryOptionID, false);

		if ((translatedText != null) && translatedText.length() > 0) {
			translatedText = translatedText + ":";
		}
		
		return translatedText;
	}

    private static String getTextAccountNumberTextFromDeliveryOption(String deliveryOptionID, boolean pm_bReceiptLocale) {
		String value = null;

		if (deliveryOptionID.equalsIgnoreCase("CARD_DEPOSIT")) {
			// For delivery option 9
			value = pm_bReceiptLocale ? Messages.getReceiptString("MGReceipt.CardDeposit") : Messages.getString("MGReceipt.CardDeposit");
		} else if (deliveryOptionID.equalsIgnoreCase("BANK_DEPOSIT")) {
			// For delivery option 10
			value = pm_bReceiptLocale ? Messages.getReceiptString("MGReceipt.AccountDeposit") : Messages.getString("MGReceipt.AccountDeposit");
		} else if (deliveryOptionID.equalsIgnoreCase("MOBILE_WALLET")) {
			// For delivery option 18
			value = pm_bReceiptLocale ? Messages.getReceiptString("MGReceipt.MobileDeposit") : Messages.getString("MGReceipt.MobileDeposit");
		} else {
			value = pm_bReceiptLocale ? Messages.getReceiptString("MGReceipt.AccountDeposit") : Messages.getString("MGReceipt.AccountDeposit");
		}
		
		return value;
	}

    /** Remove the last character from a string longer than two characters.
      The intention is to remove a check digit from a number long enough to
      contain a check digit, but this method does not currently check to
      see if the character being removed is actually a digit.
    */
    private static String trimLastDigit(String s) {
        String result;
        if (s.length() > 2)
            result = s.substring(0, s.length() - 1);
        else
            result = s;

        //Debug.println("trimLastDigit(" + s + ") => \"" + result + "\"");
        return result;
    }
}
