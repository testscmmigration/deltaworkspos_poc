package dw.printing;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import org.xml.sax.Attributes;

import com.moneygram.agentconnect.BillPaymentDetailInfo;
import com.moneygram.agentconnect.ProductVariantType;

import dw.comm.MessageFacade;
import dw.comm.MessageFacadeError;
import dw.comm.MessageFacadeParam;
import dw.comm.MessageLogic;
import dw.framework.CommCompleteInterface;
import dw.framework.WaitToken;
import dw.i18n.FormatSymbols;
import dw.io.info.ReportEventDiagInfo;
import dw.io.report.AuditLog;
import dw.io.report.MoneyGramDetail;
import dw.io.report.ReportDetail;
import dw.io.report.ReportLog;
import dw.io.report.ReportSelectionCriteria;
import dw.io.report.ReportSummary;
import dw.profile.UnitProfile;
import dw.utility.Debug;
import dw.utility.TimeUtility;

public class ProcessingFeeReport extends Report {

    private Collection<ReportDetail> utilityDetailItems;
    private Collection<ReportDetail> prepaidDetailItems;
    private ReportSummary reportSummary = null;
    private int userID;
    private boolean isSourceHost=false;

    public ProcessingFeeReport(int userID) {
        super(null);
    	this.userID = userID;
    }

    @Override
	public int print(final TEPrinter printer,
                      final ReportSelectionCriteria rsc) {
        status = OKAY;
        initSelectionCriteria(printer, rsc);
        String reportSource = "";	
        reportSummary = null;
        addFlag("super",UnitProfile.getInstance().isMultiAgentMode()); 
        addFlag("notsuper",!UnitProfile.getInstance().isMultiAgentMode()); 
        addFlag("local", !(rsc.getAccessHost())); 
        final String activityDate = FormatSymbols.formatDateForLocale(timeStamp);
        addField("activityDate",activityDate); 
        try {
            if (inDemoMode) {
                rsc.setAccessHost(true);
            }

            isSourceHost = rsc.getAccessHost();
            if (isSourceHost) { // get data from host
                reportSource = inDemoMode ? "Demo" : "Host";
                reportSummary = new ReportSummary();

                boolean success = getHostData(timeStamp);
                writeReportEventMessage(
                        ReportEventDiagInfo.PROCESSING_FEE_REPORT,
                        success);

                if (! success) {
                    status = HOST_CONNECT_ERROR;
                    return status;
                }         
            }
            else {
                final Collection<ReportDetail> mixedDetailItems = ReportLog.getDetailRecords(rsc);
                detailItems = ReportLog.getNewDetailRecordCollection();
                utilityDetailItems = ReportLog.getNewDetailRecordCollection();
                prepaidDetailItems = ReportLog.getNewDetailRecordCollection();
           
                final Iterator<ReportDetail> it = mixedDetailItems.iterator();
                while (it.hasNext()) {
                    final Object detailItem = it.next();
                    if (detailItem instanceof MoneyGramDetail) {
                        final MoneyGramDetail moneyGramDetail = (MoneyGramDetail)detailItem;
                        if (moneyGramDetail.getType() == ReportLog.UTILITY_PAYMENT_TYPE) {
                        	if (moneyGramDetail.getInfoFeeIndicator().equals("true")) {
                        		if (moneyGramDetail.getProcessingFee().compareTo(BigDecimal.ZERO) > -1) {
	                        		utilityDetailItems.add(moneyGramDetail);
	                        		detailItems.add(moneyGramDetail);
                        		}
                        	}
                        	
                        }
                        else if (moneyGramDetail.getType() == ReportLog.PREPAID_SERVICES_TYPE) {
                        	if (moneyGramDetail.getInfoFeeIndicator().equals("true")) {
                        		if (moneyGramDetail.getProcessingFee().compareTo(BigDecimal.ZERO) > -1) {
	                        		prepaidDetailItems.add(moneyGramDetail);
	                        		detailItems.add(moneyGramDetail);
                        		}
                        	}
                        	
                        }
                        else {
                            Debug.println("Invalid bill payment type " + moneyGramDetail.getType());
                        }

                       // detailItems.add(moneyGramDetail);
                    }
                }
                reportSummary = new ReportSummary(detailItems);
            }

            setBadLogins(reportSummary.getBadLogs());

            // client source (in case host fails)
            if (!isSourceHost) {
                reportSource = "Local";
                writeReportEventMessage(ReportEventDiagInfo.PROCESSING_FEE_REPORT, true);
            }
        }
        finally {
            addField("reportSource",reportSource); 
            reportSummary.addFieldValues(fieldMap);
        }      
        AuditLog.writeReportRecord(userID, rsc.getStartDateTime(), "RPT");
        return super.print(printer, rsc);
    }

    private boolean getHostDataResult;

    private void setHostDataResult(boolean b) {
        getHostDataResult = b;
    }

    /*
     * Gets data from the middleware for the MoneyGram Sales report.
     * On failure return false.
     */

    private boolean getHostData(final long timeStamp) {
        final MessageFacadeParam parameters = new MessageFacadeParam(MessageFacadeParam.REAL_TIME,
                                                                 MessageFacadeParam.NO_HOST_RECORDS_FOUND, 
                                                                 MessageFacadeParam.NO_HOST_SEND_RECORDS_FOUND);
        final WaitToken wait = new WaitToken();

        final CommCompleteInterface callingPage = new CommCompleteInterface() {
            @Override
			public void commComplete(int commTag, Object retValue) {
            	wait.setWaitFlag(false);
                if (retValue instanceof Boolean) {
                    setHostDataResult(((Boolean) retValue).booleanValue());
                }
                synchronized(wait) {
                	wait.notify();
                }
            }
        };

        final MessageLogic logic = new MessageLogic() {
            @Override
			public Object run() {
                boolean results = true;
                String inquiringUnitOffice = "";	
                //ReceiveDetailReportInfo[] rdri = null;
                //SendDetailReportInfo[] sdri = null;
                BillPaymentDetailInfo[] bpdri = null;

                String activityDate = new SimpleDateFormat(TimeUtility.DATE_MONTH_DAY_SHORT_YEAR_NO_DELIMITER_FORMAT).format(new Date(timeStamp));                             
                
             
                try {
                    bpdri = MessageFacade.getInstance().getMoneyGramBillPayDetailReport(parameters, new Date(timeStamp));
                  if(bpdri.length == 0)
                      bpdri=null;
                }
                catch (MessageFacadeError bpdriex) {
                    bpdri = null;
                }
                
                MessageFacade.hideDialogNow();

                // put data into variables
                if (status == OKAY) {                 
                    detailItems = ReportLog.getNewDetailRecordCollection();
                    utilityDetailItems = ReportLog.getNewDetailRecordCollection();
                    prepaidDetailItems = ReportLog.getNewDetailRecordCollection();
                                       
                    if (bpdri != null)
                        for (int i = 0; i < bpdri.length; i++) {
                            final BillPaymentDetailInfo updriRec = bpdri[i];                           
                            if(updriRec.getProductVariant().equals(ProductVariantType.UBP)){
                            	if (updriRec.isInfoFeeIndicator()) {                          		
                            		BigDecimal fee = updriRec.getProcessingFee();
                            		if (fee.compareTo(BigDecimal.ZERO) > -1) {
                            			utilityDetailItems.add(new MoneyGramDetail(activityDate,updriRec,false)); 
                            			detailItems.add(new MoneyGramDetail(activityDate,updriRec,false));
                            		}
                            	}
                            }
                                
                            if(updriRec.getProductVariant().equals(ProductVariantType.PREPAY)){
                            	if (updriRec.isInfoFeeIndicator()) {
                            		BigDecimal fee = updriRec.getProcessingFee();
                            		if (fee.compareTo(BigDecimal.ZERO) > -1) {
                            			prepaidDetailItems.add(new MoneyGramDetail(activityDate,updriRec,true));  
                            			detailItems.add(new MoneyGramDetail(activityDate,updriRec,true));
                            		}
                            	}
                            }
                        }                  
                }
                
                if (results) {
                	reportSummary = new ReportSummary(detailItems);
                    return Boolean.TRUE;
                }
                return Boolean.FALSE;
            }
        };
        setHostDataResult(false);
		wait.setWaitFlag(true);
        MessageFacade.run(logic, callingPage, 0, Boolean.FALSE);
        synchronized(wait) {
            try {
            	while (wait.isWaitFlag()) {
            		wait.wait();
            	}
            }
            catch (InterruptedException ie) {
            }
        }
        return getHostDataResult;
    }

    /**
     * Call back method for subclasses to provide handling for additional names
     */
    @Override
	protected void subStartElement(final String name, final Attributes attrs) {
    	
    	if (name.equals("pfutilitydetail")) {    	
        	new ProcessingFeeDetailPrinter(this,utilityDetailItems,attrs,"utility").print(isSourceHost);
        }
        else if (name.equals("pfprepaiddetail")) {   	
        	new ProcessingFeeDetailPrinter(this,prepaidDetailItems,attrs,"prepaid").print(isSourceHost);
        }

    }
}

class ProcessingFeeDetailPrinter extends DetailPrinter {
    private BigDecimal faceAmtTotal = BigDecimal.ZERO;
    private BigDecimal feeTotal = BigDecimal.ZERO;
    private String direction;

    public ProcessingFeeDetailPrinter(  final Report report,
                                    final Collection<ReportDetail> detailItems,
                                    final Attributes attrs,
                                    final String direction) {

        super(report,detailItems,attrs);
        this.direction = direction;
    }

@Override
public void checkDetail(final Object detail,boolean hostReport) {

        final MoneyGramDetail mgd = (MoneyGramDetail) detail;
        final Date time = new Date(mgd.getDateTime());
        final BigDecimal amount = mgd.getAmount();
        final BigDecimal fee = mgd.getFee();
        final BigDecimal procFee = mgd.getProcessingFee();
        final String processFee;
        final HashMap<String, Object> fieldMap = report.getFieldMap();
        
        if (UnitProfile.getInstance().isMultiAgentMode())
            fieldMap.put("emp", mgd.getAgentID()); 
        else
            fieldMap.put("emp", mgd.getEmployeeNumber());
        if(hostReport)
        	fieldMap.put("time", FormatSymbols.formatTimeFor24Hrs(time)); 
        else
            fieldMap.put("time", FormatSymbols.formatTimeForLocale24(time)); 
        fieldMap.put("refnum", mgd.getReferenceNumber()); 
        fieldMap.put("amount", FormatSymbols.convertDecimal(amount.toString()));

		if ((mgd.getType() == ReportLog.UTILITY_PAYMENT_TYPE) || (mgd.getType() == ReportLog.PREPAID_SERVICES_TYPE)) {
			if (mgd.getCurrencyCode() == null || mgd.getCurrencyCode().equals("0")
					|| mgd.getCurrencyCode().trim().length() == 0)
				fieldMap.put("currency", UnitProfile.getInstance().get("AGENT_CURRENCY", ""));
			else
				fieldMap.put("currency", mgd.getCurrencyCode());

			if (mgd.getFee() != null) {
				processFee = FormatSymbols.convertDecimal(fee.toString());
			} else {
				processFee = "0.00";
			}
		} else {

			if (mgd.getProcessingFee() != null) {
				processFee = FormatSymbols.convertDecimal(procFee.toString());
			} else {
				processFee = "0.00";
			}
		}
        	

        fieldMap.put("fee", processFee); 
        faceAmtTotal = faceAmtTotal.add(amount); 
        feeTotal = feeTotal.add(new BigDecimal(processFee));
        printLine();
    }
    
    protected void finish() {
        final BigDecimal total = faceAmtTotal.add(feeTotal);
        final HashMap<String, Object> fieldMap = report.getFieldMap();
        fieldMap.put("facetotal", FormatSymbols.convertDecimal(faceAmtTotal.toString()));
        fieldMap.put("feetotal", FormatSymbols.convertDecimal(feeTotal.toString())); 
        if (direction.equals("send") || direction.equals("utility") || direction.equals("prepaid")) {
           String currencyCode = UnitProfile.getInstance().get("AGENT_CURRENCY", "");
           fieldMap.put(direction + "total",FormatSymbols.convertDecimal(total.toString()) + " " + currencyCode);
           fieldMap.put(direction + "procfeetotal", FormatSymbols.convertDecimal(feeTotal.toString()) + " " + currencyCode);
        }
        else
           fieldMap.put(direction + "total",FormatSymbols.convertDecimal(total.toString())); 
    }

	/* (non-Javadoc)
	 * @see dw.printing.DetailPrinter#printNoDetailLine()
	 */
	@Override
	protected void printNoDetailLine() {
	}
}
