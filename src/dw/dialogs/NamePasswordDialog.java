package dw.dialogs;


import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JTextField;

import dw.framework.XmlDefinedPanel;
import dw.framework.XmlDialogPanel;
import dw.i18n.Messages;

/**
 * General purpose name and/or password dialog. Can be used to prompt for a
 * normal name/pass, a manager name/pass, a dual control name/pass, or a
 * multiple attempt name/pass.
 *
 * Used to be a subclass of NoTimeoutDialog and therefore automatically disabled
 * the idle timer while displayed. But I can't think of any good reason why the 
 * name/password dialog should be immune to idle timeouts. --gea
 *
 */
public class NamePasswordDialog extends XmlDialogPanel {
    // types of name/pass dialogs to be used when creating
    public static final int USER_LOGIN = 0;
    public static final int MANAGER_LOGIN = 1;
    public static final int DUAL_CONTROL_LOGIN = 2;

    private JTextField nameField = null;
    private JTextField passwordField = null;
    private JButton okButton = null;
    private JButton cancelButton = null;
    private boolean nameRequired = false;
    private boolean canceled = true;
    private static final int RETRIES = 3; 
    private int retryCount = RETRIES;
    //private Component parent;
    //private XmlDefinedPanel mainPanel;

    protected NamePasswordDialog(int loginType, boolean nameRequired, boolean invalid) {
        this.title = Messages.getString("NamePasswordDialog.1890"); 
        this.nameRequired = nameRequired;
        retryCount = RETRIES;
        
        String xmlFile = null;
        if (loginType == DUAL_CONTROL_LOGIN)
            xmlFile = "dialogs/DialogDualEntryNamePassword.xml";	
        else if (loginType == MANAGER_LOGIN)
            xmlFile = "dialogs/DialogManagerNamePassword.xml";	
        else 
            xmlFile = "dialogs/DialogNamePassword.xml";	

        mainPanel = new XmlDefinedPanel(xmlFile);

        nameField = (JTextField) mainPanel.getComponent("nameField");
        passwordField = (JTextField) mainPanel.getComponent("passwordField");
        okButton = (JButton) mainPanel.getComponent("okButton");
        cancelButton = (JButton) mainPanel.getComponent("cancelButton");

        if (! nameRequired) {
            mainPanel.removeComponent("namePanel");             // "Name:"
            mainPanel.removeComponent("namePassLabelPanel");    // "Name and PIN / password required"
            if (invalid) {
                mainPanel.removeComponent("passLabelPanel");    // "Password required"
            }
            else {
                mainPanel.removeComponent("invalidPanel");      // "Login unsuccessful"
            }
            passwordField.requestFocus();
        }
        else {
            mainPanel.removeComponent("passLabelPanel");        // "Password required"
            if (invalid) {
                mainPanel.removeComponent("namePassLabelPanel");    // "Name and PIN / password required"
            }
            else {
                mainPanel.removeComponent("invalidPanel");      // "Login unsuccessful"
            }
            nameField.requestFocus();
        }

//        if (! invalid)
//            mainPanel.removeComponent("invalidPanel");  // "Login unsuccessful"

        mainPanel.addActionListener("okButton", this);	
        mainPanel.addActionListener("cancelButton", this);	
        KeyListener escaper = new KeyAdapter() {
                      @Override
					public void keyPressed(KeyEvent e) {
                          if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
                              cancelButton.doClick();
                          }};
        KeyListener activator = new KeyAdapter() {
                      @Override
					public void keyPressed(KeyEvent e) {
                          if (e.getKeyCode() == KeyEvent.VK_ENTER)
                              okClick();
                          }};
        mainPanel.addKeyListener("passwordField", escaper);	
        mainPanel.addKeyListener("nameField", escaper);	
        mainPanel.addKeyListener("passwordField", activator);	
        mainPanel.addKeyListener("nameField", activator);	

        // set the size of the dialog based on what need to be displayed
/*        
        int width = Integer.parseInt(mainPanel.getParam("width"));	
        int height = Integer.parseInt(mainPanel.getParam("height"));	
        if (nameRequired)
            height += 30;
        if (invalid)
            height += 40;
        setSize(width, height);
*/
      
    }
  
    
    /** 
     * Returns false to indicate this dialog's title bar text should not
     * be logged in the trace file.
     */
    @Override
	public boolean showTitleInDebug() {
        return false;
    }
    
    public void okClick() {
        okButton.doClick();
       
        
    }

    public void okButtonAction() {    
        canceled = false;
        tryLogin();
    }

    public void cancelButtonAction() {
        closeDialog(Dialogs.CANCEL_OPTION);
    }

    public void tryLogin() {
    	retryCount--;
    	if (retryCount == 0) {
    		closeDialog(Dialogs.CANCEL_OPTION);
    	}
        if (nameField.hasFocus())
            if (passwordField.getText().length() == 0)
                passwordField.requestFocus();
            else
                closeDialog(Dialogs.OK_OPTION);
        else if (nameRequired)
            if (nameField.getText().length() == 0)
                nameField.requestFocus();
            else
                closeDialog(Dialogs.OK_OPTION);
        else
            closeDialog(Dialogs.OK_OPTION);
    }

    public boolean isCanceled() {
        return canceled;
    }
    
    public String getName() {
        if (nameRequired)
            return nameField.getText();
        else
            return null;
    }

    public String getPassword() {
        return passwordField.getText();
    }
    
    public void setName(String name) {
    	nameField.setText(name);
    }

    public void setPassword(String password) {
        passwordField.setText(password);
    }
}
