package dw.dialogs;

import java.awt.Color;
import java.awt.SystemColor;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import dw.dwgui.DWTextField;
import dw.dwgui.MoneyField;
import dw.i18n.Messages;
import dw.io.report.ReportLog;
import dw.io.report.ReportSelectionCriteria;
import dw.io.report.ReportSummary;
import dw.printing.Report;
import dw.profile.Profile;
import dw.profile.ProfileItem;
import dw.profile.UnitProfile;
import dw.utility.Scheduler;
import dw.utility.TimeUtility;

public class DetailSelectionEntryDialog extends ReportDialog {
    private JComboBox employeeBox;
    private DWTextField beginSerialNumber, endSerialNumber;
    private MoneyField beginAmount, endAmount;
    private JLabel employeeSelected, serialRangeSelected, amountRangeSelected;
    private JComboBox beginMonthBox, beginDayBox, beginYearBox, beginHourBox, beginMinuteBox, beginAmPmBox;
    private JComboBox endMonthBox, endDayBox, endYearBox, endHourBox, endMinuteBox, endAmPmBox;
    private long periodOpened; 
    private int employeeSelection;
    private String noSelection = " ";	
    private boolean editEmployee = false;
    private boolean editDate = false;
    private boolean editSerial = false;
    private boolean editAmount = false;
    private WindowListener winListener;
    private long currentTime = TimeUtility.currentTimeMillis();
    private Calendar begLocalCalendar = Scheduler.getClonedLocalCalendar();
    private Calendar endLocalCalendar = Scheduler.getClonedLocalCalendar();
    private String[] ampmSelection = {"AM","PM"};	 
    private static String[] months = {
    		Messages.getString("DetailSelectionEntryDialog.1870"), 
    		Messages.getString("DetailSelectionEntryDialog.1871"), 
    		Messages.getString("DetailSelectionEntryDialog.1872"), 
    		Messages.getString("DetailSelectionEntryDialog.1873"),    
    		Messages.getString("DetailSelectionEntryDialog.1874"), 
    		Messages.getString("DetailSelectionEntryDialog.1875"), 
    		Messages.getString("DetailSelectionEntryDialog.1876"), 
    		Messages.getString("DetailSelectionEntryDialog.1877"),    
    		Messages.getString("DetailSelectionEntryDialog.1878"), 
    		Messages.getString("DetailSelectionEntryDialog.1879"), 
    		Messages.getString("DetailSelectionEntryDialog.1880"),   
    		Messages.getString("DetailSelectionEntryDialog.1881")}; 
    
/*
    public DetailSelectionEntryDialog(final Report report,JComponent parent, String title) {
        this(report,parent, true, true, true, true, true, true, true, true, title);
    }
*/
    public DetailSelectionEntryDialog(final Report report,
            boolean showDateRangeOption,
            boolean editDate,
            boolean showEmployeeOption,
            boolean editEmployee,
            boolean showSerialRangeOption,
            boolean editSerial,
            boolean showAmountOption,
            boolean editAmount,
            String title,
			boolean csvOption,
			boolean csvEnable) {
        super("dialogs/DialogDetailSelectionEntry.xml", report, 	
                title, true, true, false, csvOption, csvEnable);
        //this.rsc = rsc;
        this.editEmployee = editEmployee;
        this.editDate = editDate;
        this.editSerial = editSerial;
        this.editAmount = editAmount;                            
        final TreeSet<ReportSummary> summaryItems = ReportLog.getSummaryRecordsClone();
        final Iterator<ReportSummary> it = summaryItems.iterator();
        while (it.hasNext()){
               final ReportSummary summaryItem = it.next();
               periodOpened = summaryItem.getPeriodOpened();
        }       
        beginSerialNumber = (DWTextField) mainPanel.getComponent("beginSerialNumber");	
        endSerialNumber = (DWTextField) mainPanel.getComponent("endSerialNumber");	
        beginAmount = (MoneyField) mainPanel.getComponent("beginAmount");	
        endAmount = (MoneyField) mainPanel.getComponent("endAmount");	
        employeeBox = (JComboBox) mainPanel.getComponent("employeeBox");	
        beginMonthBox = (JComboBox) mainPanel.getComponent("beginMonthBox");	
        beginDayBox = (JComboBox) mainPanel.getComponent("beginDayBox");	
        beginYearBox = (JComboBox) mainPanel.getComponent("beginYearBox");	
        beginHourBox = (JComboBox) mainPanel.getComponent("beginHourBox");	
        beginMinuteBox = (JComboBox) mainPanel.getComponent("beginMinuteBox");	
        beginAmPmBox = (JComboBox) mainPanel.getComponent("beginAmPmBox");	
        endMonthBox = (JComboBox) mainPanel.getComponent("endMonthBox");	
        endDayBox = (JComboBox) mainPanel.getComponent("endDayBox");	
        endYearBox = (JComboBox) mainPanel.getComponent("endYearBox");	
        endHourBox = (JComboBox) mainPanel.getComponent("endHourBox");	
        endMinuteBox = (JComboBox) mainPanel.getComponent("endMinuteBox");	
        endAmPmBox = (JComboBox) mainPanel.getComponent("endAmPmBox");	
        employeeSelected = (JLabel) mainPanel.getComponent("employeeSelected");	
        serialRangeSelected = (JLabel) mainPanel.getComponent("serialRangeSelected");	
        amountRangeSelected = (JLabel) mainPanel.getComponent("amountRangeSelected");	
        populateBeginDateTime(periodOpened);
        populateEndDateTime(currentTime);
        populateEmployeeBox();
        employeeSelected.setForeground(Color.black);
        amountRangeSelected.setForeground(Color.black);
        serialRangeSelected.setForeground(Color.black);
        mainPanel.addActionListener("employeeBox", this, "employeeAction");	 
        mainPanel.addActionListener("beginMonthBox", this, "beginMonthAction");	 
        mainPanel.addActionListener("beginDayBox", this, "beginDayAction");	 
        mainPanel.addActionListener("beginYearBox", this, "beginYearAction");	 
        mainPanel.addActionListener("beginHourBox", this, "beginHourAction");	 
        mainPanel.addActionListener("beginMinuteBox", this, "beginMinuteAction");	 
        mainPanel.addActionListener("beginAmPmBox", this, "beginHourAction");	 
        mainPanel.addActionListener("endMonthBox", this, "endMonthAction");	 
        mainPanel.addActionListener("endDayBox", this, "endDayAction");	 
        mainPanel.addActionListener("endYearBox", this, "endYearAction");	 
        mainPanel.addActionListener("endHourBox", this, "endHourAction");	 
        mainPanel.addActionListener("endMinuteBox", this, "endMinuteAction");	 
        mainPanel.addActionListener("endAmPmBox", this, "endHourAction");	 
        mainPanel.addTextListener("beginSerialNumber", this, "serialRangeAction");	 
        mainPanel.addTextListener("endSerialNumber", this, "serialRangeAction");	 
        mainPanel.addTextListener("beginAmount", this, "amountRangeAction");	 
        mainPanel.addTextListener("endAmount", this, "amountRangeAction");	 
        employeeBox.addKeyListener(ESCAPE_ADAPTER);
        beginMonthBox.addKeyListener(ESCAPE_ADAPTER);
        beginDayBox.addKeyListener(ESCAPE_ADAPTER);
        beginYearBox.addKeyListener(ESCAPE_ADAPTER);
        beginHourBox.addKeyListener(ESCAPE_ADAPTER);
        beginMinuteBox.addKeyListener(ESCAPE_ADAPTER);
        beginAmPmBox.addKeyListener(ESCAPE_ADAPTER);
        endMonthBox.addKeyListener(ESCAPE_ADAPTER);
        endDayBox.addKeyListener(ESCAPE_ADAPTER);
        endYearBox.addKeyListener(ESCAPE_ADAPTER);
        endHourBox.addKeyListener(ESCAPE_ADAPTER);
        endMinuteBox.addKeyListener(ESCAPE_ADAPTER);
        endAmPmBox.addKeyListener(ESCAPE_ADAPTER);
        beginSerialNumber.addSafeKeyListener(ESCAPE_ADAPTER);
        endSerialNumber.addSafeKeyListener(ESCAPE_ADAPTER);
        beginAmount.addSafeKeyListener(ESCAPE_ADAPTER);
        endAmount.addSafeKeyListener(ESCAPE_ADAPTER);
        
        winListener = new MyWinListener();
        
        if (! showDateRangeOption) {
            mainPanel.removeComponent("beginMonthPanel");	
            mainPanel.removeComponent("beginDayPanel");	
            mainPanel.removeComponent("beginYearPanel");	
            mainPanel.removeComponent("beginTimePanel");	
            mainPanel.removeComponent("endMonthPanel");	
            mainPanel.removeComponent("endDayPanel");	
            mainPanel.removeComponent("endYearPanel");	
            mainPanel.removeComponent("endTimePanel");	
        }
        if (! showEmployeeOption) 
            mainPanel.removeComponent("showEmployeePanel");	
        if (! showSerialRangeOption) 
            mainPanel.removeComponent("showSerialPanel");	
	    if (! showAmountOption) 
		    mainPanel.removeComponent("showAmountPanel");	
		if (showAmountOption & showSerialRangeOption)
		   mainPanel.removeComponent("employeeTextPanel");	
		else {
		   mainPanel.removeComponent("detailTextPanel");	
		   mainPanel.removeComponent("optionalPanel");	
		   mainPanel.removeComponent("optionalPanel2");	
		}   
    }
    
    class MyWinListener extends WindowAdapter {
        @Override
		public void windowClosing(WindowEvent e) {
            cancelAction();
        }
    }
    
    public WindowListener getWindowListener() {
        return winListener;
    }
    
    public long getSummaryDateTime() {return begLocalCalendar.getTime().getTime();}
    public long getBeginDateTime()   {return begLocalCalendar.getTime().getTime();}
    public long getEndDateTime()     {return endLocalCalendar.getTime().getTime();}
    public int getEmployeeNum()      {return employeeSelection;}

    public long getBeginSerialNum(){
        final String s = beginSerialNumber.getText();
    	long bsn = ReportSelectionCriteria.INT_NOT_SPECIFIED;
        if  (s != null && s.length() > 0) {
        	try {
        		bsn = Long.parseLong(s);
        	} catch (Exception e) {
        	}
        }
        return bsn;
    }
    
    public long getEndSerialNum(){
        final String s = endSerialNumber.getText();
        long esn = ReportSelectionCriteria.INT_NOT_SPECIFIED;
        if  (s != null && s.length() > 0) {
        	try {
        		esn = Long.parseLong(s);
        	} catch (Exception e) {
        	}
        }
        return esn;
    }
    
    public BigDecimal getBeginAmount(){
        final String s = beginAmount.getText();
        final BigDecimal bd = new BigDecimal(beginAmount.getText());
        if  (s.length() == 0 || bd.compareTo(BigDecimal.ZERO) == 0)
            return new BigDecimal(ReportSelectionCriteria.INT_NOT_SPECIFIED);
        else
            return bd;
    }
    
    public BigDecimal getEndAmount(){
        final String s = endAmount.getText();
        final BigDecimal bd = new BigDecimal(endAmount.getText());
        if  (s.length() == 0 || bd.compareTo(BigDecimal.ZERO) == 0)
            return new BigDecimal(ReportSelectionCriteria.INT_NOT_SPECIFIED);
        else 
            return bd;
    }
    
    private void populateBeginDateTime(long periodOpened) {
        
        begLocalCalendar.setTime(new Date(periodOpened));
        begLocalCalendar.set(Calendar.SECOND,00);
        /* add current year -1 and current year to drop down year box */
        beginYearBox.addItem("" + (begLocalCalendar.get(Calendar.YEAR)-1));	
        beginYearBox.addItem("" + begLocalCalendar.get(Calendar.YEAR));	

        beginYearBox.setSelectedItem("" + begLocalCalendar.get(Calendar.YEAR));	
        for (int i = 0; i < 12; i++)
            beginMonthBox.addItem(months[i]);
        beginMonthBox.setSelectedItem(months[begLocalCalendar.get(Calendar.MONTH)]);
        for (int i = 0; i < begLocalCalendar.getActualMaximum(Calendar.DATE); i++)
            beginDayBox.addItem("" + (i + 1));	
        beginDayBox.setSelectedItem("" + begLocalCalendar.get(Calendar.DATE));	
        for (int i = 0; i <= begLocalCalendar.getActualMaximum(Calendar.HOUR); i++)
            beginHourBox.addItem("" + (i + 1));	
        int hr = begLocalCalendar.get(Calendar.HOUR );
        if (hr == 0) 
            hr = 12;
        beginHourBox.setSelectedItem("" + hr);	
        for (int i = 0; i <= begLocalCalendar.getActualMaximum(Calendar.MINUTE); i++) {
            String s = String.valueOf(i);
            if (s.length() == 1)
                s = 0 + s;
            beginMinuteBox.addItem("" + s);	
        }
        beginMinuteBox.setSelectedItem("" + begLocalCalendar.get(Calendar.MINUTE));	
        beginAmPmBox.addItem(ampmSelection[Calendar.AM]);
        beginAmPmBox.addItem(ampmSelection[Calendar.PM]);
        beginAmPmBox.setSelectedItem(ampmSelection[begLocalCalendar.get(Calendar.AM_PM)]);
    }
    
    private void populateEndDateTime(long current) {
        
        endLocalCalendar.setTime(new Date(current));
        endLocalCalendar.set(Calendar.SECOND,59);
        endLocalCalendar.set(Calendar.MILLISECOND,999);
        /* add current year -1 and current year to drop down year box */
        endYearBox.addItem("" + (endLocalCalendar.get(Calendar.YEAR)-1));	
        endYearBox.addItem("" + endLocalCalendar.get(Calendar.YEAR));	

        endYearBox.setSelectedItem("" + endLocalCalendar.get(Calendar.YEAR));	
        for (int i = 0; i < 12; i++)
            endMonthBox.addItem(months[i]);
        endMonthBox.setSelectedItem(months[endLocalCalendar.get(Calendar.MONTH)]);
        for (int i = 0; i < endLocalCalendar.getActualMaximum(Calendar.DATE); i++)
            endDayBox.addItem("" + (i + 1));	
        endDayBox.setSelectedItem("" + endLocalCalendar.get(Calendar.DATE));	
        for (int i = 0; i <= endLocalCalendar.getActualMaximum(Calendar.HOUR); i++)
            endHourBox.addItem("" + (i + 1));	
/*        endHourBox.setSelectedItem("" + (endLocalCalendar.get(Calendar.HOUR + 1)));*/	
        int hr = endLocalCalendar.get(Calendar.HOUR );
        if (hr == 0) 
            hr = 12;
        endHourBox.setSelectedItem("" + hr);	
        for (int i = 0; i <= endLocalCalendar.getActualMaximum(Calendar.MINUTE); i++) {
            String s = String.valueOf(i);
            if (s.length() == 1)
                s = 0 + s;
            endMinuteBox.addItem("" + s);	
        }
        endMinuteBox.setSelectedItem("" + endLocalCalendar.get(Calendar.MINUTE));	
        endAmPmBox.addItem(ampmSelection[Calendar.AM]);
        endAmPmBox.addItem(ampmSelection[Calendar.PM]);
        endAmPmBox.setSelectedItem(ampmSelection[endLocalCalendar.get(Calendar.AM_PM)]);
    }
    
    public synchronized void updateBegFields() {
        Calendar newCal = (Calendar) begLocalCalendar.clone();
        newCal.set(Calendar.DATE, 1);
        int day = beginDayBox.getSelectedIndex() + 1;
        int days = newCal.getActualMaximum(Calendar.DATE);

        beginDayBox.removeAllItems();
        for (int i = 0; i < days; i++)
            beginDayBox.addItem("" + (i + 1));	
        // make sure we are not out of range for selected day of new month
        if (day > days)
            day = days;
        begLocalCalendar.set(Calendar.DATE, day);
        beginDayBox.setSelectedItem("" + day);	
    }
    
    public synchronized void updateEndFields() {
        Calendar newCal = (Calendar) endLocalCalendar.clone();
        newCal.set(Calendar.DATE, 1);
        int day = endDayBox.getSelectedIndex() + 1;
        int days = newCal.getActualMaximum(Calendar.DATE);

        endDayBox.removeAllItems();
        for (int i = 0; i < days; i++)
            endDayBox.addItem("" + (i + 1));	
        // make sure we are not out of range for selected day of new month
        if (day > days)
            day = days;
        endLocalCalendar.set(Calendar.DATE, day);
        endDayBox.setSelectedItem("" + day);	
    }
    
    private void populateEmployeeBox(){
    	int i = 1;
        Profile root = UnitProfile.getInstance().getProfileInstance();
        Iterator<ProfileItem> iterator = root.iterator();
        Vector<Object> v = new Vector<Object>();
        v.add(0, noSelection);
        DefaultComboBoxModel cbm = new DefaultComboBoxModel(v);
        employeeBox.setModel(cbm); 
        while (iterator.hasNext()) {
            Object o = iterator.next();
            if (o instanceof Profile && 
               ((Profile) o).getTag().equalsIgnoreCase("USER")) {	
                    Profile userProfile = (Profile) o;
                    if (userProfile.getStatus().equalsIgnoreCase("A")){	
                        v.add(i,
                        Integer.valueOf(userProfile.getID()));
                        i++;
                    }
            }
        }
        sortEmpID(v);
        employeeBox.setSelectedIndex(0);
        employeeSelection = ReportSelectionCriteria.INT_NOT_SPECIFIED;
    }
  /*
   *Bubble sorting the empID  
   */   
    public void sortEmpID(Vector<Object> v){    	
    	try{
    	v.remove(0);
    	Object array[] = v.toArray();
    	for (int i=0; i<array.length-1; i++) {
    		  for (int j =0; j<array.length-1-i; j++)
    		    if (((Integer)array[j+1]).intValue() < ((Integer)array[j]).intValue()) { 
    		      int tmp = ((Integer)array[j]).intValue(); 
    		      array[j] = array[j+1];
    		      array[j+1] = Integer.valueOf(tmp);
    		  }
    		}
    	v.removeAllElements();  	
       for(int k = 0; k< array.length;k++){
    	   if(k==0){
    		  v.add(0, noSelection);   
    	   }  	   
    	   v.add(k+1, array[k]);
          }
       }catch(Exception e){}
  	
    }
    public void beginDayAction() {
        begLocalCalendar.set(Calendar.DATE, beginDayBox.getSelectedIndex() + 1);
    }

    public void beginMonthAction() {
        begLocalCalendar.set(Calendar.MONTH, beginMonthBox.getSelectedIndex());
        updateBegFields();
    }

    public void beginYearAction() {
        begLocalCalendar.set(Calendar.YEAR, Integer.parseInt((String) beginYearBox.getSelectedItem()));
        updateBegFields();
    }

    public void beginHourAction() {
        if ((beginHourBox.getSelectedIndex() + 1) == 12) {
            if  (beginAmPmBox.getSelectedIndex() == Calendar.AM)
                begLocalCalendar.set(Calendar.HOUR_OF_DAY, 0);
            else
                begLocalCalendar.set(Calendar.HOUR_OF_DAY, (beginHourBox.getSelectedIndex() + 1));
        }
        else
            begLocalCalendar.set(Calendar.HOUR_OF_DAY, ((beginHourBox.getSelectedIndex() + 1) + (beginAmPmBox.getSelectedIndex() * 12)));
    }
    
    public void beginMinuteAction() {
        begLocalCalendar.set(Calendar.MINUTE, beginMinuteBox.getSelectedIndex());
    }
    
    public void endDayAction() {
        endLocalCalendar.set(Calendar.DATE, endDayBox.getSelectedIndex() + 1);
    }

    public void endMonthAction() {
        endLocalCalendar.set(Calendar.MONTH, endMonthBox.getSelectedIndex());
        updateEndFields();
    }

    public void endYearAction() {
        endLocalCalendar.set(Calendar.YEAR, Integer.parseInt((String) endYearBox.getSelectedItem()));
        updateEndFields();
    }

    public void endHourAction() {
        if ((endHourBox.getSelectedIndex() + 1) == 12) {
            if  (endAmPmBox.getSelectedIndex() == Calendar.AM)
                endLocalCalendar.set(Calendar.HOUR_OF_DAY, 0);
            else
                endLocalCalendar.set(Calendar.HOUR_OF_DAY, (endHourBox.getSelectedIndex() + 1));
        }
        else
            endLocalCalendar.set(Calendar.HOUR_OF_DAY, ((endHourBox.getSelectedIndex() + 1) + (endAmPmBox.getSelectedIndex() * 12)));
    }
    
    public void endMinuteAction() {
        endLocalCalendar.set(Calendar.MINUTE, endMinuteBox.getSelectedIndex());
    }
    
    public void employeeAction() {
       if  (employeeBox.getSelectedItem().equals(" ")) {	
           employeeSelection = ReportSelectionCriteria.INT_NOT_SPECIFIED;
           employeeSelected.setForeground(Color.black);
       }    
       else {
           employeeSelection = ((Integer) employeeBox.getSelectedItem()).intValue();
           employeeSelected.setForeground(SystemColor.inactiveCaptionText);
       }    
    }
    
    public void serialRangeAction() {
        String bsn = beginSerialNumber.getText();
        String esn = endSerialNumber.getText();
        if (bsn.length() == 0  & esn.length() == 0)
              serialRangeSelected.setForeground(Color.black);
        else
              serialRangeSelected.setForeground(SystemColor.inactiveCaptionText);
    }
    
    public void amountRangeAction() {
        String begAmount = getBeginAmount().toString();
        String endAmount = getEndAmount().toString();
        if (begAmount.equals("-1") && endAmount.equals("-1"))	 
           amountRangeSelected.setForeground(Color.black);
        else
           amountRangeSelected.setForeground(SystemColor.inactiveCaptionText);
    }

    @Override
	public void processReport() {
        noError = editSelection();
        if (noError)
           setSelection();
    }
    
    public void setSelection() {
        rsc = new ReportSelectionCriteria();
        rsc.setShowDetail(isDetailOption());
        rsc.setSummaryDateTime(getSummaryDateTime());
        rsc.setStartDateTime(getBeginDateTime());
        rsc.setEndDateTime(getEndDateTime());
        rsc.setEmployeeNumber(getEmployeeNum());
        rsc.setStartSerial(getBeginSerialNum());
        rsc.setEndSerial(getEndSerialNum());
        if ((rsc.getStartSerial() == ReportSelectionCriteria.INT_NOT_SPECIFIED) &&
           (rsc.getEndSerial() != ReportSelectionCriteria.INT_NOT_SPECIFIED))
               rsc.setStartSerial(getEndSerialNum());
        if ((rsc.getStartSerial() != ReportSelectionCriteria.INT_NOT_SPECIFIED) &&
           (rsc.getEndSerial() == ReportSelectionCriteria.INT_NOT_SPECIFIED))
              rsc.setEndSerial(getBeginSerialNum());
        if ((getBeginAmount().compareTo(new BigDecimal(ReportSelectionCriteria.INT_NOT_SPECIFIED)) == 0) && 
            (getEndAmount().compareTo(new BigDecimal(ReportSelectionCriteria.INT_NOT_SPECIFIED)) == 0)) {
            rsc.setStartAmount(new BigDecimal(ReportSelectionCriteria.INT_NOT_SPECIFIED));
            rsc.setEndAmount(new BigDecimal(ReportSelectionCriteria.INT_NOT_SPECIFIED));
        }
        else if ((getBeginAmount().compareTo(new BigDecimal(ReportSelectionCriteria.INT_NOT_SPECIFIED)) != 0) && 
                 (getEndAmount().compareTo(new BigDecimal(ReportSelectionCriteria.INT_NOT_SPECIFIED)) == 0)) {
            rsc.setEndAmount(getBeginAmount());
            rsc.setStartAmount(getBeginAmount());
        }    
        else if ((getBeginAmount().compareTo(new BigDecimal(ReportSelectionCriteria.INT_NOT_SPECIFIED)) == 0) &&
                 (getEndAmount().compareTo(new BigDecimal(ReportSelectionCriteria.INT_NOT_SPECIFIED)) != 0)) {
            rsc.setEndAmount(getEndAmount());
            rsc.setStartAmount(BigDecimal.ZERO);
        }    
        else {
            rsc.setStartAmount(getBeginAmount());
            rsc.setEndAmount(getEndAmount());
        }
    }
    
    public boolean editSelection() {
        if (editEmployee){
            if  (employeeBox.getSelectedItem().equals(" ")) {	
                Dialogs.showWarning("dialogs/DialogInvalidEmployeeSelection.xml");	
                employeeBox.requestFocus();
                return false;
            }
        }
        if (editDate){
            long beginDateTime = getBeginDateTime();
            long endDateTime = getEndDateTime();
            DateFormat df = new SimpleDateFormat(TimeUtility.DATE_LONG_YEAR_MONTH_DAY_FORMAT);
            final String beginDate = df.format(new Date(beginDateTime));
            final String endDate = df.format(new Date(endDateTime));
            final String currentDate = df.format(new Date(TimeUtility.currentTimeMillis()));
            if (Integer.parseInt(beginDate) > Integer.parseInt(currentDate)) {
                Dialogs.showWarning("dialogs/DialogInvalidBeginDate.xml");	
                beginMonthBox.requestFocus();
                return false;
            }
            
            if (Integer.parseInt(endDate) > Integer.parseInt(currentDate)){
                //InvalidEndDateDialog idrd = new InvalidEndDateDialog(endMonthBox);
                //idrd.show();
                Dialogs.showWarning("dialogs/DialogInvalidEndDate.xml");	
                endMonthBox.requestFocus();
                return false;
            }
            
            if ((beginDateTime > endDateTime) & 
                    (beginDateTime != ReportSelectionCriteria.INT_NOT_SPECIFIED) &
                    (endDateTime != ReportSelectionCriteria.INT_NOT_SPECIFIED)){
                Dialogs.showWarning("dialogs/DialogInvalidDateRange.xml");	
                beginMonthBox.requestFocus();
                return false;
            }   
        }
        if (editSerial){
            long beginSerial = getBeginSerialNum();
            long endSerial = getEndSerialNum();
            if ((beginSerial > endSerial) & (endSerial != ReportSelectionCriteria.INT_NOT_SPECIFIED)) {  
                Dialogs.showWarning("dialogs/DialogInvalidSerialRange.xml");	
                beginSerialNumber.requestFocus();
                return false;
            }
        }
        if (editAmount){
            BigDecimal begAmount = getBeginAmount();
            BigDecimal endAmount = getEndAmount();
            if ((begAmount.compareTo(endAmount) == 1 ) &&
                 (begAmount.compareTo(new BigDecimal(ReportSelectionCriteria.INT_NOT_SPECIFIED))!= 0) &&
                 (endAmount.compareTo(new BigDecimal(ReportSelectionCriteria.INT_NOT_SPECIFIED))!= 0)) {
                
                Dialogs.showWarning("dialogs/DialogInvalidAmountRange.xml");	
                beginAmount.requestFocus();
                return false;
            }
        }
        return true;
    }    
}
