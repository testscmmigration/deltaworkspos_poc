package dw.dialogs;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;

import dw.framework.XmlDialogPanel;
import dw.io.report.ReportSelectionCriteria;
import dw.main.MainPanelDelegate;
import dw.printing.FilePrinter;
import dw.printing.Report;
import dw.printing.ScreenPrinter;
import dw.printing.TEPrinter;
import dw.utility.DWValues;

public class ReportDialog extends XmlDialogPanel {

    private final Runnable PAPER_PRINT_HANDLER = new Runnable() {
        @Override
		public void run() {
            final TEPrinter printer = TEPrinter.getPrinter();
            if (printer.isReady(getMainPanel()))
                report.print(printer,rsc);
        }
    };
    
    private final Runnable SCREEN_PRINT_HANDLER = new Runnable() {
        @Override
		public void run() {
        	boolean csvEnable = (detailCheckBox.isSelected() && csvOption);
            ReportOutputDialog outDialog = new ReportOutputDialog(report,
                                                 title, rsc, 
                                                 outputToFile, csvOption, csvEnable);
            int result = report.print(new ScreenPrinter(outDialog.getOutputArea()), rsc);
            if (result == 0)
                outDialog.show();
            //enableButtons(true);
        }
    };
    
    private final Runnable FILE_PRINT_HANDLER = new Runnable() {
        @Override
		public void run() {
            int result = report.print(new FilePrinter(outputFileName), rsc);
            if (result == 0)
                Dialogs.showInformation("dialogs/DialogOutputFile.xml");	
        }
    };
    
    private final Runnable CSV_FILE_HANDLER = new Runnable() {
        @Override
		public void run() {
        	report.print(csvFileName, rsc);
        }
    };
        
    protected final KeyAdapter ESCAPE_ADAPTER = new KeyAdapter() {
        @Override
		public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                e.consume();
                cancelAction();
            }
        }
    };
    
    protected Report report;
    protected JButton printButton, fileButton, previewButton, cancelButton, csvButton;
    protected JCheckBox detailCheckBox;
    protected ReportSelectionCriteria rsc;
    protected boolean noError;
    protected String outputFileName, csvFileName;
    private boolean hasPreview, hasCheckBox, outputToFile, csvOption;
    protected JButton buttonFocus;
    
    protected boolean isiSkipFirstCancelOnExit = false;
    protected boolean isiRecordTypeNon = false;
    
    public ReportDialog(String filename, Report report,
            final String title, final boolean hasPreview, 
            final boolean hasCheckBox, final boolean outputToFile,
			final boolean csvOption,
			final boolean csvEnable) {
        
        super(filename);
        isiSkipFirstCancelOnExit = false;
        isiRecordTypeNon = false;
        this.report = report;
        this.hasPreview = hasPreview;
        this.hasCheckBox = hasCheckBox;
        this.outputToFile = outputToFile;
        this.csvOption = csvOption;
        this.title = title;
        cancelButton = (JButton) mainPanel.getComponent("cancelButton");	
        if (hasCheckBox) {
            detailCheckBox = (JCheckBox) mainPanel.getComponent("detailCheckBox");	
        	mainPanel.addActionListener("detailCheckBox", this, "detailCheckBoxAction");
        }
        
        if (csvOption && !DWValues.isMGIOwnedPC()) {
            csvButton = (JButton) mainPanel.getComponent("csvButton");	
            mainPanel.addActionListener("csvButton", this, "csvAction"); 
            csvButton.setEnabled(csvEnable);
        }
        else
        	mainPanel.removeComponent("csvButton");
        
        if (outputToFile) {
            fileButton = (JButton) mainPanel.getComponent("fileButton");	
            mainPanel.addActionListener("fileButton", this, "fileAction");	 
            mainPanel.removeComponent("printButton");	
            outputFileName = report.getOutputFileName();
        }
        else {
            printButton = (JButton) mainPanel.getComponent("printButton");	
            mainPanel.addActionListener("printButton", this, "printAction");	 
            mainPanel.removeComponent("fileButton");	
        }
        if (hasPreview) {
            previewButton = (JButton) mainPanel.getComponent("previewButton");	
            mainPanel.addActionListener("previewButton", this, "previewAction");	 
        }
        mainPanel.addActionListener("cancelButton", this, "cancelAction");	 
    }
    
    private void enableButtons(final boolean enable) {
        cancelButton.setEnabled(enable);
        if (outputToFile)
            fileButton.setEnabled(enable);
        else
            printButton.setEnabled(enable);
        if (hasPreview)
            previewButton.setEnabled(enable);
        if (noError)
            buttonFocus.requestFocus();
    }
    
    public void detailCheckBoxAction() {
    	if (csvOption && !DWValues.isMGIOwnedPC())
    		csvButton.setEnabled(detailCheckBox.isSelected());
    }
    
    public void printAction() {
        buttonFocus = printButton;
        doPrint(PAPER_PRINT_HANDLER);
        isiRecordTypeNon = true;
    }
    
    public void previewAction() {
//    	if (csvOption)
//    		csvButton.setEnabled(detailCheckBox.isSelected());
        buttonFocus = previewButton;
        doPrint(SCREEN_PRINT_HANDLER);
        isiRecordTypeNon = true;
    }
    
    public void fileAction() {
        buttonFocus = fileButton;
        doPrint(FILE_PRINT_HANDLER);
        isiRecordTypeNon = true;
    }
    
    public void csvAction() {
        buttonFocus = csvButton;
        csvFileName = report.getCsvFileName();
        if (csvFileName != null)
        	doPrintCsv(CSV_FILE_HANDLER);
        //doPrintCsv();
        isiRecordTypeNon = true;
    }
    

    
    public boolean isDetailOption() {
        if (hasCheckBox)
            return detailCheckBox.isSelected(); 
        else
            return false;
    }
    
    public void cancelAction() {
        // This is a funny way to close a dialog
        //pane = getOptionPane();
        //pane.setValue(new Integer(JOptionPane.CANCEL_OPTION));
        closeDialog(JOptionPane.CANCEL_OPTION);
        
        if (isiSkipFirstCancelOnExit)
        	isiSkipFirstCancelOnExit = false;
        else{
        	if (isiRecordTypeNon){
        		MainPanelDelegate.setupNonFinancialISI();
        	} else {
        		MainPanelDelegate.setupCancelISI("01");
        	}
         	String programName = "";
    		if (MainPanelDelegate.getIsiNfProgramArguments() != null)
    			programName = MainPanelDelegate.getIsiNfProgramArguments().trim();
    		if (programName.length() > 0 && !(programName.startsWith("-NF"))) {
    			MainPanelDelegate.executeIsiProgram(programName);
    		}
        	MainPanelDelegate.setIsiNfCommand(false);
        	MainPanelDelegate.setIsiNfProgramArguments("");
        }

    }
    private void doPrintCsv(final Runnable printHandler) {
        enableButtons(false);
        noError = true;
        processCsvReport();
        if (noError){
            new Thread(printHandler).start();
        }
    	//int result = report.print(new CsvFilePrinter(csvFileName), rsc);
    	enableButtons(true);
    }
    
    private void doPrint(final Runnable printHandler) {
        //SwingUtilities.invokeLater(enableButtons(false));
        enableButtons(false);
        noError = true;
        processReport();
 
        if (noError){
            new Thread(printHandler).start();
        }

//        SwingUtilities.invokeLater(enableButtons(true));
        enableButtons(true);
    }

    public void processReport() {
    }
    
    public void processCsvReport() {
    }
}
