package dw.dialogs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import dw.comm.MessageFacade;
import dw.comm.MessageFacadeParam;
import dw.framework.WaitToken;
import dw.framework.XmlDialogPanel;
import dw.gui.ImageIconCache;
import dw.i18n.Messages;
import dw.main.DeltaworksMainPanel;
import dw.profile.UnitProfile;
import dw.ras.RasListener;
import dw.utility.Debug;
import dw.utility.ExtraDebug;

/**
 * CommunicationDialog is used by the MessageFacade Class when a transaction is
 * in the process of connecting to the host for information.
 * 
 * @author Patrick Ottman
 */

public class CommunicationDialog extends XmlDialogPanel implements RasListener {

	public final static int RETRY = 1;
	public final static int CANCEL = 2;
	private final static int MINIMUM_HEIGHT = 150;
	private final static int MAXIMUM_HEIGHT = 400;
	
	public final static int ENABLE_RETRY_BUTTON = 1;
	public final static int ENABLE_CANCEL_BUTTON = 2;
	
	private static CommunicationDialog instance = null;

	private JButton cancelButton;
	private JButton retryButton;
	private JPanel errorPanel;
	private JLabel iconLabel;
	private JTextArea messageLabel;
	private JLabel errorLabel;
	private JLabel errorCount;
	private JScrollPane scrollpane;
	private boolean closeDialogFlag;
    private WaitToken retryLock = new WaitToken();

	static {
		instance = new CommunicationDialog();
	}

	private CommunicationDialog() {
		super("dialogs/DialogCommunication.xml"); 
		setup();
		scrollpane.setVisible(false);
		messageLabel.setVisible(false);
		errorLabel.setVisible(false);
		errorCount.setVisible(false);
	}

	private void setup() {
		mainPanel.addActionListener("cancelButton", this, "cancel");  
		mainPanel.addActionListener("retryButton", this, "retry");  

		scrollpane = (JScrollPane) mainPanel.getComponent("scrollpane");
		cancelButton = (JButton) mainPanel.getComponent("cancelButton"); 
		retryButton = (JButton) mainPanel.getComponent("retryButton"); 
		errorPanel = (JPanel) mainPanel.getComponent("errorPanel");
		errorPanel.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));
		messageLabel = (JTextArea) mainPanel.getComponent("messageLabel");
		messageLabel.setEditable(false);
		messageLabel.setBackground(mainPanel.getBackground());
		iconLabel = (JLabel) mainPanel.getComponent("iconLabel");

		String iconFileName = (UnitProfile.getInstance().isDialUpMode()) ? "xml/images/modem.gif" :  "xml/images/network.gif"; 
		ImageIcon icon = ImageIconCache.getImageIcon(iconFileName);
		iconLabel.setIcon(icon);
		
		errorCount = (JLabel) mainPanel.getComponent("errorCount");
		errorLabel = (JLabel) mainPanel.getComponent("errorLabel");
	}

	/**
	 * Sets the dialog title. For the sake of consistency, this should almost
	 * never be called. The only valid use of this method is to set the
	 * scheduled transmit and daily transmit titles from DMP.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/** for compatibility */
	public void setEnabled(boolean enabled) {
		mainPanel.setEnabled(enabled);
	}

	/**
	 * Returns false to tell Dialogs.java not to log the title every time this
	 * dialog is shown.
	 */
	@Override
	public boolean showTitleInDebug() {
		return false;
	}

	/**
	 * Returns the singleton instance.
	 */
	public static CommunicationDialog getInstance() {
		return instance;
	}

	/**
	 * refresh the dialog and create a new instance when the toggle button is
	 * selected.
	 */
	public static void refreshDialog() {
		instance = new CommunicationDialog();
	}

	/**
	 * Refresh ICON file connection mode has changed.
	 */
	public void refreshIcon() {
		String iconFileName = (UnitProfile.getInstance().isDialUpMode()) ? "xml/images/modem.gif" : "xml/images/network.gif"; 
		final ImageIcon icon = ImageIconCache.getImageIcon(iconFileName);

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				iconLabel.setIcon(icon);
			}
		});
	}

	public void setErrorMessages(final String message1, final List<String> errorList, boolean closeDialogFlag) {
		errorPanel.removeAll();
		errorCount.setText(String.valueOf(errorList.size()));
		errorLabel.setVisible(true);
		errorCount.setVisible(true);
		scrollpane.setVisible(true);
		iconLabel.setVisible(true);

		if (messageLabel != null) {
			messageLabel.setText(message1);
			messageLabel.setVisible(true);
		}
		
		for (int i = 0; i < errorList.size(); i++) {
			populateError(errorList.get(i), (i + 1));
		}

		scrollpane.invalidate();
		dialog.pack();
		dialog.repaint();

		Thread task = new Thread() {
			@Override
			public void run() {
				if (errorPanel.getSize().height > errorPanel.getPreferredSize().height) {
					errorPanel.setPreferredSize(errorPanel.getSize());
				}

				dialog.pack();

				Dimension dialogSize = dialog.getSize();
				if (dialogSize.height < CommunicationDialog.MINIMUM_HEIGHT) {
					Dimension size = new Dimension(dialogSize.width, CommunicationDialog.MINIMUM_HEIGHT);
					dialog.setSize(size);
				} else if (dialogSize.height > CommunicationDialog.MAXIMUM_HEIGHT) {
					scrollpane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
					scrollpane.getViewport().validate();
					Dimension size = new Dimension(dialogSize.width, CommunicationDialog.MAXIMUM_HEIGHT);
					dialog.setSize(size);
				} else {
					scrollpane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
					dialog.setSize(dialogSize);
				}
			}
		};
		SwingUtilities.invokeLater(task);
//		task.start();

		this.closeDialogFlag = closeDialogFlag;
	}

	public void setMessage(final String text, final int buttonOptions) {
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				retryButton.setEnabled((buttonOptions & ENABLE_RETRY_BUTTON) != 0);

				iconLabel.setVisible(true);
				mainPanel.repaint();
				messageLabel.setText(text);
				messageLabel.setVisible(true);
				errorLabel.setVisible(false);
				errorCount.setVisible(false);
				scrollpane.setVisible(false);
			}
		});
	}

	/**
	 * Action method called by the cancel button. Tell the activator we don't
	 * need the connection anymore and hide the dialog.
	 */
	public void cancel() {
		MessageFacade.getInstance().cancelConnection();
		
		if (closeDialogFlag) {
			closeDialog();
		} else {
			if (retryLock != null) {
		        synchronized (retryLock) {
		        	retryLock.setWaitFlag(false);
		        	retryLock.notifyAll();
		        }
			}
		}
	}

	public void retry() {
		MessageFacade.getInstance().retryConnection();
	}

	public void setCancelButtonProperty(boolean enabled) {
		if (cancelButton != null) {
			cancelButton.setFocusable(false);
		}
	}

	public void setRetryButtonProperty(boolean enabled) {
		if (enabled) {
			cancelButton.setFocusable(true);
		}
		retryButton.setEnabled(enabled);
	}

	/**
	 * Called to set focus to one of three button on the dialog box.
	 */
	public void setFocusTo(int focusTo) {
		// in an attempt to fix focus,
		// the button components will be reconstructed.
		mainPanel.removeActionListeners(cancelButton);
		mainPanel.removeActionListeners(retryButton);

		mainPanel.addActionListener("cancelButton", this, "cancel");  
		mainPanel.addActionListener("retryButton", this, "retry");  

		if (DeltaworksMainPanel.getFocusOwner() != null) {
			
			switch (focusTo) {
			case RETRY:
				if (retryButton.isVisible() && retryButton.isEnabled()) {
					retryButton.requestFocus();
				}
				break;
			case CANCEL:
				if (cancelButton.isVisible() && cancelButton.isEnabled()) {
					cancelButton.requestFocus();
				}
				break;
			}
		}
	}

	/** Close the dialog immediately. */
	@Override
	public void closeDialog(int option) {
		resetDialog();
		super.closeDialog(option);
	}

	/**
	 * Override superclass cleanup method to do nothing. Called by Dialogs.java
	 * when enclosing JDialog is disposed. Since this singleton instance is
	 * reused, we don't do any cleanup of the XmlDefinedPanel.
	 */
	@Override
	public void cleanup() {
		// Check if [X] pressed on the dialog
		if (getOptionPane() != null) {
			JOptionPane pane = getOptionPane();
			Object value = pane.getValue();
			if (value == null) {
				if (cancelButton.isVisible() && cancelButton.isEnabled()) {
					MessageFacade.getInstance().cancelConnection();
				}
			}
		}
		resetDialog();
	}

	private void resetDialog() {
		messageLabel.setText("");
		errorLabel.setVisible(false);
		errorCount.setVisible(false);
		errorPanel.removeAll();
		setTitle(Dialogs.TITLE);
	}

	/**
	 * Device connected event callback method.
	 */
	@Override
	public void deviceConnected(final int rasErrorCode) {
		println("deviceConnected ", rasErrorCode); 
	}

	/**
	 * Open port event callback method.
	 */
	@Override
	public void openPort(final int rasErrorCode) {
		String text = Messages.getString("CommunicationDialog.1862");
		ExtraDebug.println("-x-setStatus[" + text + "]"); 
		setMessage(text, 0); 
		println("openPort ", rasErrorCode); 
	}

	/**
	 * Port opened event callback method.
	 */
	@Override
	public void portOpened(final int rasErrorCode) {
		println("portOpened ", rasErrorCode); 
	}

	/**
	 * Connect device event callback method.
	 */
	@Override
	public void connectDevice(final int rasErrorCode) {
		println("connectDevice ", rasErrorCode); 
	}

	/**
	 * All devices connected event callback method.
	 */
	@Override
	public void allDevicesConnected(final int rasErrorCode) {
		println("allDevicesConnected ", rasErrorCode); 
	}

	/**
	 * Authenticate event callback method.
	 */
	@Override
	public void authenticate(final int rasErrorCode) {
		String text = Messages.getString("CommunicationDialog.1863");
		ExtraDebug.println("-x-setStatus[" + text + "]"); 
		setMessage(text, 0); 
		println("authenticate ", rasErrorCode); 
	}

	/**
	 * Authenticate notify event callback method.
	 */
	@Override
	public void authNotify(final int rasErrorCode) {
		println("authNotify ", rasErrorCode); 
	}

	/**
	 * Authenticate retry event callback method.
	 */
	@Override
	public void authRetry(final int rasErrorCode) {
		println("authRetry ", rasErrorCode); 
	}

	/**
	 * Authenticate callback event callback method.
	 */
	@Override
	public void authCallback(final int rasErrorCode) {
		println("authCallback ", rasErrorCode); 
	}

	/**
	 * Authenticate change password event callback method.
	 */
	@Override
	public void authChangePassword(final int rasErrorCode) {
		println("authChangePassword ", rasErrorCode); 
	}

	/**
	 * Authenticate project event callback method.
	 */
	@Override
	public void authProject(final int rasErrorCode) {
		println("authProject ", rasErrorCode); 
	}

	/**
	 * Authenticate link speed event callback method.
	 */
	@Override
	public void authLinkSpeed(final int rasErrorCode) {
		println("authLinkSpeed ", rasErrorCode); 
	}

	/**
	 * Authentication acknowledgement event callback method.
	 */
	@Override
	public void authAck(final int rasErrorCode) {
		println("authAck ", rasErrorCode); 
	}

	/**
	 * Re-authenticated event callback method.
	 */
	@Override
	public void reAuthenticate(final int rasErrorCode) {
		println("reAuthenticate ", rasErrorCode); 
	}

	/**
	 * Authenticated event callback method.
	 */
	@Override
	public void authenticated(final int rasErrorCode) {
		println("authenticated ", rasErrorCode); 
	}

	/**
	 * Prepare for callback event callback method.
	 */
	@Override
	public void prepareForCallback(final int rasErrorCode) {
		println("prepareForCallback ", rasErrorCode); 
	}

	/**
	 * Wait for modem reset event callback method.
	 */
	@Override
	public void waitForModemReset(final int rasErrorCode) {
		println("waitForModemReset ", rasErrorCode); 
	}

	/**
	 * Wait for callback event callback method.
	 */
	@Override
	public void waitForCallback(final int rasErrorCode) {
		println("waitForCallback ", rasErrorCode); 
	}

	/**
	 * Projected event callback method.
	 */
	@Override
	public void projected(final int rasErrorCode) {
		println("projected ", rasErrorCode); 
	}

	/**
	 * Subentry connected event callback method.
	 */
	@Override
	public void subentryConnected(final int rasErrorCode) {
		println("subentryConnected ", rasErrorCode); 
	}

	/**
	 * Subentry disconnected event callback method.
	 */
	@Override
	public void subentryDisconnected(final int rasErrorCode) {
		println("subentryDisconnected ", rasErrorCode); 
	}

	/**
	 * Interactive (paused) event callback method.
	 */
	@Override
	public void interactive(final int rasErrorCode) {
		println("interactive ", rasErrorCode); 
	}

	/**
	 * Retry authentication event callback method.
	 */
	@Override
	public void retryAuthentication(final int rasErrorCode) {
		println("retryAuthentication ", rasErrorCode); 
	}

	/**
	 * Callback set by caller event callback method.
	 */
	@Override
	public void callbackSetByCaller(final int rasErrorCode) {
		println("callbackSetByCaller ", rasErrorCode); 
	}

	/**
	 * Password expired event callback method.
	 */
	@Override
	public void passwordExpired(final int rasErrorCode) {
		println("passwordExpired ", rasErrorCode); 
	}

	/**
	 * Invoke EAP UI event callback method.
	 */
	@Override
	public void invokeEAPUI(final int rasErrorCode) {
		println("invokeEAPUI ", rasErrorCode); 
	}

	/**
	 * Connected event callback method.
	 */
	@Override
	public void connected(final int rasErrorCode) {
		String text = Messages.getString("CommunicationDialog.1864");
		ExtraDebug.println("-x-setStatus[" + text + "]"); 
		setMessage(text, 0); 
		Debug.println("connected " + rasErrorCode); 
	}

	/**
	 * Disconnected event callback method.
	 */
	@Override
	public void disconnected(final int rasErrorCode) {
		println("disconnected ", rasErrorCode); 
	}

	/**
	 * States that the entire dialing sequence failed.
	 */
	@Override
	public void attemptSequenceFailed() {
		Debug.println("attemptSequenceFailed "); 
	}

	/** Debug for callbacks. Print the callback and status only if error. */
	private void println(String callback, int code) {
		if (code != 0) {
			Debug.println(callback + code);
		}
	}

	private void populateError(String errorMsg, int index) {
		JPanel horizontalPanel = new JPanel();
		horizontalPanel.setLayout(new GridBagLayout());
		horizontalPanel.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));

		JLabel idxLabel = new JLabel();
		idxLabel.setText(String.valueOf(index));
		idxLabel.setBackground(SystemColor.control);
		idxLabel.setOpaque(true);
		idxLabel.setPreferredSize(new Dimension(30, 20));
		idxLabel.setHorizontalAlignment(SwingConstants.CENTER);
		idxLabel.setForeground(Color.red);
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.anchor = GridBagConstraints.NORTH;
		horizontalPanel.add(idxLabel, gbc);

		JTextArea jta = new JTextArea();
		jta.setLineWrap(true);
		jta.setWrapStyleWord(true);
		jta.setFocusable(false);
		jta.setBackground(SystemColor.control);
		jta.setText(errorMsg);
		
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.weightx = 1.0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		horizontalPanel.add(jta, gbc);

		gbc.gridx = 0;
		gbc.gridy = index;
		gbc.weightx = 1.0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		errorPanel.add(horizontalPanel, gbc);
		gbc.insets = new Insets(5, 5, 5, 5);
	}

	public void showErrors(List<String> errors, boolean showRetry) {
		try {
			setEnabled(false);
			setCancelButtonProperty(true);
			setFocusTo(CANCEL);
			setRetryButtonProperty(showRetry);
			if (errors != null && errors.size() > 0) {
				setErrorMessages(null, errors, true);
			}
			setEnabled(true);
			dialog.setVisible(true);
			this.errorPanel.setVisible(true);

			show();
		} catch (Exception e) {
			Debug.printException(e);
		}
	}
	
	@Override
	public void setDialog(JDialog dialog) {
		super.setDialog(dialog);
		this.cancelButton.setEnabled(true);
		this.retryButton.setEnabled(false);
		if (dialog != null) {
			dialog.setResizable(false);
			Dimension dialogSize = dialog.getSize();
			if (dialogSize.height < CommunicationDialog.MINIMUM_HEIGHT) {
				Dimension size = new Dimension(dialogSize.width, CommunicationDialog.MINIMUM_HEIGHT);
				dialog.setSize(size);
			}
			
			dialog.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosed(WindowEvent e) {
					if (retryLock != null) {
				        synchronized (retryLock) {
				    		MessageFacade.getInstance().cancelConnection();
				        	retryLock.setWaitFlag(false);
				        	retryLock.notifyAll();
				        }
					}
				}
			});
		}
	}
	
    /**
     * Configure the dialog to show an error message with Cancel buttons.
     */
    public void displayDialog(List<String> errors) {
    	try {
	    	MessageFacadeParam parameters = new MessageFacadeParam(MessageFacadeParam.REAL_TIME);
	    	MessageFacade.getInstance().ensureConnection(parameters);
	        setEnabled(false);
	        setCancelButtonProperty(true);
	        setFocusTo(CommunicationDialog.CANCEL);
	        setRetryButtonProperty(false);
	      
			if (errors != null && errors.size() > 0) {
		         setErrorMessages("", errors, true);
			}
	
	        retryLock.setWaitFlag(true);
	
	        synchronized (retryLock) {
	            try {
	                setEnabled(true);
	                while (retryLock.isWaitFlag()) {
	                	retryLock.wait();
	                }
	            } catch (InterruptedException ie) {
	            }
	        }
    	} catch (Exception e) {
    		Debug.printException(e);
    	}
    }
}
