package dw.dialogs;

import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import dw.dwgui.DWPasswordField;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.XmlDefinedPanel;
import dw.framework.XmlDialogPanel;
import dw.i18n.Messages;

public class MfaHardTokenDialog extends XmlDialogPanel implements DWTextPaneListener {
	
	private DWPasswordField passwordField = null;
	private boolean canceled = true;
	private JLabel headerLabel;
		
	public MfaHardTokenDialog(boolean firstFlag, boolean regOnly) {
		if(regOnly){
			this.title = (Messages.getString("DialogMfaHardToken.Title1"));
		}
		else{
			this.title = (Messages.getString("DialogMfaHardToken.Title"));
		}

		String xmlFile = null;
		xmlFile = "dialogs/DialogMfaHardToken.xml"; 

		mainPanel = new XmlDefinedPanel(xmlFile);
		
		headerLabel = (JLabel) mainPanel.getComponent("headerLabel");
		passwordField = (DWPasswordField) mainPanel.getComponent("passwordField");
		passwordField.setHandleEnter();
		
		DWTextPane message = (DWTextPane) mainPanel.getComponent("message");
		message.addListener(mainPanel, this);
		
		String label;		
		if(regOnly){
			label = (Messages.getString("DialogMfaHardToken.1a"));
		}
		else{
			label = (Messages.getString("DialogMfaHardToken.1"));
		}
		headerLabel.setText(label);
				
		String text;
		if(regOnly){				
			if (firstFlag) {
				text = Messages.getString("DialogMfaHardToken.4");
			} else {
				text = Messages.getString("DialogMfaHardToken.5");
			}
			message.setText(text);		
		}
		else{
			if (firstFlag) {
				text = Messages.getString("DialogMfaHardToken.2");
			} else {
				text = Messages.getString("DialogMfaHardToken.3");
			}
			message.setText(text);
		}	
		
		passwordField.requestFocus();
		passwordField.setPreferredSize(new Dimension(1, 1));
		passwordField.setBackground(mainPanel.getBackground());
		passwordField.setCaretColor(mainPanel.getBackground());	
		passwordField.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

		mainPanel.addActionListener("cancelButton", this); 
		final KeyListener escaper = new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					okButtonAction();
				}
			}
		};
		final KeyListener activator = new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					okButtonAction();
				}
			}
		};
		mainPanel.addKeyListener("passwordField", escaper); 
		mainPanel.addKeyListener("passwordField", activator); 
		
		int w = passwordField.getFontMetrics(passwordField.getFont()).charWidth('*') * 44;
		passwordField.setPreferredSize(new Dimension(w, 23));
	}		
		
	@Override
	public void dwTextPaneResized() {
		DWTextPane message = (DWTextPane) mainPanel.getComponent("message");
		message.setWidth(mainPanel.getPreferredSize().width);
		message.setListenerEnabled(false);
	}
	
	public void cancelButtonAction() {
		closeDialog(Dialogs.CANCEL_OPTION);
	}

	public String getToken() {
		return String.valueOf(passwordField.getPassword());
	}

	public boolean isCanceled() {
		return canceled;
	}

	public void okButtonAction() {
		canceled = false;
		closeDialog(Dialogs.OK_OPTION);
	}

	public void setPassword(String password) {
		passwordField.setText(password);
	}

	/**
	 * Returns false to indicate this dialog's title bar text should not be
	 * logged in the trace file.
	 */
	@Override
	public boolean showTitleInDebug() {
		return false;
	}
}
