package dw.dialogs;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.xml.datatype.XMLGregorianCalendar;

import com.moneygram.agentconnect.ComplianceTransactionRequest;
import com.moneygram.agentconnect.ComplianceTransactionRequestType;
import com.moneygram.agentconnect.CustomerComplianceInfo;
import com.moneygram.agentconnect.CustomerComplianceTypeCodeType;
import com.moneygram.agentconnect.LegalIdType;
import com.moneygram.agentconnect.MoneyOrderInfo;
import com.moneygram.agentconnect.PhotoIdType;

import dw.dwgui.MoneyField;
import dw.dwgui.MultiListComboBox;
import dw.dwgui.StateListComboBox;
import dw.framework.XmlDialogPanel;
import dw.i18n.Messages;
import dw.io.CCILog;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.CountryInfo.Country;
import dw.model.adapters.CountryInfo.CountrySubdivision;
import dw.profile.UnitProfile;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.TimeUtility;

/**
 * Prompts for fields on the Suspicious Activity Report (SAR),
 * Currency Transaction Report (CTR), and Federal Record Keeping (RKL).
 * Intended usage is for the calling transaction to decide what do to
 * with the entered data when the user clicks Save. This dialog can
 * also be configured to automatically save data to the CCI log.
 */
public class ComplianceDialog extends XmlDialogPanel {
    
    /** Controls whether the Save button automatically writes to CCI log. */
    private boolean autosave = false;

	/** Available XML filenames; in sequence matching the CCI type code constants above. */
	
	private static String getDialogXmlFile(CustomerComplianceTypeCodeType code) {
		switch (code) {
			case RKL:
				return "dialogs/DialogComplianceRKL.xml"; 
			case CTR:
				return "dialogs/DialogComplianceCTR.xml";
			case SAR:
				return "dialogs/DialogComplianceSAR.xml";
			default:
				return "";
		}
	}
	
	/** Holds the data entered by the user. */	
	private ComplianceTransactionRequest ctriModified;

    // Do not include dropdown lists there functionality becomes jumpy.
    private static final String commonTabFields[] = 
             {"firstNameField", "middleInitialField", "addressField", "address2Field",    
              "cityField", /*"countryList","stateList",*/ "zipField", "dobField",    
              "photoIDTypeList", "photoIDNumberField",/* "photoIDStateList",*/   
              /*"photoIDCountryList",*/ "phoneNumberField","occupationField",   
              "legalIDTypeList", "legalIDNumberField", "thirdLastNameField",   
              "thirdFirstNameField", "thirdMiddleInitialField", "thirdDBAField",    
              "thirdAddressField", "thirdAddress2Field", "thirdCityField",/* "thirdStateList",*/ "thirdZipField",     
              /*"thirdCountryList",*/ "thirdDOBField", "thirdPhoneNumberField",   
              "thirdOccupationField", /*"thirdLegalIDTypeList",*/ "thirdLegalIDNumberField"};  


    private static final String tabFieldsSAR[] = 
             {"category1", "category2", "category3", "category4", "character1",     
              "character2", "character3", "lastNameField" };   
 
    private static final String tabFieldsCTR[] =  {"employeeNumberField","transactionAmountField","commentsField", "lastNameField" };  
      
    
    private static final String requiredFieldsSAR[] = {"employeeNumberField",}; 
    private static final String requiredTextAreaSAR[] = {"suspiciousTextArea"}; 
    private static final String requiredFieldsRKL[] = 
             {"lastNameField", "firstNameField", "addressField", "cityField",     
              "zipField", "photoIDNumberField", "dobField", "phoneNumberField",     
              "occupationField", "legalIDNumberField"};  
    
    private static final String requiredFieldsCTR[] = 
             {"employeeNumberField","lastNameField", "firstNameField",   
              "addressField", "cityField", "zipField", "countryList", "photoIDTypeList", "photoIDNumberField",      
              "photoIDCountryList", "dobField", "phoneNumberField", "occupationField", "legalIDTypeList", "legalIDNumberField"};      

    // List of fields that will be validated as date type fields
    private static final String dobFields[] = {"dobField", "thirdDOBField"};  


    private static final String REQUIRED_TEXT = Messages.getString("ComplianceDialog.1865"); 
    private boolean saved = false;
    
    private JScrollPane scrollpane;
    private JViewport viewport;
    private JComponent scrollee;
    protected JButton saveButton, cancelButton;
    
    // Person filling out the form 
    private JTextField employeeNumberField;
    private MoneyField transactionAmountField;
    private JTextArea commentsField;
    private JTextArea suspiciousTextArea;
    private JCheckBox category1;
    private JCheckBox category2;
    private JCheckBox category3;
    private JCheckBox category4;
    private JTextField categoryOther;
    private JCheckBox character1;
    private JCheckBox character2;
    private JCheckBox character3;
    private JCheckBox check1;
    private JLabel check1b;
    private JCheckBox check2;
    private JCheckBox check3;
    private JCheckBox check4;
    private JCheckBox check5;
    private JCheckBox check6;
    private JCheckBox check7;
    private JCheckBox check8;
    private JLabel check8b;
    private JCheckBox check9;

    // Customer conducting transaction
    private JTextField lastNameField;
    private JTextField firstNameField;
    private JTextField middleInitialField;
    private JTextField addressField;
    private JTextField address2Field;
    private JTextField cityField;
    private StateListComboBox<CountrySubdivision> stateList;
    private JTextField zipField;
    private MultiListComboBox<Country> countryList;
    private JTextField dobField;
    private MultiListComboBox<String> photoIDTypeList;
    private JTextField photoIDNumberField;
    private StateListComboBox<CountrySubdivision> photoIDStateList;
    private MultiListComboBox<Country> photoIDCountryList;
    private JTextField phoneNumberField;
    private JTextField occupationField;
    private MultiListComboBox<String> legalIDTypeList;
    private JTextField legalIDNumberField;
  
    // Third Party 
    private JTextField thirdLastNameField;
    private JTextField thirdFirstNameField;
    private JTextField thirdMiddleInitialField;
    private JTextField thirdDBAField;
    private JTextField thirdAddressField;
    private JTextField thirdAddress2Field;
    private JTextField thirdCityField;
    private StateListComboBox<CountrySubdivision> thirdStateList;
    private JTextField thirdZipField;
    private MultiListComboBox<Country> thirdCountryList;
    private JTextField thirdDOBField;
    private JTextField thirdPhoneNumberField;
    private JTextField thirdOccupationField;
    private MultiListComboBox<String> thirdLegalIDTypeList;
    private JTextField thirdLegalIDNumberField;
    
    /** 
     * Constructs specified type of dialog with auto-save behavior.
     * A new CTRI will be created with the standalone request type. 
     */
    public ComplianceDialog(CustomerComplianceTypeCodeType typeCode) {
    	this(getDialogXmlFile(typeCode), new ComplianceTransactionRequest(), true);
    	this.ctriModified.setRequestType(ComplianceTransactionRequestType.STANDALONE);
    	CustomerComplianceInfo cci = new CustomerComplianceInfo();
    	this.ctriModified.setCci(cci);
        cci.setTypeCode(typeCode);
        this.ctriModified.setMgReferenceNumber("");
    }
    
    /** 
     * Constructs dialog with optional auto-save behavior. The supplied CTRI must 
     * have a CCI. Initial values and type are specified in the CCI. Entered data 
     * will be stored in a copy of the CTRI, which can be retrieved with a call
     * to getModifiedCTRI(). The supplied CTRI and CCI will not be modified.
     */
    public ComplianceDialog(ComplianceTransactionRequest ctri, boolean autoSave) {
    	this(getDialogXmlFile(ctri.getCci().getTypeCode()), cloneCtri(ctri), autoSave);
    }

	/**
	 * Constructs dialog. The other dialogs feed into this one. The supplied CTRI
	 * will be modified, so if a clone is needed it must be performed by the caller. 
	 * If autoSave is true, the CTRI request type will be set to STANDALONE.
	 */
	private ComplianceDialog(String filename, ComplianceTransactionRequest ctri, boolean autosave) {
		super(filename);
		ctriModified = ctri;
		this.autosave = autosave;
		if (autosave) {
			ctriModified.setRequestType(ComplianceTransactionRequestType.STANDALONE);
		}
		
		this.title = Messages.getString("DeltaworksMainPanel.title");
		initializeCommon();
		setupListenersCommon();
		scrollpane = (JScrollPane) mainPanel.getComponent("scrollpane");	
		scrollpane.getVerticalScrollBar().setUnitIncrement(27);
		scrollpane.getVerticalScrollBar().setFocusable(false);
		scrollpane.setViewportBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
		viewport = scrollpane.getViewport();
		scrollee = (JComponent) mainPanel.getComponent("scrollee");	
		setupAutoscroll(commonTabFields);		

		switch (getModifiedCCI().getTypeCode()) {
			case CTR:
				initializeCTR();               
				setupAutoscroll(tabFieldsCTR);
                setupListenersCTR();
				break;
			case SAR:
				initializeSAR();
				setupListenersSAR();
				setupAutoscroll(tabFieldsSAR);
				break;
			default:
				Debug.println("ComplianceDialog - Unsupported CCI type code" + getModifiedCCI().getTypeCode());
				break;            
		}		
		saveButton = (JButton) mainPanel.getComponent("saveButton");	
		cancelButton = (JButton) mainPanel.getComponent("cancelButton");	
        
		mainPanel.addActionListener(saveButton, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				saveButton.setEnabled(false);
				saveAction();
				saveButton.setEnabled(true);
			}
		});
        
		mainPanel.addActionListener("cancelButton", this, "cancelAction");	 
	}

    /**
     * Returns the Customer Compliance Information after 
     * the data is entered from the RKL, CTR, or SAR dialog.
     */
    public CustomerComplianceInfo getModifiedCCI() {
        return ctriModified.getCci();          
    }

    /**
     * Called when the cancel button for the RKL, CTR, or SAR dialog
     * is clicked. Just closes the dialog.
     */       
    public void cancelAction() {
        closeDialog(JOptionPane.CANCEL_OPTION);
    }
      
    /**
     * Called when the save button for the RKL, CTR, or SAR dialog
     * is clicked.  Field values are copied into the modified CCI.
     * If autosave is enabled, the modified CTRI is written.
     */       
    public void saveAction(){
        boolean valid = false;
        switch (getModifiedCCI().getTypeCode()) {
            case RKL:
                if (validateFields(requiredFieldsRKL, null, dobFields)) {
                	valid = true;
	                updateCommon();
                }
                break;
            case CTR:
                if (validateFields(requiredFieldsCTR, null, dobFields)) {
                    String amount = transactionAmountField.getText();
                    if((amount.equals("0.00"))||(amount.length() > 8)){  
                        Toolkit.getDefaultToolkit().beep();
                        transactionAmountField.requestFocus();
                        return;
                    }
                                              
                    commentsField.insert(amount+". ",0);   
                    valid = true;
					updateCommon();
                    updateCTR();
                }
                
                break;
            case SAR:
                if (validateFields(requiredFieldsSAR, requiredTextAreaSAR, dobFields)) {
                	valid = true;
					updateCommon();
                    updateSAR();
                }
                break;
                
			default:
				Debug.println("saveAction - Unsupported CCI type code" + getModifiedCCI().getTypeCode());
				break;            
        }
        if (valid) {
			if (autosave) {
				writeCTRI();
			}
           // Debug.println(getModifiedCCI().toString());
			saved = true;
			closeDialog();
		}
    }
    
    /**
     * Returns true if the dialog was closed by clicking Save.
     * Has nothing to do with autosave.
     */
    public boolean isSaved(){
        return this.saved;
    } 

    /*
     * validateFields - is used to validate three types of fields. 
     *  The first validation is checking if the JTextFields has data.
     *  The second validation is checking if the JTextArea has data.
     *  The third validation is checking if the field is a date 
     *  and the length of the field is six digits (MMDDYY).
     */
    public boolean validateFields(final String requiredFields[], String requiredTextAreas[],String dobFields[]) {
        for (int i = 0; i < requiredFields.length; i++) {
            final int j = i;
            if(mainPanel.getComponent(requiredFields[j]) instanceof JTextField ){
                if(((JTextField) mainPanel.getComponent(requiredFields[j])).getText().trim().equals("") ||	
                   ((JTextField) mainPanel.getComponent(requiredFields[j])).getText().trim().equals(REQUIRED_TEXT)) {
                        ((JTextField) mainPanel.getComponent(requiredFields[j])).setText(REQUIRED_TEXT);
                        ((JTextField) mainPanel.getComponent(requiredFields[j])).selectAll();
                        Toolkit.getDefaultToolkit().beep();
                        SwingUtilities.invokeLater(new Runnable() {@Override
						public void run() {
                            ((JTextField) mainPanel.getComponent(requiredFields[j])).requestFocus();
                        }});
                        return false;
                   }               
            }
            else if (mainPanel.getComponent(requiredFields[j]) instanceof MultiListComboBox){
                if (((((MultiListComboBox<? extends Object>) mainPanel.getComponent(requiredFields[j])).getSelectedItem() == null)) ||
                    ((((MultiListComboBox<? extends Object>) mainPanel.getComponent(requiredFields[j])).getSelectedItem().toString().trim().equals("")))){	
                     Toolkit.getDefaultToolkit().beep();
                     SwingUtilities.invokeLater(new Runnable() {@Override
					public void run() {
                         ((MultiListComboBox<? extends Object>) mainPanel.getComponent(requiredFields[j])).requestFocus();
                     }});
                     return false;
                 }
           }
        }
        
        if (requiredTextAreas != null){        
            for (int i = 0; i < requiredTextAreas.length; i++) {
                if(((JTextArea) mainPanel.getComponent(requiredTextAreas[i])).getText().trim().equals("") ||	
                   ((JTextArea) mainPanel.getComponent(requiredTextAreas[i])).getText().trim().equals(REQUIRED_TEXT)) {
                    ((JTextArea) mainPanel.getComponent(requiredTextAreas[i])).setText(REQUIRED_TEXT);
                    ((JTextArea) mainPanel.getComponent(requiredTextAreas[i])).requestFocus();
                    ((JTextArea) mainPanel.getComponent(requiredTextAreas[i])).selectAll();
                    Toolkit.getDefaultToolkit().beep();
                    return false;               
                }
            }
        }
        
        DateFormat df = new SimpleDateFormat(TimeUtility.DATE_MONTH_DAY_LONG_YEAR_NO_DELIMITER_FORMAT);
        df.setLenient(false);
        
        for (int i = 0; i < dobFields.length; i++) {
            JTextField field = ((JTextField) mainPanel.getComponent(dobFields[i]));
            if (field.getText().trim().length() != 8 &&
                    field.getText().trim().length() != 0) {
                Dialogs.showWarning("dialogs/DialogInvalidData.xml", 	
                    Messages.getString("ComplianceDialog.1866")); 
                //Toolkit.getDefaultToolkit().beep();
                field.requestFocus();
                field.selectAll();
                mainPanel.repaint();
                return false;
            }
            else if (field.getText().trim().length() == 8) {
                try {
                	df.parse(field.getText().trim());
                }
                catch (ParseException e) {
                    Dialogs.showWarning("dialogs/DialogInvalidData.xml", 	
                    		Messages.getString("ComplianceDialog.1867")); 
                    //Toolkit.getDefaultToolkit().beep();
                    field.requestFocus();
                    field.selectAll();
                    mainPanel.repaint();
                    return false;
                }
            }
        }
        return true;
    }


    /*
     * setupListenersCommon - is  used to define listeners for the customer
     * conducting the transaction fields and the third party information fields.  
     */
    private void setupListenersCommon(){
        mainPanel.addActionListener("countryList", this, "updateStateField");	 
        mainPanel.addActionListener("photoIDCountryList", this, "updatePhotoIDStateField");	 
        mainPanel.addActionListener("thirdCountryList", this, "updateThirdStateField");	 
        mainPanel.addActionListener("photoIDTypeList", this, "updatePhotoIDNumberField");	 
        mainPanel.addActionListener("legalIDTypeList", this, "updateLegalIDNumberField");	 
        mainPanel.addActionListener("thirdLegalIDTypeList", this, "updateThirdLegalIDNumberField");	 
   }

    /*
     * updateStateField - When a new country is selected the state list
     * is populated with states for the United States, Canada, and Mexico.
     * For all other countries this state list is left blank.
     */
    public void updateStateField(){
    	Country country = (Country) countryList.getSelectedItem();
        if (country == null)
            return;

        stateList.removeAllItems();
        List<CountrySubdivision> list = country.getCountrySubdivisionList();
        if (list.size() > 0){
            stateList.setEnabled(true);
            for (CountrySubdivision cs : list) {
                stateList.addItem(cs);
            }
        } else
            stateList.setEnabled(false);
    }

    /*
     * updatePhotoIDStateField - When a new country is selected for the photo
     * identification field the state list for the photo identification 
     * is populated with states for the United States, Canada, and Mexico.
     * For all other countries this state list is left blank.
     */
    public void updatePhotoIDStateField(){
    	Country country = (Country) photoIDCountryList.getSelectedItem();
        if (country == null)
            return;

        photoIDStateList.removeAllItems();
        List<CountrySubdivision> list = country.getCountrySubdivisionList();
        if (list.size() > 0){
            photoIDStateList.setEnabled(true);
            for (CountrySubdivision cs : list) {
                photoIDStateList.addItem(cs);
            }
        } else {
        	photoIDStateList.setEnabled(false);
        }
    }
    
    /*
     * updateThirdStateField - When a new country is selected for the third
     * party field the state list for the third party is populated with states 
     * for the United States, Canada, and Mexico. For all other countries 
     * this state list is left blank.
     */
     public void updateThirdStateField(){
    	 Country country = (Country) thirdCountryList.getSelectedItem();
        if (country == null)
            return;

        thirdStateList.removeAllItems();
        List<CountrySubdivision> list = country.getCountrySubdivisionList();
        if (list.size() > 0){
            thirdStateList.setEnabled(true);
            for (CountrySubdivision cs : list) {
                thirdStateList.addItem(cs);
            }
        }else
            thirdStateList.setEnabled(false);
    }
   
    /*
     * updatePhotoIDNumberField - 
     */
    public void updatePhotoIDNumberField(){
        if (((String) photoIDTypeList.getSelectedItem()).trim().length() > 0) {
            photoIDNumberField.setEnabled(true);
        } else {
            photoIDNumberField.setText("");	
            photoIDNumberField.setEnabled(false);
        }
    }

   /*
     * updateLegalIDNumberField - 
     */
    public void updateLegalIDNumberField(){
        if (((String) legalIDTypeList.getSelectedItem()).trim().length() > 0) {
            legalIDNumberField.setEnabled(true);
        } else {
            legalIDNumberField.setText("");	
            legalIDNumberField.setEnabled(false);
        }
    }

    /*
      * updateLegalIDNumberField - 
      */
     public void updateThirdLegalIDNumberField(){
         if (((String) thirdLegalIDTypeList.getSelectedItem()).trim().length() > 0) {
             thirdLegalIDNumberField.setEnabled(true);
         } else {
             thirdLegalIDNumberField.setText("");	
             thirdLegalIDNumberField.setEnabled(false);
         }
     }

    /*
     * setupListenersSAR - is  used to define listeners that need to be defined
     * in for the Person filling out this form section for a SAR dialog.
     */
    private void setupListenersSAR(){
        mainPanel.addActionListener("category4", this, "otherData");	 
        mainPanel.addActionListener("character1", this, "character1Selection");	 
        mainPanel.addActionListener("character2", this, "character2Selection");	 
        mainPanel.addActionListener("character3", this, "character3Selection");	 
        mainPanel.addKeyListener("suspiciousTextArea", new KeyAdapter() {	
            @Override
			public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_TAB) {
                    e.consume();
                    mainPanel.getComponent("category1").requestFocus();	
                }
            }});
    }

    /*
     * otherData - When the Category of suspicious activity's OTHER checkbox is
     * selected a text field to enter what other stands for is enabled. If this
     * checkbox is not selected the text field is disabled.
     */
    public void otherData(){
        mainPanel.getComponent("categoryOther").setEnabled(category4.isSelected());	
    }
    
    /*
     * character1Selection - When the Character of suspicious activity has
     * check box "Unusual use of money order(s) or traveler's check(s)" selected
     * check boxs "Unusual use of money transfer" and "Both" are unchecked and
     * the nine checks that can apply to the Character of suspicious activity are
     * enabled. If the checkbox is deselected the nine checks that can apply to
     * the Character os suspicious activity are disabled.
     */
    public void character1Selection(){
        if (character1.isSelected()){
            character2.setSelected(false);
            character3.setSelected(false);
            enableNineBoxes();
        }else{
            clearNineBoxes();
            disableNineBoxes();
        }        
    }

    /*
     * character2Selection - When the Character of suspicious activity has
     * check box "Unusual use of money transfer" selected check boxs "Unusual 
     * use of money order(s) or traveler's check(s)" and "Both" are unchecked and
     * the nine checks that can apply to the Character of suspicious activity are
     * enabled. If the checkbox is deselected the nine checks that can apply to
     * the Character os suspicious activity are disabled.
     */
    public void character2Selection(){
        if (character2.isSelected()){
            character1.setSelected(false);
            character3.setSelected(false);
            enableNineBoxes();
        } else{
            clearNineBoxes();
            disableNineBoxes();
        }        
    }
    
    /*
     * character3Selection - When the Character of suspicious activity has
     * check box "Both" selected check boxs "Unusual use of money order(s) or 
     * traveler's check(s)" and "Unusual use of money transfer" are unchecked and
     * the nine checks that can apply to the Character of suspicious activity are
     * enabled. If the checkbox is deselected the nine checks that can apply to
     * the Character os suspicious activity are disabled.
     */
    public void character3Selection(){
        if (character3.isSelected()){
            character1.setSelected(false);
            character2.setSelected(false);
            enableNineBoxes();
        } else{
            clearNineBoxes();
            disableNineBoxes();
        }        
    }

    /*
     * enableNineBoxes - When one of the three check boxes for character of
     * suspicious activity is checked, the nine check boxes for check all 
     * that apply need to be enabled.
     */
    private void enableNineBoxes() {
        check1.setEnabled(true);
        check1b.setEnabled(true);
        check2.setEnabled(true);
        check3.setEnabled(true);
        check4.setEnabled(true);
        check5.setEnabled(true);
        check6.setEnabled(true);
        check7.setEnabled(true);
        check8.setEnabled(true);
        check8b.setEnabled(true);
        check9.setEnabled(true);
    }
   
   /*
    * disableNineBoxes - When none of the three check boxes for character of
    * suspicious activity are checked, the nine check boxes for check all
    * that apply need to be cleared and disabled. Used along with clearNineBoxes()
    */
    private void disableNineBoxes() {
        check1.setEnabled(false);
        check1b.setEnabled(false);
        check2.setEnabled(false);
        check3.setEnabled(false);
        check4.setEnabled(false);
        check5.setEnabled(false);
        check6.setEnabled(false);
        check7.setEnabled(false);
        check8.setEnabled(false);
        check8b.setEnabled(false);
        check9.setEnabled(false);
    }
    
    /*
     * clearNineBoxes - When none of the three check boxes for character of
     * suspicious activity are checked, the nine check boxes for check all
     * that apply need to be cleared and disabled. Used along with disableNineBoxes()
     */
    private void clearNineBoxes() {
         check1.setSelected(false);
        check2.setSelected(false);
        check3.setSelected(false);
        check4.setSelected(false);
        check5.setSelected(false);
        check6.setSelected(false);
        check7.setSelected(false);
        check8.setSelected(false);
        check9.setSelected(false);
    }

    private void setupListenersCTR(){
        mainPanel.addKeyListener("commentsField", new KeyAdapter() {	
            @Override
			public void keyPressed(KeyEvent e) {
                final int cnt = 246; //DW42 PCR13 total length of comments field is 246 characters.
                if (e.getKeyCode() == KeyEvent.VK_TAB) {
                    e.consume();
                    mainPanel.getComponent("lastNameField").requestFocus();	
                }
                else {                    
                    String str = commentsField.getText();
                    if (str.length() >= cnt){
                        Toolkit.getDefaultToolkit().beep();
                        commentsField.setText(str.substring(0, cnt)); 
                    }
                }
            }});
        }
    
	/** Writes the modified CTRI to the CCI circular file. */ 
    private void writeCTRI() {
    	if (UnitProfile.getInstance().isTrainingMode() || UnitProfile.getInstance().isDemoMode()) {
    		Debug.println("Skipping CTRI write for demo/training mode");	
    	}
    	else {
			try {
				CCILog.getInstance().write(ctriModified);
			} 
			catch (IOException e) {
				Debug.printStackTrace(e);
			}
    	}
    }
    
   /*
    * updateCommon - This method saves the common fields in the RKL, CTR,
    * and SAR dialogs to the modified CustomerComplianceInfo (CCI). The common 
    * fields contain the "Customer conducting transaction" information and
    * the "Third party" information.
    */
    private void updateCommon() {
    	CustomerComplianceInfo cciModified = getModifiedCCI();
        cciModified.setLocalDateTime(TimeUtility.toXmlCalendar(TimeUtility.currentTime()));
        // Customer conducting transaction information
        cciModified.setLastName(lastNameField.getText().trim());
        cciModified.setFirstName(firstNameField.getText().trim());
        cciModified.setMiddleInitial(middleInitialField.getText().trim());        
        cciModified.setAddress(addressField.getText().trim());
        cciModified.setAddress2(address2Field.getText().trim());
        cciModified.setCity(cityField.getText().trim());
        cciModified.setState(stateList.getState());
        cciModified.setZipCode(zipField.getText().trim());
        if (countryList.getSelectedItem() != null)
            cciModified.setCountry(((Country)countryList.getSelectedItem()).getCountryCode());
        cciModified.setDateOfBirth(TimeUtility.toXmlCalendar(dobField.getText().trim()));
        cciModified.setPhotoIdType(PhotoIdType.valueOf((String) photoIDTypeList.getSelectedItem()));                
        cciModified.setPhotoIdNumber(photoIDNumberField.getText().trim());
        if (photoIDCountryList.getSelectedItem() != null)
            cciModified.setPhotoIdCountry(((Country)photoIDCountryList.getSelectedItem()).getCountryCode());
        cciModified.setPhotoIdState(photoIDStateList.getState());  
        cciModified.setPhoneNumber(phoneNumberField.getText().trim()); 
        cciModified.setOccupation(occupationField.getText().trim()); 
        cciModified.setLegalIdType(LegalIdType.valueOf((String) legalIDTypeList.getSelectedItem()));
        cciModified.setLegalIdNumber(legalIDNumberField.getText().trim());          
    
        // Third Party 
        cciModified.setThirdPartyLastName(thirdLastNameField.getText().trim());
        cciModified.setThirdPartyFirstName(thirdFirstNameField.getText().trim());        
        cciModified.setThirdPartyMiddleInitial(thirdMiddleInitialField.getText().trim());        
        cciModified.setThirdPartyDBA(thirdDBAField.getText().trim());       
        cciModified.setThirdPartyAddress(thirdAddressField.getText().trim());
        cciModified.setThirdPartyAddress2(thirdAddress2Field.getText().trim());
        cciModified.setThirdPartyCity(thirdCityField.getText().trim());
        cciModified.setThirdPartyState(thirdStateList.getState());
        cciModified.setThirdPartyZipCode(thirdZipField.getText().trim());
        if (thirdCountryList.getSelectedItem() != null)
            cciModified.setThirdPartyCountry(((Country)thirdCountryList.getSelectedItem()).getCountryCode());
        cciModified.setThirdPartyDOB(TimeUtility.toXmlCalendar(thirdDOBField.getText().trim()));
        cciModified.setThirdPartyPhoneNumber(thirdPhoneNumberField.getText().trim());
        cciModified.setThirdPartyOccupation(thirdOccupationField.getText().trim());
        cciModified.setThirdPartyLegalIdType(LegalIdType.valueOf((String)thirdLegalIDTypeList.getSelectedItem()));        
        cciModified.setThirdPartyLegalIdNumber(thirdLegalIDNumberField.getText().trim());
    }
    
    /*
     * updateCCIforCTR - This method saves the fields specific for the CTR
     * dialog to the modified CustomerComplianceInfo (CCI). 
     */
    private void updateCTR(){
    	CustomerComplianceInfo cciModified = getModifiedCCI();
        cciModified.setEmployeeNumber(employeeNumberField.getText().trim());
        cciModified.setComments(((JTextArea) mainPanel.getComponent("commentsField")).getText());	
    }
    
    /*
     * updateCCIforSAR - This method saves the fields specific for the SAR
     * dialog to the modified CustomerComplianceInfo (CCI). 
     */
    private void updateSAR(){
    	CustomerComplianceInfo cciModified = getModifiedCCI();
        
        // Person filling out this form information
        cciModified.setEmployeeNumber(employeeNumberField.getText().trim());
        
        // category encodes bitset sum of:
        // category1 = (1) money laundering
        // category2 = (2) structuring
        // category3 = (4) terrorist financing
        // category4 = (8) other
        int categoryTotal = 0;
        if (category1.isSelected())
            categoryTotal = categoryTotal + 1;
        if (category2.isSelected())
            categoryTotal = categoryTotal + 2;
        if (category3.isSelected())
            categoryTotal = categoryTotal + 4;
        if (category4.isSelected())
            categoryTotal = categoryTotal + 8; 
        cciModified.setCategory(categoryTotal); 
        cciModified.setOccupation(categoryOther.getText().trim());               
             

        // character - encoded bitset, sum of
        // character1 - (1) unusual use of money orders
        // character2 - (2) unusual use of money transfers
        // character3 - (3) both character1 and character2
        // check1 -     (4) alters transaction to avoid RK 
        // check2 -     (8) alters transaction to avoid CTR
        // check3 -    (16) comes in frequently and purchases less than 3000
        // check4 -    (32) changes spelling or arrangement of name
        // check5 -    (64) individual(s) using multiple or false ID
        // check6 -   (128) individuals using similar/same ID
        // check7 -   (256) individuals working together
        // check8 -   (512) multiple locations over short time period
        // check9 -  (1024) offers a bribe in the form of a tip/gratuity
        int charTotal = 0;
        if (character1.isSelected())
            charTotal = charTotal + 1;
        if (character2.isSelected())
            charTotal = charTotal + 2;
        if (character3.isSelected())
            charTotal = charTotal + 3;
        if (check1.isSelected())
            charTotal = charTotal + 4;
        if (check2.isSelected())
            charTotal = charTotal + 8;
        if (check3.isSelected())
            charTotal = charTotal + 16;
        if (check4.isSelected())
            charTotal = charTotal + 32;
        if (check5.isSelected())
            charTotal = charTotal + 64;
        if (check6.isSelected())
            charTotal = charTotal + 128;
        if (check7.isSelected())
            charTotal = charTotal + 256;
        if (check8.isSelected())
            charTotal = charTotal + 512;
        if (check9.isSelected())
            charTotal = charTotal + 1024;
        cciModified.setCharacter(charTotal);      
 
        cciModified.setComments(((JTextArea)  mainPanel.getComponent("suspiciousTextArea")).getText());    	
    }

    /*
     * initializeCommon - This method initializes and populates the common fields 
     * in the RKL, CTR, and SAR dialogs from what is found in the CustomerComplianceInfo 
     * (CCI). The common fields contain the "Customer conducting transaction" 
     * information and the "Third party" information.
     */
    private void initializeCommon(){
    	CustomerComplianceInfo cciModified = getModifiedCCI();
        // Customer conducting transaction information
        lastNameField = (JTextField) mainPanel.getComponent("lastNameField");	
        firstNameField = (JTextField) mainPanel.getComponent("firstNameField");	
        middleInitialField = (JTextField) mainPanel.getComponent("middleInitialField");	
        addressField = (JTextField) mainPanel.getComponent("addressField");	
        address2Field = (JTextField) mainPanel.getComponent("address2Field");	
        cityField = (JTextField) mainPanel.getComponent("cityField");      	
        stateList = (StateListComboBox<CountrySubdivision>) mainPanel.getComponent("stateList");	
        zipField = (JTextField) mainPanel.getComponent("zipField");	
        countryList = (MultiListComboBox<Country>) mainPanel.getComponent("countryList");	
        dobField = (JTextField) mainPanel.getComponent("dobField");	
        photoIDTypeList = (MultiListComboBox<String>) mainPanel.getComponent("photoIDTypeList");	
        photoIDNumberField = (JTextField) mainPanel.getComponent("photoIDNumberField");	
        photoIDCountryList = (MultiListComboBox<Country>) mainPanel.getComponent("photoIDCountryList");	
        photoIDStateList = (StateListComboBox<CountrySubdivision>) mainPanel.getComponent("photoIDStateList");	
        phoneNumberField = (JTextField) mainPanel.getComponent("phoneNumberField");	
        occupationField = (JTextField) mainPanel.getComponent("occupationField");	
        legalIDTypeList = (MultiListComboBox<String>) mainPanel.getComponent("legalIDTypeList");	
        legalIDNumberField =(JTextField) mainPanel.getComponent("legalIDNumberField");	
        
        // Third party transaction information
        thirdLastNameField = (JTextField) mainPanel.getComponent("thirdLastNameField");	
        thirdFirstNameField = (JTextField) mainPanel.getComponent("thirdFirstNameField");	
        thirdMiddleInitialField = (JTextField) mainPanel.getComponent("thirdMiddleInitialField");	
        thirdDBAField = (JTextField) mainPanel.getComponent("thirdDBAField");	
        thirdAddressField = (JTextField) mainPanel.getComponent("thirdAddressField");	
        thirdAddress2Field = (JTextField) mainPanel.getComponent("thirdAddress2Field");	
        thirdCityField = (JTextField) mainPanel.getComponent("thirdCityField");	
        thirdStateList = (StateListComboBox<CountrySubdivision>) mainPanel.getComponent("thirdStateList");	
        thirdZipField = (JTextField) mainPanel.getComponent("thirdZipField");	
        thirdCountryList = (MultiListComboBox<Country>) mainPanel.getComponent("thirdCountryList");	
        thirdDOBField = (JTextField) mainPanel.getComponent("thirdDOBField");	
        thirdPhoneNumberField = (JTextField) mainPanel.getComponent("thirdPhoneNumberField");	
        thirdOccupationField = (JTextField) mainPanel.getComponent("thirdOccupationField");	
        thirdLegalIDTypeList = (MultiListComboBox<String>) mainPanel.getComponent("thirdLegalIDTypeList");	
        thirdLegalIDNumberField = (JTextField) mainPanel.getComponent("thirdLegalIDNumberField");	
  
        // Display CCI data for the customer conducting transaction information
        lastNameField.setText(cciModified.getLastName()); 
        firstNameField.setText(cciModified.getFirstName()); 
        middleInitialField.setText(cciModified.getMiddleInitial());
        addressField.setText(cciModified.getAddress()); 
        address2Field.setText(cciModified.getAddress2());
        cityField.setText(cciModified.getCity());
        
        countryList.addList(CountryInfo.getCountriesPlusBlank(CountryInfo.OK_FOR_ADDRESS), "countries");       
        if ((cciModified.getCountry() != null ) && (cciModified.getCountry().trim().length() > 0 )) {       
            stateList.setEnabled(true);          
            countryList.setSelectedItem(CountryInfo.getCountry(cciModified.getCountry()));
        } else {
            countryList.setSelectedItem(CountryInfo.getCountry("  "));  	
            stateList.setEnabled(false);
        }   
        if ((cciModified.getCountry() != null ) && (cciModified.getCountry().trim().length() > 0 )) {
        	Country c = CountryInfo.getCountry(cciModified.getCountry());
        	
            stateList.addList(c.getCountrySubdivisionList(), "states");      
            if ((cciModified.getState() != null) && (cciModified.getState().trim().length() > 0)) 
                stateList.setSelectedItem(cciModified.getState());
        } else {
        	Country country = (Country) countryList.getSelectedItem();
            if (!country.getCountryCode().equals("  ")) {	
                stateList.addList(country.getCountrySubdivisionList(), "states"); 
            }
        }
                
        zipField.setText(cciModified.getZipCode());         
        dobField.setText(TimeUtility.toStringDate(cciModified.getDateOfBirth())); 
        if ((cciModified.getPhotoIdType() != null))
            photoIDTypeList.addList(DWValues.getPhotoIdNames(), "types"); 
        else
            addBlankToPhotoIDList(photoIDTypeList);
            
        if ((cciModified.getPhotoIdType() != null)){
            photoIDTypeList.setSelectedItem(DWValues.getPhotoIdName(cciModified.getPhotoIdType().value()));
            photoIDNumberField.setEnabled(true);
            photoIDNumberField.setText(cciModified.getPhotoIdNumber()); 
        } else {
            photoIDNumberField.setText("");	
            photoIDNumberField.setEnabled(false);
        }
 
        photoIDCountryList.addList(CountryInfo.getCountriesPlusBlank(CountryInfo.OK_FOR_IDENTIFICATION), "countries"); 
        if ((cciModified.getPhotoIdCountry() != null) && (cciModified.getPhotoIdCountry().trim().length() > 0))
            photoIDCountryList.setSelectedItem(CountryInfo.getCountry(cciModified.getPhotoIdCountry()));
        else {
            countryList.setSelectedItem(CountryInfo.getCountry("  "));              	
            photoIDStateList.setEnabled(false);
        }       

        if ((cciModified.getPhotoIdCountry() != null) && (cciModified.getPhotoIdCountry().trim().length() > 0)) {
        	Country c = CountryInfo.getCountry(cciModified.getPhotoIdCountry());
            photoIDStateList.addList(c.getCountrySubdivisionList(), "states");
            if (cciModified.getPhotoIdState() !=null)
                photoIDStateList.setSelectedItem(cciModified.getPhotoIdState());
        } else {
        	Country country = (Country) photoIDCountryList.getSelectedItem();
            if (! country.getCountryCode().equals("  "))	
                photoIDStateList.addList(country.getCountrySubdivisionList(), "states"); 
        }
         
        phoneNumberField.setText(cciModified.getPhoneNumber()); 
        occupationField.setText(cciModified.getOccupation());
        if ((cciModified.getLegalIdType() != null)) {
            legalIDTypeList.setSelectedItem(DWValues.getLegalIdName(cciModified.getLegalIdType().value()));
            legalIDNumberField.setEnabled(true);
            legalIDNumberField.setText(cciModified.getLegalIdNumber());
        } else {            
            addBlankToLegalIDList(legalIDTypeList);
            legalIDNumberField.setText("");	
            legalIDNumberField.setEnabled(false);
        }
  
        // Display CCI data for the third party transaction information   
        thirdLastNameField.setText(cciModified.getThirdPartyLastName());
        thirdFirstNameField.setText(cciModified.getThirdPartyFirstName());
        thirdMiddleInitialField.setText(cciModified.getThirdPartyMiddleInitial());
        thirdDBAField.setText(cciModified.getThirdPartyDBA());
        thirdAddressField.setText(cciModified.getThirdPartyAddress());
        thirdAddress2Field.setText(cciModified.getThirdPartyAddress2());
        thirdCityField.setText(cciModified.getThirdPartyCity());
        thirdZipField.setText(cciModified.getThirdPartyZipCode()); 
        
        thirdCountryList.addList(CountryInfo.getCountriesPlusBlank(CountryInfo.OK_FOR_ADDRESS), "countries"); 
        if ((cciModified.getThirdPartyCountry() !=null) && (cciModified.getThirdPartyCountry().trim().length() > 0))
            countryList.setSelectedItem(CountryInfo.getCountry(cciModified.getThirdPartyCountry()));
        else {
            thirdCountryList.setSelectedItem(CountryInfo.getCountry("  "));   	
            thirdStateList.setEnabled(false);
        }     

        if ((cciModified.getThirdPartyCountry() !=null) && (cciModified.getThirdPartyCountry().trim().length() > 0)) {
        	Country c = CountryInfo.getCountry(cciModified.getThirdPartyCountry());
            thirdStateList.addList(c.getCountrySubdivisionList(), "states"); 
            if (cciModified.getThirdPartyState() !=null)
                thirdStateList.setSelectedItem(cciModified.getThirdPartyState());
        } else {
        	Country country = (Country) thirdCountryList.getSelectedItem();
            if (! country.getCountryCode().equals("  ")) {
                thirdStateList.addList(country.getCountrySubdivisionList(), "states");
            }
        }
        
        thirdDOBField.setText(TimeUtility.toStringDate(cciModified.getThirdPartyDOB())); 
        thirdPhoneNumberField.setText(cciModified.getThirdPartyPhoneNumber()); 
        thirdOccupationField.setText(cciModified.getThirdPartyOccupation()); 
        if ((cciModified.getThirdPartyLegalIdType() != null)) { 
            thirdLegalIDTypeList.setSelectedItem(DWValues.getLegalIdName(cciModified.getThirdPartyLegalIdType().value()));
            thirdLegalIDNumberField.setEnabled(true);
            thirdLegalIDNumberField.setText(cciModified.getThirdPartyLegalIdNumber());
        } else {
            addBlankToLegalIDList(thirdLegalIDTypeList);
            thirdLegalIDNumberField.setText("");	
            thirdLegalIDNumberField.setEnabled(false);
        }
    }

    /*
     * initializeCTR - This method initializes and populates the fields 
     * specific to the CTR dialog from what is found in the CustomerComplianceInfo 
     * (CCI). 
     */
    private void initializeCTR(){
        employeeNumberField = (JTextField) mainPanel.getComponent("employeeNumberField");	
        transactionAmountField=(MoneyField) mainPanel.getComponent("transactionAmountField"); 
        commentsField = (JTextArea) mainPanel.getComponent("commentsField");	
        commentsField.setRows(5);
        commentsField.setLineWrap(true);
    }
        
    /*
     * initializeSAR - This method initializes and populates the fields required
     * specific to the SAR dialog from what is found in the CustomerComplianceInfo 
     * (CCI). 
     */
    private void initializeSAR(){
        // Person filling out this form information
        employeeNumberField = (JTextField) mainPanel.getComponent("employeeNumberField");    	

        suspiciousTextArea = (JTextArea) mainPanel.getComponent("suspiciousTextArea");	
        suspiciousTextArea.setRows(5);
        suspiciousTextArea.setColumns(20);
        suspiciousTextArea.setLineWrap(true);
        
        category1 = (JCheckBox) mainPanel.getComponent("category1");	
        category2 = (JCheckBox) mainPanel.getComponent("category2");	
        category3 = (JCheckBox) mainPanel.getComponent("category3");	
        category4 = (JCheckBox) mainPanel.getComponent("category4");	
        categoryOther = (JTextField) mainPanel.getComponent("categoryOther");	
        character1 = (JCheckBox)mainPanel.getComponent("character1");	
        character2 = (JCheckBox)mainPanel.getComponent("character2");	
        character3 = (JCheckBox)mainPanel.getComponent("character3");	
        check1 = (JCheckBox) mainPanel.getComponent("check1");	
        check1b = (JLabel) mainPanel.getComponent("check1b");	
        check2 = (JCheckBox) mainPanel.getComponent("check2");	
        check3 = (JCheckBox) mainPanel.getComponent("check3");	
        check4 = (JCheckBox) mainPanel.getComponent("check4");	
        check5 = (JCheckBox) mainPanel.getComponent("check5");	
        check6 = (JCheckBox) mainPanel.getComponent("check6");	
        check7 = (JCheckBox) mainPanel.getComponent("check7");	
        check8 = (JCheckBox) mainPanel.getComponent("check8");	
        check8b = (JLabel) mainPanel.getComponent("check8b");	
        check9 = (JCheckBox) mainPanel.getComponent("check9");	

        mainPanel.getComponent("categoryOther").setEnabled(false);	
        disableNineBoxes();
    }
        
    /*
     * addBlankToPhotoIDList - This method adds a blank space to
     * any Photo ID MultiListComboBox in case a Photo ID isn't 
     * specified in the CustomerComplianceInfo(CCI) for the country.
     */
    private void addBlankToPhotoIDList (MultiListComboBox<String> list) {
        String[] ids = DWValues.getPhotoIdNames();
        for (int i = 0; i < ids.length; i++){
            list.addItem(ids[i]);
        }
        list.addItem(" ");	
        list.setSelectedItem(" ");	
    }

    /*
     * addBlankToLegalIDList - This method adds a blank space to
     * any Legal ID MultiListComboBox in case a Legal ID isn't 
     * specified in the CustomerComplianceInfo(CCI) for the country.
     */
    private void addBlankToLegalIDList (MultiListComboBox<String> list) {
        String[] ids = DWValues.getLegalIdNames();
        for (int i = 0; i < ids.length; i++){
            list.addItem(ids[i]);
        }
        list.addItem(" ");	
        list.setSelectedItem(" ");	
    }
 
    // scrollPoint and scrollBounds are used by scrollToComponent, and
    // are defined out here for performance reasons.
    private Point scrollPoint = new Point();
    private Rectangle scrollBounds = new Rectangle();
                
	/** Scroll the viewport only if needed to make the given component visible. */
	public final void scrollToComponent(JComponent f) {
		f.getBounds(scrollBounds);
		Rectangle convertedRectangle = 
			SwingUtilities.convertRectangle(f.getParent(), scrollBounds, scrollee);
		if (! viewport.getViewRect().contains(convertedRectangle)) {
			scrollPoint.x = 0;
			scrollPoint.y = convertedRectangle.y;
			viewport.setViewPosition(scrollPoint);
		}
	}	
	
    /** Set each component to trigger an automatic scroll on focus. */
    private void setupAutoscroll(String[] fields) {
        for (int i = 0; i < fields.length; i++) {
            final JComponent c = (JComponent) mainPanel.getComponent(fields[i]);
            // use XmlDefinedPanel.addFocusListener so the listener is automatically 
            // removed when the panel gets cleaned up.
            mainPanel.addFocusListener(c, new FocusAdapter() {
                @Override
				public void focusGained(FocusEvent e) {
                    scrollToComponent(c);
                }
            });
        }
    }

	private static ComplianceTransactionRequest cloneCtri(ComplianceTransactionRequest ctri) {
		ComplianceTransactionRequest ctri2 = new ComplianceTransactionRequest();
        ctri2.setAgentID(ctri.getAgentID());
        ctri2.setAgentSequence(ctri.getAgentSequence());
        ctri2.setClientSoftwareVersion(ctri.getClientSoftwareVersion());
        ctri2.setLanguage(ctri.getLanguage());
        ctri2.setMgReferenceNumber(ctri.getMgReferenceNumber());
        ctri2.setOperatorName(ctri.getOperatorName());
        ctri2.setPoeType(ctri.getPoeType());
        ctri2.setRequestType(ctri.getRequestType());
        ctri2.setTargetAudience(ctri.getTargetAudience());
        ctri2.setUnitProfileID(ctri.getUnitProfileID());
        ctri2.setTimeStamp((XMLGregorianCalendar) ctri.getTimeStamp().clone());
        
        CustomerComplianceInfo cci = new CustomerComplianceInfo();
        ctri2.setCci(cci);
        cci.setTypeCode(ctri.getCci().getTypeCode());
        cci.setOperatorName(ctri.getCci().getOperatorName());
        cci.setLocalDateTime((XMLGregorianCalendar) ctri.getCci().getLocalDateTime().clone());
        cci.setEmployeeNumber(ctri.getCci().getEmployeeNumber());
        cci.setComments(ctri.getCci().getComments());
        cci.setCategory(ctri.getCci().getCategory());
        cci.setOtherCategory(ctri.getCci().getOtherCategory());
        cci.setCharacter(ctri.getCci().getCharacter());
        cci.setLastName(ctri.getCci().getLastName());
        cci.setFirstName(ctri.getCci().getFirstName());
        cci.setMiddleInitial(ctri.getCci().getMiddleInitial());
        cci.setAddress(ctri.getCci().getAddress());
        cci.setAddress2(ctri.getCci().getAddress2());
        cci.setCity(ctri.getCci().getCity());
        cci.setState(ctri.getCci().getState());
        cci.setZipCode(ctri.getCci().getZipCode());
        cci.setCountry(ctri.getCci().getCountry());
        cci.setPhotoIdType(ctri.getCci().getPhotoIdType());
        cci.setPhotoIdNumber(ctri.getCci().getPhotoIdNumber());
        cci.setPhotoIdState(ctri.getCci().getPhotoIdState());
        cci.setPhotoIdCountry(ctri.getCci().getPhotoIdCountry());
        cci.setDateOfBirth((XMLGregorianCalendar) ctri.getCci().getDateOfBirth().clone());
        cci.setPhoneNumber(ctri.getCci().getPhoneNumber());
        cci.setOccupation(ctri.getCci().getOccupation());
        cci.setLegalIdType(ctri.getCci().getLegalIdType());
        cci.setLegalIdNumber(ctri.getCci().getLegalIdNumber());
        cci.setThirdPartyDBA(ctri.getCci().getThirdPartyDBA());
        cci.setThirdPartyLastName(ctri.getCci().getThirdPartyLastName());
        cci.setThirdPartyFirstName(ctri.getCci().getThirdPartyFirstName());
        cci.setThirdPartyMiddleInitial(ctri.getCci().getThirdPartyMiddleInitial());
        cci.setThirdPartyAddress(ctri.getCci().getThirdPartyAddress());
        cci.setThirdPartyAddress2(ctri.getCci().getThirdPartyAddress2());
        cci.setThirdPartyCity(ctri.getCci().getThirdPartyCity());
        cci.setThirdPartyState(ctri.getCci().getThirdPartyState());
        cci.setThirdPartyZipCode(ctri.getCci().getThirdPartyZipCode());
        cci.setThirdPartyCountry(ctri.getCci().getThirdPartyCountry());
        cci.setThirdPartyDOB((XMLGregorianCalendar) ctri.getCci().getThirdPartyDOB().clone());
        cci.setThirdPartyPhoneNumber(ctri.getCci().getThirdPartyPhoneNumber());
        cci.setThirdPartyOccupation(ctri.getCci().getThirdPartyOccupation());
        cci.setThirdPartyLegalIdType(ctri.getCci().getThirdPartyLegalIdType());
        cci.setThirdPartyLegalIdNumber(ctri.getCci().getThirdPartyLegalIdNumber());

        for (MoneyOrderInfo mo : ctri.getMoneyOrder()) {
        	MoneyOrderInfo mo2 = new MoneyOrderInfo();
        	ctri2.getMoneyOrder().add(mo2);
            mo2.setPrintStatus(mo.getPrintStatus());
            mo2.setMoAccountNumber(mo.getMoAccountNumber());
            mo2.setDeviceID(mo.getDeviceID());
            mo2.setSerialNumber(mo.getSerialNumber());
            mo2.setItemAmount(mo.getItemAmount());
            mo2.setDateTimePrinted((XMLGregorianCalendar) mo.getDateTimePrinted().clone());
            mo2.setItemFee(mo.getItemFee());
            mo2.setPeriodNumber(mo.getPeriodNumber());
            mo2.setDocumentSequenceNbr(mo.getDocumentSequenceNbr());
            mo2.setDocumentType(mo.getDocumentType());
            mo2.setDispenserID(mo.getDispenserID());
            mo2.setVoidFlag(mo.isVoidFlag());
            mo2.setVoidReasonCode(mo.getVoidReasonCode());
            mo2.setTaxID(mo.getTaxID());
            mo2.setEmployeeID(mo.getEmployeeID());
            mo2.setVendorNumber(mo.getVendorNumber());
            mo2.setRemoteIssuanceFlag(mo.isRemoteIssuanceFlag());
            mo2.setSaleIssuanceTag(mo.getSaleIssuanceTag());
            mo2.setDiscountPercentage(mo.getDiscountPercentage());
            mo2.setDiscountAmount(mo.getDiscountAmount());
            mo2.setAccountingStartDay(mo.getAccountingStartDay());
        }
	        
		return ctri2;
	}
 }
