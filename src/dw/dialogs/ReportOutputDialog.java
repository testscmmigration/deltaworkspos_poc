package dw.dialogs;

import java.awt.event.KeyEvent;

import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import dw.dwgui.DWScrollPane;
import dw.i18n.Messages;
import dw.io.report.ReportSelectionCriteria;
import dw.printing.Report;

public class ReportOutputDialog extends ReportDialog {
    private JEditorPane outputArea;
    private DWScrollPane scrollPane;

    public ReportOutputDialog(final Report report,
                              String reportDescription,
                              ReportSelectionCriteria rsc,
                              final boolean outputToFile,
							  final boolean csvOption,
							  final boolean csvEnable) {
        super("dialogs/DialogReportOutput.xml", report, Messages.getString("ReportOutputDialog.1891"), 	 
                false, false, outputToFile, csvOption, csvEnable);

        this.rsc = rsc;
        outputArea = new DWEditorPane();
        scrollPane = (DWScrollPane) mainPanel.getComponent("outputScrollPane");	
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane.getViewport().add(outputArea);
        JLabel header1 = (JLabel) mainPanel.getComponent("headerLabel1");	
        header1.setText(reportDescription);
        isiSkipFirstCancelOnExit = true;
    }
    
    public JEditorPane getOutputArea() {
        return outputArea;
    }
    
    /**
     * Subclass of JEditorPane in order to make it nontraversable and to forward
     * key events to the scroll pane
     */
    private class DWEditorPane extends JEditorPane {
		private static final long serialVersionUID = 1L;

		@Override
		protected void processKeyEvent(final KeyEvent e) {
            final int keyCode = e.getKeyCode();
            if (keyCode == KeyEvent.VK_ENTER)
                e.setKeyCode(KeyEvent.VK_TAB);
            else if (keyCode != KeyEvent.VK_TAB)
                scrollPane.processKeyEvent(e);
        }
    }
}
