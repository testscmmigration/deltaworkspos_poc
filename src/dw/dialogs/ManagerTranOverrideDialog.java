package dw.dialogs;


import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import dw.framework.XmlDialogPanel;
import dw.i18n.Messages;

public class ManagerTranOverrideDialog extends XmlDialogPanel {
    private JTextField nameField = null;
    private JTextField passwordField = null;
    private JLabel amountLabel = null;
    private boolean nameRequired;
    //private JComponent parent = null;
    private JButton okButton = null;
    private JButton cancelButton = null;
    //private XmlDefinedPanel mainPanel = null;
    private boolean canceled = true;

    public ManagerTranOverrideDialog(String amount, boolean nameRequired) {
        super("dialogs/DialogManagerTranOverride.xml");	
        title = Messages.getString("ManagerTranOverrideDialog.1887"); 

        okButton = (JButton) mainPanel.getComponent("okButton");	
        cancelButton = (JButton) mainPanel.getComponent("cancelButton");	

        mainPanel.addActionListener("okButton", this, "ok");	 
        mainPanel.addActionListener("cancelButton", this, "cancel");	 

        this.nameRequired = nameRequired;
        nameField = (JTextField) mainPanel.getComponent("nameField");	
        JLabel nameLabel = (JLabel) mainPanel.getComponent("nameLabel");	
        passwordField = (JTextField) mainPanel.getComponent("passwordField");	
        amountLabel = (JLabel) mainPanel.getComponent("amountLabel");	
        JLabel label1 = (JLabel) mainPanel.getComponent("label1");	
        JLabel label2 = (JLabel) mainPanel.getComponent("label2");	
        amountLabel.setText(amount);
        if (! nameRequired) {
            nameField.setVisible(false);
            nameLabel.setVisible(false);
            label1.setText(Messages.getString("DialogManagerTranOverride.passLabel1Text")); 
            label2.setText(Messages.getString("DialogManagerTranOverride.passLabel2Text")); 
        } else {
            label1.setText(Messages.getString("DialogManagerTranOverride.namePassLabel1Text")); 
            label2.setText(Messages.getString("DialogManagerTranOverride.namePassLabel2Text")); 
        }
        mainPanel.addActionListener("nameField", this, "nameAction");	 
        mainPanel.addActionListener("passwordField", this, "passwordAction");	 
        KeyListener escaper = new KeyAdapter() {
                      @Override
					public void keyPressed(KeyEvent e) {
                          if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                              cancelButton.doClick();
                          }
                      }};
        mainPanel.addKeyListener("passwordField", escaper);	
        mainPanel.addKeyListener("nameField", escaper);	
    }

    public void nameAction() {
        passwordField.requestFocus();
    }

    public void passwordAction() {
        okButton.doClick();
    }

    public void ok() {
        canceled = false;
        closeDialog(Dialogs.CLOSED_OPTION);
    }

    public void cancel() {
        canceled = true;
        closeDialog(Dialogs.CLOSED_OPTION);
    }

    public String getName() {
        if (nameRequired)
            return nameField.getText();
        else
            return null;
    }

    public String getPassword() {
        return passwordField.getText();
    }

    public boolean isCanceled() {
        return canceled;
    }
}
