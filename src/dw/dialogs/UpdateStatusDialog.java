package dw.dialogs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

import dw.comm.MessageFacade;
import dw.framework.ClientTransaction;
import dw.framework.PauseSemaphore;
import dw.framework.XmlDefinedPanel;
import dw.framework.XmlDialogPanel;
import dw.i18n.Messages;
import dw.model.adapters.CountryInfo;
import dw.profile.UnitProfile;
import dw.softwareversions.SoftwareVersionUtility;
import dw.softwareversions.SoftwareVersionUtility.SoftwareStatus;
import dw.softwareversions.VersionManifest;
import dw.softwareversions.VersionManifestEntry;
import dw.utility.Debug;

public class UpdateStatusDialog extends XmlDialogPanel {
	private static UpdateStatusDialog instance = null;

	private JProgressBar preReqProgressBar;
	private JPanel prereqPanel;
	private JButton preReqNextButton = null;
	private JButton preReqCompleteButton = null;
	private JButton stopPreReqDownload;
	private JLabel prereqJreName;
	
	private JProgressBar postUpgrdProgressBar;
	private JPanel postUpgrdPanel;
	private JButton postUpgrdNextButton = null;
	private JButton postUpgrdCompleteButton = null;
	private JButton stopPostUpgrdDownload;
	private JLabel postUpgrdJreName;

	private JLabel statusLabel;
	private Thread prereqDownloadTask;
	private Thread postUpgrdDownloadTask;
	private static PauseSemaphore prereqDownloadSemaphore = new PauseSemaphore();
	private static PauseSemaphore postUpgrdDownloadSemaphore = new PauseSemaphore();
	private JreStatusTransaction transaction;
	private VersionManifest manifest = null;
	private JButton closeButton;
	
	public UpdateStatusDialog() {
		transaction = new JreStatusTransaction("JreTransaction", "");
		String xmlFile = "dialogs/DialogUpdateStatus.xml"; 
		mainPanel = new XmlDefinedPanel(xmlFile);
		closeButton = (JButton) mainPanel.getComponent("close");
		closeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				stopPreReqDownloadAction();
				stopPostUpgrdDownloadAction();
				UpdateStatusDialog.this.closeDialog();
			}
		});

		preReqProgressBar = (JProgressBar) mainPanel.getComponent("preReqProgressBar");
		preReqNextButton = (JButton) mainPanel.getComponent("preReqNextButton");
		preReqCompleteButton = (JButton) mainPanel.getComponent("preReqCompleteButton");
		stopPreReqDownload = (JButton) mainPanel.getComponent("stopPreReqDownload");
		prereqJreName = (JLabel) mainPanel.getComponent("prereqJreName");
		prereqPanel = (JPanel) mainPanel.getComponent("prereqPanel");
		postUpgrdProgressBar = (JProgressBar) mainPanel.getComponent("postUpgrdProgressBar");
		postUpgrdNextButton = (JButton) mainPanel.getComponent("postUpgrdNextButton");
		postUpgrdCompleteButton = (JButton) mainPanel.getComponent("postUpgrdCompleteButton");
		stopPostUpgrdDownload = (JButton) mainPanel.getComponent("stopPostUpgrdDownload");
		postUpgrdJreName = (JLabel) mainPanel.getComponent("postUpgrdJreName");
		postUpgrdPanel = (JPanel) mainPanel.getComponent("postUpgrdPanel");
		statusLabel = (JLabel) mainPanel.getComponent("statusLabel");
		
		preReqProgressBar.setMinimum(0);
		preReqProgressBar.setMaximum(100);
		preReqProgressBar.setStringPainted(true);

		postUpgrdProgressBar.setMinimum(0);
		postUpgrdProgressBar.setMaximum(100);
		postUpgrdProgressBar.setStringPainted(true);

		init();
	}

	private void init() {
		SoftwareVersionUtility.clearDownladStatus();
		
		mainPanel.addActionListener("preReqNextButton", this); 
		mainPanel.addActionListener("preReqCompleteButton", this); 
		mainPanel.addActionListener("postUpgrdNextButton", this); 
		mainPanel.addActionListener("postUpgrdCompleteButton", this); 
		mainPanel.addActionListener("stopPreReqDownload", this); 
		mainPanel.addActionListener("stopPostUpgrdDownload", this); 

		stopPreReqDownload.setEnabled(false);
		stopPostUpgrdDownload.setEnabled(false);
		preReqProgressBar.setValue(0);
		postUpgrdProgressBar.setValue(0);
		prereqPanel.setVisible(false);
		postUpgrdPanel.setVisible(false);
		manifest = null;
	}

	/**
	 * Returns the singleton instance.
	 */
	public static UpdateStatusDialog getInstance() {
		if (instance == null) {
			instance = new UpdateStatusDialog();
		}
		return instance;
	}

	public void cancelButtonAction() {
		closeDialog(Dialogs.CANCEL_OPTION);
	}

	private Boolean initPostUpgrdStatus() {
		handlePrePostUpgrade(VersionManifestEntry.TYPE_POST_UPGRADE, 0, true, postUpgrdDownloadSemaphore, manifest, CountryInfo.MANUAL_TRANSMIT_RETERIVAL, postUpgrdProgressBar);
		SoftwareStatus postUpgrdstatus = SoftwareVersionUtility.getDownloadStatus(VersionManifestEntry.TYPE_POST_UPGRADE);
		boolean isPostUpgrdStatusAvailable = postUpgrdstatus != null ? updatePostUpgrd(postUpgrdstatus.getCount(), postUpgrdstatus.getNumber(), postUpgrdstatus.getJre()) : false;
		postUpgrdProgressBar.revalidate();
		return isPostUpgrdStatusAvailable;
	}

	private Boolean initPrereqStatus() {
		handlePrePostUpgrade(VersionManifestEntry.TYPE_PRE_REQUISITE, 0, true, prereqDownloadSemaphore, manifest, CountryInfo.MANUAL_TRANSMIT_RETERIVAL, preReqProgressBar);
		SoftwareStatus prereqstatus = SoftwareVersionUtility.getDownloadStatus(VersionManifestEntry.TYPE_PRE_REQUISITE);
		boolean isPrereqStatusAvailable = prereqstatus != null ? updatePrereq(prereqstatus.getCount(), prereqstatus.getNumber(), prereqstatus.getJre()) : false;
		preReqProgressBar.revalidate();
		return isPrereqStatusAvailable;
	}

	public void postUpgrdCompleteButtonAction() {
		postUpgrdDownload(0);
	}

	private void postUpgrdDownload(final int fileLimit) {
		Runnable task = new Runnable() {
			@Override
			public void run() {
				try {
					stopPostUpgrdDownload.setEnabled(true);
					closeButton.setEnabled(false);
					statusLabel.setText(Messages.getString("DialogUpdateStatus.4"));
					postUpgrdNextButton.setEnabled(false);
					postUpgrdCompleteButton.setEnabled(false);
					
					VersionManifest.resetDownloadCounter();
					handlePrePostUpgrade(VersionManifestEntry.TYPE_POST_UPGRADE, fileLimit, false, postUpgrdDownloadSemaphore, manifest, CountryInfo.MANUAL_TRANSMIT_RETERIVAL, postUpgrdProgressBar);
					SoftwareStatus postUpgrdstatus = SoftwareVersionUtility.getDownloadStatus(VersionManifestEntry.TYPE_POST_UPGRADE);
					updatePostUpgrd(postUpgrdstatus.getCount(), postUpgrdstatus.getNumber(), postUpgrdstatus.getJre());
	
					statusLabel.setText(Messages.getString("DialogUpdateStatus.5"));
				} finally {
					stopPostUpgrdDownload.setEnabled(false);
					closeButton.setEnabled(true);
				}
			}
		};
		postUpgrdDownloadTask = new Thread(task);
		postUpgrdDownloadTask.start();
	}

	public void postUpgrdNextButtonAction() {
		postUpgrdDownload(1);
	}

	public void preReqCompleteButtonAction() {
		preReqDownload(0);// 0 means no download limit
	}

	private void preReqDownload(final int fileLimit) {
		Runnable task = new Runnable() {
			@Override
			public void run() {
				try {
					stopPreReqDownload.setEnabled(true);
					closeButton.setEnabled(false);
					statusLabel.setText(Messages.getString("DialogUpdateStatus.4"));
					preReqNextButton.setEnabled(false);
					preReqCompleteButton.setEnabled(false);
					
					VersionManifest.resetDownloadCounter();
					handlePrePostUpgrade(VersionManifestEntry.TYPE_PRE_REQUISITE, fileLimit, false, prereqDownloadSemaphore, manifest, CountryInfo.MANUAL_TRANSMIT_RETERIVAL, preReqProgressBar);
					SoftwareStatus prereqstatus = SoftwareVersionUtility.getDownloadStatus(VersionManifestEntry.TYPE_PRE_REQUISITE);
					updatePrereq(prereqstatus.getCount(), prereqstatus.getNumber(), prereqstatus.getJre());
					statusLabel.setText(Messages.getString("DialogUpdateStatus.5"));
				} finally {
					stopPreReqDownload.setEnabled(false);
					closeButton.setEnabled(true);
				}
			}
		};

		prereqDownloadTask = new Thread(task, "preReqDownload");
		prereqDownloadTask.start();
	}

	public void preReqNextButtonAction() {
		preReqDownload(1);
	}

	public void refreshDownloadStatus(final String type, final SoftwareStatus status) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				if (VersionManifestEntry.TYPE_PRE_REQUISITE.equals(type)) {
					getInstance().updatePrereq(status.getCount(), status.getNumber(), status.getJre());
				} else if (VersionManifestEntry.TYPE_POST_UPGRADE.equals(type)) {
					getInstance().updatePostUpgrd(status.getCount(), status.getNumber(), status.getJre());
				}
			}
		});
	}

	@Override
	public void show() {
		if (transaction.isUserAuthorized(false, false)) {
			init();
			statusLabel.setText(Messages.getString("DialogUpdateStatus.8"));
			preReqNextButton.setEnabled(false);
			preReqCompleteButton.setEnabled(false);
			postUpgrdNextButton.setEnabled(false);
			postUpgrdCompleteButton.setEnabled(false);
			
			Runnable task = new Runnable() {
				@Override
				public void run() {
					try {
						manifest = MessageFacade.getInstance().getCurrentManifest(new StringBuffer(), CountryInfo.AUTOMATIC_TRANSMIT_RETERIVAL);
						Boolean isPrereqStatusAvailable = initPrereqStatus();
						Boolean isPostUpgrdStatusAvailable = initPostUpgrdStatus();
					
						if (dialog != null) {
							dialog.pack();
							dialog.repaint();
						}
						
						if (! isPrereqStatusAvailable.booleanValue() && ! isPostUpgrdStatusAvailable.booleanValue()) {
							statusLabel.setText(Messages.getString("DialogUpdateStatus.10"));
						} else {
							statusLabel.setText(null);
						}
					} catch (Exception e) {
						Debug.printException(e);
					}
				}
			};
			Thread thread1 = new Thread(task);
			thread1.start();
			Dialogs.showPanel(this);
		}
	}

	public void stopPostUpgrdDownloadAction() {
		postUpgrdDownloadSemaphore.pause();
	}

	public void stopPreReqDownloadAction() {
		prereqDownloadSemaphore.pause();
	}

	private void handlePrePostUpgrade(String type, int fileLimit, boolean skipDownload, PauseSemaphore downloadSemaphore, VersionManifest manifest, int reterivalType, JProgressBar progressBar) {
		downloadSemaphore.resume();
		SoftwareVersionUtility softwareVersionUtility = new SoftwareVersionUtility();
		softwareVersionUtility.handlePrePostUpgrade(downloadSemaphore, type, fileLimit, skipDownload, manifest);
	}

	private boolean processStatus(int count, int number, String jreName, JProgressBar progressBar, JLabel prereqJreName) {
		prereqJreName.setText(jreName);
		updateProgressBar(progressBar, count, number);
		return count == number;
	}
	
	private void updateProgressBar(JProgressBar progressBar, int value, int maximumVal) {
		if (progressBar != null) {
			int percentVal = maximumVal > 0 ? value * 100 / maximumVal : 100;
			if (percentVal >= progressBar.getValue()) {
				String complStr = percentVal < 100 ? Messages.getString("DialogUpdateStatus.1") + " " + value + " " + Messages.getString("DialogUpdateStatus.9") + " " + maximumVal : Messages.getString("DialogUpdateStatus.13");
				
				progressBar.setString(complStr);
				progressBar.setValue(percentVal);
				progressBar.repaint();
			}
		}
	}

	private boolean updatePostUpgrd(int count, int number, String jreName) {
		boolean isStatusAvailable = false;
		boolean iscomplete = processStatus(count, number, jreName, postUpgrdProgressBar, postUpgrdJreName);
		postUpgrdPanel.setVisible(true);
		isStatusAvailable = true;

		postUpgrdNextButton.setEnabled(! iscomplete);
		postUpgrdCompleteButton.setEnabled(! iscomplete);
		return isStatusAvailable;
	}

	private boolean updatePrereq(int count, int number, String jreName) {
		boolean isStatusAvailable = false;
		boolean iscomplete = processStatus(count, number, jreName, preReqProgressBar, prereqJreName);
		prereqPanel.setVisible(true);
		isStatusAvailable = true;
		preReqNextButton.setEnabled(! iscomplete);
		preReqCompleteButton.setEnabled(! iscomplete);
		return isStatusAvailable;
	}

	/**
	 * @return the prereqDownloadSemaphore
	 */
	public PauseSemaphore getPrereqDownloadSemaphore() {
		return prereqDownloadSemaphore;
	}

	/**
	 * @return the postUpgrdDownloadSemaphore
	 */
	public PauseSemaphore getPostUpgrdDownloadSemaphore() {
		return postUpgrdDownloadSemaphore;
	}
	
	private class JreStatusTransaction extends ClientTransaction {
		private static final long serialVersionUID = 1L;

		public JreStatusTransaction(String tranName, String tranReceiptPrefix) {
			super(tranName, tranReceiptPrefix);
		}

		@Override
		public boolean isFinancialTransaction() {
			return false;
		}

		@Override
		public void setFinancialTransaction(boolean b) {

		}

		@Override
		public boolean isCheckinRequired() {
			return false;
		}

		@Override
		public boolean isManagerApprovalRequired() {
			return true;
		}
		
	    @Override
		public boolean isPasswordRequired() {
	        //return true;
	        String requiredValue = UnitProfile.getInstance().get("GLOBAL_NAME_PIN_REQ", "Y");
	        
	        if (requiredValue.equals("N"))  
	            return false;
	        else
	            return true;
	    }

	    /** Returns false. Name is not required, PIN is sufficient. */
	    @Override
		public boolean isNamePasswordRequired() {
	       // return false;   
	        String requiredValue = UnitProfile.getInstance().get("GLOBAL_NAME_PIN_REQ", "Y");                                 
	        if (requiredValue.equals("Y"))  
	            return true;
	        else
	            return false;   
	    }
	}
}
