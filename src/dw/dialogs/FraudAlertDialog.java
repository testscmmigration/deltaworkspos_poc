package dw.dialogs;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.UIManager;

import dw.framework.XmlDialogPanel;
import dw.i18n.Messages;
import dw.main.DeltaworksStartup;
import dw.profile.UnitProfile;

public class FraudAlertDialog extends XmlDialogPanel {
    private boolean canceled = false;
    private boolean accepted = false;
    private JButton yesButton = null;
    private JButton noButton = null;
    private JButton cancelButton = null; 
    
    public FraudAlertDialog() {
        super("dialogs/DialogFraudAlert.xml");	
        this.title = Messages.getString("FraudAlertDialog.1886"); 
         
        mainPanel.addActionListener("noButton", this, "no");	 
        mainPanel.addActionListener("yesButton", this, "yes");	 
        mainPanel.addActionListener("cancelButton", this, "cancel");	 
        
        yesButton = (JButton) mainPanel.getComponent("yesButton");
        noButton = (JButton) mainPanel.getComponent("noButton");
        cancelButton = (JButton) mainPanel.getComponent("cancelButton");
              
        KeyListener escaper = new KeyAdapter() {
           @Override
		public void keyPressed(KeyEvent e) {
              if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
                  cancelButton.doClick();
              }};
        KeyListener activator = new KeyAdapter() {
            @Override
			public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    yesButton.doClick();
                }};              
  
        mainPanel.addKeyListener("yesButton",escaper);
        mainPanel.addKeyListener("yesButton",activator);
       
        String text = UnitProfile.getInstance().get("FRAUD_ALERT_MESSAGE", "");	 
        if (text.length() > 0) {
            setCustomText(text);
        }
    }

    /** Returns true if user clicked OK. */
    public boolean isAccepted() {
        return accepted;
    }  

    /** Returns true if user clicked Cancel. */
    public boolean isCanceled() {
        return canceled;
    }

    public void yes() {
        accepted = true;
        closeDialog();
    }

    public void cancel() {
        canceled = true;
        closeDialog();
    }

    public void no() {
        closeDialog();
    }

    public void setCustomText(String text) {
        JLabel label = (JLabel) mainPanel.getComponent("customText");  
        label.setText(text);
    }
    
    public void showPanel() {
    	/*
    	 * Display dialog and default to No Button
    	 */
        Dialogs.showPanel(this, this.noButton);
    }
    
    /* for testing */
    public static void main(String args[]) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (Exception e) {
        }

       // dw.main.DeltaworksMainPanel.fixFonts();
       DeltaworksStartup.fixFonts();
        
        JFrame frame = new JFrame();
        final JTextArea textarea = new JTextArea(10, 20);
        final JButton button = new JButton("Show"); 
        
        frame.getContentPane().add(textarea, BorderLayout.CENTER);
        frame.getContentPane().add(button, BorderLayout.SOUTH);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setIconImage((new ImageIcon("../xml/images/temg_window.gif")).getImage()); 
        frame.pack();
        frame.setVisible(true);

        button.addActionListener(new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent ev) {
                FraudAlertDialog dialog = new FraudAlertDialog();
                String text = textarea.getText();
                if (text.length() > 0)
                    dialog.setCustomText(textarea.getText());
                dialog.show();
            }
        });
    }
}
