package dw.dialogs;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Calendar;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import com.moneygram.agentconnect.DwPasswordReasonCodeType;
import com.moneygram.agentconnect.DwPasswordResponse;

import dw.framework.ClientTransaction;
import dw.framework.XmlDefinedPanel;
import dw.framework.XmlDialogPanel;
import dw.i18n.Messages;
import dw.io.report.AuditLog;
import dw.profile.Profile;
import dw.profile.ProfileItem;
import dw.profile.UnitProfile;
import dw.utility.Debug;

/**
 * 
 */
public class ChangePasswordDialog extends XmlDialogPanel {
	// types of name/pass dialogs to be used when creating
	private JTextField oldPasswordField = null;

	private JTextField newPasswordField = null;

	private JTextField secondPasswordField = null;

	private JButton okButton = null;

	private JButton cancelButton = null;

	private boolean canceled = true;

	private String passwordToChange = null;

	private int userIdToChange;

	public String oldPassword = null;

	public String newPassword = null;

	private ClientTransaction transaction = null;

	private boolean communicationError = false;

	// private static Thread current1 = null;
	private DwPasswordResponse response = null;


	protected ChangePasswordDialog(int userID, String password,
			ClientTransaction transaction) {
		this.title = Messages.getString("DialogChangePassword.title"); 
		this.transaction = transaction;
		mainPanel = new XmlDefinedPanel("dialogs/DialogChangePassword.xml");

		oldPasswordField = (JTextField) mainPanel
				.getComponent("oldPasswordField");
		newPasswordField = (JTextField) mainPanel
				.getComponent("newPasswordField");
		secondPasswordField = (JTextField) mainPanel
				.getComponent("secondPasswordField");
		okButton = (JButton) mainPanel.getComponent("okButton");
		cancelButton = (JButton) mainPanel.getComponent("cancelButton");
		passwordToChange = password;
		userIdToChange = userID;

		mainPanel.addActionListener("okButton", this); 
		mainPanel.addActionListener("cancelButton", this); 
		KeyListener escaper = new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
					cancelButton.doClick();
			}
		};
		KeyListener activator = new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					okClick();
			}
		};
		KeyListener nexttor = new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					if (oldPasswordField.hasFocus())
						newPasswordField.requestFocus();
					else if (newPasswordField.hasFocus())
						secondPasswordField.requestFocus();
			}
		};

		mainPanel.addKeyListener("oldPasswordField", escaper); 
		mainPanel.addKeyListener("newPasswordField", escaper); 
		mainPanel.addKeyListener("secondPasswordField", escaper); 

		mainPanel.addKeyListener("oldPasswordField", nexttor); 
		mainPanel.addKeyListener("newPasswordField", nexttor); 
		mainPanel.addKeyListener("secondPasswordField", activator); 

	}

	/**
	 * Returns false to indicate this dialog's title bar text should not be
	 * logged in the trace file.
	 */

	@Override
	public boolean showTitleInDebug() {
		return false;
	}

	public void okClick() {

		okButton.doClick();

	}

	public void okButtonAction() {
		canceled = false;
		validateEntries();

	}

	public void cancelButtonAction() {
		closeDialog(Dialogs.CANCEL_OPTION);
		canceled = true;
	}

	public void validateEntries() {
		if (!isPasswordInUse()) {
			if (validateEntriesLocally()) {
				Thread worker = new Thread() {
					@Override
					public void run() {
						transaction.validatePin(userIdToChange,
								newPasswordField.getText().trim());
						try {
							Thread.sleep(1000);
						} catch (InterruptedException ex) {
						}
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								if (response != null) {
									if (response.getPayload().getValue().isStatus())
										updateProfile();
									else {
										if (response.getPayload().getValue().getReasonCode().equals(DwPasswordReasonCodeType.EMP_NOT_EXIST)) {
											Dialogs.showWarning("dialogs/DialogEmpDoesNotExist.xml");
										} else {
											Dialogs.showWarning("dialogs/DialogInvalid8CharPassword.xml"); 
										}
										oldPasswordField.setText("");
										newPasswordField.setText("");
										secondPasswordField.setText("");
										oldPasswordField.requestFocus();

									}

								} else {
									setCommunicationError(true);
									closeDialog(Dialogs.CANCEL_OPTION);
									canceled = true;
								}
							}
						});
					}
				};
				worker.start();

			} else {
				Dialogs.showWarning("dialogs/DialogInvalid8CharPassword.xml"); 
				canceled = true;
				oldPasswordField.setText("");
				newPasswordField.setText("");
				secondPasswordField.setText("");
				oldPasswordField.requestFocus();
			}
		} else {
			Dialogs.showWarning("dialogs/DialogPasswordInUse.xml"); 
			canceled = true;
			oldPasswordField.setText("");
			newPasswordField.setText("");
			secondPasswordField.setText("");
			oldPasswordField.requestFocus();

		}

	}

	private void updateProfile() {
		Profile userProfile;

		// Store the new users password
		userProfile = (Profile) UnitProfile.getInstance().getProfileInstance()
				.find("USER", userIdToChange); 
		userProfile.get("PASSWORD").setValue(newPasswordField.getText().trim()); 

		// Set the password date changed profile to current date
		Calendar today = Calendar.getInstance();
		today.setTime(new Date());
		today.set(Calendar.HOUR, 0);
		today.set(Calendar.MINUTE, 0);
		today.set(Calendar.SECOND, 0);
		userProfile.get("PSWD_CHANGE_DATE").setValue(
				String.valueOf(today.getTimeInMillis()));
		setProfileItem(userProfile);
		setValidatePinInfo(oldPasswordField.getText().trim(), newPasswordField
				.getText().trim());
		AuditLog.writeProfileChangeRecord(userProfile.getID(), "PASSWORD",
				userProfile.get("USER_ID").stringValue(), "N/A");
		closeDialog(Dialogs.OK_OPTION);
	}

	private void setProfileItem(Profile userProfile) {
		try {

			userProfile.find("GRACE_TIME").setValue("0");
		} catch (NoSuchElementException e) {
			Debug.println("No element :GRACE_TIME when querying ");
			ProfileItem pi = new ProfileItem("ITEM", "GRACE_TIME", "P", "0",
					null);
			userProfile.insertOrUpdate("GRACE_TIME", pi);

		}
		try {
			userProfile.find("PIN_EXPIRED").setValue("N");
		} catch (NoSuchElementException e) {
			Debug
					.println("No element :PIN_EXPIRED not found when seeting expiry to N");
			ProfileItem pi = new ProfileItem("ITEM", "PIN_EXPIRED", "P", "N",
					null);
			userProfile.insertOrUpdate("PIN_EXPIRED", pi);
		}
		try {
			userProfile.find("GRACE_LOGIN_USED").setValue("N");
		} catch (NoSuchElementException e) {
			Debug
					.println("No element :GRACE_LOGIN_USED not found when seeting expiry to N");
			ProfileItem pi = new ProfileItem("ITEM", "GRACE_LOGIN_USED", "P",
					"N", null);
			userProfile.insertOrUpdate("GRACE_LOGIN_USED", pi);
		}
	}

	private boolean isPasswordInUse() {
		if (ClientTransaction.getUserID(null,
				newPasswordField.getText().trim(), false) == 0)
			return false;
		else
			return true;

	}

	private boolean validateEntriesLocally() {
		boolean result = false;
		try {
			int digitCount = 0;
			int specialCount = 0;
			int letterCount = 0;

			String s = newPasswordField.getText().trim();
			for (int i = 0; i < s.length(); i++) {
				char c = s.charAt(i);
				String p = s.substring(i, i + 1);
				if (isvalidMatch(p)) {
					specialCount++;
				}
				if (Character.isDigit(c)) {
					digitCount++;
				}
				if (Character.isLetter(c)) {
					letterCount++;
				}
				if (Character.isSpaceChar(c)) {
					specialCount++;
				}
			}

			if ((newPasswordField.getText().trim().length() >= 8) && 
					(newPasswordField.getText().trim().length() < 51) && 
					(passwordToChange.trim().equals(oldPasswordField.getText().trim())) && 
					(newPasswordField.getText().trim().equals(secondPasswordField.getText().trim())) && 
					(! oldPasswordField.getText().trim().equals(newPasswordField.getText().trim())) && 
					(letterCount > 0) && 
					(digitCount > 0) && 
					(specialCount > 0)) {

				result = true;
			} else {
				result = false;
			}
			return result;
		} catch (Exception e) {
			Debug.println(e.getMessage());
			return false;
		}
	}

	public boolean isCanceled() {
		return canceled;
	}

	public boolean isvalidMatch(String s) {
		// \\p{Punct} is equivalent to !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~
		Pattern p = Pattern.compile("\\p{Punct}");
		Matcher m = p.matcher(s);
		boolean match = m.matches();
		return match;

	}

	private void setValidatePinInfo(String oldPassword, String newPassword) {
		this.oldPassword = oldPassword;
		this.newPassword = newPassword;

	}

	public String getOldPassword() {
		return this.oldPassword;
	}

	public String getNewPassword() {
		return this.newPassword;
	}

	public void setValidatePinResponse(DwPasswordResponse resp) {
		this.response = resp;
	}

	public void setCommunicationError(boolean error) {
		this.communicationError = error;

	}

	public boolean isCommunicationError() {
		return this.communicationError;

	}
}
