package dw.dialogs;


import java.awt.Toolkit;

import javax.swing.JTextField;

import dw.framework.XmlDialogPanel;
import dw.i18n.Messages;

public class ManualDialDialog extends XmlDialogPanel {

    private boolean canceled = true;        // set to false if user clicks OK
    
    private JTextField phoneNumberField = null;
    private JTextField usernameField = null;
    private JTextField passwordField = null;
    public static final String REQUIRED_TEXT = Messages.getString("ManualDialDialog.1888"); 

    public ManualDialDialog() {
        super("dialogs/DialogManualDial.xml");	
        this.title = Messages.getString("ManualDialDialog.1889"); 

        phoneNumberField = (JTextField) mainPanel.getComponent("phoneNumberField");	
    	usernameField = (JTextField) mainPanel.getComponent("usernameField");	
        passwordField = (JTextField) mainPanel.getComponent("passwordField");	
        mainPanel.addActionListener("okButton", this, "okButtonAction");	 
        mainPanel.addActionListener("cancelButton", this, "cancelButtonAction");	 
    }

    // populate the fields with the supplied info
    public void setDialInfo(String phoneNumber, String userName, String pwd) {
        phoneNumberField.setText(phoneNumber);
        usernameField.setText(userName);
        passwordField.setText(pwd);
    }

    public void cancelButtonAction() { 
        canceled = true;
        closeDialog();
    }

    public void okButtonAction() {
        canceled = false;
        if ((validateField(phoneNumberField)) 
                && (validateField(usernameField))
                && (validateField(passwordField))) {
            
            canceled = false;
            closeDialog();
        }
    	else {
	        //do nothing
    	}
    }
    

    public String getPhoneNumber() {
        return phoneNumberField.getText();
    }
    
    public String getUsername() {
        return usernameField.getText();
    }
    
    public String getPassword() {
        return passwordField.getText();
    }

    /** Returns true if user closed dialog without clicking OK. */
    public boolean isCanceled() {
        return canceled;
    }

    protected boolean validateField(JTextField field) {
		if (field.getText().trim().equals("") ||	
	                field.getText().trim().equals(REQUIRED_TEXT)) {
	            
		    field.setText(REQUIRED_TEXT);
		    field.select(0, field.getText().length());
		    field.requestFocus();
		    Toolkit.getDefaultToolkit().beep();
		    return false;
		}
		else {
		    return true;
		}
    }
}
