package dw.dialogs;

import java.awt.Component;
import java.awt.Frame;
import java.util.Stack;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

import dw.comm.MessageFacade;
import dw.framework.ClientTransaction;
import dw.framework.XmlDefinedPanel;
import dw.framework.XmlDialogPanel;
import dw.i18n.Messages;
import dw.main.DeltaworksStartup;
import dw.main.MainPanelDelegate;
import dw.moneyorder.MOPrintingDialog;
import dw.profile.UnitProfile;
import dw.utility.Debug;

/** 
 * Entry point for dialog boxes. This class replaces XmlDefinedDialog
 * and about 40 one-line classes from this package. It also enforces 
 * dialog consistency and prevent future memory leaks. Dialogs created
 * by this class are either disposed by this class or are cached and
 * reused by this class. 
 *
 * Note: when adding new dialog boxes (yes/no questions, warnings,
 * error messages, etc) try to keep things simple by using
 * showInformation(file), showWarning(file), showError(file),
 * showConfirm(file), showRequest(file), or showQuestion(file).
 * The other methods are for compatibility with existing dialog
 * boxes or special cases.
 *
 * Most of the methods in this class are static and are similar
 * in usage to the static methods of JOptionPane.
 * 
 * The intended uses of the major methods are as follows. See the
 * javadoc comments for each method for details.
 *
 * showInformation: an action completed successfully, when it would
 * not otherwise be obvious to the user that it completed at all.
 *
 * showError: we can't go on and there's nothing the user can do
 * about it. (unrecoverable error)
 *
 * showWarning: the user needs to be aware of something or go back
 * and fix something before we can go on. (includes input validation)
 *
 * showConfirm: the user needs to be aware of something and make a
 * decision about it before we can go on.
 *
 * showRequest: the user needs to do something for the application's
 * benefit (for example, insert a floppy disk) before we can go on.
 *
 * showQuestion: the application needs to ask the user a question 
 * before we can go on.
 *
 * showInput: the user needs to choose from among multiple options
 * (for example, to narrow down a search) before we can go on.
 *
 * showPanel: for displaying larger/more complicated secondary windows
 * that don't fit any of the above  
 * 
 */
public class Dialogs {

    private static Frame parentFrame = null;
    private static Stack<JDialog> dialogStack = new Stack<JDialog>();
    //private static JDialog currentDialog = null;
    //private static JDialog previousDialog = null;
    
    public static final String TITLE = "DeltaWorks"; 
    
    public static final int YES_OPTION = JOptionPane.YES_OPTION;        // 0
    public static final int NO_OPTION = JOptionPane.NO_OPTION;          // 1
    public static final int CANCEL_OPTION = JOptionPane.CANCEL_OPTION;  // 2
    public static final int OK_OPTION = JOptionPane.OK_OPTION;          // 0
    public static final int CLOSED_OPTION = JOptionPane.CLOSED_OPTION;  // -1
    public static final int CONT_OPTION = JOptionPane.OK_OPTION;        // 0
    public static final int ABORT_OPTION = JOptionPane.CANCEL_OPTION;   // 2
    public static final int RETRY_OPTION = JOptionPane.OK_OPTION;   // 0
    public static final int PRINT_OPTION = JOptionPane.OK_OPTION;   // 0

    // The index into the following array is one of the above OPTION values.
    private static String optionString[] = {"[CLOSED]", "[OK/YES]", "[NO]", "[CANCEL]"};
        
    private static JDialog aboutBox = null;     // cached here and reused
    private static JLabel aboutBoxMemoryLabel;
    
    private static Object empty[] = {};
    
    private static long specialDialogCounter = -1;

    private static Option YES_BUTTON = new Option(YES_OPTION, "[YES]", Option.YES_BUTTON);
    private static Option NO_BUTTON = new Option(NO_OPTION, "[NO]", Option.NO_BUTTON);
    private static Option CANCEL_BUTTON = new Option(CANCEL_OPTION, "[CANCEL]", Option.CANCEL_BUTTON);
    private static Option OK_BUTTON = new Option(OK_OPTION, "[OK]", Option.OK_BUTTON);
    private static Option CONTINUE_BUTTON = new Option(CONT_OPTION, "[Continue]", Option.CONTINUE_BUTTON);
    private static Option ABORT_BUTTON = new Option(ABORT_OPTION, "[Abort]", Option.ABORT_BUTTON);
    private static Option RETRY_BUTTON = new Option(RETRY_OPTION, "[Retry]", Option.RETRY_BUTTON);
    private static Option PRINT_BUTTON = new Option(PRINT_OPTION, "[Print]", Option.PRINT_BUTTON);
    
    private static Option DEFAULT_OPTION[] = {OK_BUTTON};                                           // -1
    private static Option YES_NO_OPTION[] = {YES_BUTTON, NO_BUTTON};                                // 0
    private static Option YES_NO_CANCEL_OPTION[] = {YES_BUTTON, NO_BUTTON, CANCEL_BUTTON};          // 1
    private static Option OK_CANCEL_OPTION[] = {OK_BUTTON, CANCEL_BUTTON};  
    private static Option CONT_ABORT_OPTION[] = {CONTINUE_BUTTON, ABORT_BUTTON}; 
    private static Option RETRY_CANCEL_OPTION[] = {RETRY_BUTTON, CANCEL_BUTTON};  
    private static Option PRINT_CANCEL_OPTION[] = {PRINT_BUTTON, CANCEL_BUTTON};  
    
    private static int OPTION2_RETRY_CANCEL = 5;
    private static int OPTION2_PRINT_CANCEL = 6;
    private static Option OPTIONS[][] = { DEFAULT_OPTION, YES_NO_OPTION, 
            YES_NO_CANCEL_OPTION, OK_CANCEL_OPTION, CONT_ABORT_OPTION, 
            RETRY_CANCEL_OPTION, PRINT_CANCEL_OPTION};
    private static ChangePasswordDialog instance;
    
    
    /** 
     * Sets the frame to use as parent component for dialogs. 
     * DeltaworksMainPanel should call this after the main window
     * is created.
     */
    public static void setParent(Component c) {
        parentFrame = JOptionPane.getFrameForComponent(c);
    }

    /**
     * Determines the parent for newly created dialogs. If another dialog
     * is currently displayed, it is the parent, otherwise the main 
     * window is the parent. 
     */
    public static Component getParent() {
        JDialog currentDialog = getCurrentDialog();
        
        if (currentDialog == null) {
        	
        	/**
			 * if parent frame is in minimize then it should be restored so that new
			 * dialog can be in center of parent frame
			 **/
			if ((parentFrame != null) && parentFrame.isMinimumSizeSet()) {
				parentFrame.setState(Frame.NORMAL);
			}
			
            return parentFrame;
        }
        else {
            return currentDialog;
        }
    }
    
    /** 
     * Show an AC Error message dialog built from an XML file.
     */
    /** 
     * Show an AC Error message dialog built from an XML file.
     */
     public static void acErrorDialog(String pm_sInfoMsg, String pm_sErrorCode,
			String pm_sErrorText) {
		String errorCode = "";
		String errorString = "";

		errorCode = pm_sErrorCode.trim();
		errorString = pm_sErrorText.trim();

		StringBuffer buf = new StringBuffer();
		// Check if Error code is not added then error should be added into
		// message
		if (pm_sErrorText.indexOf("(AC") == -1) {
			buf.append(errorString).append(" (AC ").append(Messages.getString("Error.1")).append(" ").append(errorCode)
					.append("). ");
		} else {
			buf.append(errorString);
		}
		errorString = buf.toString();

		Dialogs.showError("dialogs/AcError.xml", 
				pm_sInfoMsg, errorString);
	}
    
    
    /** 
     * Show an informational message dialog built from an XML file.
     * This is appropriate for completed actions that otherwise have
     * no obvious indication that they're done.
     * The usage of this method is intended to be similar to 
     * JOptionPane.showMessageDialog().
     */
    public static void showInformation(String filename) {
        standardDialog(filename, JOptionPane.INFORMATION_MESSAGE, 
            JOptionPane.DEFAULT_OPTION, null, null, null, null);
    }

//    /**
//     * Show an informational message dialog built from XML, with an
//     * extra message string specified programmatically. The XML file
//     * must have a label component named "message" where the message 
//     * goes. This variant of showInformation(filename) should be  
//     * avoided because the dynamic message won't be internationalized 
//     * properly.
//     */
//    public static void showInformation(String filename, String message) {
//        standardDialog(filename, JOptionPane.INFORMATION_MESSAGE, 
//            JOptionPane.DEFAULT_OPTION, null, null, null, null);
//    }
    
    /** 
     * Show a warning message dialog built from an XML file.
     * This is appropriate for user input validation errors and
     * other conditions that require user input before proceeding.
     * The usage of this method is intended to be similar to 
     * JOptionPane.showMessageDialog().
     */
    public static void showWarning(String filename) {
        standardDialog(filename, JOptionPane.WARNING_MESSAGE, 
            JOptionPane.DEFAULT_OPTION, null, null, null, null);
    }

    /**
     * Show a warning message dialog built from an XML file, with an
     * extra message string supplied programmatically. The XML file must
     * have a label component named "message" where the message goes.
     * This variant of showWarning(filename) should be avoided because
     * the dynamic message won't be internationalized properly. It is
     * used for a few money order limit dialogs where the numeric limit
     * is mentioned in the dialog text.
     */    
    public static void showWarning(String filename, String message) {
        standardDialog(filename, JOptionPane.WARNING_MESSAGE, 
            JOptionPane.DEFAULT_OPTION, message, null, null, null);
    }

    public static void showWarning(String filename, String message, 
            String message2, String message3) { 
        
        standardDialog(filename, JOptionPane.WARNING_MESSAGE, 
            JOptionPane.DEFAULT_OPTION, message, message2, message3, null);
    }
    
    public static void showWarning(String filename, String message, 
            String message2, String message3, String message4) { 
        
        standardDialog(filename, JOptionPane.WARNING_MESSAGE, 
            JOptionPane.DEFAULT_OPTION, message, message2, message3, message4);
    }

    
    
    /**
     * Show a warning message dialog built from an XML file, with two
     * extra message strings supplied programmatically. The XML file must
     * have label components named "message" and "message2" where the 
     * supplied values go. This variant of showWarning(filename) should 
     * be avoided because the dynamic messages won't be internationalized
     * properly.
     */
    public static void showWarning(String filename, String message, 
            String message2) { 
        
        standardDialog(filename, JOptionPane.WARNING_MESSAGE, 
            JOptionPane.DEFAULT_OPTION, message, message2, null, null);
    }

    /** 
     * Show an error message dialog built from an XML file. This
     * is appropriate for unrecoverable errors, meaning situations
     * where the user will not be able to do whatever they're
     * trying to do. The usage of this method is intended to be
     * similar to JOptionPane.showMessageDialog().
     */
    public static void showError(String filename) {
        standardDialog(filename, JOptionPane.ERROR_MESSAGE, 
            JOptionPane.DEFAULT_OPTION, null, null, null, null);
    }

    /**
     * Show an error message dialog built from an XML file, with one
     * extra message string supplied programmatically. The XML file must
     * have a label component named "message" where the message goes.
     * This variant of showError(filename) should be avoided because
     * the dynamic message won't be internationalized properly. It is
     * used by DeltaworksMainPanel.
     */
    public static void showError(String filename, String message) {
        standardDialog(filename, JOptionPane.ERROR_MESSAGE, 
            JOptionPane.DEFAULT_OPTION, message, null, null, null);
    }
    
    /**
     * Show an error message dialog built from an XML file, with two
     * extra message strings supplied programmatically. The XML file must
     * have label components named "message" and "message2" where the 
     * supplied values go. This variant of showError(filename) should 
     * be avoided because the dynamic messages won't be internationalized
     * properly.
     */
    public static void showError(String filename, String message, 
            String message2) {
        
        standardDialog(filename, JOptionPane.ERROR_MESSAGE, 
            JOptionPane.DEFAULT_OPTION, message, message2, null, null);
    }
    
    /**
     * Show an error message dialog built from an XML file, with two
     * extra message strings supplied programmatically. The XML file must
     * have label components named "message", message2, and "message3" where the 
     * supplied values go. This variant of showError(filename) should 
     * be avoided because the dynamic messages won't be internationalized
     * properly.
     */
    public static void showError(String filename, String message, 
            String message2, String message3) {
        
        standardDialog(filename, JOptionPane.ERROR_MESSAGE, 
            JOptionPane.DEFAULT_OPTION, message, message2, message3, null);
    }
    
    /**
     * Show an error message dialog built from an XML file, with two
     * extra message strings supplied programmatically. The XML file must
     * have label components named "message", message2, message3, and "message4" where the 
     * supplied values go. This variant of showError(filename) should 
     * be avoided because the dynamic messages won't be internationalized
     * properly.
     */
    public static void showError(String filename, String message, 
            String message2, String message3, String message4) {
        
        standardDialog(filename, JOptionPane.ERROR_MESSAGE, 
            JOptionPane.DEFAULT_OPTION, message, message2, message3, message4);
    }

    /** 
     * Show a yes/no dialog built from an XML file.
     * This is appropriate for situations where the user is being
     * asked to confirm their intentions or being invited to perform
     * some other action first. The usage of this method is intended 
     * to be similar to JOptionPane.showConfirmDialog().
     * @return either YES_OPTION, NO_OPTION, or CLOSED_OPTION
     */
    public static int showConfirm(String filename) {
        return standardDialog(filename, JOptionPane.WARNING_MESSAGE, 
            JOptionPane.YES_NO_OPTION, null, null, null, null);
    }

    public static int showYesNoCancel(String filename) {
        return standardDialog(filename, JOptionPane.WARNING_MESSAGE, 
            JOptionPane.YES_NO_CANCEL_OPTION, null, null, null, null);
    }
    /** 
     * Show a yes/no dialog built from an XML file, with two extra
     * message strings specified programmatically. The XML file must
     * have label components named "message" and "message2" where the 
     * supplied values go. This variant of showConfirm(filename) should 
     * be avoided because the dynamic messages won't be internationalized
     * properly.
     * @return either YES_OPTION, NO_OPTION, or CLOSED_OPTION
     */
    public static int showConfirm(String filename, String message, 
            String message2) {
        return standardDialog(filename, JOptionPane.WARNING_MESSAGE, 
            JOptionPane.YES_NO_OPTION, message, message2, null, null);
    }
    
    /** 
     * Show a ok/cancel dialog built from an XML file, with four extra
     * message strings specified programmatically. The XML file must
     * have label components named "message", "message2", "message3"
     * and "message4", and where the 
     * supplied values go. This variant of showConfirm(filename) should 
     * be avoided because the dynamic messages won't be internationalized
     * properly.
     * @return either OK_OPTION or CANCEL_OPTIONN
     */
    public static int showOkCancel(String filename) {
        return standardDialog(filename, JOptionPane.WARNING_MESSAGE, 
            JOptionPane.OK_CANCEL_OPTION, null, null, null, null);
    }
    
    /** 
     * Show a retry/cancel dialog built from an XML file, with four extra
     * message strings specified programmatically. The XML file must
     * have label components named "message", "message2", "message3"
     * and "message4", and where the 
     * supplied values go. This variant of showConfirm(filename) should 
     * be avoided because the dynamic messages won't be internationalized
     * properly.
     * @return either RETRY_OPTION or CANCEL_OPTIONN
     */
    public static int showRetryCancel(String filename) {
        return standardDialog(filename, JOptionPane.WARNING_MESSAGE, 
            JOptionPane.OK_CANCEL_OPTION, OPTION2_RETRY_CANCEL, null, null, null, null);
    }
    
    /** 
     * Show a print/cancel dialog built from an XML file, with four extra
     * message strings specified programmatically. The XML file must
     * have label components named "message", "message2", "message3"
     * and "message4", and where the 
     * supplied values go. This variant of showConfirm(filename) should 
     * be avoided because the dynamic messages won't be internationalized
     * properly.
     * @return either PRINT_OPTION or CANCEL_OPTIONN
     */
    public static int showPrintCancel(String filename, String message) {
        return standardDialog(filename, JOptionPane.WARNING_MESSAGE, 
            JOptionPane.OK_CANCEL_OPTION, OPTION2_PRINT_CANCEL, message, null, 
            null, null);
    }
    
    /** 
     * Show a print/cancel dialog built from an XML file, with four extra
     * message strings specified programmatically. The XML file must
     * have label components named "message", "message2", "message3"
     * and "message4", and where the 
     * supplied values go. This variant of showConfirm(filename) should 
     * be avoided because the dynamic messages won't be internationalized
     * properly.
     * @return either RETRY_OPTION or CANCEL_OPTIONN
     */
    public static int showPrintCancel(String filename, String message, 
    		String message2, String message3, String message4) {
        return standardDialog(filename, JOptionPane.WARNING_MESSAGE, 
            JOptionPane.OK_CANCEL_OPTION, OPTION2_PRINT_CANCEL, message, message2, 
            message3, message4);
    }
    
    /** 
     * Show a ok/cancel dialog built from an XML file, with four extra
     * message strings specified programmatically. The XML file must
     * have label components named "message", "message2", "message3"
     * and "message4", and where the 
     * supplied values go. This variant of showConfirm(filename) should 
     * be avoided because the dynamic messages won't be internationalized
     * properly.
     * @return either OK_OPTION or CANCEL_OPTIONN
     */
    public static int showConfirm(String filename, String message, 
    		String message2, String message3, String message4) {
        return standardDialog(filename, JOptionPane.WARNING_MESSAGE, 
            JOptionPane.OK_CANCEL_OPTION, message, message2, message3, message4);
    }

    /** 
     * Show an OK/Cancel dialog built from an XML file.
     * This is appropriate for situations where the user is being
     * asked to do something for the application's benefit. 
     * The usage of this method is intended to be similar to 
     * JOptionPane.showConfirmDialog().
     * @return either OK_OPTION, CANCEL_OPTION, or CLOSED_OPTION
     */
    public static int showRequest(String filename) {
        return standardDialog(filename, JOptionPane.PLAIN_MESSAGE, 
            JOptionPane.OK_CANCEL_OPTION, null, null, null, null);
    }
    /** 
     * Show a Yes/No dialog built from an XML file.
     * This is appropriate for situations where the app is asking
     * the user a question "out of the blue." Such situations are
     * best avoided, but they do happen. The usage of this method 
     * is intended to be similar to JOptionPane.showConfirmDialog().
     * @param filename the XML file describing the dialog
     * @return either YES_OPTION, NO_OPTION, or CLOSED_OPTION
     */
    public static int showQuestion(String filename) {
        return standardDialog(filename, JOptionPane.QUESTION_MESSAGE, 
            JOptionPane.YES_NO_OPTION, null, null, null, null);
    }
       
    /** 
     * Show a Yes/No dialog built from an XML file, with an extra
     * message string specified programmatically. The XML file must
     * have a label component named "message" where the message goes.
     * This variant of showQuestion(filename) should be avoided
     * because the dynamic message won't be internationalized 
     * properly. It is used for the exchange rate verification.
     */    
    public static int showQuestion(String filename, String message) {
        return standardDialog(filename, JOptionPane.QUESTION_MESSAGE, 
            JOptionPane.YES_NO_OPTION, message, null, null, null);
    }

    /** 
     * Show a user-input dialog. The top half of the dialog box is created
     * from an XML file, and the bottom half is created automatically by
     * JOptionPane. The idea is the top half contains message text to explain
     * the situation and prompt the user for input, while the bottom half
     * contains a list or a drop-down list of the choices. Unfortunately,
     * JOptionPane has its own algorithm for deciding between drop-down and
     * ordinary list, and there's no way to control it. The usage of this 
     * method is intended to be similar to JOptionPane.showInputDialog().
     * Returns null if the user clicks Cancel or closes the dialog.
     * Returns the chosen element of choices[] if the user clicks OK.
     */
    public static Object showInput(String filename, Object choices[], Object initial) {

        //Debug.println("showInput(" + filename + "," + choices + "," + initial + ")");

        XmlDefinedPanel panel = createPanel(filename, null, null, null, null);
        JOptionPane pane = new JOptionPane(panel, JOptionPane.PLAIN_MESSAGE, 
                JOptionPane.OK_CANCEL_OPTION, null, OK_CANCEL_OPTION, OK_BUTTON);
        pane.setWantsInput(true);
        pane.setSelectionValues(choices);
        pane.setInitialSelectionValue(initial);

        synchronized (dialogStack) { 
            push(pane.createDialog(getParent(), TITLE));
        }
        pane.selectInitialValue();
        showAndDispose();
        pop();
        panel.cleanup();

        Object result = pane.getInputValue();
        Debug.println("[" + result + "]");	 
        if (result == JOptionPane.UNINITIALIZED_VALUE) {
            return null;
        }
        return result;      
    }

    /**
     * Show a custom dialog. This is used for dialog screens that don't 
     * fit any of the above standard dialogs but can be defined as an 
     * XML panel plus code. 
     */
    public static void showPanel(XmlDialogPanel xdp) {  // was synchronized
    	showPanel(xdp, null);
    }
    
    /**
     * Show a custom dialog. This is used for dialog screens that don't 
     * fit any of the above standard dialogs but can be defined as an 
     * XML panel plus code. 
     */
    public static void showPanel(XmlDialogPanel xdp, Component cDefault) {  // was synchronized
        synchronized (xdp) {
//            Debug.println("showPanel(" + xdp + ")");	 
//            Debug.println("  showing = " + xdp.isShowing());	
//            Debug.println("  default = " + cDefault.getName());	
            if (xdp.isShowing()) {
                return;
            }
            if (xdp.showTitleInDebug()) {
                Debug.println("----- " + xdp.getTitle());	
            }
            XmlDefinedPanel panel = xdp.getMainPanel();
            JOptionPane pane = new JOptionPane(panel, 
                    JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION,
                    null, empty, null);
            xdp.setOptionPane(pane);
            synchronized (dialogStack) {
                JDialog d = pane.createDialog(getParent(), xdp.getTitle());
                xdp.setDialog(d);
                if (xdp instanceof CommunicationDialog) {
                    d.setName("commDlg"); 
                    //d.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
                }
                else if (xdp instanceof MOPrintingDialog) {
                    d.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
                    SwingUtilities.replaceUIInputMap(pane, JComponent.
                           WHEN_IN_FOCUSED_WINDOW, null);
                    SwingUtilities.replaceUIActionMap(pane, null);
                }
                push(d);
            }
            if (cDefault != null) {
                cDefault.requestFocus();
            }
            showAndDispose();
            pop();
            xdp.cleanup();
        }
        return;
        
    }
    
    /** 
     * Show the About dialog. The dialog is saved and reused so bringing it
     * up repeatedly has minimal effect on memory. (This makes the memory 
     * usage statistics shown by the dialog more stable and reliable.)
     */
    public static void showAboutDialog(String version) {
        if (aboutBox == null) {
            createAboutDialog(version);    
        }
        
        Runtime runtime = Runtime.getRuntime();
        long total = runtime.totalMemory();
        long free = runtime.freeMemory();
        long used = (total - free) / 1024;
        long alloc = (total / 1024);
        aboutBoxMemoryLabel.setText(used + " / " + alloc);

        Debug.println("----- About DeltaWorks");
        Debug.println("Version " + version + " mem " + used + " over " + alloc);
        push(aboutBox);
        aboutBox.setVisible(true);
        pop();

    }
    /**
     *  Deferred instanciation of About dialog, called from showAboutDialog(). 
     */
    private static void createAboutDialog(String version) {
        XmlDefinedPanel panel = new XmlDefinedPanel("dialogs/AboutBox.xml");    
        JLabel versionLabel = (JLabel) panel.getComponent("versionLabel");  
        versionLabel.setText(version);
        JOptionPane pane = new JOptionPane(panel,
            JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION,
            null, DEFAULT_OPTION, OK_BUTTON);
        String deviceId = "";
        deviceId += UnitProfile.getInstance().get("DEVICE_ID","");

        aboutBox = pane.createDialog(parentFrame, Messages.getString("Dialogs.1882") +"   " +deviceId); 
        aboutBoxMemoryLabel = (JLabel) panel.getComponent("memoryLabel");   	 
        
    }
    
    /** 
     * Called when the language has changed, so the cached About dialog needs
     * to be recreated. This method just discards it; the About dialog is 
     * created when needed by showAboutDialog().
     */
    public static void refreshAboutDialog() {
        aboutBox = null;
    }
    
    private static boolean passwordDialogActive = false;
    
    public static boolean getPasswordDialogActive() {
    	return passwordDialogActive;
    }

    /**
     * Show the name/password dialog. Returns the dialog object after 
     * the user has entered information and the dialog box has been 
     * closed. The actual dialog is disposed.
     */
    public static NamePasswordDialog showNamePasswordDialog(int loginType, 
            boolean nameRequired, boolean invalid) {
    	passwordDialogActive = true;
    	
        final NamePasswordDialog dialog = new NamePasswordDialog(loginType, 
            nameRequired, invalid);
        
        Dialogs.showPanel(dialog);
        
    	passwordDialogActive = false;
     	   
        return dialog;
    }
    
    /**
     * Show the change password dialog. Returns the dialog object after 
     * the user has entered information and the dialog box has been 
     * closed. The actual dialog is disposed.
     */
    public static ChangePasswordDialog showChangePasswordDialog(int userID, String password,ClientTransaction transaction) {

    	ChangePasswordDialog dialog = new ChangePasswordDialog(userID, password,transaction);
    	instance = dialog;
        Dialogs.showPanel(dialog);
        return dialog;
    }
    
    public static ChangePasswordDialog getInstance(){
       
            return instance;
        
    }
    

    /** 
     * Show the MGR transaction canceled dialogs. This is used when the
     * dispenser jams on the Deltagrams and the user cancels rather than 
     * reloading it.
     * @return either YES_OPTION, NO_OPTION, or CLOSED_OPTION
     */
    public static int showConfirmDgCancel(String refnum, String serial1, String serial2) {
        //Debug.println("showConfirmDgCancel(" + refnum + "," + serial1 + "," + serial2 + ")");
        String filename = "dialogs/DialogCancelTransaction.xml";	
        Debug.println("----- (" + filename + ")");	 
        XmlDefinedPanel panel = new XmlDefinedPanel(filename);

        ((JLabel) panel.getComponent("referenceNumberField")).setText(refnum);	
        ((JLabel) panel.getComponent("serialNumberOneField")).setText(serial1);	
        ((JLabel) panel.getComponent("serialNumberTwoField")).setText(serial2);	
        ((JPanel) panel.getComponent("serialNumberTwoBox")).setVisible(! serial2.equals("0"));	 
                
        JOptionPane pane = new JOptionPane(panel, 
                JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION,
                null, YES_NO_OPTION, YES_BUTTON);

        synchronized (dialogStack) {
            push(pane.createDialog(getParent(), TITLE));
        }
        showAndDispose();
        pop();
        panel.cleanup();

        return getResult(pane);
    }

    public static int showContinueAbort(String filename) {

        XmlDefinedPanel panel = new XmlDefinedPanel(filename);
               
        JOptionPane pane = new JOptionPane(panel, 
                JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION,
                null, CONT_ABORT_OPTION, CONTINUE_BUTTON);

        synchronized (dialogStack) {
            push(pane.createDialog(getParent(), TITLE));
        }
        showAndDispose();
        pop();
        panel.cleanup();

        return getResult(pane);
    }
    
    
    /** 
     * Removes the top dialog. Called by something that implements 
     * IdleTimeoutListener in response to idle timeout events.
     * Question: should this should close all dialogs?
     */
    public static void idleTimeout() {
        if (getCurrentDialog() != null) {
            // This causes show() to return. The code that called show()
            // is responsible for dispose() and pop().
            getCurrentDialog().setVisible(false);
        }
        // Can/should there be code to hide the previous dialog?
    }
    
    /** 
     * Called from main menu when transaction is completed so there aren't 
     * any leftover dialogs hanging around. 
     */
    public static void closeAllDialogs() {
        if (getCurrentDialog() != null) {
            // This causes show() to return. The code that called show()
            // is responsible for dispose() and pop().
            getCurrentDialog().setVisible(false);

            SwingUtilities.invokeLater(new Runnable() {
                @Override
				public void run() {
                    closeAllDialogs();
                }
            });
        }
    }
    
    /* ********************************************************************* */
    

    /**
     * Creates a panel, given a filename. Convenience method for
     * the public static methods that take an XML filename.
     */
    private static XmlDefinedPanel createPanel(String filename, 
            String message, String message2, String message3, String message4) {
        
        Debug.println("----- (" + filename + ")");	 
        XmlDefinedPanel panel = new XmlDefinedPanel(filename);
        //panel.addReturnTabListeners();    // is this needed for anything?

        if ((message != null) && (panel.getComponent("message") != null)) {	
            ((JLabel) panel.getComponent("message")).setText(message);	
            Debug.println(message);
        }
        
        if ((message2 != null) && (panel.getComponent("message2") != null)) {	
            ((JLabel) panel.getComponent("message2")).setText(message2);	
            Debug.println(message2);
        }
        
        if ((message3 != null) && (panel.getComponent("message3") != null)) {	
            ((JLabel) panel.getComponent("message3")).setText(message3);	
            Debug.println(message3);
        }
        
        if ((message4 != null) && (panel.getComponent("message4") != null)) {	
            ((JLabel) panel.getComponent("message4")).setText(message4);	
            Debug.println(message4);
        }
        
        return panel;
    }

    /** 
     * Maintain current and previous dialogs like a really small stack. 
     * A call to this method implies the caller intends to display another
     * dialog on top of the current one. Care must be taken if the current
     * dialog is the comm dialog because the comm dialog has the ability 
     * to close itself asynchronously, and it will close any child dialogs
     * when it closes. //HERE
     */
    private static void push(JDialog c) {
        synchronized (dialogStack) {
            JDialog currentDialog = getCurrentDialog();
            if ((currentDialog != null) && (currentDialog.getName().equals("commDlg"))) {	
                specialDialogCounter = MessageFacade.specialKeepDialogOpen();
            }
            dialogStack.push(c);
        }
    }

    /** 
     * Maintain current and previous dialogs like a really small stack. 
     * The previous current dialog becomes the current dialog again. 
     */
    private static void pop() {
        synchronized (dialogStack) {
            dialogStack.pop();

            JDialog currentDialog = getCurrentDialog();
            if ((currentDialog != null) && (currentDialog.getName().equals("commDlg"))) {	
                MessageFacade.specialCloseDialog(specialDialogCounter);
            }
        }
    }

    /**  Returns the top-most dialog, or null if none. */
    public static JDialog getCurrentDialog() {
        synchronized (dialogStack) {
            if (dialogStack.empty()) {
                return null;
            }
            else {
                return dialogStack.peek();
            }
        }
    }
    
    /** Convenience method for common code. */
    private static void showAndDispose() {
        JDialog currentDialog = getCurrentDialog();
        if (currentDialog.getTitle().equalsIgnoreCase(Messages.getString("NamePasswordDialog.1890"))){
	        try {
				if (MainPanelDelegate.getIsiFrontCommandOccurred()){
					MainPanelDelegate.executeIsiFrontCommand();
					MainPanelDelegate.setIsiFrontCommandOccurred(false);
				}
			} catch (RuntimeException e) {
			}
        }
        currentDialog.setVisible(true);
        currentDialog.dispose();
    }

    /** 
     * Convenience method for common code. Gets the option pane result
     * as an integer indicating which button was pressed to close the
     * dialog. Only works for standard dialogs, others will return
     * CLOSED_OPTION. Also writes a line to the debug log.
     */
    private static int getResult(JOptionPane pane) {
        // See javadoc for JOptionPane for the following pattern.
        int result = CLOSED_OPTION;
        Object value = pane.getValue();
        if ((value != null) && (value instanceof Integer)) {
            result = ((Integer) value).intValue();

            if ((result >= -1) && (result < 2)) 
                Debug.println(optionString[result + 1]);
            else 
                Debug.println("[" + result + "]");	
        }
        else if ((value != null) && (value instanceof Option)) {
            result = ((Option) value).value;
            Debug.println(((Option) value).debug);
        }
        else {
            Debug.println("[CLOSED]");
        }
        return result;
    }

    /** Convenience method for the common cases. */
    private static int standardDialog(String filename, int msgType, 
            int optionType, String message, String message2, String message3, String message4) {
        
        //Debug.println("standardDialog(" + filename + "," + type + "," + option + ","
        //        + message + "," + message2 + ")");	 
        
        XmlDefinedPanel panel = createPanel(filename, message, message2, message3, message4);
        //JOptionPane pane = new JOptionPane(panel, msgType, optionType);
        JOptionPane pane = new JOptionPane(panel, msgType, optionType, null, 
                OPTIONS[optionType + 1], OPTIONS[optionType + 1][0]);
        
        synchronized (dialogStack) {
            push(pane.createDialog(getParent(), TITLE));
        }
        showAndDispose();
        pop();
        panel.cleanup();

        return getResult(pane);        
    }

    /** Convenience method for the common cases. */
    private static int standardDialog(String filename, int msgType, 
            int optionType1, int optionType2, String message, String message2, 
            String message3, String message4) {
        
        //Debug.println("standardDialog(" + filename + "," + msgType + "," + optionType1 + ","
        //        optionType2 + "," + message + "," + message2 + ")");	
        
        XmlDefinedPanel panel = createPanel(filename, message, message2, message3, message4);
        //JOptionPane pane = new JOptionPane(panel, msgType, optionType);
        JOptionPane pane = new JOptionPane(panel, msgType, optionType1, null, 
                OPTIONS[optionType2], OPTIONS[optionType2][0]);
        
        synchronized (dialogStack) {
            push(pane.createDialog(getParent(), TITLE));
        }
        showAndDispose();
        pop();
        panel.cleanup();

        return getResult(pane);        
    }

/*
    public static void main(String args[]) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            dw.main.DeltaworksMainPanel.fixFonts();

            ManagerTranOverrideDialog dialog = new ManagerTranOverrideDialog(
                    null, "3000", false, false);	
            dialog.show();

            ManagerTranOverrideDialog dialog = new ManagerTranOverrideDialog(
                    "3000", false, false);	

            
            NamePasswordDialog dialog = new NamePasswordDialog(
                    NamePasswordDialog.DUAL_CONTROL_LOGIN, true, false);
            Dialogs.showPanel(dialog);
            Debug.println("name = " + dialog.getName() + " password = " + dialog.getPassword());/	 

            
            Object result = showInput("dialogs/DialogEnterFourDigits2.xml", null, ""); 	 
            Debug.println(result);


            VerifyAnswerDialog dialog = new VerifyAnswerDialog("purple");
            Dialogs.showPanel(dialog);
            Debug.println("result = " + dialog.isCorrect());	

            
            dw.moneyorder.MOPrintingDialog dialog 
                = new dw.moneyorder.MOPrintingDialog(true);
                
            dw.dispenserload.DispenserLoadPrintingDialog dialog 
                = new dw.dispenserload.DispenserLoadPrintingDialog();
            dialog.begin(3);
            dialog.item(2, "996421989", new java.math.BigDecimal("25.00"));	 
            Dialogs.showPanel(dialog);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        System.exit(0);
    }
*/  

    public static void main(String args[]) {
        try {
            if (args.length == 0) {
                Debug.println("usage: Dialogs [info|warning] filename.xml");	
            }

            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
           // dw.main.DeltaworksMainPanel.fixFonts();
            DeltaworksStartup.fixFonts();
            
            if ("info".equals(args[0])) { 
                showInformation(args[1]);
            }
            else if ("warning".equals(args[0])) { 
                showWarning(args[1]);
            }
            else if ("error".equals(args[0])) { 
                showError(args[1]);
            }
            else if ("question".equals(args[0])) { 
                showQuestion(args[1]);
            }
//            else if ("ctr".equals(args[0])) {
//                showPanel(new CTRDialog(args[1]));
//            }
            else {
                Debug.println("unrecognized argument: " + args[0]);	
            }
        }
        catch (Exception e) {
            Debug.printStackTrace(e);
        }
        System.exit(0);
    }
}
