package dw.dialogs;

import javax.swing.JTextField;

import dw.framework.XmlDialogPanel;

public class VerifyAnswerDialog extends XmlDialogPanel {
    private JTextField answerField = null;
    private boolean correct = false;

    public VerifyAnswerDialog(String answer) {
        super("dialogs/DialogVerifyAnswer.xml");	
        //this.title = "Verify test answer";
        answerField = (JTextField) mainPanel.getComponent("answerField");	
        answerField.setText(answer);
        mainPanel.addActionListener("yesButton", this, "yes");	 
        mainPanel.addActionListener("noButton", this, "no");	 
    }

    public void yes() {
        correct = true;
        closeDialog();
    }

    public void no() {
        correct = false;
        closeDialog();
    }

    public boolean isCorrect() {
        return correct;
    }
}
