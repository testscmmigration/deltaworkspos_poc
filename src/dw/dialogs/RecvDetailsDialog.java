package dw.dialogs;

import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.math.BigDecimal;

import javax.swing.JButton;
import javax.swing.JLabel;

import com.moneygram.agentconnect.FeeInfo;

import dw.framework.XmlDialogPanel;
import dw.i18n.Messages;
import dw.printing.Report;

public class RecvDetailsDialog extends XmlDialogPanel {

	private boolean accepted = false;
	private boolean canceled = false;
	private final FeeInfo feeInfo;

	private JButton okButton = null;

	public RecvDetailsDialog(FeeInfo pm_feeInfo) {
		super("dialogs/DialogRecvDetails.xml"); 
		this.title = Messages.getString("DialogRecvDetails.text"); 
		this.feeInfo = pm_feeInfo;
		
		mainPanel.addActionListener("okButton", this, "yes");  

		okButton = (JButton) mainPanel.getComponent("okButton");

		final KeyListener activator = new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					okButton.doClick();
				}
			}
		};

		mainPanel.addKeyListener("okButton", activator);

		setCustomText();
	}

	public void cancel() {
		canceled = true;
		closeDialog();
	}

	/** Returns true if user clicked OK. */
	public boolean isAccepted() {
		return accepted;
	}

	/** Returns true if user clicked Cancel. */
	public boolean isCanceled() {
		return canceled;
	}

	public void no() {
		closeDialog();
	}

	public void setCustomText() {
		final JLabel deliveryOption = (JLabel) mainPanel
				.getComponent("deliveryOption");
		final JLabel origAmtField = (JLabel) mainPanel
				.getComponent("origAmtField");
		final JLabel finalAmtField = (JLabel) mainPanel
				.getComponent("finalAmtField");
		final JLabel recvDetailFeeAmtField = (JLabel) mainPanel
				.getComponent("recvDetailFeeAmtField");
		final JLabel recvDetailTaxAmtField = (JLabel) mainPanel
				.getComponent("recvDetailTaxAmtField");

		deliveryOption.setText(feeInfo.getServiceOptionDisplayName());
		
		BigDecimal sTemp = feeInfo.getReceiveAmounts().getTotalReceiveFees();
		if ((sTemp != null)) {
			recvDetailFeeAmtField.setText("-" + sTemp);
			recvDetailFeeAmtField.setForeground(Color.RED);
		} else {
			mainPanel.removeComponent("recvDetailFeePanel");
		}
		
		sTemp = feeInfo.getReceiveAmounts().getTotalReceiveTaxes();
		if ((sTemp != null)) {
			recvDetailTaxAmtField.setText("-" + sTemp);
			recvDetailTaxAmtField.setForeground(Color.RED);
		} else {
			mainPanel.removeComponent("recvDetailTaxPanel");
		}
		
		/*
		 * if in demo mode, we might not have the new field with the final
		 * amt, so assume the original amt is the same as the final amt.
		 */
		String sOrigAmt = feeInfo.getReceiveAmounts().getReceiveAmount().toString();
		String sFinalAmt = feeInfo.getReceiveAmounts().getTotalReceiveAmount().toString();
		if (!Report.notNull(sFinalAmt)) {
			sFinalAmt = sOrigAmt;
		}
		
		origAmtField.setText(sOrigAmt);
		finalAmtField.setText(sFinalAmt);
		
	}

	public void yes() {
		accepted = true;
		closeDialog();
	}

}
