package dw.dialogs;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;

import dw.dwgui.MultiListComboBox;
import dw.framework.XmlDialogPanel;
import dw.utility.DWValues;

/**
 * Class for a dialog which prompts the user to select a primary receipt 
 * language
 * 
 * @author w156
 */
public class Receipt1stLanguageDialog extends XmlDialogPanel {
    private MultiListComboBox<String> receiptLanguageComboBox;
	private boolean accepted = false;
	private boolean canceled = false;
    
	private JButton okButton = null;
//	private JButton cancelButton = null;
	private String[] lLanguageList;
	private String s1stLanguage = "";
	private String s1stLanguageMnemonic = "";
	private int i1stLanguageIdx;
	private String[] s1stLanguageMnemonicArray;
	
	@SuppressWarnings("unchecked")
	public Receipt1stLanguageDialog(List<String> pm_lLanguageList, int pm_iDefaultIdx) {
		super("dialogs/DialogReceipt1stLanguage.xml"); 
		s1stLanguageMnemonicArray = pm_lLanguageList.toArray(new String[0]);
		i1stLanguageIdx = pm_iDefaultIdx;
		lLanguageList = new String[pm_lLanguageList.size()];
		int ii = 0;
        Iterator<String> i = pm_lLanguageList.iterator();
        while (i.hasNext()) {
            String sLan = i.next();
			if ("NONE".equals(sLan)) {
				lLanguageList[ii] = sLan;
			} else {
				lLanguageList[ii] = DWValues.getLanguageDisplayName(sLan);
			}
            ii++;
        }
		receiptLanguageComboBox = (MultiListComboBox<String>) mainPanel.getComponent("receiptLanguageComboBox");
        mainPanel.addActionListener("receiptLanguageComboBox", this, "languageAction");
        receiptLanguageComboBox.addList(lLanguageList, "Languages");
		mainPanel.addActionListener("okButton", this, "yes");  
		receiptLanguageComboBox.setSelectedIndex(pm_iDefaultIdx);
		okButton = (JButton) mainPanel.getComponent("okButton");

		final KeyListener activator = new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					okButton.doClick();
				}
			}
		};

		mainPanel.addKeyListener("okButton", activator);
	}
	
	public void cancel() {
		canceled = true;
		closeDialog();
	}

	/** Returns true if user clicked OK. */
	public boolean isAccepted() {
		return accepted;
	}

	/** Returns true if user clicked Cancel. */
	public boolean isCanceled() {
		return canceled;
	}

    public void languageAction() {
    	s1stLanguage = (String) receiptLanguageComboBox.getSelectedItem();
    	i1stLanguageIdx = receiptLanguageComboBox.getSelectedIndex();
    	s1stLanguageMnemonic = s1stLanguageMnemonicArray[i1stLanguageIdx];
     }
     
	public void no() {
		closeDialog();
	}

	public void yes() {
		accepted = true;
		closeDialog();
	}
	
	public int getLanguageIdx() {
		return i1stLanguageIdx;
	}

	public String getLanguage() {
		return s1stLanguage;
	}

	public String getLanguageMnemonic() {
		if (s1stLanguageMnemonic.compareToIgnoreCase(DWValues.MNEM_NONE) == 0) {
			return "";
		} else {
			return s1stLanguageMnemonic;
		}
	}
}
