package dw.dialogs;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;

import dw.dwgui.MultiListComboBox;
import dw.framework.XmlDialogPanel;
import dw.utility.DWValues;

/**
 * Class for a dialog which prompts the user to select a secondary receipt 
 * language
 * 
 * @author w156
 */
public class Receipt2ndLanguageDialog extends XmlDialogPanel {
    private MultiListComboBox<String> receiptLanguageComboBox;
	private boolean accepted = false;
	private boolean canceled = false;
    
	private JButton okButton = null;
//	private JButton cancelButton = null;
	private String[] lLanguageList;
	private String s2ndLanguage = "";
	private String s2ndLanguageMnemonic = "";
	private int i2ndLanguageIdx;
	private String[] s2ndLanguageMnemonicArray;
	
	@SuppressWarnings("unchecked")
	public Receipt2ndLanguageDialog(List<String> pm_lLanguageList, int pm_iDefaultIdx) {
		super("dialogs/DialogReceipt2ndLanguage.xml"); 
		s2ndLanguageMnemonicArray = pm_lLanguageList.toArray(new String[0]);
		i2ndLanguageIdx = pm_iDefaultIdx;
		lLanguageList = new String[pm_lLanguageList.size()];
		int ii = 0;
        Iterator<String> i = pm_lLanguageList.iterator();
        while (i.hasNext()) {
            String sLan = (String) i.next();
            lLanguageList[ii] = DWValues.getLanguageDisplayName(sLan);
            ii++;
        }
		receiptLanguageComboBox = (MultiListComboBox<String>) mainPanel.getComponent("receiptLanguageComboBox");
        mainPanel.addActionListener("receiptLanguageComboBox", this, "languageAction");
        receiptLanguageComboBox.addList(lLanguageList, "Languages");
		mainPanel.addActionListener("okButton", this, "yes");  
		receiptLanguageComboBox.setSelectedIndex(pm_iDefaultIdx);
		okButton = (JButton) mainPanel.getComponent("okButton");

		final KeyListener activator = new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					okButton.doClick();
				}
			}
		};

		mainPanel.addKeyListener("okButton", activator);
	}
	
	public void cancel() {
		canceled = true;
		closeDialog();
	}

	/** Returns true if user clicked OK. */
	public boolean isAccepted() {
		return accepted;
	}

	/** Returns true if user clicked Cancel. */
	public boolean isCanceled() {
		return canceled;
	}

    public void languageAction() {
    	s2ndLanguage = (String) receiptLanguageComboBox.getSelectedItem();
    	i2ndLanguageIdx = receiptLanguageComboBox.getSelectedIndex();
    	s2ndLanguageMnemonic = s2ndLanguageMnemonicArray[i2ndLanguageIdx];
     }
     
	public void no() {
		closeDialog();
	}

	public void yes() {
		accepted = true;
		closeDialog();
	}
	
	public int getLanguageIdx() {
		return i2ndLanguageIdx;
	}

	public String getLanguage() {
		return s2ndLanguage;
	}

	public String getLanguageMnemonic() {
		if (s2ndLanguageMnemonic.compareToIgnoreCase(DWValues.MNEM_NONE) == 0) {
			return "";
		} else {
			return s2ndLanguageMnemonic;
		}
	}

}
