package dw.dialogs;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;

import dw.i18n.Messages;
import dw.io.report.ReportSelectionCriteria;
import dw.printing.Report;
import dw.printing.TEPrinter;
import dw.profile.UnitProfile;

public class DialogReferenceNumberText extends ReportDialog {
	private JTextArea text2;
	private JLabel referenceNumber;
	private JButton okButton;
	
	private static class TransactionMessageReport extends Report {
		private TransactionMessageReport(String referenceNumberText, String messageText) {
			super();
			initialize(TEPrinter.getPrinter());
			this.fieldMap.clear();
			addFlag("training", UnitProfile.getInstance().isTrainingMode());
			addField("referenceNumber", referenceNumberText);
			addField("message", messageText);
		}
	}

	public DialogReferenceNumberText(String referenceNumberText, String messageText) {
		super("dialogs/DialogReferenceNumberText.xml", new TransactionMessageReport(referenceNumberText, messageText), Messages.getString("DialogReferenceNumberText.4"), false, false, false, false, false);
		
		cancelButton = new JButton();
		
		this.referenceNumber = (JLabel) mainPanel.getComponent("referenceNumber");
		text2 = (JTextArea) mainPanel.getComponent("text2");
		text2.setLineWrap(true);
		text2.setWrapStyleWord(true);
		text2.setBackground(referenceNumber.getBackground());
		text2.setColumns(25);
		text2.setEditable(false);
		okButton = (JButton) mainPanel.getComponent("okButton");

		text2.setText(referenceNumberText);
		
		this.referenceNumber.setText(messageText);
		
		report.setXmlFileName("xml/reports/referenceNumberText.xml");
		
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				closeDialog();
			}
		});
		
		this.rsc = new ReportSelectionCriteria();
		
		mainPanel.setPreferredSize(new Dimension(400, 150));
	}
}
