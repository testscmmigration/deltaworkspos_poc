package dw.dialogs;

import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.math.BigDecimal;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;

import com.moneygram.agentconnect.AmountInfo;
import com.moneygram.agentconnect.FeeInfo;
import com.moneygram.agentconnect.PromotionInfo;

import dw.framework.ClientTransaction;
import dw.framework.XmlDialogPanel;
import dw.i18n.Messages;
import dw.mgsend.MoneyGramSendTransaction;
import dw.model.adapters.PromoCodeInfo;
import dw.printing.Report;
import dw.profile.UnitProfile;
import dw.utility.DWValues;
import dw.utility.StringUtility;

public class FeeDetailsDialog extends XmlDialogPanel {

	private boolean accepted = false;
	private boolean canceled = false;
	private final FeeInfo feeInfo;
	private final String[] enteredPromoCodes;

	private JButton okButton = null;

	private PromoCodeInfo[] returnedPromoCodeInfo = null;

	public FeeDetailsDialog(FeeInfo pm_feeInfo,
			String[] pm_enteredPromoArray) {
		super("dialogs/DialogFeeDetails.xml"); 
		this.title = Messages.getString("DialogFeeDetails.text"); 
		this.feeInfo = pm_feeInfo;
		if (pm_enteredPromoArray != null) {
			this.enteredPromoCodes = pm_enteredPromoArray;
		} else {
			this.enteredPromoCodes = new String[0];
		}
		
		mainPanel.addActionListener("okButton", this, "yes");  

		okButton = (JButton) mainPanel.getComponent("okButton");

		final KeyListener activator = new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					okButton.doClick();
				}
			}
		};

		mainPanel.addKeyListener("okButton", activator);
		List<PromotionInfo> tList  = feeInfo.getPromotionInfos().getPromotionInfo();
		returnedPromoCodeInfo = new PromoCodeInfo[tList.size()];
		int i = 0;
		for (PromotionInfo promoInfo : tList) {
			PromoCodeInfo pcInfo = new PromoCodeInfo();
			pcInfo.setPromotionCode(promoInfo.getPromotionCode());
			pcInfo.setPromotionDiscountId(promoInfo.getPromotionDiscountId());
			pcInfo.setPromotionCategoryId(promoInfo.getPromotionCategoryId());
			pcInfo.setPromotionDiscount(promoInfo.getPromotionDiscount());
			pcInfo.setPromotionDiscountAmount(promoInfo.getPromotionDiscountAmount());
			pcInfo.setPromotionErrorCode(promoInfo.getPromotionErrorCode());
			pcInfo.setPromotionErrorMessages(promoInfo.getPromotionErrorMessages());
			returnedPromoCodeInfo[i++] = pcInfo;
		}
		setCustomText();
	}

	public void cancel() {
		canceled = true;
		closeDialog();
	}

	/** Returns true if user clicked OK. */
	public boolean isAccepted() {
		return accepted;
	}

	/** Returns true if user clicked Cancel. */
	public boolean isCanceled() {
		return canceled;
	}

	public void no() {
		closeDialog();
	}
	
	private BigDecimal getDetailSendItemAmt(FeeInfo feeInfo, String amountType) {
		if (feeInfo.getSendAmounts() != null) {
			List<AmountInfo> amounts = feeInfo.getSendAmounts().getDetailSendAmounts().getDetailSendAmount();
			if (amounts != null) {
				for (AmountInfo amountInfo : amounts) {
					if (amountInfo.getAmountType().equalsIgnoreCase(amountType)) {
						return amountInfo.getAmount();
					}
				}
			}
		}
		return null;
	}

	public void setCustomText() {
		final JLabel deliveryOption = (JLabel) mainPanel
				.getComponent("deliveryOption");
		final JLabel origFeeField = (JLabel) mainPanel
				.getComponent("origFeeField");
		final JLabel taxField = (JLabel) mainPanel.getComponent("taxField");
		final JLabel nonMgiFeeField = (JLabel) mainPanel.getComponent("nonMgiFeeField");
		final JLabel loyaltyField = (JLabel) mainPanel.getComponent("loyaltyField");
		final JLabel finalFeeField = (JLabel) mainPanel
				.getComponent("finalFeeField");
		boolean errorsPresent = false;

		deliveryOption.setText(feeInfo.getServiceOptionCategoryDisplayName());
		BigDecimal sOrigFee = getDetailSendItemAmt(feeInfo, DWValues.MF_SEND_DETAIL_ORIG_MGI_SEND_FEE);
		
		/*
		 * if in demo mode, we might not have the new field with the original
		 * fee, so assume the original fee is the same as the final fee.
		 */
		if (sOrigFee==null) {
			sOrigFee = getDetailSendItemAmt(feeInfo, DWValues.MF_SEND_DETAIL_TOTAL_SEND_FEE_TAX);
		}
		
		for (int ii = 0; ii < returnedPromoCodeInfo.length; ii++) {
			if (!returnedPromoCodeInfo[ii].hasError()) {
				if (returnedPromoCodeInfo[ii].isDisplayablePromoCode()) {
					final String sNameFieldText = "promoCodeName" + (ii + 1)
							+ "Field";
					final String sAmtFieldText = "promoCodeAmt" + (ii + 1)
							+ "Field";
					final JLabel promoCodeNameField = (JLabel) mainPanel
							.getComponent(sNameFieldText);
					final JLabel promoCodeAmtField = (JLabel) mainPanel
							.getComponent(sAmtFieldText);
					promoCodeNameField.setText(returnedPromoCodeInfo[ii]
							.getPromotionCode());
					promoCodeAmtField.setText("-"
							+ returnedPromoCodeInfo[ii].getPromotionDiscountAmount());
					promoCodeAmtField.setForeground(Color.BLUE);
				} else {
					final String sPanelText = "promoCodePanel" + (ii + 1);
					mainPanel.removeComponent(sPanelText);
				}
			} else {
				errorsPresent = true;
				final String sPanelText = "promoCodePanel" + (ii + 1);
				mainPanel.removeComponent(sPanelText);
			}
		}
		for (int ii = returnedPromoCodeInfo.length; ii < 5; ii++) {
			final String sPanelText = "promoCodePanel" + (ii + 1);
			mainPanel.removeComponent(sPanelText);
		}
		
		final BigDecimal bdTaxAmt = feeInfo.getSendAmounts().getTotalSendTaxes();
		if (bdTaxAmt.compareTo(ClientTransaction.ZERO) > 0) {
			taxField.setText("+" + bdTaxAmt);
			taxField.setForeground(Color.RED);
		} else {
			mainPanel.removeComponent("taxPanel");
		}
		
		final BigDecimal sFee = getDetailSendItemAmt(feeInfo, DWValues.MF_SEND_DETAIL_OTHER_SEND_FEE);
		if (sFee!=null && sFee.equals(BigDecimal.ZERO)) {
			nonMgiFeeField.setText("+" + sFee);
			nonMgiFeeField.setForeground(Color.RED);
		} else {
			mainPanel.removeComponent("nonMgiFeePanel");
		}
		
		boolean hasPromotionInfo = feeInfo.getPromotionInfos() != null;
		List<PromotionInfo> promoList = hasPromotionInfo ? feeInfo.getPromotionInfos().getPromotionInfo()
				: new java.util.ArrayList<PromotionInfo>();
		boolean hasLoyaltyAmt = returnedPromoCodeInfo.length > 0 && !promoList.isEmpty()
				&& MoneyGramSendTransaction.getLoyaltyDiscAmt(feeInfo.getPromotionInfos().getPromotionInfo()) != null;

		final String sLoyalty = hasLoyaltyAmt ? StringUtility.getNonNullString(
				MoneyGramSendTransaction.getLoyaltyDiscAmt(feeInfo.getPromotionInfos().getPromotionInfo()).toString())
				: "";

		if (DWValues.nonZero(sLoyalty)) {
			loyaltyField.setText("-" + sLoyalty);
			loyaltyField.setForeground(Color.BLUE);
		} else {
			mainPanel.removeComponent("loyaltyPanel");
		}
		
		finalFeeField.setText(String.valueOf(getDetailSendItemAmt(feeInfo, DWValues.MF_SEND_DETAIL_TOTAL_SEND_FEE_TAX)));
		origFeeField.setText(sOrigFee.toString());
		
		/*
		 * if in DemoMode then only one entered promo code is returned, so
		 * generate errors for the rest.
		 */
		int iEnteredCodeCount = enteredPromoCodes.length;
		int iReturnedCodeCount = returnedPromoCodeInfo.length;
		int iExtraCodeCount=0;
		
		String[] sExtraCodes = new String[iEnteredCodeCount];
		if (UnitProfile.getInstance().isDemoMode()) {
			for (int ii=0; ii < iEnteredCodeCount; ii++) {
				boolean bFound = false;
				for (int jj = 0; jj < iReturnedCodeCount; jj++) {
					if (enteredPromoCodes[ii].compareToIgnoreCase(returnedPromoCodeInfo[jj].getPromotionCode()) == 0){
						bFound = true;
					}
				}
				if (!bFound) {
					sExtraCodes[iExtraCodeCount++] = enteredPromoCodes[ii];
				}
			}
		}

		if (errorsPresent || iExtraCodeCount > 0) {
			for (int ii = 0; ii < iReturnedCodeCount; ii++) {
				final String sErrCode = returnedPromoCodeInfo[ii]
						.getPromotionErrorCode();
				if (Report.notNull(sErrCode)) {
					final String sNameFieldText = "promoErrName" + (ii + 1)
							+ "Field";
					final String sErrorFieldText = "promoErrText" + (ii + 1)
							+ "Field";
					final JLabel promoCodeNameField = (JLabel) mainPanel
							.getComponent(sNameFieldText);
					final JLabel promoCodeErrorField = (JLabel) mainPanel
							.getComponent(sErrorFieldText);
					promoCodeNameField.setText(returnedPromoCodeInfo[ii]
							.getPromotionCode());
					promoCodeErrorField.setText(returnedPromoCodeInfo[ii]
							.getPromotionErrorMsg(UnitProfile.getInstance().get("LANGUAGE","en")));
				} else {
					final String sPanelText = "promoErrCodePanel" + (ii + 1);
					mainPanel.removeComponent(sPanelText);
					errorsPresent = true;
				}
			}
			for (int ii = 0; ii < iExtraCodeCount; ii++) {
					final String sNameFieldText = "promoErrName" + (iReturnedCodeCount + ii + 1)
							+ "Field";
					final String sErrorFieldText = "promoErrText" + (iReturnedCodeCount + ii + 1)
							+ "Field";
					final JLabel promoCodeNameField = (JLabel) mainPanel
							.getComponent(sNameFieldText);
					final JLabel promoCodeErrorField = (JLabel) mainPanel
							.getComponent(sErrorFieldText);
					promoCodeNameField.setText(sExtraCodes[ii]);
					promoCodeErrorField.setText(DWValues.PROMO_NOT_FOUND_TEST);
				
			}
			for (int ii = iReturnedCodeCount+iExtraCodeCount; ii < 5; ii++) {
				final String sPanelText = "promoErrCodePanel" + (ii + 1);
				mainPanel.removeComponent(sPanelText);
			}
		} else {
			mainPanel.removeComponent("promoErrorPanel");
		}
	}

	public void yes() {
		accepted = true;
		closeDialog();
	}

}
