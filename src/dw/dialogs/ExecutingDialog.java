package dw.dialogs;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

import dw.framework.XmlDialogPanel;
import dw.utility.ExecuteCommand;
import dw.utility.ExtraDebug;

/**
 * Dialog box to go along with the ExecuteCommand class
 */

public class ExecutingDialog extends XmlDialogPanel {
	private static ExecutingDialog instance = null;

	private JTextArea messageTextArea;
	private JButton cancelButton;
	private boolean dontHide = false;

	private static String defaultDialogTitle = Dialogs.TITLE;
	public final static int CANCEL = 2;

	static {
		instance = new ExecutingDialog();
	}

	/**
	 * Returns the singleton instance.
	 */
	public static ExecutingDialog getInstance() {
		return instance;
	}

	/**
	 * refresh the dialog and create a new instance when the toggle button is
	 * selected.
	 */
	public static void refreshDialog() {
		instance = new ExecutingDialog();
	}

	/**
	 * Clears the message text area.
	 */
	public static void resetMessageLabels() {
		instance.messageTextArea.setText("");
	}

	/**
	 * Called by facade to tell user what we're doing.
	 * 
	 * @param activity
	 *            - String containing activity to put in the messageTextArea
	 */
	public static void setActivity(String activity) {
		ExtraDebug.println("-x-setActivity[" + activity + "]"); 
		getInstance().messageTextArea.setText(activity + ".");
	}

	/**
	 * setMessageLabels(String) will accept a long strings and parse it into
	 * four 40 or less character strings. The break is based on the first space
	 * prior to the 40th character.
	 */
	public static void setMessageLabels(String message1) {
		setTextArea(message1);
	}

	/**
	 * Sets the messageTextArea to the specified text.
	 * 
	 * @param message1
	 *            - String containing initial text
	 * @param message2
	 *            - String containing additional text starting on its own line.
	 */
	public static void setMessageLabels(String message1, String message2) {

		setTextArea(message1 + " \n\r" + message2);
	}

	/**
	 * Called by our listener and by forwarder to show progress.
	 * 
	 * @param status
	 *            - String containing status to put in the messageTextArea
	 */
	public static void setStatus(String status) {
		ExtraDebug.println("-x-setStatus[" + status + "]"); 
		getInstance().messageTextArea.setText(status);
	}

	/**
	 * Sets the messageTextArea to the specified text.
	 * 
	 * @param text
	 *            - String containing text for the message text area.
	 */
	public static void setTextArea(String text) {
		instance.messageTextArea.setText(text);
	}

	/**
	 * private constructor
	 */
	private ExecutingDialog() {
		super("dialogs/DialogExecuting.xml"); 
		setup();
	}

	/**
	 * Action method called by the cancel button. Tell the activator we don't
	 * need the connection anymore and hide the dialog.
	 */
	public void cancel() {
		ExecuteCommand.interrupt();
		closeDialog();
	}

	/**
	 * Override superclass cleanup method to do nothing. Called by Dialogs.java
	 * when enclosing JDialog is disposed. Since this singleton instance is
	 * reused, we don't do any cleanup of the XmlDefinedPanel.
	 */
	@Override
	public void cleanup() {
		// Check if [X] pressed on the dialog
		if (getOptionPane() != null) {
			final JOptionPane pane = getOptionPane();
			final Object value = pane.getValue();
			if (value == null) {
				cancel();
			}
		}
		resetDialog();
	}

	/**
	 * Close the dialog immediately.
	 */
	@Override
	public void closeDialog(int option) {
		resetDialog();
		super.closeDialog(option);
	}

	public boolean getDontHideFlag() {
		return dontHide;
	}

	/**
	 * Resets labels/messages to default values
	 */
	public void resetDialog() {
		resetMessageLabels();
		setTitle(defaultDialogTitle);
	}

	public void setDontHideFlag(boolean b) {
		dontHide = b;
	}

	/**
	 * for compatibility
	 */
	public void setEnabled(boolean enabled) {
		mainPanel.setEnabled(enabled);
	}

	/**
	 * Sets the dialog title. For the sake of consistency, this should almost
	 * never be called. The only valid use of this method is to set the
	 * scheduled transmit and daily transmit titles from DMP.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Returns false to tell Dialogs.java not to log the title every time this
	 * dialog is shown.
	 */
	@Override
	public boolean showTitleInDebug() {
		return false;
	}

	/**
	 * Method to setup button and data area.
	 */
	private void setup() {
		mainPanel.addActionListener("cancelButton", this, "cancel");  
		messageTextArea = (JTextArea) mainPanel.getComponent("messageTextArea");  ;
		cancelButton = (JButton) mainPanel.getComponent("cancelButton"); 

		cancelButton.setFocusable(true);
		cancelButton.setVisible(true);
		cancelButton.setEnabled(true);
	}
}
