package dw.dialogs;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.math.BigDecimal;

import javax.swing.JButton;
import javax.swing.JTextArea;

import com.moneygram.agentconnect.TransactionLookupResponse.Payload;

import dw.framework.XmlDialogPanel;
import dw.i18n.Messages;
import dw.profile.UnitProfile;

/**
 * Class for a informational dialog for displaying Receive Redirect information
 * 
 * @author w156
 */
public class RecvRedirectDialog extends XmlDialogPanel {

	private boolean accepted = false;
	private boolean canceled = false;
	private JTextArea messageTextArea;
    
	private JButton okButton = null;
	private final com.moneygram.agentconnect.TransactionLookupResponse transResponse;

	public RecvRedirectDialog(com.moneygram.agentconnect.TransactionLookupResponse transactionResponse) {
		super("dialogs/DialogRedirectReceive.xml"); 
		this.transResponse = transactionResponse;
		
		mainPanel.addActionListener("okButton", this, "yes");  
        messageTextArea = (JTextArea) mainPanel.getComponent("messageTextArea");   ;

		okButton = (JButton) mainPanel.getComponent("okButton");

		final KeyListener activator = new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					okButton.doClick();
				}
			}
		};

		mainPanel.addKeyListener("okButton", activator);
        mainPanel.addActionListener("cancelButton", this, "cancel");

		setCustomText();
	}

	public void cancel() {
		canceled = true;
		closeDialog();
	}

	/** Returns true if user clicked OK. */
	public boolean isAccepted() {
		return accepted;
	}

	/** Returns true if user clicked Cancel. */
	public boolean isCanceled() {
		return canceled;
	}

	public void no() {
		closeDialog();
	}

	public void setCustomText() {
		Payload payload = transResponse.getPayload().getValue();
		BigDecimal sRecvAmtNew = payload.getRedirectInfo().getNewReceiveAmount();
		BigDecimal sRecvAmtOld = payload.getRedirectInfo().getOriginalReceiveAmount();
		String sRecvCurrNew = payload.getRedirectInfo().getNewReceiveCurrency();
		String sRecvCurrOld = payload.getRedirectInfo().getOriginalReceiveCurrency();
		BigDecimal sSendAmt = payload.getSendAmounts().getSendAmount();
		String sSendCurr = payload.getSendAmounts().getSendCurrency();
		BigDecimal sFx = payload.getRedirectInfo().getNewExchangeRate();
		String sMessage = Messages.getString("DialogRedirectReceive.Text");
		
		sMessage = sMessage.replaceAll("OLD_CURR", sRecvCurrOld);
		sMessage = sMessage.replaceAll("NEW_CURR", sRecvCurrNew);
		sMessage = sMessage.replaceAll("AGENT_NAME",
    			UnitProfile.getInstance().getGenericDocumentStoreName());

		messageTextArea.setText( 
//	    		Messages.getString("DialogRedirectReceive.Inform") + "\n" + 
	    		sMessage + "\n\n" +
	    		Messages.getString("DialogRedirectReceive.OrigSendAmt")+ " " 
	    		+ sSendAmt + " " + sSendCurr + "\n" +
	    		Messages.getString("DialogRedirectReceive.OrigRecvAmt")+ " " 
	    		+ sRecvAmtOld + " " + sRecvCurrOld +"\n" +
	    		Messages.getString("DialogRedirectReceive.Fx")+ "1" + sSendCurr 
	    		 + " = " + sFx + " " + sRecvCurrNew+ "\n" +
	    		Messages.getString("DialogRedirectReceive.NewRecvAmt")+ " " 
	    		+ sRecvAmtNew + " " + sRecvCurrNew + "\n" 
	    		);
	      
	}

	public void yes() {
		accepted = true;
		closeDialog();
	}

}
