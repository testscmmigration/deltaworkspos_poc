package dw.dialogs;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.DateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;

import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.io.report.ReportLog;
import dw.io.report.ReportSelectionCriteria;
import dw.io.report.ReportSummary;
import dw.printing.Report;

public class DatePeriodEntryDialog extends ReportDialog {
    private JComboBox periodBox;
    private long absolutePeriod;
    private long summaryDateTime;
    private JRadioButton hostRadio;
    private WindowListener winListener;
    private boolean dispenserReport;
/*
    public DatePeriodEntryDialog(final Report report, JComponent parent, String title) {
        this(report,parent, true, title);
    }

    public DatePeriodEntryDialog(final Report report, JComponent parent, 
                                 boolean showEntryOption,
                                 String title) {
        this(report,parent, showEntryOption, false, false, title);
    }
*/
    public DatePeriodEntryDialog(
    		final Report report,
            boolean showEntryOption,
            boolean showDetailOption,
            boolean showLocalOption,
            String title,
			boolean dispenserReport,
			boolean csvOption,
			boolean csvEnable) {
        
        super("dialogs/DialogDatePeriodEntry.xml", report, title, true, true, false, csvOption, csvEnable);	

        //this.rsc = rsc;
        this.dispenserReport = dispenserReport;
        periodBox = (JComboBox) mainPanel.getComponent("periodBox");	
        mainPanel.addActionListener("periodBox", this, "periodAction");	 
        periodBox.addKeyListener(ESCAPE_ADAPTER);
        final TreeSet<ReportSummary> summaryItems = ReportLog.getSummaryRecordsClone();
        Vector<DatePeriodEntry> v = new Vector<DatePeriodEntry>();
        DefaultComboBoxModel cbm = new DefaultComboBoxModel(v);
        periodBox.setModel(cbm); 
        final Iterator<ReportSummary> it = summaryItems.iterator(); 
        DateFormat formatter = DateFormat.getDateInstance(DateFormat.LONG, FormatSymbols.getLocale());
        
        while (it.hasNext()){
             final ReportSummary summaryItem = it.next();
             v.add(0,
                 new DatePeriodEntry(
                     Messages.getString("DatePeriodEntryDialog.1868")  
                     + " " + formatter.format(new Date(summaryItem.getPeriodOpened()))  
                     + "     " 
                     + Messages.getString("DatePeriodEntryDialog.1869")  
                     + String.valueOf(summaryItem.getPeriod()),
                     summaryItem.getAbsolutePeriod(),
                     summaryItem.getPeriodOpened()));
        }
        periodBox.setSelectedIndex(0);
        hostRadio = (JRadioButton) mainPanel.getComponent("hostRadio");	
        hostRadio.setSelected(true);
        winListener = new MyWinListener();
        
        if (! showEntryOption)
            mainPanel.removeComponent("datePanel");	
        if (! showDetailOption)
            mainPanel.removeComponent("showDetailPanel");	
        if (! showLocalOption)
            mainPanel.removeComponent("localOptionPanel");	
    }
    
    class DatePeriodEntry {
        private final String display;
        private final long absolutePeriod;
        private final long summaryDateTime;
        
        public DatePeriodEntry(final String d, final long ap, final long sdt) {
            display = d;
            absolutePeriod = ap;
            summaryDateTime = sdt;
        }
        
        @Override
		public String toString() {
            return display;
        }
        
        public long getAbsolutePeriod() {
            return absolutePeriod;
        }
        
        public long getSummaryDateTime(){
            return summaryDateTime;   
        }    
    }
    
    class MyWinListener extends WindowAdapter {
        @Override
		public void windowClosing(WindowEvent e) {
            cancelAction();
        }
    }
    
    public WindowListener getWindowListener() {
        return winListener;
    }
    
    public void periodAction() {
      
       final DatePeriodEntry dpe = 
            (DatePeriodEntry)periodBox.getSelectedItem();
       absolutePeriod = dpe.getAbsolutePeriod();
       summaryDateTime = dpe.getSummaryDateTime();            
    }
    
    public long getSummaryDateTime(){
        return summaryDateTime;
    }
   
    public long getAbsolutePeriod(){
        return absolutePeriod;
    }

    public boolean isHostOption() {
        return hostRadio.isSelected();
    }

    @Override
	public void processReport() {
    	
    	if (dispenserReport)
    		setDispenserSelection();
    	else {
	        setPrepaidSelection();
	        final String fileName;
	        if (rsc.getShowDetail())
	            fileName = "xml/reports/mgcarddetailreport.xml";	
	        else
	            fileName = "xml/reports/mgcardreport.xml";	
	        report.setXmlFileName(fileName);
    	}
    		
    }
    
    public void setDispenserSelection() {
        rsc = new ReportSelectionCriteria();
        rsc.setAbsolutePeriod(getAbsolutePeriod());
        rsc.setShowDetail(isDetailOption());
        rsc.setSummaryDateTime(getSummaryDateTime());
    }
    
    public void setPrepaidSelection() {
        rsc = new ReportSelectionCriteria();
        rsc.setAbsolutePeriod(getAbsolutePeriod());
        rsc.setShowDetail(isDetailOption());
        rsc.setSummaryDateTime(getSummaryDateTime());
    }
}
