package dw.dialogs;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;

import dw.io.report.ReportSelectionCriteria;
import dw.model.adapters.CountryInfo;
import dw.printing.Report;
import dw.utility.ExtraDebug;
import dw.utility.Scheduler;
import dw.utility.TimeUtility;

public class DateTimeEntryDialog extends ReportDialog {
	private JComboBox periodBox;

	private long summaryDateTime;

	private long openDateTime;

	private long closeDateTime;

	private JRadioButton hostRadio;

	private WindowListener winListener;

	private boolean moneygramreport;

	HashMap<Integer, DateTimeEntry> localMap = new HashMap<Integer, DateTimeEntry>();

	public DateTimeEntryDialog(final Report report,
			boolean showEntryOption, boolean showDetailOption,
			boolean showLocalOption, boolean outputToFile, String title,
			boolean moneygramreport, boolean csvOption, boolean csvEnable) {

		super("dialogs/DialogDateTimeEntry.xml", report, 
				title, true, true, outputToFile, csvOption, csvEnable);
		//Debug.println("start constructor DateTimeEntryDialog");	
		//this.rsc = rsc;
		this.moneygramreport = moneygramreport;
		periodBox = (JComboBox) mainPanel.getComponent("periodBox"); 
		mainPanel.addActionListener("periodBox", this, "periodAction");  
		periodBox.addKeyListener(ESCAPE_ADAPTER);
		Vector<DateTimeEntry> hostVector = new Vector<DateTimeEntry>();
		DefaultComboBoxModel cbm = new DefaultComboBoxModel(hostVector);
		periodBox.setModel(cbm);
		final long currentTime = TimeUtility.currentTimeMillis();

		DateFormat formatter;
        if (CountryInfo.isAgentKorean()) {
        	formatter = new SimpleDateFormat(TimeUtility.KOREAN_DATE_YEAR_MONTH_DAY_FORMAT_1);
        } else {
        	formatter = new SimpleDateFormat(TimeUtility.LONG_DATE_FORMAT);
        }
		long date;
		date = currentTime - TimeUtility.MONTH;

		TimeZone hostTimeZone = Scheduler.getHostTimeZone();
		TimeZone localTimeZone = Scheduler.getLocalCalendar().getTimeZone();
		ExtraDebug.println("hostTimeZone["+hostTimeZone.getDisplayName()+"], Offset["+hostTimeZone.getRawOffset()+"]");
		ExtraDebug.println("localTimeZone["+localTimeZone.getDisplayName()+"], Offset["+localTimeZone.getRawOffset()+"]");
		int diff = localTimeZone.getRawOffset() - hostTimeZone.getRawOffset();
		if(hostTimeZone.inDaylightTime(TimeUtility.currentTime())&& 
				!localTimeZone.inDaylightTime(TimeUtility.currentTime())){	
			diff= diff -hostTimeZone.getDSTSavings();
		}
		
        ExtraDebug.println("diff["+diff+"ms,"+(diff/1000)+"sec,"+(diff/1000/60)
        		+"min,"+(diff/1000/3600)+"hr]" );

        for (int i = 0; i <= 30; i++) {
            Calendar localCalendar = Calendar.getInstance();
			final String dateString = formatter.format(new Date(date));

			Calendar hostCalendar = (Calendar) Scheduler.getHostCalendar()
					.clone();
			hostCalendar.setTime(new Date(date));
			long time = hostCalendar.getTimeInMillis()-diff;
			hostCalendar.clear();
			hostCalendar.setTime(new Date(time));
			long hostStartTime = hostCalendar.getTimeInMillis();
			final long hostEndTime = hostStartTime + TimeUtility.DAY - 1000;
			localCalendar.setTime(new Date(date));
			localCalendar.set(Calendar.HOUR_OF_DAY, 0);
			localCalendar.set(Calendar.MINUTE, 0);
			localCalendar.set(Calendar.SECOND, 0);
			final long localStartTime = localCalendar.getTimeInMillis();
			final long localEndTime = localStartTime + TimeUtility.DAY - 1000;
			final DateTimeEntry hostDte = new DateTimeEntry(dateString,
					hostStartTime, hostEndTime);
			final DateTimeEntry localDte = new DateTimeEntry(dateString,
					localStartTime, localEndTime);
			hostVector.add(0, hostDte);
			localMap.put(Integer.valueOf(i), localDte);
			date += TimeUtility.DAY;
			localCalendar.clear();
			hostCalendar.clear();
		}
		periodBox.setSelectedIndex(0);
		hostRadio = (JRadioButton) mainPanel.getComponent("hostRadio"); 
		hostRadio.setSelected(true);
		winListener = new MyWinListener();

		if (!showEntryOption)
			mainPanel.removeComponent("datePanel"); 
		if (!showDetailOption)
			mainPanel.removeComponent("showDetailPanel"); 
		if (!showLocalOption)
			mainPanel.removeComponent("localOptionPanel"); 

		//Debug.println("end constructor DateTimeEntryDialog");	
	}

	class DateTimeEntry {
		private final String display;

		private final long openDateTime;

		private final long closeDateTime;

		public DateTimeEntry(final String d, final long odt, final long cdt) {
			display = d;
			openDateTime = odt;
			closeDateTime = cdt;
		}

		@Override
		public String toString() {
			return display;
		}

		public long getSummaryDateTime() {
			return openDateTime;
		}

		public long getOpenDateTime() {
			return openDateTime;
		}

		public long getCloseDateTime() {
			return closeDateTime;
		}
	}

	class MyWinListener extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			cancelAction();
		}
	}

	public WindowListener getWindowListener() {
		return winListener;
	}

	public void periodAction() {

		final DateTimeEntry dte = (DateTimeEntry) periodBox.getSelectedItem();
		this.summaryDateTime = dte.getSummaryDateTime();
		this.openDateTime = dte.getOpenDateTime();
		this.closeDateTime = dte.getCloseDateTime();
	}

	public long getSummaryDateTime() {
		return summaryDateTime;
	}

	public long getOpenDateTime() {
		return openDateTime;
	}

	public long getCloseDateTime() {
		return closeDateTime;
	}

	public boolean isHostOption() {
		return hostRadio.isSelected();
	}

	private void createReport() {
		setSelection();
		final String fileName;
		boolean detail = rsc.getShowDetail();
		if (moneygramreport) {
			if (detail)
				fileName = "xml/reports/mgsalesdetailreport.xml"; 
			else
				fileName = "xml/reports/mgsalesreport.xml";
		} else {
			if (detail)
				fileName = "xml/reports/processingfeedetailreport.xml"; 
			else
				fileName = "xml/reports/processingfeereport.xml";
		}
		
		report.setXmlFileName(fileName);
	}

	@Override
	public void processReport() {
		createReport();
	}
	
	@Override
	public void processCsvReport() {
		setSelection();
	}

	public void setSelection() {
		rsc = new ReportSelectionCriteria();
		rsc.setShowDetail(isDetailOption());
		rsc.setAccessHost(isHostOption());
		if (!isHostOption()) {
			int x = periodBox.getSelectedIndex();
			final DateTimeEntry localdte = localMap.get(Integer.valueOf(localMap.size() - 1 - x));
			rsc.setSummaryDateTime(localdte.getSummaryDateTime());
			rsc.setStartDateTime(localdte.getOpenDateTime());
			rsc.setEndDateTime(localdte.getCloseDateTime());
		} else {
			rsc.setSummaryDateTime(getSummaryDateTime());
			rsc.setStartDateTime(getOpenDateTime());
			rsc.setEndDateTime(getCloseDateTime());
		}
	}

	/** Overrides XmlDialogPanel. MG reports trigger comm. */
	@Override
	public boolean usesComm() {
		return true;
	}
}
