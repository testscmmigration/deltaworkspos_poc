/*
 * Option.java
 * 
 * $Revision$
 *
 * Copyright (c) 2004 MoneyGram International
 */

package dw.dialogs;
import dw.i18n.Messages;

/**
 * Provide option buttons for JOptionPane that follow the currently 
 * selected language.
 */
public class Option {
	
    public static final int YES_BUTTON = 1;
    public static final int NO_BUTTON = 2;
    public static final int CANCEL_BUTTON = 3;
    public static final int OK_BUTTON = 4;
    public static final int CONTINUE_BUTTON = 5;
    public static final int ABORT_BUTTON = 6;
    public static final int RETRY_BUTTON = 7;
    public static final int PRINT_BUTTON = 8;

    int value;
    String debug;
    private int property;
    
    public Option(int value, String debug, int property) {
        this.value = value;
        this.debug = debug;
        this.property = property;
    }

    /** Returns localized text for button label. */
    @Override
	public String toString() {
    	switch (property) {
	    	case YES_BUTTON: return Messages.getString("Dialogs.Yes");
	    	case NO_BUTTON: return Messages.getString("Dialogs.No");
	    	case CANCEL_BUTTON: return Messages.getString("Dialogs.Cancel");
	    	case OK_BUTTON: return Messages.getString("Dialogs.OK");
	    	case CONTINUE_BUTTON: return Messages.getString("Dialogs.Continue");
	    	case ABORT_BUTTON: return Messages.getString("Dialogs.Abort");
	    	case RETRY_BUTTON: return Messages.getString("Dialogs.Retry");
	    	case PRINT_BUTTON: return Messages.getString("Dialogs.Print");
	    	default: return ""; 
    	}
    }
}
