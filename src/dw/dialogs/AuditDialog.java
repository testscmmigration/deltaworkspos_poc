package dw.dialogs;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import dw.framework.XmlDialogPanel;
import dw.i18n.FormatSymbols;
import dw.io.report.AuditLog;
import dw.main.MainPanelDelegate;
import dw.model.adapters.CountryInfo;
import dw.utility.Debug;
import dw.utility.Scheduler;
import dw.utility.TimeUtility;

public class AuditDialog extends XmlDialogPanel  {
    
	private JComboBox startDateBox;
    private JComboBox endDateBox;
    private JButton okButton;
    private JButton cancelButton;
    private boolean isiRecordTypeNon = false;

    public AuditDialog(int dateRange, final int userID) { 

    	super("dialogs/DialogAuditDateEntry.xml");
        startDateBox = (JComboBox) mainPanel.getComponent("startDateBox");	
        endDateBox = (JComboBox) mainPanel.getComponent("endDateBox");	
        okButton = (JButton) mainPanel.getComponent("okButton");
        cancelButton = (JButton) mainPanel.getComponent("cancelButton");
        okButton.addActionListener(new ActionListener(){
            @Override
			public void actionPerformed(ActionEvent e) {
            	isiRecordTypeNon = true;
            	processAuditRequest(userID);
            	
            }
        });
        cancelButton.addActionListener(new ActionListener(){
            @Override
			public void actionPerformed(ActionEvent e) {
                closeDialog();
               	if (isiRecordTypeNon){
            		MainPanelDelegate.setupNonFinancialISI();
            	} else {
            		MainPanelDelegate.setupCancelISI("01");
            	}
             	String programName = "";
        		if (MainPanelDelegate.getIsiNfProgramArguments() != null)
        			programName = MainPanelDelegate.getIsiNfProgramArguments().trim();
        		if (programName.length() > 0 && !(programName.startsWith("-NF"))) {
        			MainPanelDelegate.executeIsiProgram(programName);
        		}
            	MainPanelDelegate.setIsiNfCommand(false);
            	MainPanelDelegate.setIsiNfProgramArguments("");
            }
        });
        
        Vector<DateTimeEntry> v = new Vector<DateTimeEntry>();
        DefaultComboBoxModel cbm = new DefaultComboBoxModel(v);
        startDateBox.setModel(cbm);
        Vector<DateTimeEntry> v1 = new Vector<DateTimeEntry>();
        DefaultComboBoxModel cbm1 = new DefaultComboBoxModel(v1);
        endDateBox.setModel(cbm1);

        final long currentTime = TimeUtility.currentTimeMillis();
        
        DateFormat formatter;
        if (CountryInfo.isAgentKorean()) {
        	formatter = new SimpleDateFormat(TimeUtility.KOREAN_DATE_YEAR_MONTH_DAY_FORMAT_1);
        } else {
        	formatter = DateFormat.getDateInstance(DateFormat.LONG,FormatSymbols.getLocale());
        }
        long date = currentTime - ((long) TimeUtility.DAY * ((long) dateRange - 1L));
 
        for (int i = 0; i < dateRange; i++) {
            final String dateString = formatter.format(new Date(date));
            final long startTime = Scheduler.getLongDateOnly(new Date(date));
            final long endTime = startTime + (TimeUtility.DAY - 1);
            final DateTimeEntry dte = new DateTimeEntry(dateString,startTime,endTime);
            v.add(0,dte);
            v1.add(0,dte);
            date += TimeUtility.DAY;
        }
        startDateBox.setSelectedIndex(0);
        endDateBox.setSelectedIndex(0);
        
    }

    private void processAuditRequest(int userID) {
    	
    	long starttime = ((DateTimeEntry) startDateBox.getSelectedItem()).getOpenDateTime();
    	long endtime = ((DateTimeEntry) endDateBox.getSelectedItem()).getCloseDateTime();
    	if (endtime < starttime)
    		Dialogs.showError("dialogs/DialogInvalidDateRange.xml");
    	else {
	    	AuditLog.writeReportRecord(userID, starttime, endtime, "UAR");
	        getAuditInfo(starttime, endtime);
    	}
    	
    }
    private void getAuditInfo(long starttime, long endtime) {
  	
    	FileOutputStream auditfile = AuditLog.getMatchingAuditRecords(starttime, endtime);
    	if (auditfile != null) {
			JFileChooser chooser = new JFileChooser();
			Dimension d = new Dimension(550, 325);
			chooser.setPreferredSize(d);
			DateFormat df = new SimpleDateFormat(TimeUtility.DATE_MONTH_DAY_SHORT_YEAR_NO_DELIMITER_FORMAT);
			String filename = "Audit_" + df.format(new Date(starttime)) + "_" + df.format(new Date(endtime));
			chooser.setSelectedFile(new File(filename));
			chooser.setMultiSelectionEnabled(false);
			chooser.addChoosableFileFilter(new SimpleFileFilter("csv",
					"Excel CSV file (*.csv)"));
			int option = chooser.showSaveDialog(null);
			if (option == JFileChooser.APPROVE_OPTION) {
				String fileName = chooser.getSelectedFile().getAbsolutePath();
				if (! fileName.toUpperCase(Locale.US).endsWith(".CSV")) {
					fileName += ".csv";
				}
				File f = new File(fileName);
				if (f.exists()) {
					int response = JOptionPane.showConfirmDialog(null,
							"Overwrite existing file?",
							"Confirm Overwrite",
							JOptionPane.OK_CANCEL_OPTION,
							JOptionPane.QUESTION_MESSAGE);
					if (response == JOptionPane.OK_OPTION)
						createCSVFile(fileName);
				}
				else
				   createCSVFile(fileName);
			}
			deleteTempFile();
    	}
    	else
    		Dialogs.showInformation("dialogs/DialogNoAuditInfoAvailable.xml");   	
    }
    
    private void createCSVFile(String filename) {
		FileReader txtIn = null;
		BufferedReader reader = null;
		FileOutputStream out = null;
		String txtLine;
		try {
			txtIn = new FileReader(AuditLog.AUDIT_LOG_QUERY);
			reader = new BufferedReader(txtIn);
		    out = new FileOutputStream(filename);
		    txtLine = reader.readLine();
		    while(txtLine != null) {
		    	StringBuffer outData = new StringBuffer(txtLine);
		    	out.write((outData.append("\r\n").toString()).getBytes());
		    	txtLine = reader.readLine();
		    }
		} catch (FileNotFoundException e) {
			Debug.printStackTrace(e);
		} catch (IOException e) {
			Debug.printStackTrace(e);
		} finally {
			try {
				txtIn.close();
			} catch (NullPointerException e1) {
			} catch (Exception e2) {
				Debug.printStackTrace(e2);
			}
			try {
				reader.close();
			} catch (NullPointerException e1) {
			} catch (Exception e2) {
				Debug.printStackTrace(e2);
			}
			try {
				out.close();
			} catch (NullPointerException e1) {
			} catch (Exception e2) {
				Debug.printStackTrace(e2);
			}
		}
    }
     
    private void deleteTempFile() {
		File f = new File(AuditLog.AUDIT_LOG_QUERY);
		boolean b = f.delete();
        if (! b) {
        	Debug.println("File " + f.getName() + " could not be deleted");
        }
    }
    
    class DateTimeEntry {
        private final String display;
        private final long openDateTime;
        private final long closeDateTime;

        public DateTimeEntry(final String d, final long odt, 
                             final long cdt) {
            display = d;
            openDateTime = odt;
            closeDateTime = cdt;
        }

        @Override
		public String toString() {
            return display;
        }

        public long getSummaryDateTime(){
            return openDateTime;
        }
        
        public long getOpenDateTime(){
            return openDateTime;
        }
        
        public long getCloseDateTime(){
            return closeDateTime;
        }
    }

   class SimpleFileFilter extends FileFilter {
    	
    	String extension;
    	String description;
    	public SimpleFileFilter(String ext)
    	{
    		 this(ext,null);
    	}
	    public SimpleFileFilter(String ext, String desc)
	    {
	    	  extension = ext.toLowerCase(Locale.US);
	    	  description = (desc == null ? ext +"files " : desc );
	    	
	    }
	    @Override
		public boolean accept(File f)
	    {
	    	if(f.isDirectory()){return true;}
	    	String name = f.getName().toLowerCase(Locale.US);
	    	if(name.endsWith(extension)){
	    		return true;
	    	}
	    	return false;
	    }

    	@Override
		public String getDescription(){ return description;}
    }
}
