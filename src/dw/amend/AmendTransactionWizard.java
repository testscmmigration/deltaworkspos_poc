package dw.amend;

import java.awt.event.KeyEvent;

import dw.framework.AdditionalInformationWizardPage;
import dw.framework.ClientTransaction;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.PageExitListener;
import dw.framework.PageNameInterface;
import dw.framework.SupplementalInformationWizardPage;

public class AmendTransactionWizard extends AmendTransactionInterface {
	private static final long serialVersionUID = 1L;
//	private static final String MRW90_COMMUNICATION_ERROR = "communicationError";
//	private static final String MRW60_REVIEW = "review";
	
	public static final String AMW15_AMEND_DATA = "amendDataCollection";
	public static final String AMW20_AMEND_VALIDATION = "amendValidation";
	public static final String AMW05_AMEND_SEARCH = "amendSearch";
	public static final String AMW10_AMEND_DETAIL = "amendDetail"; 
	
    public AmendTransactionWizard(AmendTransaction transaction, PageNameInterface naming) {
        super("moneygramsend/MGSWizardPanel.xml", transaction, naming, true);	
        createPages();
    }

    @Override
	public void createPages() {
        addPage(ATWizardPage1.class, transaction, AMW05_AMEND_SEARCH, "AMW05", buttons);	
        addPage(ATWizardPage2.class, transaction, AMW10_AMEND_DETAIL, "AMW10", buttons);	
        addPage(SupplementalInformationWizardPage.class, transaction, AMW15_AMEND_DATA, "AMW15", buttons);	
        addPage(AdditionalInformationWizardPage.class, transaction, AMW20_AMEND_VALIDATION, "AMW20", buttons);	
    }

    /**
     * Add the single keystroke mappings to the buttons. Use the function keys
     *   for back, next, cancel, etc.
     */
    private void addKeyMappings() {
        buttons.addKeyMapping("back", KeyEvent.VK_F11, 0);	
        buttons.addKeyMapping("next", KeyEvent.VK_F12, 0);	
        buttons.addKeyMapping("cancel", KeyEvent.VK_ESCAPE, 0);	
    }

    @Override
	public void start() {
        transaction.clear();
        super.start();
        addKeyMappings();
    }

    /**
     * Page traversal logic here.
     */
    @Override
	public void exit(int direction) {
        String currentPage = cardLayout.getVisiblePageName();
        String nextPage = null;
    	boolean readyForCommit = transaction.getValidationStatus() != null ? (transaction.getValidationStatus().equals(DataCollectionStatus.VALIDATED)) : false;

        if ((direction != PageExitListener.NEXT ) && 
            (direction != PageExitListener.COMPLETED ) &&
            (direction != PageExitListener.BACK ) &&
            (direction != PageExitListener.CANCELED ) &&
            (direction != PageExitListener.FAILED )){
            // default case
            super.exit(direction);
            return;
        }
        
        if (direction == PageExitListener.NEXT) {
            if (currentPage.equals(AMW10_AMEND_DETAIL)) {
                if (transaction.getStatus() == ClientTransaction.CANCELED) {
                    direction = PageExitListener.COMPLETED;
                    nextPage = null;
                }
            }
        }
 
        if (direction == PageExitListener.NEXT) {
        	if (currentPage.equals(AMW05_AMEND_SEARCH)) {
            	nextPage = AMW10_AMEND_DETAIL;
            
        	} else if (currentPage.equals(AMW10_AMEND_DETAIL)) { 
            	if (((transaction.getValidationStatus().equals(DataCollectionStatus.NEED_MORE_DATA)) || (transaction.getValidationStatus().equals(DataCollectionStatus.NOT_VALIDATED))) && (transaction.isShowScreenAMW15())) {
            		nextPage = AMW15_AMEND_DATA;
        		} else if (transaction.getStatus() == ClientTransaction.CANCELED){
                    direction = PageExitListener.COMPLETED;
                    nextPage = null;
                } else if (readyForCommit) {
                	transaction.setValidationStatus(DataCollectionStatus.COMPLETED);
                	nextPage = AMW10_AMEND_DETAIL;
                } else {
                	 direction = PageExitListener.CANCELED;
                     nextPage = null;
                }
            
        	} else if (currentPage.equals(AMW15_AMEND_DATA)) {
            	if ((transaction.getValidationStatus().equals(DataCollectionStatus.NEED_MORE_DATA)) || (transaction.getValidationStatus().equals(DataCollectionStatus.NOT_VALIDATED))) {
            		nextPage = AMW20_AMEND_VALIDATION;
            	} else if (transaction.getValidationStatus().equals(DataCollectionStatus.COMPLETED)) {
        			nextPage = AMW10_AMEND_DETAIL;
                } else {
                	direction = PageExitListener.CANCELED;
                    nextPage = null;
        		}
            
            } else if (currentPage.equals(AMW20_AMEND_VALIDATION)) {
        		if (transaction.getValidationStatus().equals(DataCollectionStatus.NEED_MORE_DATA)) {
        			nextPage = AMW20_AMEND_VALIDATION;
        		} else if (transaction.getValidationStatus().equals(DataCollectionStatus.COMPLETED)) {
    				nextPage = AMW10_AMEND_DETAIL;
                } else {
                	direction = PageExitListener.CANCELED;
                    nextPage = null;
    			} 
            }
        } else if (direction == PageExitListener.BACK ) {
        	if (currentPage.equals(AMW10_AMEND_DETAIL)) {   
        		nextPage = AMW05_AMEND_SEARCH;
            } else if (currentPage.equals(AMW15_AMEND_DATA)) {
        		nextPage = AMW10_AMEND_DETAIL;
            }
        }

        if (nextPage != null) {
            showPage(nextPage, direction);
            return;
        } else {
            super.exit(direction);
        }
    }
}
