package dw.amend;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import com.moneygram.agentconnect.TransactionLookupResponse;
import com.moneygram.agentconnect.TransactionStatusType;

import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.FieldKey;
import dw.framework.MfaFlowPageInterface;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.mgsend.MoneyGramClientTransaction;
import dw.mgsend.MoneyGramClientTransaction.ValidationType;
import dw.model.adapters.ReceiveDetailInfo;
import dw.utility.DWValues;

public class ATWizardPage2 extends AmendFlowPage implements MfaFlowPageInterface, DWTextPaneListener {
	private static final long serialVersionUID = 1L;
	
    private DWTextPane status1Label;
    private JButton reprintReceiptButton;
	private List<DWTextPane> textPanes;
	private int tagWidth;
	
	private boolean amendComplete;
	
    public ATWizardPage2(AmendTransaction tran, String name, String pageCode, PageFlowButtons buttons) {
        super(tran, "moneygramsend/ATWizardPage2.xml", name, pageCode, buttons);	
		textPanes = new ArrayList<DWTextPane>();
        status1Label = (DWTextPane) getComponent("status1Label");	
        reprintReceiptButton = (JButton) getComponent("reprintReceiptButton"); 
        
        reprintReceiptButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				amendTransaction.printReceipts();
			}
        });
        
        status1Label.addListener(this, this);
    }

	@Override
    public void start(int direction) {
        flowButtons.reset();
        flowButtons.setVisible("expert", false); 
        flowButtons.setEnabled("next", false);   
        TransactionLookupResponse response = amendTransaction.getTransactionLookupResponse();
        if (direction == PageExitListener.NEXT) {
            populateScreen();
            amendComplete = false;
        } else if (direction == PageExitListener.BACK){
            amendComplete = false;
        } else {
            amendComplete = false;
            flowButtons.setSelected("next"); 
        }
        flowButtons.setEnabled("next", response.getPayload().getValue().getTransactionStatus().equals(TransactionStatusType.AVAIL));    
        
        DataCollectionStatus status = amendTransaction.getDataCollectionData().getValidationStatus() != null ? amendTransaction.getDataCollectionData().getValidationStatus() : DataCollectionStatus.NOT_VALIDATED; 
        if (! status.equals(DataCollectionStatus.COMPLETED)) {
        	reprintReceiptButton.setEnabled(false);
        	reprintReceiptButton.setVisible(false);
        } else {
            if (amendTransaction.isReceiptReceived()) {
            	reprintReceiptButton.setEnabled(true);
           		reprintReceiptButton.setVisible(true);
            } else {
            	reprintReceiptButton.setEnabled(false);
            	reprintReceiptButton.setVisible(false);
            }
        	flowButtons.setEnabled("back", false);
        	flowButtons.setEnabled("next", false);
        	flowButtons.getButton("cancel").setText(Messages.getString("Dialogs.OK"));
        }
        amendTransaction.updateTransaction(); 
    }
    
	@Override
	public void dwTextPaneResized() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				status1Label.setWidth(FIELD_TOTAL_WIDTH);
				
				int w = FIELD_TOTAL_WIDTH - tagWidth;
				for (DWTextPane pane : textPanes) {
					pane.setWidth(w);
				}
			}
		});
	}
	
    private void populateScreen(){
        Map<String, String> values = amendTransaction.getDataCollectionData().getCurrentValueMap();
        
        ReceiveDetailInfo details = amendTransaction.getReceiveDetail();
        
    	List<JLabel> tags = new ArrayList<JLabel>();
		
		String tag = amendTransaction.getTransactionLookupInfoLabel(FieldKey.SENDER_PRIMARYPHONE_KEY.getInfoKey());
        String phone = values.get(FieldKey.SENDER_PRIMARYPHONE_KEY.getInfoKey());
        this.displayLabelItem("senderHomePhone", tag, phone, tags);
		
		tag = amendTransaction.getTransactionLookupInfoLabel("dateTimeSent");
        this.displayLabelItem("sent", tag, FormatSymbols.formatDateTimeForLocaleWithTimeZone(details.getDateTimeSent()), tags);
        
        String sNameFirst = values.get(FieldKey.SENDER_FIRSTNAME_KEY.getInfoKey());
        String sNameMiddle = values.get(FieldKey.SENDER_MIDDLENAME_KEY.getInfoKey());
        String sNameLast = values.get(FieldKey.SENDER_LASTNAME_KEY.getInfoKey());
        String sNameLast2 = values.get(FieldKey.SENDER_LASTNAME2_KEY.getInfoKey());
        String name = DWValues.formatName(sNameFirst, sNameMiddle, sNameLast, sNameLast2);
		this.displayPaneItem("senderName", name, tags, textPanes);
        
		tag = amendTransaction.getTransactionLookupInfoLabel(FieldKey.RECEIVER_FIRSTNAME_KEY.getInfoKey());
        String rNameFirst = values.get(FieldKey.RECEIVER_FIRSTNAME_KEY.getInfoKey());
		this.displayPaneItem("receiverFirstName", tag, rNameFirst, tags, textPanes);

		tag = amendTransaction.getTransactionLookupInfoLabel(FieldKey.RECEIVER_MIDDLENAME_KEY.getInfoKey());
		String rNameMiddle = values.get(FieldKey.RECEIVER_MIDDLENAME_KEY.getInfoKey());
		this.displayPaneItem("receiverMiddleName", tag, rNameMiddle, tags, textPanes);
        
		tag = amendTransaction.getTransactionLookupInfoLabel(FieldKey.RECEIVER_LASTNAME_KEY.getInfoKey());
		String rNameLast = values.get(FieldKey.RECEIVER_LASTNAME_KEY.getInfoKey());
		this.displayPaneItem("receiverLastName", tag, rNameLast, tags, textPanes);
        
		tag = amendTransaction.getTransactionLookupInfoLabel(FieldKey.RECEIVER_LASTNAME2_KEY.getInfoKey());
		String rNameLast2 = values.get(FieldKey.RECEIVER_LASTNAME2_KEY.getInfoKey());
		this.displayPaneItem("receiverSecondLastName", tag, rNameLast2, tags, textPanes);
        
    	String message1 = values.get(FieldKey.MESSAGEFIELD1_KEY.getInfoKey());
    	String message2 = values.get(FieldKey.MESSAGEFIELD2_KEY.getInfoKey());
        String message = (message1 == null ? "" : message1) + (message2 == null ? "" : " " + message2); 
		this.displayPaneItem("message", message, tags, textPanes);
        
        this.displayLabelItem("reference", amendTransaction.getAmendReferenceNumber(), tags);
                
        String status = this.getTranactionStatus(details.getTransactionStatus(), amendTransaction.getDataCollectionData().getCurrentValueMap(), true, false);
        this.displayPaneItem("status", status, false, tags, textPanes);
        
        if (amendComplete) {
        	String text = Messages.getString("ATWizardPage2.statusLine1"); 
        	status1Label.setText(text); 
            flowButtons.setAlternateText("cancel", Messages.getString("OK")); 
            flowButtons.setEnabled("back", false); 
            flowButtons.getButton("cancel").requestFocus(); 
        } else {
        	status1Label.setText(""); 
        }

        tagWidth = this.setTagLabelWidths(tags, MINIMUM_TAG_WIDTH);

        dwTextPaneResized();
    }
    
	@Override
    public void commComplete(int commTag, Object returnValue) {
        boolean b = ((Boolean) returnValue).booleanValue();
        switch (commTag) {
            case MoneyGramClientTransaction.AMEND_VALIDATION:
                if (b) {
                    PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
                } else {
                    PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
                }
                return;

            default :
                return;
        }
    }

	@Override
    public void finish(int direction) {
        if (direction == PageExitListener.NEXT) {
			if (amendTransaction.isOKToPrintDF()) {
				amendTransaction.amendValidation(this, ValidationType.INITIAL_FORM_FREE);
				flowButtons.setButtonsEnabled(true);
				flowButtons.setVisible("expert", false);
			}
            return;
        } else if (direction == PageExitListener.CANCELED && flowButtons.getButton("cancel").getText().equals(Messages.getString("OK"))) {
            direction = PageExitListener.NEXT;
        }
        PageNotification.notifyExitListeners(this, direction);
    }

	@Override
	public void deauthorize() {
		deauthorize(amendTransaction);
        flowButtons.disable();
        flowButtons.getButton("cancel").setEnabled(true);
	}

	@Override
	public void cancel() {
	}

	@Override
	public void abort() {
	}

	@Override
	public void commit() {
	}
}
