package dw.amend;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.moneygram.agentconnect.AmendValidationRequest;
import com.moneygram.agentconnect.AmendValidationResponse;
import com.moneygram.agentconnect.BusinessError;
import com.moneygram.agentconnect.InfoBase;

import dw.comm.MessageFacade;
import dw.comm.MessageFacadeError;
import dw.comm.MessageFacadeParam;
import dw.comm.MessageLogic;
import dw.framework.ClientTransaction;
import dw.framework.DataCollectionPanel;
import dw.framework.DataCollectionSet;
import dw.framework.DataCollectionSet.DataCollectionStatus;
import dw.framework.FieldKey;
import dw.framework.FlowPage;
import dw.io.IsiReporting;
import dw.main.DeltaworksStartup;
import dw.main.MainPanelDelegate;
import dw.mgsend.MoneyGramSendTransaction;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.ExtraDebug;

public class AmendTransaction  extends MoneyGramSendTransaction {
	private static final long serialVersionUID = 1L;

    private String amendReferenceNumber;
    private String oldReceiverNameFirst;
	private String oldReceiverNameLast;
    private String oldReceiverNameLast2;

	private boolean showScreenAMW15;
	
	private boolean supplementalInformationNeeded;
	
	private boolean isSupplementalInfoNeeded() {
		return supplementalInformationNeeded;
	}

	private void setSupplementalInfoNeeded(boolean b) {
		this.supplementalInformationNeeded = b;
	}
    
	@Override
	public void validation(FlowPage callingPage) {
		this.amendValidation(callingPage, ValidationType.SECONDARY);
	}
    
    public AmendTransaction(String tranName, String tranReceiptPrefix, int docSeq) {
    	super (tranName, tranReceiptPrefix, docSeq); 
    }
    
    public void amendValidation(final FlowPage callingPage, final ValidationType  validationType) {

		final MessageFacadeParam parameters = new MessageFacadeParam(
				MessageFacadeParam.REAL_TIME,
				MessageFacadeParam.USER_CANCEL,
				MessageFacadeParam.MG_FEE_RATE_CHANGED
			);
		
		final MessageLogic logic = new MessageLogic() {
			@Override
			public Object run() {

				setStatus(ClientTransaction.IN_PROCESS);
				getDataCollectionData().setValidationStatus(DataCollectionStatus.ERROR);

				getDataCollectionData().setFieldValue(FieldKey.MF_VALID_LANG_RCPT_PRIME_KEY,
						DWValues.sConvertLang3ToLang5(getPrimaryReceiptSelectedLanguage()));
				getDataCollectionData().setFieldValue(FieldKey.MF_VALID_LANG_RCPT_SECOND_KEY,
						DWValues.sConvertLang3ToLang5(getSecondaryReceiptSelectedLanguage()));

				// Operator Name

				String opName = null;
				if (isPasswordRequired()) {
					opName = userProfile.find("NAME").stringValue();
					if ((opName != null) && (opName.length() > 7)) {
						opName = userProfile.find("NAME").stringValue().substring(0, 7);
					}
					getDataCollectionData().setFieldValue(FieldKey.OPERATORNAME_KEY, opName);
				} else {
					opName = "Unknown";
				}

				AmendValidationResponse response = null;

				try {

					// Make the AC call to validate the send transaction.

					//boolean refundFee = getRefundFee();
		            AmendValidationRequest amendValidationRequest = new AmendValidationRequest();
					response = MessageFacade.getInstance().amendValidation(parameters, amendValidationRequest, getMgiTransactionSessionId(), opName, getDataCollectionData(), validationType);
					setValidationRequest(amendValidationRequest);
					
					// Check if a valid response was returned from the receiveValidation call.
					
					if (response != null && response.getPayload() != null) {
						AmendValidationResponse.Payload payload = response.getPayload().getValue();
						if (validationType.equals(ValidationType.INITIAL_NON_FORM_FREE)) {
							getDataCollectionData().clear();
						}
						
						List<InfoBase> fieldsToCollect = payload.getFieldsToCollect() != null ? payload.getFieldsToCollect().getFieldToCollect() : null;
						List<BusinessError> errors = response.getErrors() != null ? response.getErrors().getError() : null;
						processValidationResponse(validationType, fieldsToCollect, errors, payload.isReadyForCommit(), DataCollectionSet.AMEND_EXCLUDE);
					}
					return Boolean.TRUE;
				} catch (MessageFacadeError mfe) {
					if (mfe.hasErrorCode(MessageFacadeParam.USER_CANCEL)) {
						setStatus(ClientTransaction.FAILED);
					} else {
						setStatus(ClientTransaction.CANCELED);
					}
					return Boolean.FALSE;
				}
			}
		};
		MessageFacade.run(logic, callingPage, AMEND_VALIDATION, Boolean.FALSE);
	}

    @Override
	public void clear() {
        // clear out all the transaction information
        super.clear();

//        amendResponseInfo = new AmendTransactionResponse();
        amendReferenceNumber = "";
    }
    
    public String getAmendReferenceNumber() {
        return amendReferenceNumber;
    }
    
    @Override
	public boolean isCheckinRequired() {
        return !(DeltaworksStartup.getInstance().haveConnected());
    }

    @Override
	public boolean isFinancialTransaction() {
        return true;
    }

    public void setAmendReferenceNumber(String amendReferenceNumber) {
       this.amendReferenceNumber = amendReferenceNumber;
   	}
    
    public String getOldReceiverNameFirst() {
		return oldReceiverNameFirst;
	}

	public void setOldReceiverNameFirst(String oldReceiverNameFirst) {
		this.oldReceiverNameFirst = oldReceiverNameFirst;
	}

	public String getOldReceiverNameLast() {
		return oldReceiverNameLast;
	}

	public void setOldReceiverNameLast(String oldReceiverNameLast) {
		this.oldReceiverNameLast = oldReceiverNameLast;
	}

	public String getOldReceiverNameLast2() {
		return oldReceiverNameLast2;
	}

	public void setOldReceiverNameLast2(String oldReceiverNameLast2) {
		this.oldReceiverNameLast2 = oldReceiverNameLast2;
	}

	public boolean isShowScreenAMW15() {
		return showScreenAMW15;
	}
	
	
	@Override
	public void setScreensToShow() {
		this.setExcludePanelNames(new HashSet<String>(DataCollectionSet.AMEND_EXCLUDE));
		List<DataCollectionPanel> supplementalDataCollectionPanels = getDataCollectionData().getSupplementalDataCollectionPanels(this.getExcludePanelNames());
		setSupplementalDataCollectionPanels(supplementalDataCollectionPanels);
		setSupplementalInfoNeeded(supplementalDataCollectionPanels.size() > 0);
		showScreenAMW15 = supplementalDataCollectionPanels.size() > 0;
		Debug.println("showScreenRVW15 = " + showScreenAMW15);
		ExtraDebug.println("Receiver Supplemental Data To Collect = " + isSupplementalInfoNeeded());
	}
	
	@Override
	protected void logISIEntry() {
		String newReceiverNameFirst = getDataCollectionData().getValue(FieldKey.RECEIVER_FIRSTNAME_KEY);
		String newReceiverNameLast = getDataCollectionData().getValue(FieldKey.RECEIVER_LASTNAME_KEY);
		String newReceiverNameLast2 = getDataCollectionData().getValue(FieldKey.RECEIVER_LASTNAME2_KEY); 
        String[] headerItems = {
                amendReferenceNumber,
                oldReceiverNameFirst != null ? oldReceiverNameFirst : "",
				oldReceiverNameLast != null ?  oldReceiverNameLast : "",
				oldReceiverNameLast2 != null ?  oldReceiverNameLast2 : "",
                newReceiverNameFirst != null ? newReceiverNameFirst : "",
                newReceiverNameLast != null ?  newReceiverNameLast : "",
                newReceiverNameLast2 != null ?  newReceiverNameLast2 : "",
        	};

        List<String> fields = new ArrayList<String>();
        IsiReporting isi = new IsiReporting();
    	fields.add("AMD");
        getIsiHeader(fields);

        for (int i = 0; i < headerItems.length; i++) {
            fields.add(headerItems[i]);
        }

        isi.print(fields, isi.ISI_LOG);
        if (MainPanelDelegate.getIsiExeCommand()) {
            isi.print(fields, isi.EXE_LOG);
        }
    }
	 
	public void updateTransaction() {
		if (getReceiveDetail() != null) { 
			this.oldReceiverNameFirst = this.detail.getReceiverFirstName();
			this.oldReceiverNameLast = this.detail.getReceiverLastName();
			this.oldReceiverNameLast2 = this.detail.getReceiverLastName2();
		}
	}
		
	public void printReceipts() {
       	resetPrintFailure();
		if (isReceiveAgentAndCustomerReceipts()) {
			printReceipt(false, true);
			if (! wasPrintFailure()) {
				printReceipt(true, true);
			}
		} else {
			if (isReceiveCustomerReceipt()) {
				printReceipt(true, true);
			} else if (isReceiveAgentReceipt()) {
				printReceipt(false, true);
			}
		}
	}
}
