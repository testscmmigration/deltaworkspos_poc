package dw.amend;

import dw.framework.ClientTransaction;
import dw.framework.PageNameInterface;
import dw.framework.TransactionInterface;

public abstract class AmendTransactionInterface extends TransactionInterface {
	private static final long serialVersionUID = 1L;
	
	protected AmendTransaction transaction;

    public AmendTransactionInterface(String fileName,
    			AmendTransaction transaction,
    			PageNameInterface naming,
                boolean sideBar) {
    	super(fileName, naming, sideBar);
    	this.transaction = transaction;
    }

    @Override
	public ClientTransaction getTransaction() {
    	return transaction;
    }
}
