package dw.amend;

import javax.swing.JPanel;
import javax.swing.JTextField;

import com.moneygram.agentconnect.TransactionLookupResponse;
import com.moneygram.agentconnect.TransactionStatusType;

import dw.dialogs.Dialogs;
import dw.dwgui.DWTextPane;
import dw.dwgui.DWTextPane.DWTextPaneListener;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.framework.DataCollectionSet;
import dw.framework.DataCollectionSet.DataCollectionType;
import dw.i18n.Messages;
import dw.utility.DWValues;

	/**
	 * Page 1 of the MoneyGram Amend Wizard. This page prompts the user to enter
	 *   some information to search for the desired receive transaction.
	 */
	public class ATWizardPage1 extends AmendFlowPage implements DWTextPaneListener {
		private static final long serialVersionUID = 1L;

		private JTextField referenceField;
		private DWTextPane tp;

	    public ATWizardPage1(AmendTransaction tran,String name, String pageCode, PageFlowButtons buttons) {
	        super(tran, "framework/ReferenceNumberWizardPage.xml", name,pageCode, buttons);	
	        referenceField = (JTextField) getComponent("referenceNumber"); 	
	        
			tp = (DWTextPane) getComponent("text");
			tp.addListener(this, this);
		}

		@Override
		public void dwTextPaneResized() {
			JPanel partnerTable = (JPanel) getComponent("referenceNumberPanel");
			tp.setWidth(partnerTable.getPreferredSize().width);
			tp.setListenerEnabled(false);
		}

		public void doSearch() {
	       flowButtons.getButton("next").doClick();	
	    }

	    public void setBoxStates() {
	        // set the state of the next button...only allowed if criteria entered
	        flowButtons.setEnabled("next", true);	
	    }

		@Override
	    public void start(int direction) {

	        flowButtons.reset();
	        flowButtons.setEnabled("back", false);	
	        flowButtons.setEnabled("expert", false);	
			flowButtons.setVisible("expert", false);	

	        // install the listeners
	        addTextListener("referenceField", this, "setBoxStates");	
	        addActionListener("referenceField", this, "doSearch");	
	        
	        String text = Messages.getString("ATWizardPage1.screen1Linex");
	        tp.setText(text);

	        referenceField.requestFocus();
	        referenceField.selectAll();
	    }


		@Override
	    public void commComplete(int commTag, Object returnValue) {
	        boolean b = ((Boolean) returnValue).booleanValue();
	        if (b) {
	        	TransactionLookupResponse response = amendTransaction.getTransactionLookupResponse();
	        	TransactionStatusType status = ((response != null) && (response.getPayload() != null) && (response.getPayload().getValue() != null)) ? response.getPayload().getValue().getTransactionStatus() : null;
	        	if (status != null) {
	                PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
	                return;
	        	}
                Dialogs.showError("dialogs/DialogInvalidTransactionStatus.xml");
	        }
	
	    	flowButtons.setButtonsEnabled(true);
	        flowButtons.setEnabled("back", false);	
	    }

		@Override
	    public void finish(int direction) {
	        // call setButtonsEnabled to avoid duplicated transactions.
	        flowButtons.setButtonsEnabled(false);
	        
	        if (direction == PageExitListener.NEXT) {
	            if (referenceField.getText().length() > 0) { 
	            	amendTransaction.setAmendReferenceNumber(referenceField.getText());
	            	amendTransaction.setDataCollectionData(new DataCollectionSet(DataCollectionType.VALIDATION));
	            	amendTransaction.transactionLookup(referenceField.getText().trim(), this, DWValues.TRX_LOOKUP_AMEND, true);
	            }
	            else {
	                flowButtons.setButtonsEnabled(true);
	                flowButtons.setEnabled("back", false);	
	            }    
	        }
	        else {
	            PageNotification.notifyExitListeners(this, direction);
	        }
	    }
}
