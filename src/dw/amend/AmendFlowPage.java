package dw.amend;

import dw.framework.FlowPage;
import dw.framework.PageFlowButtons;

public abstract class AmendFlowPage extends FlowPage {
	private static final long serialVersionUID = 1L;
	
	protected AmendTransaction amendTransaction;

    public AmendFlowPage(AmendTransaction tran,
    		String xmlFileName, String name,
			String pageCode, PageFlowButtons buttons) {
		super(xmlFileName, name, pageCode, buttons);
		this.amendTransaction = tran;
	}

    // subclass pages will override this to provide their own return value
    // handling.
	@Override
	public void commComplete(int commTag, Object returnValue) {
	}
}
