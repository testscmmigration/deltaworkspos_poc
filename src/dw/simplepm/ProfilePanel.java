package dw.simplepm;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.AbstractButton;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;

import dw.dwgui.DWCheckBox;
import dw.dwgui.MoneyField;
//import dw.util.Debug;
import dw.profile.ProfileItem;


/**
 * Special GUI panel for textfields and other controls that correspond 
 * to a profile item. Stores a reference to the profile item associated
 * with the textfield or whatever in this panel.
 */
public class ProfilePanel extends JPanel 
        implements DocumentListener, ItemListener, ActionListener {
	private static final long serialVersionUID = 1L;

	private ProfileItem profileItem;
    private String path;
    private boolean hidden = false;
    private boolean readonly = false;
    private boolean empty = true;
    private int col1width = 265;
   
    private JTextComponent jtc;
    private AbstractButton ab;
    
    public ProfilePanel() {
        super();
    }

    /** 
     * Sets the path to the associated profile item. The item named
     * by the path is looked up and a reference to it is stored. 
     * The profile mode of that item influences the behavior of the
     * add method.
     */
    public void setPath(String path) {
        this.path = path;
        profileItem = SimplePMTransaction.getCurrentInstance().get(path);
        if (profileItem == null) {
            hidden = true;
            return;
        }

        if ("H".equals(profileItem.getMode()))	
            hidden = true;

        if ("V".equals(profileItem.getMode()))	
            readonly = true;
    }

    /** Sets the width of the first component added. Usually a label. */
    public void setCol1(int width) {
        col1width = width;
    }

    /** Returns true if this profile item is host-only. */
    public boolean isHidden() {
        return hidden;
    }

    /** 
      * Adds a component to this panel, depending on the profile item's
      * mode. Doesn't do the add if the mode is "H". Adjusts controls	
      * to a read-only appearance and behavior if the mode is "V".	
      * Initializes component state based on the profile item value.
      * Insures first component in the panel has a minimum size.
      */
    @Override
	public Component add(Component c) {
        //Debug.println("ProfilePanel.add(" + c.getClass().getName() + ") path = " + path);
        boolean bound = true;
        
        if (c instanceof JLabel)
            bound = false;

        if (c instanceof JTextField)
            ((JTextField) c).setDisabledTextColor(SystemColor.controlText);
        
        //mode check
        if (hidden)
            return c;
    
        if (readonly) {
            if (! (c instanceof JLabel)) {
                c.setEnabled(false);
            }
            
            if (c instanceof JTextField)  {
                c.setBackground(SystemColor.control);
            }
        }

        // First component added gets a minimum size
        if (empty && (col1width != 0) && (c instanceof JComponent)) {
            ((JComponent) c).setMinimumSize(new Dimension(col1width, 0));
            ((JComponent) c).setPreferredSize(new Dimension(col1width, 0));
        }

        // initialization
        if (bound) {
            if (c instanceof JLabel) {
                ((JLabel) c).setText(profileItem.stringValue());
            }
            else if (c instanceof MoneyField) {
                MoneyField mf = (MoneyField) c;
                if (! mf.isPathSet()) {
                    mf.setPath(path);
                }
                mf.setValue(profileItem.bigDecimalValue(), profileItem.getMode());
            }
            else if (c instanceof PayeeField) {
                PayeeField pf = (PayeeField) c;
                pf.setPath(path);
            }
            else if (c instanceof JTextComponent) {
                jtc = (JTextComponent) c;
                jtc.setText(profileItem.stringValue());
                //Debug.println("adding document listener");
                jtc.getDocument().addDocumentListener(this);
            }
            else if (c instanceof JComboBox){
                JComboBox jcb = (JComboBox) c;
                String value = profileItem.stringValue();
                // If current profile item value is not on the list, add it.
                ComboBoxModel jcbModel = jcb.getModel();
                boolean found = false;
                for (int i = 0; i < jcbModel.getSize(); i++) {
                    if (value.equals(jcbModel.getElementAt(i))) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    jcb.addItem(value);
                }                
                
                jcb.setSelectedItem(value);
                jcb.addActionListener(this);
            }
            else if (c instanceof DWCheckBox){
                DWCheckBox jckb = (DWCheckBox) c;
                jckb.setSelected(profileItem.booleanValue());
                jckb.addItemListener(this); 
            }
            else if (c instanceof AbstractButton) {
                ab = (AbstractButton) c;
                ab.setSelected(profileItem.booleanValue());
                ab.addItemListener(this);
            }
            // future screens could use other controls
        }

        empty = false;
        return super.add(c);
    } 

    /* 
     * Event handlers
     */

    @Override
	public void actionPerformed(ActionEvent e) {
        if(e.getSource() instanceof JComboBox){
            SimplePMTransaction.getCurrentInstance().userChange(path,((JComboBox) e.getSource()).getSelectedItem().toString());
        }
    }

    @Override
	public void itemStateChanged(ItemEvent e) {
        SimplePMTransaction.getCurrentInstance().userChange(path,
                (e.getStateChange() == ItemEvent.SELECTED) ? "Y" : "N");  
    }

    public void update(DocumentEvent e) {
        Document doc = e.getDocument();
        try {
            SimplePMTransaction.getCurrentInstance().userChange(path,
                    doc.getText(0, doc.getLength()));
        }
        catch (BadLocationException x) {
            SimplePMTransaction.getCurrentInstance().userChange(path, "");	
        }
    }

    @Override
	public void changedUpdate(DocumentEvent e) {
        update(e);
    }
    
    @Override
	public void insertUpdate(DocumentEvent e) {
        update(e);
    }
    
    @Override
	public void removeUpdate(DocumentEvent e) {
        update(e);
    }

    @Override
	public void removeNotify() {
        //Debug.println("ProfilePanel.removeNotify()");
//        if (jtc != null) {
//            jtc.getDocument().removeDocumentListener(this);
//        }
//        else if (ab != null) {
//            ab.removeItemListener(this);
//        }
        profileItem = null;
        jtc = null;
        ab = null;
        super.removeNotify();
    }    
}


