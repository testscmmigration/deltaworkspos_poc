package dw.simplepm;

import java.io.Serializable;
/* Java imports */
import java.util.Iterator;

import dw.model.XmlUtil;
import dw.profile.FeeItem;
import dw.profile.Profile;
import dw.profile.ProfileItem;

/**
 * Encapsulates the creation of a profile change XML document functionality.
 * @author Christopher Bartling
 */
public class ProfileChangeBuilder implements Serializable {
	private static final long serialVersionUID = 1L;

	// Variables
    private boolean validating = false;
    private SimplePMTransaction tran = null;
    
    /**
     * Constructor.
     * @param tran A ProfileTransaction object.  Needed for writing to the 
     * diagnostic log.
     */
    public ProfileChangeBuilder(SimplePMTransaction tran) {
        super();
        this.tran = tran;
    } //END-CONSTRUCTOR
    
    /**
     * Constructor.
     * @param tran A ProfileTransaction object.  Needed for writing to the 
     * diagnostic log.
     * @param validating A boolean flag used to signal whether the builder
     * should include the DOCTYPE information with the profile change XML
     * for use in validating the XML doc.
     */
    public ProfileChangeBuilder(final SimplePMTransaction tran, 
                                                    final boolean validating) {
        this(tran);
        this.validating = validating;
    } // END-CONSTRUCTOR
    
    /**
     * Traverses the profile item and resets the changed flags on 
     * the profile item and its child profile items (in the case of the 
     * original profile item being profile) to FALSE.
     * @param profileItem A ProfileItem object, which may also be a Profile 
     * object.
     * @author Christopher Bartling
     */
    public void resetProfileChangedFlag(final ProfileItem profileItem) {
        if (profileItem.isChanged() && profileItem instanceof Profile && 
                            ((Profile) profileItem).getNumberOfChildren() > 0) {
            // Iterate through all the children of this Profile object. 
            // Recursively call resetProfileChangedFlag message.
            for (Iterator<ProfileItem> children = ((Profile) profileItem).iterator();
                                                        children.hasNext();) {
                resetProfileChangedFlag(children.next()); 
            }                                                        
        }
        // BASE CASE: Now set the changed flag to FALSE on this 
        // ProfileItem object.  This will actually bubble up the change status 
        // to the root profile, but that's OK.
        profileItem.setChanged(false);
    } // END-METHOD 
    
    /**
     * Builds up the XML profile string containing all changed elements
     * and their child items.
     * @param rootProfile The main root Profile object.
     * @return A String representing the profile XML containing the item 
     * modifications and containment hierarchies.
     */
    public String build(final Profile rootProfile) {
        StringBuffer buf = new StringBuffer();
        
        buf.append("<?xml version=\"1.0\"?>\n\n"); 
        if (validating) {
            buf.append("<!DOCTYPE PROFILE [\n"); 
            buf.append("\t<!ELEMENT PROFILE      (ITEM | DISPENSER | PAYEE_TABLE | PRODUCT | USER)*>\n");	
            buf.append("\t<!ELEMENT ITEM         (#PCDATA)>\n");	
            buf.append("\t<!ELEMENT DISPENSER    (ITEM)*>\n");	
            buf.append("\t<!ELEMENT PAYEE_TABLE  (PAYEE)*>\n");	
            buf.append("\t<!ELEMENT PAYEE        (#PCDATA)>\n");	
            buf.append("\t<!ELEMENT PRODUCT      (ITEM | FEE_TABLE)*>\n");	
            buf.append("\t<!ELEMENT FEE_TABLE    (FEE)*>\n");	
            buf.append("\t<!ELEMENT USER         (ITEM)*>\n");	
            buf.append("\t<!ATTLIST ITEM         tag CDATA #REQUIRED\n\t\tmode CDATA \"H\"\n\t\tid CDATA #IMPLIED>\n");	 
            buf.append("\t<!ATTLIST DISPENSER    id CDATA #REQUIRED>\n");	
            buf.append("\t<!ATTLIST PAYEE_TABLE  mode CDATA \"S\">\n");	
            buf.append("\t<!ATTLIST PAYEE        id CDATA #REQUIRED\n\t\tstatus CDATA \"A\">\n");	
            buf.append("\t<!ATTLIST PRODUCT      id CDATA #REQUIRED\n\t\tstatus CDATA \"A\">\n");	
            buf.append("\t<!ATTLIST FEE_TABLE    mode CDATA \"S\">\n");	
            buf.append("\t<!ATTLIST FEE          id CDATA #REQUIRED\n\t\tstatus CDATA \"A\"\n\t\tbreakpoint CDATA #IMPLIED\n\t\tfee CDATA #IMPLIED>\n");	 
            buf.append("\t<!ATTLIST USER         id CDATA #REQUIRED\n\t\tstatus CDATA \"A\">\n");	
            buf.append("]>\n"); 
        }
        buf.append(getProfileXML(rootProfile));
        return buf.toString();
    } // END-METHOD
    
    /**
     * Builds the profile change XML, recursively traversing the Profile
     * hierarchy, looking for profile items with change flags = TRUE.
     *
     * @param profile A Profile object.
     * @return A String representing this Profile's changed items and other 
     * important attributes necessary for updating the profile in the 
     * middleware.
     */ 
    private String getProfileXML(Profile profile) {
        StringBuffer buf = new StringBuffer();
        buf.append("<");	
        buf.append(profile.getTag());
        if (profile.getID() != 0) {
            // Note: The value of id in the ProfileItem class ought to be
            // initialized to some well known value so it can be tested for 
            // existence in the XML message.
            buf.append(" id=\""); 
            buf.append(profile.getID());
            buf.append("\""); 
        }
        
        // Don't send a status if we're dealing with a PRODUCT profile.
        if (! profile.getTag().equalsIgnoreCase("PRODUCT")) {	
            if (profile.getStatus() != null && 
                    !(profile.getStatus().equals(""))) {	
                buf.append(" status=\""); 
                buf.append(profile.getStatus());
                buf.append("\""); 
            }
        }
        
        if (profile.getMode() != null && !(profile.getMode().equals(""))) {	
            buf.append(" mode=\""); 
            buf.append(profile.getMode());
            buf.append("\""); 
        }
        
        buf.append(">\n"); 
        
        // Skip the children if we're dealing with a deleted USER profile.
        final String deletedStatus = "D";	
        if (!(profile.getTag().equalsIgnoreCase("USER") &&	
                deletedStatus.equalsIgnoreCase(profile.getStatus()))) {

            Iterator<ProfileItem> childrenIterator = profile.iterator();
            
            while(childrenIterator.hasNext()) {
                ProfileItem child = childrenIterator.next();
                if (child.isChanged()) {
                    
                    // Only write out a Profile or ProfileItem "change"
                    if (child instanceof Profile)
                        buf.append(getProfileXML((Profile) child));
                    else buf.append(getProfileItemXML(child));
                }
            }
        }
        
        buf.append("</");	
        buf.append(profile.getTag());
        buf.append(">\n"); 
        
        return buf.toString();
    } // END-METHOD
    
    
    
    /**
     * Builds the profile item change XML.
     * @param profileItem A ProfileItem object.
     * @return A String representing this ProfileItem's changed state.
     */ 
    private String getProfileItemXML(ProfileItem profileItem) {
        
        // Write a diagnostic log message to the diag log.
        if (tran != null) 
            tran.writeProfileChangeDiagLogMessage(profileItem.getTag(), 
                    profileItem.stringValue());
        
        StringBuffer buf = new StringBuffer();
        
        if (profileItem.getTag().equalsIgnoreCase("FEE") ||	
                profileItem.getTag().equalsIgnoreCase("PAYEE")) {	
            buf.append("\t<");	
            buf.append(profileItem.getTag());
        }
        else {
            buf.append("\t<ITEM tag=\""); 
            buf.append(profileItem.getTag());
            buf.append("\""); 
        }
        
        // Now determine what else goes on into the ITEM element
        if (profileItem.getID() != 0) {
            // Note: The value of id in the ProfileItem class ought to be
            // initialized to some well known value so it can be tested for 
            // existence in the XML message.
            buf.append(" id=\""); 
            buf.append(profileItem.getID());
            buf.append("\""); 
        }
        
        if (profileItem.getStatus() != null && 
                !(profileItem.getStatus().equals(""))) {	
            buf.append(" status=\""); 
            buf.append(profileItem.getStatus());
            buf.append("\""); 
        }
        
        if (profileItem.getCurr() != null &&
                !(profileItem.getCurr().equals(""))) {	
            buf.append(" curr=\""); 
            buf.append(profileItem.getCurr());
            buf.append("\""); 
        }
        
        if (profileItem.getMode() != null &&
                !(profileItem.getMode().equals(""))) {	
            buf.append(" mode=\""); 
            buf.append(profileItem.getMode());
            buf.append("\""); 
        }
        
        if (profileItem instanceof FeeItem) {
            // Now write out the FeeItem attributes 
            FeeItem feeItem = (FeeItem) profileItem;
            buf.append(" breakpoint=\""); 
            buf.append(feeItem.getBreakpoint());
            buf.append("\""); 
            buf.append(" fee=\""); 
            buf.append(feeItem.getFee());
            buf.append("\""); 
            buf.append("/>\n"); 
            return buf.toString();
        }
        
        buf.append(">");	
        XmlUtil.sanitizeXml(buf, profileItem.stringValue());
        if (profileItem.getTag().equalsIgnoreCase("FEE") ||	
                profileItem.getTag().equalsIgnoreCase("PAYEE")) {	
            buf.append("</");	
            buf.append(profileItem.getTag());
            buf.append(">\n"); 
        }
        else {
            buf.append("</ITEM>\n"); 
        }
        
        return buf.toString();
    } // END-METHOD
    
    
} // END-CLASS
