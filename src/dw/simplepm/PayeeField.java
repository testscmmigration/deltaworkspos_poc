package dw.simplepm;


import java.awt.SystemColor;

import dw.dwgui.DWTextField;
import dw.profile.ProfileItem;
import dw.profile.UnitProfile;

/**
 * Specialized text field for Payee table values.
 */
public class PayeeField extends DWTextField {
	private static final long serialVersionUID = 1L;

	private String path = null;
    
    public PayeeField() {
    }

    /** 
     * Overrides to reformat after setting the text. 
     * If the specified text does not contain a decimal point,
     * one is added to the end. This means there is no implied
     * decimal as long as the text is parsable as a big decimal.
     */
    @Override
	public void setText(String text) {
        super.setText(text);
        if (hasFocus()) selectAll();
    }

    /** 
     * Support for the new Simple Profile Maintenance package. Sets the 
     * initial value from the profile.
     */
	public void setPath(String path) {
		this.path = path;
		ProfileItem item = UnitProfile.getInstance().getProfileInstance().get(path);
		if (item == null) {
			this.setVisible(false);
			this.path = null;
			return;
		}

		super.setText(item.stringValue());
		setEnabled(false);
		setDisabledTextColor(SystemColor.controlText);
		setBackground(SystemColor.control);
	}

	public void setID(String pathID) {
		ProfileItem item = UnitProfile.getInstance().getProfileInstance().get(this.path+"["+pathID+"]");	 
		if (item == null) {
			this.setVisible(false);
			this.path = null;
			return;
		}

		super.setText(String.valueOf(item.getID()));
		setEnabled(false);
		setDisabledTextColor(SystemColor.controlText);
		setBackground(SystemColor.control);
	}

}
