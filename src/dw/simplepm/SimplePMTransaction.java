package dw.simplepm;

import java.io.Serializable;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dw.comm.MessageFacade;
import dw.comm.MessageFacadeParam;
import dw.dialogs.Dialogs;
import dw.framework.ClientTransaction;
import dw.i18n.Messages;
import dw.io.DiagLog;
import dw.io.diag.SecurityDiagMsg;
import dw.io.report.AuditLog;
import dw.main.MainPanelDelegate;
import dw.mgreceive.MoneyGramReceiveTransaction;
import dw.mgsend.MoneyGramSendTransaction;
import dw.model.adapters.CountryInfo;
import dw.printing.EpsonSpooledPrinter;
import dw.printing.EpsonTMT88II;
import dw.printing.GenericPrinter;
import dw.profile.Profile;
import dw.profile.ProfileItem;
import dw.profile.UnitProfile;
import dw.utility.DWValues;
import dw.utility.Debug;
import dw.utility.IdleBackoutTimer;
import dw.utility.Scheduler;

/** 
 * Simplified rewrite of ProfileTransaction. Like all subclasses of
 * ClientTransaction, the idea is that this class is responsible for
 * all business logic associated with the functionality, while remaining
 * ignorant of the actual screens. Any input validation which can't be
 * expressed as simple constraints in the XML screen descriptions should
 * be here.
 * 
 * This class is intended to eventually replace ProfileTransaction.
 * 
 * @author Geoff Atkin
 */
public class SimplePMTransaction extends ClientTransaction implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int currentOption;
    private int selectedUser = -1;
    private int product = 1; // product number or doc sequence
    private String pathPrefix = "";	
    private Profile selectedUserProfile = null;
    private Profile newUsers = null;    // user profiles that have been added but not saved
    private Profile payees = null;      // contains copy of the payee table
    private Profile payeeTable = null;  // contains copy of individual payee items
    private BitSet allocated = null;    // has a true bit for each existing user
//    private String employeeMaxSendAmount ="";
    
    private static SimplePMTransaction currentInstance;
    private Map<String, String> changes = null;
    private Map<String, String> oldValues = null;
    private boolean feeChange;
    private final static BigDecimal ZERO = BigDecimal.ZERO;

    /** Constants for setCurrentOption. Copied from ProfileTransaction. */
    public static final int GENERAL_OPTIONS = 0;
    public static final int PROXY_SETTING_OPTIONS = 1;
    public static final int DISPENSER_OPTIONS = 2;
    public static final int PAYEE_TABLE = 3;
    public static final int MO_OPTIONS = 4;
    public static final int VP_OPTIONS = 5;
    public static final int MGS_OPTIONS = 6;
    public static final int MGMO_OPTIONS = 7;
    public static final int BILL_PAYMENT_OPTIONS = 8;
    public static final int USER_OPTIONS = 9;
    public static final int MANAGER_OPTIONS = 10;
    public static final int PRINTER_OPTIONS = 11;
    public static final int DIALUP_SETTING_OPTIONS = 12;
    public static final int DIALUP_SETTING_OPTIONS_SECONDARY = 13;
    public static final int CARD_PURCHASE_OPTIONS = 14;
    public static final int CARD_RELOAD_OPTIONS = 15;
    
    public static final int CLOSE_DISPENSER_PERIOD_OPTIONS = 16;
    public static final int DETAIL_REPORTING_OPTIONS = 17;
    public static final int DISPENSER_ITEM_SALES_REPORT_OPTIONS = 18;
    public static final int EMPLOYEES_TOTAL_OPTIONS = 19;
    public static final int MONEYGRAM_REPORT_OPTIONS = 20;
    public static final int PREPAID_CARD_REPORT_OPTIONS = 21;
    public static final int TRAINING_MODE_OPTIONS = 22;   
    public static final int AUDIT_TRAIL_REPORT_OPTIONS = 23;
    public static final int PROCESSING_FEE_REPORT_OPTIONS = 25;
 
    public static final String USER_ELEMENT_TAG = "USER";	
    public static final String USERNAME_ITEM_TAG = "NAME";	
    public static final String EMPTY_PROFILE_CHANGE = "<PROFILE>\n</PROFILE>"; 
    public static final Integer PROFILE_ITEM_ID_LIMIT = Integer.valueOf(32000);

    private ProfileChangeBuilder profileXMLBuilder = null;

    private String newProfile = "";	
    // private Map itemTagMap = null;
    private boolean printerChanged = false;
    private static boolean saProfileItemFlag = false;
    
	private boolean modemChanged = false;

    /**
     * is a Super Agent Profile Item being changed?
     * @return true if a Super Agent Profile item is being changed
     */
    public static boolean isSaProfileItemFlag() {
		return saProfileItemFlag;
	}

	/**
	 * Sets the Super Agent Profile Item flag
	 * @param saProfileItemFlag (boolean value)
	 */
	public static void setSaProfileItemFlag(boolean saProfileItemFlag) {
		SimplePMTransaction.saProfileItemFlag = saProfileItemFlag;
	}

    /**
     * Construct a new SimpleProfileTransaction.
     * @param name logical name of the transaction
     */
    public SimplePMTransaction(String name) {
        super(name, "");
//        timeoutEnabled = false;
        timeoutEnabled = true;
        dispenserNeeded = false;
        receiptPrinterNeeded = false;
        usingWizard = false;
        changes = new HashMap<String, String>();
        oldValues = new HashMap<String, String>();
        feeChange = false;
//        try {
//            forwarder = new ProfileMessageForwarder();
//        }
//        catch (java.net.MalformedURLException e) {
//            ErrorManager.handle(e, "servletURLError", ErrorManager.ERROR, true);	
//        }
        // itemTagMap = BaseProfileItems.getXMLTagMap();
        profileXMLBuilder = new ProfileChangeBuilder(this);
    }
    
    /** 
     * Sets the product number (document seq number) used to
     * get related profile items.
     */
    public void setProduct(int product) {
        this.product = product;
    }

    /**
     * Indicates which page of options to display.
     * Intended to be called from the main panel.
     */
    public void setCurrentOption(int option) {
        currentOption = option;
    }

    /**
     * Returns the option number of the page to be displayed.
     * Intended to be called from SimplePMTransactionInterface.
     */
    public int getCurrentOption() {
        return currentOption;
    }

    private static String[] managerOnly = { "ACCOUNTING_OPTIONS",   
        "FEES", "HDV_OVERRIDE", "HOST_PHONE", "PAYEE_CHANGES",     
        "SECURITY_CLEAR", "TOGGLE_TRAINING_MODE",    
        "MGR_PIN_PRIV_CHANGE", "EMP_PIN_PRIV_CHANGE","DISPENSER_OPTIONS","PRINTER_OPTIONS","RESET_LOCKED_PINS"};   

    private static HashMap<String, String> managerOnlyMap = new HashMap<String, String>();
    
    static {
        for (int i = 0; i < managerOnly.length; i++) {
            managerOnlyMap.put(managerOnly[i], "");	
        }
    }

    /** For manager options, the currently selected user. */
    public void setSelectedUser(int userid) {
        IdleBackoutTimer.checkIn();
        selectedUser = userid;
        List<String> currencyList = UnitProfile.getInstance().getProfileInstance().findList(
        	"AGENT_CURRENCY");
    	int currencies = currencyList.size();
    	
        if (selectedUser == -1) {
            pathPrefix = "";	
            selectedUserProfile = null;
            return;
        }
        
        
        pathPrefix = "USER[" + selectedUser + "]/";	 
        selectedUserProfile = (Profile) UnitProfile.getInstance().getProfileInstance().get("USER[" 	
                + selectedUser + "]");	
        
        if (selectedUserProfile == null) {
            // The only way this can happen is right after a call to createUser.
            // Create a user profile for the selected user by cloning the profile
            // for the currently logged on user. Then remove manager options if 
            // this is supposed to be a user profile, and treat all other values
            // as changes that are waiting to be saved.

            if (newUsers == null) {
                newUsers = new Profile("NEW_USERS");	
            }
            
            boolean manager = (userid > 40);
            
            String baseCurrency = CountryInfo.getAgentCountry().getBaseReceiveCurrency();
            
            try {
                selectedUserProfile = (Profile) userProfile.clone();
                selectedUserProfile.setID(userid);
                selectedUserProfile.setStatus("A");	
                selectedUserProfile.get("EMPLOYEE_STATUS").setValue("A");	 
                selectedUserProfile.get("USER_ID").setValue(userid);	
                selectedUserProfile.get("NAME").setValue(defaultName(manager, userid));	
                selectedUserProfile.get("PASSWORD").setValue(generatePassword());	
                selectedUserProfile.get("TYPE").setValue(manager ? "M" : "E");   
                ProfileItem maxSendAmt = selectedUserProfile.findCurrencyValue(
                		DWValues.E_MAX_SEND_AMOUNT, 
                		baseCurrency);
                /*
                 * Set default item to base currency and $0.00
                 */
                selectedUserProfile.remove(maxSendAmt);
                maxSendAmt.setValue("0.00");
                maxSendAmt.setCurr(baseCurrency);
                selectedUserProfile.add(maxSendAmt);
	            
                // Add any additional currencies

                if (currencies > 1) {
	            	ProfileItem prof;
	            	String curr;
	        		for (int ii = 0; ii < currencies; ii++) {
	        			curr = currencyList.get(ii);
	        			/*
	        			 * skip base currency, since it is already done
	        			 */
	        			if (curr.compareToIgnoreCase(baseCurrency) != 0) {
	                    	prof =  new ProfileItem("ITEM", DWValues.E_MAX_SEND_AMOUNT, 
	                    			"S", "0.00", curr);
	                    	prof.setChanged();
	                    	selectedUserProfile.add(prof);
	        			}
	        		}
	            }
	            selectedUserProfile.get("USER_ID_LOCKED").setValue("N");
	//          Defaulting to January 1 2005  means millis = 1107237600953
	            if (isSixCharPinType()) {
	            	selectedUserProfile.get("PSWD_CHANGE_DATE").setValue("1107237600953");
	            }
	                       
	            Iterator<ProfileItem> i = selectedUserProfile.iterator();
	            while (i.hasNext()) {
	                ProfileItem item = i.next();
	                String tag = item.getTag();
	                if ((! manager) && managerOnlyMap.containsKey(tag)) {
	                    i.remove();
	                } else {
	                	if (tag.compareToIgnoreCase(DWValues.E_MAX_SEND_AMOUNT) != 0) {
	                        userChange(tag, item.stringValue());
	                	} else {
	                		tag = tag + "/" + item.getCurr();
	                        userChange(tag, item.stringValue());
	                	}
	                }
	            }
	            boolean userAdded= false;
	            if (newUsers != null) {
	                i = newUsers.iterator();
	                while (i.hasNext()) {
	                    ProfileItem item = i.next();
	                    // ignore users that were added and immediately deleted
	                   String userId =item.get("USER_ID").stringValue(); 
	                   if(userId.equalsIgnoreCase(selectedUserProfile.get("USER_ID").stringValue())){
	                   	userAdded = true;
	                   }
	                }
	            }
	            if (!userAdded) {
	            	newUsers.add(selectedUserProfile);
	            }
            
            
            } catch (CloneNotSupportedException e) {
                // impossible
            }
            
        } else {
//        	try {
//				if (selectedUserProfile.find("E_MAX_SEND_AMOUNT").stringLength() >0) {
//					setEmployeeMaxSendAmount(selectedUserProfile.find("E_MAX_SEND_AMOUNT").stringValue());				}
//			} catch (NoSuchElementException e) {
//				setEmployeeMaxSendAmount("");
//			}       		
        }
    }
    

/**
 * check if the selected user is a manager based on userid.
 * @return boolean true if the userid selected from user list is > 40
 */
    public boolean isManager(){
        if(selectedUserProfile != null && selectedUserProfile.getID()>40)
            return true;
        else
            return false;
    }

    public void deleteSelectedUser() {
        userChange("EMPLOYEE_STATUS", "D");	 
    }

    public String defaultName(boolean manager, int userid) {
        return (manager ? "MGR" : "EMP") + userid;	 
    }

    /** 
     * Generates a random 3 digit password. Ensures that the 
     * password is not in use. Based on code Geoff wrote for 
     * the old profile maint package.
     */
    
    public String generatePassword() {
    	String password = null;
		boolean done = false;
		Profile rootProfile = (Profile) UnitProfile.getInstance().getProfileInstance();
		boolean isSixCharPinType = Profile.isSixCharPinType(); 
		while (! done) {
			password = isSixCharPinType ? generatePassword8() : generatePassword3();
			try {
				Profile p = rootProfile.find(null, password);
				done = false;
			} catch (NoSuchElementException e) {
				done = true;
			}
		}
		return password;
    }
    
    private String generatePassword3() {
		String result = "111"; 
		try {
			SecureRandom prng = SecureRandom.getInstance("SHA1PRNG");
			result = Integer.toString(prng.nextInt(900) + 100);
		} catch (NoSuchAlgorithmException e) {
			Debug.printException(e);
			Debug.printStackTrace(e);
		}
		return result;
	}
	
	private static final String ALPHA = "AaBbCcDdEdFfGgHhJjKkLMmNnoPpQqRrSsTtUuVvWwXxYyZz";
	private static final String NUMERIC = "23456789";
	private static final String SPECIAL = "!@#$%^&*()-_=+";
	private static final String ALPHANUMERIC = NUMERIC + ALPHA + NUMERIC;
	
	private char getPasswordCharacter(String characters, SecureRandom random) {
		int index = random.nextInt(characters.length());
		return characters.charAt(index);
	}
	
	private String generatePassword8() {
		StringBuffer sb = new StringBuffer();
		try {
			SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
			int numericIndex = random.nextInt(8);
			int alphaIndex;
			int specialIndex;
			do {
				alphaIndex = random.nextInt(8);
			} while (numericIndex == alphaIndex);
			do {
				specialIndex = random.nextInt(8);
			} while ((numericIndex == specialIndex) || (alphaIndex == specialIndex));
			
			
			for (int index = 0; index < 8; index++) {
				if (index == numericIndex) {
					sb.append(getPasswordCharacter(NUMERIC, random));
				} else if (index == alphaIndex) {
					sb.append(getPasswordCharacter(ALPHA, random));
				} else if (index == specialIndex) {
					sb.append(getPasswordCharacter(SPECIAL, random));
				} else {
					sb.append(getPasswordCharacter(ALPHANUMERIC, random));
				}
			}
		} catch (Exception e) {
			Debug.printException(e);
			Debug.printStackTrace(e);
		}
		return sb.toString();
	}

    /** Finds and returns the next unused user id, or -1 if none are available. */
    public synchronized int createUser(boolean manager) {
        // for dnet compatibility, 1 thru 40 are users and 41 thru 50 are managers
        int i, low, high;
        if (manager) {
            low = 41;
            high = 50;
        }
        else {
            low = 1;
            high = 40;
        }
        if (allocated == null) {
            populateAllocated();
        }
        for (i = low; i <= high; i++) {
            if (allocated.get(i) == false) {
                allocated.set(i);       // treat as allocated now
                return i;
            }
        }
        return -1;
    }    
    
    /** 
     * Loop through the top-level profile items looking for USER profiles, 
     * and store a bit in a bitset to mark each userid as allocated.
     */
    private synchronized void populateAllocated() {
        allocated = new BitSet();
        // This loop seems to occur a lot and really should be moved
        // to UnitProfile as some sort of closure or template method.
        Iterator<ProfileItem> i = UnitProfile.getInstance().getProfileInstance().iterator();
        while (i.hasNext()) {
            ProfileItem item = i.next();
            if (item.matches("USER")) {	
                allocated.set(item.getID());
            }
        }
    }
    
    /** Delegates to UnitProfile, prepending userid to the path if needed. */
    public ProfileItem get(String path) {
    	if (path.startsWith("PAYEE_TABLE")) {	
            return getPayee(path);
        }
        
        if (selectedUser != -1 ) {
        	return selectedUserProfile.get(path);
        }
        else {
        	return UnitProfile.getInstance().getProfileInstance().get(path);
        }
    }
    
    public void setEmployeeMaxSendAmount(String currency, String amount){
    	if (amount != null){
	    	if (!amount.equals("")){    		
	    		selectedUserProfile.findCurrencyValue(
	    				DWValues.E_MAX_SEND_AMOUNT, currency).setValue(amount);
	    	}
    	}
    }
    
    public String getEmployeeMaxSendAmount(String currency) {
    	return selectedUserProfile.findCurrencyValue(
    			DWValues.E_MAX_SEND_AMOUNT, currency).stringValue();
    }
    
    public boolean isMgrPinPrivChangeAllowed(){
        boolean result = false;   
        Profile userProf = (Profile) UnitProfile.getInstance().getProfileInstance().find("USER", getUserID()); 
        try {
            result = (userProf.find("MGR_PIN_PRIV_CHANGE").booleanValue());
        }
        catch (NoSuchElementException e) {
            Debug.println("No element : " + "MGR_PIN_PRIV_CHANGE" + " in user profile.");  
        }

        return result;
    }
    
    public boolean isMgrResetPINAllowed(){
       
        boolean result = false;   
        if(!isSixCharPinType())
            return result;
        try {
        	int userId = this.getUserID();
            Profile userProf = (Profile) UnitProfile.getInstance().getProfileInstance().find("USER", userId); 
            result = (userProf.find("RESET_LOCKED_PINS").booleanValue());
        }
        catch (NoSuchElementException e) {
            Debug.println("No element : " + "RESET_LOCKED_PINS" + " in user profile.");  
            return result;
        }

        return result;
    }
    public boolean isUserLocked(int userID){
        boolean result = false; 
        try{
        Profile userProf = (Profile) UnitProfile.getInstance().getProfileInstance().find("USER",userID); 
        try {
            result = (userProf.find("USER_ID_LOCKED").booleanValue());
        }
        catch (NoSuchElementException e) {
            Debug.println("No element : " + "USER_ID_LOCKED" + " in user profile.");  
        }
       
        }  catch (NoSuchElementException e) {
            Debug.println("NO user "+"USER" +userID+ " in user profile. in isUserLocked()");  
        }
        return result;
    }

    public boolean isEmpPinPrivChangeAllowed(){
        boolean result = false; 
        Profile userProf = (Profile) UnitProfile.getInstance().getProfileInstance().find("USER", getUserID()); 
        try {
            result = (userProf.find("EMP_PIN_PRIV_CHANGE").booleanValue());
        }
        catch (NoSuchElementException e) {
            Debug.println("No element : " + "EMP_PIN_PRIV_CHANGE" + " in user profile.");  
        }

        return result;
    }

    /** 
     * Returns a payee from a local copy of the payee list, creating 
     * that list as needed.
     */
    private ProfileItem getPayee(String path) {
        if (payees == null) {
            payees = new Profile("PAYEES");	
            try {
                payeeTable = (Profile) UnitProfile.getInstance().getProfileInstance().get("PAYEE_TABLE").clone();	
            }
            catch (CloneNotSupportedException e) {
                Debug.println("Ignored CloneNotSupportedException");	
                return null;
            }
            payees.add(payeeTable);
            String mode = payeeTable.getMode();
            BitSet exists = new BitSet();
            Iterator<ProfileItem> i = payeeTable.iterator();
            while (i.hasNext()) {
                ProfileItem item = i.next();
                exists.set(item.getID());
                item.setMode(mode);
                // make sure pending deletes are shown as empty
                if ("D".equals(item.getStatus()) && (item.stringLength() != 0)) {	
                    item.setValue("");	
                }
            }
            Debug.println("exists = " + exists);	
            Debug.println("starting payees:\n" + payees);	
            for (int j = 1; j < 100; j++) {
                // vendor ids 26 thru 30 inclusive are reserved in Dnet/Tandem
                if ((j >= 26) && (j <= 30)) 
                    continue;
                
                if (! exists.get(j)) {
                    ProfileItem temp = new ProfileItem("PAYEE", null, mode, "", null);	 
                    temp.setID(j);
                    temp.setStatus("T");    // temporary	
                    payeeTable.add(temp);
                }
            }
            Debug.println("ending payees:\n" + payees);	
        }
        Debug.println("getPayee(" + path + ") = " + payees.get(path));	 
        return payees.get(path);
    }
    
    /** 
     * Adds the the path prefix
     */
    public String addPathPrefix(String path) {
        if (selectedUser != -1) {
            return pathPrefix + path;
        }
        return path;
    }
    /** 
     * Returns the the path prefix
     */
    public String getPathPrefix() {
        return pathPrefix;
    }

    /** 
     * Does nothing. This method is called by the constructor in 
     * ClientTransaction to gets any profile values needed at 
     * instantiation time. We don't need any.
     */
    protected void getProfileValues() {
    }

    /**
     * Returns true unless the current screen is USER_OPTIONS.
     * Manager approval is required for all other screens.
     */
    @Override
	public boolean isManagerApprovalRequired() {
        return (currentOption != USER_OPTIONS);
    }

    /** Returns false. Name is not required, PIN is sufficient. */
    @Override
	public boolean isNamePasswordRequired() {
       // return false;   
        String requiredValue = UnitProfile.getInstance().get("GLOBAL_NAME_PIN_REQ", "Y");                                 
        if (requiredValue.equals("Y"))  
            return true;
        else
            return false;   
    }

    /** Returns true. A PIN is required. */
    @Override
	public boolean isPasswordRequired() {
        //return true;
        String requiredValue = UnitProfile.getInstance().get("GLOBAL_NAME_PIN_REQ", "Y");
        
        if (requiredValue.equals("N"))  
            return false;
        else
            return true;
    }
    
    /** Override of method in ClientTransaction. Does extra diag write.*/
    @Override
	public int isNamePasswordValid(String name, String password) {
        int userid = super.isNamePasswordValid(name, password);
        if ((userid > 0) && (currentOption == MANAGER_OPTIONS)) {
            DiagLog.getInstance().writeSecurity(
                SecurityDiagMsg.MANAGER_OPTIONS,
                (short) userid);
        }
        return userid;
    }
    
//    public int getUserId(){
//        return super.getUserID();
//    }
//    
    /** Clears out any leftover state from previous transactions. */
    @Override
	public void clear() {
        changes.clear();
        oldValues.clear();
        feeChange = false;
        setSelectedUser(-1);
        allocated = null;
        newUsers = null;
        payees = null;
        selectedUserProfile = null;
    }
    
    /** Does nothing. We don't have an expert mode. */
    @Override
	public void setUsingWizard(boolean using) {
    }
    
    /** Returns true. We are always in wizard mode. */
    @Override
	public boolean isUsingWizard() {
        return true;
    } 

    /** 
     * Returns the current instance. We're not enforcing a singleton
     * pattern, but it is useful to know the instance associated with the
     * instance of SimplePMTransactionInterface controlling the current 
     * screen.
     */
    public static SimplePMTransaction getCurrentInstance() {
        return currentInstance;
    }

    /** 
     * Sets the current instance. Called by SimplePMTransactionInterface
     * when it sets up the screens, to allow bound controls to send changes
     * directly to us.
     */
    public static void setCurrentInstance(SimplePMTransaction t) {
        currentInstance = t;
    }

    /** 
     * Returns fee with index i from the fee table. Returns the 
     * value specified by a user change, if there is one. Otherwise 
     * returns the value in the product profile. 
     */
    private BigDecimal getFee(int i) {
        BigDecimal result;
        String path = "PRODUCT[" + product + "]"	 
            + "/FEE_TABLE/FEE[" + i + "]/FEE";  
        if (changes.containsKey(path)) {
            result = new BigDecimal(changes.get(path));
            //Debug.println(path + " = " + result + " (from changes)");	
        }
        else {
            result = UnitProfile.getInstance().get(path, ZERO);
            //Debug.println(path + " = " + result + " (from profile)");	
        }
        return result;
    }

    /** 
     * Returns breakpoint with index i from the fee table. Returns the 
     * value specified by a user change, if there is one. Otherwise 
     * returns the value in the product profile. 
     */
    private BigDecimal getBreak(int i) {
        BigDecimal result;
        String path = "PRODUCT[" + product + "]"	 
            + "/FEE_TABLE/FEE[" + i + "]/BREAKPOINT";  
        if (changes.containsKey(path)) {
            result = new BigDecimal(changes.get(path));
            //Debug.println(path + " = " + result + " (from changes)");	
        }
        else {
            result = UnitProfile.getInstance().get(path, ZERO);
            //Debug.println(path + " = " + result + " (from profile)");	
        }
        return result;
    }
    
    /** 
     * Notes a change made through a control bound to a profile item.
     * The change is not applied to the unit profile until the user 
     * commits changes (such as by clicking an OK button) but it is 
     * stored here to make it available for validation.
     */
    public void userChange(String path, String value) {
        if ((path == null) || (path.length() == 0)) {
            return;
        }  
        changes.put(addPathPrefix(path), value);
    }
    
    /** 
     * The rules for a complex password are
     * - minimum 6 characters
     * - the previous PIN may not be re-used.
     *  - check if another user has this password.
     * 
     * @param password
     */
//    public boolean isValid6CharacterPassword(String currentPassword, String password){
//    	boolean retval = false;
//    	
//    	
//    	if (password.length() >= 6 &&
//    		(!currentPassword.equals(password)) &&
//    		ClientTransaction.getUserID(null, password.trim(), false) == 0)
//    		retval = true;
//    	
//    	return retval;
//    }
    public boolean isPasswordInUse(String password){
        if(ClientTransaction.getUserID(null, password.trim(), false) == 0)
        return false;
        else 
            return true;
        
    }
    public boolean isValid8CharacterPassword(String currentPassword,String password){
        boolean retval = false;
        try{
          int digitCount = 0;
          int specialCount = 0;
          int letterCount =0;
          
         String s =password.trim();
        for(int i = 0;i<s.length() ;i++){
           char c = s.charAt(i);
          String p = s.substring(i,i+1);
          if(isvalidMatch(p))
              specialCount++; 
          if(Character.isDigit(c))
             
              digitCount++;
          
          if(Character.isLetter(c))
              letterCount++;   
          if(Character.isSpaceChar(c))
              specialCount++;             
        }
        
        
        
    	if (password.length() >= 8 &&
		(!currentPassword.equals(password)) &&
		password.trim().length() <51 &&
		(letterCount > 0 && (digitCount >0 || specialCount > 0 )))
		retval = true;
    	
    	else 
    	    retval = false;
	return retval;
      

        }
        catch(Exception e){
            Debug.println(e.getMessage());
            return false;
        }
      }
    public boolean isvalidMatch(String s){
        //\\p{Punct} is equivalent to !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~
        Pattern p = Pattern.compile("\\p{Punct}");     
        Matcher m = p.matcher(s);
        boolean match = m.matches();       
        return match;

       
    }
    
    
    public static final int VALIDATE_OK = 0;
    public static final int VALIDATE_BREAKPOINTS_NOT_ASCENDING = 1;
    public static final int VALIDATE_DUPLICATE_NAME = 2;
    public static final int VALIDATE_DUPLICATE_PASSWORD = 3;
    public static final int VALIDATE_PASSWORD_LENGTH = 4;
    public static final int VALIDATE_SHORT_PASSWORD = 5;
    
    /** 
     * Validates user changes. 
     * @return VALIDATE_OK changes are valid, otherwise an error code.
     */
    public int validate() {
        // Validate rules for individual profile items. 
        // Iterate over changes.keySet() and select rules
        // based on the path.

        if (currentOption == MANAGER_OPTIONS) {
            populateDuplicateCheckingTable();

            // Check each profile change against the duplicate checking table
            // to see if the user chose a name or password that would be a
            // duplicate of another one after the changes are applied.
            // Note this means we do not catch or complain about existing 
            // duplicates, only ones caused by the changes being made now.
            // (I think it is confusing and burdensome for the users to force
            // them to clean up problems unrelated to what they're doing.)
            // But if we wanted to do that we could loop over the duplicate
            // checking table instead of the changes, in effect comparing it
            // to itself.
            
            for (Map.Entry<String, String> entry : changes.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                if (key.endsWith("/NAME")) {
                    if (isDuplicate(0, key, value)) {
                        return VALIDATE_DUPLICATE_NAME;
                    }
                }
                else if (key.endsWith("/PASSWORD")) {
                    if (isDuplicate(1, key, value)) {
                        return VALIDATE_DUPLICATE_PASSWORD;
                    } else if (value.length() < 3) {
                        return VALIDATE_SHORT_PASSWORD;
                    } else if (value.length() > 50) {
                        return VALIDATE_PASSWORD_LENGTH;
                    }
                }
            }
        }

        // Validation for the Dial up screens
        if (currentOption == DIALUP_SETTING_OPTIONS) {
        	for (Map.Entry<String, String> entry : changes.entrySet()) {
                String key = entry.getKey();
                if(key.equals("PASS_THROUGH_LOGON")){
                    String value = entry.getValue();
            		if(value.equals("N")){
            			UnitProfile.getInstance().set("PRIMARY_SCRIPT_USER_NAME","");
            			UnitProfile.getInstance().set("PRIMARY_SCRIPT_PASSWORD","");
            			UnitProfile.getInstance().set("PRIMARY_SCRIPT_DELAY_PHONE_DIAL","1");
						UnitProfile.getInstance().set("PRIMARY_SCRIPT_DELAY_PT_USERNAME","1");
						UnitProfile.getInstance().set("PRIMARY_SCRIPT_DELAY_PT_PASSWORD","1");
            			UnitProfile.getInstance().set("PRIMARY_SCRIPT_DELAY_A","1");
            			UnitProfile.getInstance().set("PRIMARY_SCRIPT_DELAY_B","1");
            		}
                }
            }
        }
        if (currentOption == DIALUP_SETTING_OPTIONS_SECONDARY) {
        	for (Map.Entry<String, String> entry : changes.entrySet()) {
                String key = entry.getKey();
                if(key.equals("SECONDARY_PASS_THROUGH_LOGON")){
                    String value = entry.getValue();
            		if(value.equals("N")){
            			UnitProfile.getInstance().set("SECONDARY_SCRIPT_USER_NAME","");
            			UnitProfile.getInstance().set("SECONDARY_SCRIPT_PASSWORD","");
            			UnitProfile.getInstance().set("SECONDARY_SCRIPT_DELAY_PHONE_DIAL","1");
						UnitProfile.getInstance().set("SECONDARY_SCRIPT_DELAY_PT_USERNAME","1");
						UnitProfile.getInstance().set("SECONDARY_SCRIPT_DELAY_PT_PASSWORD","1");
            			UnitProfile.getInstance().set("SECONDARY_SCRIPT_DELAY_A","1");
            			UnitProfile.getInstance().set("SECONDARY_SCRIPT_DELAY_B","1");
            		}
                }
            }
        }
        
        
        // if the fee table has changed, validate it
        if (feeChange) {
            // set j to the index of the last breakpoint, not counting
            // trailing items where both fee and breakpoints are zero
            int i, j;
            for (j = 10; j > 0; j--) {
                if ((getBreak(j).signum() != 0) || (getFee(j).signum() != 0))
                    break;
            }
            //Debug.println("validate() j = " + j);	
            // check breakpoints 1 through j - 1 inclusive 
            for (i = 1; i < j; i++) {
                if (getBreak(i).compareTo(getBreak(i + 1)) >= 0) {
                    //return Resources.getString("SimplePMTransaction.Breakpoints_must_be_in_ascending_order_16"); 
                    return VALIDATE_BREAKPOINTS_NOT_ASCENDING;
                }
            }
        }

        return VALIDATE_OK;
    }

    /** Helper class for duplicate checking. */
    class KeyValue implements Serializable {
		private static final long serialVersionUID = 1L;

		String key;
        String value;
        
        KeyValue(String key, String value) {
            this.key = key;
            this.value = value;
        }
        
        /** 
         * Return true if the profile change represented by (k, v)
         * conflicts with this KeyValue by assigning the same value
         * to a different key.
         */ 
        boolean conflictsWith(String k, String v) {
            return value.equals(v) && ! key.equals(k);
        }
        
        /** For debugging. */
        @Override
		public String toString() {
            return "(" + key + "," + value  + ")";
        }
    }
    
    // table[0] is a vector of KeyValue containing names
    // table[1] is a vector of KeyValue containing passwords
    private List<Vector<KeyValue>> table;
    
    /**
     * Constructs a table used by isDuplicate() to allow efficient
     * checking for duplicates. The table shows what everybody's name
     * and password will look like if the changes are applied.
     */
    private void populateDuplicateCheckingTable() {
        //Debug.println("populateDuplicateCheckingTable()");
        table = new ArrayList<Vector<KeyValue>>();
        table.add(new Vector<KeyValue>());
        table.add(new Vector<KeyValue>());

        // Loop thru unit profile and store unchanged names and passwords
        // in the table.

        for (Iterator<ProfileItem> i = UnitProfile.getInstance().getProfileInstance().iterator(); i.hasNext();) {
            ProfileItem item = i.next();
            if (item.matches("USER")) {
                String key = "USER[" + item.getID() + "]/NAME";
                if (! changes.containsKey(key)) {
                    table.get(0).add(new KeyValue(key, item.get("NAME").stringValue()));
                    //Debug.println("0," + key + "," + item.get("NAME").stringValue());
                }
                key = "USER[" + item.getID() + "]/PASSWORD";
                if (! changes.containsKey(key)) {
                	table.get(1).add(new KeyValue(key, item.get("PASSWORD").stringValue()));
                    //Debug.println("1," + key + "," + item.get("PASSWORD").stringValue());
                }
            }
        }
        
        // Loop thru changes being made now and apply them to the table

        for (Map.Entry<String, String> entry : changes.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            if (key.endsWith("/NAME")) {
            	table.get(0).add(new KeyValue(key, value));
            }
            else if (key.endsWith("/PASSWORD")) {
            	table.get(1).add(new KeyValue(key, value));
            }
        }
        
        //Debug.println("table[0]=" + table[0]);
        //Debug.println("table[1]=" + table[1]);
    }
        
    /**
     * Return true if the profile change represented by the
     * key and value would conflict with someone else's name
     * or password.
     * @param index set to 0 to compare names, 1 to compare passwords.
     */
    private boolean isDuplicate(int index, String key, String value) {
        // Loop thru the table to see if this value is associated with
        // a different key.
        
        for (Iterator<KeyValue> i = table.get(index).iterator(); i.hasNext(); ) {
            KeyValue kv = i.next();
            //Debug.println(kv + ".conflictsWith(" + key + "," + value + ") = " 
            //    + kv.conflictsWith(key, value));
            
            if (kv.conflictsWith(key, value)) {
                return true;
            }
        }
        
        return false;
    }
   
    
    /** Return true if it is ok for the currently selected user to be deleted. */
    public boolean allowDeleteUser() {
        
        if (selectedUser == getUserID()) {
            return false;
        }
        return true;
    }
    
    /**
     * Notify the UnitProfile that we're done making changes, so 
     * it can commit them. This is a copy of the implementation
     * from ProfileTransaction. 
     */
    @Override
	public void saveProfileChanges() {
        try {
            UnitProfile.getInstance().changesApplied();
            UnitProfile.getInstance().saveChanges();
        }
        catch (java.io.IOException e) {
            Debug.println("saveProfileChanges(): " +	
                    "ignoring unrecoverable exception: " + e); 
        }
    }   


    /**
     * Sends a profile changed message to the middleware to update the profile.
     * @param triggerDownload whether or not we should trigger the app to get
     *   the current profile following the upload.
     */
    public synchronized boolean send(MessageFacadeParam parameters) {
                                                                    
        // don't send if there are no changes
        if (! UnitProfile.getInstance().getProfileInstance().isChanged())
            return true;

        // Notify the UnitProfile that we're commiting changes and sending
        // profile changes to middleware.
        UnitProfile.getInstance().changesApplied();
        final String profileChange = profileXMLBuilder.build
                                                    (UnitProfile.getInstance().getProfileInstance());
        // Unless we're in demo mode, go ahead and try to send up the profile
        // changes to the middleware.
        if (! UnitProfile.getInstance().isDemoMode()) {
            // make sure we don't send any unnecessary updates
            if (profileChange.indexOf(EMPTY_PROFILE_CHANGE) < 0) {
                // Send the profile change to the middleware.  Let the middleware
                // apply our changes and then its changes.  Then ask for a new
                // profile from the middleware.

                Object success = MessageFacade.getInstance().sendProfile(parameters, profileChange);
                if (success.equals(Boolean.FALSE))
                    return false;
            }

            // set the profile change flags all back to false
            profileXMLBuilder.resetProfileChangedFlag(UnitProfile.getInstance().getProfileInstance());
            
            // Note: I'm rather dubious of the above method. --gea
            
            // In AgentConnect, Lisa says that after a POS sends up profile changes, it is
            // supposed to turn around and get a new profile. That is, we're supposed to 
            // treat our local copy of the profile as being out of date with respect to 
            // the middleware's copy. The following method accomplishes that, though it
            // doesn't guarantee that we'll go get a new profile right away. 
            UnitProfile.getInstance().setProfileChanged();
        }
        return true;
    } // END-METHOD

    /**
     * Determine whether a new profile exists in the middleware, and if it
     * does, retrieve it.  This method is synchronized to fix problems with
     * multiple getCurrentProfileMsg messages being sent to the middleware.
     * @return A boolean stating that the profile was retrieved properly.
     * @author Chris Bartling
     */
    public synchronized boolean getUpdatedProfile(final MessageFacadeParam parameters) {
        // don't retrieve the profile if there are no changes
        
        newProfile = ""; 	
        
        if (! UnitProfile.getInstance().isProfileChanged())
            return false;

        // Now obtain the new profile.
             
        newProfile = MessageFacade.getInstance().getProfile(parameters);
        if (newProfile == null)
            return false;

        // install the new profile
        UnitProfile.getInstance().setNewProfile(newProfile);

        /*
         * Since the TimeZone may have changed and in Vista and Windows 7 we 
         * cannot trust the default Time Zone to be set correctly, we need to 
         * verify the TimeZone setup.  
         */
        Scheduler.verifyTimeZone();
        
        MoneyGramSendTransaction.populateSendReceiptLanguageList();
        MoneyGramReceiveTransaction.populateRecvReceiptLanguageList();
        
        return true;
    }

    /**
     * Write a diagnostic log message to the diagnostic log.
     * @param itemTag A String representing the profile item tag which was
     * involved in the change.
     * @param itemValue A String representing the profile item value which
     * was involved in the change.
     * @author Chris Bartling
     *
     * Note: As of version 2.9, DiagLog.getInstance().writeDWProfileChange
     * doesn't do anything, therefore we don't need this method.
     */
    public void writeProfileChangeDiagLogMessage(String itemTag, String itemValue) {
    }
    
    public void setPrinterChanged(boolean b){
        printerChanged = b;
    }
    
    public boolean getPrinterChanged(){
        return printerChanged;
    }

    public void setCommPort(String s){
    	String comm = "1";
    	if (s.length() > 3 && s.startsWith("COM")){
    		comm = s.substring(3);
    	}
    		
    	UnitProfile.getInstance().set("COM_PORT",comm);
    }
    
    public void setModemChanged(boolean b){
        modemChanged = b;
    }
    
    public boolean getModemChanged(){
        return modemChanged;
    }
    
    /** 
     * Commits changes. First, loops through the list of changes the user
     * has made, and deletes any that are not different from the current 
     * contents of the unit profile. Then performs validation checks.
     * If everything is ok, saves the changes to the unit profile.
     * @return true if all the changes were valid and saved
     */
    public boolean commit() {
        String path, newValue, currency;
        Iterator<String> i1;
        ProfileItem item;

        // remove "changes" that aren't changed, and as long as we're
        // iterating, see if we have to do fee table validation
        
        feeChange = false;

        i1 = changes.keySet().iterator();
        while (i1.hasNext()) {
            path = i1.next();
            newValue = changes.get(path);
            if (path.endsWith("/"+DWValues.E_MAX_SEND_AMOUNT)) {
            	int seperator = newValue.indexOf("/");
            	if (seperator != -1) {
                		currency = newValue.substring(0,seperator);
                		newValue = newValue.substring(seperator+1);
                        item = UnitProfile.getInstance().getProfileInstance().
                        getCurr(path, currency);
            	}
            	else {
                    item = UnitProfile.getInstance().getProfileInstance().get(path);
            	}
            }
            else {
                item = UnitProfile.getInstance().getProfileInstance().get(path);
            }
            //Debug.println("commit " + path + "=" + newValue + " (was " +
            //        ((item == null) ? "null" : item.stringValue()) + ")");	

            if (item == null) {
                // new item will have to be created
                // i.remove();
            }
            else if (item.stringValue().equals(newValue)) {
            	i1.remove();
            }
            else            
            	oldValues.put(path, item.stringValue());
            
            if (path.endsWith("/FEE") || path.endsWith("/BREAKPOINT")) {  
                // There's been a change to a fee or breakpoint; have to do
                // a validity check on the fee table as a whole.
                feeChange = true;
            }
        }

        // if there isn't anything left, we're done

        //Debug.println("commit() changes = " + changes.entrySet());
        
        if (changes.isEmpty()) {
            //Debug.println("changes.isEmpty() = true");
            return true;
        }
        
        // validate changes

        int errorCode = validate();
        
        if (errorCode == VALIDATE_BREAKPOINTS_NOT_ASCENDING) {
            Dialogs.showError("dialogs/BlankDialog.xml", 
                Messages.getString("SimplePMTransaction.2033"));
            return false;
        }
        else if (errorCode == VALIDATE_DUPLICATE_NAME) {
            Dialogs.showError("dialogs/DialogDuplicateName.xml");
            return false;
        }
        else if (errorCode == VALIDATE_DUPLICATE_PASSWORD) {
            Dialogs.showError("dialogs/DialogDuplicatePassword.xml");
            return false;
        }
        else if (errorCode == VALIDATE_SHORT_PASSWORD) {
            Dialogs.showError("dialogs/DialogShortPasswordLength.xml");
            return false;
        }
        else if (errorCode == VALIDATE_PASSWORD_LENGTH) {
            Dialogs.showError("dialogs/DialogMaxPasswordLength.xml");
            return false;
        }
        // save changes
        
        // this loop looks a lot like the one above. Should it
        // be factored out into a method that takes a closure? 

        Iterator<String> i2 = changes.keySet().iterator();
        while (i2.hasNext()) {
            path = i2.next();
            newValue = changes.get(path);
            if (newUsers != null) {
                /*
                 * If looking for Max Send Amount
                 */
        		String maxSendAmtName = "/"+DWValues.E_MAX_SEND_AMOUNT+"/";
            	int currIdx = path.lastIndexOf(maxSendAmtName);
                if ( currIdx != -1) {
                	/*
                	 * Path is path/curr.  Separate them into
                	 * their components. 
                	 */
                	currIdx += maxSendAmtName.length();
            		currency = path.substring(currIdx);
                	path = path.substring(0,currIdx-1);
                    item = newUsers.getCurr(path, currency);
                }
            	else {
                    item = newUsers.get(path);
            	}
            }
            else if (payees != null) {
                item = payees.get(path);
            }
            else {
                /*
                 * If looking for Max Send Amount
                 */
            		String maxSendAmtName = "/"+DWValues.E_MAX_SEND_AMOUNT+"/";
                	int currIdx = path.lastIndexOf(maxSendAmtName);
                    if ( currIdx != -1) {
                    	/*
                    	 * Path is path/curr.  Separate them into
                    	 * their components. 
                    	 */
                    	currIdx += maxSendAmtName.length();
                		currency = path.substring(currIdx);
                    	path = path.substring(0,currIdx-1);
                        item = UnitProfile.getInstance().getProfileInstance().
                        		getCurr(path, currency);
                    }
            	else {
                    item = UnitProfile.getInstance().getProfileInstance().get(path);
            	}
            }
            
            if (item == null) {
                Debug.println("attempt to set non-existent profile item " + path);	
            }
            else {
                if (path.endsWith("USE_AGENT_PRINTER")) {	
                	item.setValue(DWValues.getAgentPrinterListKey(newValue));
                } else {
        			//Debug.println("saving " + path + "=" + newValue);	 
                	item.setValue(newValue);
                }
                if (path.endsWith("EMPLOYEE_STATUS")) {	
                    item.getParent().setStatus(newValue);
                }
                if (item.getTag().equals("PASSWORD") && newUsers == null) {
                	AuditLog.writeProfileChangeRecord(userID, item.getTag(),item.getParent().get("USER_ID").stringValue(), oldValues.get(path));
                }
                else {
                	if (item.getParent() != null) {
                    	if (item.getParent().getTag().equals("USER"))  {
                    		if (newUsers == null)
                    			AuditLog.writeProfileChangeRecord(userID, item.getTag(), newValue, item.getParent().get("USER_ID").stringValue(), oldValues.get(path));
                    	}
                    	else if (item.getParent().getTag().equals("PRODUCT"))  {
                    		AuditLog.writeProfileChangeRecord(userID, item.getParent().getID(), item.getTag(), newValue,  oldValues.get(path));                		
                    	}
                    	else
                    		AuditLog.writeProfileChangeRecord(userID, item.getTag(), newValue, oldValues.get(path));
                	}
                	else
                		AuditLog.writeProfileChangeRecord(userID, item.getTag(), newValue, oldValues.get(path));
                }
            }
          if (path.startsWith("SA_") || path.startsWith("ALLOW_NET_SENDS")){
               saProfileItemFlag = true;
          }
                
        }

        // loop through new users, insert them into the unit profile
        if (newUsers != null) {
        	Iterator<ProfileItem> i3 = newUsers.iterator();
            while (i3.hasNext()) {
                item = i3.next();
               
                // ignore users that were added and immediately deleted
                if ("A".equals(item.getStatus())) {	        
                    UnitProfile.getInstance().getProfileInstance().add(item);                 
                    AuditLog.writeProfileChangeRecord(userID, item.getTag(), item.get("NAME").stringValue(),"N/A");
                }
            }
        }

        // loop through temp copy of payee table, insert into unit profile
        if (payeeTable != null) {
        	Iterator<ProfileItem> i4 = payeeTable.iterator();
            while (i4.hasNext()) {
                item = i4.next();
                if (item.stringLength() != 0) {
                    // non-empty payees are active now, regardless of previous status
                    item.setStatus("A");        // active	
                }
                else {
                    if ("T".equals(item.getStatus())) {	
                        // ignore empty payee that was temporarily added
                        i4.remove();
                    }
                    else {
                        // treat previously existing payee which is now empty
                        // as a (pending) delete
                        item.setStatus("D");	
                    }
                }
            }
            // replace 
            Profile unitProfile = UnitProfile.getInstance().getProfileInstance();
            Profile currentTable = (Profile) unitProfile.get("PAYEE_TABLE");	
            unitProfile.remove(currentTable);
            unitProfile.add(payeeTable);          
       }
           
        saveProfileChanges();
        //if in demo mode and superagent profile item is checked in the General option screen
        // refresh main Panel.
        if(UnitProfile.getInstance().isDemoMode() && UnitProfile.getInstance().find("SUPERAGENT").isChanged()){
            MainPanelDelegate.refreshMainPanel();
        }
//      send up SUBAGENT_MODE profile changes.
        final MessageFacadeParam param =
                    new MessageFacadeParam(MessageFacadeParam.REAL_TIME);
        if(saProfileItemFlag){
           boolean  successfulSend = send(param);
            if (!successfulSend)
                return false;
         }
        
        if(printerChanged){
            if (UnitProfile.getInstance().get("USE_AGENT_PRINTER","Y").equals("N") || UnitProfile.getInstance().get("USE_AGENT_PRINTER","Y").equals("NP")){
                Debug.println("Clearing generic printer and setting up Epson printer...");
                GenericPrinter.clearInstance();
                EpsonSpooledPrinter.clearInstance();
                EpsonTMT88II.getInstance();
            }
            else if (UnitProfile.getInstance().get("USE_AGENT_PRINTER","Y").equals("S") || UnitProfile.getInstance().get("USE_AGENT_PRINTER","Y").equals("SP")){
                Debug.println("Clearing generic printer and setting up Epson printer with spooler...");
                GenericPrinter.clearInstance();
                EpsonTMT88II.clearInstance();
                EpsonSpooledPrinter.getInstance();
            }
            else{
                Debug.println("Clearing Epson printer and setting up generic printer...");
                EpsonTMT88II.clearInstance();
                EpsonSpooledPrinter.clearInstance();
                GenericPrinter.getInstance();
            }
            printerChanged = false;
        }
 
        return true;
    }

	@Override
	public boolean isFinancialTransaction() {
	    return false;
	}

    @Override
	public void setFinancialTransaction(boolean b)
    {
        // Do nothing.
    }
	
	@Override
	public boolean isCheckinRequired() {
		return false;
	}
}

/*
        Background:
       
        When the Deltaworks GUI was in the early design stages, none of the
        stakeholders had strong opinions about the profile maintenance
        screens. It was not considered particularly important, as long as 
        the feature took advantage of standard GUI elements and was easier
        to use than its DNet counterpart. Given how profile settings are 
        set on the DNet, making something easier was no great challenge.
       
        My orignal plan was for the screens to be generated straight from
        the profile data, with each parent node becoming a screen or a 
        group, and each leaf value becoming a text field or other control.
        (This would have been similar to what I later did in the Agent
        Connect Simulator.) A simple algorithm could have figured out which
        controls to use for which profile items, and field names could have
        been generated from the XML tags. As with the AC Simulator, a lookup
        table would probably be necessary to handle exceptional cases not 
        handled by the general algorithm. The major drawback was that the 
        order of the fields and controls on the screen would have been 
        unspecified, though in practice closely related items would have 
        wound up adjacent to each other due to how the XML representation
        of the profile is generated. (Again like the AC Simulator.)

        The job was assigned to one of the contract programmers, who mostly
        followed the above design. Then some screenshots were shown to a 
        stakeholder who didn't remember the "don't care" conclusion of the 
        GUI design meetings. He didn't like the disorganization of the 
        screens and (at the request of the lead analyst) described how he 
        thought the profile items should be grouped and sorted.

        (In retrospect, I think the screens would have been better received 
        if they had depicted a realistic number of store-changeable profile 
        items. After all, how disorganized can a list of seven items be?)

        At that point, the smart thing would have been to abandon the
        original design, and write the screens using the same techniques
        as every other screen, with an XML file for each screen specifying 
        the layout of the controls and text. That's not what the contractor
        did. He stuck with the data-driven design, turning the lookup table
        of exceptions into a huge "profile cross-ref table". He was also
        talked into using an over-engineered framework for validating 
        profile values which was written by another contractor for the 
        middleware profile editor. (I don't think the code reuse was worth
        the extra coupling between POS and middleware code, and in any case
        the code was not a good fit with the screen flow framework.)

        The result was nearly unmaintainable, and as bugs crop up the 
        temptation to rewrite the whole thing is too hard to resist.
        -- Geoff Atkin, 5/7/2001
*/

