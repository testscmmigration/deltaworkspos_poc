package dw.simplepm;


import dw.framework.XmlDefinedPanel;
import dw.i18n.Messages;

/** Represents one user on the list of users. */
class UserElement implements Comparable<UserElement> {
    String name;
    int id;
    XmlDefinedPanel panel = null;
    
    public UserElement(String name, int id) {
        this.name = name;
        this.id = id;
    }
    
    @Override
	public String toString() {
        return name + Messages.getString("UserElement.2036") + id + ")";	 
    }
    
    @Override
	public int compareTo(UserElement x) {
        return this.id - x.id;
    }
}

