package dw.simplepm;
import java.awt.Color;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import com.moneygram.agentconnect.DwPasswordReasonCodeType;

import dw.comm.MessageFacade;
import dw.dialogs.Dialogs;
import dw.dwgui.CityProviderNumberListComboBox;
import dw.dwgui.MoneyField;
import dw.dwgui.MultiListComboBox;
import dw.dwgui.StateListComboBox;
import dw.framework.ClientTransaction;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNameInterface;
import dw.framework.TransactionInterface;
import dw.framework.XmlDefinedPanel;
import dw.gui.Gui;
import dw.gui.GuiException;
import dw.i18n.Messages;
import dw.io.report.AuditLog;
import dw.main.MainPanelDelegate;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.CountryInfo.Country;
import dw.model.adapters.CountryInfo.CountrySubdivision;
import dw.profile.Profile;
import dw.profile.ProfileItem;
import dw.profile.UnitProfile;
import dw.profile.UnitProfileInterface;
import dw.utility.AccessNumbers;
import dw.utility.CityProviderNumberInfo;
import dw.utility.DWValues;
import dw.utility.Debug;

/** 
 * Simplified rewrite of ProfileWizard. Like any subclass of 
 * TransactionInterface, the idea is that this class manages the 
 * individual screens in a transaction. In the case of the profile 
 * maintenance transaction, the user only sees one screen at a time, 
 * selected by which menu item was chosen, and there is no distinction
 * between wizard and expert mode.
 * 
 * Eventually, this class is intended to replace ProfileTransactionInterface
 * and ProfileWizard.
 * 
 * @author Geoff Atkin
 */
public class SimplePMTransactionInterface extends TransactionInterface
        implements ListSelectionListener {
	private static final long serialVersionUID = 1L;

    /** Refers to the transaction we're connected to. */
    protected SimplePMTransaction transaction;
    
    /** Contains GUI components for the options in this transaction. */
    private Gui components;

    /** For Manager options screen, the JList of users on the left. */
    private JList userList = null;
    private MultiListComboBox<String> printerList = null;
    private MultiListComboBox<String> agentPrinterList = null;
    private MultiListComboBox<String> receiptTypeList = null;
    private MultiListComboBox<String> receiptPaperList = null;
    private JPanel modemPanel = null;
    private MultiListComboBox<String> modemList = null;
    private String[] availableModems = null;
    private MultiListComboBox<String> commPortList = null;
    private String[] availableCommPorts = null;
    private MultiListComboBox<String> posHostConnection = null;

    
    /** Refers to the object that wants to know the page name. */
    private PageNameInterface pni;
    
    /** Used by randomize. */
    private JTextField pf = null;
    private JTextField tempPasswdField = null;
    boolean newUserOrManager=false;
    /** Used by Dial Up Networking HBOX logic  */
    private MultiListComboBox<Country> countryBox;
    private MultiListComboBox<Country> secondaryCountryBox;
    private StateListComboBox<CountrySubdivision> stateBox;
    private StateListComboBox<CountrySubdivision> secondaryStateBox;
    private CityProviderNumberListComboBox cityProviderBox;
    private CityProviderNumberListComboBox secondaryCityProviderBox;
    private JTextField hostPrimaryPhone;
    private JTextField hostSecondaryPhone;
    private JCheckBox passLogon;
    private JCheckBox secondaryPassLogon;
    private JLabel currencyLabel1, currencyLabel2;
    private JLabel receiveRegistrationLabel, directedSendsLabel;
//    private JTextField employeeMaxAmountField;
    private JPanel employeeMaxAmountPanel;
    private JPanel utilityFeePanel;
    private JCheckBox maxFeeCheckBox;
    private JTextField utilityBillFeeField;
    private JTextField saLimitField;
    private String[] noModemAvailable = {"<< No Modem Available >>"};
    private int MONEY_GRAM_SEND_DOC_SEQ = 3;
    private int BILL_PAYMENT_DOC_SEQ = 5;
    private String currentPassword;
    private JButton resetPIN1,resetPIN2;
    private JLabel userStatusNameLabel = null;
    private JLabel userStatusLockedValueLabel = null;
    private JLabel userStatusUnlockedValueLabel = null;
    private JTable table = null;
    private DefaultTableModel tableModel = null;
    //Column names
    private static final int CURRENCY_COLUMN = 0;
    private static final int EMP_MAX_AMT_COLUMN = 1;
    
	private List<String> currencyList;
	
   // private ValidatePinResponse response = null;
    boolean saveDecision = true;
    boolean unmaskedVisible = false;
    boolean maskedVisible = false;
//    boolean resetButtonClicked = false;
//    String changedPassword = null;
    ArrayList<EmpData> empList = new ArrayList<EmpData>();
    
    
    /**
     * Contains the names of the XML files for the screens. Note: this
     * table needs to be filled in as more screens are implemented.
     * This array is indexed by the constants in SimplePMTransaction.
     */
    private String[] files = {"simplepm/GeneralProfileOptions.xml",	
        "simplepm/ProxySettingOptions.xml",
        "simplepm/DispenserOptions.xml",			
        "simplepm/PayeeTableOptions.xml",			
        "simplepm/MoneyOrderOptions.xml",			
        "simplepm/VendorPaymentOptions.xml",		
        "simplepm/MoneyGramSendOptions.xml",		
        "simplepm/MoneyGramReceiveOptions.xml",		
        "simplepm/BillPaymentOptions.xml",		
        "simplepm/UserProfileOptions.xml",			
        "simplepm/ManagerProfileOptions.xml",   	
        "simplepm/PrinterOptions.xml",       		
        "simplepm/DialupSettingOptions.xml", 		
        "simplepm/DialupSettingOptionsSecondary.xml", 		
        "simplepm/CardPurchaseOptions.xml", 		
        "simplepm/CardReloadOptions.xml"}; 			
        

    /** Contains the page identifiers for the title bar. */
    private String[] pageNames = {"PM05", "PM10", "PM15", "PM20",    
        "PM25", "PM30", "PM35", "PM40", "PM45","PM50","PM55","PM60",      
        "PM65", "PM66", "PM70", "PM75"};
    /**
     * Constructs a new SimplePMTransactionInterface. The page name 
     * interface is used to inform the enclosing frame when the title
     * bar should be changed. This class manages its screens differently
     * than typical subclasses of PageFlowContainer, so the page names
     * are also handled here.
     */
    public SimplePMTransactionInterface(SimplePMTransaction transaction,
            PageNameInterface pni) {
        super("simplepm/SimpleProfileMaintenance.xml", null, false);	
        
        //These are newly added tags, set method creates the tag if one doesn't exists already
        UnitProfile.getInstance().set("PROVIDER_ACCESS_NUMBER_COUNTRY",
    			UnitProfile.getInstance().get("PROVIDER_ACCESS_NUMBER_COUNTRY", ""), "P", true);
    	UnitProfile.getInstance().set("PROVIDER_ACCESS_NUMBER_STATE",
    			UnitProfile.getInstance().get("PROVIDER_ACCESS_NUMBER_STATE", ""), "P", true);
    	UnitProfile.getInstance().set("PROVIDER_ACCESS_NUMBER_SELECTION",
    			UnitProfile.getInstance().get("PROVIDER_ACCESS_NUMBER_SELECTION", ""), "P", true);
    	UnitProfile.getInstance().set("PRIMARY_SCRIPT_USER_NAME",
    			UnitProfile.getInstance().get("PRIMARY_SCRIPT_USER_NAME", ""), "S", true);
    	UnitProfile.getInstance().set("PRIMARY_SCRIPT_PASSWORD",
    			UnitProfile.getInstance().get("PRIMARY_SCRIPT_PASSWORD", ""), "S", true);
    	UnitProfile.getInstance().set("PASS_THROUGH_LOGON",
    			UnitProfile.getInstance().get("PASS_THROUGH_LOGON", ""), "S", true);
    	UnitProfile.getInstance().set("PRIMARY_DIALUP_SCRIPT_NAME",
    			UnitProfile.getInstance().get("PRIMARY_DIALUP_SCRIPT_NAME", ""), "S", true);
    	UnitProfile.getInstance().set("PRIMARY_SCRIPT_DELAY_PHONE_DIAL",
    			UnitProfile.getInstance().get("PRIMARY_SCRIPT_DELAY_PHONE_DIAL", "1"), "S", true);
    	UnitProfile.getInstance().set("PRIMARY_SCRIPT_DELAY_PT_USERNAME",
    			UnitProfile.getInstance().get("PRIMARY_SCRIPT_DELAY_PT_USERNAME", "1"), "S", true);
    	UnitProfile.getInstance().set("PRIMARY_SCRIPT_DELAY_PT_PASSWORD",
    			UnitProfile.getInstance().get("PRIMARY_SCRIPT_DELAY_PT_PASSWORD", "1"), "S", true);
    	UnitProfile.getInstance().set("PRIMARY_SCRIPT_DELAY_A",
    			UnitProfile.getInstance().get("PRIMARY_SCRIPT_DELAY_A", "1"), "S", true);
    	UnitProfile.getInstance().set("PRIMARY_SCRIPT_DELAY_B",
    			UnitProfile.getInstance().get("PRIMARY_SCRIPT_DELAY_B", "1"), "S" , true);
        UnitProfile.getInstance().set("MODEM_NAME",
                 UnitProfile.getInstance().get("MODEM_NAME", ""), "P" , true);
        
        UnitProfileInterface upi = UnitProfile.getInstance();
        String sValue = upi.get(DWValues.PROFILE_PRINTER_RCPT_TYPE, "");
        if (sValue.length()== 0) {
        	upi.set(DWValues.PROFILE_PRINTER_RCPT_TYPE, 
        			DWValues.AGENT_PRINTER_TYPE_DEFAULT, "S" , true);
        }
        sValue = upi.get(DWValues.PROFILE_PRINTER_PAPER_SIZE, "");
        if (sValue.length()== 0) {
        	upi.set(DWValues.PROFILE_PRINTER_PAPER_SIZE, 
        			DWValues.AGENT_PRINTER_PAPER_SIZE_DEFAULT, "S" , true);
        }
        
        this.transaction = transaction;
        this.pni = pni;
        //Make the transaction available to the profile panel
        SimplePMTransaction.setCurrentInstance(transaction);
        
    }

    /** 
     * Sets up the screen. Adds action listeners for the buttons created 
     * by createButtons. Gets the current option from the transaction.
     */
    @Override
	public void start() {
        // cardPanel is set by a superclass constructor to the card
        // layout panel named "pageFlowPanel" in the XML.
        cardPanel.removeAll();
        transaction.setStatus(ClientTransaction.IN_PROCESS);
        
        buttons.reset();
        addKeyMappings();
        addActionListener("addUserButton", this, "addUser");	 
        addActionListener("addManagerButton", this, "addManager");	 
        addActionListener("deleteButton", this, "deleteUser");	 
        addActionListener("saveButton", this, "save");	 
        addActionListener("cancelButton", this, "cancel");	 

        // Make the transaction available to the profile panel
        SimplePMTransaction.setCurrentInstance(transaction);
        
        // Could use an XmlDefinedPanel, which would be more consistent
        // with other transactions. But that class has a whole lot of 
        // baggage which isn't relevant here.
        
        int option = transaction.getCurrentOption();
                
        if (option == SimplePMTransaction.USER_OPTIONS)
            transaction.setSelectedUser(transaction.getUserID());
      
        java.net.URL resource = ClassLoader.getSystemResource(
                XmlDefinedPanel.getXmlPath() + files[option]);
        pni.setCurrentPageName(pageNames[option]);
        

        try {
            components = new Gui(resource);
            Object c = components.get("main");	
            if ((c != null) && (c instanceof JComponent)){
                cardPanel.add((JComponent) c, "panel"); 
            }

            if (components.containsKey("product)")) {	
                String product = components.getParamValue("product"); 
                transaction.setProduct(Integer.parseInt(product));
            }

            if (components.containsKey("currencyLabel1")) {
            	currencyLabel1 = (JLabel) components.get("currencyLabel1");
            	currencyLabel1.setText(CountryInfo.getAgentCountry().getBaseReceiveCurrency());
            }
            
            if (components.containsKey("currencyLabel2")) {
            	currencyLabel2 = (JLabel) components.get("currencyLabel2");
            	currencyLabel2.setText(CountryInfo.getAgentCountry().getBaseReceiveCurrency());
            }
            
            if (components.containsKey("directedSendsLabel")) {
            	directedSendsLabel = (JLabel) components.get("directedSendsLabel");
            	if (UnitProfile.getInstance().isDemoMode())
            		directedSendsLabel.setVisible(true);
            	else
            		directedSendsLabel.setVisible(false);
            }
            
            if (components.containsKey("receiveRegistrationLabel")) {
            	receiveRegistrationLabel = (JLabel) components.get("receiveRegistrationLabel");
            	ProfileItem p = UnitProfile.getInstance().getProfileInstance().get("PRODUCT[9]/NAME_PIN_REQUIRED");
            	if (! p.getMode().equals("H"))
					receiveRegistrationLabel.setVisible(true);
				else
					receiveRegistrationLabel.setVisible(false);
            	
            }
          
            if(components.containsKey("UtilityFeePanel")){
                utilityFeePanel = (JPanel)components.get("UtilityFeePanel");
            ProfileItem p = UnitProfile.getInstance().getProfileInstance().get("PRODUCT[5]/UTILITY_BILL_PAY_INFO_FEE");
            if(p==null){
               utilityFeePanel.setVisible(false);
            }
            else {
                maxFeeCheckBox = (JCheckBox)components.get("maxFeeCheckBox");
                utilityBillFeeField = (JTextField)components.get("utilityBillFeeField");
                if(maxFeeCheckBox.isSelected()){
                    utilityBillFeeField.setEditable(false);
                    utilityBillFeeField.setEnabled(false); 
                }
                maxFeeCheckBox.addActionListener(new ActionListener(){
                    @Override
					public void actionPerformed(ActionEvent e){
                        if(((JCheckBox)e.getSource()).isSelected()){
                            utilityBillFeeField.setEditable(false);
                            utilityBillFeeField.setEnabled(false); 
                            
                        }
                        else{
                            utilityBillFeeField.setEditable(true);
                            utilityBillFeeField.setEnabled(true); 
                        }
                            
                    }
                });                                                   
                }
            }
           

        	/* 
        	 * Method used in the DialUp settings to populate the countryBox and stateBox. Reference to
			 * updateStateList
			 *  
			 */
        	if (components.containsKey("countryBox")) {
        	    
				countryBox = (MultiListComboBox<Country>) components.get("countryBox");
               // countryBox.getComponents().toString();
				stateBox = (StateListComboBox<CountrySubdivision>) components.get("stateBox");
				cityProviderBox = (CityProviderNumberListComboBox) components.get("cityPhoneProviderBox");
				countryBox.addList(CountryInfo.getAccessCountryList(), "countries");
				countryBox.setSelectedItem(UnitProfile.getInstance().get("PROVIDER_ACCESS_NUMBER_COUNTRY",""));
				AccessNumbers.populateStateList(stateBox);				
				String state = UnitProfile.getInstance().get("PROVIDER_ACCESS_NUMBER_STATE","");
				
				if (! state.equals("") && (state.indexOf("-") > 0)) {
					stateBox.setSelectedItem(state.substring(0, state.indexOf("-")).trim());
				}
				AccessNumbers.updatecityProviderPhoneNumber((Country) countryBox.getSelectedItem(), (CountrySubdivision) stateBox.getSelectedItem(), cityProviderBox);
				countryBox.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent ae) {
							AccessNumbers.updateStateList((Country) countryBox.getSelectedItem(), stateBox);
							AccessNumbers.updatecityProviderPhoneNumber((Country) countryBox.getSelectedItem(), (CountrySubdivision) stateBox.getSelectedItem(), cityProviderBox);
						}

				});
								
				stateBox.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent ae) {

						AccessNumbers.updatecityProviderPhoneNumber((Country) countryBox.getSelectedItem(), (CountrySubdivision) stateBox.getSelectedItem(), cityProviderBox);
					}
				});
			}
        	
        	// for the secondary dialup networking screen
        	if (components.containsKey("secondaryCountryBox")) {
        	    
				secondaryCountryBox = (MultiListComboBox<Country>) components.get("secondaryCountryBox");
				secondaryStateBox = (StateListComboBox<CountrySubdivision>) components.get("secondaryStateBox");
				secondaryCityProviderBox = (CityProviderNumberListComboBox) components.get("secondaryCityPhoneProviderBox");
				secondaryCountryBox.addList(CountryInfo.getAccessCountryList(), "countries");
				secondaryCountryBox.setSelectedItem(UnitProfile.getInstance().get("SECONDARY_PROVIDER_ACCESS_NUMBER_COUNTRY",""));
				AccessNumbers.populateStateList(secondaryStateBox);				
				String secondaryState = UnitProfile.getInstance().get("SECONDARY_PROVIDER_ACCESS_NUMBER_STATE","");
				
				if(! secondaryState.equals("") && (secondaryState.indexOf("-") > 0 )) {
					secondaryStateBox.setSelectedItem(secondaryState.substring(0,secondaryState.indexOf("-")).trim());
				}
				AccessNumbers.updatecityProviderPhoneNumber((Country) secondaryCountryBox.getSelectedItem(), (CountrySubdivision) secondaryStateBox.getSelectedItem(), secondaryCityProviderBox);
				secondaryCountryBox.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent ae) {
							AccessNumbers.updateStateList((Country) secondaryCountryBox.getSelectedItem(), secondaryStateBox);
							AccessNumbers.updatecityProviderPhoneNumber((Country) secondaryCountryBox.getSelectedItem(), (CountrySubdivision) secondaryStateBox.getSelectedItem(), secondaryCityProviderBox);
						}

				});
								
				secondaryStateBox.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent ae) {

						AccessNumbers.updatecityProviderPhoneNumber((Country) secondaryCountryBox.getSelectedItem(), (CountrySubdivision) secondaryStateBox.getSelectedItem(), secondaryCityProviderBox);
					}
				});
			}

        	// This block populates the cityPhoneProviderBox.
            if (components.containsKey("cityPhoneProviderBox")) {
            	hostPrimaryPhone = (JTextField) components.get("hostPrimaryPhone");
            	cityProviderBox = 
            		(CityProviderNumberListComboBox) components.get("cityPhoneProviderBox");
				cityProviderBox.setSelectedItem(
						UnitProfile.getInstance().get("PROVIDER_ACCESS_NUMBER_SELECTION",""));
				if(hostPrimaryPhone.getText().trim().equals(""))
					hostPrimaryPhone.setText(((CityProviderNumberInfo)cityProviderBox.getSelectedItem()).getNumber());
            	passLogon = (JCheckBox) components.get("passThroughLogon");
            	cityProviderBox.addActionListener(new ActionListener(){
            		@Override
					public void actionPerformed(ActionEvent ae){
                    		CityProviderNumberInfo cityProviderInfo = 
                    			(CityProviderNumberInfo)(
                    				(CityProviderNumberListComboBox)ae.getSource()).getSelectedItem();
                    		hostPrimaryPhone.setText(cityProviderInfo.getNumber());
	            			if(cityProviderInfo.getScript().equalsIgnoreCase("None")){
	            				passLogon.setSelected(false);
	            				passThroughLogonValidation(true, false, false);
	            				if(passLogon.isSelected()){
	            					UnitProfile.getInstance().set("PRIMARY_DIALUP_SCRIPT_NAME",
	            						"MCI", "S", false);
	            				}else{
	            					UnitProfile.getInstance().set("PRIMARY_DIALUP_SCRIPT_NAME",
	            						"", "S", false);
	            				}
	            			}else{
	            				passLogon.setSelected(true);
	            				passThroughLogonValidation(true, true, false);
	        					String str = cityProviderInfo.getScript();
	        					UnitProfile.getInstance().set("PRIMARY_DIALUP_SCRIPT_NAME",
	        							str.trim(), "S", false);
	            			}
            		}
            	});
            }            
            
        	// This block populates the secondaryCityPhoneProviderBox.
            if (components.containsKey("secondaryCityPhoneProviderBox")) {
            	hostSecondaryPhone = (JTextField) components.get("hostSecondaryPhone");
            	secondaryCityProviderBox = 
            		(CityProviderNumberListComboBox) components.get("secondaryCityPhoneProviderBox");
				secondaryCityProviderBox.setSelectedItem(
						UnitProfile.getInstance().get("SECONDARY_PROVIDER_ACCESS_NUMBER_SELECTION",""));
				if(hostSecondaryPhone.getText().trim().equals(""))
					hostSecondaryPhone.setText(((CityProviderNumberInfo)secondaryCityProviderBox.getSelectedItem()).getNumber());
            	secondaryPassLogon = (JCheckBox) components.get("secondaryPassThroughLogon");
            	secondaryCityProviderBox.addActionListener(new ActionListener(){
            		@Override
					public void actionPerformed(ActionEvent ae){
                    		CityProviderNumberInfo cityProviderInfo = 
                    			(CityProviderNumberInfo)(
                    				(CityProviderNumberListComboBox)ae.getSource()).getSelectedItem();
                    		hostSecondaryPhone.setText(cityProviderInfo.getNumber());
	            			if(cityProviderInfo.getScript().equalsIgnoreCase("None")){
	            				secondaryPassLogon.setSelected(false);
	            				passThroughLogonValidation(false, false, false);
	            				if(secondaryPassLogon.isSelected()){
	            					UnitProfile.getInstance().set("SECONDARY_DIALUP_SCRIPT_NAME",
	            						"MCI", "S", false);
	            				}else{
	            					UnitProfile.getInstance().set("SECONDARY_DIALUP_SCRIPT_NAME",
	            						"", "S", false);
	            				}
	            			}else{
	            				secondaryPassLogon.setSelected(true);
	            				passThroughLogonValidation(false, true, false);
	        					String str = cityProviderInfo.getScript();
	        					UnitProfile.getInstance().set("SECONDARY_DIALUP_SCRIPT_NAME",
	        							str.trim(), "S", false);
	            			}
            		}
            	});
            }            
            
            //This block chooses the default script
            if (components.containsKey("passThroughLogon")) {
               	passThroughLogonValidation(true, passLogon.isSelected(), false);
            	passLogon.addItemListener(new ItemListener() {
                    @Override
					public void itemStateChanged(ItemEvent e) {
                    	CityProviderNumberInfo cityProviderInfo = 
                    	(CityProviderNumberInfo) cityProviderBox.getSelectedItem();
                       	passThroughLogonValidation(true, ((JCheckBox)e.getSource()).isSelected(), false);
                       	if(passLogon.isSelected()){
                       		if(cityProviderInfo.getScript().equalsIgnoreCase("None")){
                           		UnitProfile.getInstance().set("PRIMARY_DIALUP_SCRIPT_NAME",
                           			"MCI", "S", false);
                       		}else{
                       			UnitProfile.getInstance().set("PRIMARY_DIALUP_SCRIPT_NAME", 
                       				cityProviderInfo.getScript(), "S", false);
                       		}
                       	}else{
                       		if(cityProviderInfo.getScript().equalsIgnoreCase("None")){
                           		UnitProfile.getInstance().set("PRIMARY_DIALUP_SCRIPT_NAME",
                           			"", "S", false);
                       		}
                       	}
                   	}
                });
            }
            
            //This block chooses the default script
            if (components.containsKey("secondaryPassThroughLogon")) {
               	passThroughLogonValidation(false, secondaryPassLogon.isSelected(), false);
            	secondaryPassLogon.addItemListener(new ItemListener() {
                    @Override
					public void itemStateChanged(ItemEvent e) {
                    	CityProviderNumberInfo cityProviderInfo = 
                    	(CityProviderNumberInfo) cityProviderBox.getSelectedItem();
                       	passThroughLogonValidation(false, ((JCheckBox)e.getSource()).isSelected(), false);
                       	if(secondaryPassLogon.isSelected()){
                       		if(cityProviderInfo.getScript().equalsIgnoreCase("None")){
                           		UnitProfile.getInstance().set("SECONDARY_DIALUP_SCRIPT_NAME",
                           			"MCI", "S", false);
                       		}else{
                       			UnitProfile.getInstance().set("SECONDARY_DIALUP_SCRIPT_NAME", 
                       				cityProviderInfo.getScript(), "S", false);
                       		}
                       	}else{
                       		if(cityProviderInfo.getScript().equalsIgnoreCase("None")){
                           		UnitProfile.getInstance().set("SECONDARY_DIALUP_SCRIPT_NAME",
                           			"", "S", false);
                       		}
                       	}
                   	}
                });
            }

            if (components.containsKey("modemList")){
                modemList = (MultiListComboBox<String>) components.get("modemList");
                modemPanel = (JPanel)components.get("modemPanel");
                //MessageFacade.getInstance().getModemList();
                availableModems = MessageFacade.getInstance().getModemList();
                if (availableModems.length > 0) {
                	modemList.setVisible(true);
                	modemList.addList(availableModems, "modemList");               	
                }
                else {
                	modemList.addList(noModemAvailable, "modemList");
                	modemList.setEnabled(false);
                	modemPanel.setVisible(false);
                }
                
                modemList.setSelectedIndex(MessageFacade.getInstance().getSelectedModem());
                modemList.addActionListener(new ActionListener(){
                    @Override
					public void actionPerformed(ActionEvent e) {
                        if (modemList.getSelectedIndex() != MessageFacade.getInstance().getSelectedModem())
                            transaction.setModemChanged(true);                            
                    }
                });
            }         
            if (components.containsKey("commPortList")){
            	commPortList = (MultiListComboBox<String>) components.get("commPortList");
               //MessageFacade.getInstance().getModemList();
                availableCommPorts= MessageFacade.getInstance().getCommPortList();
                if (availableCommPorts.length > 0) {
                	commPortList.addList(availableCommPorts, "commPortList");               	
                }
                else {
                	commPortList.addList(availableCommPorts, "commPortList");
                	commPortList.setEnabled(false);
                }
                
                commPortList.setSelectedIndex(MessageFacade.getInstance().getSelectedCommPort());

            }
            
            if (components.containsKey("posHostConnection")) {
            	posHostConnection = (MultiListComboBox<String>) components.get("posHostConnection");
            	posHostConnection.addActionListener(new ActionListener(){
                    @Override
					public void actionPerformed(ActionEvent e) {
                        if (posHostConnection.getSelectedItem().toString().equals("D") &&
                        	availableModems.length == 0) {
                        	Dialogs.showError("dialogs/DialogNoModemAvailable.xml"); 
                            posHostConnection.setSelectedItem("N");
                        }
                    }
                });
            	
            }
            	
            if (components.containsKey("printerList")){
                printerList = (MultiListComboBox<String>) components.get("printerList");  
                printerList.addList(DWValues.getPrinterList(),"printerList");
                printerList.setSelectedIndex(DWValues.getSelectedPrinter());
                agentPrinterList = (MultiListComboBox<String>) components.get("agentPrinterList");  
            	receiptTypeList = (MultiListComboBox<String>) components.get(DWValues.PROFILE_COMBO_BOX_RCPT_TYPE);  
            	receiptTypeList.addList(DWValues.getAgentReceiptTypeValueList(),DWValues.PROFILE_COMBO_BOX_RCPT_TYPE);
            	receiptTypeList.setSelectedIndex(DWValues.getSelectedAgentReceiptType());
            	receiptPaperList = (MultiListComboBox<String>) components.get(DWValues.PROFILE_COMBO_BOX_RCPT_PAPER);  
            	receiptPaperList.addList(DWValues.getAgentReceiptPaperValueList(),DWValues.PROFILE_COMBO_BOX_RCPT_PAPER);
            	receiptPaperList.setSelectedIndex(DWValues.getSelectedAgentReceiptPaper());
            	
                final JTextField cpl = (JTextField) components.get("charactersPerLine");
                agentPrinterList.addActionListener(new ActionListener() {
                    @Override
					public void actionPerformed(ActionEvent e) {
                    	String selectedValue = DWValues.getAgentPrinterListKey((String)agentPrinterList.getSelectedItem());
                    	Profile profileInstance = UnitProfile.getInstance().getProfileInstance();
                    	ProfileItem p = profileInstance.get(DWValues.PROFILE_PRINTER_RCPT_TYPE);
                    	boolean bReceiptTypeIsNotPrivate = !p.getMode().equals("P");
                    	p = profileInstance.get(DWValues.PROFILE_PRINTER_PAPER_SIZE);
                    	boolean bReceiptPaperIsNotPrivate = !p.getMode().equals("P");

                        if (selectedValue != null && selectedValue.equals("Y")) {
                            printerList.setEnabled(true);
                            printerList.setBackground(Color.WHITE);
                            receiptTypeList.setEnabled(bReceiptTypeIsNotPrivate);
                            receiptTypeList.setBackground(Color.WHITE);
                            receiptPaperList.setEnabled(bReceiptPaperIsNotPrivate);
                            receiptPaperList.setBackground(Color.WHITE);
                            cpl.setEnabled(true);
                            cpl.setBackground(Color.WHITE);
                        } else if (selectedValue != null && (selectedValue.equals("S") || selectedValue.equals("SP"))) {
                            printerList.setEnabled(true);
                            printerList.setBackground(Color.WHITE);
                            receiptTypeList.setEnabled(bReceiptTypeIsNotPrivate);
                            receiptTypeList.setBackground(Color.WHITE);
                            receiptPaperList.setEnabled(bReceiptPaperIsNotPrivate);
                            receiptPaperList.setBackground(Color.WHITE);
                            cpl.setEnabled(false);
                            cpl.setBackground(Color.GRAY);
                        } else {
                            printerList.setEnabled(false);
                            printerList.setBackground(Color.GRAY);
                            receiptTypeList.setEnabled(false);
                            receiptTypeList.setBackground(Color.GRAY);
                            receiptPaperList.setEnabled(false);
                            receiptPaperList.setBackground(Color.GRAY);
                            cpl.setEnabled(false);
                            cpl.setBackground(Color.GRAY);
                        }
                        transaction.setPrinterChanged(true);
                    }
                });
                
                if (components.containsKey("agentPrinterList")){
                    agentPrinterList = (MultiListComboBox<String>) components.get("agentPrinterList");  
                    agentPrinterList.addList(DWValues.getAgentPrinterList(),"agentPrinterList");
                    agentPrinterList.setSelectedIndex(DWValues.getSelectedAgentPrinter());
                	String selectedValue = DWValues.getAgentPrinterListKey((String)agentPrinterList.getSelectedItem());
                	Profile profileInstance = UnitProfile.getInstance().getProfileInstance();
                	ProfileItem p = profileInstance.get(DWValues.PROFILE_PRINTER_RCPT_TYPE);
                	boolean bReceiptTypeIsNotPrivate = !p.getMode().equals("P");
                	p = profileInstance.get(DWValues.PROFILE_PRINTER_PAPER_SIZE);
                	boolean bReceiptPaperIsNotPrivate = !p.getMode().equals("P");

                    if (selectedValue != null && selectedValue.equals("Y")){
                        printerList.setEnabled(true);
                        printerList.setBackground(Color.WHITE);
                        receiptTypeList.setEnabled(bReceiptTypeIsNotPrivate);
                        receiptTypeList.setBackground(Color.WHITE);
                        receiptPaperList.setEnabled(bReceiptPaperIsNotPrivate);
                        receiptPaperList.setBackground(Color.WHITE);
                        cpl.setEnabled(true);
                        cpl.setBackground(Color.WHITE);
                    } else if (selectedValue != null && selectedValue.equals("S")){
                        printerList.setEnabled(true);
                        printerList.setBackground(Color.WHITE);
                        receiptTypeList.setEnabled(bReceiptTypeIsNotPrivate);
                        receiptTypeList.setBackground(Color.WHITE);
                        receiptPaperList.setEnabled(bReceiptPaperIsNotPrivate);
                        receiptPaperList.setBackground(Color.WHITE);
                        cpl.setEnabled(false);
                        cpl.setBackground(Color.GRAY);
                    } else {
                        printerList.setEnabled(false);
                        printerList.setBackground(Color.GRAY);
                        receiptTypeList.setEnabled(false);
                        receiptTypeList.setBackground(Color.GRAY);
                        receiptPaperList.setEnabled(false);
                        receiptPaperList.setBackground(Color.GRAY);
                        cpl.setEnabled(false);
                        cpl.setBackground(Color.GRAY);
                    }
                }
            }

            if (components.containsKey("userList")) {	
                userList = (JList) components.get("userList");	
                initializeUserList(userList);
                userList.addListSelectionListener(this);

                buttons.setVisible("addUser", true);    
                buttons.setEnabled("addUser", SimplePMTransaction.getCurrentInstance().isEmpPinPrivChangeAllowed());    
                buttons.setVisible("addManager", true); 
                buttons.setEnabled("addManager", SimplePMTransaction.getCurrentInstance().isMgrPinPrivChangeAllowed()); 
                buttons.setVisible("delete", true);	
                buttons.setEnabled("delete", false);	
            }
            else if (option == SimplePMTransaction.USER_OPTIONS){
                buttons.setVisible("addUser", false);	
                buttons.setVisible("addManager", false);	
                buttons.setVisible("delete", false);	
            }
            else {
                transaction.setSelectedUser(-1);
                buttons.setVisible("addUser", false);	
                buttons.setVisible("addManager", false);	
                buttons.setVisible("delete", false);	
            }
            
            if (components.containsKey("randomizeButton")) {
            	JTextField password = (JTextField) components.get("passwordField");
            	JButton randomize = (JButton) components.get("randomizeButton");
            	JLabel instructions = (JLabel) components.get("instructions");

                if("3DIGIT".equalsIgnoreCase(UnitProfile.getInstance().get("USER_PIN_TYPE","3DIGIT"))){
                	instructions.setEnabled(true);
                	password.setEnabled(false);
                	password.setBackground(SystemColor.control);
                	randomize.setEnabled(true);
                	randomize.addActionListener(new ActionListener() {
                		@Override
						public void actionPerformed(ActionEvent e) {
                			randomize();
                		}
                	});
                } else {
                	currentPassword = password.getText().trim();               	
                	instructions.setEnabled(false);
                	password.setEnabled(true);
                	password.setBackground(Color.white);
                	randomize.setEnabled(false);
                }
            }

            if (components.containsKey("passwordField")) {
               pf = (JTextField) components.get("passwordField");
                
            }
            
        	if (components.containsKey("sa_limit")) {
                saLimitField = (JTextField) components.get("sa_limit");
                Profile profile = UnitProfile.getInstance().getProfileInstance();
                ProfileItem mgsItem = profile.find("PRODUCT", MONEY_GRAM_SEND_DOC_SEQ);
                ProfileItem epItem = profile.find("PRODUCT", BILL_PAYMENT_DOC_SEQ);
                if (epItem.getStatus().equalsIgnoreCase("A") || 
                	mgsItem.getStatus().equalsIgnoreCase("A"))  {
                		saLimitField.setEnabled(true);
                		saLimitField.setBackground(Color.WHITE);
                }
                else {
                	saLimitField.setEnabled(false);  
                	saLimitField.setBackground(SystemColor.control);
                }            	
        	}
        	
        	
            
        }
        catch (GuiException e) {
            Debug.println("caught exception here ");
            Debug.println(e.toString());
        }
        catch (NumberFormatException nfe) {
            Debug.println("Invalid product param " + nfe.getMessage());	
        }
        
        // set initial focus on the Save button
        buttons.setSelected("save");	
     }
    
    /**
     * This method enables or disables the username and the password fields
     * based on the passThroughLogon state. 
     *  
     * @param primaryDialup true if primary dialup and false if secondary dialup
     * @param state state of the passthroughLogon
     * @param clear true resets the values of username and password.
     */
    
    public void passThroughLogonValidation(boolean primaryDialup, boolean state, boolean clear){

    	JTextField username = (JTextField)components.get("primaryScriptUsername");
    	JTextField password = (JTextField)components.get("primaryScriptPassword");
    	JTextField delayPhDial = (JTextField)components.get("primaryScriptDelayPhoneDial");
    	JTextField delayPTName = (JTextField)components.get("primaryScriptDelayPTUsername");
    	JTextField delayPTPswd = (JTextField)components.get("primaryScriptDelayPTPassword");
    	JTextField delayA = (JTextField)components.get("primaryScriptDelayA");
    	JTextField delayB = (JTextField)components.get("primaryScriptDelayB");
    	
    	if (!primaryDialup) {
        	username = (JTextField)components.get("secondaryScriptUsername");
        	password = (JTextField)components.get("secondaryScriptPassword");
        	delayPhDial = (JTextField)components.get("secondaryScriptDelayPhoneDial");
        	delayPTName = (JTextField)components.get("secondaryScriptDelayPTUsername");
        	delayPTPswd = (JTextField)components.get("secondaryScriptDelayPTPassword");
        	delayA = (JTextField)components.get("secondaryScriptDelayA");
        	delayB = (JTextField)components.get("secondaryScriptDelayB");
    		
    	}

    	if(state == false){
        	username.setEnabled(false);
        	password.setEnabled(false);
        	delayPhDial.setEnabled(false);
			delayPTName.setEnabled(false);
			delayPTPswd.setEnabled(false);
			delayA.setEnabled(false);
			delayB.setEnabled(false);
        	username.setBackground(Color.LIGHT_GRAY);
        	password.setBackground(Color.LIGHT_GRAY);
        	delayPhDial.setBackground(Color.LIGHT_GRAY);
			delayPTName.setBackground(Color.LIGHT_GRAY);
			delayPTPswd.setBackground(Color.LIGHT_GRAY);
			delayA.setBackground(Color.LIGHT_GRAY);
			delayB.setBackground(Color.LIGHT_GRAY);
        	if(clear){
        		username.setText("");
        		password.setText("");
        	}
        }else{
        	username.setEnabled(true);
        	password.setEnabled(true);        	
        	delayPhDial.setEnabled(true);
			delayPTName.setEnabled(true);
			delayPTPswd.setEnabled(true);
			delayA.setEnabled(true);
			delayB.setEnabled(true);
        	username.setBackground(Color.WHITE);
        	password.setBackground(Color.WHITE);
        	delayPhDial.setBackground(Color.WHITE);
			delayPTName.setBackground(Color.WHITE);
			delayPTPswd.setBackground(Color.WHITE);
			delayA.setBackground(Color.WHITE);
			delayB.setBackground(Color.WHITE);
        }
    }
    

    /** 
     * Sets up the flow buttons defined in the XML page description.
     * Overrides the definition in PageFlowContainer because this is
     * a single-screen transaction with a custom set of buttons.
     * Note: called by the constructor for PageFlowContainer.
     */
    @Override
	public PageFlowButtons createButtons() {
        JButton[] buttons = {
            (JButton) getComponent("addUserButton"),	
            (JButton) getComponent("addManagerButton"),	
            (JButton) getComponent("deleteButton"),	
            (JButton) getComponent("saveButton"),	
            (JButton) getComponent("cancelButton")};	
        String[] names = {"addUser", "addManager", "delete", "save", "cancel"};     
        return new PageFlowButtons(buttons, names, this);
    }

    /**
     * Adds keystroke mappings to the buttons.
     */
    private void addKeyMappings() {
        buttons.addKeyMapping("save", KeyEvent.VK_F5, 0);	
        buttons.addKeyMapping("cancel", KeyEvent.VK_ESCAPE, 0);	
    }
    
    /** 
     * Does nothing. Subclasses of transaction interface typically define
     * a method by this name and call it from a constructor to add pages to
     * the flow. This transaction will create pages dynamically. 
     */
    @Override
	public void createPages() {
    }

    /**
     * Clear out the contents of all contained components. For this class
     * this is the same as unloadPages().
     */
    @Override
	public void clear() {
        unloadPages();
    }

    /** 
     * Unloads pages that can be recreated later. This class only has one
     * page, which is dynamically loaded by start(), so the implementation
     * is really simple.
     */
    @Override
	public void unloadPages() {
        cardPanel.removeAll();
    }

    /** Returns the associated client transaction. */
    @Override
	public ClientTransaction getTransaction() {
        return transaction;
    }

    /** 
     * Invoked when the button is pressed to generate a random password. 
     */
    public void randomize() {
        String password = transaction.generatePassword();
        
        pf.setText(password);
    }
    
    public void resetPin() {
        String password1 = transaction.generatePassword();
        UserElement elem = (UserElement) userList.getSelectedValue();  
        EmpData user = new EmpData(elem.id,password1);
        empList.add(user);
        
        
        if(maskedVisible){
            elem.panel.getComponent("maskedPanel").setVisible(false);
            elem.panel.getComponent("unmaskedPanel").setVisible(true);
            maskedVisible = false;
     	     unmaskedVisible = true;
        }
        
    	pf = (JTextField) elem.panel.getComponent("passwordField1");
    	
    	pf.setText(password1);
    	tempPasswdField.setText(password1);
    	
		    pf.setEnabled(false); 
		  
    	   pf.setBackground(SystemColor.control); 
    }
    
    /**
     * Sets the Default values for all the delay fields
     *
     */
    public void setDefaultsValues(){
    	JTextField primaryScriptDelayPhoneDial = (JTextField)components.get("primaryScriptDelayPhoneDial");
    	JTextField primaryScriptDelayPTUsername = (JTextField)components.get("primaryScriptDelayPTUsername");
    	JTextField primaryScriptDelayPTPassword = (JTextField)components.get("primaryScriptDelayPTPassword");
    	JTextField primaryScriptDelayA = (JTextField)components.get("primaryScriptDelayA");
    	JTextField primaryScriptDelayB = (JTextField)components.get("primaryScriptDelayB");
    	
    	if(primaryScriptDelayPhoneDial.getText().trim().equals("")){
    		primaryScriptDelayPhoneDial.setText("1");
    	}
    	if(primaryScriptDelayPTUsername.getText().trim().equals("")){
    		primaryScriptDelayPTUsername.setText("1");
    	}
    	if(primaryScriptDelayPTPassword.getText().trim().equals("")){
    		primaryScriptDelayPTPassword.setText("1");
    	}
    	if(primaryScriptDelayA.getText().trim().equals("")){
    		primaryScriptDelayA.setText("1");
    	}
    	if(primaryScriptDelayB.getText().trim().equals("")){
    		primaryScriptDelayB.setText("1");
    	}
    }
    
    /**
     * Invoked when the Save button is pressed.
     */
    public void save() {
        boolean saveDecision = true;
    	
    	int option = transaction.getCurrentOption();
    	ArrayList<JComponent> validationFields = new ArrayList<JComponent>();
    	// Field level validations happen here
    	if(option == SimplePMTransaction.DIALUP_SETTING_OPTIONS){
            String cString = ((JComboBox)components.get("posHostConnection")).getSelectedItem().toString();
            if (UnitProfile.getInstance().get("POS_HOST_CONNECTION", "N").equals(cString))
                MainPanelDelegate.setConnectivityChange(false);
            else
                MainPanelDelegate.setConnectivityChange(true);
                
            if(((JComboBox)components.get("posHostConnection")).getSelectedItem().equals("D"))
    			validationFields.add((JTextField) components.get("hostPrimaryPhone"));
	        saveDecision = validateFieldItems(validationFields);
	        setDefaultsValues();
	        
	       if (!((JCheckBox)components.get("passThroughLogon")).isSelected()){
				UnitProfile.getInstance().set("PRIMARY_DIALUP_SCRIPT_NAME",
						"", "S", true);
	       } 

           if (transaction.getModemChanged()){
               // hangup the phone line. because
               MessageFacade.getInstance().hangupModem();
           }
    	} else if (option == SimplePMTransaction.MANAGER_OPTIONS){
		} else if (option == SimplePMTransaction.GENERAL_OPTIONS){
				transaction.userChange("SA_LIMIT", saLimitField.getText().trim());		
		} else if (option == SimplePMTransaction.DISPENSER_OPTIONS){
			transaction.setCommPort(commPortList.getSelectedItem().toString());

			

		} else if(option == SimplePMTransaction.USER_OPTIONS){

			if("6CHAR".equalsIgnoreCase(UnitProfile.getInstance().get("USER_PIN_TYPE","3DIGIT"))){
				saveDecision = false;
				final String pswd = ((JTextField)components.get("passwordField")).getText().trim();
				if(!transaction.isPasswordInUse(pswd)){
					if (transaction.isValid8CharacterPassword(currentPassword, pswd)){ 
						Thread worker = new Thread(){
							@Override
							public void run(){
								transaction.validatePin(transaction.getUserID(), pswd); 
								try{
									Thread.sleep(1000);
								}
								catch(InterruptedException ex){}
								SwingUtilities.invokeLater(new Runnable(){
									@Override
									public void run(){

										if(transaction.getValidatePinResponse()!=null ){
											if(transaction.getValidatePinResponse().getPayload().getValue().isStatus()){
												Calendar today = Calendar.getInstance();
												today.setTime(new Date());
												today.set(Calendar.HOUR, 0);
												today.set(Calendar.MINUTE, 0);
												today.set(Calendar.SECOND, 0);
												transaction.userChange("PSWD_CHANGE_DATE", String.valueOf(today.getTimeInMillis()));
												saveChanges(true,pswd);
												setSaveDecision(false);

											}
											else{

												if(transaction.getValidatePinResponse().getPayload().getValue().getReasonCode().equals(DwPasswordReasonCodeType.EMP_NOT_EXIST)){
													Dialogs.showWarning("dialogs/DialogEmpDoesNotExist.xml");	

												}  
												else
													Dialogs.showWarning("dialogs/DialogInvalid8CharPassword.xml");	
												((JTextField) components.get("passwordField")).setText("");
												((JTextField) components.get("passwordField")).requestFocus();
												setSaveDecision(false);

											}
										}
									}
								});
							}

						};
						worker.start();

					} 
					else {

						Dialogs.showWarning("dialogs/DialogInvalid8CharPassword.xml");	
						((JTextField) components.get("passwordField")).setText("");
						((JTextField) components.get("passwordField")).requestFocus();
						saveDecision = false;
					}
				}
				else{
					Dialogs.showWarning("dialogs/DialogPasswordInUse.xml");	
					((JTextField) components.get("passwordField")).setText("");
					((JTextField) components.get("passwordField")).requestFocus();
					saveDecision = false;  
				}
			}



		} else if(option == SimplePMTransaction.PRINTER_OPTIONS){
			if (transaction.getPrinterChanged()) {
				UnitProfile.getInstance().set("USE_AGENT_PRINTER",
						DWValues.getAgentPrinterListKey((String)agentPrinterList.getSelectedItem()),
						"S", true);
				String sKeyValue = DWValues.getAgentReceiptPaperListKey((String)receiptPaperList.getSelectedItem());
				/*
				 * Need to update userChange in order to override the 'value' 
				 * (instead of key) the profile panel listener stored.  
				 */
	            transaction.userChange(DWValues.PROFILE_PRINTER_PAPER_SIZE, 
	            		sKeyValue);

				sKeyValue = DWValues.getAgentReceiptTypeListKey((String)receiptTypeList.getSelectedItem());
				/*
				 * Need to update userChange in order to override the 'value' 
				 * (instead of key) the profile panel listener stored.  
				 */
	            transaction.userChange(DWValues.PROFILE_PRINTER_RCPT_TYPE, 
	            		sKeyValue);
			} 
		}


	    if(saveDecision){
	        boolean saved = transaction.commit();
	        //Debug.println("save() saved = " + saved);
	        if (saved) {	
	           Iterator<EmpData> itr = empList.iterator();
	           while(itr.hasNext()){
	               EmpData data = itr.next();
	               updateProfile(data.getPassword(),data.getUserID());
	           }
	           empList.clear();
	            SimplePMTransaction.setSaProfileItemFlag(false);
	            exit(PageExitListener.COMPLETED);
	        }
	    }
	    
    }
    
    /**
     * Invoked when the Cancel button is pressed.
     */
    @Override
	public void cancel() {
    	AuditLog.writeBackoutRecord(transaction.getUserID());
        super.cancel();
    }
    
   public void saveChanges(boolean decision,String passwd){
       if(decision){
	        boolean saved = transaction.commit();
	       
	        //Debug.println("save() saved = " + saved);
	        if (saved) {
	            updateProfile(passwd,transaction.getUserID());
	            SimplePMTransaction.setSaProfileItemFlag(false);
	            exit(PageExitListener.COMPLETED);
	        }
	    }
   }
       private void updateProfile(String newPasswordField,int userId){
           Profile userProfile;
//         Store the new users password
          
   			userProfile = (Profile) UnitProfile.getInstance().getProfileInstance().find("USER",userId );	
            userProfile.get("PASSWORD").setValue(newPasswordField);	
   //
            // Set the password date changed profile to current date
      	    Calendar today = Calendar.getInstance();
    	    today.setTime(new Date());
    	    today.set(Calendar.HOUR, 0);
    	    today.set(Calendar.MINUTE, 0);
    	    today.set(Calendar.SECOND, 0);
    	    try{
                
    	    	userProfile.find("PSWD_CHANGE_DATE").setValue(String.valueOf(today.getTimeInMillis()));
            }
            catch(NoSuchElementException e){
                Debug.println("No element :PSWD_CHANGE_DATE when querying ");
                ProfileItem pi = new ProfileItem("ITEM", "PSWD_CHANGE_DATE", "P", String.valueOf(today.getTimeInMillis()), null);
    	            userProfile.insertOrUpdate("PSWD_CHANGE_DATE", pi);    	          
            }
    	  	setProfileItem(userProfile);
    	  	AuditLog.writeProfileChangeRecord(userProfile.getID(), "PASSWORD", userProfile.get("USER_ID").stringValue(), "N/A");                     
          
           

          
       }
       private void setProfileItem(Profile userProfile){
           
           try{
               
               if(userProfile.get("USER_ID_LOCKED")!=null)
               userProfile.get("USER_ID_LOCKED").setValue("N");
               }
               catch(NoSuchElementException e){
                   
               }
           try{
               
                userProfile.find("GRACE_TIME").setValue("0");
           }
           catch(NoSuchElementException e){
               Debug.println("No element :GRACE_TIME when querying ");
               ProfileItem pi = new ProfileItem("ITEM", "GRACE_TIME", "P", "0", null);
   	            userProfile.insertOrUpdate("GRACE_TIME", pi);
   	          
           }
           try{
               userProfile.find("PIN_EXPIRED").setValue("N");                  
           }
           catch(NoSuchElementException e){
               Debug.println("No element :PIN_EXPIRED not found when seeting expiry to N");
               ProfileItem pi = new ProfileItem("ITEM", "PIN_EXPIRED", "P", "N", null);
   	       userProfile.insertOrUpdate("PIN_EXPIRED", pi);
           }
           try{
              userProfile.find("GRACE_LOGIN_USED").setValue("N");
           }
           catch(NoSuchElementException e){
               Debug.println("No element :GRACE_LOGIN_USED not found when seeting expiry to N");
               ProfileItem pi = new ProfileItem("ITEM", "GRACE_LOGIN_USED","P" , "N", null);
               userProfile.insertOrUpdate("GRACE_LOGIN_USED", pi); 
           }
       }
    public void setSaveDecision(boolean decision){
        this.saveDecision = decision;
        
    }
   
 
    /** Invoked when the Add User button is pressed. */
    public void addUser() {
        // ask the transaction to allocate an unused user id,
        // add a new UserElement to the list, and select it.
        addUserOrManager(false, transaction.createUser(false));
    }

    /** Invoked when the Add Manager button is pressed. */
    public void addManager() {
        addUserOrManager(true, transaction.createUser(true));
    }

    
    /** Create a new UserElement, add it to the list, and select it. */
    public void addUserOrManager(boolean manager, int id) {
    	newUserOrManager=true;
        if (id == -1) {
            if (manager) {
                Dialogs.showError("dialogs/BlankDialog.xml",	
                        Messages.getString("SimplePMTransactionInterface.2034")); 
            }
            else {
                Dialogs.showError("dialogs/BlankDialog.xml",	
                		Messages.getString("SimplePMTransactionInterface.2035")); 
            }
            
            return;
           
        }

        //String name = (manager ? "MGR" : "EMP") + id;	 
        String name = transaction.defaultName(manager, id);
        UserElement elem = new UserElement(name, id);
        DefaultListModel model = (DefaultListModel) userList.getModel();
        model.addElement(elem);
        // new rows are added to the bottom
        userList.ensureIndexIsVisible(model.getSize() - 1);
        userList.setSelectedIndex(model.getSize() - 1);
    }
    
    /** 
     * Invoked when the Delete button is pressed. Informs the
     * transaction that the selected user is to be deleted, then
     * removes the user element from the user list. Note that
     * removing the selected element from the list changes the 
     * selection and will result in a call to setSelectedUser.
     */
    public void deleteUser() {
        Object selection = userList.getSelectedValue();
        if (selection == null) {
            Debug.println("SPMTI.deleteUser() selection == null");	
        }
        else {
            transaction.deleteSelectedUser();
            DefaultListModel model = (DefaultListModel) userList.getModel();
            model.removeElement(selection);
        }
    }
    
    /** Populates the list of users from profile data. */
    private void initializeUserList(JList list) {
        DefaultListModel model = (DefaultListModel) list.getModel();
        ArrayList<UserElement> users = new ArrayList<UserElement>();
        
        // loop over the top-level profile items
        Iterator<ProfileItem> i = UnitProfile.getInstance().getProfileInstance().iterator();
        while (i.hasNext()) {
            ProfileItem item = i.next();
            //item.getStatus()
            if (item.matches("USER")&& item.getStatus().equalsIgnoreCase("A")) {	
                // save user profiles in the list
                if((item.getID()>40 && transaction.isMgrPinPrivChangeAllowed()) ||
                   (item.getID()<=40 && transaction.isEmpPinPrivChangeAllowed())){
                    users.add(new UserElement(item.get("NAME").stringValue(), item.getID()));   
                }
            }
        }

        // sort the user list by id
        Collections.sort(users);

        // Fill the JList model
        Iterator<UserElement> j = users.iterator();
        while (j.hasNext()) {
            model.addElement(j.next());
        }        
    }
    
    /** Implementation of ListSelectionListener for the user list. */
    @Override
	public void valueChanged(ListSelectionEvent e) {
        if (! e.getValueIsAdjusting()) {
            setSelectedUser((UserElement) userList.getSelectedValue());
        }
    }
    
    private void setupTable(final XmlDefinedPanel panel, int userId) {

		String[] headers = {Messages.getString("SimplePMTransactionInterface.currency"),
				Messages.getString("SimplePMTransactionInterface.limit")}; 
        currencyList = UnitProfile.getInstance().getProfileInstance().findList(
                "AGENT_CURRENCY");
    	int rows = currencyList.size(); 
        
        Profile userProfile;
        try {
       		userProfile = (Profile) UnitProfile.getInstance().
				getProfileInstance().find("USER",userId);	
        }
        catch (NoSuchElementException e){
        	userProfile = null;
        }
   			
		tableModel = new DefaultTableModel(headers, 0) {
			private static final long serialVersionUID = 1L;

			/*
			 * (non-Javadoc)  
			 * @see javax.swing.table.DefaultTableModel#isCellEditable(int, int)
			 * Only allow limit column (second column) to be edited
			 */
			@Override
			public boolean isCellEditable(int row, int column) {
				return (column == 1);
			}
			/*
			 * (non-Javadoc)
			 * @see javax.swing.table.DefaultTableModel#setValueAt(java.lang.Object, int, int)
			 * Only allow changes to limit, if the result is a valid bigdecimal
			 */
	        @Override
			public void setValueAt(Object value, int row, int col) {
	        	try {
	        		if (isCellEditable(row, col)) {
	        			MoneyField moneyValue = new MoneyField((String)value);
	        			value = moneyValue.getText();
			            super.setValueAt(value, row, col);
			            transaction.userChange(DWValues.E_MAX_SEND_AMOUNT
			            		+ "/" + table.getValueAt(row, 0),(String)value);
	        		}
	        	}
	        	catch (Exception nfe) {
	        	}
	        }
		};
		table = new JTable(tableModel);
		table.setPreferredScrollableViewportSize(new java.awt.Dimension(500,
				500));
		table.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
		TableColumnModel model = table.getColumnModel();
		TableColumn col = null;
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		col = model.getColumn(CURRENCY_COLUMN);
		col.setMinWidth(30);
		col.setPreferredWidth(60);

		col = model.getColumn(EMP_MAX_AMT_COLUMN);
		col.setMinWidth(30);
		col.setPreferredWidth(110);
		
        int rowCount = tableModel.getRowCount();
        for (int i = 0; i < rowCount; i++){
            tableModel.removeRow(0);
        }

		for (int ii = 0; ii < rows; ii++) {
			String currency = currencyList.get(ii);
			String value;
			if (userProfile == null) {
				value ="0.00";
			}
			else {
				try {
					value = userProfile.findCurrencyValue(DWValues.E_MAX_SEND_AMOUNT, 
							currency).stringValue(); 
				}
				/*
				 * No item for this currency, so add one (assume agent currency
				 * list is correct) with a value of 0.00, so it can be updated.
				 */
				catch (NoSuchElementException e) {
					value = "0.00";
	                ProfileItem pi = new ProfileItem("ITEM", DWValues.E_MAX_SEND_AMOUNT,
	                		"P", value, currency);
    	            userProfile.add(pi);    	          
				}
			}
			MoneyField moneyValue = new MoneyField(value);
			String[] row = { currency, moneyValue.getText() };
			tableModel.addRow(row);
		}
    	table.setColumnSelectionAllowed(false);
    	table.setRowSelectionAllowed(false);
        /*
         * This is important, otherwise setValueAt() is not always called
         */
        table.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE); 

		panel.addComponent(table, "customerTable", "EmpLimitScrollpane");  
		
		table.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {
					@Override
					public void valueChanged(ListSelectionEvent e) {
						int col = table.getSelectedColumn();
						int row = table.getSelectedRow();
						if (col == 0) {
			                table.changeSelection(row, 1, false, false);
			                table.requestFocus();
						}
					}
				});

		table.addFocusListener(new FocusListener(){
			@Override
			public void focusGained(FocusEvent evt)
			{
                table.changeSelection(0, 1, false, false);
                table.editCellAt(0, 1);
			}

			@Override
			public void focusLost(FocusEvent e) {
				// Nothing to do here
			}
		});
 
        panel.addKeyListener("customerTable", new KeyAdapter() {	
            @Override
			public void keyPressed(KeyEvent e) {
            	int keyCode = e.getKeyCode(); 
            	switch (keyCode) {
            	case KeyEvent.VK_ENTER:
            	case KeyEvent.VK_TAB:
            		int modifiers = e.getModifiers();
                    e.consume();
                    JTable table = (JTable)e.getSource();
                    int rowCount = table.getRowCount();
                    int row = table.getSelectedRow();
     
                    if (((modifiers & 1) == 1) && (keyCode == KeyEvent.VK_TAB)) {
                        row--;
                        if (row < 0) {
                    		int idx = 0;
                    	    JCheckBox previousItem;
                    		do {
                    			idx++;
                    			previousItem = (JCheckBox)panel.getComponent(
                    					"PreSendAmtCheckBox"+Integer.toString(idx));
                    		} while((previousItem != null && !previousItem.isShowing()) && idx < 8);
                    		if(previousItem!=null){
                    			previousItem.requestFocus();
                    		}
                           return;
                        }
                    }
                    else {
                        row++;
                        if (row >= rowCount) {
                    		int idx = 0;
                    	    JCheckBox nextItem;
                    		do {
                    			idx++;
                    			nextItem = (JCheckBox)panel.getComponent(
                    					"PostSendAmtCheckBox"+Integer.toString(idx));
                    		} while((nextItem != null && !nextItem.isShowing()) && idx < 9);
                    		if(nextItem!=null){
                    			nextItem.requestFocus();
                    		}
                        	return;
                        }
                    }
                    table.changeSelection(row, 1, false, false);
                    table.editCellAt(row, 1);
                    break;
            	case KeyEvent.VK_ESCAPE:e.consume();
                    cancel();
                    break;
            	}
            }});
        
 		table.setEnabled(true);
	}

    /** Called when a user is clicked in the user list. */
    private synchronized void setSelectedUser(UserElement elem) {
        
        JPanel centerPanel = (JPanel) components.get("centerPanel");	
        centerPanel.removeAll();
        

        
        if (elem == null) {
            // this means they deselected, so there's currently no selection.
            transaction.setSelectedUser(-1);
            centerPanel.validate();
            centerPanel.repaint();
            buttons.setEnabled("delete", false);	
            return;
        }
        
        // inform the transaction logic before creating any user profile panels,
        // because they will want to query it
        
        transaction.setSelectedUser(elem.id);  
        if (elem.panel == null) 
        elem.panel = new XmlDefinedPanel("simplepm/ManagerProfileOptions2.xml");
        tempPasswdField = (JTextField) elem.panel.getComponent("passwordField2");
        tempPasswdField.setText(((JTextField) elem.panel.getComponent("passwordField1")).getText());
        elem.panel.getComponent("unmaskedPanel").setVisible(false);
        elem.panel.getComponent("maskedPanel").setVisible(false); 
        if (elem.panel != null) {
    		setupTable(elem.panel, elem.id);
            if(!transaction.isSixCharPinType()){
                elem.panel.getComponent("allowResetPinCheckBox").setVisible(false);
               
             }
        	if("Y".equals(UnitProfile.getInstance().get("MASK_USER_PIN","Y")))
        	{
        		if(!newUserOrManager && !transaction.isMgrResetPINAllowed())
               	{
        		    elem.panel.getComponent("maskedPanel").setVisible(true); 
        		    pf = (JTextField) elem.panel.getComponent("passwordField2");
        		    if(transaction.isSixCharPinType()){
        		    pf.setEnabled(false);
        	    	pf.setBackground(SystemColor.control); 
        		    }
        		    else {
        		        pf.setEnabled(true);
        		        pf.setEditable(true);
        		    }
        	    	resetPIN2 = (JButton) elem.panel.getComponent("resetPINButton2"); 
        	    	resetPIN2.setEnabled(false);
        	    	maskedVisible = true;
        	    	unmaskedVisible = false;
        		   
               		//elem.panel = new XmlDefinedPanel("simplepm/ManagerProfileOptions2.xml");
               	}
        		else if(!newUserOrManager && transaction.isMgrResetPINAllowed())
        		  {
        		    elem.panel.getComponent("maskedPanel").setVisible(true);
        		    //elem.panel.getComponent("unmaskedPanel").setVisible(true);
        		    pf = (JTextField) elem.panel.getComponent("passwordField2");
        		    resetPIN2 = (JButton) elem.panel.getComponent("resetPINButton2"); 
//        		  commented code for requirement fix #1249      
//        		    if(transaction.isUserLocked(elem.id))
//        	    	resetPIN2.setEnabled(true);
//        		    else
//        		        resetPIN2.setEnabled(false);
        		    pf.setEnabled(false);
        	    	pf.setBackground(SystemColor.control); 
        	    	maskedVisible = true;
        	    	unmaskedVisible = false;
        		}
        		 else if(newUserOrManager && !transaction.isMgrResetPINAllowed()){
        		     elem.panel.getComponent("unmaskedPanel").setVisible(true);
        		     resetPIN1 = (JButton) elem.panel.getComponent("resetPINButton1"); 
         	    	resetPIN1.setEnabled(false);
         		    pf = (JTextField) elem.panel.getComponent("passwordField1");    
         		   maskedVisible = false;
       	    	   unmaskedVisible = true;
        		 }
        		 else if(newUserOrManager && transaction.isMgrResetPINAllowed()){
        		     elem.panel.getComponent("unmaskedPanel").setVisible(true);
        		     resetPIN1 = (JButton) elem.panel.getComponent("resetPINButton1"); 
          	    	resetPIN1.setEnabled(false);
         		    pf = (JTextField) elem.panel.getComponent("passwordField1");   	
         		   if(transaction.isSixCharPinType()){
           		    pf.setEnabled(false);
           	    	pf.setBackground(SystemColor.control); 
           		    }
           		    else {
           		        pf.setEnabled(true);
           		        pf.setEditable(true);
           		    }
         		   maskedVisible = false;
       	    	   unmaskedVisible = true;
        		 }       		        		
        	}
        	else{
    		    elem.panel.getComponent("unmaskedPanel").setVisible(true); 
    		    unmaskedVisible = true;
    		    resetPIN1 = (JButton) elem.panel.getComponent("resetPINButton1"); 
    		    pf = (JTextField) elem.panel.getComponent("passwordField1");
    		    resetPIN1.setEnabled(false);
//    		    if(newUserOrManager && !transaction.isMgrResetPINAllowed()){
//    		       
//    		        pf.setEnabled(true);  
//    		    }
//    		    if(newUserOrManager && transaction.isMgrResetPINAllowed()){
//    		        pf.setEnabled(false);  
//    		    }
    		    if(transaction.isSixCharPinType()){
           		    pf.setEnabled(false);
           	    	pf.setBackground(SystemColor.control); 
           		    }
           		    else {
           		        pf.setEnabled(true);
           		        pf.setEditable(true);
           		    }
    		    if(!newUserOrManager && transaction.isMgrResetPINAllowed()){
    		        if(transaction.isUserLocked(elem.id))
    		            resetPIN1.setEnabled(true);
            		    else
            		        resetPIN1.setEnabled(false); 
    		    }
        	}
         }        
        if(unmaskedVisible){
        	resetPIN1.addActionListener(new ActionListener() {
        		@Override
				public void actionPerformed(ActionEvent e) {
        			resetPin();
        		}
        	});
        }
        if(maskedVisible){
        	resetPIN2.addActionListener(new ActionListener() {
        		@Override
				public void actionPerformed(ActionEvent e) {
        			resetPin();
        		}
        	});
        }
        userStatusNameLabel = (JLabel) (elem.panel.getComponent("userStatusNameLabel"));
        userStatusLockedValueLabel =(JLabel) (elem.panel.getComponent("userStatusLockedValueLabel"));
        userStatusUnlockedValueLabel= (JLabel) (elem.panel.getComponent("userStatusUnlockedValueLabel"));
        userStatusNameLabel.setVisible(false);
        userStatusLockedValueLabel.setVisible(false);
        userStatusUnlockedValueLabel.setVisible(false);
        if(!newUserOrManager){
        	if(transaction.isSixCharPinType()){
        		if(transaction.isUserLocked(elem.id)){
        			userStatusNameLabel.setVisible(true);
        			userStatusUnlockedValueLabel.setVisible(false); 
        			userStatusLockedValueLabel.setVisible(true);
        		}  
        		else{
        			userStatusNameLabel.setVisible(true);
        			userStatusLockedValueLabel.setVisible(false);
        			userStatusUnlockedValueLabel.setVisible(true); 
        		}
        	}
        }

        newUserOrManager=false;
        // remove "Manager Setup" label from the screen if profile is not a manager.
        if (transaction.isManager())
        	elem.panel.getComponent("MANAGER_SETUP").setVisible(true);  
        else
        	elem.panel.getComponent("MANAGER_SETUP").setVisible(false); 

        JLabel label = (JLabel) (elem.panel.getComponent("userid"));	
        label.setText(elem.id + "");	

        employeeMaxAmountPanel = (JPanel) (elem.panel.getComponent("employeeMaxAmountPanel"));
        employeeMaxAmountPanel.setVisible(!transaction.isManager());
        employeeMaxAmountPanel.setFocusable(!transaction.isManager());

        centerPanel.add(elem.panel);
        centerPanel.validate();
        centerPanel.repaint();
        buttons.setEnabled("delete",transaction.allowDeleteUser());
    }
    
   
   /** Remove event handlers and listeners not taken care of by the superclass. */
    @Override
	public void cleanup() {
        if (userList != null) {
            userList.removeListSelectionListener(this);
            DefaultListModel model = (DefaultListModel) userList.getModel();
            for (int i = 0; i < model.size(); i++) {
                UserElement elem = (UserElement) model.get(i);
                XmlDefinedPanel panel = elem.panel;
                if (panel != null) {
                    panel.cleanup();
                }
            }
        }
        super.cleanup();
    }
    
    public class EmpData{
       int userID;
       String password;
       EmpData(int userID,String password){
           this.userID =userID;
           this.password = password;
           
       }
       public int getUserID(){
           return this.userID;
       }
       public String getPassword(){
           return this.password;
       }
    }
}
    
/*
       Here's how the screen flow framework works:
       
       start(int) and finish(int) are implemented by subclasses of FlowPage
       and are invoked by subclasses of PageFlowContainer. The parameter 
       indicates the direction of the page flow. start(int) is called when
       a page is about to be displayed; typical implementations enable and
       disable controls, add action listeners, and set the tab order. 
       finish(int) is called by PageFlowContainer when the user clicks a page
       flow button. Implementations typically copy data from gui components
       to the transaction, then call PageNotification.notifyExitListeners to
       cause the flow page container to flip to the next page.

       start() is implemented by subclasses of TransactionInterface and is
       invoked by DeltaworksMainPanel when the user selects a transaction.
       Most transaction interfaces use a default implementation inherited 
       from PageFlowContainer which adds action listeners to the flow 
       buttons, shows the first page in a card layout, and calls that pages's
       start(int) method.

       exit(int) is defined by PageExitListener and implemented by subclasses
       of PageFlowContainer. It is invoked by flow pages via a call to 
       PageNotification.notifyExitListeners. A page flow container can also
       call its own exit(int) method, for example if isn't displaying a flow
       page, or in response to an idle timeout. The default implementation of
       exit(int) in PageFlowContainer handles the Back and Next directions, 
       and delegates Cancel, Complete, and Fail to its page exit listener
       (which would be the main panel) after calling cleanup().

       The implementation of cleanup() in XmlDefinedPanel removes event 
       listeners and keymappings, and restored the original focus order.
*/