/**
 * 
 */
package dw.mfaRegistration;

import dw.framework.FlowPage;
import dw.framework.MfaFlowPageInterface;
import dw.framework.PageFlowButtons;

/**
 * @author aep4
 *
 */
public abstract class RegisterMFAFlowPage extends FlowPage implements MfaFlowPageInterface {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected RegisterMfaToken registerMfaToken;
	
	public RegisterMFAFlowPage(RegisterMfaToken registerMfaToken, String xmlFileName, String name, String pageCode, PageFlowButtons buttons) {
		super(xmlFileName, name, pageCode, buttons);
		this.registerMfaToken = registerMfaToken;
	}

	// subclass pages will override this to provide their own return value handling.
	@Override
	public void commComplete(int commTag, Object returnValue) {
	}
}


