/**
 * 
 */
package dw.mfaRegistration;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.HashMap;

import javax.swing.AbstractButton;

import dw.framework.KeyMapManager;
import dw.framework.MfaAuthorization;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.i18n.Messages;
import dw.main.DeltaworksMainMenu;
import dw.main.DeltaworksMainPanel;
import dw.profile.UnitProfile;
import dw.utility.DWValues;

/**
 * @author aep4
 *
 */
public class RegisterMfaWizard1 extends RegisterMFAFlowPage {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private Component bigButton[];
    private int language ;
    private int altLanguage ;
    
    private HashMap<String, Boolean> buttonMap = new HashMap<String, Boolean>();
//    public static final int MONEY_ORDER = 0;
//    public static final int VENDOR_PAYMENT = 1;
//    public static final int MONEY_GRAM_SEND = 2;
//    public static final int MONEY_GRAM_RECEIVE = 3;
//    public static final int BILL_PAYMENT = 4;
//    public static final int TRANSACTION_QUOTE = 5;
//    public static final int DIRECTORY_AGENTS = 6;
//    public static final int DIRECTORY_BILLERS = 7;
//    public static final int LANGUAGE_TOGGLE = 8;
//    public static final int CARD = 9;
//    public static final int ADD_MINUTES = 10;
//    public static final int ACCOUNT_DEPOSIT_PARTNERS = 11;
//    public static final int BROADCAST_MESSAGES = 12;
//    public static final int MONEY_GRAM_SEND_TO_ACCOUNT = 13;
//    public static final int FORM_FREE_ENTRY = 14;

//    static private final int KEY_MAPPINGS[][] = new int[][] {
////		 English        Spanish                French                 German                 Chinese                Polish                   Russian               Portuguese
//  	{KeyEvent.VK_M, 	KeyEvent.VK_G,         KeyEvent.VK_M,         KeyEvent.VK_M,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_Y,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_O}, // Money Orders
//  	{KeyEvent.VK_V,		KeyEvent.VK_V,         KeyEvent.VK_F,         KeyEvent.VK_V,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_V,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_F}, // Vendor Payments
//  	{KeyEvent.VK_S, 	KeyEvent.VK_E,         KeyEvent.VK_E,         KeyEvent.VK_G,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_W,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_E}, // Send
//  	{KeyEvent.VK_R, 	KeyEvent.VK_R,         KeyEvent.VK_R,         KeyEvent.VK_E,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_O,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_R}, // Receive
//  	{KeyEvent.VK_P, 	KeyEvent.VK_P,         KeyEvent.VK_P,         KeyEvent.VK_R,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_Z,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_P}, // Bill Payments
//  	{KeyEvent.VK_Q, 	KeyEvent.VK_C,         KeyEvent.VK_D,         KeyEvent.VK_T,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_T,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_N}, // Transaction Quotes
//  	{KeyEvent.VK_A, 	KeyEvent.VK_A,         KeyEvent.VK_A,         KeyEvent.VK_A,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_A,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_G}, // Directory of Agents
//  	{KeyEvent.VK_B, 	KeyEvent.VK_F,         KeyEvent.VK_N,         KeyEvent.VK_B,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_P,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_B}, // Directory of Billers
//  	{KeyEvent.VK_S, 	KeyEvent.VK_C,         KeyEvent.VK_C,         KeyEvent.VK_U,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_S}, // Language Toggle
//  	{KeyEvent.VK_D, 	KeyEvent.VK_T,         KeyEvent.VK_T,         KeyEvent.VK_K,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_K,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_C}, // Add to Card
//  	{KeyEvent.VK_N, 	KeyEvent.VK_M,         KeyEvent.VK_J,         KeyEvent.VK_N,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_M,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_A}, // Add Minutes
//  	{KeyEvent.VK_K, 	KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_D}, // Account Deposit Banks
//  	{KeyEvent.VK_E, 	KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_M}, // Broadcast Messages
//  	{KeyEvent.VK_O, 	KeyEvent.VK_N, 		   KeyEvent.VK_V		, KeyEvent.VK_S, 		 KeyEvent.VK_UNDEFINED, KeyEvent.VK_J,         KeyEvent.VK_UNDEFINED, KeyEvent.VK_I},  // Send Money To Account
//	{KeyEvent.VK_T, 	KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_UNDEFINED, KeyEvent.VK_T}	// Free Form Entry
//  };

	public RegisterMfaWizard1(RegisterMfaToken transaction, String name, String pageCode, PageFlowButtons buttons) {
		super(transaction, "main/DeltaworksMainMenu.xml", name, pageCode, buttons);
        bigButton = new Component[NUMBER_BUTTONS];

        bigButton[DeltaworksMainMenu.MONEY_ORDER] = getComponent("moneyOrderButton");	
        bigButton[DeltaworksMainMenu.VENDOR_PAYMENT] = getComponent("vendorPaymentButton");	
        bigButton[DeltaworksMainMenu.MONEY_GRAM_SEND] = getComponent("sendMoneyButton");	
        bigButton[DeltaworksMainMenu.MONEY_GRAM_RECEIVE] = getComponent("receiveMoneyButton");	
        bigButton[DeltaworksMainMenu.BILL_PAYMENT] = getComponent("billPaymentButton");	
        bigButton[DeltaworksMainMenu.TRANSACTION_QUOTE] = getComponent("hotQuote");	
        bigButton[DeltaworksMainMenu.DIRECTORY_AGENTS] = getComponent("hotAgents");	
        bigButton[DeltaworksMainMenu.DIRECTORY_BILLERS] = getComponent("hotBillers");	
        bigButton[DeltaworksMainMenu.LANGUAGE_TOGGLE] = getComponent("languageToggle");      
        bigButton[DeltaworksMainMenu.CARD] = getComponent("cardButton");      
        bigButton[DeltaworksMainMenu.ADD_MINUTES] = getComponent("addMinutesButton");	
        bigButton[DeltaworksMainMenu.ACCOUNT_DEPOSIT_PARTNERS] = getComponent("hotAccountDepositPartners");      
        bigButton[DeltaworksMainMenu.BROADCAST_MESSAGES] = getComponent("hotBroadcastMessages");	
        bigButton[DeltaworksMainMenu.MONEY_GRAM_SEND_TO_ACCOUNT] = getComponent("sendMoneyToAccountButton");	
        bigButton[DeltaworksMainMenu.FORM_FREE_ENTRY] = getComponent("formFreeEntryButton");	
        setButtonColor();

        flowButtons.disable();
        flowButtons.setVisible("cancel", false);
	}

    private int getLanguage(String language) {
    	if (language.equals("en")) {
    		return ENGLISH;
    	} else if (language.equals("es")) {
    		return SPANISH;
    	} else if (language.equals("fr")) {
    		return FRENCH;
    	} else if (language.equals("de")) {
    		return GERMAN;
    	} else if (language.equals("cn")) {
    		return CHINESE;
    	} else if (language.equals("pl")) {;
			return POLISH;
		} else if (language.equals("ru")) {
			return RUSSIAN;
		} else if (language.equals("pt")) {
			return PORTUGUESE;
		} else {
			return ENGLISH;
		}
    }
    
    public void setButtonColor() {
    	language = getLanguage(UnitProfile.getInstance().get("LANGUAGE", "en"));
    	
        bigButton[DeltaworksMainMenu.MONEY_ORDER].setBackground(Color.WHITE);
        bigButton[DeltaworksMainMenu.VENDOR_PAYMENT].setBackground(Color.WHITE);
        bigButton[DeltaworksMainMenu.MONEY_GRAM_SEND].setBackground(Color.WHITE);
        bigButton[DeltaworksMainMenu.MONEY_GRAM_RECEIVE].setBackground(Color.WHITE);
        bigButton[DeltaworksMainMenu.BILL_PAYMENT].setBackground(Color.WHITE);
        bigButton[DeltaworksMainMenu.CARD].setBackground(Color.WHITE);
        bigButton[DeltaworksMainMenu.ADD_MINUTES].setBackground(Color.WHITE);
        bigButton[DeltaworksMainMenu.MONEY_GRAM_SEND_TO_ACCOUNT].setBackground(Color.WHITE);
        bigButton[DeltaworksMainMenu.FORM_FREE_ENTRY].setBackground(Color.WHITE);
    }

	@Override
	public void commComplete(int commTag, Object returnValue) {
		PageNotification.notifyExitListeners(this, PageExitListener.NEXT);
	}

	@Override
	public void commit() {
        PageNotification.notifyExitListeners(this, PageExitListener.COMPLETED);
	}

	@Override
	public void deauthorize() {
		deauthorize(registerMfaToken);		
		resetRetryCount();		
	}

	@Override
	public void abort() {
	}

	@Override
	public void cancel() {
        PageNotification.notifyExitListeners(this, PageExitListener.CANCELED);
	}

    /**
     * This method resets the button when it is reauthed 
     * @param transaction
     */
    
	private static final int ENGLISH = 0;
	private static final int SPANISH = 1;
	private static final int FRENCH = 2;
	private static final int GERMAN = 3;
	private static final int CHINESE = 4;
	private static final int POLISH = 5;
	private static final int RUSSIAN = 6;
	private static final int PORTUGUESE = 7;
	
    public void resetMapping(int buttonIndex) {
    	Component buttonComponent = bigButton[buttonIndex];
   		int key = (buttonIndex != DeltaworksMainMenu.LANGUAGE_TOGGLE) ? DeltaworksMainMenu.KEY_MAPPINGS[buttonIndex][language] : DeltaworksMainMenu.KEY_MAPPINGS[buttonIndex][altLanguage];
		if (key != KeyEvent.VK_UNDEFINED) {
			KeyMapManager.mapComponent(this, key, InputEvent.ALT_MASK, buttonComponent);
		}
       	if (buttonIndex == DeltaworksMainMenu.LANGUAGE_TOGGLE) {
        	switch (altLanguage) {
	    		case ENGLISH: ((AbstractButton)buttonComponent).setText(Messages.getString("DeltaworksMainMenu.english")); break;
	    		case SPANISH: ((AbstractButton)buttonComponent).setText(Messages.getString("DeltaworksMainMenu.spanish")); break;
	    		case FRENCH:  ((AbstractButton)buttonComponent).setText(Messages.getString("DeltaworksMainMenu.french"));  break;
	    		case GERMAN:  ((AbstractButton)buttonComponent).setText(Messages.getString("DeltaworksMainMenu.german"));  break;
	    		case CHINESE: ((AbstractButton)buttonComponent).setText(Messages.getString("DeltaworksMainMenu.chinese")); break;
	    		case POLISH:  ((AbstractButton)buttonComponent).setText(Messages.getString("DeltaworksMainMenu.polish"));  break;
	    		case RUSSIAN: ((AbstractButton)buttonComponent).setText(Messages.getString("DeltaworksMainMenu.russian")); break;
	    		case PORTUGUESE: ((AbstractButton)buttonComponent).setText(Messages.getString("DeltaworksMainMenu.portuguese")); break;
        	}
    	}
    }
 
    public static final int NUMBER_BUTTONS = 15;
    
	@Override
	public void start(int direction) {
		for (int button = 0; button < NUMBER_BUTTONS; button++) {
			resetMapping(button);
		}		
        removeMappingForInvisibleButtons();
        setTransactionEnabled();
    	MfaAuthorization ma = new MfaAuthorization();
    	ma.doMfaAuthorization(this, true, true);   
   	}

	@Override
	 public void finish(int direction) {
	        PageNotification.notifyExitListeners(this, direction);
	 }

    private void removeMappingForInvisibleButtons(){
    	for (int i = 0; i < NUMBER_BUTTONS; i++) {
    	  	if (i == DeltaworksMainMenu.LANGUAGE_TOGGLE) {
    	  		continue;
    	  	}
    	  	if (!bigButton[i].isVisible()) {           
    	  		KeyMapManager.unregisterComponent( bigButton[i]);    	           
    	        buttonMap.put(bigButton[i].getName(), Boolean.FALSE);
    	  	} else {
    	  		if (buttonMap.containsKey(bigButton[i].getName())) {   
    	  			resetMapping(i);
    	  			buttonMap.remove(bigButton[i].getName());    	      
    	  		}
    	  	}
    	}
    }
    
	private void setTransactionEnabled() {
        //Debug.dumpStack("setTransactionEnabled(" + enabled + ") called from");
		DeltaworksMainPanel deltaworksMainPanel = DeltaworksMainPanel.getMainPanel();
	    boolean isSuperAgent = UnitProfile.getInstance().get("SUPERAGENT","N").equals("Y");
	    // set authorization flags for each transaction
	    boolean moAuth = deltaworksMainPanel.getTranAuth(DWValues.MONEY_ORDER_DOC_SEQ);
	    boolean vpAuth = deltaworksMainPanel.getTranAuth(DWValues.VENDOR_PAYMENT_DOC_SEQ);
	    boolean mgsAuth = deltaworksMainPanel.getTranAuth(DWValues.MONEY_GRAM_SEND_DOC_SEQ);
	    boolean mgsffAuth = deltaworksMainPanel.getFormFreeAuth(DWValues.MONEY_GRAM_SEND_DOC_SEQ);
	    // boolean mgsToAccAuth = getTranAuth(DWValues.MONEY_GRAM_SEND_DOC_SEQ);
	    boolean mgrAuth = deltaworksMainPanel.getTranAuth(DWValues.MONEY_GRAM_RECEIVE_DOC_SEQ);
	    boolean bpAuth = deltaworksMainPanel.getTranAuth(DWValues.BILL_PAYMENT_DOC_SEQ);
	    boolean bpffAuth = deltaworksMainPanel.getFormFreeAuth(DWValues.BILL_PAYMENT_DOC_SEQ);
	    if (isSuperAgent && UnitProfile.getInstance().isDemoMode()){
	        moAuth = false;
	        vpAuth = false;
	        mgsAuth = false;
	        mgrAuth = false;
	        bpAuth = false;
	    }
	
	    bigButton[DeltaworksMainMenu.MONEY_ORDER].setVisible(moAuth);
	    bigButton[DeltaworksMainMenu.MONEY_ORDER].setEnabled(moAuth);
	
	    bigButton[DeltaworksMainMenu.MONEY_GRAM_RECEIVE].setVisible(mgrAuth);
	    bigButton[DeltaworksMainMenu.MONEY_GRAM_RECEIVE].setEnabled(mgrAuth);
	
	    bigButton[DeltaworksMainMenu.MONEY_GRAM_SEND].setVisible(mgsAuth);
	    bigButton[DeltaworksMainMenu.MONEY_GRAM_SEND].setEnabled(mgsAuth);
	
	    bigButton[DeltaworksMainMenu.MONEY_GRAM_SEND_TO_ACCOUNT].setVisible(mgsAuth);
	    bigButton[DeltaworksMainMenu.MONEY_GRAM_SEND_TO_ACCOUNT].setEnabled(mgsAuth);
	
	    bigButton[DeltaworksMainMenu.TRANSACTION_QUOTE].setVisible(mgsAuth);
	    bigButton[DeltaworksMainMenu.TRANSACTION_QUOTE].setEnabled(mgsAuth);
	
	    bigButton[DeltaworksMainMenu.ACCOUNT_DEPOSIT_PARTNERS].setVisible(mgsAuth);
	    bigButton[DeltaworksMainMenu.ACCOUNT_DEPOSIT_PARTNERS].setEnabled(mgsAuth);
	
		boolean flag = (UnitProfile.getInstance().getBroadcastMessageUpdateInterval() > 0);
	    bigButton[DeltaworksMainMenu.BROADCAST_MESSAGES].setVisible(flag);
	    bigButton[DeltaworksMainMenu.BROADCAST_MESSAGES].setEnabled(flag);
	
	    bigButton[DeltaworksMainMenu.VENDOR_PAYMENT].setVisible(vpAuth);
	    bigButton[DeltaworksMainMenu.VENDOR_PAYMENT].setEnabled(vpAuth);
	
	    bigButton[DeltaworksMainMenu.CARD].setVisible(bpAuth);
	    bigButton[DeltaworksMainMenu.CARD].setEnabled(bpAuth);
	
	    bigButton[DeltaworksMainMenu.BILL_PAYMENT].setVisible(bpAuth);
	    bigButton[DeltaworksMainMenu.BILL_PAYMENT].setEnabled(bpAuth);
	
	    bigButton[DeltaworksMainMenu.DIRECTORY_BILLERS].setVisible(bpAuth);
	    bigButton[DeltaworksMainMenu.DIRECTORY_BILLERS].setEnabled(bpAuth);
	
	    // Language toggle button should not be visible in multi-agent mode.
	    bigButton[DeltaworksMainMenu.LANGUAGE_TOGGLE].setVisible(!UnitProfile.getInstance().isMultiAgentMode());
	    
	    bigButton[DeltaworksMainMenu.DIRECTORY_AGENTS].setVisible(!isSuperAgent);
	    bigButton[DeltaworksMainMenu.DIRECTORY_AGENTS].setEnabled(!isSuperAgent);
	
	    bigButton[DeltaworksMainMenu.ADD_MINUTES].setVisible(bpAuth);
	    bigButton[DeltaworksMainMenu.ADD_MINUTES].setEnabled(bpAuth);
	
	    boolean ff = (mgsAuth && mgsffAuth) || (bpAuth && bpffAuth);
	    bigButton[DeltaworksMainMenu.FORM_FREE_ENTRY].setVisible(ff);
	    bigButton[DeltaworksMainMenu.FORM_FREE_ENTRY].setEnabled(ff);
	}   
}
