/**
 * 
 */
package dw.mfaRegistration;

import dw.framework.PageExitListener;
import dw.framework.PageNameInterface;
import dw.mgsend.MoneyGramClientTransaction;

/**
 * @author aep4
 *
 */
public class RegisterMFAWizard extends RegisterMfaTransactionInterface {
	
	private static final long serialVersionUID = 1L;
	
	public RegisterMFAWizard(RegisterMfaToken registerMfaToken, MoneyGramClientTransaction mgrTransaction, PageNameInterface naming) {
	   super("mfa/MFAWizardPanel.xml", registerMfaToken, naming, true); 	     
	   createPages();
    }	

	@Override
	protected void createPages() {
		addPage(RegisterMfaWizard1.class, registerMfaToken, "registerMfaToken", "RMFA01", buttons);				
	}	
	
	@Override
	public void start() {
		 registerMfaToken.clear();
	     super.start();	  
	} 
	
	 /**
     * Page traversal logic here.
     * 
     * */
	@Override
	 public void exit(int direction) {
        if (direction == PageExitListener.DONOTHING){
	            return;
        }
        super.exit(direction);
    }
}

