package dw.mfaRegistration;

import dw.framework.ClientTransaction;
import dw.framework.PageNameInterface;
import dw.framework.TransactionInterface;

public abstract class RegisterMfaTransactionInterface extends TransactionInterface  {

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected RegisterMfaToken registerMfaToken;
	
    /** 
     * Constructor.
     */
    public RegisterMfaTransactionInterface(String fileName, RegisterMfaToken registerMfaToken, PageNameInterface naming, boolean sideBar) {
        super(fileName, naming, sideBar);	
        this.registerMfaToken = registerMfaToken;
    }

	@Override
    public ClientTransaction getTransaction() {
        return registerMfaToken;
    }
}
