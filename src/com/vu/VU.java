/*
 * Created on Feb 17, 2005
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.vu;

import dw.comm.AgentConnect;
import dw.softwareversions.SoftwareVersionUtility;

/**
 * @author A405
 * 
 *         To change the template for this generated type comment go to
 *         Window>Preferences>Java>Code Generation>Code and Comments
 */
public class VU {

	public static void main(String[] args) {

		String[] a = null;
		if (args.length == 1) {
			a = new String[] { "validate", args[0] };
		} else if (args.length == 2) {
			a = new String[] { "validate", args[1], args[0] };
		}
		AgentConnect.enableDebugEncryption();
		SoftwareVersionUtility.validateVersion(a);
		AgentConnect.disableDebugEncryption();
	}
}
