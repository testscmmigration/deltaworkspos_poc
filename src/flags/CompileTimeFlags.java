package flags;

public class CompileTimeFlags {
	/*
	 * Note: Flags must all be false when checked in, so builds do not get
	 * debug data.
	 */

	private static boolean debug = 						false;
	private static boolean rcptLogFlags = 				false;
	private static boolean rcptLogTemplateParsing = 	false;
	private static boolean rcptNoDeleteOldFile = 		false;
	private static boolean rcptNoFileValidation = 		false;
	private static boolean rcptNoTemplateEncryption = 	false;
	private static boolean dspnLoadShortCircuit = 		false;
	private static boolean reportLogging = 				false;
	private static boolean noJarVerification = 			false;
	private static boolean validateAgainstWSDL = 		false;
	private static boolean isiDebug =					false;

	/*
	 * Use methods so we can change values while debugging.
	 */
	public static boolean debug() {
		return debug;
	}

	public static boolean dspnLoadShortCircuit() {
		return dspnLoadShortCircuit;
	}

	public static boolean rcptLogFlags() {
		return rcptLogFlags;
	}

	public static boolean rcptLogTemplateParsing() {
		return rcptLogTemplateParsing;
	}

	public static boolean rcptNoDeleteOldFile() {
		return rcptNoDeleteOldFile;
	}

	public static boolean rcptNoFileValidation() {
		return rcptNoFileValidation;
	}

	public static boolean rcptNoTemplateEncryption() {
		return rcptNoTemplateEncryption;
	}

	public static boolean reportLogging() {
		return reportLogging;
	}

	public static boolean noJarVerification() {
		return noJarVerification;
	}

	public static boolean isValidateAgainstWSDL() {
		return validateAgainstWSDL;
	}

	public static boolean isIsiDebug() {
		return isiDebug;
	}
}
