@echo off
:: DeltaWorks Build and Release batch file

:: Setup required variables
set Drive=C
set AntDir=%Drive%:\WSADdev\apache-ant-1.8.2
set DwCommand=%0
set DwPosDir=%Drive%:\WSADdev\DW_POS\DeltaWorksPOS
set JAVA_HOME=%Drive%:\jdk1.6.0_45

set CVSROOT=:extssh:%USERNAME%@cvs.moneygram.com:/opt/cvs2
echo %CVSROOT%>E:\CVS\Root
echo CVS Root files replaced with root files for %USERNAME% 
replace E:\CVS\Root C:\WSADdev\DW_POS\DeltaWorksPOS /S >cvs.log

:: Setup default variables
set Branch=
set BranchMax=20_00
set BranchMin=05_00
set BranchOption=-Dis.branch=N
set BranchPrefix=DW_
set RegionDv=0
set RegionQa=0
set ServerPassword=0
set Target=veraCode
set Version=
set VersionMax=200000
set VersionMin=500

:: Check for wanting help
if %1. EQU ?. goto :Usage
if %1. EQU -h. goto :Usage
if %1. EQU /h. goto :Usage
if %1. EQU -help. goto :Usage
if %1. EQU /help. goto :Usage

:: Verify enough disk space is available on drive I:
dir I: | findstr /r "Dir.*([0-9]*).*bytes free"
echo CAUTION: Verify that the above free space on drive I is above 30 MB!!!
pause

:: Setup REDO parameters
if %1. NEQ REDO. set cvs_move_flag=-Dcvs.move.flag=N
if %1. EQU REDO. set cvs_move_flag=-Dcvs.move.flag=Y
if %1. NEQ REDO. set cvs_move_tag=
if %1. EQU REDO. set cvs_move_tag=-Dcvs.move.tag=-F
if %1. NEQ REDO. set fcsw_off_or_on=-Dfcsw.off.or.on=on
if %1. EQU REDO. set fcsw_off_or_on=-Dfcsw.off.or.on=off
if %1. EQU REDO. shift

:: If no parameters, use the defaults (user will be prompted for the version)
:Check_for_next_parameter
if %1. EQU . goto :LoginToCvs

:: Check if MGT database number specified for DV Region (zero implies don't do)
if %1 LSS 0 goto :Check_devbuilder_target
if %1 GEQ 6 goto :Check_devbuilder_target
:: Set specified MGT database number for DV Region
set RegionDv=%1
shift

:: Check if MGT database number specified for QA Region (zero implies don't do)
if %1. EQU . goto :CheckForServerPassword
if %1 LSS 0  goto :CheckForServerPassword
if %1 GEQ 6  goto :CheckForServerPassword
:: Set specified MGT database number for QA Region
set RegionQa=%1
shift

:CheckForServerPassword
if %RegionDv% NEQ 0 goto :SetServerPassword
if %RegionQa% NEQ 0 goto :SetServerPassword
goto :Check_devbuilder_target

:SetServerPassword
if %1. EQU . goto :UsageError3
set ServerPassword=%1
shift

:: Check for devbuilder target
:Check_devbuilder_target
if %1. EQU . goto :LoginToCvs
if %1 NEQ devbuilder goto :Check_devbuilder_target
set Target=devbuilder
shift
if %1. EQU . goto :LoginToCvs

:: Check for veraCode target
:Check_devbuilder_target
if %1. EQU . goto :LoginToCvs
if %1 NEQ veraCode goto :CheckIfVersion
set Target=veraCode
shift
if %1. EQU . goto :LoginToCvs

:: This has to be the version number; check if a number
:CheckIfVersion
if %1 LSS %VersionMin% goto :UsageError1
if %1 GEQ %VersionMax% goto :UsageError1
set Version=-Dbuild.version=%1

:: Check if there is a branch parameter
if %2. EQU . goto :LoginToCvs
if %2 LSS 00000 goto :UsageError2
if %2 LSS %BranchMin% goto :UsageError2
if %2 GEQ %BranchMax% goto :UsageError2
set BranchOption=-Dis.branch=Y
set Branch=-Dbuild.branch=%BranchPrefix%%2


:: Login to CVS: Remember that one cannot log into cvs in the build.xml file because 
:: it does not work for CVSNT, see this link and look at the bottom of the page: 
::		https://issues.apache.org/bugzilla/show_bug.cgi?id=21657
:: "It's not a CVSNT bug but rather an intentional omission as they seem to prefer
:: storing passwords in the windows registry. For those like me who spent way too
:: much time trying to figure out a "supported" CVSNT workaround for this:
:: Instead of using the cvspass task and passfile attributes on your cvs task just
:: add the password to your repository path like so:
::		:pserver:USER:PASSWORD@server:/path/to/cvsroot"

:: Rather than do the above and have the password in the clear, it has been decided to 
:: have a batch file execute: cvs login, which causes the user to be prompted for the 
:: user's password, which is not displayed. After this, the batch file calls the ant 
:: program with the build.xml file as a parameter (see below).

:: Login to CVS (Note: user will be prompted for the user's LAN password)
:LoginToCvs
cvs login

:: Check if cvs login error
if %errorlevel% NEQ 0 goto :Exit

:: Update build directory
%Drive%:
cd %DwPosDir%\build
if %2. NEQ . goto :GetBuildBranch
echo Updating build files from head
::cvs update -A -C -P -d -f>>cvs.log
goto :CleanupOldFiles
:GetBuildBranch
echo Updating build files from branch %BranchPrefix%%2
::cvs update -A -C -P -d -f -r %BranchPrefix%%2>>cvs.log

:: Cleanup by deleting old, unneeded files
:CleanupOldFiles
if exist classes rmdir /s /q classes >NUL:
if exist Deltaworks rmdir /s /q DeltaWorks >NUL:
if exist temp rmdir /s /q temp >NUL:
if exist .#* del .#* >NUL:
if exist *.bak del *.bak >NUL:
if exist *.exe del *.exe >NUL:
if exist *.log del *.log >NUL:
if exist *.sql del *.sql >NUL:
if exist dw*.zip del dw*.zip >NUL:

:: Call ant to do the build and release
set dev_db=-Ddev_dbase=%RegionDv%
if $RegionDv% EQU 0 set dev_db=
set qa_db=-Dqa_dbase=%RegionQa%
if $RegionQv% EQU 0 set qa_db=
set sv_pw=-Dserver_password=%ServerPassword%
if $server_password% EQU 0 set sv_pw=

echo call %AntDir%\bin\ant.bat -f VeraCodeBuild.xml %cvs_move_flag% %cvs_move_tag% %fcsw_off_or_on% %dev_db% %qa_db% %sv_pw% %Version% %BranchOption% %Branch% %Target%
call %AntDir%\bin\ant.bat -f VeraCodeBuild.xml %cvs_move_flag% %cvs_move_tag% %fcsw_off_or_on% %dev_db% %qa_db% %sv_pw% %Version% %BranchOption% %Branch% %Target%
cd ..\
goto :Exit

:UsageError3
echo.
echo Error: There must be a server password if a DEV region and/or a QA region is specified. 
goto :Usage

:UsageError2
echo.
echo Error: The parameter, %2, is not a valid branch; it must of the form yr_mn (note: it is prefixed with DW_).
goto :Usage

:UsageError1
echo.
echo Error: The parameter, %1, is not correct; if a version it must be more than %VersionMin% and less than %VersionMax%; otherwise check the usage.
goto :Usage

::Help and usage
:Usage
echo.
echo usage: %DwCommand% [?^|-h^|/h^|-help^|/help] ^| [[[[0-5] [0-5]] [^<ServerPassword^>]] [devbuilder] [^<version^> [^<branch^>]]]
echo Note: The first digit [0-5] specifies the DV database; the second the QA database region.
echo       Specifying a database region as zero means that region (DV or QA) will not be used. 
echo example 1 - Display help:                               %DwCommand% ? 
echo example 2 - Build DW, dv2, qa2, password, 12.11.13:     %DwCommand% 2 2 pw 121113
echo example 3 - Build DW, dv3, qa2, 12.11.94 from DW_11_05: %DwCommand% 3 2 pw 121193 11_05
echo example 4 - Build DW, 12.11.94 (no db or server store): %DwCommand% 121194
echo example 5 - Build DW, ask version (no db or srvr store):%DwCommand%
echo example 6 - Build DW, dv2, , no qa, password, 12.11.13: %DwCommand% 2 0 pw 121113
echo example 7 - Redo build, dv2, password, ask for version: %DwCommand% REDO 2 pw 121113
echo example 8 - Build DW, target=devbuilder, ask version:   %DwCommand% devbuilder
echo example 9 - Build DW, target=devbuilder, 12.11.13:      %DwCommand% devbuilder 121113

:Exit
