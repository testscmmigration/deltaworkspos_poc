@echo off
:: DeltaWorks Build and Release batch file

setlocal EnableDelayedExpansion

:: Setup required variables
set Drive=C
set AntDir=%Drive%:\WSADdev\apache-ant-1.8.2
set DwCommand=%0
set DwPosDir=.
set JAVA_HOME=%Drive%:\jdk1.6.0_45

set argCount=0
set buildvar=
set Version=
set VersionMax=200000
set VersionMin=500

cd %DwPosDir%\build

:CheckIfVersion
echo CheckIfVersion
echo %1
if %1 LSS %VersionMin% goto :UsageError1 %1
if %1 GEQ %VersionMax% goto :UsageError1 %1
set Version=-Dbuild.version=%1
shift

for %%x in (%*) do (
   set /A argCount+=1
   set "argVec[!argCount!]=%%~x"
)
echo Number of processed arguments: %argCount%
for /L %%i in (2,1,%argCount%) do CALL :concat %%i 
echo %buildvar%
goto :cleanup

:cleanup
if exist classes rmdir /s /q classes >NUL:
if exist Deltaworks rmdir /s /q DeltaWorks >NUL:
if exist temp rmdir /s /q temp >NUL:
if exist .#* del .#* >NUL:
if exist *.bak del *.bak >NUL:
if exist *.exe del *.exe >NUL:
if exist *.log del *.log >NUL:
if exist *.sql del *.sql >NUL:
if exist dw*.zip del dw*.zip >NUL:

:runant
echo call %AntDir%\bin\ant.bat -f devbuild.xml %Version% %buildvar%
call %AntDir%\bin\ant.bat -f devbuild.xml %Version% %buildvar%
cd ..\
goto :Exit
:concat
set counter=%1
set /a counter-=1
set buildvar=%buildvar% !argVec[%1]!
goto :Exit

:UsageError1
echo.
echo Error: The parameter, %1, is not correct; if a version it must be more than %VersionMin% and less than %VersionMax%; otherwise check the usage.
goto :Usage

::Help and usage
:Usage
echo.
echo usage: %DwCommand% [?^|-h^|/h^|-help^|/help] ^| [[[[0-5] [0-5]] [^<ServerPassword^>]] [devbuilder] [^<version^> [^<branch^>]]]
echo Note: The first digit [0-5] specifies the DV database; the second the QA database region.
echo       Specifying a database region as zero means that region (DV or QA) will not be used. 
echo example 1 - Display help:                               %DwCommand% ? 
echo example 2 - Build DW, dv2, qa2, password, 12.11.13:     %DwCommand% 2 2 pw 121113
echo example 3 - Build DW, dv3, qa2, 12.11.94 from DW_11_05: %DwCommand% 3 2 pw 121193 11_05
echo example 4 - Build DW, 12.11.94 (no db or server store): %DwCommand% 121194
echo example 5 - Build DW, ask version (no db or srvr store):%DwCommand%
echo example 6 - Build DW, dv2, , no qa, password, 12.11.13: %DwCommand% 2 0 pw 121113
echo example 7 - Redo build, dv2, password, ask for version: %DwCommand% REDO 2 pw 121113
echo example 8 - Build DW, target=devbuilder, ask version:   %DwCommand% devbuilder
echo example 9 - Build DW, target=devbuilder, 12.11.13:      %DwCommand% devbuilder 121113

:Exit
