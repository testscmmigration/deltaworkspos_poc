@echo off
:: Inserts information about a new version of Deltaworks POS into
:: the software_version tables, to make it known to profile editor.
::
:: Rev   Date     Author    Description
:: ----- -------- --------- --------------------------------------
:: 1.0   08/15/03 G Atkin   Initial revision
:: 2.0   10/04/06 R Morgan  Changed the script to run on Cybermation
::
::
:: Parameters:
::      %1 location of Oracle PLUS80.EXE
::	%2 userid/password@database
::	%3 location of the SQL script (root where this script runs)
::	%4 Prod indicator (defaults to TEST)
::
:: Example (for production):
::
::	pmgsqdw001r d:\orant\bin posmaint/****@mgtp d:\ISAAP\MG\DBLoad\Scripts P
::
:: Example (for QA):
::
::	pmgsqdw001r c:\orant\bin posmaint/****@mgtdwq
::
:: Example (for Dev):
::
::	pmgsqdw001r c:\orant\bin dwuser/****@mgtdwd

set ev=0;
set ESPAGENTDIR="C:Program Files\Cybermation\ESP System Agent"

set orapath=%1
set oralogin=%2
set sourcepath=.
if not "%3" == "" set sourcepath=%3
set envname="TEST"
if "%4" == "P" set envname=PROD
if "%4" == "p" set envname=PROD

cd /d %sourcepath%

%orapath%\plus80.exe %oralogin% @pmgsqdw001r.sql

if %errorlevel% == 0 goto DONE

set ev=%errorlevel%
REM if %envname% == PROD cawto "-c", red, "!", "Error encountered in PMGSQDW001R SQL script see PMGSQDW001R.LOG"

:DONE
REM if %envname% == PROD cau9test.exe f=%ev%
c:
cd %ESPAGENTDIR%

SetExitC %ev%