::Deltaworks main launcher script

:: Rows of x's are necessary to preserve file offsets of GOTO labels,
:: so this script can be replaced while running.
:: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
:: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
:: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
:: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
:: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
:: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

@echo off
cls

echo Starting Deltaworks  
echo Starting Deltaworks > deltaWorks.log

if exist jre1651\manifest.dat GOTO :start
jre1651z\unzip -qq -o jre1651z\jre.zip >>deltaWorks.log

:: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
:: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
:: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
:: xxxxxxxxxxxxxxxxxxxxxx
:start

:: locate the most recent (and valid) version of Deltaworks 
:: current version flag = 1 means try to get the most recent version first
SET CURRENT_VERSION=1

:: find the most recent version directory, starting from CURRENT_VERSION and going back

:: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
:: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
:locate_version
VersionLocator.exe %CURRENT_VERSION%
call version.bat

:: preserve the return value from the utility

IF %CURRENT_VERSION%==1 GOTO :no_versions_error
IF %CURRENT_VERSION%==0 GOTO :no_versions_error
IF %CURRENT_VERSION%==9009 GOTO :no_java_error

:: the version is already valid check done in the C program.
GOTO :launch

:: launch the application
:: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
:: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
:: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
:: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
:: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
:: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
:: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
:: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
:: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
:: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
::
:launch 
type %CURRENT_VERSION%\launch.bat > start_deltaworks.bat
set RUN_RESULT=0
call start_deltaworks.bat %CURRENT_VERSION% %CURRENT_JRE% >>deltaWorks.log
del start_deltaworks.bat

:: repeat the process if application exited with a 99 (restart) exit code
::   the launch.bat file should have set this when it completed the run
IF %RUN_RESULT%==99 GOTO :restart
GOTO :exit

:: end the process and notify the user that program can not start
:: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
:: xxxxxxxx
:no_versions_error
echo DeltaWorks can not be started--no_versions_error >>deltaWorks.log
GOTO :end

:: can not continue without a java executable
:: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
:: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
:no_java_error
echo DeltaWorks can not be started. No JAVA.EXE found.
GOTO :end

:: xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
:exit
echo Deltaworks  completed
GOTO :end

:: go back up to main launch directory and start over again
:restart
GOTO :start

:: end the process
:end



