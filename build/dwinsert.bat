@echo off
:: Inserts information about a new version of Deltaworks POS into
:: the software_version tables, to make it known to profile editor.
::
:: Usage:
::	dwinsert user/password@database
::
:: Example (for QA):
::
::	dwinsert posmaint/posmaint@mgtdwq
::
:: Example (for production):
::
::	dwinsert posmaint/********@mgtp
::
:: The SQL script dwinsert.sql is taken from the current directory
:: and a spool file is written to the current directory.

:: The location of the sqlplus executable is specified here:
set sqlplus=c:\orant\bin\plus80.exe


%sqlplus% %1 @dwinsert.sql



