@echo off
call ..\variables.bat
:: Make a jreNNNu.zip file to update an Oracle jre
::
:: To make a jreNNNu.zip file to update an Oracle jre, do the following:
:: 1. From some directory, copy the Oracle jreNNN directory (e.g.; jre1866)
::    and rename it jre1866o (for original)
:: 3. In the same directory that contains jre1866o, create a jreNNNu update
::    directory (e.g.; jre1866u), which contains the only the jre updates
:: 4. Run this batch program, which creates an updated jreNNNm directory of
::    the merge of jreNNNo and jreNNNu, as well as a jreNNN result directory
:: 5. Zip up the jreNNN result directory and rename jreNNNu.zip (e.g.; jre1866u.zip)
:: 6. Copy and commit the jreNNNu.zip file to the
::    C:\WSADdev\DW_POS\DeltaWorksPOS\build\versioned directory

:: Defensive checks

set DW_BUILD=%DW_ROOT%\build
set TOOLS_CLASSPATH=%DW_ROOT%\bin-tools
if '%1' == '' goto :JreParamterNotExists
if not exist %1o goto :JreOriginalDirNotExists
if not exist %1u goto :JreUpdateDirNotExists

:: Remove the jreNNN reults directiory, if present
echo Removing %1, if it is present
if exist %1 rmdir /S /Q %1

:: Remove the updated jreNNNm merged directiory, if present
echo Removing %1m, if it is present
if exist %1m rmdir /S /Q %1m

:: Remove the jreNNNu,zip file, if present
echo Removing %1u.zip, if it is present
if exist %1u.zip del /Q %1u.zip

:: Create the jreNNN merge directory, later to be renamed jreNNNm
echo Creating the updated jre merged directory %1
mkdir %1

:: Copy the jreNNNo directory to the merge directory
echo Copying from %1o\*.* to %1, please wait ...
xcopy /S /E /Q /Y %1o\*.* %1\*.*

:: Copy (and overwrite) the jre updates  to the merge directory
echo Copying from %1u\*.* to %1, please wait ...
xcopy /S /E /Q /Y %1u\*.* %1\*.*

:: Delete original manifest file in the merge directory
echo Delete original manifest file in the merge directory
if exist %1\manifest.dat del %1\manifest.dat

:: Create the manifest file for the merged jre
echo Creating the JRE manifest file for %1m, please wait ...
:: Note: This does not automatically save the manifest file in the server base or put an entry into the database (was mean for the future)
:: Arguments: create, version, server_base, label, description
%JDK%\java -classpath %DW_ROOT%\bin;%TOOLS_CLASSPATH% dw.softwareversions.SoftwareVersionBuilder create %1 dw_client/jre/%1 %1 GenerateTemp%1 

						 
:: Rename the updated jreNNN merged directory to jreNNNm and
:: create the jreNNN result directory
echo Renaming %1 to %1m, and creating the result directory %1
ren %1 %1m
mkdir %1

:: Copy the jre updates  to the jreNNN result directory
echo Copying from %1u\*.* to %1, please wait ...
xcopy /S /E /Q /Y %1u\*.* %1\*.*

:: Copy the JRE manifest file from the updated jreNNNm merged directory to the jre result directory
echo Copying %1m\manifest.dat to %1
copy /Y %1m\manifest.dat %1

:: All done
echo.
echo Remember to do the following to complete the task:
echo ***Zip up the jreNNN directory and rename jreNNNu.zip (e.g.; jre1866u.zip)
echo ***Copy and commit the jreNNNu.zip file to the 
echo    C:\WSADdev\DW_POS\DeltaWorksPOS\build\versioned directory
echo %0 complete
goto :Exit


:JreParamterNotExists
echo Aborting: Oracle jre parameter is not specified
goto :usage

:JreOriginalDirNotExists
echo Aborting: Oracle jre original directory, %1o does not exist
goto :usage

:JreUpdateDirNotExists
echo Aborting: Oracle Jre update directory, %1u, does not exist
goto :usage

:usage
echo Usage: %0 jrennn
echo Example %0 jre1866

echo To make a jreNNNu.zip file to update an Oracle jre, do the following:
echo 1. From some directory, copy the Oracle jreNNN directory (e.g.; jre1866)
echo    and rename it jre1866o (for original)
echo 3. In the same directory that contains jre1866o, create a jreNNNu update
echo    directory (e.g.; jre1866u), which contains the only the jre updates
echo 4. Run this batch program, which creates an updated jreNNNm directory of
echo    the merge of jreNNNo and jreNNNu, as well as a jreNNN result directory
echo 5. Zip up the jreNNN result directory and rename jreNNNu.zip (e.g.; jre1866u.zip)
echo 6. Copy and commit the jreNNNu.zip file to the
echo    C:\WSADdev\DW_POS\DeltaWorksPOS\build\versioned directory

:Exit
