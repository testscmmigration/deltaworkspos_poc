:: parms : 1 - version directory of the app to perform validation in
::         2 - base JRE executable to use for running the utility
::         3 - base JRE classpath to use for running the utility

if exist jre1651\manifest.dat GOTO :validate
echo Uncompressing files...
jre1651z\unzip -qq -o jre1651z\jre.zip
echo ...done.
if errorlevel 1 GOTO :endvalidate
%2 -classpath %1\dwGui.jar;%3 com.vu.VU jre1651
if errorlevel 1 GOTO :endvalidate

:validate
%2 -classpath %1\dwGui.jar;%3 com.vu.VU %1
:endvalidate

