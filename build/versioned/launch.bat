:: Launch this DeltaWorks! version. This batch file will be used to launch the app if this
::  version has been deemed to be the most recent and valid version. 
:: The Deltaworks.bat file will copy the contents of this file to the main launch directory
::  under the name start_deltaworks.bat and run that file. Therefore, this file needs to 
::  copy all of its necessary components to the main directory and then delete them again
::  after it is complete.
:: 
:: NOTE : Since this batch file will be copied to the main launch directory and started from 
::        there, the paths are relative to there, not the version directory.
:: Parms : 1 - version that is being run

@echo off

:: copy the necessary files from the version directory to the main launch directory
copy %1\dwGui.jar >NUL:
copy %1\dwXml.jar >NUL:
copy %1\comm.jar >NUL:
copy %1\DeltaworksNew.bat >NUL:
rem copy %1\dwisi.exe >NUL:
copy %1\javax.comm.properties >NUL:
copy %1\jdun2k.dll >NUL:
copy %1\jdun98.dll >NUL:
copy %1\jdunxp.dll >NUL:
copy %1\moneygramlogo.dat >NUL:
copy %1\win32com.dll >NUL:
echo Copied the 10 common files from %1 directory

copy %1\mgi.exe >NUL:
copy %1\SumatraPdf.exe >NUL:
echo Copied 2 more common files from %1 directory

copy %1\VersionLocator.exe >NUL:
echo Copied VersionLocator.exe from %1 directory

::this deletion is used to enable a clean rewrite of the serial.txt
if exist c:\deltaworks\serial.txt del serial.txt

::setname.bat is no longer needed if exist
if exist c:\deltaworks\setname.bat del setname.bat

:: append  old software download log file to trace file
if not exist update.log goto :no_update_log
echo ---STATUS OF PREVIOUS SW UPDATE >> Trace1.txt
type update.log >> Trace1.txt
echo Status of previous software update to Trace1.txt
del update.log

:no_update_log
attrib -r Deltaworks.bat
copy DeltaworksNew.bat Deltaworks.bat >NUL:
echo Copied DeltaworksNew.bat to Deltaworks.bat

:: record the PC serial number or Dell service tag
@set MOTOINFO=MOTOINFO.EXE
@if exist C:\IBMTOOLS\IBMHELP\WINBIOS.EXE set MOTOINFO=C:\IBMTOOLS\IBMHELP\WINBIOS.EXE
@if exist C:\POS_ID\WINBIOS.EXE set MOTOINFO=C:\POS_ID\WINBIOS.EXE
@%MOTOINFO% > motoinfo.out

:: for updating the backup copy of the warehouse profile
if exist %1\newstate.txt goto :no_backup
if not exist "C:\DW Backup\state.dat" goto :no_backup
attrib -r "C:\DW Backup\state.dat"
copy %1\state.new "C:\DW Backup\state.dat"
echo newstate >> %1\newstate.txt
echo newstate
:no_backup

:: start the application, using the JRE we are dependent on, and passing in the version #
:: preserve the System.exit() code for the calling batch file
if '%2' == '' set DW_JRE_VERSION=jre1651
if not '%2' == '' set DW_JRE_VERSION=%2
echo %DW_JRE_VERSION%\bin\java -classpath dwGui.jar;dwXml.jar;comm.jar -DPath=.\%DW_JRE_VERSION% -Dsun.java2d.noddraw=true -Xms16M dw.main.DeltaworksStartup %1
%DW_JRE_VERSION%\bin\java -classpath dwGui.jar;dwXml.jar;comm.jar -DPath=.\%DW_JRE_VERSION% -Dsun.java2d.noddraw=true -Xms16M dw.main.DeltaworksStartup %1
if errorlevel 99 GOTO :setOther
if errorlevel 1 GOTO :setOne
if errorlevel 0 GOTO :setZero

:setZero
set RUN_RESULT= 0
GOTO :continue

:setOne
set RUN_RESULT= 1
GOTO :continue

:setOther
set RUN_RESULT= 99
GOTO :continue

:continue
echo Run_Result=%RUN_RESULT%

:: app is done executing, so clean the files out of the main app directory
del dwGui.jar
del dwXml.jar
del comm.jar
del DeltaworksNew.bat
rem del dwisi.exe
del javax.comm.properties
del jdun2k.dll
del jdun98.dll
del jdunxp.dll
del moneygramlogo.dat
del win32com.dll
echo Deleted the 10 common files from DeltaWorks directory

del mgi.exe
echo Deleted the 1 more common file from DeltaWorks directory
