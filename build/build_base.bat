@echo off


echo To make a base.zip file , do the following:
echo 1. From some directory, copy the Oracle Main jre - jreNNN directory (e.g.; jre1645)
echo    and rename it jre1645m
echo 2. If DW supports any future JRE, copy the future jre to the directory- jreNNNN  (e.g.; jre16155)
echo    and rename it jre1651m 
echo 2. Modify the build_base.bat,make sure the JRE_NAME and POSTUPGRADE_JRE_NAME are set correctly.
echo Eg: -
echo 	set JRE_NAME=jre1651
echo 	set JRE_NAME_ZIP=jre1651z
echo 	set POSTUPGRADE_JRE_NAME=jre1651
echo 	set POSTUPGRADE_JRE_NAME_ZIP=jre1651z

call ..\variables.bat

:: This script creates base.zip, which contains a copy of the Java Runtime
:: Environment and a handful of files needed for a "fresh install" of 
:: DeltaWorks, which are not included in the built-in software download
:: feature or the zip file for a specific version.
::
:: Before running this script, the appropriate versions of the contents
:: of the common folder need to be "gotten" from PVCS, and the specified
:: source jre folder needs to exist.
::
:: This script only needs to be executed when we change JRE versions.
:: We do not use everything in the JRE as it comes from Sun, but the 
:: redistribution license does not allow modification, and only allows 
:: certain specific files to be deleted. Because DeltaWorks is now 
:: internationalized, we're including a couple of files (charsets and 
:: localedata) that we used to delete.
::
:: We zip the JRE because otherwise the download is excessively large. 
:: To use, builds should specify a dependency on jre[NNN]zip and the
:: startup script should be modified to unzip it if needed. A manifest 
:: of the unzipped JRE is created to support future validation checks.
::
:: It's a big zip file; SoftwareVersionBuilder can split large files
:: into smaller pieces by including "2097152 true" after the create
:: parameter, but some of the details for merging the pieces together
:: at the other end were never fully worked out. I'm also creating 

set JRE_NAME=jre1651
set JRE_NAME_ZIP=jre1651z

set PREREQUISTE_JRE_NAME=JRE_NAME
set PREREQUISTE_JRE_NAME_ZIP=JRE_NAME_ZIP
set POSTUPGRADE_JRE_NAME=
set POSTUPGRADE_JRE_NAME_ZIP=


set DW_BUILD=%DW_ROOT%\build
set JRE_NAME_ZIP_TMP=%DW_BUILD%\jre_tmp
set SHIP_FOLDER=%DW_BUILD%\Deltaworks
set SHIP_JRE_FOLDER=%SHIP_FOLDER%\%JRE_NAME%
set ZIP_JRE_FOLDER=%SHIP_FOLDER%\%JRE_NAME_ZIP%
set JRE_ZIP_SPLIT=%DW_BUILD%\%JRE_NAME_ZIP%_split
set JRE_MANIFEST=%DW_BUILD%\%JRE_NAME%
set TOOLS_CLASSPATH=%DW_ROOT%\bin-tools
set MAX_FILE_SIZE=524288 
::1048576

set SOURCE_JRE_FOLDER=%DW_BUILD%\%JRE_NAME%m
set POSTUPGRADE_SOURCE_JRE_FOLDER=%DW_BUILD%\%POSTUPGRADE_JRE_NAME%m


set PREREQUISTE_SHIP_JRE_FOLDER=%SHIP_FOLDER%\%PREREQUISTE_JRE_NAME%
set PREREQUISTE_ZIP_JRE_FOLDER=%SHIP_FOLDER%\%PREREQUISTE_JRE_NAME_ZIP%
set PREREQUISTE_JRE_ZIP_SPLIT=%DW_BUILD%\%PREREQUISTE_JRE_NAME_ZIP%_split
set PREREQUISTE_JRE_MANIFEST=%DW_BUILD%\%PREREQUISTE_JRE_NAME%

set POSTUPGRADE_SHIP_JRE_FOLDER=%SHIP_FOLDER%\%POSTUPGRADE_JRE_NAME%
set POSTUPGRADE_ZIP_JRE_FOLDER=%SHIP_FOLDER%\%POSTUPGRADE_JRE_NAME_ZIP%
set POSTUPGRADE_JRE_ZIP_SPLIT=%DW_BUILD%\%POSTUPGRADE_JRE_NAME_ZIP%_split
set POSTUPGRADE_JRE_MANIFEST=%DW_BUILD%\%POSTUPGRADE_JRE_NAME%

echo DW_ROOT :: %DW_ROOT%
echo SHIP_FOLDER :: %SHIP_FOLDER%
echo SHIP_JRE_FOLDER :: %SHIP_JRE_FOLDER%
echo ZIP_JRE_FOLDER :: %ZIP_JRE_FOLDER%

echo Starting fresh...
cd %DW_BUILD%
rmdir %JRE_NAME_ZIP_TMP% /s/q
rmdir %JRE_ZIP_SPLIT% /s/q
rmdir %SHIP_FOLDER% /s/q
rmdir %POSTUPGRADE_JRE_ZIP_SPLIT% /s/q
rmdir %PREREQUISTE_JRE_ZIP_SPLIT% /s/q

mkdir %SHIP_FOLDER%
mkdir %SHIP_JRE_FOLDER%
mkdir %ZIP_JRE_FOLDER%
mkdir %JRE_ZIP_SPLIT%
mkdir %JRE_MANIFEST%
mkdir %JRE_NAME_ZIP_TMP%


echo Copying common files...
cd %SHIP_FOLDER%

xcopy %DW_BUILD%\common /s/e/q
rmdir %SHIP_FOLDER%\CVS /s/q 
attrib -r *.*

echo Copying JRE files...
cd %SHIP_JRE_FOLDER%
xcopy "%SOURCE_JRE_FOLDER%" /s/e/q

echo Deleting optional JRE files...
del lib\ext\ldapsec.jar lib\ext\dnsns.jar bin\rmid.exe bin\rmiregistry.exe bin\tnameserv.exe bin\keytool.exe bin\kinit.exe bin\klist.exe bin\ktab.exe bin\policytool.exe bin\orbd.exe bin\servertool.exe lib\audio\soundbank.gm /q /f
rmdir javaws /s /q

echo Creating JRE manifest file...
cd %SHIP_FOLDER%
%JDK%\java -classpath %DW_ROOT%\bin;%TOOLS_CLASSPATH% dw.softwareversions.SoftwareVersionBuilder create %JRE_NAME% dw_client/jre/%JRE_NAME% %JRE_NAME% description 
del insert_dw%JRE_NAME%.sql


echo Compressing JRE files...
cd %SHIP_FOLDER%
%JDK%\jar cfM %ZIP_JRE_FOLDER%\jre.zip %JRE_NAME%

copy %ZIP_JRE_FOLDER%\jre.zip %JRE_NAME_ZIP_TMP%\jre.zip

echo Copying jre manifest file for JRE.zip...
copy %SHIP_JRE_FOLDER%\manifest.dat %JRE_MANIFEST%\manifest.dat

rem copy %SHIP_JRE_FOLDER%\manifest.dat %ZIP_JRE_FOLDER%\manifest.dat

echo creating prerequisiteManifest.dat
echo spliting jre manifest file for JRE.zip...creating prerequisiteManifest.dat
echo %JDK%\java -classpath %DW_ROOT%\bin;%TOOLS_CLASSPATH% dw.softwareversions.SoftwareVersionBuilder jrecreate %MAX_FILE_SIZE% false %JRE_NAME_ZIP% dw_client/jre/%JRE_NAME_ZIP% %JRE_NAME_ZIP% description false
%JDK%\java -classpath %DW_ROOT%\bin;%TOOLS_CLASSPATH% dw.softwareversions.SoftwareVersionBuilder jrecreate %MAX_FILE_SIZE% false %JRE_NAME_ZIP% dw_client/jre/%JRE_NAME_ZIP% %JRE_NAME_ZIP% description false
rem xcopy %ZIP_JRE_FOLDER% %JRE_ZIP_SPLIT% /-Y /s/e/q

echo Rename manifest.dat to prerequisiteManifest.dat for JRE.zip..
ren %ZIP_JRE_FOLDER%\manifest.dat prerequisiteManifest.dat
copy %ZIP_JRE_FOLDER%\prerequisiteManifest.dat %JRE_NAME_ZIP_TMP%\prerequisiteManifest.dat

del /F /Q /S %ZIP_JRE_FOLDER%\*.* >NUL

echo Compressing JRE files...
%JDK%\jar cfM %ZIP_JRE_FOLDER%\jre.zip %JRE_NAME%

echo creating postUpgradeManifest.dat
echo spliting jre manifest file for JRE.zip...creating postUpgradeManifest.dat
echo %JDK%\java -classpath %DW_ROOT%\bin;%TOOLS_CLASSPATH% dw.softwareversions.SoftwareVersionBuilder jrecreate %MAX_FILE_SIZE% false %JRE_NAME_ZIP% dw_client/jre/%JRE_NAME_ZIP% %JRE_NAME_ZIP% description true
%JDK%\java -classpath %DW_ROOT%\bin;%TOOLS_CLASSPATH% dw.softwareversions.SoftwareVersionBuilder jrecreate %MAX_FILE_SIZE% false %JRE_NAME_ZIP% dw_client/jre/%JRE_NAME_ZIP% %JRE_NAME_ZIP% description true
xcopy %ZIP_JRE_FOLDER% %JRE_ZIP_SPLIT% /-Y /s/e/q
del /F /Q /S %ZIP_JRE_FOLDER%\*.* >NUL
echo Rename manifest to postUpgradeManifest for JRE.zip..
ren %JRE_ZIP_SPLIT%\manifest.dat postUpgradeManifest.dat
copy %JRE_NAME_ZIP_TMP%\prerequisiteManifest.dat %JRE_ZIP_SPLIT%\prerequisiteManifest.dat


rem copy %JRE_ZIP_SPLIT%\prerequisiteManifest.dat %JRE_ZIP_SPLIT%\postUpgradeManifest.dat

rem copy %JRE_ZIP_SPLIT%\prerequisiteManifest.dat %ZIP_JRE_FOLDER%
copy %JRE_NAME_ZIP_TMP%\jre.zip %ZIP_JRE_FOLDER%\jre.zip
copy %DW_BUILD%\unzip.exe %ZIP_JRE_FOLDER%
copy %DW_BUILD%\unzip-license.txt %ZIP_JRE_FOLDER%


echo Creating manifest file for JRE.zip...
echo %JDK%\java -classpath %DW_ROOT%\bin;%TOOLS_CLASSPATH% dw.softwareversions.SoftwareVersionBuilder create %JRE_NAME_ZIP% dw_client/jre/%JRE_NAME_ZIP% %JRE_NAME_ZIP% description 
%JDK%\java -classpath %DW_ROOT%\bin;%TOOLS_CLASSPATH% dw.softwareversions.SoftwareVersionBuilder create %JRE_NAME_ZIP% dw_client/jre/%JRE_NAME_ZIP% %JRE_NAME_ZIP% description 

del insert_dw%JRE_NAME_ZIP%.sql


copy %ZIP_JRE_FOLDER%\unzip.exe %JRE_ZIP_SPLIT%
copy %ZIP_JRE_FOLDER%\unzip-license.txt %JRE_ZIP_SPLIT%
copy %ZIP_JRE_FOLDER%\manifest.dat %JRE_ZIP_SPLIT%


rmdir %SHIP_JRE_FOLDER% /s/q
mkdir %SHIP_JRE_FOLDER%
copy %JRE_MANIFEST%\manifest.dat %SHIP_JRE_FOLDER%\manifest.dat
rmdir %JRE_MANIFEST% /s/q

if "%POSTUPGRADE_JRE_NAME%" == "" goto :CreateBase

rem ############################
:PostUpgradeJre

mkdir %POSTUPGRADE_SHIP_JRE_FOLDER%
mkdir %POSTUPGRADE_ZIP_JRE_FOLDER%
mkdir %POSTUPGRADE_JRE_ZIP_SPLIT%
mkdir %POSTUPGRADE_JRE_MANIFEST%


echo Copying Prerequisite JRE files...
cd %POSTUPGRADE_SHIP_JRE_FOLDER%
xcopy "%POSTUPGRADE_SOURCE_JRE_FOLDER%" /s/e/q



echo Deleting optional JRE files...
del lib\ext\ldapsec.jar lib\ext\dnsns.jar bin\rmid.exe bin\rmiregistry.exe bin\tnameserv.exe bin\keytool.exe bin\kinit.exe bin\klist.exe bin\ktab.exe bin\policytool.exe bin\orbd.exe bin\servertool.exe lib\audio\soundbank.gm /q /f
rmdir javaws /s /q

echo Creating JRE manifest file...
cd %SHIP_FOLDER%
%JDK%\java -classpath %DW_ROOT%\bin;%TOOLS_CLASSPATH% dw.softwareversions.SoftwareVersionBuilder create %POSTUPGRADE_JRE_NAME% dw_client/jre/%POSTUPGRADE_JRE_NAME% %POSTUPGRADE_JRE_NAME% description 
del insert_dw%POSTUPGRADE_JRE_NAME%.sql


echo Copying jre manifest file for JRE.zip...
copy %POSTUPGRADE_SHIP_JRE_FOLDER%\manifest.dat %POSTUPGRADE_JRE_MANIFEST%\manifest.dat



echo Compressing JRE files...
cd %SHIP_FOLDER%
%JDK%\jar cfM %POSTUPGRADE_ZIP_JRE_FOLDER%\jre.zip %POSTUPGRADE_JRE_NAME%
echo jre.zip created


echo creating postUpgradeManifest.dat
echo spliting jre manifest file for JRE.zip...creating postUpgradeManifest.dat
%JDK%\java -classpath %DW_ROOT%\bin;%TOOLS_CLASSPATH% dw.softwareversions.SoftwareVersionBuilder jrecreate %MAX_FILE_SIZE% true %POSTUPGRADE_JRE_NAME_ZIP% dw_client/jre/%POSTUPGRADE_JRE_NAME_ZIP% %POSTUPGRADE_JRE_NAME_ZIP% description true
xcopy %POSTUPGRADE_ZIP_JRE_FOLDER% %POSTUPGRADE_JRE_ZIP_SPLIT% /-Y /s/e/q

del /F /Q /S %POSTUPGRADE_ZIP_JRE_FOLDER%\*.* >NUL
echo Rename manifest to postUpgradeManifest for JRE.zip..
ren %POSTUPGRADE_JRE_ZIP_SPLIT%\manifest.dat postUpgradeManifest.dat

copy %POSTUPGRADE_JRE_ZIP_SPLIT%\postUpgradeManifest.dat %POSTUPGRADE_ZIP_JRE_FOLDER%
copy %POSTUPGRADE_JRE_ZIP_SPLIT%\jre.zip %POSTUPGRADE_ZIP_JRE_FOLDER%
copy %DW_BUILD%\unzip.exe %POSTUPGRADE_ZIP_JRE_FOLDER%
copy %DW_BUILD%\unzip-license.txt %POSTUPGRADE_ZIP_JRE_FOLDER%

echo Creating manifest file for JRE.zip...
%JDK%\java -classpath %DW_ROOT%\bin;%TOOLS_CLASSPATH% dw.softwareversions.SoftwareVersionBuilder create %POSTUPGRADE_JRE_NAME_ZIP% dw_client/jre/%POSTUPGRADE_JRE_NAME_ZIP% %POSTUPGRADE_JRE_NAME_ZIP% description 

del insert_dw%POSTUPGRADE_JRE_NAME_ZIP%.sql


rmdir %POSTUPGRADE_SHIP_JRE_FOLDER% /s/q
rem mkdir %POSTUPGRADE_SHIP_JRE_FOLDER%
rem copy %POSTUPGRADE_JRE_MANIFEST%\manifest.dat %POSTUPGRADE_SHIP_JRE_FOLDER%\manifest.dat
rmdir %POSTUPGRADE_JRE_MANIFEST% /s/q


rem ############# END- PostUpgradeJre###################



:CreateBase
copy %JRE_NAME_ZIP_TMP%\jre.zip %ZIP_JRE_FOLDER%\jre.zip
copy %JRE_NAME_ZIP_TMP%\jre.zip %JRE_ZIP_SPLIT%\jre.zip

rmdir %JRE_NAME_ZIP_TMP% /s/q

echo Creating base.zip...
cd %DW_BUILD%
%JDK%\jar cfM base.zip Deltaworks

