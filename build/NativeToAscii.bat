@echo off
:: native2ascii is a JDK tool that can be used to convert a file with 
::	"non-Latin 1" or "non-Unicode" characters to "Unicode-encoded" characters.
::
::	For more information, see the following links: 
::		http://download.oracle.com/javase/1.4.2/docs/tooldocs/windows/native2ascii.html
::		http://www.mkyong.com/java/java-convert-chinese-character-to-unicode-with-native2ascii/

:: Default directory for file
set I18l=C:\WSADdev\DW_POS\DeltaWorksPOS\src\xml\i18n

:: Check usage:
if '%1' == '' goto :ErrorUsage

::Set directory if specified
if not '%2' == '' set I18lDir=%2
if '%2' == '' set I18lDir=%I18l%

:: Check if directory and file exists
if not exist %I18lDir%\%1 goto :ErrorUsage

:: Convert the native file to the Ascii unicode file
native2ascii -encoding utf8 %I18lDir%\%1 %I18lDir%\Unicode_%1
echo: Success: native file converted to unicode file 
goto :Exit

:ErrorUsage
echo Error: Incorrect usage, please use the proper form:
echo %0 NonUnicodeNewTranslationFile [DirectoryLocation] 
echo Example: %0 NewChineseTranslationsFile C:\
echo Unicode file will named Unicode_NewTranslationNonUnicodeFile
echo If the directory (second) parameter not specified, the file is assumed to be at:
echo %I18l%
goto :Exit

:Exit
