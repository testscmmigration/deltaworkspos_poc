@rem The drive containing the development hierarchy
@set DW_DRIVE=C:

@rem The root of the development hierarchy
@set DW_ROOT=%DW_DRIVE%\WSADdev\dw_pos\DeltaWorksPOS

@rem Optional debugging flag or optimizing flag 
@set DW_DEBUG=

@rem Where you go to run code you just compiled
@set DW_DEV=%DW_ROOT%\dev

@rem Where jar files are kept 
@set DW_JARS=%DW_ROOT%\build\versioned

@rem The version number you want your dev copy to claim to be
@set DW_VERSION=170700

@rem Where the com and xml folders are
@set DW_SOURCE=%DW_ROOT%\src

@rem Where the tool src folders are
@set DW_TOOLS_SRC=%DW_ROOT%\tools

@rem Where too, class files are kept
@set DW_TOOL_CLASSES=%DW_ROOT%\bin-tools

@rem Where class files are kept
@set DW_CLASSES=%DW_ROOT%\bin

@rem Other Jar files we need
@set MISC_JARS=%DW_JARS%\comm.jar;%DW_JARS%\jdun.jar;

@rem Where the javac and jar commands are
@set JDK=C:\jdk1.6.0_151\bin

@rem Where the PVCS command-line tools are
@set PVCS=C:\Program Files\PVCS\VM\Win32\Bin

@set DW_BUILD=%DW_ROOT%\build

pause

@rem Typical compile options
@set DW_JAVAC_OPTIONS= %DW_DEBUG% -d %DW_CLASSES% -sourcepath %DW_SOURCE% -classpath %DW_CLASSES%;%MISC_JARS%

@rem Typical runtime options (to run code you just compiled)
@rem Note: includes dw_classes and dw_source instead of dwGui.jar and dwXml.jar
@rem so you don't have to regenerate jar files on every compile.
@rem (add -Xrunhprof:cpu=samples for profiling) -XX:+DisableExplicitGC -Xrunhprof
@set DW_JAVA_OPTIONS=-classpath %DW_CLASSES%;%DW_SOURCE%;%MISC_JARS% -Dsun.java2d.noddraw=true

