@echo off
:: Generates AgentConnect Source from WSDL and XSD

:: Setup required variables
set Drive=C
set DwCommand=%0
set DwPosDir=%~dp0
set JAVA_HOME="%Drive%:\jdk1.6.0_45"
set AC_VERSION=%1
set WSDL_SRC=%DwPosDir%\resources\META-INF\wsdl
set WSDL_CATLOG=%DwPosDir%\resources\META-INF\jax-ws-catalog.xml
set WSDL_BINDING=%DwPosDir%\build\wsdl\wsdl-binding-config.xml

set COMMAND=%JAVA_HOME%\bin\wsimport.exe %WSDL_SRC%\AgentConnect%AC_VERSION%.wsdl -b %WSDL_BINDING% -p com.moneygram.agentconnect -s ACsrc -d bin -catalog %WSDL_CATLOG% -wsdllocation http://localhost/wsdl/AgentConnect.wsdl
echo %COMMAND%
call %COMMAND%

