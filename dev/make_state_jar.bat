@call ..\variables.bat
setlocal
echo off
@set STATE_TEMP=%DW_BUILD%\stateTemp
@set STATE_TEMP_SRC=%STATE_TEMP%\src
@set STATE_TEMP_BIN=%STATE_TEMP%\bin
rmdir %STATE_TEMP% /s/q
mkdir %STATE_TEMP%
mkdir %STATE_TEMP_SRC%
mkdir %STATE_TEMP_BIN%

xcopy %DW_TOOLS_SRC%\dw\io\StateCommand.java %STATE_TEMP_SRC%\dw\io\StateCommand.java* /Y
%JDK%\jar cf state.jar -C %DW_CLASSES% dw
%JDK%\jar uf state.jar -C %DW_CLASSES% flags

%JDK%\javac -d %STATE_TEMP_BIN% -classpath %DW_CLASSES% -sourcepath %STATE_TEMP_SRC% %STATE_TEMP_SRC%/dw/io/StateCommand.java
%JDK%\jar ufm state.jar %DW_TOOLS_SRC%\state\manifest.txt -C %STATE_TEMP_BIN% .

rmdir %STATE_TEMP% /s/q