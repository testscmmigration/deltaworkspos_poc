@rem This script should be called from %DW_DEV%

@call ..\variables.bat

@cd %DW_SOURCE%\dw
@dir /b/s/a:-r *.java > %DW_DEV%\guifiles.txt

@cd %DW_DEV%
%JDK%\javac %DW_JAVAC_OPTIONS% @guifiles.txt

