@rem This script should be called from %DW_DEV%

@call ..\variables.bat

@rmdir %DW_CLASSES% /s/q
@mkdir %DW_CLASSES% 

@cd %DW_SOURCE%\dw
@dir /b/s *.java > %DW_DEV%\guifiles.txt

@cd %DW_DEV%
%JDK%\javac %DW_JAVAC_OPTIONS% @guifiles.txt

