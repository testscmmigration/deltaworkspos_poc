@rem The drive containing the development hierarchy
@set DW_DRIVE=C:

@rem The root of the development hierarchy
@set DW_ROOT=%DW_DRIVE%\WSADdev\DW6.0\DeltaWorksPOS

@rem Optional debugging flag or optimizing flag 
@set DW_DEBUG=

@rem Where you go to run code you just compiled
@set DW_DEV=%DW_ROOT%\dev

@rem Where jar files are kept 
@set DW_JARS=%DW_ROOT%\build\versioned

@rem The version number you want your dev copy to claim to be
@set DW_VERSION=550

@rem Where the com and xml folders are
@set DW_SOURCE=%DW_ROOT%\src

@rem Where class files are kept
@set DW_CLASSES=%DW_ROOT%\bin

@rem Other Jar files we need
@set MISC_JARS=%DW_JARS%\comm.jar;%DW_JARS%\jdun.jar;

@rem Where the javac and jar commands are
@set JDK=C:\j2sdk1.4.2_07\bin

@rem Where the PVCS command-line tools are
@set PVCS=C:\Program Files\PVCS\VM\Win32\Bin

pause

@rem Typical compile options
@set DW_JAVAC_OPTIONS= %DW_DEBUG% -d %DW_CLASSES% -sourcepath %DW_SOURCE% -classpath %DW_CLASSES%;%MISC_JARS%

@rem Typical runtime options (to run code you just compiled)
@rem Note: includes dw_classes and dw_source instead of dwGui.jar and dwXml.jar
@rem so you don't have to regenerate jar files on every compile.
@rem (add -Xrunhprof:cpu=samples for profiling) -XX:+DisableExplicitGC -Xrunhprof
@set DW_JAVA_OPTIONS=-classpath %DW_CLASSES%;%DW_SOURCE%;%MISC_JARS% -Dsun.java2d.noddraw=true

