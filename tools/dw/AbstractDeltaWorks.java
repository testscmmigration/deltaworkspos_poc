package dw;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;
import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.plaf.ButtonUI;
import javax.swing.plaf.LabelUI;
import javax.swing.plaf.basic.BasicButtonUI;
import javax.swing.plaf.basic.BasicLabelUI;

import dw.comm.AgentConnect;
import dw.dwgui.DWButton;
import dw.dwgui.DWLabel;
import dw.dwgui.DWScrollPane;
import dw.framework.XmlDefinedPanel;
import dw.install.InstallFlowPage;
import dw.install.InstallTransaction;
import dw.io.State;
import dw.main.DeltaworksMainPanel;
import dw.main.DeltaworksStartup;
import dw.mgsend.MoneyGramSendTransaction;
import dw.profile.UnitProfile;
import dw.simplepm.SimplePMTransaction;
import dw.utility.Debug;
import dw.utility.ReceiptTemplates;

public abstract class AbstractDeltaWorks {
	
	private final static String[] LANGUAGES = new String[] {"en", "es", "fr", "de", "cn", "pl"}; 
	private static InstallTransaction it;
	private static InstallFlowPage ifp;
	private static String currentProfile = null;
	private static Object wait;

	static {
		AgentConnect.enableDebugEncryption();
	}
	
	//==========================================================================
	
	public interface ErrorCallBack {
		public void textTruncated(String textProperity, String fullText, String truncatedText) throws Exception;
	}
	
	protected static class CheckButtonUI extends BasicButtonUI {
		ErrorCallBack ecb;
    	static Set set = new HashSet();
    	
        public CheckButtonUI(ErrorCallBack ecb) {
        	this.ecb = ecb;
		}
        
        static private void clearButtonSet() {
        	set.clear();
        }

        protected void paintText(Graphics g, AbstractButton b, Rectangle textRect, String text) {
        	try {
	        	super.paintText(g, b, textRect, text);
	        	if ((! b.getText().equals(text)) && (! set.contains(b))) {
	            	String textProperity = ((DWButton) b).getTextProperty();
	        		ecb.textTruncated(textProperity, b.getText(), text);
	            	set.add(b);
	        	}
        	} catch (Exception e) {
        		System.out.println("Exception: " + e.getMessage());
        		e.printStackTrace();
        	}
        }
	}
	
    protected static class CheckLabelUI extends BasicLabelUI {
		ErrorCallBack ecb;
    	static Set set = new HashSet();
    	
        public CheckLabelUI(ErrorCallBack ecb) {
        	this.ecb = ecb;
		}

        static private void clearLabelSet() {
        	set.clear();
        }

		protected String layoutCL(JLabel label, FontMetrics fontMetrics, String text, Icon icon, Rectangle viewR, Rectangle iconR, Rectangle textR) {
//			System.out.println("CheckLabelUI.layoutCL called...");
            String s = super.layoutCL(label, fontMetrics, text, icon, viewR, iconR, textR);
            if (label instanceof DWLabel) {
				try {
	            	String textProperity = ((DWLabel) label).getTextProperty();
		            if ((! text.equals(s)) && (! set.contains(label))) {
			            if (! set.contains(label)) {
			        		ecb.textTruncated(textProperity, text, s);
			            	set.add(label);
			            }
		            }
		    	} catch (Exception e) {
		    		System.out.println("Exception: " + e.getMessage());
		    		e.printStackTrace();
		    	}
            }
            return s;
        }
    }
    
//    protected static class CheckTextAreaUI extends BasicTextAreaUI {
//    	
//    	String page;
//    	String language;
//    	Set set = new HashSet();
//    	
//        public CheckTextAreaUI(String page, String language) {
//        	this.page = page;
//        	this.language = language;
//		}
//    }
    
	protected class ScreenPage implements Comparable {
		private String directory;
		private String base;
		private String page;
		
		public int compareTo(Object o) {
			return this.page.compareTo(((ScreenPage) o).page);
		}
		
		public String toString() {
			return page + " (" + directory + ")";
		}

		public String getDirectory() {
			return directory;
		}

		public String getBase() {
			return base;
		}

		public String getPage() {
			return page;
		}
	}
	
	//==========================================================================
	
	protected List getScreenPages(String directory, final String basePageXml) {
		return getScreenPages(directory, null, basePageXml, null);
	}

	protected List getScreenPages(String directory, final String prefix, final String basePageXml) {
		return getScreenPages(directory, prefix, basePageXml, null);
	}	
	
	protected List getScreenPages(String directory, final String prefix, final String basePageXml, final Set exclude) {
		List list = new ArrayList();
		
		String directoryName = "../src/xml/" + directory;
		File directoryFile = new File(directoryName);

//		String baseXml = null;

//		if (useBasePage) {
//			String[] base = directoryFile.list(new FilenameFilter() {
//				public boolean accept(File dir, String name) {
//					boolean prefixFlag = (prefix == null) || name.startsWith(prefix); 
//					return prefixFlag && name.endsWith("Panel.xml");
//				}
//			});
//			
//			if (base.length != 1) {
//				return list;
//			}
//			
//			baseXml = base[0];
//		}
		
		String[] fileNames = directoryFile.list(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				boolean prefixFlag = (prefix == null) || name.startsWith(prefix); 
				boolean pageFlag = (basePageXml == null) || ((basePageXml != null) && (! name.equals(basePageXml)));
				boolean excludeFlag = exclude != null ? exclude.contains(name) : false;
				return (prefixFlag && pageFlag && (! excludeFlag) && name.endsWith(".xml")); 
			}
		});
		
		for (int j = 0; j < fileNames.length; j++) {
			ScreenPage dp = new ScreenPage();
			dp.directory = directory;
			dp.base = basePageXml;
			dp.page = fileNames[j];
			list.add(dp);
		}
		
		return list;
	}
	
	protected PrintWriter openCsvFile(String fileName) throws Exception {
		return new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)));
	}
	
	private static String csvFormat(String value) {
		//value.replaceAll("\"", "\"\"");
		return "\"" + value +"\"";
	}
	
	private static void writeCsvLine(OutputStreamWriter pw, String directory, String page, String language, String properity, String fullText, String truncatedText, String englishText) throws Exception {
		pw.write(csvFormat(directory));
		pw.write(",");
		pw.write(csvFormat(page));
		pw.write(",");
		pw.write(csvFormat(language));
		pw.write(",");
		pw.write(csvFormat(properity));
		pw.write(",");
		pw.write(csvFormat(fullText));
		pw.write(",");
		pw.write(csvFormat(truncatedText));
		pw.write(",");
		pw.write(csvFormat(englishText));
		pw.write("\n");
	}
	
	private int textTruncationCount = 0;
	
//	public int checkStringLengths(String directory, final String prefix, boolean useBasePage) {
//
//		String baseXml = null;
//		
//		if (useBasePage) {
//			
//			String directoryName = "../src/xml/" + directory;
//			File directoryFile = new File(directoryName);
//
//			String[] base = directoryFile.list(new FilenameFilter() {
//				public boolean accept(File dir, String name) {
//					boolean prefixFlag = (prefix == null) || name.startsWith(prefix); 
//					return prefixFlag && name.endsWith("Panel.xml");
//				}
//			});
//			
//			if (base.length == 1) {
//				baseXml = base[0];
//			}
//		}
//
//		return checkStringLengths(directory, prefix, baseXml);
//	}

	public int checkStringLengths(List screenPages) {
		textTruncationCount = 0;
		
		// Get English values.
		
		try {
			final Properties englishTextProperties = new Properties();
			FileInputStream fis = new FileInputStream("../src/xml/i18n/deltaworks_en.properties");
			englishTextProperties.load(fis);
			
	        final JFrame frame = new JFrame();

	        if (((ScreenPage) screenPages.get(0)).getDirectory().equals("simplepm")) {
				Debug.setDebug(false);
				this.loadProfile("Q2", "80004968", "5865", "dfg");
				SimplePMTransaction.setCurrentInstance(new SimplePMTransaction("Test"));
				DeltaworksMainPanel dwmp = DeltaworksMainPanel.create(frame);
//				UnitProfile.getInstance().getProfileInstance().add(new ProfileItem("ITEM", "PRODUCT[96]/NAME_PIN_REQUIRED", "S", "1", null));
//				UnitProfile.getInstance().getProfileInstance().add(new ProfileItem("ITEM", "PRODUCT[96]/KEYBOARD_ISSUE", "S", "1", null));
//				UnitProfile.getInstance().getProfileInstance().add(new ProfileItem("ITEM", "PRODUCT[96]/AMOUNT_TENDERED", "S", "1", null));
//				UnitProfile.getInstance().getProfileInstance().add(new ProfileItem("ITEM", "PRODUCT[96]/MAX_AMOUNT_PER_ITEM", "S", "1", null));
//				UnitProfile.getInstance().getProfileInstance().add(new ProfileItem("ITEM", "PRODUCT[96]/MAX_BATCH_COUNT", "S", "1", null));
//				UnitProfile.getInstance().getProfileInstance().add(new ProfileItem("ITEM", "PRODUCT[96]/MIN_AMOUNT_PER_ITEM", "S", "1", null));
//				UnitProfile.getInstance().getProfileInstance().add(new ProfileItem("ITEM", "PRODUCT[96]/DOCUMENT_NAME", "S", "1", null));
			}
			
			DeltaworksStartup.fixFonts();
	        Container pane = frame.getContentPane();
	        
	        frame.setResizable(false);
	        frame.setSize(650, 530);
	        
	        Properties screenNames = new Properties();
	        fis = new FileInputStream("../src/xml/i18n/deltaworks_screenmap.properties");
	        screenNames.load(fis);
	        
	        FileOutputStream fos = new FileOutputStream("LanguageTruncations.csv");
	        final OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-16");
	        
//	        System.out.println("Number of screens = " + screenPages.size());
	        
	        for (int i = 0; i < screenPages.size(); i++) {
	        	final ScreenPage sp = (ScreenPage) screenPages.get(i);
				for (int j = 0; j < LANGUAGES.length; j++) {
					final String language = LANGUAGES[j];
//			    	System.out.println("i, directory, page, language = " + i + ", " + sp.getDirectory() + ", " + sp.getPage() + ", " + language);
					
					int l = sp.getPage().length() - 4;
		            String title = screenNames.getProperty(sp.getPage().substring(0, l)) + " (" + language + ")";

					UnitProfile.getInstance().setLanguage(language);
			    	XmlDefinedPanel mainPanel = null;
			    	if (sp.getBase() != null) {
			    		mainPanel = new XmlDefinedPanel(sp.getDirectory() + "/" + sp.getBase());			    		
			    	}
			    	XmlDefinedPanel pagePanel = new XmlDefinedPanel(sp.getDirectory() + "/" + sp.getPage());
			    	if (mainPanel != null) {
			    		if (sp.getDirectory().equals("softwareversions")) {
			    			DWScrollPane scrollPane = (DWScrollPane) mainPanel.getComponent("outputScrollPane");
			    			scrollPane.getViewport().add(pagePanel);
			    		} else if (sp.getDirectory().equals("simplepm")) {
			    			JPanel panel = (JPanel) mainPanel.getComponent("centerPanel");
				    		panel.add(pagePanel, "String Length Check");
			    		} else {
			    			JPanel panel = (JPanel) mainPanel.getComponent("pageFlowPanel");
				    		panel.add(pagePanel, "String Length Check");
			    		}
			    	} 
			    	
				    ErrorCallBack ecb = new ErrorCallBack() {
						public void textTruncated(String textProperity, String fullText, String truncatedText) throws Exception {
			            	System.out.println("Property: " + textProperity);
			            	System.out.println("Page: " + sp.getPage() + " (" + language + ") - Length before = " + fullText.length() + ", Length after = " + truncatedText.length());
			            	System.out.println("Original: " + fullText);
			            	System.out.println("Displayed: " + truncatedText);
			            	String englishText = englishTextProperties.getProperty(textProperity);
			            	System.out.println("English: " + englishText);
			            	System.out.println();
			            	AbstractDeltaWorks.writeCsvLine(osw, sp.getDirectory(), sp.getPage(), language, textProperity, fullText, truncatedText, englishText);
			            	textTruncationCount++;
						}
				    };

				    LabelUI labelUI = new CheckLabelUI(ecb);
				    ButtonUI buttonUI = new CheckButtonUI(ecb);
				    setCheckUI(pagePanel, labelUI, buttonUI);
			
				    if (! sp.getDirectory().equals("dialogs")) {
						pane.removeAll();
						if (mainPanel != null) {
							pane.add(mainPanel);
							mainPanel.invalidate();
						} else {
							pane.add(pagePanel);
							pagePanel.invalidate();
						}
						pane.validate();
						pane.repaint();
						frame.setTitle(title);
						frame.setVisible(true);

						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						frame.setVisible(false);
				    } else {
				    	if (! frame.isShowing()) {
							frame.setVisible(true);
				    	}
			            JOptionPane dp = new JOptionPane(pagePanel, JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION,  null);
			            JDialog dialog = dp.createDialog(frame, title);
			            dialog.setModal(false);
//			            System.out.println("Size = " + pagePanel.getPreferredSize());
//			            dialog.setSize(frame.getPreferredSize());
						dialog.setVisible(true);
						
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						dialog.setVisible(false);
				    }
				}
	        }
	        
	    	if (frame.isShowing()) {
				frame.setVisible(false);
	    	}
	        
	    	osw.flush();
	    	osw.close();
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			e.printStackTrace();
		}
        
        return textTruncationCount;
	}	
	
	private JScrollPane getScrollPane(Container parent) {
    	Component[] components = parent.getComponents();
    	for (int i = 0; i < components.length; i++) {
    		Component c = components[i];
    		if (c instanceof JScrollPane) {
    			return ((JScrollPane) c);
    		}		
    		
    		if (c instanceof Container) {
    			JScrollPane sp = getScrollPane((Container) c);
    			if (sp != null) {
    				return sp;
    			}
    		}
    	}
    	return null;
	}

	public int checkStringLengths(String directory) {
		return checkStringLengths(directory, null, null);
	}

	public int checkStringLengths(String directory, String basePage) {
		return checkStringLengths(directory, null, basePage);
	}
	
	public int checkStringLengths(String directory, String prefix, String basePage) {
        List screenPages = this.getScreenPages(directory, prefix, basePage);
        return checkStringLengths(screenPages);
	}
	
	
    protected void setCheckUI(Container parent, LabelUI labelUI, ButtonUI buttonUI) {
    	Component[] components = parent.getComponents();
    	for (int i = 0; i < components.length; i++) {
    		Component c = components[i];
    		if (c instanceof JLabel) {
    			JLabel l = (JLabel) c;
//        		System.out.println("Label = " + l.getText() + ", " + l.getPreferredSize().getWidth() + "x" + l.getPreferredSize().getHeight());
    			l.setUI(labelUI);
    		} else if (c instanceof JButton) {
    			JButton b = (JButton) c;
    			b.setUI(buttonUI);
    		}
    		c.setVisible(true);
    		if (c instanceof Container) {
    			setCheckUI((Container) c, labelUI, buttonUI);
    		}
    	}
    }
	
	protected String getEnvironmentUrl(String environment) {
		return "https://" + environment.toLowerCase() + "ws.qa.moneygram.com/ac2/services/";
	}
	
	protected String getReAuthUrl(String environment) {
		return "https://" + environment.toLowerCase() + "ws.qa.moneygram.com/ac2web/servlet/ReauthServlet";
	}
	
	protected void loadDemoProfile() throws Exception {
		
		if (currentProfile.endsWith("DEMO")) {
			return;
		}
		
		init();
		
        wait = new Object();
		it.getInitialProfile("DEMO", "DEMO", ifp);
		
        synchronized(wait) {
        	try {
        		wait.wait();
        	} catch (Exception e) {
        	}
        }
        
        currentProfile = "DEMO";
	}
	
	protected void loadProfile(String environment, String deviceId, String profileId, String password) throws Exception {
		
		if ((currentProfile != null) && (currentProfile.equals(deviceId))) {
			return;
		}
		
    	deleteTemporaryFiles();
    	
    	init();
    	
    	// Re-auth the profile Id.
		
		reAuth(profileId, getReAuthUrl(environment));

		// Load the profile for the specific device Id.
		
		wait = new Object();
		it.setAlternateUrl(getEnvironmentUrl(environment));
		it.getInitialProfile(deviceId, password, ifp);
		
        synchronized(wait) {
        	try {
        		wait.wait();
        	} catch (Exception e) {
        	}
        }
        
        // Perform a transmit.
        
        transmit();

    	currentProfile = deviceId;
    	
//    	System.out.println("Device Id = " + UnitProfile.getInstance().get("DEVICE_ID", ""));
	}
	
	protected DeltaworksMainPanel startDeltaWorks() throws Exception {
    	DeltaworksStartup.junitSetup();
        JFrame f = new JFrame();
        f.getContentPane().setLayout(new BorderLayout());
        return DeltaworksMainPanel.create(f);
	}
	
	protected Properties getPropertiesFile(String fileName) throws Exception {
		Properties properties = new Properties();
		FileInputStream fis = new FileInputStream(fileName);
		properties.load(fis);
		return properties;
	}
	
	private synchronized void init() throws Exception {
		
		if (it == null) {
	    	State state = State.getInstance();
	    	state.createProfile();

			it = new InstallTransaction("TEST");
			
			ifp = new InstallFlowPage(it, "install/AgentSetupPage1.xml", "", "", null) {
				public void start(int direction) {
				}
				public void finish(int direction) {
				}
			    public void commComplete(int commTag, Object returnValue) {
			        synchronized(wait) {
			        	wait.notifyAll();
			        }
			    }
			};
		}
	}
	
	private boolean reAuth(String profileId, String url) throws Exception {
		
		// https://q2ws.qa.moneygram.com/ac2web/servlet/ReauthServlet?UNIT_PROFILE_ID=5865&ID_TYPE=POS_ID
		
		URL obj = new URL(url);
		
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", "Mozilla/5.0");
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
 
		String urlParameters = "UNIT_PROFILE_ID=" + profileId + "&ID_TYPE=POS_ID";
 
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
 
		int responseCode = con.getResponseCode();
		InputStream is = con.getInputStream();
		InputStreamReader isr = new InputStreamReader(con.getInputStream());
		BufferedReader in = new BufferedReader(isr);
		String line;
 
		StringBuffer response = new StringBuffer(); 
		while ((line = in.readLine()) != null) {
//			System.out.println(line);
			response.append(line);
		}
		in.close();
		
		String searchText = "Unit Profile ID " + profileId.trim() + " re-authorized";
		return (response.toString().indexOf(searchText) != -1);
	}
	
	private void transmit() throws Exception {
		DeltaworksMainPanel dwmp = startDeltaWorks();
		ReceiptTemplates.initialize();
		MoneyGramSendTransaction.populateSendReceiptLanguageList();
		UnitProfile.getInstance().setSoftwareVersionPending(false);
		dwmp.junitTransmitProcess();
	}
	
	private void deleteTemporaryFiles() throws Exception {
		State.closeProfilefile();
		
		File directory = new File(System.getProperty("user.dir"));
		File[] list = directory.listFiles();
		boolean flag = true;
		for (int i = 0; i < list.length; i++) {
			File file = list[i];
			String fileName = file.getName().toLowerCase();
			
			if (fileName.equals("codetables.xml") || 
					fileName.equals("cirtcele.xml") ||
					fileName.equals("elbac.xml") ||
					fileName.equals("enohp.xml") ||
					fileName.equals("profile.dat") ||
					fileName.equals("retaw.xml") ||
					fileName.equals("sag.xml") ||
					fileName.equals("translations.xml") ||
					fileName.equals("sreq.xml") ||
					fileName.equals("breq.xml") ||
					fileName.equals("print.txt") ||
					fileName.equals("adp.obj") ||
					fileName.endsWith(".rt") || 
					fileName.endsWith(".st") || 
					fileName.endsWith(".ri") || 
					fileName.endsWith(".sr") || 
					(fileName.startsWith("00.") && fileName.endsWith(".xml")) || 
					(fileName.startsWith("01.") && fileName.endsWith(".xml")) || 
					(fileName.startsWith("02.") && fileName.endsWith(".xml")) || 
					(fileName.startsWith("03.") && fileName.endsWith(".xml")) || 
					(fileName.startsWith("trace") && fileName.endsWith(".txt") && (fileName.length() == 10))) {
//				System.out.println("*** DELETE " + fileName);
				file.delete();
				if (file.exists()) {
					flag = false;
					System.out.println("*** File " + fileName + " not deleted");
				}
			}
		}
		if (! flag) {
			System.out.println("*** Not all file were deleted.");
		}
	}
}
