/*
 * StateCommand.java
 * 
 * $Revision$
 *
 * Copyright (c) 2001-2004 MoneyGram International
 */

package dw.io;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.moneygram.agentconnect.ComplianceTransactionRequest;

import dw.comm.AgentConnect;
import dw.io.report.ReportLog;
import dw.io.report.UnifiedReportDetail;
import dw.profile.Profile;
import dw.profile.ProfileLoader;
import dw.profile.UnitProfile;
import dw.utility.Debug;
import dw.utility.Total;

/**
 * Allows developers to decrypt files and view state data.
 */
public class StateCommand extends State {
   
    private StateCommand() throws IOException {
        super();
    }

    private StateCommand(String filename) throws IOException {
        super(filename);
    }
    
    private static void reset()
                throws FileNotFoundException, IOException {
        State state = new State();
        //state.writeLong(0L, LAST_CHECK_IN);
        state.writeLong(0L, CIRCULAR_PROFILE);
        state.writeLong(0L, CIRCULAR_PROFILE + 8);
        state.writeLong(0L, CIRCULAR_ITEM);
        state.writeLong(0L, CIRCULAR_ITEM + 8);
//        state.writeLong(0L, CIRCULAR_DIAG);
//        state.writeLong(0L, CIRCULAR_DIAG + 8);
        state.writeLong(0L, CIRCULAR_REPORT);
        state.writeLong(0L, CIRCULAR_REPORT + 8);
//        state.writeLong(0L, CIRCULAR_DIAG_REPORT);
//        state.writeLong(0L, CIRCULAR_DIAG_REPORT + 8);
        state.writeLong(0L, CIRCULAR_ITEM_BAK);
        state.writeLong(0L, CIRCULAR_ITEM_BAK + 8);
//        state.writeLong(0L, CIRCULAR_DIAG_BAK);
//        state.writeLong(0L, CIRCULAR_DIAG_BAK + 8);
        state.writeLong(0L, CIRCULAR_TEMP);
        state.writeLong(0L, CIRCULAR_TEMP + 8);
    }

    private static void fullReset() throws FileNotFoundException, IOException {
        StateCommand.reset();
        State state = new State();
        state.writeLong(0L, CIRCULAR_PROFILE + 8);
    }
    
    private static void notransmit() throws FileNotFoundException, IOException {
        State state = new State();
        state.writeLong(0L, LAST_DAILY_XMIT);
        state.writeLong(0L, LAST_ATTEMPTED_DAILY_XMIT);
    }

    private static void awol() throws FileNotFoundException, IOException {
        State state = new State();
        // can't use zero here because it is treated specially in getLastReauthTime()
        state.writeLong(1L, LAST_REAUTH);
    }

    private static void faketransmit() throws FileNotFoundException, IOException {
        State state = new State();
        long t = System.currentTimeMillis();
        // don't change last_check_in because it causes deauth error
        // state.writeLong(t, LAST_CHECK_IN);              // fake a check-in
        state.writeLong(t, LAST_DAILY_XMIT);            // fake a transmit (sucess)
        state.writeLong(t, LAST_ATTEMPTED_DAILY_XMIT);  // fake a transmit (attempt)
        state.writeLong(t, LAST_REAUTH);                // clear the AWOL
        
        state.createProfile();
        UnitProfile.getInstance().set("POS_STATUS", "A");
        UnitProfile.getInstance().set("SA_RESTRICT_STATUS", "A");
        UnitProfile.getInstance().saveChanges();
    }

    private static void dummy(String filename, int length)
                throws FileNotFoundException, IOException {

        State state = new State();
        StateCommand.fullReset();
        CircularFile out = state.createCircularFile(
                "profile.dat", CIRCULAR_PROFILE, 128, 0);
    
        FileInputStream in = new FileInputStream(filename);
        byte[] buffer = new byte[256];
        int written = 0;

        while (true) {
            int bytesRead = in.read(buffer);
            if (bytesRead == -1) break;
            out.write(buffer, 0, bytesRead);
            written += bytesRead;
        }
        PrintStream ps = new PrintStream(out);
        while (written < length * 1024) {
            ps.println("                              ");
            written += 32;
        }

        ps.close();
    }


    // just for debug purposes
    private Total getTotal(int index) {
        return new Total(this, TOTAL_START + 
                index * State.BATCH_TOTAL_LENGTH);
    }

    private static void showValues() throws IOException {
	StateCommand state = new StateCommand();
	System.out.println("showValues"+Debug.class);
	byte b;
	int i;
	long n, m;
        //byte[] md = new byte[21];

        n = state.readLong(LAST_CHECK_IN);
        Debug.println("Last check in = " + n 
                + " (" + new java.util.Date(n) + ")");

        n = state.readLong(LAST_DAILY_XMIT);
        Debug.println("Last daily xmit = " + n 
                + " (" + new java.util.Date(n) + ")");

        n = state.readLong(LAST_ATTEMPTED_DAILY_XMIT);
        Debug.println("Last attempted daily xmit = " + n 
                + " (" + new java.util.Date(n) + ")");

        n = state.readLong(LAST_REAUTH);
        Debug.println("Last reauth = " + n 
                + " (" + new java.util.Date(n) + ")");

        n = state.readLong(LAST_DAILY_TOTAL_RESET);
        Debug.println("Last daily total reset = " + n 
                + " (" + new java.util.Date(n) + ")");
        n = state.readLong(LAST_DAILY_BILL_PAY_TOTAL_RESET);
        Debug.println("Last daily Bill Pay total reset = " + n 
                + " (" + new java.util.Date(n) + ")");                
        b = state.readByte(CURRENT_PERIOD);
        Debug.println("Current period = " + b);
        
        i = state.readInt(State.SUPERAGENT_PROFILE_ID);
        Debug.println("Superagent profile = " + i);

    	n = state.readLong(DISPENSER_1);
    	Debug.println("Starting serial = " + n + " (w/o check digit)");
    
    	m = state.readLong(DISPENSER_1 + 8);
    	Debug.println("Ending serial   = " + (n + m));
    
    	n = state.readLong(CIRCULAR_PROFILE);
    	Debug.println("Profile start = " + n);
    
    	n = state.readLong(CIRCULAR_PROFILE + 8);
    	Debug.println("Profile end = " + n);

        //state.stateFile.read(md, 0, 21, CIRCULAR_PROFILE + 16);
        //Debug.println(hex(md));
        
    	n = state.readLong(CIRCULAR_WAREHOUSE);
    	Debug.println("Warehouse start = " + n);
    	
    	n = state.readLong(CIRCULAR_WAREHOUSE + 8);
    	Debug.println("Warehouse end = " + n);

        Debug.println("Used ranges: ");
        for (i = 16; i < 96; i += 16) {
            Debug.println("    " + 
                    state.readLong(DISPENSER_1 + i) +
                    " .. " + 
                    state.readLong(DISPENSER_1 + 8 + i));
        }

        Debug.println("Daily total:         " + state.getTotal(0));
        Debug.println("Batch total MO:      " + state.getTotal(1));
        Debug.println("Batch total VP:      " + state.getTotal(2));
        Debug.println("Batch total GC:      " + state.getTotal(3));
        Debug.println("Batch total DG:      " + state.getTotal(4));
        Debug.println("Diag summary total:  " + state.getTotal(12));
        Debug.println("Diag summary MO:     " + state.getTotal(13));
        Debug.println("Diag summary VP:     " + state.getTotal(14));
        Debug.println("Diag summary MGS:    " + state.getTotal(15));
        Debug.println("Diag summary MGR:    " + state.getTotal(16));
        Debug.println("Diag summary DG:     " + state.getTotal(17));
        Debug.println("Diag summary XP:     " + state.getTotal(18));
        Debug.println("Diag summary void:   " + state.getTotal(19));
        Debug.println("Diag summary limbo:  " + state.getTotal(20));
        Debug.println("Diag summary fee:    " + state.getTotal(11));
        Debug.println("Bill Pay daily total:    " + state.getTotal(50));
    }

    private static void setPeriod(String p) throws IOException {
	State state = new StateCommand();
        byte period = Byte.parseByte(p);
        state.writeByte(period, CURRENT_PERIOD);
        Debug.println("Current period = " + period);
    }
    
    private static void predictValues() throws NoSuchAlgorithmException {
        MessageDigest digest;
        byte[] md;
        for (int k = 0; k < key2.length; k++) {
            digest = MessageDigest.getInstance("SHA");
            digest.update(key2[k]);
            md = digest.digest();
            Debug.println(code(md));
        }
    }

    public static String code(byte[] data) {
        String result = "{";
        int i;

        for (i = 0; i < data.length; i++) {
            if (i != 0)
                result += ",";
            result += " (byte) 0x";
            result += Integer.toHexString((data[i] >> 4) & 0x0F);
            result += Integer.toHexString(data[i] & 0x0F);
        }
        result += "}";
        return result;
    }
    
    private static void setSerialNumbers(String start, String end) 
	    throws IOException {
	State state = new StateCommand();
        long s, e;
        s = Long.parseLong(start);
        e = Long.parseLong(end);
	state.writeLong(s, DISPENSER_1);
	state.writeLong(e - s, DISPENSER_1 + 8);
    }

    private static void setCheckin(long millis) throws IOException {
        State state = new StateCommand();
        state.writeLong(millis, LAST_CHECK_IN);
    }

    public static State warehouse(String filename) 
        throws UnsupportedEncodingException, IOException, 
        org.xml.sax.SAXException,
        javax.xml.parsers.ParserConfigurationException
    {
        //Profile factoryProfile = getFactoryProfile();

        FileInputStream input2 = new FileInputStream(filename);
        ProfileLoader loader = new ProfileLoader();
        Profile warehouseProfile = loader.load(input2, false);
        //Debug.println(warehouseProfile);

        //warehouseProfile.merge(factoryProfile);
        //Debug.println(warehouseProfile);

        // Doubled the file size since we were overwritting data with the size
        // set to 16.
        fill("state.dat", 32);
        
        StateCommand state = new StateCommand("state.dat");
        state.resetEverything();
        // This was set to the wrong start value it should be 0.
        CircularFile cf = new CircularFileImpl(state.stateFile, 
                0, WAREHOUSE_START); 
        byte[] buffer = warehouseProfile.toString().getBytes("Cp1252");
        cf.write(buffer);
        cf.flush();
        state.stateFile.writeLong(WAREHOUSE_START, CIRCULAR_WAREHOUSE);
        state.stateFile.writeLong(WAREHOUSE_START + buffer.length, 
                CIRCULAR_WAREHOUSE + 8);

        return state;
    }

    private static void easyTest() throws IOException {
	State state = new StateCommand();
	int b1, b2;
	for (b1 = 0; b1 < 128; b1++ ) {
	    state.writeByte(b1, CURRENT_PERIOD);
	    b2 = state.readByte(CURRENT_PERIOD);
	    if (b1 != b2) { 
		Debug.println(b1 + " != " + b2);
	    }
	    else {
		Debug.println("" + b1);
	    }
	}
    }

    private static void exportFile(String filename, int key) 
            throws IOException {
        exportFile(filename, key, key);
    }

	private static void exportFile(String filename, int n, int m)
			throws IOException {
		State state = new State();
		long fp = CIRCULAR_PROFILE + (CIRCULAR_ITEM - CIRCULAR_PROFILE) * n;
		long start = state.stateFile.readLong(fp);
		long end = state.stateFile.readLong(fp + 8);
		// Debug.println("start = " + start + " end = " + end);
		CircularFile cf = new CircularFileImpl(
				new EncryptedRandomAccessFile(filename, "r", 
						decodeLong(key1[m])), 
				start, end);
		CircularInputStream cis = cf.getInputStream();
		// Debug.println("current = " + cis.getCurrent()
		// + " mark = " + cis.getMark());
		copyStream(cis, System.out);
	}

	private static void exportEntireCciFile(String filename, int key)
			throws IOException {
		exportEntireCciFile(filename, key, key);
	}

	private static void exportEntireCciFile(String filename, int n, int m)
			throws IOException {
		State state = new State();
		long fp = CIRCULAR_PROFILE + (CIRCULAR_ITEM - CIRCULAR_PROFILE) * n;
		long start = state.stateFile.readLong(fp);
		long end = 2097052;
		
		AgentConnect.enableDebugEncryption();
		CircularFile cf = new CircularFileImpl(new EncryptedRandomAccessFile(
				filename, "r", decodeLong(key1[m])), start, end);
		CircularInputStream cciInputStream = cf.getInputStream();

		try {
			while (true) {
				ComplianceTransactionRequest ctri = null;
				UnifiedReportDetail urd = new UnifiedReportDetail();
				urd.readFrom(new DataInputStream(cciInputStream));
				final ComplianceTransactionRequest cciItem = urd
						.ctriDetail();
				System.out.println("------------ComplianceTransactionRequest---------------");
				System.out.println(cciItem.toString());
				System.out.println("-----------ComplianceTransactionRequest  End----------------");
			}
		} catch (IOException ioe) {
            Debug.printStackTrace(ioe);
		}

	}

    private static void loadProfile(String filename) 
        throws UnsupportedEncodingException, IOException, 
        org.xml.sax.SAXException, 
        javax.xml.parsers.ParserConfigurationException
    {
        State state = new State();
        //Profile factoryProfile = getFactoryProfile();

        FileInputStream input = new FileInputStream(filename);
        ProfileLoader loader = new ProfileLoader();
        Profile newProfile = loader.load(input, false);
        //newProfile.merge(factoryProfile);

        //ProfileItem appMode = newProfile.find("APPLICATION_MODE");
        //appMode.setValue("N");
        state.writeLong(0L, CIRCULAR_PROFILE);
        state.writeLong(0L, CIRCULAR_PROFILE + 8);
        state.write(md[0],  CIRCULAR_PROFILE + 16);

        CircularFile cf = state.createProfileFile();
        byte[] buffer = newProfile.toString().getBytes("Cp1252");
        cf.write(buffer);
        cf.flush();
        state.flush();
        state.close();
    }
        
    /** Sets the start and end pointers on all the circular files to 
      * zero, thereby clearing all data. The profile file is excluded
      * and left untouched.
      */
    public void clearLogs() throws IOException {
        stateFile.writeLong(0L, CIRCULAR_ITEM);
        stateFile.writeLong(0L, CIRCULAR_ITEM + 8);
        stateFile.write(md[1],  CIRCULAR_ITEM + 16);
        stateFile.writeLong(0L, CIRCULAR_ITEM_BAK);
        stateFile.writeLong(0L, CIRCULAR_ITEM_BAK + 8);
        stateFile.write(md[5],  CIRCULAR_ITEM_BAK + 16);	    
//        stateFile.writeLong(0L, CIRCULAR_DIAG);
//        stateFile.writeLong(0L, CIRCULAR_DIAG + 8);
//        stateFile.write(md[2],  CIRCULAR_DIAG + 16);
//        stateFile.writeLong(0L, CIRCULAR_DIAG_BAK);
//        stateFile.writeLong(0L, CIRCULAR_DIAG_BAK + 8);
//        stateFile.write(md[6],  CIRCULAR_DIAG_BAK + 16);

        stateFile.writeLong(0L, CIRCULAR_REPORT);
        stateFile.writeLong(0L, CIRCULAR_REPORT + 8);
        stateFile.write(md[3],  CIRCULAR_REPORT + 16);
//        stateFile.writeLong(0L, CIRCULAR_DIAG_REPORT);
//        stateFile.writeLong(0L, CIRCULAR_DIAG_REPORT + 8);        
//        stateFile.write(md[4],  CIRCULAR_DIAG_REPORT + 16);

        stateFile.writeLong(0L, CIRCULAR_TEMP);
        stateFile.writeLong(0L, CIRCULAR_TEMP + 8);
        stateFile.write(md[7],  CIRCULAR_TEMP + 16);

        stateFile.flush();
    }

    private static void clear() throws FileNotFoundException, IOException {
        State state = new StateCommand();
        //UnitProfile.getUPInstance().setStateSaver(state);
        state.clearLogs();
    }

    private static void zero() throws IOException {
        State state = new StateCommand();
        state.zeroDispenser();
    }

    public static void usage() {
        Debug.println("usage: state fill filename size");
        Debug.println("       state clearlogs");
        Debug.println("       state zero");
        Debug.println("       state warehouse warehouse.xml");
        Debug.println("       state profile profile.xml");
        Debug.println("       state print filename.dat");
        Debug.println("       state export filename key");
        Debug.println("       state debug");
        Debug.println("       state period n");
        Debug.println("       state predict");
        Debug.println("       state checkin");
        Debug.println("       state serial start end");
        Debug.println("       state notransmit");
        Debug.println("       state awol");
        Debug.println("       state faketransmit");
    }
        
    public static void main(String[] args) {
        try {
            if (args.length < 1) {
                usage();
            }
            else if (args[0].equals("fill")) {
                fill(args[1], Integer.parseInt(args[2]));
            }
            else if (args[0].equals("clearlogs")) {
                clear();
            }
            else if (args[0].equals("zero")) {
                zero();
            }
            else if (args[0].equals("warehouse")) {
                warehouse(args[1]);
            }
            else if (args[0].equals("profile")) {
                loadProfile(args[1]);
            }
            else if (args[0].equals("print")) {
                if (args[1].equals("detail.dat")) {
                    ReportLog.printDetailRecords();
                }
                else if (args[1].equalsIgnoreCase("-debugEncryption")) 
               	   dw.comm.AgentConnect.enableDebugEncryption();
                else {
                    Debug.println("Sorry, not implemented");
                }
                
            }
            else if (args[0].equals("export")) {
                //Debug.println("args.length = " + args.length);
                if (args.length == 3)
                	if (args[1].compareToIgnoreCase("cci.dat") != 0) {
                		exportFile(args[1], Integer.parseInt(args[2]));
                	} else {
                		exportEntireCciFile(args[1], Integer.parseInt(args[2]));
                	}
                else {
                    //Debug.println(args[1] + " " + args[2] + " " + args[3]);
                	if (args[2].compareToIgnoreCase("cci.dat") != 0) {
                        exportFile(args[1], Integer.parseInt(args[2]),
                                Integer.parseInt(args[3]));
                	} else {
                		exportEntireCciFile(args[1], Integer.parseInt(args[2]),
                                Integer.parseInt(args[3]));
                	}
                }
            }
            else if (args[0].equals("debug")) {
                showValues();
            }
            else if (args[0].equals("period")) {
                setPeriod(args[1]);
            }
            else if (args[0].equals("predict")) {
                predictValues();
            }
            else if (args[0].equals("checkin")) {
                setCheckin(Long.parseLong(args[1]));
            }
	    else if (args[0].equals("serial")) {
		setSerialNumbers(args[1], args[2]);
	    }
	    else if (args[0].equals("test")) {
		easyTest();
	    }
            else if (args[0].equals("notransmit")) {
                notransmit();
            }
            else if (args[0].equals("awol")) {
                awol();
            }
            else if (args[0].equals("faketransmit")) {
                faketransmit();
            }
            else {
                usage();
            }
            
        }
        catch (Exception e) {
            Debug.println(e.getMessage());
            Debug.printStackTrace(e);
	}
    }

    private static final byte[][] key1 = {{
        (byte) 0x60, (byte) 0x14, (byte) 0x10, (byte) 0x3f,
        (byte) 0x82, (byte) 0x80, (byte) 0x37, (byte) 0xa8
    },{
        (byte) 0xb9, (byte) 0x0a, (byte) 0x8b, (byte) 0x60,
        (byte) 0x2c, (byte) 0xd8, (byte) 0x5f, (byte) 0xb4
    },{
        (byte) 0xf9, (byte) 0xcf, (byte) 0x24, (byte) 0xb9,
        (byte) 0xbd, (byte) 0xa4, (byte) 0x2e, (byte) 0x20
    },{
        (byte) 0xe2, (byte) 0xe8, (byte) 0x67, (byte) 0x4a,
        (byte) 0x85, (byte) 0xe7, (byte) 0x0a, (byte) 0x03
    },{
        (byte) 0x8e, (byte) 0xe0, (byte) 0x8b, (byte) 0x6b,
        (byte) 0x5d, (byte) 0xd7, (byte) 0xfb, (byte) 0xbc
    },{
        (byte) 0x55, (byte) 0xec, (byte) 0xb8, (byte) 0x4c,
        (byte) 0x24, (byte) 0xa7, (byte) 0xee, (byte) 0x9e
    },{
        (byte) 0xb3, (byte) 0x73, (byte) 0xae, (byte) 0x75,
        (byte) 0x9d, (byte) 0xda, (byte) 0x42, (byte) 0xa5
    },{
        (byte) 0xee, (byte) 0x2b, (byte) 0x96, (byte) 0xba,
        (byte) 0x41, (byte) 0xa2, (byte) 0x59, (byte) 0x0a
    },{
        (byte) 0xf9, (byte) 0x8e, (byte) 0x97, (byte) 0x76,
        (byte) 0x3a, (byte) 0x90, (byte) 0x61, (byte) 0x0f
    }};
    
    private static final byte[][] key2 = {{
        (byte) 0xb6, (byte) 0x61, (byte) 0x40, (byte) 0x1d,
        (byte) 0xfe, (byte) 0xfe, (byte) 0x25, (byte) 0x5c,
        (byte) 0xad, (byte) 0x91, (byte) 0xfb, (byte) 0x23,
        (byte) 0xcf, (byte) 0x46, (byte) 0xff, (byte) 0x30,
        (byte) 0xf8, (byte) 0xcc, (byte) 0xe0, (byte) 0xed,
        (byte) 0x56, (byte) 0xd3, (byte) 0xd5, (byte) 0x47
    },{
        (byte) 0x82, (byte) 0x9f, (byte) 0x07, (byte) 0xfd,
        (byte) 0xa8, (byte) 0xbc, (byte) 0x19, (byte) 0xc3,
        (byte) 0x0f, (byte) 0x38, (byte) 0xfe, (byte) 0xe3,
        (byte) 0xfa, (byte) 0xc4, (byte) 0xe2, (byte) 0x03,
        (byte) 0x02, (byte) 0x06, (byte) 0xa8, (byte) 0xe2,
        (byte) 0xbe, (byte) 0x33, (byte) 0xdd, (byte) 0xb2
    },{
        (byte) 0x36, (byte) 0xe1, (byte) 0x6d, (byte) 0x52,
        (byte) 0xe1, (byte) 0x32, (byte) 0xc7, (byte) 0x22,
        (byte) 0xfa, (byte) 0xb3, (byte) 0x40, (byte) 0x3d,
        (byte) 0x89, (byte) 0x86, (byte) 0x2c, (byte) 0x54,
        (byte) 0xde, (byte) 0xd3, (byte) 0xfe, (byte) 0x6c,
        (byte) 0x92, (byte) 0xff, (byte) 0x43, (byte) 0x28
    },{
        (byte) 0xa3, (byte) 0x88, (byte) 0xd7, (byte) 0xb3,
        (byte) 0x04, (byte) 0x31, (byte) 0x60, (byte) 0x11,
        (byte) 0x61, (byte) 0xed, (byte) 0xe9, (byte) 0xa9,
        (byte) 0xbb, (byte) 0xa9, (byte) 0xcd, (byte) 0x54,
        (byte) 0x4c, (byte) 0xdf, (byte) 0xb6, (byte) 0x04,
        (byte) 0xdb, (byte) 0xf8, (byte) 0x1e, (byte) 0xad
    },{
        (byte) 0x35, (byte) 0x3b, (byte) 0x9d, (byte) 0x41,
        (byte) 0xde, (byte) 0x15, (byte) 0x0f, (byte) 0xe5,
        (byte) 0xd3, (byte) 0x2d, (byte) 0xfa, (byte) 0x5b,
        (byte) 0x1b, (byte) 0xce, (byte) 0x52, (byte) 0x47,
        (byte) 0xfd, (byte) 0x55, (byte) 0xfe, (byte) 0xc6,
        (byte) 0x62, (byte) 0xe5, (byte) 0x36, (byte) 0xc4
    },{
        (byte) 0xab, (byte) 0xba, (byte) 0x80, (byte) 0x31,
        (byte) 0xce, (byte) 0xc4, (byte) 0xf8, (byte) 0x84,
        (byte) 0xf3, (byte) 0xf0, (byte) 0xa6, (byte) 0x88,
        (byte) 0x25, (byte) 0x8d, (byte) 0xf2, (byte) 0xf7,
        (byte) 0x10, (byte) 0x58, (byte) 0xb5, (byte) 0x3b,
        (byte) 0x4b, (byte) 0x3e, (byte) 0xd2, (byte) 0x05
    },{
        (byte) 0xa4, (byte) 0x25, (byte) 0xf8, (byte) 0x8b,
        (byte) 0xf7, (byte) 0x7f, (byte) 0x3b, (byte) 0xdd,
        (byte) 0x9d, (byte) 0x5d, (byte) 0x2e, (byte) 0xb8,
        (byte) 0x62, (byte) 0x05, (byte) 0x90, (byte) 0xb3,
        (byte) 0x28, (byte) 0xb2, (byte) 0x7a, (byte) 0x9a,
        (byte) 0xba, (byte) 0xf7, (byte) 0x84, (byte) 0xe3
    },{
        (byte) 0x91, (byte) 0xf1, (byte) 0xd7, (byte) 0x40,
        (byte) 0x19, (byte) 0x53, (byte) 0x32, (byte) 0xd9,
        (byte) 0x24, (byte) 0x67, (byte) 0xc7, (byte) 0x67,
        (byte) 0xa5, (byte) 0xbf, (byte) 0xf6, (byte) 0x5d,
        (byte) 0x17, (byte) 0x92, (byte) 0xd0, (byte) 0x0b,
        (byte) 0x1d, (byte) 0xb3, (byte) 0x6a, (byte) 0x26
    },{
        (byte) 0x84, (byte) 0xbb, (byte) 0x41, (byte) 0xcf,
        (byte) 0xb5, (byte) 0x96, (byte) 0xbb, (byte) 0xf2,
        (byte) 0xc6, (byte) 0x7b, (byte) 0xdd, (byte) 0x0c,
        (byte) 0x75, (byte) 0x1a, (byte) 0x06, (byte) 0x67,
        (byte) 0x0b, (byte) 0xef, (byte) 0x57, (byte) 0x3c,
        (byte) 0x49, (byte) 0xf1, (byte) 0xa7, (byte) 0x14        
    }};
    
    private static final byte[][] md = {{ (byte) 20,
        (byte) 0xb2, (byte) 0x66, (byte) 0xd3, (byte) 0xcd, 
        (byte) 0x52, (byte) 0x13, (byte) 0x92, (byte) 0x0a, 
        (byte) 0x23, (byte) 0x49, (byte) 0x1f, (byte) 0xc3, 
        (byte) 0xa1, (byte) 0x16, (byte) 0x09, (byte) 0x53, 
        (byte) 0xab, (byte) 0x20, (byte) 0x73, (byte) 0xe1
    },{ (byte) 20,
        (byte) 0x27, (byte) 0x74, (byte) 0x55, (byte) 0xe9, 
        (byte) 0xef, (byte) 0x10, (byte) 0x86, (byte) 0xba, 
        (byte) 0x05, (byte) 0x95, (byte) 0x99, (byte) 0x3a, 
        (byte) 0xa8, (byte) 0x60, (byte) 0xc1, (byte) 0x44, 
        (byte) 0x94, (byte) 0x54, (byte) 0xe0, (byte) 0xec
    },{ (byte) 20,
        (byte) 0x6f, (byte) 0x2b, (byte) 0x9c, (byte) 0x5f, 
        (byte) 0x0f, (byte) 0x5b, (byte) 0x19, (byte) 0xad, 
        (byte) 0x30, (byte) 0x9d, (byte) 0x7e, (byte) 0xb7, 
        (byte) 0xbc, (byte) 0x2b, (byte) 0x02, (byte) 0xbf, 
        (byte) 0x0f, (byte) 0xc8, (byte) 0x5c, (byte) 0x93
    },{ (byte) 20,
        (byte) 0xf0, (byte) 0x19, (byte) 0x66, (byte) 0xab, 
        (byte) 0x75, (byte) 0xa6, (byte) 0x53, (byte) 0xe3, 
        (byte) 0x63, (byte) 0x72, (byte) 0x53, (byte) 0x39, 
        (byte) 0x5a, (byte) 0x90, (byte) 0xa6, (byte) 0xe0, 
        (byte) 0x59, (byte) 0xca, (byte) 0x48, (byte) 0xc9
    },{ (byte) 20,
        (byte) 0xfb, (byte) 0xc8, (byte) 0x78, (byte) 0x1f, 
        (byte) 0x81, (byte) 0x48, (byte) 0x4b, (byte) 0x2a, 
        (byte) 0x59, (byte) 0xa4, (byte) 0xc7, (byte) 0xe4, 
        (byte) 0xc0, (byte) 0x18, (byte) 0x17, (byte) 0xa0, 
        (byte) 0x14, (byte) 0x96, (byte) 0x3c, (byte) 0x82
    },{ (byte) 20,
        (byte) 0x33, (byte) 0xcb, (byte) 0xb6, (byte) 0xba, 
        (byte) 0x91, (byte) 0xeb, (byte) 0x5a, (byte) 0x2c, 
        (byte) 0x99, (byte) 0x01, (byte) 0x37, (byte) 0x05, 
        (byte) 0x40, (byte) 0x2f, (byte) 0x60, (byte) 0xc1, 
        (byte) 0x07, (byte) 0x21, (byte) 0x99, (byte) 0x3e
    },{ (byte) 20,
        (byte) 0x3f, (byte) 0x33, (byte) 0x17, (byte) 0x95, 
        (byte) 0x91, (byte) 0x49, (byte) 0x30, (byte) 0x50, 
        (byte) 0xc2, (byte) 0xa6, (byte) 0xff, (byte) 0x22, 
        (byte) 0xb4, (byte) 0x86, (byte) 0xed, (byte) 0x5e, 
        (byte) 0x56, (byte) 0x62, (byte) 0xdb, (byte) 0xa3
    },{ (byte) 20,
        (byte) 0xde, (byte) 0x0d, (byte) 0xb7, (byte) 0xac, 
        (byte) 0x58, (byte) 0x52, (byte) 0x43, (byte) 0xb7, 
        (byte) 0x62, (byte) 0xb1, (byte) 0xd4, (byte) 0x77, 
        (byte) 0x5c, (byte) 0x0c, (byte) 0x31, (byte) 0xe4, 
        (byte) 0x2e, (byte) 0x63, (byte) 0x15, (byte) 0x2a
    },{ (byte) 20,
        (byte) 0xe3, (byte) 0xd5, (byte) 0xf0, (byte) 0x6b,
        (byte) 0x6c, (byte) 0xae, (byte) 0xe6, (byte) 0x75,
        (byte) 0x42, (byte) 0x70, (byte) 0xba, (byte) 0x9d,
        (byte) 0xee, (byte) 0x41, (byte) 0x2b, (byte) 0x1c,
        (byte) 0xd6, (byte) 0x92, (byte) 0xf9, (byte) 0x0d,
     }};
    
}







