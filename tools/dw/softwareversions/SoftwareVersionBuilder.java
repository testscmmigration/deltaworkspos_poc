package dw.softwareversions;

import java.io.File;
import java.io.FileWriter;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dw.utility.Debug;

public class SoftwareVersionBuilder {

    public static String POS_TYPE = "DW1.0"; 
    public static Pattern MANIFEST_PATTERN = Pattern.compile("\\d\\d\\d\\d\\d\\d/manifest.dat");
    public static Pattern SCRIPT_PATTERN = Pattern.compile("insert_dw\\d\\d\\d\\d\\d\\d.sql");
    
	/**
	     * Set up the app to use SSL if needed.
	     */
	/*    
	    private static void initializeSSL() {
	        // add the SSL security provider...usings Sun's reference Impl for now
	        java.security.Security.addProvider(
	                            new com.sun.net.ssl.internal.ssl.Provider());
	        System.setProperty("java.protocol.handler.pkgs",
	                                 "com.sun.net.ssl.internal.www.protocol");
	    }
	*/
	    /**
	     * Run the utility.
	     *   If no flags are given, prompt the user for the information needed to
	     *      create a manifest file for the version in the current directory.
	     *   If a flag of CREATE is given, create the manifest file for this
	     *      directory. If the optional JRE version name parm is given, we will
	     *      add a DEPENDENCY flag to the file specifying that JRE directory.
	     *   If a flag of VALIDATE is given with a version number specified, try to
	     *      validate the given version.
	     *   If a flag of VALIDATE is given with no version number, try to find the
	     *      most recent and valid version.
	     *   In both VALIDATION cases, exit with a system code that specifies the
	     *     the most recent valid version. If there are none, notify the user
	     *     with a message box and exit with a 0 for most recent version. The
	     *     System.exit(version) will put the version number in the ERRORLEVEL
	     *     system variable for access from the launcher file.
	     */
	      	//dw.comm.AgentConnect.enableExtraDebug();		//TEMPORARY: DO NOT RELEASE WITHOUT COMMENTING THIS OUT [EEO]
	    public static void main(String[] args) {
	    	//dw.comm.AgentConnect.disableDebugEncryption();	//TEMPORARY: DO NOT RELEASE WITHOUT COMMENTING THIS OUT [EEO]
	        if(args.length == 0)
	        	SoftwareVersionBuilder.usage();
	        String command = args[0];
	        if (command.equalsIgnoreCase("create")) {	//$NON-NLS-1$
	            if (args.length == 5)
	            {
	                SoftwareVersionBuilder.createManifest( 0,             // maxFileSize
	                                false,         // delete splits
	                                args[1],       // version
	                                args[2],       // server base
	                                args[3],       // label
	                                args[4],       // description
	                                null,          // jre name
	                                null,          // jre server dir
	                                0              // jre manifest size
	                                ,true
	                                );
	            }
	            else if (args.length == 7)
	            {
	                SoftwareVersionBuilder.createManifest(Integer.parseInt(args[1]), // maxFileSize
	                               Boolean.valueOf(args[2]).booleanValue(), // delete splits
	                               args[3], // version
	                               args[4], // server base
	                               args[5], // label
	                               args[6], // description
	                               null, // jre name
	                               null, // jre server dir
	                               0 // jre manifest size
	                               ,true
	                               );
	            }
	            else if (args.length == 8)
	            {
	                SoftwareVersionBuilder.createManifest(0,     // maxFileSize
	                               false, // delete splits
	                               args[1], // version
	                               args[2], // server base
	                               args[3], // label
	                               args[4], // description
	                               args[5], // jre name
	                               args[6], // jre server dir
	                               Integer.parseInt(args[7]), // jre manifest size
	                               null, // Prerequisite jre name
	                               null, // Prerequisite jre server dir
	                               0, // Prerequisite jre manifest size
	                               null, // Postupgrade jre name
	                               null, // Postupgradejre server dir
	                               0 // Postupgrade jre manifest size
	                               );
	            }
	
	            else if (args.length == 10)
	            {
	                SoftwareVersionBuilder.createManifest(Integer.parseInt(args[1]), // maxFileSize
	                               Boolean.valueOf(args[2]).booleanValue(), // delete splits
	                               args[3], // version
	                               args[4], // server base
	                               args[5], // label
	                               args[6], // description
	                               args[7], // jre name
	                               args[8], // jre server dir
	                               Integer.parseInt(args[9]), // jre manifest size
	                               null, // Prerequisite jre name
	                               null, // Prerequisite jre server dir
	                               0,	
	                               null, // Postupgrade jre name
	                               null, // Postupgradejre server dir
	                               0 // Postupgrade jre manifest size
	                               );
	            }
	            else if (args.length == 11)
	            {
	                SoftwareVersionBuilder.createManifest(0,     // maxFileSize
	                               false, // delete splits
	                               args[1], // version
	                               args[2], // server base
	                               args[3], // label
	                               args[4], // description
	                               args[5], // jre name
	                               args[6], // jre server dir
	                               Integer.parseInt(args[7]), // jre manifest size
	                               args[8], // Prerequisite jre name
	                               args[9], // Prerequisite jre server dir
	                               Integer.parseInt(args[10]),  // Prerequisite jre manifest size
	                               null, // Postupgrade jre name
	                               null, // Postupgradejre server dir
	                               0 // Postupgrade jre manifest size
	                               );
	            }
	            else if (args.length == 13)
	            {
	                SoftwareVersionBuilder.createManifest(Integer.parseInt(args[1]), // maxFileSize
	                               Boolean.valueOf(args[2]).booleanValue(), // delete splits
	                               args[3], // version
	                               args[4], // server base
	                               args[5], // label
	                               args[6], // description
	                               args[7], // jre name
	                               args[8], // jre server dir
	                               Integer.parseInt(args[9]) ,// jre manifest size,
	                               args[10], // Prerequisite jre name
	                               args[11], // Prerequisite jre server dir
	                               Integer.parseInt(args[12]),  // Prerequisite jre manifest size
	                               null, // Postupgrade jre name
	                               null, // Postupgradejre server dir
	                               0 // Postupgrade jre manifest size
	                               );
	            }
	            else if (args.length == 16)
	            {
	                SoftwareVersionBuilder.createManifest(Integer.parseInt(args[1]), // maxFileSize
	                               Boolean.valueOf(args[2]).booleanValue(), // delete splits
	                               args[3], // version
	                               args[4], // server base
	                               args[5], // label
	                               args[6], // description
	                               args[7], // jre name
	                               args[8], // jre server dir
	                               Integer.parseInt(args[9]) ,// jre manifest size,
	                               args[10], // Prerequisite jre name
	                               args[11], // Prerequisite jre server dir
	                               Integer.parseInt(args[12]),  // Prerequisite jre manifest size
	                                args[13], // Prerequisite jre name
	                               args[14], // Prerequisite jre server dir
	                               Integer.parseInt(args[15])  // Prerequisite jre manifest size
	                               );
	            }
	            else
	            {
	            	SoftwareVersionBuilder.usage();
	            }
			} else if (command.equalsIgnoreCase("jrecreate")) {
				
				if (args.length == 8) {
					boolean manifestEntryFlag = true;
					if ("true".equalsIgnoreCase(args[7])) {
						manifestEntryFlag = false;
					}
					
					SoftwareVersionBuilder.createManifest(Integer.parseInt(args[1]), // maxFileSize
							Boolean.valueOf(args[2]).booleanValue(), // delete splits
							args[3], // version
							args[4], // server base
							args[5], // label
							args[6], // description
							null, // jre name
							null, // jre server dir
							0 // jre manifest size
							, manifestEntryFlag);
				}
			}
	        /*
	        else if (command.equalsIgnoreCase("download") && args.length > 3) {	//$NON-NLS-1$
	            initializeSSL();
	            download(args[1], Integer.parseInt(args[2]), args[3]);
	        }
	        */
	        else if (command.equalsIgnoreCase("validate") && args.length > 1) {	//$NON-NLS-1$
	        	if(args.length==2){
	        		SoftwareVersionUtility.validateVersion(args[1],null);
	        	}else if (args.length==3){
	        		SoftwareVersionUtility.validateVersion(args[1],args[2]);
	        	}
	        }
	        else if (command.equalsIgnoreCase("delete") && args.length > 1)	//$NON-NLS-1$
	            SoftwareVersionUtility.deleteDirectory(args[1]);
	        else if (command.equalsIgnoreCase("deleteold") && args.length > 1)	//$NON-NLS-1$
	            SoftwareVersionUtility.deleteOutdatedVersions(Integer.parseInt(args[1]),null);
	        else if (command.equalsIgnoreCase("copy") && args.length > 2)	//$NON-NLS-1$
	            SoftwareVersionUtility.copyDirectory(args[1], args[2]);
	        else if (command.equalsIgnoreCase("sync") && args.length > 1)	//$NON-NLS-1$
	            SoftwareVersionUtility.synchronizeWithManifest(args[1]);
	        else
	        	SoftwareVersionBuilder.usage();
	    }

	    public static void usage() {
	        Debug.println("invalid command");	//$NON-NLS-1$
	/*        
	        Debug.println("    1. SoftwareVersionUtility create " +	//$NON-NLS-1$
	               "[<max download file size> <delete split files (true|false)>] " +
	                                                   "<version number (xyy)> " +
	                                                   "<server base directory> " +
	                                                   "<build label (10 chars)" +
	                                                   "<build desc (30 chars)" +
	                                                   "[<jre name> " +
	                                                   "<jre man server base> " +
	                                                   "<jre manifest size>]");
	        Debug.println("    2. SoftwareVersionUtility download " +	//$NON-NLS-1$
	                                                   "<manifest url> " +
	                                                   "<manifest size> " +
	                                                   "<server name>");
	        Debug.println("    3. SoftwareVersionUtility validate <directory>\n" +	//$NON-NLS-1$
	                            "         - version directory will be validated" +
	                            " and return code returned in System.exit(" +
	                            "0 = valid, 1 = invalid).");
	        Debug.println("    4. SoftwareVersionUtility delete <version number>");	//$NON-NLS-1$
	        Debug.println("    5. SoftwareVersionUtility deleteold <# to keep>");	//$NON-NLS-1$
	*/
	        System.exit(1);
	    }

		/**
		 * Create a version manifest with the given values.
		 */
		public static void createManifest(int maxFileSize, boolean deleteSplitFiles, String version, String serverBase,
					String label, String description, String jreName, String jreManServDir, int jreManifestSize,boolean manifestEntryFlag) {
				createManifest(maxFileSize, deleteSplitFiles, version, serverBase, label, description, jreName, jreManServDir,
						jreManifestSize, null, null, 0,null,null,0,manifestEntryFlag);
			}
		
		public static void createManifest(int maxFileSize, boolean deleteSplitFiles, String version, String serverBase,
				String label, String description, String jreName, String jreManServDir, int jreManifestSize,
				String preReqName, String preReqServLocation, int preReqFileSize, String postUpgrdName,
				String postUpgrdServLocation, int postUpgrdFileSize) {
			createManifest(maxFileSize, deleteSplitFiles, version, serverBase, label, description, jreName, jreManServDir,
					jreManifestSize, preReqName, preReqServLocation, preReqFileSize, postUpgrdName, postUpgrdServLocation,
					postUpgrdFileSize, true);
		}
		/**
		 * Create a version manifest with the given values.
		 */
		public static void createManifest(int maxFileSize, boolean deleteSplitFiles, String version, String serverBase,
				String label, String description, String jreName, String jreManServDir, int jreManifestSize,
				String preReqName, String preReqServLocation, int preReqFileSize,
				String postUpgrdName, String postUpgrdServLocation, int postUpgrdFileSize,boolean manifestEntryFlag) {
			VersionManifestBase manifest = new VersionManifestBase(version + "\\manifest.dat", version); //$NON-NLS-1$
			if (jreManServDir == null){
				manifest.create(serverBase, version, maxFileSize, deleteSplitFiles,manifestEntryFlag);
			}else{
				manifest.create(serverBase, version, jreName, jreManServDir, jreManifestSize, maxFileSize,
						deleteSplitFiles, preReqName, preReqServLocation, preReqFileSize, postUpgrdName, postUpgrdServLocation, postUpgrdFileSize,manifestEntryFlag);
			}
			// write out the SQL script file that we will use to insert the rows
			// into Oracle for this version
			createInsertScript(label, description, serverBase + "/manifest.dat", //$NON-NLS-1$
					version);
		}

		/**
		 * Create an SQL script that will insert rows into the Oracle tables to
		 *   make this version available for download. The script file will be
		 *   named 'insert_dw<version>.sql'
		 */
		public static void createInsertScript(String label, String description,
		                                       String fileUrlPath, String version) {
		    // truncate the label and description if necessary
		    try {
		        // strip off any quotes that were passed in on the label and desc
		        label = label.replace('"', '\0');
		        description = description.replace('"', '\0');
		        if (label.length() > 10)
		            label = label.substring(0, 10);
		        if (description.length() > 30)
		            description = description.substring(0, 30);
		        // get the size of the manifest file
		        String manifestName = version + "/manifest.dat"; //$NON-NLS-1$
				Matcher m3 = MANIFEST_PATTERN.matcher(manifestName);
				if (!m3.matches()) {
		            Debug.println("Invalid manifest file name - " + manifestName);	//$NON-NLS-1$
		            // Debug stacktrace caught upon subsequent FileIOException
				}
				
		        File manifest = new File(manifestName);
		        long manifestSize = manifest.length();
		        String scriptName = "insert_dw" + version + ".sql"; //$NON-NLS-1$ //$NON-NLS-2$
				Matcher m4 = SCRIPT_PATTERN.matcher(scriptName);
				if (!m3.matches()) {
		            Debug.println("Invalid script file name - " + scriptName);	//$NON-NLS-1$
		            // Debug stacktrace caught upon subsequent FileIOException
				}
				
		        File script = new File(scriptName);
		        // if the script file exists, delete it.
		        if (script.exists())
		            script.delete();
		        
		        FileWriter out = new FileWriter(script);
		        out.write("/* \r\n"); //$NON-NLS-1$
		        out.write("  NOTE: This script is programmatically generated by\r\n"); //$NON-NLS-1$
		        out.write("  the DeltaWorks POS build process.\r\n"); //$NON-NLS-1$
		        out.write(" \r\n"); //$NON-NLS-1$
		        out.write("  Purpose: \r\n"); //$NON-NLS-1$
		        out.write("        Deltaworks version info data for version " + version + ". \r\n"); //$NON-NLS-1$ //$NON-NLS-2$
		        out.write("\r\n");	//$NON-NLS-1$
		        out.write("  Modification history:\r\n"); //$NON-NLS-1$
		        out.write("        Auto-generated on " + new Date() + " \r\n"); //$NON-NLS-1$ //$NON-NLS-2$
		        out.write("*/\r\n"); //$NON-NLS-1$
		        out.write("\r\n");	//$NON-NLS-1$
		        out.write("WHENEVER OSERROR EXIT 12;\r\n"); //$NON-NLS-1$
		        out.write("WHENEVER SQLERROR EXIT SQL.SQLCODE;\r\n"); //$NON-NLS-1$
		        out.write("\r\n");	//$NON-NLS-1$
		        out.write("spool pmgsqdw001r.log\r\n"); //$NON-NLS-1$
		        out.write("\r\n");	//$NON-NLS-1$
		        out.write("select count(*) from software_version;\r\n"); //$NON-NLS-1$
		        out.write("select count(*) from software_version_component;\r\n"); //$NON-NLS-1$
		        out.write("\r\n");	//$NON-NLS-1$
		        out.write("--delete from software_version where version_id = '" +  //$NON-NLS-1$
		                SoftwareVersionUtility.pad5(version) + "';\r\n"); //$NON-NLS-1$
		        out.write("--delete from software_version_component where version_id = '" +  //$NON-NLS-1$
		                SoftwareVersionUtility.pad5(version) + "';\r\n"); //$NON-NLS-1$
		        out.write("\r\n");	//$NON-NLS-1$
		        out.write("insert into software_version values ('"  + SoftwareVersionUtility.pad5(version) + //$NON-NLS-1$
		                  "', '" + POS_TYPE + "', sysdate,\r\n '" + label +	//$NON-NLS-1$ //$NON-NLS-2$
		                  "', '" + description + "', 'Y', sysdate,'DW-Release',\r\n sysdate, 'DW-Release');\r\n");	//$NON-NLS-1$ //$NON-NLS-2$
		        out.write("insert into software_version_component values ('" + //$NON-NLS-1$
		                  SoftwareVersionUtility.pad5(version) + "', '" + manifestSize + "',\r\n '" +	//$NON-NLS-1$ //$NON-NLS-2$
		                  fileUrlPath + "');\r\n"); //$NON-NLS-1$
		        out.write("\r\n");	//$NON-NLS-1$
		        out.write("select count(*) from software_version;\r\n"); //$NON-NLS-1$
		        out.write("select count(*) from software_version_component;\r\n"); //$NON-NLS-1$
		        out.write("\r\n");	//$NON-NLS-1$
		        out.write("commit;\r\n"); //$NON-NLS-1$
		        out.write("spool off\r\n"); //$NON-NLS-1$
		        out.write("exit;\r\n"); //$NON-NLS-1$
		
		        out.close();
		    }
		    catch (Exception e) {
		        Debug.println("Could not create the SQL script for this version.");	//$NON-NLS-1$
		        Debug.printStackTrace(e);
		    }
		}
}
