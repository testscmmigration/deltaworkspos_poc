package dw.softwareversions;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dw.i18n.Messages;
import dw.utility.Debug;
import dw.utility.ErrorManager;

/**
 * Contains information about a software version manifest. The manifest contains
 *   VersionManifestEntries, a version number, a manifest file size, and a URL
 *   for the manifest file. The manifest also knows how to save itself to a file
 *   and load itself from a file (or URL).
 * @author Rob Campbell
 */
public class VersionManifestBase extends VersionManifestEntry {
    private static long maxFileSize = 0;
    private static boolean deleteSplitFiles = false;
    private static Pattern JRE_PATTERN = Pattern.compile("jre((\\d{3})|(\\d{4}))z");
    private static Pattern JRE_PATTERN_2 = Pattern.compile("jre((\\d{3})|(\\d{5}))z");
    private Map<String, VersionManifestEntry> manifestEntries = new TreeMap<String, VersionManifestEntry>();   // of VersionManifestEntry

    private String versionNumber;
    public static void setDeleteSplitFiles(boolean split) {
        deleteSplitFiles = split;
    }

    public VersionManifestBase() {}

    /** Used by SoftwareVersionUtility when creating a new manifest file. */
    public VersionManifestBase(String fileName, String version) {
        super(fileName, 0, "", "", "", false,TYPE_FILE,false); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        versionNumber = version;
    }

    public void print() {
        Debug.println("VersionManifest for Version " + versionNumber + "{");	//$NON-NLS-1$ //$NON-NLS-2$
        Debug.println("  super {" + toString() + "}");	//$NON-NLS-1$ //$NON-NLS-2$
        Debug.println("Entries : ");	//$NON-NLS-1$
        for (Iterator<VersionManifestEntry> it = manifestEntries.values().iterator(); it.hasNext(); ) {
            ((VersionManifestEntry) it.next()).print();
        }
        Debug.println("} Manifest.");	//$NON-NLS-1$
    }

 

    private void save() throws IOException {
        File file = createFileAndDir(getLocalFilePath());
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        bw.write("VERSIONNUMBER=" + versionNumber + "\n");	//$NON-NLS-1$ //$NON-NLS-2$
        for (Iterator<VersionManifestEntry> it = manifestEntries.values().iterator(); it.hasNext(); ) {
            bw.write(it.next().toString());
        }
        bw.close();
    }
    /**
     * Create the manifest file for everything in this directory and its
     *   subdirectories. The validation file will be in the form:
     *   Version=<version number>
     *   VersionManifestEntry*
     * Once the file has been created, save it and write out a SQL script file
     *   that will be used to insert the correcponding rows into Oracle for it.
     */
    public void create(String serverBase, String posBase, int maxFile,
                       boolean deleteSplits,boolean manifestEntryFlag) {
        maxFileSize = maxFile;
        deleteSplitFiles = deleteSplits;
        try {
            addFileEntries(new File(versionNumber), serverBase, posBase,manifestEntryFlag);
            Matcher m2 = JRE_PATTERN.matcher(getVersionNumber().trim());
            Matcher m3 = JRE_PATTERN_2.matcher(getVersionNumber().trim());
            
            if ( m2.matches() || m3.matches()) {
				String jreManifest = versionNumber.substring(0, versionNumber.length() - 1) + "/manifest.dat";
				String jreServerBase = serverBase.substring(0, serverBase.length() - 1);
				String jrePosBase = posBase.substring(0, posBase.length() - 1);
				File file = new File(jreManifest);
				if(manifestEntryFlag){
					addFileEntry(file,jreServerBase, jrePosBase, null,"manifest.dat",true);	
				}
				
			}
            save();
        }
        catch (IOException e) {
			Debug.printStackTrace(e);
            ErrorManager.handle(e, "applicationError", Messages.getString("ErrorMessages.applicationError"), ErrorManager.ERROR); //$NON-NLS-1$
        }
    }

    /**
     * Create the manifest file for everything in this directory and its
     *   subdirectories. Also add a dependency to the manifest.
     * The validation file will be in the form:
     *   Version=<version number>
     *   Dependency=...
     *   VersionManifestEntry*
     */
    public void create(String serverBase, String posBase,
                       String depName, String depServLocation,
                       int depFileSize, int maxFile, boolean deleteSplitFiles) {
        create(serverBase, posBase,
                depName, depServLocation,
                depFileSize, maxFile, deleteSplitFiles,null,null,0,null,null,0,true);
    }
    
    public void create(String serverBase, String posBase,
            String depName, String depServLocation,
            int depFileSize, int maxFile, boolean deleteSplitFiles, String preReqName, String preReqServLocation,
            int preReqFileSize,String postUpgrdName, String postUpgrdServLocation,
            int postUpgrdFileSize,boolean manifestEntryFlag) {
			addDependency(depServLocation, depName, depFileSize);
		if (validateJreName(preReqName, preReqServLocation)) {
			addPrerequisite(preReqServLocation, preReqName, preReqFileSize);
		}
		if (validateJreName(postUpgrdName, postUpgrdServLocation)) {
			addpostUpgrd(postUpgrdServLocation, postUpgrdName, postUpgrdFileSize);
		}
         create(serverBase, posBase, maxFile, deleteSplitFiles,manifestEntryFlag);
    }

    public boolean validateJreName(String preReqName,String preReqServLocation)
    {
    	if(preReqName!=null && preReqName.length()>1 && !preReqName.equals("#") 
    			&& preReqServLocation!=null && preReqServLocation.length()>1)
   	 	{
    		return true;
   	 	}
		return false;
    }
    public void create(String serverBase, String posBase, String depFileName,
                       String depServLocation, String depPosLocation,
                       int depFileSize) {
        create(serverBase, posBase, depServLocation, depPosLocation,
            depFileSize, 0, false);
    }

    /**
     * For a given directory, find all the files contained in it and add them
     *   to the list of file entries.
     * @param directory the directory in which to find the files to process
     * @param serverBase the base directory on the server where the file will
     *    be located.
     * @param manifestEntryFlag 
     */
    private void addFileEntries(File directory, String serverBase, String posBase, boolean manifestEntryFlag)
                                             throws IOException {
        File[] files = directory.listFiles();
        HashMap<String, File> splitFiles = new HashMap<String, File>();
        // first, go through the files and split any that are more than the max
        //   size, keeping track of which ones are split
        for (int i = 0; i < files.length; i++) {
            File file = files[i];
            long fileSize = files[i].length();
            if (maxFileSize > 0 && fileSize > maxFileSize) {
                // split the file into parts and keep track of the split file
                splitFiles.put(file.getName(), file);
                ZipUtility.splitFile(getTranslatedPath(file.getPath()),
                                                       (int) maxFileSize,
                                                       deleteSplitFiles);
            }
        }

        // get the new list of files, which will now include component files
        files = directory.listFiles();
        for (int i = 0; i < files.length; i++) {
            if (files[i].isDirectory()) {
                // recurse into subdirectory
                addFileEntries(files[i], serverBase, posBase,true);
            }
            else {
            	File file = files[i];
				if (!manifestEntryFlag) {
					if (!("manifest.dat".equals(file.getName()))) {
						addFileEntry(file, serverBase, posBase, splitFiles, null,false);
					}
				} else {
					addFileEntry(file, serverBase, posBase, splitFiles, null,false);
				}
            }
        }
    }

	private void addFileEntry(File file,String serverBase, String posBase, HashMap<String, File> splitFiles,String hasFileName,boolean skip) {
		
		String checksum = ChecksumUtility.getChecksum(
		                                     file.toString());
		long fileSize = file.length();

		String serverLoc = getTranslatedPath(serverBase);
		// if this file is contained in the list of files we split, set
		//   the serverLocation to "" so the manifest knows it will be	//$NON-NLS-1$
		//   joined after downloading
		if (splitFiles!=null && splitFiles.containsKey(file.getName())) {
		    serverLoc = "";	//$NON-NLS-1$
		}
		VersionManifestEntry entry = null;
		if (hasFileName!=null && hasFileName.length()>0) {
			entry = new VersionManifestEntry(getTranslatedPath(hasFileName), fileSize, checksum,
					serverLoc, posBase, false, TYPE_FILE,skip);

		} else {
			entry = new VersionManifestEntry(getTranslatedPath(getRelativePath(file.getPath())), fileSize, checksum,
					serverLoc, posBase, false, TYPE_FILE,skip);
		}
		addManifestEntry(entry);
	}

    /**
     * Get the path of the given file relative to the location of this manifest
     */
    private String getRelativePath(String filePath) {
        String manifestDir = new File(fileName).getParent();
        return filePath.substring(manifestDir.length() + 1);
    }

    /**
     * Create a new VersionManifestEntry, using the dependency flag, and add
     *   it to the list of entries.
     */
    private void addDependency(String serverLocation,
                               String posLocation, int fileSize) {
        VersionManifestEntry depEntry = new VersionManifestEntry(
                             "manifest.dat", fileSize, "", serverLocation,	//$NON-NLS-2$ //$NON-NLS-1$
                             posLocation, true,TYPE_DEPENDENCY,false);
        addManifestEntry(depEntry);
    }
    /**
     * Create a new VersionManifestEntry, using the prerequisite and add
     *   it to the list of entries.
     */
    private void addPrerequisite(String serverLocation,
                               String posLocation, int fileSize) {
        VersionManifestEntry depEntry = new VersionManifestEntry(
                             "prerequisiteManifest.dat", fileSize, "", serverLocation,	//$NON-NLS-2$ //$NON-NLS-1$
                             posLocation, false,TYPE_PRE_REQUISITE,false);
        addManifestEntry(depEntry);
    }

    /**
     * Create a new VersionManifestEntry, using the postUpgrade, and add
     *   it to the list of entries.
     */
    private void addpostUpgrd(String serverLocation,
                               String posLocation, int fileSize) {
        VersionManifestEntry depEntry = new VersionManifestEntry(
                             "postUpgradeManifest.dat", fileSize, "", serverLocation,	//$NON-NLS-2$ //$NON-NLS-1$
                             posLocation, false,TYPE_POST_UPGRADE,false);
        addManifestEntry(depEntry);
    }
    
    /**
     * Determine whether all the files listed in this manifest are present and
     *   uncorrupted in the version directory.
     * @return whether or not the manifest is valid. To be valid, all its
     *   entries must be valid.
     */
    public boolean isValid(StringBuffer resultBuffer) {
        for (Iterator<VersionManifestEntry> it = manifestEntries.values().iterator(); it.hasNext(); ) {
            VersionManifestEntry entry = (VersionManifestEntry) it.next();
            if(TYPE_PRE_REQUISITE.equals(entry.entryType) || TYPE_POST_UPGRADE.equals(entry.entryType))
            {
            	continue;
            }
            int status = entry.getStatus(resultBuffer);
            if (status != VersionManifestEntry.READY) {
                //Debug.println(resultBuffer.toString());
                return false;
            }
        }
        // all files must have passed
        return true;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public long getFileSize(){
        return fileSize;
    }

    public void addManifestEntry(VersionManifestEntry newManifestEntry) {
        manifestEntries.put(newManifestEntry.getLocalFilePath(), newManifestEntry);
    }

    public void setVersionNumber(String newVersionNumber) {
        versionNumber = newVersionNumber;
    }

    public String getVersionNumber() {
        return versionNumber;
    }
    
	public VersionManifest getDependency() throws IOException {

		File file = new File(getLocalFilePath());
		if (!file.exists())
			throw new IOException("Manifest file '" + getLocalFilePath() //$NON-NLS-1$
					+ "' does not exist."); //$NON-NLS-1$
		BufferedReader reader = new BufferedReader(new FileReader(file));

		String line;
		while ((line = reader.readLine()) != null) {
			if (line.toUpperCase().indexOf("VERSIONNUMBER=") >= 0) { //$NON-NLS-1$
				StringTokenizer strTok = new StringTokenizer(line, "="); //$NON-NLS-1$
				strTok.nextToken();
				versionNumber = strTok.nextToken();
				posLocation = versionNumber; // same as version for manifests
			}

			Map<String, String> map = getTokenized(line);

			if (map.get(TYPE_DEPENDENCY) != null) { //$NON-NLS-1$
				VersionManifest depMan;
				String depDir = (String) map.get("PosLocation"); //$NON-NLS-1$
				depMan = new VersionManifest(depDir);

				depMan.entryType = TYPE_DEPENDENCY;
				return depMan;
			}
		}
		return null;
	}

}


