package com.vu;

//From: http://www.roseindia.net/java/example/java/io/CopyDirectory.shtml

//Can't use SoftwareVersionUtility's CopyDirectory version because it doesn't copy sub-directories

import java.io.*;
import java.nio.channels.*;

public class CopyDirectory{
	public static void main(String[] args) throws IOException{
		if(args.length != 2)
		{
			System.out.println("Number of arguments is not two.");
			System.exit(1);
		}
		
		CopyDirectory cd = new CopyDirectory();
		
		String source = args[0];
		File src = new File(source);

		String destination = args[1];
		File dst = new File(destination);

		System.out.println("CopyDirectory: source " + src + " to destination " + dst);
		cd.copyDirectory(src, dst);
		System.out.println("CopyDirectory completed");
	}
	
	public void copyDirectory(File srcPath, File dstPath) throws IOException{
		if (srcPath.isDirectory())
		{
			if (!dstPath.exists())
			{
				dstPath.mkdir();
			}

			String files[] = srcPath.list();
			if (files != null) {
				for(int i = 0; i < files.length; i++)
				{
					copyDirectory(new File(srcPath, files[i]), new File(dstPath, files[i]));
				}
			}
		}
		else
		{
			if(!srcPath.exists())
			{
				System.out.println("File or directory, " + srcPath + ", does not exist.");
				System.exit(1);
			}
			else
			{
				InputStream in = new FileInputStream(srcPath);
		        OutputStream out = new FileOutputStream(dstPath);
    
				// Transfer bytes from in to out
		        byte[] buf = new byte[1024];
				int len;
		        while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				in.close();
		        out.close();
			}
		}
	}
}