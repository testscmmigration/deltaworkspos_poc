package screens;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import dw.AbstractDeltaWorks;
import dw.comm.AgentConnect;
import dw.dwgui.DWScrollPane;
import dw.framework.XmlDefinedPanel;
import dw.main.DeltaworksMainPanel;
import dw.main.DeltaworksStartup;
import dw.profile.UnitProfile;
import dw.simplepm.SimplePMTransaction;
import dw.utility.Debug;

public class DeltaWorksPageDisplay extends AbstractDeltaWorks {
	
	private static final String[] DIRECTORIES = new String[] {
		"accountdepositpartners",
		"agentdirectory",
		"billpayment",
		"dialogs",
		"dispenserload",
		"install",
		"main",
		"moneygramreceive",
		"moneygramsend",
		"moneyorder",
		"multiagent",
		"reprintreceipt",
		"simplepm",
		"softwareversions",
		"vendorpayment"
	};

	private Container pane;
	private JPanel currentPageComponent;
	private String currentPageXML;
	private String currentLanguage;
	private JComboBox directoryName;
	private JComboBox pageName;
	private DeltaworksMainPanel mainPanel;
	private Properties screenNames;
	private JLabel dimensionsLabel;
	private JLabel heightLabel;
	private XmlDefinedPanel displayPanel;
	private JButton previousButton;
	private JButton nextButton;
	private int currentIndex = -1;
	
	public static void main(String[] args) {
		DeltaWorksPageDisplay pd = new DeltaWorksPageDisplay();
		pd.init();
	}
	
	private void displayPage(JFrame frame, final ScreenPage screenPage, final String language) {
        if ((screenPage == null) || (screenPage.getPage() == null) || (screenPage.getPage().equals(currentPageXML) && language.equals(currentLanguage))) {
			return;
		}

		if (! language.equals(currentLanguage)) {
			currentLanguage = language;
			UnitProfile.getInstance().setLanguage(currentLanguage);
		}
		
		XmlDefinedPanel basePanel = null;
		if (screenPage.getBase() != null) {
			String xmlFile = screenPage.getDirectory() + "/" + screenPage.getBase();
			basePanel = new XmlDefinedPanel(xmlFile);;
		}
		String xmlFile = screenPage.getDirectory() + "/" + screenPage.getPage();
        XmlDefinedPanel pagePanel = new XmlDefinedPanel(xmlFile);
        
        if (screenPage.getDirectory().equals("main")) {
        	MenuBar mb = new MenuBar();
        	mb.add(new Menu("Item 1"));
        	mb.add(new Menu("Item 2"));
        	mb.add(new Menu("Item 3"));
        	frame.setMenuBar(mb);
        } else {
        	frame.setMenuBar(null);
        }

        if (screenPage.getDirectory().equals("dialogs")) {
        	
        	// Create the dialog
        	
            JOptionPane pane = new JOptionPane(pagePanel, JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION,  null);
            JDialog dialog = pane.createDialog(frame, "Test");
            setComponentsVisible(pane);
			dialog.setVisible(true);;
			
		} else {
			if (basePanel == null) {
				basePanel = pagePanel;
			} else if (screenPage.getDirectory().equals("softwareversions")) {
    			DWScrollPane scrollPane = (DWScrollPane) basePanel.getComponent("outputScrollPane");
    			scrollPane.getViewport().add(pagePanel);
			} else if (screenPage.getDirectory().equals("simplepm")) {
				JPanel panel = (JPanel) basePanel.getComponent("centerPanel");
				panel.add(pagePanel, screenPage.getPage());
			} else {
				JPanel panel = (JPanel) basePanel.getComponent("pageFlowPanel");
				panel.add(pagePanel, screenPage.getPage());
			}
            setComponentsVisible(basePanel);
			
			int l = screenPage.getPage().length() - 4;
            String title = screenNames.getProperty(screenPage.getPage().substring(0, l)) + " (" + language + ")";
            frame.setTitle(title);
			
            displayPanel = basePanel;
            displayPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
					
			SwingUtilities.invokeLater(new Runnable() {
			    public void run() {
			    	pageName.setSelectedItem(screenPage);
					if (currentPageComponent != null) {
						pane.remove(currentPageComponent);
					}
					pane.add(displayPanel, BorderLayout.CENTER);
					currentPageComponent = displayPanel;
					currentPageXML = screenPage.getPage();
					displayPanel.invalidate();
					pane.validate();
					pane.repaint();
			    }});
		}

        previousButton.setEnabled((pageName.getSelectedIndex() != 0) && (pageName.getItemCount() != 1));
        nextButton.setEnabled(pageName.getItemCount() > (pageName.getSelectedIndex() + 1));
	}
	
    protected void setComponentsVisible(Container parent) {
    	Component[] components = parent.getComponents();
    	for (int i = 0; i < components.length; i++) {
    		Component c = components[i];
    		c.setVisible(true);
    		if (c instanceof Container) {
    			setComponentsVisible((Container) c);
    		}
    	}
    }
	
	private void changeDirectory(String directory) {
		List pageList = null;
		
		if (directory.equals("accountdepositpartners")) {
			pageList = this.getScreenPages(directory, "ADPWizardPanel.xml");
		} else if (directory.equals("agentdirectory")) {
			pageList = this.getScreenPages(directory, "DAWizardPanel.xml");
		} else if (directory.equals("billpayment")) {
			pageList = this.getScreenPages(directory, "BPWizardPanel.xml");
		} else if (directory.equals("dialogs")) {
			pageList = this.getScreenPages(directory, null);
		} else if (directory.equals("dispenserload")) {
			pageList = new ArrayList();
			pageList.addAll(this.getScreenPages(directory, "DLWizard", "DLWizardPanel.xml"));
			pageList.addAll(this.getScreenPages(directory, "NewWizard", "NewWizardPanel.xml"));
		} else if (directory.equals("install")) {
			pageList = this.getScreenPages(directory, "AgentSetupWizardPanel.xml");
		} else if (directory.equals("main")) {
			pageList = this.getScreenPages(directory, "DeltaworksMainPanel.xml");
		} else if (directory.equals("moneygramreceive")) {
			pageList = this.getScreenPages(directory, "MGRWizardPanel.xml");
		} else if (directory.equals("moneygramsend")) {
			pageList = this.getScreenPages(directory, "MGSWizardPanel.xml");
		} else if (directory.equals("moneyorder")) {
			pageList = this.getScreenPages(directory, "MOWizardPanel.xml");
		} else if (directory.equals("multiagent")) {
			pageList = this.getScreenPages(directory, null);
		} else if (directory.equals("reprintreceipt")) {
			pageList = this.getScreenPages(directory, "RRWizardPanel.xml");
		} else if (directory.equals("simplepm")) {
			Set exclude = new HashSet();
			exclude.add("ManagerProfileOptions.xml");
			exclude.add("ManagerProfileOptions2.xml");
			exclude.add("ManagerProfileOptions3.xml");
			pageList = new ArrayList();
			pageList.addAll(this.getScreenPages("simplepm", null, null, exclude));
			pageList.addAll(this.getScreenPages("simplepm", "ManagerProfileOptions", "ManagerProfileOptions.xml"));
		} else if (directory.equals("softwareversions")) {
			pageList = this.getScreenPages(directory, "VersionBuilderOutput.xml");
		} else if (directory.equals("vendorpayment")) {
			pageList = this.getScreenPages(directory, "VPWizardPanel.xml");
		}
		
		this.pageName.removeAllItems();
		if (pageList != null) {
			for (int i = 0; i < pageList.size(); i++) {
				this.pageName.addItem(pageList.get(i));
			}
	        previousButton.setEnabled((pageName.getSelectedIndex() != 0) && (pageName.getItemCount() != 1));
	        nextButton.setEnabled(pageName.getItemCount() > (pageName.getSelectedIndex() + 1));
		}
	}
	
	public void init() {
		try {
			AgentConnect.enableDebugEncryption();
	    	
			// Load a profile
			
			Debug.setDebug(false);
			loadProfile("D3", "80004968", "5865", "dfg");
	    	mainPanel = startDeltaWorks();

			DeltaworksStartup.fixFonts();
			currentLanguage = "en";
			UnitProfile.getInstance().setLanguage(currentLanguage);
			DeltaworksStartup.junitSetup();
			SimplePMTransaction.setCurrentInstance(new SimplePMTransaction("DisplayPages"));
			
	    	DeltaworksStartup.junitSetup();
	    	JFrame.setDefaultLookAndFeelDecorated(false);
	    	UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	    	
	    	final JFrame frame = new JFrame();
	    	frame.getContentPane().setLayout(new BorderLayout());
	    	
	    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    	
			Debug.setDebug(true);
			XmlDefinedPanel.setGuiExceptionStream(new ExceptionDialog(
					System.out, frame));

//			final JFrame frame = new JFrame();
//	        frame.getInsets().left = 0;
//	        frame.getInsets().right = 0;
//	        frame.getInsets().top = 0;
//	        frame.getInsets().bottom = 0;

//	        JPanel contentPanel = new JPanel();
//	        Border padding = BorderFactory.createEmptyBorder(0, 0, 0, 0);
//	        contentPanel.setBorder(padding);
//	        frame.setContentPane(contentPanel);	        
	        
	        pane = frame.getContentPane();
	        pane.setLayout(new BorderLayout());
	        
	        String iconFileName = "xml/images/temg_window.gif"; //$NON-NLS-1$
	        java.net.URL url = ClassLoader.getSystemResource(iconFileName);
	        ImageIcon icon = null;
	        if (url != null) {
	            icon = new ImageIcon(url);
	        } else {
	            icon = new ImageIcon(iconFileName);
	        }
	        frame.setIconImage(icon.getImage());
	        frame.setLocation(XmlDefinedPanel.getLocationForCenter(frame));
	               
	//        MyWindowAdapter wndAdaptor = new MyWindowAdapter();
	//        f.addWindowListener(wndAdaptor);
	        
//	        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	        pane.add(BorderLayout.CENTER, mainPanel);
	        currentPageComponent = mainPanel;
	//        mainPanel.setupFrameComponents();
	        frame.setLocation(XmlDefinedPanel.getLocationForCenter(frame));
	        
	        screenNames = new Properties();
	        FileInputStream fis = new FileInputStream("../src/xml/i18n/deltaworks_screenmap.properties");
	        screenNames.load(fis);
			
			//----------------------------------------------------------------------
			// Bottom
			//----------------------------------------------------------------------
	
			final JPanel controls = new JPanel();
			controls.setLayout(new BoxLayout(controls, BoxLayout.Y_AXIS));
			pane.add(controls, BorderLayout.SOUTH);
			
			controls.setBorder(BorderFactory.createRaisedBevelBorder());
			controls.setBackground(Color.BLACK);
			
			JPanel line1 = new JPanel(new FlowLayout());
			controls.add(line1);
			line1.setOpaque(false);
			JPanel line2 = new JPanel(new FlowLayout());
			controls.add(line2);
			line2.setOpaque(false);
			
			// Directory Name
			
			directoryName = new JComboBox(DIRECTORIES);
			line1.add(directoryName);
			directoryName.setOpaque(false);
			directoryName.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String directory = (String) directoryName.getSelectedItem();
					changeDirectory(directory);
				}
			});
			
			// Page Name
			
			pageName = new JComboBox();
			line1.add(pageName);
			pageName.setOpaque(false);
			pageName.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int index = pageName.getSelectedIndex();
					if (index != currentIndex) {
						currentIndex = index;
						ScreenPage sp = (ScreenPage) pageName.getSelectedItem();
						if (sp != null) {
							displayPage(frame, sp, currentLanguage);
						}
					}
				}
			});
	
			// Language 
	
			final JComboBox list = new JComboBox();
			list.addItem("en");
			list.addItem("es");
			list.addItem("de");
			list.addItem("fr");
			list.addItem("cn");
			list.addItem("pl");
			list.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String language = (String) list.getSelectedItem();
					if (! language.equals(currentLanguage)) {
						ScreenPage sp = (ScreenPage) pageName.getSelectedItem();
						displayPage(frame, sp, language);
					}
				}
			});
			list.setOpaque(false);
			line2.add(list);
			
			// Previous Page Button
			
			previousButton = new JButton("Previous");
			previousButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int index = pageName.getSelectedIndex() - 1;
					if (index >= 0) {
						pageName.setSelectedIndex(index);
					}
				}
			});
			line2.add(previousButton);
			
			// Next Page Button
			
			nextButton = new JButton("Next");
			nextButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int index = pageName.getSelectedIndex() + 1;
					if (index != pageName.getItemCount()) {
						pageName.setSelectedIndex(index);
					}
				}
			});
			line2.add(nextButton);
			
			dimensionsLabel = new JLabel();
			dimensionsLabel.setOpaque(false);
			dimensionsLabel.setForeground(Color.LIGHT_GRAY);
			line2.add(dimensionsLabel);
			
			JButton button3 = new JButton("Exit");
			button3.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					System.exit(0);
				}
			});
			line2.add(button3);
			
			// Page
			
			changeDirectory(DIRECTORIES[0]);
			
			controls.doLayout();
			
			// Window size
			
	        frame.addComponentListener(new ComponentListener() {
	        	
	        	private final int MIN_WIDTH = 650;
	        	private final int MIN_HEIGHT = 530 + ((int) controls.getPreferredSize().getHeight()); 
//	        	private final int MIN_HEIGHT = 530; 
	
				public void componentResized(ComponentEvent e) {
					Dimension size = frame.getSize();
					boolean resize = false;
					int h = (int) size.getHeight();
					if (h < MIN_HEIGHT) {
						h = MIN_HEIGHT;
						resize = true;
					}
					int w = (int) size.getWidth();
					if (w < MIN_WIDTH) {
						w = MIN_WIDTH;
						resize = true;
					}
					if (resize) {
						frame.setSize(w, h);
					}
					
					int sw = (int) displayPanel.getSize().getWidth();
					int sh = (int) displayPanel.getSize().getHeight();
					String text = "Dimensions: " + sw + " x " + sh;
					dimensionsLabel.setText(text);
				}
	
				public void componentMoved(ComponentEvent e) {
				}
	
				public void componentShown(ComponentEvent e) {
				}
	
				public void componentHidden(ComponentEvent e) {
				}
	        });        

	        frame.pack();
	        frame.doLayout();
			int h = ((int) controls.getPreferredSize().getHeight()) + 530;
	        frame.setSize(650, h);
	        int borderWidth = frame.getWidth() - mainPanel.getWidth();
			frame.setVisible(true);
	        
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			e.printStackTrace();
		}
	}
}
