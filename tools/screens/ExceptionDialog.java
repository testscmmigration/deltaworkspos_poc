package screens;

import java.io.OutputStream;
import java.io.PrintStream;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class ExceptionDialog extends PrintStream {
	JFrame frame;

	public ExceptionDialog(OutputStream pm_out, JFrame pm_frame) {
		super(pm_out);
		frame = pm_frame;
		// TODO Auto-generated constructor stub
	}

	/**
	 * Displays a message in an error dialog and sends it to the console.
	 * 
	 * @param pm_sMessage
	 *            The message to be display/printed.
	 */
	public void println(String pm_sMessage) {
		synchronized (this) {
			JOptionPane.showMessageDialog(frame, pm_sMessage, "GUI Exception",
					JOptionPane.ERROR_MESSAGE);
			super.println(pm_sMessage);
		}
	}

}
