package search;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import dw.utility.Debug;

public class PrintlnSearch {

	/**
	 * @param args
	 */
	private static BufferedWriter logFile; 
	
	public static void main(String[] args) {	
		try {		
			String searchDir = null;

        	if(args.length == 2) {
        		searchDir = args[0];
        	    logFile = new BufferedWriter(new FileWriter(args[1]));
        	}
        	else {
        		System.err.println("Please enter the source directory and an error log. \n");
        	    System.exit(1);
        	}
        	
        	File dir = new File (searchDir);
        	checkAllFiles(dir);      	
	    } 
		catch (Exception e) {
	    	//TODO remove println
	    	System.out.println(e);
	    }
	    finally {
	    	try {
	    		if (logFile != null)
				    logFile.close();
			} catch (IOException e) {
				Debug.printStackTrace(e);
			}
	    }
	}
	
//	 Process only files under dir
    public static void checkAllFiles(File dir) throws IOException {
    	String fileName;
    	if (dir.isDirectory()) {
            String[] children = dir.list(); 
            //process all dir and files
            for (int i=0; i<children.length; i++) {
                checkAllFiles(new File(dir, children[i]));
            }
        } else {
        	fileName = dir.toString().substring(dir.toString().lastIndexOf("\\") + 1);
        	// skip Debug.java and only process java files
        	if (!fileName.equalsIgnoreCase("Debug.java")&& fileName.endsWith(".java"))  {
        		//process all files
                search(dir);
        	}
        }
    }
 
	private static void search (File fileName) throws FileNotFoundException {
		BufferedReader in = new BufferedReader(new FileReader(fileName)); 
		String str;
		int lineNumber = 1;
        boolean insideComment = false;
        boolean writeFile = false;  //flag to print the file name
        
		try {
			while ((str = in.readLine()) != null) {
	    		String strLower = str.toLowerCase();
	    		int sysOutIndex = strLower.indexOf("system.out.println");
	    		int sysErrIndex = strLower.indexOf("system.err.println");
	    		int oneLineCommentIndex = str.indexOf("//");
	    		int blockCommentBegIndex = str.indexOf("/*");
	    		int blockCommentEndIndex = str.indexOf("*/");
	    		
	    		//if inside comment, set comment to true
	    		if (!insideComment && blockCommentBegIndex != -1 &&  ((sysOutIndex == -1 && sysErrIndex == -1) // '/*' on a line by itself
	    			|| blockCommentBegIndex < sysOutIndex || blockCommentBegIndex < sysErrIndex))  // '/*' in front of println
	    			insideComment = true;
	    		if (!insideComment) { //not inside comment
		    		if ((sysOutIndex >= 0 && (oneLineCommentIndex > sysOutIndex || oneLineCommentIndex == -1)) 
		    			|| (sysErrIndex >=0 && (oneLineCommentIndex > sysErrIndex || oneLineCommentIndex == -1))){
		    			System.out.println("line " + lineNumber + ":" + str + oneLineCommentIndex + ":" + sysOutIndex + ":" + sysErrIndex);	
		    			//writing out to a file
		    			try {
		    				if (!writeFile) {
		    					logFile.write("\n");
		    					//taking the classname from the path to print
		    					String javaFile = fileName.toString();
		    					javaFile = javaFile.substring(javaFile.lastIndexOf("\\") + 1);
		    					logFile.write(javaFile + "\n");
		    					writeFile = true;
		    				}
		    		        logFile.write("line " + lineNumber + ":  " + str.trim() + "\n");
	
		    		    } catch (IOException e) {
		    		    	System.out.println(e);
		    		    }
		    		}
		    		// if comment block begins after the system println
		    		if (blockCommentBegIndex > sysOutIndex || blockCommentBegIndex > sysErrIndex) {
		    			if (str.lastIndexOf("\"", blockCommentBegIndex) > 0) //if there is a " before the /*
		    				if (countChar(str, blockCommentBegIndex)%2 != 0) //if there is an even # of "
		    					insideComment = true;
		    		}
	    		}
	    		// resets the comment flag when finding an end of comment: */
	    		if (blockCommentEndIndex >= 0)
	    			insideComment = false;
	    		lineNumber++;
	        }
	        in.close();
		}
		catch (IOException e) {
			System.out.println(e);
		}
	}
	
	//counts the # of instances of a char in a string
	private static int countChar (String inString, int fromIndex) {
		int numberOf = 0;

		while (fromIndex != -1) {
			System.out.println(fromIndex + "\n");
		    fromIndex = inString.lastIndexOf("\"", fromIndex-1);
		    if (fromIndex >= 0)
		    	numberOf++;		
		}
		return numberOf;
	}
}
