package profile;

import java.io.FileInputStream;

import dw.profile.FeeTable;
import dw.profile.Profile;
import dw.profile.ProfileItem;
import dw.profile.ProfileLoader;
import dw.utility.Debug;

public class ProfileLoaderRunner {
	  /**
     * Quick test.
     */
	public static void main(String args[]) {
		if (args.length < 1) {
			Debug.println("usage: java ProfileLoader filename.xml"); //$NON-NLS-1$
			System.exit(1);
		}
		try {
			FileInputStream input = new FileInputStream(args[0]);
			ProfileLoader loader = new ProfileLoader();
			boolean verify = false;
			if (args.length > 1)
				verify = Boolean.getBoolean(args[1]);

			Profile rootProfile = loader.load(input, verify);

			Debug.println(rootProfile.element); // should print PROFILE

			Profile userProfile = rootProfile.find("MGRDFLT", "456"); //$NON-NLS-1$ //$NON-NLS-2$
			Debug.println(String.valueOf(userProfile.id)); // should print 41

			Profile productProfile = (Profile) rootProfile.find("PRODUCT", 1); //$NON-NLS-1$
			FeeTable feeTable = (FeeTable) productProfile.find("FEE_TABLE"); //$NON-NLS-1$
			java.math.BigDecimal amount = new java.math.BigDecimal("200.00"); //$NON-NLS-1$
			Debug.println(feeTable.getFee(amount).toString()); // should print
																// 0.49

			ProfileItem item = rootProfile.find("AGENT_NAME"); //$NON-NLS-1$
			Debug.println(item.stringValue());
			// should print Al's Bank & Check Barn
		} catch (Exception e) {
			Debug.printStackTrace(e);
		}
	}
}
