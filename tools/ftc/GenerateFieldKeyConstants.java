package ftc;

import java.io.FileReader;
import java.io.LineNumberReader;
import java.io.Reader;

public class GenerateFieldKeyConstants {

	public static void main(String[] args) {
		try {
			Reader r = new FileReader(args[0]);
			LineNumberReader lnr = new LineNumberReader(r);
			while (true) {
				String line = lnr.readLine();
				if (line == null) {
					break;
				}
				
				// public static final FieldKey THIRD_PARTY_SENDER_ADDRESS = FieldKey.key("thirdParty_Sender_Address");
				
				String key = line.trim().replaceAll("\\.", "_");
				String name = key.toUpperCase() + "_KEY";
				System.out.println("public static final FieldKey " + name + " = FieldKey.key(\"" + key + "\");");
			}
			lnr.close();
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
		}
	}
}
