:: Version number:
@set BUILD_VERSION=2999

:: Build label:
@set BUILD_LABEL=2.9 bld 99

:: Build description:
@set BUILD_DESCRIPTION=description

:: Dependent JRE name:
@set JRE_NAME=jre140

:: JRE manifest size:
@set JRE_MANIFEST_SIZE=59755

