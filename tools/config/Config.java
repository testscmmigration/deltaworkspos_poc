package config;

import java.io.*;
import java.util.*;
import java.util.regex.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/** 
 * General purpose configuration tool. Reads and writes a file called 
 * settings.bat of the form:
 * <pre>
 * :: Version number:
 * @set BUILD_VERSION=2999
 * 
 * :: Build label:
 * @set BUILD_LABEL=2.9 bld 99
 * </pre>
 * This class also demonstrates the new regular expression facility,
 * and uses it to be reasonably forgiving about the input format.
 * @author Geoff Atkin
 */
public class Config implements ActionListener {

    String filename;
    Pattern blankPattern = Pattern.compile("^\\s*$");
    Pattern labelPattern = Pattern.compile("^\\#+\\s*(.*)");
    //Pattern settingPattern = Pattern.compile("^\\@?set (\\S+)\\s*\\=(.*)");
    Pattern settingPattern = Pattern.compile("^\\=+\\s*(.*)");
    
    Matcher blankMatcher, labelMatcher, settingMatcher;
    
    LinkedList settings;

    final int GAP = 5;
    final String SAVE = "save";
    final String CANCEL = "cancel";
    
    Config(String filename) {
        this.filename = filename;
        settings = new LinkedList();
    }
    
    /** Reads the settings file. */
    void readSettings() throws IOException {
        BufferedReader input = new BufferedReader(new FileReader(filename));
        String line;
        String label = null;
        
        while ((line = input.readLine()) != null) {
            blankMatcher = blankPattern.matcher(line);
            if (blankMatcher.matches())
                continue;
            
            labelMatcher = labelPattern.matcher(line);
            if (labelMatcher.matches()) {
                label = labelMatcher.group(1);
                continue;
            }

            settingMatcher = settingPattern.matcher(line);
            if (settingMatcher.matches()) { 
                Setting s = new Setting(label, settingMatcher.group(1),
                        settingMatcher.group(2));
                settings.add(s);
                label = null;
                continue;
            }

            settingMatcher = settingPattern.matcher(line);
            if (line.indexOf("=")>0) { 
            	String propertyName = line.substring(0, line.indexOf("=")).trim();
            	String propertyValue = line.substring(line.indexOf("=")+1).trim();
                Setting s = new Setting(label, propertyName,
                        propertyValue);
                settings.add(s);
                label = null;
                continue;
            }

            
            throw new IOException("unrecognized input: " + line);
        }
        input.close();
    }

    /** Shows the GUI. */
    void show() {
        JPanel input = new JPanel(new GridLayout(0, 2, GAP, GAP));
        populate(input);

        JPanel box = new JPanel();
        box.setLayout(new BoxLayout(box, BoxLayout.X_AXIS));
        box.add(Box.createHorizontalGlue());
        box.add(createButton("OK", SAVE, this));
        box.add(Box.createHorizontalStrut(GAP));
        box.add(createButton("Cancel", CANCEL, this));
                    
        JPanel content = new JPanel(new BorderLayout(GAP, GAP));
        content.setBorder(BorderFactory.createEmptyBorder(GAP,GAP,GAP,GAP));
        content.add(input, BorderLayout.NORTH);
        content.add(box, BorderLayout.SOUTH);

        JFrame frame = new JFrame();
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(1);
            }
        });
        frame.setTitle("Configurator");
        frame.setSize(400, 400);
        frame.setContentPane(content);
        frame.setVisible(true);
    }
    
    /** Adds labels and textfields to the panel, based on the loaded settings. */
    void populate(JPanel panel) {
        Iterator i = settings.iterator();
        while (i.hasNext()) {
            Setting s = (Setting) i.next();
            panel.add(new JLabel(s.label));
            panel.add(s.field);
        }
    }
    
    /** Dumps the list of settings to System.out. */
    void debug() {
        Iterator i = settings.iterator();
        while (i.hasNext()) {
            System.out.println(i.next());
        }
    }

    /** Save the (possibly updated) values to the settings file. */
    void saveSettings() throws IOException {
    	System.out.println("writing file "+filename);
        PrintWriter output = new PrintWriter(
                new BufferedWriter(new FileWriter(filename)));
        //PrintStream output = System.out;
        Iterator i = settings.iterator();
        while (i.hasNext()) {
            Setting s = (Setting) i.next();
            output.println("# " + s.label);
            output.println(s.name + "=" + s.getText());
            output.println();
        }
        output.close();
        //writePropertiesFile();
    }
    
    /** Save the (possibly updated) values to the settings file. */
    void writePropertiesFile() throws IOException {
    	String propFileName = 
    			filename.substring(0, filename.indexOf('.'))+".properties";
    	
    	PrintWriter output = new PrintWriter(
                new BufferedWriter(new FileWriter(propFileName)));
        Iterator i = settings.iterator();
        while (i.hasNext()) {
            Setting s = (Setting) i.next();
            String propertiesFileStyleLabel = s.label.replaceFirst("rem", "");
            output.println("# " + propertiesFileStyleLabel);
            output.println(s.name + "=" + s.getText());
            output.println();
        }
        output.close();
    }
    
    
    /**
     * Creates a JButton with the given label, action command, and 
     * action listener. 
     */
    private JButton createButton(String text, String command, 
            ActionListener listener) {
        JButton result = new JButton(text);
        result.setActionCommand(command);
        result.addActionListener(listener);
        return result;
    }

    /** Top level event handler. */
    public void actionPerformed(ActionEvent ae) {
        String command = ae.getActionCommand();
        
        try {
            if (command.equals(SAVE)) {
                saveSettings();
                System.exit(0);
            }
            else if (command.equals(CANCEL)) {
                System.exit(1);
            }
        }
        catch (Exception e) {
            JOptionPane.showMessageDialog(null, 
                    new String[] {
                        "Unable to " + command + ".",
                        e.getClass().getName() + ":",
                        e.getMessage() },
                    "Error", 
                    JOptionPane.ERROR_MESSAGE); 
        }
    }

    /** Reads the settings file and shows the GUI. */
    public static void main(String args[]) {
        try {
        	String inputFileName = null;
        	if(args.length > 0)
        		inputFileName = args[0];
        	else
        		inputFileName = "settings.properties";
        	
            // Use the native Windows look and feel
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            // Workaround for Sun bug 4420843 and 4640057
            UIManager.put("Application.useSystemFontSettings", Boolean.FALSE);

            System.out.println("reading file "+inputFileName);
            Config instance = new Config(inputFileName);
            instance.readSettings();
            instance.show();
        }
        catch (Exception e) {
            System.out.println(e);
            System.exit(1);
        }
    }

}

/** Represents a "label: name=value" setting. */
class Setting {
    String label;
    String name;
    String value;
    JTextField field;

    Setting(String label, String name, String value) {
        this.name = name;
        this.value = value;
        this.label = (label == null) ? defaultLabel(name) : label;
        this.field = new JTextField();
        field.setText(value);
    }

    /** Creates a default label from the variable name. */
    public static String defaultLabel(String s) {
        StringBuffer sb = new StringBuffer();
        
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (i == 0) {
                sb.append(Character.toUpperCase(c));
            }
            else if (c == '_') {
                sb.append(' ');
            }
            else {
                sb.append(Character.toLowerCase(c));
            }
        }
        sb.append('#');
        return sb.toString();
    }

    /** Returns the (possibly updated) value. */
    public String getText() {
        return field.getText();
    }
    
    /** Produce string representation for debug purposes. */
    public String toString() {
        return label + " " + name + "=" + getText();
    }
}
