program DW_ISI;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, Main, Utils, IsiCsv, Help, MemoWindow;

{$R *.res}

begin
  RequireDerivedFormResource:=True;
  Application.Initialize;
  Application.CreateForm(TfMainScreen, fMainScreen);
  Application.CreateForm(TisiCsvFileWindow, isiCsvFileWindow);
  Application.Run;
end.

