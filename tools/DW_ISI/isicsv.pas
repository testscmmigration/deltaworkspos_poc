unit IsiCsv;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, csvdocument;

function IsiCsvFileExists(): boolean;
function IsiCsv2FileExists(): boolean;
function IsiCvsProcessCsvFile(): string;
function IsiCvsProcessCsvFile2(): string;
procedure IsiCvsSetDir(csvDir: string);

var
  dirName: string = '';
  csvFile: string;
  csv2File: string;
  AMD_FIELDS: TStringArray;
  CAN_FIELDS: TStringArray;
  CNL_FIELDS: TStringArray;
  DOC_FIELDS: TStringArray;
  ERR_FIELDS: TStringArray;
  EXP_FIELDS: TStringArray;
  MGR_FIELDS: TStringArray;
  MGS_FIELDS: TStringArray;
  PCD_FIELDS: TStringArray;
  RFN_FIELDS: TStringArray;
  RRT_FIELDS: TStringArray;
  SDA_FIELDS: TStringArray;
  ST1_FIELDS: TStringArray;
  ST2_FIELDS: TStringArray;

const
  _ISICSV = 'DWISI.CSV';
  _ISICSV2 = 'MGTEXT.CSV';
  DATA_START = 7;

implementation

{
   IsiCsvFileExists - Returns true/false if DW DWISI.CSV file exists
}
function IsiCsvFileExists(): boolean;
begin
  IsiCsvFileExists := FileExists(csvFile);
end;

{
   IsiCsv2FileExists - Returns true/false if DW MGText.CSV file exists
}
function IsiCsv2FileExists(): boolean;
begin
  IsiCsv2FileExists := FileExists(csv2File);
end;

{
   decodeIsiRecordData - Return a string containing all the non-header ISI
   MGS record fields from the ISI CSV file in the specified row (row).
}
function decodeIsiRecordData(fileName: string; row: integer;
  FieldNames: TStringArray): string;
var
  col: integer;
  field: integer = 0;
  message: string = '';
  isiCvsFile: TCSVDocument;

begin
  isiCvsFile := TCSVDocument.Create;
  isiCvsFile.LoadFromFile(fileName);

  for col := DATA_START to isiCvsFile.ColCount[row] - 1 do
  begin
    if field <= length(FieldNames) - 1 then
    begin
      message := message + FieldNames[field] + ' = ' +
        isiCvsFile.Cells[col, row] + LineEnding;
      Inc(field);
    end
    else
    begin
      message := message + isiCvsFile.Cells[col, row] + ', ';
    end;
  end;

  decodeIsiRecordData := message;
end;

{
   decodeIsiRecordNoHeader - Return a string containing all the non-header ISI
   MGS record fields from the ISI CSV file in the specified row (row).
}
function decodeIsiRecordNoHeader(fileName: string; row: integer;
  FieldNames: TStringArray): string;
var
  col: integer;
  field: integer = 0;
  message: string = '';
  isiCvsFile: TCSVDocument;

begin
  isiCvsFile := TCSVDocument.Create;
  isiCvsFile.LoadFromFile(fileName);

  for col := 1 to isiCvsFile.ColCount[row] - 1 do
  begin
    if field <= length(FieldNames) - 1 then
    begin
      message := message + FieldNames[field] + ' = ' +
        isiCvsFile.Cells[col, row] + LineEnding;
      Inc(field);
    end
    else
    begin
      message := message + isiCvsFile.Cells[col, row] + ', ';
    end;
  end;

  decodeIsiRecordNoHeader := message;
end;

{
   dumpIsiRecordData - Return a string containing all the non-header ISI record
   fields from the ISI CSV file in the specified row (row).
}

function dumpIsiRecordData(fileName: string; row: integer): string;
var
  col: integer;
  field: integer = 7;
  message: string = '';
  isiCvsFile: TCSVDocument;

begin
  isiCvsFile := TCSVDocument.Create;
  isiCvsFile.LoadFromFile(fileName);

  message := message + 'Record Specific Data = ' + LineEnding;

  for col := field to isiCvsFile.ColCount[row] - 1 do
  begin
    if col > field then
    begin
      message := message + ', ';
    end;
    message := message + isiCvsFile.Cells[col, row];
  end;
  dumpIsiRecordData := message + LineEnding;
end;

{
   processIsiCvsFile - Processes an ISI CSV file.
}
function processCsvFile(fileName: string): string;
var
  c: TCSVDocument;
  rows: integer;
  row: integer;
  field: integer;
  fieldValue: string;
  recType: string;
  message: string = '';
  headerFields: TStringArray;

begin
  headerFields := TStringArray.Create('Record Type = ', 'App Mode = ',
    'TimeStamp = ', 'Operator ID = ', 'Operator Name = ', 'MG Account = ', 'Agent ID = ');

  c := TCSVDocument.Create;
  c.LoadFromFile(fileName);
  rows := c.RowCount;
  message := '';

  for row := 0 to Rows - 1 do
  begin

    if (row > 0) then
    begin
      message := message + LineEnding + LineEnding;
    end;

    // Parse header

    //Record Type
    field := 0;
    recType := c.Cells[field, row];
    message := message + headerFields[field] + recType;
    case recType of
      'AMD': message := message + ' (Amend)';
      'CAN': message := message + ' (Send Reversal/Cancel)';
      'CNL': message := message + ' (Cancel)';
      'DOC': message := message + ' (Financial Document)';
      'DNS': message := message + ' (Did Not Start)';
      'ERR': message := message + ' (Error)';
      'EXP': message := message + ' (Bill Payment)';
      'MGS': message := message + ' (MoneyGram Send)';
      'MGR': message := message + ' (MoneyGram Receive)';
      'NON': message := message + ' (Non-Financial)';
      'RFN': message := message + ' (Send Reversal/Refund)';
      'RRT': message := message + ' (Receive Reversal)';
      'SDA': message := message + ' (Send Detail Amounts)';
      'ST1': message := message + ' (Send Text 1)';
      'ST2': message := message + ' (Send Text 2)';
      'PCD':
      begin
        message := message + ' (Promotional Codes)' + LineEnding;
        message := message + decodeIsiRecordNoHeader(fileName, row, PCD_FIELDS);
        Continue;
      end;
    end;
    message := message + LineEnding;

    field := 1;
    fieldValue := c.Cells[field, row];
    message := message + headerFields[field] + fieldValue;
    case fieldValue of
      'P': message := message + ' (Production)';
      'D': message := message + ' (Demo)';
      'T': message := message + ' (Training)';
    end;
    message := message + LineEnding;

    field := 2;
    fieldValue := c.Cells[field, row];
    message := message + headerFields[field] + fieldValue + ' (' +
      copy(fieldValue, 1, 4) + '-' + copy(fieldValue, 5, 2) +
      '-' + copy(fieldValue, 7, 2) + ', ' + copy(fieldValue, 9, 8) +
      ')' + LineEnding;

    for field := 3 to length(headerFields) - 1 do
    begin
      if (field < c.ColCount[row]) then
        message := message + headerFields[field] + c.Cells[field, row] + LineEnding;
    end;

    //Rest of record
    Inc(field);
    case recType of
      'AMD': message := message + decodeIsiRecordData(fileName, row, AMD_FIELDS);
      'CAN': message := message + decodeIsiRecordData(fileName, row, CAN_FIELDS);
      'CNL': message := message + decodeIsiRecordData(fileName, row, CNL_FIELDS);
      'DOC': message := message + decodeIsiRecordData(fileName, row, DOC_FIELDS);
      'DNS': message := message + dumpIsiRecordData(fileName, row);
      'ERR': message := message + decodeIsiRecordData(fileName, row, ERR_FIELDS);
      'EXP': message := message + decodeIsiRecordData(fileName, row, EXP_FIELDS);
      'MGS': message := message + decodeIsiRecordData(fileName, row, MGS_FIELDS);
      'MGR': message := message + decodeIsiRecordData(fileName, row, MGR_FIELDS);
      'NON': message := message + dumpIsiRecordData(fileName, row);
      'PCD': message := message + decodeIsiRecordData(fileName, row, PCD_FIELDS);
      'RFN': message := message + decodeIsiRecordData(fileName, row, RFN_FIELDS);
      'RRT': message := message + decodeIsiRecordData(fileName, row, RRT_FIELDS);
      'SDA': message := message + decodeIsiRecordData(fileName, row, SDA_FIELDS);
      'ST1': message := message + decodeIsiRecordData(fileName, row, ST1_FIELDS);
      'ST2': message := message + decodeIsiRecordData(fileName, row, ST2_FIELDS);
      else
        message := message + dumpIsiRecordData(fileName, row);
    end;
  end;
  processCsvFile := message;
end;

{
   IsiCvsProcessCsvFile - Processes an DWISI.CSV file.
}
function IsiCvsProcessCsvFile(): string;
begin
  IsiCvsProcessCsvFile := processCsvFile(csvFile);
end;

{
   IsiCvsProcessCsvFile2 - Processes an MGTEXT.CSV file.
}
function IsiCvsProcessCsvFile2(): string;

begin
  IsiCvsProcessCsvFile2 := processCsvFile(csv2File);
end;

{
 IsiCvsSetDir - Sets the directory where the CSV file can be found for use by
 all procedures and functions in this unit.
}
procedure IsiCvsSetDir(csvDir: string);
begin
  dirName := csvDir;
  csvFile := dirName + DirectorySeparator + _ISICSV;
  csv2File := dirName + DirectorySeparator + _ISICSV2;
end;

initialization
  csvFile := dirName + DirectorySeparator + _ISICSV;
  csv2File := dirName + DirectorySeparator + _ISICSV2;


  AMD_FIELDS := TStringArray.Create('Reference Number',
    'Previous Receiver’s First Name', 'Previous Receiver’s Last Name',
    'Previous Receiver’s 2nd Last Name', 'Receiver’s First Name',
    'Receiver’s Last Name', 'Receiver’s 2nd Last Name');

  CAN_FIELDS := TStringArray.Create('Reference Number', 'Send amount',
    'Fee amount', 'Fee refunded');

  CNL_FIELDS := TStringArray.Create('SubStatus Code');

  DOC_FIELDS := TStringArray.Create('Document type', 'Document number',
    'Void flag', 'Limbo flag', 'Period', 'Serial number', 'Item face amount',
    'Item fee', 'Remote Issuance Flag', 'Sale issue tag (alter)',
    'Face discount amount', 'Fee discount amount', 'Face discount percentage',
    'Fee discount percentage', 'Payee number', 'Payee name', 'Purchaser',
    'Reference number');

  ERR_FIELDS := TStringArray.Create('Error Code');

  EXP_FIELDS := TStringArray.Create('Destination Country',
    'Sender’s first name', 'Sender’s middle name/initial', 'Sender’s last name',
    'Sender’s home phone', 'Sender’s address', 'Sender’s city',
    'Sender’s state', 'Sender’s zip', 'Sender’s country', 'Sender’s date of birth',
    'Sender’s photo ID type', 'Sender’s photo ID number',
    'Sender’s photo ID state', 'Sender’s photo ID country',
    'Sender’s legal ID type', 'Sender’s legal ID number',
    'Sender’s bank account number', 'Sender’s occupation', 'Agency ID',
    'Agency account number', 'Agency name', 'Agency city', 'Message field 1',
    'Message field 2', '3rd party first name', '3rd party middle initial',
    '3rd party last name', '3rd party address', '3rd party city',
    '3rd party state', '3rd party country', '3rd party zip',
    '3rd party legal ID type', '3rd party legal ID number',
    '3rd party date of birth', '3rd party occupation', '3rd party organization',
    'Frequent customer card number', 'Reference number', 'Send amount',
    'Fee amount', 'Total amount', 'Send currency', 'Exchange rate',
    'Bill Payment Type', 'Receive Agent ID', 'Beneficiary First Name',
    'Beneficiary Middle Name', 'Beneficiary Last Name', 'Purpose of Funds');

  MGR_FIELDS := TStringArray.Create('Reference number',
    'Check Number 1', 'Check 1 Amount', 'Authorization Code', 'Check Number 2',
    'Check 2 Amount', 'Receive currency', 'Sender’s first name',
    'Sender’s middle name/initial', 'Sender’s last name',
    'Receiver’s first name', 'Receiver’s middle name/initial',
    'Receiver’s last name', 'Receiver’s 2nd last name',
    'Receiver’s phone number', 'Receiver’s address', 'Receiver’s city',
    'Receiver’s state', 'Receiver’s zip', 'Receiver’s photo ID type',
    'Receiver’s photo ID number', 'Receiver’s photo ID state',
    'Receiver’s photo ID country', 'Receiver’s legal ID type',
    'Receiver’s legal ID number', 'Receiver’s date of birth',
    'Receiver’s occupation', '3rd party first name', '3rd party last name',
    '3rd party address', '3rd party city', '3rd party state',
    '3rd party zip', '3rd party country', '3rd party legal ID type',
    '3rd party legal ID number', '3rd party date of birth',
    '3rd party occupation', '3rd party organization',
    'Receiver birth city', 'Receiver birth country',
    'Receiver’s passport date of issue', 'Receiver’s passport issue city',
    'Receiver’s passport issue country', 'Send Country',
    'Check 1 Type', 'Check 2 Type', 'Send amount', 'Send Fee amount',
    'Send Currency', 'Exchange rate', 'Payout Type', 'CARD Payout amount',
    'Card Swipe Used', 'Last 4 digits of Card number', 'Sender’s address',
    'Sender’s city', 'Sender’s state', 'Sender’s zip', 'Sender’s country',
    'Sender’s 2nd last name', 'Sender’s home phone', 'Receiver Message',
    'Receiver’s country');

  MGS_FIELDS := TStringArray.Create('Destination Country',
    'Sender’s first name', 'Sender’s middle name/initial', 'Sender’s last name',
    'Sender’s home phone', 'Sender’s address', 'Sender’s city',
    'Sender’s state', 'Sender’s zip', 'Sender’s country', 'Sender’s date of birth',
    'Sender’s birth city', 'Sender’s birth country', 'Sender’s passport issue city',
    'Sender’s passport issue country', 'Sender’s passport date of issue',
    'Sender’s photo ID type', 'Sender’s photo ID number', 'Sender’s photo ID state',
    'Sender’s photo ID country', 'Sender’s legal ID type',
    'Sender’s legal ID number', 'Sender’s bank account number',
    'Sender’s occupation', 'Receiver’s first name', 'Receiver’s middle name/initial',
    'Receiver’s last name', 'Receiver’s 2nd last name', 'Receiver’s phone number',
    'Receiver’s address', 'Receiver’s Directions 1', 'Receiver’s Directions 2',
    'Receiver’s Directions 3', 'Receiver’s colonia', 'Receiver’s municipio',
    'Receiver’s city', 'Receiver’s state', 'Receiver’s zip', 'Test question',
    'Test answer', 'Message field 1', 'Message field 2', '3rd party first name',
    '3rd party middle initial', '3rd party last name', '3rd party address',
    '3rd party city', '3rd party state', '3rd party country', '3rd party zip',
    '3rd party legal ID type', '3rd party legal ID number',
    '3rd party date of birth', '3rd party occupation', '3rd party organization',
    'Delivery Option', 'Frequent customer card number', 'Reference number',
    'Free phone call PIN', 'Free phone call phone number', 'Send amount',
    'Fee amount', 'Discount Amount', 'Discount Type', 'Total send amount',
    'Send currency', 'Receive amount', 'Receive currency', 'Exchange rate',
    'Directed send confirmation number', 'Receiver Registration Number',
    'Expected Delivery Date', 'Approver ID', 'Original Transfer Fee Excluding Tax',
    'Total Discount Amount', 'Total Fee Amount', 'Tax Amount',
    'Promotion Code Count', 'Receiver’s Country', 'Sender’s 2nd last name');

  PCD_FIELDS := TStringArray.Create('Promotional Code',
    'Promotion Category ID', 'Promotion Discount ID', 'Promotion Discount Value',
    'Promotion Discount Amount');

  RFN_FIELDS := TStringArray.Create('Reference Number',
    'Send amount', 'Fee amount', 'Fee refunded', 'Check 1 Type', 'Check 2 Type');

  RRT_FIELDS := TStringArray.Create('Reference Number', 'Receive amount');

  SDA_FIELDS := TStringArray.Create('Send Amount', 'Send Currency',
    'Total Send Fees', 'Total Discount Amount', 'Total Send Taxes',
    'Total Amount to Collect', 'Fee Send Mgi Original No Tax',
    'Fee Send Non Mgi', 'Total Receive Taxes', 'Fee Send Total Orig No Tax',
    'Fee Send Total Final No Tax', 'Tax Send Mgi', 'Tax Send Non Mgi',
    'Fee Send Mgi Collected And Tax', 'Total Amount Mgi',
    'Fee Send Total With Tax', 'Fee Send Non Mgi With Tax',
    'Receive Amount Original', 'Receive Currency', 'Valid',
    'Payout Currency', 'Fee Receive Total', 'Tax Receive Total',
    'Receive Amount Final', 'Fee Receive MGI', 'Fee Receive Other',
    'Tax Receive MGI', 'Tax Receive Other', 'Foreign Exchange',
    'Has Fee Disclosure', 'Has Tax Disclosure', 'Has Estimated Fee',
    'Has Estimated Tax');

  ST1_FIELDS := TStringArray.Create('Primary Language',
    'Primary Language Text', 'Secondary Language', 'Secondary Language Text');

  ST2_FIELDS := TStringArray.Create('Primary Language',
    'Primary Language Text', 'Secondary Language', 'Secondary Language Text');

end.




















