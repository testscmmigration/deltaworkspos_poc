unit Help;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;
var
  helpText: string = 'Welcome to the DeltaWorks ISI Test program' +
    LineEnding + LineEnding +
  'This program sends ISI (In Store Integration) commands to a DeltaWorks' +
    '(DW) installation.' + LineEnding + LineEnding +
  ' The first step is to define the location of' +
    ' the DeltaWorks directory. The program will search common locations for' +
    ' a DW installations and default to the first one it finds.  If it does' +
    ' not find one, or it is not the one you want to use, then use the "Change"' +
    ' button to select a new directory.' + LineEnding + LineEnding +
  ' You can use the checkboxes to select ISI commands to send to the selected' +
    ' installation of DW.' + LineEnding + LineEnding +
  ' If you choose to send a -EXE command you can type in a command to send,' +
    ' or select one from the dropdown list.  Additionally parameters can' +
    ' optionally be included.  This program is supplied with two companion' +
    ' programs (DwIsiCb.bat and DwIsiCb2.bat).  If these companion programs are ' +
    ' located on the defined PATH and are specified as the first parameter, then ' +
    ' if they are executed they will create a file(s) (DwIsiCb.txt, DwIsiCb2.txt)' +
    ' that this program can detect and provide a visual representation that a ' +
    ' CallBack was performed by DW.' + LineEnding + LineEnding +
  '"Execute" will send the command to DW.  "Execute" will be disabled until' +
    'a valid DW directory has been selected' + LineEnding + LineEnding +
  '"Display CSV" will send the contents of DWISI.CSV. If DWISI.CVS does' +
    ' not exist,  "Display CSV" will be disabled.  If executing an ISI command' +
    ' resulted in a DWISI.CVS file being created, "Display CSV" will be enabled.' +
    LineEnding + LineEnding +
  '"Rename CSV" will send the contents of DWISI.CSV.  If DWISI.CVS does' +
    ' not exist,  "Rename CSV" will be disabled.  If executing an ISI command' +
    ' resulted in a DWISI.CVS file being created, "Rename CSV" will be enabled.';
implementation

end.

