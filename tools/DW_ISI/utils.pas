unit Utils;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Dialogs, Forms, MemoWindow;

procedure msgInfoBox(parent: TComponent; message: string);
procedure msgErrBox(parent: TComponent; message: string);
procedure msgMemo(message: string);


implementation

{
  MessageDlg - Displays a Message Dialog centered in the parent
}
function MessageDlg(parent: TComponent; const Msg: string; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons): integer;
var
  Dialog: TForm;
begin
  Dialog := CreateMessageDialog(Msg, DlgType, Buttons);
  try
    parent.InsertComponent(Dialog);
    Dialog.Position := poOwnerFormCenter;
    Result := Dialog.ShowModal
  finally
    Dialog.Free
  end;
end;

{
  msgInfoBox - Displays an Informational Message Dialog
}
procedure msgInfoBox(parent: TComponent; message: string);
begin
  MessageDlg(parent, message, mtInformation, [mbOK]);
end;

{
  msgErrBox - Displays an Error Message Dialog
}
procedure msgErrBox(parent: TComponent; message: string);
begin
  MessageDlg(parent, message, mtError, [mbOK]);
end;

{
  msgMemo - Displays an Informational Memo Panel
}
procedure msgMemo(message: string);
begin
  isiCsvFileWindow.Memo1.Text := message;
  isiCsvFileWindow.Show();
end;

end.

