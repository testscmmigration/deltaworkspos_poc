unit Main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, Menus, Process, fileinfo, Utils, IsiCsv, Help;

type

  { TfMainScreen }

  TfMainScreen = class(TForm)
    btnExecute: TButton;
    btnChangeDWLocation: TButton;
    btnDsplCsv: TButton;
    btnDsplCsv1: TButton;
    btnRenameCsv: TButton;
    btnRenameCsv1: TButton;
    btnUseDefaults: TButton;
    cbxUseFront: TCheckBox;
    cbxUseBack: TCheckBox;
    cbxUseExe: TCheckBox;
    cbxUseRP: TCheckBox;
    cbxUseExe2: TCheckBox;
    cbxUseNf: TCheckBox;
    cmbExeCmd: TComboBox;
    eExe2Params: TEdit;
    eNfParams: TEdit;
    eRemotePW: TEdit;
    eExeParams: TEdit;
    lblExeParms: TLabel;
    lblExe2Parms: TLabel;
    lblNfParms: TLabel;
    lblDWLocation: TLabel;
    lblDWLocationLabel: TLabel;
    MainMenu1: TMainMenu;
    mniHelp: TMenuItem;
    mniAboutBox: TMenuItem;
    SelectDWLocation: TSelectDirectoryDialog;
    timCsvFileCheck: TTimer;
    timCbFileCheck: TTimer;
    timCb2FileCheck: TTimer;
    procedure btnChangeDWLocationClick(Sender: TObject);
    procedure btnExecuteClick(Sender: TObject);
    procedure btnDsplCsvClick(Sender: TObject);
    procedure btnDsplCsv1Click(Sender: TObject);
    procedure btnRenameCsvClick(Sender: TObject);
    procedure btnRenameCsv1Click(Sender: TObject);
    procedure btnUseDefaultsClick(Sender: TObject);
    function cbFileExists(): boolean;
    function cb2FileExists(): boolean;
    procedure deleteCbFile();
    procedure deleteCb2File();
    procedure FormCreate(Sender: TObject);
    procedure mniAboutBoxClick(Sender: TObject);
    procedure mniHelpClick(Sender: TObject);
    procedure setCsvButtonState();
    function  setupDirectory(): string;
    procedure timCb2FileCheckTimer(Sender: TObject);
    procedure timCbFileCheckTimer(Sender: TObject);
    procedure timCsvFileCheckTimer(Sender: TObject);

  private
    { private declarations }
  public
    { public declarations }
  end;

type
  TStringArray = array of string;

var
  fMainScreen: TfMainScreen;
  P: TProcess;
  dirName: string = '';
  functionList: array[1..12, 1..2] of
  string = (('Send (MGS)', 'MGS'), ('Receive (MGR)', 'MGR'),
    ('Bill Payment (EXP)', 'EXP'), ('Money Order (MOD)', 'MOD'),
    ('Vendor Payment (VPD)', 'VPD'), ('Send Quote (MGQ)', 'MGQ'),
    ('Directory of Billers (DOB)', 'DOB'), ('Send Reversal (REV)', 'REV'),
    ('Amend (AMD)', 'AMD'), ('Receive Reversal (RRT)', 'RRT'),
    ('FF Send/ExpressPayment (MGP)', 'MGP'), ('Account Deposit (MGA)', 'MGA'));

const
  _BACK = '-BACK';
  _DWISI = 'DWISI.exe';
  _EXE = '-EXE ';
  _EXE2 = '-EXE2 ';
  _NF = '-NF ';
  _RP = '-RP ';
  _FRONT = '-FRONT';
  _ISI_CB_EXE_OUTPUT = 'DwIsiCb.txt';
  _ISI_CB_EXE2_OUTPUT = 'DwIsiCb2.txt';
  _DIR_FILE = 'lastDir.txt';

implementation

{$R *.lfm}

{ TfMainScreen }

procedure saveDirectory(directory: string);
var
  dirFile: TextFile;

begin
  AssignFile(dirFile, _DIR_FILE);
  Try
    {$I-} // without this, if rewrite fails then a runtime error will be generated
     Rewrite(dirFile);
    {$I+}
     Writeln(dirFile,directory);
  Finally
     CloseFile(dirFile);
  End;

end;

function TfMainScreen.setupDirectory(): string;
var
  CommonLocations: TStringArray;
  dirFile: text;
  dirName: string = '';
  done: boolean = False;
  savedDirectory: string;
  temp: string;

begin

  if FileExists(_DIR_FILE) then
  begin
     AssignFile(dirFile, _DIR_FILE);
     Reset(dirFile);
     ReadLn(dirFile, savedDirectory);
     if FileExists(savedDirectory + DirectorySeparator + _DWISI) then
     begin
          done := True;
          dirName := savedDirectory;
          IsiCvsSetDir(dirName);
          lblDWLocation.Caption := savedDirectory;
          btnExecute.Enabled := True;
     end;
     CloseFile(dirFile);
  end;
  if not done then
  begin
    CommonLocations := TStringArray.Create(
      'C:\DeltaWorks', 'C:\Program Files\Deltaworks',
      'C:\Program Files (x86)\DeltaWorks', 'C:\WSADdev\DW_POS\DeltaWorksPOS1701\Dev');
    for temp in CommonLocations do
    begin
      if FileExists(temp + DirectorySeparator + _DWISI) then
      begin
        dirName := temp;
        IsiCvsSetDir(dirName);
        lblDWLocation.Caption := temp;
        btnExecute.Enabled := True;
        saveDirectory(dirName);
        break;
      end;
    end;
  end;
  setupDirectory := dirName;
end;

{
   btnChangeDWLocationClick - Process Change button.
}
procedure TfMainScreen.btnChangeDWLocationClick(Sender: TObject);
var
  DwisiWithPath: string;

begin
  { execute an open file dialog }
  SelectDWLocation.InitialDir := dirName;

  if SelectDWLocation.Execute then
  begin
    { first check if file exists }
    dirName := SelectDWLocation.FileName;
    IsiCvsSetDir(dirName);
    if DirectoryExists(dirName) then
    begin
      DwisiWithPath := dirName + DirectorySeparator + _DWISI;
      if FileExists(DwisiWithPath) then
      begin
        lblDWLocation.Caption := dirName;
        btnExecute.Enabled := True;
        saveDirectory(dirName);
      end
      else
        msgErrBox(Self, 'DWISI not found in ' + dirName);
    end
    else
      msgErrBox(Self, 'Directory does not exist.');
  end
  else
  begin
    msgInfoBox(Self, 'Directory Selection Canceled');
  end;
end;

{
   btnExecuteClick - Process Execute button.
}
procedure TfMainScreen.btnExecuteClick(Sender: TObject);

begin
  P := TProcess.Create(nil);
  try
    P.Executable := 'C:\Windows\system32\cmd.exe';
    P.Parameters.Add('/c');
    P.CurrentDirectory := dirName;
    P.Parameters.Add(_DWISI);

    if cbxUseFront.Checked then
      P.Parameters.Add(_FRONT);

    if cbxUseBack.Checked then
      P.Parameters.Add(_BACK);

    if CbxUseRP.Checked then
    begin
      P.Parameters.Add(_RP);
      if length(eRemotePW.Text) > 0 then
        P.Parameters.Add(eRemotePW.Text);
    end;

    if cbxUseExe.Checked then
    begin
      if (cmbExeCmd.ItemIndex <= 0) then
        P.Parameters.Add(_EXE + cmbExeCmd.Text)
      else
        P.Parameters.Add(_EXE + functionList[cmbExeCmd.ItemIndex, 2]);
      if length(eExeParams.Text) > 0 then
      begin
        P.Parameters.Add(eExeParams.Text);
        timCbFileCheck.Enabled := True;
      end;
    end;

    if CbxUseExe2.Checked then
    begin
      P.Parameters.Add(_EXE2);
      if length(eExe2Params.Text) > 0 then
      begin
        P.Parameters.Add(eExe2Params.Text);
        timCb2FileCheck.Enabled := True;
      end;
    end;

    if CbxUseNf.Checked then
    begin
      P.Parameters.Add(_NF);
      if length(eNfParams.Text) > 0 then
        P.Parameters.Add(eNfParams.Text);
    end;

    deleteCbFile();
    deleteCb2File();
    timCsvFileCheck.Enabled := True;

    P.Execute;

  finally
    P.Free;
  end;
end;

{
   btnDsplCsvClick - Process Display MGISI button.
}
procedure TfMainScreen.btnDsplCsvClick(Sender: TObject);

begin
  if (IsiCsvFileExists()) then
  begin
    msgMemo(IsiCvsProcessCsvFile());
  end
  else
  begin
    msgErrBox(Self, 'DWISI.csv File not found');
  end;
end;

{
   btnDsplCsvClick - Process Display MGTEXT button.
}
procedure TfMainScreen.btnDsplCsv1Click(Sender: TObject);
begin
  if (IsiCsv2FileExists()) then
  begin
    msgMemo(IsiCvsProcessCsvFile2());
  end
  else
  begin
    msgErrBox(Self, 'MGTEXT.csv File not found');
  end;
end;

{
   btnRenameCsvClick - Process Rename CSV button.
}
procedure TfMainScreen.btnRenameCsvClick(Sender: TObject);
var
  newName: string;
  dialogTitle: string;

begin
  dialogTitle := 'Rename DWISI.cvs File';
  newName := inputbox(dialogTitle, 'New Name:', '');

  if Length(newName) > 0 then
  begin
    if not RenameFile(dirName + DirectorySeparator + _ISICSV,
      dirName + DirectorySeparator + newName) then
    begin
      msgErrBox(Self, 'Rename Failed');
    end;
  end
  else
  begin
    msgErrBox(Self, 'No new name given');
  end;
  setCsvButtonState();
end;

{
   btnRenameCsv1Click - Process Rename CSV button.
}
procedure TfMainScreen.btnRenameCsv1Click(Sender: TObject);
var
  newName: string;
  dialogTitle: string;

begin
  dialogTitle := 'Rename MGTEXT.cvs File';
  newName := inputbox(dialogTitle, 'New Name:', '');

  if Length(newName) > 0 then
  begin
    if not RenameFile(dirName + DirectorySeparator + _ISICSV2,
      dirName + DirectorySeparator + newName) then
    begin
      msgErrBox(Self, 'Rename Failed');
    end;
  end
  else
  begin
    msgErrBox(Self, 'No new name given');
  end;
  setCsvButtonState();
end;

procedure TfMainScreen.btnUseDefaultsClick(Sender: TObject);
begin
  eExeParams.Text := _ISI_CB_EXE_OUTPUT;
  eExe2Params.Text := _ISI_CB_EXE2_OUTPUT;
end;

{
   cbFileExists - Returns true/false if special callback detection file exists
}
function TfMainScreen.cbFileExists(): boolean;
begin
  cbFileExists := FileExists(dirName + DirectorySeparator + _ISI_CB_EXE_OUTPUT);
end;

{
   cb2FileExists - Returns true/false if special callback detection file exists
}
function TfMainScreen.cb2FileExists(): boolean;
begin
  cb2FileExists := FileExists(dirName + DirectorySeparator + _ISI_CB_EXE2_OUTPUT);
end;

{
  deleteCbFile - Deletes the special call back detection file.
}
procedure TfMainScreen.deleteCbFile();
begin
  if cbFileExists() then
  begin
    if not DeleteFile(dirName + DirectorySeparator + _ISI_CB_EXE_OUTPUT) then
    begin
      msgErrBox(Self, 'Could not delete CallBack file');
    end;
  end;
end;

{
  deleteCb2File - Deletes the special call back detection file.
}
procedure TfMainScreen.deleteCb2File();
begin
  if cb2FileExists() then
  begin
    if not DeleteFile(dirName + DirectorySeparator + _ISI_CB_EXE2_OUTPUT) then
    begin
      msgErrBox(Self, 'Could not delete CallBack2 file');
    end;
  end;
end;

{
   FormCreate - Special setups during form creation.
}
procedure TfMainScreen.FormCreate(Sender: TObject);
var
  ii: integer;

begin

  dirName := setupDirectory();

  setCsvButtonState();

  deleteCbFile();
  deleteCb2File();

  cmbExeCmd.Items.Clear;
  cmbExeCmd.Items.Add(' ');
  for ii := 1 to 12 do
  begin
    cmbExeCmd.Items.Add(functionList[ii, 1]);
  end;
end;

{
   mniAboutBoxClick - Process About Box menu item.
}
procedure TfMainScreen.mniAboutBoxClick(Sender: TObject);
var
  FileVerInfo: TFileVersionInfo;
  message: string = '';

begin
  FileVerInfo := TFileVersionInfo.Create(nil);
  try
    FileVerInfo.ReadFileInfo;
    message := message + FileVerInfo.VersionStrings.Values['ProductName'] + LineEnding;
    message := message + 'Version: ' + FileVerInfo.VersionStrings.Values['FileVersion'] +
      LineEnding;
    message := message + 'Copyright: ' +
      FileVerInfo.VersionStrings.Values['LegalCopyright'] + LineEnding;
    message := message + FileVerInfo.VersionStrings.Values['CompanyName'] + LineEnding;
    msgInfoBox(Self, message);
  finally
    FileVerInfo.Free;
  end;
end;

{
   mniHelpClick - Process Help menu item.
}
procedure TfMainScreen.mniHelpClick(Sender: TObject);
begin
  msgInfoBox(Self, helpText);
end;


{
   setCsvButtonState - Set CSV buttons based on existance of DW ISI CSV file.
}
procedure TfMainScreen.setCsvButtonState();
var
  csvPresent: boolean;

begin
  csvPresent := IsiCsvFileExists();
  btnDsplCsv.Enabled := csvPresent;
  btnRenameCsv.Enabled := csvPresent;
  csvPresent := IsiCsv2FileExists();
  btnDsplCsv1.Enabled := csvPresent;
  btnRenameCsv1.Enabled := csvPresent;
end;

{
   timCbFileCheckTimer - Processes the timer to check for the callback detection
                         file.
}
procedure TfMainScreen.timCbFileCheckTimer(Sender: TObject);
var
  tfIn: TextFile;
  sLine: string;
begin
  if cbFileExists() then
  begin
    { Set the name of the file to read }
    AssignFile(tfIn, dirName + DirectorySeparator + _ISI_CB_EXE_OUTPUT);
    { Open the file for reading }
    reset(tfIn);
    timCbFileCheck.Enabled := False;
    readln(tfIn, sLine);
    msgInfoBox(Self, sLine);
    { Done so close the file }
    CloseFile(tfIn);
  end;
end;

{
   timCb2FileCheckTimer - Processes the timer to check for the callback2 detection
                          file.
}
procedure TfMainScreen.timCb2FileCheckTimer(Sender: TObject);
var
  tfIn: TextFile;
  sLine: string;
begin
  if cb2FileExists() then
  begin
    { Set the name of the file to read }
    AssignFile(tfIn, dirName + DirectorySeparator + _ISI_CB_EXE2_OUTPUT);
    { Open the file for reading }
    reset(tfIn);
    timCb2FileCheck.Enabled := False;
    readln(tfIn, sLine);
    msgInfoBox(Self, sLine);
    { Done so close the file }
    CloseFile(tfIn);
  end;
end;

{
   timCsvFileCheckTimer - Processes the timer to check for the DW ISI CSV file.
}
procedure TfMainScreen.timCsvFileCheckTimer(Sender: TObject);
begin
  setCsvButtonState();
end;

end.




















