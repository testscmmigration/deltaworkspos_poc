package i18n;

import java.io.FileInputStream;
import java.util.Properties;

public class TextCheck {
	
	private static final String[] PROPERTY_KEYS = new String[] {
		"AgentSetupPage2.rewards",
		"BPWizardPage17.1187",
		"BPWizardPage5.1125",
		"BPWizardPage5.1131",
		"BPWizardPage5.1132",
		"ConfirmationReceipt.LabelLoyaltyDisc",
		"DialogMSCardNameDoesNotMatch.2",
		"DialogMSCardNameDoesNotMatch.4",
		"DialogMSCardNameDoesNotMatch.5",
		"DialogMSCardNameDoesNotMatch.7",
		"DialogMSCardNameDoesNotMatchEP.2",
		"DialogMSCardNameDoesNotMatchEP.3",
		"DialogRewardsCardExists.1",
		"FeeDetailsDialog.loyalty",
		"MGEWizardPage1.enterLabel1",
		"MGEWizardPage2.header2",
		"MGEWizardPage3.1307c",
		"MGEWizardPage3.label2",
		"MGEWizardPage4.header1",
		"MGReceipt.10",
		"MGReceipt.69",
		"MGReceipt.LabelLoyaltyDisc",
		"MGSWizardPage1.1127",
		"MGSWizardPage1.1131",
		"MGSWizardPage1.1132",
		"MGSWizardPage1.1133c",
		"MGSWizardPage1.1135",
		"MGSWizardPage1.1135a",
		"MGSWizardPage14.1187",
		"MGSWizardPage14.Instruction5_2",
		"MGSWizardPage14.Instruction5",
		"MGSWizardPage4.1216",
		"PromotionalSlip.2",
		"SRTWizardPage2.2017"		
	};

	public static void main(String[] args) {
		TextCheck tc = new TextCheck();
		tc.report();
	}
	
	private void report() {
		try {
			Properties en = this.getLanguagePackProperties("en");
			Properties fr = this.getLanguagePackProperties("fr");
			Properties de = this.getLanguagePackProperties("de");
			Properties es = this.getLanguagePackProperties("es");
			Properties pl = this.getLanguagePackProperties("pl");
			Properties cn = this.getLanguagePackProperties("cn");

			for (int i = 0; i < PROPERTY_KEYS.length; i++) {
				System.out.println("Properity: " + PROPERTY_KEYS[i]);
				System.out.println("en: " + en.getProperty(PROPERTY_KEYS[i]));
				System.out.println("fr: " + fr.getProperty(PROPERTY_KEYS[i]));
				System.out.println("de: " + de.getProperty(PROPERTY_KEYS[i]));
				System.out.println("es: " + es.getProperty(PROPERTY_KEYS[i]));
				System.out.println("pl: " + pl.getProperty(PROPERTY_KEYS[i]));
				System.out.println("cn: " + cn.getProperty(PROPERTY_KEYS[i]));
				System.out.println();
			}
			
		
		} catch (Exception e) {
			System.out.println("ExceptionL " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	private Properties getLanguagePackProperties(String language) throws Exception {
		Properties properties = new Properties();
		FileInputStream fis = new FileInputStream("src/xml/i18n/deltaworks_" + language + ".properties");
		properties.load(fis);
		return properties;
	}

}
