package unzip;

import java.io.*;
import java.util.*;
import java.util.zip.*;

/**
 * Very simple utility to extract the contents of a zip or jar file.
 * Based on the Unzipper example in Java I/O by Elliotte Rusty Harold
 * and the source to the jar utility by Sun Microsystems.
 */
public class Unzip {
    /** Extracts the contents of a zip file. */
    void extract(InputStream is) throws IOException {
	ZipInputStream input = new ZipInputStream(is);
	ZipEntry entry;
	while ((entry = input.getNextEntry()) != null) {
            extract(input, entry);
        }
    }

    /** Extracts the current entry from a zip file. */
    void extract(ZipInputStream input, ZipEntry entry) throws IOException {
        String name = entry.getName();
	File file = new File(entry.getName().replace('/', File.separatorChar));
        File dir = null;
        boolean isDir = entry.isDirectory();
        
	if (isDir) {
            dir = file;
        }
        else if (file.getParent() != null) {
            dir = new File(file.getParent());
        }

        if (dir != null && !dir.exists()) {
            dir.mkdirs();
        }

        if (!isDir) {
            FileOutputStream output = new FileOutputStream(file);
            copy(input, output);
            input.closeEntry();
            output.close();
        }
    }

    /** Buffer used by the copy method. */
    private byte buffer[] = new byte[256];

    /** Copy one stream to another. */
    void copy(InputStream input, OutputStream output) throws IOException {
        while (true) {
            int n = input.read(buffer);
            if (n == -1) break;
            output.write(buffer, 0, n);
        }
    }
 
    public static void main(String args[]) {
        try {
            Unzip unzip = new Unzip();
            if (args.length == 0) {
                unzip.extract(System.in);
            }
            else {
                unzip.extract(new BufferedInputStream(
                    new FileInputStream(args[0])));
            }
            System.exit(0);
        }
        catch (IOException e) {
            System.err.println(e);
            System.exit(1);
        }
    }
}
