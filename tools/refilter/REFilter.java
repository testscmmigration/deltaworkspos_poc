package refilter;

import java.io.*;
import java.util.*;
import java.util.regex.*;

/**
 *  General regular expression filter. Does simple grep (using Java regular
 *  expresssion syntax), replace (like sed or perl -pe), 
 *  Some characters are special to the DOS command prompt and are
 *  therefore awkward to specify to this program. These include
 *      <pre> ^ &amp; | ; , ( ) &lt; &gt; &amp;&amp; and || </pre>
 *  They can be quoted from the DOS prompt by enclosing the whole
 *  argument in double quotes. To search for a pattern which contains
 *  double quotes, backslash them and enclose the whole pattern in 
 *  quotes. For example, to search for a double-quoted string, do
 *      grep "\".+\"" input
 *
 *  The grep command prints lines that match the expression. The replace 
 *  command prints all lines and replaces all subsequences that match the
 *  pattern with the supplied replacement text. Within the replacement
 *  text, $1 through $9 are backreferences to previously matched groups.
 *  Unlike java.util.regex.Matcher.appendReplacement(), backslashes are
 *  not treated specially in the replacement text.
 */
public final class REFilter {

    Pattern pattern;
    String replacement = null;  // replacement text or count string
    String line;
    String previousLine;
    Matcher matcher = null;
    int counter = 0;
    
    final int mode;

    static final int GREP_MODE=1;
    static final int REPLACE_MODE=2;
    static final int COUNT_MODE=3;
    static final int UNIQUE_MODE=4;
    
    REFilter(int mode, String pattern, String replacement) {
        this.mode = mode;
        this.pattern = Pattern.compile(pattern);
        this.replacement = replacement;
    }

    void quoteBackslashes() {
        Pattern slashes = Pattern.compile("\\\\");  // replace one backslash
        Matcher m = slashes.matcher(replacement);
        replacement = m.replaceAll("\\\\\\\\");     // with two backslashes
    }
    
    void filter(File file) throws FileNotFoundException, IOException {
        filter(new BufferedReader(new FileReader(file)));
    }
    
    void filter(BufferedReader input) throws IOException {
        while ((line = input.readLine()) != null) {
            if (matcher != null) {
                matcher.reset(line);
            }
            else {
                matcher = pattern.matcher(line);
            }

            switch (mode) {
                case GREP_MODE: grep(); break;
                case REPLACE_MODE: replace(); break;
                case COUNT_MODE: count(); break;
                case UNIQUE_MODE: uniq(); break;
            }
        }
    }
    
    void grep() {
        if (matcher.find()) {
            System.out.println(line);
        }
    }
    
    void replace() {
        System.out.println(matcher.replaceAll(replacement));
    }

    void count() {
        if (matcher.find()) {
            ++counter;
        }
    }

    void summarize() {
        if (replacement == null) 
            System.out.println(counter);
        else 
            System.out.println(counter + " " + replacement);
    }

    void uniq() {
        if ((previousLine == null) || (! previousLine.equals(line)))
            System.out.println(line);

        previousLine = line;
    }
    
    public static void main(String args[]) {
        try {
            if (args.length < 2) {
                usage();
                System.exit(2);
            }

            if (args[0].equals("grep")) {
                REFilter refilter = new REFilter(GREP_MODE, args[1], null);
                refilter.filter(new File(args[2]));
            }
            else if (args[0].equals("replace")) {
                REFilter refilter = new REFilter(REPLACE_MODE, args[1], args[2]);
                refilter.quoteBackslashes();
                refilter.filter(new File(args[3]));
            }
            else if (args[0].equals("count")) {
                REFilter refilter = new REFilter(COUNT_MODE, args[1], args[2]);
                refilter.filter(new File(args[3]));
                refilter.summarize();
            }
            else if (args[0].equals("unique")) {
                REFilter refilter = new REFilter(UNIQUE_MODE, "", null);
                refilter.filter(new File(args[1]));
            }
            else if (args[0].equals("print")) {
                for (int i = 0; i < args.length; i++) {
                    System.out.print(args[i] + " ");
                }
                System.out.println();
            }
        }
        catch (ArrayIndexOutOfBoundsException e) {
            usage();
            System.exit(2);
        }
        catch (Exception e) {
            System.err.println(e);
            System.exit(1);
        }
    }

    private static void usage() {
        System.err.println("usage: grep pattern filename");
        System.err.println("or     replace pattern replacement filename");
        System.err.println("or     count pattern description filename");
        System.err.println("or     unique filename");
    }
}
