package xml;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlCompare {
	
	private class Tag {
		private Tag parent;
		private String name;
		private String value;
		private List<Tag> childTags;
		boolean checked = false;
		
		private Tag(Tag parent) {
			this.parent = parent;
		}
		
		private Tag findChildTag(Tag tag) {
			Tag childTag = null;
			
			if (childTags != null) {
				for (Tag t : childTags) {
					if (tag.name.compareTo(t.name) == 0) {
						if (tag.childTags != null) {
							String v1 = tag.childTags.get(0).value;
							String v2 = t.childTags.get(0).value;
							if (v1.compareTo(v2) == 0) {
								return t;
							}
							continue;
						} else {
							return t;
						}
					}
				}
				return null;
			}
			
			return childTag;
		}
	}

	public static void main(String[] args) {
		
		String file1 = args[0];
		String file2 = args[1];
		
		XmlCompare c = new XmlCompare();
		
		Tag tag1 = c.readXmlFile(file1);
		Tag tag2 = c.readXmlFile(file2);
		c.compareTags(tag1, tag2, "");
		c.checkUse(tag1, "File 1", "");
		c.checkUse(tag2, "File 2", "");
	}
	
	private Tag readXmlFile(String fileName) {
    	Tag tag = null;
	    try {
	    	File fXmlFile = new File(fileName);
	    	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	    	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	    	Document doc = dBuilder.parse(fXmlFile);
	    			
	    	doc.getDocumentElement().normalize();
	    	
	    	Node root = doc.getDocumentElement();
    		tag = readNode(root, null, "");
	    } catch (Exception e) {
	    	e.printStackTrace();
	    }
		return tag;
	}
	
	private Tag readNode(Node parentNode, Tag parentTag, String indent) {
		
		Tag tag = null;
		if (parentNode.getNodeType() == Node.ELEMENT_NODE) {
			NodeList list = parentNode.getChildNodes();
			Element element = (Element) parentNode;
			String value = getNodeValue(parentNode);
			tag = new Tag(parentTag);
			tag.name = element.getTagName();
			tag.value = value;
//			System.out.println(indent + element.getTagName() + ", " + value);

			for (int i = 0; i < list.getLength(); i++) {
				Tag childTag = readNode(list.item(i), tag, indent + "   ");
				if (childTag != null) {
					if (tag.childTags == null) {
						tag.childTags = new ArrayList<Tag>();
					}
					tag.childTags.add(childTag);
				}
			}
		}
		return tag;
	}
	
	private String getNodeValue(Node node) {
		StringBuffer sb = new StringBuffer();
		NodeList list = node.getChildNodes();
		for (int i = 0; i < list.getLength(); i++) {
			Node childNode = list.item(i);
			if (childNode.getNodeType() == Node.TEXT_NODE) {
				sb.append(childNode.getNodeValue().trim());
			}
		}
		return sb.toString();
	}

	private void compareTags(Tag tag1, Tag tag2, String node) {
		tag1.checked = true;
		tag2.checked = true;
		String nodeName = (node.equals("")  ? "" : node + ".") + tag1.name;
		String tagFirstChildValue = getFirstChildTagValue(tag1);
		if (tagFirstChildValue != null) {
			nodeName += "[" + tagFirstChildValue + "]";
		}
		if (tag1.value.compareTo(tag2.value) != 0) {
			System.out.println("> Difference: " + nodeName + ": 1) "+ tag1.value + " 2) " + tag2.value);
		}
		if (tag1.childTags != null) {
			for (Tag childTag1 : tag1.childTags) {
				Tag childTag2 = tag2.findChildTag(childTag1);
//				System.out.println("Comparing " + childTag1 + " to " + childTag2);
				if (childTag2 != null) {
					compareTags(childTag1, childTag2, nodeName);
				}
			}
		}
	}
	
	private String getFirstChildTagValue(Tag tag) {
		String tagName = tag.name;
		boolean flag = false;
		if (tag.parent != null) {
			for (Tag childTag : tag.parent.childTags) {
				if (childTag.name.compareTo(tagName) == 0) {
					if (flag) {
						if (tag.childTags != null) {
							return tag.childTags.get(0).value;
						}
					} else {
						flag = true;
					}
				}
			}
		}
		return null;
	}

	private void checkUse(Tag tag, String fileId, String node) {
		String nodeName = (node.equals("") ? "" : node + ".") + tag.name;
		String tagFirstChildValue = getFirstChildTagValue(tag);
		if (tagFirstChildValue != null) {
			nodeName += "[" + tagFirstChildValue + "]";
		}
		if (! tag.checked) {
			System.out.println("> Extra: " + fileId + " - " + nodeName);
		}
		if (tag.childTags != null) {
			for (Tag childTag : tag.childTags) {
				checkUse(childTag, fileId, nodeName);
			}
		}
	}
}
