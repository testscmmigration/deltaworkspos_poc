package gffpCompare;

public class Dbg {
	
	
	private final static boolean DBG_ON = true;
	private final static String DBG_ENTER_FLAG = "##"; 
	private final static String DBG_ENTER_START = DBG_ENTER_FLAG + " Entering: "; 
	private final static String DBG_EXIT_FLAG  = "&&"; 
	private final static String DBG_EXIT_START = DBG_EXIT_FLAG + " Exiting:  ";
	
	private final static String DBG_INFO_FLAG = "**";
	private final static String DBG_INFO_END =  " " + DBG_INFO_FLAG; 
	private final static String DBG_INFO_START  = DBG_INFO_FLAG  + " INFO:    "; 
	
    private final static int DBG_CLIENT_CODE_STACK_INDEX;

	private static int iDbgEnterExitOffset = 0;
	
	private static String sEnterExitOffset() {
		String sRetValue;
		if (iDbgEnterExitOffset > 0) {
			sRetValue = String.format("%0" + iDbgEnterExitOffset + "d", 0).replace("0"," ");
		} else {
			sRetValue = "";
		}
		return sRetValue;
	}
	
	private static String sEnterOffset() {
		String sRetValue = sEnterExitOffset();
		iDbgEnterExitOffset++;
		return sRetValue;
	}
	
	private static String sInfoOffset() {
		String sRetValue = sEnterExitOffset();
		return sRetValue;
	}
	
	private static String sExitOffset() {
		iDbgEnterExitOffset--;
		String sRetValue = sEnterExitOffset();
		return sRetValue;
	}
	
	private static void vLogMethodEnter(String pm_sParmData) {
		System.out.println(
				sEnterOffset() + DBG_ENTER_START + sGetInvokingX2MethodNameFqn() + " " + pm_sParmData + DBG_ENTER_FLAG);
	}
	
	private static void vLogMethodExit(String pm_sParmData) {
		System.out.println(
				sExitOffset() + DBG_EXIT_START + sGetInvokingX2MethodNameFqn() + " " + pm_sParmData + DBG_EXIT_FLAG);
	}
	
	protected static void vDebugMethodInfo(String sInfo) {
		if (DBG_ON) {
			System.out.println(sInfoOffset() + DBG_INFO_START + sGetInvokingMethodNameFqn() + " [" + sInfo +  "]" + DBG_INFO_END);
		}
	}

	protected static void vDebugMethodEnter() {
		if (DBG_ON) {
			vLogMethodEnter("");
		}
	}

	protected static void vDebugMethodEnter(String pm_sLabel1, String pm_value1) {
		if (DBG_ON) {
			final String PARAM = pm_sLabel1 + " [" + pm_value1 + "] ";
			vLogMethodEnter(PARAM);
		}
	}

	protected static void vDebugMethodEnter(String pm_sLabel1, String pm_value1, String pm_sLabel2, String pm_value2) {
		if (DBG_ON) {
			final String PARAM = pm_sLabel1 + " [" + pm_value1 + "] " + pm_sLabel2 + " [" + pm_value2 + "] ";
			vLogMethodEnter(PARAM);
		}
	}

	protected static void vDebugMethodEnter(String pm_sLabel1, String pm_value1, String pm_sLabel2, String pm_value2, String pm_sLabel3, String pm_value3) {
		if (DBG_ON) {
			final String PARAM = pm_sLabel1 + " [" + pm_value1 + "] " + pm_sLabel2 + " [" + pm_value2 + "] " + pm_sLabel3 + " [" + pm_value3 + "] ";
			vLogMethodEnter(PARAM);
		}
	}

	protected static void vDebugMethodExit() {
		if (DBG_ON) {
			System.out.println(sExitOffset() + DBG_EXIT_START + sGetInvokingMethodNameFqn() + " " + DBG_EXIT_FLAG);
		}
	}

	protected static void vDebugMethodExit(String pm_value1) {
		if (DBG_ON) {
			System.out.println(
					sExitOffset() + DBG_EXIT_START + sGetInvokingMethodNameFqn() + " Returning  [" + pm_value1 + "] " + DBG_EXIT_FLAG);
		}
	}

	protected static void vDebugMethodExit(String pm_sLabel1, String pm_value1) {
		if (DBG_ON) {
			final String PARAM = pm_sLabel1 + " [" + pm_value1 + "] ";
			vLogMethodExit(PARAM);
		}
	}

	protected static void vDebugMethodExit(String pm_sLabel1, String pm_value1, String pm_sLabel2, String pm_value2) {
		if (DBG_ON) {
			final String PARAM = pm_sLabel1 + " [" + pm_value1 + "] " + pm_sLabel2 + " [" + pm_value2 + "] ";
			vLogMethodExit(PARAM);
		}
	}

	protected static void vDebugMethodExit(String pm_sLabel1, String pm_value1, String pm_sLabel2, String pm_value2, String pm_sLabel3, String pm_value3) {
		if (DBG_ON) {
			final String PARAM = pm_sLabel1 + " [" + pm_value1 + "] " + pm_sLabel2 + " [" + pm_value2 + "] " + pm_sLabel3 + " [" + pm_value3 + "] ";
			vLogMethodExit(PARAM);
		}
	}

    static {
        // Finds out the index of "this code" in the returned stack trace - funny but it differs in JDK 1.5 and 1.6
        int ii = 0;
        for (StackTraceElement ste: Thread.currentThread().getStackTrace())
        {
            ii++;
            if (ste.getClassName().equals(Dbg.class.getName()))
            {
                break;
            }
        }
        DBG_CLIENT_CODE_STACK_INDEX = ii;
    }

	 public static String sGetCurrentMethodName()
	    {
	        return sGetCurrentMethodName(1);     // making additional overloaded method call requires +1 offset
	    }

	    private static String sGetCurrentMethodName(int pm_iOffset)
	    {
	        return Thread.currentThread().getStackTrace()[DBG_CLIENT_CODE_STACK_INDEX + pm_iOffset].getMethodName();
	    }

	    public static String sGetCurrentClassName()
	    {
	        return sGetCurrentClassName(1);      // making additional overloaded method call requires +1 offset
	    }

	    private static String sGetCurrentClassName(int pm_iOffset)
	    {
	    	return Thread.currentThread().getStackTrace()[DBG_CLIENT_CODE_STACK_INDEX + pm_iOffset].getClassName();
	    }

	    public static String sGetCurrentFileName()
	    {
	        return sGetCurrentFileName(1);     // making additional overloaded method call requires +1 offset
	    }

	    private static String sGetCurrentFileName(int pm_iOffset)
	    {
	        String sFilename = Thread.currentThread().getStackTrace()[DBG_CLIENT_CODE_STACK_INDEX + pm_iOffset].getFileName();
	        int iLineNumber = Thread.currentThread().getStackTrace()[DBG_CLIENT_CODE_STACK_INDEX + pm_iOffset].getLineNumber();

	        return sFilename + ":" + iLineNumber;
	    }

	    public static String sGetInvokingMethodName()
	    {
	        return sDbgGetInvokingMethodName(2); 
	    }

	    private static String sDbgGetInvokingMethodName(int pm_iOffset)
	    {
	        return sGetCurrentMethodName(pm_iOffset + 1);    // re-uses getCurrentMethodName() with desired index
	    }

	    public static String sGetInvokingClassName()
	    {
	        return sDbgGetInvokingClassName(2); 
	    }

	    private static String sDbgGetInvokingClassName(int pm_iOffset)
	    {
	        return sGetCurrentClassName(pm_iOffset + 1);     // re-uses getCurrentClassName() with desired index
	    }

	    public static String sGetInvokingFileName()
	    {
	        return sDbgGetInvokingFileName(2); 
	    }

	    private static String sDbgGetInvokingFileName(int pm_iOffset)
	    {
	        return sGetCurrentFileName(pm_iOffset + 1);     // re-uses getCurrentFileName() with desired index
	    }

	    public static String sGetCurrentMethodNameFqn()
	    {
	        return sDbgGetCurrentMethodNameFqn(1);
	    }

	    private static String sDbgGetCurrentMethodNameFqn(int pm_iOffset)
	    {
	        String sCurrentClassName = sGetCurrentClassName(pm_iOffset + 1);
	        String sCurrentMethodName = sGetCurrentMethodName(pm_iOffset + 1);

	        return sCurrentClassName + "." + sCurrentMethodName ;
	    }

	    public static String sGetCurrentFileNameFqn()
	    {
	        String sCurrentMethodNameFqn = sDbgGetCurrentMethodNameFqn(1);
	        String sCurrentFileName = sGetCurrentFileName(1);

	        return sCurrentMethodNameFqn + "(" + sCurrentFileName + ")";
	    }

	    public static String sGetInvokingMethodNameFqn()
	    {
	        return sDbgGetInvokingMethodNameFqn(2);
	    }

	    public static String sGetInvokingX2MethodNameFqn()
	    {
	        return sDbgGetInvokingMethodNameFqn(3);
	    }

	    private static String sDbgGetInvokingMethodNameFqn(int pm_iOffset)
	    {
	        String sInvokingClassName = sDbgGetInvokingClassName(pm_iOffset + 1);
	        String sInvokingMethodName = sDbgGetInvokingMethodName(pm_iOffset + 1);

	        return sInvokingClassName + "." + sInvokingMethodName;
	    }

	    public static String sGetInvokingFileNameFqn()
	    {
	        String sInvokingMethodNameFqn = sDbgGetInvokingMethodNameFqn(2);
	        String sInvokingFileName = sDbgGetInvokingFileName(2);

	        return sInvokingMethodNameFqn + "(" + sInvokingFileName + ")";
	    }	

	    public static String sGetInvokingX2FileNameFqn()
	    {
	        String sInvokingMethodNameFqn = sDbgGetInvokingMethodNameFqn(3);
	        String sInvokingFileName = sDbgGetInvokingFileName(3);

	        return sInvokingMethodNameFqn + "(" + sInvokingFileName + ")";
	    }	

}
