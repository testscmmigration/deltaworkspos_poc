package gffpCompare;

/*
 * Program comparing two GFFP results; comparing contents of similar elements regardless of order
 */

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import gffpCompare.Dbg;

public class GffpComp {
	
	private static final String GPPFCOMP_ID1 = "File1"; 
	private static final String GPPFCOMP_ID2 = "File2"; 
	
	private class Tag {
		private Tag tTagParent;
		private String sTagName;
		private String sTagValue;
		private List<Tag> tlTagChildTags;
		boolean bTagChecked = false;
		
		private Tag(Tag parent) {
			this.tTagParent = parent;
		}
		
		private Tag tFindChildTag(Tag pm_tTag) {
			Dbg.vDebugMethodEnter("pm_tTag", pm_tTag.sTagName);
			
			if (tlTagChildTags != null) {
				for (Tag tForChildTag : tlTagChildTags) {
					if (pm_tTag.sTagName.compareTo(tForChildTag.sTagName) == 0) {
						if (pm_tTag.tlTagChildTags != null) {
							String sValue1 = pm_tTag.tlTagChildTags.get(0).sTagValue;
							String sValue2 = tForChildTag.tlTagChildTags.get(0).sTagValue;
							if (sValue1.compareTo(sValue2) == 0) {
								Dbg.vDebugMethodExit(tForChildTag.sTagName);
								return tForChildTag;
							}
							continue;
						} else {
							Dbg.vDebugMethodExit(tForChildTag.sTagName);
							return tForChildTag;
						}
					}
				}
			}
			Dbg.vDebugMethodExit("Null");
			return null;
		}
	}

	protected static void main(String[] pm_saArgs) {
		
		String sFileName1 = pm_saArgs[0];
		String sFileName2 = pm_saArgs[1];
		
		GffpComp gcGffpComp = new GffpComp();
		
		Tag tFile1TopTag = gcGffpComp.tReadXmlFile(sFileName1);
		Tag tFile2TopTag = gcGffpComp.tReadXmlFile(sFileName2);
		gcGffpComp.vCompareTags(tFile1TopTag, tFile2TopTag, "");
		gcGffpComp.vCheckUse(tFile1TopTag, GPPFCOMP_ID1, "");
		gcGffpComp.vCheckUse(tFile2TopTag, GPPFCOMP_ID2, "");
	}
	
	private void vCheckUse(Tag pm_tTag, String pm_sFileDisplayName, String pm_sNode) {
		Dbg.vDebugMethodEnter("pm_tTag", pm_tTag.sTagName, "pm_sFileId", pm_sFileDisplayName, "pm_sNode", pm_sNode);
		String sNodeName = (pm_sNode.equals("") ? "" : pm_sNode + ".") + pm_tTag.sTagName;
		String sTagFirstChildValue = sGetFirstChildTagValue(pm_tTag);
		if (sTagFirstChildValue != null) {
			sNodeName += "[" + sTagFirstChildValue + "]";
		}
		if (! pm_tTag.bTagChecked) {
			String sOutString = "> Extra: " + pm_sFileDisplayName + " - " + sNodeName;
			System.out.println(sOutString);
			Gui.vPrintLogLine(sOutString);
		}
		if (pm_tTag.tlTagChildTags != null) {
			for (Tag tChildTag : pm_tTag.tlTagChildTags) {
				vCheckUse(tChildTag, pm_sFileDisplayName, sNodeName);
			}
		}
		Dbg.vDebugMethodExit("pm_tTag", pm_tTag.sTagName, "pm_sFileId", pm_sFileDisplayName, "pm_sNode", pm_sNode);
	}
	
	private void vCompareTags(Tag pm_tTag1, Tag pm_tTag2, String sNode) {
		Dbg.vDebugMethodEnter("pm_tTag1", pm_tTag1.sTagName, "pm_tTag2", pm_tTag2.sTagName, "sNode", sNode);
		
		pm_tTag1.bTagChecked = true;
		pm_tTag2.bTagChecked = true;
		String sNodeName = (sNode.equals("")  ? "" : sNode + ".") + pm_tTag1.sTagName;
		String sTagFirstChildValue = sGetFirstChildTagValue(pm_tTag1);
		
		if (sTagFirstChildValue != null) {
			sNodeName += "[" + sTagFirstChildValue + "]";
		}
		if (pm_tTag1.sTagValue.compareTo(pm_tTag2.sTagValue) != 0) {
			String sOutString = "> Difference: " + sNodeName + ": " + GPPFCOMP_ID1 + ") "+ pm_tTag1.sTagValue + " " + GPPFCOMP_ID2 + ") " + pm_tTag2.sTagValue;
			System.out.println(sOutString);
			Gui.vPrintLogLine(sOutString);
		}
		if (pm_tTag1.tlTagChildTags != null) {
			for (Tag tChildTag1 : pm_tTag1.tlTagChildTags) {
				Tag tChildTag2 = pm_tTag2.tFindChildTag(tChildTag1);
				Dbg.vDebugMethodInfo("Comparing " + tChildTag1.sTagName + " to " + (tChildTag2 != null ? tChildTag2.sTagName : "null"));
				if (tChildTag2 != null) {
					vCompareTags(tChildTag1, tChildTag2, sNodeName);
				}
			}
		}
		Dbg.vDebugMethodExit("pm_tTag1", pm_tTag1.sTagName, "pm_tTag2", pm_tTag2.sTagName, "sNode", sNode);
	}
	
	private String sGetFirstChildTagValue(Tag pm_tTag) {
		Dbg.vDebugMethodEnter("pm_tTag", pm_tTag.sTagName);
		
		String sTagName = pm_tTag.sTagName;
		boolean bFoundflag = false;
		
		if (pm_tTag.tTagParent != null) {
			for (Tag tChildTag : pm_tTag.tTagParent.tlTagChildTags) {
				if (tChildTag.sTagName.compareTo(sTagName) == 0) {
					if (bFoundflag) {
						if (pm_tTag.tlTagChildTags != null) {
							String sRetValue = pm_tTag.tlTagChildTags.get(0).sTagValue; 
							Dbg.vDebugMethodExit((sRetValue!= null ? sRetValue : "null"));
							return sRetValue;
						}
					} else {
						Dbg.vDebugMethodInfo("!bFoundflag " + sTagName);
						bFoundflag = true;
					}
				}
			}
		}
		Dbg.vDebugMethodExit("Null");
		return null;
	}

	private String sGetNodeValue(Node pm_nNode) {
		Dbg.vDebugMethodEnter("pm_nNode", pm_nNode.getNodeName());
		StringBuffer sbValue = new StringBuffer();
		NodeList nlList = pm_nNode.getChildNodes();
		for (int i = 0; i < nlList.getLength(); i++) {
			Node nChildNode = nlList.item(i);
			if (nChildNode.getNodeType() == Node.TEXT_NODE) {
				sbValue.append(nChildNode.getNodeValue().trim());
			}
		}
		String sRetValue = sbValue.toString();
		Dbg.vDebugMethodExit(sRetValue);
		return sRetValue;
	}
	
	private Tag tReadNode(Node pm_nParentNode, Tag pm_tParentTag, String pm_sIndent) {
		Dbg.vDebugMethodEnter("pm_nParentNode", (pm_nParentNode != null ? pm_nParentNode.getLocalName() : "null"), 
				"pm_tParentTag", (pm_tParentTag != null ? pm_tParentTag.sTagName : "null"), "pm_sIndent", pm_sIndent);
		
		Tag tTag = null;
		if (pm_nParentNode != null && pm_nParentNode.getNodeType() == Node.ELEMENT_NODE) {
			NodeList nlList = pm_nParentNode.getChildNodes();
			Element eElement = (Element) pm_nParentNode;
			String sValue = sGetNodeValue(pm_nParentNode);
			tTag = new Tag(pm_tParentTag);
			tTag.sTagName = eElement.getTagName();
			tTag.sTagValue = sValue;
			Dbg.vDebugMethodInfo(pm_sIndent + eElement.getTagName() + ", " + sValue);

			for (int i = 0; i < nlList.getLength(); i++) {
				Tag tChildTag = tReadNode(nlList.item(i), tTag, pm_sIndent + "   ");
				if (tChildTag != null) {
					if (tTag.tlTagChildTags == null) {
						tTag.tlTagChildTags = new ArrayList<Tag>();
					}
					tTag.tlTagChildTags.add(tChildTag);
				}
			}
		}
		Dbg.vDebugMethodExit(tTag != null ? tTag.sTagName : "null");
		return tTag;
	}

	private Tag tReadXmlFile(String pm_sFileName) {
		Dbg.vDebugMethodEnter("pm_sFileName", pm_sFileName);
    	Tag tTag = null;
	    try {
	    	File fXmlFile = new File(pm_sFileName);
	    	DocumentBuilderFactory dbfFactory = DocumentBuilderFactory.newInstance();
	    	DocumentBuilder dbBuilder = dbfFactory.newDocumentBuilder();
	    	Document dDoc = dbBuilder.parse(fXmlFile);
	    			
	    	dDoc.getDocumentElement().normalize();
	    	
	    	Node nRoot = dDoc.getDocumentElement();
    		tTag = tReadNode(nRoot, null, "");
	    } catch (Exception e) {
	    	e.printStackTrace();
	    }
		Dbg.vDebugMethodExit(tTag != null ? tTag.sTagName : "null");
		return tTag;
	}
}
