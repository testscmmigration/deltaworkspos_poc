package gffpCompare;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;

/*
 * Program comparing two GFFP results; comparing contents of similar elements regardless of order
 */
public class Gui extends JPanel implements ActionListener {
	private static final long serialVersionUID = 4617040836614447001L;

	static private final int GUI_BUTTON_HEIGTH = 50;
	static private final int GUI_BUTTON_WIDTH = 150;

	static private final String GUI_FILE1_LABEL_TEXT = "File 1: ";
	static private final String GUI_FILE2_LABEL_TEXT = "File 2: ";
	static private final int GUI_INSET_BASE = 5;
	static private final int GUI_INSET_BOTTOM = GUI_INSET_BASE;
	static private final int GUI_INSET_LEFT = GUI_INSET_BASE;
	static private final int GUI_INSET_RIGHT = GUI_INSET_BASE;
	static private final int GUI_INSET_TOP = GUI_INSET_BASE;
	static private final String GUI_NEW_LINE = "\n";
	static private final String GUI_SPACER_TEXT = "     ";
	static private JButton jbGuiCompareButton;
	
	static private JButton jbGuiFile1Button;
	static private JButton jbGuiFile2Button;
	static private JButton jbGuiSaveButton;
	static private JTextArea jtaGuiLogWindow;
	
	final static int GUI_COL_LEFT = 0;
	final static int GUI_COL_MIDDLE = 1;
	final static int GUI_COL_RIGHT = 2;

	final static int GUI_CON_BUTTON_FILL = GridBagConstraints.NONE;
	final static int GUI_CON_BUTTON_GRID_WIDTH = 1;
	final static double GUI_CON_BUTTON_WEIGHTx = .5;
	final static double GUI_CON_BUTTON_WEIGHTy = 0;

	final static int GUI_CON_LABEL_FILL = GridBagConstraints.HORIZONTAL;
	final static int GUI_CON_LABEL_GRID_WIDTH = 2;
	final static double GUI_CON_LABEL_WEIGHTx = .5;

	final static double GUI_CON_LABEL_WEIGHTy = 0;
	final static int GUI_CON_LOG_FILL = GridBagConstraints.BOTH;
	final static int GUI_CON_LOG_HEIGHT = 2;
	
	final static double GUI_CON_LOG_WEIGHT = 1.0;
	final static int GUI_CON_LOG_WIDTH = 3;
	final static int GUI_CON_SPACER_FILL = GUI_CON_LABEL_FILL;
	
	final static double GUI_CON_SPACER_WEIGHTx = GUI_CON_LOG_WEIGHT;
	final static double GUI_CON_SPACER_WEIGHTy = GUI_CON_LABEL_WEIGHTy;
	final static boolean GUI_RIGHT_TO_LEFT = false;
	final static int GUI_ROW_COMPARE_BUTTON = 2;
	final static int GUI_ROW_FILE1 = 0;
	
	final static int GUI_ROW_FILE2 = 1;
	final static int GUI_ROW_LOG_WINDOW = 3;
	final static int GUI_ROW_SAVE_BUTTON = 5;
	final static boolean GUI_SHOULD_FILL = true;
	final static boolean GUI_SHOULD_WEIGHT_X = true;
	

	/*
	 * Program Entry point
	 */
	public static void main(String[] pm_saArgs) {
		// Schedule a job for the event dispatch thread:
		// creating and showing this application's GUI.
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				// Turn off metal's use of bold fonts
				UIManager.put("swing.boldMetal", Boolean.FALSE);
				vGuiCreateAndShowGUI();
			}
		});
	}
	/**
	 * Create the GUI and show it. For thread safety, this method should be
	 * invoked from the event dispatch thread.
	 */
	private static void vGuiCreateAndShowGUI() {
		// Create and set up the window.
		JFrame jfFrame = new JFrame("GFFP Comparison");
		jfFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Add content to the window.
		JComponent jcNewContentPane = new Gui();
		jcNewContentPane.setOpaque(true); // content panes must be opaque
		jcNewContentPane.setPreferredSize(new Dimension(600, 400));
		jfFrame.setContentPane(jcNewContentPane);
		// Display the window.
		jfFrame.pack();
		jfFrame.setVisible(true);
	}
	protected static void vPrintLogLine(String pm_sDataLine) {
		jtaGuiLogWindow.append(pm_sDataLine + GUI_NEW_LINE);
		jtaGuiLogWindow.setCaretPosition(jtaGuiLogWindow.getDocument().getLength());
	}
	private JFileChooser jfcGuiFileChooser;
	

	private JLabel jlGuiFile1Label, jlGuiFile2Label;
	
	private JLabel jlGuiSpacer;
	
	private String sGuiFile1Name, sGuiFile2Name;
	
	private Gui() {
		super(new GridBagLayout());
		GridBagLayout gblGridbag = (GridBagLayout) getLayout();

		// Create the log window
		jtaGuiLogWindow = new JTextArea();
		jtaGuiLogWindow.setMargin(new Insets(GUI_INSET_TOP, GUI_INSET_LEFT, GUI_INSET_BOTTOM, GUI_INSET_RIGHT));
		jtaGuiLogWindow.setEditable(false);
		JScrollPane jspLogScrollPane = new JScrollPane(jtaGuiLogWindow);

		// Create a file chooser
		jfcGuiFileChooser = new JFileChooser(System.getProperty("user.dir"));
		FileNameExtensionFilter fnefFilter = new FileNameExtensionFilter("XML Files", "xml");
		jfcGuiFileChooser.addChoosableFileFilter(fnefFilter);

		// Create the open button.
		jbGuiFile1Button = new JButton("Select File 1");
		jbGuiFile1Button.addActionListener(this);
		jbGuiFile1Button.setPreferredSize(new Dimension(GUI_BUTTON_WIDTH, GUI_BUTTON_HEIGTH));

		// Create the save button.
		jbGuiFile2Button = new JButton("Select File 2");
		jbGuiFile2Button.addActionListener(this);
		jbGuiFile2Button.setPreferredSize(new Dimension(GUI_BUTTON_WIDTH, GUI_BUTTON_HEIGTH));

		jlGuiFile1Label = new JLabel(GUI_FILE1_LABEL_TEXT + "UNDEFINED");
		jlGuiFile2Label = new JLabel(GUI_FILE2_LABEL_TEXT + "UNDEFINED");

		jbGuiCompareButton = new JButton("Compare Files");
		jbGuiCompareButton.addActionListener(this);
		jbGuiCompareButton.setEnabled(false);
		jbGuiCompareButton.setPreferredSize(new Dimension(GUI_BUTTON_WIDTH, GUI_BUTTON_HEIGTH));

		// For layout purposes, put the save button in a the log panel
		jbGuiSaveButton = new JButton("Save Log");
		jbGuiSaveButton.addActionListener(this);
		jbGuiSaveButton.setPreferredSize(new Dimension(GUI_BUTTON_WIDTH, GUI_BUTTON_HEIGTH));

		GridBagConstraints gbcConstraints = new GridBagConstraints();
		gbcConstraints.insets = new Insets(GUI_INSET_TOP, GUI_INSET_LEFT, GUI_INSET_BOTTOM, GUI_INSET_RIGHT);
		gbcConstraints.gridy = GUI_ROW_FILE1;
		gbcConstraints.gridx = GUI_COL_LEFT;
		gbcConstraints.fill = GUI_CON_LABEL_FILL;
		gbcConstraints.gridwidth = GUI_CON_LABEL_GRID_WIDTH;
		gbcConstraints.weightx = GUI_CON_LABEL_WEIGHTx;
		gbcConstraints.weighty = GUI_CON_LABEL_WEIGHTy;
		gblGridbag.setConstraints(jlGuiFile1Label, gbcConstraints);
		add(jlGuiFile1Label);

		gbcConstraints = new GridBagConstraints();
		gbcConstraints.insets = new Insets(GUI_INSET_TOP, GUI_INSET_LEFT, GUI_INSET_BOTTOM, GUI_INSET_RIGHT);
		gbcConstraints.gridy = GUI_ROW_FILE1;
		gbcConstraints.gridx = GUI_COL_RIGHT;
		gbcConstraints.fill = GUI_CON_BUTTON_FILL;
		gbcConstraints.gridwidth = GUI_CON_BUTTON_GRID_WIDTH;
		gbcConstraints.weightx = GUI_CON_BUTTON_WEIGHTx;
		gbcConstraints.weighty = GUI_CON_BUTTON_WEIGHTy;
		gblGridbag.setConstraints(jbGuiFile1Button, gbcConstraints);
		add(jbGuiFile1Button);

		gbcConstraints = new GridBagConstraints();
		gbcConstraints.insets = new Insets(GUI_INSET_TOP, GUI_INSET_LEFT, GUI_INSET_BOTTOM, GUI_INSET_RIGHT);
		gbcConstraints.gridy = GUI_ROW_FILE2;
		gbcConstraints.gridx = GUI_COL_LEFT;
		gbcConstraints.fill = GUI_CON_LABEL_FILL;
		gbcConstraints.gridwidth = GUI_CON_LABEL_GRID_WIDTH;
		gbcConstraints.weightx = GUI_CON_LABEL_WEIGHTx;
		gbcConstraints.weighty = GUI_CON_LABEL_WEIGHTy;
		gblGridbag.setConstraints(jlGuiFile2Label, gbcConstraints);
		add(jlGuiFile2Label);

		gbcConstraints = new GridBagConstraints();
		gbcConstraints.insets = new Insets(GUI_INSET_TOP, GUI_INSET_LEFT, GUI_INSET_BOTTOM, GUI_INSET_RIGHT);
		gbcConstraints.gridy = GUI_ROW_FILE2;
		gbcConstraints.gridx = GUI_COL_RIGHT;
		gbcConstraints.fill = GUI_CON_BUTTON_FILL;
		gbcConstraints.gridwidth = GUI_CON_BUTTON_GRID_WIDTH;
		gbcConstraints.weightx = GUI_CON_BUTTON_WEIGHTx;
		gbcConstraints.weighty = GUI_CON_BUTTON_WEIGHTy;
		gblGridbag.setConstraints(jbGuiFile2Button, gbcConstraints);
		add(jbGuiFile2Button);

		jlGuiSpacer = new JLabel(GUI_SPACER_TEXT);

		gbcConstraints = new GridBagConstraints();
		gbcConstraints.insets = new Insets(GUI_INSET_TOP, GUI_INSET_LEFT, GUI_INSET_BOTTOM, GUI_INSET_RIGHT);
		gbcConstraints.gridy = GUI_ROW_COMPARE_BUTTON;
		gbcConstraints.gridx = GUI_COL_LEFT;
		gbcConstraints.fill = GUI_CON_SPACER_FILL;
		gbcConstraints.gridwidth = 1;
		gbcConstraints.weightx = GUI_CON_SPACER_WEIGHTx;
		gbcConstraints.weighty = GUI_CON_SPACER_WEIGHTy;
		gblGridbag.setConstraints(jlGuiSpacer, gbcConstraints);
		add(jlGuiSpacer);

		gbcConstraints = new GridBagConstraints();
		gbcConstraints.insets = new Insets(GUI_INSET_TOP, GUI_INSET_LEFT, GUI_INSET_BOTTOM, GUI_INSET_RIGHT);
		gbcConstraints.gridy = GUI_ROW_COMPARE_BUTTON;
		gbcConstraints.gridx = GUI_COL_MIDDLE;
		gbcConstraints.fill = GUI_CON_BUTTON_FILL;
		gbcConstraints.gridwidth = GUI_CON_BUTTON_GRID_WIDTH;
		gbcConstraints.weightx = GUI_CON_BUTTON_WEIGHTx;
		gbcConstraints.weighty = GUI_CON_BUTTON_WEIGHTy;
		gbcConstraints.anchor = GridBagConstraints.CENTER;
		gblGridbag.setConstraints(jbGuiCompareButton, gbcConstraints);
		add(jbGuiCompareButton);

		gbcConstraints = new GridBagConstraints();
		gbcConstraints.insets = new Insets(GUI_INSET_TOP, GUI_INSET_LEFT, GUI_INSET_BOTTOM, GUI_INSET_RIGHT);
		gbcConstraints.gridy = GUI_ROW_LOG_WINDOW;
		gbcConstraints.gridx = GUI_COL_LEFT;
		gbcConstraints.fill = GUI_CON_LOG_FILL;
		gbcConstraints.gridwidth = GUI_CON_LOG_WIDTH;
		gbcConstraints.gridheight = GUI_CON_LOG_HEIGHT;
		gbcConstraints.weighty = GUI_CON_LOG_WEIGHT; // request any extra vertical space
		gbcConstraints.weightx = GUI_CON_LOG_WEIGHT; // request any extra horizontal space
		gblGridbag.setConstraints(jspLogScrollPane, gbcConstraints);
		add(jspLogScrollPane);

		gbcConstraints = new GridBagConstraints();
		gbcConstraints.insets = new Insets(GUI_INSET_TOP, GUI_INSET_LEFT, GUI_INSET_BOTTOM, GUI_INSET_RIGHT);
		gbcConstraints.gridy = GUI_ROW_SAVE_BUTTON;
		gbcConstraints.gridx = GUI_COL_MIDDLE;
		gbcConstraints.fill = GUI_CON_BUTTON_FILL;
		gbcConstraints.gridwidth = GUI_CON_BUTTON_GRID_WIDTH;
		gbcConstraints.weightx = GUI_CON_BUTTON_WEIGHTx;
		gbcConstraints.weighty = GUI_CON_BUTTON_WEIGHTy;
		gbcConstraints.anchor = GridBagConstraints.PAGE_END; // bottom of space
		gblGridbag.setConstraints(jbGuiSaveButton, gbcConstraints);
		add(jbGuiSaveButton);
	}

	@Override
	public void actionPerformed(ActionEvent pm_aeEvent) {

		// Handle file 1 button action.
		if (pm_aeEvent.getSource() == jbGuiFile1Button) {
			sGuiFile1Name = sGuiGetInputFile(sGuiFile1Name);
			// Handle file 2 button action.
		} else if (pm_aeEvent.getSource() == jbGuiFile2Button) {
			sGuiFile2Name = sGuiGetInputFile(sGuiFile2Name);
			// Handle Compare button action.
		} else if (pm_aeEvent.getSource() == jbGuiCompareButton) {
			jtaGuiLogWindow.append("Compare Button Pressed." + GUI_NEW_LINE);
			jtaGuiLogWindow.setCaretPosition(jtaGuiLogWindow.getDocument().getLength());
			String[] saArgs = new String[2];
			saArgs[0] = sGuiFile1Name;
			saArgs[1] = sGuiFile2Name;
			GffpComp.main(saArgs);
			// Handle Save button action.
		} else if (pm_aeEvent.getSource() == jbGuiSaveButton) {
			jtaGuiLogWindow.append("Save Button Pressed." + GUI_NEW_LINE);
			jtaGuiLogWindow.setCaretPosition(jtaGuiLogWindow.getDocument().getLength());
			vGuiSaveOutputFile();
		}
	}

	private String sGuiGetInputFile(String pm_sFileName) {
		Boolean bFirstFile = (pm_sFileName == sGuiFile1Name);
		Boolean bFirstFileSet = false;
		Boolean bSecondFileSet = false;
		String sText = bFirstFile ? "File1" : "File2";
		String sSelectedFileName;
		int iReturnVal = jfcGuiFileChooser.showOpenDialog(Gui.this);

		if (iReturnVal == JFileChooser.APPROVE_OPTION) {
			File file = jfcGuiFileChooser.getSelectedFile();
			sSelectedFileName = file.getName();

			pm_sFileName = file.getAbsolutePath();
			if (bFirstFile) {
				jlGuiFile1Label.setText(GUI_FILE1_LABEL_TEXT + sSelectedFileName);
				bFirstFileSet = true;
			} else {
				jlGuiFile2Label.setText(GUI_FILE2_LABEL_TEXT + sSelectedFileName);
				bSecondFileSet = true;
			}
			jtaGuiLogWindow.append(sText + ": " + pm_sFileName + GUI_NEW_LINE);

		} else {
			jtaGuiLogWindow.append(sText + " command cancelled by user." + GUI_NEW_LINE);
		}
		jtaGuiLogWindow.setCaretPosition(jtaGuiLogWindow.getDocument().getLength());

		jbGuiCompareButton.setEnabled((bFirstFileSet && sGuiFile2Name != null) || (bSecondFileSet && sGuiFile1Name != null));

		return pm_sFileName;
	}

	private void vGuiSaveOutputFile() {
		Dbg.vDebugMethodEnter();
		File fFileToSave;

		JFileChooser jfcFileChooser = new JFileChooser(System.getProperty("user.dir"));
		jfcFileChooser.setDialogTitle("Specify a file to save");
		FileNameExtensionFilter fnefFilter = new FileNameExtensionFilter("Text Files", "txt");
		jfcFileChooser.addChoosableFileFilter(fnefFilter);
		jfcFileChooser.setSelectedFile(new File("GffpCompareResults.txt"));

		int iUserSelection = jfcFileChooser.showSaveDialog(this);
		if (iUserSelection == JFileChooser.APPROVE_OPTION) {
			fFileToSave = jfcFileChooser.getSelectedFile();
			File fFile = null;
			FileWriter fwOut = null;

			try {
				fFile = new File(fFileToSave.getAbsolutePath());
				fwOut = new FileWriter(fFile);
				fwOut.write(jtaGuiLogWindow.getText());
				fwOut.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			Dbg.vDebugMethodExit("Save File", fFileToSave.getName());
		} else {
			Dbg.vDebugMethodExit("Save File", "None");
		}
	}
	
}
