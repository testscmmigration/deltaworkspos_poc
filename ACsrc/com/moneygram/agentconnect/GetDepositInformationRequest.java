
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetDepositInformationRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetDepositInformationRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="maxDepositsToReturn" type="{http://www.moneygram.com/AgentConnect1705}Int3Type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetDepositInformationRequest", propOrder = {
    "maxDepositsToReturn"
})
public class GetDepositInformationRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected BigInteger maxDepositsToReturn;

    /**
     * Gets the value of the maxDepositsToReturn property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxDepositsToReturn() {
        return maxDepositsToReturn;
    }

    /**
     * Sets the value of the maxDepositsToReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxDepositsToReturn(BigInteger value) {
        this.maxDepositsToReturn = value;
    }

}
