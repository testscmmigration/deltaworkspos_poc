
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BPValidationResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BPValidationResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="mgiSessionID" type="{http://www.moneygram.com/AgentConnect1705}MGISessionIDType"/>
 *                   &lt;element name="readyForCommit" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="GAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType"/>
 *                   &lt;element name="token" type="{http://www.moneygram.com/AgentConnect1705}TokenType" minOccurs="0"/>
 *                   &lt;element name="mgiRewardsNumber" type="{http://www.moneygram.com/AgentConnect1705}StringMax20Type" minOccurs="0"/>
 *                   &lt;element name="productVariant" type="{http://www.moneygram.com/AgentConnect1705}ProductVariantType"/>
 *                   &lt;element name="billerCutoffTime" type="{http://www.moneygram.com/AgentConnect1705}StringMax12Type" minOccurs="0"/>
 *                   &lt;element name="billerAddress" type="{http://www.moneygram.com/AgentConnect1705}AddressType" minOccurs="0"/>
 *                   &lt;element name="billerAddress2" type="{http://www.moneygram.com/AgentConnect1705}AddressType" minOccurs="0"/>
 *                   &lt;element name="billerAddress3" type="{http://www.moneygram.com/AgentConnect1705}AddressType" minOccurs="0"/>
 *                   &lt;element name="billerCity" type="{http://www.moneygram.com/AgentConnect1705}CityType" minOccurs="0"/>
 *                   &lt;element name="billerState" type="{http://www.moneygram.com/AgentConnect1705}CountrySubdivisionCodeType" minOccurs="0"/>
 *                   &lt;element name="billerPostalCode" type="{http://www.moneygram.com/AgentConnect1705}PostalType" minOccurs="0"/>
 *                   &lt;element name="billerPhone" type="{http://www.moneygram.com/AgentConnect1705}PhoneType" minOccurs="0"/>
 *                   &lt;element name="agentTransactionId" type="{http://www.moneygram.com/AgentConnect1705}StringMax20Type" minOccurs="0"/>
 *                   &lt;element name="serviceOfferingID" type="{http://www.moneygram.com/AgentConnect1705}ServiceOfferingIDType" minOccurs="0"/>
 *                   &lt;element name="processingFee" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType" minOccurs="0"/>
 *                   &lt;element name="infoFeeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                   &lt;element name="exchangeRateApplied" type="{http://www.moneygram.com/AgentConnect1705}FXRateType" minOccurs="0"/>
 *                   &lt;element name="sendAmounts" type="{http://www.moneygram.com/AgentConnect1705}SendAmountInfo" minOccurs="0"/>
 *                   &lt;element name="receiveAmounts" type="{http://www.moneygram.com/AgentConnect1705}ReceiveAmountInfo" minOccurs="0"/>
 *                   &lt;element name="promotionInfos" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="promotionInfo" type="{http://www.moneygram.com/AgentConnect1705}PromotionInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="fieldsToCollect" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="fieldToCollect" type="{http://www.moneygram.com/AgentConnect1705}InfoBase" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="receipts" type="{http://www.moneygram.com/AgentConnect1705}PreCompletionReceiptType" minOccurs="0"/>
 *                   &lt;element name="receiptInfo" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="billerWebsite" type="{http://www.moneygram.com/AgentConnect1705}StringMax128Type" minOccurs="0"/>
 *                             &lt;element name="printMGICustomerServiceNumber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                             &lt;element name="expectedPostingTimeFrames" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="expectedPostingTimeFrame" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="billerNotes" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="billerNote" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="serviceOfferingDescriptions" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="serviceOfferingDescription" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="customerTips" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="customerTip" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="promotionalMessages" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="promotionalMessage" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="disclosureTexts" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="disclosureText" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BPValidationResponse", propOrder = {
    "payload"
})
public class BPValidationResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<BPValidationResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BPValidationResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<BPValidationResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BPValidationResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<BPValidationResponse.Payload> value) {
        this.payload = ((JAXBElement<BPValidationResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="mgiSessionID" type="{http://www.moneygram.com/AgentConnect1705}MGISessionIDType"/>
     *         &lt;element name="readyForCommit" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="GAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType"/>
     *         &lt;element name="token" type="{http://www.moneygram.com/AgentConnect1705}TokenType" minOccurs="0"/>
     *         &lt;element name="mgiRewardsNumber" type="{http://www.moneygram.com/AgentConnect1705}StringMax20Type" minOccurs="0"/>
     *         &lt;element name="productVariant" type="{http://www.moneygram.com/AgentConnect1705}ProductVariantType"/>
     *         &lt;element name="billerCutoffTime" type="{http://www.moneygram.com/AgentConnect1705}StringMax12Type" minOccurs="0"/>
     *         &lt;element name="billerAddress" type="{http://www.moneygram.com/AgentConnect1705}AddressType" minOccurs="0"/>
     *         &lt;element name="billerAddress2" type="{http://www.moneygram.com/AgentConnect1705}AddressType" minOccurs="0"/>
     *         &lt;element name="billerAddress3" type="{http://www.moneygram.com/AgentConnect1705}AddressType" minOccurs="0"/>
     *         &lt;element name="billerCity" type="{http://www.moneygram.com/AgentConnect1705}CityType" minOccurs="0"/>
     *         &lt;element name="billerState" type="{http://www.moneygram.com/AgentConnect1705}CountrySubdivisionCodeType" minOccurs="0"/>
     *         &lt;element name="billerPostalCode" type="{http://www.moneygram.com/AgentConnect1705}PostalType" minOccurs="0"/>
     *         &lt;element name="billerPhone" type="{http://www.moneygram.com/AgentConnect1705}PhoneType" minOccurs="0"/>
     *         &lt;element name="agentTransactionId" type="{http://www.moneygram.com/AgentConnect1705}StringMax20Type" minOccurs="0"/>
     *         &lt;element name="serviceOfferingID" type="{http://www.moneygram.com/AgentConnect1705}ServiceOfferingIDType" minOccurs="0"/>
     *         &lt;element name="processingFee" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType" minOccurs="0"/>
     *         &lt;element name="infoFeeIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *         &lt;element name="exchangeRateApplied" type="{http://www.moneygram.com/AgentConnect1705}FXRateType" minOccurs="0"/>
     *         &lt;element name="sendAmounts" type="{http://www.moneygram.com/AgentConnect1705}SendAmountInfo" minOccurs="0"/>
     *         &lt;element name="receiveAmounts" type="{http://www.moneygram.com/AgentConnect1705}ReceiveAmountInfo" minOccurs="0"/>
     *         &lt;element name="promotionInfos" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="promotionInfo" type="{http://www.moneygram.com/AgentConnect1705}PromotionInfo" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="fieldsToCollect" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="fieldToCollect" type="{http://www.moneygram.com/AgentConnect1705}InfoBase" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="receipts" type="{http://www.moneygram.com/AgentConnect1705}PreCompletionReceiptType" minOccurs="0"/>
     *         &lt;element name="receiptInfo" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="billerWebsite" type="{http://www.moneygram.com/AgentConnect1705}StringMax128Type" minOccurs="0"/>
     *                   &lt;element name="printMGICustomerServiceNumber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *                   &lt;element name="expectedPostingTimeFrames" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="expectedPostingTimeFrame" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="billerNotes" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="billerNote" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="serviceOfferingDescriptions" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="serviceOfferingDescription" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="customerTips" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="customerTip" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="promotionalMessages" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="promotionalMessage" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="disclosureTexts" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="disclosureText" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mgiSessionID",
        "readyForCommit",
        "gafVersionNumber",
        "token",
        "mgiRewardsNumber",
        "productVariant",
        "billerCutoffTime",
        "billerAddress",
        "billerAddress2",
        "billerAddress3",
        "billerCity",
        "billerState",
        "billerPostalCode",
        "billerPhone",
        "agentTransactionId",
        "serviceOfferingID",
        "processingFee",
        "infoFeeIndicator",
        "exchangeRateApplied",
        "sendAmounts",
        "receiveAmounts",
        "promotionInfos",
        "fieldsToCollect",
        "receipts",
        "receiptInfo"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(required = true)
        protected String mgiSessionID;
        protected boolean readyForCommit;
        @XmlElement(name = "GAFVersionNumber", required = true)
        protected String gafVersionNumber;
        protected String token;
        protected String mgiRewardsNumber;
        @XmlElement(required = true)
        protected ProductVariantType productVariant;
        protected String billerCutoffTime;
        protected String billerAddress;
        protected String billerAddress2;
        protected String billerAddress3;
        protected String billerCity;
        protected String billerState;
        protected String billerPostalCode;
        protected String billerPhone;
        protected String agentTransactionId;
        protected String serviceOfferingID;
        protected BigDecimal processingFee;
        protected Boolean infoFeeIndicator;
        protected BigDecimal exchangeRateApplied;
        protected SendAmountInfo sendAmounts;
        protected ReceiveAmountInfo receiveAmounts;
        protected BPValidationResponse.Payload.PromotionInfos promotionInfos;
        protected BPValidationResponse.Payload.FieldsToCollect fieldsToCollect;
        protected PreCompletionReceiptType receipts;
        protected BPValidationResponse.Payload.ReceiptInfo receiptInfo;

        /**
         * Gets the value of the mgiSessionID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMgiSessionID() {
            return mgiSessionID;
        }

        /**
         * Sets the value of the mgiSessionID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMgiSessionID(String value) {
            this.mgiSessionID = value;
        }

        /**
         * Gets the value of the readyForCommit property.
         * 
         */
        public boolean isReadyForCommit() {
            return readyForCommit;
        }

        /**
         * Sets the value of the readyForCommit property.
         * 
         */
        public void setReadyForCommit(boolean value) {
            this.readyForCommit = value;
        }

        /**
         * Gets the value of the gafVersionNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGAFVersionNumber() {
            return gafVersionNumber;
        }

        /**
         * Sets the value of the gafVersionNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGAFVersionNumber(String value) {
            this.gafVersionNumber = value;
        }

        /**
         * Gets the value of the token property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getToken() {
            return token;
        }

        /**
         * Sets the value of the token property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setToken(String value) {
            this.token = value;
        }

        /**
         * Gets the value of the mgiRewardsNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMgiRewardsNumber() {
            return mgiRewardsNumber;
        }

        /**
         * Sets the value of the mgiRewardsNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMgiRewardsNumber(String value) {
            this.mgiRewardsNumber = value;
        }

        /**
         * Gets the value of the productVariant property.
         * 
         * @return
         *     possible object is
         *     {@link ProductVariantType }
         *     
         */
        public ProductVariantType getProductVariant() {
            return productVariant;
        }

        /**
         * Sets the value of the productVariant property.
         * 
         * @param value
         *     allowed object is
         *     {@link ProductVariantType }
         *     
         */
        public void setProductVariant(ProductVariantType value) {
            this.productVariant = value;
        }

        /**
         * Gets the value of the billerCutoffTime property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBillerCutoffTime() {
            return billerCutoffTime;
        }

        /**
         * Sets the value of the billerCutoffTime property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBillerCutoffTime(String value) {
            this.billerCutoffTime = value;
        }

        /**
         * Gets the value of the billerAddress property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBillerAddress() {
            return billerAddress;
        }

        /**
         * Sets the value of the billerAddress property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBillerAddress(String value) {
            this.billerAddress = value;
        }

        /**
         * Gets the value of the billerAddress2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBillerAddress2() {
            return billerAddress2;
        }

        /**
         * Sets the value of the billerAddress2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBillerAddress2(String value) {
            this.billerAddress2 = value;
        }

        /**
         * Gets the value of the billerAddress3 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBillerAddress3() {
            return billerAddress3;
        }

        /**
         * Sets the value of the billerAddress3 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBillerAddress3(String value) {
            this.billerAddress3 = value;
        }

        /**
         * Gets the value of the billerCity property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBillerCity() {
            return billerCity;
        }

        /**
         * Sets the value of the billerCity property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBillerCity(String value) {
            this.billerCity = value;
        }

        /**
         * Gets the value of the billerState property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBillerState() {
            return billerState;
        }

        /**
         * Sets the value of the billerState property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBillerState(String value) {
            this.billerState = value;
        }

        /**
         * Gets the value of the billerPostalCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBillerPostalCode() {
            return billerPostalCode;
        }

        /**
         * Sets the value of the billerPostalCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBillerPostalCode(String value) {
            this.billerPostalCode = value;
        }

        /**
         * Gets the value of the billerPhone property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBillerPhone() {
            return billerPhone;
        }

        /**
         * Sets the value of the billerPhone property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBillerPhone(String value) {
            this.billerPhone = value;
        }

        /**
         * Gets the value of the agentTransactionId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAgentTransactionId() {
            return agentTransactionId;
        }

        /**
         * Sets the value of the agentTransactionId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAgentTransactionId(String value) {
            this.agentTransactionId = value;
        }

        /**
         * Gets the value of the serviceOfferingID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getServiceOfferingID() {
            return serviceOfferingID;
        }

        /**
         * Sets the value of the serviceOfferingID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setServiceOfferingID(String value) {
            this.serviceOfferingID = value;
        }

        /**
         * Gets the value of the processingFee property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getProcessingFee() {
            return processingFee;
        }

        /**
         * Sets the value of the processingFee property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setProcessingFee(BigDecimal value) {
            this.processingFee = value;
        }

        /**
         * Gets the value of the infoFeeIndicator property.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isInfoFeeIndicator() {
            return infoFeeIndicator;
        }

        /**
         * Sets the value of the infoFeeIndicator property.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setInfoFeeIndicator(Boolean value) {
            this.infoFeeIndicator = value;
        }

        /**
         * Gets the value of the exchangeRateApplied property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getExchangeRateApplied() {
            return exchangeRateApplied;
        }

        /**
         * Sets the value of the exchangeRateApplied property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setExchangeRateApplied(BigDecimal value) {
            this.exchangeRateApplied = value;
        }

        /**
         * Gets the value of the sendAmounts property.
         * 
         * @return
         *     possible object is
         *     {@link SendAmountInfo }
         *     
         */
        public SendAmountInfo getSendAmounts() {
            return sendAmounts;
        }

        /**
         * Sets the value of the sendAmounts property.
         * 
         * @param value
         *     allowed object is
         *     {@link SendAmountInfo }
         *     
         */
        public void setSendAmounts(SendAmountInfo value) {
            this.sendAmounts = value;
        }

        /**
         * Gets the value of the receiveAmounts property.
         * 
         * @return
         *     possible object is
         *     {@link ReceiveAmountInfo }
         *     
         */
        public ReceiveAmountInfo getReceiveAmounts() {
            return receiveAmounts;
        }

        /**
         * Sets the value of the receiveAmounts property.
         * 
         * @param value
         *     allowed object is
         *     {@link ReceiveAmountInfo }
         *     
         */
        public void setReceiveAmounts(ReceiveAmountInfo value) {
            this.receiveAmounts = value;
        }

        /**
         * Gets the value of the promotionInfos property.
         * 
         * @return
         *     possible object is
         *     {@link BPValidationResponse.Payload.PromotionInfos }
         *     
         */
        public BPValidationResponse.Payload.PromotionInfos getPromotionInfos() {
            return promotionInfos;
        }

        /**
         * Sets the value of the promotionInfos property.
         * 
         * @param value
         *     allowed object is
         *     {@link BPValidationResponse.Payload.PromotionInfos }
         *     
         */
        public void setPromotionInfos(BPValidationResponse.Payload.PromotionInfos value) {
            this.promotionInfos = value;
        }

        /**
         * Gets the value of the fieldsToCollect property.
         * 
         * @return
         *     possible object is
         *     {@link BPValidationResponse.Payload.FieldsToCollect }
         *     
         */
        public BPValidationResponse.Payload.FieldsToCollect getFieldsToCollect() {
            return fieldsToCollect;
        }

        /**
         * Sets the value of the fieldsToCollect property.
         * 
         * @param value
         *     allowed object is
         *     {@link BPValidationResponse.Payload.FieldsToCollect }
         *     
         */
        public void setFieldsToCollect(BPValidationResponse.Payload.FieldsToCollect value) {
            this.fieldsToCollect = value;
        }

        /**
         * Gets the value of the receipts property.
         * 
         * @return
         *     possible object is
         *     {@link PreCompletionReceiptType }
         *     
         */
        public PreCompletionReceiptType getReceipts() {
            return receipts;
        }

        /**
         * Sets the value of the receipts property.
         * 
         * @param value
         *     allowed object is
         *     {@link PreCompletionReceiptType }
         *     
         */
        public void setReceipts(PreCompletionReceiptType value) {
            this.receipts = value;
        }

        /**
         * Gets the value of the receiptInfo property.
         * 
         * @return
         *     possible object is
         *     {@link BPValidationResponse.Payload.ReceiptInfo }
         *     
         */
        public BPValidationResponse.Payload.ReceiptInfo getReceiptInfo() {
            return receiptInfo;
        }

        /**
         * Sets the value of the receiptInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link BPValidationResponse.Payload.ReceiptInfo }
         *     
         */
        public void setReceiptInfo(BPValidationResponse.Payload.ReceiptInfo value) {
            this.receiptInfo = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="fieldToCollect" type="{http://www.moneygram.com/AgentConnect1705}InfoBase" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "fieldToCollect"
        })
        public static class FieldsToCollect
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected List<InfoBase> fieldToCollect;

            /**
             * Gets the value of the fieldToCollect property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the fieldToCollect property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getFieldToCollect().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link InfoBase }
             * 
             * 
             */
            public List<InfoBase> getFieldToCollect() {
                if (fieldToCollect == null) {
                    fieldToCollect = new ArrayList<InfoBase>();
                }
                return this.fieldToCollect;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="promotionInfo" type="{http://www.moneygram.com/AgentConnect1705}PromotionInfo" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "promotionInfo"
        })
        public static class PromotionInfos
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected List<PromotionInfo> promotionInfo;

            /**
             * Gets the value of the promotionInfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the promotionInfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getPromotionInfo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link PromotionInfo }
             * 
             * 
             */
            public List<PromotionInfo> getPromotionInfo() {
                if (promotionInfo == null) {
                    promotionInfo = new ArrayList<PromotionInfo>();
                }
                return this.promotionInfo;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="billerWebsite" type="{http://www.moneygram.com/AgentConnect1705}StringMax128Type" minOccurs="0"/>
         *         &lt;element name="printMGICustomerServiceNumber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
         *         &lt;element name="expectedPostingTimeFrames" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="expectedPostingTimeFrame" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="billerNotes" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="billerNote" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="serviceOfferingDescriptions" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="serviceOfferingDescription" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="customerTips" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="customerTip" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="promotionalMessages" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="promotionalMessage" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="disclosureTexts" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="disclosureText" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "billerWebsite",
            "printMGICustomerServiceNumber",
            "expectedPostingTimeFrames",
            "billerNotes",
            "serviceOfferingDescriptions",
            "customerTips",
            "promotionalMessages",
            "disclosureTexts"
        })
        public static class ReceiptInfo
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected String billerWebsite;
            protected Boolean printMGICustomerServiceNumber;
            protected BPValidationResponse.Payload.ReceiptInfo.ExpectedPostingTimeFrames expectedPostingTimeFrames;
            protected BPValidationResponse.Payload.ReceiptInfo.BillerNotes billerNotes;
            protected BPValidationResponse.Payload.ReceiptInfo.ServiceOfferingDescriptions serviceOfferingDescriptions;
            protected BPValidationResponse.Payload.ReceiptInfo.CustomerTips customerTips;
            protected BPValidationResponse.Payload.ReceiptInfo.PromotionalMessages promotionalMessages;
            protected BPValidationResponse.Payload.ReceiptInfo.DisclosureTexts disclosureTexts;

            /**
             * Gets the value of the billerWebsite property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBillerWebsite() {
                return billerWebsite;
            }

            /**
             * Sets the value of the billerWebsite property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBillerWebsite(String value) {
                this.billerWebsite = value;
            }

            /**
             * Gets the value of the printMGICustomerServiceNumber property.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isPrintMGICustomerServiceNumber() {
                return printMGICustomerServiceNumber;
            }

            /**
             * Sets the value of the printMGICustomerServiceNumber property.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setPrintMGICustomerServiceNumber(Boolean value) {
                this.printMGICustomerServiceNumber = value;
            }

            /**
             * Gets the value of the expectedPostingTimeFrames property.
             * 
             * @return
             *     possible object is
             *     {@link BPValidationResponse.Payload.ReceiptInfo.ExpectedPostingTimeFrames }
             *     
             */
            public BPValidationResponse.Payload.ReceiptInfo.ExpectedPostingTimeFrames getExpectedPostingTimeFrames() {
                return expectedPostingTimeFrames;
            }

            /**
             * Sets the value of the expectedPostingTimeFrames property.
             * 
             * @param value
             *     allowed object is
             *     {@link BPValidationResponse.Payload.ReceiptInfo.ExpectedPostingTimeFrames }
             *     
             */
            public void setExpectedPostingTimeFrames(BPValidationResponse.Payload.ReceiptInfo.ExpectedPostingTimeFrames value) {
                this.expectedPostingTimeFrames = value;
            }

            /**
             * Gets the value of the billerNotes property.
             * 
             * @return
             *     possible object is
             *     {@link BPValidationResponse.Payload.ReceiptInfo.BillerNotes }
             *     
             */
            public BPValidationResponse.Payload.ReceiptInfo.BillerNotes getBillerNotes() {
                return billerNotes;
            }

            /**
             * Sets the value of the billerNotes property.
             * 
             * @param value
             *     allowed object is
             *     {@link BPValidationResponse.Payload.ReceiptInfo.BillerNotes }
             *     
             */
            public void setBillerNotes(BPValidationResponse.Payload.ReceiptInfo.BillerNotes value) {
                this.billerNotes = value;
            }

            /**
             * Gets the value of the serviceOfferingDescriptions property.
             * 
             * @return
             *     possible object is
             *     {@link BPValidationResponse.Payload.ReceiptInfo.ServiceOfferingDescriptions }
             *     
             */
            public BPValidationResponse.Payload.ReceiptInfo.ServiceOfferingDescriptions getServiceOfferingDescriptions() {
                return serviceOfferingDescriptions;
            }

            /**
             * Sets the value of the serviceOfferingDescriptions property.
             * 
             * @param value
             *     allowed object is
             *     {@link BPValidationResponse.Payload.ReceiptInfo.ServiceOfferingDescriptions }
             *     
             */
            public void setServiceOfferingDescriptions(BPValidationResponse.Payload.ReceiptInfo.ServiceOfferingDescriptions value) {
                this.serviceOfferingDescriptions = value;
            }

            /**
             * Gets the value of the customerTips property.
             * 
             * @return
             *     possible object is
             *     {@link BPValidationResponse.Payload.ReceiptInfo.CustomerTips }
             *     
             */
            public BPValidationResponse.Payload.ReceiptInfo.CustomerTips getCustomerTips() {
                return customerTips;
            }

            /**
             * Sets the value of the customerTips property.
             * 
             * @param value
             *     allowed object is
             *     {@link BPValidationResponse.Payload.ReceiptInfo.CustomerTips }
             *     
             */
            public void setCustomerTips(BPValidationResponse.Payload.ReceiptInfo.CustomerTips value) {
                this.customerTips = value;
            }

            /**
             * Gets the value of the promotionalMessages property.
             * 
             * @return
             *     possible object is
             *     {@link BPValidationResponse.Payload.ReceiptInfo.PromotionalMessages }
             *     
             */
            public BPValidationResponse.Payload.ReceiptInfo.PromotionalMessages getPromotionalMessages() {
                return promotionalMessages;
            }

            /**
             * Sets the value of the promotionalMessages property.
             * 
             * @param value
             *     allowed object is
             *     {@link BPValidationResponse.Payload.ReceiptInfo.PromotionalMessages }
             *     
             */
            public void setPromotionalMessages(BPValidationResponse.Payload.ReceiptInfo.PromotionalMessages value) {
                this.promotionalMessages = value;
            }

            /**
             * Gets the value of the disclosureTexts property.
             * 
             * @return
             *     possible object is
             *     {@link BPValidationResponse.Payload.ReceiptInfo.DisclosureTexts }
             *     
             */
            public BPValidationResponse.Payload.ReceiptInfo.DisclosureTexts getDisclosureTexts() {
                return disclosureTexts;
            }

            /**
             * Sets the value of the disclosureTexts property.
             * 
             * @param value
             *     allowed object is
             *     {@link BPValidationResponse.Payload.ReceiptInfo.DisclosureTexts }
             *     
             */
            public void setDisclosureTexts(BPValidationResponse.Payload.ReceiptInfo.DisclosureTexts value) {
                this.disclosureTexts = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="billerNote" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "billerNote"
            })
            public static class BillerNotes
                implements Serializable
            {

                private final static long serialVersionUID = 1L;
                protected List<TextTranslationType> billerNote;

                /**
                 * Gets the value of the billerNote property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the billerNote property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getBillerNote().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link TextTranslationType }
                 * 
                 * 
                 */
                public List<TextTranslationType> getBillerNote() {
                    if (billerNote == null) {
                        billerNote = new ArrayList<TextTranslationType>();
                    }
                    return this.billerNote;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="customerTip" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "customerTip"
            })
            public static class CustomerTips
                implements Serializable
            {

                private final static long serialVersionUID = 1L;
                protected List<TextTranslationType> customerTip;

                /**
                 * Gets the value of the customerTip property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the customerTip property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getCustomerTip().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link TextTranslationType }
                 * 
                 * 
                 */
                public List<TextTranslationType> getCustomerTip() {
                    if (customerTip == null) {
                        customerTip = new ArrayList<TextTranslationType>();
                    }
                    return this.customerTip;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="disclosureText" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "disclosureText"
            })
            public static class DisclosureTexts
                implements Serializable
            {

                private final static long serialVersionUID = 1L;
                protected List<TextTranslationType> disclosureText;

                /**
                 * Gets the value of the disclosureText property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the disclosureText property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getDisclosureText().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link TextTranslationType }
                 * 
                 * 
                 */
                public List<TextTranslationType> getDisclosureText() {
                    if (disclosureText == null) {
                        disclosureText = new ArrayList<TextTranslationType>();
                    }
                    return this.disclosureText;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="expectedPostingTimeFrame" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "expectedPostingTimeFrame"
            })
            public static class ExpectedPostingTimeFrames
                implements Serializable
            {

                private final static long serialVersionUID = 1L;
                protected List<TextTranslationType> expectedPostingTimeFrame;

                /**
                 * Gets the value of the expectedPostingTimeFrame property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the expectedPostingTimeFrame property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getExpectedPostingTimeFrame().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link TextTranslationType }
                 * 
                 * 
                 */
                public List<TextTranslationType> getExpectedPostingTimeFrame() {
                    if (expectedPostingTimeFrame == null) {
                        expectedPostingTimeFrame = new ArrayList<TextTranslationType>();
                    }
                    return this.expectedPostingTimeFrame;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="promotionalMessage" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "promotionalMessage"
            })
            public static class PromotionalMessages
                implements Serializable
            {

                private final static long serialVersionUID = 1L;
                protected List<TextTranslationType> promotionalMessage;

                /**
                 * Gets the value of the promotionalMessage property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the promotionalMessage property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getPromotionalMessage().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link TextTranslationType }
                 * 
                 * 
                 */
                public List<TextTranslationType> getPromotionalMessage() {
                    if (promotionalMessage == null) {
                        promotionalMessage = new ArrayList<TextTranslationType>();
                    }
                    return this.promotionalMessage;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="serviceOfferingDescription" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "serviceOfferingDescription"
            })
            public static class ServiceOfferingDescriptions
                implements Serializable
            {

                private final static long serialVersionUID = 1L;
                protected List<TextTranslationType> serviceOfferingDescription;

                /**
                 * Gets the value of the serviceOfferingDescription property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the serviceOfferingDescription property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getServiceOfferingDescription().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link TextTranslationType }
                 * 
                 * 
                 */
                public List<TextTranslationType> getServiceOfferingDescription() {
                    if (serviceOfferingDescription == null) {
                        serviceOfferingDescription = new ArrayList<TextTranslationType>();
                    }
                    return this.serviceOfferingDescription;
                }

            }

        }

    }

}
