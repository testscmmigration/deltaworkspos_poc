
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PartnerCreditType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PartnerCreditType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="creditLimitInfo" type="{http://www.moneygram.com/AgentConnect1705}CreditLimitInfo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="goodFundsAgentFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="depositProofRequiredFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartnerCreditType", propOrder = {
    "creditLimitInfo",
    "goodFundsAgentFlag",
    "depositProofRequiredFlag"
})
public class PartnerCreditType
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected List<CreditLimitInfo> creditLimitInfo;
    protected Boolean goodFundsAgentFlag;
    protected Boolean depositProofRequiredFlag;

    /**
     * Gets the value of the creditLimitInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the creditLimitInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreditLimitInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreditLimitInfo }
     * 
     * 
     */
    public List<CreditLimitInfo> getCreditLimitInfo() {
        if (creditLimitInfo == null) {
            creditLimitInfo = new ArrayList<CreditLimitInfo>();
        }
        return this.creditLimitInfo;
    }

    /**
     * Gets the value of the goodFundsAgentFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isGoodFundsAgentFlag() {
        return goodFundsAgentFlag;
    }

    /**
     * Sets the value of the goodFundsAgentFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGoodFundsAgentFlag(Boolean value) {
        this.goodFundsAgentFlag = value;
    }

    /**
     * Gets the value of the depositProofRequiredFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDepositProofRequiredFlag() {
        return depositProofRequiredFlag;
    }

    /**
     * Sets the value of the depositProofRequiredFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDepositProofRequiredFlag(Boolean value) {
        this.depositProofRequiredFlag = value;
    }

}
