
package com.moneygram.agentconnect;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MoneyOrderVoidReasonCodeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MoneyOrderVoidReasonCodeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DISPENSER_ERROR"/>
 *     &lt;enumeration value="DEMO_MODE"/>
 *     &lt;enumeration value="TRAINING_MODE"/>
 *     &lt;enumeration value="TEST_VOID"/>
 *     &lt;enumeration value="VOID_REMAINING"/>
 *     &lt;enumeration value="LAST_ITEM"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MoneyOrderVoidReasonCodeType")
@XmlEnum
public enum MoneyOrderVoidReasonCodeType {

    DISPENSER_ERROR,
    DEMO_MODE,
    TRAINING_MODE,
    TEST_VOID,
    VOID_REMAINING,
    LAST_ITEM;

    public String value() {
        return name();
    }

    public static MoneyOrderVoidReasonCodeType fromValue(String v) {
        return valueOf(v);
    }

}
