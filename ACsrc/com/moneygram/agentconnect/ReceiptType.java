
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReceiptType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReceiptType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="receiptText" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="md5CheckSum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="version" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="longLanguageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="province" type="{http://www.moneygram.com/AgentConnect1705}StringMax3Type" minOccurs="0"/>
 *         &lt;element name="additionalLanguages" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="longLanguageCode" type="{http://www.moneygram.com/AgentConnect1705}LanguageLongCodeType" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReceiptType", propOrder = {
    "receiptText",
    "md5CheckSum",
    "version",
    "longLanguageCode",
    "province",
    "additionalLanguages"
})
public class ReceiptType
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected String receiptText;
    @XmlElement(required = true)
    protected String md5CheckSum;
    @XmlElement(required = true)
    protected String version;
    @XmlElement(required = true)
    protected String longLanguageCode;
    protected String province;
    protected ReceiptType.AdditionalLanguages additionalLanguages;

    /**
     * Gets the value of the receiptText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiptText() {
        return receiptText;
    }

    /**
     * Sets the value of the receiptText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiptText(String value) {
        this.receiptText = value;
    }

    /**
     * Gets the value of the md5CheckSum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMd5CheckSum() {
        return md5CheckSum;
    }

    /**
     * Sets the value of the md5CheckSum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMd5CheckSum(String value) {
        this.md5CheckSum = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the longLanguageCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLongLanguageCode() {
        return longLanguageCode;
    }

    /**
     * Sets the value of the longLanguageCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLongLanguageCode(String value) {
        this.longLanguageCode = value;
    }

    /**
     * Gets the value of the province property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvince() {
        return province;
    }

    /**
     * Sets the value of the province property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvince(String value) {
        this.province = value;
    }

    /**
     * Gets the value of the additionalLanguages property.
     * 
     * @return
     *     possible object is
     *     {@link ReceiptType.AdditionalLanguages }
     *     
     */
    public ReceiptType.AdditionalLanguages getAdditionalLanguages() {
        return additionalLanguages;
    }

    /**
     * Sets the value of the additionalLanguages property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReceiptType.AdditionalLanguages }
     *     
     */
    public void setAdditionalLanguages(ReceiptType.AdditionalLanguages value) {
        this.additionalLanguages = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="longLanguageCode" type="{http://www.moneygram.com/AgentConnect1705}LanguageLongCodeType" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "longLanguageCode"
    })
    public static class AdditionalLanguages
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(required = true)
        protected List<String> longLanguageCode;

        /**
         * Gets the value of the longLanguageCode property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the longLanguageCode property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getLongLanguageCode().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getLongLanguageCode() {
            if (longLanguageCode == null) {
                longLanguageCode = new ArrayList<String>();
            }
            return this.longLanguageCode;
        }

    }

}
