
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SearchStagedTransactionsRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchStagedTransactionsRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="mgiRewardsNumber" type="{http://www.moneygram.com/AgentConnect1705}StringMax20Type" minOccurs="0"/>
 *         &lt;element name="senderPhoneNumber" type="{http://www.moneygram.com/AgentConnect1705}PhoneShortType" minOccurs="0"/>
 *         &lt;element name="senderFirstName" type="{http://www.moneygram.com/AgentConnect1705}NameFirstType" minOccurs="0"/>
 *         &lt;element name="senderLastName" type="{http://www.moneygram.com/AgentConnect1705}NameLastType" minOccurs="0"/>
 *         &lt;element name="billerAccountNumber" type="{http://www.moneygram.com/AgentConnect1705}BillerAccountNumberType" minOccurs="0"/>
 *         &lt;element name="mgiSessionType" type="{http://www.moneygram.com/AgentConnect1705}SessionType" minOccurs="0"/>
 *         &lt;element name="thisLocationOnly" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="maxRowsToReturn" type="{http://www.moneygram.com/AgentConnect1705}Int3Type" minOccurs="0"/>
 *         &lt;element name="returnFormFreeOnly" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchStagedTransactionsRequest", propOrder = {
    "mgiRewardsNumber",
    "senderPhoneNumber",
    "senderFirstName",
    "senderLastName",
    "billerAccountNumber",
    "mgiSessionType",
    "thisLocationOnly",
    "maxRowsToReturn",
    "returnFormFreeOnly"
})
public class SearchStagedTransactionsRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected String mgiRewardsNumber;
    protected String senderPhoneNumber;
    protected String senderFirstName;
    protected String senderLastName;
    protected String billerAccountNumber;
    protected SessionType mgiSessionType;
    protected boolean thisLocationOnly;
    protected BigInteger maxRowsToReturn;
    protected boolean returnFormFreeOnly;

    /**
     * Gets the value of the mgiRewardsNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMgiRewardsNumber() {
        return mgiRewardsNumber;
    }

    /**
     * Sets the value of the mgiRewardsNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMgiRewardsNumber(String value) {
        this.mgiRewardsNumber = value;
    }

    /**
     * Gets the value of the senderPhoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderPhoneNumber() {
        return senderPhoneNumber;
    }

    /**
     * Sets the value of the senderPhoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderPhoneNumber(String value) {
        this.senderPhoneNumber = value;
    }

    /**
     * Gets the value of the senderFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderFirstName() {
        return senderFirstName;
    }

    /**
     * Sets the value of the senderFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderFirstName(String value) {
        this.senderFirstName = value;
    }

    /**
     * Gets the value of the senderLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderLastName() {
        return senderLastName;
    }

    /**
     * Sets the value of the senderLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderLastName(String value) {
        this.senderLastName = value;
    }

    /**
     * Gets the value of the billerAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillerAccountNumber() {
        return billerAccountNumber;
    }

    /**
     * Sets the value of the billerAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillerAccountNumber(String value) {
        this.billerAccountNumber = value;
    }

    /**
     * Gets the value of the mgiSessionType property.
     * 
     * @return
     *     possible object is
     *     {@link SessionType }
     *     
     */
    public SessionType getMgiSessionType() {
        return mgiSessionType;
    }

    /**
     * Sets the value of the mgiSessionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SessionType }
     *     
     */
    public void setMgiSessionType(SessionType value) {
        this.mgiSessionType = value;
    }

    /**
     * Gets the value of the thisLocationOnly property.
     * 
     */
    public boolean isThisLocationOnly() {
        return thisLocationOnly;
    }

    /**
     * Sets the value of the thisLocationOnly property.
     * 
     */
    public void setThisLocationOnly(boolean value) {
        this.thisLocationOnly = value;
    }

    /**
     * Gets the value of the maxRowsToReturn property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxRowsToReturn() {
        return maxRowsToReturn;
    }

    /**
     * Sets the value of the maxRowsToReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxRowsToReturn(BigInteger value) {
        this.maxRowsToReturn = value;
    }

    /**
     * Gets the value of the returnFormFreeOnly property.
     * 
     */
    public boolean isReturnFormFreeOnly() {
        return returnFormFreeOnly;
    }

    /**
     * Sets the value of the returnFormFreeOnly property.
     * 
     */
    public void setReturnFormFreeOnly(boolean value) {
        this.returnFormFreeOnly = value;
    }

}
