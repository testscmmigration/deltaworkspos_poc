
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InjectedInstructionRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InjectedInstructionRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="previousPOETransactionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="additionalTransactionInfo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InjectedInstructionRequest", propOrder = {
    "previousPOETransactionID",
    "additionalTransactionInfo"
})
public class InjectedInstructionRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected String previousPOETransactionID;
    @XmlElement(required = true)
    protected String additionalTransactionInfo;

    /**
     * Gets the value of the previousPOETransactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreviousPOETransactionID() {
        return previousPOETransactionID;
    }

    /**
     * Sets the value of the previousPOETransactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreviousPOETransactionID(String value) {
        this.previousPOETransactionID = value;
    }

    /**
     * Gets the value of the additionalTransactionInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdditionalTransactionInfo() {
        return additionalTransactionInfo;
    }

    /**
     * Sets the value of the additionalTransactionInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdditionalTransactionInfo(String value) {
        this.additionalTransactionInfo = value;
    }

}
