
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for CompleteSessionResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CompleteSessionResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="confirmationNumber" type="{http://www.moneygram.com/AgentConnect1705}ConfirmationNumberType" minOccurs="0"/>
 *                   &lt;element name="referenceNumber" type="{http://www.moneygram.com/AgentConnect1705}ReferenceNumberType" minOccurs="0"/>
 *                   &lt;element name="transactionID" type="{http://www.moneygram.com/AgentConnect1705}StringMax18Type" minOccurs="0"/>
 *                   &lt;element name="referenceNumberText" type="{http://www.moneygram.com/AgentConnect1705}ReferenceNumberTextType" minOccurs="0"/>
 *                   &lt;element name="partnerConfirmationNumber" type="{http://www.moneygram.com/AgentConnect1705}PartnerConfirmationNumberType" minOccurs="0"/>
 *                   &lt;element name="partnerName" type="{http://www.moneygram.com/AgentConnect1705}PartnerNameType" minOccurs="0"/>
 *                   &lt;element name="receiveAgentAbbreviation" type="{http://www.moneygram.com/AgentConnect1705}ReceiveAgentAbbreviationType" minOccurs="0"/>
 *                   &lt;element name="freePhoneCallPIN" type="{http://www.moneygram.com/AgentConnect1705}FreePhoneCallPINType" minOccurs="0"/>
 *                   &lt;element name="tollFreePhoneNumber" type="{http://www.moneygram.com/AgentConnect1705}PhoneType" minOccurs="0"/>
 *                   &lt;element name="expectedDateOfDelivery" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="transactionDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *                   &lt;element name="agentCheckAuthorizationNumber" type="{http://www.moneygram.com/AgentConnect1705}AgentCheckAuthNumber" minOccurs="0"/>
 *                   &lt;element name="receipts" type="{http://www.moneygram.com/AgentConnect1705}CompletionReceiptType" minOccurs="0"/>
 *                   &lt;element name="receiptInfo" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="receiptTextInfos" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="receiptTextInfo" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CompleteSessionResponse", propOrder = {
    "payload"
})
public class CompleteSessionResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<CompleteSessionResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CompleteSessionResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<CompleteSessionResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CompleteSessionResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<CompleteSessionResponse.Payload> value) {
        this.payload = ((JAXBElement<CompleteSessionResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="confirmationNumber" type="{http://www.moneygram.com/AgentConnect1705}ConfirmationNumberType" minOccurs="0"/>
     *         &lt;element name="referenceNumber" type="{http://www.moneygram.com/AgentConnect1705}ReferenceNumberType" minOccurs="0"/>
     *         &lt;element name="transactionID" type="{http://www.moneygram.com/AgentConnect1705}StringMax18Type" minOccurs="0"/>
     *         &lt;element name="referenceNumberText" type="{http://www.moneygram.com/AgentConnect1705}ReferenceNumberTextType" minOccurs="0"/>
     *         &lt;element name="partnerConfirmationNumber" type="{http://www.moneygram.com/AgentConnect1705}PartnerConfirmationNumberType" minOccurs="0"/>
     *         &lt;element name="partnerName" type="{http://www.moneygram.com/AgentConnect1705}PartnerNameType" minOccurs="0"/>
     *         &lt;element name="receiveAgentAbbreviation" type="{http://www.moneygram.com/AgentConnect1705}ReceiveAgentAbbreviationType" minOccurs="0"/>
     *         &lt;element name="freePhoneCallPIN" type="{http://www.moneygram.com/AgentConnect1705}FreePhoneCallPINType" minOccurs="0"/>
     *         &lt;element name="tollFreePhoneNumber" type="{http://www.moneygram.com/AgentConnect1705}PhoneType" minOccurs="0"/>
     *         &lt;element name="expectedDateOfDelivery" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="transactionDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
     *         &lt;element name="agentCheckAuthorizationNumber" type="{http://www.moneygram.com/AgentConnect1705}AgentCheckAuthNumber" minOccurs="0"/>
     *         &lt;element name="receipts" type="{http://www.moneygram.com/AgentConnect1705}CompletionReceiptType" minOccurs="0"/>
     *         &lt;element name="receiptInfo" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="receiptTextInfos" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="receiptTextInfo" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "confirmationNumber",
        "referenceNumber",
        "transactionID",
        "referenceNumberText",
        "partnerConfirmationNumber",
        "partnerName",
        "receiveAgentAbbreviation",
        "freePhoneCallPIN",
        "tollFreePhoneNumber",
        "expectedDateOfDelivery",
        "transactionDateTime",
        "agentCheckAuthorizationNumber",
        "receipts",
        "receiptInfo"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected String confirmationNumber;
        protected String referenceNumber;
        protected String transactionID;
        protected String referenceNumberText;
        protected String partnerConfirmationNumber;
        protected String partnerName;
        protected String receiveAgentAbbreviation;
        protected String freePhoneCallPIN;
        protected String tollFreePhoneNumber;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar expectedDateOfDelivery;
        @XmlElement(required = true)
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar transactionDateTime;
        protected String agentCheckAuthorizationNumber;
        protected CompletionReceiptType receipts;
        protected CompleteSessionResponse.Payload.ReceiptInfo receiptInfo;

        /**
         * Gets the value of the confirmationNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getConfirmationNumber() {
            return confirmationNumber;
        }

        /**
         * Sets the value of the confirmationNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setConfirmationNumber(String value) {
            this.confirmationNumber = value;
        }

        /**
         * Gets the value of the referenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReferenceNumber() {
            return referenceNumber;
        }

        /**
         * Sets the value of the referenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReferenceNumber(String value) {
            this.referenceNumber = value;
        }

        /**
         * Gets the value of the transactionID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTransactionID() {
            return transactionID;
        }

        /**
         * Sets the value of the transactionID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTransactionID(String value) {
            this.transactionID = value;
        }

        /**
         * Gets the value of the referenceNumberText property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReferenceNumberText() {
            return referenceNumberText;
        }

        /**
         * Sets the value of the referenceNumberText property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReferenceNumberText(String value) {
            this.referenceNumberText = value;
        }

        /**
         * Gets the value of the partnerConfirmationNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPartnerConfirmationNumber() {
            return partnerConfirmationNumber;
        }

        /**
         * Sets the value of the partnerConfirmationNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPartnerConfirmationNumber(String value) {
            this.partnerConfirmationNumber = value;
        }

        /**
         * Gets the value of the partnerName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPartnerName() {
            return partnerName;
        }

        /**
         * Sets the value of the partnerName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPartnerName(String value) {
            this.partnerName = value;
        }

        /**
         * Gets the value of the receiveAgentAbbreviation property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReceiveAgentAbbreviation() {
            return receiveAgentAbbreviation;
        }

        /**
         * Sets the value of the receiveAgentAbbreviation property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReceiveAgentAbbreviation(String value) {
            this.receiveAgentAbbreviation = value;
        }

        /**
         * Gets the value of the freePhoneCallPIN property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFreePhoneCallPIN() {
            return freePhoneCallPIN;
        }

        /**
         * Sets the value of the freePhoneCallPIN property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFreePhoneCallPIN(String value) {
            this.freePhoneCallPIN = value;
        }

        /**
         * Gets the value of the tollFreePhoneNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTollFreePhoneNumber() {
            return tollFreePhoneNumber;
        }

        /**
         * Sets the value of the tollFreePhoneNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTollFreePhoneNumber(String value) {
            this.tollFreePhoneNumber = value;
        }

        /**
         * Gets the value of the expectedDateOfDelivery property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getExpectedDateOfDelivery() {
            return expectedDateOfDelivery;
        }

        /**
         * Sets the value of the expectedDateOfDelivery property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setExpectedDateOfDelivery(XMLGregorianCalendar value) {
            this.expectedDateOfDelivery = value;
        }

        /**
         * Gets the value of the transactionDateTime property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getTransactionDateTime() {
            return transactionDateTime;
        }

        /**
         * Sets the value of the transactionDateTime property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setTransactionDateTime(XMLGregorianCalendar value) {
            this.transactionDateTime = value;
        }

        /**
         * Gets the value of the agentCheckAuthorizationNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAgentCheckAuthorizationNumber() {
            return agentCheckAuthorizationNumber;
        }

        /**
         * Sets the value of the agentCheckAuthorizationNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAgentCheckAuthorizationNumber(String value) {
            this.agentCheckAuthorizationNumber = value;
        }

        /**
         * Gets the value of the receipts property.
         * 
         * @return
         *     possible object is
         *     {@link CompletionReceiptType }
         *     
         */
        public CompletionReceiptType getReceipts() {
            return receipts;
        }

        /**
         * Sets the value of the receipts property.
         * 
         * @param value
         *     allowed object is
         *     {@link CompletionReceiptType }
         *     
         */
        public void setReceipts(CompletionReceiptType value) {
            this.receipts = value;
        }

        /**
         * Gets the value of the receiptInfo property.
         * 
         * @return
         *     possible object is
         *     {@link CompleteSessionResponse.Payload.ReceiptInfo }
         *     
         */
        public CompleteSessionResponse.Payload.ReceiptInfo getReceiptInfo() {
            return receiptInfo;
        }

        /**
         * Sets the value of the receiptInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link CompleteSessionResponse.Payload.ReceiptInfo }
         *     
         */
        public void setReceiptInfo(CompleteSessionResponse.Payload.ReceiptInfo value) {
            this.receiptInfo = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="receiptTextInfos" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="receiptTextInfo" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "receiptTextInfos"
        })
        public static class ReceiptInfo
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected CompleteSessionResponse.Payload.ReceiptInfo.ReceiptTextInfos receiptTextInfos;

            /**
             * Gets the value of the receiptTextInfos property.
             * 
             * @return
             *     possible object is
             *     {@link CompleteSessionResponse.Payload.ReceiptInfo.ReceiptTextInfos }
             *     
             */
            public CompleteSessionResponse.Payload.ReceiptInfo.ReceiptTextInfos getReceiptTextInfos() {
                return receiptTextInfos;
            }

            /**
             * Sets the value of the receiptTextInfos property.
             * 
             * @param value
             *     allowed object is
             *     {@link CompleteSessionResponse.Payload.ReceiptInfo.ReceiptTextInfos }
             *     
             */
            public void setReceiptTextInfos(CompleteSessionResponse.Payload.ReceiptInfo.ReceiptTextInfos value) {
                this.receiptTextInfos = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="receiptTextInfo" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "receiptTextInfo"
            })
            public static class ReceiptTextInfos
                implements Serializable
            {

                private final static long serialVersionUID = 1L;
                protected List<TextTranslationType> receiptTextInfo;

                /**
                 * Gets the value of the receiptTextInfo property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the receiptTextInfo property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getReceiptTextInfo().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link TextTranslationType }
                 * 
                 * 
                 */
                public List<TextTranslationType> getReceiptTextInfo() {
                    if (receiptTextInfo == null) {
                        receiptTextInfo = new ArrayList<TextTranslationType>();
                    }
                    return this.receiptTextInfo;
                }

            }

        }

    }

}
