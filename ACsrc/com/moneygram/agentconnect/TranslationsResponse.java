
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TranslationsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TranslationsResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="translationsVersion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="countryTranslations" type="{http://www.moneygram.com/AgentConnect1705}CountryTranslationType" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="deliveryOptionTranslations" type="{http://www.moneygram.com/AgentConnect1705}DeliveryOptionTranslationType" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="currencyTranslations" type="{http://www.moneygram.com/AgentConnect1705}CurrencyTranslationType" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="fqdoTextTranslations" type="{http://www.moneygram.com/AgentConnect1705}FQDOTextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="industryTranslations" type="{http://www.moneygram.com/AgentConnect1705}IndustryTranslationType" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="loyaltyTranslations" type="{http://www.moneygram.com/AgentConnect1705}LoyaltyTranslationType" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="loyaltyCardTypeTranslations" type="{http://www.moneygram.com/AgentConnect1705}LoyaltyCardTypeTranslationType" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="loyaltyProgramTypeTranslations" type="{http://www.moneygram.com/AgentConnect1705}LoyaltyProgramTypeTranslationType" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="languageTranslations" type="{http://www.moneygram.com/AgentConnect1705}LanguageTranslationType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TranslationsResponse", propOrder = {
    "payload"
})
public class TranslationsResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<TranslationsResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TranslationsResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<TranslationsResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TranslationsResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<TranslationsResponse.Payload> value) {
        this.payload = ((JAXBElement<TranslationsResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="translationsVersion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="countryTranslations" type="{http://www.moneygram.com/AgentConnect1705}CountryTranslationType" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="deliveryOptionTranslations" type="{http://www.moneygram.com/AgentConnect1705}DeliveryOptionTranslationType" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="currencyTranslations" type="{http://www.moneygram.com/AgentConnect1705}CurrencyTranslationType" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="fqdoTextTranslations" type="{http://www.moneygram.com/AgentConnect1705}FQDOTextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="industryTranslations" type="{http://www.moneygram.com/AgentConnect1705}IndustryTranslationType" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="loyaltyTranslations" type="{http://www.moneygram.com/AgentConnect1705}LoyaltyTranslationType" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="loyaltyCardTypeTranslations" type="{http://www.moneygram.com/AgentConnect1705}LoyaltyCardTypeTranslationType" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="loyaltyProgramTypeTranslations" type="{http://www.moneygram.com/AgentConnect1705}LoyaltyProgramTypeTranslationType" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="languageTranslations" type="{http://www.moneygram.com/AgentConnect1705}LanguageTranslationType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "translationsVersion",
        "countryTranslations",
        "deliveryOptionTranslations",
        "currencyTranslations",
        "fqdoTextTranslations",
        "industryTranslations",
        "loyaltyTranslations",
        "loyaltyCardTypeTranslations",
        "loyaltyProgramTypeTranslations",
        "languageTranslations"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(required = true)
        protected String translationsVersion;
        protected List<CountryTranslationType> countryTranslations;
        protected List<DeliveryOptionTranslationType> deliveryOptionTranslations;
        protected List<CurrencyTranslationType> currencyTranslations;
        protected List<FQDOTextTranslationType> fqdoTextTranslations;
        protected List<IndustryTranslationType> industryTranslations;
        protected List<LoyaltyTranslationType> loyaltyTranslations;
        protected List<LoyaltyCardTypeTranslationType> loyaltyCardTypeTranslations;
        protected List<LoyaltyProgramTypeTranslationType> loyaltyProgramTypeTranslations;
        protected List<LanguageTranslationType> languageTranslations;

        /**
         * Gets the value of the translationsVersion property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTranslationsVersion() {
            return translationsVersion;
        }

        /**
         * Sets the value of the translationsVersion property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTranslationsVersion(String value) {
            this.translationsVersion = value;
        }

        /**
         * Gets the value of the countryTranslations property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the countryTranslations property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCountryTranslations().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link CountryTranslationType }
         * 
         * 
         */
        public List<CountryTranslationType> getCountryTranslations() {
            if (countryTranslations == null) {
                countryTranslations = new ArrayList<CountryTranslationType>();
            }
            return this.countryTranslations;
        }

        /**
         * Gets the value of the deliveryOptionTranslations property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the deliveryOptionTranslations property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDeliveryOptionTranslations().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DeliveryOptionTranslationType }
         * 
         * 
         */
        public List<DeliveryOptionTranslationType> getDeliveryOptionTranslations() {
            if (deliveryOptionTranslations == null) {
                deliveryOptionTranslations = new ArrayList<DeliveryOptionTranslationType>();
            }
            return this.deliveryOptionTranslations;
        }

        /**
         * Gets the value of the currencyTranslations property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the currencyTranslations property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCurrencyTranslations().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link CurrencyTranslationType }
         * 
         * 
         */
        public List<CurrencyTranslationType> getCurrencyTranslations() {
            if (currencyTranslations == null) {
                currencyTranslations = new ArrayList<CurrencyTranslationType>();
            }
            return this.currencyTranslations;
        }

        /**
         * Gets the value of the fqdoTextTranslations property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the fqdoTextTranslations property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFqdoTextTranslations().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link FQDOTextTranslationType }
         * 
         * 
         */
        public List<FQDOTextTranslationType> getFqdoTextTranslations() {
            if (fqdoTextTranslations == null) {
                fqdoTextTranslations = new ArrayList<FQDOTextTranslationType>();
            }
            return this.fqdoTextTranslations;
        }

        /**
         * Gets the value of the industryTranslations property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the industryTranslations property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getIndustryTranslations().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link IndustryTranslationType }
         * 
         * 
         */
        public List<IndustryTranslationType> getIndustryTranslations() {
            if (industryTranslations == null) {
                industryTranslations = new ArrayList<IndustryTranslationType>();
            }
            return this.industryTranslations;
        }

        /**
         * Gets the value of the loyaltyTranslations property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the loyaltyTranslations property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getLoyaltyTranslations().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link LoyaltyTranslationType }
         * 
         * 
         */
        public List<LoyaltyTranslationType> getLoyaltyTranslations() {
            if (loyaltyTranslations == null) {
                loyaltyTranslations = new ArrayList<LoyaltyTranslationType>();
            }
            return this.loyaltyTranslations;
        }

        /**
         * Gets the value of the loyaltyCardTypeTranslations property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the loyaltyCardTypeTranslations property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getLoyaltyCardTypeTranslations().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link LoyaltyCardTypeTranslationType }
         * 
         * 
         */
        public List<LoyaltyCardTypeTranslationType> getLoyaltyCardTypeTranslations() {
            if (loyaltyCardTypeTranslations == null) {
                loyaltyCardTypeTranslations = new ArrayList<LoyaltyCardTypeTranslationType>();
            }
            return this.loyaltyCardTypeTranslations;
        }

        /**
         * Gets the value of the loyaltyProgramTypeTranslations property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the loyaltyProgramTypeTranslations property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getLoyaltyProgramTypeTranslations().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link LoyaltyProgramTypeTranslationType }
         * 
         * 
         */
        public List<LoyaltyProgramTypeTranslationType> getLoyaltyProgramTypeTranslations() {
            if (loyaltyProgramTypeTranslations == null) {
                loyaltyProgramTypeTranslations = new ArrayList<LoyaltyProgramTypeTranslationType>();
            }
            return this.loyaltyProgramTypeTranslations;
        }

        /**
         * Gets the value of the languageTranslations property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the languageTranslations property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getLanguageTranslations().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link LanguageTranslationType }
         * 
         * 
         */
        public List<LanguageTranslationType> getLanguageTranslations() {
            if (languageTranslations == null) {
                languageTranslations = new ArrayList<LanguageTranslationType>();
            }
            return this.languageTranslations;
        }

    }

}
