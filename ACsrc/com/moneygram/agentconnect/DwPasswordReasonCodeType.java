
package com.moneygram.agentconnect;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DwPasswordReasonCodeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DwPasswordReasonCodeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PASSED"/>
 *     &lt;enumeration value="EMP_NOT_EXIST"/>
 *     &lt;enumeration value="IN_HIST"/>
 *     &lt;enumeration value="FORMAT_ERROR"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DwPasswordReasonCodeType")
@XmlEnum
public enum DwPasswordReasonCodeType {

    PASSED,
    EMP_NOT_EXIST,
    IN_HIST,
    FORMAT_ERROR;

    public String value() {
        return name();
    }

    public static DwPasswordReasonCodeType fromValue(String v) {
        return valueOf(v);
    }

}
