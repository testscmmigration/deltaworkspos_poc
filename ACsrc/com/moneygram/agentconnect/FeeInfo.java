
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FeeInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FeeInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="validReceiveAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType"/>
 *         &lt;element name="validReceiveCurrency" type="{http://www.moneygram.com/AgentConnect1705}CurrencyCodeType"/>
 *         &lt;element name="validExchangeRate" type="{http://www.moneygram.com/AgentConnect1705}FXRateType"/>
 *         &lt;element name="estimatedReceiveAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType" minOccurs="0"/>
 *         &lt;element name="estimatedReceiveCurrency" type="{http://www.moneygram.com/AgentConnect1705}CurrencyCodeType" minOccurs="0"/>
 *         &lt;element name="estimatedExchangeRate" type="{http://www.moneygram.com/AgentConnect1705}FXRateType" minOccurs="0"/>
 *         &lt;element name="displayExchangeRate" type="{http://www.moneygram.com/AgentConnect1705}FXRateType" minOccurs="0"/>
 *         &lt;element name="totalAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType"/>
 *         &lt;element name="destinationCountry" type="{http://www.moneygram.com/AgentConnect1705}CountryCodeType"/>
 *         &lt;element name="serviceOption" type="{http://www.moneygram.com/AgentConnect1705}ServiceOptionType" minOccurs="0"/>
 *         &lt;element name="receiveAmountAltered" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="saLimitAvailable" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType" minOccurs="0"/>
 *         &lt;element name="revisedInformationalFee" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="serviceOptionCategoryDisplayName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serviceOptionCategoryId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serviceOptionDisplayName" type="{http://www.moneygram.com/AgentConnect1705}StringMax50Type" minOccurs="0"/>
 *         &lt;element name="serviceOptionDisplayDescription" type="{http://www.moneygram.com/AgentConnect1705}ServiceOptionDisplayDescriptionType" minOccurs="0"/>
 *         &lt;element name="displayOrder" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="regAuthText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="receiveAgentID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="receiveAgentName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="receiveAgentAbbreviation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mgManaged" type="{http://www.moneygram.com/AgentConnect1705}StringMax20Type" minOccurs="0"/>
 *         &lt;element name="disclosureText" type="{http://www.moneygram.com/AgentConnect1705}StringMax2000Type" minOccurs="0"/>
 *         &lt;element name="mgiSessionID" type="{http://www.moneygram.com/AgentConnect1705}MGISessionIDType"/>
 *         &lt;element name="sendAmountAltered" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="additionalInfoText" type="{http://www.moneygram.com/AgentConnect1705}StringMax400Type" minOccurs="0"/>
 *         &lt;element name="promotionInfos" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="promotionInfo" type="{http://www.moneygram.com/AgentConnect1705}PromotionInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="sendAmounts" type="{http://www.moneygram.com/AgentConnect1705}SendAmountInfo" minOccurs="0"/>
 *         &lt;element name="receiveAmounts" type="{http://www.moneygram.com/AgentConnect1705}EstimatedReceiveAmountInfo" minOccurs="0"/>
 *         &lt;element name="errorInfo" type="{http://www.moneygram.com/AgentConnect1705}ErrorInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FeeInfo", propOrder = {
    "validReceiveAmount",
    "validReceiveCurrency",
    "validExchangeRate",
    "estimatedReceiveAmount",
    "estimatedReceiveCurrency",
    "estimatedExchangeRate",
    "displayExchangeRate",
    "totalAmount",
    "destinationCountry",
    "serviceOption",
    "receiveAmountAltered",
    "saLimitAvailable",
    "revisedInformationalFee",
    "serviceOptionCategoryDisplayName",
    "serviceOptionCategoryId",
    "serviceOptionDisplayName",
    "serviceOptionDisplayDescription",
    "displayOrder",
    "regAuthText",
    "receiveAgentID",
    "receiveAgentName",
    "receiveAgentAbbreviation",
    "mgManaged",
    "disclosureText",
    "mgiSessionID",
    "sendAmountAltered",
    "additionalInfoText",
    "promotionInfos",
    "sendAmounts",
    "receiveAmounts",
    "errorInfo"
})
public class FeeInfo
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected BigDecimal validReceiveAmount;
    @XmlElement(required = true)
    protected String validReceiveCurrency;
    @XmlElement(required = true)
    protected BigDecimal validExchangeRate;
    protected BigDecimal estimatedReceiveAmount;
    protected String estimatedReceiveCurrency;
    protected BigDecimal estimatedExchangeRate;
    protected BigDecimal displayExchangeRate;
    @XmlElement(required = true)
    protected BigDecimal totalAmount;
    @XmlElement(required = true)
    protected String destinationCountry;
    protected String serviceOption;
    protected boolean receiveAmountAltered;
    protected BigDecimal saLimitAvailable;
    protected boolean revisedInformationalFee;
    protected String serviceOptionCategoryDisplayName;
    protected String serviceOptionCategoryId;
    protected String serviceOptionDisplayName;
    protected String serviceOptionDisplayDescription;
    protected Integer displayOrder;
    protected String regAuthText;
    protected String receiveAgentID;
    protected String receiveAgentName;
    protected String receiveAgentAbbreviation;
    protected String mgManaged;
    protected String disclosureText;
    @XmlElement(required = true)
    protected String mgiSessionID;
    protected boolean sendAmountAltered;
    protected String additionalInfoText;
    protected FeeInfo.PromotionInfos promotionInfos;
    protected SendAmountInfo sendAmounts;
    protected EstimatedReceiveAmountInfo receiveAmounts;
    protected ErrorInfo errorInfo;

    /**
     * Gets the value of the validReceiveAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValidReceiveAmount() {
        return validReceiveAmount;
    }

    /**
     * Sets the value of the validReceiveAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValidReceiveAmount(BigDecimal value) {
        this.validReceiveAmount = value;
    }

    /**
     * Gets the value of the validReceiveCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValidReceiveCurrency() {
        return validReceiveCurrency;
    }

    /**
     * Sets the value of the validReceiveCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValidReceiveCurrency(String value) {
        this.validReceiveCurrency = value;
    }

    /**
     * Gets the value of the validExchangeRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValidExchangeRate() {
        return validExchangeRate;
    }

    /**
     * Sets the value of the validExchangeRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValidExchangeRate(BigDecimal value) {
        this.validExchangeRate = value;
    }

    /**
     * Gets the value of the estimatedReceiveAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEstimatedReceiveAmount() {
        return estimatedReceiveAmount;
    }

    /**
     * Sets the value of the estimatedReceiveAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEstimatedReceiveAmount(BigDecimal value) {
        this.estimatedReceiveAmount = value;
    }

    /**
     * Gets the value of the estimatedReceiveCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstimatedReceiveCurrency() {
        return estimatedReceiveCurrency;
    }

    /**
     * Sets the value of the estimatedReceiveCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstimatedReceiveCurrency(String value) {
        this.estimatedReceiveCurrency = value;
    }

    /**
     * Gets the value of the estimatedExchangeRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEstimatedExchangeRate() {
        return estimatedExchangeRate;
    }

    /**
     * Sets the value of the estimatedExchangeRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEstimatedExchangeRate(BigDecimal value) {
        this.estimatedExchangeRate = value;
    }

    /**
     * Gets the value of the displayExchangeRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDisplayExchangeRate() {
        return displayExchangeRate;
    }

    /**
     * Sets the value of the displayExchangeRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDisplayExchangeRate(BigDecimal value) {
        this.displayExchangeRate = value;
    }

    /**
     * Gets the value of the totalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * Sets the value of the totalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalAmount(BigDecimal value) {
        this.totalAmount = value;
    }

    /**
     * Gets the value of the destinationCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationCountry() {
        return destinationCountry;
    }

    /**
     * Sets the value of the destinationCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationCountry(String value) {
        this.destinationCountry = value;
    }

    /**
     * Gets the value of the serviceOption property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceOption() {
        return serviceOption;
    }

    /**
     * Sets the value of the serviceOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceOption(String value) {
        this.serviceOption = value;
    }

    /**
     * Gets the value of the receiveAmountAltered property.
     * 
     */
    public boolean isReceiveAmountAltered() {
        return receiveAmountAltered;
    }

    /**
     * Sets the value of the receiveAmountAltered property.
     * 
     */
    public void setReceiveAmountAltered(boolean value) {
        this.receiveAmountAltered = value;
    }

    /**
     * Gets the value of the saLimitAvailable property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSaLimitAvailable() {
        return saLimitAvailable;
    }

    /**
     * Sets the value of the saLimitAvailable property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSaLimitAvailable(BigDecimal value) {
        this.saLimitAvailable = value;
    }

    /**
     * Gets the value of the revisedInformationalFee property.
     * 
     */
    public boolean isRevisedInformationalFee() {
        return revisedInformationalFee;
    }

    /**
     * Sets the value of the revisedInformationalFee property.
     * 
     */
    public void setRevisedInformationalFee(boolean value) {
        this.revisedInformationalFee = value;
    }

    /**
     * Gets the value of the serviceOptionCategoryDisplayName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceOptionCategoryDisplayName() {
        return serviceOptionCategoryDisplayName;
    }

    /**
     * Sets the value of the serviceOptionCategoryDisplayName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceOptionCategoryDisplayName(String value) {
        this.serviceOptionCategoryDisplayName = value;
    }

    /**
     * Gets the value of the serviceOptionCategoryId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceOptionCategoryId() {
        return serviceOptionCategoryId;
    }

    /**
     * Sets the value of the serviceOptionCategoryId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceOptionCategoryId(String value) {
        this.serviceOptionCategoryId = value;
    }

    /**
     * Gets the value of the serviceOptionDisplayName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceOptionDisplayName() {
        return serviceOptionDisplayName;
    }

    /**
     * Sets the value of the serviceOptionDisplayName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceOptionDisplayName(String value) {
        this.serviceOptionDisplayName = value;
    }

    /**
     * Gets the value of the serviceOptionDisplayDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceOptionDisplayDescription() {
        return serviceOptionDisplayDescription;
    }

    /**
     * Sets the value of the serviceOptionDisplayDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceOptionDisplayDescription(String value) {
        this.serviceOptionDisplayDescription = value;
    }

    /**
     * Gets the value of the displayOrder property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDisplayOrder() {
        return displayOrder;
    }

    /**
     * Sets the value of the displayOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDisplayOrder(Integer value) {
        this.displayOrder = value;
    }

    /**
     * Gets the value of the regAuthText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegAuthText() {
        return regAuthText;
    }

    /**
     * Sets the value of the regAuthText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegAuthText(String value) {
        this.regAuthText = value;
    }

    /**
     * Gets the value of the receiveAgentID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiveAgentID() {
        return receiveAgentID;
    }

    /**
     * Sets the value of the receiveAgentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiveAgentID(String value) {
        this.receiveAgentID = value;
    }

    /**
     * Gets the value of the receiveAgentName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiveAgentName() {
        return receiveAgentName;
    }

    /**
     * Sets the value of the receiveAgentName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiveAgentName(String value) {
        this.receiveAgentName = value;
    }

    /**
     * Gets the value of the receiveAgentAbbreviation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiveAgentAbbreviation() {
        return receiveAgentAbbreviation;
    }

    /**
     * Sets the value of the receiveAgentAbbreviation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiveAgentAbbreviation(String value) {
        this.receiveAgentAbbreviation = value;
    }

    /**
     * Gets the value of the mgManaged property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMgManaged() {
        return mgManaged;
    }

    /**
     * Sets the value of the mgManaged property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMgManaged(String value) {
        this.mgManaged = value;
    }

    /**
     * Gets the value of the disclosureText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisclosureText() {
        return disclosureText;
    }

    /**
     * Sets the value of the disclosureText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisclosureText(String value) {
        this.disclosureText = value;
    }

    /**
     * Gets the value of the mgiSessionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMgiSessionID() {
        return mgiSessionID;
    }

    /**
     * Sets the value of the mgiSessionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMgiSessionID(String value) {
        this.mgiSessionID = value;
    }

    /**
     * Gets the value of the sendAmountAltered property.
     * 
     */
    public boolean isSendAmountAltered() {
        return sendAmountAltered;
    }

    /**
     * Sets the value of the sendAmountAltered property.
     * 
     */
    public void setSendAmountAltered(boolean value) {
        this.sendAmountAltered = value;
    }

    /**
     * Gets the value of the additionalInfoText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdditionalInfoText() {
        return additionalInfoText;
    }

    /**
     * Sets the value of the additionalInfoText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdditionalInfoText(String value) {
        this.additionalInfoText = value;
    }

    /**
     * Gets the value of the promotionInfos property.
     * 
     * @return
     *     possible object is
     *     {@link FeeInfo.PromotionInfos }
     *     
     */
    public FeeInfo.PromotionInfos getPromotionInfos() {
        return promotionInfos;
    }

    /**
     * Sets the value of the promotionInfos property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeeInfo.PromotionInfos }
     *     
     */
    public void setPromotionInfos(FeeInfo.PromotionInfos value) {
        this.promotionInfos = value;
    }

    /**
     * Gets the value of the sendAmounts property.
     * 
     * @return
     *     possible object is
     *     {@link SendAmountInfo }
     *     
     */
    public SendAmountInfo getSendAmounts() {
        return sendAmounts;
    }

    /**
     * Sets the value of the sendAmounts property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendAmountInfo }
     *     
     */
    public void setSendAmounts(SendAmountInfo value) {
        this.sendAmounts = value;
    }

    /**
     * Gets the value of the receiveAmounts property.
     * 
     * @return
     *     possible object is
     *     {@link EstimatedReceiveAmountInfo }
     *     
     */
    public EstimatedReceiveAmountInfo getReceiveAmounts() {
        return receiveAmounts;
    }

    /**
     * Sets the value of the receiveAmounts property.
     * 
     * @param value
     *     allowed object is
     *     {@link EstimatedReceiveAmountInfo }
     *     
     */
    public void setReceiveAmounts(EstimatedReceiveAmountInfo value) {
        this.receiveAmounts = value;
    }

    /**
     * Gets the value of the errorInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ErrorInfo }
     *     
     */
    public ErrorInfo getErrorInfo() {
        return errorInfo;
    }

    /**
     * Sets the value of the errorInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorInfo }
     *     
     */
    public void setErrorInfo(ErrorInfo value) {
        this.errorInfo = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="promotionInfo" type="{http://www.moneygram.com/AgentConnect1705}PromotionInfo" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "promotionInfo"
    })
    public static class PromotionInfos
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<PromotionInfo> promotionInfo;

        /**
         * Gets the value of the promotionInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the promotionInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPromotionInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link PromotionInfo }
         * 
         * 
         */
        public List<PromotionInfo> getPromotionInfo() {
            if (promotionInfo == null) {
                promotionInfo = new ArrayList<PromotionInfo>();
            }
            return this.promotionInfo;
        }

    }

}
