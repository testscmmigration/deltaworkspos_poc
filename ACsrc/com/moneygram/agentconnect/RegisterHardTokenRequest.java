
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RegisterHardTokenRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RegisterHardTokenRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="ldapUserId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="hardToken" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegisterHardTokenRequest", propOrder = {
    "ldapUserId",
    "hardToken"
})
public class RegisterHardTokenRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected String ldapUserId;
    @XmlElement(required = true)
    protected String hardToken;

    /**
     * Gets the value of the ldapUserId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLdapUserId() {
        return ldapUserId;
    }

    /**
     * Sets the value of the ldapUserId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLdapUserId(String value) {
        this.ldapUserId = value;
    }

    /**
     * Gets the value of the hardToken property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHardToken() {
        return hardToken;
    }

    /**
     * Sets the value of the hardToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHardToken(String value) {
        this.hardToken = value;
    }

}
