
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for MoneyOrderInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MoneyOrderInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="printStatus" type="{http://www.moneygram.com/AgentConnect1705}MoneyOrderPrintStatusType"/>
 *         &lt;element name="moAccountNumber" type="{http://www.moneygram.com/AgentConnect1705}String14Type" minOccurs="0"/>
 *         &lt;element name="deviceID" type="{http://www.moneygram.com/AgentConnect1705}String8Type" minOccurs="0"/>
 *         &lt;element name="serialNumber" type="{http://www.moneygram.com/AgentConnect1705}Int11Type" minOccurs="0"/>
 *         &lt;element name="itemAmount" type="{http://www.moneygram.com/AgentConnect1705}Decimal8Type" minOccurs="0"/>
 *         &lt;element name="dateTimePrinted" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="itemFee" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType" minOccurs="0"/>
 *         &lt;element name="periodNumber" type="{http://www.moneygram.com/AgentConnect1705}Int1Type" minOccurs="0"/>
 *         &lt;element name="documentSequenceNbr" type="{http://www.moneygram.com/AgentConnect1705}Int1Type" minOccurs="0"/>
 *         &lt;element name="documentType" type="{http://www.moneygram.com/AgentConnect1705}Int1Type" minOccurs="0"/>
 *         &lt;element name="dispenserID" type="{http://www.moneygram.com/AgentConnect1705}StringMax10Type" minOccurs="0"/>
 *         &lt;element name="voidFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="voidReasonCode" type="{http://www.moneygram.com/AgentConnect1705}MoneyOrderVoidReasonCodeType" minOccurs="0"/>
 *         &lt;element name="taxID" type="{http://www.moneygram.com/AgentConnect1705}StringMax20Type" minOccurs="0"/>
 *         &lt;element name="employeeID" type="{http://www.moneygram.com/AgentConnect1705}Int5Type" minOccurs="0"/>
 *         &lt;element name="vendorNumber" type="{http://www.moneygram.com/AgentConnect1705}Int2Type" minOccurs="0"/>
 *         &lt;element name="remoteIssuanceFlag" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="saleIssuanceTag" type="{http://www.moneygram.com/AgentConnect1705}Int1Type" minOccurs="0"/>
 *         &lt;element name="discountPercentage" type="{http://www.moneygram.com/AgentConnect1705}Decimal3Type" minOccurs="0"/>
 *         &lt;element name="discountAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType" minOccurs="0"/>
 *         &lt;element name="accountingStartDay" type="{http://www.moneygram.com/AgentConnect1705}Int1Type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MoneyOrderInfo", propOrder = {
    "printStatus",
    "moAccountNumber",
    "deviceID",
    "serialNumber",
    "itemAmount",
    "dateTimePrinted",
    "itemFee",
    "periodNumber",
    "documentSequenceNbr",
    "documentType",
    "dispenserID",
    "voidFlag",
    "voidReasonCode",
    "taxID",
    "employeeID",
    "vendorNumber",
    "remoteIssuanceFlag",
    "saleIssuanceTag",
    "discountPercentage",
    "discountAmount",
    "accountingStartDay"
})
public class MoneyOrderInfo
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected MoneyOrderPrintStatusType printStatus;
    protected String moAccountNumber;
    protected String deviceID;
    protected BigInteger serialNumber;
    protected BigDecimal itemAmount;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateTimePrinted;
    protected BigDecimal itemFee;
    protected BigInteger periodNumber;
    protected BigInteger documentSequenceNbr;
    protected BigInteger documentType;
    protected String dispenserID;
    protected Boolean voidFlag;
    protected MoneyOrderVoidReasonCodeType voidReasonCode;
    protected String taxID;
    protected BigInteger employeeID;
    protected BigInteger vendorNumber;
    protected Boolean remoteIssuanceFlag;
    protected BigInteger saleIssuanceTag;
    protected BigDecimal discountPercentage;
    protected BigDecimal discountAmount;
    protected BigInteger accountingStartDay;

    /**
     * Gets the value of the printStatus property.
     * 
     * @return
     *     possible object is
     *     {@link MoneyOrderPrintStatusType }
     *     
     */
    public MoneyOrderPrintStatusType getPrintStatus() {
        return printStatus;
    }

    /**
     * Sets the value of the printStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneyOrderPrintStatusType }
     *     
     */
    public void setPrintStatus(MoneyOrderPrintStatusType value) {
        this.printStatus = value;
    }

    /**
     * Gets the value of the moAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMoAccountNumber() {
        return moAccountNumber;
    }

    /**
     * Sets the value of the moAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMoAccountNumber(String value) {
        this.moAccountNumber = value;
    }

    /**
     * Gets the value of the deviceID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceID() {
        return deviceID;
    }

    /**
     * Sets the value of the deviceID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceID(String value) {
        this.deviceID = value;
    }

    /**
     * Gets the value of the serialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSerialNumber() {
        return serialNumber;
    }

    /**
     * Sets the value of the serialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSerialNumber(BigInteger value) {
        this.serialNumber = value;
    }

    /**
     * Gets the value of the itemAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getItemAmount() {
        return itemAmount;
    }

    /**
     * Sets the value of the itemAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setItemAmount(BigDecimal value) {
        this.itemAmount = value;
    }

    /**
     * Gets the value of the dateTimePrinted property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateTimePrinted() {
        return dateTimePrinted;
    }

    /**
     * Sets the value of the dateTimePrinted property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateTimePrinted(XMLGregorianCalendar value) {
        this.dateTimePrinted = value;
    }

    /**
     * Gets the value of the itemFee property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getItemFee() {
        return itemFee;
    }

    /**
     * Sets the value of the itemFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setItemFee(BigDecimal value) {
        this.itemFee = value;
    }

    /**
     * Gets the value of the periodNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPeriodNumber() {
        return periodNumber;
    }

    /**
     * Sets the value of the periodNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPeriodNumber(BigInteger value) {
        this.periodNumber = value;
    }

    /**
     * Gets the value of the documentSequenceNbr property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDocumentSequenceNbr() {
        return documentSequenceNbr;
    }

    /**
     * Sets the value of the documentSequenceNbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDocumentSequenceNbr(BigInteger value) {
        this.documentSequenceNbr = value;
    }

    /**
     * Gets the value of the documentType property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDocumentType() {
        return documentType;
    }

    /**
     * Sets the value of the documentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDocumentType(BigInteger value) {
        this.documentType = value;
    }

    /**
     * Gets the value of the dispenserID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDispenserID() {
        return dispenserID;
    }

    /**
     * Sets the value of the dispenserID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDispenserID(String value) {
        this.dispenserID = value;
    }

    /**
     * Gets the value of the voidFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVoidFlag() {
        return voidFlag;
    }

    /**
     * Sets the value of the voidFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVoidFlag(Boolean value) {
        this.voidFlag = value;
    }

    /**
     * Gets the value of the voidReasonCode property.
     * 
     * @return
     *     possible object is
     *     {@link MoneyOrderVoidReasonCodeType }
     *     
     */
    public MoneyOrderVoidReasonCodeType getVoidReasonCode() {
        return voidReasonCode;
    }

    /**
     * Sets the value of the voidReasonCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoneyOrderVoidReasonCodeType }
     *     
     */
    public void setVoidReasonCode(MoneyOrderVoidReasonCodeType value) {
        this.voidReasonCode = value;
    }

    /**
     * Gets the value of the taxID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxID() {
        return taxID;
    }

    /**
     * Sets the value of the taxID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxID(String value) {
        this.taxID = value;
    }

    /**
     * Gets the value of the employeeID property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getEmployeeID() {
        return employeeID;
    }

    /**
     * Sets the value of the employeeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setEmployeeID(BigInteger value) {
        this.employeeID = value;
    }

    /**
     * Gets the value of the vendorNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getVendorNumber() {
        return vendorNumber;
    }

    /**
     * Sets the value of the vendorNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setVendorNumber(BigInteger value) {
        this.vendorNumber = value;
    }

    /**
     * Gets the value of the remoteIssuanceFlag property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRemoteIssuanceFlag() {
        return remoteIssuanceFlag;
    }

    /**
     * Sets the value of the remoteIssuanceFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRemoteIssuanceFlag(Boolean value) {
        this.remoteIssuanceFlag = value;
    }

    /**
     * Gets the value of the saleIssuanceTag property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSaleIssuanceTag() {
        return saleIssuanceTag;
    }

    /**
     * Sets the value of the saleIssuanceTag property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSaleIssuanceTag(BigInteger value) {
        this.saleIssuanceTag = value;
    }

    /**
     * Gets the value of the discountPercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscountPercentage() {
        return discountPercentage;
    }

    /**
     * Sets the value of the discountPercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscountPercentage(BigDecimal value) {
        this.discountPercentage = value;
    }

    /**
     * Gets the value of the discountAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    /**
     * Sets the value of the discountAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDiscountAmount(BigDecimal value) {
        this.discountAmount = value;
    }

    /**
     * Gets the value of the accountingStartDay property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAccountingStartDay() {
        return accountingStartDay;
    }

    /**
     * Sets the value of the accountingStartDay property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAccountingStartDay(BigInteger value) {
        this.accountingStartDay = value;
    }

}
