
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.activation.DataHandler;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetReceiptForReprintResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetReceiptForReprintResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="receiptMimeType" type="{http://www.moneygram.com/AgentConnect1705}MimeType" minOccurs="0"/>
 *                   &lt;element name="agentReceiptMimeData" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *                   &lt;element name="consumerReceipt1MimeData" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *                   &lt;element name="consumerReceipt2MimeData" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetReceiptForReprintResponse", propOrder = {
    "payload"
})
public class GetReceiptForReprintResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<GetReceiptForReprintResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetReceiptForReprintResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<GetReceiptForReprintResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetReceiptForReprintResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<GetReceiptForReprintResponse.Payload> value) {
        this.payload = ((JAXBElement<GetReceiptForReprintResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="receiptMimeType" type="{http://www.moneygram.com/AgentConnect1705}MimeType" minOccurs="0"/>
     *         &lt;element name="agentReceiptMimeData" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
     *         &lt;element name="consumerReceipt1MimeData" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
     *         &lt;element name="consumerReceipt2MimeData" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "receiptMimeType",
        "agentReceiptMimeData",
        "consumerReceipt1MimeData",
        "consumerReceipt2MimeData"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected String receiptMimeType;
        @XmlMimeType("*/*")
        protected DataHandler agentReceiptMimeData;
        @XmlMimeType("*/*")
        protected DataHandler consumerReceipt1MimeData;
        @XmlMimeType("*/*")
        protected DataHandler consumerReceipt2MimeData;

        /**
         * Gets the value of the receiptMimeType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReceiptMimeType() {
            return receiptMimeType;
        }

        /**
         * Sets the value of the receiptMimeType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReceiptMimeType(String value) {
            this.receiptMimeType = value;
        }

        /**
         * Gets the value of the agentReceiptMimeData property.
         * 
         * @return
         *     possible object is
         *     {@link DataHandler }
         *     
         */
        public DataHandler getAgentReceiptMimeData() {
            return agentReceiptMimeData;
        }

        /**
         * Sets the value of the agentReceiptMimeData property.
         * 
         * @param value
         *     allowed object is
         *     {@link DataHandler }
         *     
         */
        public void setAgentReceiptMimeData(DataHandler value) {
            this.agentReceiptMimeData = value;
        }

        /**
         * Gets the value of the consumerReceipt1MimeData property.
         * 
         * @return
         *     possible object is
         *     {@link DataHandler }
         *     
         */
        public DataHandler getConsumerReceipt1MimeData() {
            return consumerReceipt1MimeData;
        }

        /**
         * Sets the value of the consumerReceipt1MimeData property.
         * 
         * @param value
         *     allowed object is
         *     {@link DataHandler }
         *     
         */
        public void setConsumerReceipt1MimeData(DataHandler value) {
            this.consumerReceipt1MimeData = value;
        }

        /**
         * Gets the value of the consumerReceipt2MimeData property.
         * 
         * @return
         *     possible object is
         *     {@link DataHandler }
         *     
         */
        public DataHandler getConsumerReceipt2MimeData() {
            return consumerReceipt2MimeData;
        }

        /**
         * Sets the value of the consumerReceipt2MimeData property.
         * 
         * @param value
         *     allowed object is
         *     {@link DataHandler }
         *     
         */
        public void setConsumerReceipt2MimeData(DataHandler value) {
            this.consumerReceipt2MimeData = value;
        }

    }

}
