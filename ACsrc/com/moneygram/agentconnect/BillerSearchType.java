
package com.moneygram.agentconnect;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BillerSearchType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BillerSearchType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="IND"/>
 *     &lt;enumeration value="CODE"/>
 *     &lt;enumeration value="NAME"/>
 *     &lt;enumeration value="ID"/>
 *     &lt;enumeration value="BIN"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BillerSearchType")
@XmlEnum
public enum BillerSearchType {


    /**
     * search by industry (and agent's DMA)
     * 
     */
    IND,

    /**
     * search by receive code / keyword
     * 
     */
    CODE,

    /**
     * search by biller name and industry
     * 						(optional)
     * 
     */
    NAME,

    /**
     * Search by billerID and productVariant
     * 						(optional)
     * 
     */
    ID,

    /**
     * Search by Bin Number
     * 
     */
    BIN;

    public String value() {
        return name();
    }

    public static BillerSearchType fromValue(String v) {
        return valueOf(v);
    }

}
