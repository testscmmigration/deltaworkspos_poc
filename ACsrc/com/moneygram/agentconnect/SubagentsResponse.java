
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubagentsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubagentsResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="subagents" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="subagent" type="{http://www.moneygram.com/AgentConnect1705}SubagentInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubagentsResponse", propOrder = {
    "payload"
})
public class SubagentsResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<SubagentsResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SubagentsResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<SubagentsResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SubagentsResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<SubagentsResponse.Payload> value) {
        this.payload = ((JAXBElement<SubagentsResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="subagents" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="subagent" type="{http://www.moneygram.com/AgentConnect1705}SubagentInfo" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subagents"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected SubagentsResponse.Payload.Subagents subagents;

        /**
         * Gets the value of the subagents property.
         * 
         * @return
         *     possible object is
         *     {@link SubagentsResponse.Payload.Subagents }
         *     
         */
        public SubagentsResponse.Payload.Subagents getSubagents() {
            return subagents;
        }

        /**
         * Sets the value of the subagents property.
         * 
         * @param value
         *     allowed object is
         *     {@link SubagentsResponse.Payload.Subagents }
         *     
         */
        public void setSubagents(SubagentsResponse.Payload.Subagents value) {
            this.subagents = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="subagent" type="{http://www.moneygram.com/AgentConnect1705}SubagentInfo" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "subagent"
        })
        public static class Subagents
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected List<SubagentInfo> subagent;

            /**
             * Gets the value of the subagent property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the subagent property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getSubagent().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link SubagentInfo }
             * 
             * 
             */
            public List<SubagentInfo> getSubagent() {
                if (subagent == null) {
                    subagent = new ArrayList<SubagentInfo>();
                }
                return this.subagent;
            }

        }

    }

}
