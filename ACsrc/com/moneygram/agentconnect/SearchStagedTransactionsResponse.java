
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SearchStagedTransactionsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchStagedTransactionsResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="stagedTransactionInfos" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="stagedTransactionInfo" type="{http://www.moneygram.com/AgentConnect1705}StagedTransactionInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchStagedTransactionsResponse", propOrder = {
    "payload"
})
public class SearchStagedTransactionsResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<SearchStagedTransactionsResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SearchStagedTransactionsResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<SearchStagedTransactionsResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SearchStagedTransactionsResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<SearchStagedTransactionsResponse.Payload> value) {
        this.payload = ((JAXBElement<SearchStagedTransactionsResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="stagedTransactionInfos" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="stagedTransactionInfo" type="{http://www.moneygram.com/AgentConnect1705}StagedTransactionInfo" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "stagedTransactionInfos"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected SearchStagedTransactionsResponse.Payload.StagedTransactionInfos stagedTransactionInfos;

        /**
         * Gets the value of the stagedTransactionInfos property.
         * 
         * @return
         *     possible object is
         *     {@link SearchStagedTransactionsResponse.Payload.StagedTransactionInfos }
         *     
         */
        public SearchStagedTransactionsResponse.Payload.StagedTransactionInfos getStagedTransactionInfos() {
            return stagedTransactionInfos;
        }

        /**
         * Sets the value of the stagedTransactionInfos property.
         * 
         * @param value
         *     allowed object is
         *     {@link SearchStagedTransactionsResponse.Payload.StagedTransactionInfos }
         *     
         */
        public void setStagedTransactionInfos(SearchStagedTransactionsResponse.Payload.StagedTransactionInfos value) {
            this.stagedTransactionInfos = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="stagedTransactionInfo" type="{http://www.moneygram.com/AgentConnect1705}StagedTransactionInfo" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "stagedTransactionInfo"
        })
        public static class StagedTransactionInfos
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected List<StagedTransactionInfo> stagedTransactionInfo;

            /**
             * Gets the value of the stagedTransactionInfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the stagedTransactionInfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getStagedTransactionInfo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link StagedTransactionInfo }
             * 
             * 
             */
            public List<StagedTransactionInfo> getStagedTransactionInfo() {
                if (stagedTransactionInfo == null) {
                    stagedTransactionInfo = new ArrayList<StagedTransactionInfo>();
                }
                return this.stagedTransactionInfo;
            }

        }

    }

}
