
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CountryInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CountryInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="countryCode" type="{http://www.moneygram.com/AgentConnect1705}CountryCodeType"/>
 *         &lt;element name="countryName" type="{http://www.moneygram.com/AgentConnect1705}CountryNameType"/>
 *         &lt;element name="baseCurrency" type="{http://www.moneygram.com/AgentConnect1705}CurrencyCodeType" minOccurs="0"/>
 *         &lt;element name="lookupKeys" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="lookupKey" type="{http://www.moneygram.com/AgentConnect1705}EnumType" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="receiveCurrencies" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="receiveCurrency" type="{http://www.moneygram.com/AgentConnect1705}CurrencyCodeType" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="sendCurrencies" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="sendCurrency" type="{http://www.moneygram.com/AgentConnect1705}CurrencyCodeType" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="countryDialCodes" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="countryDialCode" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CountryInfo", propOrder = {
    "countryCode",
    "countryName",
    "baseCurrency",
    "lookupKeys",
    "receiveCurrencies",
    "sendCurrencies",
    "countryDialCodes"
})
public class CountryInfo
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected String countryCode;
    @XmlElement(required = true)
    protected String countryName;
    protected String baseCurrency;
    protected CountryInfo.LookupKeys lookupKeys;
    protected CountryInfo.ReceiveCurrencies receiveCurrencies;
    protected CountryInfo.SendCurrencies sendCurrencies;
    protected CountryInfo.CountryDialCodes countryDialCodes;

    /**
     * Gets the value of the countryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Sets the value of the countryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

    /**
     * Gets the value of the countryName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryName() {
        return countryName;
    }

    /**
     * Sets the value of the countryName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryName(String value) {
        this.countryName = value;
    }

    /**
     * Gets the value of the baseCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBaseCurrency() {
        return baseCurrency;
    }

    /**
     * Sets the value of the baseCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBaseCurrency(String value) {
        this.baseCurrency = value;
    }

    /**
     * Gets the value of the lookupKeys property.
     * 
     * @return
     *     possible object is
     *     {@link CountryInfo.LookupKeys }
     *     
     */
    public CountryInfo.LookupKeys getLookupKeys() {
        return lookupKeys;
    }

    /**
     * Sets the value of the lookupKeys property.
     * 
     * @param value
     *     allowed object is
     *     {@link CountryInfo.LookupKeys }
     *     
     */
    public void setLookupKeys(CountryInfo.LookupKeys value) {
        this.lookupKeys = value;
    }

    /**
     * Gets the value of the receiveCurrencies property.
     * 
     * @return
     *     possible object is
     *     {@link CountryInfo.ReceiveCurrencies }
     *     
     */
    public CountryInfo.ReceiveCurrencies getReceiveCurrencies() {
        return receiveCurrencies;
    }

    /**
     * Sets the value of the receiveCurrencies property.
     * 
     * @param value
     *     allowed object is
     *     {@link CountryInfo.ReceiveCurrencies }
     *     
     */
    public void setReceiveCurrencies(CountryInfo.ReceiveCurrencies value) {
        this.receiveCurrencies = value;
    }

    /**
     * Gets the value of the sendCurrencies property.
     * 
     * @return
     *     possible object is
     *     {@link CountryInfo.SendCurrencies }
     *     
     */
    public CountryInfo.SendCurrencies getSendCurrencies() {
        return sendCurrencies;
    }

    /**
     * Sets the value of the sendCurrencies property.
     * 
     * @param value
     *     allowed object is
     *     {@link CountryInfo.SendCurrencies }
     *     
     */
    public void setSendCurrencies(CountryInfo.SendCurrencies value) {
        this.sendCurrencies = value;
    }

    /**
     * Gets the value of the countryDialCodes property.
     * 
     * @return
     *     possible object is
     *     {@link CountryInfo.CountryDialCodes }
     *     
     */
    public CountryInfo.CountryDialCodes getCountryDialCodes() {
        return countryDialCodes;
    }

    /**
     * Sets the value of the countryDialCodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link CountryInfo.CountryDialCodes }
     *     
     */
    public void setCountryDialCodes(CountryInfo.CountryDialCodes value) {
        this.countryDialCodes = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="countryDialCode" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "countryDialCode"
    })
    public static class CountryDialCodes
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(required = true)
        protected List<String> countryDialCode;

        /**
         * Gets the value of the countryDialCode property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the countryDialCode property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCountryDialCode().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getCountryDialCode() {
            if (countryDialCode == null) {
                countryDialCode = new ArrayList<String>();
            }
            return this.countryDialCode;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="lookupKey" type="{http://www.moneygram.com/AgentConnect1705}EnumType" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "lookupKey"
    })
    public static class LookupKeys
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(required = true)
        protected List<String> lookupKey;

        /**
         * Gets the value of the lookupKey property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the lookupKey property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getLookupKey().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getLookupKey() {
            if (lookupKey == null) {
                lookupKey = new ArrayList<String>();
            }
            return this.lookupKey;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="receiveCurrency" type="{http://www.moneygram.com/AgentConnect1705}CurrencyCodeType" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "receiveCurrency"
    })
    public static class ReceiveCurrencies
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(required = true)
        protected List<String> receiveCurrency;

        /**
         * Gets the value of the receiveCurrency property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the receiveCurrency property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getReceiveCurrency().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getReceiveCurrency() {
            if (receiveCurrency == null) {
                receiveCurrency = new ArrayList<String>();
            }
            return this.receiveCurrency;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="sendCurrency" type="{http://www.moneygram.com/AgentConnect1705}CurrencyCodeType" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "sendCurrency"
    })
    public static class SendCurrencies
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(required = true)
        protected List<String> sendCurrency;

        /**
         * Gets the value of the sendCurrency property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the sendCurrency property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSendCurrency().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getSendCurrency() {
            if (sendCurrency == null) {
                sendCurrency = new ArrayList<String>();
            }
            return this.sendCurrency;
        }

    }

}
