
package com.moneygram.agentconnect;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerComplianceTypeCodeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CustomerComplianceTypeCodeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="EMPTY_OR_NONE"/>
 *     &lt;enumeration value="OFAC_OR_AGGREGATION"/>
 *     &lt;enumeration value="MSL"/>
 *     &lt;enumeration value="RKL"/>
 *     &lt;enumeration value="CTR"/>
 *     &lt;enumeration value="SAR"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CustomerComplianceTypeCodeType")
@XmlEnum
public enum CustomerComplianceTypeCodeType {

    EMPTY_OR_NONE,
    OFAC_OR_AGGREGATION,
    MSL,
    RKL,
    CTR,
    SAR;

    public String value() {
        return name();
    }

    public static CustomerComplianceTypeCodeType fromValue(String v) {
        return valueOf(v);
    }

}
