
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EnumeratedTypeInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EnumeratedTypeInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.moneygram.com/AgentConnect1705}EnumerationNameType"/>
 *         &lt;element name="enumeratedItems" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="enumeratedItem" type="{http://www.moneygram.com/AgentConnect1705}EnumeratedIdentifierInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnumeratedTypeInfo", propOrder = {
    "name",
    "enumeratedItems"
})
public class EnumeratedTypeInfo
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected String name;
    protected EnumeratedTypeInfo.EnumeratedItems enumeratedItems;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the enumeratedItems property.
     * 
     * @return
     *     possible object is
     *     {@link EnumeratedTypeInfo.EnumeratedItems }
     *     
     */
    public EnumeratedTypeInfo.EnumeratedItems getEnumeratedItems() {
        return enumeratedItems;
    }

    /**
     * Sets the value of the enumeratedItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnumeratedTypeInfo.EnumeratedItems }
     *     
     */
    public void setEnumeratedItems(EnumeratedTypeInfo.EnumeratedItems value) {
        this.enumeratedItems = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="enumeratedItem" type="{http://www.moneygram.com/AgentConnect1705}EnumeratedIdentifierInfo" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "enumeratedItem"
    })
    public static class EnumeratedItems
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<EnumeratedIdentifierInfo> enumeratedItem;

        /**
         * Gets the value of the enumeratedItem property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the enumeratedItem property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEnumeratedItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link EnumeratedIdentifierInfo }
         * 
         * 
         */
        public List<EnumeratedIdentifierInfo> getEnumeratedItem() {
            if (enumeratedItem == null) {
                enumeratedItem = new ArrayList<EnumeratedIdentifierInfo>();
            }
            return this.enumeratedItem;
        }

    }

}
