
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CountryFeeInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CountryFeeInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="feeAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountNonZeroType" minOccurs="0"/>
 *         &lt;element name="sendAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountNonZeroType" minOccurs="0"/>
 *         &lt;element name="sendCurrency" type="{http://www.moneygram.com/AgentConnect1705}CurrencyCodeType" minOccurs="0"/>
 *         &lt;element name="validReceiveAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountNonZeroType" minOccurs="0"/>
 *         &lt;element name="validReceiveCurrency" type="{http://www.moneygram.com/AgentConnect1705}CurrencyCodeType" minOccurs="0"/>
 *         &lt;element name="validExchangeRate" type="{http://www.moneygram.com/AgentConnect1705}FXRateType" minOccurs="0"/>
 *         &lt;element name="validIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="estimatedReceiveAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType" minOccurs="0"/>
 *         &lt;element name="estimatedReceiveCurrency" type="{http://www.moneygram.com/AgentConnect1705}CurrencyCodeType" minOccurs="0"/>
 *         &lt;element name="estimatedExchangeRate" type="{http://www.moneygram.com/AgentConnect1705}FXRateType" minOccurs="0"/>
 *         &lt;element name="totalAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType" minOccurs="0"/>
 *         &lt;element name="receiveCountry" type="{http://www.moneygram.com/AgentConnect1705}CountryCodeType" minOccurs="0"/>
 *         &lt;element name="serviceOption" type="{http://www.moneygram.com/AgentConnect1705}ServiceOptionType" minOccurs="0"/>
 *         &lt;element name="receiveAmountAltered" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="amountExceededIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="maxAmountAllowed" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType" minOccurs="0"/>
 *         &lt;element name="receiveAgentID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="receiveAgentName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="receiveAgentAbbreviation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CountryFeeInfo", propOrder = {
    "feeAmount",
    "sendAmount",
    "sendCurrency",
    "validReceiveAmount",
    "validReceiveCurrency",
    "validExchangeRate",
    "validIndicator",
    "estimatedReceiveAmount",
    "estimatedReceiveCurrency",
    "estimatedExchangeRate",
    "totalAmount",
    "receiveCountry",
    "serviceOption",
    "receiveAmountAltered",
    "amountExceededIndicator",
    "maxAmountAllowed",
    "receiveAgentID",
    "receiveAgentName",
    "receiveAgentAbbreviation"
})
public class CountryFeeInfo
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected BigDecimal feeAmount;
    protected BigDecimal sendAmount;
    protected String sendCurrency;
    protected BigDecimal validReceiveAmount;
    protected String validReceiveCurrency;
    protected BigDecimal validExchangeRate;
    protected Boolean validIndicator;
    protected BigDecimal estimatedReceiveAmount;
    protected String estimatedReceiveCurrency;
    protected BigDecimal estimatedExchangeRate;
    protected BigDecimal totalAmount;
    protected String receiveCountry;
    protected String serviceOption;
    protected Boolean receiveAmountAltered;
    protected boolean amountExceededIndicator;
    protected BigDecimal maxAmountAllowed;
    protected String receiveAgentID;
    protected String receiveAgentName;
    protected String receiveAgentAbbreviation;

    /**
     * Gets the value of the feeAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFeeAmount() {
        return feeAmount;
    }

    /**
     * Sets the value of the feeAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFeeAmount(BigDecimal value) {
        this.feeAmount = value;
    }

    /**
     * Gets the value of the sendAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSendAmount() {
        return sendAmount;
    }

    /**
     * Sets the value of the sendAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSendAmount(BigDecimal value) {
        this.sendAmount = value;
    }

    /**
     * Gets the value of the sendCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendCurrency() {
        return sendCurrency;
    }

    /**
     * Sets the value of the sendCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendCurrency(String value) {
        this.sendCurrency = value;
    }

    /**
     * Gets the value of the validReceiveAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValidReceiveAmount() {
        return validReceiveAmount;
    }

    /**
     * Sets the value of the validReceiveAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValidReceiveAmount(BigDecimal value) {
        this.validReceiveAmount = value;
    }

    /**
     * Gets the value of the validReceiveCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValidReceiveCurrency() {
        return validReceiveCurrency;
    }

    /**
     * Sets the value of the validReceiveCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValidReceiveCurrency(String value) {
        this.validReceiveCurrency = value;
    }

    /**
     * Gets the value of the validExchangeRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValidExchangeRate() {
        return validExchangeRate;
    }

    /**
     * Sets the value of the validExchangeRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValidExchangeRate(BigDecimal value) {
        this.validExchangeRate = value;
    }

    /**
     * Gets the value of the validIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isValidIndicator() {
        return validIndicator;
    }

    /**
     * Sets the value of the validIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setValidIndicator(Boolean value) {
        this.validIndicator = value;
    }

    /**
     * Gets the value of the estimatedReceiveAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEstimatedReceiveAmount() {
        return estimatedReceiveAmount;
    }

    /**
     * Sets the value of the estimatedReceiveAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEstimatedReceiveAmount(BigDecimal value) {
        this.estimatedReceiveAmount = value;
    }

    /**
     * Gets the value of the estimatedReceiveCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstimatedReceiveCurrency() {
        return estimatedReceiveCurrency;
    }

    /**
     * Sets the value of the estimatedReceiveCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstimatedReceiveCurrency(String value) {
        this.estimatedReceiveCurrency = value;
    }

    /**
     * Gets the value of the estimatedExchangeRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEstimatedExchangeRate() {
        return estimatedExchangeRate;
    }

    /**
     * Sets the value of the estimatedExchangeRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEstimatedExchangeRate(BigDecimal value) {
        this.estimatedExchangeRate = value;
    }

    /**
     * Gets the value of the totalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * Sets the value of the totalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalAmount(BigDecimal value) {
        this.totalAmount = value;
    }

    /**
     * Gets the value of the receiveCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiveCountry() {
        return receiveCountry;
    }

    /**
     * Sets the value of the receiveCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiveCountry(String value) {
        this.receiveCountry = value;
    }

    /**
     * Gets the value of the serviceOption property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceOption() {
        return serviceOption;
    }

    /**
     * Sets the value of the serviceOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceOption(String value) {
        this.serviceOption = value;
    }

    /**
     * Gets the value of the receiveAmountAltered property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReceiveAmountAltered() {
        return receiveAmountAltered;
    }

    /**
     * Sets the value of the receiveAmountAltered property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReceiveAmountAltered(Boolean value) {
        this.receiveAmountAltered = value;
    }

    /**
     * Gets the value of the amountExceededIndicator property.
     * 
     */
    public boolean isAmountExceededIndicator() {
        return amountExceededIndicator;
    }

    /**
     * Sets the value of the amountExceededIndicator property.
     * 
     */
    public void setAmountExceededIndicator(boolean value) {
        this.amountExceededIndicator = value;
    }

    /**
     * Gets the value of the maxAmountAllowed property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaxAmountAllowed() {
        return maxAmountAllowed;
    }

    /**
     * Sets the value of the maxAmountAllowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaxAmountAllowed(BigDecimal value) {
        this.maxAmountAllowed = value;
    }

    /**
     * Gets the value of the receiveAgentID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiveAgentID() {
        return receiveAgentID;
    }

    /**
     * Sets the value of the receiveAgentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiveAgentID(String value) {
        this.receiveAgentID = value;
    }

    /**
     * Gets the value of the receiveAgentName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiveAgentName() {
        return receiveAgentName;
    }

    /**
     * Sets the value of the receiveAgentName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiveAgentName(String value) {
        this.receiveAgentName = value;
    }

    /**
     * Gets the value of the receiveAgentAbbreviation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiveAgentAbbreviation() {
        return receiveAgentAbbreviation;
    }

    /**
     * Sets the value of the receiveAgentAbbreviation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiveAgentAbbreviation(String value) {
        this.receiveAgentAbbreviation = value;
    }

}
