
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SaveDebugDataRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SaveDebugDataRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="endOfLog" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="collectionEndTimestamp" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="length" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="sequenceNumber" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="debugData" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SaveDebugDataRequest", propOrder = {
    "endOfLog",
    "collectionEndTimestamp",
    "length",
    "sequenceNumber",
    "debugData"
})
public class SaveDebugDataRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected boolean endOfLog;
    protected long collectionEndTimestamp;
    @XmlElement(required = true)
    protected BigInteger length;
    @XmlElement(required = true)
    protected BigInteger sequenceNumber;
    @XmlElement(required = true)
    protected String debugData;

    /**
     * Gets the value of the endOfLog property.
     * 
     */
    public boolean isEndOfLog() {
        return endOfLog;
    }

    /**
     * Sets the value of the endOfLog property.
     * 
     */
    public void setEndOfLog(boolean value) {
        this.endOfLog = value;
    }

    /**
     * Gets the value of the collectionEndTimestamp property.
     * 
     */
    public long getCollectionEndTimestamp() {
        return collectionEndTimestamp;
    }

    /**
     * Sets the value of the collectionEndTimestamp property.
     * 
     */
    public void setCollectionEndTimestamp(long value) {
        this.collectionEndTimestamp = value;
    }

    /**
     * Gets the value of the length property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLength() {
        return length;
    }

    /**
     * Sets the value of the length property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLength(BigInteger value) {
        this.length = value;
    }

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceNumber(BigInteger value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the debugData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebugData() {
        return debugData;
    }

    /**
     * Sets the value of the debugData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebugData(String value) {
        this.debugData = value;
    }

}
