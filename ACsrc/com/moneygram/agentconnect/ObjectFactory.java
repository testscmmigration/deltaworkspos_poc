
package com.moneygram.agentconnect;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.moneygram.agentconnect package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetEnumerationsResponsePayload_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "payload");
    private final static QName _GetBankDetailsRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getBankDetailsRequest");
    private final static QName _GetDepositInformationRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getDepositInformationRequest");
    private final static QName _ConsumerHistoryLookupResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "consumerHistoryLookupResponse");
    private final static QName _BillPaymentSummaryReportResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "billPaymentSummaryReportResponse");
    private final static QName _GetBroadcastMessagesResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getBroadcastMessagesResponse");
    private final static QName _SendValidationRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "sendValidationRequest");
    private final static QName _SaveDebugDataResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "saveDebugDataResponse");
    private final static QName _FieldToCollectInfo_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "fieldToCollectInfo");
    private final static QName _ComplianceTransactionResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "complianceTransactionResponse");
    private final static QName _BillPaymentSummaryReportRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "billPaymentSummaryReportRequest");
    private final static QName _DirectoryOfAgentsByZipResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "directoryOfAgentsByZipResponse");
    private final static QName _DwPasswordRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "dwPasswordRequest");
    private final static QName _VersionManifestRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "versionManifestRequest");
    private final static QName _BillPaymentDetailReportResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "billPaymentDetailReportResponse");
    private final static QName _CheckInRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "checkInRequest");
    private final static QName _DirectoryOfAgentsByCityResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "directoryOfAgentsByCityResponse");
    private final static QName _SendReversalValidationResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "sendReversalValidationResponse");
    private final static QName _ProfileResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "profileResponse");
    private final static QName _CreateOrUpdateProfileSenderResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "createOrUpdateProfileSenderResponse");
    private final static QName _GetServiceOptionsRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getServiceOptionsRequest");
    private final static QName _GetProfileReceiverResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getProfileReceiverResponse");
    private final static QName _GetDebugDataResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getDebugDataResponse");
    private final static QName _BillerSearchRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "billerSearchRequest");
    private final static QName _SaveConsumerProfileImageRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "saveConsumerProfileImageRequest");
    private final static QName _SaveSubagentsRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "saveSubagentsRequest");
    private final static QName _GetReceiptForReprintResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getReceiptForReprintResponse");
    private final static QName _GetDebugDataRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getDebugDataRequest");
    private final static QName _GetDepositInformationResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getDepositInformationResponse");
    private final static QName _GetAllFieldsResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getAllFieldsResponse");
    private final static QName _DirectoryOfAgentsByAreaCodePrefixResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "directoryOfAgentsByAreaCodePrefixResponse");
    private final static QName _SaveProfileRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "saveProfileRequest");
    private final static QName _SendReversalValidationRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "sendReversalValidationRequest");
    private final static QName _SearchStagedTransactionsRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "searchStagedTransactionsRequest");
    private final static QName _CompleteSessionRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "completeSessionRequest");
    private final static QName _GetCurrencyInfoRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getCurrencyInfoRequest");
    private final static QName _GetCountryInfoRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getCountryInfoRequest");
    private final static QName _MoneyGramReceiveDetailReportRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "moneyGramReceiveDetailReportRequest");
    private final static QName _GetUCPByConsumerAttributesRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getUCPByConsumerAttributesRequest");
    private final static QName _ConfirmTokenRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "confirmTokenRequest");
    private final static QName _DoddFrankStateRegulatorInfoRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "doddFrankStateRegulatorInfoRequest");
    private final static QName _DwPasswordResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "dwPasswordResponse");
    private final static QName _GetAllErrorsRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getAllErrorsRequest");
    private final static QName _AmendValidationRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "amendValidationRequest");
    private final static QName _CityListRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "cityListRequest");
    private final static QName _GetCountrySubdivisionRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getCountrySubdivisionRequest");
    private final static QName _BpValidationResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "bpValidationResponse");
    private final static QName _MoneyGramSendSummaryReportResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "moneyGramSendSummaryReportResponse");
    private final static QName _SearchConsumerProfilesResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "searchConsumerProfilesResponse");
    private final static QName _InjectedInstructionResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "injectedInstructionResponse");
    private final static QName _SaveProfileResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "saveProfileResponse");
    private final static QName _ComplianceTransactionRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "complianceTransactionRequest");
    private final static QName _ProfileChangeRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "profileChangeRequest");
    private final static QName _RegisterHardTokenRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "registerHardTokenRequest");
    private final static QName _CityListResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "cityListResponse");
    private final static QName _DirectoryOfAgentsByAreaCodePrefixRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "directoryOfAgentsByAreaCodePrefixRequest");
    private final static QName _CreateOrUpdateProfileConsumerRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "createOrUpdateProfileConsumerRequest");
    private final static QName _GetEnumerationsRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getEnumerationsRequest");
    private final static QName _TransactionLookupResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "transactionLookupResponse");
    private final static QName _TransactionLookupRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "transactionLookupRequest");
    private final static QName _DwInitialSetupResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "dwInitialSetupResponse");
    private final static QName _CreateOrUpdateProfileSenderRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "createOrUpdateProfileSenderRequest");
    private final static QName _DirectoryOfAgentsByCityRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "directoryOfAgentsByCityRequest");
    private final static QName _GetCurrencyInfoResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getCurrencyInfoResponse");
    private final static QName _MoneyOrderTotalRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "moneyOrderTotalRequest");
    private final static QName _DwProfileResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "dwProfileResponse");
    private final static QName _SaveSubagentsResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "saveSubagentsResponse");
    private final static QName _MoneyGramSendDetailReportWithTaxRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "moneyGramSendDetailReportWithTaxRequest");
    private final static QName _AmendValidationResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "amendValidationResponse");
    private final static QName _ReceiveReversalValidationRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "receiveReversalValidationRequest");
    private final static QName _GetBankDetailsByLevelResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getBankDetailsByLevelResponse");
    private final static QName _GetProfileConsumerRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getProfileConsumerRequest");
    private final static QName _GetDepositBankListRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getDepositBankListRequest");
    private final static QName _GetBankDetailsResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getBankDetailsResponse");
    private final static QName _OpenOTPLoginResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "openOTPLoginResponse");
    private final static QName _DepositAnnouncementRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "depositAnnouncementRequest");
    private final static QName _BillPaymentDetailReportRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "billPaymentDetailReportRequest");
    private final static QName _MoneyGramSendDetailReportRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "moneyGramSendDetailReportRequest");
    private final static QName _GetProfileConsumerResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getProfileConsumerResponse");
    private final static QName _DwProfileRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "dwProfileRequest");
    private final static QName _MoneyGramSendDetailReportResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "moneyGramSendDetailReportResponse");
    private final static QName _PromotionLookupByCodeRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "promotionLookupByCodeRequest");
    private final static QName _SystemError_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "SystemError");
    private final static QName _ProfileRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "profileRequest");
    private final static QName _DwInitialSetupRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "dwInitialSetupRequest");
    private final static QName _BpValidationRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "bpValidationRequest");
    private final static QName _FieldInfo_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "fieldInfo");
    private final static QName _MoneyGramSendDetailReportWithTaxResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "moneyGramSendDetailReportWithTaxResponse");
    private final static QName _SubagentsRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "subagentsRequest");
    private final static QName _SearchStagedTransactionsResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "searchStagedTransactionsResponse");
    private final static QName _TranslationsRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "translationsRequest");
    private final static QName _SubagentsResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "subagentsResponse");
    private final static QName _DisclosureTextDetailsResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "disclosureTextDetailsResponse");
    private final static QName _MoneyGramSendSummaryReportRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "moneyGramSendSummaryReportRequest");
    private final static QName _DoddFrankStateRegulatorInfoResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "doddFrankStateRegulatorInfoResponse");
    private final static QName _OpenOTPLoginRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "openOTPLoginRequest");
    private final static QName _SearchConsumerProfilesRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "searchConsumerProfilesRequest");
    private final static QName _GetCountrySubdivisionResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getCountrySubdivisionResponse");
    private final static QName _SaveDebugDataRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "saveDebugDataRequest");
    private final static QName _InfoBase_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "InfoBase");
    private final static QName _GetCountryInfoResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getCountryInfoResponse");
    private final static QName _MoneyGramReceiveSummaryReportRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "moneyGramReceiveSummaryReportRequest");
    private final static QName _DepositAnnouncementResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "depositAnnouncementResponse");
    private final static QName _CreateOrUpdateProfileReceiverResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "createOrUpdateProfileReceiverResponse");
    private final static QName _GetAllFieldsRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getAllFieldsRequest");
    private final static QName _MoneyOrderTotalResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "moneyOrderTotalResponse");
    private final static QName _CompleteSessionResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "completeSessionResponse");
    private final static QName _CategoryInfo_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "CategoryInfo");
    private final static QName _MoneyGramReceiveSummaryReportResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "moneyGramReceiveSummaryReportResponse");
    private final static QName _VariableReceiptInfoRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "variableReceiptInfoRequest");
    private final static QName _GenericBusinessError_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "GenericBusinessError");
    private final static QName _BusinessError_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "BusinessError");
    private final static QName _ConfirmTokenResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "confirmTokenResponse");
    private final static QName _ReceiveReversalValidationResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "receiveReversalValidationResponse");
    private final static QName _GetReceiptForReprintRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getReceiptForReprintRequest");
    private final static QName _DisclosureTextDetailsRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "disclosureTextDetailsRequest");
    private final static QName _RegisterHardTokenResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "registerHardTokenResponse");
    private final static QName _InitialSetupResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "initialSetupResponse");
    private final static QName _InjectedInstructionRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "injectedInstructionRequest");
    private final static QName _GetAllErrorsResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getAllErrorsResponse");
    private final static QName _PromotionLookupByCodeResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "promotionLookupByCodeResponse");
    private final static QName _CreateOrUpdateProfileConsumerResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "createOrUpdateProfileConsumerResponse");
    private final static QName _MoneyGramReceiveDetailReportResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "moneyGramReceiveDetailReportResponse");
    private final static QName _GetEnumerationsResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getEnumerationsResponse");
    private final static QName _GetProfileSenderRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getProfileSenderRequest");
    private final static QName _DirectoryOfAgentsByZipRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "directoryOfAgentsByZipRequest");
    private final static QName _ReceiveValidationResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "receiveValidationResponse");
    private final static QName _VersionManifestResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "versionManifestResponse");
    private final static QName _GetProfileReceiverRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getProfileReceiverRequest");
    private final static QName _SaveConsumerProfileImageResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "saveConsumerProfileImageResponse");
    private final static QName _IndustryResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "industryResponse");
    private final static QName _ReceiptsFormatDetailsResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "receiptsFormatDetailsResponse");
    private final static QName _GetProfileSenderResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getProfileSenderResponse");
    private final static QName _FeeLookupBySendCountryRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "feeLookupBySendCountryRequest");
    private final static QName _GetServiceOptionsResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getServiceOptionsResponse");
    private final static QName _FeeLookupResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "feeLookupResponse");
    private final static QName _CheckInResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "checkInResponse");
    private final static QName _TranslationsResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "translationsResponse");
    private final static QName _FeeLookupRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "feeLookupRequest");
    private final static QName _GetUCPByConsumerAttributesResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getUCPByConsumerAttributesResponse");
    private final static QName _ReceiptsFormatDetailsRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "receiptsFormatDetailsRequest");
    private final static QName _VariableReceiptInfoResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "variableReceiptInfoResponse");
    private final static QName _IndustryRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "industryRequest");
    private final static QName _ProfileChangeResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "profileChangeResponse");
    private final static QName _GetDepositBankListResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getDepositBankListResponse");
    private final static QName _ConsumerHistoryLookupRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "consumerHistoryLookupRequest");
    private final static QName _InitialSetupRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "initialSetupRequest");
    private final static QName _GetBroadcastMessagesRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getBroadcastMessagesRequest");
    private final static QName _BillerSearchResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "billerSearchResponse");
    private final static QName _CreateOrUpdateProfileReceiverRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "createOrUpdateProfileReceiverRequest");
    private final static QName _GetConsumerProfileTransactionHistoryResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getConsumerProfileTransactionHistoryResponse");
    private final static QName _GetConsumerProfileTransactionHistoryRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getConsumerProfileTransactionHistoryRequest");
    private final static QName _FeeLookupBySendCountryResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "feeLookupBySendCountryResponse");
    private final static QName _GetBankDetailsByLevelRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "getBankDetailsByLevelRequest");
    private final static QName _SendValidationResponse_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "sendValidationResponse");
    private final static QName _ReceiveValidationRequest_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "receiveValidationRequest");
    private final static QName _KeyValuePairTypeValue_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "value");
    private final static QName _CategoryInfoInfos_QNAME = new QName("http://www.moneygram.com/AgentConnect1705", "infos");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.moneygram.agentconnect
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SendReversalValidationResponse.Payload.ReceiptInfo.DisclosureTexts }
     * 
     */
    public SendReversalValidationResponse.Payload.ReceiptInfo.DisclosureTexts createSendReversalValidationResponsePayloadReceiptInfoDisclosureTexts() {
        return new SendReversalValidationResponse.Payload.ReceiptInfo.DisclosureTexts();
    }

    /**
     * Create an instance of {@link MoneyGramSendSummaryReportRequest }
     * 
     */
    public MoneyGramSendSummaryReportRequest createMoneyGramSendSummaryReportRequest() {
        return new MoneyGramSendSummaryReportRequest();
    }

    /**
     * Create an instance of {@link GetAllErrorsResponse }
     * 
     */
    public GetAllErrorsResponse createGetAllErrorsResponse() {
        return new GetAllErrorsResponse();
    }

    /**
     * Create an instance of {@link CompleteSessionResponse.Payload }
     * 
     */
    public CompleteSessionResponse.Payload createCompleteSessionResponsePayload() {
        return new CompleteSessionResponse.Payload();
    }

    /**
     * Create an instance of {@link VariableReceiptTextInfo }
     * 
     */
    public VariableReceiptTextInfo createVariableReceiptTextInfo() {
        return new VariableReceiptTextInfo();
    }

    /**
     * Create an instance of {@link InitialSetupResponse.Payload }
     * 
     */
    public InitialSetupResponse.Payload createInitialSetupResponsePayload() {
        return new InitialSetupResponse.Payload();
    }

    /**
     * Create an instance of {@link SubagentsResponse.Payload }
     * 
     */
    public SubagentsResponse.Payload createSubagentsResponsePayload() {
        return new SubagentsResponse.Payload();
    }

    /**
     * Create an instance of {@link FieldToCollectInfo.ChildFields }
     * 
     */
    public FieldToCollectInfo.ChildFields createFieldToCollectInfoChildFields() {
        return new FieldToCollectInfo.ChildFields();
    }

    /**
     * Create an instance of {@link MoneyGramReceiveSummaryInfo }
     * 
     */
    public MoneyGramReceiveSummaryInfo createMoneyGramReceiveSummaryInfo() {
        return new MoneyGramReceiveSummaryInfo();
    }

    /**
     * Create an instance of {@link BPValidationResponse.Payload.ReceiptInfo.PromotionalMessages }
     * 
     */
    public BPValidationResponse.Payload.ReceiptInfo.PromotionalMessages createBPValidationResponsePayloadReceiptInfoPromotionalMessages() {
        return new BPValidationResponse.Payload.ReceiptInfo.PromotionalMessages();
    }

    /**
     * Create an instance of {@link SenderLookupInfo.BillerInfos }
     * 
     */
    public SenderLookupInfo.BillerInfos createSenderLookupInfoBillerInfos() {
        return new SenderLookupInfo.BillerInfos();
    }

    /**
     * Create an instance of {@link SearchStagedTransactionsResponse.Payload }
     * 
     */
    public SearchStagedTransactionsResponse.Payload createSearchStagedTransactionsResponsePayload() {
        return new SearchStagedTransactionsResponse.Payload();
    }

    /**
     * Create an instance of {@link SubdivisionInfo }
     * 
     */
    public SubdivisionInfo createSubdivisionInfo() {
        return new SubdivisionInfo();
    }

    /**
     * Create an instance of {@link SearchConsumerProfilesResponse.Payload.ConsumerProfileSearchInfos }
     * 
     */
    public SearchConsumerProfilesResponse.Payload.ConsumerProfileSearchInfos createSearchConsumerProfilesResponsePayloadConsumerProfileSearchInfos() {
        return new SearchConsumerProfilesResponse.Payload.ConsumerProfileSearchInfos();
    }

    /**
     * Create an instance of {@link DirectoryOfAgentsByAreaCodePrefixResponse }
     * 
     */
    public DirectoryOfAgentsByAreaCodePrefixResponse createDirectoryOfAgentsByAreaCodePrefixResponse() {
        return new DirectoryOfAgentsByAreaCodePrefixResponse();
    }

    /**
     * Create an instance of {@link MoneyOrderInfo }
     * 
     */
    public MoneyOrderInfo createMoneyOrderInfo() {
        return new MoneyOrderInfo();
    }

    /**
     * Create an instance of {@link ReceiptType }
     * 
     */
    public ReceiptType createReceiptType() {
        return new ReceiptType();
    }

    /**
     * Create an instance of {@link ReceiveReversalValidationResponse }
     * 
     */
    public ReceiveReversalValidationResponse createReceiveReversalValidationResponse() {
        return new ReceiveReversalValidationResponse();
    }

    /**
     * Create an instance of {@link DCATokenType }
     * 
     */
    public DCATokenType createDCATokenType() {
        return new DCATokenType();
    }

    /**
     * Create an instance of {@link VariableReceiptInfoRequest }
     * 
     */
    public VariableReceiptInfoRequest createVariableReceiptInfoRequest() {
        return new VariableReceiptInfoRequest();
    }

    /**
     * Create an instance of {@link SendReversalValidationRequest.VerifiedFields }
     * 
     */
    public SendReversalValidationRequest.VerifiedFields createSendReversalValidationRequestVerifiedFields() {
        return new SendReversalValidationRequest.VerifiedFields();
    }

    /**
     * Create an instance of {@link AgentAddressType.AgentAddress }
     * 
     */
    public AgentAddressType.AgentAddress createAgentAddressTypeAgentAddress() {
        return new AgentAddressType.AgentAddress();
    }

    /**
     * Create an instance of {@link ReceiveValidationRequest.ReceiptImages }
     * 
     */
    public ReceiveValidationRequest.ReceiptImages createReceiveValidationRequestReceiptImages() {
        return new ReceiveValidationRequest.ReceiptImages();
    }

    /**
     * Create an instance of {@link ReceiveValidationResponse.Payload.ReceiptInfo.DisclosureTexts }
     * 
     */
    public ReceiveValidationResponse.Payload.ReceiptInfo.DisclosureTexts createReceiveValidationResponsePayloadReceiptInfoDisclosureTexts() {
        return new ReceiveValidationResponse.Payload.ReceiptInfo.DisclosureTexts();
    }

    /**
     * Create an instance of {@link BankInfo }
     * 
     */
    public BankInfo createBankInfo() {
        return new BankInfo();
    }

    /**
     * Create an instance of {@link CheckInResponse }
     * 
     */
    public CheckInResponse createCheckInResponse() {
        return new CheckInResponse();
    }

    /**
     * Create an instance of {@link MoneyGramReceiveDetailReportRequest }
     * 
     */
    public MoneyGramReceiveDetailReportRequest createMoneyGramReceiveDetailReportRequest() {
        return new MoneyGramReceiveDetailReportRequest();
    }

    /**
     * Create an instance of {@link TransactionLookupRequest }
     * 
     */
    public TransactionLookupRequest createTransactionLookupRequest() {
        return new TransactionLookupRequest();
    }

    /**
     * Create an instance of {@link ProfileRequest }
     * 
     */
    public ProfileRequest createProfileRequest() {
        return new ProfileRequest();
    }

    /**
     * Create an instance of {@link GetConsumerProfileTransactionHistoryResponse }
     * 
     */
    public GetConsumerProfileTransactionHistoryResponse createGetConsumerProfileTransactionHistoryResponse() {
        return new GetConsumerProfileTransactionHistoryResponse();
    }

    /**
     * Create an instance of {@link PromotionLookupByCodeRequest }
     * 
     */
    public PromotionLookupByCodeRequest createPromotionLookupByCodeRequest() {
        return new PromotionLookupByCodeRequest();
    }

    /**
     * Create an instance of {@link DepositAnnouncementRequest }
     * 
     */
    public DepositAnnouncementRequest createDepositAnnouncementRequest() {
        return new DepositAnnouncementRequest();
    }

    /**
     * Create an instance of {@link ConsumerProfileIdentifierType }
     * 
     */
    public ConsumerProfileIdentifierType createConsumerProfileIdentifierType() {
        return new ConsumerProfileIdentifierType();
    }

    /**
     * Create an instance of {@link GetServiceOptionsRequest }
     * 
     */
    public GetServiceOptionsRequest createGetServiceOptionsRequest() {
        return new GetServiceOptionsRequest();
    }

    /**
     * Create an instance of {@link ReceiveReversalValidationResponse.Payload.ReceiptInfo }
     * 
     */
    public ReceiveReversalValidationResponse.Payload.ReceiptInfo createReceiveReversalValidationResponsePayloadReceiptInfo() {
        return new ReceiveReversalValidationResponse.Payload.ReceiptInfo();
    }

    /**
     * Create an instance of {@link CreateOrUpdateProfileReceiverResponse }
     * 
     */
    public CreateOrUpdateProfileReceiverResponse createCreateOrUpdateProfileReceiverResponse() {
        return new CreateOrUpdateProfileReceiverResponse();
    }

    /**
     * Create an instance of {@link SendValidationRequest.ReceiptImages }
     * 
     */
    public SendValidationRequest.ReceiptImages createSendValidationRequestReceiptImages() {
        return new SendValidationRequest.ReceiptImages();
    }

    /**
     * Create an instance of {@link DisclosureTextDetailsResponse }
     * 
     */
    public DisclosureTextDetailsResponse createDisclosureTextDetailsResponse() {
        return new DisclosureTextDetailsResponse();
    }

    /**
     * Create an instance of {@link PromotionInfo.PromotionErrorMessages }
     * 
     */
    public PromotionInfo.PromotionErrorMessages createPromotionInfoPromotionErrorMessages() {
        return new PromotionInfo.PromotionErrorMessages();
    }

    /**
     * Create an instance of {@link SendValidationRequest.PromoCodes }
     * 
     */
    public SendValidationRequest.PromoCodes createSendValidationRequestPromoCodes() {
        return new SendValidationRequest.PromoCodes();
    }

    /**
     * Create an instance of {@link IndustryInfo }
     * 
     */
    public IndustryInfo createIndustryInfo() {
        return new IndustryInfo();
    }

    /**
     * Create an instance of {@link BillPaymentDetailReportRequest }
     * 
     */
    public BillPaymentDetailReportRequest createBillPaymentDetailReportRequest() {
        return new BillPaymentDetailReportRequest();
    }

    /**
     * Create an instance of {@link VersionManifestResponse }
     * 
     */
    public VersionManifestResponse createVersionManifestResponse() {
        return new VersionManifestResponse();
    }

    /**
     * Create an instance of {@link DwProfileResponse.Payload }
     * 
     */
    public DwProfileResponse.Payload createDwProfileResponsePayload() {
        return new DwProfileResponse.Payload();
    }

    /**
     * Create an instance of {@link SendReversalValidationResponse.Payload }
     * 
     */
    public SendReversalValidationResponse.Payload createSendReversalValidationResponsePayload() {
        return new SendReversalValidationResponse.Payload();
    }

    /**
     * Create an instance of {@link GetConsumerProfileTransactionHistoryRequest }
     * 
     */
    public GetConsumerProfileTransactionHistoryRequest createGetConsumerProfileTransactionHistoryRequest() {
        return new GetConsumerProfileTransactionHistoryRequest();
    }

    /**
     * Create an instance of {@link AgentAddressType }
     * 
     */
    public AgentAddressType createAgentAddressType() {
        return new AgentAddressType();
    }

    /**
     * Create an instance of {@link ConfirmTokenResponse }
     * 
     */
    public ConfirmTokenResponse createConfirmTokenResponse() {
        return new ConfirmTokenResponse();
    }

    /**
     * Create an instance of {@link KeyValuePairType }
     * 
     */
    public KeyValuePairType createKeyValuePairType() {
        return new KeyValuePairType();
    }

    /**
     * Create an instance of {@link SendValidationResponse }
     * 
     */
    public SendValidationResponse createSendValidationResponse() {
        return new SendValidationResponse();
    }

    /**
     * Create an instance of {@link GetProfileSenderResponse.Payload }
     * 
     */
    public GetProfileSenderResponse.Payload createGetProfileSenderResponsePayload() {
        return new GetProfileSenderResponse.Payload();
    }

    /**
     * Create an instance of {@link CheckInRequest }
     * 
     */
    public CheckInRequest createCheckInRequest() {
        return new CheckInRequest();
    }

    /**
     * Create an instance of {@link SendValidationRequest.FieldValues }
     * 
     */
    public SendValidationRequest.FieldValues createSendValidationRequestFieldValues() {
        return new SendValidationRequest.FieldValues();
    }

    /**
     * Create an instance of {@link CreateOrUpdateProfileReceiverResponse.Payload.ConsumerProfileIDs }
     * 
     */
    public CreateOrUpdateProfileReceiverResponse.Payload.ConsumerProfileIDs createCreateOrUpdateProfileReceiverResponsePayloadConsumerProfileIDs() {
        return new CreateOrUpdateProfileReceiverResponse.Payload.ConsumerProfileIDs();
    }

    /**
     * Create an instance of {@link GetCurrencyInfoResponse }
     * 
     */
    public GetCurrencyInfoResponse createGetCurrencyInfoResponse() {
        return new GetCurrencyInfoResponse();
    }

    /**
     * Create an instance of {@link ReceiveReversalValidationResponse.Payload }
     * 
     */
    public ReceiveReversalValidationResponse.Payload createReceiveReversalValidationResponsePayload() {
        return new ReceiveReversalValidationResponse.Payload();
    }

    /**
     * Create an instance of {@link OpenOTPLoginResponse.Payload }
     * 
     */
    public OpenOTPLoginResponse.Payload createOpenOTPLoginResponsePayload() {
        return new OpenOTPLoginResponse.Payload();
    }

    /**
     * Create an instance of {@link SearchStagedTransactionsRequest }
     * 
     */
    public SearchStagedTransactionsRequest createSearchStagedTransactionsRequest() {
        return new SearchStagedTransactionsRequest();
    }

    /**
     * Create an instance of {@link StoreHourInfo }
     * 
     */
    public StoreHourInfo createStoreHourInfo() {
        return new StoreHourInfo();
    }

    /**
     * Create an instance of {@link SendReversalValidationResponse.Payload.DetailSendAmounts }
     * 
     */
    public SendReversalValidationResponse.Payload.DetailSendAmounts createSendReversalValidationResponsePayloadDetailSendAmounts() {
        return new SendReversalValidationResponse.Payload.DetailSendAmounts();
    }

    /**
     * Create an instance of {@link TransactionLookupResponse.Payload }
     * 
     */
    public TransactionLookupResponse.Payload createTransactionLookupResponsePayload() {
        return new TransactionLookupResponse.Payload();
    }

    /**
     * Create an instance of {@link IndustryTranslationType }
     * 
     */
    public IndustryTranslationType createIndustryTranslationType() {
        return new IndustryTranslationType();
    }

    /**
     * Create an instance of {@link AmendValidationRequest.FieldValues }
     * 
     */
    public AmendValidationRequest.FieldValues createAmendValidationRequestFieldValues() {
        return new AmendValidationRequest.FieldValues();
    }

    /**
     * Create an instance of {@link com.moneygram.agentconnect.Payload }
     * 
     */
    public com.moneygram.agentconnect.Payload createPayload() {
        return new com.moneygram.agentconnect.Payload();
    }

    /**
     * Create an instance of {@link BPValidationRequest.FieldValues }
     * 
     */
    public BPValidationRequest.FieldValues createBPValidationRequestFieldValues() {
        return new BPValidationRequest.FieldValues();
    }

    /**
     * Create an instance of {@link DwProfileResponse }
     * 
     */
    public DwProfileResponse createDwProfileResponse() {
        return new DwProfileResponse();
    }

    /**
     * Create an instance of {@link InitialSetupResponse }
     * 
     */
    public InitialSetupResponse createInitialSetupResponse() {
        return new InitialSetupResponse();
    }

    /**
     * Create an instance of {@link CreateOrUpdateProfileConsumerRequest }
     * 
     */
    public CreateOrUpdateProfileConsumerRequest createCreateOrUpdateProfileConsumerRequest() {
        return new CreateOrUpdateProfileConsumerRequest();
    }

    /**
     * Create an instance of {@link GetDebugDataResponse.Payload }
     * 
     */
    public GetDebugDataResponse.Payload createGetDebugDataResponsePayload() {
        return new GetDebugDataResponse.Payload();
    }

    /**
     * Create an instance of {@link GetReceiptForReprintRequest }
     * 
     */
    public GetReceiptForReprintRequest createGetReceiptForReprintRequest() {
        return new GetReceiptForReprintRequest();
    }

    /**
     * Create an instance of {@link SearchConsumerProfilesRequest }
     * 
     */
    public SearchConsumerProfilesRequest createSearchConsumerProfilesRequest() {
        return new SearchConsumerProfilesRequest();
    }

    /**
     * Create an instance of {@link GetAllErrorsResponse.Payload.DefinedErrors }
     * 
     */
    public GetAllErrorsResponse.Payload.DefinedErrors createGetAllErrorsResponsePayloadDefinedErrors() {
        return new GetAllErrorsResponse.Payload.DefinedErrors();
    }

    /**
     * Create an instance of {@link SaveProfileRequest.ProfileItems }
     * 
     */
    public SaveProfileRequest.ProfileItems createSaveProfileRequestProfileItems() {
        return new SaveProfileRequest.ProfileItems();
    }

    /**
     * Create an instance of {@link GetCurrencyInfoResponse.Payload.CurrencyInfos }
     * 
     */
    public GetCurrencyInfoResponse.Payload.CurrencyInfos createGetCurrencyInfoResponsePayloadCurrencyInfos() {
        return new GetCurrencyInfoResponse.Payload.CurrencyInfos();
    }

    /**
     * Create an instance of {@link ConfirmTokenRequest }
     * 
     */
    public ConfirmTokenRequest createConfirmTokenRequest() {
        return new ConfirmTokenRequest();
    }

    /**
     * Create an instance of {@link SendValidationResponse.Payload.ReceiptInfo }
     * 
     */
    public SendValidationResponse.Payload.ReceiptInfo createSendValidationResponsePayloadReceiptInfo() {
        return new SendValidationResponse.Payload.ReceiptInfo();
    }

    /**
     * Create an instance of {@link MoneyGramSendDetailReportResponse.Payload }
     * 
     */
    public MoneyGramSendDetailReportResponse.Payload createMoneyGramSendDetailReportResponsePayload() {
        return new MoneyGramSendDetailReportResponse.Payload();
    }

    /**
     * Create an instance of {@link ComplianceTransactionRequest }
     * 
     */
    public ComplianceTransactionRequest createComplianceTransactionRequest() {
        return new ComplianceTransactionRequest();
    }

    /**
     * Create an instance of {@link GetProfileSenderResponse }
     * 
     */
    public GetProfileSenderResponse createGetProfileSenderResponse() {
        return new GetProfileSenderResponse();
    }

    /**
     * Create an instance of {@link Response.Errors }
     * 
     */
    public Response.Errors createResponseErrors() {
        return new Response.Errors();
    }

    /**
     * Create an instance of {@link MoneyGramSendSummaryReportResponse.Payload }
     * 
     */
    public MoneyGramSendSummaryReportResponse.Payload createMoneyGramSendSummaryReportResponsePayload() {
        return new MoneyGramSendSummaryReportResponse.Payload();
    }

    /**
     * Create an instance of {@link GetConsumerProfileTransactionHistoryResponse.Payload }
     * 
     */
    public GetConsumerProfileTransactionHistoryResponse.Payload createGetConsumerProfileTransactionHistoryResponsePayload() {
        return new GetConsumerProfileTransactionHistoryResponse.Payload();
    }

    /**
     * Create an instance of {@link VariableReceiptInfoRequest.Languages }
     * 
     */
    public VariableReceiptInfoRequest.Languages createVariableReceiptInfoRequestLanguages() {
        return new VariableReceiptInfoRequest.Languages();
    }

    /**
     * Create an instance of {@link RegisterHardTokenResponse.Payload }
     * 
     */
    public RegisterHardTokenResponse.Payload createRegisterHardTokenResponsePayload() {
        return new RegisterHardTokenResponse.Payload();
    }

    /**
     * Create an instance of {@link MoneyGramSendDetailReportWithTaxRequest }
     * 
     */
    public MoneyGramSendDetailReportWithTaxRequest createMoneyGramSendDetailReportWithTaxRequest() {
        return new MoneyGramSendDetailReportWithTaxRequest();
    }

    /**
     * Create an instance of {@link SendReversalValidationResponse }
     * 
     */
    public SendReversalValidationResponse createSendReversalValidationResponse() {
        return new SendReversalValidationResponse();
    }

    /**
     * Create an instance of {@link GetConsumerProfileTransactionHistoryResponse.Payload.Transactions }
     * 
     */
    public GetConsumerProfileTransactionHistoryResponse.Payload.Transactions createGetConsumerProfileTransactionHistoryResponsePayloadTransactions() {
        return new GetConsumerProfileTransactionHistoryResponse.Payload.Transactions();
    }

    /**
     * Create an instance of {@link ChildField }
     * 
     */
    public ChildField createChildField() {
        return new ChildField();
    }

    /**
     * Create an instance of {@link GetCountrySubdivisionResponse.Payload }
     * 
     */
    public GetCountrySubdivisionResponse.Payload createGetCountrySubdivisionResponsePayload() {
        return new GetCountrySubdivisionResponse.Payload();
    }

    /**
     * Create an instance of {@link SearchConsumerProfilesResponse }
     * 
     */
    public SearchConsumerProfilesResponse createSearchConsumerProfilesResponse() {
        return new SearchConsumerProfilesResponse();
    }

    /**
     * Create an instance of {@link ReceiveAmountInfo }
     * 
     */
    public ReceiveAmountInfo createReceiveAmountInfo() {
        return new ReceiveAmountInfo();
    }

    /**
     * Create an instance of {@link GetProfileSenderResponse.Payload.Billers }
     * 
     */
    public GetProfileSenderResponse.Payload.Billers createGetProfileSenderResponsePayloadBillers() {
        return new GetProfileSenderResponse.Payload.Billers();
    }

    /**
     * Create an instance of {@link CompletionReceiptType }
     * 
     */
    public CompletionReceiptType createCompletionReceiptType() {
        return new CompletionReceiptType();
    }

    /**
     * Create an instance of {@link GetProfileReceiverResponse }
     * 
     */
    public GetProfileReceiverResponse createGetProfileReceiverResponse() {
        return new GetProfileReceiverResponse();
    }

    /**
     * Create an instance of {@link FeeLookupRequest.PromoCodes }
     * 
     */
    public FeeLookupRequest.PromoCodes createFeeLookupRequestPromoCodes() {
        return new FeeLookupRequest.PromoCodes();
    }

    /**
     * Create an instance of {@link IndustryRequest }
     * 
     */
    public IndustryRequest createIndustryRequest() {
        return new IndustryRequest();
    }

    /**
     * Create an instance of {@link SaveSubagentsResponse }
     * 
     */
    public SaveSubagentsResponse createSaveSubagentsResponse() {
        return new SaveSubagentsResponse();
    }

    /**
     * Create an instance of {@link SaveDebugDataResponse }
     * 
     */
    public SaveDebugDataResponse createSaveDebugDataResponse() {
        return new SaveDebugDataResponse();
    }

    /**
     * Create an instance of {@link InfoBase }
     * 
     */
    public InfoBase createInfoBase() {
        return new InfoBase();
    }

    /**
     * Create an instance of {@link FeeInfo }
     * 
     */
    public FeeInfo createFeeInfo() {
        return new FeeInfo();
    }

    /**
     * Create an instance of {@link CreateOrUpdateProfileConsumerResponse.Payload.FieldsToCollect }
     * 
     */
    public CreateOrUpdateProfileConsumerResponse.Payload.FieldsToCollect createCreateOrUpdateProfileConsumerResponsePayloadFieldsToCollect() {
        return new CreateOrUpdateProfileConsumerResponse.Payload.FieldsToCollect();
    }

    /**
     * Create an instance of {@link GetDepositBankListResponse }
     * 
     */
    public GetDepositBankListResponse createGetDepositBankListResponse() {
        return new GetDepositBankListResponse();
    }

    /**
     * Create an instance of {@link CreateOrUpdateProfileConsumerRequest.FieldValues }
     * 
     */
    public CreateOrUpdateProfileConsumerRequest.FieldValues createCreateOrUpdateProfileConsumerRequestFieldValues() {
        return new CreateOrUpdateProfileConsumerRequest.FieldValues();
    }

    /**
     * Create an instance of {@link SaveDebugDataRequest }
     * 
     */
    public SaveDebugDataRequest createSaveDebugDataRequest() {
        return new SaveDebugDataRequest();
    }

    /**
     * Create an instance of {@link SaveConsumerProfileImageRequest }
     * 
     */
    public SaveConsumerProfileImageRequest createSaveConsumerProfileImageRequest() {
        return new SaveConsumerProfileImageRequest();
    }

    /**
     * Create an instance of {@link ReceiveReversalValidationRequest }
     * 
     */
    public ReceiveReversalValidationRequest createReceiveReversalValidationRequest() {
        return new ReceiveReversalValidationRequest();
    }

    /**
     * Create an instance of {@link ProfileChangeResponse }
     * 
     */
    public ProfileChangeResponse createProfileChangeResponse() {
        return new ProfileChangeResponse();
    }

    /**
     * Create an instance of {@link BillPaymentSummaryReportResponse }
     * 
     */
    public BillPaymentSummaryReportResponse createBillPaymentSummaryReportResponse() {
        return new BillPaymentSummaryReportResponse();
    }

    /**
     * Create an instance of {@link MoneyOrderTotalRequest }
     * 
     */
    public MoneyOrderTotalRequest createMoneyOrderTotalRequest() {
        return new MoneyOrderTotalRequest();
    }

    /**
     * Create an instance of {@link DwInitialSetupResponse.Payload }
     * 
     */
    public DwInitialSetupResponse.Payload createDwInitialSetupResponsePayload() {
        return new DwInitialSetupResponse.Payload();
    }

    /**
     * Create an instance of {@link MoneyGramReceiveSummaryReportRequest }
     * 
     */
    public MoneyGramReceiveSummaryReportRequest createMoneyGramReceiveSummaryReportRequest() {
        return new MoneyGramReceiveSummaryReportRequest();
    }

    /**
     * Create an instance of {@link MoneyGramSendSummaryInfo }
     * 
     */
    public MoneyGramSendSummaryInfo createMoneyGramSendSummaryInfo() {
        return new MoneyGramSendSummaryInfo();
    }

    /**
     * Create an instance of {@link CreateOrUpdateProfileReceiverRequest.FieldValues }
     * 
     */
    public CreateOrUpdateProfileReceiverRequest.FieldValues createCreateOrUpdateProfileReceiverRequestFieldValues() {
        return new CreateOrUpdateProfileReceiverRequest.FieldValues();
    }

    /**
     * Create an instance of {@link EnumeratedTypeInfo.EnumeratedItems }
     * 
     */
    public EnumeratedTypeInfo.EnumeratedItems createEnumeratedTypeInfoEnumeratedItems() {
        return new EnumeratedTypeInfo.EnumeratedItems();
    }

    /**
     * Create an instance of {@link GetDebugDataResponse }
     * 
     */
    public GetDebugDataResponse createGetDebugDataResponse() {
        return new GetDebugDataResponse();
    }

    /**
     * Create an instance of {@link TransactionLookupResponse.Payload.ReceiveFeeDisclosureTexts }
     * 
     */
    public TransactionLookupResponse.Payload.ReceiveFeeDisclosureTexts createTransactionLookupResponsePayloadReceiveFeeDisclosureTexts() {
        return new TransactionLookupResponse.Payload.ReceiveFeeDisclosureTexts();
    }

    /**
     * Create an instance of {@link ConsumerHistoryLookupResponse }
     * 
     */
    public ConsumerHistoryLookupResponse createConsumerHistoryLookupResponse() {
        return new ConsumerHistoryLookupResponse();
    }

    /**
     * Create an instance of {@link DirectoryOfAgentsByCityResponse }
     * 
     */
    public DirectoryOfAgentsByCityResponse createDirectoryOfAgentsByCityResponse() {
        return new DirectoryOfAgentsByCityResponse();
    }

    /**
     * Create an instance of {@link CountryInfo }
     * 
     */
    public CountryInfo createCountryInfo() {
        return new CountryInfo();
    }

    /**
     * Create an instance of {@link ReceiptsFormatDetailsRequest.DcaTokenValues }
     * 
     */
    public ReceiptsFormatDetailsRequest.DcaTokenValues createReceiptsFormatDetailsRequestDcaTokenValues() {
        return new ReceiptsFormatDetailsRequest.DcaTokenValues();
    }

    /**
     * Create an instance of {@link RegisterHardTokenRequest }
     * 
     */
    public RegisterHardTokenRequest createRegisterHardTokenRequest() {
        return new RegisterHardTokenRequest();
    }

    /**
     * Create an instance of {@link FeeLookupBySendCountryResponse }
     * 
     */
    public FeeLookupBySendCountryResponse createFeeLookupBySendCountryResponse() {
        return new FeeLookupBySendCountryResponse();
    }

    /**
     * Create an instance of {@link CheckInResponse.Payload }
     * 
     */
    public CheckInResponse.Payload createCheckInResponsePayload() {
        return new CheckInResponse.Payload();
    }

    /**
     * Create an instance of {@link GetEnumerationsResponse.Payload }
     * 
     */
    public GetEnumerationsResponse.Payload createGetEnumerationsResponsePayload() {
        return new GetEnumerationsResponse.Payload();
    }

    /**
     * Create an instance of {@link AmendValidationResponse.Payload.ReceiptInfo }
     * 
     */
    public AmendValidationResponse.Payload.ReceiptInfo createAmendValidationResponsePayloadReceiptInfo() {
        return new AmendValidationResponse.Payload.ReceiptInfo();
    }

    /**
     * Create an instance of {@link MoneyGramReceiveSummaryReportResponse }
     * 
     */
    public MoneyGramReceiveSummaryReportResponse createMoneyGramReceiveSummaryReportResponse() {
        return new MoneyGramReceiveSummaryReportResponse();
    }

    /**
     * Create an instance of {@link BillPaymentDetailInfo }
     * 
     */
    public BillPaymentDetailInfo createBillPaymentDetailInfo() {
        return new BillPaymentDetailInfo();
    }

    /**
     * Create an instance of {@link SaveSubagentsRequest }
     * 
     */
    public SaveSubagentsRequest createSaveSubagentsRequest() {
        return new SaveSubagentsRequest();
    }

    /**
     * Create an instance of {@link VariableReceiptInfoResponse.Payload }
     * 
     */
    public VariableReceiptInfoResponse.Payload createVariableReceiptInfoResponsePayload() {
        return new VariableReceiptInfoResponse.Payload();
    }

    /**
     * Create an instance of {@link BillPaymentSummaryReportResponse.Payload }
     * 
     */
    public BillPaymentSummaryReportResponse.Payload createBillPaymentSummaryReportResponsePayload() {
        return new BillPaymentSummaryReportResponse.Payload();
    }

    /**
     * Create an instance of {@link TranslationsRequest }
     * 
     */
    public TranslationsRequest createTranslationsRequest() {
        return new TranslationsRequest();
    }

    /**
     * Create an instance of {@link InjectedInstructionRequest }
     * 
     */
    public InjectedInstructionRequest createInjectedInstructionRequest() {
        return new InjectedInstructionRequest();
    }

    /**
     * Create an instance of {@link DwInitialSetupRequest }
     * 
     */
    public DwInitialSetupRequest createDwInitialSetupRequest() {
        return new DwInitialSetupRequest();
    }

    /**
     * Create an instance of {@link GetReceiptForReprintResponse.Payload }
     * 
     */
    public GetReceiptForReprintResponse.Payload createGetReceiptForReprintResponsePayload() {
        return new GetReceiptForReprintResponse.Payload();
    }

    /**
     * Create an instance of {@link DirectoryOfAgentsByAreaCodePrefixRequest }
     * 
     */
    public DirectoryOfAgentsByAreaCodePrefixRequest createDirectoryOfAgentsByAreaCodePrefixRequest() {
        return new DirectoryOfAgentsByAreaCodePrefixRequest();
    }

    /**
     * Create an instance of {@link SaveProfileResponse }
     * 
     */
    public SaveProfileResponse createSaveProfileResponse() {
        return new SaveProfileResponse();
    }

    /**
     * Create an instance of {@link DwPasswordResponse }
     * 
     */
    public DwPasswordResponse createDwPasswordResponse() {
        return new DwPasswordResponse();
    }

    /**
     * Create an instance of {@link ConfirmTokenResponse.Payload }
     * 
     */
    public ConfirmTokenResponse.Payload createConfirmTokenResponsePayload() {
        return new ConfirmTokenResponse.Payload();
    }

    /**
     * Create an instance of {@link GetBroadcastMessagesRequest }
     * 
     */
    public GetBroadcastMessagesRequest createGetBroadcastMessagesRequest() {
        return new GetBroadcastMessagesRequest();
    }

    /**
     * Create an instance of {@link ReceiveValidationResponse.Payload.FieldsToCollect }
     * 
     */
    public ReceiveValidationResponse.Payload.FieldsToCollect createReceiveValidationResponsePayloadFieldsToCollect() {
        return new ReceiveValidationResponse.Payload.FieldsToCollect();
    }

    /**
     * Create an instance of {@link GetDepositInformationResponse }
     * 
     */
    public GetDepositInformationResponse createGetDepositInformationResponse() {
        return new GetDepositInformationResponse();
    }

    /**
     * Create an instance of {@link UCPRequestEmailType }
     * 
     */
    public UCPRequestEmailType createUCPRequestEmailType() {
        return new UCPRequestEmailType();
    }

    /**
     * Create an instance of {@link DirectoryOfAgentsByCityResponse.Payload }
     * 
     */
    public DirectoryOfAgentsByCityResponse.Payload createDirectoryOfAgentsByCityResponsePayload() {
        return new DirectoryOfAgentsByCityResponse.Payload();
    }

    /**
     * Create an instance of {@link DisclosureTextDetailsRequest }
     * 
     */
    public DisclosureTextDetailsRequest createDisclosureTextDetailsRequest() {
        return new DisclosureTextDetailsRequest();
    }

    /**
     * Create an instance of {@link SaveProfileResponse.Payload }
     * 
     */
    public SaveProfileResponse.Payload createSaveProfileResponsePayload() {
        return new SaveProfileResponse.Payload();
    }

    /**
     * Create an instance of {@link SearchStagedTransactionsResponse.Payload.StagedTransactionInfos }
     * 
     */
    public SearchStagedTransactionsResponse.Payload.StagedTransactionInfos createSearchStagedTransactionsResponsePayloadStagedTransactionInfos() {
        return new SearchStagedTransactionsResponse.Payload.StagedTransactionInfos();
    }

    /**
     * Create an instance of {@link SendValidationRequest }
     * 
     */
    public SendValidationRequest createSendValidationRequest() {
        return new SendValidationRequest();
    }

    /**
     * Create an instance of {@link StateRegulatorInfo }
     * 
     */
    public StateRegulatorInfo createStateRegulatorInfo() {
        return new StateRegulatorInfo();
    }

    /**
     * Create an instance of {@link ConsumerProfileTransactionHistoryType }
     * 
     */
    public ConsumerProfileTransactionHistoryType createConsumerProfileTransactionHistoryType() {
        return new ConsumerProfileTransactionHistoryType();
    }

    /**
     * Create an instance of {@link PaymentTypeInfo }
     * 
     */
    public PaymentTypeInfo createPaymentTypeInfo() {
        return new PaymentTypeInfo();
    }

    /**
     * Create an instance of {@link ReceiveValidationResponse.Payload }
     * 
     */
    public ReceiveValidationResponse.Payload createReceiveValidationResponsePayload() {
        return new ReceiveValidationResponse.Payload();
    }

    /**
     * Create an instance of {@link PartnerCreditType }
     * 
     */
    public PartnerCreditType createPartnerCreditType() {
        return new PartnerCreditType();
    }

    /**
     * Create an instance of {@link CreateOrUpdateProfileSenderResponse }
     * 
     */
    public CreateOrUpdateProfileSenderResponse createCreateOrUpdateProfileSenderResponse() {
        return new CreateOrUpdateProfileSenderResponse();
    }

    /**
     * Create an instance of {@link GetDepositBankListResponse.Payload }
     * 
     */
    public GetDepositBankListResponse.Payload createGetDepositBankListResponsePayload() {
        return new GetDepositBankListResponse.Payload();
    }

    /**
     * Create an instance of {@link CustomerComplianceInfo }
     * 
     */
    public CustomerComplianceInfo createCustomerComplianceInfo() {
        return new CustomerComplianceInfo();
    }

    /**
     * Create an instance of {@link EnumeratedTypeInfo }
     * 
     */
    public EnumeratedTypeInfo createEnumeratedTypeInfo() {
        return new EnumeratedTypeInfo();
    }

    /**
     * Create an instance of {@link GetBroadcastMessagesResponse.Payload.MessageInfos }
     * 
     */
    public GetBroadcastMessagesResponse.Payload.MessageInfos createGetBroadcastMessagesResponsePayloadMessageInfos() {
        return new GetBroadcastMessagesResponse.Payload.MessageInfos();
    }

    /**
     * Create an instance of {@link BPValidationResponse.Payload.ReceiptInfo.BillerNotes }
     * 
     */
    public BPValidationResponse.Payload.ReceiptInfo.BillerNotes createBPValidationResponsePayloadReceiptInfoBillerNotes() {
        return new BPValidationResponse.Payload.ReceiptInfo.BillerNotes();
    }

    /**
     * Create an instance of {@link DisclosureTextDetailsType }
     * 
     */
    public DisclosureTextDetailsType createDisclosureTextDetailsType() {
        return new DisclosureTextDetailsType();
    }

    /**
     * Create an instance of {@link ReceiptSegmentType }
     * 
     */
    public ReceiptSegmentType createReceiptSegmentType() {
        return new ReceiptSegmentType();
    }

    /**
     * Create an instance of {@link FieldInfo }
     * 
     */
    public FieldInfo createFieldInfo() {
        return new FieldInfo();
    }

    /**
     * Create an instance of {@link CompleteSessionResponse.Payload.ReceiptInfo }
     * 
     */
    public CompleteSessionResponse.Payload.ReceiptInfo createCompleteSessionResponsePayloadReceiptInfo() {
        return new CompleteSessionResponse.Payload.ReceiptInfo();
    }

    /**
     * Create an instance of {@link TranslationsResponse.Payload }
     * 
     */
    public TranslationsResponse.Payload createTranslationsResponsePayload() {
        return new TranslationsResponse.Payload();
    }

    /**
     * Create an instance of {@link ReceiveReversalValidationRequest.FieldValues }
     * 
     */
    public ReceiveReversalValidationRequest.FieldValues createReceiveReversalValidationRequestFieldValues() {
        return new ReceiveReversalValidationRequest.FieldValues();
    }

    /**
     * Create an instance of {@link GetDepositBankListRequest }
     * 
     */
    public GetDepositBankListRequest createGetDepositBankListRequest() {
        return new GetDepositBankListRequest();
    }

    /**
     * Create an instance of {@link ConsumerProfileIDInfo }
     * 
     */
    public ConsumerProfileIDInfo createConsumerProfileIDInfo() {
        return new ConsumerProfileIDInfo();
    }

    /**
     * Create an instance of {@link PromotionLookupByCodeResponse.Payload }
     * 
     */
    public PromotionLookupByCodeResponse.Payload createPromotionLookupByCodeResponsePayload() {
        return new PromotionLookupByCodeResponse.Payload();
    }

    /**
     * Create an instance of {@link BPValidationResponse.Payload.ReceiptInfo.ExpectedPostingTimeFrames }
     * 
     */
    public BPValidationResponse.Payload.ReceiptInfo.ExpectedPostingTimeFrames createBPValidationResponsePayloadReceiptInfoExpectedPostingTimeFrames() {
        return new BPValidationResponse.Payload.ReceiptInfo.ExpectedPostingTimeFrames();
    }

    /**
     * Create an instance of {@link VersionManifestResponse.Payload }
     * 
     */
    public VersionManifestResponse.Payload createVersionManifestResponsePayload() {
        return new VersionManifestResponse.Payload();
    }

    /**
     * Create an instance of {@link DwPasswordResponse.Payload }
     * 
     */
    public DwPasswordResponse.Payload createDwPasswordResponsePayload() {
        return new DwPasswordResponse.Payload();
    }

    /**
     * Create an instance of {@link MessageInfo }
     * 
     */
    public MessageInfo createMessageInfo() {
        return new MessageInfo();
    }

    /**
     * Create an instance of {@link BPValidationResponse.Payload.FieldsToCollect }
     * 
     */
    public BPValidationResponse.Payload.FieldsToCollect createBPValidationResponsePayloadFieldsToCollect() {
        return new BPValidationResponse.Payload.FieldsToCollect();
    }

    /**
     * Create an instance of {@link ReceiveReversalValidationResponse.Payload.FieldsToCollect }
     * 
     */
    public ReceiveReversalValidationResponse.Payload.FieldsToCollect createReceiveReversalValidationResponsePayloadFieldsToCollect() {
        return new ReceiveReversalValidationResponse.Payload.FieldsToCollect();
    }

    /**
     * Create an instance of {@link TranslationsResponse }
     * 
     */
    public TranslationsResponse createTranslationsResponse() {
        return new TranslationsResponse();
    }

    /**
     * Create an instance of {@link CreateOrUpdateProfileConsumerRequest.VerifiedFields }
     * 
     */
    public CreateOrUpdateProfileConsumerRequest.VerifiedFields createCreateOrUpdateProfileConsumerRequestVerifiedFields() {
        return new CreateOrUpdateProfileConsumerRequest.VerifiedFields();
    }

    /**
     * Create an instance of {@link CreateOrUpdateProfileReceiverResponse.Payload.FieldsToCollect }
     * 
     */
    public CreateOrUpdateProfileReceiverResponse.Payload.FieldsToCollect createCreateOrUpdateProfileReceiverResponsePayloadFieldsToCollect() {
        return new CreateOrUpdateProfileReceiverResponse.Payload.FieldsToCollect();
    }

    /**
     * Create an instance of {@link FieldToCollectInfo }
     * 
     */
    public FieldToCollectInfo createFieldToCollectInfo() {
        return new FieldToCollectInfo();
    }

    /**
     * Create an instance of {@link ServiceOptionInfo }
     * 
     */
    public ServiceOptionInfo createServiceOptionInfo() {
        return new ServiceOptionInfo();
    }

    /**
     * Create an instance of {@link IndustryResponse }
     * 
     */
    public IndustryResponse createIndustryResponse() {
        return new IndustryResponse();
    }

    /**
     * Create an instance of {@link GetAllErrorsRequest }
     * 
     */
    public GetAllErrorsRequest createGetAllErrorsRequest() {
        return new GetAllErrorsRequest();
    }

    /**
     * Create an instance of {@link CompleteSessionRequest.ReceiptImages }
     * 
     */
    public CompleteSessionRequest.ReceiptImages createCompleteSessionRequestReceiptImages() {
        return new CompleteSessionRequest.ReceiptImages();
    }

    /**
     * Create an instance of {@link CreateOrUpdateProfileConsumerResponse.Payload.ConsumerProfileIDs }
     * 
     */
    public CreateOrUpdateProfileConsumerResponse.Payload.ConsumerProfileIDs createCreateOrUpdateProfileConsumerResponsePayloadConsumerProfileIDs() {
        return new CreateOrUpdateProfileConsumerResponse.Payload.ConsumerProfileIDs();
    }

    /**
     * Create an instance of {@link DepositAndProofInfo }
     * 
     */
    public DepositAndProofInfo createDepositAndProofInfo() {
        return new DepositAndProofInfo();
    }

    /**
     * Create an instance of {@link DestinationCountryInfo.ServiceOptionInfos }
     * 
     */
    public DestinationCountryInfo.ServiceOptionInfos createDestinationCountryInfoServiceOptionInfos() {
        return new DestinationCountryInfo.ServiceOptionInfos();
    }

    /**
     * Create an instance of {@link InjectedInstructionResponse.Payload }
     * 
     */
    public InjectedInstructionResponse.Payload createInjectedInstructionResponsePayload() {
        return new InjectedInstructionResponse.Payload();
    }

    /**
     * Create an instance of {@link NameType }
     * 
     */
    public NameType createNameType() {
        return new NameType();
    }

    /**
     * Create an instance of {@link TimeZoneCorrectionType }
     * 
     */
    public TimeZoneCorrectionType createTimeZoneCorrectionType() {
        return new TimeZoneCorrectionType();
    }

    /**
     * Create an instance of {@link ReceiveValidationResponse.Payload.ReceiptInfo }
     * 
     */
    public ReceiveValidationResponse.Payload.ReceiptInfo createReceiveValidationResponsePayloadReceiptInfo() {
        return new ReceiveValidationResponse.Payload.ReceiptInfo();
    }

    /**
     * Create an instance of {@link AmendValidationResponse.Payload.ReceiptInfo.DisclosureTexts }
     * 
     */
    public AmendValidationResponse.Payload.ReceiptInfo.DisclosureTexts createAmendValidationResponsePayloadReceiptInfoDisclosureTexts() {
        return new AmendValidationResponse.Payload.ReceiptInfo.DisclosureTexts();
    }

    /**
     * Create an instance of {@link DoddFrankStateRegulatorInfoResponse.Payload }
     * 
     */
    public DoddFrankStateRegulatorInfoResponse.Payload createDoddFrankStateRegulatorInfoResponsePayload() {
        return new DoddFrankStateRegulatorInfoResponse.Payload();
    }

    /**
     * Create an instance of {@link MoneyGramReceiveDetailInfo }
     * 
     */
    public MoneyGramReceiveDetailInfo createMoneyGramReceiveDetailInfo() {
        return new MoneyGramReceiveDetailInfo();
    }

    /**
     * Create an instance of {@link MoneyOrderTotalResponse }
     * 
     */
    public MoneyOrderTotalResponse createMoneyOrderTotalResponse() {
        return new MoneyOrderTotalResponse();
    }

    /**
     * Create an instance of {@link GetBankDetailsByLevelResponse.Payload }
     * 
     */
    public GetBankDetailsByLevelResponse.Payload createGetBankDetailsByLevelResponsePayload() {
        return new GetBankDetailsByLevelResponse.Payload();
    }

    /**
     * Create an instance of {@link SendReversalValidationRequest }
     * 
     */
    public SendReversalValidationRequest createSendReversalValidationRequest() {
        return new SendReversalValidationRequest();
    }

    /**
     * Create an instance of {@link PromotionLookupByCodeResponse }
     * 
     */
    public PromotionLookupByCodeResponse createPromotionLookupByCodeResponse() {
        return new PromotionLookupByCodeResponse();
    }

    /**
     * Create an instance of {@link CityListResponse.Payload }
     * 
     */
    public CityListResponse.Payload createCityListResponsePayload() {
        return new CityListResponse.Payload();
    }

    /**
     * Create an instance of {@link SendValidationResponse.Payload.ReceiptInfo.DisclosureTexts }
     * 
     */
    public SendValidationResponse.Payload.ReceiptInfo.DisclosureTexts createSendValidationResponsePayloadReceiptInfoDisclosureTexts() {
        return new SendValidationResponse.Payload.ReceiptInfo.DisclosureTexts();
    }

    /**
     * Create an instance of {@link EmployeeProfileItemType }
     * 
     */
    public EmployeeProfileItemType createEmployeeProfileItemType() {
        return new EmployeeProfileItemType();
    }

    /**
     * Create an instance of {@link GetServiceOptionsResponse.Payload }
     * 
     */
    public GetServiceOptionsResponse.Payload createGetServiceOptionsResponsePayload() {
        return new GetServiceOptionsResponse.Payload();
    }

    /**
     * Create an instance of {@link MoneyGramReceiveDetailReportResponse.Payload }
     * 
     */
    public MoneyGramReceiveDetailReportResponse.Payload createMoneyGramReceiveDetailReportResponsePayload() {
        return new MoneyGramReceiveDetailReportResponse.Payload();
    }

    /**
     * Create an instance of {@link MoneyGramReceiveDetailReportResponse }
     * 
     */
    public MoneyGramReceiveDetailReportResponse createMoneyGramReceiveDetailReportResponse() {
        return new MoneyGramReceiveDetailReportResponse();
    }

    /**
     * Create an instance of {@link ProfileResponse.Payload }
     * 
     */
    public ProfileResponse.Payload createProfileResponsePayload() {
        return new ProfileResponse.Payload();
    }

    /**
     * Create an instance of {@link CityListRequest }
     * 
     */
    public CityListRequest createCityListRequest() {
        return new CityListRequest();
    }

    /**
     * Create an instance of {@link CategoryInfo.Infos }
     * 
     */
    public CategoryInfo.Infos createCategoryInfoInfos() {
        return new CategoryInfo.Infos();
    }

    /**
     * Create an instance of {@link TransactionLookupResponse.Payload.PromotionInfos }
     * 
     */
    public TransactionLookupResponse.Payload.PromotionInfos createTransactionLookupResponsePayloadPromotionInfos() {
        return new TransactionLookupResponse.Payload.PromotionInfos();
    }

    /**
     * Create an instance of {@link BillerSearchRequest }
     * 
     */
    public BillerSearchRequest createBillerSearchRequest() {
        return new BillerSearchRequest();
    }

    /**
     * Create an instance of {@link MoneyGramSendSummaryReportResponse }
     * 
     */
    public MoneyGramSendSummaryReportResponse createMoneyGramSendSummaryReportResponse() {
        return new MoneyGramSendSummaryReportResponse();
    }

    /**
     * Create an instance of {@link GetAllFieldsResponse }
     * 
     */
    public GetAllFieldsResponse createGetAllFieldsResponse() {
        return new GetAllFieldsResponse();
    }

    /**
     * Create an instance of {@link ReceiveAmountInfo.DetailReceiveAmounts }
     * 
     */
    public ReceiveAmountInfo.DetailReceiveAmounts createReceiveAmountInfoDetailReceiveAmounts() {
        return new ReceiveAmountInfo.DetailReceiveAmounts();
    }

    /**
     * Create an instance of {@link GetProfileSenderResponse.Payload.Receivers }
     * 
     */
    public GetProfileSenderResponse.Payload.Receivers createGetProfileSenderResponsePayloadReceivers() {
        return new GetProfileSenderResponse.Payload.Receivers();
    }

    /**
     * Create an instance of {@link GetProfileConsumerResponse.Payload }
     * 
     */
    public GetProfileConsumerResponse.Payload createGetProfileConsumerResponsePayload() {
        return new GetProfileConsumerResponse.Payload();
    }

    /**
     * Create an instance of {@link SystemError }
     * 
     */
    public SystemError createSystemError() {
        return new SystemError();
    }

    /**
     * Create an instance of {@link SaveProfileRequest }
     * 
     */
    public SaveProfileRequest createSaveProfileRequest() {
        return new SaveProfileRequest();
    }

    /**
     * Create an instance of {@link MoneyGramReceiveSummaryReportResponse.Payload }
     * 
     */
    public MoneyGramReceiveSummaryReportResponse.Payload createMoneyGramReceiveSummaryReportResponsePayload() {
        return new MoneyGramReceiveSummaryReportResponse.Payload();
    }

    /**
     * Create an instance of {@link GetBankDetailsByLevelResponse }
     * 
     */
    public GetBankDetailsByLevelResponse createGetBankDetailsByLevelResponse() {
        return new GetBankDetailsByLevelResponse();
    }

    /**
     * Create an instance of {@link AmendValidationRequest.ReceiptImages }
     * 
     */
    public AmendValidationRequest.ReceiptImages createAmendValidationRequestReceiptImages() {
        return new AmendValidationRequest.ReceiptImages();
    }

    /**
     * Create an instance of {@link BPValidationRequest.PromoCodes }
     * 
     */
    public BPValidationRequest.PromoCodes createBPValidationRequestPromoCodes() {
        return new BPValidationRequest.PromoCodes();
    }

    /**
     * Create an instance of {@link SaveProfileRequest.ProductProfileItems }
     * 
     */
    public SaveProfileRequest.ProductProfileItems createSaveProfileRequestProductProfileItems() {
        return new SaveProfileRequest.ProductProfileItems();
    }

    /**
     * Create an instance of {@link GetEnumerationsRequest }
     * 
     */
    public GetEnumerationsRequest createGetEnumerationsRequest() {
        return new GetEnumerationsRequest();
    }

    /**
     * Create an instance of {@link CreateOrUpdateProfileSenderRequest }
     * 
     */
    public CreateOrUpdateProfileSenderRequest createCreateOrUpdateProfileSenderRequest() {
        return new CreateOrUpdateProfileSenderRequest();
    }

    /**
     * Create an instance of {@link SendValidationResponse.Payload.FieldsToCollect }
     * 
     */
    public SendValidationResponse.Payload.FieldsToCollect createSendValidationResponsePayloadFieldsToCollect() {
        return new SendValidationResponse.Payload.FieldsToCollect();
    }

    /**
     * Create an instance of {@link GetEnumerationsResponse.Payload.Enumerations }
     * 
     */
    public GetEnumerationsResponse.Payload.Enumerations createGetEnumerationsResponsePayloadEnumerations() {
        return new GetEnumerationsResponse.Payload.Enumerations();
    }

    /**
     * Create an instance of {@link CreateOrUpdateProfileReceiverRequest.VerifiedFields }
     * 
     */
    public CreateOrUpdateProfileReceiverRequest.VerifiedFields createCreateOrUpdateProfileReceiverRequestVerifiedFields() {
        return new CreateOrUpdateProfileReceiverRequest.VerifiedFields();
    }

    /**
     * Create an instance of {@link SearchConsumerProfilesRequest.FieldValues }
     * 
     */
    public SearchConsumerProfilesRequest.FieldValues createSearchConsumerProfilesRequestFieldValues() {
        return new SearchConsumerProfilesRequest.FieldValues();
    }

    /**
     * Create an instance of {@link GetProfileSenderRequest }
     * 
     */
    public GetProfileSenderRequest createGetProfileSenderRequest() {
        return new GetProfileSenderRequest();
    }

    /**
     * Create an instance of {@link AttributeType }
     * 
     */
    public AttributeType createAttributeType() {
        return new AttributeType();
    }

    /**
     * Create an instance of {@link PromotionLookupInfo }
     * 
     */
    public PromotionLookupInfo createPromotionLookupInfo() {
        return new PromotionLookupInfo();
    }

    /**
     * Create an instance of {@link FeeLookupResponse }
     * 
     */
    public FeeLookupResponse createFeeLookupResponse() {
        return new FeeLookupResponse();
    }

    /**
     * Create an instance of {@link GetEnumerationsResponse }
     * 
     */
    public GetEnumerationsResponse createGetEnumerationsResponse() {
        return new GetEnumerationsResponse();
    }

    /**
     * Create an instance of {@link ProductProfileItemType }
     * 
     */
    public ProductProfileItemType createProductProfileItemType() {
        return new ProductProfileItemType();
    }

    /**
     * Create an instance of {@link ProfileResponse }
     * 
     */
    public ProfileResponse createProfileResponse() {
        return new ProfileResponse();
    }

    /**
     * Create an instance of {@link ReceiverLookupInfo }
     * 
     */
    public ReceiverLookupInfo createReceiverLookupInfo() {
        return new ReceiverLookupInfo();
    }

    /**
     * Create an instance of {@link BillPaymentSummaryReportRequest }
     * 
     */
    public BillPaymentSummaryReportRequest createBillPaymentSummaryReportRequest() {
        return new BillPaymentSummaryReportRequest();
    }

    /**
     * Create an instance of {@link DynamicContentAssemblyDetailsType }
     * 
     */
    public DynamicContentAssemblyDetailsType createDynamicContentAssemblyDetailsType() {
        return new DynamicContentAssemblyDetailsType();
    }

    /**
     * Create an instance of {@link GetCurrencyInfoRequest }
     * 
     */
    public GetCurrencyInfoRequest createGetCurrencyInfoRequest() {
        return new GetCurrencyInfoRequest();
    }

    /**
     * Create an instance of {@link DepositAnnouncementResponse.Payload }
     * 
     */
    public DepositAnnouncementResponse.Payload createDepositAnnouncementResponsePayload() {
        return new DepositAnnouncementResponse.Payload();
    }

    /**
     * Create an instance of {@link DoddFrankStateRegulatorInfoRequest.Languages }
     * 
     */
    public DoddFrankStateRegulatorInfoRequest.Languages createDoddFrankStateRegulatorInfoRequestLanguages() {
        return new DoddFrankStateRegulatorInfoRequest.Languages();
    }

    /**
     * Create an instance of {@link GetAllErrorsResponse.Payload }
     * 
     */
    public GetAllErrorsResponse.Payload createGetAllErrorsResponsePayload() {
        return new GetAllErrorsResponse.Payload();
    }

    /**
     * Create an instance of {@link FeeLookupBySendCountryRequest }
     * 
     */
    public FeeLookupBySendCountryRequest createFeeLookupBySendCountryRequest() {
        return new FeeLookupBySendCountryRequest();
    }

    /**
     * Create an instance of {@link ComplianceTransactionResponse.Payload }
     * 
     */
    public ComplianceTransactionResponse.Payload createComplianceTransactionResponsePayload() {
        return new ComplianceTransactionResponse.Payload();
    }

    /**
     * Create an instance of {@link ReceiptFormatDetailsType }
     * 
     */
    public ReceiptFormatDetailsType createReceiptFormatDetailsType() {
        return new ReceiptFormatDetailsType();
    }

    /**
     * Create an instance of {@link EnumeratedIdentifierInfo }
     * 
     */
    public EnumeratedIdentifierInfo createEnumeratedIdentifierInfo() {
        return new EnumeratedIdentifierInfo();
    }

    /**
     * Create an instance of {@link SendAmountInfo.DetailSendAmounts }
     * 
     */
    public SendAmountInfo.DetailSendAmounts createSendAmountInfoDetailSendAmounts() {
        return new SendAmountInfo.DetailSendAmounts();
    }

    /**
     * Create an instance of {@link SearchConsumerProfilesResponse.Payload.FieldsToCollect }
     * 
     */
    public SearchConsumerProfilesResponse.Payload.FieldsToCollect createSearchConsumerProfilesResponsePayloadFieldsToCollect() {
        return new SearchConsumerProfilesResponse.Payload.FieldsToCollect();
    }

    /**
     * Create an instance of {@link BPValidationResponse.Payload.ReceiptInfo.CustomerTips }
     * 
     */
    public BPValidationResponse.Payload.ReceiptInfo.CustomerTips createBPValidationResponsePayloadReceiptInfoCustomerTips() {
        return new BPValidationResponse.Payload.ReceiptInfo.CustomerTips();
    }

    /**
     * Create an instance of {@link FeeLookupBySendCountryResponse.Payload }
     * 
     */
    public FeeLookupBySendCountryResponse.Payload createFeeLookupBySendCountryResponsePayload() {
        return new FeeLookupBySendCountryResponse.Payload();
    }

    /**
     * Create an instance of {@link RawAddressType }
     * 
     */
    public RawAddressType createRawAddressType() {
        return new RawAddressType();
    }

    /**
     * Create an instance of {@link GetBroadcastMessagesResponse.Payload }
     * 
     */
    public GetBroadcastMessagesResponse.Payload createGetBroadcastMessagesResponsePayload() {
        return new GetBroadcastMessagesResponse.Payload();
    }

    /**
     * Create an instance of {@link EstimatedReceiveAmountInfo }
     * 
     */
    public EstimatedReceiveAmountInfo createEstimatedReceiveAmountInfo() {
        return new EstimatedReceiveAmountInfo();
    }

    /**
     * Create an instance of {@link DisclosureTextDetailsRequest.DcaTokenValues }
     * 
     */
    public DisclosureTextDetailsRequest.DcaTokenValues createDisclosureTextDetailsRequestDcaTokenValues() {
        return new DisclosureTextDetailsRequest.DcaTokenValues();
    }

    /**
     * Create an instance of {@link UCPRequestPhoneType }
     * 
     */
    public UCPRequestPhoneType createUCPRequestPhoneType() {
        return new UCPRequestPhoneType();
    }

    /**
     * Create an instance of {@link DwPasswordRequest }
     * 
     */
    public DwPasswordRequest createDwPasswordRequest() {
        return new DwPasswordRequest();
    }

    /**
     * Create an instance of {@link UCPResponsePhoneType }
     * 
     */
    public UCPResponsePhoneType createUCPResponsePhoneType() {
        return new UCPResponsePhoneType();
    }

    /**
     * Create an instance of {@link LanguageTranslationType }
     * 
     */
    public LanguageTranslationType createLanguageTranslationType() {
        return new LanguageTranslationType();
    }

    /**
     * Create an instance of {@link ReceiveValidationRequest.VerifiedFields }
     * 
     */
    public ReceiveValidationRequest.VerifiedFields createReceiveValidationRequestVerifiedFields() {
        return new ReceiveValidationRequest.VerifiedFields();
    }

    /**
     * Create an instance of {@link VariableReceiptInfoResponse }
     * 
     */
    public VariableReceiptInfoResponse createVariableReceiptInfoResponse() {
        return new VariableReceiptInfoResponse();
    }

    /**
     * Create an instance of {@link UCPResponseEmailType }
     * 
     */
    public UCPResponseEmailType createUCPResponseEmailType() {
        return new UCPResponseEmailType();
    }

    /**
     * Create an instance of {@link BPValidationRequest.VerifiedFields }
     * 
     */
    public BPValidationRequest.VerifiedFields createBPValidationRequestVerifiedFields() {
        return new BPValidationRequest.VerifiedFields();
    }

    /**
     * Create an instance of {@link GetUCPByConsumerAttributesRequest }
     * 
     */
    public GetUCPByConsumerAttributesRequest createGetUCPByConsumerAttributesRequest() {
        return new GetUCPByConsumerAttributesRequest();
    }

    /**
     * Create an instance of {@link ComplianceTransactionResponse }
     * 
     */
    public ComplianceTransactionResponse createComplianceTransactionResponse() {
        return new ComplianceTransactionResponse();
    }

    /**
     * Create an instance of {@link FeeInfo.PromotionInfos }
     * 
     */
    public FeeInfo.PromotionInfos createFeeInfoPromotionInfos() {
        return new FeeInfo.PromotionInfos();
    }

    /**
     * Create an instance of {@link EstimatedReceiveAmountInfo.DetailEstimatedReceiveAmounts }
     * 
     */
    public EstimatedReceiveAmountInfo.DetailEstimatedReceiveAmounts createEstimatedReceiveAmountInfoDetailEstimatedReceiveAmounts() {
        return new EstimatedReceiveAmountInfo.DetailEstimatedReceiveAmounts();
    }

    /**
     * Create an instance of {@link CurrencyInfo }
     * 
     */
    public CurrencyInfo createCurrencyInfo() {
        return new CurrencyInfo();
    }

    /**
     * Create an instance of {@link GetCountryInfoResponse.Payload }
     * 
     */
    public GetCountryInfoResponse.Payload createGetCountryInfoResponsePayload() {
        return new GetCountryInfoResponse.Payload();
    }

    /**
     * Create an instance of {@link BillerSearchResponse }
     * 
     */
    public BillerSearchResponse createBillerSearchResponse() {
        return new BillerSearchResponse();
    }

    /**
     * Create an instance of {@link SendReversalValidationResponse.Payload.ReceiptInfo }
     * 
     */
    public SendReversalValidationResponse.Payload.ReceiptInfo createSendReversalValidationResponsePayloadReceiptInfo() {
        return new SendReversalValidationResponse.Payload.ReceiptInfo();
    }

    /**
     * Create an instance of {@link GetProfileReceiverRequest }
     * 
     */
    public GetProfileReceiverRequest createGetProfileReceiverRequest() {
        return new GetProfileReceiverRequest();
    }

    /**
     * Create an instance of {@link CurrencyTranslationType }
     * 
     */
    public CurrencyTranslationType createCurrencyTranslationType() {
        return new CurrencyTranslationType();
    }

    /**
     * Create an instance of {@link LoyaltyTranslationType }
     * 
     */
    public LoyaltyTranslationType createLoyaltyTranslationType() {
        return new LoyaltyTranslationType();
    }

    /**
     * Create an instance of {@link RedirectInfo }
     * 
     */
    public RedirectInfo createRedirectInfo() {
        return new RedirectInfo();
    }

    /**
     * Create an instance of {@link CountryInfo.ReceiveCurrencies }
     * 
     */
    public CountryInfo.ReceiveCurrencies createCountryInfoReceiveCurrencies() {
        return new CountryInfo.ReceiveCurrencies();
    }

    /**
     * Create an instance of {@link ReceiveReversalValidationRequest.ReceiptImages }
     * 
     */
    public ReceiveReversalValidationRequest.ReceiptImages createReceiveReversalValidationRequestReceiptImages() {
        return new ReceiveReversalValidationRequest.ReceiptImages();
    }

    /**
     * Create an instance of {@link ReceiptsFormatDetailsRequest.ReceiptTypeValues }
     * 
     */
    public ReceiptsFormatDetailsRequest.ReceiptTypeValues createReceiptsFormatDetailsRequestReceiptTypeValues() {
        return new ReceiptsFormatDetailsRequest.ReceiptTypeValues();
    }

    /**
     * Create an instance of {@link DirectoryOfAgentsByZipResponse }
     * 
     */
    public DirectoryOfAgentsByZipResponse createDirectoryOfAgentsByZipResponse() {
        return new DirectoryOfAgentsByZipResponse();
    }

    /**
     * Create an instance of {@link ProfileChangeResponse.Payload }
     * 
     */
    public ProfileChangeResponse.Payload createProfileChangeResponsePayload() {
        return new ProfileChangeResponse.Payload();
    }

    /**
     * Create an instance of {@link BPValidationRequest }
     * 
     */
    public BPValidationRequest createBPValidationRequest() {
        return new BPValidationRequest();
    }

    /**
     * Create an instance of {@link SaveConsumerProfileImageRequest.FieldValues }
     * 
     */
    public SaveConsumerProfileImageRequest.FieldValues createSaveConsumerProfileImageRequestFieldValues() {
        return new SaveConsumerProfileImageRequest.FieldValues();
    }

    /**
     * Create an instance of {@link SendReversalValidationRequest.ReceiptImages }
     * 
     */
    public SendReversalValidationRequest.ReceiptImages createSendReversalValidationRequestReceiptImages() {
        return new SendReversalValidationRequest.ReceiptImages();
    }

    /**
     * Create an instance of {@link SendValidationResponse.Payload }
     * 
     */
    public SendValidationResponse.Payload createSendValidationResponsePayload() {
        return new SendValidationResponse.Payload();
    }

    /**
     * Create an instance of {@link ProfileChangeRequest }
     * 
     */
    public ProfileChangeRequest createProfileChangeRequest() {
        return new ProfileChangeRequest();
    }

    /**
     * Create an instance of {@link ErrorInfo }
     * 
     */
    public ErrorInfo createErrorInfo() {
        return new ErrorInfo();
    }

    /**
     * Create an instance of {@link DoddFrankStateRegulatorInfoRequest }
     * 
     */
    public DoddFrankStateRegulatorInfoRequest createDoddFrankStateRegulatorInfoRequest() {
        return new DoddFrankStateRegulatorInfoRequest();
    }

    /**
     * Create an instance of {@link ReceiveValidationResponse }
     * 
     */
    public ReceiveValidationResponse createReceiveValidationResponse() {
        return new ReceiveValidationResponse();
    }

    /**
     * Create an instance of {@link OpenOTPLoginRequest }
     * 
     */
    public OpenOTPLoginRequest createOpenOTPLoginRequest() {
        return new OpenOTPLoginRequest();
    }

    /**
     * Create an instance of {@link DoddFrankStateRegulatorInfoResponse }
     * 
     */
    public DoddFrankStateRegulatorInfoResponse createDoddFrankStateRegulatorInfoResponse() {
        return new DoddFrankStateRegulatorInfoResponse();
    }

    /**
     * Create an instance of {@link StagedTransactionInfo }
     * 
     */
    public StagedTransactionInfo createStagedTransactionInfo() {
        return new StagedTransactionInfo();
    }

    /**
     * Create an instance of {@link CreateOrUpdateProfileConsumerResponse.Payload }
     * 
     */
    public CreateOrUpdateProfileConsumerResponse.Payload createCreateOrUpdateProfileConsumerResponsePayload() {
        return new CreateOrUpdateProfileConsumerResponse.Payload();
    }

    /**
     * Create an instance of {@link StandardizedAddressType }
     * 
     */
    public StandardizedAddressType createStandardizedAddressType() {
        return new StandardizedAddressType();
    }

    /**
     * Create an instance of {@link DisclosureTextDetailsRequest.DcaTextTagValues }
     * 
     */
    public DisclosureTextDetailsRequest.DcaTextTagValues createDisclosureTextDetailsRequestDcaTextTagValues() {
        return new DisclosureTextDetailsRequest.DcaTextTagValues();
    }

    /**
     * Create an instance of {@link CompleteSessionRequest }
     * 
     */
    public CompleteSessionRequest createCompleteSessionRequest() {
        return new CompleteSessionRequest();
    }

    /**
     * Create an instance of {@link BillerSearchResponse.Payload }
     * 
     */
    public BillerSearchResponse.Payload createBillerSearchResponsePayload() {
        return new BillerSearchResponse.Payload();
    }

    /**
     * Create an instance of {@link ConsumerProfileImageContentType }
     * 
     */
    public ConsumerProfileImageContentType createConsumerProfileImageContentType() {
        return new ConsumerProfileImageContentType();
    }

    /**
     * Create an instance of {@link DirectoryOfAgentsByZipRequest }
     * 
     */
    public DirectoryOfAgentsByZipRequest createDirectoryOfAgentsByZipRequest() {
        return new DirectoryOfAgentsByZipRequest();
    }

    /**
     * Create an instance of {@link SearchStagedTransactionsResponse }
     * 
     */
    public SearchStagedTransactionsResponse createSearchStagedTransactionsResponse() {
        return new SearchStagedTransactionsResponse();
    }

    /**
     * Create an instance of {@link GetBroadcastMessagesRequest.MessageInfos }
     * 
     */
    public GetBroadcastMessagesRequest.MessageInfos createGetBroadcastMessagesRequestMessageInfos() {
        return new GetBroadcastMessagesRequest.MessageInfos();
    }

    /**
     * Create an instance of {@link InitialSetupRequest }
     * 
     */
    public InitialSetupRequest createInitialSetupRequest() {
        return new InitialSetupRequest();
    }

    /**
     * Create an instance of {@link SaveSubagentsRequest.SubagentProfileUpdates }
     * 
     */
    public SaveSubagentsRequest.SubagentProfileUpdates createSaveSubagentsRequestSubagentProfileUpdates() {
        return new SaveSubagentsRequest.SubagentProfileUpdates();
    }

    /**
     * Create an instance of {@link SubagentsResponse.Payload.Subagents }
     * 
     */
    public SubagentsResponse.Payload.Subagents createSubagentsResponsePayloadSubagents() {
        return new SubagentsResponse.Payload.Subagents();
    }

    /**
     * Create an instance of {@link DisclosureTextDetailsResponse.Payload }
     * 
     */
    public DisclosureTextDetailsResponse.Payload createDisclosureTextDetailsResponsePayload() {
        return new DisclosureTextDetailsResponse.Payload();
    }

    /**
     * Create an instance of {@link GetCountryInfoResponse.Payload.CountryInfos }
     * 
     */
    public GetCountryInfoResponse.Payload.CountryInfos createGetCountryInfoResponsePayloadCountryInfos() {
        return new GetCountryInfoResponse.Payload.CountryInfos();
    }

    /**
     * Create an instance of {@link VersionInfo }
     * 
     */
    public VersionInfo createVersionInfo() {
        return new VersionInfo();
    }

    /**
     * Create an instance of {@link ReceiptType.AdditionalLanguages }
     * 
     */
    public ReceiptType.AdditionalLanguages createReceiptTypeAdditionalLanguages() {
        return new ReceiptType.AdditionalLanguages();
    }

    /**
     * Create an instance of {@link GenericBusinessError }
     * 
     */
    public GenericBusinessError createGenericBusinessError() {
        return new GenericBusinessError();
    }

    /**
     * Create an instance of {@link ReceiptsFormatDetailsRequest }
     * 
     */
    public ReceiptsFormatDetailsRequest createReceiptsFormatDetailsRequest() {
        return new ReceiptsFormatDetailsRequest();
    }

    /**
     * Create an instance of {@link CountryTranslationType }
     * 
     */
    public CountryTranslationType createCountryTranslationType() {
        return new CountryTranslationType();
    }

    /**
     * Create an instance of {@link ProfileItemType }
     * 
     */
    public ProfileItemType createProfileItemType() {
        return new ProfileItemType();
    }

    /**
     * Create an instance of {@link SaveDebugDataResponse.Payload }
     * 
     */
    public SaveDebugDataResponse.Payload createSaveDebugDataResponsePayload() {
        return new SaveDebugDataResponse.Payload();
    }

    /**
     * Create an instance of {@link GetBroadcastMessagesRequest.CustomFields }
     * 
     */
    public GetBroadcastMessagesRequest.CustomFields createGetBroadcastMessagesRequestCustomFields() {
        return new GetBroadcastMessagesRequest.CustomFields();
    }

    /**
     * Create an instance of {@link CountrySubdivisionInfo.Subdivisions }
     * 
     */
    public CountrySubdivisionInfo.Subdivisions createCountrySubdivisionInfoSubdivisions() {
        return new CountrySubdivisionInfo.Subdivisions();
    }

    /**
     * Create an instance of {@link OpenOTPLoginResponse }
     * 
     */
    public OpenOTPLoginResponse createOpenOTPLoginResponse() {
        return new OpenOTPLoginResponse();
    }

    /**
     * Create an instance of {@link SenderLookupInfo.ReceiverInfos }
     * 
     */
    public SenderLookupInfo.ReceiverInfos createSenderLookupInfoReceiverInfos() {
        return new SenderLookupInfo.ReceiverInfos();
    }

    /**
     * Create an instance of {@link MoneyGramSendDetailReportWithTaxResponse }
     * 
     */
    public MoneyGramSendDetailReportWithTaxResponse createMoneyGramSendDetailReportWithTaxResponse() {
        return new MoneyGramSendDetailReportWithTaxResponse();
    }

    /**
     * Create an instance of {@link DeliveryOptionTranslationType }
     * 
     */
    public DeliveryOptionTranslationType createDeliveryOptionTranslationType() {
        return new DeliveryOptionTranslationType();
    }

    /**
     * Create an instance of {@link BPValidationResponse.Payload.PromotionInfos }
     * 
     */
    public BPValidationResponse.Payload.PromotionInfos createBPValidationResponsePayloadPromotionInfos() {
        return new BPValidationResponse.Payload.PromotionInfos();
    }

    /**
     * Create an instance of {@link ReceiptResponseType }
     * 
     */
    public ReceiptResponseType createReceiptResponseType() {
        return new ReceiptResponseType();
    }

    /**
     * Create an instance of {@link SendValidationRequest.VerifiedFields }
     * 
     */
    public SendValidationRequest.VerifiedFields createSendValidationRequestVerifiedFields() {
        return new SendValidationRequest.VerifiedFields();
    }

    /**
     * Create an instance of {@link VersionManifestRequest }
     * 
     */
    public VersionManifestRequest createVersionManifestRequest() {
        return new VersionManifestRequest();
    }

    /**
     * Create an instance of {@link GetCountryInfoRequest }
     * 
     */
    public GetCountryInfoRequest createGetCountryInfoRequest() {
        return new GetCountryInfoRequest();
    }

    /**
     * Create an instance of {@link Response }
     * 
     */
    public Response createResponse() {
        return new Response();
    }

    /**
     * Create an instance of {@link CreateOrUpdateProfileSenderResponse.Payload }
     * 
     */
    public CreateOrUpdateProfileSenderResponse.Payload createCreateOrUpdateProfileSenderResponsePayload() {
        return new CreateOrUpdateProfileSenderResponse.Payload();
    }

    /**
     * Create an instance of {@link ReceiptFormatDetailsType.AdditionalLanguages }
     * 
     */
    public ReceiptFormatDetailsType.AdditionalLanguages createReceiptFormatDetailsTypeAdditionalLanguages() {
        return new ReceiptFormatDetailsType.AdditionalLanguages();
    }

    /**
     * Create an instance of {@link Request }
     * 
     */
    public Request createRequest() {
        return new Request();
    }

    /**
     * Create an instance of {@link DestinationCountryInfo }
     * 
     */
    public DestinationCountryInfo createDestinationCountryInfo() {
        return new DestinationCountryInfo();
    }

    /**
     * Create an instance of {@link ConsumerProfileSearchInfo }
     * 
     */
    public ConsumerProfileSearchInfo createConsumerProfileSearchInfo() {
        return new ConsumerProfileSearchInfo();
    }

    /**
     * Create an instance of {@link GetServiceOptionsResponse }
     * 
     */
    public GetServiceOptionsResponse createGetServiceOptionsResponse() {
        return new GetServiceOptionsResponse();
    }

    /**
     * Create an instance of {@link GetCountrySubdivisionResponse.Payload.CountrySubdivisionInfos }
     * 
     */
    public GetCountrySubdivisionResponse.Payload.CountrySubdivisionInfos createGetCountrySubdivisionResponsePayloadCountrySubdivisionInfos() {
        return new GetCountrySubdivisionResponse.Payload.CountrySubdivisionInfos();
    }

    /**
     * Create an instance of {@link SubagentsResponse }
     * 
     */
    public SubagentsResponse createSubagentsResponse() {
        return new SubagentsResponse();
    }

    /**
     * Create an instance of {@link GetReceiptForReprintResponse }
     * 
     */
    public GetReceiptForReprintResponse createGetReceiptForReprintResponse() {
        return new GetReceiptForReprintResponse();
    }

    /**
     * Create an instance of {@link InjectedInstructionResponse }
     * 
     */
    public InjectedInstructionResponse createInjectedInstructionResponse() {
        return new InjectedInstructionResponse();
    }

    /**
     * Create an instance of {@link GetAllFieldsRequest }
     * 
     */
    public GetAllFieldsRequest createGetAllFieldsRequest() {
        return new GetAllFieldsRequest();
    }

    /**
     * Create an instance of {@link AmendValidationResponse.Payload }
     * 
     */
    public AmendValidationResponse.Payload createAmendValidationResponsePayload() {
        return new AmendValidationResponse.Payload();
    }

    /**
     * Create an instance of {@link BaseRawAddressType }
     * 
     */
    public BaseRawAddressType createBaseRawAddressType() {
        return new BaseRawAddressType();
    }

    /**
     * Create an instance of {@link ReceiveReversalValidationResponse.Payload.ReceiptInfo.DisclosureTexts }
     * 
     */
    public ReceiveReversalValidationResponse.Payload.ReceiptInfo.DisclosureTexts createReceiveReversalValidationResponsePayloadReceiptInfoDisclosureTexts() {
        return new ReceiveReversalValidationResponse.Payload.ReceiptInfo.DisclosureTexts();
    }

    /**
     * Create an instance of {@link BPValidationResponse }
     * 
     */
    public BPValidationResponse createBPValidationResponse() {
        return new BPValidationResponse();
    }

    /**
     * Create an instance of {@link ErrorInfo.ErrorMessages }
     * 
     */
    public ErrorInfo.ErrorMessages createErrorInfoErrorMessages() {
        return new ErrorInfo.ErrorMessages();
    }

    /**
     * Create an instance of {@link SenderLookupInfo }
     * 
     */
    public SenderLookupInfo createSenderLookupInfo() {
        return new SenderLookupInfo();
    }

    /**
     * Create an instance of {@link GetBankDetailsRequest }
     * 
     */
    public GetBankDetailsRequest createGetBankDetailsRequest() {
        return new GetBankDetailsRequest();
    }

    /**
     * Create an instance of {@link BusinessError }
     * 
     */
    public BusinessError createBusinessError() {
        return new BusinessError();
    }

    /**
     * Create an instance of {@link SubagentProfileUpdateType }
     * 
     */
    public SubagentProfileUpdateType createSubagentProfileUpdateType() {
        return new SubagentProfileUpdateType();
    }

    /**
     * Create an instance of {@link MoneyGramSendDetailReportResponse }
     * 
     */
    public MoneyGramSendDetailReportResponse createMoneyGramSendDetailReportResponse() {
        return new MoneyGramSendDetailReportResponse();
    }

    /**
     * Create an instance of {@link UCPResponseProfileType }
     * 
     */
    public UCPResponseProfileType createUCPResponseProfileType() {
        return new UCPResponseProfileType();
    }

    /**
     * Create an instance of {@link LoyaltyProgramTypeTranslationType }
     * 
     */
    public LoyaltyProgramTypeTranslationType createLoyaltyProgramTypeTranslationType() {
        return new LoyaltyProgramTypeTranslationType();
    }

    /**
     * Create an instance of {@link SearchConsumerProfilesResponse.Payload }
     * 
     */
    public SearchConsumerProfilesResponse.Payload createSearchConsumerProfilesResponsePayload() {
        return new SearchConsumerProfilesResponse.Payload();
    }

    /**
     * Create an instance of {@link GetAllFieldsResponse.Payload }
     * 
     */
    public GetAllFieldsResponse.Payload createGetAllFieldsResponsePayload() {
        return new GetAllFieldsResponse.Payload();
    }

    /**
     * Create an instance of {@link LevelInfo }
     * 
     */
    public LevelInfo createLevelInfo() {
        return new LevelInfo();
    }

    /**
     * Create an instance of {@link DwProfileRequest }
     * 
     */
    public DwProfileRequest createDwProfileRequest() {
        return new DwProfileRequest();
    }

    /**
     * Create an instance of {@link CountryInfo.CountryDialCodes }
     * 
     */
    public CountryInfo.CountryDialCodes createCountryInfoCountryDialCodes() {
        return new CountryInfo.CountryDialCodes();
    }

    /**
     * Create an instance of {@link PreCompletionReceiptType }
     * 
     */
    public PreCompletionReceiptType createPreCompletionReceiptType() {
        return new PreCompletionReceiptType();
    }

    /**
     * Create an instance of {@link ConsumerHistoryLookupRequest }
     * 
     */
    public ConsumerHistoryLookupRequest createConsumerHistoryLookupRequest() {
        return new ConsumerHistoryLookupRequest();
    }

    /**
     * Create an instance of {@link GetDepositInformationResponse.Payload }
     * 
     */
    public GetDepositInformationResponse.Payload createGetDepositInformationResponsePayload() {
        return new GetDepositInformationResponse.Payload();
    }

    /**
     * Create an instance of {@link FeeLookupResponse.Payload }
     * 
     */
    public FeeLookupResponse.Payload createFeeLookupResponsePayload() {
        return new FeeLookupResponse.Payload();
    }

    /**
     * Create an instance of {@link PromotionInfo }
     * 
     */
    public PromotionInfo createPromotionInfo() {
        return new PromotionInfo();
    }

    /**
     * Create an instance of {@link IdentificationDocumentType }
     * 
     */
    public IdentificationDocumentType createIdentificationDocumentType() {
        return new IdentificationDocumentType();
    }

    /**
     * Create an instance of {@link GetUCPByConsumerAttributesResponse.Payload }
     * 
     */
    public GetUCPByConsumerAttributesResponse.Payload createGetUCPByConsumerAttributesResponsePayload() {
        return new GetUCPByConsumerAttributesResponse.Payload();
    }

    /**
     * Create an instance of {@link BillPaymentDetailReportResponse.Payload }
     * 
     */
    public BillPaymentDetailReportResponse.Payload createBillPaymentDetailReportResponsePayload() {
        return new BillPaymentDetailReportResponse.Payload();
    }

    /**
     * Create an instance of {@link CountrySubdivisionInfo }
     * 
     */
    public CountrySubdivisionInfo createCountrySubdivisionInfo() {
        return new CountrySubdivisionInfo();
    }

    /**
     * Create an instance of {@link CurrentValuesType }
     * 
     */
    public CurrentValuesType createCurrentValuesType() {
        return new CurrentValuesType();
    }

    /**
     * Create an instance of {@link RegisterHardTokenResponse }
     * 
     */
    public RegisterHardTokenResponse createRegisterHardTokenResponse() {
        return new RegisterHardTokenResponse();
    }

    /**
     * Create an instance of {@link TransactionLookupResponse }
     * 
     */
    public TransactionLookupResponse createTransactionLookupResponse() {
        return new TransactionLookupResponse();
    }

    /**
     * Create an instance of {@link MoneyGramSendDetailReportWithTaxResponse.Payload }
     * 
     */
    public MoneyGramSendDetailReportWithTaxResponse.Payload createMoneyGramSendDetailReportWithTaxResponsePayload() {
        return new MoneyGramSendDetailReportWithTaxResponse.Payload();
    }

    /**
     * Create an instance of {@link DisclosureTextDetailsRequest.Languages }
     * 
     */
    public DisclosureTextDetailsRequest.Languages createDisclosureTextDetailsRequestLanguages() {
        return new DisclosureTextDetailsRequest.Languages();
    }

    /**
     * Create an instance of {@link PropertyType }
     * 
     */
    public PropertyType createPropertyType() {
        return new PropertyType();
    }

    /**
     * Create an instance of {@link Request.PoeCapabilities }
     * 
     */
    public Request.PoeCapabilities createRequestPoeCapabilities() {
        return new Request.PoeCapabilities();
    }

    /**
     * Create an instance of {@link SaveConsumerProfileImageResponse }
     * 
     */
    public SaveConsumerProfileImageResponse createSaveConsumerProfileImageResponse() {
        return new SaveConsumerProfileImageResponse();
    }

    /**
     * Create an instance of {@link CountryInfo.LookupKeys }
     * 
     */
    public CountryInfo.LookupKeys createCountryInfoLookupKeys() {
        return new CountryInfo.LookupKeys();
    }

    /**
     * Create an instance of {@link GetCountrySubdivisionRequest }
     * 
     */
    public GetCountrySubdivisionRequest createGetCountrySubdivisionRequest() {
        return new GetCountrySubdivisionRequest();
    }

    /**
     * Create an instance of {@link GetCurrencyInfoResponse.Payload }
     * 
     */
    public GetCurrencyInfoResponse.Payload createGetCurrencyInfoResponsePayload() {
        return new GetCurrencyInfoResponse.Payload();
    }

    /**
     * Create an instance of {@link FeeLookupRequest }
     * 
     */
    public FeeLookupRequest createFeeLookupRequest() {
        return new FeeLookupRequest();
    }

    /**
     * Create an instance of {@link GetBankDetailsByLevelRequest }
     * 
     */
    public GetBankDetailsByLevelRequest createGetBankDetailsByLevelRequest() {
        return new GetBankDetailsByLevelRequest();
    }

    /**
     * Create an instance of {@link DirectoryOfAgentsByCityRequest }
     * 
     */
    public DirectoryOfAgentsByCityRequest createDirectoryOfAgentsByCityRequest() {
        return new DirectoryOfAgentsByCityRequest();
    }

    /**
     * Create an instance of {@link GetServiceOptionsResponse.Payload.DestinationCountryInfos }
     * 
     */
    public GetServiceOptionsResponse.Payload.DestinationCountryInfos createGetServiceOptionsResponsePayloadDestinationCountryInfos() {
        return new GetServiceOptionsResponse.Payload.DestinationCountryInfos();
    }

    /**
     * Create an instance of {@link BPValidationResponse.Payload.ReceiptInfo.DisclosureTexts }
     * 
     */
    public BPValidationResponse.Payload.ReceiptInfo.DisclosureTexts createBPValidationResponsePayloadReceiptInfoDisclosureTexts() {
        return new BPValidationResponse.Payload.ReceiptInfo.DisclosureTexts();
    }

    /**
     * Create an instance of {@link MoneyOrderTotalResponse.Payload }
     * 
     */
    public MoneyOrderTotalResponse.Payload createMoneyOrderTotalResponsePayload() {
        return new MoneyOrderTotalResponse.Payload();
    }

    /**
     * Create an instance of {@link ReceiveValidationRequest }
     * 
     */
    public ReceiveValidationRequest createReceiveValidationRequest() {
        return new ReceiveValidationRequest();
    }

    /**
     * Create an instance of {@link CustomFieldsType }
     * 
     */
    public CustomFieldsType createCustomFieldsType() {
        return new CustomFieldsType();
    }

    /**
     * Create an instance of {@link HierarchyLevelInfo }
     * 
     */
    public HierarchyLevelInfo createHierarchyLevelInfo() {
        return new HierarchyLevelInfo();
    }

    /**
     * Create an instance of {@link TransactionLookupResponse.Payload.DisclosureTexts }
     * 
     */
    public TransactionLookupResponse.Payload.DisclosureTexts createTransactionLookupResponsePayloadDisclosureTexts() {
        return new TransactionLookupResponse.Payload.DisclosureTexts();
    }

    /**
     * Create an instance of {@link MoneyGramSendDetailReportRequest }
     * 
     */
    public MoneyGramSendDetailReportRequest createMoneyGramSendDetailReportRequest() {
        return new MoneyGramSendDetailReportRequest();
    }

    /**
     * Create an instance of {@link BPValidationResponse.Payload }
     * 
     */
    public BPValidationResponse.Payload createBPValidationResponsePayload() {
        return new BPValidationResponse.Payload();
    }

    /**
     * Create an instance of {@link CategoryInfo }
     * 
     */
    public CategoryInfo createCategoryInfo() {
        return new CategoryInfo();
    }

    /**
     * Create an instance of {@link FeeLookupResponse.Payload.FeeInfos }
     * 
     */
    public FeeLookupResponse.Payload.FeeInfos createFeeLookupResponsePayloadFeeInfos() {
        return new FeeLookupResponse.Payload.FeeInfos();
    }

    /**
     * Create an instance of {@link SendReversalValidationRequest.FieldValues }
     * 
     */
    public SendReversalValidationRequest.FieldValues createSendReversalValidationRequestFieldValues() {
        return new SendReversalValidationRequest.FieldValues();
    }

    /**
     * Create an instance of {@link NotificationPreferenceType }
     * 
     */
    public NotificationPreferenceType createNotificationPreferenceType() {
        return new NotificationPreferenceType();
    }

    /**
     * Create an instance of {@link CountryInfo.SendCurrencies }
     * 
     */
    public CountryInfo.SendCurrencies createCountryInfoSendCurrencies() {
        return new CountryInfo.SendCurrencies();
    }

    /**
     * Create an instance of {@link AmendValidationResponse }
     * 
     */
    public AmendValidationResponse createAmendValidationResponse() {
        return new AmendValidationResponse();
    }

    /**
     * Create an instance of {@link CreateOrUpdateProfileSenderResponse.Payload.ConsumerProfileIDs }
     * 
     */
    public CreateOrUpdateProfileSenderResponse.Payload.ConsumerProfileIDs createCreateOrUpdateProfileSenderResponsePayloadConsumerProfileIDs() {
        return new CreateOrUpdateProfileSenderResponse.Payload.ConsumerProfileIDs();
    }

    /**
     * Create an instance of {@link CreateOrUpdateProfileConsumerResponse }
     * 
     */
    public CreateOrUpdateProfileConsumerResponse createCreateOrUpdateProfileConsumerResponse() {
        return new CreateOrUpdateProfileConsumerResponse();
    }

    /**
     * Create an instance of {@link LoyaltyCardTypeTranslationType }
     * 
     */
    public LoyaltyCardTypeTranslationType createLoyaltyCardTypeTranslationType() {
        return new LoyaltyCardTypeTranslationType();
    }

    /**
     * Create an instance of {@link SaveSubagentsResponse.Payload }
     * 
     */
    public SaveSubagentsResponse.Payload createSaveSubagentsResponsePayload() {
        return new SaveSubagentsResponse.Payload();
    }

    /**
     * Create an instance of {@link GetUCPByConsumerAttributesResponse }
     * 
     */
    public GetUCPByConsumerAttributesResponse createGetUCPByConsumerAttributesResponse() {
        return new GetUCPByConsumerAttributesResponse();
    }

    /**
     * Create an instance of {@link GetCountryInfoResponse }
     * 
     */
    public GetCountryInfoResponse createGetCountryInfoResponse() {
        return new GetCountryInfoResponse();
    }

    /**
     * Create an instance of {@link MoneyGramSendWithTaxDetailInfo }
     * 
     */
    public MoneyGramSendWithTaxDetailInfo createMoneyGramSendWithTaxDetailInfo() {
        return new MoneyGramSendWithTaxDetailInfo();
    }

    /**
     * Create an instance of {@link GetBroadcastMessagesResponse }
     * 
     */
    public GetBroadcastMessagesResponse createGetBroadcastMessagesResponse() {
        return new GetBroadcastMessagesResponse();
    }

    /**
     * Create an instance of {@link ConsumerHistoryLookupResponse.Payload }
     * 
     */
    public ConsumerHistoryLookupResponse.Payload createConsumerHistoryLookupResponsePayload() {
        return new ConsumerHistoryLookupResponse.Payload();
    }

    /**
     * Create an instance of {@link SubagentsRequest }
     * 
     */
    public SubagentsRequest createSubagentsRequest() {
        return new SubagentsRequest();
    }

    /**
     * Create an instance of {@link ReceiveReversalValidationRequest.VerifiedFields }
     * 
     */
    public ReceiveReversalValidationRequest.VerifiedFields createReceiveReversalValidationRequestVerifiedFields() {
        return new ReceiveReversalValidationRequest.VerifiedFields();
    }

    /**
     * Create an instance of {@link CreateOrUpdateProfileSenderRequest.FieldValues }
     * 
     */
    public CreateOrUpdateProfileSenderRequest.FieldValues createCreateOrUpdateProfileSenderRequestFieldValues() {
        return new CreateOrUpdateProfileSenderRequest.FieldValues();
    }

    /**
     * Create an instance of {@link DwInitialSetupResponse }
     * 
     */
    public DwInitialSetupResponse createDwInitialSetupResponse() {
        return new DwInitialSetupResponse();
    }

    /**
     * Create an instance of {@link SubagentInfo }
     * 
     */
    public SubagentInfo createSubagentInfo() {
        return new SubagentInfo();
    }

    /**
     * Create an instance of {@link AmendValidationResponse.Payload.FieldsToCollect }
     * 
     */
    public AmendValidationResponse.Payload.FieldsToCollect createAmendValidationResponsePayloadFieldsToCollect() {
        return new AmendValidationResponse.Payload.FieldsToCollect();
    }

    /**
     * Create an instance of {@link GetBankDetailsResponse.Payload }
     * 
     */
    public GetBankDetailsResponse.Payload createGetBankDetailsResponsePayload() {
        return new GetBankDetailsResponse.Payload();
    }

    /**
     * Create an instance of {@link AmountInfo }
     * 
     */
    public AmountInfo createAmountInfo() {
        return new AmountInfo();
    }

    /**
     * Create an instance of {@link ReceiptsFormatDetailsResponse.Payload }
     * 
     */
    public ReceiptsFormatDetailsResponse.Payload createReceiptsFormatDetailsResponsePayload() {
        return new ReceiptsFormatDetailsResponse.Payload();
    }

    /**
     * Create an instance of {@link BillPaymentSummaryInfo }
     * 
     */
    public BillPaymentSummaryInfo createBillPaymentSummaryInfo() {
        return new BillPaymentSummaryInfo();
    }

    /**
     * Create an instance of {@link CreateOrUpdateProfileSenderResponse.Payload.FieldsToCollect }
     * 
     */
    public CreateOrUpdateProfileSenderResponse.Payload.FieldsToCollect createCreateOrUpdateProfileSenderResponsePayloadFieldsToCollect() {
        return new CreateOrUpdateProfileSenderResponse.Payload.FieldsToCollect();
    }

    /**
     * Create an instance of {@link SendReversalValidationResponse.Payload.FieldsToCollect }
     * 
     */
    public SendReversalValidationResponse.Payload.FieldsToCollect createSendReversalValidationResponsePayloadFieldsToCollect() {
        return new SendReversalValidationResponse.Payload.FieldsToCollect();
    }

    /**
     * Create an instance of {@link BillerLookupInfo }
     * 
     */
    public BillerLookupInfo createBillerLookupInfo() {
        return new BillerLookupInfo();
    }

    /**
     * Create an instance of {@link CompleteSessionResponse.Payload.ReceiptInfo.ReceiptTextInfos }
     * 
     */
    public CompleteSessionResponse.Payload.ReceiptInfo.ReceiptTextInfos createCompleteSessionResponsePayloadReceiptInfoReceiptTextInfos() {
        return new CompleteSessionResponse.Payload.ReceiptInfo.ReceiptTextInfos();
    }

    /**
     * Create an instance of {@link DirectoryOfAgentsByAreaCodePrefixResponse.Payload }
     * 
     */
    public DirectoryOfAgentsByAreaCodePrefixResponse.Payload createDirectoryOfAgentsByAreaCodePrefixResponsePayload() {
        return new DirectoryOfAgentsByAreaCodePrefixResponse.Payload();
    }

    /**
     * Create an instance of {@link AmendValidationRequest }
     * 
     */
    public AmendValidationRequest createAmendValidationRequest() {
        return new AmendValidationRequest();
    }

    /**
     * Create an instance of {@link CityListResponse }
     * 
     */
    public CityListResponse createCityListResponse() {
        return new CityListResponse();
    }

    /**
     * Create an instance of {@link BPValidationResponse.Payload.ReceiptInfo.ServiceOfferingDescriptions }
     * 
     */
    public BPValidationResponse.Payload.ReceiptInfo.ServiceOfferingDescriptions createBPValidationResponsePayloadReceiptInfoServiceOfferingDescriptions() {
        return new BPValidationResponse.Payload.ReceiptInfo.ServiceOfferingDescriptions();
    }

    /**
     * Create an instance of {@link GetProfileConsumerResponse }
     * 
     */
    public GetProfileConsumerResponse createGetProfileConsumerResponse() {
        return new GetProfileConsumerResponse();
    }

    /**
     * Create an instance of {@link CompleteSessionResponse }
     * 
     */
    public CompleteSessionResponse createCompleteSessionResponse() {
        return new CompleteSessionResponse();
    }

    /**
     * Create an instance of {@link GetBankDetailsResponse }
     * 
     */
    public GetBankDetailsResponse createGetBankDetailsResponse() {
        return new GetBankDetailsResponse();
    }

    /**
     * Create an instance of {@link SendValidationResponse.Payload.PromotionInfos }
     * 
     */
    public SendValidationResponse.Payload.PromotionInfos createSendValidationResponsePayloadPromotionInfos() {
        return new SendValidationResponse.Payload.PromotionInfos();
    }

    /**
     * Create an instance of {@link GetCountrySubdivisionResponse }
     * 
     */
    public GetCountrySubdivisionResponse createGetCountrySubdivisionResponse() {
        return new GetCountrySubdivisionResponse();
    }

    /**
     * Create an instance of {@link CreditLimitInfo }
     * 
     */
    public CreditLimitInfo createCreditLimitInfo() {
        return new CreditLimitInfo();
    }

    /**
     * Create an instance of {@link UCPRequestProfileType }
     * 
     */
    public UCPRequestProfileType createUCPRequestProfileType() {
        return new UCPRequestProfileType();
    }

    /**
     * Create an instance of {@link SaveConsumerProfileImageResponse.Payload }
     * 
     */
    public SaveConsumerProfileImageResponse.Payload createSaveConsumerProfileImageResponsePayload() {
        return new SaveConsumerProfileImageResponse.Payload();
    }

    /**
     * Create an instance of {@link AgentInfo }
     * 
     */
    public AgentInfo createAgentInfo() {
        return new AgentInfo();
    }

    /**
     * Create an instance of {@link ConsumerHistoryLookupResponse.Payload.SenderInfos }
     * 
     */
    public ConsumerHistoryLookupResponse.Payload.SenderInfos createConsumerHistoryLookupResponsePayloadSenderInfos() {
        return new ConsumerHistoryLookupResponse.Payload.SenderInfos();
    }

    /**
     * Create an instance of {@link DepositAnnouncementResponse }
     * 
     */
    public DepositAnnouncementResponse createDepositAnnouncementResponse() {
        return new DepositAnnouncementResponse();
    }

    /**
     * Create an instance of {@link FQDOTextTranslationType }
     * 
     */
    public FQDOTextTranslationType createFQDOTextTranslationType() {
        return new FQDOTextTranslationType();
    }

    /**
     * Create an instance of {@link ReceiveValidationRequest.FieldValues }
     * 
     */
    public ReceiveValidationRequest.FieldValues createReceiveValidationRequestFieldValues() {
        return new ReceiveValidationRequest.FieldValues();
    }

    /**
     * Create an instance of {@link IndustryResponse.Payload }
     * 
     */
    public IndustryResponse.Payload createIndustryResponsePayload() {
        return new IndustryResponse.Payload();
    }

    /**
     * Create an instance of {@link GetDebugDataRequest }
     * 
     */
    public GetDebugDataRequest createGetDebugDataRequest() {
        return new GetDebugDataRequest();
    }

    /**
     * Create an instance of {@link BPValidationResponse.Payload.ReceiptInfo }
     * 
     */
    public BPValidationResponse.Payload.ReceiptInfo createBPValidationResponsePayloadReceiptInfo() {
        return new BPValidationResponse.Payload.ReceiptInfo();
    }

    /**
     * Create an instance of {@link CreateOrUpdateProfileReceiverResponse.Payload }
     * 
     */
    public CreateOrUpdateProfileReceiverResponse.Payload createCreateOrUpdateProfileReceiverResponsePayload() {
        return new CreateOrUpdateProfileReceiverResponse.Payload();
    }

    /**
     * Create an instance of {@link GetProfileConsumerRequest }
     * 
     */
    public GetProfileConsumerRequest createGetProfileConsumerRequest() {
        return new GetProfileConsumerRequest();
    }

    /**
     * Create an instance of {@link MoneyGramSendDetailInfo }
     * 
     */
    public MoneyGramSendDetailInfo createMoneyGramSendDetailInfo() {
        return new MoneyGramSendDetailInfo();
    }

    /**
     * Create an instance of {@link BPValidationRequest.ReceiptImages }
     * 
     */
    public BPValidationRequest.ReceiptImages createBPValidationRequestReceiptImages() {
        return new BPValidationRequest.ReceiptImages();
    }

    /**
     * Create an instance of {@link BillerInfo }
     * 
     */
    public BillerInfo createBillerInfo() {
        return new BillerInfo();
    }

    /**
     * Create an instance of {@link TextTranslationType }
     * 
     */
    public TextTranslationType createTextTranslationType() {
        return new TextTranslationType();
    }

    /**
     * Create an instance of {@link SendAmountInfo }
     * 
     */
    public SendAmountInfo createSendAmountInfo() {
        return new SendAmountInfo();
    }

    /**
     * Create an instance of {@link GetProfileReceiverResponse.Payload }
     * 
     */
    public GetProfileReceiverResponse.Payload createGetProfileReceiverResponsePayload() {
        return new GetProfileReceiverResponse.Payload();
    }

    /**
     * Create an instance of {@link CountryFeeInfo }
     * 
     */
    public CountryFeeInfo createCountryFeeInfo() {
        return new CountryFeeInfo();
    }

    /**
     * Create an instance of {@link AmendValidationRequest.VerifiedFields }
     * 
     */
    public AmendValidationRequest.VerifiedFields createAmendValidationRequestVerifiedFields() {
        return new AmendValidationRequest.VerifiedFields();
    }

    /**
     * Create an instance of {@link DirectoryOfAgentsByZipResponse.Payload }
     * 
     */
    public DirectoryOfAgentsByZipResponse.Payload createDirectoryOfAgentsByZipResponsePayload() {
        return new DirectoryOfAgentsByZipResponse.Payload();
    }

    /**
     * Create an instance of {@link GetDepositInformationRequest }
     * 
     */
    public GetDepositInformationRequest createGetDepositInformationRequest() {
        return new GetDepositInformationRequest();
    }

    /**
     * Create an instance of {@link CreateOrUpdateProfileReceiverRequest }
     * 
     */
    public CreateOrUpdateProfileReceiverRequest createCreateOrUpdateProfileReceiverRequest() {
        return new CreateOrUpdateProfileReceiverRequest();
    }

    /**
     * Create an instance of {@link TransactionLookupResponse.Payload.ReceiveTaxDisclosureTexts }
     * 
     */
    public TransactionLookupResponse.Payload.ReceiveTaxDisclosureTexts createTransactionLookupResponsePayloadReceiveTaxDisclosureTexts() {
        return new TransactionLookupResponse.Payload.ReceiveTaxDisclosureTexts();
    }

    /**
     * Create an instance of {@link CreateOrUpdateProfileSenderRequest.VerifiedFields }
     * 
     */
    public CreateOrUpdateProfileSenderRequest.VerifiedFields createCreateOrUpdateProfileSenderRequestVerifiedFields() {
        return new CreateOrUpdateProfileSenderRequest.VerifiedFields();
    }

    /**
     * Create an instance of {@link ReceiptsFormatDetailsResponse }
     * 
     */
    public ReceiptsFormatDetailsResponse createReceiptsFormatDetailsResponse() {
        return new ReceiptsFormatDetailsResponse();
    }

    /**
     * Create an instance of {@link InfosType }
     * 
     */
    public InfosType createInfosType() {
        return new InfosType();
    }

    /**
     * Create an instance of {@link ReceiptImageContentType }
     * 
     */
    public ReceiptImageContentType createReceiptImageContentType() {
        return new ReceiptImageContentType();
    }

    /**
     * Create an instance of {@link SaveProfileRequest.EmployeeProfileItems }
     * 
     */
    public SaveProfileRequest.EmployeeProfileItems createSaveProfileRequestEmployeeProfileItems() {
        return new SaveProfileRequest.EmployeeProfileItems();
    }

    /**
     * Create an instance of {@link SendValidationResponse.Payload.ReceiptInfo.PromotionalMessages }
     * 
     */
    public SendValidationResponse.Payload.ReceiptInfo.PromotionalMessages createSendValidationResponsePayloadReceiptInfoPromotionalMessages() {
        return new SendValidationResponse.Payload.ReceiptInfo.PromotionalMessages();
    }

    /**
     * Create an instance of {@link BillPaymentDetailReportResponse }
     * 
     */
    public BillPaymentDetailReportResponse createBillPaymentDetailReportResponse() {
        return new BillPaymentDetailReportResponse();
    }

    /**
     * Create an instance of {@link ReceiptsFormatDetailsRequest.Languages }
     * 
     */
    public ReceiptsFormatDetailsRequest.Languages createReceiptsFormatDetailsRequestLanguages() {
        return new ReceiptsFormatDetailsRequest.Languages();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEnumerationsResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = GetEnumerationsResponse.class)
    public JAXBElement<GetEnumerationsResponse.Payload> createGetEnumerationsResponsePayload(GetEnumerationsResponse.Payload value) {
        return new JAXBElement<GetEnumerationsResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, GetEnumerationsResponse.Payload.class, GetEnumerationsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBankDetailsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getBankDetailsRequest")
    public JAXBElement<GetBankDetailsRequest> createGetBankDetailsRequest(GetBankDetailsRequest value) {
        return new JAXBElement<GetBankDetailsRequest>(_GetBankDetailsRequest_QNAME, GetBankDetailsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDepositInformationRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getDepositInformationRequest")
    public JAXBElement<GetDepositInformationRequest> createGetDepositInformationRequest(GetDepositInformationRequest value) {
        return new JAXBElement<GetDepositInformationRequest>(_GetDepositInformationRequest_QNAME, GetDepositInformationRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsumerHistoryLookupResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "consumerHistoryLookupResponse")
    public JAXBElement<ConsumerHistoryLookupResponse> createConsumerHistoryLookupResponse(ConsumerHistoryLookupResponse value) {
        return new JAXBElement<ConsumerHistoryLookupResponse>(_ConsumerHistoryLookupResponse_QNAME, ConsumerHistoryLookupResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BillPaymentSummaryReportResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "billPaymentSummaryReportResponse")
    public JAXBElement<BillPaymentSummaryReportResponse> createBillPaymentSummaryReportResponse(BillPaymentSummaryReportResponse value) {
        return new JAXBElement<BillPaymentSummaryReportResponse>(_BillPaymentSummaryReportResponse_QNAME, BillPaymentSummaryReportResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBroadcastMessagesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getBroadcastMessagesResponse")
    public JAXBElement<GetBroadcastMessagesResponse> createGetBroadcastMessagesResponse(GetBroadcastMessagesResponse value) {
        return new JAXBElement<GetBroadcastMessagesResponse>(_GetBroadcastMessagesResponse_QNAME, GetBroadcastMessagesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendValidationRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "sendValidationRequest")
    public JAXBElement<SendValidationRequest> createSendValidationRequest(SendValidationRequest value) {
        return new JAXBElement<SendValidationRequest>(_SendValidationRequest_QNAME, SendValidationRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDebugDataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "saveDebugDataResponse")
    public JAXBElement<SaveDebugDataResponse> createSaveDebugDataResponse(SaveDebugDataResponse value) {
        return new JAXBElement<SaveDebugDataResponse>(_SaveDebugDataResponse_QNAME, SaveDebugDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FieldToCollectInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "fieldToCollectInfo")
    public JAXBElement<FieldToCollectInfo> createFieldToCollectInfo(FieldToCollectInfo value) {
        return new JAXBElement<FieldToCollectInfo>(_FieldToCollectInfo_QNAME, FieldToCollectInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ComplianceTransactionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "complianceTransactionResponse")
    public JAXBElement<ComplianceTransactionResponse> createComplianceTransactionResponse(ComplianceTransactionResponse value) {
        return new JAXBElement<ComplianceTransactionResponse>(_ComplianceTransactionResponse_QNAME, ComplianceTransactionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BillPaymentSummaryReportRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "billPaymentSummaryReportRequest")
    public JAXBElement<BillPaymentSummaryReportRequest> createBillPaymentSummaryReportRequest(BillPaymentSummaryReportRequest value) {
        return new JAXBElement<BillPaymentSummaryReportRequest>(_BillPaymentSummaryReportRequest_QNAME, BillPaymentSummaryReportRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DirectoryOfAgentsByZipResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "directoryOfAgentsByZipResponse")
    public JAXBElement<DirectoryOfAgentsByZipResponse> createDirectoryOfAgentsByZipResponse(DirectoryOfAgentsByZipResponse value) {
        return new JAXBElement<DirectoryOfAgentsByZipResponse>(_DirectoryOfAgentsByZipResponse_QNAME, DirectoryOfAgentsByZipResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DwPasswordRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "dwPasswordRequest")
    public JAXBElement<DwPasswordRequest> createDwPasswordRequest(DwPasswordRequest value) {
        return new JAXBElement<DwPasswordRequest>(_DwPasswordRequest_QNAME, DwPasswordRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionManifestRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "versionManifestRequest")
    public JAXBElement<VersionManifestRequest> createVersionManifestRequest(VersionManifestRequest value) {
        return new JAXBElement<VersionManifestRequest>(_VersionManifestRequest_QNAME, VersionManifestRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BillPaymentDetailReportResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "billPaymentDetailReportResponse")
    public JAXBElement<BillPaymentDetailReportResponse> createBillPaymentDetailReportResponse(BillPaymentDetailReportResponse value) {
        return new JAXBElement<BillPaymentDetailReportResponse>(_BillPaymentDetailReportResponse_QNAME, BillPaymentDetailReportResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckInRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "checkInRequest")
    public JAXBElement<CheckInRequest> createCheckInRequest(CheckInRequest value) {
        return new JAXBElement<CheckInRequest>(_CheckInRequest_QNAME, CheckInRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DirectoryOfAgentsByCityResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "directoryOfAgentsByCityResponse")
    public JAXBElement<DirectoryOfAgentsByCityResponse> createDirectoryOfAgentsByCityResponse(DirectoryOfAgentsByCityResponse value) {
        return new JAXBElement<DirectoryOfAgentsByCityResponse>(_DirectoryOfAgentsByCityResponse_QNAME, DirectoryOfAgentsByCityResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendReversalValidationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "sendReversalValidationResponse")
    public JAXBElement<SendReversalValidationResponse> createSendReversalValidationResponse(SendReversalValidationResponse value) {
        return new JAXBElement<SendReversalValidationResponse>(_SendReversalValidationResponse_QNAME, SendReversalValidationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProfileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "profileResponse")
    public JAXBElement<ProfileResponse> createProfileResponse(ProfileResponse value) {
        return new JAXBElement<ProfileResponse>(_ProfileResponse_QNAME, ProfileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateOrUpdateProfileSenderResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "createOrUpdateProfileSenderResponse")
    public JAXBElement<CreateOrUpdateProfileSenderResponse> createCreateOrUpdateProfileSenderResponse(CreateOrUpdateProfileSenderResponse value) {
        return new JAXBElement<CreateOrUpdateProfileSenderResponse>(_CreateOrUpdateProfileSenderResponse_QNAME, CreateOrUpdateProfileSenderResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetServiceOptionsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getServiceOptionsRequest")
    public JAXBElement<GetServiceOptionsRequest> createGetServiceOptionsRequest(GetServiceOptionsRequest value) {
        return new JAXBElement<GetServiceOptionsRequest>(_GetServiceOptionsRequest_QNAME, GetServiceOptionsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProfileReceiverResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getProfileReceiverResponse")
    public JAXBElement<GetProfileReceiverResponse> createGetProfileReceiverResponse(GetProfileReceiverResponse value) {
        return new JAXBElement<GetProfileReceiverResponse>(_GetProfileReceiverResponse_QNAME, GetProfileReceiverResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDebugDataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getDebugDataResponse")
    public JAXBElement<GetDebugDataResponse> createGetDebugDataResponse(GetDebugDataResponse value) {
        return new JAXBElement<GetDebugDataResponse>(_GetDebugDataResponse_QNAME, GetDebugDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BillerSearchRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "billerSearchRequest")
    public JAXBElement<BillerSearchRequest> createBillerSearchRequest(BillerSearchRequest value) {
        return new JAXBElement<BillerSearchRequest>(_BillerSearchRequest_QNAME, BillerSearchRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveConsumerProfileImageRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "saveConsumerProfileImageRequest")
    public JAXBElement<SaveConsumerProfileImageRequest> createSaveConsumerProfileImageRequest(SaveConsumerProfileImageRequest value) {
        return new JAXBElement<SaveConsumerProfileImageRequest>(_SaveConsumerProfileImageRequest_QNAME, SaveConsumerProfileImageRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveSubagentsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "saveSubagentsRequest")
    public JAXBElement<SaveSubagentsRequest> createSaveSubagentsRequest(SaveSubagentsRequest value) {
        return new JAXBElement<SaveSubagentsRequest>(_SaveSubagentsRequest_QNAME, SaveSubagentsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetReceiptForReprintResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getReceiptForReprintResponse")
    public JAXBElement<GetReceiptForReprintResponse> createGetReceiptForReprintResponse(GetReceiptForReprintResponse value) {
        return new JAXBElement<GetReceiptForReprintResponse>(_GetReceiptForReprintResponse_QNAME, GetReceiptForReprintResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDebugDataRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getDebugDataRequest")
    public JAXBElement<GetDebugDataRequest> createGetDebugDataRequest(GetDebugDataRequest value) {
        return new JAXBElement<GetDebugDataRequest>(_GetDebugDataRequest_QNAME, GetDebugDataRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDepositInformationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getDepositInformationResponse")
    public JAXBElement<GetDepositInformationResponse> createGetDepositInformationResponse(GetDepositInformationResponse value) {
        return new JAXBElement<GetDepositInformationResponse>(_GetDepositInformationResponse_QNAME, GetDepositInformationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllFieldsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getAllFieldsResponse")
    public JAXBElement<GetAllFieldsResponse> createGetAllFieldsResponse(GetAllFieldsResponse value) {
        return new JAXBElement<GetAllFieldsResponse>(_GetAllFieldsResponse_QNAME, GetAllFieldsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DirectoryOfAgentsByAreaCodePrefixResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "directoryOfAgentsByAreaCodePrefixResponse")
    public JAXBElement<DirectoryOfAgentsByAreaCodePrefixResponse> createDirectoryOfAgentsByAreaCodePrefixResponse(DirectoryOfAgentsByAreaCodePrefixResponse value) {
        return new JAXBElement<DirectoryOfAgentsByAreaCodePrefixResponse>(_DirectoryOfAgentsByAreaCodePrefixResponse_QNAME, DirectoryOfAgentsByAreaCodePrefixResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveProfileRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "saveProfileRequest")
    public JAXBElement<SaveProfileRequest> createSaveProfileRequest(SaveProfileRequest value) {
        return new JAXBElement<SaveProfileRequest>(_SaveProfileRequest_QNAME, SaveProfileRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendReversalValidationRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "sendReversalValidationRequest")
    public JAXBElement<SendReversalValidationRequest> createSendReversalValidationRequest(SendReversalValidationRequest value) {
        return new JAXBElement<SendReversalValidationRequest>(_SendReversalValidationRequest_QNAME, SendReversalValidationRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchStagedTransactionsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "searchStagedTransactionsRequest")
    public JAXBElement<SearchStagedTransactionsRequest> createSearchStagedTransactionsRequest(SearchStagedTransactionsRequest value) {
        return new JAXBElement<SearchStagedTransactionsRequest>(_SearchStagedTransactionsRequest_QNAME, SearchStagedTransactionsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CompleteSessionRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "completeSessionRequest")
    public JAXBElement<CompleteSessionRequest> createCompleteSessionRequest(CompleteSessionRequest value) {
        return new JAXBElement<CompleteSessionRequest>(_CompleteSessionRequest_QNAME, CompleteSessionRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrencyInfoRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getCurrencyInfoRequest")
    public JAXBElement<GetCurrencyInfoRequest> createGetCurrencyInfoRequest(GetCurrencyInfoRequest value) {
        return new JAXBElement<GetCurrencyInfoRequest>(_GetCurrencyInfoRequest_QNAME, GetCurrencyInfoRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCountryInfoRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getCountryInfoRequest")
    public JAXBElement<GetCountryInfoRequest> createGetCountryInfoRequest(GetCountryInfoRequest value) {
        return new JAXBElement<GetCountryInfoRequest>(_GetCountryInfoRequest_QNAME, GetCountryInfoRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoneyGramReceiveDetailReportRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "moneyGramReceiveDetailReportRequest")
    public JAXBElement<MoneyGramReceiveDetailReportRequest> createMoneyGramReceiveDetailReportRequest(MoneyGramReceiveDetailReportRequest value) {
        return new JAXBElement<MoneyGramReceiveDetailReportRequest>(_MoneyGramReceiveDetailReportRequest_QNAME, MoneyGramReceiveDetailReportRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUCPByConsumerAttributesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getUCPByConsumerAttributesRequest")
    public JAXBElement<GetUCPByConsumerAttributesRequest> createGetUCPByConsumerAttributesRequest(GetUCPByConsumerAttributesRequest value) {
        return new JAXBElement<GetUCPByConsumerAttributesRequest>(_GetUCPByConsumerAttributesRequest_QNAME, GetUCPByConsumerAttributesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConfirmTokenRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "confirmTokenRequest")
    public JAXBElement<ConfirmTokenRequest> createConfirmTokenRequest(ConfirmTokenRequest value) {
        return new JAXBElement<ConfirmTokenRequest>(_ConfirmTokenRequest_QNAME, ConfirmTokenRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DoddFrankStateRegulatorInfoRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "doddFrankStateRegulatorInfoRequest")
    public JAXBElement<DoddFrankStateRegulatorInfoRequest> createDoddFrankStateRegulatorInfoRequest(DoddFrankStateRegulatorInfoRequest value) {
        return new JAXBElement<DoddFrankStateRegulatorInfoRequest>(_DoddFrankStateRegulatorInfoRequest_QNAME, DoddFrankStateRegulatorInfoRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DwPasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "dwPasswordResponse")
    public JAXBElement<DwPasswordResponse> createDwPasswordResponse(DwPasswordResponse value) {
        return new JAXBElement<DwPasswordResponse>(_DwPasswordResponse_QNAME, DwPasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllErrorsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getAllErrorsRequest")
    public JAXBElement<GetAllErrorsRequest> createGetAllErrorsRequest(GetAllErrorsRequest value) {
        return new JAXBElement<GetAllErrorsRequest>(_GetAllErrorsRequest_QNAME, GetAllErrorsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AmendValidationRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "amendValidationRequest")
    public JAXBElement<AmendValidationRequest> createAmendValidationRequest(AmendValidationRequest value) {
        return new JAXBElement<AmendValidationRequest>(_AmendValidationRequest_QNAME, AmendValidationRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CityListRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "cityListRequest")
    public JAXBElement<CityListRequest> createCityListRequest(CityListRequest value) {
        return new JAXBElement<CityListRequest>(_CityListRequest_QNAME, CityListRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCountrySubdivisionRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getCountrySubdivisionRequest")
    public JAXBElement<GetCountrySubdivisionRequest> createGetCountrySubdivisionRequest(GetCountrySubdivisionRequest value) {
        return new JAXBElement<GetCountrySubdivisionRequest>(_GetCountrySubdivisionRequest_QNAME, GetCountrySubdivisionRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BPValidationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "bpValidationResponse")
    public JAXBElement<BPValidationResponse> createBpValidationResponse(BPValidationResponse value) {
        return new JAXBElement<BPValidationResponse>(_BpValidationResponse_QNAME, BPValidationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoneyGramSendSummaryReportResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "moneyGramSendSummaryReportResponse")
    public JAXBElement<MoneyGramSendSummaryReportResponse> createMoneyGramSendSummaryReportResponse(MoneyGramSendSummaryReportResponse value) {
        return new JAXBElement<MoneyGramSendSummaryReportResponse>(_MoneyGramSendSummaryReportResponse_QNAME, MoneyGramSendSummaryReportResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchConsumerProfilesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "searchConsumerProfilesResponse")
    public JAXBElement<SearchConsumerProfilesResponse> createSearchConsumerProfilesResponse(SearchConsumerProfilesResponse value) {
        return new JAXBElement<SearchConsumerProfilesResponse>(_SearchConsumerProfilesResponse_QNAME, SearchConsumerProfilesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InjectedInstructionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "injectedInstructionResponse")
    public JAXBElement<InjectedInstructionResponse> createInjectedInstructionResponse(InjectedInstructionResponse value) {
        return new JAXBElement<InjectedInstructionResponse>(_InjectedInstructionResponse_QNAME, InjectedInstructionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveProfileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "saveProfileResponse")
    public JAXBElement<SaveProfileResponse> createSaveProfileResponse(SaveProfileResponse value) {
        return new JAXBElement<SaveProfileResponse>(_SaveProfileResponse_QNAME, SaveProfileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ComplianceTransactionRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "complianceTransactionRequest")
    public JAXBElement<ComplianceTransactionRequest> createComplianceTransactionRequest(ComplianceTransactionRequest value) {
        return new JAXBElement<ComplianceTransactionRequest>(_ComplianceTransactionRequest_QNAME, ComplianceTransactionRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProfileChangeRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "profileChangeRequest")
    public JAXBElement<ProfileChangeRequest> createProfileChangeRequest(ProfileChangeRequest value) {
        return new JAXBElement<ProfileChangeRequest>(_ProfileChangeRequest_QNAME, ProfileChangeRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterHardTokenRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "registerHardTokenRequest")
    public JAXBElement<RegisterHardTokenRequest> createRegisterHardTokenRequest(RegisterHardTokenRequest value) {
        return new JAXBElement<RegisterHardTokenRequest>(_RegisterHardTokenRequest_QNAME, RegisterHardTokenRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CityListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "cityListResponse")
    public JAXBElement<CityListResponse> createCityListResponse(CityListResponse value) {
        return new JAXBElement<CityListResponse>(_CityListResponse_QNAME, CityListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DirectoryOfAgentsByAreaCodePrefixRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "directoryOfAgentsByAreaCodePrefixRequest")
    public JAXBElement<DirectoryOfAgentsByAreaCodePrefixRequest> createDirectoryOfAgentsByAreaCodePrefixRequest(DirectoryOfAgentsByAreaCodePrefixRequest value) {
        return new JAXBElement<DirectoryOfAgentsByAreaCodePrefixRequest>(_DirectoryOfAgentsByAreaCodePrefixRequest_QNAME, DirectoryOfAgentsByAreaCodePrefixRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateOrUpdateProfileConsumerRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "createOrUpdateProfileConsumerRequest")
    public JAXBElement<CreateOrUpdateProfileConsumerRequest> createCreateOrUpdateProfileConsumerRequest(CreateOrUpdateProfileConsumerRequest value) {
        return new JAXBElement<CreateOrUpdateProfileConsumerRequest>(_CreateOrUpdateProfileConsumerRequest_QNAME, CreateOrUpdateProfileConsumerRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEnumerationsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getEnumerationsRequest")
    public JAXBElement<GetEnumerationsRequest> createGetEnumerationsRequest(GetEnumerationsRequest value) {
        return new JAXBElement<GetEnumerationsRequest>(_GetEnumerationsRequest_QNAME, GetEnumerationsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionLookupResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "transactionLookupResponse")
    public JAXBElement<TransactionLookupResponse> createTransactionLookupResponse(TransactionLookupResponse value) {
        return new JAXBElement<TransactionLookupResponse>(_TransactionLookupResponse_QNAME, TransactionLookupResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionLookupRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "transactionLookupRequest")
    public JAXBElement<TransactionLookupRequest> createTransactionLookupRequest(TransactionLookupRequest value) {
        return new JAXBElement<TransactionLookupRequest>(_TransactionLookupRequest_QNAME, TransactionLookupRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DwInitialSetupResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "dwInitialSetupResponse")
    public JAXBElement<DwInitialSetupResponse> createDwInitialSetupResponse(DwInitialSetupResponse value) {
        return new JAXBElement<DwInitialSetupResponse>(_DwInitialSetupResponse_QNAME, DwInitialSetupResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateOrUpdateProfileSenderRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "createOrUpdateProfileSenderRequest")
    public JAXBElement<CreateOrUpdateProfileSenderRequest> createCreateOrUpdateProfileSenderRequest(CreateOrUpdateProfileSenderRequest value) {
        return new JAXBElement<CreateOrUpdateProfileSenderRequest>(_CreateOrUpdateProfileSenderRequest_QNAME, CreateOrUpdateProfileSenderRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DirectoryOfAgentsByCityRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "directoryOfAgentsByCityRequest")
    public JAXBElement<DirectoryOfAgentsByCityRequest> createDirectoryOfAgentsByCityRequest(DirectoryOfAgentsByCityRequest value) {
        return new JAXBElement<DirectoryOfAgentsByCityRequest>(_DirectoryOfAgentsByCityRequest_QNAME, DirectoryOfAgentsByCityRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrencyInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getCurrencyInfoResponse")
    public JAXBElement<GetCurrencyInfoResponse> createGetCurrencyInfoResponse(GetCurrencyInfoResponse value) {
        return new JAXBElement<GetCurrencyInfoResponse>(_GetCurrencyInfoResponse_QNAME, GetCurrencyInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoneyOrderTotalRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "moneyOrderTotalRequest")
    public JAXBElement<MoneyOrderTotalRequest> createMoneyOrderTotalRequest(MoneyOrderTotalRequest value) {
        return new JAXBElement<MoneyOrderTotalRequest>(_MoneyOrderTotalRequest_QNAME, MoneyOrderTotalRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DwProfileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "dwProfileResponse")
    public JAXBElement<DwProfileResponse> createDwProfileResponse(DwProfileResponse value) {
        return new JAXBElement<DwProfileResponse>(_DwProfileResponse_QNAME, DwProfileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveSubagentsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "saveSubagentsResponse")
    public JAXBElement<SaveSubagentsResponse> createSaveSubagentsResponse(SaveSubagentsResponse value) {
        return new JAXBElement<SaveSubagentsResponse>(_SaveSubagentsResponse_QNAME, SaveSubagentsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoneyGramSendDetailReportWithTaxRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "moneyGramSendDetailReportWithTaxRequest")
    public JAXBElement<MoneyGramSendDetailReportWithTaxRequest> createMoneyGramSendDetailReportWithTaxRequest(MoneyGramSendDetailReportWithTaxRequest value) {
        return new JAXBElement<MoneyGramSendDetailReportWithTaxRequest>(_MoneyGramSendDetailReportWithTaxRequest_QNAME, MoneyGramSendDetailReportWithTaxRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AmendValidationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "amendValidationResponse")
    public JAXBElement<AmendValidationResponse> createAmendValidationResponse(AmendValidationResponse value) {
        return new JAXBElement<AmendValidationResponse>(_AmendValidationResponse_QNAME, AmendValidationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceiveReversalValidationRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "receiveReversalValidationRequest")
    public JAXBElement<ReceiveReversalValidationRequest> createReceiveReversalValidationRequest(ReceiveReversalValidationRequest value) {
        return new JAXBElement<ReceiveReversalValidationRequest>(_ReceiveReversalValidationRequest_QNAME, ReceiveReversalValidationRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBankDetailsByLevelResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getBankDetailsByLevelResponse")
    public JAXBElement<GetBankDetailsByLevelResponse> createGetBankDetailsByLevelResponse(GetBankDetailsByLevelResponse value) {
        return new JAXBElement<GetBankDetailsByLevelResponse>(_GetBankDetailsByLevelResponse_QNAME, GetBankDetailsByLevelResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProfileConsumerRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getProfileConsumerRequest")
    public JAXBElement<GetProfileConsumerRequest> createGetProfileConsumerRequest(GetProfileConsumerRequest value) {
        return new JAXBElement<GetProfileConsumerRequest>(_GetProfileConsumerRequest_QNAME, GetProfileConsumerRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDepositBankListRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getDepositBankListRequest")
    public JAXBElement<GetDepositBankListRequest> createGetDepositBankListRequest(GetDepositBankListRequest value) {
        return new JAXBElement<GetDepositBankListRequest>(_GetDepositBankListRequest_QNAME, GetDepositBankListRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBankDetailsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getBankDetailsResponse")
    public JAXBElement<GetBankDetailsResponse> createGetBankDetailsResponse(GetBankDetailsResponse value) {
        return new JAXBElement<GetBankDetailsResponse>(_GetBankDetailsResponse_QNAME, GetBankDetailsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenOTPLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "openOTPLoginResponse")
    public JAXBElement<OpenOTPLoginResponse> createOpenOTPLoginResponse(OpenOTPLoginResponse value) {
        return new JAXBElement<OpenOTPLoginResponse>(_OpenOTPLoginResponse_QNAME, OpenOTPLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DepositAnnouncementRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "depositAnnouncementRequest")
    public JAXBElement<DepositAnnouncementRequest> createDepositAnnouncementRequest(DepositAnnouncementRequest value) {
        return new JAXBElement<DepositAnnouncementRequest>(_DepositAnnouncementRequest_QNAME, DepositAnnouncementRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BillPaymentDetailReportRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "billPaymentDetailReportRequest")
    public JAXBElement<BillPaymentDetailReportRequest> createBillPaymentDetailReportRequest(BillPaymentDetailReportRequest value) {
        return new JAXBElement<BillPaymentDetailReportRequest>(_BillPaymentDetailReportRequest_QNAME, BillPaymentDetailReportRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoneyGramSendDetailReportRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "moneyGramSendDetailReportRequest")
    public JAXBElement<MoneyGramSendDetailReportRequest> createMoneyGramSendDetailReportRequest(MoneyGramSendDetailReportRequest value) {
        return new JAXBElement<MoneyGramSendDetailReportRequest>(_MoneyGramSendDetailReportRequest_QNAME, MoneyGramSendDetailReportRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProfileConsumerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getProfileConsumerResponse")
    public JAXBElement<GetProfileConsumerResponse> createGetProfileConsumerResponse(GetProfileConsumerResponse value) {
        return new JAXBElement<GetProfileConsumerResponse>(_GetProfileConsumerResponse_QNAME, GetProfileConsumerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DwProfileRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "dwProfileRequest")
    public JAXBElement<DwProfileRequest> createDwProfileRequest(DwProfileRequest value) {
        return new JAXBElement<DwProfileRequest>(_DwProfileRequest_QNAME, DwProfileRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoneyGramSendDetailReportResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "moneyGramSendDetailReportResponse")
    public JAXBElement<MoneyGramSendDetailReportResponse> createMoneyGramSendDetailReportResponse(MoneyGramSendDetailReportResponse value) {
        return new JAXBElement<MoneyGramSendDetailReportResponse>(_MoneyGramSendDetailReportResponse_QNAME, MoneyGramSendDetailReportResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PromotionLookupByCodeRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "promotionLookupByCodeRequest")
    public JAXBElement<PromotionLookupByCodeRequest> createPromotionLookupByCodeRequest(PromotionLookupByCodeRequest value) {
        return new JAXBElement<PromotionLookupByCodeRequest>(_PromotionLookupByCodeRequest_QNAME, PromotionLookupByCodeRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SystemError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "SystemError")
    public JAXBElement<SystemError> createSystemError(SystemError value) {
        return new JAXBElement<SystemError>(_SystemError_QNAME, SystemError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProfileRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "profileRequest")
    public JAXBElement<ProfileRequest> createProfileRequest(ProfileRequest value) {
        return new JAXBElement<ProfileRequest>(_ProfileRequest_QNAME, ProfileRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DwInitialSetupRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "dwInitialSetupRequest")
    public JAXBElement<DwInitialSetupRequest> createDwInitialSetupRequest(DwInitialSetupRequest value) {
        return new JAXBElement<DwInitialSetupRequest>(_DwInitialSetupRequest_QNAME, DwInitialSetupRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BPValidationRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "bpValidationRequest")
    public JAXBElement<BPValidationRequest> createBpValidationRequest(BPValidationRequest value) {
        return new JAXBElement<BPValidationRequest>(_BpValidationRequest_QNAME, BPValidationRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FieldInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "fieldInfo")
    public JAXBElement<FieldInfo> createFieldInfo(FieldInfo value) {
        return new JAXBElement<FieldInfo>(_FieldInfo_QNAME, FieldInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoneyGramSendDetailReportWithTaxResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "moneyGramSendDetailReportWithTaxResponse")
    public JAXBElement<MoneyGramSendDetailReportWithTaxResponse> createMoneyGramSendDetailReportWithTaxResponse(MoneyGramSendDetailReportWithTaxResponse value) {
        return new JAXBElement<MoneyGramSendDetailReportWithTaxResponse>(_MoneyGramSendDetailReportWithTaxResponse_QNAME, MoneyGramSendDetailReportWithTaxResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubagentsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "subagentsRequest")
    public JAXBElement<SubagentsRequest> createSubagentsRequest(SubagentsRequest value) {
        return new JAXBElement<SubagentsRequest>(_SubagentsRequest_QNAME, SubagentsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchStagedTransactionsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "searchStagedTransactionsResponse")
    public JAXBElement<SearchStagedTransactionsResponse> createSearchStagedTransactionsResponse(SearchStagedTransactionsResponse value) {
        return new JAXBElement<SearchStagedTransactionsResponse>(_SearchStagedTransactionsResponse_QNAME, SearchStagedTransactionsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TranslationsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "translationsRequest")
    public JAXBElement<TranslationsRequest> createTranslationsRequest(TranslationsRequest value) {
        return new JAXBElement<TranslationsRequest>(_TranslationsRequest_QNAME, TranslationsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubagentsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "subagentsResponse")
    public JAXBElement<SubagentsResponse> createSubagentsResponse(SubagentsResponse value) {
        return new JAXBElement<SubagentsResponse>(_SubagentsResponse_QNAME, SubagentsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DisclosureTextDetailsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "disclosureTextDetailsResponse")
    public JAXBElement<DisclosureTextDetailsResponse> createDisclosureTextDetailsResponse(DisclosureTextDetailsResponse value) {
        return new JAXBElement<DisclosureTextDetailsResponse>(_DisclosureTextDetailsResponse_QNAME, DisclosureTextDetailsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoneyGramSendSummaryReportRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "moneyGramSendSummaryReportRequest")
    public JAXBElement<MoneyGramSendSummaryReportRequest> createMoneyGramSendSummaryReportRequest(MoneyGramSendSummaryReportRequest value) {
        return new JAXBElement<MoneyGramSendSummaryReportRequest>(_MoneyGramSendSummaryReportRequest_QNAME, MoneyGramSendSummaryReportRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DoddFrankStateRegulatorInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "doddFrankStateRegulatorInfoResponse")
    public JAXBElement<DoddFrankStateRegulatorInfoResponse> createDoddFrankStateRegulatorInfoResponse(DoddFrankStateRegulatorInfoResponse value) {
        return new JAXBElement<DoddFrankStateRegulatorInfoResponse>(_DoddFrankStateRegulatorInfoResponse_QNAME, DoddFrankStateRegulatorInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenOTPLoginRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "openOTPLoginRequest")
    public JAXBElement<OpenOTPLoginRequest> createOpenOTPLoginRequest(OpenOTPLoginRequest value) {
        return new JAXBElement<OpenOTPLoginRequest>(_OpenOTPLoginRequest_QNAME, OpenOTPLoginRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchConsumerProfilesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "searchConsumerProfilesRequest")
    public JAXBElement<SearchConsumerProfilesRequest> createSearchConsumerProfilesRequest(SearchConsumerProfilesRequest value) {
        return new JAXBElement<SearchConsumerProfilesRequest>(_SearchConsumerProfilesRequest_QNAME, SearchConsumerProfilesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCountrySubdivisionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getCountrySubdivisionResponse")
    public JAXBElement<GetCountrySubdivisionResponse> createGetCountrySubdivisionResponse(GetCountrySubdivisionResponse value) {
        return new JAXBElement<GetCountrySubdivisionResponse>(_GetCountrySubdivisionResponse_QNAME, GetCountrySubdivisionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDebugDataRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "saveDebugDataRequest")
    public JAXBElement<SaveDebugDataRequest> createSaveDebugDataRequest(SaveDebugDataRequest value) {
        return new JAXBElement<SaveDebugDataRequest>(_SaveDebugDataRequest_QNAME, SaveDebugDataRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InfoBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "InfoBase")
    public JAXBElement<InfoBase> createInfoBase(InfoBase value) {
        return new JAXBElement<InfoBase>(_InfoBase_QNAME, InfoBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCountryInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getCountryInfoResponse")
    public JAXBElement<GetCountryInfoResponse> createGetCountryInfoResponse(GetCountryInfoResponse value) {
        return new JAXBElement<GetCountryInfoResponse>(_GetCountryInfoResponse_QNAME, GetCountryInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoneyGramReceiveSummaryReportRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "moneyGramReceiveSummaryReportRequest")
    public JAXBElement<MoneyGramReceiveSummaryReportRequest> createMoneyGramReceiveSummaryReportRequest(MoneyGramReceiveSummaryReportRequest value) {
        return new JAXBElement<MoneyGramReceiveSummaryReportRequest>(_MoneyGramReceiveSummaryReportRequest_QNAME, MoneyGramReceiveSummaryReportRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DepositAnnouncementResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "depositAnnouncementResponse")
    public JAXBElement<DepositAnnouncementResponse> createDepositAnnouncementResponse(DepositAnnouncementResponse value) {
        return new JAXBElement<DepositAnnouncementResponse>(_DepositAnnouncementResponse_QNAME, DepositAnnouncementResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateOrUpdateProfileReceiverResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "createOrUpdateProfileReceiverResponse")
    public JAXBElement<CreateOrUpdateProfileReceiverResponse> createCreateOrUpdateProfileReceiverResponse(CreateOrUpdateProfileReceiverResponse value) {
        return new JAXBElement<CreateOrUpdateProfileReceiverResponse>(_CreateOrUpdateProfileReceiverResponse_QNAME, CreateOrUpdateProfileReceiverResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllFieldsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getAllFieldsRequest")
    public JAXBElement<GetAllFieldsRequest> createGetAllFieldsRequest(GetAllFieldsRequest value) {
        return new JAXBElement<GetAllFieldsRequest>(_GetAllFieldsRequest_QNAME, GetAllFieldsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoneyOrderTotalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "moneyOrderTotalResponse")
    public JAXBElement<MoneyOrderTotalResponse> createMoneyOrderTotalResponse(MoneyOrderTotalResponse value) {
        return new JAXBElement<MoneyOrderTotalResponse>(_MoneyOrderTotalResponse_QNAME, MoneyOrderTotalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CompleteSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "completeSessionResponse")
    public JAXBElement<CompleteSessionResponse> createCompleteSessionResponse(CompleteSessionResponse value) {
        return new JAXBElement<CompleteSessionResponse>(_CompleteSessionResponse_QNAME, CompleteSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CategoryInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "CategoryInfo")
    public JAXBElement<CategoryInfo> createCategoryInfo(CategoryInfo value) {
        return new JAXBElement<CategoryInfo>(_CategoryInfo_QNAME, CategoryInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoneyGramReceiveSummaryReportResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "moneyGramReceiveSummaryReportResponse")
    public JAXBElement<MoneyGramReceiveSummaryReportResponse> createMoneyGramReceiveSummaryReportResponse(MoneyGramReceiveSummaryReportResponse value) {
        return new JAXBElement<MoneyGramReceiveSummaryReportResponse>(_MoneyGramReceiveSummaryReportResponse_QNAME, MoneyGramReceiveSummaryReportResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VariableReceiptInfoRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "variableReceiptInfoRequest")
    public JAXBElement<VariableReceiptInfoRequest> createVariableReceiptInfoRequest(VariableReceiptInfoRequest value) {
        return new JAXBElement<VariableReceiptInfoRequest>(_VariableReceiptInfoRequest_QNAME, VariableReceiptInfoRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenericBusinessError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "GenericBusinessError")
    public JAXBElement<GenericBusinessError> createGenericBusinessError(GenericBusinessError value) {
        return new JAXBElement<GenericBusinessError>(_GenericBusinessError_QNAME, GenericBusinessError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BusinessError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "BusinessError")
    public JAXBElement<BusinessError> createBusinessError(BusinessError value) {
        return new JAXBElement<BusinessError>(_BusinessError_QNAME, BusinessError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConfirmTokenResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "confirmTokenResponse")
    public JAXBElement<ConfirmTokenResponse> createConfirmTokenResponse(ConfirmTokenResponse value) {
        return new JAXBElement<ConfirmTokenResponse>(_ConfirmTokenResponse_QNAME, ConfirmTokenResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceiveReversalValidationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "receiveReversalValidationResponse")
    public JAXBElement<ReceiveReversalValidationResponse> createReceiveReversalValidationResponse(ReceiveReversalValidationResponse value) {
        return new JAXBElement<ReceiveReversalValidationResponse>(_ReceiveReversalValidationResponse_QNAME, ReceiveReversalValidationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetReceiptForReprintRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getReceiptForReprintRequest")
    public JAXBElement<GetReceiptForReprintRequest> createGetReceiptForReprintRequest(GetReceiptForReprintRequest value) {
        return new JAXBElement<GetReceiptForReprintRequest>(_GetReceiptForReprintRequest_QNAME, GetReceiptForReprintRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DisclosureTextDetailsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "disclosureTextDetailsRequest")
    public JAXBElement<DisclosureTextDetailsRequest> createDisclosureTextDetailsRequest(DisclosureTextDetailsRequest value) {
        return new JAXBElement<DisclosureTextDetailsRequest>(_DisclosureTextDetailsRequest_QNAME, DisclosureTextDetailsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterHardTokenResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "registerHardTokenResponse")
    public JAXBElement<RegisterHardTokenResponse> createRegisterHardTokenResponse(RegisterHardTokenResponse value) {
        return new JAXBElement<RegisterHardTokenResponse>(_RegisterHardTokenResponse_QNAME, RegisterHardTokenResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InitialSetupResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "initialSetupResponse")
    public JAXBElement<InitialSetupResponse> createInitialSetupResponse(InitialSetupResponse value) {
        return new JAXBElement<InitialSetupResponse>(_InitialSetupResponse_QNAME, InitialSetupResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InjectedInstructionRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "injectedInstructionRequest")
    public JAXBElement<InjectedInstructionRequest> createInjectedInstructionRequest(InjectedInstructionRequest value) {
        return new JAXBElement<InjectedInstructionRequest>(_InjectedInstructionRequest_QNAME, InjectedInstructionRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllErrorsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getAllErrorsResponse")
    public JAXBElement<GetAllErrorsResponse> createGetAllErrorsResponse(GetAllErrorsResponse value) {
        return new JAXBElement<GetAllErrorsResponse>(_GetAllErrorsResponse_QNAME, GetAllErrorsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PromotionLookupByCodeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "promotionLookupByCodeResponse")
    public JAXBElement<PromotionLookupByCodeResponse> createPromotionLookupByCodeResponse(PromotionLookupByCodeResponse value) {
        return new JAXBElement<PromotionLookupByCodeResponse>(_PromotionLookupByCodeResponse_QNAME, PromotionLookupByCodeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateOrUpdateProfileConsumerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "createOrUpdateProfileConsumerResponse")
    public JAXBElement<CreateOrUpdateProfileConsumerResponse> createCreateOrUpdateProfileConsumerResponse(CreateOrUpdateProfileConsumerResponse value) {
        return new JAXBElement<CreateOrUpdateProfileConsumerResponse>(_CreateOrUpdateProfileConsumerResponse_QNAME, CreateOrUpdateProfileConsumerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoneyGramReceiveDetailReportResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "moneyGramReceiveDetailReportResponse")
    public JAXBElement<MoneyGramReceiveDetailReportResponse> createMoneyGramReceiveDetailReportResponse(MoneyGramReceiveDetailReportResponse value) {
        return new JAXBElement<MoneyGramReceiveDetailReportResponse>(_MoneyGramReceiveDetailReportResponse_QNAME, MoneyGramReceiveDetailReportResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEnumerationsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getEnumerationsResponse")
    public JAXBElement<GetEnumerationsResponse> createGetEnumerationsResponse(GetEnumerationsResponse value) {
        return new JAXBElement<GetEnumerationsResponse>(_GetEnumerationsResponse_QNAME, GetEnumerationsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProfileSenderRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getProfileSenderRequest")
    public JAXBElement<GetProfileSenderRequest> createGetProfileSenderRequest(GetProfileSenderRequest value) {
        return new JAXBElement<GetProfileSenderRequest>(_GetProfileSenderRequest_QNAME, GetProfileSenderRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DirectoryOfAgentsByZipRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "directoryOfAgentsByZipRequest")
    public JAXBElement<DirectoryOfAgentsByZipRequest> createDirectoryOfAgentsByZipRequest(DirectoryOfAgentsByZipRequest value) {
        return new JAXBElement<DirectoryOfAgentsByZipRequest>(_DirectoryOfAgentsByZipRequest_QNAME, DirectoryOfAgentsByZipRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceiveValidationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "receiveValidationResponse")
    public JAXBElement<ReceiveValidationResponse> createReceiveValidationResponse(ReceiveValidationResponse value) {
        return new JAXBElement<ReceiveValidationResponse>(_ReceiveValidationResponse_QNAME, ReceiveValidationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionManifestResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "versionManifestResponse")
    public JAXBElement<VersionManifestResponse> createVersionManifestResponse(VersionManifestResponse value) {
        return new JAXBElement<VersionManifestResponse>(_VersionManifestResponse_QNAME, VersionManifestResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProfileReceiverRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getProfileReceiverRequest")
    public JAXBElement<GetProfileReceiverRequest> createGetProfileReceiverRequest(GetProfileReceiverRequest value) {
        return new JAXBElement<GetProfileReceiverRequest>(_GetProfileReceiverRequest_QNAME, GetProfileReceiverRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveConsumerProfileImageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "saveConsumerProfileImageResponse")
    public JAXBElement<SaveConsumerProfileImageResponse> createSaveConsumerProfileImageResponse(SaveConsumerProfileImageResponse value) {
        return new JAXBElement<SaveConsumerProfileImageResponse>(_SaveConsumerProfileImageResponse_QNAME, SaveConsumerProfileImageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IndustryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "industryResponse")
    public JAXBElement<IndustryResponse> createIndustryResponse(IndustryResponse value) {
        return new JAXBElement<IndustryResponse>(_IndustryResponse_QNAME, IndustryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceiptsFormatDetailsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "receiptsFormatDetailsResponse")
    public JAXBElement<ReceiptsFormatDetailsResponse> createReceiptsFormatDetailsResponse(ReceiptsFormatDetailsResponse value) {
        return new JAXBElement<ReceiptsFormatDetailsResponse>(_ReceiptsFormatDetailsResponse_QNAME, ReceiptsFormatDetailsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProfileSenderResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getProfileSenderResponse")
    public JAXBElement<GetProfileSenderResponse> createGetProfileSenderResponse(GetProfileSenderResponse value) {
        return new JAXBElement<GetProfileSenderResponse>(_GetProfileSenderResponse_QNAME, GetProfileSenderResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FeeLookupBySendCountryRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "feeLookupBySendCountryRequest")
    public JAXBElement<FeeLookupBySendCountryRequest> createFeeLookupBySendCountryRequest(FeeLookupBySendCountryRequest value) {
        return new JAXBElement<FeeLookupBySendCountryRequest>(_FeeLookupBySendCountryRequest_QNAME, FeeLookupBySendCountryRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetServiceOptionsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getServiceOptionsResponse")
    public JAXBElement<GetServiceOptionsResponse> createGetServiceOptionsResponse(GetServiceOptionsResponse value) {
        return new JAXBElement<GetServiceOptionsResponse>(_GetServiceOptionsResponse_QNAME, GetServiceOptionsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FeeLookupResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "feeLookupResponse")
    public JAXBElement<FeeLookupResponse> createFeeLookupResponse(FeeLookupResponse value) {
        return new JAXBElement<FeeLookupResponse>(_FeeLookupResponse_QNAME, FeeLookupResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckInResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "checkInResponse")
    public JAXBElement<CheckInResponse> createCheckInResponse(CheckInResponse value) {
        return new JAXBElement<CheckInResponse>(_CheckInResponse_QNAME, CheckInResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TranslationsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "translationsResponse")
    public JAXBElement<TranslationsResponse> createTranslationsResponse(TranslationsResponse value) {
        return new JAXBElement<TranslationsResponse>(_TranslationsResponse_QNAME, TranslationsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FeeLookupRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "feeLookupRequest")
    public JAXBElement<FeeLookupRequest> createFeeLookupRequest(FeeLookupRequest value) {
        return new JAXBElement<FeeLookupRequest>(_FeeLookupRequest_QNAME, FeeLookupRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUCPByConsumerAttributesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getUCPByConsumerAttributesResponse")
    public JAXBElement<GetUCPByConsumerAttributesResponse> createGetUCPByConsumerAttributesResponse(GetUCPByConsumerAttributesResponse value) {
        return new JAXBElement<GetUCPByConsumerAttributesResponse>(_GetUCPByConsumerAttributesResponse_QNAME, GetUCPByConsumerAttributesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceiptsFormatDetailsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "receiptsFormatDetailsRequest")
    public JAXBElement<ReceiptsFormatDetailsRequest> createReceiptsFormatDetailsRequest(ReceiptsFormatDetailsRequest value) {
        return new JAXBElement<ReceiptsFormatDetailsRequest>(_ReceiptsFormatDetailsRequest_QNAME, ReceiptsFormatDetailsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VariableReceiptInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "variableReceiptInfoResponse")
    public JAXBElement<VariableReceiptInfoResponse> createVariableReceiptInfoResponse(VariableReceiptInfoResponse value) {
        return new JAXBElement<VariableReceiptInfoResponse>(_VariableReceiptInfoResponse_QNAME, VariableReceiptInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IndustryRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "industryRequest")
    public JAXBElement<IndustryRequest> createIndustryRequest(IndustryRequest value) {
        return new JAXBElement<IndustryRequest>(_IndustryRequest_QNAME, IndustryRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProfileChangeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "profileChangeResponse")
    public JAXBElement<ProfileChangeResponse> createProfileChangeResponse(ProfileChangeResponse value) {
        return new JAXBElement<ProfileChangeResponse>(_ProfileChangeResponse_QNAME, ProfileChangeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDepositBankListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getDepositBankListResponse")
    public JAXBElement<GetDepositBankListResponse> createGetDepositBankListResponse(GetDepositBankListResponse value) {
        return new JAXBElement<GetDepositBankListResponse>(_GetDepositBankListResponse_QNAME, GetDepositBankListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsumerHistoryLookupRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "consumerHistoryLookupRequest")
    public JAXBElement<ConsumerHistoryLookupRequest> createConsumerHistoryLookupRequest(ConsumerHistoryLookupRequest value) {
        return new JAXBElement<ConsumerHistoryLookupRequest>(_ConsumerHistoryLookupRequest_QNAME, ConsumerHistoryLookupRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InitialSetupRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "initialSetupRequest")
    public JAXBElement<InitialSetupRequest> createInitialSetupRequest(InitialSetupRequest value) {
        return new JAXBElement<InitialSetupRequest>(_InitialSetupRequest_QNAME, InitialSetupRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBroadcastMessagesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getBroadcastMessagesRequest")
    public JAXBElement<GetBroadcastMessagesRequest> createGetBroadcastMessagesRequest(GetBroadcastMessagesRequest value) {
        return new JAXBElement<GetBroadcastMessagesRequest>(_GetBroadcastMessagesRequest_QNAME, GetBroadcastMessagesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BillerSearchResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "billerSearchResponse")
    public JAXBElement<BillerSearchResponse> createBillerSearchResponse(BillerSearchResponse value) {
        return new JAXBElement<BillerSearchResponse>(_BillerSearchResponse_QNAME, BillerSearchResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateOrUpdateProfileReceiverRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "createOrUpdateProfileReceiverRequest")
    public JAXBElement<CreateOrUpdateProfileReceiverRequest> createCreateOrUpdateProfileReceiverRequest(CreateOrUpdateProfileReceiverRequest value) {
        return new JAXBElement<CreateOrUpdateProfileReceiverRequest>(_CreateOrUpdateProfileReceiverRequest_QNAME, CreateOrUpdateProfileReceiverRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetConsumerProfileTransactionHistoryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getConsumerProfileTransactionHistoryResponse")
    public JAXBElement<GetConsumerProfileTransactionHistoryResponse> createGetConsumerProfileTransactionHistoryResponse(GetConsumerProfileTransactionHistoryResponse value) {
        return new JAXBElement<GetConsumerProfileTransactionHistoryResponse>(_GetConsumerProfileTransactionHistoryResponse_QNAME, GetConsumerProfileTransactionHistoryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetConsumerProfileTransactionHistoryRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getConsumerProfileTransactionHistoryRequest")
    public JAXBElement<GetConsumerProfileTransactionHistoryRequest> createGetConsumerProfileTransactionHistoryRequest(GetConsumerProfileTransactionHistoryRequest value) {
        return new JAXBElement<GetConsumerProfileTransactionHistoryRequest>(_GetConsumerProfileTransactionHistoryRequest_QNAME, GetConsumerProfileTransactionHistoryRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FeeLookupBySendCountryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "feeLookupBySendCountryResponse")
    public JAXBElement<FeeLookupBySendCountryResponse> createFeeLookupBySendCountryResponse(FeeLookupBySendCountryResponse value) {
        return new JAXBElement<FeeLookupBySendCountryResponse>(_FeeLookupBySendCountryResponse_QNAME, FeeLookupBySendCountryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBankDetailsByLevelRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "getBankDetailsByLevelRequest")
    public JAXBElement<GetBankDetailsByLevelRequest> createGetBankDetailsByLevelRequest(GetBankDetailsByLevelRequest value) {
        return new JAXBElement<GetBankDetailsByLevelRequest>(_GetBankDetailsByLevelRequest_QNAME, GetBankDetailsByLevelRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendValidationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "sendValidationResponse")
    public JAXBElement<SendValidationResponse> createSendValidationResponse(SendValidationResponse value) {
        return new JAXBElement<SendValidationResponse>(_SendValidationResponse_QNAME, SendValidationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceiveValidationRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "receiveValidationRequest")
    public JAXBElement<ReceiveValidationRequest> createReceiveValidationRequest(ReceiveValidationRequest value) {
        return new JAXBElement<ReceiveValidationRequest>(_ReceiveValidationRequest_QNAME, ReceiveValidationRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TranslationsResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = TranslationsResponse.class)
    public JAXBElement<TranslationsResponse.Payload> createTranslationsResponsePayload(TranslationsResponse.Payload value) {
        return new JAXBElement<TranslationsResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, TranslationsResponse.Payload.class, TranslationsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllErrorsResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = GetAllErrorsResponse.class)
    public JAXBElement<GetAllErrorsResponse.Payload> createGetAllErrorsResponsePayload(GetAllErrorsResponse.Payload value) {
        return new JAXBElement<GetAllErrorsResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, GetAllErrorsResponse.Payload.class, GetAllErrorsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProfileResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = ProfileResponse.class)
    public JAXBElement<ProfileResponse.Payload> createProfileResponsePayload(ProfileResponse.Payload value) {
        return new JAXBElement<ProfileResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, ProfileResponse.Payload.class, ProfileResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConfirmTokenResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = ConfirmTokenResponse.class)
    public JAXBElement<ConfirmTokenResponse.Payload> createConfirmTokenResponsePayload(ConfirmTokenResponse.Payload value) {
        return new JAXBElement<ConfirmTokenResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, ConfirmTokenResponse.Payload.class, ConfirmTokenResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "value", scope = KeyValuePairType.class)
    public JAXBElement<String> createKeyValuePairTypeValue(String value) {
        return new JAXBElement<String>(_KeyValuePairTypeValue_QNAME, String.class, KeyValuePairType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendValidationResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = SendValidationResponse.class)
    public JAXBElement<SendValidationResponse.Payload> createSendValidationResponsePayload(SendValidationResponse.Payload value) {
        return new JAXBElement<SendValidationResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, SendValidationResponse.Payload.class, SendValidationResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProfileChangeResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = ProfileChangeResponse.class)
    public JAXBElement<ProfileChangeResponse.Payload> createProfileChangeResponsePayload(ProfileChangeResponse.Payload value) {
        return new JAXBElement<ProfileChangeResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, ProfileChangeResponse.Payload.class, ProfileChangeResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterHardTokenResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = RegisterHardTokenResponse.class)
    public JAXBElement<RegisterHardTokenResponse.Payload> createRegisterHardTokenResponsePayload(RegisterHardTokenResponse.Payload value) {
        return new JAXBElement<RegisterHardTokenResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, RegisterHardTokenResponse.Payload.class, RegisterHardTokenResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IndustryResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = IndustryResponse.class)
    public JAXBElement<IndustryResponse.Payload> createIndustryResponsePayload(IndustryResponse.Payload value) {
        return new JAXBElement<IndustryResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, IndustryResponse.Payload.class, IndustryResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchStagedTransactionsResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = SearchStagedTransactionsResponse.class)
    public JAXBElement<SearchStagedTransactionsResponse.Payload> createSearchStagedTransactionsResponsePayload(SearchStagedTransactionsResponse.Payload value) {
        return new JAXBElement<SearchStagedTransactionsResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, SearchStagedTransactionsResponse.Payload.class, SearchStagedTransactionsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionLookupResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = TransactionLookupResponse.class)
    public JAXBElement<TransactionLookupResponse.Payload> createTransactionLookupResponsePayload(TransactionLookupResponse.Payload value) {
        return new JAXBElement<TransactionLookupResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, TransactionLookupResponse.Payload.class, TransactionLookupResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrencyInfoResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = GetCurrencyInfoResponse.class)
    public JAXBElement<GetCurrencyInfoResponse.Payload> createGetCurrencyInfoResponsePayload(GetCurrencyInfoResponse.Payload value) {
        return new JAXBElement<GetCurrencyInfoResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, GetCurrencyInfoResponse.Payload.class, GetCurrencyInfoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BillPaymentSummaryReportResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = BillPaymentSummaryReportResponse.class)
    public JAXBElement<BillPaymentSummaryReportResponse.Payload> createBillPaymentSummaryReportResponsePayload(BillPaymentSummaryReportResponse.Payload value) {
        return new JAXBElement<BillPaymentSummaryReportResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, BillPaymentSummaryReportResponse.Payload.class, BillPaymentSummaryReportResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CityListResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = CityListResponse.class)
    public JAXBElement<CityListResponse.Payload> createCityListResponsePayload(CityListResponse.Payload value) {
        return new JAXBElement<CityListResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, CityListResponse.Payload.class, CityListResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProfileConsumerResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = GetProfileConsumerResponse.class)
    public JAXBElement<GetProfileConsumerResponse.Payload> createGetProfileConsumerResponsePayload(GetProfileConsumerResponse.Payload value) {
        return new JAXBElement<GetProfileConsumerResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, GetProfileConsumerResponse.Payload.class, GetProfileConsumerResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CompleteSessionResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = CompleteSessionResponse.class)
    public JAXBElement<CompleteSessionResponse.Payload> createCompleteSessionResponsePayload(CompleteSessionResponse.Payload value) {
        return new JAXBElement<CompleteSessionResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, CompleteSessionResponse.Payload.class, CompleteSessionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveConsumerProfileImageResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = SaveConsumerProfileImageResponse.class)
    public JAXBElement<SaveConsumerProfileImageResponse.Payload> createSaveConsumerProfileImageResponsePayload(SaveConsumerProfileImageResponse.Payload value) {
        return new JAXBElement<SaveConsumerProfileImageResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, SaveConsumerProfileImageResponse.Payload.class, SaveConsumerProfileImageResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDebugDataResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = GetDebugDataResponse.class)
    public JAXBElement<GetDebugDataResponse.Payload> createGetDebugDataResponsePayload(GetDebugDataResponse.Payload value) {
        return new JAXBElement<GetDebugDataResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, GetDebugDataResponse.Payload.class, GetDebugDataResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsumerHistoryLookupResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = ConsumerHistoryLookupResponse.class)
    public JAXBElement<ConsumerHistoryLookupResponse.Payload> createConsumerHistoryLookupResponsePayload(ConsumerHistoryLookupResponse.Payload value) {
        return new JAXBElement<ConsumerHistoryLookupResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, ConsumerHistoryLookupResponse.Payload.class, ConsumerHistoryLookupResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DirectoryOfAgentsByCityResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = DirectoryOfAgentsByCityResponse.class)
    public JAXBElement<DirectoryOfAgentsByCityResponse.Payload> createDirectoryOfAgentsByCityResponsePayload(DirectoryOfAgentsByCityResponse.Payload value) {
        return new JAXBElement<DirectoryOfAgentsByCityResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, DirectoryOfAgentsByCityResponse.Payload.class, DirectoryOfAgentsByCityResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBankDetailsResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = GetBankDetailsResponse.class)
    public JAXBElement<GetBankDetailsResponse.Payload> createGetBankDetailsResponsePayload(GetBankDetailsResponse.Payload value) {
        return new JAXBElement<GetBankDetailsResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, GetBankDetailsResponse.Payload.class, GetBankDetailsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCountrySubdivisionResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = GetCountrySubdivisionResponse.class)
    public JAXBElement<GetCountrySubdivisionResponse.Payload> createGetCountrySubdivisionResponsePayload(GetCountrySubdivisionResponse.Payload value) {
        return new JAXBElement<GetCountrySubdivisionResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, GetCountrySubdivisionResponse.Payload.class, GetCountrySubdivisionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FeeLookupBySendCountryResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = FeeLookupBySendCountryResponse.class)
    public JAXBElement<FeeLookupBySendCountryResponse.Payload> createFeeLookupBySendCountryResponsePayload(FeeLookupBySendCountryResponse.Payload value) {
        return new JAXBElement<FeeLookupBySendCountryResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, FeeLookupBySendCountryResponse.Payload.class, FeeLookupBySendCountryResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DirectoryOfAgentsByAreaCodePrefixResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = DirectoryOfAgentsByAreaCodePrefixResponse.class)
    public JAXBElement<DirectoryOfAgentsByAreaCodePrefixResponse.Payload> createDirectoryOfAgentsByAreaCodePrefixResponsePayload(DirectoryOfAgentsByAreaCodePrefixResponse.Payload value) {
        return new JAXBElement<DirectoryOfAgentsByAreaCodePrefixResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, DirectoryOfAgentsByAreaCodePrefixResponse.Payload.class, DirectoryOfAgentsByAreaCodePrefixResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DwProfileResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = DwProfileResponse.class)
    public JAXBElement<DwProfileResponse.Payload> createDwProfileResponsePayload(DwProfileResponse.Payload value) {
        return new JAXBElement<DwProfileResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, DwProfileResponse.Payload.class, DwProfileResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InitialSetupResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = InitialSetupResponse.class)
    public JAXBElement<InitialSetupResponse.Payload> createInitialSetupResponsePayload(InitialSetupResponse.Payload value) {
        return new JAXBElement<InitialSetupResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, InitialSetupResponse.Payload.class, InitialSetupResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoneyGramReceiveSummaryReportResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = MoneyGramReceiveSummaryReportResponse.class)
    public JAXBElement<MoneyGramReceiveSummaryReportResponse.Payload> createMoneyGramReceiveSummaryReportResponsePayload(MoneyGramReceiveSummaryReportResponse.Payload value) {
        return new JAXBElement<MoneyGramReceiveSummaryReportResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, MoneyGramReceiveSummaryReportResponse.Payload.class, MoneyGramReceiveSummaryReportResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenOTPLoginResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = OpenOTPLoginResponse.class)
    public JAXBElement<OpenOTPLoginResponse.Payload> createOpenOTPLoginResponsePayload(OpenOTPLoginResponse.Payload value) {
        return new JAXBElement<OpenOTPLoginResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, OpenOTPLoginResponse.Payload.class, OpenOTPLoginResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceiveReversalValidationResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = ReceiveReversalValidationResponse.class)
    public JAXBElement<ReceiveReversalValidationResponse.Payload> createReceiveReversalValidationResponsePayload(ReceiveReversalValidationResponse.Payload value) {
        return new JAXBElement<ReceiveReversalValidationResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, ReceiveReversalValidationResponse.Payload.class, ReceiveReversalValidationResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoneyGramSendDetailReportWithTaxResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = MoneyGramSendDetailReportWithTaxResponse.class)
    public JAXBElement<MoneyGramSendDetailReportWithTaxResponse.Payload> createMoneyGramSendDetailReportWithTaxResponsePayload(MoneyGramSendDetailReportWithTaxResponse.Payload value) {
        return new JAXBElement<MoneyGramSendDetailReportWithTaxResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, MoneyGramSendDetailReportWithTaxResponse.Payload.class, MoneyGramSendDetailReportWithTaxResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveProfileResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = SaveProfileResponse.class)
    public JAXBElement<SaveProfileResponse.Payload> createSaveProfileResponsePayload(SaveProfileResponse.Payload value) {
        return new JAXBElement<SaveProfileResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, SaveProfileResponse.Payload.class, SaveProfileResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DwPasswordResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = DwPasswordResponse.class)
    public JAXBElement<DwPasswordResponse.Payload> createDwPasswordResponsePayload(DwPasswordResponse.Payload value) {
        return new JAXBElement<DwPasswordResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, DwPasswordResponse.Payload.class, DwPasswordResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CategoryInfo.Infos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "infos", scope = CategoryInfo.class)
    public JAXBElement<CategoryInfo.Infos> createCategoryInfoInfos(CategoryInfo.Infos value) {
        return new JAXBElement<CategoryInfo.Infos>(_CategoryInfoInfos_QNAME, CategoryInfo.Infos.class, CategoryInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoneyOrderTotalResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = MoneyOrderTotalResponse.class)
    public JAXBElement<MoneyOrderTotalResponse.Payload> createMoneyOrderTotalResponsePayload(MoneyOrderTotalResponse.Payload value) {
        return new JAXBElement<MoneyOrderTotalResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, MoneyOrderTotalResponse.Payload.class, MoneyOrderTotalResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DepositAnnouncementResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = DepositAnnouncementResponse.class)
    public JAXBElement<DepositAnnouncementResponse.Payload> createDepositAnnouncementResponsePayload(DepositAnnouncementResponse.Payload value) {
        return new JAXBElement<DepositAnnouncementResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, DepositAnnouncementResponse.Payload.class, DepositAnnouncementResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDepositInformationResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = GetDepositInformationResponse.class)
    public JAXBElement<GetDepositInformationResponse.Payload> createGetDepositInformationResponsePayload(GetDepositInformationResponse.Payload value) {
        return new JAXBElement<GetDepositInformationResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, GetDepositInformationResponse.Payload.class, GetDepositInformationResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VariableReceiptInfoResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = VariableReceiptInfoResponse.class)
    public JAXBElement<VariableReceiptInfoResponse.Payload> createVariableReceiptInfoResponsePayload(VariableReceiptInfoResponse.Payload value) {
        return new JAXBElement<VariableReceiptInfoResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, VariableReceiptInfoResponse.Payload.class, VariableReceiptInfoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ComplianceTransactionResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = ComplianceTransactionResponse.class)
    public JAXBElement<ComplianceTransactionResponse.Payload> createComplianceTransactionResponsePayload(ComplianceTransactionResponse.Payload value) {
        return new JAXBElement<ComplianceTransactionResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, ComplianceTransactionResponse.Payload.class, ComplianceTransactionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProfileSenderResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = GetProfileSenderResponse.class)
    public JAXBElement<GetProfileSenderResponse.Payload> createGetProfileSenderResponsePayload(GetProfileSenderResponse.Payload value) {
        return new JAXBElement<GetProfileSenderResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, GetProfileSenderResponse.Payload.class, GetProfileSenderResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PromotionLookupByCodeResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = PromotionLookupByCodeResponse.class)
    public JAXBElement<PromotionLookupByCodeResponse.Payload> createPromotionLookupByCodeResponsePayload(PromotionLookupByCodeResponse.Payload value) {
        return new JAXBElement<PromotionLookupByCodeResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, PromotionLookupByCodeResponse.Payload.class, PromotionLookupByCodeResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoneyGramReceiveDetailReportResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = MoneyGramReceiveDetailReportResponse.class)
    public JAXBElement<MoneyGramReceiveDetailReportResponse.Payload> createMoneyGramReceiveDetailReportResponsePayload(MoneyGramReceiveDetailReportResponse.Payload value) {
        return new JAXBElement<MoneyGramReceiveDetailReportResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, MoneyGramReceiveDetailReportResponse.Payload.class, MoneyGramReceiveDetailReportResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckInResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = CheckInResponse.class)
    public JAXBElement<CheckInResponse.Payload> createCheckInResponsePayload(CheckInResponse.Payload value) {
        return new JAXBElement<CheckInResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, CheckInResponse.Payload.class, CheckInResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BillerSearchResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = BillerSearchResponse.class)
    public JAXBElement<BillerSearchResponse.Payload> createBillerSearchResponsePayload(BillerSearchResponse.Payload value) {
        return new JAXBElement<BillerSearchResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, BillerSearchResponse.Payload.class, BillerSearchResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetServiceOptionsResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = GetServiceOptionsResponse.class)
    public JAXBElement<GetServiceOptionsResponse.Payload> createGetServiceOptionsResponsePayload(GetServiceOptionsResponse.Payload value) {
        return new JAXBElement<GetServiceOptionsResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, GetServiceOptionsResponse.Payload.class, GetServiceOptionsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AmendValidationResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = AmendValidationResponse.class)
    public JAXBElement<AmendValidationResponse.Payload> createAmendValidationResponsePayload(AmendValidationResponse.Payload value) {
        return new JAXBElement<AmendValidationResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, AmendValidationResponse.Payload.class, AmendValidationResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoneyGramSendSummaryReportResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = MoneyGramSendSummaryReportResponse.class)
    public JAXBElement<MoneyGramSendSummaryReportResponse.Payload> createMoneyGramSendSummaryReportResponsePayload(MoneyGramSendSummaryReportResponse.Payload value) {
        return new JAXBElement<MoneyGramSendSummaryReportResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, MoneyGramSendSummaryReportResponse.Payload.class, MoneyGramSendSummaryReportResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllFieldsResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = GetAllFieldsResponse.class)
    public JAXBElement<GetAllFieldsResponse.Payload> createGetAllFieldsResponsePayload(GetAllFieldsResponse.Payload value) {
        return new JAXBElement<GetAllFieldsResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, GetAllFieldsResponse.Payload.class, GetAllFieldsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateOrUpdateProfileConsumerResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = CreateOrUpdateProfileConsumerResponse.class)
    public JAXBElement<CreateOrUpdateProfileConsumerResponse.Payload> createCreateOrUpdateProfileConsumerResponsePayload(CreateOrUpdateProfileConsumerResponse.Payload value) {
        return new JAXBElement<CreateOrUpdateProfileConsumerResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, CreateOrUpdateProfileConsumerResponse.Payload.class, CreateOrUpdateProfileConsumerResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendReversalValidationResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = SendReversalValidationResponse.class)
    public JAXBElement<SendReversalValidationResponse.Payload> createSendReversalValidationResponsePayload(SendReversalValidationResponse.Payload value) {
        return new JAXBElement<SendReversalValidationResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, SendReversalValidationResponse.Payload.class, SendReversalValidationResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateOrUpdateProfileSenderResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = CreateOrUpdateProfileSenderResponse.class)
    public JAXBElement<CreateOrUpdateProfileSenderResponse.Payload> createCreateOrUpdateProfileSenderResponsePayload(CreateOrUpdateProfileSenderResponse.Payload value) {
        return new JAXBElement<CreateOrUpdateProfileSenderResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, CreateOrUpdateProfileSenderResponse.Payload.class, CreateOrUpdateProfileSenderResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetConsumerProfileTransactionHistoryResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = GetConsumerProfileTransactionHistoryResponse.class)
    public JAXBElement<GetConsumerProfileTransactionHistoryResponse.Payload> createGetConsumerProfileTransactionHistoryResponsePayload(GetConsumerProfileTransactionHistoryResponse.Payload value) {
        return new JAXBElement<GetConsumerProfileTransactionHistoryResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, GetConsumerProfileTransactionHistoryResponse.Payload.class, GetConsumerProfileTransactionHistoryResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubagentsResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = SubagentsResponse.class)
    public JAXBElement<SubagentsResponse.Payload> createSubagentsResponsePayload(SubagentsResponse.Payload value) {
        return new JAXBElement<SubagentsResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, SubagentsResponse.Payload.class, SubagentsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetReceiptForReprintResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = GetReceiptForReprintResponse.class)
    public JAXBElement<GetReceiptForReprintResponse.Payload> createGetReceiptForReprintResponsePayload(GetReceiptForReprintResponse.Payload value) {
        return new JAXBElement<GetReceiptForReprintResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, GetReceiptForReprintResponse.Payload.class, GetReceiptForReprintResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUCPByConsumerAttributesResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = GetUCPByConsumerAttributesResponse.class)
    public JAXBElement<GetUCPByConsumerAttributesResponse.Payload> createGetUCPByConsumerAttributesResponsePayload(GetUCPByConsumerAttributesResponse.Payload value) {
        return new JAXBElement<GetUCPByConsumerAttributesResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, GetUCPByConsumerAttributesResponse.Payload.class, GetUCPByConsumerAttributesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InjectedInstructionResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = InjectedInstructionResponse.class)
    public JAXBElement<InjectedInstructionResponse.Payload> createInjectedInstructionResponsePayload(InjectedInstructionResponse.Payload value) {
        return new JAXBElement<InjectedInstructionResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, InjectedInstructionResponse.Payload.class, InjectedInstructionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBankDetailsByLevelResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = GetBankDetailsByLevelResponse.class)
    public JAXBElement<GetBankDetailsByLevelResponse.Payload> createGetBankDetailsByLevelResponsePayload(GetBankDetailsByLevelResponse.Payload value) {
        return new JAXBElement<GetBankDetailsByLevelResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, GetBankDetailsByLevelResponse.Payload.class, GetBankDetailsByLevelResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchConsumerProfilesResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = SearchConsumerProfilesResponse.class)
    public JAXBElement<SearchConsumerProfilesResponse.Payload> createSearchConsumerProfilesResponsePayload(SearchConsumerProfilesResponse.Payload value) {
        return new JAXBElement<SearchConsumerProfilesResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, SearchConsumerProfilesResponse.Payload.class, SearchConsumerProfilesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCountryInfoResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = GetCountryInfoResponse.class)
    public JAXBElement<GetCountryInfoResponse.Payload> createGetCountryInfoResponsePayload(GetCountryInfoResponse.Payload value) {
        return new JAXBElement<GetCountryInfoResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, GetCountryInfoResponse.Payload.class, GetCountryInfoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBroadcastMessagesResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = GetBroadcastMessagesResponse.class)
    public JAXBElement<GetBroadcastMessagesResponse.Payload> createGetBroadcastMessagesResponsePayload(GetBroadcastMessagesResponse.Payload value) {
        return new JAXBElement<GetBroadcastMessagesResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, GetBroadcastMessagesResponse.Payload.class, GetBroadcastMessagesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BPValidationResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = BPValidationResponse.class)
    public JAXBElement<BPValidationResponse.Payload> createBPValidationResponsePayload(BPValidationResponse.Payload value) {
        return new JAXBElement<BPValidationResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, BPValidationResponse.Payload.class, BPValidationResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceiptsFormatDetailsResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = ReceiptsFormatDetailsResponse.class)
    public JAXBElement<ReceiptsFormatDetailsResponse.Payload> createReceiptsFormatDetailsResponsePayload(ReceiptsFormatDetailsResponse.Payload value) {
        return new JAXBElement<ReceiptsFormatDetailsResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, ReceiptsFormatDetailsResponse.Payload.class, ReceiptsFormatDetailsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DirectoryOfAgentsByZipResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = DirectoryOfAgentsByZipResponse.class)
    public JAXBElement<DirectoryOfAgentsByZipResponse.Payload> createDirectoryOfAgentsByZipResponsePayload(DirectoryOfAgentsByZipResponse.Payload value) {
        return new JAXBElement<DirectoryOfAgentsByZipResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, DirectoryOfAgentsByZipResponse.Payload.class, DirectoryOfAgentsByZipResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProfileReceiverResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = GetProfileReceiverResponse.class)
    public JAXBElement<GetProfileReceiverResponse.Payload> createGetProfileReceiverResponsePayload(GetProfileReceiverResponse.Payload value) {
        return new JAXBElement<GetProfileReceiverResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, GetProfileReceiverResponse.Payload.class, GetProfileReceiverResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateOrUpdateProfileReceiverResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = CreateOrUpdateProfileReceiverResponse.class)
    public JAXBElement<CreateOrUpdateProfileReceiverResponse.Payload> createCreateOrUpdateProfileReceiverResponsePayload(CreateOrUpdateProfileReceiverResponse.Payload value) {
        return new JAXBElement<CreateOrUpdateProfileReceiverResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, CreateOrUpdateProfileReceiverResponse.Payload.class, CreateOrUpdateProfileReceiverResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveSubagentsResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = SaveSubagentsResponse.class)
    public JAXBElement<SaveSubagentsResponse.Payload> createSaveSubagentsResponsePayload(SaveSubagentsResponse.Payload value) {
        return new JAXBElement<SaveSubagentsResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, SaveSubagentsResponse.Payload.class, SaveSubagentsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoneyGramSendDetailReportResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = MoneyGramSendDetailReportResponse.class)
    public JAXBElement<MoneyGramSendDetailReportResponse.Payload> createMoneyGramSendDetailReportResponsePayload(MoneyGramSendDetailReportResponse.Payload value) {
        return new JAXBElement<MoneyGramSendDetailReportResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, MoneyGramSendDetailReportResponse.Payload.class, MoneyGramSendDetailReportResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DisclosureTextDetailsResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = DisclosureTextDetailsResponse.class)
    public JAXBElement<DisclosureTextDetailsResponse.Payload> createDisclosureTextDetailsResponsePayload(DisclosureTextDetailsResponse.Payload value) {
        return new JAXBElement<DisclosureTextDetailsResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, DisclosureTextDetailsResponse.Payload.class, DisclosureTextDetailsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDebugDataResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = SaveDebugDataResponse.class)
    public JAXBElement<SaveDebugDataResponse.Payload> createSaveDebugDataResponsePayload(SaveDebugDataResponse.Payload value) {
        return new JAXBElement<SaveDebugDataResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, SaveDebugDataResponse.Payload.class, SaveDebugDataResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReceiveValidationResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = ReceiveValidationResponse.class)
    public JAXBElement<ReceiveValidationResponse.Payload> createReceiveValidationResponsePayload(ReceiveValidationResponse.Payload value) {
        return new JAXBElement<ReceiveValidationResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, ReceiveValidationResponse.Payload.class, ReceiveValidationResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DwInitialSetupResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = DwInitialSetupResponse.class)
    public JAXBElement<DwInitialSetupResponse.Payload> createDwInitialSetupResponsePayload(DwInitialSetupResponse.Payload value) {
        return new JAXBElement<DwInitialSetupResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, DwInitialSetupResponse.Payload.class, DwInitialSetupResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FeeLookupResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = FeeLookupResponse.class)
    public JAXBElement<FeeLookupResponse.Payload> createFeeLookupResponsePayload(FeeLookupResponse.Payload value) {
        return new JAXBElement<FeeLookupResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, FeeLookupResponse.Payload.class, FeeLookupResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionManifestResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = VersionManifestResponse.class)
    public JAXBElement<VersionManifestResponse.Payload> createVersionManifestResponsePayload(VersionManifestResponse.Payload value) {
        return new JAXBElement<VersionManifestResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, VersionManifestResponse.Payload.class, VersionManifestResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BillPaymentDetailReportResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = BillPaymentDetailReportResponse.class)
    public JAXBElement<BillPaymentDetailReportResponse.Payload> createBillPaymentDetailReportResponsePayload(BillPaymentDetailReportResponse.Payload value) {
        return new JAXBElement<BillPaymentDetailReportResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, BillPaymentDetailReportResponse.Payload.class, BillPaymentDetailReportResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDepositBankListResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = GetDepositBankListResponse.class)
    public JAXBElement<GetDepositBankListResponse.Payload> createGetDepositBankListResponsePayload(GetDepositBankListResponse.Payload value) {
        return new JAXBElement<GetDepositBankListResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, GetDepositBankListResponse.Payload.class, GetDepositBankListResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DoddFrankStateRegulatorInfoResponse.Payload }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.moneygram.com/AgentConnect1705", name = "payload", scope = DoddFrankStateRegulatorInfoResponse.class)
    public JAXBElement<DoddFrankStateRegulatorInfoResponse.Payload> createDoddFrankStateRegulatorInfoResponsePayload(DoddFrankStateRegulatorInfoResponse.Payload value) {
        return new JAXBElement<DoddFrankStateRegulatorInfoResponse.Payload>(_GetEnumerationsResponsePayload_QNAME, DoddFrankStateRegulatorInfoResponse.Payload.class, DoddFrankStateRegulatorInfoResponse.class, value);
    }

}
