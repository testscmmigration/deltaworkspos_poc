
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetUCPByConsumerAttributesResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetUCPByConsumerAttributesResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="consumerProfile" type="{http://www.moneygram.com/AgentConnect1705}UCPResponseProfileType"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetUCPByConsumerAttributesResponse", propOrder = {
    "payload"
})
public class GetUCPByConsumerAttributesResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<GetUCPByConsumerAttributesResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetUCPByConsumerAttributesResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<GetUCPByConsumerAttributesResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetUCPByConsumerAttributesResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<GetUCPByConsumerAttributesResponse.Payload> value) {
        this.payload = ((JAXBElement<GetUCPByConsumerAttributesResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="consumerProfile" type="{http://www.moneygram.com/AgentConnect1705}UCPResponseProfileType"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "consumerProfile"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(required = true)
        protected UCPResponseProfileType consumerProfile;

        /**
         * Gets the value of the consumerProfile property.
         * 
         * @return
         *     possible object is
         *     {@link UCPResponseProfileType }
         *     
         */
        public UCPResponseProfileType getConsumerProfile() {
            return consumerProfile;
        }

        /**
         * Sets the value of the consumerProfile property.
         * 
         * @param value
         *     allowed object is
         *     {@link UCPResponseProfileType }
         *     
         */
        public void setConsumerProfile(UCPResponseProfileType value) {
            this.consumerProfile = value;
        }

    }

}
