
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConsumerHistoryLookupResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConsumerHistoryLookupResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="senderInfos" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="senderInfo" type="{http://www.moneygram.com/AgentConnect1705}SenderLookupInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                             &lt;element name="infos" type="{http://www.moneygram.com/AgentConnect1705}InfosType" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsumerHistoryLookupResponse", propOrder = {
    "payload"
})
public class ConsumerHistoryLookupResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<ConsumerHistoryLookupResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ConsumerHistoryLookupResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<ConsumerHistoryLookupResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ConsumerHistoryLookupResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<ConsumerHistoryLookupResponse.Payload> value) {
        this.payload = ((JAXBElement<ConsumerHistoryLookupResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="senderInfos" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="senderInfo" type="{http://www.moneygram.com/AgentConnect1705}SenderLookupInfo" maxOccurs="unbounded" minOccurs="0"/>
     *                   &lt;element name="infos" type="{http://www.moneygram.com/AgentConnect1705}InfosType" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "senderInfos"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected ConsumerHistoryLookupResponse.Payload.SenderInfos senderInfos;

        /**
         * Gets the value of the senderInfos property.
         * 
         * @return
         *     possible object is
         *     {@link ConsumerHistoryLookupResponse.Payload.SenderInfos }
         *     
         */
        public ConsumerHistoryLookupResponse.Payload.SenderInfos getSenderInfos() {
            return senderInfos;
        }

        /**
         * Sets the value of the senderInfos property.
         * 
         * @param value
         *     allowed object is
         *     {@link ConsumerHistoryLookupResponse.Payload.SenderInfos }
         *     
         */
        public void setSenderInfos(ConsumerHistoryLookupResponse.Payload.SenderInfos value) {
            this.senderInfos = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="senderInfo" type="{http://www.moneygram.com/AgentConnect1705}SenderLookupInfo" maxOccurs="unbounded" minOccurs="0"/>
         *         &lt;element name="infos" type="{http://www.moneygram.com/AgentConnect1705}InfosType" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "senderInfo",
            "infos"
        })
        public static class SenderInfos
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected List<SenderLookupInfo> senderInfo;
            protected InfosType infos;

            /**
             * Gets the value of the senderInfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the senderInfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getSenderInfo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link SenderLookupInfo }
             * 
             * 
             */
            public List<SenderLookupInfo> getSenderInfo() {
                if (senderInfo == null) {
                    senderInfo = new ArrayList<SenderLookupInfo>();
                }
                return this.senderInfo;
            }

            /**
             * Gets the value of the infos property.
             * 
             * @return
             *     possible object is
             *     {@link InfosType }
             *     
             */
            public InfosType getInfos() {
                return infos;
            }

            /**
             * Sets the value of the infos property.
             * 
             * @param value
             *     allowed object is
             *     {@link InfosType }
             *     
             */
            public void setInfos(InfosType value) {
                this.infos = value;
            }

        }

    }

}
