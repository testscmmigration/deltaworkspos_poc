
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for IdentificationDocumentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IdentificationDocumentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="endDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="expirationDay" type="{http://www.moneygram.com/AgentConnect1705}dayType" minOccurs="0"/>
 *         &lt;element name="expirationMonth" type="{http://www.moneygram.com/AgentConnect1705}monthType" minOccurs="0"/>
 *         &lt;element name="expirationYear" type="{http://www.moneygram.com/AgentConnect1705}yearType" minOccurs="0"/>
 *         &lt;element name="idIssuerAuthorityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idNumber" type="{http://www.moneygram.com/AgentConnect1705}StringMax20Type" minOccurs="0"/>
 *         &lt;element name="issueCity" type="{http://www.moneygram.com/AgentConnect1705}CityType" minOccurs="0"/>
 *         &lt;element name="issueCountryCode" type="{http://www.moneygram.com/AgentConnect1705}CountryCodeType" minOccurs="0"/>
 *         &lt;element name="issueCountrySubDivisionCode" type="{http://www.moneygram.com/AgentConnect1705}StringMax7Type" minOccurs="0"/>
 *         &lt;element name="issueDay" type="{http://www.moneygram.com/AgentConnect1705}dayType" minOccurs="0"/>
 *         &lt;element name="issueMonth" type="{http://www.moneygram.com/AgentConnect1705}monthType" minOccurs="0"/>
 *         &lt;element name="issueYear" type="{http://www.moneygram.com/AgentConnect1705}yearType" minOccurs="0"/>
 *         &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="stronglyAuthenticated" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentificationDocumentType", propOrder = {
    "endDate",
    "expirationDay",
    "expirationMonth",
    "expirationYear",
    "idIssuerAuthorityCode",
    "idNumber",
    "issueCity",
    "issueCountryCode",
    "issueCountrySubDivisionCode",
    "issueDay",
    "issueMonth",
    "issueYear",
    "startDate",
    "stronglyAuthenticated",
    "type"
})
public class IdentificationDocumentType
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar endDate;
    protected String expirationDay;
    protected String expirationMonth;
    protected String expirationYear;
    protected String idIssuerAuthorityCode;
    protected String idNumber;
    protected String issueCity;
    protected String issueCountryCode;
    protected String issueCountrySubDivisionCode;
    protected String issueDay;
    protected String issueMonth;
    protected String issueYear;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startDate;
    protected Boolean stronglyAuthenticated;
    protected String type;

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the expirationDay property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpirationDay() {
        return expirationDay;
    }

    /**
     * Sets the value of the expirationDay property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpirationDay(String value) {
        this.expirationDay = value;
    }

    /**
     * Gets the value of the expirationMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpirationMonth() {
        return expirationMonth;
    }

    /**
     * Sets the value of the expirationMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpirationMonth(String value) {
        this.expirationMonth = value;
    }

    /**
     * Gets the value of the expirationYear property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpirationYear() {
        return expirationYear;
    }

    /**
     * Sets the value of the expirationYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpirationYear(String value) {
        this.expirationYear = value;
    }

    /**
     * Gets the value of the idIssuerAuthorityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdIssuerAuthorityCode() {
        return idIssuerAuthorityCode;
    }

    /**
     * Sets the value of the idIssuerAuthorityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdIssuerAuthorityCode(String value) {
        this.idIssuerAuthorityCode = value;
    }

    /**
     * Gets the value of the idNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdNumber() {
        return idNumber;
    }

    /**
     * Sets the value of the idNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdNumber(String value) {
        this.idNumber = value;
    }

    /**
     * Gets the value of the issueCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssueCity() {
        return issueCity;
    }

    /**
     * Sets the value of the issueCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssueCity(String value) {
        this.issueCity = value;
    }

    /**
     * Gets the value of the issueCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssueCountryCode() {
        return issueCountryCode;
    }

    /**
     * Sets the value of the issueCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssueCountryCode(String value) {
        this.issueCountryCode = value;
    }

    /**
     * Gets the value of the issueCountrySubDivisionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssueCountrySubDivisionCode() {
        return issueCountrySubDivisionCode;
    }

    /**
     * Sets the value of the issueCountrySubDivisionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssueCountrySubDivisionCode(String value) {
        this.issueCountrySubDivisionCode = value;
    }

    /**
     * Gets the value of the issueDay property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssueDay() {
        return issueDay;
    }

    /**
     * Sets the value of the issueDay property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssueDay(String value) {
        this.issueDay = value;
    }

    /**
     * Gets the value of the issueMonth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssueMonth() {
        return issueMonth;
    }

    /**
     * Sets the value of the issueMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssueMonth(String value) {
        this.issueMonth = value;
    }

    /**
     * Gets the value of the issueYear property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssueYear() {
        return issueYear;
    }

    /**
     * Sets the value of the issueYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssueYear(String value) {
        this.issueYear = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the stronglyAuthenticated property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStronglyAuthenticated() {
        return stronglyAuthenticated;
    }

    /**
     * Sets the value of the stronglyAuthenticated property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStronglyAuthenticated(Boolean value) {
        this.stronglyAuthenticated = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

}
