
package com.moneygram.agentconnect;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransactionStatusType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TransactionStatusType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="AVAIL"/>
 *     &lt;enumeration value="CANCL"/>
 *     &lt;enumeration value="RECVD"/>
 *     &lt;enumeration value="REFND"/>
 *     &lt;enumeration value="AFR"/>
 *     &lt;enumeration value="UNCOMMITED"/>
 *     &lt;enumeration value="PRCSS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TransactionStatusType")
@XmlEnum
public enum TransactionStatusType {


    /**
     * 
     * 						Funds are available to be received
     * 					
     * 
     */
    AVAIL,

    /**
     * 
     * 						Transaction has been cancelled
     * 					
     * 
     */
    CANCL,

    /**
     * 
     * 						Funds have been picked up by the receiver 
     * 					
     * 
     */
    RECVD,

    /**
     * 
     * 						Transaction has been refunded to the sender
     * 					
     * 
     */
    REFND,

    /**
     * 
     * 						Transaction is available for refund to the sender. It can no longer be received.
     * 						For example, this may happen in an account deposit scenario when the account is closed.
     * 					
     * 
     */
    AFR,

    /**
     * 
     * 						Transaction has not been committed (via the completeSession). MGI considers these transactions to be unfunded.
     * 						FormFree staged transactions also fall into this category until they are completed at the POE.
     * 					
     * 
     */
    UNCOMMITED,

    /**
     * 
     * 						Transaction is being processed and may not yet be available for receive.
     * 					
     * 
     */
    PRCSS;

    public String value() {
        return name();
    }

    public static TransactionStatusType fromValue(String v) {
        return valueOf(v);
    }

}
