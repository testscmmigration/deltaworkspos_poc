
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EstimatedReceiveAmountInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EstimatedReceiveAmountInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="receiveAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountNonZeroType" minOccurs="0"/>
 *         &lt;element name="receiveCurrency" type="{http://www.moneygram.com/AgentConnect1705}CurrencyCodeType" minOccurs="0"/>
 *         &lt;element name="validCurrencyIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="displayPayoutCurrency" type="{http://www.moneygram.com/AgentConnect1705}CurrencyCodeType"/>
 *         &lt;element name="totalReceiveFees" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType" minOccurs="0"/>
 *         &lt;element name="totalReceiveTaxes" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType" minOccurs="0"/>
 *         &lt;element name="totalReceiveAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType" minOccurs="0"/>
 *         &lt;element name="receiveFeesAreEstimated" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="receiveTaxesAreEstimated" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="detailEstimatedReceiveAmounts" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="detailEstimatedReceiveAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EstimatedReceiveAmountInfo", propOrder = {
    "receiveAmount",
    "receiveCurrency",
    "validCurrencyIndicator",
    "displayPayoutCurrency",
    "totalReceiveFees",
    "totalReceiveTaxes",
    "totalReceiveAmount",
    "receiveFeesAreEstimated",
    "receiveTaxesAreEstimated",
    "detailEstimatedReceiveAmounts"
})
public class EstimatedReceiveAmountInfo
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected BigDecimal receiveAmount;
    protected String receiveCurrency;
    protected boolean validCurrencyIndicator;
    @XmlElement(required = true)
    protected String displayPayoutCurrency;
    protected BigDecimal totalReceiveFees;
    protected BigDecimal totalReceiveTaxes;
    protected BigDecimal totalReceiveAmount;
    protected boolean receiveFeesAreEstimated;
    protected boolean receiveTaxesAreEstimated;
    protected EstimatedReceiveAmountInfo.DetailEstimatedReceiveAmounts detailEstimatedReceiveAmounts;

    /**
     * Gets the value of the receiveAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getReceiveAmount() {
        return receiveAmount;
    }

    /**
     * Sets the value of the receiveAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setReceiveAmount(BigDecimal value) {
        this.receiveAmount = value;
    }

    /**
     * Gets the value of the receiveCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiveCurrency() {
        return receiveCurrency;
    }

    /**
     * Sets the value of the receiveCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiveCurrency(String value) {
        this.receiveCurrency = value;
    }

    /**
     * Gets the value of the validCurrencyIndicator property.
     * 
     */
    public boolean isValidCurrencyIndicator() {
        return validCurrencyIndicator;
    }

    /**
     * Sets the value of the validCurrencyIndicator property.
     * 
     */
    public void setValidCurrencyIndicator(boolean value) {
        this.validCurrencyIndicator = value;
    }

    /**
     * Gets the value of the displayPayoutCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisplayPayoutCurrency() {
        return displayPayoutCurrency;
    }

    /**
     * Sets the value of the displayPayoutCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisplayPayoutCurrency(String value) {
        this.displayPayoutCurrency = value;
    }

    /**
     * Gets the value of the totalReceiveFees property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalReceiveFees() {
        return totalReceiveFees;
    }

    /**
     * Sets the value of the totalReceiveFees property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalReceiveFees(BigDecimal value) {
        this.totalReceiveFees = value;
    }

    /**
     * Gets the value of the totalReceiveTaxes property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalReceiveTaxes() {
        return totalReceiveTaxes;
    }

    /**
     * Sets the value of the totalReceiveTaxes property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalReceiveTaxes(BigDecimal value) {
        this.totalReceiveTaxes = value;
    }

    /**
     * Gets the value of the totalReceiveAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalReceiveAmount() {
        return totalReceiveAmount;
    }

    /**
     * Sets the value of the totalReceiveAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalReceiveAmount(BigDecimal value) {
        this.totalReceiveAmount = value;
    }

    /**
     * Gets the value of the receiveFeesAreEstimated property.
     * 
     */
    public boolean isReceiveFeesAreEstimated() {
        return receiveFeesAreEstimated;
    }

    /**
     * Sets the value of the receiveFeesAreEstimated property.
     * 
     */
    public void setReceiveFeesAreEstimated(boolean value) {
        this.receiveFeesAreEstimated = value;
    }

    /**
     * Gets the value of the receiveTaxesAreEstimated property.
     * 
     */
    public boolean isReceiveTaxesAreEstimated() {
        return receiveTaxesAreEstimated;
    }

    /**
     * Sets the value of the receiveTaxesAreEstimated property.
     * 
     */
    public void setReceiveTaxesAreEstimated(boolean value) {
        this.receiveTaxesAreEstimated = value;
    }

    /**
     * Gets the value of the detailEstimatedReceiveAmounts property.
     * 
     * @return
     *     possible object is
     *     {@link EstimatedReceiveAmountInfo.DetailEstimatedReceiveAmounts }
     *     
     */
    public EstimatedReceiveAmountInfo.DetailEstimatedReceiveAmounts getDetailEstimatedReceiveAmounts() {
        return detailEstimatedReceiveAmounts;
    }

    /**
     * Sets the value of the detailEstimatedReceiveAmounts property.
     * 
     * @param value
     *     allowed object is
     *     {@link EstimatedReceiveAmountInfo.DetailEstimatedReceiveAmounts }
     *     
     */
    public void setDetailEstimatedReceiveAmounts(EstimatedReceiveAmountInfo.DetailEstimatedReceiveAmounts value) {
        this.detailEstimatedReceiveAmounts = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="detailEstimatedReceiveAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountInfo" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "detailEstimatedReceiveAmount"
    })
    public static class DetailEstimatedReceiveAmounts
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<AmountInfo> detailEstimatedReceiveAmount;

        /**
         * Gets the value of the detailEstimatedReceiveAmount property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the detailEstimatedReceiveAmount property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDetailEstimatedReceiveAmount().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AmountInfo }
         * 
         * 
         */
        public List<AmountInfo> getDetailEstimatedReceiveAmount() {
            if (detailEstimatedReceiveAmount == null) {
                detailEstimatedReceiveAmount = new ArrayList<AmountInfo>();
            }
            return this.detailEstimatedReceiveAmount;
        }

    }

}
