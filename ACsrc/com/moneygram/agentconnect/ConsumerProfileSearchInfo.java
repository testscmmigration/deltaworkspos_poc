
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConsumerProfileSearchInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConsumerProfileSearchInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="consumerProfileID" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileIDType"/>
 *         &lt;element name="consumerProfileIDType" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileIDTypeType"/>
 *         &lt;element name="currentValues" type="{http://www.moneygram.com/AgentConnect1705}CurrentValuesType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsumerProfileSearchInfo", propOrder = {
    "consumerProfileID",
    "consumerProfileIDType",
    "currentValues"
})
public class ConsumerProfileSearchInfo
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected String consumerProfileID;
    @XmlElement(required = true)
    protected String consumerProfileIDType;
    protected CurrentValuesType currentValues;

    /**
     * Gets the value of the consumerProfileID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsumerProfileID() {
        return consumerProfileID;
    }

    /**
     * Sets the value of the consumerProfileID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsumerProfileID(String value) {
        this.consumerProfileID = value;
    }

    /**
     * Gets the value of the consumerProfileIDType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsumerProfileIDType() {
        return consumerProfileIDType;
    }

    /**
     * Sets the value of the consumerProfileIDType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsumerProfileIDType(String value) {
        this.consumerProfileIDType = value;
    }

    /**
     * Gets the value of the currentValues property.
     * 
     * @return
     *     possible object is
     *     {@link CurrentValuesType }
     *     
     */
    public CurrentValuesType getCurrentValues() {
        return currentValues;
    }

    /**
     * Sets the value of the currentValues property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrentValuesType }
     *     
     */
    public void setCurrentValues(CurrentValuesType value) {
        this.currentValues = value;
    }

}
