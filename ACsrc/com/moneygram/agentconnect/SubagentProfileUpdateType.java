
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubagentProfileUpdateType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubagentProfileUpdateType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agentID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sequenceNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="profileItemName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="profileItemValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubagentProfileUpdateType", propOrder = {
    "agentID",
    "sequenceNumber",
    "profileItemName",
    "profileItemValue"
})
public class SubagentProfileUpdateType
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected String agentID;
    protected int sequenceNumber;
    @XmlElement(required = true)
    protected String profileItemName;
    @XmlElement(required = true)
    protected String profileItemValue;

    /**
     * Gets the value of the agentID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentID() {
        return agentID;
    }

    /**
     * Sets the value of the agentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentID(String value) {
        this.agentID = value;
    }

    /**
     * Gets the value of the sequenceNumber property.
     * 
     */
    public int getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     */
    public void setSequenceNumber(int value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the profileItemName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfileItemName() {
        return profileItemName;
    }

    /**
     * Sets the value of the profileItemName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfileItemName(String value) {
        this.profileItemName = value;
    }

    /**
     * Gets the value of the profileItemValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfileItemValue() {
        return profileItemValue;
    }

    /**
     * Sets the value of the profileItemValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfileItemValue(String value) {
        this.profileItemValue = value;
    }

}
