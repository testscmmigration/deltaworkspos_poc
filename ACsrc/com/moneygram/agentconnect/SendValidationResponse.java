
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SendValidationResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SendValidationResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="mgiSessionID" type="{http://www.moneygram.com/AgentConnect1705}MGISessionIDType"/>
 *                   &lt;element name="readyForCommit" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="GAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType"/>
 *                   &lt;element name="token" type="{http://www.moneygram.com/AgentConnect1705}TokenType" minOccurs="0"/>
 *                   &lt;element name="customerReceiveNumber" type="{http://www.moneygram.com/AgentConnect1705}CustomerReceiveNumberType" minOccurs="0"/>
 *                   &lt;element name="displayAccountID" type="{http://www.moneygram.com/AgentConnect1705}CustomerReceiveNumberType" minOccurs="0"/>
 *                   &lt;element name="customerServiceMessage" type="{http://www.moneygram.com/AgentConnect1705}StringMax255Type" minOccurs="0"/>
 *                   &lt;element name="receiveAgentName" type="{http://www.moneygram.com/AgentConnect1705}StringMax40Type" minOccurs="0"/>
 *                   &lt;element name="receiveAgentAddress" type="{http://www.moneygram.com/AgentConnect1705}AgentAddressType" minOccurs="0"/>
 *                   &lt;element name="receiveAgentAbbreviation" type="{http://www.moneygram.com/AgentConnect1705}ReceiveAgentAbbreviationType" minOccurs="0"/>
 *                   &lt;element name="exchangeRateApplied" type="{http://www.moneygram.com/AgentConnect1705}FXRateType" minOccurs="0"/>
 *                   &lt;element name="sendAmounts" type="{http://www.moneygram.com/AgentConnect1705}SendAmountInfo" minOccurs="0"/>
 *                   &lt;element name="receiveAmounts" type="{http://www.moneygram.com/AgentConnect1705}ReceiveAmountInfo" minOccurs="0"/>
 *                   &lt;element name="promotionInfos" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="promotionInfo" type="{http://www.moneygram.com/AgentConnect1705}PromotionInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="fieldsToCollect" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="fieldToCollect" type="{http://www.moneygram.com/AgentConnect1705}InfoBase" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="receipts" type="{http://www.moneygram.com/AgentConnect1705}PreCompletionReceiptType" minOccurs="0"/>
 *                   &lt;element name="receiptInfo" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="receiveFeeDisclosureText" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                             &lt;element name="receiveTaxDisclosureText" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                             &lt;element name="promotionalMessages" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="promotionalMessage" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="disclosureTexts" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="disclosureText" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SendValidationResponse", propOrder = {
    "payload"
})
public class SendValidationResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<SendValidationResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SendValidationResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<SendValidationResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SendValidationResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<SendValidationResponse.Payload> value) {
        this.payload = ((JAXBElement<SendValidationResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="mgiSessionID" type="{http://www.moneygram.com/AgentConnect1705}MGISessionIDType"/>
     *         &lt;element name="readyForCommit" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="GAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType"/>
     *         &lt;element name="token" type="{http://www.moneygram.com/AgentConnect1705}TokenType" minOccurs="0"/>
     *         &lt;element name="customerReceiveNumber" type="{http://www.moneygram.com/AgentConnect1705}CustomerReceiveNumberType" minOccurs="0"/>
     *         &lt;element name="displayAccountID" type="{http://www.moneygram.com/AgentConnect1705}CustomerReceiveNumberType" minOccurs="0"/>
     *         &lt;element name="customerServiceMessage" type="{http://www.moneygram.com/AgentConnect1705}StringMax255Type" minOccurs="0"/>
     *         &lt;element name="receiveAgentName" type="{http://www.moneygram.com/AgentConnect1705}StringMax40Type" minOccurs="0"/>
     *         &lt;element name="receiveAgentAddress" type="{http://www.moneygram.com/AgentConnect1705}AgentAddressType" minOccurs="0"/>
     *         &lt;element name="receiveAgentAbbreviation" type="{http://www.moneygram.com/AgentConnect1705}ReceiveAgentAbbreviationType" minOccurs="0"/>
     *         &lt;element name="exchangeRateApplied" type="{http://www.moneygram.com/AgentConnect1705}FXRateType" minOccurs="0"/>
     *         &lt;element name="sendAmounts" type="{http://www.moneygram.com/AgentConnect1705}SendAmountInfo" minOccurs="0"/>
     *         &lt;element name="receiveAmounts" type="{http://www.moneygram.com/AgentConnect1705}ReceiveAmountInfo" minOccurs="0"/>
     *         &lt;element name="promotionInfos" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="promotionInfo" type="{http://www.moneygram.com/AgentConnect1705}PromotionInfo" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="fieldsToCollect" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="fieldToCollect" type="{http://www.moneygram.com/AgentConnect1705}InfoBase" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="receipts" type="{http://www.moneygram.com/AgentConnect1705}PreCompletionReceiptType" minOccurs="0"/>
     *         &lt;element name="receiptInfo" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="receiveFeeDisclosureText" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *                   &lt;element name="receiveTaxDisclosureText" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *                   &lt;element name="promotionalMessages" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="promotionalMessage" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="disclosureTexts" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="disclosureText" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mgiSessionID",
        "readyForCommit",
        "gafVersionNumber",
        "token",
        "customerReceiveNumber",
        "displayAccountID",
        "customerServiceMessage",
        "receiveAgentName",
        "receiveAgentAddress",
        "receiveAgentAbbreviation",
        "exchangeRateApplied",
        "sendAmounts",
        "receiveAmounts",
        "promotionInfos",
        "fieldsToCollect",
        "receipts",
        "receiptInfo"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(required = true)
        protected String mgiSessionID;
        protected boolean readyForCommit;
        @XmlElement(name = "GAFVersionNumber", required = true)
        protected String gafVersionNumber;
        protected String token;
        protected String customerReceiveNumber;
        protected String displayAccountID;
        protected String customerServiceMessage;
        protected String receiveAgentName;
        protected AgentAddressType receiveAgentAddress;
        protected String receiveAgentAbbreviation;
        protected BigDecimal exchangeRateApplied;
        protected SendAmountInfo sendAmounts;
        protected ReceiveAmountInfo receiveAmounts;
        protected SendValidationResponse.Payload.PromotionInfos promotionInfos;
        protected SendValidationResponse.Payload.FieldsToCollect fieldsToCollect;
        protected PreCompletionReceiptType receipts;
        protected SendValidationResponse.Payload.ReceiptInfo receiptInfo;

        /**
         * Gets the value of the mgiSessionID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMgiSessionID() {
            return mgiSessionID;
        }

        /**
         * Sets the value of the mgiSessionID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMgiSessionID(String value) {
            this.mgiSessionID = value;
        }

        /**
         * Gets the value of the readyForCommit property.
         * 
         */
        public boolean isReadyForCommit() {
            return readyForCommit;
        }

        /**
         * Sets the value of the readyForCommit property.
         * 
         */
        public void setReadyForCommit(boolean value) {
            this.readyForCommit = value;
        }

        /**
         * Gets the value of the gafVersionNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGAFVersionNumber() {
            return gafVersionNumber;
        }

        /**
         * Sets the value of the gafVersionNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGAFVersionNumber(String value) {
            this.gafVersionNumber = value;
        }

        /**
         * Gets the value of the token property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getToken() {
            return token;
        }

        /**
         * Sets the value of the token property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setToken(String value) {
            this.token = value;
        }

        /**
         * Gets the value of the customerReceiveNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustomerReceiveNumber() {
            return customerReceiveNumber;
        }

        /**
         * Sets the value of the customerReceiveNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustomerReceiveNumber(String value) {
            this.customerReceiveNumber = value;
        }

        /**
         * Gets the value of the displayAccountID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDisplayAccountID() {
            return displayAccountID;
        }

        /**
         * Sets the value of the displayAccountID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDisplayAccountID(String value) {
            this.displayAccountID = value;
        }

        /**
         * Gets the value of the customerServiceMessage property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustomerServiceMessage() {
            return customerServiceMessage;
        }

        /**
         * Sets the value of the customerServiceMessage property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustomerServiceMessage(String value) {
            this.customerServiceMessage = value;
        }

        /**
         * Gets the value of the receiveAgentName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReceiveAgentName() {
            return receiveAgentName;
        }

        /**
         * Sets the value of the receiveAgentName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReceiveAgentName(String value) {
            this.receiveAgentName = value;
        }

        /**
         * Gets the value of the receiveAgentAddress property.
         * 
         * @return
         *     possible object is
         *     {@link AgentAddressType }
         *     
         */
        public AgentAddressType getReceiveAgentAddress() {
            return receiveAgentAddress;
        }

        /**
         * Sets the value of the receiveAgentAddress property.
         * 
         * @param value
         *     allowed object is
         *     {@link AgentAddressType }
         *     
         */
        public void setReceiveAgentAddress(AgentAddressType value) {
            this.receiveAgentAddress = value;
        }

        /**
         * Gets the value of the receiveAgentAbbreviation property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReceiveAgentAbbreviation() {
            return receiveAgentAbbreviation;
        }

        /**
         * Sets the value of the receiveAgentAbbreviation property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReceiveAgentAbbreviation(String value) {
            this.receiveAgentAbbreviation = value;
        }

        /**
         * Gets the value of the exchangeRateApplied property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getExchangeRateApplied() {
            return exchangeRateApplied;
        }

        /**
         * Sets the value of the exchangeRateApplied property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setExchangeRateApplied(BigDecimal value) {
            this.exchangeRateApplied = value;
        }

        /**
         * Gets the value of the sendAmounts property.
         * 
         * @return
         *     possible object is
         *     {@link SendAmountInfo }
         *     
         */
        public SendAmountInfo getSendAmounts() {
            return sendAmounts;
        }

        /**
         * Sets the value of the sendAmounts property.
         * 
         * @param value
         *     allowed object is
         *     {@link SendAmountInfo }
         *     
         */
        public void setSendAmounts(SendAmountInfo value) {
            this.sendAmounts = value;
        }

        /**
         * Gets the value of the receiveAmounts property.
         * 
         * @return
         *     possible object is
         *     {@link ReceiveAmountInfo }
         *     
         */
        public ReceiveAmountInfo getReceiveAmounts() {
            return receiveAmounts;
        }

        /**
         * Sets the value of the receiveAmounts property.
         * 
         * @param value
         *     allowed object is
         *     {@link ReceiveAmountInfo }
         *     
         */
        public void setReceiveAmounts(ReceiveAmountInfo value) {
            this.receiveAmounts = value;
        }

        /**
         * Gets the value of the promotionInfos property.
         * 
         * @return
         *     possible object is
         *     {@link SendValidationResponse.Payload.PromotionInfos }
         *     
         */
        public SendValidationResponse.Payload.PromotionInfos getPromotionInfos() {
            return promotionInfos;
        }

        /**
         * Sets the value of the promotionInfos property.
         * 
         * @param value
         *     allowed object is
         *     {@link SendValidationResponse.Payload.PromotionInfos }
         *     
         */
        public void setPromotionInfos(SendValidationResponse.Payload.PromotionInfos value) {
            this.promotionInfos = value;
        }

        /**
         * Gets the value of the fieldsToCollect property.
         * 
         * @return
         *     possible object is
         *     {@link SendValidationResponse.Payload.FieldsToCollect }
         *     
         */
        public SendValidationResponse.Payload.FieldsToCollect getFieldsToCollect() {
            return fieldsToCollect;
        }

        /**
         * Sets the value of the fieldsToCollect property.
         * 
         * @param value
         *     allowed object is
         *     {@link SendValidationResponse.Payload.FieldsToCollect }
         *     
         */
        public void setFieldsToCollect(SendValidationResponse.Payload.FieldsToCollect value) {
            this.fieldsToCollect = value;
        }

        /**
         * Gets the value of the receipts property.
         * 
         * @return
         *     possible object is
         *     {@link PreCompletionReceiptType }
         *     
         */
        public PreCompletionReceiptType getReceipts() {
            return receipts;
        }

        /**
         * Sets the value of the receipts property.
         * 
         * @param value
         *     allowed object is
         *     {@link PreCompletionReceiptType }
         *     
         */
        public void setReceipts(PreCompletionReceiptType value) {
            this.receipts = value;
        }

        /**
         * Gets the value of the receiptInfo property.
         * 
         * @return
         *     possible object is
         *     {@link SendValidationResponse.Payload.ReceiptInfo }
         *     
         */
        public SendValidationResponse.Payload.ReceiptInfo getReceiptInfo() {
            return receiptInfo;
        }

        /**
         * Sets the value of the receiptInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link SendValidationResponse.Payload.ReceiptInfo }
         *     
         */
        public void setReceiptInfo(SendValidationResponse.Payload.ReceiptInfo value) {
            this.receiptInfo = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="fieldToCollect" type="{http://www.moneygram.com/AgentConnect1705}InfoBase" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "fieldToCollect"
        })
        public static class FieldsToCollect
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected List<InfoBase> fieldToCollect;

            /**
             * Gets the value of the fieldToCollect property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the fieldToCollect property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getFieldToCollect().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link InfoBase }
             * 
             * 
             */
            public List<InfoBase> getFieldToCollect() {
                if (fieldToCollect == null) {
                    fieldToCollect = new ArrayList<InfoBase>();
                }
                return this.fieldToCollect;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="promotionInfo" type="{http://www.moneygram.com/AgentConnect1705}PromotionInfo" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "promotionInfo"
        })
        public static class PromotionInfos
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected List<PromotionInfo> promotionInfo;

            /**
             * Gets the value of the promotionInfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the promotionInfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getPromotionInfo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link PromotionInfo }
             * 
             * 
             */
            public List<PromotionInfo> getPromotionInfo() {
                if (promotionInfo == null) {
                    promotionInfo = new ArrayList<PromotionInfo>();
                }
                return this.promotionInfo;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="receiveFeeDisclosureText" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
         *         &lt;element name="receiveTaxDisclosureText" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
         *         &lt;element name="promotionalMessages" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="promotionalMessage" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="disclosureTexts" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="disclosureText" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "receiveFeeDisclosureText",
            "receiveTaxDisclosureText",
            "promotionalMessages",
            "disclosureTexts"
        })
        public static class ReceiptInfo
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected Boolean receiveFeeDisclosureText;
            protected Boolean receiveTaxDisclosureText;
            protected SendValidationResponse.Payload.ReceiptInfo.PromotionalMessages promotionalMessages;
            protected SendValidationResponse.Payload.ReceiptInfo.DisclosureTexts disclosureTexts;

            /**
             * Gets the value of the receiveFeeDisclosureText property.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isReceiveFeeDisclosureText() {
                return receiveFeeDisclosureText;
            }

            /**
             * Sets the value of the receiveFeeDisclosureText property.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setReceiveFeeDisclosureText(Boolean value) {
                this.receiveFeeDisclosureText = value;
            }

            /**
             * Gets the value of the receiveTaxDisclosureText property.
             * 
             * @return
             *     possible object is
             *     {@link Boolean }
             *     
             */
            public Boolean isReceiveTaxDisclosureText() {
                return receiveTaxDisclosureText;
            }

            /**
             * Sets the value of the receiveTaxDisclosureText property.
             * 
             * @param value
             *     allowed object is
             *     {@link Boolean }
             *     
             */
            public void setReceiveTaxDisclosureText(Boolean value) {
                this.receiveTaxDisclosureText = value;
            }

            /**
             * Gets the value of the promotionalMessages property.
             * 
             * @return
             *     possible object is
             *     {@link SendValidationResponse.Payload.ReceiptInfo.PromotionalMessages }
             *     
             */
            public SendValidationResponse.Payload.ReceiptInfo.PromotionalMessages getPromotionalMessages() {
                return promotionalMessages;
            }

            /**
             * Sets the value of the promotionalMessages property.
             * 
             * @param value
             *     allowed object is
             *     {@link SendValidationResponse.Payload.ReceiptInfo.PromotionalMessages }
             *     
             */
            public void setPromotionalMessages(SendValidationResponse.Payload.ReceiptInfo.PromotionalMessages value) {
                this.promotionalMessages = value;
            }

            /**
             * Gets the value of the disclosureTexts property.
             * 
             * @return
             *     possible object is
             *     {@link SendValidationResponse.Payload.ReceiptInfo.DisclosureTexts }
             *     
             */
            public SendValidationResponse.Payload.ReceiptInfo.DisclosureTexts getDisclosureTexts() {
                return disclosureTexts;
            }

            /**
             * Sets the value of the disclosureTexts property.
             * 
             * @param value
             *     allowed object is
             *     {@link SendValidationResponse.Payload.ReceiptInfo.DisclosureTexts }
             *     
             */
            public void setDisclosureTexts(SendValidationResponse.Payload.ReceiptInfo.DisclosureTexts value) {
                this.disclosureTexts = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="disclosureText" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "disclosureText"
            })
            public static class DisclosureTexts
                implements Serializable
            {

                private final static long serialVersionUID = 1L;
                protected List<TextTranslationType> disclosureText;

                /**
                 * Gets the value of the disclosureText property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the disclosureText property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getDisclosureText().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link TextTranslationType }
                 * 
                 * 
                 */
                public List<TextTranslationType> getDisclosureText() {
                    if (disclosureText == null) {
                        disclosureText = new ArrayList<TextTranslationType>();
                    }
                    return this.disclosureText;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="promotionalMessage" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "promotionalMessage"
            })
            public static class PromotionalMessages
                implements Serializable
            {

                private final static long serialVersionUID = 1L;
                protected List<TextTranslationType> promotionalMessage;

                /**
                 * Gets the value of the promotionalMessage property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the promotionalMessage property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getPromotionalMessage().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link TextTranslationType }
                 * 
                 * 
                 */
                public List<TextTranslationType> getPromotionalMessage() {
                    if (promotionalMessage == null) {
                        promotionalMessage = new ArrayList<TextTranslationType>();
                    }
                    return this.promotionalMessage;
                }

            }

        }

    }

}
