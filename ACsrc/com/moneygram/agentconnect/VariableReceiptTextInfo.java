
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VariableReceiptTextInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VariableReceiptTextInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="receiptTextType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="textTranslation" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VariableReceiptTextInfo", propOrder = {
    "receiptTextType",
    "textTranslation"
})
public class VariableReceiptTextInfo
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected String receiptTextType;
    @XmlElement(required = true)
    protected List<TextTranslationType> textTranslation;

    /**
     * Gets the value of the receiptTextType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiptTextType() {
        return receiptTextType;
    }

    /**
     * Sets the value of the receiptTextType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiptTextType(String value) {
        this.receiptTextType = value;
    }

    /**
     * Gets the value of the textTranslation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the textTranslation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTextTranslation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextTranslationType }
     * 
     * 
     */
    public List<TextTranslationType> getTextTranslation() {
        if (textTranslation == null) {
            textTranslation = new ArrayList<TextTranslationType>();
        }
        return this.textTranslation;
    }

}
