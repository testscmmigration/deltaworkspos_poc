
package com.moneygram.agentconnect;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProductVariantType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ProductVariantType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="EP"/>
 *     &lt;enumeration value="PREPAY"/>
 *     &lt;enumeration value="UBP"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ProductVariantType")
@XmlEnum
public enum ProductVariantType {


    /**
     * Express Payment
     * 
     */
    EP,

    /**
     * Prepaid Payment
     * 
     */
    PREPAY,

    /**
     * Utility Bill Payment
     * 
     */
    UBP;

    public String value() {
        return name();
    }

    public static ProductVariantType fromValue(String v) {
        return valueOf(v);
    }

}
