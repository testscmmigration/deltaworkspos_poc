
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetBankDetailsByLevelResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetBankDetailsByLevelResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="hierarchyLevelNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="hierarchyLevelLabel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="hierarchyLevelInfos" type="{http://www.moneygram.com/AgentConnect1705}HierarchyLevelInfo" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetBankDetailsByLevelResponse", propOrder = {
    "payload"
})
public class GetBankDetailsByLevelResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<GetBankDetailsByLevelResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetBankDetailsByLevelResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<GetBankDetailsByLevelResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetBankDetailsByLevelResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<GetBankDetailsByLevelResponse.Payload> value) {
        this.payload = ((JAXBElement<GetBankDetailsByLevelResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="hierarchyLevelNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="hierarchyLevelLabel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="hierarchyLevelInfos" type="{http://www.moneygram.com/AgentConnect1705}HierarchyLevelInfo" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "hierarchyLevelNumber",
        "hierarchyLevelLabel",
        "hierarchyLevelInfos"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected int hierarchyLevelNumber;
        protected String hierarchyLevelLabel;
        @XmlElement(required = true)
        protected List<HierarchyLevelInfo> hierarchyLevelInfos;

        /**
         * Gets the value of the hierarchyLevelNumber property.
         * 
         */
        public int getHierarchyLevelNumber() {
            return hierarchyLevelNumber;
        }

        /**
         * Sets the value of the hierarchyLevelNumber property.
         * 
         */
        public void setHierarchyLevelNumber(int value) {
            this.hierarchyLevelNumber = value;
        }

        /**
         * Gets the value of the hierarchyLevelLabel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHierarchyLevelLabel() {
            return hierarchyLevelLabel;
        }

        /**
         * Sets the value of the hierarchyLevelLabel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHierarchyLevelLabel(String value) {
            this.hierarchyLevelLabel = value;
        }

        /**
         * Gets the value of the hierarchyLevelInfos property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the hierarchyLevelInfos property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getHierarchyLevelInfos().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link HierarchyLevelInfo }
         * 
         * 
         */
        public List<HierarchyLevelInfo> getHierarchyLevelInfos() {
            if (hierarchyLevelInfos == null) {
                hierarchyLevelInfos = new ArrayList<HierarchyLevelInfo>();
            }
            return this.hierarchyLevelInfos;
        }

    }

}
