
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FeeLookupBySendCountryRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FeeLookupBySendCountryRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="mgiSessionType" type="{http://www.moneygram.com/AgentConnect1705}SessionType"/>
 *         &lt;element name="productVariant" type="{http://www.moneygram.com/AgentConnect1705}ProductVariantType" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element name="amountIncludingFee" type="{http://www.moneygram.com/AgentConnect1705}AmountNonZeroType"/>
 *           &lt;element name="amountExcludingFee" type="{http://www.moneygram.com/AgentConnect1705}AmountNonZeroType"/>
 *           &lt;element name="receiveAmount" type="{http://www.moneygram.com/AgentConnect1705}Decimal9nonZeroType"/>
 *           &lt;element name="indicativeReceiveAmount" type="{http://www.moneygram.com/AgentConnect1705}Decimal9nonZeroType"/>
 *         &lt;/choice>
 *         &lt;element name="receiveCountry" type="{http://www.moneygram.com/AgentConnect1705}CountryCodeType" minOccurs="0"/>
 *         &lt;element name="serviceOption" type="{http://www.moneygram.com/AgentConnect1705}ServiceOptionType" minOccurs="0"/>
 *         &lt;element name="freqCustCardNumber" type="{http://www.moneygram.com/AgentConnect1705}StringMax20Type" minOccurs="0"/>
 *         &lt;element name="promotionCode" type="{http://www.moneygram.com/AgentConnect1705}StringMax10Type" minOccurs="0"/>
 *         &lt;element name="receiveCode" type="{http://www.moneygram.com/AgentConnect1705}ReceiveCodeType" minOccurs="0"/>
 *         &lt;element name="indicativeReceiveCurrency" type="{http://www.moneygram.com/AgentConnect1705}CurrencyCodeType" minOccurs="0"/>
 *         &lt;element name="issueFreqCustCard" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="receiveAgentID" type="{http://www.moneygram.com/AgentConnect1705}AgentIDType" minOccurs="0"/>
 *         &lt;element name="receiveCurrency" type="{http://www.moneygram.com/AgentConnect1705}CurrencyCodeType" minOccurs="0"/>
 *         &lt;element name="mgCustomerReceiveNumber" type="{http://www.moneygram.com/AgentConnect1705}MgCustomerReceiveNumberType" minOccurs="0"/>
 *         &lt;element name="sendCountry" type="{http://www.moneygram.com/AgentConnect1705}CountryCodeType" minOccurs="0"/>
 *         &lt;element name="sendCurrency" type="{http://www.moneygram.com/AgentConnect1705}CurrencyCodeType" minOccurs="0"/>
 *         &lt;element name="allOptions" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FeeLookupBySendCountryRequest", propOrder = {
    "mgiSessionType",
    "productVariant",
    "amountIncludingFee",
    "amountExcludingFee",
    "receiveAmount",
    "indicativeReceiveAmount",
    "receiveCountry",
    "serviceOption",
    "freqCustCardNumber",
    "promotionCode",
    "receiveCode",
    "indicativeReceiveCurrency",
    "issueFreqCustCard",
    "receiveAgentID",
    "receiveCurrency",
    "mgCustomerReceiveNumber",
    "sendCountry",
    "sendCurrency",
    "allOptions"
})
public class FeeLookupBySendCountryRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected SessionType mgiSessionType;
    protected ProductVariantType productVariant;
    protected BigDecimal amountIncludingFee;
    protected BigDecimal amountExcludingFee;
    protected BigDecimal receiveAmount;
    protected BigDecimal indicativeReceiveAmount;
    protected String receiveCountry;
    protected String serviceOption;
    protected String freqCustCardNumber;
    protected String promotionCode;
    protected String receiveCode;
    protected String indicativeReceiveCurrency;
    protected Boolean issueFreqCustCard;
    protected String receiveAgentID;
    protected String receiveCurrency;
    protected String mgCustomerReceiveNumber;
    protected String sendCountry;
    protected String sendCurrency;
    protected boolean allOptions;

    /**
     * Gets the value of the mgiSessionType property.
     * 
     * @return
     *     possible object is
     *     {@link SessionType }
     *     
     */
    public SessionType getMgiSessionType() {
        return mgiSessionType;
    }

    /**
     * Sets the value of the mgiSessionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SessionType }
     *     
     */
    public void setMgiSessionType(SessionType value) {
        this.mgiSessionType = value;
    }

    /**
     * Gets the value of the productVariant property.
     * 
     * @return
     *     possible object is
     *     {@link ProductVariantType }
     *     
     */
    public ProductVariantType getProductVariant() {
        return productVariant;
    }

    /**
     * Sets the value of the productVariant property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductVariantType }
     *     
     */
    public void setProductVariant(ProductVariantType value) {
        this.productVariant = value;
    }

    /**
     * Gets the value of the amountIncludingFee property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmountIncludingFee() {
        return amountIncludingFee;
    }

    /**
     * Sets the value of the amountIncludingFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmountIncludingFee(BigDecimal value) {
        this.amountIncludingFee = value;
    }

    /**
     * Gets the value of the amountExcludingFee property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmountExcludingFee() {
        return amountExcludingFee;
    }

    /**
     * Sets the value of the amountExcludingFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmountExcludingFee(BigDecimal value) {
        this.amountExcludingFee = value;
    }

    /**
     * Gets the value of the receiveAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getReceiveAmount() {
        return receiveAmount;
    }

    /**
     * Sets the value of the receiveAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setReceiveAmount(BigDecimal value) {
        this.receiveAmount = value;
    }

    /**
     * Gets the value of the indicativeReceiveAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIndicativeReceiveAmount() {
        return indicativeReceiveAmount;
    }

    /**
     * Sets the value of the indicativeReceiveAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIndicativeReceiveAmount(BigDecimal value) {
        this.indicativeReceiveAmount = value;
    }

    /**
     * Gets the value of the receiveCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiveCountry() {
        return receiveCountry;
    }

    /**
     * Sets the value of the receiveCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiveCountry(String value) {
        this.receiveCountry = value;
    }

    /**
     * Gets the value of the serviceOption property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceOption() {
        return serviceOption;
    }

    /**
     * Sets the value of the serviceOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceOption(String value) {
        this.serviceOption = value;
    }

    /**
     * Gets the value of the freqCustCardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFreqCustCardNumber() {
        return freqCustCardNumber;
    }

    /**
     * Sets the value of the freqCustCardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFreqCustCardNumber(String value) {
        this.freqCustCardNumber = value;
    }

    /**
     * Gets the value of the promotionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPromotionCode() {
        return promotionCode;
    }

    /**
     * Sets the value of the promotionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPromotionCode(String value) {
        this.promotionCode = value;
    }

    /**
     * Gets the value of the receiveCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiveCode() {
        return receiveCode;
    }

    /**
     * Sets the value of the receiveCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiveCode(String value) {
        this.receiveCode = value;
    }

    /**
     * Gets the value of the indicativeReceiveCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicativeReceiveCurrency() {
        return indicativeReceiveCurrency;
    }

    /**
     * Sets the value of the indicativeReceiveCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicativeReceiveCurrency(String value) {
        this.indicativeReceiveCurrency = value;
    }

    /**
     * Gets the value of the issueFreqCustCard property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIssueFreqCustCard() {
        return issueFreqCustCard;
    }

    /**
     * Sets the value of the issueFreqCustCard property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIssueFreqCustCard(Boolean value) {
        this.issueFreqCustCard = value;
    }

    /**
     * Gets the value of the receiveAgentID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiveAgentID() {
        return receiveAgentID;
    }

    /**
     * Sets the value of the receiveAgentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiveAgentID(String value) {
        this.receiveAgentID = value;
    }

    /**
     * Gets the value of the receiveCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiveCurrency() {
        return receiveCurrency;
    }

    /**
     * Sets the value of the receiveCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiveCurrency(String value) {
        this.receiveCurrency = value;
    }

    /**
     * Gets the value of the mgCustomerReceiveNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMgCustomerReceiveNumber() {
        return mgCustomerReceiveNumber;
    }

    /**
     * Sets the value of the mgCustomerReceiveNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMgCustomerReceiveNumber(String value) {
        this.mgCustomerReceiveNumber = value;
    }

    /**
     * Gets the value of the sendCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendCountry() {
        return sendCountry;
    }

    /**
     * Sets the value of the sendCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendCountry(String value) {
        this.sendCountry = value;
    }

    /**
     * Gets the value of the sendCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendCurrency() {
        return sendCurrency;
    }

    /**
     * Sets the value of the sendCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendCurrency(String value) {
        this.sendCurrency = value;
    }

    /**
     * Gets the value of the allOptions property.
     * 
     */
    public boolean isAllOptions() {
        return allOptions;
    }

    /**
     * Sets the value of the allOptions property.
     * 
     */
    public void setAllOptions(boolean value) {
        this.allOptions = value;
    }

}
