
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConsumerHistoryLookupRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConsumerHistoryLookupRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="GAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType" minOccurs="0"/>
 *         &lt;element name="consumerPhone" type="{http://www.moneygram.com/AgentConnect1705}Phone14Type" minOccurs="0"/>
 *         &lt;element name="mgiRewardsNumber" type="{http://www.moneygram.com/AgentConnect1705}StringMax20Type" minOccurs="0"/>
 *         &lt;element name="billerAccountNumber" type="{http://www.moneygram.com/AgentConnect1705}BillerAccountNumberType" minOccurs="0"/>
 *         &lt;element name="agentFrequentCustomerNumber" type="{http://www.moneygram.com/AgentConnect1705}AgentFrequentCustomerNumberType" minOccurs="0"/>
 *         &lt;element name="maxSendersToReturn" type="{http://www.moneygram.com/AgentConnect1705}IntMaxToReturnType"/>
 *         &lt;element name="maxReceiversToReturn" type="{http://www.moneygram.com/AgentConnect1705}IntMaxToReturnType"/>
 *         &lt;element name="mgiSessionType" type="{http://www.moneygram.com/AgentConnect1705}SessionType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsumerHistoryLookupRequest", propOrder = {
    "gafVersionNumber",
    "consumerPhone",
    "mgiRewardsNumber",
    "billerAccountNumber",
    "agentFrequentCustomerNumber",
    "maxSendersToReturn",
    "maxReceiversToReturn",
    "mgiSessionType"
})
public class ConsumerHistoryLookupRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "GAFVersionNumber")
    protected String gafVersionNumber;
    protected String consumerPhone;
    protected String mgiRewardsNumber;
    protected String billerAccountNumber;
    protected String agentFrequentCustomerNumber;
    protected int maxSendersToReturn;
    protected int maxReceiversToReturn;
    @XmlElement(required = true)
    protected SessionType mgiSessionType;

    /**
     * Gets the value of the gafVersionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGAFVersionNumber() {
        return gafVersionNumber;
    }

    /**
     * Sets the value of the gafVersionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGAFVersionNumber(String value) {
        this.gafVersionNumber = value;
    }

    /**
     * Gets the value of the consumerPhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsumerPhone() {
        return consumerPhone;
    }

    /**
     * Sets the value of the consumerPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsumerPhone(String value) {
        this.consumerPhone = value;
    }

    /**
     * Gets the value of the mgiRewardsNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMgiRewardsNumber() {
        return mgiRewardsNumber;
    }

    /**
     * Sets the value of the mgiRewardsNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMgiRewardsNumber(String value) {
        this.mgiRewardsNumber = value;
    }

    /**
     * Gets the value of the billerAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillerAccountNumber() {
        return billerAccountNumber;
    }

    /**
     * Sets the value of the billerAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillerAccountNumber(String value) {
        this.billerAccountNumber = value;
    }

    /**
     * Gets the value of the agentFrequentCustomerNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentFrequentCustomerNumber() {
        return agentFrequentCustomerNumber;
    }

    /**
     * Sets the value of the agentFrequentCustomerNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentFrequentCustomerNumber(String value) {
        this.agentFrequentCustomerNumber = value;
    }

    /**
     * Gets the value of the maxSendersToReturn property.
     * 
     */
    public int getMaxSendersToReturn() {
        return maxSendersToReturn;
    }

    /**
     * Sets the value of the maxSendersToReturn property.
     * 
     */
    public void setMaxSendersToReturn(int value) {
        this.maxSendersToReturn = value;
    }

    /**
     * Gets the value of the maxReceiversToReturn property.
     * 
     */
    public int getMaxReceiversToReturn() {
        return maxReceiversToReturn;
    }

    /**
     * Sets the value of the maxReceiversToReturn property.
     * 
     */
    public void setMaxReceiversToReturn(int value) {
        this.maxReceiversToReturn = value;
    }

    /**
     * Gets the value of the mgiSessionType property.
     * 
     * @return
     *     possible object is
     *     {@link SessionType }
     *     
     */
    public SessionType getMgiSessionType() {
        return mgiSessionType;
    }

    /**
     * Sets the value of the mgiSessionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SessionType }
     *     
     */
    public void setMgiSessionType(SessionType value) {
        this.mgiSessionType = value;
    }

}
