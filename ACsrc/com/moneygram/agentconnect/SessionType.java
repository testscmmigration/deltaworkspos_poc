
package com.moneygram.agentconnect;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SessionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SessionType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="BP"/>
 *     &lt;enumeration value="SEND"/>
 *     &lt;enumeration value="RCV"/>
 *     &lt;enumeration value="SREV"/>
 *     &lt;enumeration value="RREV"/>
 *     &lt;enumeration value="AMD"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SessionType")
@XmlEnum
public enum SessionType {


    /**
     * Bill Payment
     * 
     */
    BP,

    /**
     * MoneyGram Send
     * 
     */
    SEND,

    /**
     * MoneyGram Receive
     * 
     */
    RCV,

    /**
     * Send Reversal
     * 
     */
    SREV,

    /**
     * Receive Reversal
     * 
     */
    RREV,

    /**
     * Amend
     * 
     */
    AMD;

    public String value() {
        return name();
    }

    public static SessionType fromValue(String v) {
        return valueOf(v);
    }

}
