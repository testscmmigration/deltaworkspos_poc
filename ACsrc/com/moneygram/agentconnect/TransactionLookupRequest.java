
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransactionLookupRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionLookupRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="GAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType" minOccurs="0"/>
 *         &lt;element name="purposeOfLookup" type="{http://www.moneygram.com/AgentConnect1705}EnumType"/>
 *         &lt;element name="referenceNumber" type="{http://www.moneygram.com/AgentConnect1705}ReferenceNumberType" minOccurs="0"/>
 *         &lt;element name="confirmationNumber" type="{http://www.moneygram.com/AgentConnect1705}ConfirmationNumberType" minOccurs="0"/>
 *         &lt;element name="transactionPin" type="{http://www.moneygram.com/AgentConnect1705}TransactionPinType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionLookupRequest", propOrder = {
    "gafVersionNumber",
    "purposeOfLookup",
    "referenceNumber",
    "confirmationNumber",
    "transactionPin"
})
public class TransactionLookupRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "GAFVersionNumber")
    protected String gafVersionNumber;
    @XmlElement(required = true)
    protected String purposeOfLookup;
    protected String referenceNumber;
    protected String confirmationNumber;
    protected String transactionPin;

    /**
     * Gets the value of the gafVersionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGAFVersionNumber() {
        return gafVersionNumber;
    }

    /**
     * Sets the value of the gafVersionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGAFVersionNumber(String value) {
        this.gafVersionNumber = value;
    }

    /**
     * Gets the value of the purposeOfLookup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurposeOfLookup() {
        return purposeOfLookup;
    }

    /**
     * Sets the value of the purposeOfLookup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurposeOfLookup(String value) {
        this.purposeOfLookup = value;
    }

    /**
     * Gets the value of the referenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceNumber() {
        return referenceNumber;
    }

    /**
     * Sets the value of the referenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceNumber(String value) {
        this.referenceNumber = value;
    }

    /**
     * Gets the value of the confirmationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfirmationNumber() {
        return confirmationNumber;
    }

    /**
     * Sets the value of the confirmationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfirmationNumber(String value) {
        this.confirmationNumber = value;
    }

    /**
     * Gets the value of the transactionPin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionPin() {
        return transactionPin;
    }

    /**
     * Sets the value of the transactionPin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionPin(String value) {
        this.transactionPin = value;
    }

}
