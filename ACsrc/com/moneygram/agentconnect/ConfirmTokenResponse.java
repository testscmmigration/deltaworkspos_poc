
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConfirmTokenResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConfirmTokenResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="profileChanged" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="codeTableChanged" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConfirmTokenResponse", propOrder = {
    "payload"
})
public class ConfirmTokenResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<ConfirmTokenResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ConfirmTokenResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<ConfirmTokenResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ConfirmTokenResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<ConfirmTokenResponse.Payload> value) {
        this.payload = ((JAXBElement<ConfirmTokenResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="profileChanged" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="codeTableChanged" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "profileChanged",
        "codeTableChanged"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected boolean profileChanged;
        protected boolean codeTableChanged;

        /**
         * Gets the value of the profileChanged property.
         * 
         */
        public boolean isProfileChanged() {
            return profileChanged;
        }

        /**
         * Sets the value of the profileChanged property.
         * 
         */
        public void setProfileChanged(boolean value) {
            this.profileChanged = value;
        }

        /**
         * Gets the value of the codeTableChanged property.
         * 
         */
        public boolean isCodeTableChanged() {
            return codeTableChanged;
        }

        /**
         * Sets the value of the codeTableChanged property.
         * 
         */
        public void setCodeTableChanged(boolean value) {
            this.codeTableChanged = value;
        }

    }

}
