
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProfileResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProfileResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="profileItem" type="{http://www.moneygram.com/AgentConnect1705}ProfileItemType" maxOccurs="unbounded"/>
 *                   &lt;element name="productProfileItem" type="{http://www.moneygram.com/AgentConnect1705}ProductProfileItemType" maxOccurs="unbounded"/>
 *                   &lt;element name="employeeProfileItem" type="{http://www.moneygram.com/AgentConnect1705}EmployeeProfileItemType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProfileResponse", propOrder = {
    "payload"
})
public class ProfileResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<ProfileResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ProfileResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<ProfileResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ProfileResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<ProfileResponse.Payload> value) {
        this.payload = ((JAXBElement<ProfileResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="profileItem" type="{http://www.moneygram.com/AgentConnect1705}ProfileItemType" maxOccurs="unbounded"/>
     *         &lt;element name="productProfileItem" type="{http://www.moneygram.com/AgentConnect1705}ProductProfileItemType" maxOccurs="unbounded"/>
     *         &lt;element name="employeeProfileItem" type="{http://www.moneygram.com/AgentConnect1705}EmployeeProfileItemType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "profileItem",
        "productProfileItem",
        "employeeProfileItem"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(required = true)
        protected List<ProfileItemType> profileItem;
        @XmlElement(required = true)
        protected List<ProductProfileItemType> productProfileItem;
        protected List<EmployeeProfileItemType> employeeProfileItem;

        /**
         * Gets the value of the profileItem property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the profileItem property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getProfileItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ProfileItemType }
         * 
         * 
         */
        public List<ProfileItemType> getProfileItem() {
            if (profileItem == null) {
                profileItem = new ArrayList<ProfileItemType>();
            }
            return this.profileItem;
        }

        /**
         * Gets the value of the productProfileItem property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the productProfileItem property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getProductProfileItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ProductProfileItemType }
         * 
         * 
         */
        public List<ProductProfileItemType> getProductProfileItem() {
            if (productProfileItem == null) {
                productProfileItem = new ArrayList<ProductProfileItemType>();
            }
            return this.productProfileItem;
        }

        /**
         * Gets the value of the employeeProfileItem property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the employeeProfileItem property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEmployeeProfileItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link EmployeeProfileItemType }
         * 
         * 
         */
        public List<EmployeeProfileItemType> getEmployeeProfileItem() {
            if (employeeProfileItem == null) {
                employeeProfileItem = new ArrayList<EmployeeProfileItemType>();
            }
            return this.employeeProfileItem;
        }

    }

}
