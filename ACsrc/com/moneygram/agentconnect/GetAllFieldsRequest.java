
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetAllFieldsRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetAllFieldsRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="cachedVersion" type="{http://www.moneygram.com/AgentConnect1705}VersionType" minOccurs="0"/>
 *         &lt;element name="transactionType" type="{http://www.moneygram.com/AgentConnect1705}EnumType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetAllFieldsRequest", propOrder = {
    "cachedVersion",
    "transactionType"
})
public class GetAllFieldsRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected String cachedVersion;
    @XmlElement(required = true)
    protected String transactionType;

    /**
     * Gets the value of the cachedVersion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCachedVersion() {
        return cachedVersion;
    }

    /**
     * Sets the value of the cachedVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCachedVersion(String value) {
        this.cachedVersion = value;
    }

    /**
     * Gets the value of the transactionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * Sets the value of the transactionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionType(String value) {
        this.transactionType = value;
    }

}
