
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DwInitialSetupResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DwInitialSetupResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="unitProfileID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="token" type="{http://www.moneygram.com/AgentConnect1705}TokenType"/>
 *                   &lt;element name="profile" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DwInitialSetupResponse", propOrder = {
    "payload"
})
public class DwInitialSetupResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<DwInitialSetupResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DwInitialSetupResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<DwInitialSetupResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DwInitialSetupResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<DwInitialSetupResponse.Payload> value) {
        this.payload = ((JAXBElement<DwInitialSetupResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="unitProfileID" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="token" type="{http://www.moneygram.com/AgentConnect1705}TokenType"/>
     *         &lt;element name="profile" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "unitProfileID",
        "token",
        "profile"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected int unitProfileID;
        @XmlElement(required = true)
        protected String token;
        @XmlElement(required = true)
        protected String profile;

        /**
         * Gets the value of the unitProfileID property.
         * 
         */
        public int getUnitProfileID() {
            return unitProfileID;
        }

        /**
         * Sets the value of the unitProfileID property.
         * 
         */
        public void setUnitProfileID(int value) {
            this.unitProfileID = value;
        }

        /**
         * Gets the value of the token property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getToken() {
            return token;
        }

        /**
         * Sets the value of the token property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setToken(String value) {
            this.token = value;
        }

        /**
         * Gets the value of the profile property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProfile() {
            return profile;
        }

        /**
         * Sets the value of the profile property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProfile(String value) {
            this.profile = value;
        }

    }

}
