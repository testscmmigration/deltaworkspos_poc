
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetDebugDataResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetDebugDataResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="endOfLog" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="collectionEndTimestamp" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="length" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="sequenceNumber" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="debugData" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetDebugDataResponse", propOrder = {
    "payload"
})
public class GetDebugDataResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<GetDebugDataResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetDebugDataResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<GetDebugDataResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetDebugDataResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<GetDebugDataResponse.Payload> value) {
        this.payload = ((JAXBElement<GetDebugDataResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="endOfLog" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="collectionEndTimestamp" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="length" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="sequenceNumber" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="debugData" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "endOfLog",
        "collectionEndTimestamp",
        "length",
        "sequenceNumber",
        "debugData"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected boolean endOfLog;
        protected long collectionEndTimestamp;
        @XmlElement(required = true)
        protected BigInteger length;
        @XmlElement(required = true)
        protected BigInteger sequenceNumber;
        @XmlElement(required = true)
        protected Object debugData;

        /**
         * Gets the value of the endOfLog property.
         * 
         */
        public boolean isEndOfLog() {
            return endOfLog;
        }

        /**
         * Sets the value of the endOfLog property.
         * 
         */
        public void setEndOfLog(boolean value) {
            this.endOfLog = value;
        }

        /**
         * Gets the value of the collectionEndTimestamp property.
         * 
         */
        public long getCollectionEndTimestamp() {
            return collectionEndTimestamp;
        }

        /**
         * Sets the value of the collectionEndTimestamp property.
         * 
         */
        public void setCollectionEndTimestamp(long value) {
            this.collectionEndTimestamp = value;
        }

        /**
         * Gets the value of the length property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getLength() {
            return length;
        }

        /**
         * Sets the value of the length property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setLength(BigInteger value) {
            this.length = value;
        }

        /**
         * Gets the value of the sequenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getSequenceNumber() {
            return sequenceNumber;
        }

        /**
         * Sets the value of the sequenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setSequenceNumber(BigInteger value) {
            this.sequenceNumber = value;
        }

        /**
         * Gets the value of the debugData property.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getDebugData() {
            return debugData;
        }

        /**
         * Sets the value of the debugData property.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setDebugData(Object value) {
            this.debugData = value;
        }

    }

}
