
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CheckInResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CheckInResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="token" type="{http://www.moneygram.com/AgentConnect1705}TokenType" minOccurs="0"/>
 *                   &lt;element name="timeZoneCorrection" type="{http://www.moneygram.com/AgentConnect1705}TimeZoneCorrectionType" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CheckInResponse", propOrder = {
    "payload"
})
public class CheckInResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<CheckInResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CheckInResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<CheckInResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CheckInResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<CheckInResponse.Payload> value) {
        this.payload = ((JAXBElement<CheckInResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="token" type="{http://www.moneygram.com/AgentConnect1705}TokenType" minOccurs="0"/>
     *         &lt;element name="timeZoneCorrection" type="{http://www.moneygram.com/AgentConnect1705}TimeZoneCorrectionType" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "token",
        "timeZoneCorrection"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected String token;
        protected TimeZoneCorrectionType timeZoneCorrection;

        /**
         * Gets the value of the token property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getToken() {
            return token;
        }

        /**
         * Sets the value of the token property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setToken(String value) {
            this.token = value;
        }

        /**
         * Gets the value of the timeZoneCorrection property.
         * 
         * @return
         *     possible object is
         *     {@link TimeZoneCorrectionType }
         *     
         */
        public TimeZoneCorrectionType getTimeZoneCorrection() {
            return timeZoneCorrection;
        }

        /**
         * Sets the value of the timeZoneCorrection property.
         * 
         * @param value
         *     allowed object is
         *     {@link TimeZoneCorrectionType }
         *     
         */
        public void setTimeZoneCorrection(TimeZoneCorrectionType value) {
            this.timeZoneCorrection = value;
        }

    }

}
