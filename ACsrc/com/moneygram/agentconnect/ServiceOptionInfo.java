
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ServiceOptionInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ServiceOptionInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="serviceOption" type="{http://www.moneygram.com/AgentConnect1705}ServiceOptionType" minOccurs="0"/>
 *         &lt;element name="receiveCurrency" type="{http://www.moneygram.com/AgentConnect1705}CurrencyCodeType" minOccurs="0"/>
 *         &lt;element name="receiveAgentID" type="{http://www.moneygram.com/AgentConnect1705}AgentIDType" minOccurs="0"/>
 *         &lt;element name="serviceOptionDisplayName" type="{http://www.moneygram.com/AgentConnect1705}StringMax50Type" minOccurs="0"/>
 *         &lt;element name="serviceOptionDisplayDescription" type="{http://www.moneygram.com/AgentConnect1705}StringMax500Type" minOccurs="0"/>
 *         &lt;element name="serviceOptionCategoryId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serviceOptionCategoryDisplayName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="localCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="indicativeRateAvailable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceOptionInfo", propOrder = {
    "serviceOption",
    "receiveCurrency",
    "receiveAgentID",
    "serviceOptionDisplayName",
    "serviceOptionDisplayDescription",
    "serviceOptionCategoryId",
    "serviceOptionCategoryDisplayName",
    "localCurrency",
    "indicativeRateAvailable"
})
public class ServiceOptionInfo
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected String serviceOption;
    protected String receiveCurrency;
    protected String receiveAgentID;
    protected String serviceOptionDisplayName;
    protected String serviceOptionDisplayDescription;
    protected String serviceOptionCategoryId;
    protected String serviceOptionCategoryDisplayName;
    protected String localCurrency;
    protected boolean indicativeRateAvailable;

    /**
     * Gets the value of the serviceOption property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceOption() {
        return serviceOption;
    }

    /**
     * Sets the value of the serviceOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceOption(String value) {
        this.serviceOption = value;
    }

    /**
     * Gets the value of the receiveCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiveCurrency() {
        return receiveCurrency;
    }

    /**
     * Sets the value of the receiveCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiveCurrency(String value) {
        this.receiveCurrency = value;
    }

    /**
     * Gets the value of the receiveAgentID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiveAgentID() {
        return receiveAgentID;
    }

    /**
     * Sets the value of the receiveAgentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiveAgentID(String value) {
        this.receiveAgentID = value;
    }

    /**
     * Gets the value of the serviceOptionDisplayName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceOptionDisplayName() {
        return serviceOptionDisplayName;
    }

    /**
     * Sets the value of the serviceOptionDisplayName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceOptionDisplayName(String value) {
        this.serviceOptionDisplayName = value;
    }

    /**
     * Gets the value of the serviceOptionDisplayDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceOptionDisplayDescription() {
        return serviceOptionDisplayDescription;
    }

    /**
     * Sets the value of the serviceOptionDisplayDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceOptionDisplayDescription(String value) {
        this.serviceOptionDisplayDescription = value;
    }

    /**
     * Gets the value of the serviceOptionCategoryId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceOptionCategoryId() {
        return serviceOptionCategoryId;
    }

    /**
     * Sets the value of the serviceOptionCategoryId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceOptionCategoryId(String value) {
        this.serviceOptionCategoryId = value;
    }

    /**
     * Gets the value of the serviceOptionCategoryDisplayName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceOptionCategoryDisplayName() {
        return serviceOptionCategoryDisplayName;
    }

    /**
     * Sets the value of the serviceOptionCategoryDisplayName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceOptionCategoryDisplayName(String value) {
        this.serviceOptionCategoryDisplayName = value;
    }

    /**
     * Gets the value of the localCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocalCurrency() {
        return localCurrency;
    }

    /**
     * Sets the value of the localCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocalCurrency(String value) {
        this.localCurrency = value;
    }

    /**
     * Gets the value of the indicativeRateAvailable property.
     * 
     */
    public boolean isIndicativeRateAvailable() {
        return indicativeRateAvailable;
    }

    /**
     * Sets the value of the indicativeRateAvailable property.
     * 
     */
    public void setIndicativeRateAvailable(boolean value) {
        this.indicativeRateAvailable = value;
    }

}
