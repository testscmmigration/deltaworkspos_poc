
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Payload complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Payload">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="doCheckIn" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="timeStamp" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="flags" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Payload", propOrder = {
    "doCheckIn",
    "timeStamp",
    "flags"
})
@XmlSeeAlso({
    com.moneygram.agentconnect.DirectoryOfAgentsByAreaCodePrefixResponse.Payload.class,
    com.moneygram.agentconnect.GetAllFieldsResponse.Payload.class,
    com.moneygram.agentconnect.GetDepositInformationResponse.Payload.class,
    com.moneygram.agentconnect.GetReceiptForReprintResponse.Payload.class,
    com.moneygram.agentconnect.GetDebugDataResponse.Payload.class,
    com.moneygram.agentconnect.GetProfileReceiverResponse.Payload.class,
    com.moneygram.agentconnect.CreateOrUpdateProfileSenderResponse.Payload.class,
    com.moneygram.agentconnect.ProfileResponse.Payload.class,
    com.moneygram.agentconnect.ComplianceTransactionResponse.Payload.class,
    com.moneygram.agentconnect.SaveDebugDataResponse.Payload.class,
    com.moneygram.agentconnect.GetBroadcastMessagesResponse.Payload.class,
    com.moneygram.agentconnect.BillPaymentSummaryReportResponse.Payload.class,
    com.moneygram.agentconnect.ConsumerHistoryLookupResponse.Payload.class,
    com.moneygram.agentconnect.SendReversalValidationResponse.Payload.class,
    com.moneygram.agentconnect.DirectoryOfAgentsByCityResponse.Payload.class,
    com.moneygram.agentconnect.BillPaymentDetailReportResponse.Payload.class,
    com.moneygram.agentconnect.DirectoryOfAgentsByZipResponse.Payload.class,
    com.moneygram.agentconnect.SaveProfileResponse.Payload.class,
    com.moneygram.agentconnect.InjectedInstructionResponse.Payload.class,
    com.moneygram.agentconnect.SearchConsumerProfilesResponse.Payload.class,
    com.moneygram.agentconnect.MoneyGramSendSummaryReportResponse.Payload.class,
    com.moneygram.agentconnect.BPValidationResponse.Payload.class,
    com.moneygram.agentconnect.CityListResponse.Payload.class,
    com.moneygram.agentconnect.DwPasswordResponse.Payload.class,
    com.moneygram.agentconnect.AmendValidationResponse.Payload.class,
    com.moneygram.agentconnect.SaveSubagentsResponse.Payload.class,
    com.moneygram.agentconnect.DwProfileResponse.Payload.class,
    com.moneygram.agentconnect.GetBankDetailsResponse.Payload.class,
    com.moneygram.agentconnect.GetBankDetailsByLevelResponse.Payload.class,
    com.moneygram.agentconnect.GetCurrencyInfoResponse.Payload.class,
    com.moneygram.agentconnect.DwInitialSetupResponse.Payload.class,
    com.moneygram.agentconnect.TransactionLookupResponse.Payload.class,
    com.moneygram.agentconnect.MoneyGramSendDetailReportWithTaxResponse.Payload.class,
    com.moneygram.agentconnect.GetCountrySubdivisionResponse.Payload.class,
    com.moneygram.agentconnect.DoddFrankStateRegulatorInfoResponse.Payload.class,
    com.moneygram.agentconnect.SubagentsResponse.Payload.class,
    com.moneygram.agentconnect.DisclosureTextDetailsResponse.Payload.class,
    com.moneygram.agentconnect.SearchStagedTransactionsResponse.Payload.class,
    com.moneygram.agentconnect.GetProfileConsumerResponse.Payload.class,
    com.moneygram.agentconnect.OpenOTPLoginResponse.Payload.class,
    com.moneygram.agentconnect.MoneyGramSendDetailReportResponse.Payload.class,
    com.moneygram.agentconnect.CreateOrUpdateProfileReceiverResponse.Payload.class,
    com.moneygram.agentconnect.DepositAnnouncementResponse.Payload.class,
    com.moneygram.agentconnect.MoneyGramReceiveSummaryReportResponse.Payload.class,
    com.moneygram.agentconnect.CompleteSessionResponse.Payload.class,
    com.moneygram.agentconnect.MoneyOrderTotalResponse.Payload.class,
    com.moneygram.agentconnect.GetCountryInfoResponse.Payload.class,
    com.moneygram.agentconnect.RegisterHardTokenResponse.Payload.class,
    com.moneygram.agentconnect.ReceiveReversalValidationResponse.Payload.class,
    com.moneygram.agentconnect.ConfirmTokenResponse.Payload.class,
    com.moneygram.agentconnect.VersionManifestResponse.Payload.class,
    com.moneygram.agentconnect.ReceiveValidationResponse.Payload.class,
    com.moneygram.agentconnect.GetProfileSenderResponse.Payload.class,
    com.moneygram.agentconnect.ReceiptsFormatDetailsResponse.Payload.class,
    com.moneygram.agentconnect.IndustryResponse.Payload.class,
    com.moneygram.agentconnect.SaveConsumerProfileImageResponse.Payload.class,
    com.moneygram.agentconnect.InitialSetupResponse.Payload.class,
    com.moneygram.agentconnect.GetEnumerationsResponse.Payload.class,
    com.moneygram.agentconnect.MoneyGramReceiveDetailReportResponse.Payload.class,
    com.moneygram.agentconnect.CreateOrUpdateProfileConsumerResponse.Payload.class,
    com.moneygram.agentconnect.PromotionLookupByCodeResponse.Payload.class,
    com.moneygram.agentconnect.GetAllErrorsResponse.Payload.class,
    com.moneygram.agentconnect.GetDepositBankListResponse.Payload.class,
    com.moneygram.agentconnect.SendValidationResponse.Payload.class,
    com.moneygram.agentconnect.FeeLookupBySendCountryResponse.Payload.class,
    com.moneygram.agentconnect.GetConsumerProfileTransactionHistoryResponse.Payload.class,
    com.moneygram.agentconnect.BillerSearchResponse.Payload.class,
    com.moneygram.agentconnect.TranslationsResponse.Payload.class,
    com.moneygram.agentconnect.CheckInResponse.Payload.class,
    com.moneygram.agentconnect.FeeLookupResponse.Payload.class,
    com.moneygram.agentconnect.GetServiceOptionsResponse.Payload.class,
    com.moneygram.agentconnect.ProfileChangeResponse.Payload.class,
    com.moneygram.agentconnect.VariableReceiptInfoResponse.Payload.class,
    com.moneygram.agentconnect.GetUCPByConsumerAttributesResponse.Payload.class
})
public class Payload
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected boolean doCheckIn;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    protected int flags;

    /**
     * Gets the value of the doCheckIn property.
     * 
     */
    public boolean isDoCheckIn() {
        return doCheckIn;
    }

    /**
     * Sets the value of the doCheckIn property.
     * 
     */
    public void setDoCheckIn(boolean value) {
        this.doCheckIn = value;
    }

    /**
     * Gets the value of the timeStamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Sets the value of the timeStamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Gets the value of the flags property.
     * 
     */
    public int getFlags() {
        return flags;
    }

    /**
     * Sets the value of the flags property.
     * 
     */
    public void setFlags(int value) {
        this.flags = value;
    }

}
