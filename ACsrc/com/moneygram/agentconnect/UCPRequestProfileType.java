
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for UCPRequestProfileType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UCPRequestProfileType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dateOfBirth" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="emailAddress" type="{http://www.moneygram.com/AgentConnect1705}UCPRequestEmailType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.moneygram.com/AgentConnect1705}NameType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="phoneNumber" type="{http://www.moneygram.com/AgentConnect1705}UCPRequestPhoneType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rawAddress" type="{http://www.moneygram.com/AgentConnect1705}RawAddressType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UCPRequestProfileType", propOrder = {
    "dateOfBirth",
    "emailAddress",
    "name",
    "phoneNumber",
    "rawAddress"
})
public class UCPRequestProfileType
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateOfBirth;
    protected List<UCPRequestEmailType> emailAddress;
    protected List<NameType> name;
    protected List<UCPRequestPhoneType> phoneNumber;
    protected List<RawAddressType> rawAddress;

    /**
     * Gets the value of the dateOfBirth property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the value of the dateOfBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfBirth(XMLGregorianCalendar value) {
        this.dateOfBirth = value;
    }

    /**
     * Gets the value of the emailAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the emailAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEmailAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UCPRequestEmailType }
     * 
     * 
     */
    public List<UCPRequestEmailType> getEmailAddress() {
        if (emailAddress == null) {
            emailAddress = new ArrayList<UCPRequestEmailType>();
        }
        return this.emailAddress;
    }

    /**
     * Gets the value of the name property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the name property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NameType }
     * 
     * 
     */
    public List<NameType> getName() {
        if (name == null) {
            name = new ArrayList<NameType>();
        }
        return this.name;
    }

    /**
     * Gets the value of the phoneNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the phoneNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPhoneNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UCPRequestPhoneType }
     * 
     * 
     */
    public List<UCPRequestPhoneType> getPhoneNumber() {
        if (phoneNumber == null) {
            phoneNumber = new ArrayList<UCPRequestPhoneType>();
        }
        return this.phoneNumber;
    }

    /**
     * Gets the value of the rawAddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rawAddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRawAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RawAddressType }
     * 
     * 
     */
    public List<RawAddressType> getRawAddress() {
        if (rawAddress == null) {
            rawAddress = new ArrayList<RawAddressType>();
        }
        return this.rawAddress;
    }

}
