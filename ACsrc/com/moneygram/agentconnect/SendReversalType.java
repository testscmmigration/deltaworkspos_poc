
package com.moneygram.agentconnect;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SendReversalType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SendReversalType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="C"/>
 *     &lt;enumeration value="R"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SendReversalType")
@XmlEnum
public enum SendReversalType {


    /**
     * Cancel
     * 
     */
    C,

    /**
     * Refund
     * 
     */
    R;

    public String value() {
        return name();
    }

    public static SendReversalType fromValue(String v) {
        return valueOf(v);
    }

}
