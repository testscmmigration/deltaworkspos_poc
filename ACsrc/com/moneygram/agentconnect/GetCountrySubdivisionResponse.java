
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCountrySubdivisionResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCountrySubdivisionResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="version" type="{http://www.moneygram.com/AgentConnect1705}VersionType"/>
 *                   &lt;element name="countrySubdivisionInfos" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="countrySubdivisionInfo" type="{http://www.moneygram.com/AgentConnect1705}CountrySubdivisionInfo" maxOccurs="unbounded"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCountrySubdivisionResponse", propOrder = {
    "payload"
})
public class GetCountrySubdivisionResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<GetCountrySubdivisionResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetCountrySubdivisionResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<GetCountrySubdivisionResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetCountrySubdivisionResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<GetCountrySubdivisionResponse.Payload> value) {
        this.payload = ((JAXBElement<GetCountrySubdivisionResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="version" type="{http://www.moneygram.com/AgentConnect1705}VersionType"/>
     *         &lt;element name="countrySubdivisionInfos" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="countrySubdivisionInfo" type="{http://www.moneygram.com/AgentConnect1705}CountrySubdivisionInfo" maxOccurs="unbounded"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "version",
        "countrySubdivisionInfos"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(required = true)
        protected String version;
        protected GetCountrySubdivisionResponse.Payload.CountrySubdivisionInfos countrySubdivisionInfos;

        /**
         * Gets the value of the version property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVersion() {
            return version;
        }

        /**
         * Sets the value of the version property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVersion(String value) {
            this.version = value;
        }

        /**
         * Gets the value of the countrySubdivisionInfos property.
         * 
         * @return
         *     possible object is
         *     {@link GetCountrySubdivisionResponse.Payload.CountrySubdivisionInfos }
         *     
         */
        public GetCountrySubdivisionResponse.Payload.CountrySubdivisionInfos getCountrySubdivisionInfos() {
            return countrySubdivisionInfos;
        }

        /**
         * Sets the value of the countrySubdivisionInfos property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetCountrySubdivisionResponse.Payload.CountrySubdivisionInfos }
         *     
         */
        public void setCountrySubdivisionInfos(GetCountrySubdivisionResponse.Payload.CountrySubdivisionInfos value) {
            this.countrySubdivisionInfos = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="countrySubdivisionInfo" type="{http://www.moneygram.com/AgentConnect1705}CountrySubdivisionInfo" maxOccurs="unbounded"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "countrySubdivisionInfo"
        })
        public static class CountrySubdivisionInfos
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            @XmlElement(required = true)
            protected List<CountrySubdivisionInfo> countrySubdivisionInfo;

            /**
             * Gets the value of the countrySubdivisionInfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the countrySubdivisionInfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getCountrySubdivisionInfo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link CountrySubdivisionInfo }
             * 
             * 
             */
            public List<CountrySubdivisionInfo> getCountrySubdivisionInfo() {
                if (countrySubdivisionInfo == null) {
                    countrySubdivisionInfo = new ArrayList<CountrySubdivisionInfo>();
                }
                return this.countrySubdivisionInfo;
            }

        }

    }

}
