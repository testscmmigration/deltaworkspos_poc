
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetProfileSenderResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetProfileSenderResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="mgiSessionID" type="{http://www.moneygram.com/AgentConnect1705}MGISessionIDType"/>
 *                   &lt;element name="GAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType"/>
 *                   &lt;element name="currentValues" type="{http://www.moneygram.com/AgentConnect1705}CurrentValuesType" minOccurs="0"/>
 *                   &lt;element name="infos" type="{http://www.moneygram.com/AgentConnect1705}InfosType" minOccurs="0"/>
 *                   &lt;element name="receivers" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="receiver" type="{http://www.moneygram.com/AgentConnect1705}CurrentValuesType" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="billers" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="biller" type="{http://www.moneygram.com/AgentConnect1705}CurrentValuesType" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetProfileSenderResponse", propOrder = {
    "payload"
})
public class GetProfileSenderResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<GetProfileSenderResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetProfileSenderResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<GetProfileSenderResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetProfileSenderResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<GetProfileSenderResponse.Payload> value) {
        this.payload = ((JAXBElement<GetProfileSenderResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="mgiSessionID" type="{http://www.moneygram.com/AgentConnect1705}MGISessionIDType"/>
     *         &lt;element name="GAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType"/>
     *         &lt;element name="currentValues" type="{http://www.moneygram.com/AgentConnect1705}CurrentValuesType" minOccurs="0"/>
     *         &lt;element name="infos" type="{http://www.moneygram.com/AgentConnect1705}InfosType" minOccurs="0"/>
     *         &lt;element name="receivers" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="receiver" type="{http://www.moneygram.com/AgentConnect1705}CurrentValuesType" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="billers" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="biller" type="{http://www.moneygram.com/AgentConnect1705}CurrentValuesType" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mgiSessionID",
        "gafVersionNumber",
        "currentValues",
        "infos",
        "receivers",
        "billers"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(required = true)
        protected String mgiSessionID;
        @XmlElement(name = "GAFVersionNumber", required = true)
        protected String gafVersionNumber;
        protected CurrentValuesType currentValues;
        protected InfosType infos;
        protected GetProfileSenderResponse.Payload.Receivers receivers;
        protected GetProfileSenderResponse.Payload.Billers billers;

        /**
         * Gets the value of the mgiSessionID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMgiSessionID() {
            return mgiSessionID;
        }

        /**
         * Sets the value of the mgiSessionID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMgiSessionID(String value) {
            this.mgiSessionID = value;
        }

        /**
         * Gets the value of the gafVersionNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGAFVersionNumber() {
            return gafVersionNumber;
        }

        /**
         * Sets the value of the gafVersionNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGAFVersionNumber(String value) {
            this.gafVersionNumber = value;
        }

        /**
         * Gets the value of the currentValues property.
         * 
         * @return
         *     possible object is
         *     {@link CurrentValuesType }
         *     
         */
        public CurrentValuesType getCurrentValues() {
            return currentValues;
        }

        /**
         * Sets the value of the currentValues property.
         * 
         * @param value
         *     allowed object is
         *     {@link CurrentValuesType }
         *     
         */
        public void setCurrentValues(CurrentValuesType value) {
            this.currentValues = value;
        }

        /**
         * Gets the value of the infos property.
         * 
         * @return
         *     possible object is
         *     {@link InfosType }
         *     
         */
        public InfosType getInfos() {
            return infos;
        }

        /**
         * Sets the value of the infos property.
         * 
         * @param value
         *     allowed object is
         *     {@link InfosType }
         *     
         */
        public void setInfos(InfosType value) {
            this.infos = value;
        }

        /**
         * Gets the value of the receivers property.
         * 
         * @return
         *     possible object is
         *     {@link GetProfileSenderResponse.Payload.Receivers }
         *     
         */
        public GetProfileSenderResponse.Payload.Receivers getReceivers() {
            return receivers;
        }

        /**
         * Sets the value of the receivers property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetProfileSenderResponse.Payload.Receivers }
         *     
         */
        public void setReceivers(GetProfileSenderResponse.Payload.Receivers value) {
            this.receivers = value;
        }

        /**
         * Gets the value of the billers property.
         * 
         * @return
         *     possible object is
         *     {@link GetProfileSenderResponse.Payload.Billers }
         *     
         */
        public GetProfileSenderResponse.Payload.Billers getBillers() {
            return billers;
        }

        /**
         * Sets the value of the billers property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetProfileSenderResponse.Payload.Billers }
         *     
         */
        public void setBillers(GetProfileSenderResponse.Payload.Billers value) {
            this.billers = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="biller" type="{http://www.moneygram.com/AgentConnect1705}CurrentValuesType" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "biller"
        })
        public static class Billers
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected List<CurrentValuesType> biller;

            /**
             * Gets the value of the biller property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the biller property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getBiller().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link CurrentValuesType }
             * 
             * 
             */
            public List<CurrentValuesType> getBiller() {
                if (biller == null) {
                    biller = new ArrayList<CurrentValuesType>();
                }
                return this.biller;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="receiver" type="{http://www.moneygram.com/AgentConnect1705}CurrentValuesType" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "receiver"
        })
        public static class Receivers
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected List<CurrentValuesType> receiver;

            /**
             * Gets the value of the receiver property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the receiver property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getReceiver().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link CurrentValuesType }
             * 
             * 
             */
            public List<CurrentValuesType> getReceiver() {
                if (receiver == null) {
                    receiver = new ArrayList<CurrentValuesType>();
                }
                return this.receiver;
            }

        }

    }

}
