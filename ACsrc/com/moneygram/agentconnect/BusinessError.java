
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BusinessError complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BusinessError">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="errorCode" type="{http://www.moneygram.com/AgentConnect1705}ErrorCodeType"/>
 *         &lt;element name="message" type="{http://www.moneygram.com/AgentConnect1705}ErrorStringLongType"/>
 *         &lt;element name="subErrorCode" type="{http://www.moneygram.com/AgentConnect1705}ErrorCodeType" minOccurs="0"/>
 *         &lt;element name="detailString" type="{http://www.moneygram.com/AgentConnect1705}ErrorStringShortType" minOccurs="0"/>
 *         &lt;element name="offendingField" type="{http://www.moneygram.com/AgentConnect1705}InfoKeyType" minOccurs="0"/>
 *         &lt;element name="errorCategory" type="{http://www.moneygram.com/AgentConnect1705}ErrorCategoryType"/>
 *         &lt;element name="messageShort" type="{http://www.moneygram.com/AgentConnect1705}ErrorStringShortType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BusinessError", propOrder = {
    "errorCode",
    "message",
    "subErrorCode",
    "detailString",
    "offendingField",
    "errorCategory",
    "messageShort"
})
public class BusinessError
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected BigInteger errorCode;
    @XmlElement(required = true)
    protected String message;
    protected BigInteger subErrorCode;
    protected String detailString;
    protected String offendingField;
    @XmlElement(required = true)
    protected String errorCategory;
    protected String messageShort;

    /**
     * Gets the value of the errorCode property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getErrorCode() {
        return errorCode;
    }

    /**
     * Sets the value of the errorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setErrorCode(BigInteger value) {
        this.errorCode = value;
    }

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Gets the value of the subErrorCode property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSubErrorCode() {
        return subErrorCode;
    }

    /**
     * Sets the value of the subErrorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSubErrorCode(BigInteger value) {
        this.subErrorCode = value;
    }

    /**
     * Gets the value of the detailString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailString() {
        return detailString;
    }

    /**
     * Sets the value of the detailString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailString(String value) {
        this.detailString = value;
    }

    /**
     * Gets the value of the offendingField property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOffendingField() {
        return offendingField;
    }

    /**
     * Sets the value of the offendingField property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOffendingField(String value) {
        this.offendingField = value;
    }

    /**
     * Gets the value of the errorCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorCategory() {
        return errorCategory;
    }

    /**
     * Sets the value of the errorCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorCategory(String value) {
        this.errorCategory = value;
    }

    /**
     * Gets the value of the messageShort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageShort() {
        return messageShort;
    }

    /**
     * Sets the value of the messageShort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageShort(String value) {
        this.messageShort = value;
    }

}
