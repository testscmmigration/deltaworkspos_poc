
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PromotionLookupByCodeResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PromotionLookupByCodeResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="promotionLookupInfo" type="{http://www.moneygram.com/AgentConnect1705}PromotionLookupInfo" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PromotionLookupByCodeResponse", propOrder = {
    "payload"
})
public class PromotionLookupByCodeResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<PromotionLookupByCodeResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link PromotionLookupByCodeResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<PromotionLookupByCodeResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link PromotionLookupByCodeResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<PromotionLookupByCodeResponse.Payload> value) {
        this.payload = ((JAXBElement<PromotionLookupByCodeResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="promotionLookupInfo" type="{http://www.moneygram.com/AgentConnect1705}PromotionLookupInfo" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "promotionLookupInfo"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected PromotionLookupInfo promotionLookupInfo;

        /**
         * Gets the value of the promotionLookupInfo property.
         * 
         * @return
         *     possible object is
         *     {@link PromotionLookupInfo }
         *     
         */
        public PromotionLookupInfo getPromotionLookupInfo() {
            return promotionLookupInfo;
        }

        /**
         * Sets the value of the promotionLookupInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link PromotionLookupInfo }
         *     
         */
        public void setPromotionLookupInfo(PromotionLookupInfo value) {
            this.promotionLookupInfo = value;
        }

    }

}
