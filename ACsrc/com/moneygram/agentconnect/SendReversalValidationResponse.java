
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SendReversalValidationResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SendReversalValidationResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="mgiSessionID" type="{http://www.moneygram.com/AgentConnect1705}MGISessionIDType"/>
 *                   &lt;element name="readyForCommit" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="GAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType"/>
 *                   &lt;element name="sendAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountNonZeroType" minOccurs="0"/>
 *                   &lt;element name="sendCurrency" type="{http://www.moneygram.com/AgentConnect1705}CurrencyCodeType" minOccurs="0"/>
 *                   &lt;element name="totalSendFees" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType" minOccurs="0"/>
 *                   &lt;element name="totalSendTaxes" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType" minOccurs="0"/>
 *                   &lt;element name="totalReversalAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountNonZeroType" minOccurs="0"/>
 *                   &lt;element name="detailSendAmounts" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="detailSendAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="reversalType" type="{http://www.moneygram.com/AgentConnect1705}SendReversalType" minOccurs="0"/>
 *                   &lt;element name="fieldsToCollect" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="fieldToCollect" type="{http://www.moneygram.com/AgentConnect1705}InfoBase" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="receipts" type="{http://www.moneygram.com/AgentConnect1705}PreCompletionReceiptType" minOccurs="0"/>
 *                   &lt;element name="receiptInfo" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="disclosureTexts" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="disclosureText" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SendReversalValidationResponse", propOrder = {
    "payload"
})
public class SendReversalValidationResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<SendReversalValidationResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SendReversalValidationResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<SendReversalValidationResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SendReversalValidationResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<SendReversalValidationResponse.Payload> value) {
        this.payload = ((JAXBElement<SendReversalValidationResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="mgiSessionID" type="{http://www.moneygram.com/AgentConnect1705}MGISessionIDType"/>
     *         &lt;element name="readyForCommit" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="GAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType"/>
     *         &lt;element name="sendAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountNonZeroType" minOccurs="0"/>
     *         &lt;element name="sendCurrency" type="{http://www.moneygram.com/AgentConnect1705}CurrencyCodeType" minOccurs="0"/>
     *         &lt;element name="totalSendFees" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType" minOccurs="0"/>
     *         &lt;element name="totalSendTaxes" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType" minOccurs="0"/>
     *         &lt;element name="totalReversalAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountNonZeroType" minOccurs="0"/>
     *         &lt;element name="detailSendAmounts" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="detailSendAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountInfo" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="reversalType" type="{http://www.moneygram.com/AgentConnect1705}SendReversalType" minOccurs="0"/>
     *         &lt;element name="fieldsToCollect" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="fieldToCollect" type="{http://www.moneygram.com/AgentConnect1705}InfoBase" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="receipts" type="{http://www.moneygram.com/AgentConnect1705}PreCompletionReceiptType" minOccurs="0"/>
     *         &lt;element name="receiptInfo" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="disclosureTexts" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="disclosureText" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mgiSessionID",
        "readyForCommit",
        "gafVersionNumber",
        "sendAmount",
        "sendCurrency",
        "totalSendFees",
        "totalSendTaxes",
        "totalReversalAmount",
        "detailSendAmounts",
        "reversalType",
        "fieldsToCollect",
        "receipts",
        "receiptInfo"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(required = true)
        protected String mgiSessionID;
        protected boolean readyForCommit;
        @XmlElement(name = "GAFVersionNumber", required = true)
        protected String gafVersionNumber;
        protected BigDecimal sendAmount;
        protected String sendCurrency;
        protected BigDecimal totalSendFees;
        protected BigDecimal totalSendTaxes;
        protected BigDecimal totalReversalAmount;
        protected SendReversalValidationResponse.Payload.DetailSendAmounts detailSendAmounts;
        protected SendReversalType reversalType;
        protected SendReversalValidationResponse.Payload.FieldsToCollect fieldsToCollect;
        protected PreCompletionReceiptType receipts;
        protected SendReversalValidationResponse.Payload.ReceiptInfo receiptInfo;

        /**
         * Gets the value of the mgiSessionID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMgiSessionID() {
            return mgiSessionID;
        }

        /**
         * Sets the value of the mgiSessionID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMgiSessionID(String value) {
            this.mgiSessionID = value;
        }

        /**
         * Gets the value of the readyForCommit property.
         * 
         */
        public boolean isReadyForCommit() {
            return readyForCommit;
        }

        /**
         * Sets the value of the readyForCommit property.
         * 
         */
        public void setReadyForCommit(boolean value) {
            this.readyForCommit = value;
        }

        /**
         * Gets the value of the gafVersionNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGAFVersionNumber() {
            return gafVersionNumber;
        }

        /**
         * Sets the value of the gafVersionNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGAFVersionNumber(String value) {
            this.gafVersionNumber = value;
        }

        /**
         * Gets the value of the sendAmount property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getSendAmount() {
            return sendAmount;
        }

        /**
         * Sets the value of the sendAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setSendAmount(BigDecimal value) {
            this.sendAmount = value;
        }

        /**
         * Gets the value of the sendCurrency property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSendCurrency() {
            return sendCurrency;
        }

        /**
         * Sets the value of the sendCurrency property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSendCurrency(String value) {
            this.sendCurrency = value;
        }

        /**
         * Gets the value of the totalSendFees property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getTotalSendFees() {
            return totalSendFees;
        }

        /**
         * Sets the value of the totalSendFees property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setTotalSendFees(BigDecimal value) {
            this.totalSendFees = value;
        }

        /**
         * Gets the value of the totalSendTaxes property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getTotalSendTaxes() {
            return totalSendTaxes;
        }

        /**
         * Sets the value of the totalSendTaxes property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setTotalSendTaxes(BigDecimal value) {
            this.totalSendTaxes = value;
        }

        /**
         * Gets the value of the totalReversalAmount property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getTotalReversalAmount() {
            return totalReversalAmount;
        }

        /**
         * Sets the value of the totalReversalAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setTotalReversalAmount(BigDecimal value) {
            this.totalReversalAmount = value;
        }

        /**
         * Gets the value of the detailSendAmounts property.
         * 
         * @return
         *     possible object is
         *     {@link SendReversalValidationResponse.Payload.DetailSendAmounts }
         *     
         */
        public SendReversalValidationResponse.Payload.DetailSendAmounts getDetailSendAmounts() {
            return detailSendAmounts;
        }

        /**
         * Sets the value of the detailSendAmounts property.
         * 
         * @param value
         *     allowed object is
         *     {@link SendReversalValidationResponse.Payload.DetailSendAmounts }
         *     
         */
        public void setDetailSendAmounts(SendReversalValidationResponse.Payload.DetailSendAmounts value) {
            this.detailSendAmounts = value;
        }

        /**
         * Gets the value of the reversalType property.
         * 
         * @return
         *     possible object is
         *     {@link SendReversalType }
         *     
         */
        public SendReversalType getReversalType() {
            return reversalType;
        }

        /**
         * Sets the value of the reversalType property.
         * 
         * @param value
         *     allowed object is
         *     {@link SendReversalType }
         *     
         */
        public void setReversalType(SendReversalType value) {
            this.reversalType = value;
        }

        /**
         * Gets the value of the fieldsToCollect property.
         * 
         * @return
         *     possible object is
         *     {@link SendReversalValidationResponse.Payload.FieldsToCollect }
         *     
         */
        public SendReversalValidationResponse.Payload.FieldsToCollect getFieldsToCollect() {
            return fieldsToCollect;
        }

        /**
         * Sets the value of the fieldsToCollect property.
         * 
         * @param value
         *     allowed object is
         *     {@link SendReversalValidationResponse.Payload.FieldsToCollect }
         *     
         */
        public void setFieldsToCollect(SendReversalValidationResponse.Payload.FieldsToCollect value) {
            this.fieldsToCollect = value;
        }

        /**
         * Gets the value of the receipts property.
         * 
         * @return
         *     possible object is
         *     {@link PreCompletionReceiptType }
         *     
         */
        public PreCompletionReceiptType getReceipts() {
            return receipts;
        }

        /**
         * Sets the value of the receipts property.
         * 
         * @param value
         *     allowed object is
         *     {@link PreCompletionReceiptType }
         *     
         */
        public void setReceipts(PreCompletionReceiptType value) {
            this.receipts = value;
        }

        /**
         * Gets the value of the receiptInfo property.
         * 
         * @return
         *     possible object is
         *     {@link SendReversalValidationResponse.Payload.ReceiptInfo }
         *     
         */
        public SendReversalValidationResponse.Payload.ReceiptInfo getReceiptInfo() {
            return receiptInfo;
        }

        /**
         * Sets the value of the receiptInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link SendReversalValidationResponse.Payload.ReceiptInfo }
         *     
         */
        public void setReceiptInfo(SendReversalValidationResponse.Payload.ReceiptInfo value) {
            this.receiptInfo = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="detailSendAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountInfo" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "detailSendAmount"
        })
        public static class DetailSendAmounts
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected List<AmountInfo> detailSendAmount;

            /**
             * Gets the value of the detailSendAmount property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the detailSendAmount property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getDetailSendAmount().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link AmountInfo }
             * 
             * 
             */
            public List<AmountInfo> getDetailSendAmount() {
                if (detailSendAmount == null) {
                    detailSendAmount = new ArrayList<AmountInfo>();
                }
                return this.detailSendAmount;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="fieldToCollect" type="{http://www.moneygram.com/AgentConnect1705}InfoBase" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "fieldToCollect"
        })
        public static class FieldsToCollect
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected List<InfoBase> fieldToCollect;

            /**
             * Gets the value of the fieldToCollect property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the fieldToCollect property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getFieldToCollect().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link InfoBase }
             * 
             * 
             */
            public List<InfoBase> getFieldToCollect() {
                if (fieldToCollect == null) {
                    fieldToCollect = new ArrayList<InfoBase>();
                }
                return this.fieldToCollect;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="disclosureTexts" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="disclosureText" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "disclosureTexts"
        })
        public static class ReceiptInfo
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected SendReversalValidationResponse.Payload.ReceiptInfo.DisclosureTexts disclosureTexts;

            /**
             * Gets the value of the disclosureTexts property.
             * 
             * @return
             *     possible object is
             *     {@link SendReversalValidationResponse.Payload.ReceiptInfo.DisclosureTexts }
             *     
             */
            public SendReversalValidationResponse.Payload.ReceiptInfo.DisclosureTexts getDisclosureTexts() {
                return disclosureTexts;
            }

            /**
             * Sets the value of the disclosureTexts property.
             * 
             * @param value
             *     allowed object is
             *     {@link SendReversalValidationResponse.Payload.ReceiptInfo.DisclosureTexts }
             *     
             */
            public void setDisclosureTexts(SendReversalValidationResponse.Payload.ReceiptInfo.DisclosureTexts value) {
                this.disclosureTexts = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="disclosureText" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "disclosureText"
            })
            public static class DisclosureTexts
                implements Serializable
            {

                private final static long serialVersionUID = 1L;
                protected List<TextTranslationType> disclosureText;

                /**
                 * Gets the value of the disclosureText property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the disclosureText property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getDisclosureText().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link TextTranslationType }
                 * 
                 * 
                 */
                public List<TextTranslationType> getDisclosureText() {
                    if (disclosureText == null) {
                        disclosureText = new ArrayList<TextTranslationType>();
                    }
                    return this.disclosureText;
                }

            }

        }

    }

}
