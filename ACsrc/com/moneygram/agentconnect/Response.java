
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Response complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Response">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="errors" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="error" type="{http://www.moneygram.com/AgentConnect1705}BusinessError" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Response", propOrder = {
    "errors"
})
@XmlSeeAlso({
    DirectoryOfAgentsByAreaCodePrefixResponse.class,
    GetAllFieldsResponse.class,
    GetDepositInformationResponse.class,
    GetReceiptForReprintResponse.class,
    GetDebugDataResponse.class,
    GetProfileReceiverResponse.class,
    CreateOrUpdateProfileSenderResponse.class,
    ProfileResponse.class,
    ComplianceTransactionResponse.class,
    SaveDebugDataResponse.class,
    GetBroadcastMessagesResponse.class,
    BillPaymentSummaryReportResponse.class,
    ConsumerHistoryLookupResponse.class,
    SendReversalValidationResponse.class,
    DirectoryOfAgentsByCityResponse.class,
    BillPaymentDetailReportResponse.class,
    DirectoryOfAgentsByZipResponse.class,
    SaveProfileResponse.class,
    InjectedInstructionResponse.class,
    SearchConsumerProfilesResponse.class,
    MoneyGramSendSummaryReportResponse.class,
    BPValidationResponse.class,
    CityListResponse.class,
    DwPasswordResponse.class,
    AmendValidationResponse.class,
    SaveSubagentsResponse.class,
    DwProfileResponse.class,
    GetBankDetailsResponse.class,
    GetBankDetailsByLevelResponse.class,
    GetCurrencyInfoResponse.class,
    DwInitialSetupResponse.class,
    TransactionLookupResponse.class,
    MoneyGramSendDetailReportWithTaxResponse.class,
    GetCountrySubdivisionResponse.class,
    DoddFrankStateRegulatorInfoResponse.class,
    SubagentsResponse.class,
    DisclosureTextDetailsResponse.class,
    SearchStagedTransactionsResponse.class,
    GetProfileConsumerResponse.class,
    OpenOTPLoginResponse.class,
    MoneyGramSendDetailReportResponse.class,
    CreateOrUpdateProfileReceiverResponse.class,
    DepositAnnouncementResponse.class,
    MoneyGramReceiveSummaryReportResponse.class,
    CompleteSessionResponse.class,
    MoneyOrderTotalResponse.class,
    GetCountryInfoResponse.class,
    RegisterHardTokenResponse.class,
    ReceiveReversalValidationResponse.class,
    ConfirmTokenResponse.class,
    VersionManifestResponse.class,
    ReceiveValidationResponse.class,
    GetProfileSenderResponse.class,
    ReceiptsFormatDetailsResponse.class,
    IndustryResponse.class,
    SaveConsumerProfileImageResponse.class,
    InitialSetupResponse.class,
    GetEnumerationsResponse.class,
    MoneyGramReceiveDetailReportResponse.class,
    CreateOrUpdateProfileConsumerResponse.class,
    PromotionLookupByCodeResponse.class,
    GetAllErrorsResponse.class,
    GetDepositBankListResponse.class,
    SendValidationResponse.class,
    FeeLookupBySendCountryResponse.class,
    GetConsumerProfileTransactionHistoryResponse.class,
    BillerSearchResponse.class,
    TranslationsResponse.class,
    CheckInResponse.class,
    FeeLookupResponse.class,
    GetServiceOptionsResponse.class,
    ProfileChangeResponse.class,
    VariableReceiptInfoResponse.class,
    GetUCPByConsumerAttributesResponse.class
})
public class Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected Response.Errors errors;

    /**
     * Gets the value of the errors property.
     * 
     * @return
     *     possible object is
     *     {@link Response.Errors }
     *     
     */
    public Response.Errors getErrors() {
        return errors;
    }

    /**
     * Sets the value of the errors property.
     * 
     * @param value
     *     allowed object is
     *     {@link Response.Errors }
     *     
     */
    public void setErrors(Response.Errors value) {
        this.errors = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="error" type="{http://www.moneygram.com/AgentConnect1705}BusinessError" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "error"
    })
    public static class Errors
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<BusinessError> error;

        /**
         * Gets the value of the error property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the error property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getError().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BusinessError }
         * 
         * 
         */
        public List<BusinessError> getError() {
            if (error == null) {
                error = new ArrayList<BusinessError>();
            }
            return this.error;
        }

    }

}
