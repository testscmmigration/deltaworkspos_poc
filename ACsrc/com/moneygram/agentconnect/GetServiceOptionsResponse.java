
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetServiceOptionsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetServiceOptionsResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="destinationCountryInfos" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="destinationCountryInfo" type="{http://www.moneygram.com/AgentConnect1705}DestinationCountryInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetServiceOptionsResponse", propOrder = {
    "payload"
})
public class GetServiceOptionsResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<GetServiceOptionsResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetServiceOptionsResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<GetServiceOptionsResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetServiceOptionsResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<GetServiceOptionsResponse.Payload> value) {
        this.payload = ((JAXBElement<GetServiceOptionsResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="destinationCountryInfos" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="destinationCountryInfo" type="{http://www.moneygram.com/AgentConnect1705}DestinationCountryInfo" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "destinationCountryInfos"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected GetServiceOptionsResponse.Payload.DestinationCountryInfos destinationCountryInfos;

        /**
         * Gets the value of the destinationCountryInfos property.
         * 
         * @return
         *     possible object is
         *     {@link GetServiceOptionsResponse.Payload.DestinationCountryInfos }
         *     
         */
        public GetServiceOptionsResponse.Payload.DestinationCountryInfos getDestinationCountryInfos() {
            return destinationCountryInfos;
        }

        /**
         * Sets the value of the destinationCountryInfos property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetServiceOptionsResponse.Payload.DestinationCountryInfos }
         *     
         */
        public void setDestinationCountryInfos(GetServiceOptionsResponse.Payload.DestinationCountryInfos value) {
            this.destinationCountryInfos = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="destinationCountryInfo" type="{http://www.moneygram.com/AgentConnect1705}DestinationCountryInfo" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "destinationCountryInfo"
        })
        public static class DestinationCountryInfos
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected List<DestinationCountryInfo> destinationCountryInfo;

            /**
             * Gets the value of the destinationCountryInfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the destinationCountryInfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getDestinationCountryInfo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link DestinationCountryInfo }
             * 
             * 
             */
            public List<DestinationCountryInfo> getDestinationCountryInfo() {
                if (destinationCountryInfo == null) {
                    destinationCountryInfo = new ArrayList<DestinationCountryInfo>();
                }
                return this.destinationCountryInfo;
            }

        }

    }

}
