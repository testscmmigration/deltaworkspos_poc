
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetBroadcastMessagesRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetBroadcastMessagesRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="msgLanguageCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="offsetGMT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="messageInfos" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="messageInfo" type="{http://www.moneygram.com/AgentConnect1705}MessageInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="customFields" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="customField" type="{http://www.moneygram.com/AgentConnect1705}CustomFieldsType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetBroadcastMessagesRequest", propOrder = {
    "msgLanguageCode",
    "offsetGMT",
    "messageInfos",
    "customFields"
})
public class GetBroadcastMessagesRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected String msgLanguageCode;
    @XmlElement(required = true)
    protected String offsetGMT;
    protected GetBroadcastMessagesRequest.MessageInfos messageInfos;
    protected GetBroadcastMessagesRequest.CustomFields customFields;

    /**
     * Gets the value of the msgLanguageCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsgLanguageCode() {
        return msgLanguageCode;
    }

    /**
     * Sets the value of the msgLanguageCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsgLanguageCode(String value) {
        this.msgLanguageCode = value;
    }

    /**
     * Gets the value of the offsetGMT property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOffsetGMT() {
        return offsetGMT;
    }

    /**
     * Sets the value of the offsetGMT property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOffsetGMT(String value) {
        this.offsetGMT = value;
    }

    /**
     * Gets the value of the messageInfos property.
     * 
     * @return
     *     possible object is
     *     {@link GetBroadcastMessagesRequest.MessageInfos }
     *     
     */
    public GetBroadcastMessagesRequest.MessageInfos getMessageInfos() {
        return messageInfos;
    }

    /**
     * Sets the value of the messageInfos property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetBroadcastMessagesRequest.MessageInfos }
     *     
     */
    public void setMessageInfos(GetBroadcastMessagesRequest.MessageInfos value) {
        this.messageInfos = value;
    }

    /**
     * Gets the value of the customFields property.
     * 
     * @return
     *     possible object is
     *     {@link GetBroadcastMessagesRequest.CustomFields }
     *     
     */
    public GetBroadcastMessagesRequest.CustomFields getCustomFields() {
        return customFields;
    }

    /**
     * Sets the value of the customFields property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetBroadcastMessagesRequest.CustomFields }
     *     
     */
    public void setCustomFields(GetBroadcastMessagesRequest.CustomFields value) {
        this.customFields = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="customField" type="{http://www.moneygram.com/AgentConnect1705}CustomFieldsType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "customField"
    })
    public static class CustomFields
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<CustomFieldsType> customField;

        /**
         * Gets the value of the customField property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the customField property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCustomField().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link CustomFieldsType }
         * 
         * 
         */
        public List<CustomFieldsType> getCustomField() {
            if (customField == null) {
                customField = new ArrayList<CustomFieldsType>();
            }
            return this.customField;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="messageInfo" type="{http://www.moneygram.com/AgentConnect1705}MessageInfo" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "messageInfo"
    })
    public static class MessageInfos
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<MessageInfo> messageInfo;

        /**
         * Gets the value of the messageInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the messageInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getMessageInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link MessageInfo }
         * 
         * 
         */
        public List<MessageInfo> getMessageInfo() {
            if (messageInfo == null) {
                messageInfo = new ArrayList<MessageInfo>();
            }
            return this.messageInfo;
        }

    }

}
