
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCurrencyInfoResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCurrencyInfoResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="version" type="{http://www.moneygram.com/AgentConnect1705}VersionType"/>
 *                   &lt;element name="currencyInfos" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="currencyInfo" type="{http://www.moneygram.com/AgentConnect1705}CurrencyInfo" maxOccurs="unbounded"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCurrencyInfoResponse", propOrder = {
    "payload"
})
public class GetCurrencyInfoResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<GetCurrencyInfoResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetCurrencyInfoResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<GetCurrencyInfoResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetCurrencyInfoResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<GetCurrencyInfoResponse.Payload> value) {
        this.payload = ((JAXBElement<GetCurrencyInfoResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="version" type="{http://www.moneygram.com/AgentConnect1705}VersionType"/>
     *         &lt;element name="currencyInfos" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="currencyInfo" type="{http://www.moneygram.com/AgentConnect1705}CurrencyInfo" maxOccurs="unbounded"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "version",
        "currencyInfos"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(required = true)
        protected String version;
        protected GetCurrencyInfoResponse.Payload.CurrencyInfos currencyInfos;

        /**
         * Gets the value of the version property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVersion() {
            return version;
        }

        /**
         * Sets the value of the version property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVersion(String value) {
            this.version = value;
        }

        /**
         * Gets the value of the currencyInfos property.
         * 
         * @return
         *     possible object is
         *     {@link GetCurrencyInfoResponse.Payload.CurrencyInfos }
         *     
         */
        public GetCurrencyInfoResponse.Payload.CurrencyInfos getCurrencyInfos() {
            return currencyInfos;
        }

        /**
         * Sets the value of the currencyInfos property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetCurrencyInfoResponse.Payload.CurrencyInfos }
         *     
         */
        public void setCurrencyInfos(GetCurrencyInfoResponse.Payload.CurrencyInfos value) {
            this.currencyInfos = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="currencyInfo" type="{http://www.moneygram.com/AgentConnect1705}CurrencyInfo" maxOccurs="unbounded"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "currencyInfo"
        })
        public static class CurrencyInfos
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            @XmlElement(required = true)
            protected List<CurrencyInfo> currencyInfo;

            /**
             * Gets the value of the currencyInfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the currencyInfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getCurrencyInfo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link CurrencyInfo }
             * 
             * 
             */
            public List<CurrencyInfo> getCurrencyInfo() {
                if (currencyInfo == null) {
                    currencyInfo = new ArrayList<CurrencyInfo>();
                }
                return this.currencyInfo;
            }

        }

    }

}
