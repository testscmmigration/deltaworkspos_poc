
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CompleteSessionRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CompleteSessionRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="commit" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="mgiSessionType" type="{http://www.moneygram.com/AgentConnect1705}SessionType"/>
 *         &lt;element name="receiptImages" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="receiptImage" type="{http://www.moneygram.com/AgentConnect1705}ReceiptImageContentType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CompleteSessionRequest", propOrder = {
    "commit",
    "mgiSessionType",
    "receiptImages"
})
public class CompleteSessionRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected boolean commit;
    @XmlElement(required = true)
    protected SessionType mgiSessionType;
    protected CompleteSessionRequest.ReceiptImages receiptImages;

    /**
     * Gets the value of the commit property.
     * 
     */
    public boolean isCommit() {
        return commit;
    }

    /**
     * Sets the value of the commit property.
     * 
     */
    public void setCommit(boolean value) {
        this.commit = value;
    }

    /**
     * Gets the value of the mgiSessionType property.
     * 
     * @return
     *     possible object is
     *     {@link SessionType }
     *     
     */
    public SessionType getMgiSessionType() {
        return mgiSessionType;
    }

    /**
     * Sets the value of the mgiSessionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SessionType }
     *     
     */
    public void setMgiSessionType(SessionType value) {
        this.mgiSessionType = value;
    }

    /**
     * Gets the value of the receiptImages property.
     * 
     * @return
     *     possible object is
     *     {@link CompleteSessionRequest.ReceiptImages }
     *     
     */
    public CompleteSessionRequest.ReceiptImages getReceiptImages() {
        return receiptImages;
    }

    /**
     * Sets the value of the receiptImages property.
     * 
     * @param value
     *     allowed object is
     *     {@link CompleteSessionRequest.ReceiptImages }
     *     
     */
    public void setReceiptImages(CompleteSessionRequest.ReceiptImages value) {
        this.receiptImages = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="receiptImage" type="{http://www.moneygram.com/AgentConnect1705}ReceiptImageContentType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "receiptImage"
    })
    public static class ReceiptImages
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<ReceiptImageContentType> receiptImage;

        /**
         * Gets the value of the receiptImage property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the receiptImage property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getReceiptImage().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ReceiptImageContentType }
         * 
         * 
         */
        public List<ReceiptImageContentType> getReceiptImage() {
            if (receiptImage == null) {
                receiptImage = new ArrayList<ReceiptImageContentType>();
            }
            return this.receiptImage;
        }

    }

}
