
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SaveSubagentsRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SaveSubagentsRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="subagentProfileUpdates" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="subagentProfileUpdate" type="{http://www.moneygram.com/AgentConnect1705}SubagentProfileUpdateType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SaveSubagentsRequest", propOrder = {
    "subagentProfileUpdates"
})
public class SaveSubagentsRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected SaveSubagentsRequest.SubagentProfileUpdates subagentProfileUpdates;

    /**
     * Gets the value of the subagentProfileUpdates property.
     * 
     * @return
     *     possible object is
     *     {@link SaveSubagentsRequest.SubagentProfileUpdates }
     *     
     */
    public SaveSubagentsRequest.SubagentProfileUpdates getSubagentProfileUpdates() {
        return subagentProfileUpdates;
    }

    /**
     * Sets the value of the subagentProfileUpdates property.
     * 
     * @param value
     *     allowed object is
     *     {@link SaveSubagentsRequest.SubagentProfileUpdates }
     *     
     */
    public void setSubagentProfileUpdates(SaveSubagentsRequest.SubagentProfileUpdates value) {
        this.subagentProfileUpdates = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="subagentProfileUpdate" type="{http://www.moneygram.com/AgentConnect1705}SubagentProfileUpdateType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subagentProfileUpdate"
    })
    public static class SubagentProfileUpdates
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<SubagentProfileUpdateType> subagentProfileUpdate;

        /**
         * Gets the value of the subagentProfileUpdate property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the subagentProfileUpdate property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSubagentProfileUpdate().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SubagentProfileUpdateType }
         * 
         * 
         */
        public List<SubagentProfileUpdateType> getSubagentProfileUpdate() {
            if (subagentProfileUpdate == null) {
                subagentProfileUpdate = new ArrayList<SubagentProfileUpdateType>();
            }
            return this.subagentProfileUpdate;
        }

    }

}
