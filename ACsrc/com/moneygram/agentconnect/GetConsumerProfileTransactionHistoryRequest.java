
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for GetConsumerProfileTransactionHistoryRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetConsumerProfileTransactionHistoryRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="GAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType" minOccurs="0"/>
 *         &lt;element name="consumerProfileID" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileIDType"/>
 *         &lt;element name="consumerProfileIDType" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileIDTypeType"/>
 *         &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="endDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="maxRowsToReturn" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="sortDateOrder" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetConsumerProfileTransactionHistoryRequest", propOrder = {
    "gafVersionNumber",
    "consumerProfileID",
    "consumerProfileIDType",
    "startDate",
    "endDate",
    "maxRowsToReturn",
    "sortDateOrder"
})
public class GetConsumerProfileTransactionHistoryRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "GAFVersionNumber")
    protected String gafVersionNumber;
    @XmlElement(required = true)
    protected String consumerProfileID;
    @XmlElement(required = true)
    protected String consumerProfileIDType;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar startDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar endDate;
    protected int maxRowsToReturn;
    @XmlElement(required = true)
    protected String sortDateOrder;

    /**
     * Gets the value of the gafVersionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGAFVersionNumber() {
        return gafVersionNumber;
    }

    /**
     * Sets the value of the gafVersionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGAFVersionNumber(String value) {
        this.gafVersionNumber = value;
    }

    /**
     * Gets the value of the consumerProfileID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsumerProfileID() {
        return consumerProfileID;
    }

    /**
     * Sets the value of the consumerProfileID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsumerProfileID(String value) {
        this.consumerProfileID = value;
    }

    /**
     * Gets the value of the consumerProfileIDType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsumerProfileIDType() {
        return consumerProfileIDType;
    }

    /**
     * Sets the value of the consumerProfileIDType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsumerProfileIDType(String value) {
        this.consumerProfileIDType = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the maxRowsToReturn property.
     * 
     */
    public int getMaxRowsToReturn() {
        return maxRowsToReturn;
    }

    /**
     * Sets the value of the maxRowsToReturn property.
     * 
     */
    public void setMaxRowsToReturn(int value) {
        this.maxRowsToReturn = value;
    }

    /**
     * Gets the value of the sortDateOrder property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSortDateOrder() {
        return sortDateOrder;
    }

    /**
     * Sets the value of the sortDateOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSortDateOrder(String value) {
        this.sortDateOrder = value;
    }

}
