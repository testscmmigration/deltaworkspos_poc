
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 *  Returns new sessionID if purpose
 * 				needs one 
 * 
 * <p>Java class for TransactionLookupResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionLookupResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="GAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType" minOccurs="0"/>
 *                   &lt;element name="mgiSessionID" type="{http://www.moneygram.com/AgentConnect1705}MGISessionIDType" minOccurs="0"/>
 *                   &lt;element name="transactionStatus" type="{http://www.moneygram.com/AgentConnect1705}TransactionStatusType"/>
 *                   &lt;element name="redirectInfo" type="{http://www.moneygram.com/AgentConnect1705}RedirectInfo" minOccurs="0"/>
 *                   &lt;element name="sendAmounts" type="{http://www.moneygram.com/AgentConnect1705}SendAmountInfo" minOccurs="0"/>
 *                   &lt;element name="receiveAmounts" type="{http://www.moneygram.com/AgentConnect1705}ReceiveAmountInfo" minOccurs="0"/>
 *                   &lt;element name="promotionInfos" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="promotionInfo" type="{http://www.moneygram.com/AgentConnect1705}PromotionInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="disclosureTexts" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="disclosureText" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="receiveFeeDisclosureTexts" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="receiveFeeDisclosureText" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="receiveTaxDisclosureTexts" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="receiveTaxDisclosureText" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="currentValues" type="{http://www.moneygram.com/AgentConnect1705}CurrentValuesType" minOccurs="0"/>
 *                   &lt;element name="infos" type="{http://www.moneygram.com/AgentConnect1705}InfosType" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionLookupResponse", propOrder = {
    "payload"
})
public class TransactionLookupResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<TransactionLookupResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TransactionLookupResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<TransactionLookupResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TransactionLookupResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<TransactionLookupResponse.Payload> value) {
        this.payload = ((JAXBElement<TransactionLookupResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="GAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType" minOccurs="0"/>
     *         &lt;element name="mgiSessionID" type="{http://www.moneygram.com/AgentConnect1705}MGISessionIDType" minOccurs="0"/>
     *         &lt;element name="transactionStatus" type="{http://www.moneygram.com/AgentConnect1705}TransactionStatusType"/>
     *         &lt;element name="redirectInfo" type="{http://www.moneygram.com/AgentConnect1705}RedirectInfo" minOccurs="0"/>
     *         &lt;element name="sendAmounts" type="{http://www.moneygram.com/AgentConnect1705}SendAmountInfo" minOccurs="0"/>
     *         &lt;element name="receiveAmounts" type="{http://www.moneygram.com/AgentConnect1705}ReceiveAmountInfo" minOccurs="0"/>
     *         &lt;element name="promotionInfos" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="promotionInfo" type="{http://www.moneygram.com/AgentConnect1705}PromotionInfo" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="disclosureTexts" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="disclosureText" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="receiveFeeDisclosureTexts" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="receiveFeeDisclosureText" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="receiveTaxDisclosureTexts" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="receiveTaxDisclosureText" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="currentValues" type="{http://www.moneygram.com/AgentConnect1705}CurrentValuesType" minOccurs="0"/>
     *         &lt;element name="infos" type="{http://www.moneygram.com/AgentConnect1705}InfosType" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "gafVersionNumber",
        "mgiSessionID",
        "transactionStatus",
        "redirectInfo",
        "sendAmounts",
        "receiveAmounts",
        "promotionInfos",
        "disclosureTexts",
        "receiveFeeDisclosureTexts",
        "receiveTaxDisclosureTexts",
        "currentValues",
        "infos"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "GAFVersionNumber")
        protected String gafVersionNumber;
        protected String mgiSessionID;
        @XmlElement(required = true)
        protected TransactionStatusType transactionStatus;
        protected RedirectInfo redirectInfo;
        protected SendAmountInfo sendAmounts;
        protected ReceiveAmountInfo receiveAmounts;
        protected TransactionLookupResponse.Payload.PromotionInfos promotionInfos;
        protected TransactionLookupResponse.Payload.DisclosureTexts disclosureTexts;
        protected TransactionLookupResponse.Payload.ReceiveFeeDisclosureTexts receiveFeeDisclosureTexts;
        protected TransactionLookupResponse.Payload.ReceiveTaxDisclosureTexts receiveTaxDisclosureTexts;
        protected CurrentValuesType currentValues;
        protected InfosType infos;

        /**
         * Gets the value of the gafVersionNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGAFVersionNumber() {
            return gafVersionNumber;
        }

        /**
         * Sets the value of the gafVersionNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGAFVersionNumber(String value) {
            this.gafVersionNumber = value;
        }

        /**
         * Gets the value of the mgiSessionID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMgiSessionID() {
            return mgiSessionID;
        }

        /**
         * Sets the value of the mgiSessionID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMgiSessionID(String value) {
            this.mgiSessionID = value;
        }

        /**
         * Gets the value of the transactionStatus property.
         * 
         * @return
         *     possible object is
         *     {@link TransactionStatusType }
         *     
         */
        public TransactionStatusType getTransactionStatus() {
            return transactionStatus;
        }

        /**
         * Sets the value of the transactionStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link TransactionStatusType }
         *     
         */
        public void setTransactionStatus(TransactionStatusType value) {
            this.transactionStatus = value;
        }

        /**
         * Gets the value of the redirectInfo property.
         * 
         * @return
         *     possible object is
         *     {@link RedirectInfo }
         *     
         */
        public RedirectInfo getRedirectInfo() {
            return redirectInfo;
        }

        /**
         * Sets the value of the redirectInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link RedirectInfo }
         *     
         */
        public void setRedirectInfo(RedirectInfo value) {
            this.redirectInfo = value;
        }

        /**
         * Gets the value of the sendAmounts property.
         * 
         * @return
         *     possible object is
         *     {@link SendAmountInfo }
         *     
         */
        public SendAmountInfo getSendAmounts() {
            return sendAmounts;
        }

        /**
         * Sets the value of the sendAmounts property.
         * 
         * @param value
         *     allowed object is
         *     {@link SendAmountInfo }
         *     
         */
        public void setSendAmounts(SendAmountInfo value) {
            this.sendAmounts = value;
        }

        /**
         * Gets the value of the receiveAmounts property.
         * 
         * @return
         *     possible object is
         *     {@link ReceiveAmountInfo }
         *     
         */
        public ReceiveAmountInfo getReceiveAmounts() {
            return receiveAmounts;
        }

        /**
         * Sets the value of the receiveAmounts property.
         * 
         * @param value
         *     allowed object is
         *     {@link ReceiveAmountInfo }
         *     
         */
        public void setReceiveAmounts(ReceiveAmountInfo value) {
            this.receiveAmounts = value;
        }

        /**
         * Gets the value of the promotionInfos property.
         * 
         * @return
         *     possible object is
         *     {@link TransactionLookupResponse.Payload.PromotionInfos }
         *     
         */
        public TransactionLookupResponse.Payload.PromotionInfos getPromotionInfos() {
            return promotionInfos;
        }

        /**
         * Sets the value of the promotionInfos property.
         * 
         * @param value
         *     allowed object is
         *     {@link TransactionLookupResponse.Payload.PromotionInfos }
         *     
         */
        public void setPromotionInfos(TransactionLookupResponse.Payload.PromotionInfos value) {
            this.promotionInfos = value;
        }

        /**
         * Gets the value of the disclosureTexts property.
         * 
         * @return
         *     possible object is
         *     {@link TransactionLookupResponse.Payload.DisclosureTexts }
         *     
         */
        public TransactionLookupResponse.Payload.DisclosureTexts getDisclosureTexts() {
            return disclosureTexts;
        }

        /**
         * Sets the value of the disclosureTexts property.
         * 
         * @param value
         *     allowed object is
         *     {@link TransactionLookupResponse.Payload.DisclosureTexts }
         *     
         */
        public void setDisclosureTexts(TransactionLookupResponse.Payload.DisclosureTexts value) {
            this.disclosureTexts = value;
        }

        /**
         * Gets the value of the receiveFeeDisclosureTexts property.
         * 
         * @return
         *     possible object is
         *     {@link TransactionLookupResponse.Payload.ReceiveFeeDisclosureTexts }
         *     
         */
        public TransactionLookupResponse.Payload.ReceiveFeeDisclosureTexts getReceiveFeeDisclosureTexts() {
            return receiveFeeDisclosureTexts;
        }

        /**
         * Sets the value of the receiveFeeDisclosureTexts property.
         * 
         * @param value
         *     allowed object is
         *     {@link TransactionLookupResponse.Payload.ReceiveFeeDisclosureTexts }
         *     
         */
        public void setReceiveFeeDisclosureTexts(TransactionLookupResponse.Payload.ReceiveFeeDisclosureTexts value) {
            this.receiveFeeDisclosureTexts = value;
        }

        /**
         * Gets the value of the receiveTaxDisclosureTexts property.
         * 
         * @return
         *     possible object is
         *     {@link TransactionLookupResponse.Payload.ReceiveTaxDisclosureTexts }
         *     
         */
        public TransactionLookupResponse.Payload.ReceiveTaxDisclosureTexts getReceiveTaxDisclosureTexts() {
            return receiveTaxDisclosureTexts;
        }

        /**
         * Sets the value of the receiveTaxDisclosureTexts property.
         * 
         * @param value
         *     allowed object is
         *     {@link TransactionLookupResponse.Payload.ReceiveTaxDisclosureTexts }
         *     
         */
        public void setReceiveTaxDisclosureTexts(TransactionLookupResponse.Payload.ReceiveTaxDisclosureTexts value) {
            this.receiveTaxDisclosureTexts = value;
        }

        /**
         * Gets the value of the currentValues property.
         * 
         * @return
         *     possible object is
         *     {@link CurrentValuesType }
         *     
         */
        public CurrentValuesType getCurrentValues() {
            return currentValues;
        }

        /**
         * Sets the value of the currentValues property.
         * 
         * @param value
         *     allowed object is
         *     {@link CurrentValuesType }
         *     
         */
        public void setCurrentValues(CurrentValuesType value) {
            this.currentValues = value;
        }

        /**
         * Gets the value of the infos property.
         * 
         * @return
         *     possible object is
         *     {@link InfosType }
         *     
         */
        public InfosType getInfos() {
            return infos;
        }

        /**
         * Sets the value of the infos property.
         * 
         * @param value
         *     allowed object is
         *     {@link InfosType }
         *     
         */
        public void setInfos(InfosType value) {
            this.infos = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="disclosureText" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "disclosureText"
        })
        public static class DisclosureTexts
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected List<TextTranslationType> disclosureText;

            /**
             * Gets the value of the disclosureText property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the disclosureText property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getDisclosureText().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link TextTranslationType }
             * 
             * 
             */
            public List<TextTranslationType> getDisclosureText() {
                if (disclosureText == null) {
                    disclosureText = new ArrayList<TextTranslationType>();
                }
                return this.disclosureText;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="promotionInfo" type="{http://www.moneygram.com/AgentConnect1705}PromotionInfo" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "promotionInfo"
        })
        public static class PromotionInfos
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected List<PromotionInfo> promotionInfo;

            /**
             * Gets the value of the promotionInfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the promotionInfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getPromotionInfo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link PromotionInfo }
             * 
             * 
             */
            public List<PromotionInfo> getPromotionInfo() {
                if (promotionInfo == null) {
                    promotionInfo = new ArrayList<PromotionInfo>();
                }
                return this.promotionInfo;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="receiveFeeDisclosureText" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "receiveFeeDisclosureText"
        })
        public static class ReceiveFeeDisclosureTexts
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected List<TextTranslationType> receiveFeeDisclosureText;

            /**
             * Gets the value of the receiveFeeDisclosureText property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the receiveFeeDisclosureText property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getReceiveFeeDisclosureText().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link TextTranslationType }
             * 
             * 
             */
            public List<TextTranslationType> getReceiveFeeDisclosureText() {
                if (receiveFeeDisclosureText == null) {
                    receiveFeeDisclosureText = new ArrayList<TextTranslationType>();
                }
                return this.receiveFeeDisclosureText;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="receiveTaxDisclosureText" type="{http://www.moneygram.com/AgentConnect1705}TextTranslationType" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "receiveTaxDisclosureText"
        })
        public static class ReceiveTaxDisclosureTexts
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected List<TextTranslationType> receiveTaxDisclosureText;

            /**
             * Gets the value of the receiveTaxDisclosureText property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the receiveTaxDisclosureText property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getReceiveTaxDisclosureText().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link TextTranslationType }
             * 
             * 
             */
            public List<TextTranslationType> getReceiveTaxDisclosureText() {
                if (receiveTaxDisclosureText == null) {
                    receiveTaxDisclosureText = new ArrayList<TextTranslationType>();
                }
                return this.receiveTaxDisclosureText;
            }

        }

    }

}
