
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MoneyOrderTotalRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MoneyOrderTotalRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="batchID" type="{http://www.moneygram.com/AgentConnect1705}StringMax20Type" minOccurs="0"/>
 *         &lt;element name="batchCount" type="{http://www.moneygram.com/AgentConnect1705}Int11Type" minOccurs="0"/>
 *         &lt;element name="batchTotal" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType" minOccurs="0"/>
 *         &lt;element name="moAccountNumber" type="{http://www.moneygram.com/AgentConnect1705}String14Type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MoneyOrderTotalRequest", propOrder = {
    "batchID",
    "batchCount",
    "batchTotal",
    "moAccountNumber"
})
public class MoneyOrderTotalRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected String batchID;
    protected BigInteger batchCount;
    protected BigDecimal batchTotal;
    protected String moAccountNumber;

    /**
     * Gets the value of the batchID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBatchID() {
        return batchID;
    }

    /**
     * Sets the value of the batchID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBatchID(String value) {
        this.batchID = value;
    }

    /**
     * Gets the value of the batchCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBatchCount() {
        return batchCount;
    }

    /**
     * Sets the value of the batchCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBatchCount(BigInteger value) {
        this.batchCount = value;
    }

    /**
     * Gets the value of the batchTotal property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBatchTotal() {
        return batchTotal;
    }

    /**
     * Sets the value of the batchTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBatchTotal(BigDecimal value) {
        this.batchTotal = value;
    }

    /**
     * Gets the value of the moAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMoAccountNumber() {
        return moAccountNumber;
    }

    /**
     * Sets the value of the moAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMoAccountNumber(String value) {
        this.moAccountNumber = value;
    }

}
