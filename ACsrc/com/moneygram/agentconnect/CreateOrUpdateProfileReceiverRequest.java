
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				This API updates or creates a consumer profile. If a consumerProfileID is known, this API updates the profile. 
 * 				Otherwise, it attempts to find/update the appropriate profile through merging. 
 * 				If no profile is found, a new one is created, if enough information is provided.
 * 				
 * 				This API is to be used within the receive flow.
 * 				
 * 				This API can be repeated multiple times. If not enough data is provided to create a profile in the initial rounds, the API
 * 				will ask for additional data to be provided. 
 * 				
 * 				This API uses GAF with a transactionType of "createOrUpdateProfileReceiver"
 * 				
 * 				If this API is used on it's own, completeSession should be used. 
 * 				If it's used as a precursor to xxxValidation, then the completeSession is with the transaction.
 * 				
 * 				If a consumerProfileID is returned in the response, it should be included on subsequent createOrUpdateProfileReceiverRequests.
 * 			
 * 
 * <p>Java class for CreateOrUpdateProfileReceiverRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreateOrUpdateProfileReceiverRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="GAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType" minOccurs="0"/>
 *         &lt;element name="consumerProfileID" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileIDType" minOccurs="0"/>
 *         &lt;element name="consumerProfileIDType" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileIDTypeType" minOccurs="0"/>
 *         &lt;element name="consumerProfileIDTypeToReturn" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileIDTypeType" minOccurs="0"/>
 *         &lt;element name="fieldValues" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="fieldValue" type="{http://www.moneygram.com/AgentConnect1705}KeyValuePairType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="verifiedFields" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="infoKey" type="{http://www.moneygram.com/AgentConnect1705}InfoKeyType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateOrUpdateProfileReceiverRequest", propOrder = {
    "gafVersionNumber",
    "consumerProfileID",
    "consumerProfileIDType",
    "consumerProfileIDTypeToReturn",
    "fieldValues",
    "verifiedFields"
})
public class CreateOrUpdateProfileReceiverRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "GAFVersionNumber")
    protected String gafVersionNumber;
    protected String consumerProfileID;
    protected String consumerProfileIDType;
    protected String consumerProfileIDTypeToReturn;
    protected CreateOrUpdateProfileReceiverRequest.FieldValues fieldValues;
    protected CreateOrUpdateProfileReceiverRequest.VerifiedFields verifiedFields;

    /**
     * Gets the value of the gafVersionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGAFVersionNumber() {
        return gafVersionNumber;
    }

    /**
     * Sets the value of the gafVersionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGAFVersionNumber(String value) {
        this.gafVersionNumber = value;
    }

    /**
     * Gets the value of the consumerProfileID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsumerProfileID() {
        return consumerProfileID;
    }

    /**
     * Sets the value of the consumerProfileID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsumerProfileID(String value) {
        this.consumerProfileID = value;
    }

    /**
     * Gets the value of the consumerProfileIDType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsumerProfileIDType() {
        return consumerProfileIDType;
    }

    /**
     * Sets the value of the consumerProfileIDType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsumerProfileIDType(String value) {
        this.consumerProfileIDType = value;
    }

    /**
     * Gets the value of the consumerProfileIDTypeToReturn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsumerProfileIDTypeToReturn() {
        return consumerProfileIDTypeToReturn;
    }

    /**
     * Sets the value of the consumerProfileIDTypeToReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsumerProfileIDTypeToReturn(String value) {
        this.consumerProfileIDTypeToReturn = value;
    }

    /**
     * Gets the value of the fieldValues property.
     * 
     * @return
     *     possible object is
     *     {@link CreateOrUpdateProfileReceiverRequest.FieldValues }
     *     
     */
    public CreateOrUpdateProfileReceiverRequest.FieldValues getFieldValues() {
        return fieldValues;
    }

    /**
     * Sets the value of the fieldValues property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateOrUpdateProfileReceiverRequest.FieldValues }
     *     
     */
    public void setFieldValues(CreateOrUpdateProfileReceiverRequest.FieldValues value) {
        this.fieldValues = value;
    }

    /**
     * Gets the value of the verifiedFields property.
     * 
     * @return
     *     possible object is
     *     {@link CreateOrUpdateProfileReceiverRequest.VerifiedFields }
     *     
     */
    public CreateOrUpdateProfileReceiverRequest.VerifiedFields getVerifiedFields() {
        return verifiedFields;
    }

    /**
     * Sets the value of the verifiedFields property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateOrUpdateProfileReceiverRequest.VerifiedFields }
     *     
     */
    public void setVerifiedFields(CreateOrUpdateProfileReceiverRequest.VerifiedFields value) {
        this.verifiedFields = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="fieldValue" type="{http://www.moneygram.com/AgentConnect1705}KeyValuePairType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fieldValue"
    })
    public static class FieldValues
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<KeyValuePairType> fieldValue;

        /**
         * Gets the value of the fieldValue property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the fieldValue property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFieldValue().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link KeyValuePairType }
         * 
         * 
         */
        public List<KeyValuePairType> getFieldValue() {
            if (fieldValue == null) {
                fieldValue = new ArrayList<KeyValuePairType>();
            }
            return this.fieldValue;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="infoKey" type="{http://www.moneygram.com/AgentConnect1705}InfoKeyType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "infoKey"
    })
    public static class VerifiedFields
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<String> infoKey;

        /**
         * Gets the value of the infoKey property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the infoKey property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getInfoKey().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getInfoKey() {
            if (infoKey == null) {
                infoKey = new ArrayList<String>();
            }
            return this.infoKey;
        }

    }

}
