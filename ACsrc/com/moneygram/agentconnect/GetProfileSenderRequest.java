
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				This API is to be used in to return consumer profile information. 
 * 				It can also be used to return previous receivers/billers to facilitate repeat transactions. 
 * 				
 * 				This API is to be used within the send or billpayment flows.
 * 								
 * 				This API does not return a complete history of transactions.
 * 				If complete history is needed, use getConsumerProfileTransactionHistory.
 * 				
 * 				For example, if a consumer has sent to the same receiver (with the same delivery option)
 * 				on multiple occasions, only one instance will be returned in this API.
 * 			
 * 
 * <p>Java class for GetProfileSenderRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetProfileSenderRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="GAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType" minOccurs="0"/>
 *         &lt;element name="consumerProfileID" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileIDType"/>
 *         &lt;element name="consumerProfileIDType" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileIDTypeType"/>
 *         &lt;element name="maxReceiversToReturn" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="maxBillersToReturn" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetProfileSenderRequest", propOrder = {
    "gafVersionNumber",
    "consumerProfileID",
    "consumerProfileIDType",
    "maxReceiversToReturn",
    "maxBillersToReturn"
})
public class GetProfileSenderRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "GAFVersionNumber")
    protected String gafVersionNumber;
    @XmlElement(required = true)
    protected String consumerProfileID;
    @XmlElement(required = true)
    protected String consumerProfileIDType;
    protected int maxReceiversToReturn;
    protected int maxBillersToReturn;

    /**
     * Gets the value of the gafVersionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGAFVersionNumber() {
        return gafVersionNumber;
    }

    /**
     * Sets the value of the gafVersionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGAFVersionNumber(String value) {
        this.gafVersionNumber = value;
    }

    /**
     * Gets the value of the consumerProfileID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsumerProfileID() {
        return consumerProfileID;
    }

    /**
     * Sets the value of the consumerProfileID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsumerProfileID(String value) {
        this.consumerProfileID = value;
    }

    /**
     * Gets the value of the consumerProfileIDType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsumerProfileIDType() {
        return consumerProfileIDType;
    }

    /**
     * Sets the value of the consumerProfileIDType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsumerProfileIDType(String value) {
        this.consumerProfileIDType = value;
    }

    /**
     * Gets the value of the maxReceiversToReturn property.
     * 
     */
    public int getMaxReceiversToReturn() {
        return maxReceiversToReturn;
    }

    /**
     * Sets the value of the maxReceiversToReturn property.
     * 
     */
    public void setMaxReceiversToReturn(int value) {
        this.maxReceiversToReturn = value;
    }

    /**
     * Gets the value of the maxBillersToReturn property.
     * 
     */
    public int getMaxBillersToReturn() {
        return maxBillersToReturn;
    }

    /**
     * Sets the value of the maxBillersToReturn property.
     * 
     */
    public void setMaxBillersToReturn(int value) {
        this.maxBillersToReturn = value;
    }

}
