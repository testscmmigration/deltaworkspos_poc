
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 *  A Complex type to define ConsumerProfileID Collection with date
 * 
 * 
 * <p>Java class for ConsumerProfileIDInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConsumerProfileIDInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="consumerProfileID" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileIDType" minOccurs="0"/>
 *         &lt;element name="consumerProfileIDType" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileIDTypeType" minOccurs="0"/>
 *         &lt;element name="consumerProfileIDCreateDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsumerProfileIDInfo", propOrder = {
    "consumerProfileID",
    "consumerProfileIDType",
    "consumerProfileIDCreateDate"
})
public class ConsumerProfileIDInfo
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected String consumerProfileID;
    protected String consumerProfileIDType;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar consumerProfileIDCreateDate;

    /**
     * Gets the value of the consumerProfileID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsumerProfileID() {
        return consumerProfileID;
    }

    /**
     * Sets the value of the consumerProfileID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsumerProfileID(String value) {
        this.consumerProfileID = value;
    }

    /**
     * Gets the value of the consumerProfileIDType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsumerProfileIDType() {
        return consumerProfileIDType;
    }

    /**
     * Sets the value of the consumerProfileIDType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsumerProfileIDType(String value) {
        this.consumerProfileIDType = value;
    }

    /**
     * Gets the value of the consumerProfileIDCreateDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getConsumerProfileIDCreateDate() {
        return consumerProfileIDCreateDate;
    }

    /**
     * Sets the value of the consumerProfileIDCreateDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setConsumerProfileIDCreateDate(XMLGregorianCalendar value) {
        this.consumerProfileIDCreateDate = value;
    }

}
