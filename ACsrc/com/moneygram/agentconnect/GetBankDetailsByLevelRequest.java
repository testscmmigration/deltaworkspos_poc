
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetBankDetailsByLevelRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetBankDetailsByLevelRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="countryCode" type="{http://www.moneygram.com/AgentConnect1705}CountryCodeType"/>
 *         &lt;element name="hierarchyLevelNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="previousLevelElementNumber" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetBankDetailsByLevelRequest", propOrder = {
    "countryCode",
    "hierarchyLevelNumber",
    "previousLevelElementNumber"
})
public class GetBankDetailsByLevelRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected String countryCode;
    protected int hierarchyLevelNumber;
    protected Long previousLevelElementNumber;

    /**
     * Gets the value of the countryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Sets the value of the countryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

    /**
     * Gets the value of the hierarchyLevelNumber property.
     * 
     */
    public int getHierarchyLevelNumber() {
        return hierarchyLevelNumber;
    }

    /**
     * Sets the value of the hierarchyLevelNumber property.
     * 
     */
    public void setHierarchyLevelNumber(int value) {
        this.hierarchyLevelNumber = value;
    }

    /**
     * Gets the value of the previousLevelElementNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPreviousLevelElementNumber() {
        return previousLevelElementNumber;
    }

    /**
     * Sets the value of the previousLevelElementNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPreviousLevelElementNumber(Long value) {
        this.previousLevelElementNumber = value;
    }

}
