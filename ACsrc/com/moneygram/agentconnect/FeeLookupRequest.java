
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FeeLookupRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FeeLookupRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="mgiSessionType" type="{http://www.moneygram.com/AgentConnect1705}SessionType"/>
 *         &lt;element name="productVariant" type="{http://www.moneygram.com/AgentConnect1705}ProductVariantType" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element name="amountIncludingFee" type="{http://www.moneygram.com/AgentConnect1705}AmountNonZeroType" minOccurs="0"/>
 *           &lt;element name="amountExcludingFee" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType" minOccurs="0"/>
 *           &lt;element name="receiveAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountNonZeroType" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="destinationCountry" type="{http://www.moneygram.com/AgentConnect1705}CountryCodeType"/>
 *         &lt;element name="destinationCountrySubdivisionCode" type="{http://www.moneygram.com/AgentConnect1705}CountrySubdivisionCodeType" minOccurs="0"/>
 *         &lt;element name="sendCountry" type="{http://www.moneygram.com/AgentConnect1705}CountryCodeType" minOccurs="0"/>
 *         &lt;element name="serviceOption" type="{http://www.moneygram.com/AgentConnect1705}ServiceOptionType" minOccurs="0"/>
 *         &lt;element name="mgiRewardsNumber" type="{http://www.moneygram.com/AgentConnect1705}StringMax20Type" minOccurs="0"/>
 *         &lt;element name="receiveCode" type="{http://www.moneygram.com/AgentConnect1705}ReceiveCodeType" minOccurs="0"/>
 *         &lt;element name="receiveAgentID" type="{http://www.moneygram.com/AgentConnect1705}AgentIDType" minOccurs="0"/>
 *         &lt;element name="receiveCurrency" type="{http://www.moneygram.com/AgentConnect1705}CurrencyCodeType" minOccurs="0"/>
 *         &lt;element name="sendCurrency" type="{http://www.moneygram.com/AgentConnect1705}CurrencyCodeType" minOccurs="0"/>
 *         &lt;element name="mgCustomerReceiveNumber" type="{http://www.moneygram.com/AgentConnect1705}MgCustomerReceiveNumberType" minOccurs="0"/>
 *         &lt;element name="defaultInformationalFee" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType" minOccurs="0"/>
 *         &lt;element name="serviceOfferingID" type="{http://www.moneygram.com/AgentConnect1705}ServiceOfferingIDType" minOccurs="0"/>
 *         &lt;element name="defaultMaxFee" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="allOptions" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="promoCodes" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="promoCode" type="{http://www.moneygram.com/AgentConnect1705}StringMax20Type" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FeeLookupRequest", propOrder = {
    "mgiSessionType",
    "productVariant",
    "amountIncludingFee",
    "amountExcludingFee",
    "receiveAmount",
    "destinationCountry",
    "destinationCountrySubdivisionCode",
    "sendCountry",
    "serviceOption",
    "mgiRewardsNumber",
    "receiveCode",
    "receiveAgentID",
    "receiveCurrency",
    "sendCurrency",
    "mgCustomerReceiveNumber",
    "defaultInformationalFee",
    "serviceOfferingID",
    "defaultMaxFee",
    "allOptions",
    "promoCodes"
})
public class FeeLookupRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected SessionType mgiSessionType;
    protected ProductVariantType productVariant;
    protected BigDecimal amountIncludingFee;
    protected BigDecimal amountExcludingFee;
    protected BigDecimal receiveAmount;
    @XmlElement(required = true)
    protected String destinationCountry;
    protected String destinationCountrySubdivisionCode;
    protected String sendCountry;
    protected String serviceOption;
    protected String mgiRewardsNumber;
    protected String receiveCode;
    protected String receiveAgentID;
    protected String receiveCurrency;
    protected String sendCurrency;
    protected String mgCustomerReceiveNumber;
    protected BigDecimal defaultInformationalFee;
    protected String serviceOfferingID;
    protected boolean defaultMaxFee;
    protected boolean allOptions;
    protected FeeLookupRequest.PromoCodes promoCodes;

    /**
     * Gets the value of the mgiSessionType property.
     * 
     * @return
     *     possible object is
     *     {@link SessionType }
     *     
     */
    public SessionType getMgiSessionType() {
        return mgiSessionType;
    }

    /**
     * Sets the value of the mgiSessionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SessionType }
     *     
     */
    public void setMgiSessionType(SessionType value) {
        this.mgiSessionType = value;
    }

    /**
     * Gets the value of the productVariant property.
     * 
     * @return
     *     possible object is
     *     {@link ProductVariantType }
     *     
     */
    public ProductVariantType getProductVariant() {
        return productVariant;
    }

    /**
     * Sets the value of the productVariant property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductVariantType }
     *     
     */
    public void setProductVariant(ProductVariantType value) {
        this.productVariant = value;
    }

    /**
     * Gets the value of the amountIncludingFee property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmountIncludingFee() {
        return amountIncludingFee;
    }

    /**
     * Sets the value of the amountIncludingFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmountIncludingFee(BigDecimal value) {
        this.amountIncludingFee = value;
    }

    /**
     * Gets the value of the amountExcludingFee property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmountExcludingFee() {
        return amountExcludingFee;
    }

    /**
     * Sets the value of the amountExcludingFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmountExcludingFee(BigDecimal value) {
        this.amountExcludingFee = value;
    }

    /**
     * Gets the value of the receiveAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getReceiveAmount() {
        return receiveAmount;
    }

    /**
     * Sets the value of the receiveAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setReceiveAmount(BigDecimal value) {
        this.receiveAmount = value;
    }

    /**
     * Gets the value of the destinationCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationCountry() {
        return destinationCountry;
    }

    /**
     * Sets the value of the destinationCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationCountry(String value) {
        this.destinationCountry = value;
    }

    /**
     * Gets the value of the destinationCountrySubdivisionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationCountrySubdivisionCode() {
        return destinationCountrySubdivisionCode;
    }

    /**
     * Sets the value of the destinationCountrySubdivisionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationCountrySubdivisionCode(String value) {
        this.destinationCountrySubdivisionCode = value;
    }

    /**
     * Gets the value of the sendCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendCountry() {
        return sendCountry;
    }

    /**
     * Sets the value of the sendCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendCountry(String value) {
        this.sendCountry = value;
    }

    /**
     * Gets the value of the serviceOption property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceOption() {
        return serviceOption;
    }

    /**
     * Sets the value of the serviceOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceOption(String value) {
        this.serviceOption = value;
    }

    /**
     * Gets the value of the mgiRewardsNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMgiRewardsNumber() {
        return mgiRewardsNumber;
    }

    /**
     * Sets the value of the mgiRewardsNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMgiRewardsNumber(String value) {
        this.mgiRewardsNumber = value;
    }

    /**
     * Gets the value of the receiveCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiveCode() {
        return receiveCode;
    }

    /**
     * Sets the value of the receiveCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiveCode(String value) {
        this.receiveCode = value;
    }

    /**
     * Gets the value of the receiveAgentID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiveAgentID() {
        return receiveAgentID;
    }

    /**
     * Sets the value of the receiveAgentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiveAgentID(String value) {
        this.receiveAgentID = value;
    }

    /**
     * Gets the value of the receiveCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiveCurrency() {
        return receiveCurrency;
    }

    /**
     * Sets the value of the receiveCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiveCurrency(String value) {
        this.receiveCurrency = value;
    }

    /**
     * Gets the value of the sendCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendCurrency() {
        return sendCurrency;
    }

    /**
     * Sets the value of the sendCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendCurrency(String value) {
        this.sendCurrency = value;
    }

    /**
     * Gets the value of the mgCustomerReceiveNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMgCustomerReceiveNumber() {
        return mgCustomerReceiveNumber;
    }

    /**
     * Sets the value of the mgCustomerReceiveNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMgCustomerReceiveNumber(String value) {
        this.mgCustomerReceiveNumber = value;
    }

    /**
     * Gets the value of the defaultInformationalFee property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDefaultInformationalFee() {
        return defaultInformationalFee;
    }

    /**
     * Sets the value of the defaultInformationalFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDefaultInformationalFee(BigDecimal value) {
        this.defaultInformationalFee = value;
    }

    /**
     * Gets the value of the serviceOfferingID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceOfferingID() {
        return serviceOfferingID;
    }

    /**
     * Sets the value of the serviceOfferingID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceOfferingID(String value) {
        this.serviceOfferingID = value;
    }

    /**
     * Gets the value of the defaultMaxFee property.
     * 
     */
    public boolean isDefaultMaxFee() {
        return defaultMaxFee;
    }

    /**
     * Sets the value of the defaultMaxFee property.
     * 
     */
    public void setDefaultMaxFee(boolean value) {
        this.defaultMaxFee = value;
    }

    /**
     * Gets the value of the allOptions property.
     * 
     */
    public boolean isAllOptions() {
        return allOptions;
    }

    /**
     * Sets the value of the allOptions property.
     * 
     */
    public void setAllOptions(boolean value) {
        this.allOptions = value;
    }

    /**
     * Gets the value of the promoCodes property.
     * 
     * @return
     *     possible object is
     *     {@link FeeLookupRequest.PromoCodes }
     *     
     */
    public FeeLookupRequest.PromoCodes getPromoCodes() {
        return promoCodes;
    }

    /**
     * Sets the value of the promoCodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeeLookupRequest.PromoCodes }
     *     
     */
    public void setPromoCodes(FeeLookupRequest.PromoCodes value) {
        this.promoCodes = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="promoCode" type="{http://www.moneygram.com/AgentConnect1705}StringMax20Type" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "promoCode"
    })
    public static class PromoCodes
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<String> promoCode;

        /**
         * Gets the value of the promoCode property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the promoCode property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPromoCode().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getPromoCode() {
            if (promoCode == null) {
                promoCode = new ArrayList<String>();
            }
            return this.promoCode;
        }

    }

}
