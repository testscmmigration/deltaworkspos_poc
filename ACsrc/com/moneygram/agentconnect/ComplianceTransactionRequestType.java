
package com.moneygram.agentconnect;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ComplianceTransactionRequestType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ComplianceTransactionRequestType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="STANDALONE"/>
 *     &lt;enumeration value="PROPOSED"/>
 *     &lt;enumeration value="COMPLETED_WITH_CHANGES"/>
 *     &lt;enumeration value="COMPLETED_WITHOUT_CHANGES"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ComplianceTransactionRequestType")
@XmlEnum
public enum ComplianceTransactionRequestType {

    STANDALONE,
    PROPOSED,
    COMPLETED_WITH_CHANGES,
    COMPLETED_WITHOUT_CHANGES;

    public String value() {
        return name();
    }

    public static ComplianceTransactionRequestType fromValue(String v) {
        return valueOf(v);
    }

}
