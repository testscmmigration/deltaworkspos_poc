
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HierarchyLevelInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HierarchyLevelInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hierarchyLevelElementNumber" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="hierarchyLevelValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="attributes" type="{http://www.moneygram.com/AgentConnect1705}AttributeType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HierarchyLevelInfo", propOrder = {
    "hierarchyLevelElementNumber",
    "hierarchyLevelValue",
    "attributes"
})
public class HierarchyLevelInfo
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected long hierarchyLevelElementNumber;
    @XmlElement(required = true)
    protected String hierarchyLevelValue;
    protected List<AttributeType> attributes;

    /**
     * Gets the value of the hierarchyLevelElementNumber property.
     * 
     */
    public long getHierarchyLevelElementNumber() {
        return hierarchyLevelElementNumber;
    }

    /**
     * Sets the value of the hierarchyLevelElementNumber property.
     * 
     */
    public void setHierarchyLevelElementNumber(long value) {
        this.hierarchyLevelElementNumber = value;
    }

    /**
     * Gets the value of the hierarchyLevelValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHierarchyLevelValue() {
        return hierarchyLevelValue;
    }

    /**
     * Sets the value of the hierarchyLevelValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHierarchyLevelValue(String value) {
        this.hierarchyLevelValue = value;
    }

    /**
     * Gets the value of the attributes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attributes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttributes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttributeType }
     * 
     * 
     */
    public List<AttributeType> getAttributes() {
        if (attributes == null) {
            attributes = new ArrayList<AttributeType>();
        }
        return this.attributes;
    }

}
