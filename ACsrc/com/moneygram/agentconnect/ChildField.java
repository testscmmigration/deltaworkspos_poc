
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChildField complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChildField">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fieldValue" type="{http://www.moneygram.com/AgentConnect1705}FieldValueType"/>
 *         &lt;element name="infos" type="{http://www.moneygram.com/AgentConnect1705}InfosType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChildField", propOrder = {
    "fieldValue",
    "infos"
})
public class ChildField
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected String fieldValue;
    protected InfosType infos;

    /**
     * Gets the value of the fieldValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFieldValue() {
        return fieldValue;
    }

    /**
     * Sets the value of the fieldValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFieldValue(String value) {
        this.fieldValue = value;
    }

    /**
     * Gets the value of the infos property.
     * 
     * @return
     *     possible object is
     *     {@link InfosType }
     *     
     */
    public InfosType getInfos() {
        return infos;
    }

    /**
     * Sets the value of the infos property.
     * 
     * @param value
     *     allowed object is
     *     {@link InfosType }
     *     
     */
    public void setInfos(InfosType value) {
        this.infos = value;
    }

}
