
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SenderLookupInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SenderLookupInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="currentValues" type="{http://www.moneygram.com/AgentConnect1705}CurrentValuesType" minOccurs="0"/>
 *         &lt;element name="receiverInfos" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="receiverInfo" type="{http://www.moneygram.com/AgentConnect1705}ReceiverLookupInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="billerInfos" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="billerInfo" type="{http://www.moneygram.com/AgentConnect1705}BillerLookupInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SenderLookupInfo", propOrder = {
    "currentValues",
    "receiverInfos",
    "billerInfos"
})
public class SenderLookupInfo
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected CurrentValuesType currentValues;
    protected SenderLookupInfo.ReceiverInfos receiverInfos;
    protected SenderLookupInfo.BillerInfos billerInfos;

    /**
     * Gets the value of the currentValues property.
     * 
     * @return
     *     possible object is
     *     {@link CurrentValuesType }
     *     
     */
    public CurrentValuesType getCurrentValues() {
        return currentValues;
    }

    /**
     * Sets the value of the currentValues property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrentValuesType }
     *     
     */
    public void setCurrentValues(CurrentValuesType value) {
        this.currentValues = value;
    }

    /**
     * Gets the value of the receiverInfos property.
     * 
     * @return
     *     possible object is
     *     {@link SenderLookupInfo.ReceiverInfos }
     *     
     */
    public SenderLookupInfo.ReceiverInfos getReceiverInfos() {
        return receiverInfos;
    }

    /**
     * Sets the value of the receiverInfos property.
     * 
     * @param value
     *     allowed object is
     *     {@link SenderLookupInfo.ReceiverInfos }
     *     
     */
    public void setReceiverInfos(SenderLookupInfo.ReceiverInfos value) {
        this.receiverInfos = value;
    }

    /**
     * Gets the value of the billerInfos property.
     * 
     * @return
     *     possible object is
     *     {@link SenderLookupInfo.BillerInfos }
     *     
     */
    public SenderLookupInfo.BillerInfos getBillerInfos() {
        return billerInfos;
    }

    /**
     * Sets the value of the billerInfos property.
     * 
     * @param value
     *     allowed object is
     *     {@link SenderLookupInfo.BillerInfos }
     *     
     */
    public void setBillerInfos(SenderLookupInfo.BillerInfos value) {
        this.billerInfos = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="billerInfo" type="{http://www.moneygram.com/AgentConnect1705}BillerLookupInfo" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "billerInfo"
    })
    public static class BillerInfos
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<BillerLookupInfo> billerInfo;

        /**
         * Gets the value of the billerInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the billerInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getBillerInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BillerLookupInfo }
         * 
         * 
         */
        public List<BillerLookupInfo> getBillerInfo() {
            if (billerInfo == null) {
                billerInfo = new ArrayList<BillerLookupInfo>();
            }
            return this.billerInfo;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="receiverInfo" type="{http://www.moneygram.com/AgentConnect1705}ReceiverLookupInfo" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "receiverInfo"
    })
    public static class ReceiverInfos
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<ReceiverLookupInfo> receiverInfo;

        /**
         * Gets the value of the receiverInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the receiverInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getReceiverInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ReceiverLookupInfo }
         * 
         * 
         */
        public List<ReceiverLookupInfo> getReceiverInfo() {
            if (receiverInfo == null) {
                receiverInfo = new ArrayList<ReceiverLookupInfo>();
            }
            return this.receiverInfo;
        }

    }

}
