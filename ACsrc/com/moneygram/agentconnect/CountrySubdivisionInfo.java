
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CountrySubdivisionInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CountrySubdivisionInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="countryCode" type="{http://www.moneygram.com/AgentConnect1705}CountryCodeType"/>
 *         &lt;element name="subdivisions" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="subdivision" type="{http://www.moneygram.com/AgentConnect1705}SubdivisionInfo" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CountrySubdivisionInfo", propOrder = {
    "countryCode",
    "subdivisions"
})
public class CountrySubdivisionInfo
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected String countryCode;
    protected CountrySubdivisionInfo.Subdivisions subdivisions;

    /**
     * Gets the value of the countryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Sets the value of the countryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

    /**
     * Gets the value of the subdivisions property.
     * 
     * @return
     *     possible object is
     *     {@link CountrySubdivisionInfo.Subdivisions }
     *     
     */
    public CountrySubdivisionInfo.Subdivisions getSubdivisions() {
        return subdivisions;
    }

    /**
     * Sets the value of the subdivisions property.
     * 
     * @param value
     *     allowed object is
     *     {@link CountrySubdivisionInfo.Subdivisions }
     *     
     */
    public void setSubdivisions(CountrySubdivisionInfo.Subdivisions value) {
        this.subdivisions = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="subdivision" type="{http://www.moneygram.com/AgentConnect1705}SubdivisionInfo" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subdivision"
    })
    public static class Subdivisions
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(required = true)
        protected List<SubdivisionInfo> subdivision;

        /**
         * Gets the value of the subdivision property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the subdivision property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSubdivision().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SubdivisionInfo }
         * 
         * 
         */
        public List<SubdivisionInfo> getSubdivision() {
            if (subdivision == null) {
                subdivision = new ArrayList<SubdivisionInfo>();
            }
            return this.subdivision;
        }

    }

}
