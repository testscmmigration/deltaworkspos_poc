
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StandardizedAddressType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StandardizedAddressType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}BaseRawAddressType">
 *       &lt;sequence>
 *         &lt;element name="addressCleansingScore" type="{http://www.moneygram.com/AgentConnect1705}AddressCleansingScoreType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StandardizedAddressType", propOrder = {
    "addressCleansingScore"
})
public class StandardizedAddressType
    extends BaseRawAddressType
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected String addressCleansingScore;

    /**
     * Gets the value of the addressCleansingScore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressCleansingScore() {
        return addressCleansingScore;
    }

    /**
     * Sets the value of the addressCleansingScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressCleansingScore(String value) {
        this.addressCleansingScore = value;
    }

}
