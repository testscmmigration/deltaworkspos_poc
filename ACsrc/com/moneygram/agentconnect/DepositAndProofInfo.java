
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DepositAndProofInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DepositAndProofInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="mgiEmployeeAutoAccept" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="depositReferenceNumber" type="{http://www.moneygram.com/AgentConnect1705}DepositReferenceNumberType" minOccurs="0"/>
 *         &lt;element name="depositDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="announcementDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="announcementAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType" minOccurs="0"/>
 *         &lt;element name="announcementCurrencyCode" type="{http://www.moneygram.com/AgentConnect1705}CurrencyCodeType" minOccurs="0"/>
 *         &lt;element name="paymentTypeCode" type="{http://www.moneygram.com/AgentConnect1705}StringMax2Type" minOccurs="0"/>
 *         &lt;element name="bankShortCode" type="{http://www.moneygram.com/AgentConnect1705}BankShortCodeType" minOccurs="0"/>
 *         &lt;element name="bankName" type="{http://www.moneygram.com/AgentConnect1705}StringMax100Type" minOccurs="0"/>
 *         &lt;element name="announcementRemarks" type="{http://www.moneygram.com/AgentConnect1705}AnnouncementRemarksType" minOccurs="0"/>
 *         &lt;element name="announcementStatus" type="{http://www.moneygram.com/AgentConnect1705}StringMax40Type" minOccurs="0"/>
 *         &lt;element name="customerAccountSiteID" type="{http://www.moneygram.com/AgentConnect1705}StringMax15Type" minOccurs="0"/>
 *         &lt;element name="agentEmployeeID" type="{http://www.moneygram.com/AgentConnect1705}StringMax8Type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DepositAndProofInfo", propOrder = {
    "mgiEmployeeAutoAccept",
    "depositReferenceNumber",
    "depositDate",
    "announcementDate",
    "announcementAmount",
    "announcementCurrencyCode",
    "paymentTypeCode",
    "bankShortCode",
    "bankName",
    "announcementRemarks",
    "announcementStatus",
    "customerAccountSiteID",
    "agentEmployeeID"
})
public class DepositAndProofInfo
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected Boolean mgiEmployeeAutoAccept;
    protected String depositReferenceNumber;
    protected String depositDate;
    protected String announcementDate;
    protected BigDecimal announcementAmount;
    protected String announcementCurrencyCode;
    protected String paymentTypeCode;
    protected String bankShortCode;
    protected String bankName;
    protected String announcementRemarks;
    protected String announcementStatus;
    protected String customerAccountSiteID;
    protected String agentEmployeeID;

    /**
     * Gets the value of the mgiEmployeeAutoAccept property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMgiEmployeeAutoAccept() {
        return mgiEmployeeAutoAccept;
    }

    /**
     * Sets the value of the mgiEmployeeAutoAccept property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMgiEmployeeAutoAccept(Boolean value) {
        this.mgiEmployeeAutoAccept = value;
    }

    /**
     * Gets the value of the depositReferenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepositReferenceNumber() {
        return depositReferenceNumber;
    }

    /**
     * Sets the value of the depositReferenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepositReferenceNumber(String value) {
        this.depositReferenceNumber = value;
    }

    /**
     * Gets the value of the depositDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepositDate() {
        return depositDate;
    }

    /**
     * Sets the value of the depositDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepositDate(String value) {
        this.depositDate = value;
    }

    /**
     * Gets the value of the announcementDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnouncementDate() {
        return announcementDate;
    }

    /**
     * Sets the value of the announcementDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnouncementDate(String value) {
        this.announcementDate = value;
    }

    /**
     * Gets the value of the announcementAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAnnouncementAmount() {
        return announcementAmount;
    }

    /**
     * Sets the value of the announcementAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAnnouncementAmount(BigDecimal value) {
        this.announcementAmount = value;
    }

    /**
     * Gets the value of the announcementCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnouncementCurrencyCode() {
        return announcementCurrencyCode;
    }

    /**
     * Sets the value of the announcementCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnouncementCurrencyCode(String value) {
        this.announcementCurrencyCode = value;
    }

    /**
     * Gets the value of the paymentTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentTypeCode() {
        return paymentTypeCode;
    }

    /**
     * Sets the value of the paymentTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentTypeCode(String value) {
        this.paymentTypeCode = value;
    }

    /**
     * Gets the value of the bankShortCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankShortCode() {
        return bankShortCode;
    }

    /**
     * Sets the value of the bankShortCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankShortCode(String value) {
        this.bankShortCode = value;
    }

    /**
     * Gets the value of the bankName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * Sets the value of the bankName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankName(String value) {
        this.bankName = value;
    }

    /**
     * Gets the value of the announcementRemarks property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnouncementRemarks() {
        return announcementRemarks;
    }

    /**
     * Sets the value of the announcementRemarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnouncementRemarks(String value) {
        this.announcementRemarks = value;
    }

    /**
     * Gets the value of the announcementStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnouncementStatus() {
        return announcementStatus;
    }

    /**
     * Sets the value of the announcementStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnouncementStatus(String value) {
        this.announcementStatus = value;
    }

    /**
     * Gets the value of the customerAccountSiteID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerAccountSiteID() {
        return customerAccountSiteID;
    }

    /**
     * Sets the value of the customerAccountSiteID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerAccountSiteID(String value) {
        this.customerAccountSiteID = value;
    }

    /**
     * Gets the value of the agentEmployeeID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentEmployeeID() {
        return agentEmployeeID;
    }

    /**
     * Sets the value of the agentEmployeeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentEmployeeID(String value) {
        this.agentEmployeeID = value;
    }

}
