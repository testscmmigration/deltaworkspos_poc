
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SaveProfileRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SaveProfileRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="profileItems" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="profileItem" type="{http://www.moneygram.com/AgentConnect1705}ProfileItemType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="productProfileItems" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="productProfileItem" type="{http://www.moneygram.com/AgentConnect1705}ProductProfileItemType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="employeeProfileItems" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="employeeProfileItem" type="{http://www.moneygram.com/AgentConnect1705}EmployeeProfileItemType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SaveProfileRequest", propOrder = {
    "profileItems",
    "productProfileItems",
    "employeeProfileItems"
})
public class SaveProfileRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected SaveProfileRequest.ProfileItems profileItems;
    protected SaveProfileRequest.ProductProfileItems productProfileItems;
    protected SaveProfileRequest.EmployeeProfileItems employeeProfileItems;

    /**
     * Gets the value of the profileItems property.
     * 
     * @return
     *     possible object is
     *     {@link SaveProfileRequest.ProfileItems }
     *     
     */
    public SaveProfileRequest.ProfileItems getProfileItems() {
        return profileItems;
    }

    /**
     * Sets the value of the profileItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link SaveProfileRequest.ProfileItems }
     *     
     */
    public void setProfileItems(SaveProfileRequest.ProfileItems value) {
        this.profileItems = value;
    }

    /**
     * Gets the value of the productProfileItems property.
     * 
     * @return
     *     possible object is
     *     {@link SaveProfileRequest.ProductProfileItems }
     *     
     */
    public SaveProfileRequest.ProductProfileItems getProductProfileItems() {
        return productProfileItems;
    }

    /**
     * Sets the value of the productProfileItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link SaveProfileRequest.ProductProfileItems }
     *     
     */
    public void setProductProfileItems(SaveProfileRequest.ProductProfileItems value) {
        this.productProfileItems = value;
    }

    /**
     * Gets the value of the employeeProfileItems property.
     * 
     * @return
     *     possible object is
     *     {@link SaveProfileRequest.EmployeeProfileItems }
     *     
     */
    public SaveProfileRequest.EmployeeProfileItems getEmployeeProfileItems() {
        return employeeProfileItems;
    }

    /**
     * Sets the value of the employeeProfileItems property.
     * 
     * @param value
     *     allowed object is
     *     {@link SaveProfileRequest.EmployeeProfileItems }
     *     
     */
    public void setEmployeeProfileItems(SaveProfileRequest.EmployeeProfileItems value) {
        this.employeeProfileItems = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="employeeProfileItem" type="{http://www.moneygram.com/AgentConnect1705}EmployeeProfileItemType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "employeeProfileItem"
    })
    public static class EmployeeProfileItems
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<EmployeeProfileItemType> employeeProfileItem;

        /**
         * Gets the value of the employeeProfileItem property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the employeeProfileItem property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEmployeeProfileItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link EmployeeProfileItemType }
         * 
         * 
         */
        public List<EmployeeProfileItemType> getEmployeeProfileItem() {
            if (employeeProfileItem == null) {
                employeeProfileItem = new ArrayList<EmployeeProfileItemType>();
            }
            return this.employeeProfileItem;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="productProfileItem" type="{http://www.moneygram.com/AgentConnect1705}ProductProfileItemType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "productProfileItem"
    })
    public static class ProductProfileItems
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<ProductProfileItemType> productProfileItem;

        /**
         * Gets the value of the productProfileItem property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the productProfileItem property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getProductProfileItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ProductProfileItemType }
         * 
         * 
         */
        public List<ProductProfileItemType> getProductProfileItem() {
            if (productProfileItem == null) {
                productProfileItem = new ArrayList<ProductProfileItemType>();
            }
            return this.productProfileItem;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="profileItem" type="{http://www.moneygram.com/AgentConnect1705}ProfileItemType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "profileItem"
    })
    public static class ProfileItems
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<ProfileItemType> profileItem;

        /**
         * Gets the value of the profileItem property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the profileItem property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getProfileItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ProfileItemType }
         * 
         * 
         */
        public List<ProfileItemType> getProfileItem() {
            if (profileItem == null) {
                profileItem = new ArrayList<ProfileItemType>();
            }
            return this.profileItem;
        }

    }

}
