
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Request complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Request">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="unitProfileID" type="{http://www.moneygram.com/AgentConnect1705}UnitProfileIDType" minOccurs="0"/>
 *         &lt;element name="agentID" type="{http://www.moneygram.com/AgentConnect1705}AgentIDType" minOccurs="0"/>
 *         &lt;element name="agentSequence" type="{http://www.moneygram.com/AgentConnect1705}AgentSequenceType" minOccurs="0"/>
 *         &lt;element name="language" type="{http://www.moneygram.com/AgentConnect1705}LocaleType" minOccurs="0"/>
 *         &lt;element name="mgiSessionID" type="{http://www.moneygram.com/AgentConnect1705}MGISessionIDType" minOccurs="0"/>
 *         &lt;element name="timeStamp" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="clientSoftwareVersion" type="{http://www.moneygram.com/AgentConnect1705}ClientSWVersionType"/>
 *         &lt;element name="poeType" type="{http://www.moneygram.com/AgentConnect1705}EnumType"/>
 *         &lt;element name="channelType" type="{http://www.moneygram.com/AgentConnect1705}EnumType"/>
 *         &lt;element name="operatorName" type="{http://www.moneygram.com/AgentConnect1705}OperatorNameType" minOccurs="0"/>
 *         &lt;element name="targetAudience" type="{http://www.moneygram.com/AgentConnect1705}EnumType"/>
 *         &lt;element name="poeCapabilities" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="poeCapability" type="{http://www.moneygram.com/AgentConnect1705}KeyValuePairType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Request", propOrder = {
    "unitProfileID",
    "agentID",
    "agentSequence",
    "language",
    "mgiSessionID",
    "timeStamp",
    "clientSoftwareVersion",
    "poeType",
    "channelType",
    "operatorName",
    "targetAudience",
    "poeCapabilities"
})
@XmlSeeAlso({
    SaveProfileRequest.class,
    GetDebugDataRequest.class,
    SaveSubagentsRequest.class,
    SaveConsumerProfileImageRequest.class,
    BillerSearchRequest.class,
    GetServiceOptionsRequest.class,
    GetUCPByConsumerAttributesRequest.class,
    GetCountryInfoRequest.class,
    MoneyGramReceiveDetailReportRequest.class,
    CompleteSessionRequest.class,
    GetCurrencyInfoRequest.class,
    SearchStagedTransactionsRequest.class,
    SendReversalValidationRequest.class,
    SendValidationRequest.class,
    GetDepositInformationRequest.class,
    GetBankDetailsRequest.class,
    CheckInRequest.class,
    VersionManifestRequest.class,
    DwPasswordRequest.class,
    BillPaymentSummaryReportRequest.class,
    ProfileChangeRequest.class,
    ComplianceTransactionRequest.class,
    GetCountrySubdivisionRequest.class,
    CityListRequest.class,
    DirectoryOfAgentsByAreaCodePrefixRequest.class,
    RegisterHardTokenRequest.class,
    DoddFrankStateRegulatorInfoRequest.class,
    ConfirmTokenRequest.class,
    AmendValidationRequest.class,
    GetAllErrorsRequest.class,
    ReceiveReversalValidationRequest.class,
    MoneyGramSendDetailReportWithTaxRequest.class,
    MoneyOrderTotalRequest.class,
    GetDepositBankListRequest.class,
    GetProfileConsumerRequest.class,
    GetEnumerationsRequest.class,
    CreateOrUpdateProfileConsumerRequest.class,
    DirectoryOfAgentsByCityRequest.class,
    CreateOrUpdateProfileSenderRequest.class,
    TransactionLookupRequest.class,
    SubagentsRequest.class,
    SearchConsumerProfilesRequest.class,
    OpenOTPLoginRequest.class,
    MoneyGramSendSummaryReportRequest.class,
    TranslationsRequest.class,
    DwProfileRequest.class,
    MoneyGramSendDetailReportRequest.class,
    DepositAnnouncementRequest.class,
    BillPaymentDetailReportRequest.class,
    BPValidationRequest.class,
    ProfileRequest.class,
    PromotionLookupByCodeRequest.class,
    MoneyGramReceiveSummaryReportRequest.class,
    GetAllFieldsRequest.class,
    SaveDebugDataRequest.class,
    DisclosureTextDetailsRequest.class,
    GetReceiptForReprintRequest.class,
    VariableReceiptInfoRequest.class,
    DirectoryOfAgentsByZipRequest.class,
    GetProfileReceiverRequest.class,
    InjectedInstructionRequest.class,
    GetProfileSenderRequest.class,
    GetBroadcastMessagesRequest.class,
    InitialSetupRequest.class,
    ConsumerHistoryLookupRequest.class,
    ReceiveValidationRequest.class,
    GetBankDetailsByLevelRequest.class,
    GetConsumerProfileTransactionHistoryRequest.class,
    CreateOrUpdateProfileReceiverRequest.class,
    FeeLookupBySendCountryRequest.class,
    IndustryRequest.class,
    ReceiptsFormatDetailsRequest.class,
    FeeLookupRequest.class
})
public class Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected Integer unitProfileID;
    protected String agentID;
    protected String agentSequence;
    protected String language;
    protected String mgiSessionID;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar timeStamp;
    @XmlElement(required = true)
    protected String clientSoftwareVersion;
    @XmlElement(required = true)
    protected String poeType;
    @XmlElement(required = true)
    protected String channelType;
    protected String operatorName;
    @XmlElement(required = true)
    protected String targetAudience;
    protected Request.PoeCapabilities poeCapabilities;

    /**
     * Gets the value of the unitProfileID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getUnitProfileID() {
        return unitProfileID;
    }

    /**
     * Sets the value of the unitProfileID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setUnitProfileID(Integer value) {
        this.unitProfileID = value;
    }

    /**
     * Gets the value of the agentID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentID() {
        return agentID;
    }

    /**
     * Sets the value of the agentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentID(String value) {
        this.agentID = value;
    }

    /**
     * Gets the value of the agentSequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentSequence() {
        return agentSequence;
    }

    /**
     * Sets the value of the agentSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentSequence(String value) {
        this.agentSequence = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Gets the value of the mgiSessionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMgiSessionID() {
        return mgiSessionID;
    }

    /**
     * Sets the value of the mgiSessionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMgiSessionID(String value) {
        this.mgiSessionID = value;
    }

    /**
     * Gets the value of the timeStamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTimeStamp() {
        return timeStamp;
    }

    /**
     * Sets the value of the timeStamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTimeStamp(XMLGregorianCalendar value) {
        this.timeStamp = value;
    }

    /**
     * Gets the value of the clientSoftwareVersion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientSoftwareVersion() {
        return clientSoftwareVersion;
    }

    /**
     * Sets the value of the clientSoftwareVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientSoftwareVersion(String value) {
        this.clientSoftwareVersion = value;
    }

    /**
     * Gets the value of the poeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPoeType() {
        return poeType;
    }

    /**
     * Sets the value of the poeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPoeType(String value) {
        this.poeType = value;
    }

    /**
     * Gets the value of the channelType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelType() {
        return channelType;
    }

    /**
     * Sets the value of the channelType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelType(String value) {
        this.channelType = value;
    }

    /**
     * Gets the value of the operatorName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperatorName() {
        return operatorName;
    }

    /**
     * Sets the value of the operatorName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperatorName(String value) {
        this.operatorName = value;
    }

    /**
     * Gets the value of the targetAudience property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetAudience() {
        return targetAudience;
    }

    /**
     * Sets the value of the targetAudience property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetAudience(String value) {
        this.targetAudience = value;
    }

    /**
     * Gets the value of the poeCapabilities property.
     * 
     * @return
     *     possible object is
     *     {@link Request.PoeCapabilities }
     *     
     */
    public Request.PoeCapabilities getPoeCapabilities() {
        return poeCapabilities;
    }

    /**
     * Sets the value of the poeCapabilities property.
     * 
     * @param value
     *     allowed object is
     *     {@link Request.PoeCapabilities }
     *     
     */
    public void setPoeCapabilities(Request.PoeCapabilities value) {
        this.poeCapabilities = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="poeCapability" type="{http://www.moneygram.com/AgentConnect1705}KeyValuePairType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "poeCapability"
    })
    public static class PoeCapabilities
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<KeyValuePairType> poeCapability;

        /**
         * Gets the value of the poeCapability property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the poeCapability property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPoeCapability().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link KeyValuePairType }
         * 
         * 
         */
        public List<KeyValuePairType> getPoeCapability() {
            if (poeCapability == null) {
                poeCapability = new ArrayList<KeyValuePairType>();
            }
            return this.poeCapability;
        }

    }

}
