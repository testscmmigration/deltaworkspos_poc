
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EnumeratedIdentifierInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EnumeratedIdentifierInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="identifier" type="{http://www.moneygram.com/AgentConnect1705}FieldValueType"/>
 *         &lt;element name="labelShort" type="{http://www.moneygram.com/AgentConnect1705}LabelShortType"/>
 *         &lt;element name="label" type="{http://www.moneygram.com/AgentConnect1705}LabelType"/>
 *         &lt;element name="helpTextShort" type="{http://www.moneygram.com/AgentConnect1705}HelpTextShortType" minOccurs="0"/>
 *         &lt;element name="helpTextLong" type="{http://www.moneygram.com/AgentConnect1705}HelpTextLongType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnumeratedIdentifierInfo", propOrder = {
    "identifier",
    "labelShort",
    "label",
    "helpTextShort",
    "helpTextLong"
})
public class EnumeratedIdentifierInfo
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected String identifier;
    @XmlElement(required = true)
    protected String labelShort;
    @XmlElement(required = true)
    protected String label;
    protected String helpTextShort;
    protected String helpTextLong;

    /**
     * Gets the value of the identifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Sets the value of the identifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentifier(String value) {
        this.identifier = value;
    }

    /**
     * Gets the value of the labelShort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabelShort() {
        return labelShort;
    }

    /**
     * Sets the value of the labelShort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabelShort(String value) {
        this.labelShort = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the helpTextShort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHelpTextShort() {
        return helpTextShort;
    }

    /**
     * Sets the value of the helpTextShort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHelpTextShort(String value) {
        this.helpTextShort = value;
    }

    /**
     * Gets the value of the helpTextLong property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHelpTextLong() {
        return helpTextLong;
    }

    /**
     * Sets the value of the helpTextLong property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHelpTextLong(String value) {
        this.helpTextLong = value;
    }

}
