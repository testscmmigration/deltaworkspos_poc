
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetServiceOptionsRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetServiceOptionsRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="agentAllowedOnly" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="destinationCountry" type="{http://www.moneygram.com/AgentConnect1705}CountryCodeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetServiceOptionsRequest", propOrder = {
    "agentAllowedOnly",
    "destinationCountry"
})
public class GetServiceOptionsRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected boolean agentAllowedOnly;
    protected String destinationCountry;

    /**
     * Gets the value of the agentAllowedOnly property.
     * 
     */
    public boolean isAgentAllowedOnly() {
        return agentAllowedOnly;
    }

    /**
     * Sets the value of the agentAllowedOnly property.
     * 
     */
    public void setAgentAllowedOnly(boolean value) {
        this.agentAllowedOnly = value;
    }

    /**
     * Gets the value of the destinationCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationCountry() {
        return destinationCountry;
    }

    /**
     * Sets the value of the destinationCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationCountry(String value) {
        this.destinationCountry = value;
    }

}
