
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetDepositInformationResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetDepositInformationResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="startingARBalance" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType"/>
 *                   &lt;element name="depositAndProofTotalAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType"/>
 *                   &lt;element name="outstandingTransactionTotalAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType"/>
 *                   &lt;element name="currentCreditLimit" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType"/>
 *                   &lt;element name="outstandingBalance" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType"/>
 *                   &lt;element name="availableCredit" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType"/>
 *                   &lt;element name="depositAndProofInfo" type="{http://www.moneygram.com/AgentConnect1705}DepositAndProofInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="partnerCredit" type="{http://www.moneygram.com/AgentConnect1705}PartnerCreditType" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetDepositInformationResponse", propOrder = {
    "payload"
})
public class GetDepositInformationResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<GetDepositInformationResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetDepositInformationResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<GetDepositInformationResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetDepositInformationResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<GetDepositInformationResponse.Payload> value) {
        this.payload = ((JAXBElement<GetDepositInformationResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="startingARBalance" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType"/>
     *         &lt;element name="depositAndProofTotalAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType"/>
     *         &lt;element name="outstandingTransactionTotalAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType"/>
     *         &lt;element name="currentCreditLimit" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType"/>
     *         &lt;element name="outstandingBalance" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType"/>
     *         &lt;element name="availableCredit" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType"/>
     *         &lt;element name="depositAndProofInfo" type="{http://www.moneygram.com/AgentConnect1705}DepositAndProofInfo" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="partnerCredit" type="{http://www.moneygram.com/AgentConnect1705}PartnerCreditType" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "startingARBalance",
        "depositAndProofTotalAmount",
        "outstandingTransactionTotalAmount",
        "currentCreditLimit",
        "outstandingBalance",
        "availableCredit",
        "depositAndProofInfo",
        "partnerCredit"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(required = true)
        protected BigDecimal startingARBalance;
        @XmlElement(required = true)
        protected BigDecimal depositAndProofTotalAmount;
        @XmlElement(required = true)
        protected BigDecimal outstandingTransactionTotalAmount;
        @XmlElement(required = true)
        protected BigDecimal currentCreditLimit;
        @XmlElement(required = true)
        protected BigDecimal outstandingBalance;
        @XmlElement(required = true)
        protected BigDecimal availableCredit;
        protected List<DepositAndProofInfo> depositAndProofInfo;
        protected PartnerCreditType partnerCredit;

        /**
         * Gets the value of the startingARBalance property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getStartingARBalance() {
            return startingARBalance;
        }

        /**
         * Sets the value of the startingARBalance property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setStartingARBalance(BigDecimal value) {
            this.startingARBalance = value;
        }

        /**
         * Gets the value of the depositAndProofTotalAmount property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getDepositAndProofTotalAmount() {
            return depositAndProofTotalAmount;
        }

        /**
         * Sets the value of the depositAndProofTotalAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setDepositAndProofTotalAmount(BigDecimal value) {
            this.depositAndProofTotalAmount = value;
        }

        /**
         * Gets the value of the outstandingTransactionTotalAmount property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getOutstandingTransactionTotalAmount() {
            return outstandingTransactionTotalAmount;
        }

        /**
         * Sets the value of the outstandingTransactionTotalAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setOutstandingTransactionTotalAmount(BigDecimal value) {
            this.outstandingTransactionTotalAmount = value;
        }

        /**
         * Gets the value of the currentCreditLimit property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getCurrentCreditLimit() {
            return currentCreditLimit;
        }

        /**
         * Sets the value of the currentCreditLimit property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setCurrentCreditLimit(BigDecimal value) {
            this.currentCreditLimit = value;
        }

        /**
         * Gets the value of the outstandingBalance property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getOutstandingBalance() {
            return outstandingBalance;
        }

        /**
         * Sets the value of the outstandingBalance property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setOutstandingBalance(BigDecimal value) {
            this.outstandingBalance = value;
        }

        /**
         * Gets the value of the availableCredit property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAvailableCredit() {
            return availableCredit;
        }

        /**
         * Sets the value of the availableCredit property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAvailableCredit(BigDecimal value) {
            this.availableCredit = value;
        }

        /**
         * Gets the value of the depositAndProofInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the depositAndProofInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDepositAndProofInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DepositAndProofInfo }
         * 
         * 
         */
        public List<DepositAndProofInfo> getDepositAndProofInfo() {
            if (depositAndProofInfo == null) {
                depositAndProofInfo = new ArrayList<DepositAndProofInfo>();
            }
            return this.depositAndProofInfo;
        }

        /**
         * Gets the value of the partnerCredit property.
         * 
         * @return
         *     possible object is
         *     {@link PartnerCreditType }
         *     
         */
        public PartnerCreditType getPartnerCredit() {
            return partnerCredit;
        }

        /**
         * Sets the value of the partnerCredit property.
         * 
         * @param value
         *     allowed object is
         *     {@link PartnerCreditType }
         *     
         */
        public void setPartnerCredit(PartnerCreditType value) {
            this.partnerCredit = value;
        }

    }

}
