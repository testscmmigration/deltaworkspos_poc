
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AgentAddressType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AgentAddressType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agentAddress" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="agentAddressLine" type="{http://www.moneygram.com/AgentConnect1705}AddressType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgentAddressType", propOrder = {
    "agentAddress"
})
public class AgentAddressType
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected AgentAddressType.AgentAddress agentAddress;

    /**
     * Gets the value of the agentAddress property.
     * 
     * @return
     *     possible object is
     *     {@link AgentAddressType.AgentAddress }
     *     
     */
    public AgentAddressType.AgentAddress getAgentAddress() {
        return agentAddress;
    }

    /**
     * Sets the value of the agentAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link AgentAddressType.AgentAddress }
     *     
     */
    public void setAgentAddress(AgentAddressType.AgentAddress value) {
        this.agentAddress = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="agentAddressLine" type="{http://www.moneygram.com/AgentConnect1705}AddressType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "agentAddressLine"
    })
    public static class AgentAddress
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<String> agentAddressLine;

        /**
         * Gets the value of the agentAddressLine property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the agentAddressLine property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAgentAddressLine().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getAgentAddressLine() {
            if (agentAddressLine == null) {
                agentAddressLine = new ArrayList<String>();
            }
            return this.agentAddressLine;
        }

    }

}
