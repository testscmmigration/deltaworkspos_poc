
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LevelInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LevelInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="levelNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="levelValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="levelAttributeLabel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="levelAttributeTag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="levelAttributeVal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LevelInfo", propOrder = {
    "levelNumber",
    "levelValue",
    "levelAttributeLabel",
    "levelAttributeTag",
    "levelAttributeVal"
})
public class LevelInfo
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected int levelNumber;
    @XmlElement(required = true)
    protected String levelValue;
    protected String levelAttributeLabel;
    @XmlElement(required = true)
    protected String levelAttributeTag;
    @XmlElement(required = true)
    protected String levelAttributeVal;

    /**
     * Gets the value of the levelNumber property.
     * 
     */
    public int getLevelNumber() {
        return levelNumber;
    }

    /**
     * Sets the value of the levelNumber property.
     * 
     */
    public void setLevelNumber(int value) {
        this.levelNumber = value;
    }

    /**
     * Gets the value of the levelValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLevelValue() {
        return levelValue;
    }

    /**
     * Sets the value of the levelValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLevelValue(String value) {
        this.levelValue = value;
    }

    /**
     * Gets the value of the levelAttributeLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLevelAttributeLabel() {
        return levelAttributeLabel;
    }

    /**
     * Sets the value of the levelAttributeLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLevelAttributeLabel(String value) {
        this.levelAttributeLabel = value;
    }

    /**
     * Gets the value of the levelAttributeTag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLevelAttributeTag() {
        return levelAttributeTag;
    }

    /**
     * Sets the value of the levelAttributeTag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLevelAttributeTag(String value) {
        this.levelAttributeTag = value;
    }

    /**
     * Gets the value of the levelAttributeVal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLevelAttributeVal() {
        return levelAttributeVal;
    }

    /**
     * Sets the value of the levelAttributeVal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLevelAttributeVal(String value) {
        this.levelAttributeVal = value;
    }

}
