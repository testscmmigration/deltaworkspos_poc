
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DestinationCountryInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DestinationCountryInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="destinationCountry" type="{http://www.moneygram.com/AgentConnect1705}CountryCodeType"/>
 *         &lt;element name="serviceOptionInfos" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="serviceOptionInfo" type="{http://www.moneygram.com/AgentConnect1705}ServiceOptionInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DestinationCountryInfo", propOrder = {
    "destinationCountry",
    "serviceOptionInfos"
})
public class DestinationCountryInfo
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected String destinationCountry;
    protected DestinationCountryInfo.ServiceOptionInfos serviceOptionInfos;

    /**
     * Gets the value of the destinationCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationCountry() {
        return destinationCountry;
    }

    /**
     * Sets the value of the destinationCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationCountry(String value) {
        this.destinationCountry = value;
    }

    /**
     * Gets the value of the serviceOptionInfos property.
     * 
     * @return
     *     possible object is
     *     {@link DestinationCountryInfo.ServiceOptionInfos }
     *     
     */
    public DestinationCountryInfo.ServiceOptionInfos getServiceOptionInfos() {
        return serviceOptionInfos;
    }

    /**
     * Sets the value of the serviceOptionInfos property.
     * 
     * @param value
     *     allowed object is
     *     {@link DestinationCountryInfo.ServiceOptionInfos }
     *     
     */
    public void setServiceOptionInfos(DestinationCountryInfo.ServiceOptionInfos value) {
        this.serviceOptionInfos = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="serviceOptionInfo" type="{http://www.moneygram.com/AgentConnect1705}ServiceOptionInfo" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "serviceOptionInfo"
    })
    public static class ServiceOptionInfos
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<ServiceOptionInfo> serviceOptionInfo;

        /**
         * Gets the value of the serviceOptionInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the serviceOptionInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getServiceOptionInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ServiceOptionInfo }
         * 
         * 
         */
        public List<ServiceOptionInfo> getServiceOptionInfo() {
            if (serviceOptionInfo == null) {
                serviceOptionInfo = new ArrayList<ServiceOptionInfo>();
            }
            return this.serviceOptionInfo;
        }

    }

}
