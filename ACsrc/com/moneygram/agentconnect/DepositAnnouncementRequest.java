
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for DepositAnnouncementRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DepositAnnouncementRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="mgiEmployeeAutoAccept" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="depositReferenceNumber" type="{http://www.moneygram.com/AgentConnect1705}DepositReferenceNumberType"/>
 *         &lt;element name="depositDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="announcementDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="announcementAmount" type="{http://www.moneygram.com/AgentConnect1705}AmountIncludeZeroType"/>
 *         &lt;element name="announcementCurrencyCode" type="{http://www.moneygram.com/AgentConnect1705}CurrencyCodeType"/>
 *         &lt;element name="paymentTypeCode" type="{http://www.moneygram.com/AgentConnect1705}StringMax2Type"/>
 *         &lt;element name="bankShortCode" type="{http://www.moneygram.com/AgentConnect1705}BankShortCodeType"/>
 *         &lt;element name="bankName" type="{http://www.moneygram.com/AgentConnect1705}StringMax100Type"/>
 *         &lt;element name="announcementRemarks" type="{http://www.moneygram.com/AgentConnect1705}AnnouncementRemarksType"/>
 *         &lt;element name="agentEmployeeID" type="{http://www.moneygram.com/AgentConnect1705}StringMax8Type"/>
 *         &lt;element name="proofImage" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DepositAnnouncementRequest", propOrder = {
    "mgiEmployeeAutoAccept",
    "depositReferenceNumber",
    "depositDate",
    "announcementDate",
    "announcementAmount",
    "announcementCurrencyCode",
    "paymentTypeCode",
    "bankShortCode",
    "bankName",
    "announcementRemarks",
    "agentEmployeeID",
    "proofImage"
})
public class DepositAnnouncementRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected boolean mgiEmployeeAutoAccept;
    @XmlElement(required = true)
    protected String depositReferenceNumber;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar depositDate;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar announcementDate;
    @XmlElement(required = true)
    protected BigDecimal announcementAmount;
    @XmlElement(required = true)
    protected String announcementCurrencyCode;
    @XmlElement(required = true)
    protected String paymentTypeCode;
    @XmlElement(required = true)
    protected String bankShortCode;
    @XmlElement(required = true)
    protected String bankName;
    @XmlElement(required = true)
    protected String announcementRemarks;
    @XmlElement(required = true)
    protected String agentEmployeeID;
    @XmlMimeType("*/*")
    protected DataHandler proofImage;

    /**
     * Gets the value of the mgiEmployeeAutoAccept property.
     * 
     */
    public boolean isMgiEmployeeAutoAccept() {
        return mgiEmployeeAutoAccept;
    }

    /**
     * Sets the value of the mgiEmployeeAutoAccept property.
     * 
     */
    public void setMgiEmployeeAutoAccept(boolean value) {
        this.mgiEmployeeAutoAccept = value;
    }

    /**
     * Gets the value of the depositReferenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepositReferenceNumber() {
        return depositReferenceNumber;
    }

    /**
     * Sets the value of the depositReferenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepositReferenceNumber(String value) {
        this.depositReferenceNumber = value;
    }

    /**
     * Gets the value of the depositDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDepositDate() {
        return depositDate;
    }

    /**
     * Sets the value of the depositDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDepositDate(XMLGregorianCalendar value) {
        this.depositDate = value;
    }

    /**
     * Gets the value of the announcementDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAnnouncementDate() {
        return announcementDate;
    }

    /**
     * Sets the value of the announcementDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAnnouncementDate(XMLGregorianCalendar value) {
        this.announcementDate = value;
    }

    /**
     * Gets the value of the announcementAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAnnouncementAmount() {
        return announcementAmount;
    }

    /**
     * Sets the value of the announcementAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAnnouncementAmount(BigDecimal value) {
        this.announcementAmount = value;
    }

    /**
     * Gets the value of the announcementCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnouncementCurrencyCode() {
        return announcementCurrencyCode;
    }

    /**
     * Sets the value of the announcementCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnouncementCurrencyCode(String value) {
        this.announcementCurrencyCode = value;
    }

    /**
     * Gets the value of the paymentTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentTypeCode() {
        return paymentTypeCode;
    }

    /**
     * Sets the value of the paymentTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentTypeCode(String value) {
        this.paymentTypeCode = value;
    }

    /**
     * Gets the value of the bankShortCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankShortCode() {
        return bankShortCode;
    }

    /**
     * Sets the value of the bankShortCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankShortCode(String value) {
        this.bankShortCode = value;
    }

    /**
     * Gets the value of the bankName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * Sets the value of the bankName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankName(String value) {
        this.bankName = value;
    }

    /**
     * Gets the value of the announcementRemarks property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnouncementRemarks() {
        return announcementRemarks;
    }

    /**
     * Sets the value of the announcementRemarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnouncementRemarks(String value) {
        this.announcementRemarks = value;
    }

    /**
     * Gets the value of the agentEmployeeID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentEmployeeID() {
        return agentEmployeeID;
    }

    /**
     * Sets the value of the agentEmployeeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentEmployeeID(String value) {
        this.agentEmployeeID = value;
    }

    /**
     * Gets the value of the proofImage property.
     * 
     * @return
     *     possible object is
     *     {@link DataHandler }
     *     
     */
    public DataHandler getProofImage() {
        return proofImage;
    }

    /**
     * Sets the value of the proofImage property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataHandler }
     *     
     */
    public void setProofImage(DataHandler value) {
        this.proofImage = value;
    }

}
