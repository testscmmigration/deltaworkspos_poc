
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * MoneyOrders
 * 
 * <p>Java class for ComplianceTransactionRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ComplianceTransactionRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="requestType" type="{http://www.moneygram.com/AgentConnect1705}ComplianceTransactionRequestType"/>
 *         &lt;element name="cci" type="{http://www.moneygram.com/AgentConnect1705}CustomerComplianceInfo" minOccurs="0"/>
 *         &lt;element name="mgReferenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="moneyOrder" type="{http://www.moneygram.com/AgentConnect1705}MoneyOrderInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComplianceTransactionRequest", propOrder = {
    "requestType",
    "cci",
    "mgReferenceNumber",
    "moneyOrder"
})
public class ComplianceTransactionRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected ComplianceTransactionRequestType requestType;
    protected CustomerComplianceInfo cci;
    protected String mgReferenceNumber;
    protected List<MoneyOrderInfo> moneyOrder;

    /**
     * Gets the value of the requestType property.
     * 
     * @return
     *     possible object is
     *     {@link ComplianceTransactionRequestType }
     *     
     */
    public ComplianceTransactionRequestType getRequestType() {
        return requestType;
    }

    /**
     * Sets the value of the requestType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComplianceTransactionRequestType }
     *     
     */
    public void setRequestType(ComplianceTransactionRequestType value) {
        this.requestType = value;
    }

    /**
     * Gets the value of the cci property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerComplianceInfo }
     *     
     */
    public CustomerComplianceInfo getCci() {
        return cci;
    }

    /**
     * Sets the value of the cci property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerComplianceInfo }
     *     
     */
    public void setCci(CustomerComplianceInfo value) {
        this.cci = value;
    }

    /**
     * Gets the value of the mgReferenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMgReferenceNumber() {
        return mgReferenceNumber;
    }

    /**
     * Sets the value of the mgReferenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMgReferenceNumber(String value) {
        this.mgReferenceNumber = value;
    }

    /**
     * Gets the value of the moneyOrder property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the moneyOrder property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMoneyOrder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MoneyOrderInfo }
     * 
     * 
     */
    public List<MoneyOrderInfo> getMoneyOrder() {
        if (moneyOrder == null) {
            moneyOrder = new ArrayList<MoneyOrderInfo>();
        }
        return this.moneyOrder;
    }

}
