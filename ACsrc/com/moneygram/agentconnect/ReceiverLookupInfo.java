
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReceiverLookupInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReceiverLookupInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="currentValues" type="{http://www.moneygram.com/AgentConnect1705}CurrentValuesType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReceiverLookupInfo", propOrder = {
    "currentValues"
})
public class ReceiverLookupInfo
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected CurrentValuesType currentValues;

    /**
     * Gets the value of the currentValues property.
     * 
     * @return
     *     possible object is
     *     {@link CurrentValuesType }
     *     
     */
    public CurrentValuesType getCurrentValues() {
        return currentValues;
    }

    /**
     * Sets the value of the currentValues property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrentValuesType }
     *     
     */
    public void setCurrentValues(CurrentValuesType value) {
        this.currentValues = value;
    }

}
