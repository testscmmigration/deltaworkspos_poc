
package com.moneygram.agentconnect;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MoneyOrderPrintStatusType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MoneyOrderPrintStatusType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PROPOSED"/>
 *     &lt;enumeration value="NOT_ATTEMPTED"/>
 *     &lt;enumeration value="ATTEMPTED"/>
 *     &lt;enumeration value="CONFIRMED_PRINTED"/>
 *     &lt;enumeration value="CONFIRMED_NOT_PRINTED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MoneyOrderPrintStatusType")
@XmlEnum
public enum MoneyOrderPrintStatusType {

    PROPOSED,
    NOT_ATTEMPTED,
    ATTEMPTED,
    CONFIRMED_PRINTED,
    CONFIRMED_NOT_PRINTED;

    public String value() {
        return name();
    }

    public static MoneyOrderPrintStatusType fromValue(String v) {
        return valueOf(v);
    }

}
