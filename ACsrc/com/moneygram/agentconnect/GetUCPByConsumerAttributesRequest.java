
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetUCPByConsumerAttributesRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetUCPByConsumerAttributesRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="functionName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="repeatRequest" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="plusEnrollmentFlag" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="responseFilter" type="{http://www.moneygram.com/AgentConnect1705}StringMax20Type" minOccurs="0"/>
 *         &lt;element name="consumerProfile" type="{http://www.moneygram.com/AgentConnect1705}UCPRequestProfileType"/>
 *         &lt;element name="id" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileIDType" minOccurs="0"/>
 *         &lt;element name="type" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileIDTypeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetUCPByConsumerAttributesRequest", propOrder = {
    "functionName",
    "repeatRequest",
    "plusEnrollmentFlag",
    "responseFilter",
    "consumerProfile",
    "id",
    "type"
})
public class GetUCPByConsumerAttributesRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected String functionName;
    protected boolean repeatRequest;
    protected boolean plusEnrollmentFlag;
    protected String responseFilter;
    @XmlElement(required = true)
    protected UCPRequestProfileType consumerProfile;
    protected String id;
    protected String type;

    /**
     * Gets the value of the functionName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFunctionName() {
        return functionName;
    }

    /**
     * Sets the value of the functionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFunctionName(String value) {
        this.functionName = value;
    }

    /**
     * Gets the value of the repeatRequest property.
     * 
     */
    public boolean isRepeatRequest() {
        return repeatRequest;
    }

    /**
     * Sets the value of the repeatRequest property.
     * 
     */
    public void setRepeatRequest(boolean value) {
        this.repeatRequest = value;
    }

    /**
     * Gets the value of the plusEnrollmentFlag property.
     * 
     */
    public boolean isPlusEnrollmentFlag() {
        return plusEnrollmentFlag;
    }

    /**
     * Sets the value of the plusEnrollmentFlag property.
     * 
     */
    public void setPlusEnrollmentFlag(boolean value) {
        this.plusEnrollmentFlag = value;
    }

    /**
     * Gets the value of the responseFilter property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseFilter() {
        return responseFilter;
    }

    /**
     * Sets the value of the responseFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseFilter(String value) {
        this.responseFilter = value;
    }

    /**
     * Gets the value of the consumerProfile property.
     * 
     * @return
     *     possible object is
     *     {@link UCPRequestProfileType }
     *     
     */
    public UCPRequestProfileType getConsumerProfile() {
        return consumerProfile;
    }

    /**
     * Sets the value of the consumerProfile property.
     * 
     * @param value
     *     allowed object is
     *     {@link UCPRequestProfileType }
     *     
     */
    public void setConsumerProfile(UCPRequestProfileType value) {
        this.consumerProfile = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

}
