
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CategoryInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CategoryInfo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}InfoBase">
 *       &lt;sequence>
 *         &lt;element name="infos" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="info" type="{http://www.moneygram.com/AgentConnect1705}InfoBase" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CategoryInfo", propOrder = {
    "infos"
})
public class CategoryInfo
    extends InfoBase
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "infos", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<CategoryInfo.Infos> infos;

    /**
     * Gets the value of the infos property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CategoryInfo.Infos }{@code >}
     *     
     */
    public JAXBElement<CategoryInfo.Infos> getInfos() {
        return infos;
    }

    /**
     * Sets the value of the infos property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CategoryInfo.Infos }{@code >}
     *     
     */
    public void setInfos(JAXBElement<CategoryInfo.Infos> value) {
        this.infos = ((JAXBElement<CategoryInfo.Infos> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="info" type="{http://www.moneygram.com/AgentConnect1705}InfoBase" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "info"
    })
    public static class Infos
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<InfoBase> info;

        /**
         * Gets the value of the info property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the info property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link InfoBase }
         * 
         * 
         */
        public List<InfoBase> getInfo() {
            if (info == null) {
                info = new ArrayList<InfoBase>();
            }
            return this.info;
        }

    }

}
