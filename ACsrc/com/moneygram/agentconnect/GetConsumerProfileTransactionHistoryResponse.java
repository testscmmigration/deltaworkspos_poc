
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				This API is to be used in conjunction with the getConsumerProfile API. 
 * 				This API returns detailed transaction history, but does not return any information about the consumer profile.
 * 			
 * 
 * <p>Java class for GetConsumerProfileTransactionHistoryResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetConsumerProfileTransactionHistoryResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="mgiSessionID" type="{http://www.moneygram.com/AgentConnect1705}MGISessionIDType"/>
 *                   &lt;element name="GAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType"/>
 *                   &lt;element name="numberOfRowsFound" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="transactions" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="transaction" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileTransactionHistoryType" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="infos" type="{http://www.moneygram.com/AgentConnect1705}InfosType" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetConsumerProfileTransactionHistoryResponse", propOrder = {
    "payload"
})
public class GetConsumerProfileTransactionHistoryResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<GetConsumerProfileTransactionHistoryResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetConsumerProfileTransactionHistoryResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<GetConsumerProfileTransactionHistoryResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetConsumerProfileTransactionHistoryResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<GetConsumerProfileTransactionHistoryResponse.Payload> value) {
        this.payload = ((JAXBElement<GetConsumerProfileTransactionHistoryResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="mgiSessionID" type="{http://www.moneygram.com/AgentConnect1705}MGISessionIDType"/>
     *         &lt;element name="GAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType"/>
     *         &lt;element name="numberOfRowsFound" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="transactions" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="transaction" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileTransactionHistoryType" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="infos" type="{http://www.moneygram.com/AgentConnect1705}InfosType" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mgiSessionID",
        "gafVersionNumber",
        "numberOfRowsFound",
        "transactions",
        "infos"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(required = true)
        protected String mgiSessionID;
        @XmlElement(name = "GAFVersionNumber", required = true)
        protected String gafVersionNumber;
        protected int numberOfRowsFound;
        protected GetConsumerProfileTransactionHistoryResponse.Payload.Transactions transactions;
        protected InfosType infos;

        /**
         * Gets the value of the mgiSessionID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMgiSessionID() {
            return mgiSessionID;
        }

        /**
         * Sets the value of the mgiSessionID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMgiSessionID(String value) {
            this.mgiSessionID = value;
        }

        /**
         * Gets the value of the gafVersionNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGAFVersionNumber() {
            return gafVersionNumber;
        }

        /**
         * Sets the value of the gafVersionNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGAFVersionNumber(String value) {
            this.gafVersionNumber = value;
        }

        /**
         * Gets the value of the numberOfRowsFound property.
         * 
         */
        public int getNumberOfRowsFound() {
            return numberOfRowsFound;
        }

        /**
         * Sets the value of the numberOfRowsFound property.
         * 
         */
        public void setNumberOfRowsFound(int value) {
            this.numberOfRowsFound = value;
        }

        /**
         * Gets the value of the transactions property.
         * 
         * @return
         *     possible object is
         *     {@link GetConsumerProfileTransactionHistoryResponse.Payload.Transactions }
         *     
         */
        public GetConsumerProfileTransactionHistoryResponse.Payload.Transactions getTransactions() {
            return transactions;
        }

        /**
         * Sets the value of the transactions property.
         * 
         * @param value
         *     allowed object is
         *     {@link GetConsumerProfileTransactionHistoryResponse.Payload.Transactions }
         *     
         */
        public void setTransactions(GetConsumerProfileTransactionHistoryResponse.Payload.Transactions value) {
            this.transactions = value;
        }

        /**
         * Gets the value of the infos property.
         * 
         * @return
         *     possible object is
         *     {@link InfosType }
         *     
         */
        public InfosType getInfos() {
            return infos;
        }

        /**
         * Sets the value of the infos property.
         * 
         * @param value
         *     allowed object is
         *     {@link InfosType }
         *     
         */
        public void setInfos(InfosType value) {
            this.infos = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="transaction" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileTransactionHistoryType" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "transaction"
        })
        public static class Transactions
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected List<ConsumerProfileTransactionHistoryType> transaction;

            /**
             * Gets the value of the transaction property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the transaction property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getTransaction().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ConsumerProfileTransactionHistoryType }
             * 
             * 
             */
            public List<ConsumerProfileTransactionHistoryType> getTransaction() {
                if (transaction == null) {
                    transaction = new ArrayList<ConsumerProfileTransactionHistoryType>();
                }
                return this.transaction;
            }

        }

    }

}
