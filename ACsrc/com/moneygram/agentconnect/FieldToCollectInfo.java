
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FieldToCollectInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FieldToCollectInfo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}FieldInfo">
 *       &lt;sequence>
 *         &lt;element name="readOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="required" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="childFieldsKnown" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="fieldMin" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="fieldMax" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="defaultValue" type="{http://www.moneygram.com/AgentConnect1705}FieldValueType" minOccurs="0"/>
 *         &lt;element name="validationRegEx" type="{http://www.moneygram.com/AgentConnect1705}ValidationRegExType" minOccurs="0"/>
 *         &lt;element name="exampleFormat" type="{http://www.moneygram.com/AgentConnect1705}ExampleFormatType" minOccurs="0"/>
 *         &lt;element name="childFields" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="childField" type="{http://www.moneygram.com/AgentConnect1705}ChildField" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="lookupDataSource" type="{http://www.moneygram.com/AgentConnect1705}LookupDataSourceType" minOccurs="0"/>
 *         &lt;element name="lookupKey" type="{http://www.moneygram.com/AgentConnect1705}LookupKeyType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FieldToCollectInfo", propOrder = {
    "readOnly",
    "required",
    "childFieldsKnown",
    "fieldMin",
    "fieldMax",
    "defaultValue",
    "validationRegEx",
    "exampleFormat",
    "childFields",
    "lookupDataSource",
    "lookupKey"
})
public class FieldToCollectInfo
    extends FieldInfo
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected Boolean readOnly;
    protected Boolean required;
    protected Boolean childFieldsKnown;
    protected Long fieldMin;
    protected Long fieldMax;
    protected String defaultValue;
    protected String validationRegEx;
    protected String exampleFormat;
    protected FieldToCollectInfo.ChildFields childFields;
    protected String lookupDataSource;
    protected String lookupKey;

    /**
     * Gets the value of the readOnly property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReadOnly() {
        return readOnly;
    }

    /**
     * Sets the value of the readOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReadOnly(Boolean value) {
        this.readOnly = value;
    }

    /**
     * Gets the value of the required property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRequired() {
        return required;
    }

    /**
     * Sets the value of the required property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRequired(Boolean value) {
        this.required = value;
    }

    /**
     * Gets the value of the childFieldsKnown property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isChildFieldsKnown() {
        return childFieldsKnown;
    }

    /**
     * Sets the value of the childFieldsKnown property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setChildFieldsKnown(Boolean value) {
        this.childFieldsKnown = value;
    }

    /**
     * Gets the value of the fieldMin property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getFieldMin() {
        return fieldMin;
    }

    /**
     * Sets the value of the fieldMin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setFieldMin(Long value) {
        this.fieldMin = value;
    }

    /**
     * Gets the value of the fieldMax property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getFieldMax() {
        return fieldMax;
    }

    /**
     * Sets the value of the fieldMax property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setFieldMax(Long value) {
        this.fieldMax = value;
    }

    /**
     * Gets the value of the defaultValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultValue() {
        return defaultValue;
    }

    /**
     * Sets the value of the defaultValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultValue(String value) {
        this.defaultValue = value;
    }

    /**
     * Gets the value of the validationRegEx property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValidationRegEx() {
        return validationRegEx;
    }

    /**
     * Sets the value of the validationRegEx property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValidationRegEx(String value) {
        this.validationRegEx = value;
    }

    /**
     * Gets the value of the exampleFormat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExampleFormat() {
        return exampleFormat;
    }

    /**
     * Sets the value of the exampleFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExampleFormat(String value) {
        this.exampleFormat = value;
    }

    /**
     * Gets the value of the childFields property.
     * 
     * @return
     *     possible object is
     *     {@link FieldToCollectInfo.ChildFields }
     *     
     */
    public FieldToCollectInfo.ChildFields getChildFields() {
        return childFields;
    }

    /**
     * Sets the value of the childFields property.
     * 
     * @param value
     *     allowed object is
     *     {@link FieldToCollectInfo.ChildFields }
     *     
     */
    public void setChildFields(FieldToCollectInfo.ChildFields value) {
        this.childFields = value;
    }

    /**
     * Gets the value of the lookupDataSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLookupDataSource() {
        return lookupDataSource;
    }

    /**
     * Sets the value of the lookupDataSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLookupDataSource(String value) {
        this.lookupDataSource = value;
    }

    /**
     * Gets the value of the lookupKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLookupKey() {
        return lookupKey;
    }

    /**
     * Sets the value of the lookupKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLookupKey(String value) {
        this.lookupKey = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="childField" type="{http://www.moneygram.com/AgentConnect1705}ChildField" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "childField"
    })
    public static class ChildFields
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<ChildField> childField;

        /**
         * Gets the value of the childField property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the childField property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getChildField().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ChildField }
         * 
         * 
         */
        public List<ChildField> getChildField() {
            if (childField == null) {
                childField = new ArrayList<ChildField>();
            }
            return this.childField;
        }

    }

}
