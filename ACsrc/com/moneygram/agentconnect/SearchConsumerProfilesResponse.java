
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * 		
 * 				The response contains a collection of consumers, each with key/value pairs for the consumer information. 
 * 				Find the keys using GAF with a transactionType of "SearchConsumerProfile".
 * 				
 * 				If a referenceNumber is used for the search, searchConsumerProfiles will validate the transaction is available to be received.
 * 				If it is not, an error will be returned.
 * 				
 * 				No session ID will be created on this API. 
 * 			
 * 
 * <p>Java class for SearchConsumerProfilesResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchConsumerProfilesResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="requestGAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType" minOccurs="0"/>
 *                   &lt;element name="responseGAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType" minOccurs="0"/>
 *                   &lt;element name="consumerProfileSearchInfos" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="consumerProfileSearch" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileSearchInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="infos" type="{http://www.moneygram.com/AgentConnect1705}InfosType" minOccurs="0"/>
 *                   &lt;element name="fieldsToCollect" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="fieldToCollect" type="{http://www.moneygram.com/AgentConnect1705}InfoBase" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchConsumerProfilesResponse", propOrder = {
    "payload"
})
public class SearchConsumerProfilesResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<SearchConsumerProfilesResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SearchConsumerProfilesResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<SearchConsumerProfilesResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SearchConsumerProfilesResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<SearchConsumerProfilesResponse.Payload> value) {
        this.payload = ((JAXBElement<SearchConsumerProfilesResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="requestGAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType" minOccurs="0"/>
     *         &lt;element name="responseGAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType" minOccurs="0"/>
     *         &lt;element name="consumerProfileSearchInfos" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="consumerProfileSearch" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileSearchInfo" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="infos" type="{http://www.moneygram.com/AgentConnect1705}InfosType" minOccurs="0"/>
     *         &lt;element name="fieldsToCollect" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="fieldToCollect" type="{http://www.moneygram.com/AgentConnect1705}InfoBase" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "requestGAFVersionNumber",
        "responseGAFVersionNumber",
        "consumerProfileSearchInfos",
        "infos",
        "fieldsToCollect"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected String requestGAFVersionNumber;
        protected String responseGAFVersionNumber;
        protected SearchConsumerProfilesResponse.Payload.ConsumerProfileSearchInfos consumerProfileSearchInfos;
        protected InfosType infos;
        protected SearchConsumerProfilesResponse.Payload.FieldsToCollect fieldsToCollect;

        /**
         * Gets the value of the requestGAFVersionNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRequestGAFVersionNumber() {
            return requestGAFVersionNumber;
        }

        /**
         * Sets the value of the requestGAFVersionNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRequestGAFVersionNumber(String value) {
            this.requestGAFVersionNumber = value;
        }

        /**
         * Gets the value of the responseGAFVersionNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResponseGAFVersionNumber() {
            return responseGAFVersionNumber;
        }

        /**
         * Sets the value of the responseGAFVersionNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResponseGAFVersionNumber(String value) {
            this.responseGAFVersionNumber = value;
        }

        /**
         * Gets the value of the consumerProfileSearchInfos property.
         * 
         * @return
         *     possible object is
         *     {@link SearchConsumerProfilesResponse.Payload.ConsumerProfileSearchInfos }
         *     
         */
        public SearchConsumerProfilesResponse.Payload.ConsumerProfileSearchInfos getConsumerProfileSearchInfos() {
            return consumerProfileSearchInfos;
        }

        /**
         * Sets the value of the consumerProfileSearchInfos property.
         * 
         * @param value
         *     allowed object is
         *     {@link SearchConsumerProfilesResponse.Payload.ConsumerProfileSearchInfos }
         *     
         */
        public void setConsumerProfileSearchInfos(SearchConsumerProfilesResponse.Payload.ConsumerProfileSearchInfos value) {
            this.consumerProfileSearchInfos = value;
        }

        /**
         * Gets the value of the infos property.
         * 
         * @return
         *     possible object is
         *     {@link InfosType }
         *     
         */
        public InfosType getInfos() {
            return infos;
        }

        /**
         * Sets the value of the infos property.
         * 
         * @param value
         *     allowed object is
         *     {@link InfosType }
         *     
         */
        public void setInfos(InfosType value) {
            this.infos = value;
        }

        /**
         * Gets the value of the fieldsToCollect property.
         * 
         * @return
         *     possible object is
         *     {@link SearchConsumerProfilesResponse.Payload.FieldsToCollect }
         *     
         */
        public SearchConsumerProfilesResponse.Payload.FieldsToCollect getFieldsToCollect() {
            return fieldsToCollect;
        }

        /**
         * Sets the value of the fieldsToCollect property.
         * 
         * @param value
         *     allowed object is
         *     {@link SearchConsumerProfilesResponse.Payload.FieldsToCollect }
         *     
         */
        public void setFieldsToCollect(SearchConsumerProfilesResponse.Payload.FieldsToCollect value) {
            this.fieldsToCollect = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="consumerProfileSearch" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileSearchInfo" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "consumerProfileSearch"
        })
        public static class ConsumerProfileSearchInfos
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected List<ConsumerProfileSearchInfo> consumerProfileSearch;

            /**
             * Gets the value of the consumerProfileSearch property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the consumerProfileSearch property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getConsumerProfileSearch().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ConsumerProfileSearchInfo }
             * 
             * 
             */
            public List<ConsumerProfileSearchInfo> getConsumerProfileSearch() {
                if (consumerProfileSearch == null) {
                    consumerProfileSearch = new ArrayList<ConsumerProfileSearchInfo>();
                }
                return this.consumerProfileSearch;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="fieldToCollect" type="{http://www.moneygram.com/AgentConnect1705}InfoBase" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "fieldToCollect"
        })
        public static class FieldsToCollect
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected List<InfoBase> fieldToCollect;

            /**
             * Gets the value of the fieldToCollect property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the fieldToCollect property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getFieldToCollect().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link InfoBase }
             * 
             * 
             */
            public List<InfoBase> getFieldToCollect() {
                if (fieldToCollect == null) {
                    fieldToCollect = new ArrayList<InfoBase>();
                }
                return this.fieldToCollect;
            }

        }

    }

}
