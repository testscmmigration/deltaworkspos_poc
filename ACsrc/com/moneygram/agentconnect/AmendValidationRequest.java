
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AmendValidationRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AmendValidationRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="GAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType" minOccurs="0"/>
 *         &lt;element name="fieldValues" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="fieldValue" type="{http://www.moneygram.com/AgentConnect1705}KeyValuePairType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="verifiedFields" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="infoKey" type="{http://www.moneygram.com/AgentConnect1705}InfoKeyType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="primaryReceiptLanguage" type="{http://www.moneygram.com/AgentConnect1705}LocaleType" minOccurs="0"/>
 *         &lt;element name="secondaryReceiptLanguage" type="{http://www.moneygram.com/AgentConnect1705}LocaleType" minOccurs="0"/>
 *         &lt;element name="receiptImages" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="receiptImage" type="{http://www.moneygram.com/AgentConnect1705}ReceiptImageContentType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AmendValidationRequest", propOrder = {
    "gafVersionNumber",
    "fieldValues",
    "verifiedFields",
    "primaryReceiptLanguage",
    "secondaryReceiptLanguage",
    "receiptImages"
})
public class AmendValidationRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "GAFVersionNumber")
    protected String gafVersionNumber;
    protected AmendValidationRequest.FieldValues fieldValues;
    protected AmendValidationRequest.VerifiedFields verifiedFields;
    protected String primaryReceiptLanguage;
    protected String secondaryReceiptLanguage;
    protected AmendValidationRequest.ReceiptImages receiptImages;

    /**
     * Gets the value of the gafVersionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGAFVersionNumber() {
        return gafVersionNumber;
    }

    /**
     * Sets the value of the gafVersionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGAFVersionNumber(String value) {
        this.gafVersionNumber = value;
    }

    /**
     * Gets the value of the fieldValues property.
     * 
     * @return
     *     possible object is
     *     {@link AmendValidationRequest.FieldValues }
     *     
     */
    public AmendValidationRequest.FieldValues getFieldValues() {
        return fieldValues;
    }

    /**
     * Sets the value of the fieldValues property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmendValidationRequest.FieldValues }
     *     
     */
    public void setFieldValues(AmendValidationRequest.FieldValues value) {
        this.fieldValues = value;
    }

    /**
     * Gets the value of the verifiedFields property.
     * 
     * @return
     *     possible object is
     *     {@link AmendValidationRequest.VerifiedFields }
     *     
     */
    public AmendValidationRequest.VerifiedFields getVerifiedFields() {
        return verifiedFields;
    }

    /**
     * Sets the value of the verifiedFields property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmendValidationRequest.VerifiedFields }
     *     
     */
    public void setVerifiedFields(AmendValidationRequest.VerifiedFields value) {
        this.verifiedFields = value;
    }

    /**
     * Gets the value of the primaryReceiptLanguage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryReceiptLanguage() {
        return primaryReceiptLanguage;
    }

    /**
     * Sets the value of the primaryReceiptLanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryReceiptLanguage(String value) {
        this.primaryReceiptLanguage = value;
    }

    /**
     * Gets the value of the secondaryReceiptLanguage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondaryReceiptLanguage() {
        return secondaryReceiptLanguage;
    }

    /**
     * Sets the value of the secondaryReceiptLanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondaryReceiptLanguage(String value) {
        this.secondaryReceiptLanguage = value;
    }

    /**
     * Gets the value of the receiptImages property.
     * 
     * @return
     *     possible object is
     *     {@link AmendValidationRequest.ReceiptImages }
     *     
     */
    public AmendValidationRequest.ReceiptImages getReceiptImages() {
        return receiptImages;
    }

    /**
     * Sets the value of the receiptImages property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmendValidationRequest.ReceiptImages }
     *     
     */
    public void setReceiptImages(AmendValidationRequest.ReceiptImages value) {
        this.receiptImages = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="fieldValue" type="{http://www.moneygram.com/AgentConnect1705}KeyValuePairType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fieldValue"
    })
    public static class FieldValues
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<KeyValuePairType> fieldValue;

        /**
         * Gets the value of the fieldValue property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the fieldValue property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFieldValue().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link KeyValuePairType }
         * 
         * 
         */
        public List<KeyValuePairType> getFieldValue() {
            if (fieldValue == null) {
                fieldValue = new ArrayList<KeyValuePairType>();
            }
            return this.fieldValue;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="receiptImage" type="{http://www.moneygram.com/AgentConnect1705}ReceiptImageContentType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "receiptImage"
    })
    public static class ReceiptImages
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<ReceiptImageContentType> receiptImage;

        /**
         * Gets the value of the receiptImage property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the receiptImage property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getReceiptImage().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ReceiptImageContentType }
         * 
         * 
         */
        public List<ReceiptImageContentType> getReceiptImage() {
            if (receiptImage == null) {
                receiptImage = new ArrayList<ReceiptImageContentType>();
            }
            return this.receiptImage;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="infoKey" type="{http://www.moneygram.com/AgentConnect1705}InfoKeyType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "infoKey"
    })
    public static class VerifiedFields
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<String> infoKey;

        /**
         * Gets the value of the infoKey property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the infoKey property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getInfoKey().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getInfoKey() {
            if (infoKey == null) {
                infoKey = new ArrayList<String>();
            }
            return this.infoKey;
        }

    }

}
