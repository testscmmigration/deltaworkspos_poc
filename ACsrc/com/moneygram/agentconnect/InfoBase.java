
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InfoBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InfoBase">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="infoKey" type="{http://www.moneygram.com/AgentConnect1705}InfoKeyType"/>
 *         &lt;element name="label" type="{http://www.moneygram.com/AgentConnect1705}LabelType" minOccurs="0"/>
 *         &lt;element name="labelStandAlone" type="{http://www.moneygram.com/AgentConnect1705}LabelType" minOccurs="0"/>
 *         &lt;element name="displayOrder" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="helpTextShort" type="{http://www.moneygram.com/AgentConnect1705}HelpTextShortType" minOccurs="0"/>
 *         &lt;element name="helpTextLong" type="{http://www.moneygram.com/AgentConnect1705}HelpTextLongType" minOccurs="0"/>
 *         &lt;element name="documentation" type="{http://www.moneygram.com/AgentConnect1705}DocumentationType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InfoBase", propOrder = {
    "infoKey",
    "label",
    "labelStandAlone",
    "displayOrder",
    "helpTextShort",
    "helpTextLong",
    "documentation"
})
@XmlSeeAlso({
    FieldInfo.class,
    CategoryInfo.class
})
public class InfoBase
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    protected String infoKey;
    protected String label;
    protected String labelStandAlone;
    protected Integer displayOrder;
    protected String helpTextShort;
    protected String helpTextLong;
    protected String documentation;

    /**
     * Gets the value of the infoKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfoKey() {
        return infoKey;
    }

    /**
     * Sets the value of the infoKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfoKey(String value) {
        this.infoKey = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the labelStandAlone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabelStandAlone() {
        return labelStandAlone;
    }

    /**
     * Sets the value of the labelStandAlone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabelStandAlone(String value) {
        this.labelStandAlone = value;
    }

    /**
     * Gets the value of the displayOrder property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDisplayOrder() {
        return displayOrder;
    }

    /**
     * Sets the value of the displayOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDisplayOrder(Integer value) {
        this.displayOrder = value;
    }

    /**
     * Gets the value of the helpTextShort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHelpTextShort() {
        return helpTextShort;
    }

    /**
     * Sets the value of the helpTextShort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHelpTextShort(String value) {
        this.helpTextShort = value;
    }

    /**
     * Gets the value of the helpTextLong property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHelpTextLong() {
        return helpTextLong;
    }

    /**
     * Sets the value of the helpTextLong property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHelpTextLong(String value) {
        this.helpTextLong = value;
    }

    /**
     * Gets the value of the documentation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentation() {
        return documentation;
    }

    /**
     * Sets the value of the documentation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentation(String value) {
        this.documentation = value;
    }

}
