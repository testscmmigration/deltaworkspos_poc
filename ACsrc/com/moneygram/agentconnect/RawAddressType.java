
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RawAddressType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RawAddressType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}BaseRawAddressType">
 *       &lt;sequence>
 *         &lt;element name="standardizedAddress" type="{http://www.moneygram.com/AgentConnect1705}StandardizedAddressType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RawAddressType", propOrder = {
    "standardizedAddress"
})
public class RawAddressType
    extends BaseRawAddressType
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected StandardizedAddressType standardizedAddress;

    /**
     * Gets the value of the standardizedAddress property.
     * 
     * @return
     *     possible object is
     *     {@link StandardizedAddressType }
     *     
     */
    public StandardizedAddressType getStandardizedAddress() {
        return standardizedAddress;
    }

    /**
     * Sets the value of the standardizedAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link StandardizedAddressType }
     *     
     */
    public void setStandardizedAddress(StandardizedAddressType value) {
        this.standardizedAddress = value;
    }

}
