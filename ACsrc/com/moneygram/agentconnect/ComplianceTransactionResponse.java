
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ComplianceTransactionResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ComplianceTransactionResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="complianceInfoRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="cci" type="{http://www.moneygram.com/AgentConnect1705}CustomerComplianceInfo" minOccurs="0"/>
 *                   &lt;element name="ofacStatus" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="ofacSourceID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="batchID" type="{http://www.moneygram.com/AgentConnect1705}StringMax20Type" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComplianceTransactionResponse", propOrder = {
    "payload"
})
public class ComplianceTransactionResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<ComplianceTransactionResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ComplianceTransactionResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<ComplianceTransactionResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ComplianceTransactionResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<ComplianceTransactionResponse.Payload> value) {
        this.payload = ((JAXBElement<ComplianceTransactionResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="complianceInfoRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="cci" type="{http://www.moneygram.com/AgentConnect1705}CustomerComplianceInfo" minOccurs="0"/>
     *         &lt;element name="ofacStatus" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="ofacSourceID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="batchID" type="{http://www.moneygram.com/AgentConnect1705}StringMax20Type" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "complianceInfoRequired",
        "cci",
        "ofacStatus",
        "ofacSourceID",
        "batchID"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected boolean complianceInfoRequired;
        protected CustomerComplianceInfo cci;
        protected boolean ofacStatus;
        protected String ofacSourceID;
        protected String batchID;

        /**
         * Gets the value of the complianceInfoRequired property.
         * 
         */
        public boolean isComplianceInfoRequired() {
            return complianceInfoRequired;
        }

        /**
         * Sets the value of the complianceInfoRequired property.
         * 
         */
        public void setComplianceInfoRequired(boolean value) {
            this.complianceInfoRequired = value;
        }

        /**
         * Gets the value of the cci property.
         * 
         * @return
         *     possible object is
         *     {@link CustomerComplianceInfo }
         *     
         */
        public CustomerComplianceInfo getCci() {
            return cci;
        }

        /**
         * Sets the value of the cci property.
         * 
         * @param value
         *     allowed object is
         *     {@link CustomerComplianceInfo }
         *     
         */
        public void setCci(CustomerComplianceInfo value) {
            this.cci = value;
        }

        /**
         * Gets the value of the ofacStatus property.
         * 
         */
        public boolean isOfacStatus() {
            return ofacStatus;
        }

        /**
         * Sets the value of the ofacStatus property.
         * 
         */
        public void setOfacStatus(boolean value) {
            this.ofacStatus = value;
        }

        /**
         * Gets the value of the ofacSourceID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOfacSourceID() {
            return ofacSourceID;
        }

        /**
         * Sets the value of the ofacSourceID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOfacSourceID(String value) {
            this.ofacSourceID = value;
        }

        /**
         * Gets the value of the batchID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBatchID() {
            return batchID;
        }

        /**
         * Sets the value of the batchID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBatchID(String value) {
            this.batchID = value;
        }

    }

}
