
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				This API saves an image to a consumer profile.  
 * 				This API is used when the createOrUpdateConsumerProfileRequest requires a personalIDx_ImageReferenceID.
 * 				The returned imageReferenceID needs to be returned on the createOrUpdateConsumerProfileRequest where it's referenced.
 * 				The personalIDx information needs to be sent in the fieldValues to ensure the image is saved with the correct personal ID information.
 * 				
 * 				This API uses GAF with a transactionType of "saveConsumerProfileImage"
 * 			
 * 
 * <p>Java class for SaveConsumerProfileImageRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SaveConsumerProfileImageRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="GAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType" minOccurs="0"/>
 *         &lt;element name="consumerProfileID" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileIDType"/>
 *         &lt;element name="consumerProfileIDType" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileIDTypeType"/>
 *         &lt;element name="ConsumerProfileImage" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileImageContentType"/>
 *         &lt;element name="fieldValues" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="fieldValue" type="{http://www.moneygram.com/AgentConnect1705}KeyValuePairType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SaveConsumerProfileImageRequest", propOrder = {
    "gafVersionNumber",
    "consumerProfileID",
    "consumerProfileIDType",
    "consumerProfileImage",
    "fieldValues"
})
public class SaveConsumerProfileImageRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "GAFVersionNumber")
    protected String gafVersionNumber;
    @XmlElement(required = true)
    protected String consumerProfileID;
    @XmlElement(required = true)
    protected String consumerProfileIDType;
    @XmlElement(name = "ConsumerProfileImage", required = true)
    protected ConsumerProfileImageContentType consumerProfileImage;
    protected SaveConsumerProfileImageRequest.FieldValues fieldValues;

    /**
     * Gets the value of the gafVersionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGAFVersionNumber() {
        return gafVersionNumber;
    }

    /**
     * Sets the value of the gafVersionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGAFVersionNumber(String value) {
        this.gafVersionNumber = value;
    }

    /**
     * Gets the value of the consumerProfileID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsumerProfileID() {
        return consumerProfileID;
    }

    /**
     * Sets the value of the consumerProfileID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsumerProfileID(String value) {
        this.consumerProfileID = value;
    }

    /**
     * Gets the value of the consumerProfileIDType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsumerProfileIDType() {
        return consumerProfileIDType;
    }

    /**
     * Sets the value of the consumerProfileIDType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsumerProfileIDType(String value) {
        this.consumerProfileIDType = value;
    }

    /**
     * Gets the value of the consumerProfileImage property.
     * 
     * @return
     *     possible object is
     *     {@link ConsumerProfileImageContentType }
     *     
     */
    public ConsumerProfileImageContentType getConsumerProfileImage() {
        return consumerProfileImage;
    }

    /**
     * Sets the value of the consumerProfileImage property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsumerProfileImageContentType }
     *     
     */
    public void setConsumerProfileImage(ConsumerProfileImageContentType value) {
        this.consumerProfileImage = value;
    }

    /**
     * Gets the value of the fieldValues property.
     * 
     * @return
     *     possible object is
     *     {@link SaveConsumerProfileImageRequest.FieldValues }
     *     
     */
    public SaveConsumerProfileImageRequest.FieldValues getFieldValues() {
        return fieldValues;
    }

    /**
     * Sets the value of the fieldValues property.
     * 
     * @param value
     *     allowed object is
     *     {@link SaveConsumerProfileImageRequest.FieldValues }
     *     
     */
    public void setFieldValues(SaveConsumerProfileImageRequest.FieldValues value) {
        this.fieldValues = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="fieldValue" type="{http://www.moneygram.com/AgentConnect1705}KeyValuePairType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fieldValue"
    })
    public static class FieldValues
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<KeyValuePairType> fieldValue;

        /**
         * Gets the value of the fieldValue property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the fieldValue property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFieldValue().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link KeyValuePairType }
         * 
         * 
         */
        public List<KeyValuePairType> getFieldValue() {
            if (fieldValue == null) {
                fieldValue = new ArrayList<KeyValuePairType>();
            }
            return this.fieldValue;
        }

    }

}
