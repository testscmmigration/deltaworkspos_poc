
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SaveConsumerProfileImageResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SaveConsumerProfileImageResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="mgiSessionID" type="{http://www.moneygram.com/AgentConnect1705}MGISessionIDType"/>
 *                   &lt;element name="GAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType"/>
 *                   &lt;element name="ImageReferenceID" type="{http://www.moneygram.com/AgentConnect1705}StringMax255Type" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SaveConsumerProfileImageResponse", propOrder = {
    "payload"
})
public class SaveConsumerProfileImageResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<SaveConsumerProfileImageResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SaveConsumerProfileImageResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<SaveConsumerProfileImageResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SaveConsumerProfileImageResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<SaveConsumerProfileImageResponse.Payload> value) {
        this.payload = ((JAXBElement<SaveConsumerProfileImageResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="mgiSessionID" type="{http://www.moneygram.com/AgentConnect1705}MGISessionIDType"/>
     *         &lt;element name="GAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType"/>
     *         &lt;element name="ImageReferenceID" type="{http://www.moneygram.com/AgentConnect1705}StringMax255Type" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mgiSessionID",
        "gafVersionNumber",
        "imageReferenceID"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(required = true)
        protected String mgiSessionID;
        @XmlElement(name = "GAFVersionNumber", required = true)
        protected String gafVersionNumber;
        @XmlElement(name = "ImageReferenceID")
        protected String imageReferenceID;

        /**
         * Gets the value of the mgiSessionID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMgiSessionID() {
            return mgiSessionID;
        }

        /**
         * Sets the value of the mgiSessionID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMgiSessionID(String value) {
            this.mgiSessionID = value;
        }

        /**
         * Gets the value of the gafVersionNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGAFVersionNumber() {
            return gafVersionNumber;
        }

        /**
         * Sets the value of the gafVersionNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGAFVersionNumber(String value) {
            this.gafVersionNumber = value;
        }

        /**
         * Gets the value of the imageReferenceID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getImageReferenceID() {
            return imageReferenceID;
        }

        /**
         * Sets the value of the imageReferenceID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setImageReferenceID(String value) {
            this.imageReferenceID = value;
        }

    }

}
