
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DisclosureTextDetailsRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DisclosureTextDetailsRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="paperFormat" type="{http://www.moneygram.com/AgentConnect1705}PaperFormatType" minOccurs="0"/>
 *         &lt;element name="mimeType" type="{http://www.moneygram.com/AgentConnect1705}MimeType" minOccurs="0"/>
 *         &lt;element name="downloadAsAttachment" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="languages">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="longLanguageCode" type="{http://www.moneygram.com/AgentConnect1705}LanguageLongCodeType" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="dcaTokenValues" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="dcaToken" type="{http://www.moneygram.com/AgentConnect1705}DCATokenType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="province" type="{http://www.moneygram.com/AgentConnect1705}StateType" minOccurs="0"/>
 *         &lt;element name="destinationCountry" type="{http://www.moneygram.com/AgentConnect1705}CountryCodeType"/>
 *         &lt;element name="disclosureTextRevisionNumber" type="{http://www.moneygram.com/AgentConnect1705}StringMax4Type" minOccurs="0"/>
 *         &lt;element name="dcaTextTagValues">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="dcaTextTag" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DisclosureTextDetailsRequest", propOrder = {
    "paperFormat",
    "mimeType",
    "downloadAsAttachment",
    "languages",
    "dcaTokenValues",
    "province",
    "destinationCountry",
    "disclosureTextRevisionNumber",
    "dcaTextTagValues"
})
public class DisclosureTextDetailsRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected String paperFormat;
    protected String mimeType;
    protected Boolean downloadAsAttachment;
    @XmlElement(required = true)
    protected DisclosureTextDetailsRequest.Languages languages;
    protected DisclosureTextDetailsRequest.DcaTokenValues dcaTokenValues;
    protected String province;
    @XmlElement(required = true)
    protected String destinationCountry;
    protected String disclosureTextRevisionNumber;
    @XmlElement(required = true)
    protected DisclosureTextDetailsRequest.DcaTextTagValues dcaTextTagValues;

    /**
     * Gets the value of the paperFormat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaperFormat() {
        return paperFormat;
    }

    /**
     * Sets the value of the paperFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaperFormat(String value) {
        this.paperFormat = value;
    }

    /**
     * Gets the value of the mimeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMimeType() {
        return mimeType;
    }

    /**
     * Sets the value of the mimeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMimeType(String value) {
        this.mimeType = value;
    }

    /**
     * Gets the value of the downloadAsAttachment property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDownloadAsAttachment() {
        return downloadAsAttachment;
    }

    /**
     * Sets the value of the downloadAsAttachment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDownloadAsAttachment(Boolean value) {
        this.downloadAsAttachment = value;
    }

    /**
     * Gets the value of the languages property.
     * 
     * @return
     *     possible object is
     *     {@link DisclosureTextDetailsRequest.Languages }
     *     
     */
    public DisclosureTextDetailsRequest.Languages getLanguages() {
        return languages;
    }

    /**
     * Sets the value of the languages property.
     * 
     * @param value
     *     allowed object is
     *     {@link DisclosureTextDetailsRequest.Languages }
     *     
     */
    public void setLanguages(DisclosureTextDetailsRequest.Languages value) {
        this.languages = value;
    }

    /**
     * Gets the value of the dcaTokenValues property.
     * 
     * @return
     *     possible object is
     *     {@link DisclosureTextDetailsRequest.DcaTokenValues }
     *     
     */
    public DisclosureTextDetailsRequest.DcaTokenValues getDcaTokenValues() {
        return dcaTokenValues;
    }

    /**
     * Sets the value of the dcaTokenValues property.
     * 
     * @param value
     *     allowed object is
     *     {@link DisclosureTextDetailsRequest.DcaTokenValues }
     *     
     */
    public void setDcaTokenValues(DisclosureTextDetailsRequest.DcaTokenValues value) {
        this.dcaTokenValues = value;
    }

    /**
     * Gets the value of the province property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvince() {
        return province;
    }

    /**
     * Sets the value of the province property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvince(String value) {
        this.province = value;
    }

    /**
     * Gets the value of the destinationCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinationCountry() {
        return destinationCountry;
    }

    /**
     * Sets the value of the destinationCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinationCountry(String value) {
        this.destinationCountry = value;
    }

    /**
     * Gets the value of the disclosureTextRevisionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisclosureTextRevisionNumber() {
        return disclosureTextRevisionNumber;
    }

    /**
     * Sets the value of the disclosureTextRevisionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisclosureTextRevisionNumber(String value) {
        this.disclosureTextRevisionNumber = value;
    }

    /**
     * Gets the value of the dcaTextTagValues property.
     * 
     * @return
     *     possible object is
     *     {@link DisclosureTextDetailsRequest.DcaTextTagValues }
     *     
     */
    public DisclosureTextDetailsRequest.DcaTextTagValues getDcaTextTagValues() {
        return dcaTextTagValues;
    }

    /**
     * Sets the value of the dcaTextTagValues property.
     * 
     * @param value
     *     allowed object is
     *     {@link DisclosureTextDetailsRequest.DcaTextTagValues }
     *     
     */
    public void setDcaTextTagValues(DisclosureTextDetailsRequest.DcaTextTagValues value) {
        this.dcaTextTagValues = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="dcaTextTag" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dcaTextTag"
    })
    public static class DcaTextTagValues
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(required = true)
        protected List<String> dcaTextTag;

        /**
         * Gets the value of the dcaTextTag property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the dcaTextTag property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDcaTextTag().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getDcaTextTag() {
            if (dcaTextTag == null) {
                dcaTextTag = new ArrayList<String>();
            }
            return this.dcaTextTag;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="dcaToken" type="{http://www.moneygram.com/AgentConnect1705}DCATokenType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dcaToken"
    })
    public static class DcaTokenValues
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected List<DCATokenType> dcaToken;

        /**
         * Gets the value of the dcaToken property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the dcaToken property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDcaToken().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DCATokenType }
         * 
         * 
         */
        public List<DCATokenType> getDcaToken() {
            if (dcaToken == null) {
                dcaToken = new ArrayList<DCATokenType>();
            }
            return this.dcaToken;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="longLanguageCode" type="{http://www.moneygram.com/AgentConnect1705}LanguageLongCodeType" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "longLanguageCode"
    })
    public static class Languages
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(required = true)
        protected List<String> longLanguageCode;

        /**
         * Gets the value of the longLanguageCode property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the longLanguageCode property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getLongLanguageCode().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getLongLanguageCode() {
            if (longLanguageCode == null) {
                longLanguageCode = new ArrayList<String>();
            }
            return this.longLanguageCode;
        }

    }

}
