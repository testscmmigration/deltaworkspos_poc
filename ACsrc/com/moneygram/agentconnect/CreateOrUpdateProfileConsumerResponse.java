
package com.moneygram.agentconnect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreateOrUpdateProfileConsumerResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreateOrUpdateProfileConsumerResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="mgiSessionID" type="{http://www.moneygram.com/AgentConnect1705}MGISessionIDType"/>
 *                   &lt;element name="GAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType"/>
 *                   &lt;element name="customerServiceMessage" type="{http://www.moneygram.com/AgentConnect1705}StringMax255Type" minOccurs="0"/>
 *                   &lt;element name="consumerProfileID" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileIDType" minOccurs="0"/>
 *                   &lt;element name="consumerProfileIDs" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="consumerProfileIDInfo" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileIDInfo" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="fieldsToCollect" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="fieldToCollect" type="{http://www.moneygram.com/AgentConnect1705}InfoBase" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateOrUpdateProfileConsumerResponse", propOrder = {
    "payload"
})
public class CreateOrUpdateProfileConsumerResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<CreateOrUpdateProfileConsumerResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CreateOrUpdateProfileConsumerResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<CreateOrUpdateProfileConsumerResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CreateOrUpdateProfileConsumerResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<CreateOrUpdateProfileConsumerResponse.Payload> value) {
        this.payload = ((JAXBElement<CreateOrUpdateProfileConsumerResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="mgiSessionID" type="{http://www.moneygram.com/AgentConnect1705}MGISessionIDType"/>
     *         &lt;element name="GAFVersionNumber" type="{http://www.moneygram.com/AgentConnect1705}GAFVersionNumberType"/>
     *         &lt;element name="customerServiceMessage" type="{http://www.moneygram.com/AgentConnect1705}StringMax255Type" minOccurs="0"/>
     *         &lt;element name="consumerProfileID" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileIDType" minOccurs="0"/>
     *         &lt;element name="consumerProfileIDs" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="consumerProfileIDInfo" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileIDInfo" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="fieldsToCollect" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="fieldToCollect" type="{http://www.moneygram.com/AgentConnect1705}InfoBase" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mgiSessionID",
        "gafVersionNumber",
        "customerServiceMessage",
        "consumerProfileID",
        "consumerProfileIDs",
        "fieldsToCollect"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(required = true)
        protected String mgiSessionID;
        @XmlElement(name = "GAFVersionNumber", required = true)
        protected String gafVersionNumber;
        protected String customerServiceMessage;
        protected String consumerProfileID;
        protected CreateOrUpdateProfileConsumerResponse.Payload.ConsumerProfileIDs consumerProfileIDs;
        protected CreateOrUpdateProfileConsumerResponse.Payload.FieldsToCollect fieldsToCollect;

        /**
         * Gets the value of the mgiSessionID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMgiSessionID() {
            return mgiSessionID;
        }

        /**
         * Sets the value of the mgiSessionID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMgiSessionID(String value) {
            this.mgiSessionID = value;
        }

        /**
         * Gets the value of the gafVersionNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGAFVersionNumber() {
            return gafVersionNumber;
        }

        /**
         * Sets the value of the gafVersionNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGAFVersionNumber(String value) {
            this.gafVersionNumber = value;
        }

        /**
         * Gets the value of the customerServiceMessage property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustomerServiceMessage() {
            return customerServiceMessage;
        }

        /**
         * Sets the value of the customerServiceMessage property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustomerServiceMessage(String value) {
            this.customerServiceMessage = value;
        }

        /**
         * Gets the value of the consumerProfileID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getConsumerProfileID() {
            return consumerProfileID;
        }

        /**
         * Sets the value of the consumerProfileID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setConsumerProfileID(String value) {
            this.consumerProfileID = value;
        }

        /**
         * Gets the value of the consumerProfileIDs property.
         * 
         * @return
         *     possible object is
         *     {@link CreateOrUpdateProfileConsumerResponse.Payload.ConsumerProfileIDs }
         *     
         */
        public CreateOrUpdateProfileConsumerResponse.Payload.ConsumerProfileIDs getConsumerProfileIDs() {
            return consumerProfileIDs;
        }

        /**
         * Sets the value of the consumerProfileIDs property.
         * 
         * @param value
         *     allowed object is
         *     {@link CreateOrUpdateProfileConsumerResponse.Payload.ConsumerProfileIDs }
         *     
         */
        public void setConsumerProfileIDs(CreateOrUpdateProfileConsumerResponse.Payload.ConsumerProfileIDs value) {
            this.consumerProfileIDs = value;
        }

        /**
         * Gets the value of the fieldsToCollect property.
         * 
         * @return
         *     possible object is
         *     {@link CreateOrUpdateProfileConsumerResponse.Payload.FieldsToCollect }
         *     
         */
        public CreateOrUpdateProfileConsumerResponse.Payload.FieldsToCollect getFieldsToCollect() {
            return fieldsToCollect;
        }

        /**
         * Sets the value of the fieldsToCollect property.
         * 
         * @param value
         *     allowed object is
         *     {@link CreateOrUpdateProfileConsumerResponse.Payload.FieldsToCollect }
         *     
         */
        public void setFieldsToCollect(CreateOrUpdateProfileConsumerResponse.Payload.FieldsToCollect value) {
            this.fieldsToCollect = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="consumerProfileIDInfo" type="{http://www.moneygram.com/AgentConnect1705}ConsumerProfileIDInfo" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "consumerProfileIDInfo"
        })
        public static class ConsumerProfileIDs
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected List<ConsumerProfileIDInfo> consumerProfileIDInfo;

            /**
             * Gets the value of the consumerProfileIDInfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the consumerProfileIDInfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getConsumerProfileIDInfo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ConsumerProfileIDInfo }
             * 
             * 
             */
            public List<ConsumerProfileIDInfo> getConsumerProfileIDInfo() {
                if (consumerProfileIDInfo == null) {
                    consumerProfileIDInfo = new ArrayList<ConsumerProfileIDInfo>();
                }
                return this.consumerProfileIDInfo;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="fieldToCollect" type="{http://www.moneygram.com/AgentConnect1705}InfoBase" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "fieldToCollect"
        })
        public static class FieldsToCollect
            implements Serializable
        {

            private final static long serialVersionUID = 1L;
            protected List<InfoBase> fieldToCollect;

            /**
             * Gets the value of the fieldToCollect property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the fieldToCollect property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getFieldToCollect().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link InfoBase }
             * 
             * 
             */
            public List<InfoBase> getFieldToCollect() {
                if (fieldToCollect == null) {
                    fieldToCollect = new ArrayList<InfoBase>();
                }
                return this.fieldToCollect;
            }

        }

    }

}
