
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VersionManifestResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VersionManifestResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Response">
 *       &lt;sequence>
 *         &lt;element name="payload" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
 *                 &lt;sequence>
 *                   &lt;element name="manifestSize" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="manifestURL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="versionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="unitProfileID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VersionManifestResponse", propOrder = {
    "payload"
})
public class VersionManifestResponse
    extends Response
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElementRef(name = "payload", namespace = "http://www.moneygram.com/AgentConnect1705", type = JAXBElement.class)
    protected JAXBElement<VersionManifestResponse.Payload> payload;

    /**
     * Gets the value of the payload property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link VersionManifestResponse.Payload }{@code >}
     *     
     */
    public JAXBElement<VersionManifestResponse.Payload> getPayload() {
        return payload;
    }

    /**
     * Sets the value of the payload property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link VersionManifestResponse.Payload }{@code >}
     *     
     */
    public void setPayload(JAXBElement<VersionManifestResponse.Payload> value) {
        this.payload = ((JAXBElement<VersionManifestResponse.Payload> ) value);
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Payload">
     *       &lt;sequence>
     *         &lt;element name="manifestSize" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="manifestURL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="versionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="unitProfileID" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "manifestSize",
        "manifestURL",
        "versionID",
        "unitProfileID"
    })
    public static class Payload
        extends com.moneygram.agentconnect.Payload
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        protected long manifestSize;
        @XmlElement(required = true)
        protected String manifestURL;
        @XmlElement(required = true)
        protected String versionID;
        protected int unitProfileID;

        /**
         * Gets the value of the manifestSize property.
         * 
         */
        public long getManifestSize() {
            return manifestSize;
        }

        /**
         * Sets the value of the manifestSize property.
         * 
         */
        public void setManifestSize(long value) {
            this.manifestSize = value;
        }

        /**
         * Gets the value of the manifestURL property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getManifestURL() {
            return manifestURL;
        }

        /**
         * Sets the value of the manifestURL property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setManifestURL(String value) {
            this.manifestURL = value;
        }

        /**
         * Gets the value of the versionID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVersionID() {
            return versionID;
        }

        /**
         * Sets the value of the versionID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVersionID(String value) {
            this.versionID = value;
        }

        /**
         * Gets the value of the unitProfileID property.
         * 
         */
        public int getUnitProfileID() {
            return unitProfileID;
        }

        /**
         * Sets the value of the unitProfileID property.
         * 
         */
        public void setUnitProfileID(int value) {
            this.unitProfileID = value;
        }

    }

}
