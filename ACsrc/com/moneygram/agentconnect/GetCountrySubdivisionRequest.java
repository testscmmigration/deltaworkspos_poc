
package com.moneygram.agentconnect;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCountrySubdivisionRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCountrySubdivisionRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.moneygram.com/AgentConnect1705}Request">
 *       &lt;sequence>
 *         &lt;element name="cachedVersion" type="{http://www.moneygram.com/AgentConnect1705}VersionType" minOccurs="0"/>
 *         &lt;element name="countryCode" type="{http://www.moneygram.com/AgentConnect1705}CountryCodeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCountrySubdivisionRequest", propOrder = {
    "cachedVersion",
    "countryCode"
})
public class GetCountrySubdivisionRequest
    extends Request
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    protected String cachedVersion;
    protected String countryCode;

    /**
     * Gets the value of the cachedVersion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCachedVersion() {
        return cachedVersion;
    }

    /**
     * Sets the value of the cachedVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCachedVersion(String value) {
        this.cachedVersion = value;
    }

    /**
     * Gets the value of the countryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Sets the value of the countryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

}
