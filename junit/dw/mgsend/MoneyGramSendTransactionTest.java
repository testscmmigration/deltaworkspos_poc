
package dw.mgsend;

import java.math.BigDecimal;
import java.util.List;

import junit.framework.TestCase;
import dw.TestCountryInfo;
import dw.TestPage;
import dw.TestQdo;
import dw.UnitProfileMockHelper;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.CountryInfo.Country;
import dw.model.adapters.FeeMsgMC;
import dw.model.adapters.QualifiedDeliveryOption;
import dw.utility.DWValues;
import dw.utility.ExtraDebug;


/**
 * @author A121
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MoneyGramSendTransactionTest extends TestCase {
	UnitProfileMockHelper theMock;
	
	
	private int MONEY_GRAM_SEND_DOC_SEQ = 3;
	private int MONEY_GRAM_RECEIVER_REGISTRATION = 9;
	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		doMockSetup();
	}
	
	protected void doMockSetup() throws Exception {
		theMock = new UnitProfileMockHelper();
		theMock.setup();
	}
	
	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
		theMock.teardown();
	}
	
	public void testCheckLimits() throws Exception {
		theMock.setupWalmartAgent();
		theMock.setupProduct(MONEY_GRAM_SEND_DOC_SEQ, "91");
		theMock.replayMocks();
		
		MoneyGramSendTransaction mgst = new MoneyGramSendTransaction("Foo",
				"Bar", MONEY_GRAM_SEND_DOC_SEQ, false, false);

		assertEquals(MoneyGramSendTransaction.OK,
				mgst.setAmount(new BigDecimal(60)));
		assertEquals(new BigDecimal(60), mgst.getAmount());
		assertEquals(MoneyGramSendTransaction.MAX_ITEM_AMOUNT_EXCEEDED,
				mgst.setAmount(new BigDecimal(10000.00)));
		assertEquals(MoneyGramSendTransaction.MAX_ITEM_AMOUNT_EXCEEDED,
				mgst.setAmount(new BigDecimal(100001.00)));
	}
	
    public void testSetMGCustomerReceiveNumber() throws Exception {

		theMock.setupWalmartAgent();
		theMock.setupProduct(MONEY_GRAM_SEND_DOC_SEQ, "90");
		theMock.replayMocks();
    	
		MoneyGramSendTransaction mgst = new MoneyGramSendTransaction("Foo",
				"Bar", MONEY_GRAM_SEND_DOC_SEQ, false, false);

		mgst.setCustomerReceiveNumber("MG12345678");
		
        assertEquals("MG12345678", mgst.getCustomerReceiveNumber());
        
    }
    
    public void testGetChangeDue() throws Exception {

		theMock.setupWalmartAgent();
		theMock.setupProduct(MONEY_GRAM_SEND_DOC_SEQ, "91");
		theMock.replayMocks();
    	
		MoneyGramSendTransaction mgst = new MoneyGramSendTransaction("Foo",
				"Bar", MONEY_GRAM_SEND_DOC_SEQ, false, false);

		mgst.setCustomerReceiveNumber("MG12345678");
		BigDecimal totalAmtToCollect = new BigDecimal("1500.50");
		BigDecimal amtTendered = new BigDecimal("2000.00");
		BigDecimal change = mgst.getChangeDue(totalAmtToCollect.toString(),
											  amtTendered.toString());
        assertEquals("499.50", change.toString());      
        
    }


    public void testFormatPhoneNumber() throws Exception {
    	
		theMock.setupWalmartAgent();
		theMock.setupProduct(MONEY_GRAM_SEND_DOC_SEQ, "91");
		theMock.replayMocks();
		
		MoneyGramSendTransaction mgst = new MoneyGramSendTransaction("Foo",
				"Bar", MONEY_GRAM_SEND_DOC_SEQ, false, false);

		assertEquals("1-234-567-1234", mgst.formatPhoneNumber("12345671234"));
		assertEquals("123456712", mgst.formatPhoneNumber("123456712"));
		assertEquals("123456789012", mgst.formatPhoneNumber("123456789012"));

    }
    
    public void testGetDefaultCountry() throws Exception {
    	
		theMock.setupWalmartAgent();
		theMock.setupProduct(MONEY_GRAM_SEND_DOC_SEQ, "91");
		theMock.replayMocks();
		
		MoneyGramSendTransaction mgst = new MoneyGramSendTransaction("Foo",
				"Bar", MONEY_GRAM_SEND_DOC_SEQ, false, false);

		mgst.setDestination("");
		assertEquals("UNITED STATES", mgst.getDestination().getCountryName());		
		mgst.setDestination("PHL");		
		assertEquals("PHILIPPINES", mgst.getDestination().getCountryName());
		
    } 

    public void testSetExpectedRecieveAmt() throws Exception {
    	
		theMock.setupWalmartAgent();
		theMock.setupProduct(MONEY_GRAM_SEND_DOC_SEQ, "91");
		theMock.replayMocks();
		
		MoneyGramSendTransaction mgst = new MoneyGramSendTransaction("Foo",
				"Bar", MONEY_GRAM_SEND_DOC_SEQ, false, false);

		mgst.setExpectedRecieveAmt(null);		
		assertEquals(new BigDecimal("0"), mgst.getExpectedRecieveAmt());
		mgst.setExpectedRecieveAmt(new BigDecimal("100.00"));		
		assertEquals(new BigDecimal("100.00"), mgst.getExpectedRecieveAmt());		
		
    } 
    
    public void testGetTableRows() throws Exception {
		theMock.setupWalmartAgent();
		theMock.setupProduct(MONEY_GRAM_SEND_DOC_SEQ, "91");
		theMock.replayMocks();

		MoneyGramSendTransaction mgst = new MoneyGramSendTransaction("Foo",
				"Bar", MONEY_GRAM_SEND_DOC_SEQ, false, false);
		
		// each test depends upon given feeInfo array and country info
		TestCountryInfo cInfoSingleton = TestCountryInfo.getInstance();
		TestQdo qdoSingleton = TestQdo.getInstance();
		MGSHistoryPage page = new TestPage();
		FeeMsgMC[] feeArray = new FeeMsgMC[0]; // empty array used in test #1
		String[] tableRows;

		try {   // test #1 ==> empty fee table
			Country cInfo = TestCountryInfo.getInfo("Canada");
			List test1 = mgst.getTableRows(page, feeArray, cInfo);
			assertTrue(test1.isEmpty());

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		// test #2 ==> only 1 entry in feeTable (receive cash DO)
		feeArray = TestQdo.getFeesForSendOptions("Canada");
		try {
			Country cInfo = TestCountryInfo.getInfo("Canada");
			List test2 = mgst.getTableRows(page, feeArray, cInfo);
			assertTrue(test2.size() == 1);
			// note: tableRows[0] determination for a split option display
			tableRows = (String[]) test2.get(0);			
			assertFalse(new Boolean(tableRows[0]).booleanValue());
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		// test #3 ==> 2 entries in feeTable (1 cash DO, 1 account DO)
		feeArray = TestQdo.getFeesForSendOptions("Brazil");
		try {
			Country cInfo = TestCountryInfo.getInfo("Brazil");
			List test3 = mgst.getTableRows(page, feeArray, cInfo);
			assertTrue(test3.size() == 2);
			tableRows = (String[]) test3.get(0);
			// note: tableRows[1] label for display built from qdo.toString()
			assertFalse(new Boolean(tableRows[0]).booleanValue());
			assertTrue(tableRows[1].equals("10 Minute Service - BRL"));
			tableRows = (String[]) test3.get(1);
			assertTrue(new Boolean(tableRows[0]).booleanValue());
			assertEquals(tableRows[1], "Account Deposit - ALL BANKS - BRL");

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		// test #4 ==> 4 entries in feeTable (3 cash DO, 1 account DO)
		feeArray = TestQdo.getFeesForSendOptions("Poland");
		try {
			Country cInfo = TestCountryInfo.getInfo("Poland");
			List test4 = mgst.getTableRows(page, feeArray, cInfo);
			assertTrue(test4.size() == 4);
			tableRows = (String[]) test4.get(0);
			assertFalse(new Boolean(tableRows[0]).booleanValue());
			assertTrue(tableRows[1].equals("10 Minute Service - PLN"));
			tableRows = (String[]) test4.get(1);
			assertFalse(new Boolean(tableRows[0]).booleanValue());
			assertTrue(tableRows[1].equals("10 Minute Service - EUR"));
			tableRows = (String[]) test4.get(2);
			assertFalse(new Boolean(tableRows[0]).booleanValue());
			assertTrue(tableRows[1].equals("10 Minute Service - USD"));
			tableRows = (String[]) test4.get(3);
			assertTrue(new Boolean(tableRows[0]).booleanValue());
			assertTrue(tableRows[1].equals("Account Deposit - ALL BANKS - PLN"));

		} catch (Exception ex) {
			ex.printStackTrace();
		}

    }
    
}
