package dw.mgsend;

import dw.framework.*;
import dw.model.adapters.PersonalInfo;
import dw.pdfreceipt.PdfReceipt;

import java.awt.Component;
import java.awt.event.*;

public class MoneyGramSendWizardTest extends MoneyGramSendTransactionInterface {

	public MoneyGramSendWizardTest(MoneyGramSendTransaction transaction,
			PageNameInterface naming) {
		super("moneygramsend/MGSWizardPanel.xml", transaction, naming, true); //$NON-NLS-1$
		createPages();
	}

	public void createPages() {

		addPage(MGFQWizardPage1.class, transaction,
				"feeQueryEntry", "FQW05", buttons); //$NON-NLS-1$ //$NON-NLS-2$
		addPage(MGFQWizardPage2.class, transaction,
				"feeInformation", "FQW10", buttons); //$NON-NLS-1$ //$NON-NLS-2$        
		addPage(MGSWizardPageTest1.class, transaction,
				"cardNumberEntry", "MSW06", buttons); //$NON-NLS-1$ //$NON-NLS-2$
		addPage(MGSWizardPageTest3.class, transaction,
				"senderSelection", "MSW16", buttons); //$NON-NLS-1$ //$NON-NLS-2$
		addPage(MGSWizardPageTest4.class, transaction,
				"senderDetail", "MSW20", buttons); //$NON-NLS-1$ //$NON-NLS-2$
		addPage(MGSWizardPageTest5.class, transaction,
				"amountEntry", "MSW25", buttons); //$NON-NLS-1$ //$NON-NLS-2$
		addPage(MGSWizardPageTest5a.class, transaction,
				"msfeeInformation", "MSW10", buttons); //$NON-NLS-1$ //$NON-NLS-2$
		addPage(MGSWizardPage6.class, transaction,
				"identification", "MSW30", buttons); //$NON-NLS-1$ //$NON-NLS-2$
		addPage(MGSWizardPage8.class, transaction,
				"thirdParty", "MSW50", buttons); //$NON-NLS-1$ //$NON-NLS-2$
		addPage(MGSWizardPageTest9.class, transaction,
				"recipientDetail", "MSW60", buttons); //$NON-NLS-1$ //$NON-NLS-2$       
		addPage(MGSWizardPage9a.class, transaction,
				"directedSendDetail", "MSW61", buttons); //$NON-NLS-1$ //$NON-NLS-2$       
		addPage(MGSWizardPageTest13.class, transaction,
				"sendVerification", "MSW65", buttons); //$NON-NLS-1$ //$NON-NLS-2$
		addPage(MGSWizardPageTest14.class, transaction,
				"receiptPrinting", "MSW70", buttons); //$NON-NLS-1$ //$NON-NLS-2$
		addPage(MGSWizardPage15.class, transaction,
				"errorPage", "MSW75", buttons, true); //$NON-NLS-1$ //$NON-NLS-2$
		// page added for registration number lookup
		addPage(MGSWizardPage11a.class, transaction,
				"regLookUp", "MSW57", buttons); //$NON-NLS-1$ //$NON-NLS-2$
		addPage(MGSWizardPage11b.class, transaction,
				"regLookResp", "MSW62", buttons); //$NON-NLS-1$ //$NON-NLS-2$
		// page added for registration
		addPage(DSWizardPage2.class, transaction, 
				"regEnroll", "DSR10", buttons); //$NON-NLS-1$ //$NON-NLS-2$
		addPage(DSWizardPage3.class, transaction,
				"printRegistration", "DSR15", buttons); //$NON-NLS-1$ //$NON-NLS-2$
		// Page added for Rewards Enrollment
		addPage(MGEWizardPage1.class, transaction,
				"enrollment", "MEW10", buttons); //$NON-NLS-1$ //$NON-NLS-2$
		addPage(MGEWizardPage2.class, transaction,
				"idInformation", "MEW20", buttons); //$NON-NLS-1$ //$NON-NLS-2$
		addPage(MGEWizardPage3.class, transaction,
				"complianceInfo", "MEW30", buttons); //$NON-NLS-1$ //$NON-NLS-2$
		addPage(MGEWizardPage4.class, transaction,
				"consumerPrefConsent", "MEW40", buttons); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Add the single keystroke mappings to the buttons. Use the function keys
	 * for back, next, cancel, etc.
	 */
	private void addKeyMappings() {
		KeyMapManager.mapMethod(this, KeyEvent.VK_F2, 0, "f2SAR"); //$NON-NLS-1$
		buttons.addKeyMapping("expert", KeyEvent.VK_F4, 0); //$NON-NLS-1$
		buttons.addKeyMapping("back", KeyEvent.VK_F11, 0); //$NON-NLS-1$
		buttons.addKeyMapping("next", KeyEvent.VK_F12, 0); //$NON-NLS-1$
		buttons.addKeyMapping("cancel", KeyEvent.VK_ESCAPE, 0); //$NON-NLS-1$
	}

	public void start() {
		transaction.clear();
		if (transaction.isFeeQuery())
			super.start();
		else
			super.start(2);
		addKeyMappings();
	}

	/**
	 * Page traversal logic here.
	 */
	public void exit(int direction) {
		String currentPage = cardLayout.getVisiblePageName();
		String nextPage = null;

		if (direction == PageExitListener.DONOTHING) {
			return;
		}

		if (direction == PageExitListener.CANCELED) {
			transaction.escSAR();
			super.exit(direction);
			return;
		}

		if (direction == PageExitListener.COMPLETED) {
			transaction.f2NAG();
			super.exit(direction);
			return;
		}

		if (direction == PageExitListener.BACK) {
			/*
			 * For Directed Sends the msFeeInformation screen can show up
			 * multiple times in history, force the back to go back to the 
			 * amount entry screen instead of back through the RRN find
			 * screens.
			 */
			if (currentPage.equals("msfeeInformation")) {
				popHistoryUntil("amountEntry");
				return;
			/*
			 * If amount entry screen and edit is active, go back to the 
			 * senderDetail screen, so the sender info can be edited
			 */
			} else if (currentPage.equals("amountEntry") && transaction.isEditActive()){
				nextPage = "senderDetail"; //$NON-NLS-1$
				direction = BACK;
				showPage(nextPage, direction);
				return;
			/*
			 * Starting with 12.11 POEs needs to redo the feeLook to get a new
			 * session ID in order to be able to send a 2nd sendVerification.
			 */
			} else if (currentPage.equals("sendVerification")){
				popHistoryUntil("amountEntry");
				transaction.setEditActive(true);
				return;
			} else {
				super.exit(direction);
			}
			return;
		}

		if (direction == PageExitListener.EDIT_TRANSACTION) {
			nextPage = "senderDetail"; //$NON-NLS-1$
			direction = NEXT;
			transaction.setEditActive(true);
			showPage(nextPage, direction);
			return;
		}
		
		if (direction != PageExitListener.NEXT) {
			// default case
			super.exit(direction);
			return;
		}

		if (currentPage.equals("cardNumberEntry")) { //$NON-NLS-1$
			if (transaction.getFormFreeSendLookupInfo() != null) {
				nextPage = "sendVerification";
			} else if ((transaction.getCreateMoneySaver()
					&& (transaction.getCardPhoneNumber() == null || transaction
							.getCardPhoneNumber().length() == 0)) ||
							(transaction.getCreateMoneySaver() && transaction.newCustomer) ) {
				nextPage = "enrollment"; //$NON-NLS-1$
			} else if (transaction.getCustomers() == null
					|| transaction.getCustomers().length == 0
					|| transaction.getCustomers()[0].getSender().getLastName() == null
					|| transaction.getCustomers()[0].getSender().getLastName()
							.trim().length() == 0) {
				nextPage = "senderDetail"; //$NON-NLS-1$
			} else {
				nextPage = "senderSelection"; //$NON-NLS-1$
			}
		} else if (currentPage.equals("senderSelection")) {
			PersonalInfo sender = transaction.getSender();
			if (transaction.getCreateMoneySaver()
					&& (!(null != sender.getActiveFreqCustCardNumber() 
							&& !sender.getActiveFreqCustCardNumber().equals(""))
						|| !transaction.getMoneySaverID().equals(""))){
				nextPage = "enrollment";
			}else{
				/*
				 * If we have a sender name already, skip the 
				 * sender detail screen
				 */
				String senderName = sender.getFullName().trim();
				if ((senderName != null) && (senderName.length() > 0)
						&& !transaction.isEditActive()) {
					nextPage = "amountEntry";
				} else {
					nextPage = "senderDetail";
				}
			}
		} else if (currentPage.equals("enrollment")) { //$NON-NLS-1$
			if (transaction.isGovernmentIdNeeded()) {
				nextPage = "idInformation"; //$NON-NLS-1$
			} else if (transaction.isComplianceDataNeeded()) {
				nextPage = "complianceInfo"; //$NON-NLS-1$
			} else {
				nextPage = "consumerPrefConsent";
			}
		} else if (currentPage.equals("idInformation")) { //$NON-NLS-1$
			if (transaction.isComplianceDataNeeded()) {
				nextPage = "complianceInfo"; //$NON-NLS-1$
			} else {
				nextPage = "consumerPrefConsent";
			}
		} else if (currentPage.equals("complianceInfo")) { //$NON-NLS-1$
			nextPage = "consumerPrefConsent"; //$NON-NLS-1$
		} else if (currentPage.equals("consumerPrefConsent")) { //$NON-NLS-1$
			nextPage = "senderDetail"; //$NON-NLS-1$
		} else if (currentPage.equals("amountEntry")) { //$NON-NLS-1$
			nextPage = "msfeeInformation"; //$NON-NLS-1$

		} else if (currentPage.equals("msfeeInformation")) { //$NON-NLS-1$
			if (transaction.isFindClicked())
				nextPage = "regLookUp";
			else if (transaction.isRegisterClicked())
				nextPage = "regEnroll";
			else if (transaction.isComplianceOptional())
				nextPage = "identification"; //$NON-NLS-1$
			else if (transaction.isThirdPartyNeeded())
				nextPage = "thirdParty";
			else {
				if (transaction.isInlineDSR() && transaction.isDSS()) {
					/*
					 * If a RRN is available, go straight to confirmation screen
					 */
					if (transaction.getRegistrationNumber().length() > 0) {
						nextPage = "sendVerification"; //$NON-NLS-1$
					}else {
						nextPage = "directedSendDetail"; //$NON-NLS-1$
					}
				} else {
					nextPage = nextPageRecipientDetail();
				}
			}
		} else if (currentPage.equals("directedSendDetail")) { //$NON-NLS-1$
			if (transaction.isFindClicked())
				nextPage = "regLookUp";
			else {
				nextPage = "sendVerification"; //$NON-NLS-1$
			}
		} else if (currentPage.equals("regLookResp")) { //$NON-NLS-1$
			if (transaction.isInlineDSR()) {
				nextPage = "sendVerification"; //$NON-NLS-1$
			} else {
				nextPage = "msfeeInformation"; //$NON-NLS-1$
			}
		} else if (currentPage.equals("identification")) { //$NON-NLS-1$
			if (transaction.isThirdPartyNeeded()) {
				nextPage = "thirdParty";
			}
			else {
				if (transaction.isInlineDSR() && transaction.isDSS()) {
					/*
					 * If a RRN is available, go straight to confirmation screen
					 */
					if (transaction.getRegistrationNumber().length() > 0) {
						nextPage = "sendVerification"; //$NON-NLS-1$
					}else {
						nextPage = "directedSendDetail"; //$NON-NLS-1$
					}
				} else {
					nextPage = nextPageRecipientDetail();
				}
			}
		} else if (currentPage.equals("thirdParty")) { //$NON-NLS-1$
			if (transaction.isInlineDSR() && transaction.isDSS()) {
				/*
				 * If a RRN is available, go straight to confirmation screen
				 */
				if (transaction.getRegistrationNumber().length() > 0) {
					nextPage = "sendVerification"; //$NON-NLS-1$
				}else {
					nextPage = "directedSendDetail"; //$NON-NLS-1$
				}
			} else {
				nextPage = nextPageRecipientDetail();
			}
		} else if (currentPage.equals("recipientDetail")) { //$NON-NLS-1$       	
			nextPage = "sendVerification"; //$NON-NLS-1$
		} else if (currentPage.equals("regEnroll")) {

			nextPage = "printRegistration";
		} else if (currentPage.equals("printRegistration")) {
			nextPage = "recipientDetail";
		} else if (currentPage.equals("sendVerification")) { //$NON-NLS-1$
			if (transaction.getStatus() == ClientTransaction.FAILED)
				nextPage = "errorPage"; //$NON-NLS-1$
			if (transaction.getStatus() == ClientTransaction.CANCELED)
				nextPage = "sendVerification"; //$NON-NLS-1$
		}
		if (nextPage != null) {
			showPage(nextPage, direction);
			return;
		} else {
			super.exit(direction);
		}
	}

	public void f2SAR() {
		Component current = cardLayout.getVisiblePage();
		if (current instanceof FlowPage) {
			((FlowPage) current).updateTransaction();
		}
		transaction.f2SAR();
	}
	
	private String nextPageRecipientDetail() {
		String nextPage;
		/*
		 * If we have a receiver name already, skip the 
		 * recipient detail screen
		 */
		String receiverName = transaction.getReceiver().getFullName().trim();
		if ((receiverName != null) && (receiverName.length() > 0)
				&& !transaction.isEditActive()) {
			nextPage = "sendVerification"; //$NON-NLS-1$
		}
		else {
			nextPage = "recipientDetail"; //$NON-NLS-1$
		}
		return nextPage;
	}
}
