package dw.mgsend;

import dw.comm.AgentConnect;
import dw.comm.MessageFacade;
import dw.comm.MessageFacadeParam;
import dw.framework.AbstractDeltaWorksTest;
import dw.framework.CommCompleteInterface;
import dw.framework.TestUtility;
import dw.io.StateCommand;
import dw.main.DeltaworksMainPanel;
import dw.main.DeltaworksStartup;
import dw.model.Element;
import dw.model.adapters.FeeLookupMsg;
import dw.model.adapters.FeeMsgMC;
import dw.model.adapters.MoneyGramSendInfo40;
import dw.utility.CodeTables;
import dw.utility.DWValues;
import dw.utility.Debug;
import flags.CompileTimeFlags;

public class MgSendTest extends AbstractDeltaWorksTest implements
		CommCompleteInterface {
	private static MgSendTest mgSendTest = new MgSendTest();
	DeltaworksMainPanel deltaworksMainPanel;
	static {
		try {
			AgentConnect.enableDebugEncryption();
			Debug.setDebug(true);
			CompileTimeFlags.setTest(true);
			if ("create".equalsIgnoreCase(TestUtility.getData("MgSendTest.createProfile"))) {
				StateCommand.warehouse("warehouse_junit.xml");
			}
			mgSendTest.loadProfile(
					TestUtility.getData("MgSendTest.environment"),
					TestUtility.getData("MgSendTest.deviceId"),
					TestUtility.getData("MgSendTest.profileId"),
					TestUtility.getData("MgSendTest.password"));
			mgSendTest.loadMainPanel();
			mgSendTest.transmit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	public void loadMainPanel() throws Exception {
		//Debug.debugTest("loadMainPanel");
		/*
		 * Thread t = new Thread(new Runnable() { public void run() { try {
		 * deltaworksMainPanel = startDeltaWorks();
		 * //Debug.debugTest("Mainpanel created"); } catch (Exception e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); } } }); t.start();
		 */
		deltaworksMainPanel = startDeltaWorks();
	}

	public void transmit() throws Exception {
		//Debug.debugTest("About to transmit");
		deltaworksMainPanel.transmitAll(true);
	}

	public static void testEnhancedTransactionHistory() throws Exception {

		TestUtility.startTestLog("EnhancedTransactionHistory");

		try {

			final MessageFacadeParam parameters = new MessageFacadeParam(
					MessageFacadeParam.REAL_TIME,
					MessageFacadeParam.MG_CUSTOMER_NOT_FOUND,
					MessageFacadeParam.MG_CARD_NOT_FOUND);
			String number = "952541416555";
			int lookupType = DWValues.LOOKUP_PHONE;
			boolean isbillPayment = false;
			MoneyGramSendInfo40[] customers = MessageFacade.getInstance()
					.getEnhancedTransactionHistory(null, parameters, number,
							lookupType, isbillPayment);
			
			assertNotNull(customers);
		} finally {
			TestUtility.endTestLog("EnhancedTransactionHistory");
		}

	}

	public void testFeeCalculation() throws Exception {

		TestUtility.startTestLog("FeeCalculation");
		try {
			FeeMsgMC feeMsg = new FeeMsgMC();

			feeMsg.setSendCurrency("USD");
			feeMsg.setAmountExcludingFee("120.00");
			feeMsg.setDestinationCountry("USA");
			feeMsg.setDestinationState("MN");
			feeMsg.setReceiveCurrency("USD");

			feeMsg.setDeliveryOption("");
			feeMsg.setReceiveCurrency("");
			feeMsg.setAllOptions(true);
			feeMsg.setProductType(DWValues.PROD_TYPE_SEND);
			feeMsg.setReceiveAgentID("");
			feeMsg.setMgCustomerReceiveNumber("");
			// feeMsg.setEnteredPromoCodes("");

			feeMsg.setIssueFreqCustCard(false);
			final MessageFacadeParam parameters = new MessageFacadeParam(
					MessageFacadeParam.REAL_TIME);

			FeeLookupMsg feeMsgResp = MessageFacade.getInstance()
					.getFeeCalculation(feeMsg, parameters);
			//Debug.debugTest("testFeeCalculation::data::"+ feeMsgResp.getFees().length);
			assertNotSame(new Integer(0), new Integer(
					feeMsgResp.getFees().length));
		} finally {
			TestUtility.endTestLog("testFeeCalculation");
		}
	}

//<<<<<<< MgSendTest.java
////	public void testFieldsForProduct() throws Exception {
////		TestUtility.startTestLog("FieldsForProduct");
////		try {
////			final MessageFacadeParam parameters = new MessageFacadeParam(
////					MessageFacadeParam.REAL_TIME);
////
////			Element response = MessageFacade.getInstance().getFieldsForProduct(
////					this, parameters, "USA", "NONE", "", "", "USD", "120",
////					"USD", DWValues.PROD_TYPE_SEND, "", "", "WILL_CALL", "0");
////
////			FieldsForProductInfo[] fieldsForProduct = new FieldsForProductInfo[0];
////			DynamicFieldInfo[] dynamicFieldInfo = new DynamicFieldInfo[0];
////			if (response != null) {
////				fieldsForProduct = (FieldsForProductInfo[]) response.getAll(
////						"productFieldInfo").toArray(fieldsForProduct);
////				dynamicFieldInfo = (DynamicFieldInfo[]) response.getAll(
////						"dynamicFieldInfo").toArray(dynamicFieldInfo);
////
////			}
////			Debug.debugTest("testFieldsForProduct::data:fieldsForProduct:"
////					+ fieldsForProduct.length);
////			Debug.debugTest("testFieldsForProduct::data:dynamicFieldInfo:"
////					+ fieldsForProduct.length);
////			assertNotSame(new Integer(0), new Integer(fieldsForProduct.length));
////			assertNotSame(new Integer(0), new Integer(dynamicFieldInfo.length));
////		} finally {
////			TestUtility.endTestLog("FieldsForProduct");
////		}
////
////	}
//=======
//	public void testFieldsForProduct() throws Exception {
//		TestUtility.startTestLog("FieldsForProduct");
//		try {
//			final MessageFacadeParam parameters = new MessageFacadeParam(
//					MessageFacadeParam.REAL_TIME);
//
//			Element response = MessageFacade.getInstance().getFieldsForProduct(
//					this, parameters, "USA", "NONE", "", "", "USD", "120",
//					"USD", DWValues.PROD_TYPE_SEND, "", "", "WILL_CALL", "0");
//
//			FieldsForProductInfo[] fieldsForProduct = new FieldsForProductInfo[0];
//			DynamicFieldInfo[] dynamicFieldInfo = new DynamicFieldInfo[0];
//			if (response != null) {
//				fieldsForProduct = (FieldsForProductInfo[]) response.getAll(
//						"productFieldInfo").toArray(fieldsForProduct);
//				dynamicFieldInfo = (DynamicFieldInfo[]) response.getAll(
//						"dynamicFieldInfo").toArray(dynamicFieldInfo);
//
//			}
//			assertNotSame(new Integer(0), new Integer(fieldsForProduct.length));
//			assertNotSame(new Integer(0), new Integer(dynamicFieldInfo.length));
//		} finally {
//			TestUtility.endTestLog("FieldsForProduct");
//		}
//
//	}
//>>>>>>> 1.2

	public void commComplete(int commTag, Object returnValue) {
		// TODO Auto-generated method stub

	}

	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
		CompileTimeFlags.setTest(false);
	}

}
