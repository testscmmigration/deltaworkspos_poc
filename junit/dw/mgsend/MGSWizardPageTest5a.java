package dw.mgsend;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import dw.dialogs.Dialogs;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.framework.TestUtility;
import dw.i18n.Messages;
import dw.i18n.Resources;
import dw.model.adapters.CompanyInfo;
import dw.model.adapters.MoneyGramSendInfo40;
import dw.model.adapters.QualifiedDeliveryOption;
import dw.printing.ReceiptReport;
import dw.utility.CodeTables;
import dw.utility.Debug;

/**
 * "senderSelection" Page 3 of the MoneyGram Send Wizard. This page displays a
 * table of customers that match the home phone number given. The user may
 * choose zero or one of the customers listed in the table and continue.
 */
public class MGSWizardPageTest5a extends MGSWizardPage5a {

	public MGSWizardPageTest5a(MoneyGramSendTransaction tran, String name,
			String pageCode, PageFlowButtons buttons) {
		super(tran, name, pageCode, buttons); //$NON-NLS-1$
	}

	public void start(int direction) {
		if (direction == PageExitListener.NEXT) {
			super.start(direction);
			if("testMGSend1".equals(TestUtility.getCurrentProcess()))
			{
				testMGSend1();
			}else if("testMGSend2".equals(TestUtility.getCurrentProcess()))
			{
				testMGSend1();
			}
			flowButtons.getButton("next").doClick();
		}else
		{
			//Debug.debugTest("MGSWizardPageTest5a ::  TEST ERROR");
			flowButtons.getButton("cancel").setEnabled(true);
		}
	}

	private void testMGSend1() {
		String rownum = TestUtility.getData("testSend1.depositoption.rownum");
		if(rownum !=null)
		{
			int rownumval=Integer.parseInt(rownum);
			table1.setRowSelectionInterval(rownumval, rownumval);
		}else {
			table1.setRowSelectionInterval(0, 0);
		}
	}
	
	public void commComplete(int commTag, Object returnValue) {
		super.commComplete(commTag, returnValue);
		//Debug.debugTest("End of Page(MGSWizardPageTest5a :: MSW10)");
		if(returnValue instanceof Boolean && ((Boolean)returnValue).equals(new Boolean(false)))
		{
			//Debug.debugTest("Error on Page(MGSWizardPageTest5a :: MSW10)");
			flowButtons.getButton("cancel").doClick();
			TestUtility.unLock();
		}
	}
}
