package dw.mgsend;

import java.util.Random;

import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.TestUtility;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.CountryInfo.Country;
import dw.model.adapters.CountryInfo.CountrySubdivision;
//import dw.model.adapters.StateProvinceInfo;
import dw.utility.Debug;

/**
 * "senderSelection" Page 3 of the MoneyGram Send Wizard. This page displays a
 * table of customers that match the home phone number given. The user may
 * choose zero or one of the customers listed in the table and continue.
 */
public class MGSWizardPageTest5 extends MGSWizardPage5 {

	public MGSWizardPageTest5(MoneyGramSendTransaction tran, String name,
			String pageCode, PageFlowButtons buttons) {
		super(tran, name, pageCode, buttons); //$NON-NLS-1$
	}

	public void start(int direction) {
		if (direction == PageExitListener.NEXT) {
			super.start(direction);
			if("testMGSend1".equals(TestUtility.getCurrentProcess()))
			{
				testMGSend1();
			}else if("testMGSend2".equals(TestUtility.getCurrentProcess()))
			{
				testMGSend2();
			}
			
			flowButtons.getButton("next").doClick();
		}else
		{
			//Debug.debugTest("MGSWizardPageTest4 ::  TEST ERROR");
			flowButtons.getButton("cancel").setEnabled(true);
		}
	}

	private void testMGSend1() {
		int low = 10;
		int high = 100;
		
		String lowStr = TestUtility.getData("testSend1.amt.lowval");
		String highStr =TestUtility.getData("testSend1.amt.highval");
		if(lowStr!=null && highStr!=null)
		{
			low =  Integer.parseInt(lowStr);
			high =  Integer.parseInt(highStr);
		}
		amountField.setText(TestUtility.getRandomAmt(low,high)+".00");
		
		String newCountry = ((Country) destinationComboBox.getSelectedItem()).getCountryCode();

		
		for (int ii = 1; ii < stateBox.getItemCount(); ii++ ) {
			CountrySubdivision spiListItem = (CountrySubdivision)stateBox.getItemAt(ii);
			if (spiListItem.getCountrySubdivisionCode().equals("MN")) {
				stateBox.setSelectedItem(spiListItem);
				break;
			}
		}
	}
	
	private void testMGSend2() {
		int low = 10;
		int high = 100;
		String lowStr = TestUtility.getData("testSend1.amt.lowval");
		String highStr =TestUtility.getData("testSend1.amt.highval");
		if(lowStr!=null && highStr!=null)
		{
			low =  Integer.parseInt(lowStr);
			high =  Integer.parseInt(highStr);
		}
		amountField.setText(TestUtility.getRandomAmt(low,high)+".00");


		String newCountry = ((Country) destinationComboBox.getSelectedItem()).getCountryCode();

		
		for (int ii = 1; ii < stateBox.getItemCount(); ii++ ) {
			CountrySubdivision spiListItem = (CountrySubdivision)stateBox.getItemAt(ii);
			if (spiListItem.getCountrySubdivisionCode().equals("AK")) {
				stateBox.setSelectedItem(spiListItem);
				break;
			}
		}
	}
	public void commComplete(int commTag, Object returnValue) {
		super.commComplete(commTag, returnValue);
		//Debug.debugTest("End of Page(MGSWizardPageTest5 :: MSW25)");
		if(returnValue instanceof Boolean && ((Boolean)returnValue).equals(new Boolean(false)))
		{
			//Debug.debugTest("Error on Page(MGSWizardPageTest5 :: MSW25)");
			flowButtons.getButton("cancel").doClick();
			TestUtility.unLock();
		}
	}
	
	
	
	
}
