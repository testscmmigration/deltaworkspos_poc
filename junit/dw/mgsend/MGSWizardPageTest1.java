/*
 * MGFQWizardPage1.java
 * 
 * $Revision$
 *
 * Copyright (c) 2002-2005 MoneyGram International
 */

package dw.mgsend;

import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import dw.dwgui.DelimitedTextField;
import dw.dwgui.MoneyField;
import dw.dwgui.MultiListComboBox;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.framework.TestUtility;
import dw.i18n.FormatSymbols;
import dw.i18n.Messages;
import dw.model.Element;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.QualifiedDeliveryOption;
//import dw.model.adapters.StateProvinceInfo;
import dw.utility.AgentCountryInfo;
import dw.utility.CodeTables;
import dw.utility.DWValues;
import dw.utility.Debug;

public class MGSWizardPageTest1 extends MGSWizardPage1 {

	public MGSWizardPageTest1(MoneyGramSendTransaction tran, String name,
			String pageCode, PageFlowButtons buttons) {
		super(tran, name, pageCode, buttons);
	}

	public void start(int direction) {
		super.start(direction);
		try {			
			if (direction == PageExitListener.NEXT) {
				if("testMGSend1".equals(TestUtility.getCurrentProcess()))
				{
					testMGSend1();
				}else if("testMGSend2".equals(TestUtility.getCurrentProcess()))
				{
					testMGSend2();
				}
				flowButtons.getButton("next").doClick();
			}else
			{
				flowButtons.getButton("cancel").setEnabled(true);
			}
		} catch (Exception e) {
			if("testMGSend1".equals(TestUtility.getCurrentProcess()))
			{
				// Unable to find customer details with phone number
				flowButtons.getButton("cancel").doClick();
				TestUtility.unLock();
			}
			
		}
	}

	private void testMGSend1() {
//		//Debug.debugTest("Data -phonenumber-  "+TestUtility.getData("testSend1.phonenumber1"),"testMGSend1");		
		phoneNumberField.setText(TestUtility.getData("testSend1.phonenumber1"));
	}
	
	private void testMGSend2() {
//		//Debug.debugTest("Data -phonenumber-  "+TestUtility.getData("testSend1.phonenumber2"),"testMGSend2");
		phoneNumberField.setText(TestUtility.getData("testSend1.phonenumber1"));
	}
	public void commComplete(int commTag, Object returnValue) {
		super.commComplete(commTag, returnValue);
//		//Debug.debugTest("End of Page(MGSWizardPageTest1 :: MSW06)");
		if(returnValue instanceof Boolean && ((Boolean)returnValue).equals(new Boolean(false)))
		{
//			//Debug.debugTest("Error on Page(MGSWizardPageTest1 :: MSW06)");
			flowButtons.getButton("cancel").doClick();
			TestUtility.unLock();
		}
	}
	

}
