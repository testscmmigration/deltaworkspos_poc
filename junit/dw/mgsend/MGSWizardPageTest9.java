package dw.mgsend;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import dw.dialogs.Dialogs;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.framework.TestUtility;
import dw.i18n.Messages;
import dw.i18n.Resources;
import dw.model.adapters.CompanyInfo;
import dw.model.adapters.MoneyGramSendInfo40;
import dw.model.adapters.QualifiedDeliveryOption;
import dw.printing.ReceiptReport;
import dw.utility.CodeTables;
import dw.utility.Debug;

/**
 * "senderSelection" Page 3 of the MoneyGram Send Wizard. This page displays a
 * table of customers that match the home phone number given. The user may
 * choose zero or one of the customers listed in the table and continue.
 */
public class MGSWizardPageTest9 extends MGSWizardPage9 {

	public MGSWizardPageTest9(MoneyGramSendTransaction tran, String name,
			String pageCode, PageFlowButtons buttons) {
		super(tran, name, pageCode, buttons); //$NON-NLS-1$
	}

	public void start(int direction) {
		if (direction == PageExitListener.NEXT) {
			super.start(direction);
			flowButtons.getButton("next").doClick();
		}else
		{
			//Debug.debugTest("MGSWizardPageTest9 ::  TEST ERROR");
			flowButtons.getButton("cancel").setEnabled(true);
		}
	}
	public void commComplete(int commTag, Object returnValue) {
		super.commComplete(commTag, returnValue);
		//Debug.debugTest("End of Page(MGSWizardPageTest9 :: MSW60)");
		if(returnValue instanceof Boolean && ((Boolean)returnValue).equals(new Boolean(false)))
		{
			//Debug.debugTest("Error on Page(MGSWizardPageTest9 :: MSW60)");
			flowButtons.getButton("cancel").doClick();
			TestUtility.unLock();
		}
	}
}
