package dw.mgsend;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.swing.SwingUtilities;

import junit.framework.TestCase;
import dw.framework.TestUtility;
import dw.main.DeltaworksStartup;
import dw.utility.Debug;
import flags.CompileTimeFlags;

public class MGAutoSendTest extends TestCase {

	
	
	protected static void doMockSetup() throws Exception {
		DeltaworksStartup.initApp(new String[] { TestUtility.getData("MGAutoSendTest.cmd.args")});
	}


	public static void main(String[] args) {
		try {

			CompileTimeFlags.setTest(true);
			doMockSetup();
			testMGSend1();
			testMGSend2();
			
			System.exit(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void testMGSend1() throws Exception {
		TestUtility.startTestLog("testMGSend1");
		TestUtility.setCurrentProcess("testMGSend1");
		
//		//Debug.debugTest("*Start of testMGSend1*");
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				DeltaworksStartup.getInstance().doClickMenu("sendMoneyButton");
			}
		});
		TestUtility.lock();
	}
	
	public static void testMGSend2() throws Exception {
		TestUtility.startTestLog("testMGSend2");
//		//Debug.debugTest("*Start of testMGSend2*");
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				TestUtility.setCurrentProcess("testMGSend2");
				DeltaworksStartup.getInstance().doClickMenu("sendMoneyButton");
			}
		});
		TestUtility.lock();
		
	}
	
}
