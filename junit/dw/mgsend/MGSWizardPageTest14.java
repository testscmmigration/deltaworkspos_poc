package dw.mgsend;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import dw.dialogs.Dialogs;
import dw.framework.PageExitListener;
import dw.framework.PageFlowButtons;
import dw.framework.PageNotification;
import dw.framework.TestUtility;
import dw.i18n.Messages;
import dw.i18n.Resources;
import dw.model.adapters.CompanyInfo;
import dw.model.adapters.MoneyGramSendInfo40;
import dw.model.adapters.QualifiedDeliveryOption;
import dw.printing.ReceiptReport;
import dw.utility.CodeTables;
import dw.utility.Debug;

/**
 * "senderSelection" Page 3 of the MoneyGram Send Wizard. This page displays a
 * table of customers that match the home phone number given. The user may
 * choose zero or one of the customers listed in the table and continue.
 */
public class MGSWizardPageTest14 extends MGSWizardPage14 {

	public MGSWizardPageTest14(MoneyGramSendTransaction tran, String name,
			String pageCode, PageFlowButtons buttons) {
		super(tran, name, pageCode, buttons); //$NON-NLS-1$
	}

	public void start(int direction) {
		if (direction == PageExitListener.NEXT) {
			super.start(direction);
			flowButtons.getButton("cancel").doClick();
			TestUtility.unLock();			
			if("testMGSend1".equals(TestUtility.getCurrentProcess()))
			{
				TestUtility.endTestLog("testMGSend1");
			}else if("testMGSend2".equals(TestUtility.getCurrentProcess()))
			{
				TestUtility.endTestLog("testMGSend2");
			}
		}else
		{
			//Debug.debugTest("MGSWizardPageTest14 ::  TEST ERROR");
			flowButtons.getButton("cancel").setEnabled(true);
			flowButtons.getButton("cancel").doClick();
			TestUtility.unLock();
		}
	}
	public void commComplete(int commTag, Object returnValue) {
		super.commComplete(commTag, returnValue);
		//Debug.debugTest("End of Page(MGSWizardPageTest14 :: MSW70)");
		if(returnValue instanceof Boolean && ((Boolean)returnValue).equals(new Boolean(false)))
		{
			//Debug.debugTest("Error on Page(MGSWizardPageTest14 :: MSW70)");
			flowButtons.getButton("cancel").doClick();
			TestUtility.unLock();
		}
		
	
	}
}
