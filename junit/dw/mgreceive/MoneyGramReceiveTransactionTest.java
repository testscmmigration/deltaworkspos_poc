/*
 * Created on Jul 26, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package dw.mgreceive;

import junit.framework.TestCase;
import dw.UnitProfileMockHelper;
import dw.framework.ClientTransaction;
import dw.i18n.Resources;


/**
 * @author A121
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MoneyGramReceiveTransactionTest extends TestCase {
	UnitProfileMockHelper theMock;
	
	
	private int MONEY_GRAM_RECEIVE_DOC_SEQ = 4;
	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		doMockSetup();
	}
	
	protected void doMockSetup() throws Exception {
		theMock = new UnitProfileMockHelper();
		theMock.setup();
	}
	
	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
		theMock.teardown();
	}
	
	public void test() throws Exception {
		theMock.setupWalmartAgent();
		theMock.setupProduct(MONEY_GRAM_RECEIVE_DOC_SEQ, "4");
		//theMock.setupDispenser(1);
		theMock.replayMocks();
		
		assertEquals("60", "60");

	}
	
	public void testGetPayoutScenario() throws Exception {
		theMock.setupWalmartAgent();
		theMock.setupProduct(MONEY_GRAM_RECEIVE_DOC_SEQ, "4");
		theMock.setupDispenser(1);
		theMock.replayMocks();
		
		MoneyGramReceiveTransaction mgrt = 
			new MoneyGramReceiveTransaction(Resources.getString("DeltaworksMainPanel.MoneyGram_Receive_114"), 
				                            ClientTransaction.RECEIPT_PREFIX_RECEIVE, 
				                            MONEY_GRAM_RECEIVE_DOC_SEQ);

		//mgrt.setAgentAmount(new BigDecimal(10));
		//mgrt.setCustomerAmount(new BigDecimal(60));
		
		//System.err.println("retvalue = " + mgrt.getPayoutScenario());
		//assertEquals("CU", mgrt.getPayoutScenario());
	}

    /**
     * There are 4 possible scenarios :
     *   AG_CU : Cash to customer (MO to agent), MO to customer
     *   CU_CU : 2 money orders to customer.
     *   AG : Cash only to customer (MO to agent)
     *   CU : 1 money order to customer.
     * The scenario is based on the selected payout in the transaction.
     */
/*    public String getPayoutScenario() {
        BigDecimal amount1 = getAgentAmount();
        BigDecimal amount2 = getCustomerAmount();

        if (amount1.compareTo(ZERO) == 0)
            return "CU";	//$NON-NLS-1$

        if (amount2.compareTo(ZERO) == 0)
            if (amount1Cash)
                return "AG";	//$NON-NLS-1$
            else
                return "CU";	//$NON-NLS-1$

        if (amount1Cash)
             return "AG_CU";	//$NON-NLS-1$
        else
             return "CU_CU";	//$NON-NLS-1$
  }
    
*/
    
}
