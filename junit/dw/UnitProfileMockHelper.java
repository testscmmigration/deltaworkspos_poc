package dw;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;

import org.easymock.MockControl;
import org.easymock.classextension.MockClassControl;

import dw.profile.Profile;
import dw.profile.ProfileItem;
import dw.profile.UnitProfile;
import dw.profile.UnitProfileInterface;

public class UnitProfileMockHelper {
	private UnitProfileInterface upMock;

	private MockControl upMockControl;

	/** This is the top-level mock */
	private Profile profMock;

	private MockControl profMockControl;

	/** This holds product mocks */
	private HashMap productMocks = new HashMap(19);
	private HashMap dispenserMocks = new HashMap(19);

	private class ProfileMockSet {
		Profile productMock;
		Profile dispenserMock;

		MockControl productMockControl;
		MockControl dispenserMockControl;

		public ProfileMockSet() {
			super();
		}
	}

/*	*//** This holds dispenser mocks *//*

	private class DispenserMockSet {
		Profile dispenserMock;

		MockControl dispenserMockControl;

		public DispenserMockSet() {
			super();
		}
	}
*/
	public void setup() {
		upMockControl = MockControl
				.createNiceControl(UnitProfileInterface.class);
		upMock = (UnitProfileInterface) upMockControl.getMock();

		UnitProfile.setMOCK(upMock);

		profMockControl = MockClassControl.createNiceControl(Profile.class,
				new Class[] { String.class }, new Object[] { "" });
		profMock = (Profile) profMockControl.getMock();

		setupStdProfile();
	}

	public void teardown() {
		UnitProfile.setMOCK(null);
	}

	public void replayMocks() throws Exception {
		upMockControl.replay();
		profMockControl.replay();

		Iterator i = productMocks.values().iterator();
		while (i.hasNext()) {
			ProfileMockSet pms = (ProfileMockSet) i.next();
			pms.productMockControl.replay();
		}
		
		Iterator j = dispenserMocks.values().iterator();
		while (j.hasNext()) {
			ProfileMockSet pms = (ProfileMockSet) j.next();
			pms.dispenserMockControl.replay();
		}
	}

	protected ProfileItem createPI(String name, String value) {
		ProfileItem pi = new ProfileItem(name);
		pi.setValue(value);
		return pi;
	}

	public void setupProduct(int productId, String productType) {
		ProfileMockSet pms = new ProfileMockSet();
		MockControl productMockCntrl = pms.productMockControl = MockClassControl
				.createNiceControl(Profile.class, new Class[] { String.class },
						new Object[] { "PRODUCT" });
		Profile productMock = pms.productMock = (Profile) productMockCntrl.getMock();
		productMocks.put(new Integer(productId), pms);

		profMock.find("PRODUCT", productId);
		profMockControl.setReturnValue(productMock, MockControl.ZERO_OR_MORE);

		productMock.getID();
		productMockCntrl.setReturnValue(productId);

		productMock.find("PRODUCT_TYPE");
		productMockCntrl.setReturnValue(createPI("PRODUCT_TYPE", productType),
				MockControl.ZERO_OR_MORE);
		
        switch (productId) {
    		case 1 :  // MONEY ORDER
        		// Setup Max Batch Amount
        		productMock.find("MAX_BATCH_AMOUNT");
        		productMockCntrl.setReturnValue(createPI("MAX_BATCH_AMOUNT",
        				"9999.99"), MockControl.ZERO_OR_MORE);

        		// Setup Max Batch Count
        		productMock.find("MAX_BATCH_COUNT");
        		productMockCntrl.setReturnValue(createPI("MAX_BATCH_COUNT",
        				"1"), MockControl.ZERO_OR_MORE);

        		// Setup Logo Option
	    		productMock.find("LOGO_OPTION");
	    		productMockCntrl.setReturnValue(createPI("LOGO_OPTION",
					"0"), MockControl.ZERO_OR_MORE);
	
	    		// Setup No Fee Option
	    		productMock.find("NO_FEE_OPTION");
	    		productMockCntrl.setReturnValue(createPI("NO_FEE_OPTION",
					"Y"), MockControl.ZERO_OR_MORE);
	
	    		// Setup Xmit Batch Amount
	    		productMock.find("XMIT_BATCH_AMOUNT");
	    		productMockCntrl.setReturnValue(createPI("XMIT_BATCH_AMOUNT",
					"79999"), MockControl.ZERO_OR_MORE);

	    		// Setup Max Amount Per Item
	    		productMock.find("MAX_AMOUNT_PER_ITEM");
	    		productMockCntrl.setReturnValue(createPI("MAX_AMOUNT_PER_ITEM",
					"99999"), MockControl.ZERO_OR_MORE);
	
	    		// Setup Dispenser Number
	    		productMock.find("DISPENSER_NUMBER");
	    		productMockCntrl.setReturnValue(createPI("DISPENSER_NUMBER",
					"1"), MockControl.ZERO_OR_MORE);
	 		
	    		// Setup Fraud Limit Photo Id
	    		productMock.find("FRAUD_LIMIT_PHOTO_ID");
	    		productMockCntrl.setReturnValue(createPI("FRAUD_LIMIT_PHOTO_ID",
					"99999"), MockControl.ZERO_OR_MORE);
	  		
	    		// Setup Xmit Batch Count
	    		productMock.find("XMIT_BATCH_COUNT");
	    		productMockCntrl.setReturnValue(createPI("XMIT_BATCH_COUNT",
					"799"), MockControl.ZERO_OR_MORE);

	    		// Setup Document Sequence Number
	    		productMock.find("DOC_SEQ_NUM");
	    		productMockCntrl.setReturnValue(createPI("DOC_SEQ_NUM",
					"1"), MockControl.ZERO_OR_MORE);
	  		
	    		// Setup Name Pin Required
	    		productMock.find("NAME_PIN_REQUIRED");
	    		productMockCntrl.setReturnValue(createPI("NAME_PIN_REQUIRED",
					"P"), MockControl.ZERO_OR_MORE);
	      		
	    		// Setup Print Purchaser
	    		productMock.find("PRINT_PURCHASER");
	    		productMockCntrl.setReturnValue(createPI("PRINT_PURCHASER",
					"N"), MockControl.ZERO_OR_MORE);
	      		
	    		// Setup Document Name
	    		productMock.find("DOCUMENT_NAME");
	    		productMockCntrl.setReturnValue(createPI("DOCUMENT_NAME",
					"MONEY ORDER"), MockControl.ZERO_OR_MORE);
	      		
	    		// Setup Keyboard Issue
	    		productMock.find("KEYBOARD_ISSUE");
	    		productMockCntrl.setReturnValue(createPI("KEYBOARD_ISSUE",
					"Y"), MockControl.ZERO_OR_MORE);
	      		
	    		// Setup Enter Payee
	    		productMock.find("ENTER_PAYEE");
	    		productMockCntrl.setReturnValue(createPI("ENTER_PAYEE",
					"N"), MockControl.ZERO_OR_MORE);
	  		
	    		// Setup Agg Enabled
	    		productMock.find("AGG_ENABLE");
	    		productMockCntrl.setReturnValue(createPI("AGG_ENABLE",
					"N"), MockControl.ZERO_OR_MORE);
	  		
	    		// Setup Amount Tendered
	    		productMock.find("AMOUNT_TENDERED");
	    		productMockCntrl.setReturnValue(createPI("AMOUNT_TENDERED",
					"Y"), MockControl.ZERO_OR_MORE);
	  		
	    		// Setup Document Number
	    		productMock.find("DOCUMENT_NUMBER");
	    		productMockCntrl.setReturnValue(createPI("DOCUMENT_NUMBER",
					"1"), MockControl.ZERO_OR_MORE);
  		
	    		break;
	    		
    		case 2 :  // VENDOR PAYMENT
        		// Setup Max Batch Amount
        		productMock.find("MAX_BATCH_AMOUNT");
        		productMockCntrl.setReturnValue(createPI("MAX_BATCH_AMOUNT",
        				"9999.99"), MockControl.ZERO_OR_MORE);

        		// Setup Max Batch Count
        		productMock.find("MAX_BATCH_COUNT");
        		productMockCntrl.setReturnValue(createPI("MAX_BATCH_COUNT",
        				"1"), MockControl.ZERO_OR_MORE);

        		// Setup Logo Option
	    		productMock.find("LOGO_OPTION");
	    		productMockCntrl.setReturnValue(createPI("LOGO_OPTION",
					"0"), MockControl.ZERO_OR_MORE);
	
	    		// Setup No Fee Option
	    		productMock.find("NO_FEE_OPTION");
	    		productMockCntrl.setReturnValue(createPI("NO_FEE_OPTION",
					"Y"), MockControl.ZERO_OR_MORE);
	
	    		// Setup Xmit Batch Amount
	    		productMock.find("XMIT_BATCH_AMOUNT");
	    		productMockCntrl.setReturnValue(createPI("XMIT_BATCH_AMOUNT",
					"79999"), MockControl.ZERO_OR_MORE);
	
	    		// Setup Max Amount Per Item
	    		productMock.find("MAX_AMOUNT_PER_ITEM");
	    		productMockCntrl.setReturnValue(createPI("MAX_AMOUNT_PER_ITEM",
					"99999"), MockControl.ZERO_OR_MORE);
	
	    		// Setup Dispenser Number
	    		productMock.find("DISPENSER_NUMBER");
	    		productMockCntrl.setReturnValue(createPI("DISPENSER_NUMBER",
					"1"), MockControl.ZERO_OR_MORE);
	
	    		// Setup Xmit Batch Count
	    		productMock.find("XMIT_BATCH_COUNT");
	    		productMockCntrl.setReturnValue(createPI("XMIT_BATCH_COUNT",
					"799"), MockControl.ZERO_OR_MORE);
	  		
	    		// Setup Document Sequence Number
	    		productMock.find("DOC_SEQ_NUM");
	    		productMockCntrl.setReturnValue(createPI("DOC_SEQ_NUM",
					"2"), MockControl.ZERO_OR_MORE);
	  		
	    		// Setup Name Pin Required
	    		productMock.find("NAME_PIN_REQUIRED");
	    		productMockCntrl.setReturnValue(createPI("NAME_PIN_REQUIRED",
					"P"), MockControl.ZERO_OR_MORE);
	      		
	    		// Setup Print Purchaser
	    		productMock.find("PRINT_PURCHASER");
	    		productMockCntrl.setReturnValue(createPI("PRINT_PURCHASER",
					"Y"), MockControl.ZERO_OR_MORE);
	      		
	    		// Setup Document Name
	    		productMock.find("DOCUMENT_NAME");
	    		productMockCntrl.setReturnValue(createPI("DOCUMENT_NAME",
					"VENDOR PAYMENT"), MockControl.ZERO_OR_MORE);
	      		
	    		// Setup Keyboard Issue
	    		productMock.find("KEYBOARD_ISSUE");
	    		productMockCntrl.setReturnValue(createPI("KEYBOARD_ISSUE",
					"Y"), MockControl.ZERO_OR_MORE);
	      		
	    		// Setup Enter Payee
	    		productMock.find("ENTER_PAYEE");
	    		productMockCntrl.setReturnValue(createPI("ENTER_PAYEE",
					"N"), MockControl.ZERO_OR_MORE);
	  		
	    		// Setup fEE Enabled
	    		productMock.find("FEE_ENABLE");
	    		productMockCntrl.setReturnValue(createPI("FEE_ENABLE",
					"N"), MockControl.ZERO_OR_MORE);
	  		
	    		// Setup Amount Tendered
	    		productMock.find("AMOUNT_TENDERED");
	    		productMockCntrl.setReturnValue(createPI("AMOUNT_TENDERED",
					"Y"), MockControl.ZERO_OR_MORE);
	  		
	    		// Setup Document Number
	    		productMock.find("DOCUMENT_NUMBER");
	    		productMockCntrl.setReturnValue(createPI("DOCUMENT_NUMBER",
					"1"), MockControl.ZERO_OR_MORE);
	  		
	    		break;
	    		
	    	case 3 :  // MONEYGRAM SEND
        		// Setup Max Batch Amount
        		productMock.find("MAX_BATCH_AMOUNT");
        		productMockCntrl.setReturnValue(createPI("MAX_BATCH_AMOUNT",
        				"9999.99"), MockControl.ZERO_OR_MORE);

        		// Setup Max Batch Count
        		productMock.find("MAX_BATCH_COUNT");
        		productMockCntrl.setReturnValue(createPI("MAX_BATCH_COUNT",
        				"1"), MockControl.ZERO_OR_MORE);

        		// Setup Restriciton code
	    		productMock.find("RESTRICTION_CODE");
	    		productMockCntrl.setReturnValue(createPI("RESTRICTION_CODE",
					"NON"), MockControl.ZERO_OR_MORE);
	
	    		// Setup Fraud Limit Photo Id
	    		productMock.find("FRAUD_LIMIT_PHOTO_ID");
	    		productMockCntrl.setReturnValue(createPI("FRAUD_LIMIT_PHOTO_ID",
					"900"), MockControl.ZERO_OR_MORE);

	    		// Setup Allow Delivery Options
	    		productMock.find("ALLOW_DEL_OPT", 1);
	    		productMockCntrl.setReturnValue(createPI("ALLOW_DEL_OPT",
					"Y"), MockControl.ZERO_OR_MORE);

	    		productMock.find("ALLOW_DEL_OPT", 2);
	    		productMockCntrl.setReturnValue(createPI("ALLOW_DEL_OPT",
					"Y"), MockControl.ZERO_OR_MORE);

	    		productMock.find("ALLOW_DEL_OPT", 3);
	    		productMockCntrl.setReturnValue(createPI("ALLOW_DEL_OPT",
					"Y"), MockControl.ZERO_OR_MORE);

	    		productMock.find("ALLOW_DEL_OPT", 4);
	    		productMockCntrl.setReturnValue(createPI("ALLOW_DEL_OPT",
					"Y"), MockControl.ZERO_OR_MORE);

	    		productMock.find("ALLOW_DEL_OPT", 5);
	    		productMockCntrl.setReturnValue(createPI("ALLOW_DEL_OPT",
					"Y"), MockControl.ZERO_OR_MORE);

	    		productMock.find("ALLOW_DEL_OPT", 6);
	    		productMockCntrl.setReturnValue(createPI("ALLOW_DEL_OPT",
					"Y"), MockControl.ZERO_OR_MORE);

	    		productMock.find("ALLOW_DEL_OPT", 7);
	    		productMockCntrl.setReturnValue(createPI("ALLOW_DEL_OPT",
					"Y"), MockControl.ZERO_OR_MORE);

	    		productMock.find("ALLOW_DEL_OPT", 8);
	    		productMockCntrl.setReturnValue(createPI("ALLOW_DEL_OPT",
					"Y"), MockControl.ZERO_OR_MORE);

	    		productMock.find("ALLOW_DEL_OPT", 9);
	    		productMockCntrl.setReturnValue(createPI("ALLOW_DEL_OPT",
					"Y"), MockControl.ZERO_OR_MORE);

	    		productMock.find("ALLOW_DEL_OPT", 10);
	    		productMockCntrl.setReturnValue(createPI("ALLOW_DEL_OPT",
					"Y"), MockControl.ZERO_OR_MORE);

	    		productMock.findCurrencyDecimalValue("MAX_AMOUNT_PER_ITEM", "USD");
	    		productMockCntrl.setReturnValue(new BigDecimal(9999.99), 8);
	    		productMockCntrl.setReturnValue(new BigDecimal(10001.00), 4);
	
	    		// Setup Receipt Format
	    		productMock.find("RECEIPT_FORMAT");
	    		productMockCntrl.setReturnValue(createPI("RECEIPT_FORMAT",
					"mgsus49"), MockControl.ZERO_OR_MORE);
	      		
	    		// Setup Keyboard Issue
	    		productMock.find("KEYBOARD_ISSUE");
	    		productMockCntrl.setReturnValue(createPI("KEYBOARD_ISSUE",
					"Y"), MockControl.ZERO_OR_MORE);
	      		
	    		// Setup Fraud Limit Legal Id
	    		productMock.find("FRAUD_LIMIT_LEGAL_ID");
	    		productMockCntrl.setReturnValue(createPI("FRAUD_LIMIT_LEGAL_ID",
					"3000"), MockControl.ZERO_OR_MORE);
	      		
	    		// Setup Document Name
	    		productMock.find("DOCUMENT_NAME");
	    		productMockCntrl.setReturnValue(createPI("DOCUMENT_NAME",
					"MONEYGRAM SEND"), MockControl.ZERO_OR_MORE);
	      		
	    		// Setup Discount Option
	    		productMock.find("DISCOUNT_OPTION");
	    		productMockCntrl.setReturnValue(createPI("DISCOUNT_OPTION",
					"Y"), MockControl.ZERO_OR_MORE);
	  		
	    		// Setup Amount Tendered
	    		productMock.find("AMOUNT_TENDERED");
	    		productMockCntrl.setReturnValue(createPI("AMOUNT_TENDERED",
					"Y"), MockControl.ZERO_OR_MORE);
	  		
	    		// Setup Name Pin Required
	    		productMock.find("NAME_PIN_REQUIRED");
	    		productMockCntrl.setReturnValue(createPI("NAME_PIN_REQUIRED",
					"P"), MockControl.ZERO_OR_MORE);
	  		
	    		// Setup Fraud Limit Test Question
	    		productMock.find("FRAUD_LIMIT_TEST_QUESTION");
	    		productMockCntrl.setReturnValue(createPI("FRAUD_LIMIT_TEST_QUESTION",
					"900"), MockControl.ZERO_OR_MORE);
		
	    		// Setup Fraud Limit Cash Warning
	    		productMock.find("FRAUD_LIMIT_CASH_WARNING");
	    		productMockCntrl.setReturnValue(createPI("FRAUD_LIMIT_CASH_WARNING",
					"500"), MockControl.ZERO_OR_MORE);
	
	    		// Setup Document Sequence Number
	    		productMock.find("DOC_SEQ_NUM");
	    		productMockCntrl.setReturnValue(createPI("DOC_SEQ_NUM",
					"5"), MockControl.ZERO_OR_MORE);
	
	    		// Setup Fraud Limit Third Party Id
	    		productMock.find("FRAUD_LIMIT_THIRD_PARTY_ID");
	    		productMockCntrl.setReturnValue(createPI("FRAUD_LIMIT_THIRD_PARTY_ID",
					"3000"), MockControl.ZERO_OR_MORE);
	  		
	    		break;
        		
	    	case 4 :  // MONEYGRAM RECEIVE
        		// Setup Max Batch Amount
        		productMock.find("MAX_BATCH_AMOUNT");
        		productMockCntrl.setReturnValue(createPI("MAX_BATCH_AMOUNT",
        				"9999.99"), MockControl.ZERO_OR_MORE);

        		// Setup Max Batch Count
        		productMock.find("MAX_BATCH_COUNT");
        		productMockCntrl.setReturnValue(createPI("MAX_BATCH_COUNT",
        				"1"), MockControl.ZERO_OR_MORE);

        		// Setup Max Amount Per Item
        		productMock.find("MAX_AMOUNT_PER_ITEM");
        		productMockCntrl.setReturnValue(createPI("MAX_AMOUNT_PER_ITEM",
        				"9999.99"), MockControl.ZERO_OR_MORE);
	
	    		// Setup Receipt Format
	    		productMock.find("RECEIPT_FORMAT");
	    		productMockCntrl.setReturnValue(createPI("RECEIPT_FORMAT",
					"mgrus49"), MockControl.ZERO_OR_MORE);
	      		
	    		// Setup Keyboard Issue
	    		productMock.find("KEYBOARD_ISSUE");
	    		productMockCntrl.setReturnValue(createPI("KEYBOARD_ISSUE",
					"Y"), MockControl.ZERO_OR_MORE);
	      		
	    		// Setup Fraud Limit Photo Id
	    		productMock.find("FRAUD_LIMIT_LEGAL_ID");
	    		productMockCntrl.setReturnValue(createPI("FRAUD_LIMIT_LEGAL_ID",
					"900"), MockControl.ZERO_OR_MORE);
	
	    		// Setup Document Name
	    		productMock.find("DOCUMENT_NAME");
	    		productMockCntrl.setReturnValue(createPI("DOCUMENT_NAME",
					"MG MONEY ORDER"), MockControl.ZERO_OR_MORE);
	      		
	    		productMock.find("DOCUMENT_NUMBER");
	    		productMockCntrl.setReturnValue(createPI("DOCUMENT_NUMBER",
					"4"), MockControl.ZERO_OR_MORE);

	      			    		
	    		// Setup Fraud Limit Receiver
	    		productMock.find("FRAUD_LIMIT_RECEIVER");
	    		productMockCntrl.setReturnValue(createPI("FRAUD_LIMIT_RECEIVER",
					"900"), MockControl.ZERO_OR_MORE);
	      		
	    		// Setup Dispenser Number
	    		productMock.find("DISPENSER_NUMBER");
	    		productMockCntrl.setReturnValue(createPI("DISPENSER_NUMBER",
					"1"), MockControl.ZERO_OR_MORE);
	
	    		// Setup Default Cash Payout Amount
	    		productMock.find("DEFAULT_CASH_PAYOUT_AMOUNT");
	    		productMockCntrl.setReturnValue(createPI("DEFAULT_CASH_PAYOUT_AMOUNT",
					"500"), MockControl.ZERO_OR_MORE);
	      		
	    		// Setup Name Pin Required
	    		productMock.find("NAME_PIN_REQUIRED");
	    		productMockCntrl.setReturnValue(createPI("NAME_PIN_REQUIRED",
					"P"), MockControl.ZERO_OR_MORE);
	  			
	    		// Setup Logo Option
	    		productMock.find("LOGO_OPTION");
	    		productMockCntrl.setReturnValue(createPI("LOGO_OPTION",
					"0"), MockControl.ZERO_OR_MORE);
	
	    		// Setup Fraud Limit Legal Address
	    		productMock.find("FRAUD_LIMIT_ADDRESS");
	    		productMockCntrl.setReturnValue(createPI("FRAUD_LIMIT_ADDRESS",
					"300"), MockControl.ZERO_OR_MORE);
	      		
	    		// Setup Document Sequence Number
	    		productMock.find("DOC_SEQ_NUM");
	    		productMockCntrl.setReturnValue(createPI("DOC_SEQ_NUM",
					"4"), MockControl.ZERO_OR_MORE);
	  		
	    		// Setup Fraud Limit Third Party Id
	    		productMock.find("FRAUD_LIMIT_THIRD_PARTY_ID");
	    		productMockCntrl.setReturnValue(createPI("FRAUD_LIMIT_THIRD_PARTY_ID",
					"3000"), MockControl.ZERO_OR_MORE);
	  		
	    		break;
        		
	    	case 5 :  // BILL PAYMENT
        		// Setup Max Batch Amount
        		productMock.find("MAX_BATCH_AMOUNT");
        		productMockCntrl.setReturnValue(createPI("MAX_BATCH_AMOUNT",
        				"9999.99"), MockControl.ZERO_OR_MORE);

        		// Setup Max Batch Count
        		productMock.find("MAX_BATCH_COUNT");
        		productMockCntrl.setReturnValue(createPI("MAX_BATCH_COUNT",
        				"1"), MockControl.ZERO_OR_MORE);

        		// Setup Fraud Limit Photo Id
	    		productMock.find("FRAUD_LIMIT_PHOTO_ID");
	    		productMockCntrl.setReturnValue(createPI("FRAUD_LIMIT_PHOTO_ID",
					"900"), MockControl.ZERO_OR_MORE);
	
	    		// Setup Max Amount Per Item
	    		productMock.find("MAX_AMOUNT_PER_ITEM");
	    		productMockCntrl.setReturnValue(createPI("MAX_AMOUNT_PER_ITEM",
					"9099.99"), MockControl.ZERO_OR_MORE);
	
	    		// Setup Receipt Format
	    		productMock.find("RECEIPT_FORMAT");
	    		productMockCntrl.setReturnValue(createPI("RECEIPT_FORMAT",
					"expus49"), MockControl.ZERO_OR_MORE);
	      		
	    		// Setup Keyboard Issue
	    		productMock.find("KEYBOARD_ISSUE");
	    		productMockCntrl.setReturnValue(createPI("KEYBOARD_ISSUE",
					"Y"), MockControl.ZERO_OR_MORE);
	      		
	    		// Setup Fraud Limit Legal Id
	    		productMock.find("FRAUD_LIMIT_LEGAL_ID");
	    		productMockCntrl.setReturnValue(createPI("FRAUD_LIMIT_LEGAL_ID",
					"3000"), MockControl.ZERO_OR_MORE);
	      		
	    		// Setup Document Name
	    		productMock.find("DOCUMENT_NAME");
	    		productMockCntrl.setReturnValue(createPI("DOCUMENT_NAME",
					"BILL PAYMENT"), MockControl.ZERO_OR_MORE);
	      		
	    		// Setup Discount Option
	    		productMock.find("DISCOUNT_OPTION");
	    		productMockCntrl.setReturnValue(createPI("DISCOUNT_OPTION",
					"Y"), MockControl.ZERO_OR_MORE);
	  		
	    		// Setup Amount Tendered
	    		productMock.find("AMOUNT_TENDERED");
	    		productMockCntrl.setReturnValue(createPI("AMOUNT_TENDERED",
					"Y"), MockControl.ZERO_OR_MORE);
	  		
	    		// Setup Name Pin Required
	    		productMock.find("NAME_PIN_REQUIRED");
	    		productMockCntrl.setReturnValue(createPI("NAME_PIN_REQUIRED",
					"P"), MockControl.ZERO_OR_MORE);
	  			
	    		// Setup Fraud Limit Cash Warning
	    		productMock.find("FRAUD_LIMIT_CASH_WARNING");
	    		productMockCntrl.setReturnValue(createPI("FRAUD_LIMIT_CASH_WARNING",
					"500"), MockControl.ZERO_OR_MORE);
	
	    		// Setup Fraud Limit Third Party Id
	    		productMock.find("FRAUD_LIMIT_THIRD_PARTY_ID");
	    		productMockCntrl.setReturnValue(createPI("FRAUD_LIMIT_THIRD_PARTY_ID",
					"3000"), MockControl.ZERO_OR_MORE);
	  		
	    		break;
	    		
	    	case 9 :  // RECEIVE CUSTOMER REGISTRATION
        		// Setup Document Name
	    		productMock.find("DOCUMENT_NAME");
	    		productMockCntrl.setReturnValue(createPI("DOCUMENT_NAME",
					"Receive Customer Registration"), MockControl.ZERO_OR_MORE);
	
	    		// Setup Document Sequence Number
	    		productMock.find("DOC_SEQ_NUM");
	    		productMockCntrl.setReturnValue(createPI("DOC_SEQ_NUM",
					"9"), MockControl.ZERO_OR_MORE);
	
	    		// Setup Name Pin Required
	    		productMock.find("NAME_PIN_REQUIRED");
	    		productMockCntrl.setReturnValue(createPI("NAME_PIN_REQUIRED",
					"P"), MockControl.ZERO_OR_MORE);
	  		
	
	    		break;
        }
	}
		
	public void setupDispenser(int dispenserId) {
		System.err.println("In setupDispenser()");
		ProfileMockSet pms = new ProfileMockSet();
		MockControl dispenserMockCntrl = pms.dispenserMockControl = MockClassControl
				.createNiceControl(Profile.class, new Class[] { String.class },
						new Object[] { "DISPENSER" });
		Profile dispenserMock = pms.dispenserMock = (Profile) dispenserMockCntrl.getMock();
		dispenserMocks.put(new Integer(dispenserId), pms);

		profMock.find("DISPENSER", dispenserId);
		profMockControl.setReturnValue(dispenserMock, MockControl.ZERO_OR_MORE);

		dispenserMock.getID();
		dispenserMockCntrl.setReturnValue(dispenserId);

		// Setup Dispenser Number
		dispenserMock.find("DISPENSER_NUMBER");
		dispenserMockCntrl.setReturnValue(createPI("DISPENSER_NUMBER",
			"1"), MockControl.ZERO_OR_MORE);

		// Setup Low Form Warning
		dispenserMock.find("LOW_FORM_WARNING");
		dispenserMockCntrl.setReturnValue(createPI("LOW_FORM_WARNING",
			"15"), MockControl.ZERO_OR_MORE);

		// Setup Parts Per Form
		dispenserMock.find("PARTS_PER_FORM");
		dispenserMockCntrl.setReturnValue(createPI("PARTS_PER_FORM",
			"1"), MockControl.ZERO_OR_MORE);

		// Setup Bundle Size
		dispenserMock.find("BUNDLE_SIZE");
		dispenserMockCntrl.setReturnValue(createPI("BUNDLE_SIZE",
			"700"), MockControl.ZERO_OR_MORE);

		// Setup Max Voidable Forms
		dispenserMock.find("MAX_VOIDABLE_FORMS");
		dispenserMockCntrl.setReturnValue(createPI("MAX_VOIDABLE_FORMS",
			"15"), MockControl.ZERO_OR_MORE);

		// Setup Max Forms Per Sale
		dispenserMock.find("MAX_FORMS_PER_SALE");
		dispenserMockCntrl.setReturnValue(createPI("MAX_FORMS_PER_SALE",
			"5"), MockControl.ZERO_OR_MORE);

		// Setup Stub Format
		dispenserMock.find("STUB_FORMAT");
		dispenserMockCntrl.setReturnValue(createPI("STUB_FORMAT",
			"4LNE"), MockControl.ZERO_OR_MORE);

		// Setup Unit Number
		dispenserMock.find("UNIT_NUMBER");
		dispenserMockCntrl.setReturnValue(createPI("UNIT_NUMBER",
			"000010010906"), MockControl.ZERO_OR_MORE);
	}

	private void setupStdProfile() {

		upMock.getProfileInstance();
		upMockControl.setReturnValue(profMock, MockControl.ZERO_OR_MORE);

		profMock.get("PROFILE_ID");
		profMockControl.setReturnValue(createPI("PROFILE_ID", "107099"),
				MockControl.ZERO_OR_MORE);

		profMock.get("CONNECTION_CLOSE_DELAY");
		profMockControl.setReturnValue(createPI("CONNECTION_CLOSE_DELAY", "90"),
				MockControl.ZERO_OR_MORE);

		profMock.get("HOST_SPECIAL_DIAL");
		profMockControl.setReturnValue(createPI("HOST_SPECIAL_DIAL", "90"),
				MockControl.ZERO_OR_MORE);

		profMock.get("HOST_PHONE_TYPE");
		profMockControl.setReturnValue(createPI("HOST_PHONE_TYPE", "T"),
				MockControl.ZERO_OR_MORE);

		profMock.get("PRIMARY_DUN_PASSWORD");
		profMockControl.setReturnValue(createPI("PRIMARY_DUN_PASSWORD", "107096"),
				MockControl.ZERO_OR_MORE);

		profMock.get("PRIMARY_DUN_USER_ID");
		profMockControl.setReturnValue(createPI("PRIMARY_DUN_USER_ID", "408264740010"),
				MockControl.ZERO_OR_MORE);

		profMock.get("PRIMARY_NETWORK_TYPE");
		profMockControl.setReturnValue(createPI("PRIMARY_NETWORK_TYPE", "UUNet"),
				MockControl.ZERO_OR_MORE);

		profMock.get("HOST_PRIMARY_PHONE");
		profMockControl.setReturnValue(createPI("HOST_PRIMARY_PHONE", "612-630-0770"),
				MockControl.ZERO_OR_MORE);

		profMock.get("DEVICE_SERVICE_TAG");
		profMockControl.setReturnValue(createPI("DEVICE_SERVICE_TAG", ""),
				MockControl.ZERO_OR_MORE);

		profMock.get("PINS_PRIVS_LOCK");
		profMockControl.setReturnValue(createPI("PINS_PRIVS_LOCK", "N"),
				MockControl.ZERO_OR_MORE);

		profMock.get("DEVICE_TYPE");
		profMockControl.setReturnValue(createPI("DEVICE_TYPE", "DWPC"),
				MockControl.ZERO_OR_MORE);
		
		profMock.find("DUAL_CONTROL");
		profMockControl.setReturnValue(createPI("DUAL_CONTROL", "N"),
				MockControl.ZERO_OR_MORE);
		
		profMock.get("ALLOW_UNIT_OFFICE_REPORTING");
		profMockControl.setReturnValue(createPI("ALLOW_UNIT_OFFICE_REPORTING", "N"),
				MockControl.ZERO_OR_MORE);

		profMock.get("IDLE_BACKOUT_TIME");
		profMockControl.setReturnValue(createPI("IDLE_BACKOUT_TIME", "2"),
				MockControl.ZERO_OR_MORE);
		
		profMock.find("ALLOW_REF_BY_PHONE");
		profMockControl.setReturnValue(createPI("ALLOW_REF_BY_PHONE", "Y"),
				MockControl.ZERO_OR_MORE);
		
		profMock.get("GENERIC_DOCUMENT_STORE");
		profMockControl.setReturnValue(createPI("GENERIC_DOCUMENT_STORE", "P &amp; C #74"),
				MockControl.ZERO_OR_MORE);

		profMock.get("ACCOUNT_SPECIALIST");
		profMockControl.setReturnValue(createPI("ACCOUNT_SPECIALIST", "455"),
				MockControl.ZERO_OR_MORE);

		profMock.find("HDV_LIMIT");
		profMockControl.setReturnValue(createPI("HDV_LIMIT", "3000.00"),
				MockControl.ZERO_OR_MORE);

		profMock.get("ALLOW_REF_BY_NAME");
		profMockControl.setReturnValue(createPI("ALLOW_REF_BY_NAME", "Y"),
				MockControl.ZERO_OR_MORE);

		profMock.get("SOFTWARE_DOWNLOAD_URL");
		profMockControl.setReturnValue(createPI("SOFTWARE_DOWNLOAD_URL", "https://expressnt1/dwprod/"),
				MockControl.ZERO_OR_MORE);

		profMock.get("ALLOW_CONFIRM_STATUS");
		profMockControl.setReturnValue(createPI("ALLOW_CONFIRM_STATUS", "Y"),
				MockControl.ZERO_OR_MORE);

		profMock.find("FINANCIAL_INSTITUTION");
		profMockControl.setReturnValue(createPI("FINANCIAL_INSTITUTION", "N"),
				MockControl.ZERO_OR_MORE);

		profMock.get("RESET_PASSWORD");
		profMockControl.setReturnValue(createPI("RESET_PASSWORD", "123456"),
				MockControl.ZERO_OR_MORE);

		profMock.get("ALLOW_PHONE_LOOKUPS");
		profMockControl.setReturnValue(createPI("ALLOW_PHONE_LOOKUPS", "Y"),
				MockControl.ZERO_OR_MORE);

		profMock.get("DAYS_ALLOWED_AWOL");
		profMockControl.setReturnValue(createPI("DAYS_ALLOWED_AWOL", "1"),
				MockControl.ZERO_OR_MORE);

		profMock.get("ALLOW_AMENDS");
		profMockControl.setReturnValue(createPI("ALLOW_AMENDS", "Y"),
				MockControl.ZERO_OR_MORE);

		profMock.get("AGENT_PHONE");
		profMockControl.setReturnValue(createPI("AGENT_PHONE", "31592355010"),
				MockControl.ZERO_OR_MORE);

		profMock.get("ALLOW_RECEIVE_REVERSALS");
		profMockControl.setReturnValue(createPI("ALLOW_RECEIVE_REVERSALS", "Y"),
				MockControl.ZERO_OR_MORE);

		profMock.get("ALLOW_FEE_REFUND");
		profMockControl.setReturnValue(createPI("ALLOW_FEE_REFUND", "Y"),
				MockControl.ZERO_OR_MORE);

		profMock.get("ALLOW_REFUND");
		profMockControl.setReturnValue(createPI("ALLOW_REFUND", "Y"),
				MockControl.ZERO_OR_MORE);

		profMock.get("ALLOW_SEND_CANCELS");
		profMockControl.setReturnValue(createPI("ALLOW_SEND_CANCELS", "Y"),
				MockControl.ZERO_OR_MORE);

		profMock.find("MONEYSAVER_ENABLE");
		profMockControl.setReturnValue(createPI("MONEYSAVER_ENABLE", "Y"),
				MockControl.ZERO_OR_MORE);
		
		profMock.get("MANUAL_CTR_ENABLE");
		profMockControl.setReturnValue(createPI("MANUAL_CTR_ENABLE", "N"),
				MockControl.ZERO_OR_MORE);

		profMock.get("SAR_NAG");
		profMockControl.setReturnValue(createPI("SAR_NAG", "N"),
				MockControl.ZERO_OR_MORE);

		profMock.get("SAR_ENABLE");
		profMockControl.setReturnValue(createPI("SAR_ENABLE", "N"),
				MockControl.ZERO_OR_MORE);

		profMock.find("TRANSLATIONS_VERSION");
		profMockControl.setReturnValue(createPI("TRANSLATIONS_VERSION", "20050706092950"),
				MockControl.ZERO_OR_MORE);
		
		profMock.get("AUTO_ENROLL_OPTION");
		profMockControl.setReturnValue(createPI("AUTO_ENROLL_OPTION", "N"),
				MockControl.ZERO_OR_MORE);

		profMock.get("ACTIVATE_REWARDS_CARD_OPTION");
		profMockControl.setReturnValue(createPI("ACTIVATE_REWARDS_CARD_OPTION", "N"),
				MockControl.ZERO_OR_MORE);

		profMock.get("SOFTWARE_VERSION_ID");
		profMockControl.setReturnValue(createPI("SOFTWARE_VERSION_ID", "440"),
				MockControl.ZERO_OR_MORE);

		profMock.get("POS_STATUS");
		profMockControl.setReturnValue(createPI("POS_STATUS", "A"),
				MockControl.ZERO_OR_MORE);

		profMock.find("POS_HOST_CONNECTION");
		profMockControl.setReturnValue(createPI("POS_HOST_CONNECTION", "N"),
				MockControl.ZERO_OR_MORE);
		
		profMock.get("MG_PIN");
		profMockControl.setReturnValue(createPI("MG_PIN", "3281"),
				MockControl.ZERO_OR_MORE);
		
		profMock.get("MG_ACCOUNT");
		profMockControl.setReturnValue(createPI("MG_ACCOUNT", "110570"),
				MockControl.ZERO_OR_MORE);

		profMock.get("ALT_LANGUAGE");
		profMockControl.setReturnValue(createPI("ALT_LANGUAGE", "es"),
				MockControl.ZERO_OR_MORE);

		profMock.find("HISTORY_ROWS");
		profMockControl.setReturnValue(createPI("HISTORY_ROWS", "15"),
				MockControl.ZERO_OR_MORE);
		
		profMock.find("LANGUAGE");
		profMockControl.setReturnValue(createPI("LANGUAGE", "en"),
				MockControl.ZERO_OR_MORE);
		
		profMock.get("ACCOUNTING_WINDOW");
		profMockControl.setReturnValue(createPI("ACCOUNTING_WINDOW", "0"),
				MockControl.ZERO_OR_MORE);

		profMock.get("ACCOUNTING_START_TIME");
		profMockControl.setReturnValue(createPI("ACCOUNTING_START_TIME", "00:00"),
				MockControl.ZERO_OR_MORE);

		profMock.get("ACCOUNTING_START_DAY");
		profMockControl.setReturnValue(createPI("ACCOUNTING_START_DAY", "1"),
				MockControl.ZERO_OR_MORE);

		profMock.find("FRAUD_ALERT_MESSAGE");
		profMockControl.setReturnValue(createPI("FRAUD_ALERT_MESSAGE", "&lt;html&gt;&lt;FONT color=red &lt;B&gt;Do you have cash in hand for this transaction?&lt;/B&gt; Please collect cash from the customer.  MoneyGram  will not call you to perform a test transaction&lt;TABLE cell Padding=5 border=1&gt;&lt;TR&gt;&lt;TD vAlign=top &gt;&lt;B&gt;&lt;FONT color=blue&gt;Warning: Don't forget to process this transaction in your internal system to avoid a reconciliation problem."),
				MockControl.ZERO_OR_MORE);
		
		profMock.get("AUTO_ACCOUNTING");
		profMockControl.setReturnValue(createPI("AUTO_ACCOUNTING", "Y"),
				MockControl.ZERO_OR_MORE);

		profMock.get("TRANSMIT_DEBUG");
		profMockControl.setReturnValue(createPI("TRANSMIT_DEBUG", "N"),
				MockControl.ZERO_OR_MORE);

		profMock.get("TRAINING_MODE");
		profMockControl.setReturnValue(createPI("TRAINING_MODE", "N"),
				MockControl.ZERO_OR_MORE);

		profMock.get("USE_OTHER_TIME");
		profMockControl.setReturnValue(createPI("USE_OTHER_TIME", "Y"),
				MockControl.ZERO_OR_MORE);

		profMock.get("LONG_TIMEOUT");
		profMockControl.setReturnValue(createPI("LONG_TIMEOUT", "360"),
				MockControl.ZERO_OR_MORE);

		profMock.get("HOST_XMIT_TIME_1");
		profMockControl.setReturnValue(createPI("HOST_XMIT_TIME_1", "00:01"),
				MockControl.ZERO_OR_MORE);

		profMock.get("HOST_XMIT_TIME_2");
		profMockControl.setReturnValue(createPI("HOST_XMIT_TIME_2", "00:01"),
				MockControl.ZERO_OR_MORE);

		profMock.get("PROXY_TYPE");
		profMockControl.setReturnValue(createPI("PROXY_TYPE", "SOCKS"),
				MockControl.ZERO_OR_MORE);

		profMock.get("SHORT_TIMEOUT");
		profMockControl.setReturnValue(createPI("SHORT_TIMEOUT", "0"),
				MockControl.ZERO_OR_MORE);
		
		profMock.find("GLOBAL_NAME_PIN_REQ");
		profMockControl.setReturnValue(createPI("GLOBAL_NAME_PIN_REQ", "P"),
				MockControl.ZERO_OR_MORE);

		profMock.get("AGENT_DST");
		profMockControl.setReturnValue(createPI("AGENT_DST", "Y"),
				MockControl.ZERO_OR_MORE);

		profMock.get("SUBAGENT_MODE");
		profMockControl.setReturnValue(createPI("SUBAGENT_MODE", "N"),
				MockControl.ZERO_OR_MORE);

		profMock.get("SUPERAGENT");
		profMockControl.setReturnValue(createPI("SUPERAGENT", "N"),
				MockControl.ZERO_OR_MORE);

		profMock.get("USE_PROXY");
		profMockControl.setReturnValue(createPI("USE_PROXY", "N"),
				MockControl.ZERO_OR_MORE);

		profMock.get("AGENT_PRINTER_COLUMNS");
		profMockControl.setReturnValue(createPI("AGENT_PRINTER_COLUMNS", "40"),
				MockControl.ZERO_OR_MORE);

		profMock.get("AC_DIALUP_URL");
		profMockControl.setReturnValue(createPI("AC_DIALUP_URL", "https://temgdwdd.int.temgweb.com/ac50/services/AgentConnect60"),
				MockControl.ZERO_OR_MORE);

		profMock.get("AGENT_POS_SEQ");
		profMockControl.setReturnValue(createPI("AGENT_POS_SEQ", "9"),
				MockControl.ZERO_OR_MORE);

		profMock.get("AC_NETWORK_URL");
		profMockControl.setReturnValue(createPI("AC_NETWORK_URL", "https://temgdwdd.int.temgweb.com/ac50/services/AgentConnect60"),
				MockControl.ZERO_OR_MORE);

		profMock.get("AGENT_ID");
		profMockControl.setReturnValue(createPI("AGENT_ID", "40826474"),
				MockControl.ZERO_OR_MORE);

		profMock.get("USE_AGENT_PRINTER");
		profMockControl.setReturnValue(createPI("USE_AGENT_PRINTER", "N"),
				MockControl.ZERO_OR_MORE);

		profMock.get("HOST_BLIND_DIAL");
		profMockControl.setReturnValue(createPI("HOST_BLIND_DIAL", "N"),
				MockControl.ZERO_OR_MORE);

		profMock.get("COM_PORT");
		profMockControl.setReturnValue(createPI("COM_PORT", "2"),
				MockControl.ZERO_OR_MORE);

		// setup the dispenser
		
		profMock.get("DISPENSER_NUMBER");
		profMockControl.setReturnValue(createPI("DISPENSER_NUMBER", "1"),
				MockControl.ZERO_OR_MORE);

		profMock.get("LOW_FORM_WARNING");
		profMockControl.setReturnValue(createPI("LOW_FORM_WARNING", "15"),
				MockControl.ZERO_OR_MORE);

		profMock.get("PARTS_PER_FORM");
		profMockControl.setReturnValue(createPI("PARTS_PER_FORM", "1"),
				MockControl.ZERO_OR_MORE);

		profMock.get("BUNDLE_SIZE");
		profMockControl.setReturnValue(createPI("BUNDLE_SIZE", "700"),
				MockControl.ZERO_OR_MORE);

		profMock.get("MAX_VOIDABLE_FORMS");
		profMockControl.setReturnValue(createPI("MAX_VOIDABLE_FORMS", "15"),
				MockControl.ZERO_OR_MORE);

		profMock.get("MAX_FORMS_PER_SALE");
		profMockControl.setReturnValue(createPI("MAX_FORMS_PER_SALE", "15"),
				MockControl.ZERO_OR_MORE);

		profMock.get("STUB_FORMAT");
		profMockControl.setReturnValue(createPI("STUB_FORMAT", "4LNE"),
				MockControl.ZERO_OR_MORE);

		profMock.get("UNIT_NUMBER");
		profMockControl.setReturnValue(createPI("UNIT_NUMBER", "000010010906"),
				MockControl.ZERO_OR_MORE);
	}

	public void setupWalmartAgent() {
		profMock.get("DEVICE_ID");
		profMockControl.setReturnValue(createPI("DEVICE_ID", "80110016"),
				MockControl.ZERO_OR_MORE);

		profMock.get("AGENT_CURRENCY");
		profMockControl.setReturnValue(createPI("AGENT_CURRENCY", "USD"),
				MockControl.ZERO_OR_MORE);

		profMock.find("DAILY_LIMIT");
		profMockControl.setReturnValue(createPI("DAILY_LIMIT", "100000.00"),
				MockControl.ZERO_OR_MORE);
		
		profMock.get("AGENT_COUNTRY");
		profMockControl.setReturnValue(createPI("AGENT_COUNTRY", "UNITED STATES"),
				MockControl.ZERO_OR_MORE);

		profMock.get("AGENT_ZIP");
		profMockControl.setReturnValue(createPI("AGENT_ZIP", "14433"),
				MockControl.ZERO_OR_MORE);

		profMock.get("AGENT_STATE");
		profMockControl.setReturnValue(createPI("AGENT_STATE", "NEW YORK"),
				MockControl.ZERO_OR_MORE);

		profMock.get("AGENT_CITY");
		profMockControl.setReturnValue(createPI("AGENT_CITY", "CLYDEK"),
				MockControl.ZERO_OR_MORE);

		profMock.find("AGENT_ADDRESS_1");
		profMockControl.setReturnValue(createPI("AGENT_ADDRESS_1", "COLUMBIA &amp; FACTORY ST"),
				MockControl.ZERO_OR_MORE);
		
		profMock.find("AGENT_ADDRESS_2");
		profMockControl.setReturnValue(createPI("AGENT_ADDRESS_2", ""),
				MockControl.ZERO_OR_MORE);
		
		profMock.find("AGENT_ADDRESS_3");
		profMockControl.setReturnValue(createPI("AGENT_ADDRESS_3", ""),
				MockControl.ZERO_OR_MORE);
		
		profMock.get("AGENT_NAME");
		profMockControl.setReturnValue(createPI("AGENT_NAME", "P &amp; C #74"),
				MockControl.ZERO_OR_MORE);

		profMock.get("RECEIPTS_ENABLE");
		profMockControl.setReturnValue(createPI("RECEIPTS_ENABLE", "Y"),
				MockControl.ZERO_OR_MORE);

		profMock.get("MG_CHECKS");
		profMockControl.setReturnValue(createPI("MG_CHECKS", "DG"),
				MockControl.ZERO_OR_MORE);

		profMock.get("USE_OUR_PAPER");
		profMockControl.setReturnValue(createPI("USE_OUR_PAPER", "Y"),
				MockControl.ZERO_OR_MORE);
		
		profMock.get("RECEIPT_800_NUMBER_OPTION");
		profMockControl.setReturnValue(createPI("RECEIPT_800_NUMBER_OPTION", "Y"),
				MockControl.ZERO_OR_MORE);

		profMock.get("POS_DEVICE_OWNED");
		profMockControl.setReturnValue(createPI("POS_DEVICE_OWNED", "Y"),
				MockControl.ZERO_OR_MORE);

		profMock.get("MO_RELATION");
		profMockControl.setReturnValue(createPI("MO_RELATION", "00053727"),
				MockControl.ZERO_OR_MORE);

		profMock.get("LOCALE");
		profMockControl.setReturnValue(createPI("LOCALE", "en_US"),
				MockControl.ZERO_OR_MORE);

		profMock.get("MO_ACCOUNT");
		profMockControl.setReturnValue(createPI("MO_ACCOUNT", "98000101711017"),
				MockControl.ZERO_OR_MORE);

		profMock.get("MO_AGENT_ID");
		profMockControl.setReturnValue(createPI("MO_AGENT_ID", "811003901017"),
				MockControl.ZERO_OR_MORE);

		profMock.get("AGENT_TIME_ZONE");
		profMockControl.setReturnValue(createPI("AGENT_TIME_ZONE", "GMT-5:00"),
				MockControl.ZERO_OR_MORE);

		upMock.getDailyTotal();
		upMockControl.setReturnValue(new BigDecimal(20.0),
				MockControl.ZERO_OR_MORE);
	}

	public void setupForeignAgent() {
		profMock.get("DEVICE_ID");
		profMockControl.setReturnValue(createPI("DEVICE_ID", "80110016"),
				MockControl.ZERO_OR_MORE);

		profMock.get("AGENT_CURRENCY");
		profMockControl.setReturnValue(createPI("AGENT_CURRENCY", "CAD"),
				MockControl.ZERO_OR_MORE);

		profMock.find("DAILY_LIMIT");
		profMockControl.setReturnValue(createPI("DAILY_LIMIT", "100000.00"),
				MockControl.ZERO_OR_MORE);
		
		profMock.get("AGENT_COUNTRY");
		profMockControl.setReturnValue(createPI("AGENT_COUNTRY", "CANADA"),
				MockControl.ZERO_OR_MORE);

		profMock.get("AGENT_ZIP");
		profMockControl.setReturnValue(createPI("AGENT_ZIP", "14433"),
				MockControl.ZERO_OR_MORE);
		
		profMock.get("AGENT_STATE");
		profMockControl.setReturnValue(createPI("AGENT_STATE", ""),
				MockControl.ZERO_OR_MORE);

		profMock.get("AGENT_CITY");
		profMockControl.setReturnValue(createPI("AGENT_CITY", "ROGERSVILLE"),
				MockControl.ZERO_OR_MORE);

		profMock.get("AGENT_ADDRESS_3");
		profMockControl.setReturnValue(createPI("AGENT_ADDRESS_3", ""),
				MockControl.ZERO_OR_MORE);

		profMock.get("AGENT_ADDRESS_2");
		profMockControl.setReturnValue(createPI("AGENT_ADDRESS_2", ""),
				MockControl.ZERO_OR_MORE);

		profMock.get("AGENT_ADDRESS_1");
		profMockControl.setReturnValue(createPI("AGENT_ADDRESS_1", "RR 4/RT 480"),
				MockControl.ZERO_OR_MORE);

		profMock.get("AGENT_NAME");
		profMockControl.setReturnValue(createPI("AGENT_NAME", "CAISSE POPULAIR"),
				MockControl.ZERO_OR_MORE);
		
		profMock.get("RECEIPTS_ENABLE");
		profMockControl.setReturnValue(createPI("RECEIPTS_ENABLE", "N"),
				MockControl.ZERO_OR_MORE);

		profMock.get("MG_CHECKS");
		profMockControl.setReturnValue(createPI("MG_CHECKS", "MTC"),
				MockControl.ZERO_OR_MORE);

		profMock.get("USE_OUR_PAPER");
		profMockControl.setReturnValue(createPI("USE_OUR_PAPER", "Y"),
				MockControl.ZERO_OR_MORE);

		profMock.get("RECEIPT_800_NUMBER_OPTION");
		profMockControl.setReturnValue(createPI("RECEIPT_800_NUMBER_OPTION", "Y"),
				MockControl.ZERO_OR_MORE);

		profMock.get("POS_DEVICE_OWNED");
		profMockControl.setReturnValue(createPI("POS_DEVICE_OWNED", "Y"),
				MockControl.ZERO_OR_MORE);

		profMock.get("MO_RELATION");
		profMockControl.setReturnValue(createPI("MO_RELATION", ""),
				MockControl.ZERO_OR_MORE);
		
		profMock.get("LOCALE");
		profMockControl.setReturnValue(createPI("LOCALE", "en_US"),
				MockControl.ZERO_OR_MORE);

		profMock.get("MO_ACCOUNT");
		profMockControl.setReturnValue(createPI("MO_ACCOUNT", ""),
				MockControl.ZERO_OR_MORE);

		profMock.get("MO_AGENT_ID");
		profMockControl.setReturnValue(createPI("MO_AGENT_ID", ""),
				MockControl.ZERO_OR_MORE);

		profMock.get("AGENT_TIME_ZONE");
		profMockControl.setReturnValue(createPI("AGENT_TIME_ZONE", "GMT-5:00"),
				MockControl.ZERO_OR_MORE);

		upMock.getDailyTotal();
		upMockControl.setReturnValue(new BigDecimal(20.0),
				MockControl.ZERO_OR_MORE);
	}
}