package dw;

import dw.mgreceive.MoneyGramReceiveTransactionTest;
import dw.mgreversal.ReversalTransactionTest;
import dw.mgsend.MoneyGramSendTransactionTest;
import dw.moneyorder.MoneyOrderTransactionTest;
import dw.billpayment.BillPaymentTransactionTest;
import dw.vendorpayment.VendorPaymentTransactionTest;
import junit.framework.Test;
import junit.framework.TestSuite;

public class DeltaWorksTestSuite extends TestSuite {
	public static Test suite() {
		TestSuite ts = new TestSuite();	
		ts.addTestSuite(MoneyGramReceiveTransactionTest.class);
		ts.addTestSuite(BillPaymentTransactionTest.class);
		ts.addTestSuite(MoneyGramSendTransactionTest.class);
		ts.addTestSuite(VendorPaymentTransactionTest.class);
		ts.addTestSuite(MoneyOrderTransactionTest.class);
		ts.addTestSuite(ReversalTransactionTest.class);
		return ts;
	}
}
