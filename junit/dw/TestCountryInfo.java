package dw;

import java.util.Hashtable;

import dw.model.adapters.CountryInfo;

/*
 * Canned test data for Country Info
 * (currently contains only Canada, Brazil, Poland, and Mexico)
 */
public class TestCountryInfo {

	private static TestCountryInfo instance = null;
	private static Hashtable countryTable;
	
	// private constructor
	private TestCountryInfo () {
		countryTable = new Hashtable();
		createRepresentativeSet();
	}

	public static TestCountryInfo getInstance() {
		if (instance == null) {
		 	instance = new TestCountryInfo();
		}
		return instance;
	}
	
	public static CountryInfo getInfo(String country) {
		return (CountryInfo) countryTable.get(country);
	}

	private void createRepresentativeSet() {
		CountryInfo info;
		
		// case 1:  only one receive cash option
		info = new CountryInfo();
		info.setCountryCode("CAN");
		info.setCountryName("Canada");
		info.setCountryLegacyCode("CD");
		info.setSendActive("true");
		info.setReceiveActive("true");
		info.setPrettyName("Canada");
		countryTable.put(info.getCountryName(), info);
		
		// case 2:  2 delivery options; 1 cash and 1 account
		info = new CountryInfo();
		info.setCountryCode("BRA");
		info.setCountryName("Brazil");
		info.setCountryLegacyCode("BR");
		info.setSendActive("true");
		info.setReceiveActive("true");
		info.setPrettyName("Brazil");
		countryTable.put(info.getCountryName(), info);

		// case 3:  4 delivery options; 3 cash and 1 account
		info = new CountryInfo();
		info.setCountryCode("POL");
		info.setCountryName("Poland");
		info.setCountryLegacyCode("PL");
		info.setSendActive("true");
		info.setReceiveActive("true");
		info.setPrettyName("Poland");
		countryTable.put(info.getCountryName(), info);		
	}
}
