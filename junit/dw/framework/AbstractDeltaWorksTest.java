package dw.framework;

import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Iterator;
import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;
import javax.swing.JFrame;

import junit.framework.TestCase;
import dw.comm.AgentConnect;
import dw.framework.ClientTransaction;
import dw.framework.CommCompleteInterface;
import dw.i18n.Resources;
import dw.install.InstallFlowPage;
import dw.install.InstallTransaction;
import dw.io.CircularFile;
import dw.io.State;
import dw.main.DeltaworksMainPanel;
import dw.main.DeltaworksStartup;
import dw.mgsend.MoneyGramClientTransaction;
import dw.mgsend.MoneyGramSendTransaction;
import dw.model.adapters.MultiAgentInfo;
import dw.multiagent.MultiAgentTransaction;
import dw.profile.Profile;
import dw.profile.ProfileItem;
import dw.profile.ProfileLoader;
import dw.profile.UnitProfile;
import dw.profile.UnitProfileInterface;
import dw.simplepm.ProfileChangeBuilder;
import dw.simplepm.SimplePMTransaction;
import dw.utility.DWValues;
import dw.utility.ReceiptTemplates;

public abstract class AbstractDeltaWorksTest extends TestCase {
	
	private static State state;
	private static InstallTransaction it;
	private static InstallFlowPage ifp;
	private static DeltaworksMainPanel mainPanel;
	
	private static String currentProfile = null;
	
	private static Object wait;
	

	static {
		AgentConnect.enableDebugEncryption();
		System.out.println("AgentConnect.enableDebugEncryption() called in static code");
	}
	
	protected String getEnvironmentUrl(String environment) {
		return "https://" + environment.toLowerCase() + "ws.qa.moneygram.com/ac2/services/";
	}
	
	protected String getReAuthUrl(String environment) {
		return "https://" + environment.toLowerCase() + "ws.qa.moneygram.com/ac2web/servlet/ReauthServlet";
	}
	
	private void init() throws Exception {
		
		if (it == null) {
	    	State state = State.getInstance();
	    	System.out.println("before createProfile");
	    	state.createProfile();
	    	System.out.println("after createProfile");

			it = new InstallTransaction("TEST");
	    	System.out.println("after new InstallTransaction");
			
			ifp = new InstallFlowPage(it, "install/AgentSetupPage1.xml", "", "", null) {
				public void start(int direction) {
					System.out.println("start: direction = " + direction);
				}
				public void finish(int direction) {
					System.out.println("finish: direction = " + direction);
				}
			    public void commComplete(int commTag, Object returnValue) {
					System.out.println("commComplete: commTag, returnValue = " + commTag + ", " + returnValue);
			        synchronized(wait) {
			        	wait.notifyAll();
			        }
			    }
			};
		}
	}

	protected void loadDemoProfile() throws Exception {
		
		if (currentProfile.endsWith("DEMO")) {
			return;
		}
		
		init();
		
        wait = new Object();
		it.getInitialProfile("DEMO", "DEMO", ifp);
		
        synchronized(wait) {
        	try {
        		wait.wait();
        	} catch (Exception e) {
        	}
        }
        
        currentProfile = "DEMO";
	}
	
	protected void loadProfile(String environment, String deviceId, String profileId, String password) throws Exception {
		
		if ((currentProfile != null) && (currentProfile.equals(deviceId))) {
			return;
		}
		//transmit() ;
		
    	init();
    	
    	// Re-auth the profile Id.
		
		reAuth(profileId, getReAuthUrl(environment));

		// Load the profile for the specific device Id.
		
		wait = new Object();
		it.setAlternateUrl(getEnvironmentUrl(environment));
		it.getInitialProfile(deviceId, password, ifp);
		
        synchronized(wait) {
        	try {
        		wait.wait();
        	} catch (Exception e) {
        	}
        }
        
        // Perform a transmit.
        
        //transmit();

    	currentProfile = deviceId;
	}
	
	private boolean reAuth(String profileId, String url) throws Exception {
		
		// https://q2ws.qa.moneygram.com/ac2web/servlet/ReauthServlet?UNIT_PROFILE_ID=5865&ID_TYPE=POS_ID
		
		URL obj = new URL(url);
		
		System.out.println("Attemping to reauthorize profile " + profileId + " on " + url);

		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
		System.out.println("con = " + con);

		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", "Mozilla/5.0");
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
 
		String urlParameters = "UNIT_PROFILE_ID=" + profileId + "&ID_TYPE=POS_ID";
 
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
 
		int responseCode = con.getResponseCode();
		System.out.println("responseCode = " + responseCode);
		InputStream is = con.getInputStream();
		System.out.println("is = " + is);
		InputStreamReader isr = new InputStreamReader(con.getInputStream());
		System.out.println("isr = " + isr);
		BufferedReader in = new BufferedReader(isr);
		String line;
 
		StringBuffer response = new StringBuffer(); 
		while ((line = in.readLine()) != null) {
			System.out.println(line);
			response.append(line);
		}
		in.close();
		System.out.println("ReAuth complete");
		
		String searchText = "Unit Profile ID " + profileId.trim() + " re-authorized";
		return (response.toString().indexOf(searchText) != -1);
	}
	
	protected void transmit() throws Exception {
		
		DeltaworksMainPanel dwmp = startDeltaWorks();
//		dwmp.setupFrameComponents();
//		MoneyGramSendTransaction mgst = new MoneyGramSendTransaction(Resources.getString("DeltaworksMainPanel.Bill_Payment_117"), ClientTransaction.RECEIPT_PREFIX_BILL_PAYMENT, DWValues.BILL_PAYMENT_DOC_SEQ, true);
		ReceiptTemplates.initialize();
		MoneyGramSendTransaction.populateSendReceiptLanguageList();
		UnitProfile.getInstance().setSoftwareVersionPending(false);
		dwmp.transmitAll(false);
	}
	
	protected DeltaworksMainPanel startDeltaWorks() throws Exception {
    	//DeltaworksStartup.junitSetup();
        JFrame f = new JFrame();
        f.getContentPane().setLayout(new BorderLayout());
        mainPanel = DeltaworksMainPanel.create(f);
        return mainPanel;
	}
	
	protected Properties getPropertiesFile(String fileName) throws Exception {
		Properties properties = new Properties();
		FileInputStream fis = new FileInputStream(fileName);
		properties.load(fis);
		return properties;
	}
	
//	protected void dumpProfile() {
//		dumpProfile(UnitProfile.getInstance().getProfileInstance(), null);
//		
//		SimplePMTransaction spt = new SimplePMTransaction("DUMP");
//		ProfileChangeBuilder pcb = new ProfileChangeBuilder(spt);
//		String xml = pcb.build(UnitProfile.getInstance().getProfileInstance());
//		System.out.println(xml);
//	}
//	
//	private void dumpProfile(Profile profile, String parent) {
//		Iterator i = profile.iterator();
//		while (i.hasNext()) {
//			Object o = i.next();
//			if (o instanceof Profile) {
//				Profile p = (Profile) o;
//				String tag = p.getTag();
//				dumpProfile(p, (parent != null ? parent + "." : "") + tag);
//			} else if (o instanceof ProfileItem) {
//				ProfileItem pi = (ProfileItem) o;
//				String value = pi.stringValue();
//				String mode = pi.getMode();
//				String tag = pi.getTag();
//				String curr = pi.getCurr();
//				System.out.println((parent != null ? parent + "." : "") + tag + "=" + mode + "." + (curr != null ? curr : "") + "." + value);
//			}
//		}
//	}
}
