package dw.framework;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Random;

import dw.utility.Debug;

public class TestUtility {

	private static TestUtility instance;
	private static Properties prop;
	final static Object lock = new Object();
	private static String currentProcess; 
	
	private TestUtility()
	{
		loadTestData();
	}
	
	public static TestUtility getInstance()
	{
		if(instance==null)
		{
			instance = new TestUtility();
		}
		return instance;
	}
	
	
	
	private void loadTestData()
	{
		prop = new Properties();
		InputStream in = getClass().getResourceAsStream("/resources/testdata.properties");
		
		try {
			prop.load(in);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static String getData(String key)
	{
		if(prop==null)
		{
			getInstance().loadTestData();
		}
		return prop.getProperty(key);
		
	}

	/**
	 * @return the currentProcess
	 */
	public static String getCurrentProcess() {
		return currentProcess;
	}
	
	
	
	/**
	 * @param currentProcess the currentProcess to set
	 */
	public static void setCurrentProcess(String currentProcess) {
		TestUtility.currentProcess = currentProcess;
	}
	
	public static void lock()
	{
		synchronized (lock) {
			try {
				lock.wait();
			} catch (InterruptedException ie) {
				
			}
		}
	}
	
	public static void unLock()
	{
		synchronized (lock) {
            lock.notify();
         }
	}
	
	public static void startTestLog(String testName)
	{
		//Debug.debugTest("-------------------------------Starting Test "+testName+"----------------------------------------------");
	}
	
	public static void endTestLog(String testName)
	{
		//Debug.debugTest("-------------------------------Ending Test "+testName+"----------------------------------------------");
	}
	public static int getRandomAmt(int low,int high)
	{
		Random random = new Random();
		
		int randonmInt = random.nextInt(high-low) + low;
		
		return randonmInt;

	}
}
