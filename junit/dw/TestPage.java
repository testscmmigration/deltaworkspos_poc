package dw;

import dw.framework.PageFlowButtons;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import dw.mgsend.MGSHistoryPage;
/*
 * Canned test data for a MGSHistoryPage
 * (currently applicable for only MSFQWizardPage2 and MGSWizardPage5a)
 */
public class TestPage implements MGSHistoryPage {

	JTable table;
	DefaultTableModel tableModel;
	PageFlowButtons pfButtons;
	
	private JPanel parent;
	private JButton[] buttonArray;
	private String[] buttonNames;
	
	public TestPage() {
		this(new JTable(), new DefaultTableModel());
	}
	
	public TestPage(JTable table, DefaultTableModel tableModel,
			PageFlowButtons pfButtons) {
		super();
		this.table = table;
		this.tableModel = tableModel;
		this.pfButtons = pfButtons;
	}
	
	public TestPage(JTable table, DefaultTableModel tableModel) {
		super();
		this.table = table;
		this.tableModel = tableModel;
		// create a default set of buttons
		buttonArray = createButtons();
		buttonNames = createButtonNames();
		parent = new JPanel();
		addButtons(buttonArray);
		// and create a PageFlowButtons structure
		this.pfButtons = new PageFlowButtons(buttonArray, buttonNames, parent);
	}

	// implements MGSHistoryPage interface
	public JTable getTable() {
		// default table for cash delivery options
		return table;
	}

	// implements MGSHistoryPage interface
	public DefaultTableModel getTableModel() {
		// default model underlying the cash delivery options
		return tableModel;
	}
	
	public PageFlowButtons getPageFlowButtons() {
		return pfButtons;
	}

	private JButton[] createButtons() {
		JButton[] buttons = new JButton[3];
		// default array of buttons
		buttons[0] = new JButton("1");
		buttons[1] = new JButton("2");
		buttons[2] = new JButton("3");
		return buttons;
	}

	private String[] createButtonNames() {
		String[] names = new String[3];
		// mocked array of strings for the button labels
		names[0] = new String("Back");
		names[1] = new String("Next");
		names[2] = new String("Cancel");
		return names;
	}

	public void addButtons(JButton[] buttons) {
		for (int i=0; i < buttons.length; i++) {
			parent.add(buttons[i]);
		}
	}
}
