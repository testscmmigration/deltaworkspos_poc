/*
 * Created on Jul 26, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package dw.moneyorder;

import java.math.BigDecimal;

import junit.framework.TestCase;
import dw.UnitProfileMockHelper;

/**
 * @author A121
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MoneyOrderTransactionTest extends TestCase {
	UnitProfileMockHelper theMock;
	
	
	private int MONEY_ORDER_DOC_SEQ = 1;
	private String RECEIPT_PREFIX_BILL_PAYMENT = "exp";
	private String RECEIPT_PREFIX_SEND = "mgs";
	private String RECEIPT_PREFIX_RECEIVE = "mgr";
	private String RECEIPT_PREFIX_REVERSAL = "mgstr";
	private String RECEIPT_PREFIX_MONEY_ORDER = "ofacError";
	private String RECEIPT_PREFIX_RECEIVER_REGISTRATION = "mgrr";
	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		doMockSetup();
	}
	
	protected void doMockSetup() throws Exception {
		theMock = new UnitProfileMockHelper();
		theMock.setup();
	}
	
	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
		theMock.teardown();
	}
	
	
	
	public void test() throws Exception {
		theMock.setupWalmartAgent();
		theMock.setupProduct(MONEY_ORDER_DOC_SEQ, "1");
		theMock.replayMocks();
		
		assertEquals("60", "60");

	}
	
	public void testGetDiscountOptionPercentage() throws Exception {
		theMock.setupWalmartAgent();
		theMock.setupProduct(MONEY_ORDER_DOC_SEQ, "1");
		theMock.replayMocks();
				
        MoneyOrderTransaction mot = new MoneyOrderTransaction40("Money Order ",
        		RECEIPT_PREFIX_MONEY_ORDER, MONEY_ORDER_DOC_SEQ);
		
        System.err.println(mot.getDiscountOptionPercentage());
		assertEquals(new BigDecimal("60"), mot.getDiscountOptionPercentage());

	}
	
//    public BigDecimal getDiscountOptionPercentage(){
//        BigDecimal discount = ZERO;
//        ProfileItem item = getProductProfileOption("DISCOUNT_PERCENTAGE");	//$NON-NLS-1$
//        if (item != null){
//            discount = item.bigDecimalValue();
//        }
//        return discount;
//    }
  
}
