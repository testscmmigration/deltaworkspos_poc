package dw;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import dw.i18n.FormatSymbols;
import dw.model.Element;
import dw.model.adapters.CountryInfo;
import dw.model.adapters.FeeMsgMC;
import dw.model.adapters.QualifiedDeliveryOption;
import dw.profile.UnitProfile;
import dw.profile.UnitProfileInterface;
import dw.utility.DWValues;
/*
 * Canned test data for Qualified Delivery Options
 * (currently contains delivery options for Canada, Brazil, Poland, and Mexico,)
 */
public class TestQdo {

	private static TestQdo instance = null;
	private static Hashtable qdoTable;
	private static UnitProfileInterface mockProfile;

	DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.getDefault());
	char decimalSeperator = dfs.getDecimalSeparator();
	
	// private constructor
	private TestQdo () {
		UnitProfile.setMOCK(mockProfile);
		qdoTable = new Hashtable();
		createRepresentativeSet();
	}

	public static TestQdo getInstance() {
		if (instance == null) {
		 	instance = new TestQdo();
		}
		return instance;
	}

	public static QualifiedDeliveryOption getQdo(String option) {
		return (QualifiedDeliveryOption) qdoTable.get(option);
	}

	//notes: test is set up for send amount of $100.00
	public static FeeMsgMC[] getFeesForSendOptions(String countryId) {
		FeeMsgMC[] fees = new FeeMsgMC[0];  // empty array
		List feeList = new ArrayList();
		List keyLookup = new ArrayList();
		Set qdoKeys = qdoTable.keySet();
		
		
		if ((CountryInfo) TestCountryInfo.getInfo(countryId) != null
				&& qdoKeys.size() > 0) {
			
			Enumeration keysEnumeration = qdoTable.keys();		
			while (keysEnumeration.hasMoreElements()) {
				String keyName = (String) keysEnumeration.nextElement();
				if (keyName.startsWith(countryId)) {
					keyLookup.add(keyName);
				}
			}
			Collections.sort(keyLookup);

			// for each country match add the corresponding delivery option
			Iterator it = keyLookup.iterator();
			while (it.hasNext()) {
				String key = (String) it.next();
				// add matching element;
				feeList.add(getFeeForSend100(key));
			}	
			fees = (FeeMsgMC[]) feeList.toArray(new FeeMsgMC[feeList.size()]);
		}
		return fees;
	}
	
	// specifically for $100;
	public static FeeMsgMC getFeeForSend100(String qdoId) {
		return getFeeForSend(qdoId, "100.00");
	}

	// generalized amount - i.e., double
	public static FeeMsgMC getFeeForSend(String qdoId, double amount) {
		String convertedAmount = "100.00";
		
		try {
			String agent = UnitProfile.getInstance().get("AGENT_COUNTRY",
					"UNITED STATES");
			String send = new Double(amount).toString();
			DecimalFormat formatter = new DecimalFormat("###,###,###.00");
			convertedAmount = formatter.format(amount);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return getFeeForSend(qdoId, convertedAmount);
	}
	
	// fees calculated for $100 as test data setup
	public static FeeMsgMC getFeeForSend(String qdoId, String amount) {
		FeeMsgMC fee;
		List eList = new ArrayList();
		QualifiedDeliveryOption qdo = getQdo(qdoId);
		fee = new FeeMsgMC();

		fee.setProductType("SEND");
		fee.setDeliveryOption(qdo.getDeliveryOption());
		fee.setReceiveAgentAbbreviation(qdo.getRecieveAgentAbbreviation());

		Element append = new Element(DWValues.MF_SEND_AMT);
		fee.getModel().add(new Element(DWValues.MF_SEND_AMTS));
		append.setText(amount);
		eList.add(append);
		fee.getModel().getChild(DWValues.MF_SEND_AMTS).addChildList(eList);
		fee.setDeliveryOptId(qdo.getDeliveryOptionID());

		// note: for retrieval need child element under DWValues.MF_RECV_AMTS
		fee.getModel().add(new Element(DWValues.MF_RECV_AMTS));
		eList.clear();
		// note: needs child element DWValues.MF_RECV_CURRENCY
		append = new Element(DWValues.MF_RECV_CURRENCY);
		append.setText(qdo.getReceiveCurrency());
		eList.add(append);
		Element appendValid = new Element(DWValues.MF_VALID_CURRENCY);
		appendValid.setText("true");
		eList.add(appendValid);
		fee.getModel().getChild(DWValues.MF_RECV_AMTS).addChildList(eList);

		if (qdoId.equals("Canada_1")) {
			fee.setValidExchangeRate("1.0596");
			fee.setEstimatedExchangeRate("1.0596");
			fee.setEstimatedReceiveCurrency("CAD");
			
			// calculate fee ($12.50 for $100.00)
			fee.setFeeSendTotalWithTax("12.50");

		} else if (qdoId.equals("Brazil_1")) {
			fee.setValidExchangeRate("2.1060");
			fee.setEstimatedExchangeRate("2.1060");
			fee.setEstimatedReceiveCurrency("BRL");
			
			// calculate fee ($9.99 for $100.00)
			fee.setFeeSendTotalWithTax("9.99");	
		} else if (qdoId.equals("Brazil_2")) {
			fee.setValidExchangeRate("2.1362");
			fee.setEstimatedExchangeRate("2.1362");
			fee.setEstimatedReceiveCurrency("BRL");
			
			// calculate fee ($7.99 for $100.00)
			fee.setFeeSendTotalWithTax("7.99");				
		} else if (qdoId.equals("Poland_1")) {
			fee.setValidExchangeRate("2.9893");
			fee.setEstimatedExchangeRate("2.9893");
			fee.setEstimatedReceiveCurrency("PLN");
			
			// calculate fee ($9.99 for $100.00)
			fee.setFeeSendTotalWithTax("9.99");
		} else if (qdoId.equals("Poland_2")) {
			fee.setValidExchangeRate("0.7110");
			fee.setEstimatedExchangeRate("0.7110");
			fee.setEstimatedReceiveCurrency("EUR");
			
			// calculate fee ($9.99 for $100.00)
			fee.setFeeSendTotalWithTax("9.99");
		} else if (qdoId.equals("Poland_3")) {
			fee.setValidExchangeRate("1.0000");
			fee.setEstimatedExchangeRate("1.0000");
			fee.setEstimatedReceiveCurrency("USD");
			
			// calculate fee ($9.99 for $100.00)
			fee.setFeeSendTotalWithTax("9.99");
		} else if (qdoId.equals("Poland_4")) {
			fee.setValidExchangeRate("2.9893");
			fee.setEstimatedExchangeRate("2.9393");
			fee.setEstimatedReceiveCurrency("PLN");
			
			// calculate fee ($9.99 for $100.00)
			fee.setFeeSendTotalWithTax("9.99");
		}
		return fee;
	}

	private void createRepresentativeSet() {
		QualifiedDeliveryOption qdo;

		/* case 1:  only one receive cash option
		 * 
		 */
		qdo = new QualifiedDeliveryOption(); // root element "QDO"
		qdo.setDeliveryOption("WILL_CALL");
		qdo.setReceiveAgentID("");
		qdo.setDeliveryOptionDisplayName("10 Minute Service");
		qdo.setDeliveryOptionID("0");
		qdo.setRecieveAgentAbbreviation("");
		// add child element
		qdo.getModel().add(new Element(DWValues.MF_RECV_CURRENCY));
		qdo.setReceiveCurrency("CAD");
		qdo.setPayoutCurrency("CAD");
		qdoTable.put("Canada_1", qdo);
		
		/* case 2:  2 delivery options; 1 cash and one account option
		 * 
		 */
		qdo = new QualifiedDeliveryOption();
		qdo.setDeliveryOption("WILL_CALL");
		qdo.setReceiveAgentID("67126406");
		qdo.setDeliveryOptionDisplayName("10 Minute Service");
		qdo.setDeliveryOptionID("0");
		qdo.setRecieveAgentAbbreviation("");
		qdo.getModel().add(new Element(DWValues.MF_RECV_CURRENCY));
		qdo.setReceiveCurrency("BRL");
		qdo.setPayoutCurrency("BRL");
		qdoTable.put("Brazil_1", qdo);
		
		qdo = new QualifiedDeliveryOption();
		qdo.setDeliveryOption("BANK_DEPOSIT");
		qdo.setReceiveAgentID("67126406");
		qdo.setDeliveryOptionDisplayName("Account Deposit");
		qdo.setDeliveryOptionID("10");
		qdo.setRecieveAgentAbbreviation("ALL BANKS");
		qdo.getModel().add(new Element(DWValues.MF_RECV_CURRENCY));
		qdo.setReceiveCurrency("BRL");
		qdo.setPayoutCurrency("BRL");
		qdoTable.put("Brazil_2", qdo);

		/* case 3:  4 delivery options; 3 cash and one account option
		 *   3 cash are options for PLN, EUR, USD
		 *   and 1 bank deposit option for PLN
		 */
		qdo = new QualifiedDeliveryOption();
		qdo.setDeliveryOption("WILL_CALL");
		qdo.setReceiveAgentID("");
		qdo.setDeliveryOptionDisplayName("10 Minute Service");
		qdo.setDeliveryOptionID("0");
		qdo.setRecieveAgentAbbreviation("");
		// add child element
		qdo.getModel().add(new Element(DWValues.MF_RECV_CURRENCY));
		qdo.setReceiveCurrency("PLN");
		qdo.setPayoutCurrency("PLN");
		qdoTable.put("Poland_1", qdo);

		qdo = new QualifiedDeliveryOption();
		qdo.setDeliveryOption("WILL_CALL");
		qdo.setReceiveAgentID("");
		qdo.setDeliveryOptionDisplayName("10 Minute Service");
		qdo.setDeliveryOptionID("0");
		qdo.setRecieveAgentAbbreviation("");
		// add child element
		qdo.getModel().add(new Element(DWValues.MF_RECV_CURRENCY));
		qdo.setReceiveCurrency("EUR");
		qdo.setPayoutCurrency("EUR");
		qdoTable.put("Poland_2", qdo);

		qdo = new QualifiedDeliveryOption();
		qdo.setDeliveryOption("WILL_CALL");
		qdo.setReceiveAgentID("");
		qdo.setDeliveryOptionDisplayName("10 Minute Service");
		qdo.setDeliveryOptionID("0");
		qdo.setRecieveAgentAbbreviation("");
		// add child element
		qdo.getModel().add(new Element(DWValues.MF_RECV_CURRENCY));
		qdo.setReceiveCurrency("USD");
		qdo.setPayoutCurrency("USD");
		qdoTable.put("Poland_3", qdo);

		qdo = new QualifiedDeliveryOption();
		qdo.setDeliveryOption("BANK_DEPOSIT");
		qdo.setReceiveAgentID("45811144");
		qdo.setDeliveryOptionDisplayName("Account Deposit");
		qdo.setDeliveryOptionID("10");
		qdo.setRecieveAgentAbbreviation("ALL BANKS");
		qdo.getModel().add(new Element(DWValues.MF_RECV_CURRENCY));
		qdo.setReceiveCurrency("PLN");
		qdo.setPayoutCurrency("PLN");
		qdoTable.put("Poland_4", qdo);
	}		
}
