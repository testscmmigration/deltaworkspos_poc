/*
 * Created on Jul 26, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package dw.vendorpayment;

import junit.framework.TestCase;
import dw.UnitProfileMockHelper;


/**
 * @author A121
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class VendorPaymentTransactionTest extends TestCase {
	UnitProfileMockHelper theMock;
	
	
	private int VENDOR_PAYMENT_DOC_SEQ = 2;
	/**
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		doMockSetup();
	}
	
	protected void doMockSetup() throws Exception {
		theMock = new UnitProfileMockHelper();
		theMock.setup();
	}
	
	/**
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
		theMock.teardown();
	}
	
	public void test() throws Exception {
		theMock.setupWalmartAgent();
		theMock.setupProduct(VENDOR_PAYMENT_DOC_SEQ, "2");
		theMock.replayMocks();
		
		assertEquals("60", "60");

	}
  
}
